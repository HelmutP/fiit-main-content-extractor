# Main Content Extractor #

## What is it? ##
Main Content Extractor is application programming interface (API) for extracting main content from web pages. API is specialized for web pages containing some coherent text (as news etc.) in some latin language. API is accessible on http://mceapi.com.

## How can it be used? ##
Output can be used for recommendation, text processing or you can display it on small displays instead of full web page.
For example, from some news article, application extract the main text content (no advertisments, navigation, images, videos, webpage structure ...).

## Technology ##
Ruby on Rails

## More info about MCE ##
Contact me by helmutposch4@gmail.com if you want more information (how it works etc.). You can also improve MCE by your own ideas but I would be happy if you contact me before.
