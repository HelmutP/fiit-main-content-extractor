class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  require 'builder'
  require 'json'
  require 'net/http'
  require 'nokogiri'
  require 'open-uri'
  require 'mechanize'
  require 'sanitize'
  require 'uri'
  require 'cgi'
  require 'words_counted'
end
