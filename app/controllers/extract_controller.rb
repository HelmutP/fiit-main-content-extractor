class ExtractController < ApplicationController

  def initialize
    @final_content = String.new('')
    @success = false
    @web_page
    @format
    @url = String.new('')
  end

  def get_options
    #begin
      myurl = request.fullpath.to_s

      second_part_url = myurl.split("&url=")
      encoded_url = second_part_url[0]
     
      uri_params = CGI.parse(URI.parse(encoded_url).query)
      @format = uri_params['form'][0].to_s

      second_part_url[1...second_part_url.length].each do |part|
        if second_part_url[second_part_url.length-1] != part
          @url = @url + part.to_s + "&url="
        else
          @url = @url + part.to_s
        end
      end

      @url = Webpage.invalid_url(@url)
      puts @url
      if (@format.eql?("xml") == false && @format.eql?("json") == false) || @url == nil || @url.length == 0
        puts "Error: invalid option or connection error."
        return false
      else
        @web_page = Webpage.new(@url)
        return true
      end
    #rescue
     #puts "Error: fatal error during processing options."
     # return false
    #end
  end

  def index
  	if get_options
      begin
        @web_page.content = @web_page.extract_content
        if @web_page.content.length > 0
          @final_content = @web_page.get_final_content
          @success = true
        else
          raise 'Error: content not found!'
        end
        @web_page.node_hierarchy.eval_nodes_list.each do |node|
           puts "SOURCE #{node.source.to_s}"
           puts "CONTENT #{node.content.to_s}"
           puts "SCORE #{node.score.to_f}"
           puts "/////////////////////////////////////////////////"
           puts @web_page.node_hierarchy.avg_score
           puts "/////////////////////////////////////////////////"
         end
      rescue
        puts "Error: fatal error during processing."
        @success = false
      end
    end
  	@output = Output.new(@format,@success,@url,@final_content)
  end
end
