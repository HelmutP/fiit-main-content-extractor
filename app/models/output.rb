class Output

	attr_accessor :data

	def initialize type, success, url, content
		@type = type
		@success = success
		@url = url
		@content = content

		if @type == 'xml'
			create_xml
		else
			create_json
		end
	end

	def create_xml
		begin
		  @builder = Nokogiri::XML::Builder.new do |xml|
          	xml.output{
            	xml.success @success.to_s
            	xml.url @url.to_s
            	xml.content @content.to_s
          	}
          end
        rescue
          	puts "Error: can not create output."
        end
          @data = @builder.to_xml
	end

	def create_json
		@data = {:success => @success.to_s, :url => @url.to_s, :content => @content.to_s}.to_json
	end

	private :create_json, :create_xml
end






















