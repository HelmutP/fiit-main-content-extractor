class Webpage

	attr_accessor :url, :original_html, :content, :node_hierarchy

	def initialize url
		@url = url
		@content
		@original_html = Nokogiri::HTML(open(@url).read)
		@node_hierarchy = Node_hierarchy.new(Nokogiri::HTML(@original_html.to_s).css('body'))
	end

  	def get_final_content #uprava vystupu
  		return @content.gsub("\n",' ').tr_s(' ',' ')
  	end

  	def self.invalid_url url# kontrola konektivity so zadanou url
	    begin
	      url.sub!(/^https/, "http") # s https to nefunguje
	      uri = URI(url)
	      rq = Net::HTTP.new uri.host
	      response = rq.request_head uri.path
	      return url
	    rescue
	      begin
	      	urlup = "http://" + url
	      	rq = Net::HTTP.new urlup.host
	      	response = rq.request_head urlup.path
	      	return urlup
	      rescue
	      	puts "Error: connection failed."
	      	return nil
    	  end
    	end

  	end

  	def extract_content # najdenie uzlu s najvacsim skore 
		sum = 0
		not_null = 0
		extracted_content = String.new()
		extracted_content_tmp = String.new()

		@node_hierarchy.eval_nodes_list.each do |node| # vypocet priemernej hodnoty uzlov
			sum += node.score.to_f
			not_null += 1
		end
		not_null = 1 if not_null == 0
		@node_hierarchy.avg_score = sum.to_f / not_null.to_f

		max_score = -1
		max_div = nil
		potencial_nodes = Array.new()
		@node_hierarchy.eval_nodes_list.each do |node| #vyber nadpriemernych uzlov
			if node.score.to_f >= @node_hierarchy.avg_score.to_f
				if node.score.to_f > max_score
					max_score = node.score.to_f
					max_div = node
					extracted_content_tmp = extracted_content_tmp + node.content.to_s 
				end
				potencial_nodes[potencial_nodes.length] = node
			end
		end

		min_full_part = nil
		min_length = 99999999

		puts max_div.full_source.to_s
		@node_hierarchy.all_nodes_list.each do |part| #najdenie skupiny divov stranky, ktora je priamym rodicom najlepsieho divu
			if (part.to_s.length < min_length && max_div.full_source.to_s.length != part.to_s.length)
				if part.to_s.include? max_div.full_source.to_s
					min_full_part = part
					min_length = part.to_s.length
				end
			end
		end

		allowed_divs = Nokogiri::HTML(min_full_part.to_s).css('div') #rozdelenie priameho rodica na vsetky poddivy
		potencial_nodes.each do |potnode| #hladanie vsetkych nadrpiemernych divov ci su dietatom priameho rodica najlepsieho divu
			allowed_divs.each do |allown|
				if potnode.full_source.to_s == allown.to_s
					extracted_content = extracted_content + potnode.content.to_s 
					break
				end
			end
		end

		if extracted_content.to_s.length > 0
			return extracted_content.to_s
		else
			return extracted_content_tmp.to_s
		end
	end
end