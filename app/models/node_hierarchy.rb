class Node_hierarchy

	attr_accessor :eval_nodes_list, :avg_score, :all_nodes_list

	def initialize source
		@eval_nodes_list = Array.new() # len nenulovo ohodnotene divy (bez deti)
		@all_nodes_list = Array.new() #vsetky mooznosti vnorenia divov na webe
		create_hierarchy source
		@avg_score = -1
	end

	def create_hierarchy source # hlada a pridava divy do zoznamu
		return_array = Array.new(source.css('div'))
		return_array.each do |div|
			@all_nodes_list[@all_nodes_list.length] = div.to_s
		end
		return_array.each do |div|
			add_to_list(div)
		end
	end

	def add_to_list source # vytvorenie, ohodnotenie a pridanie uzlu do zoznamu
		full_source = source
		source.css("div").each {|i| i.remove} if source.css('div').length > 0
		n = Node.new(full_source, source)
		n.evaluate
		@eval_nodes_list[@eval_nodes_list.length] = n if n.score.to_f > 0.0
	end

	private :create_hierarchy, :add_to_list
end