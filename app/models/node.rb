class Node

	attr_accessor :content, :source, :score, :full_source

	def initialize full_source, source
		#zabezpecenie spravneho kodovania
		full_source = full_source.to_s.encode("UTF-16be", :invalid=>:replace, :replace=>"?").encode('UTF-8') if ! full_source.to_s.valid_encoding?
		source = source.to_s.encode("UTF-16be", :invalid=>:replace, :replace=>"?").encode('UTF-8') if ! source.to_s.valid_encoding?

		@full_source = full_source # aj s detskymi divmi atd
		@source = delete_style_script_tags source #samotny priamy obsah divu s html kodom
		@content = delete_all_html_tags @source #priamy obsah divu bez html
		@score = -1 # default aby nebol nil
	end

	def delete_style_script_tags source # odstrani script a style tagy z obsahu divu (source)
		html_doc = Nokogiri::HTML(source.to_s)
		html_doc.css("script,style").each{|i| i.remove}
		html_doc = Nokogiri::HTML(html_doc.to_s) # obnovenie len dokumentu aby sa spravne mohol vratit
		return html_doc.css('div')[0] # hlavny div vyberieme
	end

	def delete_all_html_tags source
		return Sanitize.fragment(source.to_s).to_s # odstrani vsetky tagy
	end

	def evaluate
		# vypocet realnej dlzky obsahu divu
		content_real_length = (@content.to_s.delete " ").length
		
		#dlzka anchor textu v casti
		anchor_text = (@source.css('a').text.to_s)
		anchor_text = delete_all_html_tags anchor_text
		anchor_length = anchor_text.to_s.length

		# vypocet poctu ciarok v obsahu mimo linkov
		content_commas_num = (@content.to_s.count ",").to_f - (@source.css('a').text.to_s.count ",").to_f

		# vypocet bodiek a ciarok v linkoch
		anchor_commas_fullstops_num = (@source.css('a').text.to_s.count ",.").to_f

		# prisposobenie absolutnej hodnoty score
		const = 100

		# zabezpecenie delenia nenulovou hodnotou
		anchor_commas_fullstops_num += 1
		anchor_length = 1 if anchor_length == 0
		if @content.to_s.length.to_f == 0.0
			content_lenght = 1.0
		else
			content_lenght = @content.to_s.length.to_f
		end

		# vypocet score
		@score = (((content_commas_num.to_f**3) * (content_real_length.to_f / anchor_length.to_f) ) / (anchor_commas_fullstops_num.to_f**2)) * const.to_f
	end
end