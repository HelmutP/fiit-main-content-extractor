
 SEDLISKÁ. Policajti zo Slovenskej Kajne (okres Vranov) vyšetrujú krádež stromov z lesa pri obci Sedliská. Zatiaľ neznámy páchateľ ukradol 7 dubov, 31 agátov, 7 javorov, 81 jaseňov, 5 líp, 12 bukov a 1 hrab. Celkový objem ukradnutej drevnej hmoty bol 15 metrov kubických a spôsobenú škodu vycíslili na 300 eur. Polícia zacala trestné stíhanie vo veci pre precin krádeže.  
