
 BRATISLAVA. Fond národného majetku (FNM) SR od 1. októbra umožnil všetkým obcanom bezodplatne previesť na úcet FNM takzvané "bezcenné" cenné papiere, ktoré im ostali z prvej vlny kupónovej privatizácie. Obcania sa tak v budúcnosti vyhnú plateniu poplatkov za ich evidenciu. Na dnešnej tlacovej konferencii o tom informoval predseda Výkonného výboru FNM Vladimír Kocourek. "Každý môže bez platenia poplatkov požiadať o vystavenie výpisu z úctu majiteľa cenných papierov a slobodne sa rozhodnúť, ci ich prevedie na FNM, alebo si ich ponechá a v budúcnosti bude mať povinnosť platiť poplatky za ich evidenciu v Centrálnom depozitári cenných papierov," uviedol Kocourek. Cenné papiere z prvej vlny kupónovej privatizácie vlastní takmer milión Slovákov, pricom mnohí z nich o tom ani nevedia. O bezodplatný prevod cenných papierov môžu obcania požiadať na vybraných pracoviskách Slovenskej pošty a spolocnosti RM-S Market. K dispozícii je 40 pracovísk v rámci celého Slovenska a obcan za ich prevod nebude platiť.  
