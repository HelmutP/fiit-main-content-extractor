
 Tréningový zoskok Felixa Baumgartnera z výšky 7600 m nad púšťou Mojave v Kalifornii v skafandri, v ktorom chce tento rok skociť z výšky 37 km – z hranice medzi atmosférou Zeme a vesmírom. Špeciálny padák, ktorého súcasťou je aj kyslíková bomba. Kapsula, v ktorej majú Baumgartnera vyniesť na okraj atmosféry.  
