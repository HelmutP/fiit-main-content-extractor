
 V medzivojnových rokoch mala Bratislava povesť tolerantného mesta.Z nepokojného Pressburgu – Požoňu – Prešporka do pokojnej Bratislavy však viedla spletitá cesta. Svoj k svojmu V prvých rokoch republiky mali ceskí prisťahovalci v Bratislave znacné existencné ťažkosti. Mali ich všetci, ale oni tu boli cudzí. Problémy boli so zásobovaním, pretože maďarské jednotky odchádzajúce zo slovenského územia odniesli nielen rušne a vagóny, ale aj množstvo dobytka a potravín. V meste bola drahota, ktorú zvyšovalo (podľa ich slov) umelé zvyšovanie cien ceskoslovenským zákazníkom. K ochrane pred drahotou založili Hospodářské a zásobovací spolecenstvo, s. r. o. (1919). Oprášili staré Palackého heslo „svoj k svojmu“ a vyzývali ceskoslovenské obcianstvo, aby podporovalo vlastných podnikateľov a bojkotovalo voci nim nie priateľsky naklonené podniky, obchody a služby: „jak naše inteligence může chodit do místností takového nejúhlavnějšího nepřítele naší národnosti, ať již je to do Štefanie, k Schmiedt-Hanselovi, Lerchnerovi, Kochovi…“ V dusnej atmosfére spolocenského vydeľovania si hľadali svoj priestor. Najprv to bola tzv. vládna  budova, dnešná filozofická fakulta na Gondovej ulici. Potom usilovali o Československý dom: „Bez vlastního domu pro soustředení ceskoslovenské spolecnosti se dá velmi těžko pracovati, není možno nalézti v celém městě koutek, kde by clověk byl skutecně mezi svými a kde by nově příchozí do mladého hlavního města měl možnost okamžitě navázati styky s ceskoslovenskou spolecností.“ Preto bol v roku 1919 zakúpený „velmi pěkný hotel s kavárnou, kabaretními místnostmi na hlavní třídě k nádraží...“ Bol to hotel Deák na konci Štefánikovej ulice, po ktorom dnes neostala ani stopa. Československá spolocnosť sa vtedy schádzala aj v reštaurácii U Jaklicov na Michalskej ulici, v Bláhovej reštaurácii na dnešnom Šafárikovom námestí, v Šulcovej reštaurácii na Dunajskej. V prvých rokoch republiky bolo treba ceskoslovenskú ideu neustále živiť, propagovať a upevňovať. Na politických, kultúrnych a spolocenských podujatiach sa republikánsky orientovaní Česi a Slováci snažili predstavovať ako jednotná komunita, ktorá má  spolocné národné záujmy. Za veľmi dôležitý považovali aktívny spolkový život. Historicka Elena Mannová dokonca napísala, že v poprevratovej psychóze sa z akéhosi pudu sebazáchovy rozpútala v meste spolková horúcka a v prvom desaťrocí republiky sa viaceré bratislavské spolky slovakizovali práve prostredníctvom Čechov. Požadovali, aby každý uvedomelý, republike verný obcan bol clenom ceskoslovenských spolkov a zúcastňoval sa na ich cinnosti. Osobitne vyzývali na úcasť na nemnohých slovenských podujatiach, napr. v roku 1920 sa docítame: „Na tomto koncertu nesmí chyběti žádný Čech, bychom prokázáli bratrům Slovákům, že máme porozumění pro jejich kulturní potřeby.“ Pritom však zdôrazňovali, že „je třeba, aby Slováci pochopili a osvojili si naší ceskou kulturní vyspělost“, alebo že „úkolem všech Čechů na Slovensku je, aby propagovali tady ceskou kulturu tak, aby přinášeli sem, co máme nejlepšího“. Pokracovanie nabudúce  
