
 VYSOKÉ TATRY. Že v horách možno prísť k úrazu ani sa nenazdáte, dokazuje pondelkový prípad poľskej turistky. Tá sa neďaleko Zeleného plesa pošmykla na zľadovatelom chodníku a poranila si nohy. Záchranári horskej služby po príchode na miesto 37-rocnú turistku ošetrili. Na snežnom skútri a neskôr terénnym vozidlom ju transportovali na Bielu vodu, kde ju cakala pripravená sanitka.  
