
 LIPTOVSKÝ HRÁDOK. Liptovský rezbár Dalibor Novotný má vrelý vzťah nielen k drevu, ale svoj cit pre krásu a jemnosť dokazuje výrobou kraslíc z rozlicných semien. Do záhradkárskych obchodov nevchádza s otázkou - co chcem sadiť, ale aké tvary a farby majú semená. S trpezlivosťou ucil na tvorivých dielňach na Mestskom úrade v Liptovskom Hrádku tomuto umeniu aj ženy.  
