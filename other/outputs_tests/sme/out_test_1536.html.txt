
 SOĽ. Na cintoríne v obci Soľ vo Vranovskom okrese vycíňali neznámi vandali. Na 35 hrobov poškodili náhrobné tabule, povytrhávali a porozbíjali svietniky na pomníkoch. Ako agentúru SITA informoval zástupca prešovskej policajnej hovorkyne Daniel Džobanik, škody zatiaľ nie sú vycíslené. Polícia v prípade zacala trestné stíhanie vo veci pre hanobenie miesta posledného odpocinku.    
