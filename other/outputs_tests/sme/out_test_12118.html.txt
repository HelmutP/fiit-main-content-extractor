
 BRATISLAVA. Slovenský hydrometeorologický ústav varuje pred rizikom povodní z trvalých dažďov v južných okresoch Slovenska. Výstraha prvého stupňa platí oddnes od 16.00 h do odvolania a týka sa povodia dolného Hrona, Ipľa, Slanej a povodia Hornádu a Hnilca. Pozrite si podrobnosti varovania na stránke SHMÚ  
