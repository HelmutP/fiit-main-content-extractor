
 Tak ako clovek na svoje prežitie potrebuje vhodné prostredie, aj živocíchy a rastliny vyžadujú územie. Ak napríklad vyzbierame medveďovi lesné plody, nemôžeme sa cudovať, že si od nás koncom zimy „príde požicať k nám domov".  
