
 Preventívna akcia žilinských policajtov a ich kolegýň, ktoré boli oblecené v ľudových krojoch. Vodicom dávali drobné darceky a vyzývali ich na ohľaduplnosť a opatrnosť na cestách pocas Veľkonocných dní. Netradicnú kontrolu zažili vodici 1. apríla 2010 na Zelený štvrtok na ceste I/18 z Martina do Žiliny pri Hyze.  
