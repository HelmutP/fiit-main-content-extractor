
 Kamarátka ma pozvala na koncert Slovenskej filharmónie. Pocúvať klasiku naživo, tak na to som sa tešil ešte pred svojím príchodom na Slovensko. Bol som naozaj rád, že pocúvam živý orchester, sedím v koncertnej sále v Európe, ktorá je kolískou klasickej hudby.  
