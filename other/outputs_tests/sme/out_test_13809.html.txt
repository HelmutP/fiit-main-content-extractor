
 Nevynucovanie pravidiel diskriminuje tých, ktorí ich dodržiavajú. Dlhodobo ich to dokonca z podnikania vytláca, lebo v takej konkurencii neobstoja. Aj preto sa ľuďom niekedy zdá, že „poctivý podnikateľ" je u nás oxymoron.  
