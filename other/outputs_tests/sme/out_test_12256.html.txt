
 Za posledné dni a týždne som toľko toho zažil, toľko videl, vstúpil do toľkých vzťahov, toľko udalostí som chcel komentovať. Ale žiada sa mi mlcaním postaviť aspoň malý múrik proti tej lavíne hovorenia, ktorá sa valí okolo nás a cez nás.  
