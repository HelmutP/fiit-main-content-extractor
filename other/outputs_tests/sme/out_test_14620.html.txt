
 Hausbóty sú obydlia na vode. Na jaroveckých ramenách Dunaja, za bratislavskou Petržalkou, je ich ukotvených niekoľko desiatok. Skromné s dušou, ale aj okázalé symboly solventných. Je to zaujímavý, romantický kúsok sveta. Páči sa mi tam.  
