

                                    June 29, 2007 -- More blackouts roiled Queens yesterday as anger at Con Ed boiled over. 
<P> Mother Nature was the culprit, the company said, blaming the blackouts on Wednesday night&#39;s storms. But that did little to restore confidence in the utility as it struggled to restore power to homes and businesses. 
<P> A fast-moving storm knocked out power for about 5,000 customers in Middle Village yesterday afternoon. Power was restored by early evening. 
<P> And as of yesterday evening, about 200 customers in South Ozone Park, Jamaica and Cambria Heights were still without power from Wednesday night&#39;s thunderstorms, the company said. 
<P> Less than 1,000 customers in parts of Westchester County, including Scarsdale, Rye, Rye Brook and White Plains were powerless, too. 
<P> Con Ed still could not explain the &quot;transmission disturbance&quot; Wednesday that knocked out power in the southwest Bronx and the Upper East Side for 48 tense minutes. 
<P> That incident threw the evening commute into chaos and further cast doubt on Con Ed&#39;s ability to cope with summer demand. 
<P> &quot;Engineers are examining possible links between the outage and lightning strikes in the area,&quot; the company said in a statement. 
<P> Con Ed is adamant that the outage wasn&#39;t sparked by power-hungry air conditioners during the heat wave. It said protective devices allowed power to be restored in less than an hour. 
<P> At a press conference in Astoria near the transmission substation that failed Wednesday, local politicians were still steaming mad at Con Ed. 
<P> &quot;We are left to hope for the best and prepare for the worst,&quot; said Assemblyman Mike Gianaris (D-Queens), whose district suffered through nine sweltering powerless days last August. 
<P> &quot;This is an unreliable, unaccountable monopoly that is running our utility system. We must stop coddling Con Ed.&quot; 
<P> Astoria business owners, with last year&#39;s blackout still fresh in their minds, had little confidence their lights would stay on for the rest of the summer. 
<P> Tahseen Meraj, owner of Copycom copy shop on 31st Street in Astoria, said he lost about &#36;20,000 in business from last year&#39;s blackout and couldn&#39;t understand all the power problems. 
<P> &quot;When we moved from India to America, I couldn&#39;t even imagine we would be sitting in a blackout,&quot; he said. 
<P> Earlier this month, Con Edison officials announced they had made more than &#36;90 million in improvements to the utility&#39;s distribution system in northwestern Queens.