

                                    August 21, 2007 -- ATLANTA - Wrestler Chris Benoit had a hormone disorder and needed the amount of steroids he was prescribed before he killed his wife, son and himself in June, an attorney for Benoit&#39;s doctor said in court papers yesterday. 
<P> A federal agent failed to tell the judge, who issued search warrants in the case, about the legitimate uses of steroids, according to a motion to suppress evidence against Dr. Phil Astin. 
<P> A spokesman for the U.S. Attorney&#39;s Office declined to comment. 
<P> Investigators have not given a motive for the killings, but the question of whether steroids played a role has lingered. Steroids were was found in Benoit&#39;s home, and tests showed he had roughly 10 times the normal level of testosterone in his system when he died. 
<P> Authorities have said Astin prescribed a 10-month supply of anabolic steroids to Benoit every three to four weeks between May 2006 and May 2007.