
						
						
McDonald's Corp. said Tuesday that it posted its second-ever loss -- its first in nearly five years -- losing $711.7 million in the second quarter because of a hefty one-time charge. 
<P>For the quarter ending June 30, the world's largest restaurant chain said its net income swung to a loss of 60 cents per share. That's compared to a second-quarter profit of $834.1 million or 67 cents per share, a year earlier. 
<P>Tuesday's report mirrored McDonald's pre-announced earnings results. 
<P>McDonald's only other quarterly loss, caused by a drawn-out sales slump, was during the fourth quarter of 2002. 
<P>Excluding the non-cash impairment charge of $1.6 billion relating to the sale of some Latin American operations, the Oak Brook-based company said its net income grew about 4 percent to $869.9 million, or 71 cents per share. 
<P>''We continue to increase our relevance to busy consumers by delivering choice, variety and convenience that our customers have come to expect from McDonald's,'' Chief Executive Jim Skinner said in a statement. ''Our business around the world is strong, and the energy, alignment and commitment behind enhancing the McDonald's brand have never been better.'' 
<P>Revenue climbed 12 percent to $6 billion, from $5.36 billion, spurred by strong breakfast sales and robust business in stores open at least a year. 
<P>On average, analysts surveyed by Thomson Financial forecast earnings of 71 cents per share and revenue of $5.9 billion. 
<P>Those forecasts typically exclude one-time items. 
<P>In April, McDonald's announced a plan to cede control of 1,600 restaurants in 18 Latin American and Caribbean countries in an effort to strengthen profit. While the deal includes a $1.31 per share charge, it reduces the company's financial exposure in a challenging region and will net McDonald's about $700 million in cash, which it said will be used to increase share buybacks and dividends. 
<P>McDonald's said Tuesday that it repurchased $664 million of its stock during the quarter. 
<P>McDonald's shares fell 50 cents, or 1 percent, to $52.00 in pre-market trading Tuesday.