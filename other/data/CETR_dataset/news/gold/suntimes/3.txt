
						
						



Poonam Desai said the procedure was no big deal and she'd happily do it again.<p>

She insists she is not a hero. <p>

But the 47-year-old man who was dying of leukemia before Desai, of the South Loop, donated her bone marrow to him last year, says otherwise. <p>

The man survived.<p>

Desai, whose parents are from India, is among the relatively few bone marrow donors of South Asian origin nationwide. The need is so great that this summer there is a national campaign to build up the donor registry of South Asians. A registration drive was held Saturday in Aurora.<p>

The campaign is crucial because only 100,000 of the 6.6 million registered U.S. donors are of South Asian origin. That means South Asians who need transplants have a 1 in 20,000 chance of finding a match from the list, compared with a 1 in 15 chance for Caucasians.<p>

While 30 percent of patients find a match in their family, the rest need to find an unrelated donor. Donors of the same ethnicity are more likely to match, campaign organizers said. <p>

The campaign started in part as a way to help Vinay Chakravarthy, 28, a Boston medical resident who has leukemia and is recovering from chemotherapy. He still hasn't found a match. But Chakravarthy hopes the drive will ""help many people in the future,'' said his cousin Sumita Bhatta, of Lake View. <p>

So far, more than 21,000 people have registered since the drive began. Doing so involves only a cheek swab. <p>

If someone appears to be a match, a confirming blood test is then conducted. <p>

There are two procedures to collect marrow. The more common procedure, organizers said, is called peripheral blood stem cell collection. The donor receives injections of a drug to increase marrow production for several days before the transplant. The excess marrow is then removed from the donor's blood over several hours in a hospital.<p>

Desai, 28, compared the experience to giving blood. She said she did not need anesthesia, and that other than having a headache and being tired for a few days, she had no side effects. <p>

Another procedure, marrow harvest, requires surgically removing marrow from the donor's hip. The procedure can take as little as an hour and can be done on an outpatient basis, organizers said.<p>

A registration event will be held from 11 a.m. to 4 p.m. today at the Sri Venkateswara Swami Temple of Greater Chicago, 1145 W. Sullivan Rd., Aurora. <p>

Another drive will be Aug. 12 from 1 to 4 p.m. at the Hindu Temple of Greater Chicago, 10915 Lemont Rd., in Lemont. For more information on these or future drives, call (773) 610-6511 or visit <i>www.helpvinay.org.</i>