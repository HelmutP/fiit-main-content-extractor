
						
						
Two males, including one juvenile, suspected of allegedly burglarizing a Rogers Park mosque early Monday are being questioned by police in the Rogers Park neighborhood on the North Side. 
<P>No charges have been filed as of 5:30 a.m. in connection with the incident, which occurred at 12:46 a.m. Monday, according to Town Hall District Police Lt. Ken Barnas, who was assigned to the Rogers Park District. 
<P>The mosque, at the 3600 block of North Maplewood Avenue, was broken into and gang symbols had been spray painted inside, according to the lieutenant. 
<P>The two persons of interest, a 17-year-old man from the south suburbs and a 16-year-old from the North Side of Chicago, admitted to allegedly damaging the property inside by spraying the symbols, Barnas said. 
<P>The incident is ?definitely not? being investigated as a hate crime, the lieutenant said. 
<P>Belmont Area detectives are investigating.