
						
						
WANBOROUGH, England -- Britain raced to avert economic disaster Saturday by halting meat and dairy exports and the movement of livestock around the country after foot-and-mouth disease was found on a southern English farm. 
<P>Prime Minister Gordon Brown vowed to work ''night and day'' to avoid a repeat of a 2001 outbreak, when millions of dead animals were burned on pyres, swathes of the countryside were closed, rural tourism was badly hurt and British meat was shut out of international markets. 
<P>''Our first priority has been to act quickly and decisively,'' Brown said. 
<P>The Environment, Food and Rural Affairs Department said Britain had banned the export of live cattle, pigs, sheep and goats, as well as carcasses, meat and milk. 
<P>The United States and Japan banned British pigs and pork products in response to the outbreak. British beef already is banned in both countries because of mad-cow disease. 
<P>Foot-and-mouth causes fever and blisterlike lesions on affected animals. 
<P>It can be deadly in livestock but is harmless to humans.