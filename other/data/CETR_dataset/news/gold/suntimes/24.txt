
They've heard from a caretaker, a former U.S. senator, a lottery winner and even the head of homeland security under California Gov. Arnold Schwarzenegger.<p><P>
In all, jurors have endured testimony from nearly 50 witnesses in the 13 weeks since George Ryan's public corruption trial began. But there's still one person who could drastically reshape their impressions: the former governor himself.<p><P>
And he may have that opportunity, because it seems likely that Ryan will take the stand in his own defense.<p><P>
Lawyers representing Ryan and his co-defendant Lawrence Warner said last week they expect to put their clients on the stand after the prosecution rests this month.<p><P>
&quot;I expect him to testify,&quot; Ryan's lead defense attorney Dan Webb said. &quot;It's my belief and my expectation that he will testify.&quot;<p><P>
Warner's attorney, Ed Genson, said he too plans to put his client on the stand. &quot;My inclination is he will testify,&quot; Genson said.<p><P>
Both Webb and Genson said they'll make a final decision when the prosecution rests, which is expected mid-month. Neither would go into the tactical reasoning of doing something that defense lawyers typically avoid.<p><P>
Keeping defendants off the stand is a safe, predictable strategy, even in the highest-profile cases -- such as Michael Jackson's, experts say. With the burden of proof on the government, defense attorneys often argue the prosecution hasn't proven its case beyond a reasonable doubt, and a judge will tell jurors they're not allowed to hold it against a defendant for not testifying.<p><P>
Even if he once held the highest government post in Illinois.<p><P>
But DePaul University law professor Len Cavise, who has been monitoring the Ryan trial, said Ryan doesn't have a choice.<p><P>
&quot;He absolutely has to take the stand. The case against him is totally circumstantial. The jury needs to hear a coherent explanation. It's up to Ryan to kind of put it all together,&quot; Cavise said.<p><P>
<div class=""story_subhead"">Could have a lot to explain</div><P>
<P>
<P>
Ryan, 71, who also served two terms as Illinois secretary of state, is charged with using his state posts to steer big-money contracts and leases to friends. Prosecutors say he then accepted vacations, loans and cash in return. His friend Warner, 67, allegedly shook down vendors for business and made millions off state deals.<p><P>
The prosecution's case is a mural painted through thousands of strokes by a parade of witnesses and reams of documents. Jurors have to connect the dots rather than point to a smoking gun.<p><P>
Ryan's testimony could provide a narrative jurors are seeking and instantly shift the focus of the trial onto one thing: whether he is believable, DePaul University law professor Stephan Landsman said. &quot;One minute of his testimony could galvanize it one way or another,&quot; Landsman said.<p><P>
Prosecutors, who no doubt would subject Ryan to a vigorous cross-examination, wouldn't comment.<p><P>
But when Webb first told U.S. District Judge Rebecca Pallmeyer in November of his intentions of putting Ryan on the witness stand, Assistant U.S. Attorney Patrick Collins said: &quot;We look forward to the day.&quot;<p><P>
Ryan could have to explain why security guards and friends said they always saw him carrying cash, and yet bank records show he never used an ATM and he and his wife withdrew just $6,700 from the bank from 1993 to 2002, according to prosecutors.<p><P>
<div class=""story_subhead"">'Love and prostitution'</div><P>
<P>
<P>
Ryan also might have to explain the disparity between his public remarks and that of former U.S. Sen. Phil Gramm. During the trial and outside of the jury's presence, Ryan said Gramm paid him about $11,000 in a consulting fee during Gramm's presidential campaign. Prosecutors say Ryan, then secretary of state, and two of his aides secretly set up a scheme to take money from Gramm's fund. Ryan said Gramm knew about the payment. <p><P>
But Gramm testified he would never agree to pay Ryan as he sought an endorsement in 1995: &quot;It's the difference between love and prostitution,&quot; Gramm contended.<p><P>
Ryan also might have to explain why his friend Harry Klein testified that Ryan wrote him $1,000 checks to stay at Klein's Jamaican villa every year but Klein reimbursed him $1,000 in cash. Ryan told federal agents he always paid for his visits to Klein, who won a state lease under Ryan. Ryan's lawyers have hinted that Ryan used the reimbursed cash on villa employees and to, among other things, buy a TV for the vacation home.<p><P>
<div class=""story_subhead"">Would Ryan lose his temper?</div><P>
<P>
<P>
With Ryan on the stand, the government could also argue to bring in evidence not permissible under other witnesses. That could include Ryan allegedly giving &quot;make-work&quot; jobs to two former state lawmakers so they could boost their pensions.<p><P>
Jurors in other trials didn't hear from high-profile defendants who have recently come through the Dirksen Federal Building, including white supremacist Matt Hale, insurance mogul Michael Segal and Scott Fawell, Ryan's former right-hand man convicted in 2003 of misdeeds under Ryan.<p><P>
Cavise said Warner's testimony could be more risky because he's taken more &quot;direct hits,&quot; including from witnesses who say they felt they were being shaken down by Warner.<p><P>
But Ryan is different. He's a lifelong politician and most recently a national traveling speaker against the death penalty who is used to pushing his agenda and coming off as credible, Cavise said.<p><P>
&quot;You can't tell a guy who's been a successful politician that he can't convince people of things,&quot; Cavise said.<p><P>
Though Ryan in his later years in office was known to lose his temper and angrily snap at news conferences, Cavise doesn't think Ryan would lose it on the stand. <p><P>
&quot;I don't see it. He knows how to undergo cross examination. He doesn't have a downside to his testifying.&quot;