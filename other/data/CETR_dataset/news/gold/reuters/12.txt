
    


<p>NEW YORK (Reuters) - Hasbro Inc. (HAS.N: <a href=""/stocks/quote?symbol=HAS.N"">Quote</a>, <a href=""/stocks/companyProfile?symbol=HAS.N"">Profile</a>, <a href=""/stocks/researchReports?symbol=HAS.N"">Research</a>), the second largest U.S. toy maker, reported a 15 percent rise in fourth-quarter profit on Friday, helped by strong sales of Nerf balls and Monopoly board games.</p><span id=""midArticle_1""></span>
    


<p>But Hasbro's shares fell 2.54 percent to close at $28.38 with investors likely spooked by the company's pricey valuation and its uninspiring forecast for Marvel Entertainment Inc. (MVL.N: <a href=""/stocks/quote?symbol=MVL.N"">Quote</a>, <a href=""/stocks/companyProfile?symbol=MVL.N"">Profile</a>, <a href=""/stocks/researchReports?symbol=MVL.N"">Research</a>) toys like Spiderman and Fantastic Four, said Gerrick Johnson, an analyst with BMO Capital Markets.</p><span id=""midArticle_2""></span>
    


<p>Hasbro is adding more toys this year because of its licensing deal with Marvel Entertainment. The company said toys related to films like ""Spiderman 3"" and ""Fantastic Four: Rise of Silver Surfer"" would be just as profitable as its other brands.</p><span id=""midArticle_3""></span>
    


<p>""Why would you take on all this risk if it's only going to be as profitable as everything else you've done? The stock has rallied to where it is based on the toys for these movies,"" Johnson said.""</p><span id=""midArticle_4""></span>
    


<p>Hasbro's Transformers brand is also tied to a summer movie due to be released July 4.</p><span id=""midArticle_5""></span>
    


<p>Hasbro finished 2006 on a strong note as its Playskool and Transformers toys sold well during a holiday season that was kind to most major toy makers.</p><span id=""midArticle_6""></span>
    


<p>U.S. toy makers saw $10.6 billion in sales during the holidays, which allowed them to post a slim gain in total sales for the year.&nbsp; <span class=""label""><strong>