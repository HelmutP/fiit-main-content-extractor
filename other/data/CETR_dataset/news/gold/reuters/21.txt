
    
<p> (Adds CEO quotes, previous job cut and cost cut targets) </p><span id=""midArticle_1""></span>
    
<p>    PARIS, Feb 9 (Reuters) - Alcatel-Lucent (ALU.PA: <a href=""/stocks/quote?symbol=ALU.PA"">Quote</a>, <a href=""/stocks/companyProfile?symbol=ALU.PA"">Profile</a>, <a href=""/stocks/researchReports?symbol=ALU.PA"">Research</a>) on Friday said it would cut about 12,500 jobs over three years, more than previously announced, as it reported a fourth-quarter net loss  and forecast a drop in first-quarter revenues. </p><span id=""midArticle_2""></span>
    
<p>    However, the world's second-largest supplier of telecoms network and mobile equipment said it expected full-year revenues to grow at least in line with the telecoms carrier market at 5 percent. The company declined to comment on how much it expects first-quarter revenues to fall. </p><span id=""midArticle_3""></span>
    
<p>    Alcatel-Lucent, which issued a profit warning last month, made a net loss of 618 million euros ($802.3 million) in the three months to Dec. 31 compared with a profit of 381 million euros a year earlier. </p><span id=""midArticle_4""></span>
    
<p>    Workers at Alcatel-Lucent have called for a strike on Feb. 15 to protest against the job cuts. </p><span id=""midArticle_5""></span>
    
<p>    Alcatel-Lucent said it now expected pretax savings to total 1.7 billion euros over three years. The company had previously said it expected job losses of 9,000 and had pencilled in cost-savings of 1.4 billion euros. </p><span id=""midArticle_6""></span>
    
<p>    The U.S. and French companies started operating as a merged entity on Dec.1. &nbsp; <span class=""label""><strong>