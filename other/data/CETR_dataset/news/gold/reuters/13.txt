
    


<p>NEW YORK (Reuters) - Oil rose above $60 a barrel for the first time in more than a month on Friday as Nigeria moved to cut output and amid worries about mounting tension between the United States and Iran.</p><span id=""midArticle_1""></span>
    


<p>U.S. crude oil (CLc1: <a href=""/stocks/quote?symbol=CLc1"">Quote</a>, <a href=""/stocks/companyProfile?symbol=CLc1"">Profile</a>, <a href=""/stocks/researchReports?symbol=CLc1"">Research</a>) rose 95 cents to $60.66 a barrel by 1:47 p.m. (1847 GMT) Earlier, it hit a session peak of $60.80, its highest level since January 3.</p><span id=""midArticle_2""></span>
    


<p>London Brent crude (LCOc1: <a href=""/stocks/quote?symbol=LCOc1"">Quote</a>, <a href=""/stocks/companyProfile?symbol=LCOc1"">Profile</a>, <a href=""/stocks/researchReports?symbol=LCOc1"">Research</a>) was up 52 cents to $59.55.</p><span id=""midArticle_3""></span>
    


<p>Nigeria on Friday cut seven cargoes of crude oil from its February export program, firming up earlier plans to improve its compliance with OPEC production cuts. Another 11 cargoes were cut from the March program.</p><span id=""midArticle_4""></span>
    


<p>Doubts over whether some OPEC members were willing to comply with the group's planned output reduction of 1.7 million barrel per day have pressured oil prices in recent weeks.</p><span id=""midArticle_5""></span>
    


<p>Iran's Supreme Leader Ayatollah Ali Khamenei said on Thursday the Islamic Republic would target U.S. interests worldwide if it came under attack over its nuclear program.</p><span id=""midArticle_6""></span>
    


<p>Traders were also nervous ahead of the anniversary on Sunday of the 1979 Islamic Revolution, analysts said, citing worries the war of words could intensify between OPEC oil producer Iran and the United States over Tehran's nuclear program.&nbsp; <span class=""label""><strong>