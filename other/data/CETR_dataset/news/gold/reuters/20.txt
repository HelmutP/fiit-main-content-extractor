
    
<p> (adds background, share reaction) </p><span id=""midArticle_1""></span>
    
<p>    PARIS, Feb 9 (Reuters) - French drugs group Sanofi-Aventis (SASY.PA: <a href=""/stocks/quote?symbol=SASY.PA"">Quote</a>, <a href=""/stocks/companyProfile?symbol=SASY.PA"">Profile</a>, <a href=""/stocks/researchReports?symbol=SASY.PA"">Research</a>) said on Friday a California court had ruled against it in a case pitting it against Amphastar (AMPR.O: <a href=""/stocks/quote?symbol=AMPR.O"">Quote</a>, <a href=""/stocks/companyProfile?symbol=AMPR.O"">Profile</a>, <a href=""/stocks/researchReports?symbol=AMPR.O"">Research</a>) and Teva (TEVA.O: <a href=""/stocks/quote?symbol=TEVA.O"">Quote</a>, <a href=""/stocks/companyProfile?symbol=TEVA.O"">Profile</a>, <a href=""/stocks/researchReports?symbol=TEVA.O"">Research</a>) over the best-selling Lovenox drug. </p><span id=""midArticle_2""></span>
    
<p>    Sanofi said it was reviewing its options and intended to continue vigorously to defend its intellectual property rights. </p><span id=""midArticle_3""></span>
    
<p>    The company is battling with the generic rivals over the patent of its bloodthinner Lovenox. </p><span id=""midArticle_4""></span>
    
<p>    The French drug maker, faced with a slew of generic threats, hoped the trial would address the issue of intent which was unresolved in April when a U.S. Court of Appeals ruling reversed a lower court decision that had invalidated the Lovenox patent.  Following Sanofi's appeal victory, the case returned to the U.S. district court of California. Lovenox is Sanofi's best-selling drug, with sales of 2.14 billion euros ($2.75 billion) in 2005. </p><span id=""midArticle_5""></span>
    
<p>    The generic drugmakers are trying to sell cheaper Lovenox copies in the United States even though the patent expires in 2012. Lovenox is also under attack from Momenta Pharmaceuticals (MNTA.O: <a href=""/stocks/quote?symbol=MNTA.O"">Quote</a>, <a href=""/stocks/companyProfile?symbol=MNTA.O"">Profile</a>, <a href=""/stocks/researchReports?symbol=MNTA.O"">Research</a>) and Sandoz, owned by Switzerland's Novartis (NOVN.VX: <a href=""/stocks/quote?symbol=NOVN.VX"">Quote</a>, <a href=""/stocks/companyProfile?symbol=NOVN.VX"">Profile</a>, <a href=""/stocks/researchReports?symbol=NOVN.VX"">Research</a>). </p><span id=""midArticle_6""></span>
    
<p>    Lovenox is a complex mixture of ingredients based on live organisms and Sanofi has said that part of the drug cannot be characterised and therefore cannot be copied. &nbsp; <span class=""label""><strong>