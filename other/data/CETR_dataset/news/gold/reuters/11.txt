
    <p>NEW YORK (Reuters) - Fund manager Brandes Investment Partners LP said on Friday it intended to vote against billionaire investor Carl Icahn's proposed $2.31 billion buyout offer for Lear Corp. (LEA.N: <a href=""/stocks/quote?symbol=LEA.N"">Quote</a>, <a href=""/stocks/companyProfile?symbol=LEA.N"">Profile</a>, <a href=""/stocks/researchReports?symbol=LEA.N"">Research</a>), which the auto parts maker has accepted.<span id=""midArticle_byline""></span></p><span id=""midArticle_0""></span>
    


<p>Brandes said it would vote against the deal on behalf of its clients who own 2.85 million shares in Lear, or about 3.9 percent of total shares outstanding, because it believes Icahn's offer is below Lear's long-term fair value.</p><span id=""midArticle_1""></span>
    


</p><span id=""midArticle_2""></span>
    