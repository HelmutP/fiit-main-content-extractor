
    


<p>NEW YORK (Reuters) - Volatility could be the name of the game on Wall Street this week as rising crude oil prices revive inflation fears and stock investors await congressional testimony from the Federal Reserve's chairman.</p><span id=""midArticle_1""></span>
    


<p>Investors hope that Ben Bernanke will speak plainly about how he sees the outlook for U.S. interest rates in the months ahead after recent data showing the economy is strong.</p><span id=""midArticle_2""></span>
    


<p>The outcome of the weekend's Group of Seven finance ministers' meeting will get attention early this week as investors consider its impact, if any, on foreign-exchange rates.</p><span id=""midArticle_3""></span>
    


<p>Oil climbed back above $60 a barrel last week as OPEC member Nigeria reduced exports and tensions grew between the United States and Iran over Iran's nuclear program. Temperatures below freezing persisted in much of the United States and spurred demand for heating oil and natural gas.</p><span id=""midArticle_4""></span>
    


<p>Stock trading this week could prove choppy if oil prices rise much further, analysts said.</p><span id=""midArticle_5""></span>
    


<p>""Crude and housing are the wild cards for this market,"" said Harry Clark, president and CEO of Clark Capital Management Group in Philadelphia. ""If crude goes above $60 to $65 a barrel, that could give the market a correction.""</p><span id=""midArticle_6""></span>
    


<p>For the past week, stocks fell despite a spurt of deal news, solid profits from bellwethers such as Cisco Systems Inc. (CSCO.O: <a href=""/stocks/quote?symbol=CSCO.O"">Quote</a>, <a href=""/stocks/companyProfile?symbol=CSCO.O"">Profile</a>, <a href=""/stocks/researchReports?symbol=CSCO.O"">Research</a>) and a sparkling market debut by Fortress Investment Group LLC (FIG.N: <a href=""/stocks/quote?symbol=FIG.N"">Quote</a>, <a href=""/stocks/companyProfile?symbol=FIG.N"">Profile</a>, <a href=""/stocks/researchReports?symbol=FIG.N"">Research</a>). The Dow Jones industrial average (.DJI: <a href=""/stocks/quote?symbol=.DJI"">Quote</a>, <a href=""/stocks/companyProfile?symbol=.DJI"">Profile</a>, <a href=""/stocks/researchReports?symbol=.DJI"">Research</a>) fell 0.57 percent, the Standard &amp; Poor's 500 index (.SPX: <a href=""/stocks/quote?symbol=.SPX"">Quote</a>, <a href=""/stocks/companyProfile?symbol=.SPX"">Profile</a>, <a href=""/stocks/researchReports?symbol=.SPX"">Research</a>) slipped 0.71 percent and the Nasdaq Composite Index (.IXIC: <a href=""/stocks/quote?symbol=.IXIC"">Quote</a>, <a href=""/stocks/companyProfile?symbol=.IXIC"">Profile</a>, <a href=""/stocks/researchReports?symbol=.IXIC"">Research</a>) dropped 0.65 percent.&nbsp; <span class=""label""><strong>