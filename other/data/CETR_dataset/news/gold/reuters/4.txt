
    <p>By John Whitesides, Political Correspondent<span id=""midArticle_byline""></span></p><span id=""midArticle_0""></span>
    


<p>SPRINGFIELD, Illinois (Reuters) - Democratic presidential hopeful Barack Obama, citing the legacy of Abraham Lincoln, pledged on Saturday to bridge the partisan divide in Washington, end the war in Iraq and transform American politics as the first black U.S. president.</p><span id=""midArticle_1""></span>
    


<p>Launching his 2008 White House campaign outside the building where Lincoln began his fight against slavery with an 1858 speech declaring ""a house divided against itself cannot stand,"" Obama said it was time to ""turn the page"" to a new politics.</p><span id=""midArticle_2""></span>
    


<p>""Let us begin this hard work together. Let us transform this nation,"" Obama, 45, told thousands of cheering supporters who braved sub-freezing temperatures outside the old state Capitol building.</p><span id=""midArticle_3""></span>
    


<p>""By ourselves, this change will not happen. Divided, we are bound to fail,"" he said.</p><span id=""midArticle_4""></span>
    


<p>Obama, a rising party star and the only black U.S. senator, said the United States had overcome many challenges, from gaining its independence to the Civil War to the Great Depression.</p><span id=""midArticle_5""></span>
    


<p>""Each and every time, a new generation has risen up and done what's needed to be done. Today we are called once more -- and it is time for our generation to answer that call,"" he said.</p><span id=""midArticle_6""></span>
    


<p>Obama's candidacy has intrigued Democrats looking for a fresh face and sparked waves of publicity and grass-roots buzz about the first black presidential candidate seen as having a chance to capture the White House.&nbsp; <span class=""label""><strong>