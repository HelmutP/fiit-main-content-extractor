
    


<p>LOS ANGELES (Reuters) - They call themselves the Bounty Hunters, the Midget Locos, Project Gangster Blood and China Town Boyz and most of them don't expect to live beyond age 20.</p><span id=""midArticle_1""></span>
    


<p>Forty years after the Bloods and the Crips put Los Angeles on the map, the number of gangs in Los Angeles County has swelled to about 1,000 and their estimated 88,000 members are drawn from every ethnicity -- Asians, blacks, Latinos, whites.</p><span id=""midArticle_2""></span>
    


<p>""Los Angeles county and city is, unfortunately, the gang capital of America,"" Los Angeles County Sheriff Lee Baca said as police on Thursday announced a crackdown on the city's 11 worst gangs.</p><span id=""midArticle_3""></span>
    


<p>Gang culture may have gone mainstream in the last 10 years, its rap music, bling, tattoos, baggy pants fashions and slang aped by middle-class white youths across the United States and beyond.</p><span id=""midArticle_4""></span>
    


<p>""All the stuff that was street related has become mainstream so now it's hard to tell the difference between the real and the fake,"" said Alex Alonso, who runs the Web site www.streetgangs.com.</p><span id=""midArticle_5""></span>
    


<p>But there is nothing glamorous about life in the tough neighborhoods of the second largest U.S. city.</p><span id=""midArticle_6""></span>
    


<p>Gangs are blamed for 56 percent of the 478 murders in Los Angeles last year. Many of the dead were victims of inter-gang warfare or drive-by shootings so commonplace and so far away from affluent Beverly Hills that they barely make the local news.&nbsp; <span class=""label""><strong>