
    <p>KADENA AIR BASE, Japan (Reuters) - The arrival of 12 U.S. F-22 fighter planes at a U.S. air base on Japan's southern island of Okinawa was delayed on Sunday without word on a new date for their first deployment outside the United States.<span id=""midArticle_byline""></span></p><span id=""midArticle_0""></span>
    


<p>The U.S. Air Force's newest fighter planes had been scheduled to arrive at the weekend at the U.S. Kadena Air Base.</p><span id=""midArticle_1""></span>
    


<p>But a statement from the Air Force in Okinawa said the aircraft had turned back to Hawaii for operational reasons, without elaborating. They were still scheduled to be deployed.</p><span id=""midArticle_2""></span>
    


<p>The three-month deployment of the stealth fighters comes as diplomats struggle to reach agreement at talks in Beijing aimed at ending North Korea's nuclear arms program, although military officials have said there was no direct link to the negotiations.</p><span id=""midArticle_3""></span>
    


<p>""What the F-22 deployment does is ... make the (U.S.-Japan) alliance stronger, and that is a factor for dealing with the complexities of North Korea,"" U.S. Forces Japan Commander Lieutenant-General Bruce Wright told journalists in Tokyo last week.</p><span id=""midArticle_4""></span>
    


<p>The Raptor -- said to be the most expensive fighter ever built -- is a ""very formidable asset, with very formidable capability compared to any other fighter,"" Wright said.</p><span id=""midArticle_5""></span>
    


<p>The planes are able to gather data from multiple sources to track, identify and kill air-to-air threats before being detected by radar, and have significant surface-strike capability, according to the U.S. Air Force Web site.</p><span id=""midArticle_6""></span>
    


<p>Japan is looking to replace its F-15 fighters and Wrignt said the F-22 was one option.</p><span id=""midArticle_7""></span>
    


<p>On Saturday, over 200 people gathered for a peaceful protest outside the air base, with signs reading ""Raptor, Go Home"". Residents of Okinawa often complain of crime, noise, pollution and accidents associated with U.S. bases stationed in the southern prefecture.</p><span id=""midArticle_8""></span>
    


</p><span id=""midArticle_9""></span>
    