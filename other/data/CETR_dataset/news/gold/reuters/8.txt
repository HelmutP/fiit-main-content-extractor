
    


<p>MEXICO CITY (Reuters Life!) - Got 48 hours to explore Mexico City? Reuters correspondents with local knowledge help visitors get the most from a short visit in the Mexican capital:</p><span id=""midArticle_1""></span>
    


<p>FRIDAY</p><span id=""midArticle_2""></span>
    


<p>4 p.m.</p><span id=""midArticle_3""></span>
    


<p>Head straight for the huge Zocalo square in the Centro Historico, Mexico City's spiritual and historic heart. It's dominated by a giant Mexican flag and the Metropolitan Cathedral, which is slowly sinking into the ground through subsidence. You'll see dancers in feathered headdresses stamping their shell-adorned feet to Aztec drum rhythms.</p><span id=""midArticle_4""></span>
    


<p>Pop into the Palacio Nacional and be wowed by dramatic larger-than-life murals by Diego Rivera depicting scenes from Mexico's history. If you're feeling brave, get lost in the labyrinth of streets behind the Cathedral.</p><span id=""midArticle_5""></span>
    


<p>6 p.m.</p><span id=""midArticle_6""></span>
    


<p>Feeling thirsty? Stop for tequila and guacamole in La Opera Bar (Ave Cinco de Mayo 10), an ornate early 20th century watering hole with velvet-cushioned wooden booths and a bullet hole in the ceiling left by revolutionary Pancho Villa.&nbsp; <span class=""label""><strong>