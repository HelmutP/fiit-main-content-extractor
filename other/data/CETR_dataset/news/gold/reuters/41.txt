
    <p>NEW YORK (Reuters Health) - People who think they're having a stroke, and their loved ones, often call others for advice before calling an ambulance, hints a study conducted in Australia. This could lead to delays in the administration of potentially life-saving treatment.<span id=""midArticle_byline""></span></p><span id=""midArticle_0""></span>
    


<p>Half of stroke patients or the people with them at the time of the stroke consulted a third-party, who frequently came to the patient's home before calling an ambulance, Dr. Ian Mosley of the National Stroke Research Institute in Heidelberg Heights, Victoria and colleagues report.</p><span id=""midArticle_1""></span>
    


<p>""Knowledge of the benefits of immediately calling an ambulance may reduce pre-hospital delays more than symptom recognition alone,"" Mosley and his team write in the journal Stroke.</p><span id=""midArticle_2""></span>
    


<p>The clot-busting drug alteplase is ""highly effective,"" the researchers note, when given within three hours of a stroke. But just 3 percent of stroke patients in Australia receive this medication, largely because they don't get to the hospital in time.</p><span id=""midArticle_3""></span>
    


<p>To investigate what factors were associated with promptly calling an ambulance after a stroke, Mosley and colleagues looked at 198 stroke patients transported to one of three hospitals in Melbourne over six months.</p><span id=""midArticle_4""></span>
    


<p>Calls were made within one hour of symptom onset for 52 percent of patients, the researchers found. Callers identified stroke as the problem in 44 percent of cases, and recognized the problem as stroke after prompting in 47 percent of cases.</p><span id=""midArticle_5""></span>
    


<p>Callers were most likely to report the problem as a stroke if the patient suffered facial droop or had a history of stroke or ""mini-strokes.""</p><span id=""midArticle_6""></span>
    


<p>But only 22 percent of callers recognized the problem as a stroke and made the call for an ambulance within an hour after symptoms first appeared.&nbsp; <span class=""label""><strong>