
    
<p>    LOS ANGELES, Feb 9 (Reuters) - IHOP Corp. (IHP.N: <a href=""/stocks/quote?symbol=IHP.N"">Quote</a>, <a href=""/stocks/companyProfile?symbol=IHP.N"">Profile</a>, <a href=""/stocks/researchReports?symbol=IHP.N"">Research</a>) said on Friday it would delay the completion of its corporate refinancing while it resolves issues related to the lapsing of some of its federal trademark and service mark registrations. </p><span id=""midArticle_1""></span>
    
<p>    In a statement, the pancake house restaurant chain said it is in the process of obtaining new registrations for the affected trademarks and service marks. It has not yet determined the financial impact of these measures, but said they would not have a material impact on its business or franchisees. </p><span id=""midArticle_2""></span>
    
<p> ((Reporting by Nichola Groom; Reuters Messaging: nichola.groom.reuters.com@reuters.net, Tel:+1-213-955-6755)) Keywords: IHOP/  </p><span id=""midArticle_3""></span>
    
<p>(C) Reuters 2007.  All rights reserved.  Republication or redistribution ofReuters content, including by caching, framing or similar means, is expresslyprohibited without the prior written consent of Reuters. Reuters and the Reuterssphere logo are registered trademarks and trademarks of the Reuters group ofcompanies around the world.nWEN4076</p><span id=""midArticle_4""></span>
    