So, Blair Hull wakes up with a $29 million hangover and now he's got all the time in the world to consider how he's never going to see that money again.<br>
<br>
But even before the voters delivered their verdict--a sorry third place finish--Hull knew he was toast and he knew why.<br>
<br>
The divorce records that showed he hit his wife--records he tried for a time to keep secret--well, ""I wish I had exposed them (the messy divorce files) earlier, probably a year earlier,"" he said in a rare moment of introspection in the final hours of his campaign. Woulda, Coulda, Shoulda.<br>
<br>
<hr noshade><img src=""http://images.chicagotribune.com/media/thumbnails/photo/2004-01/11102791.jpg""align=""left""> As every Illinois resident with a television, a radio or a telephone knows by now, Republican Senate nominee Jack Ryan also has a problem with his sealed divorce records. But, so far, he has successfully argued they should remain closed to protect the privacy of his nine-year-old son.<br>
<br>
Democrats are ebullient that the Ryan records haven't been disclosed yet. That way they still lurk as a possible time bomb to explode the Ryan candidacy before the general election in November---when the GOP will not be able to replace him with a less damaged candidate---and Democrat Barack Obama sails into the Senate. No muss, no fuss.<br>
<br>
One thing is sure: If Jack Ryan has something to hide, it will come out in the coming months. And, if there's nothing of substance in the boxes of sealed records, then the voters can settle back and savor the politically unusual: A thoughtful issue-oriented campaign. A Ryan-Obama face off could make for two smart, charismatic, Harvard-educated opponents who agree on almost nothing and offer Illinois voters a genuine choice.<br>
<br>
<hr noshade>
Back to Blair: Much of the $29 million (or more) that Hull spent from his own fortune went to TV ads. No surprise that in the final hours of the race, he accused sponsor Sen. John McCain (R-Ariz.) of dropping the ball in his recent campaign reform law. It should have provided for FREE TV advertising for candidates, said Hull.<br>
<br>
<hr noshade>
<img src=""http://images.chicagotribune.com/media/thumbnails/photo/2004-01/11102770.jpg""align=""left""> Close don't count, except in horseshoes. And Dan Hynes wasn't all that close. But if your neighbors are telling you that this means that the Democratic Machine is dead, tell them they're dead wrong.<br>
<br>
As for State Comptroller Hynes, stay tuned. At 35 he was the youngest of the 15 people running for the U.S. Senate here and you can be sure this is not the last time he'll be on the ballot. He'll be back, trying to move up the political food chain---again with the help of the regular Democratic organization, especially his clout-heavy dad, ward boss Tom Hynes.<br>
<br>
And the vaunted Hynes' grass roots effort? It flopped but it <I>was</I> out in force.  Two examples: Just as I was walking out my front door to vote Tuesday, I got a call (from a real human being) asking me to vote for Hynes. Then, at my polling place there was only one volunteer outside in the cold. Yup, he was handing out ""Punch 28"" flyers for ? Dan Hynes.<br>
<br>
Foot soldiers weren't the problem. The multi-culti cross over appeal of African-American Obama was more than all those Streets & San workers and plumbers and precinct captains could overcome.<br>
<br>
<hr noshade>
Speaking of horses (remember the lame horseshoe reference above), Democratic Senate candidate Maria Pappas wound up her campaign with a swing through restaurants with her nine-year-old nephew, Alexander, and his stuffed horse toy, Seabiscuit.<br>
<br>
 County treasurer Pappas likened her candidacy to the famed, underrated horse of recent book and movie fame. However, the analogy broke down at the end since Seabiscuit, of course, was a winner, and Pappas?wasn't.<br>
<br>
<hr noshade>                      
<img src=""http://images.chicagotribune.com/media/thumbnails/photo/2004-01/11102770.jpg""align=""left"">Seabiscuit is also a popular reference on the presidential circuit where 5-foot 6-inch Dennis Kucinich is fond of likening himself to the come-from-behind racehorse with variations on this line: ""We're a little bit small and have little spindly legs, but we're going to burst from the back of the pack and take this race.""<br>
<br>
OK. That didn't happen.<br>
<br>
Instead, surprise, John Kerry won the Illinois Democratic presidential primary. I know this because I went to the victory party, which started at a bar on Wells Street an hour BEFORE the polls closed.<br>
<br>
""We're shocked,"" vamped Kerry's Illinois chairman, David Wilhelm, discussing Kerry's victory here as he held a glass of red wine and mingled with perhaps 75 volunteers. Among them was a man who said he was Kerry's godson and had been nestled in the Senator's arms as a baby.  We're waiting for photographic proof.<br>
<br>
While this was not exactly a raucous event at Brehon Pub, the party in a different room there seemed a lot more exciting with four guys in kilts playing drums and bagpipes, and celebrants wearing funny free hats to celebrate single malt Scotch. Now <I>that's</I> worth drinking to.<br>
<br>
Kerry wasn't in Illinois for his win here but he made the local evening news when he declared, from West Virginia, ""Tonight Illinois has put us over the top for the nomination."" (By other counting methods, he was already over the top with enough delegates to guarantee the nomination after last week's round of primaries.)<br>
<br>
Anyhow, right now you're probably asking, on this day that Illinois (might but might not have) put Kerry ""over the top,"" what was Kerry talking about in West Virginia?<br>
<br>
And now, I can reveal, he was talking about woodchucks, of course.<br>
<br>
This being the Internet where there's no shortage of ""space,"" I could set out the entire passage here about the woodchucks. But that would be boring. In short, Kerry was talking about his views on guns:  ""I've been a hunter since I was about 12 years old. ?I hunted like any kid going out after woodchuck in the fields.""<br>
<br>
The obvious follow-up question, ""How much wood would a woodchuck chuck?"" did not come up.<br>
<br>
But, a reporter did, oddly, ask about the Democrats' terrible presidential candidate, former Massachusetts Gov. Michael Dukakis. That prompted this odd revelation from Kerry, the junior senator from Massachusetts:  ""He's doing pretty well. He had an auto accident a few years ago. His neck is kind of cricked.""<br>
<br>
Party on dudes! Now that he's got the nomination locked up, Kerry and his wife, Teresa Heinz Kerry are heading for Spring Break in Ketchum, Idaho. And none of that Candidates Gone Wild, stuff, kids.<br>
<br>
<hr noshade>
<img src=""http://images.chicagotribune.com/media/thumbnails/photo/2004-01/11102770.jpg""align=""left"">And finally?it wouldn't be an election night in Illinois without a little camera hogging from the Rev. Jesse Jackson who, naturally, was available for television interviews at Obama's victory party.<br>
<br>
But earlier in the day, with no TV cameras present to record it, WVON radio's ""Your Morning Miracle"" Cliff Kelley brokered a peace treaty of sorts between Rev. Jackson and presidential candidate Rev. Al Sharpton, who have been feuding (again) for the past few months.<br>
<br>
The Revs--Jackson and Sharpton--have a long love-hate history and as of Tuesday morning's d?tente they seem to have moved into the ""love,"" or ""like"" or maybe ""don't dislike"" phase.<br>
<br>
It happened this way:  Sharpton's longtime aide, Rachel Noerdlinger, called the radio station to put Sharpton on the line for a pre-arranged interview with Kelley. When she called in, suddenly she was talking to Jackson who was in the studio doing the Kelley show himself.<br>
<br>
Then Sharpton got on the line and the two Revs talked like old friends (which they were until they became rivals and enemies).<br>
<br>
The most recent feud, dating back to last fall, came after Rev. Jackson's son, U.S. Rep. Jesse Jackson Jr. (D-Ill.), endorsed Howard Dean (remember him?). This prompted Sharpton to blast both Dean and Rep. Jackson, and denounce ""any so-called African-American leader that would endorse Dean despite his anti-black record.""<br>
<br>
Now Hull and Hynes have endorsed Obama.  GOP losing candidates have endorsed Ryan and ?the Revs have patched it up.  Can't we all just get along?<br>
<br>
Doubt it!
EW