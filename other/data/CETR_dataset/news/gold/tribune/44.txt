Q:</b> I'm pleased with my stock in Caterpillar Inc. but wonder if it can continue on its current pace. What do you think? <i>---C.L., via the Internet </i><br>
<br>
<b>A:</b>  This famous maker of construction and agricultural equipment could have faded into memory as simply a model for children's toys.<br>
<br>
Instead, it is a cutting-edge worldwide company committed to strategy, diversification, and research and development. It enjoys a dominant market share in engines and equipment used in mining and construction.<br>
<br></p>

                    <div class=""rail"">
                        
                        
							
                            
                                

                                

                                
                                    <div id=""module-related-links"">
                                        
                                        	<h4>Related links</h4>
										
                                        <ul><li>
														
														
															
															


    
    <a href=""/business/yourmoney/sns-yourmoney-leckey-jpg,0,5911442.photo"" target=""win_10846359"" onclick=""if (window.windoid) windoid('','win_10846359',800,645,'resizable=1,scrollbars=1')"">Andrew Leckey</a>



<a href=""/business/yourmoney/sns-yourmoney-leckey-jpg,0,5911442.photo"" target=""win_10846359"" class=""multimedia-item-type"" onclick=""if (window.windoid) windoid('','win_10846359',800,645,'resizable=1,scrollbars=1')"">Photo</a>












														

													</li>
													
												
										
											
													<li>
														
															<font class=""rail-text"">

 

<a href=""/business/yourmoney/sns-yourmoney-0819leckey,0,5184667.story""><b>Aug. 19:</b> Electronic Arts</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0812leckey,0,1973396.story""><b>Aug. 12:</b> Sara Lee</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0805leckey,0,2825366.story""><b>Aug. 5:</b> Kellogg</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0729leckey,0,5119131.story""><b>July 29:</b> Wendy's</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0722leckey,0,1907860.story""><b>July 22:</b> Expedia</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0715leckey,0,2759830.story""><b>July 15:</b> Best Buy
</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0708leckey,0,3611800.story""><b>July 8:</b> Sears</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0701leckey,0,400529.story""><b>July 1:</b> Verizon</a><br>


<br>

<a href=""/business/yourmoney/sns-yourmoney-0617leckey,0,3087511.story""><b>June 17:</b> Prudential financial</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0610leckey,0,7876233.story""><b>June 10:</b> Microsoft</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0603leckey,0,728210.story""><b>June 3:</b> Chevron</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0527leckey,0,3021975.story""><b>May 27:</b> Alcoa</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0520leckey,0,7810697.story""><b>May 20:</b> Citigroup</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0513leckey,0,662674.story""><b>May 13:</b> Northrop Grumman</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0506leckey,0,1514644.story""><b>May 6:</b> ConocoPhillips</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0429leckey,0,3349656.story""><b>April 29:</b> Gap</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0422leckey,0,138385.story""><b>April 22:</b> Tivo </a><br>

<a href=""/business/yourmoney/sns-yourmoney-0415leckey,0,990355.story""><b>April 15:</b> Saks</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0408leckey,0,1842325.story""><b>April 8:</b> Jetblue</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0401leckey,0,6631047.story""><b>April 1:</b> AT&T</a><br>


<a href=""/business/yourmoney/sns-yourmoney-0325leckey,0,924819.story""><b>March 25:</b> Apple</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0318leckey,0,1776789.story""><b>March 18:</b> Coke</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0311leckey,0,6565511.story""><b>March 11:</b> Amazon.com</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0304leckey,0,7417481.story""><b>March 4:</b> Humana</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0225leckey,0,334994.story""><b>Feb. 25:</b> General Electric</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0218leckey,0,1186964.story""><b>Feb. 18:</b> Conagra</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0211leckey,0,5975686.story""><b>Feb. 11:</b> TJXh</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0204leckey,0,6827656.story""><b>Feb. 4:</b> BP</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0128leckey,0,1121428.story""><b>Jan. 28:</b> J.C. Penney</a><br>

<a href=""/business/yourmoney/sns-yourmoney-0121leckey,0,5910150.story""><b>Jan. 21:</b> Rite Aid</a><br>

 
 
</font>
														
														

													</li>
													
												
										
											
													<li>
														
															<li><font class=""rail-text""><a href=""/business/yourmoney/sns-yourmoney-pkg,0,4479963.special"">Main
  page</a></font></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604dividends,0,195934.story"">Despite cash hoard, companies wary of dividends</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604taxes,0,674251.story"">Tax issues crop up when honeymoon is over</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604marksjarvis,0,493781.story"">Time to evaluate international funds</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604journey,0,7346255.story"">Working in retirement means less of the same</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604cruz,0,1976907.story"">The savings
  game</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604leckeyfile,0,6678261.story"">The Leckey
  file</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604gettingstarted,0,7897082.story"">Getting
  started</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604spending,0,6099912.story"">Spending
  smart</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604cantheydothat,0,4194851.story"">Can they do that?</a></li>


<li><a href=""/business/yourmoney/sns-yourmoney-0604leckey,0,1186963.story"">Taking stock</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604watch,0,7887798.story"">Stock watch</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604russia,0,5507764.story"">Investing in Russia is not for the timid</a></li>

<li><a href=""/business/yourmoney/sns-yourmoney-0604weekahead,0,6950716.story"">The week ahead</a></li>



														
														

													</li></ul>
                                    </div>
                                

                            
                        

                        <!-- google ads -->
                        




    
        
            <iframe src=""/common/includes/google-adsense-content.html?client=ca-tribune_news3_html&channel_content=chicagotribune.com&channel_section=chicagotribune_section&type=wide&keywords=investment%2C%20investment%20jobs%2C%20mutual%20funds%2C%20online%20broker&page_url=http://www.chicagotribune.com/business/yourmoney/sns-yourmoney-0604leckey,0,4102859,print.story"" frameborder=""0"" width=""290"" height=""0"" marginwidth=""0"" marginheight=""0"" scrolling=""no""></iframe>
        
    

                        <!-- END google ads -->

                        <!-- topix links -->
                        <div class=""clearfix"">
                            
                                <iframe src=""/common/includes/topix.html?pcode=6003&url=http%3A%2F%2Fwww.chicagotribune.com%2Fbusiness%2Fyourmoney%2Fsns-yourmoney-0604leckey%2C0%2C4102859%2Cprint.story%3Flast_modified%3D6%2F2%2F06%204%3A31%3A36"" frameborder=""0"" width=""280"" height=""0"" marginwidth=""0"" marginheight=""0"" scrolling=""no"" allowTransparency=""true""></iframe>
                            
                        </div>
                        <!-- END topix links -->
                    </div><!-- END rail -->

                    <p id=""story-body2"">
To expand into the railroad industry, it recently bought Progress Rail Services Inc., a supplier of remanufactured rail products, for $800 million in cash and stock. It also purchased the rail remanufacturing business of Finning International Inc. for an undisclosed amount.<br>
<br>
Targeting China's potential, Caterpillar is opening a new parts-distribution center in Shanghai and has received a contract to supply methane-powered generators to a Chinese coal-mining company.<br>
<br>
With copper, oil and gas prices soaring, Caterpillar is profiting from tremendous demand for its machinery and must overcome production bottlenecks from its heavy orders. It also intends to produce a line of electric-drive mining trucks that includes a partnership with Mitsubishi Electric to supply power semiconductors.<br>
<br>
Caterpillar shares (CAT) are up 26 percent this year, following gains of 18 percent last year, 17 percent in 2004 and 82 percent in 2003. After a 45 percent profit increase in its recent quarter due in part to price increases, the company boosted its earnings outlook for the year.<br>
<br>
But this has been so much of a good thing that investors lately have wondered how long it can continue at this pace.<br>
<br>
The stock price already reflects the positives and the company has passed along a lot of rising costs. Although few see an end in sight to good fortune in the industries it serves, they are cyclical in nature and can encounter problems such as raw-material shortages.<br>
<br>
Because the upside potential of Caterpillar shares could be limited for a time, the consensus rating from Wall Street analysts is a ""hold,"" according to Thomson Financial, consisting of three ""strong buys,"" three ""buys,"" nine ""holds"" and one ""underperform.""<br>
<br>
Earnings are expected to increase 31 percent this year, versus the 22 percent expected for the farm and construction machinery industry. Next year' expected 13 percent growth rate compares to 8 percent projected industrywide. The five-year annualized growth rate is forecast as 10 percent versus 12 percent for its peers.<br>
<br>
<b>Q:</b> Latin American stocks have been booming. What do you think of Fidelity Latin America Fund? <i>--L.M., via the Internet </i><br>
<br>
<b>A:</b> You can't avoid excitement over the high returns of funds investing south of the border. Unfortunately, you can't avoid their volatility either.<br>
<br>
The $3.4 billion Fidelity Latin America Fund (FLATX) is up 70 percent over the past 12 months and has a three-year annualized return of 53 percent. Both results rank in the upper 14 percent of Latin America funds.<br>
<br>
Nearly half of this fund's assets are in Brazil and more than one-third in Mexico, with the remainder in Chile, the U.S. and Peru. Industrial materials represent one-third of assets, while other significant components are telecommunications, energy and financial services.<br>
<br>
""Fidelity Latin America is a heavy bet on commodities, which makes sense because that's what is worth investment in that region, and it does have some telecoms and banks,"" said Jack Bowers, editor of the independent Fidelity Monitor newsletter in Rocklin, Calif. ""Because that is a very narrow, cyclical market with high volatility and much more risk than the Standard & Poor's 500, I have the fund rated a `hold.'""<br>
<br>
Bowers doesn't rate the fund a ""sell,"" because commodities are doing well and could continue to do so for a while. But he doesn't like the fact that its portfolio consists of cyclicals and virtually no growth stocks. Besides being focused on a few countries and industries, it has a concentrated portfolio of 86 stocks.<br>
<br>
Although it has had strong returns, Fidelity Latin America Fund also has had three manager changes in recent years.<br>
<br>
Former Latin America analysts Brent Bottamini and Adam Kutas become co-managers in April 2005. The fund emphasizes strong cash flows and healthy balance sheets of undervalued blue-chip companies.<br>
<br>
Largest holdings are Mexico's America Movil SA and Cemex SA; Brazil's Petroleo Brasileiro, Unibanco Uniao de Bancos Brasileiros and Usinas Siderurgicas de Minas; Mexico's Telefonos de Mexico; and Brazil's Bco Itau Holdings, Companhia Siderurgica Nacional, Banco Bradesco SA and Companhia Vale Do Rio Doce.<br>
<br>
This ""no-load"" (no sales charge) fund has a $2,500 minimum initial investment and annual expense ratio of 1.04 percent.<br>
<br>
<b>Q:</b> My husband and I bought $120,000 worth of I bonds in August 2005. We are conservative investors trying to protect our capital from inflation, which seems to be going up. Can you explain why the new rate is so low? <i>--M.K., via the Internet </i><br>
<br>
<b>A:</b> The return of Treasury Series I inflation-indexed bonds changes twice a year.<br>
<br>
It consists of two parts: a fixed rate of return that remains the same for the life of the bond, and a variable semiannual inflation rate based on changes in the consumer price index announced each May and November.<br>
<br>
""Inflation was up about 3.5 percent during the past year, but bunched in the first six months due to a run-up in energy costs and very low in the past six-month period,"" explained Daniel Pederson, president of the Savings Bond Informer in Detroit and author of ""Savings Bonds: When to Hold, When to Fold and Everything In-Between.""<br>
<br>
In the case of your I bonds, the latest inflation rate of 1 percent will be added to your fixed rate of 1.2 percent on your next six-month anniversary in August, reducing your return to 2.2 percent for the six months from Aug. 1 to Feb. 1, Pederson said.