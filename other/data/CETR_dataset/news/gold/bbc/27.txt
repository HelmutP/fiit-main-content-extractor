
Both Europe and the US have made the objective of bringing back Martian rocks to Earth laboratories a top priority for their space programmes.  A joint venture is likely to occur within the next 15-20 years.
<P>
But getting on and off a large planet will be extremely difficult, and the British satellite manufacturer Astrium is proposing to test the required technologies on the low-gravity target of Phobos first.
<P>
""It would be a three-year mission.  We're looking at a 2016 launch,"" said Marie-Claire Perkinson, a principal mission systems engineer at the Stevenage company.
<P>
<B>Many links</B>
<P>
Even so, the Phobos concept has a number of challenging steps, all of them capable of killing the mission if a single element goes wrong.
<P>
It is envisaged that a ""mothership"", powered by an ion engine, would fly into orbit around Mars where it would release a lander craft down on to the surface of the moon.
<P>

    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                
                     
                    <div>
	<div class=""mva"">
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/start_quote_rb.gif"" width=""24"" height=""13"" alt="""" border=""0"">
		<b>It's challenging because it requires a lot of new technology development</b>
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/end_quote_rb.gif"" align=""right"" width=""23"" height=""13"" alt="""" border=""0"" vspace=""0""><br clear=""all""/>	</div>




</div>
                
                     
                    <div class=""mva"">
	<div>Marie-Claire Perkinson, Astrium</div>


</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    



This robot vehicle might do some in-situ experiments but its main task would be to core, drill, or scoop up surface ""soil"" into a sealed vessel.
<P>
Then, it would lift off from Phobos using chemical thrusters to attempt to dock with, or be captured by, the passing mothership.  If that succeeds, the sample vessel would be transferred across and packaged inside an additional bio-secure sealed-container ready for the trip home.
<P>
Close to Earth, this capsule would be jettisoned into the atmosphere to make a hard landing; it would need no parachute assistance.   
<P>
""It's really the sample transfer chain which is the critical issue - right from landing on Phobos and taking the sample, and then passing it through the various vehicles to return to Earth,"" explained Ms Perkinson.
<P>
""It's challenging because it requires a lot of new technology development, and it's reliant on a lot of mechanisms, which is something we usually try to avoid."" 
<P>

    
    <!-- S IBOX -->
	<table cellspacing=""0"" width=""416"" border=""0"" cellpadding=""0"">
	<tr>
            
            <td class=""sibtbg"">
                
                        <div class=""sih"">
                            HOW THE PHOBOS SAMPLE RETURN MISSION MIGHT LOOK
                        </div>
                
		
                    <div class=""o"">
                            <img src=""http://newsimg.bbc.co.uk/media/images/42554000/gif/_42554811_astr_phob_mis.gif"" width=""416"" height=""233"" alt=""Mission architecture (BBC) "" border=""0"" vspace=""0"" hspace=""0"">
                    </div>
		
                
                     
                    <div class=""mva""><div class=""bull"">(1) The spacecraft could leave in 2016 when Earth and Mars are in a favourable alignment, reducing the mission length to three years</div>


<div class=""bull"">(2) Cruise phase would use a solar-electric engine.  This relies on solar power to accelerate xenon ions to produce forward thrust</div>


<div class=""bull"">(3) The mothership would go into orbit around Mars; the lander would be ejected to make its own way down to the surface of Phobos</div>


<div class=""bull"">(4) The lander could do some in-situ experiments, but its primary objective would be to package away surface material</div>


<div class=""bull"">(5) After lift-off, the lander would dock with, or be captured by, the mothership - a key test for Mars sample return technology</div>


<div class=""bull"">(6) The Phobos samples will be transferred to a sealed and bio-secure re-entry capsule for the journey home:  </div>


<div class=""bull"">(7) After ejection and Earth re-entry, the capsule would crash-land; no parachute would be used to slow its fall</div>


</div>
                
            </td>
        </tr>
	</table>
	