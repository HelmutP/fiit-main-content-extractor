
<!-- S SF -->
<B>The governor of Helmand province in Afghanistan says up to 700 insurgents have crossed over from Pakistan and are preparing to fight British forces.</B>
<P>
Haji Asadullah Wafa, who has been in his job just a few weeks, told the BBC foreign fighters were among them.
<P>
He said their intention was to disturb a major dam project being protected by British troops.
<P>
The UK taskforce in Helmand said it was aware of reports that insurgents had moved into the Sangin area.
<!-- E SF -->
<P>
A spokesman said it was nothing unusual and if it became necessary they would strike at a time of their choosing.
<P>
<B>Drugs trade</B>
<P>
There has been much talk of a spring offensive but the Helmand governor has given the biggest indication yet that hundreds of insurgents are preparing to fight British troops in southern Afghanistan.
<P>
He said the 700, including Arabs, Chechens and Pakistani Taleban had crossed into Helmand from Pakistan and had moved to Sangin, the centre of the drugs trade where British forces faced some of the heaviest fighting last summer.