
<!-- S SF -->
<B>The head of the Cartoon Network has resigned over a marketing stunt which caused a terror alert in Boston.</B>
<P>
The city went on high alert last month, with bridges and roads closed, after 38 flashing devices were discovered. 
<P>
One of the battery-powered signs, which promoted a Cartoon Network show, was destroyed in a controlled explosion. 
<P>
Station manager Jim Samples said he felt ""compelled to step down, effective immediately, in recognition of the gravity of the situation"".
<!-- E SF -->
<P>
""It's my hope that my decision allows us to put this chapter behind us,"" he wrote in a memo to staff.
<P>
<b>Compensation deal</b>
<P>
On Monday, US TV giant Turner Broadcasting, which owns Cartoon Network, and an advertising agency involved in the stunt agreed to pay Boston $2m (?1m) in compensation for the emergency response.
<P>
Half of the sum  is meant to cover the cost of security operation, while the other $1m is a goodwill gesture. 
<P>
The agreement resolved any potential civil or criminal claims against the two companies, but it is not clear whether charges against two men who allegedly placed the devices will also be dropped.  
<P>

<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42520000/jpg/_42520661_police_ap203.jpg"" width=""203"" height=""152"" alt=""The bomb squad investigates a device at Sullivan Square subway station"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">A police bomb squad blew up one of the marketing devices</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->
Sean Stevens, 28, and Peter Berdovsky, 27, have pleaded not guilty to placing hoax devices and disorderly conduct. 
<P>
The battery-powered signs had also been placed in nine other cities as part of the publicity campaign without causing any problems. They had apparently been in place for several weeks before the Boston alert. 
<P>
Each device featured a character from the adult-themed late-night show Aqua Teen Hunger Force raising its middle finger. 
<P>
But the publicity stunt does not seem to have raised interest in the show.
<P>
The cartoon averaged 386,000 viewers last week among its target audience of 18-to-24-year-olds, according to Nielsen Media Research, almost the same as the week before.
<P>
<P>