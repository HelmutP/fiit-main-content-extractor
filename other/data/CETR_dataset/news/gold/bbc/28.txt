

<p>
Snow Patrol's Chasing Cars was one of the biggest selling singles of 2006. On sales of CDs and downloads it went as high as number six in the UK charts. 
<p>
As is standard practice, a few weeks later the record company deleted the CD and removed it from the shops. 
<p>
With no physical format available to buy, the song no longer qualified for a chart position and disappeared from the top 40.
<p>
But with music download sites now the UK's favourite place to buy singles, each with massive back catalogues of songs, it was decided that just listing the singles currently on release may not reflect the way people were actually buying songs. 
<p>
So from 1 January 2007, every song that is available to download is now allowed to chart.
<p>
<B>Long-tail</B>
<p>
""In the days of the physical single you were basically restricted to what record companies released in a particular week, and what physical retailers were able to stock,"" explained Steve Redford of the Official UK Charts Company. 
<p>
""And even the biggest store wasn't going to stock more than about 100 singles at any one time.""
<p>
""In this new world you've literally got a choice of 300 million tracks every week. This changes the whole economics of the record business, because keeping things in stock isn't really that expensive in the digital world.""
<p>
It is called the long-tail, the gradual sales of music, books and DVDs that are permanently available long after their release date. 
<p>
Some analysts think this is going to make the most money in the internet age. Instead of trying to selling a lot of a little, you sell a little of a lot.
<p>
So how did all this affect the new look chart? Well, in the first week, long after the CD was deleted, Snow Patrol re-entered the charts at number nine on downloads alone - people had been downloading it in enough numbers all along.
<p>
<B>Unsigned and in the charts</B>
<p>

<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42434000/jpg/_42434611_koopa1_203.jpg"" width=""203"" height=""152"" alt=""Koopa"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">Koopa are the first unsigned band to land a UK top 40 hit</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->
<p>
The following week, another first - the band Koopa became the first band to enter the top 40 without ever having a record deal, or a record in the shops. 
<p>
Doing their own online marketing, the band bypassed the need to have a big record company behind them. 
<p>
So, definitely one in the eye for the industry, but as Joe Murphy from the band points out - without any help, it was tough going.
<p>
""We built our own website. Then we started advertising that on Google, places like that. From there it was just getting on MySpace and our website, and making sure you're keeping people up to date with regular newsletters, messages and blogs on MySpace.""
<p>
<p>
""From MySpace people were taking our banners and things like that, and putting them on their MySpace pages and we thought we could take that further and have things on MySpace and our website that people can download or send to their friends, just to invite them to check out the band.""
<p>
It is not the first time an artist has used the power of the web, and social networking sites like MySpace, to create the hype to launch their career - Lily Allen famously did just that last year. 
<p>

    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                    <div class=""o"">
                            <img src=""http://newsimg.bbc.co.uk/media/images/42541000/jpg/_42541469_beatles_bw_203.jpg"" width=""203"" height=""152"" alt=""The Beatles"" border=""0"" vspace=""0"" hspace=""0"">
                    </div>
		
                
                     
                    <div>
	<div class=""mva"">
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/start_quote_rb.gif"" width=""24"" height=""13"" alt="""" border=""0"">
		<b>There's growing speculation that the Beatles catalogue is going to be made available online for the first time in the next few weeks</b>
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/end_quote_rb.gif"" align=""right"" width=""23"" height=""13"" alt="""" border=""0"" vspace=""0""><br clear=""all""/>	</div>




</div>
                
                     
                    <div class=""mva"">
	<div>Steve Redford, Official UK Charts Company.</div>


</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    



<p>
But Koopa were the first successful band who did not even wait to be signed up by a record company.
<p>
So is this the nirvana every band has been searching for? A world where you can have a hit without having to impress the suits at the big music labels? The suits do not think so.
<p>
""To make a splash in the consumer mind these days, more often than not, you need the power of a big company behind you,"" says Mr Redford. 
<p>
""Even in the digital world there's a requirement for someone to do that job.""
<p>
""It may well be the case that some bands decide, effectively, to create their own record company, but nobody should be in any doubt that there's a lot of work attached to that. They can't simply decide that they're going to have a hit, they're going to have to work it just like a record company would do.""
<p>
""In other words, the band becomes the record company.""
<p>
In fact, the big music companies may be tempted to just sit back and let the bands do the work.
<p>
<B>Fan base</B>
<p>
""I've heard rumours of A&R departments that will only go to a band's gig once they've got a thousand friends on MySpace because they want that momentum to exist already, they don't want to have to create it themselves,"" said Paul Stokes, news editor of NME. 
<p>
""So it's almost like the bands have to do the work and then A&Rs can come along and cherry pick the ones they want.""
<p>
The UK is ahead of the curve on this one. Worldwide, downloads only account for 10% of music sales, but it is a good indicator of how the industry in each country will eventually change. 
<p>
And there are more changes to come, thinks Steve Redford.
<p>
""Go back a couple of years ago and there was a real chance that the singles chart had gone stale. It was predictable. Record companies became very good at marketing things into the charts. In the new digital world all bets are off.""
<p>
""There's growing speculation that the Beatles catalogue is going to be made available online for the first time in the next few weeks. 
<p>
""The significance of this new rule change in the charts is it's entirely possible that you could end up with the top 10 in the singles chart entirely dominated by Beatles tracks.""
<p>
