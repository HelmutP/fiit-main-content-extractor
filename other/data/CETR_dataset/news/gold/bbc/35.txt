

If you are driving through Switzerland, south to Italy, you are likely to take the route via the charming town of Lucerne and that means driving through the Sonnenberg tunnel. 
<p>
Those tunnels around Lucerne can be quite irritating, especially in fine weather. Just as you are enjoying a spectacular view of the lake and the mountains, you are plunged into darkness. 
<p>
But when you get to the Sonnenberg, make sure your eyes adjust, and take a closer look, for this is much more than a tunnel. In here is the world's largest nuclear shelter. 
<p>
Under Swiss law, local governments are required to provide shelter spaces for everyone, and in the early 1970s Lucerne was short  by several thousand. The new Sonnenberg motorway tunnel, just being built, seemed a neat solution: kit it out as a nuclear shelter as well and it could hold 20,000 people.
<p>
    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                
                     
                    <div>
	<div class=""mva"">
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/start_quote_rb.gif"" width=""24"" height=""13"" alt="""" border=""0"">
		<b>The Sonnenberg, in theory, is able to withstand a one megaton nuclear bomb, as close as half a mile away</b>
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/end_quote_rb.gif"" align=""right"" width=""23"" height=""13"" alt="""" border=""0"" vspace=""0""><br clear=""all""/>	</div>




</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    




 ""Actually we got the idea from you British,"" explains Werner Fischer, the local civil protection chief, as he shows me around. ""Londoners used the underground as shelter during the blitz."" 
<p>
Well maybe, but believe me, there are things in the Sonnenberg that you will never find down the Finchley Road tube station.
<p>

<B>'Engineering feat'</B><p>

<p>

It starts with the doors, which are a metre and a half thick (5ft), and weigh 350 tonnes each. The Sonnenberg, in theory, is able to withstand a one megaton nuclear bomb, as close as half a mile away. 
<p>
<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42555000/jpg/_42555909_operating_theatre203.jpg"" width=""203"" height=""152"" alt=""Operation theatre in the Sonnenberg shelter"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">The shelter was designed to be self-sufficient </div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->

One megaton is 70 Hiroshimas. That means the Sonnenberg residents would have emerged to a world reduced not to smoking rubble, but to ash. 
<p>

Inside, the tunnel is a surreal monument to neutral Switzerland's desire to survive a total war which would, naturally, have been started and waged by someone else. 
<p>

Every eventuality has been thought of. 
<p>
There are vast sleeping quarters, with bunk beds four layers deep. There is an operating theatre, a command post, and as Mr Fischer points out, a prison. ""Just because there's a nuclear war outside doesn't mean we won't have any social problems in here,"" he says. 
<p>
<p>
    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                
                     
                    <div>
	<div class=""mva"">
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/start_quote_rb.gif"" width=""24"" height=""13"" alt="""" border=""0"">
		<b>Some of my friends have private ones in their own houses, used, these days, mostly to store wine or skis.</b>
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/end_quote_rb.gif"" align=""right"" width=""23"" height=""13"" alt="""" border=""0"" vspace=""0""><br clear=""all""/>	</div>




</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    




<p>


There were even, it is rumoured, plans for a post office, until someone asked the obvious question ""when the world outside is burning, who would you write to? What would the address be, not to mention who would deliver your letter?""
<p>

Then there are the coloured lights, indicating whether it is night or day outside. Obviously the country which produces the world's top watches would not like to lose track of time. 

<p>


There are some truly impressive feats of engineering: the air filters, designed to supply those 20,000 souls with 192 cubic metres each of non-radioactive air every day, are indeed breathtaking. So large, the hall they are housed in has the dimensions of a medieval cathedral.
<p>

<B>Shelter choice</B>

<p>

But while the Sonnenberg may be the biggest shelter, it is by no means the only one.
<p>
<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42555000/jpg/_42555915_boots203.jpg"" width=""203"" height=""152"" alt=""Boots in a family's nuclear shelter which is being used for storage"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">Many shelters are now being used a storage spaces</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->


In fact, there are over a quarter of a million of them in Switzerland, because, 17 years after the end of the Cold War, the policy of providing shelters for the entire population still stands. 
<p>
Some of my friends have private ones in their own houses, used, these days, mostly to store wine or skis. My house, though does not have one. 
<p>

An anxious telephone call to my local civil protection office brings a reassuring answer. ""Actually your community has 40% overcapacity in shelters,"" I'm told. 
<p>
It turns out that, should the unthinkable happen, I have got a luxury of choice. I can settle into a cosy neighbourhood shelter designed for 10. Sounds good, as long as my family and the neighbours we get on with get there first. 
<p>

Or, there is a larger shelter, beneath the local fire station, which those without private shelters would share with the firemen. I can see it is not going to be the easiest of decisions. 
<p>

And down on the main street of my village, new houses are going up, the bulldozers are digging remarkably deep and  blast resistant concrete is arriving by the tonne. 
<p>
But why add an estimated 4% to the house price, just to carry on preparing for a threat that has gone away? 
<p>
<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42555000/jpg/_42555913_digger_203.jpg"" width=""203"" height=""152"" alt=""A man building a deep trench for a nuclear bunker with a digger"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">Until the law changes, bunkers will continue to be dug</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->

<p>

Karl Widmer, deputy director of Switzerland's civil defence department, looks a little sheepish when I put this to him. 
<p>

""We asked ourselves this question,"" he admits. ""But then we thought, we've built all these things, so let's just carry on. And there could be new threats around the corner.""
<p>

""What threats exactly?"" snorts a Social Democrat member of parliament. ""Bird flu? Terrorism? An underground bunker won't protect against that. It's time we stopped this nonsense, all we're doing is building very expensive wine cellars.""
<p>

Later this year, the Swiss government will decide whether to continue the shelters-for-all policy, but this week, sirens right across Switzerland were tested, and the population had to check their bunkers were up to scratch.
<p>

The monstrous Sonnenberg shelter though, is being gradually dismantled. But not because it has finally been deemed unnecessary: no, no, the real problem is those 350 tonne blast doors. When they were tested, they would not shut.

<p>





<b>From Our Own Correspondent was broadcast on Saturday, 10 February, 2007 at 1130 GMT on BBC Radio 4. Please check the <a class=""inlineText"" href=""http://news.bbc.co.uk/1/hi/programmes/from_our_own_correspondent/3187926.stm"">programme schedules </a> for World Service transmission times.</b>