
<!-- S SF -->
<B>Six people have been killed exploring a tunnel on the Spanish island of Tenerife, one of the Canary Islands, local officials have said.</B>
<P>
The six probably died from a lack of oxygen in the tunnel, authorities said.
<P>
A group of 30 people, including scientists, went into the Los Silos tunnel system on Saturday and became lost when they took a wrong turn.
<P>
One of the group managed to escape and alerted authorities who rescued the other 23 survivors of the group.
<!-- E SF -->
<P>
To recover the bodies the emergency services said they needed to use oxygen tanks. 
<P>
Among the tunnel explorers were scientists from the Canary Islands Astrophysics Institute and members of the Tenerife Friends of Nature Association.
<P>
The tunnels were dug 200 years ago in an attempt to find water.
<P>
The Canary Islands are in the Atlantic Ocean south-west of Spain, off the coast of Africa.
<P>