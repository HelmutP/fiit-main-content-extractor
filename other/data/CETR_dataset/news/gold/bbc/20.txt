

In a speech in the Macedonian capital Skopje on Thursday, Mr Rehn described developments in the country over the last year as ""alarming and a cause for concern"".  
<p>
Mr Rehn's main worry is the tension between the ruling conservatives and Macedonia's largest ethnic Albanian party DUI, led by Ali Ahmeti. 
<p>
Sixteen deputies, mainly from Mr Ahmeti's party, walked out of parliament and are threatening to boycott municipal authorities. 
<p>
DUI is accusing the government of lacking legitimacy and ignoring the rights of ethnic Albanians, who form about one-quarter of the country's population of two million.
<p>
<B>Changing times</B>
<p>
Mr Ahmeti is a guerrilla leader turned politician. 
<p>
    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                
                     
                    <div>
	<div class=""mva"">
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/start_quote_rb.gif"" width=""24"" height=""13"" alt="""" border=""0"">
		<b>We need to exert pressure on both the government and the opposition to respect the rules of the game</b>
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/end_quote_rb.gif"" align=""right"" width=""23"" height=""13"" alt="""" border=""0"" vspace=""0""><br clear=""all""/>	</div>




</div>
                
                     
                    <div class=""mva"">
	<div>Olli Rehn<br />EU Enlargement Commissioner</div>


</div>
                
                    
                        <div class=""o"">
                            <img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/inline_dashed_line.gif"" width=""203"" height=""1"" alt="""" border=""0"" vspace=""2"" hspace=""0""><br />
                        </div>
                     
                    <div class=""miiib"">
    
	<!-- S ILIN -->
        
        
		<div class=""arr"">
			<a class="""" href=""/2/hi/europe/country_profiles/1067125.stm""><b>Profile: Former Yugoslav Republic of Macedonia</b></a>
		</div>
        
	<!-- E ILIN -->
    


</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    





One of the founders of the Kosovo Liberation Army, he returned to his native Macedonia to lead ethnic Albanians in the 2001 revolt against government forces that took the country to the brink of civil war. 
<p>
He then joined the coalition government formed as a result of the Ohrid peace accord, mediated by the EU and Nato. 
<p>

But despite winning most Albanian votes in the 2006 election, Mr Ahmeti has found himself in opposition for the first time, after Prime Minister Nikola Gruevski chose a smaller Albanian party for his coalition.
<p>
A year ago, the EU praised Macedonia as the only functioning multi-ethnic democracy in the Balkans, rewarding it with the coveted candidate status. 
<p>
Now things are different, with Mr Rehn criticising all parties for ""obstruction and lack of faith"".
<p>
He called on them to work within the democratic institutions they had worked so hard to set up. 
<p>
<B>'Wake-up call'</B>
<p>
To press his point, the EU commissioner met the Macedonian prime minister no less than five times during his one-day trip. 
<p>
<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42552000/jpg/_42552205_afp_rehn203.jpg"" width=""203"" height=""152"" alt=""EU Enlargement Commissioner Olli Rehn"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">The EU commissioner has changed his tune on Macedonia</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->

He also held separate talks with all party leaders one by one. 
<p>
Even the silver-haired Mr Ahmeti was persuaded to return to the parliament building to meet the man from Brussels. 
<p>
""It's very important that at the time of negotiations on the future status of Kosovo and concerns over the stability of the region, we can prevent the political stalemate in this country turning into a crisis,"" Mr Rehn told the BBC. 
<p>
""We need to exert pressure on both the government and the opposition to respect the rules of the game and ensure the legitimacy of the political system. 
<p>
""This was meant to be a strong wake-up call and I hope we'll see a reinforcement of mutual trust, a consensus on the rights of minorities and reforms related to European aspirations."" 
<p><b>Sluggish reforms</b>
<p>
Diplomats are concerned that reforms have slowed down in Macedonia after it received candidate status, especially in key areas such as fighting corruption and reforming the police. 
<p>
EU membership was not automatic, Olli Rehn warned, and substantial reforms were needed before the European Commission could seriously consider recommending the start of membership talks. 
<p>
The government hopes to receive a date for accession negotiations at the end of this year, but that looks increasingly unrealistic.