
<!-- S SF -->
<B>Mauritania has agreed to let a stricken ship carrying several hundred migrants ashore, the Spanish government says.</B>
<P>
It says the Marine 1 is being towed to the Mauritanian port of Nouadhibou.
<P>
A deal was initially reached on Friday, but Mauritania insisted on further guarantees before authorising the arrival of the vessel, Madrid says.
<P>
It says the sick and vulnerable will be treated in Mauritania, while Spain will arrange the repatriation of the rest of the migrants from Asia and Africa.
<!-- E SF -->
<P>
The ship broke down in international waters last week and was towed close to shore by a Spanish boat.
<P>
The migrants were believed to be trying to reach Spain's Canary Islands in the Atlantic.
<P>
<b>Marathon talks</b>
<P>
Spanish Interior Minister Alfredo Perez Rubalcaba made an announcement about the deal with Mauritania on Saturday. 
<P>
He told reporters that the Mauritanian authorities would now allow the ship to be towed to dry land, after a legal wrangle lasting eight days. 
<P>

    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                    <div class=""o"">
                            <img src=""http://newsimg.bbc.co.uk/media/images/42539000/gif/_42539587_africa_noadhibou_map203.gif"" width=""203"" height=""152"" alt=""map"" border=""0"" vspace=""0"" hspace=""0"">
                    </div>
		
                
                    
                        <div class=""o"">
                            <img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/inline_dashed_line.gif"" width=""203"" height=""1"" alt="""" border=""0"" vspace=""2"" hspace=""0""><br />
                        </div>
                     
                    <div class=""miiib"">
    
	<!-- S ILIN -->
        
        
		<div class=""arr"">
			<a class="""" href=""/2/hi/in_pictures/6347397.stm""><B>In pictures: Migrant ship</B></a>
		</div>
        
	<!-- E ILIN -->
    


</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    




<P>
""Yesterday [Friday] there was a verbal agreement and when we came to writing the document Mauritania added some new clauses and that reopened negotiations, which went on all night and until midday today,"" Mr Rubalcaba told reporters. 
<P>
Mauritania initially refused to let the ship ashore, saying it had no connection to the country.
<P>
The case has taken on particular importance for Madrid, which last year launched a political offensive in west Africa to stem the flow of poverty-stricken immigrants trying to reach the Canary Islands as a means of entry into Europe, the BBC's Mike Lanchin says. 
<P>
The migrants have said some of them come from Kashmir but did not specify whether it was Pakistan- or India-administered Kashmir.
<P>
Aid workers have delivered food and water to the migrants, who are thought to have set sail from Guinea.
<P>
After the ship lands, Guinean officials will attempt to identify the migrants, so they can be sent to their home countries, Spanish Deputy Prime Minister Maria Teresa Fernandez de la Vega said on Friday.
<P>
Neighbouring Senegal has also said it could not handle the ship.
<P>
About 30,000 Africans were caught trying to reach the Canary Islands last year.
<P>
EU patrols are now trying to stem the flow of immigrants.