
<!-- S SF -->
<B>The Socialist candidate in France's presidential election, Segolene Royal, has launched her manifesto in Paris. </B>
<P>
She announced a 100-point platform with a strong emphasis on social programmes, promising a higher minimum wage and the construction of more low-rent housing.
<P>
Ms Royal has been criticised for delaying the release of her platform until just 10 weeks before the first round of elections.
<P>
She has fallen behind her right-wing rival Nicolas Sarkozy in opinion polls.
<!-- E SF -->
<P>
Ms Royal unveiled her ""presidential pact"" in front of a cheering crowd of Socialist Party delegates who frequently broke into chants of ""Segolene, president!""
<P>
""I feel today I can propose to you something more than a platform,"" she said.
<P>
""A pact of honour, a presidential pact that I propose to everyone, the most vulnerable and the strong, those who have been our supporters all along and those who have not, because France needs all its people.""
<P>
Many of the 100 points already feature in the Socialist Party's election programme released last year.
<P>
Others, such as proposals to set up citizens' juries to evaluate the work of the National Assembly and military-style boot camps for young offenders are new ideas.
<P>
<b>'Listening phase'</b>
<P>
Ms Royal said she wanted to boost the monthly minimum wage from 1,250 euros ($1,625, ?834) to 1,500 euros, and build 120,000 low-rent homes every year. 
<P>

<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42560000/jpg/_42560123_royalsupporter_ap203b.jpg"" width=""203"" height=""152"" alt=""Segolene Royal supporter at her manifesto speech"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">Ms Royal spoke to a crowd of enthusiastic supporters</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->
Benefits for the unemployed would be raised and pensions increased for low-income retirees. 
<P>
Ms Royal has defended her decision to delay the release of her manifesto, saying she had been in a ""listening phase"" - gathering ideas from the people of France via the internet and thousands of public meetings across the country.
<P>
Segolene Royal was France's political star of the second half of last year in France. 
<P>
She had a meteoric rise thanks to her crowd-pleasing campaigning skills and trounced two more senior party figures in the Socialist domination for April's election. 
<P>
But since the New Year things have begun to unravel, reports Hugh Schofield. There have been gaffes, signs of internal dissent and Nicolas Sarkozy has surged ahead in the polls.