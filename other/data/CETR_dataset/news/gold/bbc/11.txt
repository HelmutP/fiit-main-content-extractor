
<B>The face of the two-year-old boy is stained with tears but take a look closer and you see that the youngster, in torn jumper and shorts, is wracked by fever. </B>
<p>
His name is Anaclit and his father Jeremia, also skinny and frail, explains that his son has grown ill after the food in the house started to run out - a looming crisis triggered by  the onset of floods.
<p>
""We have one meal a day for everyone. In the morning there are always fights among the children over food,"" says Jeremia Higuburundi.
<p>
Jeremia's fields of maize and sweet potato were ruined by intense rains. Like his neighbours, he has lost his capacity to feed his six children.  
<p>
Tens of thousands of other families in Ngosi in northern Burundi face a similar plight. 
<p>
A sign of trouble ahead in this central African state, trying to get back on its feet after 13 years of civil war.
<p>
<B>Visual contradictions</B>
<p>
On the surface it is hard to believe that Burundi is teetering on the edge of a food crisis.
<p>
The lush green hills belie the fact that as many as one in four people who live in this country of seven million soon may not have food to eat. 
<p>

<!-- S IIMA -->
	
		<table border=""0"" cellspacing=""0"" align=""right"" width=""203"" cellpadding=""0"">
			<tr><td>
			<div>
				<img src=""http://newsimg.bbc.co.uk/media/images/42541000/jpg/_42541267_burundi.jpg"" width=""203"" height=""152"" alt=""A child in front of flooded houses in Gatumba, near Bujumbura. Pic: WFP"" border=""0"" vspace=""0"" hspace=""0"">
				<div class=""cap"">A state of emergency has been declared in several provinces</div>
			</div>
			</td></tr>
		</table>
		
	

	
<!-- E IIMA -->
For the aid agencies trying to convince the world that Burundi needs urgent international assistance, the visual contradictions, make it a tough message to get across.
<p>
The recent floods have devastated 80% of last November's crop. The next harvest is not due until June. 
<p>
In Ngosi fields which at this time of year should be rich with sweet potato, rice and maize, have been turned into swamps. 
<p>
The government has declared a state of emergency in nearly half of the provinces and the UN's World Food Programme is appealing for an extra $12m (?6.1m).
<p>
The landscape in Burundi makes it particularly vulnerable to the erratic weather conditions brought about by climate change affecting large swathes of Africa.  
<p>
The stunning hills makes soil erosion a particular problem when the rainfall is sudden and intense.
<p>
<B>Tough times ahead</B>
<p>
With more and more headlines about food crises in the developing world there is now a push to get African governments to take responsibility for their own humanitarian crises.
<p>
Ministers have rightly been accused of mishandling food stocks around the region. But Burundi is so poor it simply does not have any reserves to draw upon. 
<p>

    
    <!-- S IBOX -->
	<table cellspacing=""0"" align=""right"" width=""208"" border=""0"" cellpadding=""0"">
	<tr>
            <td width=""5""><img src=""http://newsimg.bbc.co.uk/shared/img/o.gif"" width=""5"" height=""1"" alt="""" border=""0"" vspace=""0"" hspace=""0""></td>
            <td class=""sibtbg"">
                
		
                
                     
                    <div>
	<div class=""mva"">
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/start_quote_rb.gif"" width=""24"" height=""13"" alt="""" border=""0"">
		<b>You can't get economic growth on an empty stomach or sustain peace on an empty stomach</b>
		<img src=""http://newsimg.bbc.co.uk/nol/shared/img/v3/end_quote_rb.gif"" align=""right"" width=""23"" height=""13"" alt="""" border=""0"" vspace=""0""><br clear=""all""/>	</div>




</div>
                
                     
                    <div class=""mva"">
	<div>Stephanie Savariaud<br />UN World Food Programme</div>


</div>
                
            </td>
        </tr>
	</table>
	
    <!-- E IBOX -->
    




<p>
As a country in transition it has relied on the UN World Food Programme (WFP) to help feed its own people. Critics see this as fostering a culture of dependence. 
<p>
But Stephanie Savariaud from WFP says that fails to take into account Burundi's complex food security issues and levels of poverty.
<p>
""You definitely need more than food aid to get a country back on its feet after 13 years of civil war, and it will take other people to help Burundi recover.
<p>
""But you can't get economic growth on an empty stomach or sustain peace on an empty stomach,"" Ms Savariaud said.
<p>
For the people of Ngosi the next few months signal tough times ahead. They already feature high on the malnutrition statistics. 
<p>
Up to 45% of the population is suffering from chronic malnutrition - not distended stomachs - but an insidious form of hunger which stunts growth and a country's ability to prise itself out of poverty.
<p>
In Burundi this has traditionally been the lean season - but unable to rely on the fat of the land because crops have been destroyed, there are very real fears that the plight of its people could rapidly escalate. 
