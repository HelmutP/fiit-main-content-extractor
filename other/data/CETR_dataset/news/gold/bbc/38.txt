
<!-- S SF -->
<B>Iconic rock group The Doors and folk singer Joan Baez have received Grammy awards for lifetime achievement.</B>
<P>
They were among the acts celebrated by the US Recording Academy in recognition of careers that changed music. 
<P>
Also honoured were psychedelic rockers The Grateful Dead, soul band Booker T and The MGs, jazz legend Ornette Coleman and opera singer Maria Callas.
<P>
The lifetime awards were presented on the eve of the main Grammy ceremony, where Mary J Blige leads nominations.
<!-- E SF -->
<P>
The Doors' guitarist Robby Krieger said the band's late singer, Jim Morrison, would have been ""very honoured"" to be recognised.
<P>
""People think he was anti-establishment, but in reality he wanted to be bigger than the Beatles,"" he said.
<P>
<b>Tributes</b>
<P>
Surviving members of The Grateful Dead, whose lead singer Jerry Garcia died in 1995, also paid tribute to their former bandmate.
<P>



    
        
            <!-- S IBOX -->
            <p />
            
            
                    


	&quot;<i>President Bush is the best publicity agent I've ever had&quot;</i>



            
                    


	<br /><b>Joan Baez</b><br />

            
            <br />
            
                    <img src=""http://newsimg.bbc.co.uk/media/images/42559000/jpg/_42559469_baez_203.jpg"" align=""left"" width=""203"" height=""152"" alt=""Joan Baez"" border=""0"" vspace=""0"" hspace=""4"">
            
            <!-- E IBOX -->
        
    

""I wish the rest of my brothers in the band could be here,"" said Bill Kreutzmann, one of the group's two drummers.
<P>
Booker T. Jones, whose band epitomized the Memphis soul sound, thanked his family ""for keeping me alive all these years.
<P>
""It's been a difficult thing to do,"" he said.
<P>
He and other musicians from his group made reference to band-member Al Jackson, who died from a gunshot in 1975.
<P>
Saxophonist Ornette Coleman, one of the main innovators in the 1950s free jazz movement, also used his speech to muse about the meaning of life and death.
<P>
""How do we kill death since it kills everything?"" he asked. ""You don't have to die to kill and you don't have to kill to die.""
<P>
<b>Bush tribute</b>
<P>
Protest singer Baez, who was a key figure in the anti-Vietnam war movement. closed the ceremony by paying tribute to US President George W Bush.
<P>
""President Bush is the best publicity agent I've ever had,"" she said.
<P>
 ""People always ask me to compare then versus now. It's very much like a re-run."" 
<P>
Other prizes handed out at the ceremony included the Trustees award, presented to Stax Records co-founder Estelle Axton, theatrical composer Stephen Sondheim, and New Orleans-based engineer Cosimo Matassa.
<P>
The award recognises outstanding contributions to the recording industry in a non-performing capacity.
<P>
Recording engineer David M. Smith and the Yamaha Corporation, which manufactures a variety of recording equipment and musical instruments, each won a Technical Grammy award.
<P>
<P>
<P>