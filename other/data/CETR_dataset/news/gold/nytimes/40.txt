<img src=""http://graphics.nytimes.com/images/u.gif"" align=left alt=U>ntil Greg LeMond showed
that an American could win
the Tour de France, following
professional cycle racing from the
United States was a desperate business. Race results from the tour and
other races like the Giro D'Italia and
the Tour of Flanders typically came
weeks, if not months, late through the
pages of outrageously overpriced
European magazines like the old
Miroir du Cyclisme.
<p>
<p>
<NYT_INLINEIMAGE version=""1.0"" type=""main"">
<table align=right border=0 cellpadding=2 cellspacing=2 width=260>
<tr><td rowspan=2 width=10><br></td><td align=right>
<font size=-2>
</font></td></tr><p>
<tr><td align=left><font size=-1>
<HR SIZE=1>
<font size=-1>Overview
<P> &#149;&nbsp;<a href=""04libe.html"">Behind the Tour de France, and Beyond</a>
<p>
<font size=-1>This Week's Reviews
<P> &#149;&nbsp;<a href=""04libe-rive.html"">'Rivendell Reader'</a>
<P> &#149;&nbsp;<a href=""04libe-velo.html"">'Velo News Interactive'</a>
<P> &#149;&nbsp;<a href=""04libe-unio.html"">'Union Cycliste Internationale'</a>
<P> &#149;&nbsp;<a href=""04libe-tota.html"">'The Total Cycling Encyclopedia'</a>
<P> &#149;&nbsp;<a href=""04libe-soci.html"">'Official Tour de France'</a>
<P> &#149;&nbsp;<a href=""04libe-saga.html"">'The Tour Saga'</a>
<HR SIZE=1>
</td></tr></table>
<!--@ -->
</NYT_INLINEIMAGE>
LeMond's extraordinary success -- he won the Tour de France
three times, first in 1986 -- unquestionably increased cycling coverage,
both in print and on television. But
despite that, timely reporting remained largely concentrated on just
the Tour de France. Now the Web
has changed all that.
<p> Through a variety of sources,
American fans are now being informed as quickly and almost as well
as their European counterparts. All
that is lacking now is live, full-motion
video; Web experiments last year by
France 3, one of the two broadcasters of the Tour de France in its
homeland, suggest that some form of
that may not be far off.
<p> That may put fans ahead of the cycling press. The dirty secret of covering the Tour de France, which I have
done, is that the journalists rarely
see the racers in action. Instead, they
crowd into French cars for hours of
white-knuckle driving through huge
crowds ahead of the racers to catch
the finish of the race on television. Be
grateful: no Web site recreates that.
<p>
<p>
<p>
&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
