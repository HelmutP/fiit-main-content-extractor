<img src=""http://graphics.nytimes.com/images/m.gif"" align=left alt=M>aps do more than lead you 
from point A to point B. For 
the closet cartographer, a 
map's hieroglyphics reveal a portrait of the world's countries, cities, 
mountains, plains and oceans 
through a singular vocabulary of 
symbols, colors and typefaces.
<p>    Of course, maps are a two-way 
street: they not only depict the world 
for their readers but also  say volumes about the people who made 
them by what they include and leave 
out, where the borders are and what 
a city is called -- whether Londonderry in Northern Ireland, for example, is rendered as Derry, the Irish 
nationalists' preference. A good map 
fires the imagination like a good novel. Here are three CD-ROM sets to 
curl up with on a rainy day. 
<p> 
<HR SIZE=1>
<NYT_INLINEIMAGE version=""1.0"" type=""main"">
<table align=left border=0 cellpadding=2 cellspacing=2 width=486>
<tr><td align=left><font size=-1>
<font size=-1> Overview </b> 
<P> &#149;&nbsp;<a href=""30libe.html"">Magic Carpets for Armchair Explorations</a>
<p>
<font size=-1> This Week's Articles </b> 
<P> &#149;&nbsp;<a href=""30libe-appl.html"">Appalachian Trail</a>
<P> &#149;&nbsp;<a href=""30libe-ngeo.html"">National Geographic Maps</a>
<P> &#149;&nbsp;<a href=""30libe-topo.html"">Topo USA</a>
</td></tr></table>
<!--@ -->
</NYT_INLINEIMAGE>
<p> 
<p> 
<BR CLEAR=ALL>
<p> 