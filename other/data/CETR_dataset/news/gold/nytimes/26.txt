<img src=""http://graphics.nytimes.com/images/h.gif"" align=left alt=H>uman
anatomy is a subject
that can veer in an instant
from thrilling and chilling to
dry as dust.
<p>
<p>
<NYT_INLINEIMAGE version=""1.0"" type=""main"">
<table align=right border=0 cellpadding=2 cellspacing=2 width=260>
<tr><td rowspan=2 width=10><br></td><td align=right>
<img src=""08libe.1.jpg""><br>
<font size=-2>
George B. Diebold/The Stock Market
</font></td></tr><p>
<tr><td align=left><font size=-1>
<font size=-1>Overview
<P> &#149;&nbsp;<a href=""08libe.html"">Computers Open a Window on the Body</a>
<p>
<font size=-1>This Week's Articles
<P> &#149;&nbsp;<a href=""08libe-voya.html"">Body Voyage</a>
<P> &#149;&nbsp;<a href=""08libe-visi.html"">Visible Human CDs</a>
<P> &#149;&nbsp;<a href=""08libe-work.html"">Bodyworks</a>
<HR SIZE=1>
</td></tr></table>
<!--@ -->
</NYT_INLINEIMAGE>
During the Renaissance, public
dissections provided all the recreational titillation that we get from
gory movies and roller coasters today. In Italy, prominent people, often
masked and dressed in costume,
crowded into dissection amphitheaters during carnival time. Servants
passed out flowers, oranges and
sticks of incense to dispel the noxious
odors of the proceedings. Two smoking torches illuminated the dissection table, where the professor of
anatomy, robed in red, pointed out
structures of interest with a long, tapered rod.
<p> Although the demonstrations were
intended to provoke a mood of solemn moral reflection, audiences
tended to become a little giddy, particularly when female anatomy was
displayed.
<p>In Holland, the medical
historian Frank Gonzalez-Crussi
wrote in ""Suspended Animation""
(Harcourt Brace, 1995), laws actually had to be passed ""to check the inappropriate outbursts of persons
who laughed, clapped, asked indecent questions, grabbed the specimens prepared by the dissector, or
otherwise attempted to disrupt the
solemnity of the occasion.""
<p> For anatomy without a trace of
pageantry or prurience, open a copy
of ""Gray's Anatomy,"" which has
been periodically reissued in all its
incomparable turgor for the last century and a half.
<p>
<p> ""The entire skeleton in the adult
consists of 200 distinct bones,"" begins Dr. Gray, and he proceeds to
list them all, followed by all the
joints, and all the ligaments, muscles, nerves, arteries and veins, totaling tens of thousands of structures,
most with long Latin names, their
three-dimensional intricacy reduced
to enormous incomprehensible paragraphs with very few illustrations.
Medical students are reduced to
tears by Dr. Gray and anatomists
like him who bled all the color and
art from their subject and demand
rote memorization instead.
<p>
<p> Computers have provided the opportunity to combine the precision of
the classical texts with some of the
three-dimensional thrills and chills
that kept those Renaissance audiences glued to their benches.
<P>
<HR SIZE=1>
<b> Brain Site </b> 
<BR>
<a href=""http://www.vh.org/Providers/Textbooks/BrainAnatomy/BrainAnatomy.html"">www.vh.org/Providers/Textbooks/BrainAnatomy/BrainAnatomy.html
</a>
<p>  The anatomy of the brain is 
the most difficult of any part 
of the body to visualize, with 
complicated structures crumpled together into a space the 
size of a cantaloupe. Neuroanatomists at the University of 
Iowa have put together a set of 
detailed photographs of sections of the human brain,  with 
schematic drawings that allow 
each structure to be identified. 
The result is the equivalent of 
a very good textbook of brain 
anatomy.
<P>
<b> AMA Site </b> 
<BR>
<a href=""http://www.ama-assn.org/insight/gen_hlth/atlas/atlas.htm"">www.ama-assn.org/insight/gen_hlth/atlas/atlas.htm
</a>
<p>  Sometimes detailed anatomical photographs are only 
distracting when what you 
really need is a quick idea of 
what exactly it means that a 
relative's stroke may have 
damaged Wernicke's area of 
the brain.   
<p>This site, sponsored 
by the American Medical Association, provides simple  line 
drawings ,  with brief explanations of how the organs work.
<p> 
<P>
<i> Dr. Abigail Zuger, a practicing internist, often writes about medical 
issues for The New York Times.
</i> 
