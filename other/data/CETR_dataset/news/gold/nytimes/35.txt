<img src=""http://graphics.nytimes.com/images/a.gif"" align=left alt=A>rmchair mountaineering is a
time-honored tradition among
those seeking vicarious thrills
from the comfort of their own homes.
In the last few years, however, it has
been supplemented by a new occupation: desktop mountaineering, the
practice of following a climbing expedition on the Web as its members
try to reach the summit of one of the
world's greatest peaks. And May is
the start of the peak climbing season.
<p><p>
<NYT_INLINEIMAGE version=""1.0"" type=""main"">
<table align=right border=0 cellpadding=2 cellspacing=2 width=260>
<tr><td rowspan=2 width=10><br></td><td align=right>
</font></td></tr><p>
<tr><td align=left><font size=-1>
<HR SIZE=1>
<font size=-1> <b> Overview </b> 
<P> &#149;&nbsp;<a href=""06libe.html"">Mountain Climbing as a Spectator Sport</a>
<p>
<font size=-1> <b> This Week's Articles </b> 
<P> &#149;&nbsp;<a href=""06libe-zone.html"">The Mountain Zone</a>
<P> &#149;&nbsp;<a href=""06libe-lost.html"">Nova Online: Lost on Everest -- the Search  for Mallory and Irvine</a>
<P> &#149;&nbsp;<a href=""06libe-expe.html"">Everest Internet Experiment 1999</a>
<P> &#149;&nbsp;<a href=""06libe-ever.html"">Everest News</a> 
<P> &#149;&nbsp;<a href=""06libe-bbcc.html"">BBC Online: Everest Online
</a>
<HR SIZE=1>
</td></tr></table>
<!--@ -->
</NYT_INLINEIMAGE> Mount Everest, for instance, has
seen a huge increase in foot traffic in
the 1990's. But record numbers of
people have also paid a virtual visit
to Everest, the world's highest
mountain, in the last few years. Some
20 expeditions (and at least 150
climbers) are trying Everest this
year -- some have already reached
the summit -- and more than a dozen
have their own Web sites, with journal-style dispatches, photos, route
maps, audio and video clips, and
thumbnail Everest histories. Far
from being a remote adventure,
high-altitude mountaineering is now
a spectator sport.
<p> It wasn't always this way, of
course.
<p>Sir Edmund Hillary, of New
Zealand, and Tenzing Norgay, a
Sherpa guide, were the first climbers
to reach Everest's summit, on May
29, 1953.
<p>The news of the feat, by a
British expedition, reached the public on June 2 (the day of Queen Elizabeth's inauguration, by happy coincidence) -- and the news moved that
fast only because The London Times
had sent a correspondent, James
Morris, to Nepal.
<p>
<p> Times have changed: James Morris now writes as Jan Morris, and
when Everest is climbed today, the
public knows about it before the
climbers are off the mountain.
Climbers radio from the top down to
base camp, where team members
call home on satellite phones. Then
expedition Web sites quickly post updates that read like breaking news,
even if there is a little lag time.
<p> Because of the Web, desktop
mountaineers now get a taste of the
unfolding anxiety and uncertainty
faced by real mountaineers. Take
Carolyn Hahn, a writer living in New
York, who, along with other family
members, uses the Web to follow the
feats of her brother Dave, a professional climber who has reached the
Everest summit and sent witty Web
dispatches back from the mountain
in each of the last two years.
<p> ""I only know what's on the Web,""
she said. ""When two climbers died on
his route last year, we got an e-mail
saying he was O.K.
<p>But otherwise, I
just read the same updates as everyone else.""
<p> The Web would be well suited to
accounts of climbing feats even without this immediacy. Good sites include abundant photographs (most
can be enlarged with a click) and
other visual effects, combining them
with text in creative ways to give
readers more of that elusive you-are-there feeling than can be had while
curled up in an armchair with a book
or magazine. With so many sites,
though, it may not be clear where to
begin. Here are a few ways to make
the mountains come to you.
<P>
<p><NYT_LINKS_OFFSITE version=""1.0"" type=""main""><a name=1></a><hr size=1><b>Related Sites</b><br> <font size=-1>These sites are not part of The New York Times on the Web, and The Times has no control over their content or availability.</font><p> 
<P>
<I>""Into Thin Air,"" a book by Jon Krakauer  about the 1996 climbing disaster on Mount Everest, spent a long time atop the best-seller list, and the 
Imax film ""Everest"" enjoyed a long run in theaters. If you want to find 
out more about the disaster, in which five people died, here are a few 
Web sites related to it and to other climbs in 1996: 
</I>
<P>
A LOOK BACK AT EVEREST<BR>
<a href=""http://www.outsidemag.com:80/places/features/everest.html"">www.outsidemag.com:80/places/features/everest.html</a>
<P>
Updates from 
Scott Fischer's expedition, Jon Krakauer's original magazine article 
and more, from Outside Online. 
<p> 
THE EVEREST ASSAULT STORY
<BR>
<a href=""http://peacock.nbc.com/everest/main.html#home"">peacock.nbc.com/everest/main.html#home</a>
<P> Sandy Hill Pittman was criticized for her role in the 1996 disaster, but her site is well done.
<p> 
EVEREST SUMMIT JOURNAL COMPENDIUM
<BR>
<a href=""http://members.xoom.com/Everest96/links.htm"">members.xoom.com/Everest96/links.htm</a>
<P> A thorough listing of articles 
and interviews related to the 1996 climbing year.   
<p>
<p> 
RISK ONLINE
<BR><a href=""http://www.risk.ru"">www.risk.ru</a>
<P> This English version of a Russian site contains the best single-expedition Web site I've ever seen, in the Mountaineering section 
under the heading Togliatti K2 1996. It is the story of a Russian team 
that climbed K2 (the world's second-highest mountain) in the summer 
of 1996, told from the perspectives of two climbers. Lavishly illustrated 
with stunning photos and video. 
<p>
<i> Peter Dizikes is a freelance writer 
and recreational climber who lives in 
New York. </i> 
</ul>
<p>