<img src=""/images/o.gif"" alt=O align=left border=0 height=34 width=34>f all sports, golf has a reputation for being among the most 
averse to change. While tennis and basketball players compete 
with fluorescent clothes  and shaved 
heads, many professional golfers 
wear the long pants and polo shirts  
that have been worn since before the 
turn of the century. As if to emphasize the conservatism,  Payne Stewart won  the United States Open this 
month wearing navy blue plus fours.   
<p>
<p>
<NYT_INLINEIMAGE version=""1.0"" type=""main"">
<table align=right border=0 cellpadding=2 cellspacing=2 width=260>
<tr><td rowspan=2 width=10><br></td><td align=right>
<img src=""24libe.1.jpg""><br>
<font size=-2>
Allsport
</font></td></tr><p>
<tr><td align=left><font size=-1>
<font size=-1>Overview
<P> &#149;&nbsp;<a href=""24libe.html"">Virtual Golf, Without the Caddies or Carts</a>
<p>
<font size=-1>This Week's Site Reviews
<P> &#149;&nbsp;<a href=""24libe-glob.html"">globalgolfguide.com</a>
<P> &#149;&nbsp;<a href=""24libe-lpga.html"">lpga.com</a>
<P> &#149;&nbsp;<a href=""24libe-pgaa.html"">pgatour.com</a>
<P> &#149;&nbsp;<a href=""24libe-worl.html"">worldgolf.com</a>
<HR SIZE=1>
</td></tr></table>
<!--@ -->
</NYT_INLINEIMAGE>
<p>  But golf has not been so reluctant 
to throw tradition aside and expand 
onto the Web. The sport is flourishing 
there, with hundreds of  sites that focus on the best courses, firsthand accounts of  holes in one and what to 
look for in equipment.   
<p> 
<p>  Among the sites that generate particular interest are those devoted to 
topics  like the  Open,  the  Masters, the 
Professional Golfers' Association 
Championship and the British Open. 
<p>On-line magazines, fans sites and 
sports television sites chronicled 
each round; the P.G.A. site provided 
updates every few minutes.   
<p> 
<p>  Reading about the competitors 
may inspire some weekend players.   
<p>
Dozens of sites offer tips on driving 
(go for the middle of the green), 
putting (keep in balance) and practice (do it as much as possible). 
<p>  But people who are looking for information about equipment and resorts may have a harder time finding 
it on the Web. There are  ample articles about golf courses and new 
equipment, but they are often so positive that they might as well have 
been written by a publicity agent.   
<p>
<p> As George Plimpton once observed about sports writing, the 
smaller the ball, the better the prose. 
Though little on the Web approaches 
the quality of the golf writing found 
elsewhere, there is plenty on line to 
keep the enthusiast glued to the computer screen -- at least until the 
game starts on television.
<HR SIZE=1>
<p>
<I>Here are more sources of golf-related information and entertainment.
</I>
<P>
UNITED STATES GOLF ASSOCIATION: <a href=""http://www.usga.com"">www.usga.com</a>
<BR>
Features one of the 
most complete  rule books on the  Web. 
<P>
BAD GOLF MONTHLY: <a href=""http://www.badgolfmonthly.com"">www.badgolfmonthly.com</a>
<BR>
A comforting destination 
for golfers with a big handicap. 
<P>
GOLF ONLINE: <a href=""http://www.golfonline.com"">www.golfonline.com</a>
<BR>
Offers a comprehensive selection of 
playing tips,  tournament reports and travel articles. 
<P>
TIGER WOODS OFFICIAL GOLF WEBSITE: <a href=""http://www.tigerwoods.com"">www.tigerwoods.com</a>
<BR>
A fan site 
for those who believe that Mr. Woods should win every tournament.   
<P>
MR. GOLF ETIQUETTE: <a href=""http://www.mrgolf.com"">www.mrgolf.com</a>
<BR>
Includes a primer on how to 
behave on the course. 
<P>
CADDYSHACK AND HAPPY GILMORE ARCHIVE: <a href=""http://www.bushwood.net"">www.bushwood.net</a>
<BR>
Features pictures, trivia and sound clips from ""Caddyshack,"" perhaps one 
of the most popular golfing films ever. 
<P>
AMATEUR GOLF ADVISOR: <a href=""http://www.tagagolf.com"">www.tagagolf.com</a>
<BR>
Gives tips on a number of 
topics, including driving and what to look for when buying clubs.  
<P>
TRIVIA GOLF COUNTRY CLUB: <a href=""http://www.triviagolf.com"">www.triviagolf.com</a>
<BR>
Tests of knowledge. 
<p>
<p>
<p> 
</ul>
<p> 
