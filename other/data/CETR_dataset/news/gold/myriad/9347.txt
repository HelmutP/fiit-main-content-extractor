"Nearly 1 billion people hungry worldwide, UN human rights expert says</h2><div id=PhotoHolder style=width:180px><img src=http://www.un.org/News/dh/photos/2008/27-10-2008schutter.jpg Title  =  'Special Rapporteur on Right to Food  Olivier de Schutter'  border=1><p class=phtocaption>Special Rapporteur on Right to Food  Olivier de Schutter</p></div><span class=fullstory>27 October 2008 &#150 </span><span class=fullstory>Propelled by this year?s global food crisis, nearly one billion people worldwide are now hungry, an independent United Nations expert said today, urging the issue to be viewed through the lens of human rights.<P>

Prices have dropped around the world, but ?the crisis is still with us,? cautioned Olivier De Schutter, the Special Rapporteur on the right to food, noting that the number of hungry has grown significantly as a result.<P>

Numerous international responses ? including Secretary-General Ban Ki-moon?s convening of a high-level task force ? have centred around the need to boost food production to meet rising demand and lower prices, he told reporters in New York.<P>

?The human rights dimension has been all too often absent from these reactions,? Mr. De Schutter, who addressed the General Assembly today, said.<P>

The ?real problem of hunger? is not linked to inadequate food supplies, but rather that many people lack the purchasing power to buy available food, he pointed out.<P>

?If you double the number of supermarkets in New York, those who today are hungry will still be hungry if they don?t see their incomes increase [and therefore] if their purchasing power remains too low for them to afford the food which is on the market.?<P>

Hunger is a political problem, he stressed, with poor governance leading to insufficient attention being paid to swathes of the population traditionally discriminated against.<P>

It is crucial to empower smallhold farmers, comprising half of the world?s hungry, as well as landless labourers, pastoralists and others, the Rapporteur, who took up the position this May, said.<P>

?We must avoid at all costs that under the pretext of producing more food, we increase the marginalization of smallhold farmers and increase the dualization of the farming system for the benefit only of the very few large agricultural producers,? he stressed.