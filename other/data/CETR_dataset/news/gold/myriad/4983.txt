"
					
					The top adviser at Obama&#039;s&nbsp;side</h1>
					<!-- /kicker & headline -->
					
											<!-- subhead -->
			            
						<!-- /subhead -->
			            
						<!-- byline -->
			            
						<div class=""byline"">
							<div class=""dots""><img src=""http://img.iht.com/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
							<div id=""author"" style=""float: left;""><strong><a href=""http://www.iht.com/cgi-bin/search.cgi?query=By Jeff Zeleny&amp;sort=publicationdate&amp;submit=Search"">By Jeff Zeleny</a></strong> </div>
							<div id=""pubDate"" style=""float: right;"">Published: October 27, 2008</div>
							<div class=""dots""><img src=""http://img.iht.com/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
						</div>
			            
						<!-- /byline -->
										
											<!-- body text -->
						<script language=""JavaScript"" type=""text/javascript"">
							document.writeln('<div id=""bodyText"" style=""font-size: ' + currentTextSize + 'px; line-height: ' + currentLineHeight + 'px;"">');
						</script>
			                
<!-- article tools - narrow (used with span photos) -->
<div class=""ISI_IGNORE"" id=""at_narrow_wrapper"">
	<div><img src=""http://img.iht.com/images/articletools/at_narrow_top.gif"" width=""123"" height=""2"" alt="""" /></div>
	<div id=""at_narrow_inner"">
		<!-- email article -->
		<div class=""at_link""><a href=""javascript:pop_me_up('/bin/email.php?id=17272287&amp;code=0&amp;type=Article','email','height=600,width=600,scrollbars=yes,resize=yes');""><img src=""http://img.iht.com/images/articletools/icon_at_email.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; E-Mail Article</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /email article -->
		<!-- audionews -->
		<div id=""at_listen"">
			<div class=""at_link"">
				<form name=""listenForm"" id=""listenForm"" method=""post"" action=""/bin/listen.php"" style=""display:inline;"" target=""listenPopup""><input type=""hidden"" name=""id"" value=""17272287"" /><input type=""Hidden"" name=""headline"" value=""The top adviser at Obama&#039;s side"" /><input type=""Hidden"" name=""section"" value=""america"" /> <a href=""#"" onclick=""form2pop('listenForm','300','240'); return false;"" onmouseover=""window.status='Listen to Article'; return true;"" onmouseout=""window.status=''; return true;""><img src=""http://img.iht.com/images/articletools/icon_at_audionews.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Listen to Article</a></form>
			</div>
			<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		</div>
		<!-- /audionews -->
		<!-- printer friendly -->
		<div class=""at_link""><a href=""javascript:printFriendly();""><img src=""http://img.iht.com/images/articletools/icon_at_printerfriendly.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Printer-Friendly</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /printer friendly -->
		<!-- 3 column -->
		<div class=""at_link""><a href=""javascript:pop_me_up('/bin/3-col.php?id=17272287','3col','width=980,height=694');""><img src=""http://img.iht.com/images/articletools/icon_at_3col.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; 3-Column Format</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /3 column -->
		<!-- translate -->
		<div class=""at_link""><a href=""#"" onClick=""return dropdownmenu(this, event, translateMenu, '150px')"" onMouseout=""delayhidemenu()""><img src=""http://img.iht.com/images/articletools/icon_at_translate.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Translate</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /translate -->
		<!-- share article -->
		<div class=""at_link""><a href=""#"" onClick=""return dropdownmenu(this, event, shareMenu, '150px')"" onMouseout=""delayhidemenu()""><img src=""http://img.iht.com/images/articletools/icon_at_share.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Share Article</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /share article -->
		<!-- text size -->
		<div class=""at_link"">
			<div style=""float: right; padding-right: 4px;""><a href=""javascript:textSize('down')""><img src=""http://img.iht.com/images/articletools/btn_textsize_sm.gif"" width=""15"" height=""15"" alt="""" border=""0"" /></a>&nbsp;&nbsp;<a href=""javascript:textSize('up')""><img src=""http://img.iht.com/images/articletools/btn_textsize_lg.gif"" width=""15"" height=""15"" alt="""" border=""0"" /></a></div>
			<div><img src=""http://img.iht.com/images/articletools/icon_at_textsize.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Text Size</div>
		</div>
<!-- /text size -->
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<div align=""center"" style=""padding: 5px;"">
			<!-- 88x31 button -->
<!-- No ad for america_88x31_article -->
			<!-- /88x31 button -->
		</div>
	</div>
	<div><img src=""http://img.iht.com/images/articletools/at_narrow_bot.gif"" width=""123"" height=""3"" alt="""" /></div>
</div>
<!-- /article tools - narrow (used with span photos) -->


							
							<!-- copy -->
			                <p><strong><a id=""articleLocation"" title=""Click to view map"" href=""#"">CHICAGO</a>:</strong> The senator calls before&nbsp;bedtime.</p> 
<p>The cellphone in David Axelrod&#039;s shirt pocket comes to life, sometimes before midnight, sometimes after. If the ring tone is ""Signed, Sealed, Delivered I&#039;m Yours"" by Stevie Wonder, he steps away from the dinner table or the barstool. It almost certainly means that Senator Barack Obama is on the line, ready for another turn of a rolling conversation the two men have been having almost every day for what has been a remarkable two&nbsp;years.</p> 
<p>""When the phone rings at 11, I have a pretty good sense of who it&#039;s going to be,"" Axelrod said. ""He does a lot of thinking and working late at&nbsp;night.""</p> 
<p>As Obama, the Democratic presidential nominee, heads into the final days of his race for the White House, an ever-widening sphere of aides surrounds him. But almost none is as responsible for his current station as Axelrod, whose title of chief strategist only hints at the extensive role he has played in the senator&#039;s evolution: friend, adviser and confidant, always at the elbow of this&nbsp;candidate.</p> 
<p>In many ways, Axelrod is a classic example of the Washington political consultant (even though he lives in Chicago and says he has no intention of moving to the capital if Obama wins). He has been making advertisements and offering advice for candidates for mayor, senator and president for a generation, since quitting his job as a newspaper reporter in 1984. He has a particular specialty in helping black candidates appeal to white&nbsp;electorates.</p> 


<!-- sidebar -->
<div class=""ISI_IGNORE"" id=""sidebar"">
	<!-- Custompage -->
	<div class=""sidebar_content_box"">
		<h3>More Coverage</h3>
		<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
		<!-- Related Custompage -->
		<div class=""sidebar_promo_content"">
			<div class=""sidebar_promo_thumb""><a href=""/indexes/special/uselections2008/index.php""><img src=""/images/2008/06/18/13electhumb.jpg"" width=""75"" height=""75"" alt="""" border=""0"" /></a></div>
			<div class=""sidebar_promo_text""><a href=""/indexes/special/uselections2008/index.php"">Elections 2008</a></div>
		</div>
		<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
	</div>
		<!-- /Related Custompage -->
	<!-- today in links -->
<div class=""sidebar_content_box"">
    <h3>Today in Americas</h3>
    <div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
    <div class=""sidebar_item"" style=""margin: 4px 0px; overflow: hidden"">
	<div class=""sidebar_item_link""><a href=""/articles/2008/10/28/america/homeless.php"">Experts in economic malfunction</a></div>
</div>
<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
<div class=""sidebar_item"" style=""margin: 4px 0px; overflow: hidden"">
	<div class=""sidebar_item_link""><a href=""/articles/2008/10/28/america/29stevens.php"">McCain says Alaska senator should resign</a></div>
</div>
<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
<div class=""sidebar_item"" style=""margin: 4px 0px; overflow: hidden"">
	<div class=""sidebar_item_link""><a href=""/articles/2008/10/27/america/wife.php"">New to campaigning, but Michelle Obama is no longer a novice</a></div>
</div>
<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>

</div>

	<!-- /today in links -->
	<!-- 170 x 60 ad -->
	<div align=""center"">
		<!-- No ad for america_170x60_article -->

	</div>
	<!-- /170 x 60 ad --></div>
<!-- /sidebar -->

<p>But Axelrod, in this client-consultant relationship, appears to be something different, with a personal investment in Obama&#039;s success that is obvious in the distress marked on his face whenever the candidate comes under&nbsp;attack.</p> 
<p>Every politician has a guardian angel, and every presidential hopeful has a right-hand dispenser of wisdom. Yet in the trio of top strategists around Obama, including Robert Gibbs, a senior communications adviser, and David Plouffe, the campaign manager, it is Axelrod who has been at Obama&#039;s side the longest and has the most interwoven relationship with&nbsp;him.</p> 
<p>""Although he is as tough as they come, he&#039;s actually not a mercenary,"" Obama said in an interview. ""He actually believes in what we&#039;re doing, which actually makes him a bad consultant when he doesn&#039;t believe in the candidate. And he&#039;s a great consultant when he&nbsp;believes.""</p> 
<p>Oh, Axelrod believes. But since the moment he met Obama in 1992? That would be revising&nbsp;history.</p> 
<p>Their relationship began when Bettylu Saltzman, a Democratic activist in Chicago, suggested that the two men should become acquainted. She was a volunteer in Illinois for Bill Clinton&#039;s first presidential campaign, and Obama was leading a voter-registration&nbsp;drive.</p> 
<p>Impressed, Saltzman predicted to her friends that Obama would be the first black president. So she thought he should know the city&#039;s best-known political strategist. (Told about the story recently, Axelrod conceded remembering few specifics about their meeting and said, ""We went out to lunch,&nbsp;maybe?"")</p> 
<p>""I certainly put the bug in his ear,"" said Saltzman, proudly recalling those days 16 years ago when she urged Axelrod to take time to meet the young lawyer. ""They are sort of a yin-and-yang personality. David can be so much more volatile than&nbsp;Barack.""</p> 
<p>She added, ""He&#039;s calmed David down a&nbsp;lot.""</p> 
<p>It took several years after that first meeting for Axelrod to agree to take Obama on as a client. But since then they have become linked in a way that is more than just&nbsp;professional.</p> 
<p>""He is always at his best when we&#039;re at our worst; that&#039;s sustained us through some difficult times,"" Axelrod said. ""This is unusual for a national campaign, but everybody likes each&nbsp;other.""</p> 
<p>When campaigns struggle, candidates often substitute their top players by showing them the door or by layering them with other strategists. But neither is the case with Obama. The team that surrounded him two years ago as he made his decision to run for president is the same group with him at the finish&nbsp;line.</p> 
<p>If Clinton had James Carville and President George W. Bush had Karl Rove, it could well be said that Obama has Axelrod. For advice on how to navigate the politics of race. For suggestions on how to calibrate his message. And for comedic&nbsp;relief.</p> 
<p>As Obama was practicing his acceptance speech at the Democratic convention, at the very hour that a Denver stadium was filling up with supporters, a knock sounded on the door of his hotel suite. He was at an emotional high point of the address, the moment he declared, ""Enough!"" But he was near the door, so he answered&nbsp;it."
"4983","By Jeff Zeleny</a></strong> </div>
							<div id=""pubDate"" style=""float: right;"">Published: October 27, 2008</div>
							<div class=""dots""><img src=""http://img.iht.com/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
						</div>
			            
						<!-- /byline -->
										
											<!-- body text -->
						<script language=""JavaScript"" type=""text/javascript"">
							document.writeln('<div id=""bodyText"" style=""font-size: ' + currentTextSize + 'px; line-height: ' + currentLineHeight + 'px;"">');
						</script>
			                
<!-- article tools - narrow (used with span photos) -->
<div class=""ISI_IGNORE"" id=""at_narrow_wrapper"">
	<div><img src=""http://img.iht.com/images/articletools/at_narrow_top.gif"" width=""123"" height=""2"" alt="""" /></div>
	<div id=""at_narrow_inner"">
		<!-- email article -->
		<div class=""at_link""><a href=""javascript:pop_me_up('/bin/email.php?id=17272287&amp;code=0&amp;type=Article','email','height=600,width=600,scrollbars=yes,resize=yes');""><img src=""http://img.iht.com/images/articletools/icon_at_email.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; E-Mail Article</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /email article -->
		<!-- audionews -->
		<div id=""at_listen"">
			<div class=""at_link"">
				<form name=""listenForm"" id=""listenForm"" method=""post"" action=""/bin/listen.php"" style=""display:inline;"" target=""listenPopup""><input type=""hidden"" name=""id"" value=""17272287"" /><input type=""Hidden"" name=""headline"" value=""The top adviser at Obama&#039;s side"" /><input type=""Hidden"" name=""section"" value=""america"" /> <a href=""#"" onclick=""form2pop('listenForm','300','240'); return false;"" onmouseover=""window.status='Listen to Article'; return true;"" onmouseout=""window.status=''; return true;""><img src=""http://img.iht.com/images/articletools/icon_at_audionews.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Listen to Article</a></form>
			</div>
			<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		</div>
		<!-- /audionews -->
		<!-- printer friendly -->
		<div class=""at_link""><a href=""javascript:printFriendly();""><img src=""http://img.iht.com/images/articletools/icon_at_printerfriendly.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Printer-Friendly</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /printer friendly -->
		<!-- 3 column -->
		<div class=""at_link""><a href=""javascript:pop_me_up('/bin/3-col.php?id=17272287','3col','width=980,height=694');""><img src=""http://img.iht.com/images/articletools/icon_at_3col.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; 3-Column Format</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /3 column -->
		<!-- translate -->
		<div class=""at_link""><a href=""#"" onClick=""return dropdownmenu(this, event, translateMenu, '150px')"" onMouseout=""delayhidemenu()""><img src=""http://img.iht.com/images/articletools/icon_at_translate.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Translate</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /translate -->
		<!-- share article -->
		<div class=""at_link""><a href=""#"" onClick=""return dropdownmenu(this, event, shareMenu, '150px')"" onMouseout=""delayhidemenu()""><img src=""http://img.iht.com/images/articletools/icon_at_share.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Share Article</a></div>
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<!-- /share article -->
		<!-- text size -->
		<div class=""at_link"">
			<div style=""float: right; padding-right: 4px;""><a href=""javascript:textSize('down')""><img src=""http://img.iht.com/images/articletools/btn_textsize_sm.gif"" width=""15"" height=""15"" alt="""" border=""0"" /></a>&nbsp;&nbsp;<a href=""javascript:textSize('up')""><img src=""http://img.iht.com/images/articletools/btn_textsize_lg.gif"" width=""15"" height=""15"" alt="""" border=""0"" /></a></div>
			<div><img src=""http://img.iht.com/images/articletools/icon_at_textsize.gif"" width=""12"" height=""12"" alt="""" border=""0"" />&nbsp; Text Size</div>
		</div>
<!-- /text size -->
		<div><img src=""http://img.iht.com/images/articletools/dots_at_narrow.gif"" width=""108"" height=""1"" alt="""" /></div>
		<div align=""center"" style=""padding: 5px;"">
			<!-- 88x31 button -->
<!-- No ad for america_88x31_article -->
			<!-- /88x31 button -->
		</div>
	</div>
	<div><img src=""http://img.iht.com/images/articletools/at_narrow_bot.gif"" width=""123"" height=""3"" alt="""" /></div>
</div>
<!-- /article tools - narrow (used with span photos) -->


							
							<!-- copy -->
			                <p><strong><a id=""articleLocation"" title=""Click to view map"" href=""#"">CHICAGO</a>:</strong> The senator calls before&nbsp;bedtime.</p> 
<p>The cellphone in David Axelrod&#039;s shirt pocket comes to life, sometimes before midnight, sometimes after. If the ring tone is ""Signed, Sealed, Delivered I&#039;m Yours"" by Stevie Wonder, he steps away from the dinner table or the barstool. It almost certainly means that Senator Barack Obama is on the line, ready for another turn of a rolling conversation the two men have been having almost every day for what has been a remarkable two&nbsp;years.</p> 
<p>""When the phone rings at 11, I have a pretty good sense of who it&#039;s going to be,"" Axelrod said. ""He does a lot of thinking and working late at&nbsp;night.""</p> 
<p>As Obama, the Democratic presidential nominee, heads into the final days of his race for the White House, an ever-widening sphere of aides surrounds him. But almost none is as responsible for his current station as Axelrod, whose title of chief strategist only hints at the extensive role he has played in the senator&#039;s evolution: friend, adviser and confidant, always at the elbow of this&nbsp;candidate.</p> 
<p>In many ways, Axelrod is a classic example of the Washington political consultant (even though he lives in Chicago and says he has no intention of moving to the capital if Obama wins). He has been making advertisements and offering advice for candidates for mayor, senator and president for a generation, since quitting his job as a newspaper reporter in 1984. He has a particular specialty in helping black candidates appeal to white&nbsp;electorates.</p> 


<!-- sidebar -->
<div class=""ISI_IGNORE"" id=""sidebar"">
	<!-- Custompage -->
	<div class=""sidebar_content_box"">
		<h3>More Coverage</h3>
		<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
		<!-- Related Custompage -->
		<div class=""sidebar_promo_content"">
			<div class=""sidebar_promo_thumb""><a href=""/indexes/special/uselections2008/index.php""><img src=""/images/2008/06/18/13electhumb.jpg"" width=""75"" height=""75"" alt="""" border=""0"" /></a></div>
			<div class=""sidebar_promo_text""><a href=""/indexes/special/uselections2008/index.php"">Elections 2008</a></div>
		</div>
		<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
	</div>
		<!-- /Related Custompage -->
	<!-- today in links -->
<div class=""sidebar_content_box"">
    <h3>Today in Americas</h3>
    <div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
    <div class=""sidebar_item"" style=""margin: 4px 0px; overflow: hidden"">
	<div class=""sidebar_item_link""><a href=""/articles/2008/10/28/america/homeless.php"">Experts in economic malfunction</a></div>
</div>
<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
<div class=""sidebar_item"" style=""margin: 4px 0px; overflow: hidden"">
	<div class=""sidebar_item_link""><a href=""/articles/2008/10/28/america/29stevens.php"">McCain says Alaska senator should resign</a></div>
</div>
<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>
<div class=""sidebar_item"" style=""margin: 4px 0px; overflow: hidden"">
	<div class=""sidebar_item_link""><a href=""/articles/2008/10/27/america/wife.php"">New to campaigning, but Michelle Obama is no longer a novice</a></div>
</div>
<div class=""dots""><img src=""/images/dot_h.gif"" alt="""" width=""3"" height=""1"" /></div>

</div>

	<!-- /today in links -->
	<!-- 170 x 60 ad -->
	<div align=""center"">
		<!-- No ad for america_170x60_article -->

	</div>
	<!-- /170 x 60 ad --></div>
<!-- /sidebar -->

<p>But Axelrod, in this client-consultant relationship, appears to be something different, with a personal investment in Obama&#039;s success that is obvious in the distress marked on his face whenever the candidate comes under&nbsp;attack.</p> 
<p>Every politician has a guardian angel, and every presidential hopeful has a right-hand dispenser of wisdom. Yet in the trio of top strategists around Obama, including Robert Gibbs, a senior communications adviser, and David Plouffe, the campaign manager, it is Axelrod who has been at Obama&#039;s side the longest and has the most interwoven relationship with&nbsp;him.</p> 
<p>""Although he is as tough as they come, he&#039;s actually not a mercenary,"" Obama said in an interview. ""He actually believes in what we&#039;re doing, which actually makes him a bad consultant when he doesn&#039;t believe in the candidate. And he&#039;s a great consultant when he&nbsp;believes.""</p> 
<p>Oh, Axelrod believes. But since the moment he met Obama in 1992? That would be revising&nbsp;history.</p> 
<p>Their relationship began when Bettylu Saltzman, a Democratic activist in Chicago, suggested that the two men should become acquainted. She was a volunteer in Illinois for Bill Clinton&#039;s first presidential campaign, and Obama was leading a voter-registration&nbsp;drive.</p> 
<p>Impressed, Saltzman predicted to her friends that Obama would be the first black president. So she thought he should know the city&#039;s best-known political strategist. (Told about the story recently, Axelrod conceded remembering few specifics about their meeting and said, ""We went out to lunch,&nbsp;maybe?"")</p> 
<p>""I certainly put the bug in his ear,"" said Saltzman, proudly recalling those days 16 years ago when she urged Axelrod to take time to meet the young lawyer. ""They are sort of a yin-and-yang personality. David can be so much more volatile than&nbsp;Barack.""</p> 
<p>She added, ""He&#039;s calmed David down a&nbsp;lot.""</p> 
<p>It took several years after that first meeting for Axelrod to agree to take Obama on as a client. But since then they have become linked in a way that is more than just&nbsp;professional.</p> 
<p>""He is always at his best when we&#039;re at our worst; that&#039;s sustained us through some difficult times,"" Axelrod said. ""This is unusual for a national campaign, but everybody likes each&nbsp;other.""</p> 
<p>When campaigns struggle, candidates often substitute their top players by showing them the door or by layering them with other strategists. But neither is the case with Obama. The team that surrounded him two years ago as he made his decision to run for president is the same group with him at the finish&nbsp;line.</p> 
<p>If Clinton had James Carville and President George W. Bush had Karl Rove, it could well be said that Obama has Axelrod. For advice on how to navigate the politics of race. For suggestions on how to calibrate his message. And for comedic&nbsp;relief.</p> 
<p>As Obama was practicing his acceptance speech at the Democratic convention, at the very hour that a Denver stadium was filling up with supporters, a knock sounded on the door of his hotel suite. He was at an emotional high point of the address, the moment he declared, ""Enough!"" But he was near the door, so he answered&nbsp;it.