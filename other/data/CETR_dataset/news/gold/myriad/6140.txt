"Flaherty poised to be ?Deficit Jim?</h2><p class=""subheadline"">Only reason to renew Finance contract is for comforting optics</p><p class=""author""><strong>Don Martin,
          National Post 
        </strong><span>
            Published: Tuesday, October 28, 2008</span></p><div class=""story-tools""><div class=""newsblock triline""><h4>Story Tools</h4><ul class=""category""><li><p><a href=""#"" title=""Decrease font size..."" class=""icon-font-small"" onclick=""javascript:return font_size(-1)""><span>-</span></a><a href=""#"" title=""Increase font size..."" class=""icon-font-big"" onclick=""javascript:return font_size(1)""><span>+</span></a> Change font size
            </p></li><li><p><a class=""icon-print"" href=""/print/"" onclick=""return print_click()"" target=""_blank"">Print this story</a><script type=""text/javascript"">
                function print_click() {
                u=location.href;
                u=delineate(u);
                window.open('/story-printer.html?id='+encodeURIComponent(u),'sharer','toolbar=1,status=1,location=0,menubar=1,scrollbars=1,width=650,height=600');
                return false;
                }
                function delineate(str)
                {
                theleft = str.indexOf(""="") + 1;
                theright = str.lastIndexOf(""&"");
                if ( theright == -1 ) {
                theright = str.length;
                }
                return(str.substring(theleft, theright));
                }
              </script></p></li><li><p><a class=""icon-email"" href=""#email"" onclick=""javascript:return story_email(true)"">E-Mail this story</a></p></li></ul></div><div class=""newsblock triline""><script type=""text/javascript"">
					str_url = encodeURIComponent(location.href);
					str_title = encodeURIComponent(document.title);
					// Facebook
					function fbs_click(){
						var w = 600, h = 400;
						window.open('http://www.facebook.com/sharer.php?u=' + str_url + '&t=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// Digg
					function digg_click(){
						var w = 960, h = screen.height - 200;
						window.open('http://digg.com/submit?phase=2&url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// StumbleUpon
					function stumble_click(){
						var w = 700, h = 400;
						window.open('http://www.stumbleupon.com/submit?url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// AddThis
					function addthis_click(){
						var w = 700, h = 750;
						window.open('http://www.addthis.com/bookmark.php?url=' + str_url + '&title=' + str_title + '&pub=nationalpost.com', 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					/* Delicious
					function dels_click(){
						var w = 700, h = 400;
						window.open('http://del.icio.us/post?url=' + str_url + '&jump=close&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}//*/
				</script><h4>Share This Story</h4><ul class=""category""><li><p><a href=""http://www.facebook.com/"" target=""_blank"" class=""icon-facebook"" onclick=""return fbs_click()"">Facebook</a></p></li><li><p><a href=""http://www.digg.com/"" target=""_blank"" class=""icon-digg"" onclick=""return digg_click()"">Digg</a></p></li><li><p><a href=""http://www.stumbleupon.com/"" target=""_blank"" class=""icon-stumbleupon"" onclick=""return stumble_click()"">Stumble Upon</a></p></li><li><p><a href=""http://www.addthis.com/"" target=""_blank"" class=""icon-addthis"" onclick=""return addthis_click()"">More</a></p></li></ul></div><div class=""sponsor""><p>Story tools presented by</p><div class=""ad-microbar""><script type=""text/javascript"">
								try{
									var arr_da = document.getElementById('ad-leaderboard').getElementsByTagName('script');
									for(var i in arr_da){
										if(typeof(arr_da[i].src) != 'undefined' && (/http:\/\/ad\.ca\.doubleclick\.net/.test(arr_da[i].src))){
											var str_da_src = arr_da[i].src;
											str_da_src = str_da_src.replace(/loc=\w+;/gi, 'loc=microbar;');
											str_da_src = str_da_src.replace(/sz=\d+x\d+;/gi, 'sz=88x31;');
											str_da_src = str_da_src.replace(/ptile=\d+;/gi, 'ptile=4;');
											document.write('<script type=""text/javascript"" src=""' + str_da_src + '""><\/script>');
											break;
										}
									}
								}catch(e){}
							</script></div></div></div><div class=""story-content""><p>OTTAWA -When rookie MPs are ushered into the House of Commons for the first time, tradition dictates they put up a mild show of resistance, as if they are being dragged to their seat.</p><p>For one Cabinet minister expected to be sworn in on Thursday, being reluctantly hauled before the Governor-General shouldn?t be an act.</p><p>There is no safer bet for ministerial reappointment than Jim Flaherty. There will be no lousier assignment than to be saddled with finance minister responsibilities that no longer come with a Santa clause for big-ticket spending.</p><p>Before this economic downturn hits bottom, it?s almost a given Mr. Flaherty will wear the Deficit Jim label as the Conservative who put the red back into Canada?s budget books for the first time since they were balanced in the mid-1990s.</p><p>That would be an ugly legacy for the charismatic Mr. Flaherty, who served as Ontario?s finance minister in 2001 when federal Liberals were racking up the largest surpluses in fiscal history.</p><p>It?s been almost two years since his most notorious flip-flop ? the Halloween announcement the government would break its pre-election promise and start taxing income trusts ? and now he?s careening toward another big-gulp policy reversal. Mr. Flaherty, it seems certain, will have to renege on his hell-or-high-water vow that the Conservatives would not lurch into deficit.</p><p>He?s now waffling on that campaign promise in the face of $10-billion deficit projections by some economists, a red tidal wave that could not be rebuffed with spending cuts or tax increases.</p><p>To cut a nickel off every dollar of spending by the federal government, no matter how braced the voters are for tough times, is an impossible mission and poor fit with the times.</p><p>That?s why, beyond serving as the reassuring face of continuity during a financial crisis, Mr. Flaherty?s record doesn?t necessarily crown</p><p>him as the best steady hand and confidence booster for troubled times.</p><p>There have been several performance hiccups and policy inconsistencies. His office broke the government?s own contracting rules by hiring a speechwriter without putting the job to tender. There was Mr. Flaherty?s infamous crack about overtaxed Ontario being a lousy place to invest, which, with the benefit of the current meltdown as hindsight, was prophetic.</p><p>His budgets have featured steep spending boosts averaging roughly 6% per year, a surge described as ?reckless? by the Canadian Taxpayers? Federation.</p><p>He decried the Liberal practice of underestimating budget surpluses ? and proceeded to low-ball all but one during his three years in finance.</p><p>Mr. Flaherty advanced the $6-billion GST cut by four years, knowing it was poor economic policy, and left the government without a contingency cushion to absorb an oncoming recession of a severity that seemed to catch him by surprise.</p><p>All the queasy economics point to a November update that will graphically illustrate the grim intersection between rising expenditures and falling revenues, making it difficult for the government to deliver on its earlier budget promises, never mind election goodies from the recent campaign."
"6140","Don Martin,
          National Post 
        </strong><span>
            Published: Tuesday, October 28, 2008</span></p><div class=""story-tools""><div class=""newsblock triline""><h4>Story Tools</h4><ul class=""category""><li><p><a href=""#"" title=""Decrease font size..."" class=""icon-font-small"" onclick=""javascript:return font_size(-1)""><span>-</span></a><a href=""#"" title=""Increase font size..."" class=""icon-font-big"" onclick=""javascript:return font_size(1)""><span>+</span></a> Change font size
            </p></li><li><p><a class=""icon-print"" href=""/print/"" onclick=""return print_click()"" target=""_blank"">Print this story</a><script type=""text/javascript"">
                function print_click() {
                u=location.href;
                u=delineate(u);
                window.open('/story-printer.html?id='+encodeURIComponent(u),'sharer','toolbar=1,status=1,location=0,menubar=1,scrollbars=1,width=650,height=600');
                return false;
                }
                function delineate(str)
                {
                theleft = str.indexOf(""="") + 1;
                theright = str.lastIndexOf(""&"");
                if ( theright == -1 ) {
                theright = str.length;
                }
                return(str.substring(theleft, theright));
                }
              </script></p></li><li><p><a class=""icon-email"" href=""#email"" onclick=""javascript:return story_email(true)"">E-Mail this story</a></p></li></ul></div><div class=""newsblock triline""><script type=""text/javascript"">
					str_url = encodeURIComponent(location.href);
					str_title = encodeURIComponent(document.title);
					// Facebook
					function fbs_click(){
						var w = 600, h = 400;
						window.open('http://www.facebook.com/sharer.php?u=' + str_url + '&t=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// Digg
					function digg_click(){
						var w = 960, h = screen.height - 200;
						window.open('http://digg.com/submit?phase=2&url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// StumbleUpon
					function stumble_click(){
						var w = 700, h = 400;
						window.open('http://www.stumbleupon.com/submit?url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// AddThis
					function addthis_click(){
						var w = 700, h = 750;
						window.open('http://www.addthis.com/bookmark.php?url=' + str_url + '&title=' + str_title + '&pub=nationalpost.com', 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					/* Delicious
					function dels_click(){
						var w = 700, h = 400;
						window.open('http://del.icio.us/post?url=' + str_url + '&jump=close&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}//*/
				</script><h4>Share This Story</h4><ul class=""category""><li><p><a href=""http://www.facebook.com/"" target=""_blank"" class=""icon-facebook"" onclick=""return fbs_click()"">Facebook</a></p></li><li><p><a href=""http://www.digg.com/"" target=""_blank"" class=""icon-digg"" onclick=""return digg_click()"">Digg</a></p></li><li><p><a href=""http://www.stumbleupon.com/"" target=""_blank"" class=""icon-stumbleupon"" onclick=""return stumble_click()"">Stumble Upon</a></p></li><li><p><a href=""http://www.addthis.com/"" target=""_blank"" class=""icon-addthis"" onclick=""return addthis_click()"">More</a></p></li></ul></div><div class=""sponsor""><p>Story tools presented by</p><div class=""ad-microbar""><script type=""text/javascript"">
								try{
									var arr_da = document.getElementById('ad-leaderboard').getElementsByTagName('script');
									for(var i in arr_da){
										if(typeof(arr_da[i].src) != 'undefined' && (/http:\/\/ad\.ca\.doubleclick\.net/.test(arr_da[i].src))){
											var str_da_src = arr_da[i].src;
											str_da_src = str_da_src.replace(/loc=\w+;/gi, 'loc=microbar;');
											str_da_src = str_da_src.replace(/sz=\d+x\d+;/gi, 'sz=88x31;');
											str_da_src = str_da_src.replace(/ptile=\d+;/gi, 'ptile=4;');
											document.write('<script type=""text/javascript"" src=""' + str_da_src + '""><\/script>');
											break;
										}
									}
								}catch(e){}
							</script></div></div></div><div class=""story-content""><p>OTTAWA -When rookie MPs are ushered into the House of Commons for the first time, tradition dictates they put up a mild show of resistance, as if they are being dragged to their seat.</p><p>For one Cabinet minister expected to be sworn in on Thursday, being reluctantly hauled before the Governor-General shouldn?t be an act.</p><p>There is no safer bet for ministerial reappointment than Jim Flaherty. There will be no lousier assignment than to be saddled with finance minister responsibilities that no longer come with a Santa clause for big-ticket spending.</p><p>Before this economic downturn hits bottom, it?s almost a given Mr. Flaherty will wear the Deficit Jim label as the Conservative who put the red back into Canada?s budget books for the first time since they were balanced in the mid-1990s.</p><p>That would be an ugly legacy for the charismatic Mr. Flaherty, who served as Ontario?s finance minister in 2001 when federal Liberals were racking up the largest surpluses in fiscal history.</p><p>It?s been almost two years since his most notorious flip-flop ? the Halloween announcement the government would break its pre-election promise and start taxing income trusts ? and now he?s careening toward another big-gulp policy reversal. Mr. Flaherty, it seems certain, will have to renege on his hell-or-high-water vow that the Conservatives would not lurch into deficit.</p><p>He?s now waffling on that campaign promise in the face of $10-billion deficit projections by some economists, a red tidal wave that could not be rebuffed with spending cuts or tax increases.</p><p>To cut a nickel off every dollar of spending by the federal government, no matter how braced the voters are for tough times, is an impossible mission and poor fit with the times.</p><p>That?s why, beyond serving as the reassuring face of continuity during a financial crisis, Mr. Flaherty?s record doesn?t necessarily crown</p><p>him as the best steady hand and confidence booster for troubled times.</p><p>There have been several performance hiccups and policy inconsistencies. His office broke the government?s own contracting rules by hiring a speechwriter without putting the job to tender. There was Mr. Flaherty?s infamous crack about overtaxed Ontario being a lousy place to invest, which, with the benefit of the current meltdown as hindsight, was prophetic.</p><p>His budgets have featured steep spending boosts averaging roughly 6% per year, a surge described as ?reckless? by the Canadian Taxpayers? Federation.</p><p>He decried the Liberal practice of underestimating budget surpluses ? and proceeded to low-ball all but one during his three years in finance.</p><p>Mr. Flaherty advanced the $6-billion GST cut by four years, knowing it was poor economic policy, and left the government without a contingency cushion to absorb an oncoming recession of a severity that seemed to catch him by surprise.</p><p>All the queasy economics point to a November update that will graphically illustrate the grim intersection between rising expenditures and falling revenues, making it difficult for the government to deliver on its earlier budget promises, never mind election goodies from the recent campaign.