"Rebels Advance in Eastern Congo</span></td></tr>
			<tr>
			<td valign=""top"">
				<span class=""byline"">
	By Derek Kilner</span> 
				<br>
				
				<span class=""dateline"">Nairobi</span><br>
				
					<span class=""datetime""><em>28 October 2008</em></span><br>
					
				</td>
				<td align=""left"" valign=""top""> </td>
			</tr>
		</table>
		
		

	<a href=""http://www.voanews.com/mediaassets/english/2008_10/Audio/mp3/kilner_DRC_fighting_28oct08.mp3"" class=""media-asset"" onclick=""dcsMedia(event);"">
	<span class=""media-asset"">Kilner report</span><span class=""media-asset-small"">&nbsp;-&nbsp;Download (MP3)
	
	<img src=""/voanews_shared/images/audio_icon.gif"" border=""0"" alt=""audio clip"">
	</span></a><br>

	
	
	
		<a class=""media-asset"" href=""http://www.voanews.com/english/figleaf/mp3filegenerate.cfm?filepath=http://www.voanews.com/mediaassets/english/2008_10/Audio/mp3/kilner_DRC_fighting_28oct08.mp3"" onclick=""dcsMedia(event);"">
		<span class=""media-asset"">
		
		Kilner report</span><span class=""media-asset-small"">
		
		-&nbsp;Listen&nbsp;(MP3)
		</span>
		
		<img src=""/voanews_shared/images/audio_icon.gif"" border=""0"" alt=""audio clip"">
		
		</span></a><br>
		<br>
		
		
		<span class=""body""><p><b>Rebels in eastern Democratic Republic of Congo continued on offensive near the town of Goma, as government troops retreated from their positions.  Derek Kilner reports from VOA's East Africa bureau in Nairobi that the U.N. refugee agency says 30,000 civilians displaced by the fighting are heading for Goma. </b><br><br>The National Congress for People's Defense, a rebel group led by former Congolese General Laurent Nkunda, is continuing an offensive it began Sunday.<br><br><table class=""APIMAGE"" style=""direction: ltr;"" align=""left"" width=""190""><tbody><tr><td style=""""><img id=""||CPIMAGE:558255|"" title=""Two boys, fleeing from the town of Kibumba, walk as a tank of the Congolese army moves on the road, about 35 kilometers north of Goma, 27 Oct 2008"" alt=""Two boys, fleeing from the town of Kibumba, walk as a tank of the Congolese army moves on the road, about 35 kilometers north of Goma, 27 Oct 2008"" src=""/english/images/afp_drc_unrest_175_27Oct08.jpg"" border=""0"" height=""190"" hspace=""2"" vspace=""2"" width=""190""></td></tr><tr><td style="""" class=""imagecaption"">Two boys, fleeing from the town of Kibumba, walk as a  Congolese army tank  moves on the road, about 35 kilometers north of Goma, 27 Oct 2008</td></tr></tbody></table>Government forces retreated as rebels led attacks near the towns of Kibumba and Rutshuru, north of Goma near the border with Uganda.<br><br>The U.N. refugee agency says 30,000 civilians have been displaced by the fighting and are moving toward Goma near the border with Rwanda.<br><br>Nkunda says his forces are protecting the minority Tutsi ethnic group in eastern Congo, and he has threatened to capture Goma.  His troops have been reported as close as seven kilometers from the city.  <br><br>Congolese security researcher Charles Nasibu, who spoke to VOA from Norway, says Nkunda's ultimate objective is difficult to determine.<br><br>""He said that he would like to organize a national movement in order to take power,"" he said. ""But that sounds very, not impossible, but very difficult.  I do not see how he can achieve that.  But if he succeeds to take Goma, that will change the situation at the general level.  He will have control of the Goma airport and he will have control of the border between Rwanda and DRC.""<br><br>Helicopters from the U.N. peacekeeping mission, known by its French acronym MONUC, fired on rebels Monday near Kibumba.  Nkunda's forces fired rockets at a U.N. convoy on Sunday, hitting two vehicles. <br><br>Officials with the U.N. mission say its mandate, which focuses on the protection of civilians, includes defending urban areas.  Peacekeepers repelled an advance on Goma by Nkunda's forces last December.<br><br>But many civilians are upset with the peacekeepers failure to halt the fighting.  As many as five people were killed Monday as demonstrators threw stones at the U.N. offices in Goma and peacekeepers fired into the air to disperse the crowd.&nbsp;  U.N. staff members have been told to stay inside.<br><br>Adding to the mission's troubles, the commander of the 17,000-member U.N. force resigned Monday, after less than three weeks in the country.  Congolese President Laurent Kabila also named a new defense minister on Sunday.<br><br>The Congolese government says Nkunda's forces are backed by Rwanda.  Congo says its forces recovered Rwandan military equipment in fighting earlier this month.  <br><br>Rwanda says Congolese troops are cooperating with the FDLR, a militia operating in eastern Congo that is composed of Rwandan Hutus, many of them involved in carrying out the 1994 genocide.<br><br>Nkunda signed a peace agreement with Congo's government in January, but fighting broke out again in August.  Up to 250,000 civilians have been displaced since then.  The United Nations estimates a total of one million people are displaced in eastern Congo.<br><br>According to Nasibu, the widespread availability of weapons in the region has undermined efforts to find a peaceful solution.<br><br>""The last agreement did not address correctly the small arms issue,"" he said. ""I was very, very skeptical when I read the peace agreement.  Because it was very, very clear that nothing can be done there in terms of peace without addressing the small arms issue.""<br><br>Nearly 5.5 million people are estimated to have been killed in Congo since civil war broke out in 1998."
"9681","
	By Derek Kilner</span> 
				<br>
				
				<span class=""dateline"">Nairobi</span><br>
				
					<span class=""datetime""><em>28 October 2008</em></span><br>
					
				</td>
				<td align=""left"" valign=""top""> </td>
			</tr>
		</table>
		
		

	<a href=""http://www.voanews.com/mediaassets/english/2008_10/Audio/mp3/kilner_DRC_fighting_28oct08.mp3"" class=""media-asset"" onclick=""dcsMedia(event);"">
	<span class=""media-asset"">Kilner report</span><span class=""media-asset-small"">&nbsp;-&nbsp;Download (MP3)
	
	<img src=""/voanews_shared/images/audio_icon.gif"" border=""0"" alt=""audio clip"">
	</span></a><br>

	
	
	
		<a class=""media-asset"" href=""http://www.voanews.com/english/figleaf/mp3filegenerate.cfm?filepath=http://www.voanews.com/mediaassets/english/2008_10/Audio/mp3/kilner_DRC_fighting_28oct08.mp3"" onclick=""dcsMedia(event);"">
		<span class=""media-asset"">
		
		Kilner report</span><span class=""media-asset-small"">
		
		-&nbsp;Listen&nbsp;(MP3)
		</span>
		
		<img src=""/voanews_shared/images/audio_icon.gif"" border=""0"" alt=""audio clip"">
		
		</span></a><br>
		<br>
		
		
		<span class=""body""><p><b>Rebels in eastern Democratic Republic of Congo continued on offensive near the town of Goma, as government troops retreated from their positions.  Derek Kilner reports from VOA's East Africa bureau in Nairobi that the U.N. refugee agency says 30,000 civilians displaced by the fighting are heading for Goma. </b><br><br>The National Congress for People's Defense, a rebel group led by former Congolese General Laurent Nkunda, is continuing an offensive it began Sunday.<br><br><table class=""APIMAGE"" style=""direction: ltr;"" align=""left"" width=""190""><tbody><tr><td style=""""><img id=""||CPIMAGE:558255|"" title=""Two boys, fleeing from the town of Kibumba, walk as a tank of the Congolese army moves on the road, about 35 kilometers north of Goma, 27 Oct 2008"" alt=""Two boys, fleeing from the town of Kibumba, walk as a tank of the Congolese army moves on the road, about 35 kilometers north of Goma, 27 Oct 2008"" src=""/english/images/afp_drc_unrest_175_27Oct08.jpg"" border=""0"" height=""190"" hspace=""2"" vspace=""2"" width=""190""></td></tr><tr><td style="""" class=""imagecaption"">Two boys, fleeing from the town of Kibumba, walk as a  Congolese army tank  moves on the road, about 35 kilometers north of Goma, 27 Oct 2008</td></tr></tbody></table>Government forces retreated as rebels led attacks near the towns of Kibumba and Rutshuru, north of Goma near the border with Uganda.<br><br>The U.N. refugee agency says 30,000 civilians have been displaced by the fighting and are moving toward Goma near the border with Rwanda.<br><br>Nkunda says his forces are protecting the minority Tutsi ethnic group in eastern Congo, and he has threatened to capture Goma.  His troops have been reported as close as seven kilometers from the city.  <br><br>Congolese security researcher Charles Nasibu, who spoke to VOA from Norway, says Nkunda's ultimate objective is difficult to determine.<br><br>""He said that he would like to organize a national movement in order to take power,"" he said. ""But that sounds very, not impossible, but very difficult.  I do not see how he can achieve that.  But if he succeeds to take Goma, that will change the situation at the general level.  He will have control of the Goma airport and he will have control of the border between Rwanda and DRC.""<br><br>Helicopters from the U.N. peacekeeping mission, known by its French acronym MONUC, fired on rebels Monday near Kibumba.  Nkunda's forces fired rockets at a U.N. convoy on Sunday, hitting two vehicles. <br><br>Officials with the U.N. mission say its mandate, which focuses on the protection of civilians, includes defending urban areas.  Peacekeepers repelled an advance on Goma by Nkunda's forces last December.<br><br>But many civilians are upset with the peacekeepers failure to halt the fighting.  As many as five people were killed Monday as demonstrators threw stones at the U.N. offices in Goma and peacekeepers fired into the air to disperse the crowd.&nbsp;  U.N. staff members have been told to stay inside.<br><br>Adding to the mission's troubles, the commander of the 17,000-member U.N. force resigned Monday, after less than three weeks in the country.  Congolese President Laurent Kabila also named a new defense minister on Sunday.<br><br>The Congolese government says Nkunda's forces are backed by Rwanda.  Congo says its forces recovered Rwandan military equipment in fighting earlier this month.  <br><br>Rwanda says Congolese troops are cooperating with the FDLR, a militia operating in eastern Congo that is composed of Rwandan Hutus, many of them involved in carrying out the 1994 genocide.<br><br>Nkunda signed a peace agreement with Congo's government in January, but fighting broke out again in August.  Up to 250,000 civilians have been displaced since then.  The United Nations estimates a total of one million people are displaced in eastern Congo.<br><br>According to Nasibu, the widespread availability of weapons in the region has undermined efforts to find a peaceful solution.<br><br>""The last agreement did not address correctly the small arms issue,"" he said. ""I was very, very skeptical when I read the peace agreement.  Because it was very, very clear that nothing can be done there in terms of peace without addressing the small arms issue.""<br><br>Nearly 5.5 million people are estimated to have been killed in Congo since civil war broke out in 1998.