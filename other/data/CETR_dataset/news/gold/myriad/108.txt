Brief history of women with a total dedication</span></div><br><div style=""width:200;"" class=picboxl><img src=images/books121008.jpg><br>Flaherty, A</div><p><b>CROSSINGS IN MERCY: THE STORY OF THE SISTERS OF MERCY PAPUA NEW GUINEA 1956-2006<br />
Teresa A Flaherty; The Sisters of Mercy ? Papua New Guinea, 2008<br />
Reviewed by Sr Rosa MacGinley pbvm</b><br />
<br />
THIS book, attractively produced and extensively illustrated, makes a significant contribution not only to religious history in both Australia and Papua New Guinea, but also to their general social history and to women?s history.<br />
<br />
The author, South Australian Sister of Mercy Teresa Flaherty, is well qualified to undertake the multi-faceted historical task posed by this study. She has spent many years in PNG and been a closely involved player in the educational progress of the country over that time.<br />
<br />
The greater part of this involvement has been at the tertiary level for which she was well qualified by postgraduate study in the USA and England, this culminating in a doctoral degree through Macquarie University in Sydney.<br />
<br />
She also undertook six short-term United Nations visits (1995-2001) to Bougainville in a peace promotion mission.  <br />
 <br />
Covering a very wide canvas and at many levels, this book has as a constant backdrop the continuing supportive and supply role of the Sisters of Mercy in Australia, together with the evolution of their own structures of government over the 50-year span of this study.<br />
<br />
Initially, by the early 1940s, represented by 17 autonomous congregations, spread across Australia to form its largest women?s religious institute, the Mercy Sisters in the course of the 1950s negotiated two large mutually independent groupings: a close-knit union comprising eight of these congregations and a looser federation chosen by the remaining nine.<br />
<br />
Directly influential in these moves towards greater unity was the Apostolic Delegate for Australia and Oceania Archbishop Romolo Carboni (1953-59), who also strongly urged Pius XII?s post-War call for every religious institute to become missionary ? a call promptly and generously responded to by both Mercy groupings.<br />
<br />
In 1956, the Union Sisters? Mother Patricia O?Neill acceded to Carboni?s request for a community for Goroka, calling for sisters to volunteer from across the union, as numbers willingly did.<br />
<br />
The following year, Mother Damian Duncombe of the federation sent a community of volunteer sisters to Kunjungini in the Wewak diocese.<br />
Other foundations in various dioceses followed, with contributions from all 17 Mercy units, until the final entry to the Aitape Diocese in 1977 to take over a hospital.<br />
<br />
Many more foundations, attracting additional sisters from Australia, were made within PNG itself from these initial bases, with an accompanying great expansion of the involvements undertaken. <br />
<br />
Meanwhile, the Mercy Sisters in Australia moved to national unification with the dissolving of both union and federation and the forming in 1981 of the Institute of the Sisters of Mercy of Australia (ISMA), a midway structure between a canonical union and federation, in the forms in which these were initially adopted.<br />
<br />
With the increasing number of sisters from Australia and with local young women seeking entry, a regional superior for PNG was approved in 1981.<br />
<br />
In 2006, the Jubilee Year, the sisters in PNG received Roman recognition as an autonomous region within the Institute of the Sisters of Mercy of Australia.<br />
<br />
Six Australian sisters volunteered to stay as members of the region, together with its eighteen local sisters.<br />
<br />
It is impossible within the scope of this review to cover adequately the range of ministries undertaken by the Mercy Sisters in PNG: initially, local primary schools and assistance with local health care in the mission areas; then more advanced education at secondary and tertiary levels; hospitals and clinics; training of both teachers and nurses for PNG certification; pastoral care in all its forms; administrative roles in dioceses and at national level in pastoral renewal movements.<br />
<br />
In short, the sisters played a central part in the emergence of a local PNG Church and indeed made a key contribution to the resources of a developing nation. <br />
<br />
Almost 180 sisters from Australia served in PNG over the 50 years, rising to a peak of 50 at any one time.<br />
<br />
Written in an easily readable style with clear sub-headings, many first-hand quotations and careful references, this book will remain a definitive study, both as a valuable historical resource and an impressive account of both the achievements and the human responses, in a variety of challenging circumstances, of a significant body of women.<br />
<br />
Bound together by a common charism in the legacy of their foundress Catherine McAuley, these women dared and achieved much."
"108","
Teresa A Flaherty; The Sisters of Mercy ? Papua New Guinea, 2008<br />
Reviewed by Sr Rosa MacGinley pbvm</b><br />
<br />
THIS book, attractively produced and extensively illustrated, makes a significant contribution not only to religious history in both Australia and Papua New Guinea, but also to their general social history and to women?s history.<br />
<br />
The author, South Australian Sister of Mercy Teresa Flaherty, is well qualified to undertake the multi-faceted historical task posed by this study. She has spent many years in PNG and been a closely involved player in the educational progress of the country over that time.<br />
<br />
The greater part of this involvement has been at the tertiary level for which she was well qualified by postgraduate study in the USA and England, this culminating in a doctoral degree through Macquarie University in Sydney.<br />
<br />
She also undertook six short-term United Nations visits (1995-2001) to Bougainville in a peace promotion mission.  <br />
 <br />
Covering a very wide canvas and at many levels, this book has as a constant backdrop the continuing supportive and supply role of the Sisters of Mercy in Australia, together with the evolution of their own structures of government over the 50-year span of this study.<br />
<br />
Initially, by the early 1940s, represented by 17 autonomous congregations, spread across Australia to form its largest women?s religious institute, the Mercy Sisters in the course of the 1950s negotiated two large mutually independent groupings: a close-knit union comprising eight of these congregations and a looser federation chosen by the remaining nine.<br />
<br />
Directly influential in these moves towards greater unity was the Apostolic Delegate for Australia and Oceania Archbishop Romolo Carboni (1953-59), who also strongly urged Pius XII?s post-War call for every religious institute to become missionary ? a call promptly and generously responded to by both Mercy groupings.<br />
<br />
In 1956, the Union Sisters? Mother Patricia O?Neill acceded to Carboni?s request for a community for Goroka, calling for sisters to volunteer from across the union, as numbers willingly did.<br />
<br />
The following year, Mother Damian Duncombe of the federation sent a community of volunteer sisters to Kunjungini in the Wewak diocese.<br />
Other foundations in various dioceses followed, with contributions from all 17 Mercy units, until the final entry to the Aitape Diocese in 1977 to take over a hospital.<br />
<br />
Many more foundations, attracting additional sisters from Australia, were made within PNG itself from these initial bases, with an accompanying great expansion of the involvements undertaken. <br />
<br />
Meanwhile, the Mercy Sisters in Australia moved to national unification with the dissolving of both union and federation and the forming in 1981 of the Institute of the Sisters of Mercy of Australia (ISMA), a midway structure between a canonical union and federation, in the forms in which these were initially adopted.<br />
<br />
With the increasing number of sisters from Australia and with local young women seeking entry, a regional superior for PNG was approved in 1981.<br />
<br />
In 2006, the Jubilee Year, the sisters in PNG received Roman recognition as an autonomous region within the Institute of the Sisters of Mercy of Australia.<br />
<br />
Six Australian sisters volunteered to stay as members of the region, together with its eighteen local sisters.<br />
<br />
It is impossible within the scope of this review to cover adequately the range of ministries undertaken by the Mercy Sisters in PNG: initially, local primary schools and assistance with local health care in the mission areas; then more advanced education at secondary and tertiary levels; hospitals and clinics; training of both teachers and nurses for PNG certification; pastoral care in all its forms; administrative roles in dioceses and at national level in pastoral renewal movements.<br />
<br />
In short, the sisters played a central part in the emergence of a local PNG Church and indeed made a key contribution to the resources of a developing nation. <br />
<br />
Almost 180 sisters from Australia served in PNG over the 50 years, rising to a peak of 50 at any one time.<br />
<br />
Written in an easily readable style with clear sub-headings, many first-hand quotations and careful references, this book will remain a definitive study, both as a valuable historical resource and an impressive account of both the achievements and the human responses, in a variety of challenging circumstances, of a significant body of women.<br />
<br />
Bound together by a common charism in the legacy of their foundress Catherine McAuley, these women dared and achieved much.