"13 Points on<BR>LAND RIGHTS<BR>for<BR>Aboriginal People</B></CENTER></FONT>

<FONT  SIZE=+0><B>1</B> For at least 40,000 years Aboriginal people lived on this 
continent, <B>owning, caring for and being sustained by the 
land.</B> With their deep knowledge of nature and respect for the 
environment in which they lived, they developed a successful 
economy and a rich spiritual and cultural life.

<B>2</B> In 1788 the British invaded this land and <B>used 
military force</B> to begin the land grab which continues to 
this day. <B>The Aboriginal people fought back </B>to protect their 
lands. Aboriginal people suffered murder on a huge scale, death 
through new diseases and poverty, and the destruction of much of 
Aboriginal traditional society.

<B>3</B> Despite having to fight a war for the land, the British 
declared this continent <B>terra nullius </B>? that the land was empty 
and belonged to no-one when they colonised it.

<B>4</B> For over 200 years the lie of <B>terra nullius </B>was the cruel and 
brutal cover for the mass murder, for the refusal to recognise 
Australia's indigenous race as people, for the forced removal of 
children from their families, for the inhuman exploitation of the 
labour of Aboriginal people, for the racist treatment and 
apartheid Aboriginal people have been subjected to.
<B>Terra Nullius </B>was the justification for the denial of <B>LAND 
RIGHTS.</B>

<B>5</B> But the Aboriginal people have survived and their struggle for 
<B>LAND and JUSTICE </B>has never ceased.

<B>6</B> In June 1992, the High Court of Australia recognised the 
concept of <B>Native Title, </B>stating it had existed <B>before 
</B>settlement and had continued <B>after </B>colonisation. 
However, <B>it said Native Title was extinguished </B>whenever 
land had been sold or set aside for some other purpose.

<B>7</B> The Howard Government's amendments to the Native Title Act have 
opened the way for another <B>massive land grab </B>by miners and 
pastoralists ? including Kerry Packer, Janet Holmes � Court and the 
Sultan of Brunei ? as their leases are converted (de facto) to 
freehold.

<B>8</B> This may bring <B>""certainty"" </B>to miners and pastoralists, the 
certainty of knowing their ill-gotten wealth and profits are 
secure. But for Aboriginal people it is, as Northern Land Council 
Chairman Galarrwuy Yunupingu said, <B>""the final drink from the 
poisonous water hole"".</B>

<B>9</B> Even though <B>Native Title </B>opens the way for only a small number 
of Aboriginal people to make land claims, it should none-the-less 
be protected from efforts to destroy it.

<B>10</B> The High Court's extremely narrow interpretation of <B>Native 
Title </B>in effect cuts off most Aboriginal people from making 
legitimate claims to land.

<B>11</B> Aboriginal people <B>should not be forced </B>to accept 
the <B>racist legal fiction </B>that land they have been <B>forcibly prevented </B>
from maintaining a ""continuing association"" with is therefore
the property of the colonisers for all time.

<B>12</B> In addition to <B>Native Title for the few, </B>there 
should be <B>LAND RIGHTS for ALL </B>Aborigines.

<B>13</B> <B>LAND RIGHTS mean recognition of Aboriginal prior ownership of 
all the continent of Australia. </B>There must be legislation to 
return land to its traditional owners on the basis of traditional 
ownership, religious association, long occupancy and / or need, 
including full rights to minerals and other natural resources.