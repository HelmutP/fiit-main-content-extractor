"Now for the real test</h1>
			<div id=""article_byline"">
						<strong>THABO MOHLALA</strong>
						
             | 				JOHANNESBURG, SOUTH AFRICA									-
						Oct 24 2008 06:00
			</div>
            </td>
         </tr>
     </table>
</div>
<div id=""article_commentcount"">
	<a href=""/article/2008-10-24-now-for-the-real-test#comments"">
		<img src=""images/icon_comments.gif"" alt=""comments"" width=""22"" height=""16"" border=""0"" /> 2 comment(s) | <a href=""comment/2008-10-24-now-for-the-real-test"">Post your comment</a>
	</a>
</div>
<div id=""sidebar"">
<div id=""article_tools"">
<table width=""100%"">
	<tr>
		<td colspan=""2"">
		<div class=""tools_heading"">ARTICLE TOOLS</div></td>
	</tr>
	<tr>
		<td><img src=""images/icon_addtoclippings.gif"" /></td>
		<td><a href=""/account/addtoclippings/233116"">Add to Clippings</td></td>
	</tr>
	<tr>
		<td><img src=""images/icon_sendtofriend.gif"" /></td>
		<td><a href=""emailarticle/233116""> Email to friend</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_print.gif"" /></td>
		<td><a href=""printformat/single/2008-10-24-now-for-the-real-test"">Print </a></td>
	</tr>
	<tr>
		<td colspan=""2""><br /><div class=""tools_heading"">MORE TOOLS</div></td>
	</tr>
	<tr>
		<td><img src=""images/icon_facebook.gif"" /></td>
		<td><a href=""http://apps.facebook.com/sanewsheadlines/"" target=""_new"">Add to Facebook</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_muti.gif"" /></td>
		<td><a href=""javascript:location.href='http://muti.co.za/submit?url='+encodeURIComponent (location.href)+'&title='+encodeURIComponent(document.title)"">Add to Muti</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_delicious.gif"" /></td>
		<td><a href=""javascript:window.open('http://del.icio.us/post?v=4&partner=mail&noui&jump=close&url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title), 'delicious','toolbar=no,width=700,height=400');"">Add to del.icio.us</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_technorati.gif"" /></td>
		<td><a href=""http://technorati.com/search/www.mg.co.za"" target=""_new"">Blog Reactions</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_digg.gif"" /></td>
		<td><script>thispageDigg = ""<a href='http://digg.com/submit?phase=2&url="" + encodeURIComponent(location.href) + ""&title="" + document.title + ""&bodytext=&topic=world_news'>Digg this story</a><br/>""; document.write(thispageDigg); </script></td>
	</tr>
		<tr>
		<td colspan=""2""><br /><div class=""tools_heading"">MAP</div></td>
	</tr>
	<tr>
		<td colspan=""2"">

    <!-- Code Block for Map Starts here -->

    <div id='map_article'>
        The map will replace this text. If any users do not have Flash Player 8 (or above), they'll see this message.
    </div>
    <script type=""text/javascript"">
        var map = new FusionMaps(""/fusionmaps/Maps/FCMap_Africa.swf"", ""map"", 170, 170, ""0"", ""0"");
         map.setDataXML(""<map showBevel='0' showShadow='0' showCanvasBorder='0' showLabels='0' showMarkerLabels='1' fillColor='C0C0C0' borderColor='FFFFFF' baseFont='Arial' baseFontSize='10' markerBorderColor='000000' markerBgColor='FF5904' markerRadius='6' legendPosition='bottom' useHoverColor='1' showMarkerToolTip='1' ><data><entity id='001' link='/country/algeria'/><entity id='002' link='/country/angola'/><entity id='003' link='/country/benin'/><entity id='004' link='/country/botswana'/><entity id='005' link='/country/burkina-faso'/><entity id='006' link='/country/burundi'/><entity id='007' link='/country/cameroon'/><entity id='008' link='/country/cape-verde'/><entity id='009' link='/country/central-african-republic'/><entity id='010' link='/country/chad'/><entity id='011' link='/country/comoros'/><entity id='012' link='/country/'/><entity id='013' link='/country/congo-the-democratic-republic-of'/><entity id='014' link='/country/djibouti'/><entity id='015' link='/country/egypt'/><entity id='016' link='/country/equatorial-guinea'/><entity id='017' link='/country/eritrea'/><entity id='018' link='/country/ethiopia'/><entity id='019' link='/country/gabon'/><entity id='020' link='/country/ghana'/><entity id='021' link='/country/guinea'/><entity id='022' link='/country/guineabissau'/><entity id='023' link='/country/kenya'/><entity id='024' link='/country/lesotho'/><entity id='025' link='/country/liberia'/><entity id='026' link='/country/libyan-arab-jamahiriya'/><entity id='027' link='/country/madagascar'/><entity id='028' link='/country/malawi'/><entity id='029' link='/country/mali'/><entity id='030' link='/country/mauritania'/><entity id='032' link='/country/morocco'/><entity id='033' link='/country/mozambique'/><entity id='034' link='/country/namibia'/><entity id='035' link='/country/niger'/><entity id='036' link='/country/nigeria'/><entity id='038' link='/country/rwanda'/><entity id='040' link='/country/sao-tome-and-principe'/><entity id='041' link='/country/senegal'/><entity id='042' link='/country/seychelles'/><entity id='043' link='/country/sierra-leone'/><entity id='044' link='/country/somalia'/><entity id='045' link='/country/south-africa'/><entity id='046' link='/country/sudan'/><entity id='047' link='/country/swaziland'/><entity id='048' link='/country/tanzania-united-republic-of'/><entity id='049' link='/country/togo'/><entity id='051' link='/country/tunisia'/><entity id='052' link='/country/uganda'/><entity id='053' link='/country/western-sahara'/><entity id='054' link='/country/zambia'/><entity id='055' link='/country/zimbabwe'/><entity id='056' link='/country/gambia'/><entity id='057' link='/country/congo'/><entity id='058' link='/country/mauritius'/><entity id='045' color='FF5904' link='country/2008-10-24-now-for-the-real-test'/></data></map>"");
        map.render(""map_article"");
    </script>
 	<!-- Code Block for Map Ends here -->





		</td>
	</tr>
	</table>
</div>


</div>

<div id=""storycontainer"">
<br />
<br />

	<span class=""article_lead"">
	Next week a new form of matric exam commences -- and tensions among learners, teachers and parents were heightened by recent blunders around trial maths papers. This has left many wondering whether the department of education will deliver a credible final exam. Penny Vinjevold, deputy director general for further education and training, set out to put Thabo Mohlala�s mind at rest.	</span>
	<br />
	<br />
	<span class=""article_body"">
	
<b>This marks the first year in which grade 12 learners would be sitting for their National Senior Certificate examinations since the introduction of the new curriculum, how ready are you for this challenging operation? </b><br />
Administratively, we have been preparing for these exams since 2005, we started to plan how we would build up to give both confidence and to improve our ability to examine and distribute and to mark the exam papers. We appointed [exam] panels and so since 2006 we have been training our exam panels who set grade 10,11 and 12 papers. In 2006 a sample of schools wrote grade 10 and in 2007 all grade 11 learners wrote the exams at the end of last year set by our papers.<br/><br/>
Then at the beginning of 2008 grade 12 exemplar papers set by the same panel saying were released to schools so that teachers and learners could see what the exams would look like at the end of the year. We have already recruited 35 000 markers and started training them together with invigilators. We made sure we recruit more senior markers with lot of experience. Our exam capturing system and back up systems are ready to release the results at the end of December.<br/><br/>
<b>The National Department of Education DoE has made available exemplar papers to all learners. But the similarities between the exemplar and trial papers amount to what has been called ""coaching"" or ""rigging"". </b><br />
No. Exams are set in completely different ways. Good examinations and good assessment practices throughout the world are not about putting obstacles in front of the children�you don't try to deliberately trip them up. What good exam does is test how much do children know, it doesn't try to hide the rules of the game from them. So all we've said was we want to give you the format so that you don't get surprised or frightened or get nervous during the exams. The exam itself would not be predictable. Secondly we made sure there are sets of questions that are problem-solving, questions that have high cognitive demand. In previous years children had passed papers to work through. We believe it is our duty to provide our learners with lots of examples of what the paper might look like. So we don't see this as coaching at all.<br/><br/>
<b>There are concerns that the exam timetable provides too little time between papers. </b><br />
We looked at this very carefully. In the past exams ran over five weeks and we lost the whole of October for teaching. Parents and teachers complained that the learners were often at home for five days between one exam and the next and in fact that this is much worse to the learners. Because they start to lose concentration and some get up to mischief or some would have forgotten what they were taught. So our view is that we needed to start in November and in that way we recover the whole month for teaching. We tried to do some good things like making sure that maths is written in the morning and we also interspersed the subjects with languages. <br/><br/>
<b>Change of content in subjects like physical science and visual arts were introduced late in the year and this has caused confusion among learners and teachers as they have already gone through those sections.</b><br />
It was never an external examination subject. It has always been an internally examined subject. But our strong provincial and national moderation team will look at the quality of the tasks that the learners have done during the year. Our moderators would make sure the quality of the internal assessment is acceptable standard. We have responded to teachers who said there was an overload in some subject like physical science and that 90% of the content would be examined while those aspects that cannot be examined would be dealt with through assessment. <br/><br/>
In the case of the arts, our panel of advisors alerted us to the fact that quite a large amount of one particular artist [Steven Cohen]'s work was unsuitable for school study. We received complaints from parents saying that the work bordered on pornography. We felt it would be irresponsible of the department [DoE] to leave the artist whose work was unsuitable as part of our recommended list, so we removed. But teachers who felt they wanted to teach the artist could do so as long as parents are consulted.<br/><br/>
<b>Life orientation would not be externally examined, yet this subject can make a learner pass or fail. Could DoE and Umalusi give guarantees that there would be a uniform standard in this subject across all the provinces and schools?</b><br />
It was never an external examination subject. It has always been clear that it is an internally examined and contextually based subject. But we do have a strong provincial and national moderation team who go around and look at the quality of the task that the learners have done during the year. Those that are not up to scratch either because it is not of quality or the work was never handed in could make a learner fail the subject. In November our team of moderators will be going through the nine provinces to make sure the quality of the internal assessment is of acceptable standard.<br/><br/>
<b>It is rumoured that the actual pass rate for last year's matric exam was 50% against the public figure of 65%. Is this true and how do you justify such a massive increase?</b><br />
The rumour is definitely incorrect. Learner performance is measured using carefully and thoughtfully constructed question papers using the best expertise available. After the paper is set and moderated by independent expert it is then presented to Umalusi for final approval. Therefore after the examination is written and the question papers marked, Umalusi reviews the marks for each subject and compare the marks obtained in the current examination to previous examinations. <br/><br/><div class='articlecontinues'><img src='/images/icon_downarrow.gif' /> CONTINUES BELOW <img src='/images/icon_downarrow.gif' /></div><center><div style='width:300px; height:250px;'><!-- begin ad tag (tile=3) n5503.mg.national, 300x250,250x250-->
<script language=""JavaScript"" type=""text/javascript"">
if (typeof ord=='undefined') {ord=Math.random()*10000000000000000;}
document.write('<script language=""JavaScript"" src=""http://ad.za.doubleclick.net/adj/n5503.mg/national;pos=rightmid;tile=3;sz=300x250,250x250;posno=2;tile=3;ord=' + ord + '?"" type=""text/javascript""><\/script>');
</script><noscript><a href=""http://ad.za.doubleclick.net/jump/n5503.mg/national;pos=rightmid;tile=3;sz=300x250,250x250;posno=2;tile=3;ord=123456789?"" target=""_blank""><img src=""http://ad.za.doubleclick.net/ad/n5503.mg/national;pos=rightmid;tile=3;sz=300x250,250x250;posno=2;tile=3;ord=123456789?"" width=""300"" height=""250"" border=""0"" alt=""Click here�"">Click here�</a></noscript>
<!-- End ad tag --></div></center><br/><br/>
If there are no compelling reasons why marks in current examination should be higher or lower than previous, Umalusi adjust the marks, within strict boundaries, to be in line with the marks of previous history. The adjustment of the marks is an internationally acknowledged process to ensure that equivalent standards are maintained from year to year. The principle was applied in the Senior Certificate in public exam and has been in use as early as 1933. <br/><br/>
<b>During the marking of the exam papers markers rely on memorandums, which have to be fairly broad as to accommodate different answers learners may give. Will the markers be able to consider answers that are correct albeit different to those in the memorandum?</b><br />
That is why the markers we have chosen are teachers who have produced high results in previous years. In other words, we are going for best teachers who understand the curriculum best. We have intensive memorandum discussion sessions where chief markers from all provinces may sit the whole day to discuss relevant points like looking at possible and alternative answers learners may give. They then take the memo back and work through the papers and if anything comes out of that marking they then feed that back in so that everybody knows if there is an open-ended question all the alternatives answers the children could put forward are considered.<br/><br/>
<b>There are reports that in the Northern Cape only 42 schools were selected to sit for the trial examinations and this created anxiety among learners who did not take part in the exercise, why is this so?</b><br />
These schools obtained under-60% pass rate in 2007, so they had to take part in the compulsory trial exams. We have to protect the children. The exams were available to all schools but we didn't want to impose it on schools that have already set their own exams and who have been getting between 80%-90% pass rates. <br/><br/>
<b>Why can't you supply the public with raw marks as opposed to Umalusi's adjusted marks. Is this not misleading to the public, universities and the learners themselves?</b><br />
The raw marks are those from the written examination only, which are then presented to Umalusi for their quality assurance as explained earlier. After they have been approved the marks are then combined with the internal assessment marks, which includes school based assessment (orals and practical marks), to arrive at the final marks per learner.<br/><br/>
<b>Can this year's matriculants enroll with top universities overseas without taking on additional courses first or writing entrance tests? What system has our matric exam being benchmarked against?</b><br />
The National Senior Certificate (NSC) allows candidates an admission to university either for higher certificate, diploma, or Bachelor's degree studies. Candidates need to satisfy specific pass requirements for each of these areas of study and some universities may have additional requirements for admission. The NSC has been benchmarked with Scottish Qualification Authority, Cambridge International Examinations and the Board of Studies, New South Wales. <br/><br/>
These bodies evaluated the scope and content of [our] curriculum and exemplar question papers. They evaluated question papers of ten major subjects including: accounting, English, first additional language, economics business studies, history, geography, physical science, life sciences, mathematics and mathematical literacy. The three assessment bodies concluded that the subjects are internationally comparable.<br/><br/>
<b>There is now no standard or high-grade differentiation, which was meant to accommodate learners of different abilities. How will they now be catered for?</b><br />
That is a very good question. What we've done is to ask our examiners to make sure that 30% of questions are standard grade level. In other words, you shouldn't fail this year if you'd have passed last year. So there is a good bunch of questions that are for standard grade level. <br/><br/>
Then the level goes up quite steeply to 50%, which is higher grade. We have made sure that in every question paper there are difficult questions right up to 80%-90% level just to make sure there is differentiation. Our exams have been structured to make sure we've high level of differentiation in the papers.<br/><br/>
<b>What are the penalties for staff who leak exam papers and what will happen to exam cheats?</b><br />
Irregularities that occur during the conduct of the NSC's examination are managed in terms of the various applicable and relevant regulations. Staff members who are involved in leaking the exam question paper will be dealt with in accordance with the prescribed regulations and this can result in a dismissal. The matter may also be referred to police for a possible criminal charge. <br/><br/>
In cases where there are reports of alleged irregularities, the matter is investigated and will be handled by the Provincial Examination Irregularities Committee. In the event that it is established that a learner gained unfair advantage over others, his or her marks will be declared null and void and the candidate may be barred from writing the examination for a maximum period of 3-5 years. <br/><br/>
<b>Finally in light of the recent slip-up with regard to preliminary mathematics paper one and mathematical literacy paper, for which you apologized, what assurance can you give to learners, teachers and parents who might still be harbouring doubts about the integrity and credibility of the forthcoming national examinations?</b><br />
The first point is that those exams were not set by DoE's national panel. They were not internally and externally moderated. And the only thing that went wrong was that the memorandum was not checked. And it was only in mathematics. <br/><br/>
So I think we got out the memorandum immediately within 48 hours.But I still want to re-assure people that that [trial exams] was not set by the national panel and what I've done after that was to be absolutely sure that we have checked, re-checked and double checked the papers and memorandums and of course there would be memorandum discussion sessions that I spoke about earlier."
"5828","THABO MOHLALA</strong>
						
             | 				JOHANNESBURG, SOUTH AFRICA									-
						Oct 24 2008 06:00
			</div>
            </td>
         </tr>
     </table>
</div>
<div id=""article_commentcount"">
	<a href=""/article/2008-10-24-now-for-the-real-test#comments"">
		<img src=""images/icon_comments.gif"" alt=""comments"" width=""22"" height=""16"" border=""0"" /> 2 comment(s) | <a href=""comment/2008-10-24-now-for-the-real-test"">Post your comment</a>
	</a>
</div>
<div id=""sidebar"">
<div id=""article_tools"">
<table width=""100%"">
	<tr>
		<td colspan=""2"">
		<div class=""tools_heading"">ARTICLE TOOLS</div></td>
	</tr>
	<tr>
		<td><img src=""images/icon_addtoclippings.gif"" /></td>
		<td><a href=""/account/addtoclippings/233116"">Add to Clippings</td></td>
	</tr>
	<tr>
		<td><img src=""images/icon_sendtofriend.gif"" /></td>
		<td><a href=""emailarticle/233116""> Email to friend</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_print.gif"" /></td>
		<td><a href=""printformat/single/2008-10-24-now-for-the-real-test"">Print </a></td>
	</tr>
	<tr>
		<td colspan=""2""><br /><div class=""tools_heading"">MORE TOOLS</div></td>
	</tr>
	<tr>
		<td><img src=""images/icon_facebook.gif"" /></td>
		<td><a href=""http://apps.facebook.com/sanewsheadlines/"" target=""_new"">Add to Facebook</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_muti.gif"" /></td>
		<td><a href=""javascript:location.href='http://muti.co.za/submit?url='+encodeURIComponent (location.href)+'&title='+encodeURIComponent(document.title)"">Add to Muti</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_delicious.gif"" /></td>
		<td><a href=""javascript:window.open('http://del.icio.us/post?v=4&partner=mail&noui&jump=close&url='+encodeURIComponent(location.href)+'&title='+encodeURIComponent(document.title), 'delicious','toolbar=no,width=700,height=400');"">Add to del.icio.us</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_technorati.gif"" /></td>
		<td><a href=""http://technorati.com/search/www.mg.co.za"" target=""_new"">Blog Reactions</a></td>
	</tr>
	<tr>
		<td><img src=""images/icon_digg.gif"" /></td>
		<td><script>thispageDigg = ""<a href='http://digg.com/submit?phase=2&url="" + encodeURIComponent(location.href) + ""&title="" + document.title + ""&bodytext=&topic=world_news'>Digg this story</a><br/>""; document.write(thispageDigg); </script></td>
	</tr>
		<tr>
		<td colspan=""2""><br /><div class=""tools_heading"">MAP</div></td>
	</tr>
	<tr>
		<td colspan=""2"">

    <!-- Code Block for Map Starts here -->

    <div id='map_article'>
        The map will replace this text. If any users do not have Flash Player 8 (or above), they'll see this message.
    </div>
    <script type=""text/javascript"">
        var map = new FusionMaps(""/fusionmaps/Maps/FCMap_Africa.swf"", ""map"", 170, 170, ""0"", ""0"");
         map.setDataXML(""<map showBevel='0' showShadow='0' showCanvasBorder='0' showLabels='0' showMarkerLabels='1' fillColor='C0C0C0' borderColor='FFFFFF' baseFont='Arial' baseFontSize='10' markerBorderColor='000000' markerBgColor='FF5904' markerRadius='6' legendPosition='bottom' useHoverColor='1' showMarkerToolTip='1' ><data><entity id='001' link='/country/algeria'/><entity id='002' link='/country/angola'/><entity id='003' link='/country/benin'/><entity id='004' link='/country/botswana'/><entity id='005' link='/country/burkina-faso'/><entity id='006' link='/country/burundi'/><entity id='007' link='/country/cameroon'/><entity id='008' link='/country/cape-verde'/><entity id='009' link='/country/central-african-republic'/><entity id='010' link='/country/chad'/><entity id='011' link='/country/comoros'/><entity id='012' link='/country/'/><entity id='013' link='/country/congo-the-democratic-republic-of'/><entity id='014' link='/country/djibouti'/><entity id='015' link='/country/egypt'/><entity id='016' link='/country/equatorial-guinea'/><entity id='017' link='/country/eritrea'/><entity id='018' link='/country/ethiopia'/><entity id='019' link='/country/gabon'/><entity id='020' link='/country/ghana'/><entity id='021' link='/country/guinea'/><entity id='022' link='/country/guineabissau'/><entity id='023' link='/country/kenya'/><entity id='024' link='/country/lesotho'/><entity id='025' link='/country/liberia'/><entity id='026' link='/country/libyan-arab-jamahiriya'/><entity id='027' link='/country/madagascar'/><entity id='028' link='/country/malawi'/><entity id='029' link='/country/mali'/><entity id='030' link='/country/mauritania'/><entity id='032' link='/country/morocco'/><entity id='033' link='/country/mozambique'/><entity id='034' link='/country/namibia'/><entity id='035' link='/country/niger'/><entity id='036' link='/country/nigeria'/><entity id='038' link='/country/rwanda'/><entity id='040' link='/country/sao-tome-and-principe'/><entity id='041' link='/country/senegal'/><entity id='042' link='/country/seychelles'/><entity id='043' link='/country/sierra-leone'/><entity id='044' link='/country/somalia'/><entity id='045' link='/country/south-africa'/><entity id='046' link='/country/sudan'/><entity id='047' link='/country/swaziland'/><entity id='048' link='/country/tanzania-united-republic-of'/><entity id='049' link='/country/togo'/><entity id='051' link='/country/tunisia'/><entity id='052' link='/country/uganda'/><entity id='053' link='/country/western-sahara'/><entity id='054' link='/country/zambia'/><entity id='055' link='/country/zimbabwe'/><entity id='056' link='/country/gambia'/><entity id='057' link='/country/congo'/><entity id='058' link='/country/mauritius'/><entity id='045' color='FF5904' link='country/2008-10-24-now-for-the-real-test'/></data></map>"");
        map.render(""map_article"");
    </script>
 	<!-- Code Block for Map Ends here -->





		</td>
	</tr>
	</table>
</div>


</div>

<div id=""storycontainer"">
<br />
<br />

	<span class=""article_lead"">
	Next week a new form of matric exam commences -- and tensions among learners, teachers and parents were heightened by recent blunders around trial maths papers. This has left many wondering whether the department of education will deliver a credible final exam. Penny Vinjevold, deputy director general for further education and training, set out to put Thabo Mohlala�s mind at rest.	</span>
	<br />
	<br />
	<span class=""article_body"">
	
<b>This marks the first year in which grade 12 learners would be sitting for their National Senior Certificate examinations since the introduction of the new curriculum, how ready are you for this challenging operation? </b><br />
Administratively, we have been preparing for these exams since 2005, we started to plan how we would build up to give both confidence and to improve our ability to examine and distribute and to mark the exam papers. We appointed [exam] panels and so since 2006 we have been training our exam panels who set grade 10,11 and 12 papers. In 2006 a sample of schools wrote grade 10 and in 2007 all grade 11 learners wrote the exams at the end of last year set by our papers.<br/><br/>
Then at the beginning of 2008 grade 12 exemplar papers set by the same panel saying were released to schools so that teachers and learners could see what the exams would look like at the end of the year. We have already recruited 35 000 markers and started training them together with invigilators. We made sure we recruit more senior markers with lot of experience. Our exam capturing system and back up systems are ready to release the results at the end of December.<br/><br/>
<b>The National Department of Education DoE has made available exemplar papers to all learners. But the similarities between the exemplar and trial papers amount to what has been called ""coaching"" or ""rigging"". </b><br />
No. Exams are set in completely different ways. Good examinations and good assessment practices throughout the world are not about putting obstacles in front of the children�you don't try to deliberately trip them up. What good exam does is test how much do children know, it doesn't try to hide the rules of the game from them. So all we've said was we want to give you the format so that you don't get surprised or frightened or get nervous during the exams. The exam itself would not be predictable. Secondly we made sure there are sets of questions that are problem-solving, questions that have high cognitive demand. In previous years children had passed papers to work through. We believe it is our duty to provide our learners with lots of examples of what the paper might look like. So we don't see this as coaching at all.<br/><br/>
<b>There are concerns that the exam timetable provides too little time between papers. </b><br />
We looked at this very carefully. In the past exams ran over five weeks and we lost the whole of October for teaching. Parents and teachers complained that the learners were often at home for five days between one exam and the next and in fact that this is much worse to the learners. Because they start to lose concentration and some get up to mischief or some would have forgotten what they were taught. So our view is that we needed to start in November and in that way we recover the whole month for teaching. We tried to do some good things like making sure that maths is written in the morning and we also interspersed the subjects with languages. <br/><br/>
<b>Change of content in subjects like physical science and visual arts were introduced late in the year and this has caused confusion among learners and teachers as they have already gone through those sections.</b><br />
It was never an external examination subject. It has always been an internally examined subject. But our strong provincial and national moderation team will look at the quality of the tasks that the learners have done during the year. Our moderators would make sure the quality of the internal assessment is acceptable standard. We have responded to teachers who said there was an overload in some subject like physical science and that 90% of the content would be examined while those aspects that cannot be examined would be dealt with through assessment. <br/><br/>
In the case of the arts, our panel of advisors alerted us to the fact that quite a large amount of one particular artist [Steven Cohen]'s work was unsuitable for school study. We received complaints from parents saying that the work bordered on pornography. We felt it would be irresponsible of the department [DoE] to leave the artist whose work was unsuitable as part of our recommended list, so we removed. But teachers who felt they wanted to teach the artist could do so as long as parents are consulted.<br/><br/>
<b>Life orientation would not be externally examined, yet this subject can make a learner pass or fail. Could DoE and Umalusi give guarantees that there would be a uniform standard in this subject across all the provinces and schools?</b><br />
It was never an external examination subject. It has always been clear that it is an internally examined and contextually based subject. But we do have a strong provincial and national moderation team who go around and look at the quality of the task that the learners have done during the year. Those that are not up to scratch either because it is not of quality or the work was never handed in could make a learner fail the subject. In November our team of moderators will be going through the nine provinces to make sure the quality of the internal assessment is of acceptable standard.<br/><br/>
<b>It is rumoured that the actual pass rate for last year's matric exam was 50% against the public figure of 65%. Is this true and how do you justify such a massive increase?</b><br />
The rumour is definitely incorrect. Learner performance is measured using carefully and thoughtfully constructed question papers using the best expertise available. After the paper is set and moderated by independent expert it is then presented to Umalusi for final approval. Therefore after the examination is written and the question papers marked, Umalusi reviews the marks for each subject and compare the marks obtained in the current examination to previous examinations. <br/><br/><div class='articlecontinues'><img src='/images/icon_downarrow.gif' /> CONTINUES BELOW <img src='/images/icon_downarrow.gif' /></div><center><div style='width:300px; height:250px;'><!-- begin ad tag (tile=3) n5503.mg.national, 300x250,250x250-->
<script language=""JavaScript"" type=""text/javascript"">
if (typeof ord=='undefined') {ord=Math.random()*10000000000000000;}
document.write('<script language=""JavaScript"" src=""http://ad.za.doubleclick.net/adj/n5503.mg/national;pos=rightmid;tile=3;sz=300x250,250x250;posno=2;tile=3;ord=' + ord + '?"" type=""text/javascript""><\/script>');
</script><noscript><a href=""http://ad.za.doubleclick.net/jump/n5503.mg/national;pos=rightmid;tile=3;sz=300x250,250x250;posno=2;tile=3;ord=123456789?"" target=""_blank""><img src=""http://ad.za.doubleclick.net/ad/n5503.mg/national;pos=rightmid;tile=3;sz=300x250,250x250;posno=2;tile=3;ord=123456789?"" width=""300"" height=""250"" border=""0"" alt=""Click here�"">Click here�</a></noscript>
<!-- End ad tag --></div></center><br/><br/>
If there are no compelling reasons why marks in current examination should be higher or lower than previous, Umalusi adjust the marks, within strict boundaries, to be in line with the marks of previous history. The adjustment of the marks is an internationally acknowledged process to ensure that equivalent standards are maintained from year to year. The principle was applied in the Senior Certificate in public exam and has been in use as early as 1933. <br/><br/>
<b>During the marking of the exam papers markers rely on memorandums, which have to be fairly broad as to accommodate different answers learners may give. Will the markers be able to consider answers that are correct albeit different to those in the memorandum?</b><br />
That is why the markers we have chosen are teachers who have produced high results in previous years. In other words, we are going for best teachers who understand the curriculum best. We have intensive memorandum discussion sessions where chief markers from all provinces may sit the whole day to discuss relevant points like looking at possible and alternative answers learners may give. They then take the memo back and work through the papers and if anything comes out of that marking they then feed that back in so that everybody knows if there is an open-ended question all the alternatives answers the children could put forward are considered.<br/><br/>
<b>There are reports that in the Northern Cape only 42 schools were selected to sit for the trial examinations and this created anxiety among learners who did not take part in the exercise, why is this so?</b><br />
These schools obtained under-60% pass rate in 2007, so they had to take part in the compulsory trial exams. We have to protect the children. The exams were available to all schools but we didn't want to impose it on schools that have already set their own exams and who have been getting between 80%-90% pass rates. <br/><br/>
<b>Why can't you supply the public with raw marks as opposed to Umalusi's adjusted marks. Is this not misleading to the public, universities and the learners themselves?</b><br />
The raw marks are those from the written examination only, which are then presented to Umalusi for their quality assurance as explained earlier. After they have been approved the marks are then combined with the internal assessment marks, which includes school based assessment (orals and practical marks), to arrive at the final marks per learner.<br/><br/>
<b>Can this year's matriculants enroll with top universities overseas without taking on additional courses first or writing entrance tests? What system has our matric exam being benchmarked against?</b><br />
The National Senior Certificate (NSC) allows candidates an admission to university either for higher certificate, diploma, or Bachelor's degree studies. Candidates need to satisfy specific pass requirements for each of these areas of study and some universities may have additional requirements for admission. The NSC has been benchmarked with Scottish Qualification Authority, Cambridge International Examinations and the Board of Studies, New South Wales. <br/><br/>
These bodies evaluated the scope and content of [our] curriculum and exemplar question papers. They evaluated question papers of ten major subjects including: accounting, English, first additional language, economics business studies, history, geography, physical science, life sciences, mathematics and mathematical literacy. The three assessment bodies concluded that the subjects are internationally comparable.<br/><br/>
<b>There is now no standard or high-grade differentiation, which was meant to accommodate learners of different abilities. How will they now be catered for?</b><br />
That is a very good question. What we've done is to ask our examiners to make sure that 30% of questions are standard grade level. In other words, you shouldn't fail this year if you'd have passed last year. So there is a good bunch of questions that are for standard grade level. <br/><br/>
Then the level goes up quite steeply to 50%, which is higher grade. We have made sure that in every question paper there are difficult questions right up to 80%-90% level just to make sure there is differentiation. Our exams have been structured to make sure we've high level of differentiation in the papers.<br/><br/>
<b>What are the penalties for staff who leak exam papers and what will happen to exam cheats?</b><br />
Irregularities that occur during the conduct of the NSC's examination are managed in terms of the various applicable and relevant regulations. Staff members who are involved in leaking the exam question paper will be dealt with in accordance with the prescribed regulations and this can result in a dismissal. The matter may also be referred to police for a possible criminal charge. <br/><br/>
In cases where there are reports of alleged irregularities, the matter is investigated and will be handled by the Provincial Examination Irregularities Committee. In the event that it is established that a learner gained unfair advantage over others, his or her marks will be declared null and void and the candidate may be barred from writing the examination for a maximum period of 3-5 years. <br/><br/>
<b>Finally in light of the recent slip-up with regard to preliminary mathematics paper one and mathematical literacy paper, for which you apologized, what assurance can you give to learners, teachers and parents who might still be harbouring doubts about the integrity and credibility of the forthcoming national examinations?</b><br />
The first point is that those exams were not set by DoE's national panel. They were not internally and externally moderated. And the only thing that went wrong was that the memorandum was not checked. And it was only in mathematics. <br/><br/>
So I think we got out the memorandum immediately within 48 hours.But I still want to re-assure people that that [trial exams] was not set by the national panel and what I've done after that was to be absolutely sure that we have checked, re-checked and double checked the papers and memorandums and of course there would be memorandum discussion sessions that I spoke about earlier.