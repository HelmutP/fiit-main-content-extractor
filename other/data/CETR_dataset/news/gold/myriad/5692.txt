"England vow to step it up after World Cup scare</h1>
<span class=""byline"">Sunday, October   26, 2008</span>
<div class=""ff_img_fix""><img  src=""http://img.metro.co.uk/i/pix/2008/10b/PeacockG_450x300.jpg"" alt=""Peacock"" width=""450"" height=""300"" border=""1"" /><br /><div class=""caption"">Warning: England captain Peacock
</div>
</div>
<h2 class=""articlestandfirst"">England captain Jamie Peacock insists his side will improve on their unconvincing opening display when they face Australia in the 
second week of the World Cup.
</h2><p class=""article"">
Tony Smith&#39;s men were forced to come from behind to beat Pool One minnows Papua New Guinea 32-22 in an entertaining tussle at the Dairy Farmers Stadium.
</p><p class=""article"">
England&#39;s victory virtually ensures they will qualify for the semi-finals, regardless of the 
outcome of their remaining group matches against the Kangaroos and New Zealand.
</p><div id=""intelliTXT""><p class=""article"">
 <div class=""divider""></div>
<div class=""boxdivider"">
<strong>RELATED ITEMS</strong><br/>
<ul class=""related-items"">
<li class=""video""><a href=""javascript:PopUp('you_popup','/video/videoPlayer.html?inMediaId=22213&in_page_id=1','942','645','0')""><img src=""http://img.metro.co.uk/i/std/video/camera.gif"" border=""0""/> Latest sport bulletin</a></li>
<li class=""arrow""><a href=""/sport/chnHeadlines.html?in_page_id=35"" >Today's top sport headlines</a></li>
<li class=""arrow""><a href=""http://fantasyfootball.metro.co.uk/"">Play Free Fantasy Football - &#163;40,000 to be won</a></li>
</ul>
</div>
<br clear=""all"">
</p><p class=""article"">
But Peacock, who was one of England&#39;s best performers on Saturday, said: &#39;The World Cup is not won in the first game.
</p><p class=""article"">
&#39;We know there are areas where we need to vastly improve and I think we&#39;re used to that in Super League, improving on your performance from the week before.&#39;
</p><p class=""article"">
Trailing 12-6 after 28 minutes, the fired-up Kumuls stung England with three tries in the second quarter to lead 16-12 at the break.
</p><p class=""article"">
England were indebted to coach Smith&#39;s interval pep talk as they began to match their opponents&#39; first-half passion.
</p><p class=""article"">
St Helens winger Ade Gardner added to his first-half try and Leeds winger Lee Smith completed his hat-trick on his debut as England used the width of the pitch to take advantage of a tiring Kumuls side.
</p><p class=""article"">
&#39;Smithy [coach Tony Smith] got it covered at half-time and we came out and played a lot better in the second half,&#39; said Peacock.
</p><p class=""article"">
&#39;I had confidence in the players alongside me. It was a matter of sticking to it in the second half.&#39;
</p><p class=""article"">
Smith added: &#39;Credit to my boys, we knew we were a bit off and to improve in the second half, was more of what England are about.&#39;