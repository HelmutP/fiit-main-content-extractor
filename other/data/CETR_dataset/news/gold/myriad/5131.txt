"Soyuz spacecraft returns Russians and American</h1>
	</em>
</p>
<p class=""info"">
            <author>By Steve Gutterman, AP</author><br/>
            <em>Friday, 24 October 2008</em>
        </p>
    <div class=""photoCaption"" style=""width: 300px; margin-left: 10px;"">
                    <a href=""javascript:launchPopup('http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?action=Popup&gallery=no','', 650, 610, true, true, true, false);"">
                                    <img src=""http://www.independent.co.uk/multimedia/archive/00064/astronaut_64081t.jpg"" width=""300"" height=""204"" alt=""null"">
                                </a><br>
                    <p class=""credits""><strong>AP Photo/Dmitry Kostyukov</strong></p>
                    <ul class=""paging""> <!-- more -->
                                    <li class=""label"">
                                        <a href=""javascript:launchPopup('http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?action=Popup&gallery=no','', 650, 610, true, true, true, false);"">
                                        <img src=""http://www.independent.co.uk/independent.co.uk/images/i_photos.gif"" alt=""Photos"" title=""Photos"" width=""14"" height=""10"" /> enlarge</a>
                                    </li>
                                </ul>
                    </div>

<!--
    Create a list of all articles, collections and links which are ""from the archives""
-->
<!--
    Create a list of all articles, collections and links which are ""from the archives""
-->
<div class=""articleRelated"">
	<div class=""articleTools wrapper"">

		<ul class=""articleTools"">
			<li><!--proximic_exclude_on-->

			<a onclick=""popup=window.open(this.href, 'contentservices', 'width=508,height=550,scrollbars=yes,resizable=yes'); popup.focus(); return false;""
   href=""http://license.icopyright.net/g2/3.7463?icx_id=news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?service=PrintICopyright""
   target=""_blank"">
					<img src=""http://www.independent.co.uk/independent.co.uk/images/i_print.gif"" alt=""Print"" title=""Print"" width=""17"" height=""15"" /> Print
				</a>
			</li>
			<li>
			<a onclick=""popup=window.open(this.href, 'contentservices', 'width=508,height=550,scrollbars=yes,resizable=yes'); popup.focus(); return false;""
   href=""http://license.icopyright.net/g1/3.7463?icx_id=news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?service=PrintICopyright""
   target=""_blank"">
					<img src=""http://www.independent.co.uk/independent.co.uk/images/i_mail.gif"" alt=""Email"" title=""Email"" width=""16"" height=""10"" /> Email
				</a>
			<!--proximic_exclude_off--></li>
		</ul>
	</div>
<form method=""post"" action=""http://www.independent.co.uk/search/index.jsp"">
		<fieldset class=""searchBox"">
			<legend>Search</legend>
			<label for=""f_searchtool"">Search</label>
			<input type=""text"" name=""eceExpr"" id=""f_searchtool"" value='' class=""required""/>
			<!--
			<input type=""text"" id=""zoek"" name=""eceExpr"" value='' class=""required""/>
			-->
			<input type=""hidden"" name=""eceSort"" value=""relevance""/>
			<input type=""hidden"" name=""eceMode"" value=""search""/>
			<input type=""hidden"" name=""eceForm"" value=""simple""/>
			<button type=""submit"">Go</button><br/>
			<input type=""radio"" name=""where"" value=""this"" id=""f_searchtarget-1"" class=""radio"" /> <label for=""f_searchtarget-1"" class=""static"">Independent.co.uk</label>
			<input type=""radio"" name=""where"" value=""web"" id=""f_searchtarget-2"" class=""radio"" /> <label for=""f_searchtarget-2"" class=""static"">Web</label>
		</fieldset>
	</form>

	<div class=""bookmarksBox"">
		<h2>Bookmark &amp; Share</h2>
		<ul>
			<li class=""digg""><!--proximic_exclude_on--><a href=""http://digg.com/submit?phase=2&amp;url=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html&amp;title=Soyuz spacecraft returns Russians and American - Science - Independent.co.uk"">Digg It</a></li>
			<li class=""delicious""><a href=""http://del.icio.us/post?url=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html&amp;title=Soyuz spacecraft returns Russians and American - Science - Independent.co.uk"">del.icio.us</a></li>
      <li class=""facebook""><a href=""http://www.facebook.com/sharer.php?u=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html"">Facebook</a></li>
			<li class=""reddit""><a href=""http://www.reddit.com/r/independent/submit?url=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html&amp;title=Soyuz spacecraft returns Russians and American - Science - Independent.co.uk"">Reddit</a><!--proximic_exclude_off--></li>
		</ul>
		<p class=""help""><a href=""/independent.co.uk/static/_bookmarks.html?KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=400"" class=""thickbox"">What are these?</a></p>
	</div>
</div>
<div class=""body"">
        <p>
            <!--proximic_content_on--><p>
A Soyuz capsule carrying an American and two Russians touched down on target 
  in Kazakhstan today after a descent from the international space station, 
  safely delivering the first two men to follow their fathers into space.
</p>
<!--proximic_content_off-->
            </p>
        <!--proximic_content_on-->
            <p>
The Soyuz TMA-12 capsule landed at 9:37 a.m. local time (0337GMT), about 55 
  miles north of Arkalyk in north-central Kazakhstan, Russian Mission Control 
  spokesman Valery Lyndin told The Associated Press.
</p>
<p>
Search and recovery crews buzzed in on Mi-8 helicopters and extracted Richard 
  Garriott, Sergei Volkov and Oleg Kononenko from the capsule, which landed on 
  its side on the brushy surface under a clear sky.
</p>
<p>
&quot;What a great ride that was,&quot; said Garriott, an American computer game 
  designer who paid some $30 million for a 10-day stay on the space station. 
  Sitting in an armchair and wrapped in a blue blanket against the 
  near-freezing temperature on the steppe, he smiled broadly.
</p>
<p>
&quot;This is obviously a pinnacle experience,&quot; Garriott said in televised comments.
</p>
<p>
Garriott was greeted by his father, Owen Garriott, a retired NASA astronaut 
  who flew on the U.S. space station Skylab in 1973.
</p>
<p>
&quot;Hey, Papa-san,&quot; said Richard Garriott, 47. The pair shook hands.
</p>
<p>
&quot;How come you look so fresh and ready to go?&quot; Owen Garriott, 77, asked his son.
</p>
<p>
&quot;Because I'm fresh and ready to go &mdash; again,&quot; he replied.
</p>
<p>
Not right away, though.
</p>
<p>
&quot;I'm looking forward to some fresh food and to calling my loved ones,&quot; said 
  Garriott, who lives in Austin, Texas, and was seen off by his girlfriend and 
  brother, among others, when he rocketed up to the station on another Soyuz 
  craft on Oct. 12.
</p>
<p>
&quot;I've got my father here, but I've got other family back home I want to get a 
  hold of.&quot;
</p>
<p>
Volkov sat next to Garriott. The son of a cosmonaut, he beat out Garriott as 
  the first human being to follow a parent into space when he flew up to the 
  space station six months ago. Kononenko, who also spent 199 days in space, 
  was the last out of the capsule and could not be seen in the TV footage.
</p>
<p>
The head of the Russian space agency Roskosmos, Anatoly Perminov, said on 
  state-run Vesti-24 television that Kononenko had a tougher time than his 
  crewmates during the descent but &quot;feels good now.&quot; It was the first space 
  mission for all three men.
</p>
<p>
The uneventful descent was a relief for space officials &mdash; and the crew &mdash; after 
  technical problems caused unusually steep &quot;ballistic descents&quot; for the last 
  two returning crews, putting them hundreds of kilometers (miles) off course 
  and subjecting them to stronger gravitational force than in a usual.
</p>
<p>
On a Soyuz returning in May, the malfunction of an explosive bolt delayed the 
  separation of the re-entry capsule from the rest of the ship. It forced the 
  crew &mdash; including a U.S. astronaut and South Korea's first space traveler &mdash; 
  to endure a rough ride as the gyrating capsule descended facing the wrong 
  way.
</p>
<p>
It took nearly half an hour for search helicopters to locate the capsule, 
  which landed some 20 minutes late and 420 kilometers (260 miles) off target, 
  and determine the crew was unharmed.
</p>
<p>
Last October, a computer glitch sent Malaysia's first astronaut and two 
  Russian cosmonauts on a steeper-than-normal path during their return to 
  Earth.
</p>
<p>
Russian space officials said changes had been made to equipment and computer 
  programming to prevent another ballistic descent, but they were clearly 
  relieved at Friday's on-time, on-target landing.
</p>
<p>
The Soyuz TMA-12's module separated without a hitch before it entered the 
  atmosphere, and a series of parachutes gradually slowed its speed from 230 
  meters (755 feet) per 5 second to about 1.5 meters (5 feet) per second.
</p>
<p>
&quot;I can't recall a more ideal landing,&quot; Perminov said.
</p>
<p>
Garriott, who created the Ultima computer game series, spent time on the 
  station conducting experiments &mdash; including some whose sponsors helped pay 
  for a trip he said cost him a large chunk of his wealth. He also took 
  pictures of the Earth's surface to measure changes since his father did the 
  same 35 years ago.
</p>
<p>
Garriott took a Soyuz up to the 10-year-old station along with U.S. astronaut 
  Michael Fincke and Russian cosmonaut Yuri Lonchakov, who will stay in orbit 
  for six months. Also on board is U.S. astronaut Gregory Chamitoff.
</p>
<p>
The U.S. shuttle Endeavor is due to launch in November and carry equipment 
  needed for raising the number of astronauts living at the orbiting outpost 
  from three to six. That transition should occur in the first half of next 
  year.
</p>
<p>
The head of the Russian state-controlled RKK Energiya company, which builds 
  the Soyuz spacecraft and Progress cargo ships, said today that construction 
  of ships for the next few missions was on schedule, but further plans could 
  be jeopardized by a money crunch caused by the nation's financial crisis. 
  Vitaly Lopota said the banks had been slow to provide loans to the company, 
  and he urged the government to quickly earmark funds."
"5131","By Steve Gutterman, AP</author><br/>
            <em>Friday, 24 October 2008</em>
        </p>
    <div class=""photoCaption"" style=""width: 300px; margin-left: 10px;"">
                    <a href=""javascript:launchPopup('http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?action=Popup&gallery=no','', 650, 610, true, true, true, false);"">
                                    <img src=""http://www.independent.co.uk/multimedia/archive/00064/astronaut_64081t.jpg"" width=""300"" height=""204"" alt=""null"">
                                </a><br>
                    <p class=""credits""><strong>AP Photo/Dmitry Kostyukov</strong></p>
                    <ul class=""paging""> <!-- more -->
                                    <li class=""label"">
                                        <a href=""javascript:launchPopup('http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?action=Popup&gallery=no','', 650, 610, true, true, true, false);"">
                                        <img src=""http://www.independent.co.uk/independent.co.uk/images/i_photos.gif"" alt=""Photos"" title=""Photos"" width=""14"" height=""10"" /> enlarge</a>
                                    </li>
                                </ul>
                    </div>

<!--
    Create a list of all articles, collections and links which are ""from the archives""
-->
<!--
    Create a list of all articles, collections and links which are ""from the archives""
-->
<div class=""articleRelated"">
	<div class=""articleTools wrapper"">

		<ul class=""articleTools"">
			<li><!--proximic_exclude_on-->

			<a onclick=""popup=window.open(this.href, 'contentservices', 'width=508,height=550,scrollbars=yes,resizable=yes'); popup.focus(); return false;""
   href=""http://license.icopyright.net/g2/3.7463?icx_id=news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?service=PrintICopyright""
   target=""_blank"">
					<img src=""http://www.independent.co.uk/independent.co.uk/images/i_print.gif"" alt=""Print"" title=""Print"" width=""17"" height=""15"" /> Print
				</a>
			</li>
			<li>
			<a onclick=""popup=window.open(this.href, 'contentservices', 'width=508,height=550,scrollbars=yes,resizable=yes'); popup.focus(); return false;""
   href=""http://license.icopyright.net/g1/3.7463?icx_id=news/science/soyuz-spacecraft-returns-russians-and-american-971753.html?service=PrintICopyright""
   target=""_blank"">
					<img src=""http://www.independent.co.uk/independent.co.uk/images/i_mail.gif"" alt=""Email"" title=""Email"" width=""16"" height=""10"" /> Email
				</a>
			<!--proximic_exclude_off--></li>
		</ul>
	</div>
<form method=""post"" action=""http://www.independent.co.uk/search/index.jsp"">
		<fieldset class=""searchBox"">
			<legend>Search</legend>
			<label for=""f_searchtool"">Search</label>
			<input type=""text"" name=""eceExpr"" id=""f_searchtool"" value='' class=""required""/>
			<!--
			<input type=""text"" id=""zoek"" name=""eceExpr"" value='' class=""required""/>
			-->
			<input type=""hidden"" name=""eceSort"" value=""relevance""/>
			<input type=""hidden"" name=""eceMode"" value=""search""/>
			<input type=""hidden"" name=""eceForm"" value=""simple""/>
			<button type=""submit"">Go</button><br/>
			<input type=""radio"" name=""where"" value=""this"" id=""f_searchtarget-1"" class=""radio"" /> <label for=""f_searchtarget-1"" class=""static"">Independent.co.uk</label>
			<input type=""radio"" name=""where"" value=""web"" id=""f_searchtarget-2"" class=""radio"" /> <label for=""f_searchtarget-2"" class=""static"">Web</label>
		</fieldset>
	</form>

	<div class=""bookmarksBox"">
		<h2>Bookmark &amp; Share</h2>
		<ul>
			<li class=""digg""><!--proximic_exclude_on--><a href=""http://digg.com/submit?phase=2&amp;url=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html&amp;title=Soyuz spacecraft returns Russians and American - Science - Independent.co.uk"">Digg It</a></li>
			<li class=""delicious""><a href=""http://del.icio.us/post?url=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html&amp;title=Soyuz spacecraft returns Russians and American - Science - Independent.co.uk"">del.icio.us</a></li>
      <li class=""facebook""><a href=""http://www.facebook.com/sharer.php?u=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html"">Facebook</a></li>
			<li class=""reddit""><a href=""http://www.reddit.com/r/independent/submit?url=http://www.independent.co.uk/news/science/soyuz-spacecraft-returns-russians-and-american-971753.html&amp;title=Soyuz spacecraft returns Russians and American - Science - Independent.co.uk"">Reddit</a><!--proximic_exclude_off--></li>
		</ul>
		<p class=""help""><a href=""/independent.co.uk/static/_bookmarks.html?KeepThis=true&amp;TB_iframe=true&amp;height=500&amp;width=400"" class=""thickbox"">What are these?</a></p>
	</div>
</div>
<div class=""body"">
        <p>
            <!--proximic_content_on--><p>
A Soyuz capsule carrying an American and two Russians touched down on target 
  in Kazakhstan today after a descent from the international space station, 
  safely delivering the first two men to follow their fathers into space.
</p>
<!--proximic_content_off-->
            </p>
        <!--proximic_content_on-->
            <p>
The Soyuz TMA-12 capsule landed at 9:37 a.m. local time (0337GMT), about 55 
  miles north of Arkalyk in north-central Kazakhstan, Russian Mission Control 
  spokesman Valery Lyndin told The Associated Press.
</p>
<p>
Search and recovery crews buzzed in on Mi-8 helicopters and extracted Richard 
  Garriott, Sergei Volkov and Oleg Kononenko from the capsule, which landed on 
  its side on the brushy surface under a clear sky.
</p>
<p>
&quot;What a great ride that was,&quot; said Garriott, an American computer game 
  designer who paid some $30 million for a 10-day stay on the space station. 
  Sitting in an armchair and wrapped in a blue blanket against the 
  near-freezing temperature on the steppe, he smiled broadly.
</p>
<p>
&quot;This is obviously a pinnacle experience,&quot; Garriott said in televised comments.
</p>
<p>
Garriott was greeted by his father, Owen Garriott, a retired NASA astronaut 
  who flew on the U.S. space station Skylab in 1973.
</p>
<p>
&quot;Hey, Papa-san,&quot; said Richard Garriott, 47. The pair shook hands.
</p>
<p>
&quot;How come you look so fresh and ready to go?&quot; Owen Garriott, 77, asked his son.
</p>
<p>
&quot;Because I'm fresh and ready to go &mdash; again,&quot; he replied.
</p>
<p>
Not right away, though.
</p>
<p>
&quot;I'm looking forward to some fresh food and to calling my loved ones,&quot; said 
  Garriott, who lives in Austin, Texas, and was seen off by his girlfriend and 
  brother, among others, when he rocketed up to the station on another Soyuz 
  craft on Oct. 12.
</p>
<p>
&quot;I've got my father here, but I've got other family back home I want to get a 
  hold of.&quot;
</p>
<p>
Volkov sat next to Garriott. The son of a cosmonaut, he beat out Garriott as 
  the first human being to follow a parent into space when he flew up to the 
  space station six months ago. Kononenko, who also spent 199 days in space, 
  was the last out of the capsule and could not be seen in the TV footage.
</p>
<p>
The head of the Russian space agency Roskosmos, Anatoly Perminov, said on 
  state-run Vesti-24 television that Kononenko had a tougher time than his 
  crewmates during the descent but &quot;feels good now.&quot; It was the first space 
  mission for all three men.
</p>
<p>
The uneventful descent was a relief for space officials &mdash; and the crew &mdash; after 
  technical problems caused unusually steep &quot;ballistic descents&quot; for the last 
  two returning crews, putting them hundreds of kilometers (miles) off course 
  and subjecting them to stronger gravitational force than in a usual.
</p>
<p>
On a Soyuz returning in May, the malfunction of an explosive bolt delayed the 
  separation of the re-entry capsule from the rest of the ship. It forced the 
  crew &mdash; including a U.S. astronaut and South Korea's first space traveler &mdash; 
  to endure a rough ride as the gyrating capsule descended facing the wrong 
  way.
</p>
<p>
It took nearly half an hour for search helicopters to locate the capsule, 
  which landed some 20 minutes late and 420 kilometers (260 miles) off target, 
  and determine the crew was unharmed.
</p>
<p>
Last October, a computer glitch sent Malaysia's first astronaut and two 
  Russian cosmonauts on a steeper-than-normal path during their return to 
  Earth.
</p>
<p>
Russian space officials said changes had been made to equipment and computer 
  programming to prevent another ballistic descent, but they were clearly 
  relieved at Friday's on-time, on-target landing.
</p>
<p>
The Soyuz TMA-12's module separated without a hitch before it entered the 
  atmosphere, and a series of parachutes gradually slowed its speed from 230 
  meters (755 feet) per 5 second to about 1.5 meters (5 feet) per second.
</p>
<p>
&quot;I can't recall a more ideal landing,&quot; Perminov said.
</p>
<p>
Garriott, who created the Ultima computer game series, spent time on the 
  station conducting experiments &mdash; including some whose sponsors helped pay 
  for a trip he said cost him a large chunk of his wealth. He also took 
  pictures of the Earth's surface to measure changes since his father did the 
  same 35 years ago.
</p>
<p>
Garriott took a Soyuz up to the 10-year-old station along with U.S. astronaut 
  Michael Fincke and Russian cosmonaut Yuri Lonchakov, who will stay in orbit 
  for six months. Also on board is U.S. astronaut Gregory Chamitoff.
</p>
<p>
The U.S. shuttle Endeavor is due to launch in November and carry equipment 
  needed for raising the number of astronauts living at the orbiting outpost 
  from three to six. That transition should occur in the first half of next 
  year.
</p>
<p>
The head of the Russian state-controlled RKK Energiya company, which builds 
  the Soyuz spacecraft and Progress cargo ships, said today that construction 
  of ships for the next few missions was on schedule, but further plans could 
  be jeopardized by a money crunch caused by the nation's financial crisis. 
  Vitaly Lopota said the banks had been slow to provide loans to the company, 
  and he urged the government to quickly earmark funds.