"Man to be sentenced over female assaults</h1>
            <a href=""javascript:showPlayer('fogartyp_av.html')"" class=""AVicons"">
              <img src=""/news/images/icon_video.gif"" alt=""watch"" />
            </a>
            <span class=""storyDate"">Tuesday, 28 October 2008 19:49</span>
          </div>
          <div class=""storyBody"" xmlns="""">
            <rte:body><p>A 43-year-old man who attacked two women, holding one of them prisoner overnight, will be sentenced tomorrow at the Central Criminal Court.</p>
<p>Patrick Fogarty, a homeless alcoholic who is originally from England, attacked a school teacher on Portmarnock beach and later a woman in Malahide, whom he also sexually assaulted.</p>
<p>Mr Fogarty attacked his first victim on Portmarnock beach on 24 April last year.</p><div id=""story_island""><div class=""storyIslandTitle"">Advertisement</div> </div>
<p>The 27-year-old school teacher was returning from an evening walk when he hit her over the head with a wine bottle and tried to drag her into nearby bushes. </p>
<p>She managed to escape after biting his hand while he had her in a 'headlock'.</p>
<p>The following night he attacked a 49-year-old woman who was living rough in Malahide. </p>
<p>He sexually assaulted her and held her captive until the following day when she escaped and alerted garda�. </p>
<p>However Mr Fogarty was waiting for her when she returned to her makeshift home that night and repeatedly abused her physically and sexually throughout the night.</p>
<p>He tied her up and tried to move her to another location when he feared they might be found. </p>
<p>The court heard the woman is still suffering the effects of the physical violence of the attack. </p>
<p>Both women have suffered lasting psychological effects.</p>
<p>Mr Fogarty recognised himself from a photo-fit picture in a newspaper and some months later handed himself in to garda�. </p>
<p>The court heard he moved to Ireland three years ago after living rough in England for more than 20 years. </p>
<p>He survived on food from supermarket dustbins and help from local people. </p>
<p>He refused social welfare because he said having money led him to abuse alcohol. </p>
<p>At the time of the offences he had been given &#x20AC;350 by a local man to help him travel to see relatives. </p>
<p>He is due to be sentenced tomorrow.