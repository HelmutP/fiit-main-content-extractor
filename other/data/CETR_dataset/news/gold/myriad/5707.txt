"Becks to 'live alone' in Milan</h1>
<span class=""byline"">Wednesday, October   22, 2008</span>
<div class=""ff_img_fix""><img  src=""http://img.metro.co.uk/i/pix/2007/07/beckhamsAP_450x300.jpg"" alt=""Beckhams best dressed"" width=""450"" height=""300"" border=""1"" /></div>
<p class=""article"">David Beckham plans to fly off to Milan to play football for four months and leave Posh behind in LA, according to reports. 
</p><p class=""article"">
Becks is set to sign for Italian giants AC Milan in a loan move from LA Galaxy, but his wife and three kids will likely remain in Los Angeles and visit him only in the school holidays, according to reports.
</p><div id=""intelliTXT""><p class=""article"">
The couple are apparently not keen on uprooting Brooklyn, nine, Romeo, six, and three-year-old Cruz by taking them out of school and away from friends barely a year after their move to the US.
</p><p class=""article"">
The deal is set to begin in January and could keep Beckham in the England team in the lead-up to the World Cup in South Africa.
</p><p class=""article"">
 <div class=""divider""></div>
<div class=""boxdivider"">
<strong>RELATED ITEMS</strong><br/>
<ul class=""related-items"">
<li class=""video""><a href=""javascript:PopUp('you_popup','/video/videoPlayer.html?inMediaId=22233&in_page_id=1','942','645','0')""><img src=""http://img.metro.co.uk/i/std/video/camera.gif"" border=""0""/> Latest showbiz bulletin</a></li>
<li class=""arrow""><a href=""/fame/chnHeadlines.html?in_page_id=7"" >Today's top showbiz headlines</a></li>
<li class=""arrow""><a href=""/fame/article.html?Will_Posh_win_WAG_war_with_Milans_best?&in_article_id=367798&in_page_id=7"">Will Posh win WAG war with Milan's best?</a></li>
<li class=""arrow""><a href=""/sport/oddballs/article.html?Lost_in_translation:_Becks_Italian_phrasebook&in_article_id=367794&in_page_id=46"">Beckham's Italian phrasebook</a></li>
</ul>
</div>
<br clear=""all"">
</p><p class=""article"">
A close friend of Posh told the Daily Mail: ""Victoria supports everything he does. But they will not want to remove the children from their school.
</p><p class=""article"">
""Victoria stays wherever the kids are. She won't move out, but she will just be there when she can.""
</p><p class=""article"">
Becks was of course away from the family when he played for large stints of his Real Madrid career, which was soured off the field by allegations of an affair with his former PA Rebecca Loos.