"Zuma: don't worry about defactors</h1>
        <div class=""ArticleDate""><em>28 October 2008, 12:21</em></div>
        <div class=""ArticleMPUHolder"">
          <h5 class=""ArticleViewRelatedHeader"">Related Articles</h5>
          <div class=""RelatedArticleFeatureLinks"">
            <ul>
              <li><a href=""/index.php?fSectionId=3532&amp;fArticleId=nw20081028072025798C487903"" title=""'Angry people' won't misdirect ANC - Zuma"">'Angry people' won't misdirect ANC - Zuma</a></li>
              <li><a href=""/index.php?fSectionId=3532&amp;fArticleId=vn20081028060253233C213881"" title=""You can't stop us - Shilowa"">You can't stop us - Shilowa</a></li>
              <li><a href=""/index.php?fSectionId=3532&amp;fArticleId=vn20081028055522145C641520"" title=""'Shikota' goes to Sandton"">'Shikota' goes to Sandton</a></li>
              <li><a href=""/index.php?fSectionId=3532&amp;fArticleId=nw20081027173143418C760814"" title=""Zuma visits Free State"">Zuma visits Free State</a></li>
              <li><a href=""/index.php?fSectionId=3532&amp;fArticleId=vn20081027054806718C351601"" title=""More leave even as Cyril warns them"">More leave even as Cyril warns them</a></li>
              <li><a href=""/index.php?fSectionId=3532&amp;fArticleId=vn20081027054107878C483612"" title=""The ANC campaign machinery is creaking"">The ANC campaign machinery is creaking</a></li>
            </ul>
          </div>
          <div class=""SquareAdHolder""><script type=""text/javascript"">DisplayAds('MPUAV');</script></div>
        </div>
        <div class=""ArticleContent"">
	  ANC leader Jacob Zuma has dismissed the leaders of a breakaway faction as ""angry people"" trying to distract the ruling party from its bid to improve the lives of South Africans, the state broadcaster said on Tuesday.<br />
<br />
A small group of supporters of former South African president Thabo Mbeki split from the African National Congress after it ousted him last month over allegations that he had abused his power and tried to smear Zuma in a corruption case.<br />
<br />
Defectors including former cabinet ministers plan to hold a convention this week to lay the foundations of a new party, hoping to threaten the ANC's 14-year stranglehold on power.<br />
<br />
""I don't think we should worry about angry people. They are likely to take your time, your effort and misdirect it,"" Zuma told an ANC Women's League function on Monday night, according to SABC.<br />
<br />
Zuma also denied former defence minister Mosiuoa Lekota's accusation that the ANC had deviated from its goals in the wake of Zuma's victory over Mbeki in the ANC leadership contest last year. Lekota is one of the leaders of the new party.<br />
<br />
The ANC is determined to prevent a trickle of defections swelling in the months leading up to a general election, which is expected around April 2009.<br />
<br />
ANC officials have attacked the breakaway leaders for abandoning the movement that overthrew white minority rule; ANC supporters have disrupted the dissidents' meetings.<br />
<br />
Mbhazima Shilowa, who resigned as premier of Gauteng, centre of South Africa's business and industry, in sympathy with Mbeki, said on Tuesday all political parties should be able to campaign freely.<br />
<br />
He confirmed that the convention would be in Johannesburg and said it would be inclusive.<br />
<br />
""We are going to invite all political parties represented in parliament, and that includes (opposition leader) Helen Zille,"" he told Talk Radio 702.<br />
<br />
Although the ANC traditionally wins about two-thirds of the vote in national elections against a divided opposition, polls indicate that it has lost support since Zuma took over the helm.<br />
<br />
Zuma's strong backing from trade unions has spooked foreign investors, and his battle to fend off a corruption scandal has unsettled upper- and middle-class blacks, who had strongly supported Mbeki in previous elections.<br />
<br />
A judge threw out bribery, fraud and other charges against Zuma last month, alleging high-level political meddling in the case. Prosecutors are appealing, and have suggested that charges could be reinstated before the end of the year.<br />
<br />
Many analysts say the ANC still commands deep loyalty, even from Mbeki supporters, and that a complete break-up is unlikely.<br />
<br />
Most of Mbeki's top cabinet ministers agreed to serve under his successor as president, Zuma ally Kgalema Motlanthe. (Reporting by Paul Simao; editing by Kevin Liffey) - Reuters