"Arizona: escaping the US elections</h1>
<h2 class=""sub-heading padding-top-5 padding-bottom-15"">In an isolated corner of Arizona, you can find peace, ranchers and absolutely no TV, says Stanley Stewart</h2>
<!-- END: Module - Main Heading -->	
</div>
<div id=""region-column1-layout2"">
<!--CMA user Call Diffrenet Variation Of Image -->
<!-- BEGIN: M24 Article Headline with landscape image (d) -->
<script type=""text/javascript"" src=""/tol/js/m24-image-browser.js""></script>
<script type=""text/javascript"" src=""/tol/js/tol.js""></script>
<!-- BEGIN: Module - M24 Article Headline with landscape image (d) -->
<script type=""text/javascript"">
<!--
/* Global variables that are used for ""image browsing"". Used on article pages to rotate the images of a story. */
var sImageBrowserImagePath = '';
var aArticleImages = new Array();
var aImageDescriptions = new Array();
var aImageEnlargeLink = new Array();
var aImageEnlargePopupWidth = '500';
var aImageEnlargePopupHeight = '500';
var aImagePhotographer = new Array();
var nSelectedArticleImage = 0;
var aImageAltText= new Array(); 
var i=0;
//-->
</script>
<script type=""text/javascript"">
<!--
aArticleImages[i] = '/multimedia/archive/00419/cowboy-385_419730a.jpg';
//-->
</script>
<script type=""text/javascript"">
<!--
aImageDescriptions[i] = '';
//-->
</script>
<!--Don't Display undifined test for credit -->
<script type=""text/javascript"">
<!--
aImageAltText[i] = ""A cowboy"" ;	
aImageAltText[i] = aImageAltText[i].replace(/&quot;/g,""\"""");
//-->	
</script>
<script type=""text/javascript"">
<!--
aImageEnlargeLink[i] = '/multimedia/archive/00419/cowboy-385_419730a.jpg';
i=i+1;
//-->
</script>	
<div id=""dynamic-image-holder""><img src=""/multimedia/archive/00419/cowboy-385_419730a.jpg"" width=""385"" height=""185"" border=""0"" alt=""A cowboy"" /></div>
<!-- Remove following <div> to not show photographer information -->
<!-- Remove following <div> to not show image description -->
<!-- Remove following <div> to not show enlarge option -->
<!---->
<div id=""pagination-container"" class=""pagination-container"">
<script type=""text/javascript"">
<!--
fCreateImageBrowser(nSelectedArticleImage,'landscape',""/tol/"");
//-->
</script>
</div>
<div class=""clear""></div>
<div class=""padding-bottom-15""></div>
<!-- Print Author name associated with the article -->
<div id=""main-article"">
<div class=""article-author"">
<!-- Print Author name from By Line associated with the article -->	
<span class=""small""></span><span class=""byline"">
Stanley Stewart 
</span>
<div class=""clear""></div>
</div>
</div> 
<!-- END: Module - M24 Article Headline with landscape image (d) -->
<!-- Article Copy module -->
<!-- BEGIN: Module - Main Article -->
<!-- Check the Article Type and display accordingly-->
<!-- Print Author image associated with the Author-->
<!-- Print the body of the article-->
<style type=""text/css"">
div#related-article-links p a, div#related-article-links p a:visited {
color:#06c;
} 
</style>	
<div id=""related-article-links"">
<!-- Pagination -->
<!--Display article with page breaks -->
<p>
I<b> </b>had only been in Tombstone half an hour when the shooting started. 
Apparently, it was the Republicans and the Democrats at it again. 
</p>
<p>
This was just the kind of thing I had come to Arizona to escape. 
</p>
<p>
Like most people in this year of interminable electioneering, I had had enough 
of politicking, of spin and soundbites, of poll figures and attack ads. 
</p>
<p>
I was heading deep into the southeastern corner of the state in the hope of 
escape. I was looking for a quieter America, for a ranch somewhere with 
wide, empty landscapes, and cowboys with few opinions beyond yup and nope. 
When I stopped for lunch in Tombstone, I hadn&rsquo;t expected to be detained by a 
gunfight on Main Street. 
</p>
<!--#include file=""m63-article-related-attachements.html""-->
<!-- BEGIN: Module - M63 - Article Related Attachements -->
<script type=""text/javascript"">
<!--
function pictureGalleryPopup(pubUrl,articleId) {
var newWin = window.open(pubUrl+'template/2.0-0/element/pictureGalleryPopup.jsp?id='+articleId+'&&offset=0&&sectionName=DestinationsUSA','mywindow','menubar=0,resizable=0,width=1000,height=711');
}
//-->
</script>
<!-- BEGIN: Comment Teaser Module -->
<div class=""float-left related-attachements-container"">
<!-- END: Comment Teaser Module -->	
<!-- BEGIN: Module - M63 - Article Related Package -->
<form name=""packageArticle"" method=""post"" action="""">	
<div class=""related-attachements-top padding-top-10""></div>
<div class=""related-attachements-side padding-top-7 padding-bottom-10 padding-right-7"">
<div class=""padding-bottom-5 padding-top-3"">
<img src=""/multimedia/archive/00362/indian-385_362724b.jpg"" alt="""" width=""70"" height=""70"" border=""0"" />
<h2 class=""sub-heading-puff color-06c padding-bottom-5""> <a href='/tol/travel/destinations/usa/article4268164.ece' class=""link-06c"">Going native in the USA</a></h2>
<!-- Display Teaser text -->
<p class=""small"">Oregon&rsquo;s annual Nez Perce Indian gathering is a Native American ritual, a pilgrimage and a knees-up</p>
</div>
</div>
</form>	
<form name=""packageArticle"" method=""post"" action="""">	
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<img src=""/multimedia/archive/00377/ST_Travel_377559b.jpg"" alt="""" width=""70"" height=""70"" border=""0"" />
<h2 class=""sub-heading-puff color-06c padding-bottom-5""> <a href='/tol/travel/destinations/usa/article4442943.ece' class=""link-06c"">Obama v McCain fever in Washington</a></h2>
<!-- Display Teaser text -->
<p class=""small"">It&rsquo;s an election battle with global implications, and the front line is Washington, DC. Rob Ryan is in the thick of it</p>
</div>
</div>
</form>	
<form name=""packageArticle"" method=""post"" action="""">	
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<img src=""/multimedia/archive/00322/Travel__322354b.jpg"" alt="""" width=""70"" height=""70"" border=""0"" />
<h2 class=""sub-heading-puff color-06c padding-bottom-5""> <a href='/tol/travel/destinations/usa/article3817494.ece' class=""link-06c"">Six of the best US holidays</a></h2>
<!-- Display Teaser text -->
<p class=""small"">If planning the trip of a lifetime Stateside, where would six Times writers and US specialists go?</p>
</div>
</div>
</form>	
<!-- attached links -->
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<h3 class=""section-heading"">Related Internet Links</h3>
<ul class=""chevron-list chevron-blue"">
<li><a href=""http://www.timesonline.co.uk/tol/travel.do?region=North+America&country=USA&mapClicked=travelmap&sectionId=1045&publicationId=17"" class=""link-666"">Read more than 500 USA travel articles</a></li>	
</ul>
</div>
</div>
<!-- end attached links -->
<!-- END: Module - M63 - Article Related Package -->
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<h3 class=""section-heading"">Related Links</h3>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<ul class=""chevron-list chevron-blue"">
<li><a href='/tol/travel/destinations/usa/article4442943.ece' class=""link-666"">
Obama v McCain fever in Washington
</a></li>
</ul>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
</div>
</div>
<!-- BEGIN: POLL -->
<!--This block will execute if an article of type Poll is attached-->	
<!-- END : POLL -->
<!-- BEGIN: DEBATE-->
<!-- END: DEBATE-->
<div class=""clear related-attachements-bottom""></div>
<div class=""padding-top-5""></div>
</div>
<!-- END: Module - M63 - Article Related Attachements -->
<!-- Call Wide Article Attachment Module -->
<!--TEMPLATE:call file=""wideArticleAttachment.jsp"" /-->	
<p>
Trouble had been brewing all morning. There had been heated words outside the 
Bird Cage Theatre. Threats had been exchanged at Big Nose Kate&rsquo;s. Now, Wyatt 
Earp and his brothers, Virgil and Morgan, with old friend Doc Holliday, were 
advancing down Main Street in spangly waistcoats. The Clan-ton gang had been 
spotted at the OK Corral. A crowd drifted after them. &ldquo;There&rsquo;s going to be 
shooting,&rdquo; a fat man from Ohio helpfully informed his fat wife. 
</p>
<p>
To some people, this was the Gunfight at the OK Corral, in which the Clantons 
are gunned down in a hail of blanks every day at 2pm. To others, it is part 
of America&rsquo;s political feud. The Earp brothers were the hired guns of the 
Republican hierarchy, the owners of the big mining and cattle operations who 
wanted to make Tombstone &ldquo;safe for investment&rdquo;. The Clantons, humble cowboys 
who wanted to preserve a bit of economic space for freelance prospectors and 
small independent ranchers, were aligned with the Democrats. You can see how 
much has changed in 127 years. 
</p>
<p>
I didn&rsquo;t wait for the final body count. I was a man on a mission. I climbed 
back into the hire car, a big beast appropriately named Bronco, and spun 
away down an empty highway. Telegraph poles flashed by. Yellow hills rolled 
into empty distance. On the radio, crackling with static, a man was singing, 
&ldquo;All my exes live in Texas, and that&rsquo;s why I hang my hat in Tennessee.&rdquo;
</p>
<p>
In Douglas I stopped for a coffee at the Gadsen Hotel. It is the town&rsquo;s only 
moment of glamour, built a century ago for cattle barons. In the lobby, a 
grand double staircase rises past a vast spread of Tiffany stained glass. 
One night, a drunken Pancho Villa is said to have ridden his horse up these 
stairs, firing into the ceiling as he went. You can still see the chipped 
marble on the seventh step. 
</p>
<p>
Pancho probably didn&rsquo;t read English &ndash; which explains why he missed the sign on 
the door of the Saddle &amp; Spur lobby bar: &ldquo;No firearms or weapons of any 
kind&rdquo;. The bar was empty but for the barman, who had fallen asleep in front 
of a &ldquo;wanted&rdquo; poster. On the television in the corner, people were waving 
flags and banners while John McCain, the Arizona senator, was giving them 
the thumbs up. I turned the set off and stepped next door into the diner for 
&ldquo;All the coffee you can drink &ndash; one dollar&rdquo;. 
</p>
<p>
BACK IN the Bronco, I followed Highway 80. It ran like a drawn line through 
the empty grasslands of the San Bernardino Valley. In an hour&rsquo;s driving I 
saw two other cars, neither of them sporting political bumper stickers. From 
time to time, distant homesteads appeared, set back a mile or so from the 
road, tucked into folds in the long, yellow hills. It was remote country. My 
mobile couldn&rsquo;t get a signal. Hopefully, I was beyond the reach of Fox News 
as well. 
</p>
<p>
Price Canyon Ranch lies at the end of a long dirt road in the foothills of the 
Chiricahua Mountains. It was just the kind of place I was looking for. I 
wanted a real working ranch, not a dude ranch with an infinity pool and a 
spa and a yoga class. I wanted somewhere that felt like the West, somewhere 
comfortable but rustic, not a citified luxury resort where you expected to 
find the horses on the sun loungers, sipping martinis. 
</p>
<p>
Price Canyon has 10 guest rooms elegantly decorated in western style with 
hardwood floors and Navajo rugs. One of the old barns has been beautifully 
converted into a large central lounge with a stone fireplace, deep leather 
sofas, a library of western books and a dining area where meals are produced 
by the wonderful Fred Tullis, a painter-turned-chef. If food is the heart of 
a home, Fred and his generous country meals are the heart and soul of Price 
Canyon."
"8868","
Stanley Stewart 
</span>
<div class=""clear""></div>
</div>
</div> 
<!-- END: Module - M24 Article Headline with landscape image (d) -->
<!-- Article Copy module -->
<!-- BEGIN: Module - Main Article -->
<!-- Check the Article Type and display accordingly-->
<!-- Print Author image associated with the Author-->
<!-- Print the body of the article-->
<style type=""text/css"">
div#related-article-links p a, div#related-article-links p a:visited {
color:#06c;
} 
</style>	
<div id=""related-article-links"">
<!-- Pagination -->
<!--Display article with page breaks -->
<p>
I<b> </b>had only been in Tombstone half an hour when the shooting started. 
Apparently, it was the Republicans and the Democrats at it again. 
</p>
<p>
This was just the kind of thing I had come to Arizona to escape. 
</p>
<p>
Like most people in this year of interminable electioneering, I had had enough 
of politicking, of spin and soundbites, of poll figures and attack ads. 
</p>
<p>
I was heading deep into the southeastern corner of the state in the hope of 
escape. I was looking for a quieter America, for a ranch somewhere with 
wide, empty landscapes, and cowboys with few opinions beyond yup and nope. 
When I stopped for lunch in Tombstone, I hadn&rsquo;t expected to be detained by a 
gunfight on Main Street. 
</p>
<!--#include file=""m63-article-related-attachements.html""-->
<!-- BEGIN: Module - M63 - Article Related Attachements -->
<script type=""text/javascript"">
<!--
function pictureGalleryPopup(pubUrl,articleId) {
var newWin = window.open(pubUrl+'template/2.0-0/element/pictureGalleryPopup.jsp?id='+articleId+'&&offset=0&&sectionName=DestinationsUSA','mywindow','menubar=0,resizable=0,width=1000,height=711');
}
//-->
</script>
<!-- BEGIN: Comment Teaser Module -->
<div class=""float-left related-attachements-container"">
<!-- END: Comment Teaser Module -->	
<!-- BEGIN: Module - M63 - Article Related Package -->
<form name=""packageArticle"" method=""post"" action="""">	
<div class=""related-attachements-top padding-top-10""></div>
<div class=""related-attachements-side padding-top-7 padding-bottom-10 padding-right-7"">
<div class=""padding-bottom-5 padding-top-3"">
<img src=""/multimedia/archive/00362/indian-385_362724b.jpg"" alt="""" width=""70"" height=""70"" border=""0"" />
<h2 class=""sub-heading-puff color-06c padding-bottom-5""> <a href='/tol/travel/destinations/usa/article4268164.ece' class=""link-06c"">Going native in the USA</a></h2>
<!-- Display Teaser text -->
<p class=""small"">Oregon&rsquo;s annual Nez Perce Indian gathering is a Native American ritual, a pilgrimage and a knees-up</p>
</div>
</div>
</form>	
<form name=""packageArticle"" method=""post"" action="""">	
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<img src=""/multimedia/archive/00377/ST_Travel_377559b.jpg"" alt="""" width=""70"" height=""70"" border=""0"" />
<h2 class=""sub-heading-puff color-06c padding-bottom-5""> <a href='/tol/travel/destinations/usa/article4442943.ece' class=""link-06c"">Obama v McCain fever in Washington</a></h2>
<!-- Display Teaser text -->
<p class=""small"">It&rsquo;s an election battle with global implications, and the front line is Washington, DC. Rob Ryan is in the thick of it</p>
</div>
</div>
</form>	
<form name=""packageArticle"" method=""post"" action="""">	
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<img src=""/multimedia/archive/00322/Travel__322354b.jpg"" alt="""" width=""70"" height=""70"" border=""0"" />
<h2 class=""sub-heading-puff color-06c padding-bottom-5""> <a href='/tol/travel/destinations/usa/article3817494.ece' class=""link-06c"">Six of the best US holidays</a></h2>
<!-- Display Teaser text -->
<p class=""small"">If planning the trip of a lifetime Stateside, where would six Times writers and US specialists go?</p>
</div>
</div>
</form>	
<!-- attached links -->
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<h3 class=""section-heading"">Related Internet Links</h3>
<ul class=""chevron-list chevron-blue"">
<li><a href=""http://www.timesonline.co.uk/tol/travel.do?region=North+America&country=USA&mapClicked=travelmap&sectionId=1045&publicationId=17"" class=""link-666"">Read more than 500 USA travel articles</a></li>	
</ul>
</div>
</div>
<!-- end attached links -->
<!-- END: Module - M63 - Article Related Package -->
<div class=""puff-top"">
<div class=""related-attachements-side padding-top-10 padding-bottom-10 padding-right-7"">
<h3 class=""section-heading"">Related Links</h3>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
<ul class=""chevron-list chevron-blue"">
<li><a href='/tol/travel/destinations/usa/article4442943.ece' class=""link-666"">
Obama v McCain fever in Washington
</a></li>
</ul>
<form name=""relatedLinksform"" action="""" method=""post"">
</form>
</div>
</div>
<!-- BEGIN: POLL -->
<!--This block will execute if an article of type Poll is attached-->	
<!-- END : POLL -->
<!-- BEGIN: DEBATE-->
<!-- END: DEBATE-->
<div class=""clear related-attachements-bottom""></div>
<div class=""padding-top-5""></div>
</div>
<!-- END: Module - M63 - Article Related Attachements -->
<!-- Call Wide Article Attachment Module -->
<!--TEMPLATE:call file=""wideArticleAttachment.jsp"" /-->	
<p>
Trouble had been brewing all morning. There had been heated words outside the 
Bird Cage Theatre. Threats had been exchanged at Big Nose Kate&rsquo;s. Now, Wyatt 
Earp and his brothers, Virgil and Morgan, with old friend Doc Holliday, were 
advancing down Main Street in spangly waistcoats. The Clan-ton gang had been 
spotted at the OK Corral. A crowd drifted after them. &ldquo;There&rsquo;s going to be 
shooting,&rdquo; a fat man from Ohio helpfully informed his fat wife. 
</p>
<p>
To some people, this was the Gunfight at the OK Corral, in which the Clantons 
are gunned down in a hail of blanks every day at 2pm. To others, it is part 
of America&rsquo;s political feud. The Earp brothers were the hired guns of the 
Republican hierarchy, the owners of the big mining and cattle operations who 
wanted to make Tombstone &ldquo;safe for investment&rdquo;. The Clantons, humble cowboys 
who wanted to preserve a bit of economic space for freelance prospectors and 
small independent ranchers, were aligned with the Democrats. You can see how 
much has changed in 127 years. 
</p>
<p>
I didn&rsquo;t wait for the final body count. I was a man on a mission. I climbed 
back into the hire car, a big beast appropriately named Bronco, and spun 
away down an empty highway. Telegraph poles flashed by. Yellow hills rolled 
into empty distance. On the radio, crackling with static, a man was singing, 
&ldquo;All my exes live in Texas, and that&rsquo;s why I hang my hat in Tennessee.&rdquo;
</p>
<p>
In Douglas I stopped for a coffee at the Gadsen Hotel. It is the town&rsquo;s only 
moment of glamour, built a century ago for cattle barons. In the lobby, a 
grand double staircase rises past a vast spread of Tiffany stained glass. 
One night, a drunken Pancho Villa is said to have ridden his horse up these 
stairs, firing into the ceiling as he went. You can still see the chipped 
marble on the seventh step. 
</p>
<p>
Pancho probably didn&rsquo;t read English &ndash; which explains why he missed the sign on 
the door of the Saddle &amp; Spur lobby bar: &ldquo;No firearms or weapons of any 
kind&rdquo;. The bar was empty but for the barman, who had fallen asleep in front 
of a &ldquo;wanted&rdquo; poster. On the television in the corner, people were waving 
flags and banners while John McCain, the Arizona senator, was giving them 
the thumbs up. I turned the set off and stepped next door into the diner for 
&ldquo;All the coffee you can drink &ndash; one dollar&rdquo;. 
</p>
<p>
BACK IN the Bronco, I followed Highway 80. It ran like a drawn line through 
the empty grasslands of the San Bernardino Valley. In an hour&rsquo;s driving I 
saw two other cars, neither of them sporting political bumper stickers. From 
time to time, distant homesteads appeared, set back a mile or so from the 
road, tucked into folds in the long, yellow hills. It was remote country. My 
mobile couldn&rsquo;t get a signal. Hopefully, I was beyond the reach of Fox News 
as well. 
</p>
<p>
Price Canyon Ranch lies at the end of a long dirt road in the foothills of the 
Chiricahua Mountains. It was just the kind of place I was looking for. I 
wanted a real working ranch, not a dude ranch with an infinity pool and a 
spa and a yoga class. I wanted somewhere that felt like the West, somewhere 
comfortable but rustic, not a citified luxury resort where you expected to 
find the horses on the sun loungers, sipping martinis. 
</p>
<p>
Price Canyon has 10 guest rooms elegantly decorated in western style with 
hardwood floors and Navajo rugs. One of the old barns has been beautifully 
converted into a large central lounge with a stone fireplace, deep leather 
sofas, a library of western books and a dining area where meals are produced 
by the wonderful Fred Tullis, a painter-turned-chef. If food is the heart of 
a home, Fred and his generous country meals are the heart and soul of Price 
Canyon.