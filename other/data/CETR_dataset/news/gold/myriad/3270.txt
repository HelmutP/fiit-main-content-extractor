"TSX, Dow Jones make huge triple-digit gains</h3>



<p class=""timeStamp"">Updated Tue. Oct. 28 2008 4:27 PM ET</p>

<p class=""storyAttributes"">CTV.ca News Staff</p>





<p>The Toronto Stock Exchange finished up 614 points Monday, while the Dow Jones soared nearly 890 points, largely driven by bargain hunters and a possible interest rate cut by the U.S. Federal Reserve. </p>
<p>After the closing bell rang, the S&amp;P/TSX composite index was up 614.29 points, or 7.2 per cent, to finish at 9,151.63 points. The index had plummeted more than eight per cent the previous day. </p>
<p>In New York, the Dow Jones industrial average was up an impressive 889.67 points to close at 9,065.44. </p>
<p>""That's up more than 10 per cent, the second-biggest percentage-point gain in Wall Street history,"" BNN's Michael Hainsworth told CTV Newsnet. ""Why? Bargain hunting. Investors were buying some of those beaten-down blue chip stocks."" </p>
<p>Meanwhile, the U.S. central bank is expected to announce another interest rate cut Wednesday. Earlier this month, it already dropped its target rate to 1.5 per cent from 2 per cent. </p>
<p>The Canadian dollar was up slightly at 78 cents US from 77.59 cents US on Monday. Those low levels haven't been seen in about four years, as the loonie is held down by falling commodity prices and a stronger U.S. greenback. </p>
<p>The early morning North American surge came after investors made solid gains in overseas markets. </p>
<p>In London, Britain's FTSE 100 rose 2.61 per cent, Germany's DAX index jumped 11.28 per cent, and France's CAC-40 rose 1.38 per cent. </p>
<p>Earlier, most Asian stock markets made gains after suffering steep declines in recent days. </p>
<p>Hong Kong's Hang Seng index rose 14.4 per cent -- its biggest gain in 11 years -- to 12,596.29. On Monday, the Hang Seng plunged more than 12 per cent. </p>
<p>South Korea's Kospi jumped 5.6 per cent, helped along by the South Korean central bank's interest rate cut on Monday. </p>
<p>In Japan, the benchmark Nikkei 225 index surged 459.02 points, or 6.4 per cent, to 7,621.92 after closing Monday at its lowest point in 26 years."
"3270","TSX, Dow Jones make huge triple-digit gains</h3>



<p class=""timeStamp"">Updated Tue. Oct. 28 2008 4:27 PM ET</p>

<p class=""storyAttributes"">CTV.ca News Staff</p>





<p>The Toronto Stock Exchange finished up 614 points Monday, while the Dow Jones soared nearly 890 points, largely driven by bargain hunters and a possible interest rate cut by the U.S. Federal Reserve. </p>
<p>After the closing bell rang, the S&amp;P/TSX composite index was up 614.29 points, or 7.2 per cent, to finish at 9,151.63 points. The index had plummeted more than eight per cent the previous day. </p>
<p>In New York, the Dow Jones industrial average was up an impressive 889.67 points to close at 9,065.44. </p>
<p>""That's up more than 10 per cent, the second-biggest percentage-point gain in Wall Street history,"" BNN's Michael Hainsworth told CTV Newsnet. ""Why? Bargain hunting. Investors were buying some of those beaten-down blue chip stocks."" </p>
<p>Meanwhile, the U.S. central bank is expected to announce another interest rate cut Wednesday. Earlier this month, it already dropped its target rate to 1.5 per cent from 2 per cent. </p>
<p>The Canadian dollar was up slightly at 78 cents US from 77.59 cents US on Monday. Those low levels haven't been seen in about four years, as the loonie is held down by falling commodity prices and a stronger U.S. greenback. </p>
<p>The early morning North American surge came after investors made solid gains in overseas markets. </p>
<p>In London, Britain's FTSE 100 rose 2.61 per cent, Germany's DAX index jumped 11.28 per cent, and France's CAC-40 rose 1.38 per cent. </p>
<p>Earlier, most Asian stock markets made gains after suffering steep declines in recent days. </p>
<p>Hong Kong's Hang Seng index rose 14.4 per cent -- its biggest gain in 11 years -- to 12,596.29. On Monday, the Hang Seng plunged more than 12 per cent. </p>
<p>South Korea's Kospi jumped 5.6 per cent, helped along by the South Korean central bank's interest rate cut on Monday. </p>
<p>In Japan, the benchmark Nikkei 225 index surged 459.02 points, or 6.4 per cent, to 7,621.92 after closing Monday at its lowest point in 26 years. </p>
<p><em>With files from The Canadian Press"
"3270","CTV.ca News Staff</p>





<p>The Toronto Stock Exchange finished up 614 points Monday, while the Dow Jones soared nearly 890 points, largely driven by bargain hunters and a possible interest rate cut by the U.S. Federal Reserve. </p>
<p>After the closing bell rang, the S&amp;P/TSX composite index was up 614.29 points, or 7.2 per cent, to finish at 9,151.63 points. The index had plummeted more than eight per cent the previous day. </p>
<p>In New York, the Dow Jones industrial average was up an impressive 889.67 points to close at 9,065.44. </p>
<p>""That's up more than 10 per cent, the second-biggest percentage-point gain in Wall Street history,"" BNN's Michael Hainsworth told CTV Newsnet. ""Why? Bargain hunting. Investors were buying some of those beaten-down blue chip stocks."" </p>
<p>Meanwhile, the U.S. central bank is expected to announce another interest rate cut Wednesday. Earlier this month, it already dropped its target rate to 1.5 per cent from 2 per cent. </p>
<p>The Canadian dollar was up slightly at 78 cents US from 77.59 cents US on Monday. Those low levels haven't been seen in about four years, as the loonie is held down by falling commodity prices and a stronger U.S. greenback. </p>
<p>The early morning North American surge came after investors made solid gains in overseas markets. </p>
<p>In London, Britain's FTSE 100 rose 2.61 per cent, Germany's DAX index jumped 11.28 per cent, and France's CAC-40 rose 1.38 per cent. </p>
<p>Earlier, most Asian stock markets made gains after suffering steep declines in recent days. </p>
<p>Hong Kong's Hang Seng index rose 14.4 per cent -- its biggest gain in 11 years -- to 12,596.29. On Monday, the Hang Seng plunged more than 12 per cent. </p>
<p>South Korea's Kospi jumped 5.6 per cent, helped along by the South Korean central bank's interest rate cut on Monday. </p>
<p>In Japan, the benchmark Nikkei 225 index surged 459.02 points, or 6.4 per cent, to 7,621.92 after closing Monday at its lowest point in 26 years."
"3270","CTV.ca News Staff</p>





<p>The Toronto Stock Exchange finished up 614 points Monday, while the Dow Jones soared nearly 890 points, largely driven by bargain hunters and a possible interest rate cut by the U.S. Federal Reserve. </p>
<p>After the closing bell rang, the S&amp;P/TSX composite index was up 614.29 points, or 7.2 per cent, to finish at 9,151.63 points. The index had plummeted more than eight per cent the previous day. </p>
<p>In New York, the Dow Jones industrial average was up an impressive 889.67 points to close at 9,065.44. </p>
<p>""That's up more than 10 per cent, the second-biggest percentage-point gain in Wall Street history,"" BNN's Michael Hainsworth told CTV Newsnet. ""Why? Bargain hunting. Investors were buying some of those beaten-down blue chip stocks."" </p>
<p>Meanwhile, the U.S. central bank is expected to announce another interest rate cut Wednesday. Earlier this month, it already dropped its target rate to 1.5 per cent from 2 per cent. </p>
<p>The Canadian dollar was up slightly at 78 cents US from 77.59 cents US on Monday. Those low levels haven't been seen in about four years, as the loonie is held down by falling commodity prices and a stronger U.S. greenback. </p>
<p>The early morning North American surge came after investors made solid gains in overseas markets. </p>
<p>In London, Britain's FTSE 100 rose 2.61 per cent, Germany's DAX index jumped 11.28 per cent, and France's CAC-40 rose 1.38 per cent. </p>
<p>Earlier, most Asian stock markets made gains after suffering steep declines in recent days. </p>
<p>Hong Kong's Hang Seng index rose 14.4 per cent -- its biggest gain in 11 years -- to 12,596.29. On Monday, the Hang Seng plunged more than 12 per cent. </p>
<p>South Korea's Kospi jumped 5.6 per cent, helped along by the South Korean central bank's interest rate cut on Monday. </p>
<p>In Japan, the benchmark Nikkei 225 index surged 459.02 points, or 6.4 per cent, to 7,621.92 after closing Monday at its lowest point in 26 years. </p>
<p><em>With files from The Canadian Press"
"3270","The Toronto Stock Exchange finished up 614 points Monday, while the Dow Jones soared nearly 890 points, largely driven by bargain hunters and a possible interest rate cut by the U.S. Federal Reserve. </p>
<p>After the closing bell rang, the S&amp;P/TSX composite index was up 614.29 points, or 7.2 per cent, to finish at 9,151.63 points. The index had plummeted more than eight per cent the previous day. </p>
<p>In New York, the Dow Jones industrial average was up an impressive 889.67 points to close at 9,065.44. </p>
<p>""That's up more than 10 per cent, the second-biggest percentage-point gain in Wall Street history,"" BNN's Michael Hainsworth told CTV Newsnet. ""Why? Bargain hunting. Investors were buying some of those beaten-down blue chip stocks."" </p>
<p>Meanwhile, the U.S. central bank is expected to announce another interest rate cut Wednesday. Earlier this month, it already dropped its target rate to 1.5 per cent from 2 per cent. </p>
<p>The Canadian dollar was up slightly at 78 cents US from 77.59 cents US on Monday. Those low levels haven't been seen in about four years, as the loonie is held down by falling commodity prices and a stronger U.S. greenback. </p>
<p>The early morning North American surge came after investors made solid gains in overseas markets. </p>
<p>In London, Britain's FTSE 100 rose 2.61 per cent, Germany's DAX index jumped 11.28 per cent, and France's CAC-40 rose 1.38 per cent. </p>
<p>Earlier, most Asian stock markets made gains after suffering steep declines in recent days. </p>
<p>Hong Kong's Hang Seng index rose 14.4 per cent -- its biggest gain in 11 years -- to 12,596.29. On Monday, the Hang Seng plunged more than 12 per cent. </p>
<p>South Korea's Kospi jumped 5.6 per cent, helped along by the South Korean central bank's interest rate cut on Monday. </p>
<p>In Japan, the benchmark Nikkei 225 index surged 459.02 points, or 6.4 per cent, to 7,621.92 after closing Monday at its lowest point in 26 years. </p>
<p><em>With files from The Canadian Press