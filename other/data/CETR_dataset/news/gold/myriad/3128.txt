"Retired unionists:<BR>fair go for pensioners</span><BR><BR>
<IMG SRC=""../pics08/1387-pens.jpg""><BR><BR>
<span class=""ip"">Australian Manufacturing Workers? Union (AMWU) retired members have welcomed the federal government?s $4.8 billion payments to pensioners and called for an increase of the pension basic rate next year.</span><BR><BR>

A delegation from the AMWU Retired Members? Division (RMD) and the Fair Go for Pensioners coalition were in Canberra last week lobbying federal ministers for a better deal for pensioners.<BR><BR>

Victorian Secretary of the RMD, Jack Brown, said that while everyone was pleased with the government?s announcement to pay single pensioners $1,400 and couples $2,100, the RMD would keep up its demand for a general increase as part of long-term reform. <BR><BR>

""Older people on fixed incomes are really struggling with the cost of living which is rising drastically and our trip to Canberra is part of our long campaign for a better standard of living for all older people.""<BR><BR>

The Fair Go For Pensioners coalition, of which the AMWU RMD is a founding member, has called for an increase in the pension from 25% to 35% of the average male weekly earnings.<BR><BR>

""We are concerned about the effects of the global financial crisis, and we have been asking members of parliament to help protect those most vulnerable. We are pleased to see that the government has listened and its response has been very well received,"" said Mr Brown.<BR><BR>

Mr Brown said there were other important issues that needed addressing besides an increase in the pension.<BR><BR>

""We need access to improved health care and there must be an increase to the level of funding for aged care services.<BR><BR>

""These payments will provide some relief for the rising cost of living, but more action is needed. We are confident that the government has also heard our message on the need for a general increase as part of permanent reform and we will maintain our campaign until the Harmer government inquiry into pensions makes its recommendations to the government.""<BR><BR>

The Coalition will be holding a series of rallies across Australia starting in Adelaide on October 21 to raise awareness of the plight of older people.