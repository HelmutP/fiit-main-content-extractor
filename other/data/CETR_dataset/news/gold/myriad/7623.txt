"Red revolution</h1>
        <div class=""ArticleDate""><em>24 October 2008, 07:50</em></div>
        <div class=""ArticleMPUHolder"">
          <h5 class=""ArticleViewRelatedHeader"">Related Articles</h5>
          <div class=""RelatedArticleFeatureLinks"">
            <ul>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081023111603535C655652"" title=""Scolari wary of Liverpool threat"">Scolari wary of Liverpool threat</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081023070143171C789778"" title=""How the mighty have fallen"">How the mighty have fallen</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081018154231392C912276"" title=""Five-star Chelsea stay top"">Five-star Chelsea stay top</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081017071207520C531526"" title=""Torres injury blow for Liverpool"">Torres injury blow for Liverpool</a></li>
            </ul>
          </div>
          <div class=""SquareAdHolder""><script type=""text/javascript"">DisplayAds('MPUAV');</script></div>
        </div>
        <div class=""ArticleContent"">
	  The growing belief that Liverpool are equipped to end their long wait for a Premier League title faces its sternest examination so far when they meet a flying Chelsea side at Stamford Bridge on Sunday. <br />
<br />
Chelsea are top on goal difference from Rafael Benitez's side, although the emphatic nature of their recent performances in the league contrasts sharply with Liverpool's. <br />
<br />
Last weekend, Liverpool scrambled a 3-2 victory at home to Wigan Athletic after trailing twice while in their previous game they battled back from 2-0 down to beat Manchester City. Give Chelsea a lead and they are unlikely to hand out second chances. <br />
<br />
Luiz Felipe Scolari has barely put a foot wrong since taking over at Stamford Bridge from the dour Avram Grant. The Brazilian sent a patched-up side out at Middlesbrough last weekend and they came home with a resounding 5-0 victory. <br />
<br />
They have scored 19 goals and conceded just three in eight matches and where Jose Mourinho's title-winning teams were powerful and attritional, Scolari's version is playing with a swagger and no little panache. <br />
<br />
Benitez has been criticised in the past for a poor Premier League points return against the big three of Chelsea, Manchester United and Arsenal, although this season's 2-1 defeat of United at Anfield was thoroughly deserved.  <br />
<br />
The Spaniard twice got the better of Mourinho's Chelsea in the Champions League but last season the sides met five times, including another two-legged European semifinal, and Liverpool failed to record a victory. <br />
<br />
However, Scolari identified this week as Chelsea's toughest of the season so far with Liverpool's visit following so closely after Wednesday's 1-0 Champions League win over AS Roma. <br />
<br />
Liverpool will badly miss striker Fernando Torres, absent from their 1-1 draw at Atletico Madrid with a hamstring injury, and the emphasis will be on Robbie Keane and Dirk Kuyt to try to end Chelsea's four and a half year unbeaten home league record. <br />
<br />
However, the belief that even without Torres they are now real contenders is shared by Luis Garcia, whose goal helped Liverpool beat Chelsea in the 2005 Champions League semifinal. <br />
<br />
""I have watched and you can see the team growing. If they continue in this way they could win the league,"" Garcia, now with Atletico, told Liverpool's website. <br />
<br />
""When I first joined, Chelsea were far better than us, but the gap is closing and Liverpool can now win the league."" <br />
<br />
Fourth-placed Arsenal, fresh from romping to 5-2 win at Fenerbahce on Tuesday, visit West Ham United on Sunday. <br />
<br />
Manchester United, who are hitting their stride after a sluggish start and are fifth with a game in hand, travel to struggling Everton on Saturday. Third-placed Hull City aim to continue their dream start at West Bromwich Albion."
"7623","Red revolution</h1>
        <div class=""ArticleDate""><em>24 October 2008, 07:50</em></div>
        <div class=""ArticleMPUHolder"">
          <h5 class=""ArticleViewRelatedHeader"">Related Articles</h5>
          <div class=""RelatedArticleFeatureLinks"">
            <ul>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081023111603535C655652"" title=""Scolari wary of Liverpool threat"">Scolari wary of Liverpool threat</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081023070143171C789778"" title=""How the mighty have fallen"">How the mighty have fallen</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081018154231392C912276"" title=""Five-star Chelsea stay top"">Five-star Chelsea stay top</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081017071207520C531526"" title=""Torres injury blow for Liverpool"">Torres injury blow for Liverpool</a></li>
            </ul>
          </div>
          <div class=""SquareAdHolder""><script type=""text/javascript"">DisplayAds('MPUAV');</script></div>
        </div>
        <div class=""ArticleContent"">
	  The growing belief that Liverpool are equipped to end their long wait for a Premier League title faces its sternest examination so far when they meet a flying Chelsea side at Stamford Bridge on Sunday. <br />
<br />
Chelsea are top on goal difference from Rafael Benitez's side, although the emphatic nature of their recent performances in the league contrasts sharply with Liverpool's. <br />
<br />
Last weekend, Liverpool scrambled a 3-2 victory at home to Wigan Athletic after trailing twice while in their previous game they battled back from 2-0 down to beat Manchester City. Give Chelsea a lead and they are unlikely to hand out second chances. <br />
<br />
Luiz Felipe Scolari has barely put a foot wrong since taking over at Stamford Bridge from the dour Avram Grant. The Brazilian sent a patched-up side out at Middlesbrough last weekend and they came home with a resounding 5-0 victory. <br />
<br />
They have scored 19 goals and conceded just three in eight matches and where Jose Mourinho's title-winning teams were powerful and attritional, Scolari's version is playing with a swagger and no little panache. <br />
<br />
Benitez has been criticised in the past for a poor Premier League points return against the big three of Chelsea, Manchester United and Arsenal, although this season's 2-1 defeat of United at Anfield was thoroughly deserved.  <br />
<br />
The Spaniard twice got the better of Mourinho's Chelsea in the Champions League but last season the sides met five times, including another two-legged European semifinal, and Liverpool failed to record a victory. <br />
<br />
However, Scolari identified this week as Chelsea's toughest of the season so far with Liverpool's visit following so closely after Wednesday's 1-0 Champions League win over AS Roma. <br />
<br />
Liverpool will badly miss striker Fernando Torres, absent from their 1-1 draw at Atletico Madrid with a hamstring injury, and the emphasis will be on Robbie Keane and Dirk Kuyt to try to end Chelsea's four and a half year unbeaten home league record. <br />
<br />
However, the belief that even without Torres they are now real contenders is shared by Luis Garcia, whose goal helped Liverpool beat Chelsea in the 2005 Champions League semifinal. <br />
<br />
""I have watched and you can see the team growing. If they continue in this way they could win the league,"" Garcia, now with Atletico, told Liverpool's website. <br />
<br />
""When I first joined, Chelsea were far better than us, but the gap is closing and Liverpool can now win the league."" <br />
<br />
Fourth-placed Arsenal, fresh from romping to 5-2 win at Fenerbahce on Tuesday, visit West Ham United on Sunday. <br />
<br />
Manchester United, who are hitting their stride after a sluggish start and are fifth with a game in hand, travel to struggling Everton on Saturday. Third-placed Hull City aim to continue their dream start at West Bromwich Albion.<br />
<br />
<br><br><ul><li>This article was originally published on page 30 of <a href='http://www.thestar.co.za/index.php?fArticleId=4676435' target='_blank'> The Star</a> on October 24, 2008"
"7623","
	  The growing belief that Liverpool are equipped to end their long wait for a Premier League title faces its sternest examination so far when they meet a flying Chelsea side at Stamford Bridge on Sunday. <br />
<br />
Chelsea are top on goal difference from Rafael Benitez's side, although the emphatic nature of their recent performances in the league contrasts sharply with Liverpool's. <br />
<br />
Last weekend, Liverpool scrambled a 3-2 victory at home to Wigan Athletic after trailing twice while in their previous game they battled back from 2-0 down to beat Manchester City. Give Chelsea a lead and they are unlikely to hand out second chances. <br />
<br />
Luiz Felipe Scolari has barely put a foot wrong since taking over at Stamford Bridge from the dour Avram Grant. The Brazilian sent a patched-up side out at Middlesbrough last weekend and they came home with a resounding 5-0 victory. <br />
<br />
They have scored 19 goals and conceded just three in eight matches and where Jose Mourinho's title-winning teams were powerful and attritional, Scolari's version is playing with a swagger and no little panache. <br />
<br />
Benitez has been criticised in the past for a poor Premier League points return against the big three of Chelsea, Manchester United and Arsenal, although this season's 2-1 defeat of United at Anfield was thoroughly deserved.  <br />
<br />
The Spaniard twice got the better of Mourinho's Chelsea in the Champions League but last season the sides met five times, including another two-legged European semifinal, and Liverpool failed to record a victory. <br />
<br />
However, Scolari identified this week as Chelsea's toughest of the season so far with Liverpool's visit following so closely after Wednesday's 1-0 Champions League win over AS Roma. <br />
<br />
Liverpool will badly miss striker Fernando Torres, absent from their 1-1 draw at Atletico Madrid with a hamstring injury, and the emphasis will be on Robbie Keane and Dirk Kuyt to try to end Chelsea's four and a half year unbeaten home league record. <br />
<br />
However, the belief that even without Torres they are now real contenders is shared by Luis Garcia, whose goal helped Liverpool beat Chelsea in the 2005 Champions League semifinal. <br />
<br />
""I have watched and you can see the team growing. If they continue in this way they could win the league,"" Garcia, now with Atletico, told Liverpool's website. <br />
<br />
""When I first joined, Chelsea were far better than us, but the gap is closing and Liverpool can now win the league."" <br />
<br />
Fourth-placed Arsenal, fresh from romping to 5-2 win at Fenerbahce on Tuesday, visit West Ham United on Sunday. <br />
<br />
Manchester United, who are hitting their stride after a sluggish start and are fifth with a game in hand, travel to struggling Everton on Saturday. Third-placed Hull City aim to continue their dream start at West Bromwich Albion.<br />
<br />
<br><br><ul><li>This article was originally published on page 30 of <a href='http://www.thestar.co.za/index.php?fArticleId=4676435' target='_blank'> The Star</a> on October 24, 2008