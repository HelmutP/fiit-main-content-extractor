"CECH HOMES IN</h1>
	
					
		<!-- MAIN IMAGE, IT'S CAPTION AND THE GALLERY BIT -->
		<div style=""float: left; width: 281px; margin: 0 10px 10px 0;"">
		
			<div style=""float: left; width: 281px; height: 351px; background: url(http://images.dailystar-uk.co.uk/dynamic/58/281x351/56427_1.jpg); background-repeat: no-repeat;"" title=""Cech wants his Chelsea teammates to do it again"">
			
							
			</div>
			
						<div  class=""articleFirstImageCaption"" style=""width: 261px; float: left;"">
				ABOVE: Cech wants his Chelsea teammates to do it again			</div>
						
					
		</div>
		
			
					
			<div style=""positin: absolute; width: 175px; overflow: hidden;"">
			<div class=""left date"" style=""width:177px; height: 20px;"">
				28th October 2008			</div>
			<div class=""left"" style=""width:177px; height: auto; border-bottom: solid 1px #000; margin: 0 0 5px 0;"">
				
								<h4 class=""left author"" style=""font-size: 12px;"">By <span class=""bold"">Paul Brown </span></h4>
								
							</div>
			
						<div class=""left"" style=""width:177px; height: 30px"">				<p>
					<a href=""#comments"" class=""speechBubble"">Your Shout  ( <span class=""bigCommentsNum"" id=""commentscountertop"">0</span> )</a>
				</p>
			</div>
						</div>


		<p class=""introcopy"">PETR CECH watched Chelsea?s record run bite the dust and then told his team-mates: Let?s do it again!</p>
		<br />
	
	
	
			
		<style>
			#bodycopy a { font-size: 12px; }
		</style>
	
		<p id=""bodycopy"">The Blues went 86 home league games without defeat before Liverpool ended their proud record at fortress Stamford Bridge.<br>                    <br>                                          Cech reckons it?s a mark that may never be beaten ? but he is determined to make sure Chelsea give it a go.<br>                     <br>                                          The keeper said: ?Defeat happens in football and you have to forget about it and start again.<br>                     <br>                                          ?It has been a great run, 86 games, I don?t think someone can really come close to that in the future because it?s a fantastic run.<br>                     <br>                                          ?But now it is all behind us and we have to think about the game on Wednesday when we go to Hull.<br>                     <br>                                          ?We have the same number of points and we have to bounce back with a victory.<br>                     <br>                                          ?We say that with something ending, something new begins, so we hope that is the case here.? Cech admits Liverpool have made giant strides this season and are genuine contenders for the Premier League title.<br>                     <br>                                          But he claims Chelsea will get their revenge when the two teams meet at Anfield on January 31.<br>                     <br>                                          Cech said: ?It is a big leap for them. They have three points from Stamford Bridge.<br>                    <br>                                          ?They are top of the league now and at the beginning of the season they didn?t play that well but they were getting results.<br>                     <br>                                          ?They came with a plan to close the space and even though we were working the ball well, we didn?t have the space to open the box.<br>                     <br>                                          ?But there are a lot of games to go and we have a game away from home against them later on, so we can get those three points back.<br>                     <br>                                          ?We know exactly what to do. We have to bounce back and we have the strength in our side to do that.?"
"3579","By <span class=""bold"">Paul Brown </span></h4>
								
							</div>
			
						<div class=""left"" style=""width:177px; height: 30px"">				<p>
					<a href=""#comments"" class=""speechBubble"">Your Shout  ( <span class=""bigCommentsNum"" id=""commentscountertop"">0</span> )</a>
				</p>
			</div>
						</div>


		<p class=""introcopy"">PETR CECH watched Chelsea?s record run bite the dust and then told his team-mates: Let?s do it again!</p>
		<br />
	
	
	
			
		<style>
			#bodycopy a { font-size: 12px; }
		</style>
	
		<p id=""bodycopy"">The Blues went 86 home league games without defeat before Liverpool ended their proud record at fortress Stamford Bridge.<br>                    <br>                                          Cech reckons it?s a mark that may never be beaten ? but he is determined to make sure Chelsea give it a go.<br>                     <br>                                          The keeper said: ?Defeat happens in football and you have to forget about it and start again.<br>                     <br>                                          ?It has been a great run, 86 games, I don?t think someone can really come close to that in the future because it?s a fantastic run.<br>                     <br>                                          ?But now it is all behind us and we have to think about the game on Wednesday when we go to Hull.<br>                     <br>                                          ?We have the same number of points and we have to bounce back with a victory.<br>                     <br>                                          ?We say that with something ending, something new begins, so we hope that is the case here.? Cech admits Liverpool have made giant strides this season and are genuine contenders for the Premier League title.<br>                     <br>                                          But he claims Chelsea will get their revenge when the two teams meet at Anfield on January 31.<br>                     <br>                                          Cech said: ?It is a big leap for them. They have three points from Stamford Bridge.<br>                    <br>                                          ?They are top of the league now and at the beginning of the season they didn?t play that well but they were getting results.<br>                     <br>                                          ?They came with a plan to close the space and even though we were working the ball well, we didn?t have the space to open the box.<br>                     <br>                                          ?But there are a lot of games to go and we have a game away from home against them later on, so we can get those three points back.<br>                     <br>                                          ?We know exactly what to do. We have to bounce back and we have the strength in our side to do that.?