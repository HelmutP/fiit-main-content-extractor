
                    Nasrallah and al-Hariri enter talks
                </span>
            </td>
        </tr>
        <tr>
            <td height=""5"">
                <!---->
                </td>
        </tr>
        
        <tr>
            <td valign=""top""  >
                <table cellspacing=""0"" cellpadding=""0"" align=""right"" style=""display: inline"" border=""0"">
                    <tr>
                        <td valign=""top"" width=""100%"" align=""right"" style=""height: 14px"">
                            <span align=""right"" style=""display: inline"" width=""150px"" border=""1"" class=""ImageTable"">
                                <table style=""width: 33px; border-collapse: collapse;"" border=""0"" cellspacing=""0"" cellpadding=""0"">
<tbody>
<tr>
<td><img src=""/mritems/Images//2008/10/27/2008102713414932734_5.jpg"" alt="""" /></td>
</tr>
<tr>
<td align=""center""><span style=""font-size: 10px; font-family: Verdana;""><span style=""font-size: 10px; font-family: Verdana;""><strong>Nasrallah, left, and al-Hariri held talks in the run-up to a national dialogue meeting in November [AFP]</strong></span></span><strong></strong></td>
</tr>
</tbody>
</table>

                            </span>
                        </td>
                    </tr>
                </table>
                <span class=""DetaildSuammary"" id=""Htmlphcontrol1"">
                    
                    <p>Hassan Nasrallah, the secretary-general of Hezbollah, has held talks with his political rival Saad al-Hariri, leader of Lebanon's parliamentary majority, a statement issued by both sides has said.</p>
<p>The meeting on Sunday was the first between the two leaders since Israel's 2006 war in Lebanon and comes five months after Qatar mediated an end to an 18-month political conflict in Lebanon which had escalated into street battles.</p>
                    
                </span>
                <span class=""DetaildSuammary"" id=""Span1"">
                    
                    <p>""There was an affirmation of national unity and civil peace and the need to take all measures to prevent tension ... and to reinforce dialogue and to avoid strife regardless of political differences,"" the statement issued by both sides said.</p>
<p>The rare meeting between Nasrallah and al-Hariri marks a thaw in relations between the two opponents before parliamentary elections scheduled for&nbsp;2009.</p>
                    
                    <p>Hezbollah's al-Manar television aired footage of the meeting, which was also attended by aides to both leaders.</p>
<p>The joint statement also said that Nasrallah and al-Hariri would be in ""mutual contact"".</p>
<p><strong>Political differences</strong></p>
<p>The differences between Hezbollah, a Shia Muslim political organisation which runs an armed wing, and member parties of the March 14 parliamentary majority flared into armed conflict in May.</p>
<p>Fighters from Hezbollah and its allies briefly took control of predominantly Muslim west Beirut, prompting an armed response by supporters&nbsp;of the March 14 bloc, including those allied to al-Hariri's Future Movement.</p>
<p>The joint statement released on Monday said that the meeting was ""honest and open"" and said that the leaders would take ""steps to calm the situation in the media and in the street"".</p>
<p>The talks between al-Hariri and Nasrallah are being seen in Lebanon as the most significant of a series of meetings between politicians from March 14 and the Hezbollah-led opposition.</p>
<p>Nasrallah and al-Hariri are also committed to implementing the Qatar-mediated deal which in May had called for ""national dialogue"" talks, Monday's joint statement said.</p>
<p>The next session is scheduled for November 5.</p>
<p><strong>Weapons issue</strong></p>
<p>The parliamentary majority is calling for Hezbollah to disarm, in favour of building a stronger Lebanese national army.</p>
<p>However, Hezbollah insists that its weapons are essential to a Lebanese national resistance against Israel, its southern neighbour.</p>
<p>Israel failed to destroy Hezbollah&nbsp;during a 34-day war in 2006,&nbsp;which was sparked by a Hezbollah cross-border raid.</p>
<p>Demands for the disarmament of Hezbollah, which is backed by Syria and Iran, have become increasingly shrill since the 2005 assassination of Rafiq al-Hariri, a former Lebanese prime minister and&nbsp;Saad's father.</p>
<p>Hezbollah is the most powerful faction in Lebanon and its opposition bloc holds veto power over decisions taken in Lebanon's cabinet.</p>
<p>While Hezbollah is not willing to disarm, its leaders have said that the organisation is willing to discuss a defence strategy that would define the role of its fighters.