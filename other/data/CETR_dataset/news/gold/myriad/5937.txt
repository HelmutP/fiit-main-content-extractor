"Guy Ritchie pulls plug on Madonna's plans for Kabbalah water swimming pool</h1>
	
	<!-- font resizer -->
	<ul id=""font-size"">
	<li class=""up""><a onclick=""javascript:fontsizeup();""><img src=""http://images.mirror.co.uk/design/transparent.gif"" alt="""" /></a></li>
	<li class=""down""><a onclick=""javascript:fontsizedown();""><img src=""http://images.mirror.co.uk/design/transparent.gif"" alt="""" /></a></li>
	</ul>
	
	<p class=""article-date"">
	
	By 3am
	
	
	<a href=""http://www.mirror.co.uk/celebs/3am/2008/10/28/"" title=""Find all articles published on 28/10/2008 to the 3am section"">28/10/2008</a>
	
	
	</p>
	
	<div id=""article-body"">
 	<div class=""art-o art-align-center"">
	<img src=""http://images.mirror.co.uk/upl/m4/oct2008/2/9/01169F2F-D37A-4981-A2A77CAA3D2898B0.jpg"" height=""328"" width=""450"" alt=""Madonna and Guy Ritchie (Pic:Getty)"" border=""0"" />
	</div>
	<p>Relations between warring couple Madonna and Guy Ritchie have plunged to an all-time low &#8211; after he blasted her pool plans out of the water.</p>
	<p>Film director Guy, 40, has pulled the plug on his estranged missus&#8217; &#8220;ridiculous&#8221; scheme to have their new indoor swimming pool filled with Kabbalah water.</p>
	<p>He has cancelled an order for the religion&#8217;s special H20 for the pool at their Wiltshire estate &#8211; insisting it&#8217;s filled with good old-fashioned chlorinated water, straight from the tap.</p>
	<p>Having received the go-ahead from local council chiefs to forge on with designs for the indoor pool, the Lock Stock director will build it as he sees fit.</p>
	<p>Even though changing the earlier instructions will cost him almost &#163;10,000 &#8211; water way to dip into your savings!</p>
	<p>We hear: &#8220;Madonna and Guy originally applied for planning permission for the pool when they were together. They wanted to build it for the kids to use, more than as a fitness tool for themselves.</p>
	<p>&#8220;But Madonna wanted it filled with special Kabbalah imported water, in keeping with her dedication to the faith.</p>
	<div class=""advert"">
	<p>Advertisement - article continues below &raquo;</p>
	<div class=""mpu-ad"">
	

















	











	

	
	
	
	
	
	
	
	
	
	<script type=""text/javascript"">//<![CDATA[

if (typeof dartOrd == 'undefined') dartOrd=Math.random()*10000000000000000000;

document.write('<scr' +
	'ipt type=""text/javascript"" ' +
 'src=""http://ad.uk.doubleclick.net/adj/dailymirror.4240/' +
 'home_mputwo__300x250;'+mirrDartBLU()+';sz=300x250;pos=;sect=;psect=;zone=home;templ=page;'+mirrDartSIP()+';oid=20845877' +
 ';tile=4' +
 ';ord=' +
 dartOrd +
 '?""><\/scr' +
 'ipt>');
	//]]></script>
	<noscript>
	<a href=""http://ad.uk.doubleclick.net/jump/dailymirror.4240/home_mputwo__300x250;sz=300x250;pos=;sect=;psect=;zone=home;templ=page;tile=4;ord=769254843?"" target=""_blank"">
	<img
	src=""http://ad.uk.doubleclick.net/ad/dailymirror.4240/home_mputwo__300x250;sz=300x250;pos=;sect=;psect=;zone=home;templ=page;tile=4;ord=769254843?""
	
	width=""300""
	height=""250""
	
	border=""0""
	alt=""mputwoAdvertisement"" />
	</a>
	</noscript>
	

	


	</div>
	</div>
	<p>&#8220;There would also have been water fountains dotted around, again filled with Kabbalah water. It would have cost a small fortune, but obviously money for Madonna isn&#8217;t really an issue.</p>
	<p>&#8220;Since splitting, though, they&#8217;ve agreed to let Guy keep the country home &#8211; meaning he can do what he wants with it.</p>
	<p>&#8220;One of the first things he&#8217;s insisted on is scrapping the ridiculous pool plans. </p>
	<p>&#8220;He partly blames Kabbalah for the deterioration of their marriage, so the last thing he wants is an Olympic pool-sized reminder of the religion!</p>
	<p>&#8220;Cancelling the order means he&#8217;ll have to fork out almost &#163;10,000 plus he will have to pay a regular firm to fill the pool.&#8221;</p>
	<p>Since announcing their split earlier this month, the Material Girl continues to flex her muscles on the American leg of her world tour, while Guy&#8217;s been working in the <?xml:namespace prefix = st1 ns = ""urn:schemas-microsoft-com:office:smarttags"" /><st1:country-region w:st=""on""><st1:place w:st=""on"">UK</st1:place></st1:country-region> on his new movie, Sherlock Holmes.</p>
	<p>Even with the <?xml:namespace prefix = st1 ns = ""urn:schemas-microsoft-com:office:smarttags"" /><st1:place w:st=""on"">Atlantic</st1:place> dividing them they still sail into troubled waters..."
"5937","
	
	By 3am
	
	
	<a href=""http://www.mirror.co.uk/celebs/3am/2008/10/28/"" title=""Find all articles published on 28/10/2008 to the 3am section"">28/10/2008</a>
	
	
	</p>
	
	<div id=""article-body"">
 	<div class=""art-o art-align-center"">
	<img src=""http://images.mirror.co.uk/upl/m4/oct2008/2/9/01169F2F-D37A-4981-A2A77CAA3D2898B0.jpg"" height=""328"" width=""450"" alt=""Madonna and Guy Ritchie (Pic:Getty)"" border=""0"" />
	</div>
	<p>Relations between warring couple Madonna and Guy Ritchie have plunged to an all-time low &#8211; after he blasted her pool plans out of the water.</p>
	<p>Film director Guy, 40, has pulled the plug on his estranged missus&#8217; &#8220;ridiculous&#8221; scheme to have their new indoor swimming pool filled with Kabbalah water.</p>
	<p>He has cancelled an order for the religion&#8217;s special H20 for the pool at their Wiltshire estate &#8211; insisting it&#8217;s filled with good old-fashioned chlorinated water, straight from the tap.</p>
	<p>Having received the go-ahead from local council chiefs to forge on with designs for the indoor pool, the Lock Stock director will build it as he sees fit.</p>
	<p>Even though changing the earlier instructions will cost him almost &#163;10,000 &#8211; water way to dip into your savings!</p>
	<p>We hear: &#8220;Madonna and Guy originally applied for planning permission for the pool when they were together. They wanted to build it for the kids to use, more than as a fitness tool for themselves.</p>
	<p>&#8220;But Madonna wanted it filled with special Kabbalah imported water, in keeping with her dedication to the faith.</p>
	<div class=""advert"">
	<p>Advertisement - article continues below &raquo;</p>
	<div class=""mpu-ad"">
	

















	











	

	
	
	
	
	
	
	
	
	
	<script type=""text/javascript"">//<![CDATA[

if (typeof dartOrd == 'undefined') dartOrd=Math.random()*10000000000000000000;

document.write('<scr' +
	'ipt type=""text/javascript"" ' +
 'src=""http://ad.uk.doubleclick.net/adj/dailymirror.4240/' +
 'home_mputwo__300x250;'+mirrDartBLU()+';sz=300x250;pos=;sect=;psect=;zone=home;templ=page;'+mirrDartSIP()+';oid=20845877' +
 ';tile=4' +
 ';ord=' +
 dartOrd +
 '?""><\/scr' +
 'ipt>');
	//]]></script>
	<noscript>
	<a href=""http://ad.uk.doubleclick.net/jump/dailymirror.4240/home_mputwo__300x250;sz=300x250;pos=;sect=;psect=;zone=home;templ=page;tile=4;ord=769254843?"" target=""_blank"">
	<img
	src=""http://ad.uk.doubleclick.net/ad/dailymirror.4240/home_mputwo__300x250;sz=300x250;pos=;sect=;psect=;zone=home;templ=page;tile=4;ord=769254843?""
	
	width=""300""
	height=""250""
	
	border=""0""
	alt=""mputwoAdvertisement"" />
	</a>
	</noscript>
	

	


	</div>
	</div>
	<p>&#8220;There would also have been water fountains dotted around, again filled with Kabbalah water. It would have cost a small fortune, but obviously money for Madonna isn&#8217;t really an issue.</p>
	<p>&#8220;Since splitting, though, they&#8217;ve agreed to let Guy keep the country home &#8211; meaning he can do what he wants with it.</p>
	<p>&#8220;One of the first things he&#8217;s insisted on is scrapping the ridiculous pool plans. </p>
	<p>&#8220;He partly blames Kabbalah for the deterioration of their marriage, so the last thing he wants is an Olympic pool-sized reminder of the religion!</p>
	<p>&#8220;Cancelling the order means he&#8217;ll have to fork out almost &#163;10,000 plus he will have to pay a regular firm to fill the pool.&#8221;</p>
	<p>Since announcing their split earlier this month, the Material Girl continues to flex her muscles on the American leg of her world tour, while Guy&#8217;s been working in the <?xml:namespace prefix = st1 ns = ""urn:schemas-microsoft-com:office:smarttags"" /><st1:country-region w:st=""on""><st1:place w:st=""on"">UK</st1:place></st1:country-region> on his new movie, Sherlock Holmes.</p>
	<p>Even with the <?xml:namespace prefix = st1 ns = ""urn:schemas-microsoft-com:office:smarttags"" /><st1:place w:st=""on"">Atlantic</st1:place> dividing them they still sail into troubled waters...