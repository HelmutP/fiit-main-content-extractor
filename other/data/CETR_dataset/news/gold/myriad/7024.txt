"DAVID BOWIE AND ELTON JOHN LOSE &#163;1M ROCK MEMORIES IN BLAZE</div>
	<div class=""M2FullAStandfirst"">Bowie and Elton lose &pound;1m rock memories in blaze</div><div class=""M2FullAByline"">By Alice Walker & Katie Hind</div><p class=""add-linkout"" ALIGN=""left"">Rock legends including The Rolling Stones and David Bowie have been left devastated after &#163;1million of rare memorabilia was destroyed in a fire.</p><p class=""add-linkout"" ALIGN=""left"">Tour pictures and promotional material marking different eras in The Stones' 42-year career were lost in the warehouse blaze.</p><p class=""add-linkout"" ALIGN=""left"">And Ashes To Ashes singer Bowie, 61, is said to be ""distraught"" after legal documents signed at the start of his career were lost forever.</p><p class=""add-linkout""><div style=""display: none;"" id=""mpuad"" class=""center""><div class=""M2MpuAdvertisement"" id=""mpuadheader"">Advertisement</div><div class=""center"">
	<script type=""text/javascript"">//<![CDATA[

if (typeof dartOrd == 'undefined') dartOrd=Math.random()*10000000000000000000;

document.write('<scr' +
	'ipt type=""text/javascript"" ' +
 'src=""http://ad.uk.doubleclick.net/adj/people.4240/' +
 'showbiz_mpu_centre_300x250;'+mirrDartBLU()+';sz=300x250;pos=centre;sect=showbiz;psect=;zone=showbiz;templ=page;'+mirrDartSIP()+';oid=20839866' +
 ';tile=3' +
 ';ord=' +
 dartOrd +
 '?""><\/scr' +
 'ipt>');
	//]]></script>
	<noscript>
	<a href=""http://ad.uk.doubleclick.net/jump/people.4240/showbiz_mpu_centre_300x250;sz=300x250;pos=centre;sect=showbiz;psect=;zone=showbiz;templ=page;tile=3;ord=849149929?"" target=""_blank"">
	<img
	src=""http://ad.uk.doubleclick.net/ad/people.4240/showbiz_mpu_centre_300x250;sz=300x250;pos=centre;sect=showbiz;psect=;zone=showbiz;templ=page;tile=3;ord=849149929?""
	
	width=""300""
	height=""250""
	
	border=""0""
	alt=""mpuAdvertisement"" />
	</a>
	</noscript>
	</div></div></p><p class=""add-linkout"" ALIGN=""left"">Items belonging to Sir Elton John, Duran Duran and The Sex Pistols were also being kept at the depot in Bow, east London by storage experts Iron Mountain. Managers for The Stones and Bowie are now seeking compensation. A music industry source revealed: ""Everyone is very upset. The material is irreplaceable. Some of it is years old and worth thousands of pounds.</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">""Lawyers have been instructed to deal with it. People just want some compensation for their loss. They refuse to take it lightly.</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">""There is no way that this will be let go, there has not even been a proper explanation.""</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">It is the third blaze to hit Iron Mountain after warehouses in&nbsp;Ottawa and New Jersey&nbsp;burned to the ground in 2006 and 1996.</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">The firm would not comment on the London fire but said: ""The security of our customers' valuables is paramount."""
"7024","By Alice Walker & Katie Hind</div><p class=""add-linkout"" ALIGN=""left"">Rock legends including The Rolling Stones and David Bowie have been left devastated after &#163;1million of rare memorabilia was destroyed in a fire.</p><p class=""add-linkout"" ALIGN=""left"">Tour pictures and promotional material marking different eras in The Stones' 42-year career were lost in the warehouse blaze.</p><p class=""add-linkout"" ALIGN=""left"">And Ashes To Ashes singer Bowie, 61, is said to be ""distraught"" after legal documents signed at the start of his career were lost forever.</p><p class=""add-linkout""><div style=""display: none;"" id=""mpuad"" class=""center""><div class=""M2MpuAdvertisement"" id=""mpuadheader"">Advertisement</div><div class=""center"">
	<script type=""text/javascript"">//<![CDATA[

if (typeof dartOrd == 'undefined') dartOrd=Math.random()*10000000000000000000;

document.write('<scr' +
	'ipt type=""text/javascript"" ' +
 'src=""http://ad.uk.doubleclick.net/adj/people.4240/' +
 'showbiz_mpu_centre_300x250;'+mirrDartBLU()+';sz=300x250;pos=centre;sect=showbiz;psect=;zone=showbiz;templ=page;'+mirrDartSIP()+';oid=20839866' +
 ';tile=3' +
 ';ord=' +
 dartOrd +
 '?""><\/scr' +
 'ipt>');
	//]]></script>
	<noscript>
	<a href=""http://ad.uk.doubleclick.net/jump/people.4240/showbiz_mpu_centre_300x250;sz=300x250;pos=centre;sect=showbiz;psect=;zone=showbiz;templ=page;tile=3;ord=849149929?"" target=""_blank"">
	<img
	src=""http://ad.uk.doubleclick.net/ad/people.4240/showbiz_mpu_centre_300x250;sz=300x250;pos=centre;sect=showbiz;psect=;zone=showbiz;templ=page;tile=3;ord=849149929?""
	
	width=""300""
	height=""250""
	
	border=""0""
	alt=""mpuAdvertisement"" />
	</a>
	</noscript>
	</div></div></p><p class=""add-linkout"" ALIGN=""left"">Items belonging to Sir Elton John, Duran Duran and The Sex Pistols were also being kept at the depot in Bow, east London by storage experts Iron Mountain. Managers for The Stones and Bowie are now seeking compensation. A music industry source revealed: ""Everyone is very upset. The material is irreplaceable. Some of it is years old and worth thousands of pounds.</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">""Lawyers have been instructed to deal with it. People just want some compensation for their loss. They refuse to take it lightly.</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">""There is no way that this will be let go, there has not even been a proper explanation.""</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">It is the third blaze to hit Iron Mountain after warehouses in&nbsp;Ottawa and New Jersey&nbsp;burned to the ground in 2006 and 1996.</p><p class=""add-linkout""></p><p class=""add-linkout"" ALIGN=""left"">The firm would not comment on the London fire but said: ""The security of our customers' valuables is paramount.""