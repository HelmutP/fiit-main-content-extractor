"Municipality denies neglect of resort</font></b></p>
			<p style=""margin-top: 0; margin-bottom: 0"">
			<font face=""Arial"">The municipality of Madibeng has denied reports 
			that the municipal resort on the Schoemansville foreshore is in a 
			state of neglect after complaints that the area is not being 
			properly maintained. Kormorant has received several complaints that 
			the resort is no credit to the ?Pearl of the North West? slogan used 
			to promote tourism to the area. <br>
			There are several spots around the Dam where day visitors can reach 
			the shore to picnic or fish. Schoemansville?s Oewer Club, for 
			instance, is perhaps the ideal picnic spot but the number of 
			visitors that can be accommodated is limited. The municipal resort 
			next door, also known as the Eatern forshore, comprises a much 
			bigger part of the shoreline and is also one of the best shore bird 
			habitats in North West, according to some enthusiasts, where one 
			will find in excess of twenty to thirty different species of birds 
			within ten minutes. But, as local resident, Jan Loysen says, the 
			place is ?a thundering disgrace?. <br>
			He says that at or near the water?s edge there is not a square metre 
			where one will not find rubbish strewn about. The litter spoils the 
			beauty of the area. Another Schoemansville resident, who prefers to 
			remain anonymous, said that the litter is not removed regularly and 
			the ablution blocks in the resort are in such a bad state that they 
			are unfit for human use. <br>
			There are no waste drums for visitors to dump their rubbish before 
			leaving and instead of taking it with them, it is left behind and 
			scattered by the wind. As braai facilities at the Eastern foreshore 
			are limited and badly maintained, visitors make fires wherever they 
			choose. <br>
			In response to enquiries, Tumelo Tshabalala, the spokesperson for 
			the Madibeng local municipality, says that there is no rubbish and 
			litter as reported and that the ablution blocks only have a few 
			broken windows that will be fixed soon. He says that a security 
			guard is always on duty on the site. Tshabalala also says that 
			rubbish bins will be supplied soon and braai facilities will be 
			erected in all the resorts in the new financial year.