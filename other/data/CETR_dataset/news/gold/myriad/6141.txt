"Humbled by the truthers</h2><p class=""subheadline"">9/11 conspiracy theorists are perversely misguided. But they know how to argue</p><p class=""author""><strong>Jonathan Kay,
          National Post 
        </strong><span>
            Published: Tuesday, October 28, 2008</span></p><div class=""story-tools""><div class=""newsblock triline""><h4>Story Tools</h4><ul class=""category""><li><p><a href=""#"" title=""Decrease font size..."" class=""icon-font-small"" onclick=""javascript:return font_size(-1)""><span>-</span></a><a href=""#"" title=""Increase font size..."" class=""icon-font-big"" onclick=""javascript:return font_size(1)""><span>+</span></a> Change font size
            </p></li><li><p><a class=""icon-print"" href=""/print/"" onclick=""return print_click()"" target=""_blank"">Print this story</a><script type=""text/javascript"">
                function print_click() {
                u=location.href;
                u=delineate(u);
                window.open('/story-printer.html?id='+encodeURIComponent(u),'sharer','toolbar=1,status=1,location=0,menubar=1,scrollbars=1,width=650,height=600');
                return false;
                }
                function delineate(str)
                {
                theleft = str.indexOf(""="") + 1;
                theright = str.lastIndexOf(""&"");
                if ( theright == -1 ) {
                theright = str.length;
                }
                return(str.substring(theleft, theright));
                }
              </script></p></li><li><p><a class=""icon-email"" href=""#email"" onclick=""javascript:return story_email(true)"">E-Mail this story</a></p></li></ul></div><div class=""newsblock triline""><script type=""text/javascript"">
					str_url = encodeURIComponent(location.href);
					str_title = encodeURIComponent(document.title);
					// Facebook
					function fbs_click(){
						var w = 600, h = 400;
						window.open('http://www.facebook.com/sharer.php?u=' + str_url + '&t=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// Digg
					function digg_click(){
						var w = 960, h = screen.height - 200;
						window.open('http://digg.com/submit?phase=2&url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// StumbleUpon
					function stumble_click(){
						var w = 700, h = 400;
						window.open('http://www.stumbleupon.com/submit?url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// AddThis
					function addthis_click(){
						var w = 700, h = 750;
						window.open('http://www.addthis.com/bookmark.php?url=' + str_url + '&title=' + str_title + '&pub=nationalpost.com', 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					/* Delicious
					function dels_click(){
						var w = 700, h = 400;
						window.open('http://del.icio.us/post?url=' + str_url + '&jump=close&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}//*/
				</script><h4>Share This Story</h4><ul class=""category""><li><p><a href=""http://www.facebook.com/"" target=""_blank"" class=""icon-facebook"" onclick=""return fbs_click()"">Facebook</a></p></li><li><p><a href=""http://www.digg.com/"" target=""_blank"" class=""icon-digg"" onclick=""return digg_click()"">Digg</a></p></li><li><p><a href=""http://www.stumbleupon.com/"" target=""_blank"" class=""icon-stumbleupon"" onclick=""return stumble_click()"">Stumble Upon</a></p></li><li><p><a href=""http://www.addthis.com/"" target=""_blank"" class=""icon-addthis"" onclick=""return addthis_click()"">More</a></p></li></ul></div><div class=""sponsor""><p>Story tools presented by</p><div class=""ad-microbar""><script type=""text/javascript"">
								try{
									var arr_da = document.getElementById('ad-leaderboard').getElementsByTagName('script');
									for(var i in arr_da){
										if(typeof(arr_da[i].src) != 'undefined' && (/http:\/\/ad\.ca\.doubleclick\.net/.test(arr_da[i].src))){
											var str_da_src = arr_da[i].src;
											str_da_src = str_da_src.replace(/loc=\w+;/gi, 'loc=microbar;');
											str_da_src = str_da_src.replace(/sz=\d+x\d+;/gi, 'sz=88x31;');
											str_da_src = str_da_src.replace(/ptile=\d+;/gi, 'ptile=4;');
											document.write('<script type=""text/javascript"" src=""' + str_da_src + '""><\/script>');
											break;
										}
									}
								}catch(e){}
							</script></div></div></div><div class=""story-content""><p>These are the people who dismiss the ?official? 9/11 explanation as a White Housepeddled lie. One Truther sect believes the American government (or rogue elements within it) planned the Sept. 11 attacks under a false flag. Another concedes al-Qaeda?s involvement ? but insists the plot was known to a warmongering Bush administration. Other Truthers favour more exotic explanations ? featuring missiles, remote-controlled airliners, the Mossad, mini-nukes and even space weapons. What they all have in common, I?ve learned, is a hate-on for journalists who blithely dismiss them as ?nut-bars,? as I did a few weeks ago in a column about ex-Liberal candidate (and unapologetic Truther) Lesley Hughes.</p><p>Normally, I confess, these are the sort of correspondents I delete from my inbox without much guilt. As with all conspiracy theorists, debate is pointless. Since they have no chance of convincing me, nor I them, why waste my time parsing their arguments?</p><p>But something about the Truther e-mail I got aroused my interest.</p><p>For one thing, there was a lot of it. Which perhaps should not surprise me: As the Post?s Adrian Humphreys reported last week, 39% of Canadians either reject or doubt the official explanation for the 9/11 attacks.</p><p>Secondly, many of the messages ? a majority, in fact ? were lucid and coherent, well-written even. Some of the senders had letters after their names, and their e-mail addresses contained the domain names of recognizable universities. Conspiracy theorists, they may be. But they?re certainly not the subhuman weirdos who try to convince me the Holocaust never happened, nor the anti-social paranoiacs who send me weekly updates about their decades-long litigation campaign against their ex-landlords.</p><p>Thirdly, they have a point ? something that hit home when I read this stinging comment in a message from Winnipeg-area writer Dallas Hansen: ?Take a look at the mountain of evidence suggesting a black op ? I?m sure you?re a smart guy, but posturing as though you?ve got all the answers to 9/11, and defaming those who question the explanation we?ve been given, is a little much.?</p><p>Sept. 11 wasn?t a ?black op.? But Hansen is right that, like most mainstream journalists, I overplay my intellectual hand when it comes to 9/11 ? and just about everything else besides.</p><p>I could lie to Mr. Hansen, and to my readers, and tell them that I?ve read the U. S. government?s 571-page 9/11 Commission Report, or Popular Mechanics magazine?s authoritative report on ?Debunking the 9/11 Myths.? But the truth is that I haven?t. I never felt the need to because, on a purely instinctive level, I always believed the Truthers? case was complete nonsense.</p><p>I don?t believe any group of people ? let alone any group of people in government ? could possibly pull off such a fantastically elaborate plot as 9/11, one involving hundreds, if not thousands of conspirators, without being definitively outed many times over. If 9/11 truly had been a U. S. government conspiracy, dozens of collaborators would have come forward by now for their multi-million dollar book deals; the mainstream media, always desperate for a scoop (trust me), would have broken the story open long ago; and respectable peer-reviewed journals would have published the work of professionals debunking the official account of the World Trade Center?s destruction."
"6141","Jonathan Kay,
          National Post 
        </strong><span>
            Published: Tuesday, October 28, 2008</span></p><div class=""story-tools""><div class=""newsblock triline""><h4>Story Tools</h4><ul class=""category""><li><p><a href=""#"" title=""Decrease font size..."" class=""icon-font-small"" onclick=""javascript:return font_size(-1)""><span>-</span></a><a href=""#"" title=""Increase font size..."" class=""icon-font-big"" onclick=""javascript:return font_size(1)""><span>+</span></a> Change font size
            </p></li><li><p><a class=""icon-print"" href=""/print/"" onclick=""return print_click()"" target=""_blank"">Print this story</a><script type=""text/javascript"">
                function print_click() {
                u=location.href;
                u=delineate(u);
                window.open('/story-printer.html?id='+encodeURIComponent(u),'sharer','toolbar=1,status=1,location=0,menubar=1,scrollbars=1,width=650,height=600');
                return false;
                }
                function delineate(str)
                {
                theleft = str.indexOf(""="") + 1;
                theright = str.lastIndexOf(""&"");
                if ( theright == -1 ) {
                theright = str.length;
                }
                return(str.substring(theleft, theright));
                }
              </script></p></li><li><p><a class=""icon-email"" href=""#email"" onclick=""javascript:return story_email(true)"">E-Mail this story</a></p></li></ul></div><div class=""newsblock triline""><script type=""text/javascript"">
					str_url = encodeURIComponent(location.href);
					str_title = encodeURIComponent(document.title);
					// Facebook
					function fbs_click(){
						var w = 600, h = 400;
						window.open('http://www.facebook.com/sharer.php?u=' + str_url + '&t=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// Digg
					function digg_click(){
						var w = 960, h = screen.height - 200;
						window.open('http://digg.com/submit?phase=2&url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// StumbleUpon
					function stumble_click(){
						var w = 700, h = 400;
						window.open('http://www.stumbleupon.com/submit?url=' + str_url + '&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					// AddThis
					function addthis_click(){
						var w = 700, h = 750;
						window.open('http://www.addthis.com/bookmark.php?url=' + str_url + '&title=' + str_title + '&pub=nationalpost.com', 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}
					/* Delicious
					function dels_click(){
						var w = 700, h = 400;
						window.open('http://del.icio.us/post?url=' + str_url + '&jump=close&title=' + str_title, 'sharer', 'width=' + w + ',height=' + h + ',top=' + ((screen.height - h) / 2) + ',left=' + ((screen.width - w) / 2) + ',scrollbars=1');
						return false;
					}//*/
				</script><h4>Share This Story</h4><ul class=""category""><li><p><a href=""http://www.facebook.com/"" target=""_blank"" class=""icon-facebook"" onclick=""return fbs_click()"">Facebook</a></p></li><li><p><a href=""http://www.digg.com/"" target=""_blank"" class=""icon-digg"" onclick=""return digg_click()"">Digg</a></p></li><li><p><a href=""http://www.stumbleupon.com/"" target=""_blank"" class=""icon-stumbleupon"" onclick=""return stumble_click()"">Stumble Upon</a></p></li><li><p><a href=""http://www.addthis.com/"" target=""_blank"" class=""icon-addthis"" onclick=""return addthis_click()"">More</a></p></li></ul></div><div class=""sponsor""><p>Story tools presented by</p><div class=""ad-microbar""><script type=""text/javascript"">
								try{
									var arr_da = document.getElementById('ad-leaderboard').getElementsByTagName('script');
									for(var i in arr_da){
										if(typeof(arr_da[i].src) != 'undefined' && (/http:\/\/ad\.ca\.doubleclick\.net/.test(arr_da[i].src))){
											var str_da_src = arr_da[i].src;
											str_da_src = str_da_src.replace(/loc=\w+;/gi, 'loc=microbar;');
											str_da_src = str_da_src.replace(/sz=\d+x\d+;/gi, 'sz=88x31;');
											str_da_src = str_da_src.replace(/ptile=\d+;/gi, 'ptile=4;');
											document.write('<script type=""text/javascript"" src=""' + str_da_src + '""><\/script>');
											break;
										}
									}
								}catch(e){}
							</script></div></div></div><div class=""story-content""><p>These are the people who dismiss the ?official? 9/11 explanation as a White Housepeddled lie. One Truther sect believes the American government (or rogue elements within it) planned the Sept. 11 attacks under a false flag. Another concedes al-Qaeda?s involvement ? but insists the plot was known to a warmongering Bush administration. Other Truthers favour more exotic explanations ? featuring missiles, remote-controlled airliners, the Mossad, mini-nukes and even space weapons. What they all have in common, I?ve learned, is a hate-on for journalists who blithely dismiss them as ?nut-bars,? as I did a few weeks ago in a column about ex-Liberal candidate (and unapologetic Truther) Lesley Hughes.</p><p>Normally, I confess, these are the sort of correspondents I delete from my inbox without much guilt. As with all conspiracy theorists, debate is pointless. Since they have no chance of convincing me, nor I them, why waste my time parsing their arguments?</p><p>But something about the Truther e-mail I got aroused my interest.</p><p>For one thing, there was a lot of it. Which perhaps should not surprise me: As the Post?s Adrian Humphreys reported last week, 39% of Canadians either reject or doubt the official explanation for the 9/11 attacks.</p><p>Secondly, many of the messages ? a majority, in fact ? were lucid and coherent, well-written even. Some of the senders had letters after their names, and their e-mail addresses contained the domain names of recognizable universities. Conspiracy theorists, they may be. But they?re certainly not the subhuman weirdos who try to convince me the Holocaust never happened, nor the anti-social paranoiacs who send me weekly updates about their decades-long litigation campaign against their ex-landlords.</p><p>Thirdly, they have a point ? something that hit home when I read this stinging comment in a message from Winnipeg-area writer Dallas Hansen: ?Take a look at the mountain of evidence suggesting a black op ? I?m sure you?re a smart guy, but posturing as though you?ve got all the answers to 9/11, and defaming those who question the explanation we?ve been given, is a little much.?</p><p>Sept. 11 wasn?t a ?black op.? But Hansen is right that, like most mainstream journalists, I overplay my intellectual hand when it comes to 9/11 ? and just about everything else besides.</p><p>I could lie to Mr. Hansen, and to my readers, and tell them that I?ve read the U. S. government?s 571-page 9/11 Commission Report, or Popular Mechanics magazine?s authoritative report on ?Debunking the 9/11 Myths.? But the truth is that I haven?t. I never felt the need to because, on a purely instinctive level, I always believed the Truthers? case was complete nonsense.</p><p>I don?t believe any group of people ? let alone any group of people in government ? could possibly pull off such a fantastically elaborate plot as 9/11, one involving hundreds, if not thousands of conspirators, without being definitively outed many times over. If 9/11 truly had been a U. S. government conspiracy, dozens of collaborators would have come forward by now for their multi-million dollar book deals; the mainstream media, always desperate for a scoop (trust me), would have broken the story open long ago; and respectable peer-reviewed journals would have published the work of professionals debunking the official account of the World Trade Center?s destruction.