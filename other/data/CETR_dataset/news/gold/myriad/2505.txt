"Prominent bystander killings in Canada</h1>

<h4 class=""lastupdated"">Last Updated: 

Tuesday, October 28, 2008 | 11:52 AM ET

</h4>

<h5 class=""byline"">

<a href=""/news/credit.html"">CBC News</a>
</h5>

				</div>
				<div id=""storybody"">
					

<h4>Dec. 26, 2005</h4> <p><span class=""photo right"" style=""width:202px""><img src=""http://www.cbc.ca/gfx/Toronto/photos/jane-creba-031506.jpg"" alt=""Jane Creba, 15, was shot to death on Dec. 26, 2005, as two groups of people exchanged gunfire in a crowd of holiday shoppers on Yonge Street. "" /><em>Jane Creba, 15, was shot to death on Dec. 26, 2005, as two groups of people exchanged gunfire in a crowd of holiday shoppers on Yonge Street. </em> </span>Two rival groups of men start shooting at each other around dinnertime on Toronto's busy Yonge Street on Boxing Day 2005. Jane Creba, a 15-year-old girl out shopping with her sister, is struck by a bullet and killed. Six other people on the street are wounded.</p>
<p>Two young offenders and seven adults would later face charges in the shooting. At the trial of one of the youths, identified only as J.S.R., a police officer who was at the scene would describe it as ""sheer pandemonium.""</p>
<p>""There were hundreds of people running everywhere. It was complete mayhem,"" Const. Brian Callanan testified.</p> <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/toronto/story/2008/10/24/creba-friday.html"">'Panic, mayhem' on Yonge Street after Creba shot: witness</a> </li> </ul> 
<div class=""fullbar""> <h4>Oct. 19, 2007</h4> <p>Six men are found dead in what police described as a gang-style slaying on the 15th floor of an apartment building in Surrey, B.C.</p>
<p>RCMP would later describe two of the men �?? Chris Mohan, 22, of Surrey and Edward J. Schellenberg, 55, of Abbotsford �?? as innocent bystanders who were caught ""in the wrong place at the wrong time."" Schellenberg was a gas fireplace repairman who was working in the building that day. Police did not say why Mohan was at the apartment.</p>
<p>The RCMP said the killings were related to drugs and gang activity. ""The nature of the murders leaves us with the opinion this incident is not a random event,"" said Chief Supt. Fraser MacRae of the Surrey RCMP detachment.</p> <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/british-columbia/story/2007/10/23/bc-homicides.html"">2 men 'in the wrong place at the wrong time' in Surrey slayings: police</a></li> </ul> </div>
 <h4>Jan. 12, 2008</h4> <p>Two men are kicked out of the Brass Rail strip club on Yonge Street in Toronto about 1:30 a.m. The men later return to the front of the club and fire shots at a crowd outside. John O'Keefe, who was heading home from the Duke of Gloucester Pub, also on Yonge Street, is hit and killed.</p>
<p>Toronto police Det. Sgt. Dan Nielsen says the two men kicked out of the strip club tried to shoot a bouncer and O'Keefe just got in the way.</p>
<p>Awet Zekarias, 22, and Edward Paredes, 23, are arrested just hours after the shooting. Both men are from Scarborough.</p> <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/toronto/story/2008/01/14/tto-shooting.html"">Passerby shot, killed on Yonge Street</a> </li> </ul> 
<div class=""fullbar""> <span class=""photo right"" style=""width:202px""><img src=""http://www.cbc.ca/gfx/images/news/photos/2008/01/18/toronto-victim080118.jpg"" alt=""Hou Chang Mao was fatally wounded by a stray bullet while stacking fruit at a Toronto grocery store."" /><em>Hou Chang Mao was fatally wounded by a stray bullet while stacking fruit at a Toronto grocery store.</em>  <em class=""credit"">(Toronto Police Service)</em></span> <h4>Jan. 17, 2008</h4> <p>Hou Chang Mao, 47, is killed by a stray bullet as he is stacking fruit at a grocery store on Gerrard Street East in Toronto's Chinatown East during the evening rush hour.</p>
<p>Police would later say they are frustrated by a language barrier and a lack of co-operation from members of the Chinese community.</p>
<p>Police release images from security video cameras of a silver Nissan Maxima and of two black men whom they describe as persons of interest.</p>  <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/toronto/story/2008/01/25/mao-funeral.html"">Police canvas neighbourhood looking for witnesses to East Chinatown killing</a> </li> </ul> </div>
 <span class=""photo right"" style=""width:202px""><img src=""http://www.cbc.ca/gfx/images/news/photos/2008/10/26/bailey-zaveda-ho-250.jpg"" alt=""Bailey Zaveda, 23.   "" /><em>Bailey Zaveda, 23.   </em>  <em class=""credit"">(Toronto Police Service)</em></span> <h4>Oct. 25, 2008</h4> <p>Two men start fighting in a crowd of a dozen people outside the Duke of York tavern on Toronto's Queen Street East. One of the men starts shooting at the other, and hits him and four other people. One of them, Bailey Zaveda, 23, is killed.</p>
<p>Toronto police later issue a Canada-wide arrest warrant for Kyle Weese, 24, of no fixed address, for second-degree murder in connection with the shooting. Toronto police Det.-Sgt. Gary Giroux describes Weese as a ""very violent person"" with a ""very extensive criminal record."""
"2505","CBC News</a>
</h5>

				</div>
				<div id=""storybody"">
					

<h4>Dec. 26, 2005</h4> <p><span class=""photo right"" style=""width:202px""><img src=""http://www.cbc.ca/gfx/Toronto/photos/jane-creba-031506.jpg"" alt=""Jane Creba, 15, was shot to death on Dec. 26, 2005, as two groups of people exchanged gunfire in a crowd of holiday shoppers on Yonge Street. "" /><em>Jane Creba, 15, was shot to death on Dec. 26, 2005, as two groups of people exchanged gunfire in a crowd of holiday shoppers on Yonge Street. </em> </span>Two rival groups of men start shooting at each other around dinnertime on Toronto's busy Yonge Street on Boxing Day 2005. Jane Creba, a 15-year-old girl out shopping with her sister, is struck by a bullet and killed. Six other people on the street are wounded.</p>
<p>Two young offenders and seven adults would later face charges in the shooting. At the trial of one of the youths, identified only as J.S.R., a police officer who was at the scene would describe it as ""sheer pandemonium.""</p>
<p>""There were hundreds of people running everywhere. It was complete mayhem,"" Const. Brian Callanan testified.</p> <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/toronto/story/2008/10/24/creba-friday.html"">'Panic, mayhem' on Yonge Street after Creba shot: witness</a> </li> </ul> 
<div class=""fullbar""> <h4>Oct. 19, 2007</h4> <p>Six men are found dead in what police described as a gang-style slaying on the 15th floor of an apartment building in Surrey, B.C.</p>
<p>RCMP would later describe two of the men �?? Chris Mohan, 22, of Surrey and Edward J. Schellenberg, 55, of Abbotsford �?? as innocent bystanders who were caught ""in the wrong place at the wrong time."" Schellenberg was a gas fireplace repairman who was working in the building that day. Police did not say why Mohan was at the apartment.</p>
<p>The RCMP said the killings were related to drugs and gang activity. ""The nature of the murders leaves us with the opinion this incident is not a random event,"" said Chief Supt. Fraser MacRae of the Surrey RCMP detachment.</p> <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/british-columbia/story/2007/10/23/bc-homicides.html"">2 men 'in the wrong place at the wrong time' in Surrey slayings: police</a></li> </ul> </div>
 <h4>Jan. 12, 2008</h4> <p>Two men are kicked out of the Brass Rail strip club on Yonge Street in Toronto about 1:30 a.m. The men later return to the front of the club and fire shots at a crowd outside. John O'Keefe, who was heading home from the Duke of Gloucester Pub, also on Yonge Street, is hit and killed.</p>
<p>Toronto police Det. Sgt. Dan Nielsen says the two men kicked out of the strip club tried to shoot a bouncer and O'Keefe just got in the way.</p>
<p>Awet Zekarias, 22, and Edward Paredes, 23, are arrested just hours after the shooting. Both men are from Scarborough.</p> <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/toronto/story/2008/01/14/tto-shooting.html"">Passerby shot, killed on Yonge Street</a> </li> </ul> 
<div class=""fullbar""> <span class=""photo right"" style=""width:202px""><img src=""http://www.cbc.ca/gfx/images/news/photos/2008/01/18/toronto-victim080118.jpg"" alt=""Hou Chang Mao was fatally wounded by a stray bullet while stacking fruit at a Toronto grocery store."" /><em>Hou Chang Mao was fatally wounded by a stray bullet while stacking fruit at a Toronto grocery store.</em>  <em class=""credit"">(Toronto Police Service)</em></span> <h4>Jan. 17, 2008</h4> <p>Hou Chang Mao, 47, is killed by a stray bullet as he is stacking fruit at a grocery store on Gerrard Street East in Toronto's Chinatown East during the evening rush hour.</p>
<p>Police would later say they are frustrated by a language barrier and a lack of co-operation from members of the Chinese community.</p>
<p>Police release images from security video cameras of a silver Nissan Maxima and of two black men whom they describe as persons of interest.</p>  <ul> <li>CBC Story: <a href=""http://www.cbc.ca/canada/toronto/story/2008/01/25/mao-funeral.html"">Police canvas neighbourhood looking for witnesses to East Chinatown killing</a> </li> </ul> </div>
 <span class=""photo right"" style=""width:202px""><img src=""http://www.cbc.ca/gfx/images/news/photos/2008/10/26/bailey-zaveda-ho-250.jpg"" alt=""Bailey Zaveda, 23.   "" /><em>Bailey Zaveda, 23.   </em>  <em class=""credit"">(Toronto Police Service)</em></span> <h4>Oct. 25, 2008</h4> <p>Two men start fighting in a crowd of a dozen people outside the Duke of York tavern on Toronto's Queen Street East. One of the men starts shooting at the other, and hits him and four other people. One of them, Bailey Zaveda, 23, is killed.</p>
<p>Toronto police later issue a Canada-wide arrest warrant for Kyle Weese, 24, of no fixed address, for second-degree murder in connection with the shooting. Toronto police Det.-Sgt. Gary Giroux describes Weese as a ""very violent person"" with a ""very extensive criminal record.""