"Harry Potter</i> franchise 
    showing signs of sequel fatigue</span></b>  <br /><span style=""font-size: 80%;align:right;"">Return to <b><i><a href=""http://www.canadianchristianity.com/bc/index.html"">digital</a></i></b> BC Christian News</span>
</td></tr>


<tr><td width=""50%"" valign=""top""  style=""border-right: solid 1px #999999"">



<p>
   
    By Peter T. Chattaway 
 



<p>
    <img src=""http://www.canadianchristianity.com/film/graphics/peterimage2.jpg"" width=""92"" height=""135"" border=""0"" align=""left"" hspace=""2"">THERE is a lot that could be said about Harry Potter and the Order of the Phoenix . It is the longest of the books in J.K. Rowling&#8217;s 
    phenomenally popular series, yet it is also the shortest of the five movies 
    that have come out so far. 

<p>
    The book, which came out in 2003, was the first to be 
    written after the movies went into production &#8211; and it is tempting to 
    wonder whether Rowling&#8217;s description of her characters was influenced 
    in any way by the actors assigned to the roles. 

<p>
    The book was also the first to come out after the 9/11 
    terrorist attacks, which lent a heavy subtext to the plot of the novel 
    &#8211; which concerns a cultural and political establishment determined to 
    do anything but deal with the evildoers in its midst. 

<p>
    But if there is one thing Harry 
    Potter&nbsp;fans remember about the book, it is 
    that it was something of a disappointment. Despite its length, and despite 
    the fact that a significant character dies, not a lot seems to have 
    happened by the end. And this is a problem the movie version never really 
    overcomes. 

<p>
    The previous film, Harry 
    Potter and the Goblet of Fire, ended on a dark 
    and serious note &#8211; with the return of Voldemort (Ralph Fiennes) and 
    the murder of a student by his hands. These actions were witnessed only by 
    Voldemort&#8217;s followers, known as the Death Eaters &#8211; and by Harry 
    Potter (Daniel Radcliffe), who barely escaped alive. 

<p>
    To this darkness, the new film adds a sense of mounting 
    frustration. Just as the greatest trick the devil ever played was to 
    convince the world he did not exist, so too Voldemort keeps a low profile 
    throughout most of this story &#8211; allowing the wizarding world to think 
    rumours of his return have been greatly exaggerated. 

<p>
    Thus, when Harry and his mentor, Hogwarts Headmaster 
    Albus Dumbledore (Michael Gambon), try to warn their fellow wizards that 
    Voldemort has come back, and that the wizarding world needs to prepare for 
    the inevitable showdown, they are accused &#8211; by the government, the 
    media and their friends &#8211; of lying and fearmongering. 

<p>
    Chief among these accusers is Dolores Umbridge (Vera Drake&#8217;s Imelda 
    Staunton) &#8211; an official from the Ministry of Magic who has been 
    appointed the new Defence Against the Dark Arts teacher, so the government 
    can keep an eye on Dumbledore. 



<p align=""right""><span style=""font-size: 90%;""><a href=""#articletop"">Continue article >></a></span>
</td><td width=""50%"" valign=""top"">

<p>
    Unlike previous DADA teachers, who were all buffoonish 
    or sympathetic, Umbridge is a genuinely nasty person. Her exceedingly 
    proper etiquette and her demure sense of style &#8211; the smiles, the wee 
    laugh, the pink clothes and the china plates with cute animated kittens 
    &#8211; mask a cruel authoritarianism. She is, in fact, a sadist. 

<p>
    Thanks to story elements such as these, the film 
    version of Phoenix&nbsp;was 
    never going to be the fun, escapist lark the previous movies were. But one 
    still cannot help but think the film could have been more than what it is. 

<p>
    Where, for example, is the sense of wonder and 
    enchantment? Once again, Harry learns a few new things about Hogwarts and 
    the wizarding world in general; but there is little sense of awe about it 
    this time. For example, when a door to a secret room appears in the wall of 
    one corridor, it is a convenient plot device, nothing more. 

<p>
    And where is the humour? At times, the film aims for a 
    little levity; but more often than not, these moments feel forced, dutiful 
    &#8211; even recycled from earlier movies. 

<p>
    Worst of all, with the exception of a few scenes 
    featuring Alan Rickman&#8217;s delectably devious Severus Snape, there are 
    few &#8211; if any &#8211; of the &#8216;grace notes&#8217; which have made 
    each of the previous films memorable in their own way. Indeed, it is 
    striking how this film brings together so many talented British actors, yet 
    gives them so little to do. 

<p>
    This may be because the film is directed by David 
    Yates, who is the fourth director to work on this series and the first with 
    no experience on a Hollywood movie. But a bigger problem is probably the 
    screenplay, by Michael Goldenberg. The earlier films, all of which were 
    written by Steven Kloves, had to leave out bits of the books, and sometimes 
    the omissions were puzzling &#8211; but they were never this clumsy. 

<p>
    At a few points in this film, we get flashbacks to 
    Harry&#8217;s younger days &#8211; to scenes from the earlier movies. It is 
    startling to realize just how much growing up Harry has done since the 
    first film came out six years ago.  

<p>
    It is also sobering to think that a series which began 
    with such potential is beginning to show serious signs of sequel fatigue. 
    Let us hope that the franchise gets its second wind, and soon."
"2329","
   
    By Peter T. Chattaway 
 



<p>
    <img src=""http://www.canadianchristianity.com/film/graphics/peterimage2.jpg"" width=""92"" height=""135"" border=""0"" align=""left"" hspace=""2"">THERE is a lot that could be said about Harry Potter and the Order of the Phoenix . It is the longest of the books in J.K. Rowling&#8217;s 
    phenomenally popular series, yet it is also the shortest of the five movies 
    that have come out so far. 

<p>
    The book, which came out in 2003, was the first to be 
    written after the movies went into production &#8211; and it is tempting to 
    wonder whether Rowling&#8217;s description of her characters was influenced 
    in any way by the actors assigned to the roles. 

<p>
    The book was also the first to come out after the 9/11 
    terrorist attacks, which lent a heavy subtext to the plot of the novel 
    &#8211; which concerns a cultural and political establishment determined to 
    do anything but deal with the evildoers in its midst. 

<p>
    But if there is one thing Harry 
    Potter&nbsp;fans remember about the book, it is 
    that it was something of a disappointment. Despite its length, and despite 
    the fact that a significant character dies, not a lot seems to have 
    happened by the end. And this is a problem the movie version never really 
    overcomes. 

<p>
    The previous film, Harry 
    Potter and the Goblet of Fire, ended on a dark 
    and serious note &#8211; with the return of Voldemort (Ralph Fiennes) and 
    the murder of a student by his hands. These actions were witnessed only by 
    Voldemort&#8217;s followers, known as the Death Eaters &#8211; and by Harry 
    Potter (Daniel Radcliffe), who barely escaped alive. 

<p>
    To this darkness, the new film adds a sense of mounting 
    frustration. Just as the greatest trick the devil ever played was to 
    convince the world he did not exist, so too Voldemort keeps a low profile 
    throughout most of this story &#8211; allowing the wizarding world to think 
    rumours of his return have been greatly exaggerated. 

<p>
    Thus, when Harry and his mentor, Hogwarts Headmaster 
    Albus Dumbledore (Michael Gambon), try to warn their fellow wizards that 
    Voldemort has come back, and that the wizarding world needs to prepare for 
    the inevitable showdown, they are accused &#8211; by the government, the 
    media and their friends &#8211; of lying and fearmongering. 

<p>
    Chief among these accusers is Dolores Umbridge (Vera Drake&#8217;s Imelda 
    Staunton) &#8211; an official from the Ministry of Magic who has been 
    appointed the new Defence Against the Dark Arts teacher, so the government 
    can keep an eye on Dumbledore. 



<p align=""right""><span style=""font-size: 90%;""><a href=""#articletop"">Continue article >></a></span>
</td><td width=""50%"" valign=""top"">

<p>
    Unlike previous DADA teachers, who were all buffoonish 
    or sympathetic, Umbridge is a genuinely nasty person. Her exceedingly 
    proper etiquette and her demure sense of style &#8211; the smiles, the wee 
    laugh, the pink clothes and the china plates with cute animated kittens 
    &#8211; mask a cruel authoritarianism. She is, in fact, a sadist. 

<p>
    Thanks to story elements such as these, the film 
    version of Phoenix&nbsp;was 
    never going to be the fun, escapist lark the previous movies were. But one 
    still cannot help but think the film could have been more than what it is. 

<p>
    Where, for example, is the sense of wonder and 
    enchantment? Once again, Harry learns a few new things about Hogwarts and 
    the wizarding world in general; but there is little sense of awe about it 
    this time. For example, when a door to a secret room appears in the wall of 
    one corridor, it is a convenient plot device, nothing more. 

<p>
    And where is the humour? At times, the film aims for a 
    little levity; but more often than not, these moments feel forced, dutiful 
    &#8211; even recycled from earlier movies. 

<p>
    Worst of all, with the exception of a few scenes 
    featuring Alan Rickman&#8217;s delectably devious Severus Snape, there are 
    few &#8211; if any &#8211; of the &#8216;grace notes&#8217; which have made 
    each of the previous films memorable in their own way. Indeed, it is 
    striking how this film brings together so many talented British actors, yet 
    gives them so little to do. 

<p>
    This may be because the film is directed by David 
    Yates, who is the fourth director to work on this series and the first with 
    no experience on a Hollywood movie. But a bigger problem is probably the 
    screenplay, by Michael Goldenberg. The earlier films, all of which were 
    written by Steven Kloves, had to leave out bits of the books, and sometimes 
    the omissions were puzzling &#8211; but they were never this clumsy. 

<p>
    At a few points in this film, we get flashbacks to 
    Harry&#8217;s younger days &#8211; to scenes from the earlier movies. It is 
    startling to realize just how much growing up Harry has done since the 
    first film came out six years ago.  

<p>
    It is also sobering to think that a series which began 
    with such potential is beginning to show serious signs of sequel fatigue. 
    Let us hope that the franchise gets its second wind, and soon.