"Zuma to campaign on IFP turf</h1>
        <div class=""ArticleDate""><em>28 October 2008, 18:34</em></div>
        <div class=""ArticleMPUHolder"">
          <h5 class=""ArticleViewRelatedHeader"">Related Articles</h5>
          <div class=""RelatedArticleFeatureLinks"">
            <ul>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081028121650801C106775"" title=""Zuma: don't worry about defactors"">Zuma: don't worry about defactors</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081027173143418C760814"" title=""Zuma visits Free State"">Zuma visits Free State</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081027054411683C812608"" title=""ANC, IFP clash during campaign in KZN"">ANC, IFP clash during campaign in KZN</a></li>
            </ul>
          </div>
          <div class=""SquareAdHolder""><script type=""text/javascript"">DisplayAds('MPUAV');</script></div>
        </div>
        <div class=""ArticleContent"">
	  ANC President Jacob Zuma will join his party's election campaign in KwaZulu-Natal when he holds a rally in the heart of territory controlled by the Inkatha Freedom Party.<br />
<br />
Spokesperson Nomfundo Mcetywa said Zuma's electioneering activities would start off on Friday with Zuma meeting local business people in the town of St Lucia.<br />
<br />
He then heads to Jozini where he will hold a meeting with traditional leader, farmers, civil servants, religious and community leaders.<br />
<br />
Later on the same day he is expected to hold a rally at the sports ground in Manguzi near the Mozambican border.<br />
<br />
On Saturday Zuma will be in the Newcastle area where he will visit an informal settlement before attending a church service, followed by a rally at the Osizweni stadium.<br />
<br />
Mcetywa said: ""As the ANC in KwaZulu-Natal we believe that we can win this province by a 60 percent majority and believe that we have made major inroads in the areas of the north. The ANC president will during his two-day visit conduct door to door campaigns where he will listen to the communities concerns with regards to issues relating to service delivery.""<br />
<br />
St Lucia, Jozini and Manguzi are in the Umkhanyakude district where the IFP won 69 percent of the popular vote in the 2004 election and the  ANC nearly 25 percent of the vote.<br />
<br />
Newcastle is in the Amajuba district where the ANC won 46 percent of the votes to the IFP's 36 percent. - Sapa