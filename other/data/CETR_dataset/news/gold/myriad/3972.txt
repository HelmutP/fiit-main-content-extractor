"
			FOOD PRICES FALL AT LAST 		</h1>
	</div>



	<div style=""width: 300px; float:left;"">
		<div class=""articleFirstImage"">
						<img src=""http://images.dailyexpress.co.uk/img/dynamic/1/285x214/68239_1.jpg"" width=""285"" class=""smallArticleImage"" alt=""Story Image"" />
						<p><br /></p>
			<p class=""articleFirstImageCaption"">
				BOOST: Cheaper shopping bills will bring cheer to millions			</p>
						<a href=""/myexpress/"">
				<img src=""http://images.dailyexpress.co.uk/img/common/myExpressLoginRegisterSmall.gif"" class=""myExpressSmall"" />
			</a>
					</div>
	</div>
	
	<div class=""left"" style=""width:230px; height: 20px; margin: 10px 0 0 0;"">
		<p class=""date"">
			Tuesday October 28,2008		</p>
	</div>
	<div class=""left"" style=""width:230px; height: auto; border-bottom: solid 1px #000; margin: 0 0 5px 0;"">
				<h4 class=""left padding7north"" style=""font-size: 12px;"">
			By <span class=""bold"">Louise Barnett</span>
		</h4>
			</div>
	 	<div class=""left"" style=""width:230px; height: 30px"">
		<p>
			<img src=""http://images.dailyexpress.co.uk/img/common/commentBubble.gif"" alt=""Comment Speech Bubble"" class=""commentBubble"" />
			<a href=""/comments/add/68239"">Have your say(5)</a>
		</p>
	</div>
		<div style=""margin: 0 0 15px 0;"">
		<p class=""introcopy"">STRUGGLING shoppers received a huge boost last night with figures showing the cost of several popular food items has started to fall.

</p>
	</div>





			<div class=""padding7north"" style=""font-size: 12px; text-align: justify; margin: 4px 5px 4px 0; line-height: 17px;"">
				<p class=""storycopy"">


<div>A basket of a dozen everyday meat and fish products is now cheaper than last month ? the first time prices have dropped for a year.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>It is a strong signal that food price inflation, which has rocketed over the past 12 months, has finally peaked and that further cuts are on the way.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
                     

<div>British Retail Consortium spokesman Richard Dodd said it was further evidence that the worst was over for hard-pressed families. <br>                                                               <br>                                                                                    ?Now that many of the costs, such as oil and wheat, which pushed up prices earlier in the year are falling, retailers are moving quickly to pass the benefits on,? he said.<br> 
</p>
<div style=""width: 534px;display:block;"">
	<table cellpadding=""0"" cellspacing=""0"" style=""clear:both;float:none;"">
		<tr>
			<td>
				<img src=""/img/dynamic/1/x190/68239_2.jpg"" />
			</td>
		</tr>
		<tr>
			<td class=""moreImagesCaption"">
				The news is a strong sign that food price inflation has peaked
			</td>
		</tr>
	</table>
</div>
<p class=""storycopy"">                                                            <br>                                                                                    <a style=""font-weight: bold;"" href=""http://www.dailyexpress.co.uk/money/yourmoney""></a>?The good news is the downward pressure on prices appears to have further to go.?&nbsp;</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Food industry bible The Grocer magazine carried out a price comparison at the UK?s four major supermarkets.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>It found that a basket of 12 meat and fish products was 0.4 per cent cheaper in September than in August.&nbsp;</div> </p>
<div class=""articlePullQuote"">
		<table cellpadding=""0"" cellspacing=""0"" style=""clear:both;float:none;"">
		<tr>
		<td>
		<div class=""articleLeftApostrophe left"">
			<img src=""http://images.dailyexpress.co.uk/img/apostropheLeft.jpg"" alt=""�"" style=""margin: 0 0px 0 0;"" />
		</div>	
			<div class=""pullQuoteText"">It is a promising sign and, hopefully, it will keep going as well</div>
		<div class=""articleRightApostrophe left"">
			<img src=""http://images.dailyexpress.co.uk/img/apostropheRight.jpg"" alt=""�"" style=""margin: 15px 0 0 0px;"" />
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class=""personQuotedBox"" style=""margin: 20px 0 0 0;"">
			<p class=""personQuoted"">James Ball, of The Grocer</p>
		</div>
		</td>
		</tr>
		</table>
</div><p class=""storycopy"">                                                           
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>The drop is significant because it is the first time meat has become cheaper month-on- month this year.&nbsp;</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>A total of seven products fell in price, with sausages seeing the biggest fall, down 7.2 per cent last month. <br>                                                               <br>                                                                                    A pack of thick pork sausages is 9p cheaper now at an average �1.16.</div> <form action=""/posts/search/"">
<input type=""hidden"" name=""where"" value=""section"" /><input type=""hidden"" name=""section"" value=""1"" />
	<div class=""newsSearch"">				
		<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""534"">
			<tr>
				<td>
					<img src=""http://images.dailyexpress.co.uk/img/dynamic/sections/searchbox/1.gif"" class=""newsSearchbar"" />
				</td>
				<td>
					<div class=""separatorTextBold"" style=""margin:12px 0 0 0;"">
						SEARCH UK NEWS for:
					</div>
				</td>
				<td style=""width:125px;"">
					<input type=""text"" style=""margin: 10px 0 0 5px;width:120px;"" name=""keyword"" />
				</td>
				<td>
					<input type=""image"" src=""http://images.dailyexpress.co.uk/img/common/buttons/newsSearchButton.gif"" style=""margin:0; padding:0;"" />
				</td>
			</tr>
		</table>
	</div>
</form><div class=""padding7north""><p class=""storycopy"">                                                             
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Pork chops are 20p a kilo cheaper at �5.01 per kilo ? a 3.8 per cent fall.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                              
<div>Fresh beef sirloin steak is 36p a kilo cheaper at �15.15 ? a drop of 2.3 per cent.&nbsp;</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>The price of lamb chops, skinless chicken breast fillets, unsmoked back bacon and whole free-range chicken all fell slightly while whole trout stayed the same.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Lean beef steak mince, organic beef mince, salmon fillets and unsmoked gammon joint are all more expensive.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                              
<div>James Ball, of The Grocer, said meat was falling in price due to cheaper oil and animal feed bringing down production costs.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>?It has been the fastest to go up and it has been one of the first categories where we have actually seen a month-on-month price fall,? he said. <br>                                                              <br>                                                                                    It is a promising sign and, hopefully, it will keep going as well.?</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Despite the price falls, the meat and fish products are still a massive 16.4 per cent more expensive than last year.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Asda boss Andy Bond said last night: ?Food inflation has definitely peaked. In fact, shoppers are beginning to see the price of staple products like bread falling again.?</div>                                                             
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>The wholesale cost of other key commodities like coffee, cocoa beans and palm oil have also fallen in the past week.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Charles Davis, of the Centre for Economics and Business Research, predicted that fierce competition between UK food stores would help falling wholesale costs feed through to shop shelf prices.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                              
<div>Latest figures from the Office for National Statistics also indicated food inflation had run out of steam. <br>                                                               <br>                                                                                    Food prices rose 12.7 per cent in the year to September, down from a 14.5 per cent increase in the year to August."
"3972","
			By <span class=""bold"">Louise Barnett</span>
		</h4>
			</div>
	 	<div class=""left"" style=""width:230px; height: 30px"">
		<p>
			<img src=""http://images.dailyexpress.co.uk/img/common/commentBubble.gif"" alt=""Comment Speech Bubble"" class=""commentBubble"" />
			<a href=""/comments/add/68239"">Have your say(5)</a>
		</p>
	</div>
		<div style=""margin: 0 0 15px 0;"">
		<p class=""introcopy"">STRUGGLING shoppers received a huge boost last night with figures showing the cost of several popular food items has started to fall.

</p>
	</div>





			<div class=""padding7north"" style=""font-size: 12px; text-align: justify; margin: 4px 5px 4px 0; line-height: 17px;"">
				<p class=""storycopy"">


<div>A basket of a dozen everyday meat and fish products is now cheaper than last month ? the first time prices have dropped for a year.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>It is a strong signal that food price inflation, which has rocketed over the past 12 months, has finally peaked and that further cuts are on the way.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
                     

<div>British Retail Consortium spokesman Richard Dodd said it was further evidence that the worst was over for hard-pressed families. <br>                                                               <br>                                                                                    ?Now that many of the costs, such as oil and wheat, which pushed up prices earlier in the year are falling, retailers are moving quickly to pass the benefits on,? he said.<br> 
</p>
<div style=""width: 534px;display:block;"">
	<table cellpadding=""0"" cellspacing=""0"" style=""clear:both;float:none;"">
		<tr>
			<td>
				<img src=""/img/dynamic/1/x190/68239_2.jpg"" />
			</td>
		</tr>
		<tr>
			<td class=""moreImagesCaption"">
				The news is a strong sign that food price inflation has peaked
			</td>
		</tr>
	</table>
</div>
<p class=""storycopy"">                                                            <br>                                                                                    <a style=""font-weight: bold;"" href=""http://www.dailyexpress.co.uk/money/yourmoney""></a>?The good news is the downward pressure on prices appears to have further to go.?&nbsp;</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Food industry bible The Grocer magazine carried out a price comparison at the UK?s four major supermarkets.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>It found that a basket of 12 meat and fish products was 0.4 per cent cheaper in September than in August.&nbsp;</div> </p>
<div class=""articlePullQuote"">
		<table cellpadding=""0"" cellspacing=""0"" style=""clear:both;float:none;"">
		<tr>
		<td>
		<div class=""articleLeftApostrophe left"">
			<img src=""http://images.dailyexpress.co.uk/img/apostropheLeft.jpg"" alt=""�"" style=""margin: 0 0px 0 0;"" />
		</div>	
			<div class=""pullQuoteText"">It is a promising sign and, hopefully, it will keep going as well</div>
		<div class=""articleRightApostrophe left"">
			<img src=""http://images.dailyexpress.co.uk/img/apostropheRight.jpg"" alt=""�"" style=""margin: 15px 0 0 0px;"" />
		</div>
		</td>
		</tr>
		<tr>
		<td>
		<div class=""personQuotedBox"" style=""margin: 20px 0 0 0;"">
			<p class=""personQuoted"">James Ball, of The Grocer</p>
		</div>
		</td>
		</tr>
		</table>
</div><p class=""storycopy"">                                                           
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>The drop is significant because it is the first time meat has become cheaper month-on- month this year.&nbsp;</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>A total of seven products fell in price, with sausages seeing the biggest fall, down 7.2 per cent last month. <br>                                                               <br>                                                                                    A pack of thick pork sausages is 9p cheaper now at an average �1.16.</div> <form action=""/posts/search/"">
<input type=""hidden"" name=""where"" value=""section"" /><input type=""hidden"" name=""section"" value=""1"" />
	<div class=""newsSearch"">				
		<table border=""0"" cellpadding=""0"" cellspacing=""0"" width=""534"">
			<tr>
				<td>
					<img src=""http://images.dailyexpress.co.uk/img/dynamic/sections/searchbox/1.gif"" class=""newsSearchbar"" />
				</td>
				<td>
					<div class=""separatorTextBold"" style=""margin:12px 0 0 0;"">
						SEARCH UK NEWS for:
					</div>
				</td>
				<td style=""width:125px;"">
					<input type=""text"" style=""margin: 10px 0 0 5px;width:120px;"" name=""keyword"" />
				</td>
				<td>
					<input type=""image"" src=""http://images.dailyexpress.co.uk/img/common/buttons/newsSearchButton.gif"" style=""margin:0; padding:0;"" />
				</td>
			</tr>
		</table>
	</div>
</form><div class=""padding7north""><p class=""storycopy"">                                                             
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Pork chops are 20p a kilo cheaper at �5.01 per kilo ? a 3.8 per cent fall.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                              
<div>Fresh beef sirloin steak is 36p a kilo cheaper at �15.15 ? a drop of 2.3 per cent.&nbsp;</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>The price of lamb chops, skinless chicken breast fillets, unsmoked back bacon and whole free-range chicken all fell slightly while whole trout stayed the same.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Lean beef steak mince, organic beef mince, salmon fillets and unsmoked gammon joint are all more expensive.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                              
<div>James Ball, of The Grocer, said meat was falling in price due to cheaper oil and animal feed bringing down production costs.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>?It has been the fastest to go up and it has been one of the first categories where we have actually seen a month-on-month price fall,? he said. <br>                                                              <br>                                                                                    It is a promising sign and, hopefully, it will keep going as well.?</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Despite the price falls, the meat and fish products are still a massive 16.4 per cent more expensive than last year.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Asda boss Andy Bond said last night: ?Food inflation has definitely peaked. In fact, shoppers are beginning to see the price of staple products like bread falling again.?</div>                                                             
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>The wholesale cost of other key commodities like coffee, cocoa beans and palm oil have also fallen in the past week.</div>                                                               
<div><br class=""webkit-block-placeholder""></div>                                                               
<div>Charles Davis, of the Centre for Economics and Business Research, predicted that fierce competition between UK food stores would help falling wholesale costs feed through to shop shelf prices.</div>                                                              
<div><br class=""webkit-block-placeholder""></div>                                                              
<div>Latest figures from the Office for National Statistics also indicated food inflation had run out of steam. <br>                                                               <br>                                                                                    Food prices rose 12.7 per cent in the year to September, down from a 14.5 per cent increase in the year to August.