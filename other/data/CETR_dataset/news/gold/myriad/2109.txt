"Character trumping experience</h1>


<div id=""articleBodyTop"">

<div id=""articleBodyImageH"">
<span id=""articleImageH""></span>
<img src=""http://cache.boston.com/resize/bonzai-fba/Globe_Photo/2008/10/27/1225152902_8273/539w.jpg"" title=""Jimmy Stewart in ''Mr. Smith Goes to Washington.''"" height=""355"" width=""539"" alt=""Jimmy Stewart in ''Mr. Smith Goes to Washington.''"" border=""0"" />
Jimmy Stewart in ''Mr. Smith Goes to Washington.''
(The Boston Globe/File)
</div></div>


<div class=""utility"">
    <span id=""byline"">
        By 
        H.D.S. Greenway
    </span>
    <div class=""cf""></div>
    <span id=""dateline"">
          
          
          
          October 28, 2008
    </span>
    <div class=""cf""></div>
        <div id=""tools"">
                <ul>
                        <li id=""shareEmail""><a class=""etaf""
    href=""javascript:openWindow('http://tools.boston.com/pass-it-on?story_url=http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience','mailit','scrollbars,resizable,width=770,height=450');"">Email</a><span class=""listPipe etafPipe"">|</span></li>
                        <li id=""sharePrint""><a
    href=""http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience?mode=PF"">Print</a><span class=""listPipe"">|</span></li>
                        <li id=""shareSingle""><a onclick=""switchSingleOn()"" href=""#"">Single Page</a><span class=""listPipe"">|</span></li>
                        <li id=""toolsYahooB""><script
    showbranding=""0"" src=""http://d.yimg.com/ds/badge.js""
    badgetype=""text"">bostoncom751:http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience/</script><span class=""listPipe"">|</span></li>
                        <li id=""toolsShareThis""><script type=""text/javascript"" src=""http://w.sharethis.com/widget/?tabs=web%2Cpost&amp;charset=utf-8&amp;services=%2Cfacebook%2Cdigg%2Cdelicious%2Creddit%2Cmixx%2Cnewsvine%2Ctechnorati%2Cstumbleupon&amp;style=default&amp;publisher=e1e0ea5a-a326-4731-b1d1-f21623043511""></script></li>
                </ul>
                <span class=""articleTextsize"">
                        Text size
                        <span class=""minus""><span onclick=""javascript:fontsizedown();"" class=""imageLink"">&ndash;</span></span>
                        <span class=""plus""><span onclick=""javascript:fontsizeup();"" class=""imageLink"">+</span></span>
                </span>
        </div><!-- end tools -->


</div><!-- End utility -->
</div><!-- End headTools -->
</div><!-- End articleHeader -->


<div id=""articleGraphs"">
<div id=""page1""><p>JOHN MCCAIN'S  choice of Sarah Palin as his running mate has been mocked and lauded as either a brilliant political move or an act of utter cynicism. We know that McCain originally wanted Joseph Lieberman, and it must be said that, despite Palin's unpreparedness, she has livened up the race whereas Lieberman would put the country into snooze mode.</p><p>Even though the Republican Party allegedly spent $150,000 in New York City to dress up Palin and her family, she still manages to represent a deep and historic schism in America, that of the small town versus the city.</p><p>In the early days of the republic, cities were seen as something suspect, perhaps a bit anti-American. Thomas Jefferson is said to have told James Madison that if cities were to become dominant in America ""we shall become as corrupt as Europe."" Two centuries later, William Jennings Bryan, in his famous ""cross of gold"" speech, summed up the rural-vs.-urban case by saying: ""Burn down your cities and leave our farms and your cities will spring up again as if by magic. But destroy our farms, and the grass will grow in the streets of every city in the country.""</p><p>As the frontier expanded, a new dimension of this schism was born: that of the stalwart west against the effete east.</p><p>Small-town innocence portrayed as virtue is a part of American popular culture, as movie director Frank Capra well knew. Jimmy Stewart, in Capra's ""It's a Wonderful Life,"" makes everyone want to live in Bedford Falls and hand the mean banker his comeuppance. Gary Cooper in ""Mr. Deeds Goes to Town"" personified the theme of the noble hick outwitting city slickers.</p><p>There is also a know-nothing theme running through our national legend: that the salt-of-the-earth American character can trump knowledge and sophistication when the chips are down.</p><p>Consider Alfred Hitchcock's 1940 film ""Foreign Correspondent."" War is looming in Europe, and the publisher of the New York Globe is unhappy with the dispatches he is getting from his foreign staff. He hears that he has a crime reporter in the city room who beat up a policeman while getting a story. ""Sounds ideal for Europe,"" the publisher says, and he calls Johnny Jones, played by Joel McCrae, up to his office.</p><p>""Ever been to Europe?""</p><p>""No,"" says Jones.</p><p>""What is your opinion of the present European crisis?""</p><p>""What crisis?"" asks Jones, which brings a smile to the publisher's face, and Jones gets the job instead of more experienced reporters on the staff.</p><p>Jones gets a big expense account and new clothes for the assignment, perhaps costing a bit less than the Palin makeover. But, naturally, the simple and guileless McCrae character prevails over the wily and wicked Europeans.</p><p>The ultimate in this genre is Capra's ""Mr. Smith Goes to Washington,"" in which a senator dies and Jimmy Stewart, as Jefferson Smith, a Boy Scout master, is appointed to the post because the corrupt bosses think he can be controlled. Stewart extols the virtues of the prairie, says ""gee whiz"" and ""doggone,"" and is referred to as ""Daniel Boone"" by his world-weary Senate secretary.</p><p>Sophisticates call him a ""yokel"" and a ""squirrel chaser,"" and wonder if he will spend his Senate years saving buffaloes. The press provokes him into making a fool of himself making bird calls, and he is referred to as ""an incompetent clown."" But in the end, Smith cannot be controlled, and in a  climactic Senate filibuster scene he exposes the evil wrongdoing to which he was supposed to accede.</p><p>Palin aspires to this image. She was chosen to ""put government back on the side of the people of Joe Six-pack, like me,"" she says.  But far from breaking free, she has been tightly controlled by the McCain campaign.</p><p>Watching Mr. Smith again for the fourth time the other day, I couldn't help think that the Stewart character reminded me more of Barack Obama  - character trumping experience  - than it did the pistol-packing Palin and her ""you betcha"" politics of bundled resentments. McCain chose ""country first"" as his campaign motto, but with Palin he got country and western instead."
"2109","Character trumping experience</h1>


<div id=""articleBodyTop"">

<div id=""articleBodyImageH"">
<span id=""articleImageH""></span>
<img src=""http://cache.boston.com/resize/bonzai-fba/Globe_Photo/2008/10/27/1225152902_8273/539w.jpg"" title=""Jimmy Stewart in ''Mr. Smith Goes to Washington.''"" height=""355"" width=""539"" alt=""Jimmy Stewart in ''Mr. Smith Goes to Washington.''"" border=""0"" />
Jimmy Stewart in ''Mr. Smith Goes to Washington.''
(The Boston Globe/File)
</div></div>


<div class=""utility"">
    <span id=""byline"">
        By 
        H.D.S. Greenway
    </span>
    <div class=""cf""></div>
    <span id=""dateline"">
          
          
          
          October 28, 2008
    </span>
    <div class=""cf""></div>
        <div id=""tools"">
                <ul>
                        <li id=""shareEmail""><a class=""etaf""
    href=""javascript:openWindow('http://tools.boston.com/pass-it-on?story_url=http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience','mailit','scrollbars,resizable,width=770,height=450');"">Email</a><span class=""listPipe etafPipe"">|</span></li>
                        <li id=""sharePrint""><a
    href=""http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience?mode=PF"">Print</a><span class=""listPipe"">|</span></li>
                        <li id=""shareSingle""><a onclick=""switchSingleOn()"" href=""#"">Single Page</a><span class=""listPipe"">|</span></li>
                        <li id=""toolsYahooB""><script
    showbranding=""0"" src=""http://d.yimg.com/ds/badge.js""
    badgetype=""text"">bostoncom751:http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience/</script><span class=""listPipe"">|</span></li>
                        <li id=""toolsShareThis""><script type=""text/javascript"" src=""http://w.sharethis.com/widget/?tabs=web%2Cpost&amp;charset=utf-8&amp;services=%2Cfacebook%2Cdigg%2Cdelicious%2Creddit%2Cmixx%2Cnewsvine%2Ctechnorati%2Cstumbleupon&amp;style=default&amp;publisher=e1e0ea5a-a326-4731-b1d1-f21623043511""></script></li>
                </ul>
                <span class=""articleTextsize"">
                        Text size
                        <span class=""minus""><span onclick=""javascript:fontsizedown();"" class=""imageLink"">&ndash;</span></span>
                        <span class=""plus""><span onclick=""javascript:fontsizeup();"" class=""imageLink"">+</span></span>
                </span>
        </div><!-- end tools -->


</div><!-- End utility -->
</div><!-- End headTools -->
</div><!-- End articleHeader -->


<div id=""articleGraphs"">
<div id=""page1""><p>JOHN MCCAIN'S  choice of Sarah Palin as his running mate has been mocked and lauded as either a brilliant political move or an act of utter cynicism. We know that McCain originally wanted Joseph Lieberman, and it must be said that, despite Palin's unpreparedness, she has livened up the race whereas Lieberman would put the country into snooze mode.</p><p>Even though the Republican Party allegedly spent $150,000 in New York City to dress up Palin and her family, she still manages to represent a deep and historic schism in America, that of the small town versus the city.</p><p>In the early days of the republic, cities were seen as something suspect, perhaps a bit anti-American. Thomas Jefferson is said to have told James Madison that if cities were to become dominant in America ""we shall become as corrupt as Europe."" Two centuries later, William Jennings Bryan, in his famous ""cross of gold"" speech, summed up the rural-vs.-urban case by saying: ""Burn down your cities and leave our farms and your cities will spring up again as if by magic. But destroy our farms, and the grass will grow in the streets of every city in the country.""</p><p>As the frontier expanded, a new dimension of this schism was born: that of the stalwart west against the effete east.</p><p>Small-town innocence portrayed as virtue is a part of American popular culture, as movie director Frank Capra well knew. Jimmy Stewart, in Capra's ""It's a Wonderful Life,"" makes everyone want to live in Bedford Falls and hand the mean banker his comeuppance. Gary Cooper in ""Mr. Deeds Goes to Town"" personified the theme of the noble hick outwitting city slickers.</p><p>There is also a know-nothing theme running through our national legend: that the salt-of-the-earth American character can trump knowledge and sophistication when the chips are down.</p><p>Consider Alfred Hitchcock's 1940 film ""Foreign Correspondent."" War is looming in Europe, and the publisher of the New York Globe is unhappy with the dispatches he is getting from his foreign staff. He hears that he has a crime reporter in the city room who beat up a policeman while getting a story. ""Sounds ideal for Europe,"" the publisher says, and he calls Johnny Jones, played by Joel McCrae, up to his office.</p><p>""Ever been to Europe?""</p><p>""No,"" says Jones.</p><p>""What is your opinion of the present European crisis?""</p><p>""What crisis?"" asks Jones, which brings a smile to the publisher's face, and Jones gets the job instead of more experienced reporters on the staff.</p><p>Jones gets a big expense account and new clothes for the assignment, perhaps costing a bit less than the Palin makeover. But, naturally, the simple and guileless McCrae character prevails over the wily and wicked Europeans.</p><p>The ultimate in this genre is Capra's ""Mr. Smith Goes to Washington,"" in which a senator dies and Jimmy Stewart, as Jefferson Smith, a Boy Scout master, is appointed to the post because the corrupt bosses think he can be controlled. Stewart extols the virtues of the prairie, says ""gee whiz"" and ""doggone,"" and is referred to as ""Daniel Boone"" by his world-weary Senate secretary.</p><p>Sophisticates call him a ""yokel"" and a ""squirrel chaser,"" and wonder if he will spend his Senate years saving buffaloes. The press provokes him into making a fool of himself making bird calls, and he is referred to as ""an incompetent clown."" But in the end, Smith cannot be controlled, and in a  climactic Senate filibuster scene he exposes the evil wrongdoing to which he was supposed to accede.</p><p>Palin aspires to this image. She was chosen to ""put government back on the side of the people of Joe Six-pack, like me,"" she says.  But far from breaking free, she has been tightly controlled by the McCain campaign.</p><p>Watching Mr. Smith again for the fourth time the other day, I couldn't help think that the Stewart character reminded me more of Barack Obama  - character trumping experience  - than it did the pistol-packing Palin and her ""you betcha"" politics of bundled resentments. McCain chose ""country first"" as his campaign motto, but with Palin he got country and western instead.</p><p><em>H.D.S. Greenway's column appears regularly in the Globe."
"2109","
        By 
        H.D.S. Greenway
    </span>
    <div class=""cf""></div>
    <span id=""dateline"">
          
          
          
          October 28, 2008
    </span>
    <div class=""cf""></div>
        <div id=""tools"">
                <ul>
                        <li id=""shareEmail""><a class=""etaf""
    href=""javascript:openWindow('http://tools.boston.com/pass-it-on?story_url=http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience','mailit','scrollbars,resizable,width=770,height=450');"">Email</a><span class=""listPipe etafPipe"">|</span></li>
                        <li id=""sharePrint""><a
    href=""http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience?mode=PF"">Print</a><span class=""listPipe"">|</span></li>
                        <li id=""shareSingle""><a onclick=""switchSingleOn()"" href=""#"">Single Page</a><span class=""listPipe"">|</span></li>
                        <li id=""toolsYahooB""><script
    showbranding=""0"" src=""http://d.yimg.com/ds/badge.js""
    badgetype=""text"">bostoncom751:http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience/</script><span class=""listPipe"">|</span></li>
                        <li id=""toolsShareThis""><script type=""text/javascript"" src=""http://w.sharethis.com/widget/?tabs=web%2Cpost&amp;charset=utf-8&amp;services=%2Cfacebook%2Cdigg%2Cdelicious%2Creddit%2Cmixx%2Cnewsvine%2Ctechnorati%2Cstumbleupon&amp;style=default&amp;publisher=e1e0ea5a-a326-4731-b1d1-f21623043511""></script></li>
                </ul>
                <span class=""articleTextsize"">
                        Text size
                        <span class=""minus""><span onclick=""javascript:fontsizedown();"" class=""imageLink"">&ndash;</span></span>
                        <span class=""plus""><span onclick=""javascript:fontsizeup();"" class=""imageLink"">+</span></span>
                </span>
        </div><!-- end tools -->


</div><!-- End utility -->
</div><!-- End headTools -->
</div><!-- End articleHeader -->


<div id=""articleGraphs"">
<div id=""page1""><p>JOHN MCCAIN'S  choice of Sarah Palin as his running mate has been mocked and lauded as either a brilliant political move or an act of utter cynicism. We know that McCain originally wanted Joseph Lieberman, and it must be said that, despite Palin's unpreparedness, she has livened up the race whereas Lieberman would put the country into snooze mode.</p><p>Even though the Republican Party allegedly spent $150,000 in New York City to dress up Palin and her family, she still manages to represent a deep and historic schism in America, that of the small town versus the city.</p><p>In the early days of the republic, cities were seen as something suspect, perhaps a bit anti-American. Thomas Jefferson is said to have told James Madison that if cities were to become dominant in America ""we shall become as corrupt as Europe."" Two centuries later, William Jennings Bryan, in his famous ""cross of gold"" speech, summed up the rural-vs.-urban case by saying: ""Burn down your cities and leave our farms and your cities will spring up again as if by magic. But destroy our farms, and the grass will grow in the streets of every city in the country.""</p><p>As the frontier expanded, a new dimension of this schism was born: that of the stalwart west against the effete east.</p><p>Small-town innocence portrayed as virtue is a part of American popular culture, as movie director Frank Capra well knew. Jimmy Stewart, in Capra's ""It's a Wonderful Life,"" makes everyone want to live in Bedford Falls and hand the mean banker his comeuppance. Gary Cooper in ""Mr. Deeds Goes to Town"" personified the theme of the noble hick outwitting city slickers.</p><p>There is also a know-nothing theme running through our national legend: that the salt-of-the-earth American character can trump knowledge and sophistication when the chips are down.</p><p>Consider Alfred Hitchcock's 1940 film ""Foreign Correspondent."" War is looming in Europe, and the publisher of the New York Globe is unhappy with the dispatches he is getting from his foreign staff. He hears that he has a crime reporter in the city room who beat up a policeman while getting a story. ""Sounds ideal for Europe,"" the publisher says, and he calls Johnny Jones, played by Joel McCrae, up to his office.</p><p>""Ever been to Europe?""</p><p>""No,"" says Jones.</p><p>""What is your opinion of the present European crisis?""</p><p>""What crisis?"" asks Jones, which brings a smile to the publisher's face, and Jones gets the job instead of more experienced reporters on the staff.</p><p>Jones gets a big expense account and new clothes for the assignment, perhaps costing a bit less than the Palin makeover. But, naturally, the simple and guileless McCrae character prevails over the wily and wicked Europeans.</p><p>The ultimate in this genre is Capra's ""Mr. Smith Goes to Washington,"" in which a senator dies and Jimmy Stewart, as Jefferson Smith, a Boy Scout master, is appointed to the post because the corrupt bosses think he can be controlled. Stewart extols the virtues of the prairie, says ""gee whiz"" and ""doggone,"" and is referred to as ""Daniel Boone"" by his world-weary Senate secretary.</p><p>Sophisticates call him a ""yokel"" and a ""squirrel chaser,"" and wonder if he will spend his Senate years saving buffaloes. The press provokes him into making a fool of himself making bird calls, and he is referred to as ""an incompetent clown."" But in the end, Smith cannot be controlled, and in a  climactic Senate filibuster scene he exposes the evil wrongdoing to which he was supposed to accede.</p><p>Palin aspires to this image. She was chosen to ""put government back on the side of the people of Joe Six-pack, like me,"" she says.  But far from breaking free, she has been tightly controlled by the McCain campaign.</p><p>Watching Mr. Smith again for the fourth time the other day, I couldn't help think that the Stewart character reminded me more of Barack Obama  - character trumping experience  - than it did the pistol-packing Palin and her ""you betcha"" politics of bundled resentments. McCain chose ""country first"" as his campaign motto, but with Palin he got country and western instead."
"2109","
        By 
        H.D.S. Greenway
    </span>
    <div class=""cf""></div>
    <span id=""dateline"">
          
          
          
          October 28, 2008
    </span>
    <div class=""cf""></div>
        <div id=""tools"">
                <ul>
                        <li id=""shareEmail""><a class=""etaf""
    href=""javascript:openWindow('http://tools.boston.com/pass-it-on?story_url=http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience','mailit','scrollbars,resizable,width=770,height=450');"">Email</a><span class=""listPipe etafPipe"">|</span></li>
                        <li id=""sharePrint""><a
    href=""http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience?mode=PF"">Print</a><span class=""listPipe"">|</span></li>
                        <li id=""shareSingle""><a onclick=""switchSingleOn()"" href=""#"">Single Page</a><span class=""listPipe"">|</span></li>
                        <li id=""toolsYahooB""><script
    showbranding=""0"" src=""http://d.yimg.com/ds/badge.js""
    badgetype=""text"">bostoncom751:http://www.boston.com/news/politics/2008/articles/2008/10/28/character_trumping_experience/</script><span class=""listPipe"">|</span></li>
                        <li id=""toolsShareThis""><script type=""text/javascript"" src=""http://w.sharethis.com/widget/?tabs=web%2Cpost&amp;charset=utf-8&amp;services=%2Cfacebook%2Cdigg%2Cdelicious%2Creddit%2Cmixx%2Cnewsvine%2Ctechnorati%2Cstumbleupon&amp;style=default&amp;publisher=e1e0ea5a-a326-4731-b1d1-f21623043511""></script></li>
                </ul>
                <span class=""articleTextsize"">
                        Text size
                        <span class=""minus""><span onclick=""javascript:fontsizedown();"" class=""imageLink"">&ndash;</span></span>
                        <span class=""plus""><span onclick=""javascript:fontsizeup();"" class=""imageLink"">+</span></span>
                </span>
        </div><!-- end tools -->


</div><!-- End utility -->
</div><!-- End headTools -->
</div><!-- End articleHeader -->


<div id=""articleGraphs"">
<div id=""page1""><p>JOHN MCCAIN'S  choice of Sarah Palin as his running mate has been mocked and lauded as either a brilliant political move or an act of utter cynicism. We know that McCain originally wanted Joseph Lieberman, and it must be said that, despite Palin's unpreparedness, she has livened up the race whereas Lieberman would put the country into snooze mode.</p><p>Even though the Republican Party allegedly spent $150,000 in New York City to dress up Palin and her family, she still manages to represent a deep and historic schism in America, that of the small town versus the city.</p><p>In the early days of the republic, cities were seen as something suspect, perhaps a bit anti-American. Thomas Jefferson is said to have told James Madison that if cities were to become dominant in America ""we shall become as corrupt as Europe."" Two centuries later, William Jennings Bryan, in his famous ""cross of gold"" speech, summed up the rural-vs.-urban case by saying: ""Burn down your cities and leave our farms and your cities will spring up again as if by magic. But destroy our farms, and the grass will grow in the streets of every city in the country.""</p><p>As the frontier expanded, a new dimension of this schism was born: that of the stalwart west against the effete east.</p><p>Small-town innocence portrayed as virtue is a part of American popular culture, as movie director Frank Capra well knew. Jimmy Stewart, in Capra's ""It's a Wonderful Life,"" makes everyone want to live in Bedford Falls and hand the mean banker his comeuppance. Gary Cooper in ""Mr. Deeds Goes to Town"" personified the theme of the noble hick outwitting city slickers.</p><p>There is also a know-nothing theme running through our national legend: that the salt-of-the-earth American character can trump knowledge and sophistication when the chips are down.</p><p>Consider Alfred Hitchcock's 1940 film ""Foreign Correspondent."" War is looming in Europe, and the publisher of the New York Globe is unhappy with the dispatches he is getting from his foreign staff. He hears that he has a crime reporter in the city room who beat up a policeman while getting a story. ""Sounds ideal for Europe,"" the publisher says, and he calls Johnny Jones, played by Joel McCrae, up to his office.</p><p>""Ever been to Europe?""</p><p>""No,"" says Jones.</p><p>""What is your opinion of the present European crisis?""</p><p>""What crisis?"" asks Jones, which brings a smile to the publisher's face, and Jones gets the job instead of more experienced reporters on the staff.</p><p>Jones gets a big expense account and new clothes for the assignment, perhaps costing a bit less than the Palin makeover. But, naturally, the simple and guileless McCrae character prevails over the wily and wicked Europeans.</p><p>The ultimate in this genre is Capra's ""Mr. Smith Goes to Washington,"" in which a senator dies and Jimmy Stewart, as Jefferson Smith, a Boy Scout master, is appointed to the post because the corrupt bosses think he can be controlled. Stewart extols the virtues of the prairie, says ""gee whiz"" and ""doggone,"" and is referred to as ""Daniel Boone"" by his world-weary Senate secretary.</p><p>Sophisticates call him a ""yokel"" and a ""squirrel chaser,"" and wonder if he will spend his Senate years saving buffaloes. The press provokes him into making a fool of himself making bird calls, and he is referred to as ""an incompetent clown."" But in the end, Smith cannot be controlled, and in a  climactic Senate filibuster scene he exposes the evil wrongdoing to which he was supposed to accede.</p><p>Palin aspires to this image. She was chosen to ""put government back on the side of the people of Joe Six-pack, like me,"" she says.  But far from breaking free, she has been tightly controlled by the McCain campaign.</p><p>Watching Mr. Smith again for the fourth time the other day, I couldn't help think that the Stewart character reminded me more of Barack Obama  - character trumping experience  - than it did the pistol-packing Palin and her ""you betcha"" politics of bundled resentments. McCain chose ""country first"" as his campaign motto, but with Palin he got country and western instead.</p><p><em>H.D.S. Greenway's column appears regularly in the Globe."
"2109","JOHN MCCAIN'S  choice of Sarah Palin as his running mate has been mocked and lauded as either a brilliant political move or an act of utter cynicism. We know that McCain originally wanted Joseph Lieberman, and it must be said that, despite Palin's unpreparedness, she has livened up the race whereas Lieberman would put the country into snooze mode.</p><p>Even though the Republican Party allegedly spent $150,000 in New York City to dress up Palin and her family, she still manages to represent a deep and historic schism in America, that of the small town versus the city.</p><p>In the early days of the republic, cities were seen as something suspect, perhaps a bit anti-American. Thomas Jefferson is said to have told James Madison that if cities were to become dominant in America ""we shall become as corrupt as Europe."" Two centuries later, William Jennings Bryan, in his famous ""cross of gold"" speech, summed up the rural-vs.-urban case by saying: ""Burn down your cities and leave our farms and your cities will spring up again as if by magic. But destroy our farms, and the grass will grow in the streets of every city in the country.""</p><p>As the frontier expanded, a new dimension of this schism was born: that of the stalwart west against the effete east.</p><p>Small-town innocence portrayed as virtue is a part of American popular culture, as movie director Frank Capra well knew. Jimmy Stewart, in Capra's ""It's a Wonderful Life,"" makes everyone want to live in Bedford Falls and hand the mean banker his comeuppance. Gary Cooper in ""Mr. Deeds Goes to Town"" personified the theme of the noble hick outwitting city slickers.</p><p>There is also a know-nothing theme running through our national legend: that the salt-of-the-earth American character can trump knowledge and sophistication when the chips are down.</p><p>Consider Alfred Hitchcock's 1940 film ""Foreign Correspondent."" War is looming in Europe, and the publisher of the New York Globe is unhappy with the dispatches he is getting from his foreign staff. He hears that he has a crime reporter in the city room who beat up a policeman while getting a story. ""Sounds ideal for Europe,"" the publisher says, and he calls Johnny Jones, played by Joel McCrae, up to his office.</p><p>""Ever been to Europe?""</p><p>""No,"" says Jones.</p><p>""What is your opinion of the present European crisis?""</p><p>""What crisis?"" asks Jones, which brings a smile to the publisher's face, and Jones gets the job instead of more experienced reporters on the staff.</p><p>Jones gets a big expense account and new clothes for the assignment, perhaps costing a bit less than the Palin makeover. But, naturally, the simple and guileless McCrae character prevails over the wily and wicked Europeans.</p><p>The ultimate in this genre is Capra's ""Mr. Smith Goes to Washington,"" in which a senator dies and Jimmy Stewart, as Jefferson Smith, a Boy Scout master, is appointed to the post because the corrupt bosses think he can be controlled. Stewart extols the virtues of the prairie, says ""gee whiz"" and ""doggone,"" and is referred to as ""Daniel Boone"" by his world-weary Senate secretary.</p><p>Sophisticates call him a ""yokel"" and a ""squirrel chaser,"" and wonder if he will spend his Senate years saving buffaloes. The press provokes him into making a fool of himself making bird calls, and he is referred to as ""an incompetent clown."" But in the end, Smith cannot be controlled, and in a  climactic Senate filibuster scene he exposes the evil wrongdoing to which he was supposed to accede.</p><p>Palin aspires to this image. She was chosen to ""put government back on the side of the people of Joe Six-pack, like me,"" she says.  But far from breaking free, she has been tightly controlled by the McCain campaign.</p><p>Watching Mr. Smith again for the fourth time the other day, I couldn't help think that the Stewart character reminded me more of Barack Obama  - character trumping experience  - than it did the pistol-packing Palin and her ""you betcha"" politics of bundled resentments. McCain chose ""country first"" as his campaign motto, but with Palin he got country and western instead.</p><p><em>H.D.S. Greenway's column appears regularly in the Globe.