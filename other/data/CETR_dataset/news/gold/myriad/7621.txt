"Torres still hamstrung</h1>
        <div class=""ArticleDate""><em>28 October 2008, 18:58</em></div>
        <div class=""ArticleMPUHolder"">
          <h5 class=""ArticleViewRelatedHeader"">Related Articles</h5>
          <div class=""RelatedArticleFeatureLinks"">
            <ul>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081024111511917C885115"" title=""Torres poser for the Reds"">Torres poser for the Reds</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081024065220919C942345"" title=""Red revolution"">Red revolution</a></li>
            </ul>
          </div>
          <div class=""SquareAdHolder""><script type=""text/javascript"">DisplayAds('MPUAV');</script></div>
        </div>
        <div class=""ArticleContent"">
	  Premier League leaders Liverpool are set to be without star striker Fernando Torres for the fourth successive match when Portsmouth arrive at Anfield on Wednesday.<br />
<br />
The Spain international is still recovering from a hamstring injury and, although he trained Tuesday, this weekend's match against Tottenham is seen as a more realistic target for a comeback  appearance.<br />
<br />
Torres's fellow forward Robbie Keane is still troubled by a groin problem.<br />
<br />
Portsmouth are set to again be without injured defenders Sol Campbell (hamstring) and Glen Johnson (pelvis) for what will be Tony Adams's first match as Pompey manager following his appointment as successor to Harry Redknapp.<br />
<br />
Younes Kaboul and Noe Pamarot are set to deputise again while Lassana Diarra is also in line to play at Anfield despite limping off near the end of last weekend's 1-1 draw at home to Fulham.<br />
<br />
Adams does have options in midfield with Sean Davis, back from a one-game suspension, and the fit-again Niko Kranjcar, who had six minutes as a substitute against Fulham, both in contention for  starting spots.<br />
<br />
Chelsea, looking to bounce back from the disappointment of seeing their 86-game unbeaten home run in the Premier League ended by Liverpool last weekend, will have Joe Cole available for  Wednesday's match away to Hull City.<br />
<br />
England midfielder Cole has been sidelined for the past three weeks by a foot injury.<br />
<br />
But Didier Drogba (knee), Michael Ballack (foot) and Michael Essien (knee) are all set to miss the journey to the KC Stadium.<br />
<br />
Australia midfielder Tim Cahill will return to the Everton side for their match away to fellow strugglers Bolton after serving a three-game ban for being sent-off in last month's Merseyside derby.<br />
<br />
Yakubu, injured during Everton's 1-1 draw with Manchester United last weekend and Nuno Valente, who also has a sore knee, will both have their fitness assessed on the morning of the match. - Sapa-AFP