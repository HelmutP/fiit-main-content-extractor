"ANC left 'cold' after resignations</h1>
        <div class=""ArticleDate""><em>28 October 2008, 17:40</em></div>
        <div class=""ArticleMPUHolder"">
          <h5 class=""ArticleViewRelatedHeader"">Related Articles</h5>
          <div class=""RelatedArticleFeatureLinks"">
            <ul>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081028155601828C156641"" title=""More top members quit ANC"">More top members quit ANC</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081026163745108C796586"" title=""Shikota, ANC members clash"">Shikota, ANC members clash</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=vn20081012082101801C447035"" title=""The art of the possible?"">The art of the possible?</a></li>
              <li><a href=""/index.php?fSectionId=&amp;fArticleId=nw20081028183133789C196880"" title=""Dexter's letter of resignation"">Dexter's letter of resignation</a></li>
            </ul>
          </div>
          <div class=""SquareAdHolder""><script type=""text/javascript"">DisplayAds('MPUAV');</script></div>
        </div>
        <div class=""ArticleContent"">
	  Senior ANC member Phillip Dexter and former ANC Women's caucus parliamentary chairperson Kiki Rwexana quit the ANC on Tuesday.<br />
<br />
They told reporters in Cape Town they would take part in the national convention called by Terror Lekota and Mbhazima Shilowa in Gauteng this weekend.<br />
<br />
Dexter recently resigned from the SA Communist Party.<br />
<br />
Rwexana is also a former deputy secretary general of the ANC Women's League (ANCWL), and is the first sitting ANC MP to resign from the party and from parliament.<br />
<br />
Dexter said he was leaving the ANC with a great deal of sadness, but the ANC had undergone a ""transformation"" since about 2004.<br />
<br />
Personal ambition, revenge, and vindictiveness had become the main driving force of the movement.<br />
<br />
""Dishonesty has really become the order of the day. Most alarming has been the peddling of many lies by current ANC leaders in recent times to justify political positions and decisions, and in particular, the election or removal of people to and from political office.""<br />
<br />
The current ANC leadership was at the forefront of undermining the Constitution, Dexter said.<br />
<br />
Rwexana said she too was sad to leave the party, which had been her second home for a long time.<br />
<br />
However, ""today the ANC is very cold...It depends on which side you are supporting.<br />
<br />
""The ANC we used to know is no more. The ANC that taught democracy in South Africa is no more...that taught respect to each, respect to the elderly, is no more.<br />
<br />
""There's hatred and anger in the ANC [now]. If you have a different view, you are no longer regarded as a member of the ANC. You are regarded as someone who is anti-ANC,"" she said.<br />
<br />
Speaking at the same briefing, Shilowa said work for the convention was progressing well and it was hoped to release the programme and list  of speakers on Thursday.<br />
<br />
All political parties in parliament had been invited to attend, as well as business, trade unions, and individuals, he said.<br />
<br />
Dexter also rejected suggestions he was a ""counter-revolutionary"" and the like.<br />
<br />
During the two terms he served on the ANC national executive committee (NEC), a number of terms on the SACP central committee, and on trade union executives, he never once felt that because he differed,  his position was in jeopardy or that he was going to be excluded in any  way from the organisation.<br />
<br />
""The first time that happened to me was when a number of individuals  spoke to me about who they wanted to elect as a leader in the [ANC's] Polokwane conference.<br />
<br />
""And I differed with them. I can tell you the day it happened and everything that happened after that day.<br />
<br />
""Because from that day onwards, I experienced nothing but people insulting me, accusing me of being a counter-revolutionary, labelling me, attacking me and spreading rumours about me.<br />
<br />
""And why, because I disagreed about this issue of electing somebody.""<br />
<br />
Dexter said it was a matter of public record that he was one of the people who disagreed with former president Thabo Mbeki on, among other things, macro-economic policy, and HIV and Aids.<br />
<br />
""Never once did he or anybody around him, suggest to me that there was no space for me in the organisation. In fact, to give credit, he would engage me.""<br />
<br />
Dexter said it was ironic that many of the people who today held leadership positions were those who used to ""hound me because we were accused of being anti-Thabo Mbeki"".<br />
<br />
""Those very same people today are the 100 percent Jacob Zuma people, who today, turn around and say Thabo Mbeki did this wrong and that wrong.<br />
<br />
""They never once raised those issues when he was apparently doing those things.""<br />
<br />
He also dismissed accusations that Lekota was ""anti-democratic"" when  he was ANC chair.<br />
<br />
In fact, Lekota had been too democratic and allowed people to speak ""forever"" at meetings.<br />
<br />
""So these kinds of claims really, honestly, I think you must take them with a pinch of salt,"" he said. - Sapa