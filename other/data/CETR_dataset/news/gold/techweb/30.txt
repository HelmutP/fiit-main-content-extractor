Several Japanese banks have reported that a spyware thief tapped compromised accounts more or less simultaneously, and spirited away hundreds of thousands of yen.
<P>
According to published reports in the Asahi Shimbun, one of Japan's largest dailies, a trio of banks--eBank, Mizuho Bank, and Japan Net Bank-- said that illegal transfers were made to the same receiving account, making it likely the work of just one thief.
<P>
Sources told the Shimbun that ""hundreds of thousands"" of yen were moved from the account of an Mizuho Bank customer on July 1, and 130,000 yen ($1,170). Losses to accounts at Japan Net Bank were not disclosed, but multiple transfers were made.
<P>
In the eBank incident, the customer reported receiving e-mail in mid-June with an attached file, bank officials told the newspaper. They speculated that spyware was installed on the customer's computer; the spyware probably had a keylogger component which watched for online bank log-in passwords, then transmitted them to the thief.
<P>
By the time eBank froze the customer's account, the transfer had already taken place, the bank said.