A new unpatched vulnerability in Microsoft Windows and an in-the-wild exploit appeared Wednesday as security firms raised their alarms to Critical.
<P>
The bug is in Windows' rendering of Windows Metafile (WMF) images, a component that's been patched three times in the last two years, most recently in November by the bulletin <a href= ""http://www.microsoft.com/technet/security/Bulletin/MS05-053.mspx"">MS05-053.</a> The newest flaw, however, is different enough from November's that fully-patched Windows XP SP2 and Windows Server 2003 machines can be compromised.
<P>
""This exploit is doing something a bit different,"" said Shane Coursen, a senior technical analyst with Moscow-based Kaspersky Labs. ""It looks like it affects the same DLL as MS05-053, but it's not overflowing the buffer."" According to Microsoft's MS05-053 bulletin, the November vulnerability was in an unchecked buffer.
<P>
Microsoft would only acknowledge that it's looking into the problem, the usual response from the Redmond, Wash.-based developer to news of zero-day exploits of its software.
<P>
""Microsoft is investigating new public reports of a possible vulnerability in Windows and will continue to investigate the public reports to help provide additional guidance for customers,"" said a Microsoft spokesperson. ""Upon completion of this investigation, Microsoft will take the appropriate action, which may include providing a fix through our monthly release process or issuing a security advisory, depending on customer needs.""
<P>
Security and vulnerability tracking companies' reactions were more dramatic: they immediately raised alert levels, both because the flaw was an unpatched <a href= ""http://www.techweb.com/encyclopedia/defineterm.jhtml?term=zero-day+exploit"">""zero-day""</a> bug, and also because exploits were already out and about. Danish security company Secunia, for instance, tagged the new flaw as ""Extremely critical,"" its highest warning; Symantec, meanwhile, gave it a rating of 9.4 on its 10-point scale for vulnerability alerts.
<P>
Multiple Web sites, said Ken Dunham, the director of Reston, Va.-based iDefense's rapid response team, are using a working exploit to compromise Windows machines. Attackers need only to cajole users into visiting sites planted with malicious WMF files, or get them to open such image files sent as e-mail attachments.
<P>
""WMF exploitation has taken off in the past twelve hours,"" said Dunham. ""It's likely that WMF exploitation will be very successful in the near term.""