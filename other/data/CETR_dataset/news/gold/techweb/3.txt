Music In Your Own Words (Miyow) Pty Ltd. is planning to develop a cellular phone application where consumers can purchase personalized tunes and send them as e-carol audio files to friends, the company said Thursday.
<P>
Requests from executives at the company's distributors, such as Arena Mobile in the U.K. and Mobile Media in Asia-Pacific, prompted the decision to create the platform, said Lars Harold, co-founder and chief executive officer at Miyow. ""It could take between 10 and 16 months to develop the platform, but work should begin shortly,"" Harold said. ""Perhaps, we'll have it ready by next Christmas."" 
<P>
Miyow, headquartered in Sydney, Australia, already offers ringtones for cellular phones where the name is sung as a personal message in the tune. The company also launched Singaname.com to deliver personalized Christmas e-carols in early December. There also is a plan to expand the selection to love ballads for Valentines Day. 
<P>
Today, the site offers four category of Christmas song: classic, funny, naughty and romantic. Once on the site, customers type in a name they want to insert in the song. A click of the mouse personalizes the lyrics with audio slices and text.
<P>
LyriMix is the software that customizes the songs. The proprietary application from Miyow contains thousands name recordings and background tracks. The software platform is comprised of recording software that plugs into studio software for musicians, a MySQL database running on <a href = ""http://www.techweb.com/encyclopedia/defineterm.jhtml?term=microsoft"">Microsoft</a> Corp.'s Windows operating system, and assembly algorithms where the songs are assembled. 
<P>
The files are stored in the <a href = ""http://www.techweb.com/encyclopedia/defineterm.jhtml?term=mysql"">MySQL</a> database as uncompressed wave files. Thousands of names have been prerecorded. The platform uses a Web-based application programming interface (API) to easy assemble the song with the name audio clip. The technology automatically assembles the song in about half a second. It took Miyow 18 months to develop the LyriMix software. The platform sits on a server hosted by GoDaddy.com, Harold said. 
<P>
For now, the songs are recorded by Miyow, but Harold said the company is in discussions with several music labels to also offer copyrighted tunes. 
<P>
Research firm IDC Corp. estimates four in 10 wireless subscribers will have purchased at least one ringtone by 2009, forecasting the market to reach nearly 100 million. 
<P>
Research firm Yankee Group puts the ringtone market in the United States this year at $458.3 million, up from $221.4 million in 2004.
<P>
Those looking for Miyow's personalized ringtones can find them through content provider MediaPlazza; or Arena Mobile, which is powered by the telecommunications carrier Orange U.K.; as well as several in Asia-Pacific, Harold said. ""We've began to sell the personalized ringtones earlier this year and have sold about 10,000 through our carriers,"" he said. ""In the U.K. they sell for between $3 and $4 for a 30-second ringtone. Pricing varies because it's set by the carrier.""