India's Tata Consultancy Services Ltd. (TCS) continued on a roll, predicting a doubling in sales in an 18 month period, while it prepares to bid on Tyco's gigantic undersea fiber-optic network.
<P>
Reporting a 52 percent rise in second quarter earnings Wednesday, TCS said its net income for the quarter reached $125.7 million (5.76 billion rupees), while revenues hit 24.30 billion rupees (about $1 billion.) ""We've had $1 billion in revenues for the first six months,"" TCS executive vice president Phiroz Vandrevala told the <i>Wall Street Journal</i> in an interview. ""We should definitely be a $2 billion company.""
<P>
U.S. offshore outsourcing is Tata's largest business area, and the firm's chief executive, S. Ramadorai, said resistance to the movement of IT services from the U.S. to India was fading. ""We no longer regard this as a hurdle,"" he was quoted as saying.
<P>
Tata launched its $1.2 billion IPO in July.
<P>
While the firm's largest customer&#8212;GE--accounted for nearly 16 percent of its business, GE's percentage of Tata's business actually declined slightly, as Tata signed up more foreign companies for its offshore-outsourcing business.
<P>
What's more, <i>The Indian Express</i> newspaper reported Wednesday that sources said Tata has lined up a $200 million war chest to bid for Tyco's fiber network. Indian outsourcing firms have been purchasing U.S. data communications networks at bargain basement prices, in the wake of the collapse of the U.S. telecom bubble in 2000.
<P>
Vandrevala said Tata has been picking up outsourcing business in telecommunications, banking and financial services. He noted that while its U. S. business represents some 60 percent of revenues, Tata has been gaining substantial new business in Europe and Asia.