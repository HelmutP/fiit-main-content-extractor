Expedia and Travelocity have earned top rankings overall in Consumer Reports' latest evaluation of the six largest independent travel Web sites. 
<P>
<P>
The non-profit magazine rated the Web sites based on privacy and security policies, customer service, clear disclosure of advertising relationships, usability, and content.
<P>
<P>
Expedia and Travelocity bested Cheap Tickets, One Travel, Orbitz, and TravelNow.com in the rankings. Orbitz ran close behind the leaders in promising to maintain consumer privacy, enabling easy navigation through the site, and providing a satisfying online experience. Expedia and Travelocity also led the pack in Consumer Reports last evaluation in September 2002.
<P>
<P>
However, none of the sites adequately disclosed their relationship with airline partners, making it nearly impossible to judge the objectivity of flight prices, Consumer Reports said.