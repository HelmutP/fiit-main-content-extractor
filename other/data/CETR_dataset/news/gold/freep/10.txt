


Longtime local chef <i>Rick Halberg,</i> 58, closed his highly regarded Emily's restaurant in Northville in 2006. It was a victim, after 12 years, of ""extenuating circumstances, the economy being the biggest one,"" he says. ""The fine-dining market seemed to have really dried up in this area."" The Culinary Institute of America graduate closed one chapter of his life and opened a new one: He became director of culinary services for Southfield-based Hiller's Markets, a new position for the local grocery chain. He's developing ready-to-go foods for Hiller's, which will open its seventh location in a few months in a remodeled Farmer Jack in Commerce Township.</p><p>


<b>QUESTION: How was it going from the high-end restaurant business to the mass market?</b></p><p>


<table id=""articlead"">
	<tr>
		<td style=""text-align: center; "">
			<p>Advertisement</p>
		
			
			<SCRIPT LANGUAGE=JavaScript>
			OAS_AD('ArticleFlex_1');
			
			</SCRIPT>
			
		</td>
	</tr>
</table>

ANSWER: It's not an easy transition. ... Certainly it's a much bigger challenge than I may have thought, but in today's economy, everything is. We are all fighting for a piece of the same pie.</p><p>


<b>Q: What's your biggest challenge?</b></p><p>


A: Keeping it fresh and getting people to try new things.</p><p>


<b>Q: Got an example?</b></p><p>


A: We do a really great farro salad. Farro is one of the ancient grains, an Italian grain. ... I think the taste is great -- nutty, chewy. ... I've had it made as a soup, like polenta, as a risotto -- very versatile and so good!</p><p>


<b>Q: What else are you doing?</b></p><p>


A: We're using a lot of whole grains and fresh vegetables. Natural chicken. The best-quality, healthiest products that we can offer. Zero trans fats whenever possible. ... We have a pretty wide range of foods, from rotisserie chickens to more interesting things like ... Moroccan-inspired tofu salad, paella salad, wild rice salad.</p><p>


<b>Q: How can you compete with gourmet palaces like Papa Joe's?</b></p><p>


It makes it very exciting and interesting. We don't have the facilities that Papa Joe's has. But we have some good, talented people, and we just keep pushing. ... Long-term, we are really trying to raise the bar with our prepared foods, to keep it fresh and keep it exciting.</p><p>


<b>By KATHLEEN O'GORMAN, FREE PRESS STAFF WRITER