BY MATT GOURAS</p>
				
<p class=""story_authortitle"" style=""padding-top: 0; "">ASSOCIATED PRESS</p>

				<p>


HELENA, Mont. -- The owner of a fast food restaurant in Montana's booming oil patch found himself outsourcing the drive-thru window to a Texas telemarketing firm, not because it's cheaper but because he can't find workers.</p><p>


Record low unemployment across parts of the West has created tough staffing conditions for business owners, who are being forced to boost wages or be creative to fill jobs.</p><p>


<table id=""articlead"">
	<tr>
		<td style=""text-align: center; "">
			<p>Advertisement</p>
		
			
			<SCRIPT LANGUAGE=JavaScript>
			OAS_AD('ArticleFlex_1');
			
			</SCRIPT>
			
		</td>
	</tr>
</table>

John Francis, who owns the McDonald's in Sidney, Mont., said he tried advertising and even offered up to $10 an hour to compete with oil field jobs. Yet the only calls were from other business owners who were upset they would have to raise wages, too. Of course, Francis' current employees also wanted a pay hike.</p><p>


Unemployment rates have been as low as 2% this year in Montana, and nearly as low in neighboring states. Economists cite such factors as an aging workforce and booming tourism economies for the tight labor market.</p><p>


Places like Montana have seen a steady climb in the nearly two decades since the timber and mining industry recession. The state approached double-digit unemployment levels in the 1980s.</p><p>


""This is actually the biggest economic story of our time, and we don't quite grasp it because it is 15 years in the making,"" said economist Larry Swanson, director of the O'Connor Center for the Rocky Mountain West at the University of Montana.</p><p>


The U.S. Department of Labor reports the mountain West region -- eight states along the Rocky Mountains -- hit an all-time low unemployment rate of 3.4% in May.</p><p>


The effects are everywhere. Logging equipment in Idaho sits idle. A shortage of lifeguards forced the City of Helena, Mont., to shorten hours at pools. A local paper in Jackson, Wyo., has page after page of help wanted ads.</p><p>


In Jackson Hole, the Four Seasons Resort still had openings in late July. The problem created longer hours and tougher working conditions for employees.</p><p>


Economists say there are a number of reasons why parts of the West are feeling the labor pinch. Established baby boomers have been moving into Montana for the mountain views and recreation, bringing with them money for new homes that fuel construction job growth, Swanson said.</p><p>


Along the way, young people moved away searching for bigger paychecks. Wages still lag behind other areas but are increasing slowly overall. Now, the aging workforce is unable to expand.</p><p>


Workers have benefited. Utah workers saw a 5.4% average wage increase in 2006, said Mark Knold, chief economist at the Utah Department of Workforce Services.</p><p>


But questions remain about how long the West can weather the problems.</p><p>


""The hardest thing is to keep the economy growing at a strong rate when you have a low unemployment rate,"" he said. ""Take a company that wants to expand. Where is the next worker going to come from?""