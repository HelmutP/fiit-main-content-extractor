BY JIM SCHAEFER and JOE SWICKARD</p>
				
<p class=""story_authortitle"" style=""padding-top: 0; "">FREE PRESS STAFF WRITERS</p>

				<p>


Death moved east.</p><p>


By April 2006, emergency workers in Philadelphia, Camden, N.J., and Delaware were swamped with overdoses. Heroin laced with fentanyl and sold as Al Capone, Flatline, Rest in Peace, Rolex and Exorcist was dropping addicts everywhere.</p><p>


<table id=""articlead"">
	<tr>
		<td style=""text-align: center; "">
			<p>Advertisement</p>
		
			
			<SCRIPT LANGUAGE=JavaScript>
			OAS_AD('ArticleFlex_1');
			
			</SCRIPT>
			
		</td>
	</tr>
</table>

As in Chicago, Philadelphia emergency workers were going through an astonishing amount of Narcan.</p><p>


""Today, when we give people Narcan, they're not coming out of it,"" Philadelphia Fire Capt. Richard Bossert said.</p><p>


""We had no idea what we were getting into.""</p><p>


Across the river, New Jersey was also counting dead bodies and, as in Philadelphia, addicts died with needles in their arms, unused dope still in the syringe. Emergency responders were handling 60 overdoses a day, compared with the usual 10 cases.</p><p>


Minutes after New Jersey health officials posted the jump in overdose deaths in an Internet alert, an official from Maryland called: People were dying there, too.