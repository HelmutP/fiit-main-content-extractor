Gas prices back over $3 a gallon</h5>
				
				
				<p class=""story_date"">August 21, 2007</p>
				
				<p class=""story_byline"" style=""padding-bottom: 0; "">By ALEJANDRO BODIPO-MEMBA</p>
				
<p class=""story_authortitle"" style=""padding-top: 0; "">FREE PRESS BUSINESS WRITER</p>

				<p>


Michigan gasoline prices hit $3 after rising 3 cents overnight, according to AAA Michigan.</p><p>


It is the first time fuel prices have averaged $3 or more since July 25 when it was $3.03 a gallon. The all-time record of $3.53 a gallon was set on May 26.</p><p>


<table id=""articlead"">
	<tr>
		<td style=""text-align: center; "">
			<p>Advertisement</p>
		
			
			<SCRIPT LANGUAGE=JavaScript>
			OAS_AD('ArticleFlex_1');
			
			</SCRIPT>
			
		</td>
	</tr>
</table>

Pump prices in Michigan are 22 cents higher than the $2.78 a gallon national average, according to the Web site <a href=""http://www.fuelgaugereport.com"" target=""_blank"">www.fuelgaugereport.com</a>.<br><p>?The main reason was the concern about Hurricane Dean and also because Labor Day is coming up and gas prices tend to go up as you get closer to any of the holidays,? said Nancy Cain, a spokeswoman for AAA Michigan in Dearborn. <br><p>Hurricane Dean hit central Mexico overnight and missed domestic oil production in the Gulf Coast region. As a result, oil prices on the New York Mercantile Exchange fell below $70 a barrel to $69.10 at 1 p.m. Tuesday.