BY SARAH A. WEBSTER</p>
				
<p class=""story_authortitle"" style=""padding-top: 0; "">FREE PRESS BUSINESS WRITER</p>

				<p>


General Motors Corp. Chairman and CEO Rick Wagoner -- not one to be out-Sync'd by crosstown rival Ford Motor Co. -- will be one of the keynote speakers at the 2008 International Consumer Electronics Show in Las Vegas.</p><p>


The show, which runs Jan. 7-10, is the world's largest trade show for technology.</p><p>


<table id=""articlead"">
	<tr>
		<td style=""text-align: center; "">
			<p>Advertisement</p>
		
			
			<SCRIPT LANGUAGE=JavaScript>
			OAS_AD('ArticleFlex_1');
			
			</SCRIPT>
			
		</td>
	</tr>
</table>

The lineup of keynote speakers includes Microsoft Chairman Bill Gates and leaders from Panasonic, Intel and other major firms.</p><p>


Wagoner's address is slated for Jan. 8, five days before the start of the news media preview at the 2008 North American International Auto Show in Detroit. (Public days will be Jan. 19-27.)</p><p>


""I'm sure it'll be about cool stuff they'll be doing,"" Gary Shapiro, president and CEO of the Consumer Electronics Association, told the Free Press.</p><p>


The Consumer Electronics Association has never hosted a keynote speaker from the auto industry, but Shapiro said automobiles are taking on increasing significance at the event.</p><p>


Last year, Mark Fields, Ford's president of the Americas, briefly joined Gates on stage during his keynote speech for the launch of Sync. That is a new wireless, hands-free communications and entertainment technology that runs on Microsoft's Windows Automotive operating system and will be available on Ford vehicles.</p><p>


The Sync system, which lets users to talk on the phone, text message and play digital audio files by voice command, is being launched in the 2008 Ford Focus.</p><p>


GM, known for its OnStar system, has said it is working on technology that also would integrate so-called nomadic electronic systems, such as mobile devices with Bluetooth connectivity, into vehicle electronics.