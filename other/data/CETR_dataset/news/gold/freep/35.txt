BY CHRIS KUCHARSKI</p>
				
<p class=""story_authortitle"" style=""padding-top: 0; "">FREE PRESS STAFF WRITER</p>

				<p>


<b>Company</b><b>:</b> Fuchsia Frog, 320 E. Maple, Birmingham.</p><p>


<b>Owner</b><b>:</b> Meg Ferron of Bloomfield Hills.</p><p>


<table id=""articlead"">
	<tr>
		<td style=""text-align: center; "">
			<p>Advertisement</p>
		
			
			<SCRIPT LANGUAGE=JavaScript>
			OAS_AD('ArticleFlex_1');
			
			</SCRIPT>
			
		</td>
	</tr>
</table>

<b>Background:</b> The store opened in May 2003.</p><p>


Ferron's mother came up with the name. ""We both loved the sound of it,"" Ferron said.</p><p>


Ferron had periodically worked in retail, and she wanted a creative environment.</p><p>


""I grew up surrounded by beautiful things,"" Ferron said.</p><p>


<b>Its niche</b><b>:</b> The store carries a range of gift items, from home accents to baby clothes. Some of its most popular items are Marye-Kelley decoupage frames that can be personalized for any occasion. The Marye-Kelley line also offers monogrammed wine coasters.</p><p>


The store has a baby registry and carries the Zutano line of baby clothing, which is popular because the soft cotton pieces come in a wide variety of prints and colors that can be continually mixed and matched.</p><p>


Items for the home include scented candles by Seda France, Sabre knives and serving pieces, and unique message pillows.</p><p>


Gift wrapping is available for an additional cost.</p><p>


<b>Hours:</b> 10 a.m.-6 p.m. Monday-Friday, 10 a.m.- 5 p.m. Saturday, closed Sunday.</p><p>


<b>For more information</b><b>:</b> Call 248-203-6550.