            <h>2004圣裘德精英赛冠军大卫-汤姆斯球袋中的装备

　　<p>新浪体育讯　汤姆斯从手术中完全复员回来了！汤姆斯开始星期天圣裘德精英赛决赛的时候已经领先7杆，没有人能阻挡他，即使大风天气也不能。虽然这一轮汤姆斯的成绩并不理想，为73杆，高于标准杆2杆，但他仍以268杆(67-63-65-73)，低于标准杆16杆，领先6杆的优势成功卫冕。这是汤姆斯在PGA巡回赛第十个胜利，也是自从去年6月份以来首次获得胜利。以下是汤姆斯球袋中的装备：
<l>Driver:  Cleveland Launcher 400 9.5º with Grafalloy Blue (X) shaft 
<l>3 Wood:  Cleveland Quad Pro 15º 
<l>5 Wood:  Cleveland Quad Pro 19º 
<l>3 Iron:  Cleveland TA6 
<l>Irons (4-9):  Cleveland 588P 
<l>Wedges (PW,SW,LW):  Cleveland 588 49º, 56º, 60º 
<l>Putter:  T.P. Mills by David Mills 
<l>Ball:  Titleist Pro V1x 
