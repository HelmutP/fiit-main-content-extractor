
<h>   巨大的市场需求赋予室内环境治理产业广阔的发展空间
      遭遇信任危机 室内环境治理，想说爱你不容易

<h>	  本报记者 宋鹏霞 本报实习生 杨群
   
<p>   　　随着一系列国家标准、行业规范相继浮出水面，市场规模上百亿元的室内环
   境治理产业，将不得不纠一次由新生概念走向成熟市场的蜕变。
<p>   　　因为对绿色家居生活的日益崇尚，室内环境治理类产品逐渐走俏。据有关专
   家介绍，室内环境治理产业已经
   渐成我国的一种“走俏行业”，目前市场规模达上百亿元。但与此同时，又有一
   些人指出甲醛清除品并不安全。一时间，室内环境治理产业众说纷纭。
   
<h>   　　信任危机
<p>   　　日前，有媒体报道，现在建材市场上销售的许多打着防治装修污染、清除甲
   醛的产品，实际上绝大多数是表面“封闭剂”而非“分解剂”，只能把甲醛暂时
   包裹在板材等释放源中。据说，像甲醛一喷灵只是在家具表面形成一层涂膜，把
   甲醛等有害物质暂时性地掩盖起来，这层膜一旦出现问题，甲醛等有害物质就又
   会释放出来，成为家庭暗藏的“定时炸弹”。
<p>   　　报道一出，行业内反响强烈。本月初，上海市环境保护产业协
   会室内环境治理分会以“甲醛清除品是定时炸弹吗”为主题，召开了一场专家研
   讨会。上海市消保委、科研院所、高校、生产厂家以及部分消费者代表参加了这
   次会议。他们发表了许多意见，认为因某种产品夸大效果而否定整个产业，有失
   偏颇。
<p>   　　上海市产品质量监督检验所高级工程师王今明介绍，甲醛、苯及苯系物、氨
   气、氡、总挥发性有机物（TVOC）是当前造成室内空气污染的五大污染源。目前
   市场上流行竹炭、活性炭、光触媒、负离子等多种室内空气污染治理方法,这些
   煌的治理方法有不同的利弊特点，市民若根据具体需要选择适当的方法，可以
   起到一定效果。例如：新近从国外引进的光触媒技术，对重度污染治理见效快，
   此类清除剂对甲醛去除效果为70%左右，对苯、总挥发性有机物的去除效果均在80
   %以上；臭氧技术则是目前国际公认的治理室内空气的一种常用、安全的物理方
   法，适于中度、轻度污染，对甲醛去除效果为40%左右，对苯的去除效果在90%以
   希对总挥发性有机物的去除效果在50%左右。
<p>   　　然而，室内环境治理产业遭遇信任危机也非偶然。上海市环境保护产业协
   会室内环境治理分会副秘书长王芳坦言，伴随着市场规模的急剧膨胀，国内从事
   室内空气净化的公司剧增，实力、技术、产品参差不齐，部分企业的产品宣传夸
   大治理效果。事实上，甲醛清除品是“定时炸弹”说并不是这个新兴产业受到的
   第一次质疑。关于各类室内环境治污产品和服务的投诉、纠纷不时见诸媒体，有
   的治理企业没有资质，有的治理产品号称环保绿色实则污染物超标，有的治理产
   品甚至还会产生二次污染。

<h>   　　庞大产业
<p>   　　室内环境治理作为一个新兴产业，包括室内治污产品、室内污染检测服务、
   室内环境治理服务等。尽管它正面临信任危机，但其潜在的市场却不容小觑。
<p>   　　上海市环境科学研究所高级工程师钱华指出，室内污染猛如虎。经
   历了工业革命带来的“煤烟型污染”和“光化学烟雾型污染”之后，现代人正进
   入以“室内空气污染”为标志的第三污染期。由于某些建筑材料不过关，在我国
   建房热潮中产生了触目惊心的室内空气污染问题。据统计，我国因室内空气污染
   导致疾病甚至死亡的约在11万人次左右，由此导致的经
   济损失每年高达数百亿元。室内空气污染已被列入对公众健康危害最大的五种环
   境污染之一。
<p>   　　上海人的室内环保意识正在觉醒。市场调查结果显示，上海不少市民家庭在
   新居装饰装修后，选择通过委托监测了解污染状况的占11%，选择委托专业治理
   司治理或者自己购买产品治理的占27%，还有7%的市民通过法律维权，5%的市民
   采取更换家具等措施———几项合计超过50%，表明上海市民对室内环境污染危
   害性的认识接近发达国家的水平。
<p>   　　巨大的市场需求赋予室内环境治理产业广阔的发展空间。据调查，2004年，
   我国室内环境治理产品销售市场规模达104亿元，未来几年内，预计将保持每年2
   8%的递增，2008年全行业销售将达到278亿元。
<h>   　　披荆前行
<p>   　　值得关注的是，我国室内空气污染治理消费市场离世界发达国家还有差距。
   仅以民用空气净化器的使用为例，这一产品在美国的普及率为27%，在日本为17%
   ，而在我国仅为1%。
<p>   　　据悉，发达国家对我国的室内空气治理市场兴趣不小。月初，第二届上海国
   际室（车）内环境净化治理技术及产品展览会上，许多国际知名企业有备而来，
   霍尼韦尔的新风净化机、日立的空气净化机、夏普的空气净化器、约克的空气净
   化器、松下的新风换气机系统同台亮相，对中国市场争夺之激烈不言而喻。
<p>   　　面对国际企业的竞争，我国的室内环境治理产业亟须加快前行。针对室内环
   境治理领域目前出现的一拥而上、鱼龙混杂的情况，有关部门应采取措施加快规
   范、整合。
<p>   　　首先，鉴于室内空气污染对人们健康的影响，我们急需一套完整有效的室内
   空气质量测评手段和体系。目前，国内已经
   聚集了世界领先的有效测评手段。诸如美国华瑞（RAE）公司等一些国际大型检
   测设备生产商纷纷在我国建立企业或代理。但由于我国至今还没有一套关于室内
   掌质量的完整评价体系，致使厂商与消费者之间的纠纷时有发生。
<p>    我国对电梯等设备已实施了强制性年检，但对于室内空气质量的强制性标准
   评价体系仍是空白。目前，我国有关室内空气质量方面的国家标准，主要有卫生
   部等部门颁布的《室内空气卫生标准》，国家环保局等部门颁布的《环境标志产
   品技术要求》、《室内装饰装修材料有害物质限量国家标准（10种物质）》和建
   设部等颁布的《建筑工程室内环境污染控制规范》等。专家指出，《室内空气质
   量标准》虽然比较系统地提出了室内空气质量的19个因素指标，但因为是一个推
   荐性标准，所以在具体执行过程中面临着一系列问题。
<p>   　　为规范室内空气治理，上海已经相继出台一系列措施。市环境保护产业协
   会室内环境治理分会制定了行业推荐合同文本《室内环境治理服务行业规范合同
   》。该文本对容易产生纠纷的治理效果认定和验收环节作了明确规定，还标示了
   市消保委和协
   会的投诉电话，以方便消费者及时维权。另外，“室内环境治理员”也已于去年
   9月被列入上海市劳动保障局正式向社会发布的上海65种新职业之一，今后室内
   污染治理人员将持证上岗。
<p>   　　随着一系列国家标准、行业规范的逐步推出，市场规模上百亿元的室内环境
   治理产业，将不得不纠一次由新生概念走向成熟市场的蜕变。在经
   历了这一蜕变之后，室内环境治理产业将越做越大，越做越好。
