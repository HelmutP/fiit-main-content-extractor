
<h>第二十七届粮农组织
<h>亚洲及 太平洋区域会议

<h>2004年5月17-21日，中国北京

<h>分区域粮食安全战略
（太平洋分区域）

   <h>目录

<h>I. 引言
<h> II. 背景情况
<h>III. 区域粮食安全战略
<h>IV. 区域粮食安全计划
<h> V. 结论

<h> I.引言

   <p>1.
   到2015年实现所有人的粮食安全并将营养不良人数减少一半，是国际发展议程中
   的一个基本内容。1996年世界粮食首脑会议发表的《世界粮食安全罗马宣言》中
   即指出了这一点，2002年世界各国领导人又在"世界粮食首脑会议：五年之后"（
   WFS:
   fyl）上作了重申。《世界粮食安全罗马宣言》和《世界粮食首脑会议行动计划
   肺实现家庭、国家、区域和全球一级的粮食安全这一共同目标开辟了途径。小
   河旆⒄怪泄家，包括大多数太平洋岛国，受到了特别的关注。

   <p>2.
   太平洋岛国论坛秘书处于2002年正式核准了“区域农业发展和粮食安全战略”以
   及“论坛成员国区域粮食安全计划”。论坛成员国包括太平洋区域所有粮农组织
   成员国。这些文件由论坛秘书处编写，作为区域集团支持世界粮食首脑会议后续
   行动的一部分，粮农组织太平洋岛国分区域办事处对文件的编写给予了支持。“
   论坛成员国区域农业和粮食安全战略”提醒人们注意到国家和区域一级在处理粮
   食安全过程中存在制定政策和计划能力不足的问题。

   <p>3.
   在制定“区域粮食安全计划”过程中，审议并纳入了各国的“国家农业发展和粮
   食安全战略--2010年展望”。这些文件原
   本是为1996年世界粮食首脑会议准备背景资料而编写的，2000年各成员国还在粮
   农组织太平洋岛国分区域办事处的帮助下对上述文件进行了重新审议。根据各成
   员国的国别战略，“区域粮食安全计划”项目将在每个国家确定一些项目，以改
   善营养状况，更好地获得粮食（本文件第III部分还列出了其他一些目标）。

   <p>4.
   本文将首先评估太平洋岛国的农业与粮食安全状况，这样有助于对粮食安全问题
   进行全面的讨论。文件介绍总的资源状况、农业与粮食安全、农产品销售情况以
   及可持续农业与农村发展之间的重要关系。然后，本文件讨论“区域粮食安全战
   略”以及通过“区域粮食安全计划”实施该战略。

  <h>  II. 背景情况

   <h>A. 总的自然资源

   <p>5.
   太平洋岛国^1陆地和海洋生态系统纷繁复杂，种类多样。陆地分布于几千个岛屿
   上，其中大多数为小岛屿和环状珊瑚岛，总面积仅有50万平方公里左右。而与之
   相对比，通过专属经
   济区管辖的海洋面积大约为1700万平方公里。太平洋各岛国在面积以及农业、林
   业和渔业自然资源方面差异很大。巴布亚新几内亚的陆地面积占该区域陆地总面
   积的将近90%。所罗门群岛、瓦努阿图和斐济由大片高低不平的火山岩陆地组成
   总体上生物和物质自然资源丰富。一些岛国拥有大量矿产资源，巴布亚新几内
   恰⑼吲阿图和斐济尤其如此。与之相反，珊瑚岛国家的资源非常有限。这些国
   野括密克罗尼西亚联邦、基里巴斯、瑙鲁、纽埃、马绍尔群岛、图瓦卢和帕劳
   Ｕ庑┕家面积小、地理位置闭塞，虽然一些国家有矿产，但土壤十分贫瘠。处
   谏鲜隽礁黾端中间的，是库克群岛、汤加和萨摩亚。它们属于中型火山岛，土
   婪饰郑但矿产极少，甚至没有任何矿产。

   <p>6. 虽然太平洋岛国之间差异很大，但它们都面临着共同的制约因素，有碍
   他们实现经
   济平衡发展和可持续粮食安全。这些制约因素是发展中小岛国所共有的，包括面
   积小、位置偏远、领土分散、生态系统脆弱、容易受到自然灾害的侵袭，以及对
   外部炯靡蛩厝缛球贸易环境的变化依赖性强。

   <p>7. 人们越来越意识到，环境保护措施对于实现持续的经
   济发展至关重要。太平洋诸岛脆弱的生态系统受到城市化、公共用地和泻湖资源
   过度开发、边际土地农业开垦、采伐森
   林等各种趋势的威胁。目前面临的挑战是实现自然资源的保护与可持续利用之间
   的适当平衡。这就要求进行有效的水土保持、水资源保护以及对森
   林和渔业资源的仔细管理。生产系统需要多样化，以实现土地资源利用的最大效
   率，同时尽量减少环境和经
   济风险。海洋和海岸环境具有战略重要性，是保障粮食安全的宝贵却又脆弱的资
   源。

   <h>B. 农业与粮食安全

   <p>8.
   太平洋岛国的多数人口居住在农村，有的国家农村人口比例甚至超过80%，他们
   饕依赖农、林、渔业作为生计和粮食安全的来源。农业在各国国内生产总值中
   谋壤从3%（密克罗尼西亚联邦）至40%（所罗门群岛）不等。农业部门（包括渔
   业和林业）同旅游业一样，仍然是为太平洋岛国提供收入和就业机会潜力的主要
   部门，也是提供生计来源的主要部门。

   <p>9.
   在许多太平洋岛国，包括库克群岛、斐济、基里巴斯、马绍尔群岛、密克罗尼西
   亚联邦、帕劳、萨摩亚、汤加和瓦努阿图，旅游业是主要的创汇来源。例如斐济
   2003年总共吸引了42.6万游客，创汇4.649亿斐济元（资料来源：2003年斐济信
   ⒂朊教骞叵挡砍霭嫖铮。其他国家如巴布亚新几内亚和所罗门群岛也鼓励将旅
   我底魑主要的创汇部门。多数国家还鼓励生态旅游，作为主流旅游业的补充。
   态旅游被认为是将旅游收入拓展到旅游业的主要集中领域以外、增加当地人参
   氲淖钣行Х绞健Ｓ捎诙燃偾为当地人所有，这样可以增加他们的现金收入，从
   提高他们的粮食安全和生活水平。政府从政策上支持旅游业，有助于游客人数
   黾樱而且由于对食品的需求增加，也可以使食用农产品（水果、蔬菜、鱼类和
   倚螅┑南售量增加。太平洋岛国旅游业的未来增长潜力取决于其吸纳能力，包
   旅店业及其他住宿设施和餐馆的发展。

   <p>10.
   从人均收入的平均数据中，也许看不出该区域的人们面临特别严重的贫困和粮食
   短缺问题。然而，国家平均指数掩盖了各国内部严重的地域和社会差距。就人均
   国内生产总值（2000年）而言，各国各不相同，从低收入（所罗门群岛：880美
   ）到中上等收入（帕劳：超过8000美元）不等，而多数国家被列为中低收入国
   遥其人均国内生产总值为1070美元（萨摩亚）至2300美元（斐济））。看起来
   首都所在岛屿的经
   济得到了发展，而偏远小岛却受到不利影响。这种现象有多方面的砸颍包括经
   济规模、人口数量和人口迁徙、基础设施（包括农产品销售基础设施）不足、医
   疗和教育等服务设施不齐全以及极其偏远的岛屿上土地资源匮乏。另外，一些太
   平洋岛国收入分配极不平衡。如巴布亚新几内亚，人均国内生产总值估计为约11
   00美元，但据信，约80%的人口年收入低于350美元。

   <p>11.
   该区域平均膳食能量供应为2380大卡，仅比发展中国家总体平均水平稍低（1994
   -96年）。这掩盖了该区域多数国家存在长期食物不足人群的事实。贫血、铁和
   生素A缺乏的现象普遍存在。各国中能量供应水平最低为所罗门群岛，为2050大
   卡/天，最高为斐济，超过3000大卡/天，显示了国家之间的巨大差距。

   <p>12.
   小农进行的是传统耕种，通常耕种的土地面积少于2公顷，多数是使用家庭劳力
   很少有外来投入。在美拉
   尼西亚国家，粮食生产大多由妇女负责，而在传统波利尼西亚式耕作体系中男子
   则发挥更大作用。生产的产品大部分用于家庭消费，但有一些可以出售。块根作
   物是本区域主要的粮食作物，一般同其他粮食或经
   济作物如椰子、面包果、香蕉和芭蕉、各种其它水果和坚果、香料植物、药用植
   物以及其它为建筑、手工艺、薪柴等提供原
   材料的植物一起在农林混作系统中种植。在斐济，农业更多的是商业运作，但自
   给农业仍然很重要。大规模农业生产主要包括棕榈油、椰子、甘蔗、可可和咖啡
   种植园以及肉牛生产。生产者面临的典型制约因素包括劳力短缺、种苗质量低下
   且数量有限、缺乏有效的病虫害防治和监测计划、产后损失大、动物保健差、购
   买饲料成本高以及国内和出口市场销售薄弱。

   <p>13.
   越靠近海岸线，块根耕作体系常常就逐渐和沿海手工渔业融合在一起。自给渔业
   为太平洋岛国提供了重要的蛋白质来源。虽然现有统计数字非常有限，但我们知
   道自给性捕鱼的总产量常常比商业捕鱼的产量大几倍。例如在斐济，虽然商业的
   “金枪鱼捕捞业”很发达，但自给渔业仍然是渔业中最大的部门。太平洋岛国大
   多数沿海资源都很少，因此也非常脆弱。商业捕鱼的开始，或者是从自给性捕鱼
   到商业捕鱼的转变，常常会造成沿海地区资源的过度开发。虽然水产养殖业对太
   平洋诸岛经
   济的影响相对较小，但太平洋岛国均已认识到，水产养殖业，包括重新放养和鱼
   群增养计划，是长期可持续地利用沿海渔业资源的许多方法之一。

   <h>C. 农产品销售和贸易

   <p>14．
   虽然太平洋各岛国农业生产模式可能差异很大，但从销售角度讲，它们的共同点
   可能比差异更多。多数太平洋岛国由许多岛屿组成，人口相对较小，这就意味着
   除海上运输带来的困难外，更复杂的是，各岛屿可供出售的产品数量很小，它们
   买得起的商品数量也很小。例如所罗门群岛和库克群岛的一些边远小岛由小船负
   责运输，每月甚至更长时间才运输一次。很明显，这种状况决定了它们可以种植
   的用于出售的作物品种。甚至那些面积较大、人口较多的岛屿都存在与外界沟通
   的困难。如巴布亚新几内亚高地生产的多数新鲜产品必须从莱城运到莫尔兹比港
   。从高地到首都莫尔兹比港没有公路，而空运费用又太高。

   <p>15．
   由于很大一部分农产品如块根作物、水果、蔬菜和鱼类都是在当地新鲜销售，开
   发适当的产后加工和储藏技术至关重要。当地的农民、推广人员、中间商和零售
   商一般缺少关于新鲜产品产后处理和产品售前适当包装、运输和储藏的信息。因
   此需要在贸易商中推广简单、低成本的技术，最好是利用当地资源对新鲜产品进
   行处理和包装，减少运输和储存过程中的损失。出口产品的产后处理一般组织得
   较好，因为出口商往往自己进行包装，或提供包装材料并监督包装。例如，汤加
   的南瓜出口商提供包装材料，监督包装并进行质量检查，并且负责从包装地点到
   码头的运输。新鲜产品出口面临的主要问题似乎是达到质量和检疫标准。

   <p>16. 出口部门在多数太平洋岛国的收入中占有核心位置，包含范围很窄
   的一些初级农产品，主要出口到海外的固定市场。树木、糖、水果和蔬菜以及木
   材和海产品是该区域的主要出口产品。主要贸易伙伴包括澳大利亚、新西兰、日
   本、美国和欧洲。岛屿内部贸易规模很小，原
   因无疑主要是可供出售的商品相似，而且加强小国之间的贸易关系没有规模效益
   。因此出口极易受到外部干扰的影响，包括贸易伙伴国经
   济的衰退、天气对出口产品的影响以及来自该区域比较优势更大的面积大、成本
   低的国家的强大竞争。

   <p>17.
   斐济、巴布亚新几内亚和所罗门群岛是世界贸易组织成员国，而萨摩亚、汤加和
   瓦努阿图已提出加入的申请。世贸组织业已建立的规则对农业贸易和国家政策有
   一定影响。

   <p>18.
   在大多数太平洋岛国，农产品出口占总出口的50%以上，而同时要进口大量非食
   返闹饕商品，包括机械、资本货物和石油产品，因此除三个国家（瑙鲁、巴布
   切录改谘呛退罗门群岛）外，其他国家都存在巨额贸易逆差。农产品贸易除遵
   乌拦缁睾稀杜┮敌定》之外，还需要遵守大量其他协
   定。在区域层面上，根据《南太平洋区域贸易和炯煤献餍
   定》，各国享有澳大利亚和新西兰市场的优惠准入。进入欧洲市场是根据《洛美
   卸ā/《科托努卸ā泛推栈葜啤Ｅ┎品进入日本和美国遵循
   的是上述两国各自的普惠制。在上述所有市场中，太平洋岛国的产品可以免税或
   享受优惠关税进入。由于进口国降低了它们的最惠国关税，优惠带来的的边际利
   益减少了。同时，国内炯猛ü关税和其他措施得到保护。

   <p>19.
   所有国家政府均已认识到从传统出口产品转向出口产品多样化的必要性，并且正
   在致力于促进生产升级和开发高档市场。有潜力的出口商品包括园艺作物、新鲜
   和加工水果、块根作物、香料和草药、当地特产坚果、花卉、非椰肉椰子产品、
   救现さ挠谢产品和新鲜鱼类等。

   <p>20.
   食品质量和安全控制仍然是关系到太平洋岛国粮食安全的重要问题。各国必须达
   到国际公认的食品质量和安全标准，才能从农产品贸易自由化中获益。要实现这
   一点，需要加强自身的食品安全体系，协
   调统一食品安全法规，有效参与粮农组织和世界卫生组织共同设立的食品法典委
   员会的工作。虽然各国食品法规发展水平不同，但总体来说，太平洋岛国食品标
   准和法规是不够充分的。

   <h>D. 农业与可持续乡村发展

   <p>21．
   像许多发展中小岛国一样，太平洋岛国人口中大部分居住在农村，严重依赖农、
   林、渔业作为生计和粮食安全的来源。太平洋岛国正越来越多地面临着新兴全球
   贸易环境、与营养有关的健康问题的出现和粮食依赖进口所带来的挑战。为应对
   这些挑战，各国正在寻找机会促进农业系统多样化。在捐助方和粮农组织的支持
   下，它们已经
   进行了一些工作，包括加强传统生产系统，振兴传统粮食作物生产，开发病虫害
   综合防治和生产综合管理方法，以及自然资源综合保护和管理。

   <p>22.
   尽管如此，各国仍然有一个倾向，即在发展过程中缺乏对农业、乡村发展和环境
   之间的复杂关系和相互联系进行适当的评估和分析。例如，环境（生物多样化）
   保护工作往往只重视设立保护区，着重通过将自然森
   林和灌木林、未受破坏的珊瑚礁和沿海生态系统建立保护区，而不是通过重新恢
   复已经
   退化的区域。这些保护区几十年来一直为农村人口提供生计（食物、薪柴、原
   材料等），将这些区域封闭起来在很多情况下造成了粮食减产和生产率降低，并
   且最终导致农村的粮食不安全加剧。

   <p>23.
   联合国大会以A/57/262号决议，批准于2004年8月30日至9月3日在毛里求斯召开
   淮畏⒄怪行〉汗国际会议。这将提供一次机会，全面审议1994年《发展中小岛
   可持续发展的巴巴多斯行动计划》的实施情况。为了筹备2004年毛里求斯会议
   2003年召开了三次分区域会议，2004年初还将在巴哈马和纽约分别召开一次区
   蚣浠嵋楹鸵淮卧け富嵋椤

   <p>24.
   出席2003年8月举行的毛里求斯会议区域预备会的太平洋岛国代表们同意，太平
   笄域评估和立场草案中的目标，应与该区域提交给可持续发展世界首脑会议的
   募中的目标相似，即“在太平洋区域为提高所有人的生活质量而实现可衡量的
   沙中发展”。这就要保证太平洋区域的可持续发展是以这里的人民、海洋和岛
   煳重点。然而，人们也认识到，1994年《发展中小岛国可持续发展的巴巴多斯
   卸计划》中对农业的重视不够。另外，在所有这些预备会议上，发展中小岛国
   拇表主要来自关心环境问题的国家机构。因此，很难预想毛里求斯会议会对农
   导捌湓谔平洋岛国（及其它发展中小岛国）可持续发展中的作用问题给予充分
   厥樱除非各国能够成功将农业部门纳入各自的可持续发展议程中。

   <p>25.
   在制订旨在加强可持续乡村发展的发展计划，或评估农村地区乡村发展计划的得
   失时，农业对公共产品的影响常常得到充分的承认和考虑。虽然农业能够为农业
   生物多样化、气候变化的缓解、集水区保护和防洪以及粮食安全等公共产品做出
   重大贡献，但它也会带来一些负面的结果，如养分流失、下游洪水发生次数增多
   以及自然森
   林和湿地消失。除非各种发展项目总体上都能产生正面的结果，否则这些努力不
   大可能实现农业的长期可持续发展。

   <p>26.
   衡量农业对公共产品所做贡献的指标是很难确定的。但是，一些行动如恢复退化
   的土地、采用保护性农业（有机耕种、使用有机肥料和病虫害综合治理和生产综
   合管理措施）以及扩充农民的知识基础，都被认为对促进公共产品的生产有积极
   贡献。这些行动基本上符合太平洋岛国农业发展和粮食安全区域战略。

<h> III. 区域粮食安全战略

   <p>27. 太平洋岛国区域粮食安全战略提出了计划制定过程的一系列原
   则，包括重视各国的优先领域、从各国的优先领域得出区域的优先领域、捐助方
   对上述区域优先领域的充分认识、对区域要求进行优先次序排列以及在太平洋岛
   国通过的文件基础上编制项目/计划建议书。主要战略重点是发展集约化的、多
   和可持续的农业，并且在区域和国家一级根据全球贸易环境的变化对农业政策
   岢鼋ㄒ楹妥鞒龅髡。这些战略符合粮农组织的整体战略，解决成员国在下述领
   虻男枰：降低粮食不安全和农村贫困；确保粮食、农业、渔业和林业的有利管
   砜蚣埽皇迪峙┮怠⒂嬉岛蜕
   林产品供应量和可供量的可持续增长；保护和加强自然资源基础的可持续利用；
   以及丰富粮食和农业、渔业以及林业方面的知识。

   <p>28.
   在短期至中期内，太平洋岛国区域粮食安全战略的目标是，在可持续基础上加速
   农业生产力的提高，提高粮食和农业部门的竞争力，同时保护和改善自然资源基
   础。实现了这一目标，就一定能够广泛实现男女平等的粮食安全和减贫目标。

 <h> IV. 区域粮食安全计划

   <p>29.
   区域粮食安全计划的总体目标是，加强区域和国家以及社区和家庭层面的粮食安
   全。这一计划将通过提高粮食作物的生产率、增加产量和贸易，在可持续的基础
   上改善该区域内所有人在任何时候均能获得健康和活跃生活所需要的充足食物的
   能力。因此它的重点非常广泛，包括与发展更集约化、多样化和可持续的农业，
   以及农业政策支持、投资援助和全球贸易环境变化有关的问题。

   <p>30.
   为实现上述总体目标，该计划是围绕两个主要的相互关联的组成部分来组织的，
   这两个组成部分的基础是太平洋岛国论坛确定的一个区域战略发展框架和区域农
   业优先领域。这两个主要组成部分是：

   <p>第一部分：加强粮食生产和安全。这一部分的重点是对与生产相关的具体活动进
   行投资（供应方面）。它将根据农、林、渔各个分部门关键的优先领域对每个参
   与国的社区试点活动给予资助，同时考虑适应小农农业的技术的应用研究与开发
   以及为解决具体生产问题开发参与性方法。

   <p>另外，在单个国家层面上，与粮农组织的“粮食安全特别计划”相关的分项目将
   在可行时推广小型灌溉，并促进耕种方式的改善和包括小家畜生产在内的农作体
   系的多样化。还将重点放在改善销售基础设施方面，包括加强检疫、质量控制和
   检验系统。

   <p>第二部分：加强农产品贸易和政策。这一部分的目标是帮助在太平洋岛国建立一
   种开放的、竞争性的国内政策框架，从而根据比较优势促进有效的生产体系。这
   一部分的战略活动是，加强机构能力，以建立农产品出口的卫生及植物检疫标准
   ，促进贸易发展。它将致力于以下领域：（1）食品质量和安全标准；（2）促进
   区域内部的农产品贸易；（3）商品开发计划；（4）应对目前贸易自由化的过渡
   性措施；以及（5）帮助各国加强有效参与农业多边贸易谈判的能力。

 <h>V. 结论

   <p>31.
   太平洋岛国区域粮食安全战略认识到了农业与环境之间的复杂关系以及粮食不安
   全与农村贫困的联系。该战略旨在可持续地加速农业生产率的提高，在保护和改
   善环境和自然资源的同时增强粮食和农业部门的竞争力。“太平洋岛国区域粮食
   安全特别计划”的总体目标是在区域、国家、社区、家庭层面上加强粮食安全，
   它将通过提高生产率、增加产量和促进粮食作物贸易来增加维持健康和活跃生活
   所需要的充足食物的机会。

   <p>32.
   要在区域和国家层面上促进农村可持续发展和加强粮食安全，就需要将农业纳入
   太平洋岛国的可持续发展议程中。此外，应重视提高人们对农业在公共产品生产
   方面发挥更广泛的作用的意识，这样就可以建立一个解决乡村发展和环境问题、
   增强政治意愿和供资的有效框架。

   <p>33.
   粮农组织应通过其太平洋岛国分区域办事处，在下列领域对太平洋岛国粮食安全
   战略的实施提供战略支持：
  <l>* 支持成员国加强农业、畜牧业、林业、渔业和农村可持续发展的分部门国家
  政策，即：制订国家粮食安全战略和投资计划；制订国家扶贫/脱贫战略；
   立或加强信息系统，以提高对粮食不安全和易受害性的了解和监测；在农
 嫡策制定、国家灾害管理和减缓政策及战略方面的能力建设；乡村综合发
  拐策以及农业企业发展。
    <l> * 通过加速先进生产技术的转让，为农业增长和生产率的提高提供技术支持；
   加强土壤肥力研究和改良计划；促进畜牧和作物的综合发展；改进种子生产
   、选育和鉴定，并加速繁殖。
   <l>* 鼓励农产品销售和信息系统的升级，即筹集资金支持更长期的基础设施投资
 ；支持加强销售情报和资料库系统；消除市场准入障啊
  <l>* 为水资源利用开发提供技术和组织支持，即通过农民培训和各国农业部的能
  力建设，推广小型灌溉和水土保持技术；推广适用于受旱灾影响地区的灌溉
   技术。
  <l>* 通过制订旨在改善畜牧生产技术、疾病监测和健康保证体系的战略来振兴畜
    牧业；推广虫害综合治理和疾病控制措施；通过改善管理来提高牧场的生产
    率；扩大城市周边小型牲畜的饲养。
    <l> * 通过制订国家政策和行动计划，为环境上可持续的农业发展提供技术支持，
  即采用社区参与的方法进行综合的环境保护、资源保护和生物多样性保护；
   推广可提高环保意识的政策；制定农林兼作发展战略。
    <l> * 通过向成员国提供渔业政策和计划方面的技术建议，支持渔业和水产养殖资
   源的保护、管理和开发；制订计划，以加强成员国实施粮农组织《负责任渔
    业行为守则》及其最近的“国际行动计划”的能力；提供技术援助，帮助各
    国进一步加强对沿海和自给性渔业和水产养殖的统计能力；提供关于鱼类利
 用和销售的技术指导，特别将重点放在小规模渔业上；实施有利于环境的水
  产养殖发展计划。
   <l>* 通过农业普查和调查以及国家统计数据处理方面的能力建设，为农业统计系
  统发展提供技术支持。
   <l>* 继续为农业、畜牧业、渔业和林业部门的简报和回顾工作提供技术支持和建
   议。
  <l>* 为太平洋岛国论坛秘书处及其他区域机构提供技术支持与协
   调，以实施“太平洋岛国粮食安全区域计划”。
  <l>* 在协
   调会议上继续加强与本地捐助方的对话。需要改进此类会议的技术筹备。因
   此，政策援助组/多学科小组的成员必须及时获取对实地计划开发（FPD）项
    目和计划的必要投入和技术支持并及时送到分区域办事处。
  <l>* 加强当前与所有发展伙伴的对话。分区域办事处政策援助组/多学科小组每
  曛辽儆τ胨有相关政府部门召开一次会，正式评估对粮农组织技术支持的
    咛逡求，并且应该在可能时作出临时预算。
  <l>* 继续在各学科领域组织技术专家磋商会，这些领域包括：农业、畜牧业、林
    业、渔业、水产养殖业、耕作系统、营养、粮食安全和不安全、可持续乡村
    发展和支持计划的制定。

   <h>34. 总之，区域会议不妨提出以下建议：

  <p> (a) 农业和粮食安全与区域粮食安全计划 -
   为支持成员国和太平洋岛国论坛秘书处加强农业、畜牧业、林业、渔业和可持续
   乡村发展的部门政策以及制定国家粮食安全战略和投资计划，区域会议不妨建议
   捐增方增加对该区域农业发展的资金支持，并且建议粮农组织在各区域的粮食安
   全计划中进一步加强南南合作。

   <p>(b) 农业销售和贸易 -
   区域会议不妨考虑建议粮农组织和其他捐助方增加对农产品销售和信息系统开发
   的技术援助以及筹集更多资金用于更长期基础设施的建设；支持加强销售信息和
   资料库系统；促进消除市场准入障碍
   的能力建设，包括涉及到世贸组织、《卫生和植物检疫措施协
   定》和《贸易技术壁垒卸ā返奈侍狻

   <p>(c) 农业和可持续乡村发展 -
   区域会议还不妨建议国际组织如粮农组织和捐助界通过制订国家政策和行动计划
   来为环保上可持续的农业发展提供技术支持，具体办法是使用以社区参与为基础
   的综合环境保护、资源保护和生物多样性保护方法，推广提高环保意识的政策，
   支持渔业和水产养殖资源的保护、管理和开发，以及促进农林兼作的发展战略。

   <p>^1
   1太平洋岛国，是指属于太平洋岛国论坛成员并被列为发展中国家的国家，包括
   饪巳旱骸⒚芸寺弈嵛餮橇邦、斐济、基里巴斯、马绍尔群岛、瑙鲁、纽埃、帕
   汀巴布亚新几内亚、萨摩亚、所罗门群岛、汤加、图瓦卢和瓦努阿图。
