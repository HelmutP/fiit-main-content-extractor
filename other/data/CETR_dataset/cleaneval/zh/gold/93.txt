
<h>                 和16岁少女上演激情戏 刘烨称是他最猛的一次


<p>   　新一代留美导演颢然执导，金马奖影帝刘烨、王思懿等明星主演的影片《青春
   爱人事件》，将于5月全国上映。日前，身在香港拍摄电影《阿嫂》的刘烨向自
                       己透露了拍摄过程中的一些细节。

<p>   　　《青春爱人事件》以繁华都市为背景，描绘了青年小谢与花季少女柳芭之间
   一段充满悬念和诱惑的都市爱情故事。“刚开始我不知道，自己得和16岁的石周
   靓饰演的少女演段激情戏。”刘烨有点不好意思地告诉记者，虽然在《蓝宇》中
    有过激情演出，但石周靓毕竟还是未成年人，所以当时很别扭，两人老是NG。
	
<p>   　　“最后还是导演颢然给我们上了一课，他说这部戏就是为我打造的，而且选
   择石周靓来出演柳芭，也是剧本需要，我们只要把自己最本色的东西表现出来就
   行了，不用顾虑太多。”于是，刘烨下定决心“排除万难，有条件要上，没条件
   创造条件也要上”。花了很长时间和对方沟通交流后，放开包袱，完成了那段火
        爆的激情戏：“我觉得那是我有史以来最猛的一次，感觉很舒服。”

  
