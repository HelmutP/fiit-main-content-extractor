<h>瞿同祖先生与中国地方政府传统研究――《清代地方政府》及其杰出成就

                                <h>by 范忠信


                                <p>［评论书籍：瞿同祖：《清代地方政府》，范忠信等译，法律出版社2003年7月版。］ 
                                　　<p>内容提要：瞿同祖先生对清代地方政府的研究，是运用社会学的方法研究中国政治制度史的典范。《清代地方政府》一书以清代地方政治活动或政府体制中的个人（参加者）及其惯常行为为重心去研究中国传统社会政治生活中的实际政治制度，注重非正式的私人性的因素对传统地方政府体制和政治行政过程的重要影响，并运用了从前政治制度史研究者们易于忽视的大量丰富而生动的史料，揭示了关于政治制度的文字规范与政治生活中的实际制度之间的巨大差异，揭示了在许多行政场合国家法律并未被执行的事实以及实际上的政治“潜规则”的运作，向我们展示了一套更加活生生的传统政治制度及其运作模式。其杰出成就值得我们重视和效法。
                                　　<p>关键词：地方政府，清代，州县政府，州县官，士绅，幕友，书吏，长随，衙役
                                　　<h>英文标题：prof. T’ung-tsu  ch’ü and his studies of 
                                the local governmental tradition in traditional 
                                China.
                                　　<p>作者范忠信，１９５９年生，中国人民大学法学博士，现为中南财经政法大学教授、博士生导师。著作有《中国法律传统的基本精神》、《中西法文化的暗合与差异》等５种。曾在《中国社会科学》、《法学研究》等十余刊物发表重要论文５０余篇。通讯地址：武汉市洪山区政院路一号，中南财经政法大学法学院，邮政编码430074。
								<h> <center>一</center>                            
 <p>《清代地方政府》（local Government  in  China  under  the                         Ch’ing）[1]是瞿同祖先生旅美期间的英文力作。因为是讲中国历史的书，翻译难度容易被低估，于是1998年我就主动向瞿老“请缨”翻译。前后花了五年时间，现在书总算翻译完成了。此刻我唯一的感慨是：若早知如此，当初根本就不敢主动揽下这桩活儿。
                                　　<p>我们新中国的法科学子得以认识瞿老，中华书局功不可没。1981年，中华书局重印了瞿老1939~1944年间完成、1947年由商务印书馆出版的《中国法律与中国社会》一书。这本书，讲的是中国传统社会中的动态的、实践中的法律状况，讲的是家族、婚姻、阶级、宗教等具体社会生活范畴中的社会习惯与国家法律规范之间的复杂关系。五千年法律史，被还原为五彩缤纷的活生生的画卷，瞿老就象是一位站在画卷旁的杰出评论家，为我们娓娓点评画卷中常人难以看出的无穷奥妙。
                                　　<p>绝大多数知道瞿老的人，只知道瞿老有《中国法律与中国社会》，除此以外大概一无所知。我在1987 
                                年秋以前也是如此。1987年秋以后情况丕变。我从中国政法大学研究生院毕业分配到中直西苑机关工作，住在万寿寺甲二号现代国际关系研究所图书馆旁。这个图书馆的外文藏书帮我认识了一个叫T’ung-tsu 
                                 ch’ü的“美籍华人学者”（图书馆的文字介绍）。这位学者的书，该馆收藏的有3种，分别是《Law 
                                and Society in Traditional 
                                China》(《传统中国的法律与社会》，巴黎和海牙，1961),《local 
                                Government  in  China  under the Ch’ing 
                                》(《清代地方政府》，哈佛大学出版社，1962)，《Han  Social  
                                Structure》(《汉代社会》，华盛顿大学出版社，1972)。请教一位老同志，方知T’ung-tsu 
                                 
                                ch’ü是威氏音标，这位“美籍华人学者”其实就是我所景仰的瞿同祖先生。后来大致翻阅一下这几本书，发现部头都不太大，但差不多占全书文字量一半的注释（广泛得无以复加的史料！）就让我惊叹不已。后来再翻翻西方学者和华裔学者研究中国政治史和法律史的英文著作，我发现，引用瞿同祖先生著作的频率非常高，我所翻过的20多种西人作品附录的参考书目中，几乎每本都有引T’ung-tsu 
                                 ch’ü著作者，可见瞿先生在海外的巨大影响。
                                　　<p>瞿先生是我国法学家中旅居西方时间最长的一位（可能至今仍算是）。1934年瞿先生毕业于燕京大学社会学系，随即转读该校社会史专业研究生，1936年获文学硕士学位。1939~1944年任教于云南大学，其间还在西南联合大学兼职。1944年秋，34岁的瞿先生离开云南大学，应邀到美国哥伦比亚大学作访问研究。1945至1955年十年间，瞿老一直在哥伦比亚大学从事汉代社会组织和结构的研究；用英文写成了《汉代社会》一书。其间，还以英文重写了《中国法律与中国社会》（补充了原来在国内因为抗战迁徙而难以找到的一些史料），更名为《Law 
                                and Society in Traditional 
                                China》。1955至1962年7年间，瞿老应著名汉学家费正清（J.K.Fairbank）的邀请，到哈佛大学东亚研究中心做研究员，此间完成了《清代地方政府》的创作。1962至1965年间，瞿老又应聘到加拿大不列颠哥伦比亚大学任教，直到1965年秋回国。从1944年至1965年，瞿老在美国和加拿大一共工作了21年。21年里，他的学术成果，瞿老曾自嘲说：就是两本半书（用英文重写《中国法律与中国社会》算半本），此外几乎没有单独发表过论文。
                                　　<p>历史公正地记住了真正的学术成果，40年后人们仍发现瞿老的书象金子或钻石在闪闪发光，光芒穿透了历史的陈封，因而好几位学者早就提议翻译瞿老的《清代地方政府》、《汉代社会》。但瞿老似乎一直没有表态。1997年，我在参与编辑《瞿同祖法学论著集》（中国政法大学出版社1998年版）时，要求将《清代地方政府》中关于“司法”和“刑名幕友”的章节翻译出来收入集子中，瞿老欣然同意。译稿后来得到瞿老肯定并收入集子时，我又趁势要求翻译全书，瞿老再次欣然同意。这让我无比荣幸且感责任重大。1998年秋，我调中南政法学院工作，即开始动手翻译。1999年初，就初译完了一半，但此后因为公务繁忙一直拖延。2000年底，我开始请我校英语不错的青年教师晏锋翻译另一半。不到一年，晏锋的初译完成了。但由于书中涉及的清代历史知识太细致、太具体，第一次全书通篇校译断断续续花了我一年半时间，许多地方几乎是重译。2002年底，我又让在研究生中英语绩优的何鹏同学将原稿、译文通篇校读一遍，发现问题用红笔标注出来，由我再加校译。这样发现的问题又有大小百处之多，这样的第二次审校也花了我们两人一共几个月时间。何鹏还跑了多家图书馆查阅书中所引用近百处中文史料原文，完成了大部分回译。有一部分难查的回译是我自己跑北京图书馆新馆和老馆好多天查找出来的。何鹏还翻译或制作了中文书目、西文书目、重要名词译名对照等几个重要附录。
                             <h>   <center>二</center>
                                　　<p>《清代地方政府》是一部用社会学的方法研究中国清代地方政府的实际构成及其实际运作模式的著作，是一本研究中国古代地方政府传统的杰出著作。这本书与我从前看到的所有关于中国古代或明清时代政治制度研究的书都不一样。
                                　　从前研究中国古代政治制度的书，有三大特征是比较明显的。
                                　　<l>以现代政治学的观念或概念去切割中国古代政治史，瓦解其特有的整体性特征或风格。这些书的结构一般是：先是概论（总论），然后把中国历史分成奴隶社会、封建社会、半殖民地半封建社会等几个阶段，在每个阶段分别是行政机构、监察制度、法律制度、军事制度、科举及教育制度、人事制度等等的列举分析。[2]或是仅仅研究古代政府体制：先列出政府的部门构成、官员配备及其职掌等，再说各机构之间的权力关系等等。这种用现代概念对古代政治史分类归堆式的研究，易于让现代人凭着今天的经验去理解历史，但也非常容易让人们误解历史。研究中国古代法律制度的书也大多如此。[3]
                                　　<l>只注意制度上的正式性、官方性因素，不注意非正式的、私人性的（或个人的）因素在政治中的作用（包括对正式制度的抵制或限制作用，也有改造或创新制度的作用）。这些著作即使注意到成文制度的内容和运作之外的个人或私人因素，那也多半只见到有权势的个人随意否定或践踏制度的因素（“好制度——坏个人”的思维模式支配了政治制度史研究），没有见到不成文制度或法律性惯例在非正式、私人性因素影响下形成和作用的过程，不能正视特殊个人超越成文制度束缚开创新惯例或制度的努力及其正当性。[4]
                                　　<l>惯于以成文法的规定为依据来研究古代的政治制度和政府体制，不注意社会实际政治生活与成文法规之间的巨大差异。比如研究明清时代的政治制度，基本上只限于《明会典》、《清会典》关于衙门设置、官员配置及其职掌或权限的规定，只限于《大明律集解附例》、《大清律例》和《六部则例》、《理藩院则例》之类的刑事或行政性法典（规）的规定，只限于《职官志》或《百官志》、《食货志》、《刑法志》等关于制度改革或运作的记载。把这些规定或改革当作古代政治制度或体制的全部实际情形。虽然有时也引用野史或正史中极少数实际事例（案例）记述，但又过于偏爱引用负面的记述，即关于成文制度被暴君和权臣践踏的事例。只从卷帙浩繁的典章、政书中找古代制度，而不从实际行政过程和社会生活的活生生事实中去总结潜在的制度惯例及其实际运行模式。[5]
                                　　<p>从前制度史研究著作的上述弊端，有很多人作过批评或局部的纠正，但一直缺乏比较全面深入的纠正。瞿老的《清代地方政府》一书正是纠正这类弊端、活生生地再现在中国古代政治过程中实际形成和运作的政治制度或惯例的典型范例。
                                　　<p>这是第一本系统、深入研究清代地方政府模式的专著。实际上，它研究的不仅仅是那一个时期的政府组织机构及其运作，而是研究一种“政府传统”。这本书可以说是对清代二百多年间（甚至上溯至明代）政府传统的研究，进而可以看作是关于整个中国的政府传统的研究。这种研究，着重于研究在一个相当漫长的时间里形成的一以贯之的政治机器构成和运作的传统。这种对传统的认识，是在把各个不同的历史阶段的丰富多彩的变化加以抽象、归纳总结，并相对忽略那些没有持续性或规律性的偶发事件或因素的影响后得出的。这本书已经出色地引导了我们去把握近古中国地方政府体制和运作的传统。
                                　　<p>本书没有象从前的政制史著作那样用现代政府职能分类的概念去分别列举讨论清代地方政府的财政、治安、司法、教育、福利、军事、经济等各方面的职能机构（人员）及其运作模式，而是从“哪些人构成（或参与）政府”和“政府做（及如何做）那些事”的两条线索出发来探讨政府传统。书的第一章（州县政府），首先介绍州县政府在中国古代政府机构体系中的位置，特别强调它作为唯一“亲民”的政府层级的特殊性和重要性。紧接着，第二、三、四、五、六章，分别探讨地方政府的五类正式组成人员（州县官及其书吏、衙役、长随、师爷等四类助员），包括他们的资格和录用、职位分类、职能及行使方式、待遇和升迁机会、贪污腐败形式、监督和约束模式等等。然后，第七、八、九三章，是分析州县地方政府的职能及其执行方式。地方政府的职能以刑名、钱粮为典型，所以各专用一章来探讨司法和税收。第九章大致介绍了地方政府除司法、征税以外的各项次要职能（如户口登记、治安、邮政、公共工程、公共福利、教育教化、祭祀等等）。最后一章，探讨中国特有的半官半民或作为官民中介的地方精英集团——士绅集团对地方政治的参与和影响。把他们作为地方政府的非正式组成人员来探讨。这种研究构思，的确令我们耳目一新。
                                　　<p>（一）本书的内容重心在于政治或政府体制中的个人及其行为，重心在于对政治制度体制传统的完整性认识。这对于我们完整地认识中国古代地方政府的特质有着十分重要的意义。
                                　　<p>首先，本书以州县官作为地方权力的化身，以其为州县地方政府的主干或本质。瞿老说，州县政府的所有职能都由州县官一人负责，州县官就是“一人政府”，分配到地方的一切权力都无可分割地被确定为州县官这一职位的独享权力，其他一切僚属显然只扮演着无关紧要的角色。除非得到州县官的委派（包括有独立辖区的佐贰官），否则都没有任何规定的权力。州县官职位或其个人，是把地方一切事务或政治职能整合起来的关键或枢纽，透过他的行为可以考察地方政府的一切。州县官的这种“一人政府”属性，瞿老给我们作了多方面的阐释。例如，州县官是地方一切事务的唯一受托人和责任人，税收、司法、治安、教育、福利、公共工程等等，归根结底是他一人承担，一人负责任的。税收完不成，官库有亏空，盗匪未抓获，水利工程毁坏，司法有错案，人口有逃漏，驿站死了马，科考有舞弊，理论上都由州县官一人负责并受罚，除非法律特别规定其他僚属或书役要一同负责。轻则罚俸、包赔、降级，重则革职、受笞杖，直至判处徒流刑罚。[6]
                                　　<p>其次，从四类助员自身功能及与州县官的关系的探究，也同样阐明了古代政府体制的“混沌整体”特色。这种特色进一步说明了“一人政府”：这几类个人在政府中的地位作用，实际上就体现为他们与州县官关系。他们随一人进退而进退，他们与州县官权力之间的依附关系的程度注释了他们的地位作用。瞿老的研究告诉我们，他们不是现代意义上的政府成员，仅仅是州县官行使权力的走卒或工具，但又有着不可忽视的作用。在州县官和他的四个辅助集团（书吏、衙役、长随和幕友）之间，没有任何中间权力，四类人都直接向州县官负责。他们共同协助州县官行使的是同一个州县官的混沌一体的权力，这种权力我们不可能用现代的职能分类、权力制约的眼光去观察。这四个集团之间实际上不存在行政权力或职能的分工分类，只有在一人政府或一人权力思路下的事务分派。幕友是州县官的私人秘书或顾问，干的是脑力方面的活儿；书吏是州县官们的文字方面的工役，干的是起草抄写之类的文字手工活儿；衙役是州县官的体力方面的工役，干的是缉捕、行刑、抬轿、传递、守卫之类的力气活儿；长随是州县官们用来监督前三者并在三者之间传达州县官指令并跑腿、跟班、照顾生活起居的角色。他们都围绕着州县官这一个中心转动。他们之间只有活儿粗细、受州县官信任和委托的轻重、与州县官关系亲疏的差别，没有实质的权责分工差别。他们都是州县官行使那个混沌一体的权力的工具而已。这一点，用曾为师爷30多年后出仕为县官的汪辉祖的话说，叫做“官须自做”：“事无巨细，权操在手，则人为我用。若胸无成见，听人主张，将用亲而亲官，用友而友官，用长随吏役而长随吏役无一人非官。人人有权即人人做官，势必尾大不掉。故曰官须自做。”[7] 
                                 只能允许一人做官，以保证政府是一人政府。
                                　　<p>再次，四类助员之间职能的严重重叠，更说明了“一人政府”及州县政府的“混沌整体” 
                                属性。例如，关于税收事务，幕友中有“钱谷”、“征比”、“钱粮总”等负责，书吏中有户房书吏、“总书”、“里书”、“柜书”、“漕书”等负责，长随中又有“钱粮”或“钱漕”、“杂税”负责。关于刑名（司法）事务，在幕友中有“刑名”、“案总”负责，在书吏中有刑房书吏、“招书”负责，在长随中有“案件”、“呈词”、“管监”、“值堂”等负责，在衙役中有捕快办事。关于仓库事务，幕友中有“廒友”负责，长随中有“司仓”负责，书吏中有“库书”或“仓书”，衙役中有“库丁”、“库卒”、“仓夫”等。关于警卫保安或及文书命令传递事务，衙役中有“门子”、“禁卒（子）”等负责，长随中又有“司阍”、“门上”或“门丁”（“门上”中又细分为多种差事）。关于挂号登记收发，幕友中有“挂号”，长随中有“签押”、“号件”，书吏中有“承发房”书吏。关于通信，幕友中有“书启”或“书禀”，书吏中有“柬房”书吏，长随中也有负责通讯的“书启”。关于文书起草誊抄，幕友中有“硃墨”或“红黑笔”，书吏中各房皆有起草誊抄公文任务（特别是低级书吏“帖写”）。这种叠床架屋的职岗设置，清人谢金銮评论说：“凡此头上加头，脚下添脚，直以官场为戏，自取纷淆而增弊窦，以虐民害官而求其必败而已。”[8] 
                                 
                                <p>谢氏当然无法理解：这是一人政府体制的必然。既然权力高度集中于一人，被视为一个囫囵整体，那么就不能对政府职能进行全面横向分工分权，而只能由四类人分别去办理同一类事务的不同阶段。每一阶段派一类人办理，后一类人有监督前一类人的责任，甚至同一类人中的每个人也有互相监视的责任。每人“螳螂捕蝉，黄雀在后”般地盯住别人，最后所有人都被州县官本人盯着。黄宗羲在批评三代以后的专制之法时说：“后世之法，藏天下于筐箧者也：利不欲其遗于下，福必欲其敛于上；用一人焉则疑其私，而又用一人以制其私；行一事焉则虑其可欺，而又设一事以防其欺。”[9]黄氏的这一批评，也适用于地方官权力高度集中时的情形。皇帝的权力是一个大筐箧，州县官的权力是一个小筐箧，都必须“以人制人、以事防事”的方式来看护。顾炎武说：“郡县之失，其专在上。君人者……人人而疑之，事事而制之，科条文簿日多于一日，而又设之监司……。”[10]这也适用于州县机构或职员设置情形。瞿老说，幕友、书吏、长随之间职责的这种重叠，实即在政府职员之间推行隔绝政策――亦即不让他们有机会相互接触的政策的必然结果；这作为一种控制方法不仅使得有效的监督难以实现，这种思路造成的组织不善和缺乏协调必然导致地方政府的效率低下[11]。
                                　　<p>最后，州县衙门没有法定的财政收入，没有正常的经费预算和决算制度。理论上讲，州县官个人的薪水要支付州县衙门的行政开支及职员薪水。这也充分说明了“一人政府”及其政府体制的“混沌整体”属性。瞿老在引言中说：“对于期望有专门章节论述地方财政的读者来说，本书为什么没有这种内容，应是显而易见的：中国的地方政府并没有自己的岁入；州县官们必须用他自己的收入来支付办公费用和个人开销。公务开支和私人开支之间是没有什么界限的。因此,在州县官薪给的标题下讨论地方财政也许更妥当。在那里，‘惯例性收费’（‘陋规’）制度将被讨论。”[12]瞿老这一处理，是用社会学的方法认识中国古代政府的财政问题的一个典范。在第二章“州县官的收入与地方财政”一节中，瞿老总结了清代州县官的全部收入为两者：一是朝廷规定的薪俸（包括名义薪水和养廉银），二是“陋规”收入。瞿老的研究向我们展示，这两种收入，虽然来源有区别，但它们共同构成了州县官的个人收入暨一个州县政府的财政收入。这也是州县官给衙门职员支付工资和满足一切办公费用的经费来源。他要用这些收入来解决（或满足）以下一切费用（耗费）：幕友和长随的工资，衙门办公文具的费用，上级衙门的名目繁多的摊捐(为弥补政府经费赤字，或为政府的特定活动费用，或为填补“历年亏空”)，接待到访或过境的各种官员的费用，为上司装修房舍添置家具薪炭的费用，各种节庆时给上司的礼敬费用（陋规），给上司衙门的职员送“门包”、“茶仪”、“解费”等费用，还有最重要的税银税粮的熔铸或储运的损耗，赋税的征收和转运过程中必要的费用，等等。[13]我们看到，这些费用，多半应该是由政府财政支付的费用，是维持地方政府行政运转所必须的办公费用。但是，这些费用，都要州县官一人去支付。本来是地方政府的正常行政费用，现在都变成了州县官的私人负担。朝廷不为此拨付专门经费，那么只好容许地方官员们利用“陋规”筹措这些经费。因为在政治理念上把州县官假设为一人政府，这些费用当然就要由你自己解决。
                                　　<p>此外，书中关于州县的司法、税收和其他职能如户口登记、治安、邮政、公共工程、公共福利、教育教化、祭祀等等的研究，也是以官员的权限责任及其权力行使方式为着眼点，是以人（包括官员、助员、士绅）的地位作用和活动模式为中心的制度史研究，而不是象过去那样以机构和权力关系为中心的研究。瞿老在这些研究方面也给我们以出色的示范。
                                　　<p>（二）注重非正式的私人性的因素对传统地方政府体制和政治行政过程的重要影响。
                                　　<p>瞿老此书的另一个重要的特征或贡献是：非常独到而全面地阐释了非正式的、私人的因素在地方政府体制和政治行政过程中的地位或对它的深刻影响。瞿老通过以下几个方面揭示了这种地位和影响。
                                　　<l>士绅对地方政府或政治的非正式或私人性参与。瞿老的研究揭示，士绅作为地方精英，其代表地方社区的权利，是得到政府和公众普遍认可的。他们可在地方官与百姓之间充当斡旋者，向地方官员提供咨询，受官员个人委托主持慈善机构和事业等等。但是，他们在地方行政的方面扮演的角色，完全是非正式的、私人性的。他们作为百姓的某种意义上的代表，既非百姓选举，也非政府任命，只不过凭藉特权地位而被习惯上接纳为地方社群的代言人而已。但是他们参与政府事务和代表地方社群说话的权利或义务，并没有象西方民选议员那样在法律上正式明确下来。如果说有什么权利或义务的话，那主要是道德义务，并且主要是依据自觉的和非正式的标准去履行的。法律并没有规定哪个士绅成员应该被咨询或应被邀请参与行政事务，这些都主要随州县官们自便。尽管士绅可以且实际上常常干预（政务），但却没有他们可以用来质疑或否决官员所作决定的合法程序。实际上，士绅的介入主要是基于私人标准或因素，其效力也主要依赖于特定士绅个人所具有的影响力——个人社会地位的高低及与官员的私人交谊深浅。根据美国学者C．E．麦瑞安的观点，所谓政府，应视为“统治过程的参与”或“决策过程的参与”，因而它既包括“正式的”政府，也包括“非正式的”政府。瞿老认为中国的士绅集团实为中国地方的“非正式政府”，地方权力只在官吏（正式政府）和士绅（非正式政府）之间进行分配。这种分配也是非正式的。[14]
                                　　<l>衙门职员的私人性任用。衙门里的四类职员中，最重要是幕友和长随，但他们只是州县官私人聘用的参谋和私仆，通常不作为政府雇员来看待。他们虽与州县官之间维持着一种私人的、非正式的关系，但确确实实操作着地方政府的部分权力。他们的聘用，都不需经政府的正式考试考核选拔，而是凭着与州县官的特殊私人关系——或为朋友同窗，或为出仕前的家仆，或为子弟宗党亲戚，或为上司私人推荐的人。其总的任用原则是“任人唯亲”，总之必须是私人关系亲近，有强烈的私人信任。                             用亲属（特别是“三爷”）为幕友可能是当时最普遍的情形，所以清醒的官员如汪辉祖者就大声呼吁革除这一积弊：“谚曰莫用三爷，废职亡家。盖子为少爷，婿为姑爷，妻兄弟为舅爷也。之三者，未必无才可用，第内有蔽聪塞明之方，外有投鼠忌器之虑也。……总不若择贤友而用之。友以义合，守义则尊而礼之；苟其负义，何嫌乎绝交？”[15] 
                                 
                                汪辉祖虽反对用亲，但并不是主张通过正式的、官方或公义的关系或标准来任用幕友。他所主张的用友，仍是凭非正式的私人的关系或标准。“友以义合”的“义”，仍旧是私人恩义，不是公义和法律。此外，长随的工作模式，基本上是以私人方式监督衙门的其他职员（甚至包括幕友），在监督问题上完全充当州县官的鹰犬的角色，以保证州县官个人不被书吏衙役们欺骗蒙蔽。这与政府公务中的分工负责互相监督制约完全是不同的思路。
                                　　<l>地方政府靠介于合法非法之间灰暗的“陋规”方式建立其财政岁入，州县官和衙门职员们的也多以“陋规”方式取得薪水，这些途径都是私人的非正式的途径。瞿老告诉我们，州县官及其助员们取得陋规收入有五花八门的途径。首先是税收时加收“火耗”、“余平”、“耗米”、“耗羡”，这是朝廷基本半正式认可的“陋规”收入。这可以看成是地方政府的财政收入。因此，以弥补熔铸、储运中损耗的名义预先加收这样一个折扣额，实际上变成了地方官搭车加收的某种“地方税”。不过，这一收入的最初立意并不是用在地方建设或福利事务，而是用于保证足量完成朝廷规定的税收任务的必要费用或损耗。至于事实上地方官们可能将这些收入部分地用于衙门的办公费用、地方公共工程和福利救济，但那都是体制外的安排，是完全出自地方官个人的名声需要或道德责任感。此外，地方官员们还直接或通过衙门职员之手间接得到各种陋规收入（或其中一部分）。如向管理银库或粮库的书吏衙役、被任命为头领的衙役、值勤的衙役、担任负责催税的“里长”或“催头”的百姓、想逃避看守州县仓库之杂徭的富户百姓、为官府或公共节庆提供商品或服务的行户、请领保甲门牌户册时的民户、接受土地面积勘测或领取赋税定额凭单（“由单”）时的税户、到公堂接受审讯的两造及证人或仅仅参与办案的书吏衙役、申请灾害勘查和救济时的百姓……，都可以索要种种习惯承认的“陋规”（各种礼金或费钱）。这些与前者不同，大概是不会进入正式可以在前后任官员之间移交的衙门收支帐册记录的，这纯粹是私人性的灰色收入。这些收入，既可以看作是地方财政收入的一部分（因为它的确有一些用于本当由正式预算经费满足的公事需要），也可以看作是官员的个人收入或者（欠额）薪俸的变相补偿方式。至于衙门职员的薪水，朝廷正式预算解决的只有书吏和衙役（书吏薪水曾被取消），但那仅仅是象征性的，如衙役每年六、七两银子，书吏每年十几两银子，这只够他们和妻子每天吃一顿或两顿最粗简的饭。于是，他们只好借助“陋规”和更出格的贪狡伎俩谋取收入，如仅仅诉讼过程中，衙役就可以向当事人索要“鞋袜钱”、“酒饭钱”、“车船钱”、“招结费”、“解锁钱”、“带堂费”等等，书吏可以向当事人索要“纸笔费”、“挂号费”、“传呈费”、“买批费”、“出票费”、“到案费”、“铺堂费”、“踏勘费”、“结案费”、“和息费”等等，任何官员对此都不能不默许（顶多稍加约束而已），因为有时的确是解决薪水不足的需要。有的官员甚至公开主张，可以委托书吏办理一些偶发的讼案，以便他们从中搞到一些“纸笔钱”；对那些勤勉尽职的衙役可以多签发一些传（捕）票，以便他们从中多收点“草鞋钱”[16]。这纯粹是私人的、非正式的解决方式。至于幕友、长随的薪水完全由州县官自己从个人薪水或其他收入中支付，就更不必说了。他们通过获得各种委派差事向百姓索取名目繁多的陋规收入，其中相当一部分就是州县官有意安排的补充薪水的方式。瞿老提醒我们，应该把“陋规”与贿赂或其他形式的贪污区分开来，因为陋规是在法律的默许之内的，而贪贿是法律禁止的。但是，在某些情况下，二者之间并没有一个明确的分界线[17]。这正揭示了私人性非正式因素在古代政治中的一种实际作用——混淆了公与私、合法与非法 
                                。
                                　　<l>其他行政方式或过程中也充满了非正式的私人性的因素。比如，作为地方政府的最重要的公务——税收，并不是仅仅依靠国家官吏完成，很多情况下依靠民间非正式的途径或力量。先是依靠家中成年男丁最多的里长甲首，运用其在乡村的个人影响力催税。然后又搞“催头制”，由五户或十户人家中欠税最多者为“催头”，代理政府衙役向欠税人（首先是自己）催收赋税。催头交纳完自己的欠税后，这一苦差事就转给别的欠税最多的花户。又如，政府为督催交税而采取的“比责”笞惩，最能反映利用非正式的私人性因素于政治过程。如果有税户拖欠赋税，首先是乡村的里长甲首要受讯笞，其次是派到乡下催税的衙役（“图差”、“里差”）要受讯笞，最后是欠税花户本人要受讯笞。里长甲首和衙役因为别人欠税而受笞惩，这显然是为了迫使他们为逃避笞惩创痛而不择手段地去逼花户交税，甚至迫使他们自己掏钱代欠税花户交税。只要能完成税收任务，一切正式的、公义的、官方的规则可以不管。再比如，在办案衙役超期未能捕获罪犯时，法律甚至允许州县官将办案衙役的家属拘禁起来，以示惩罚和督促。将衙役的亲属拘禁来催促衙役尽力追捕罪犯，这做法简直有些滑稽，这无疑是法律正式允许在行政中利用私人情感恐惧等因素。还有，州县官向省城和府城派驻“坐省家人”和“坐府家人”从事上下级之间的感情联络工作，也是典型地在运用非正式的私人性的因素。这种在上司衙门附近设办事处的做法，与我们今天的下级地方政府向上级政府驻地派设办事处有些相似，但也有着关键的不同。首先，派驻的办事人员是“家人”即长随，是州县官的私人仆役，不是政府的正式工作人员；其次，其经费由州县官私人支付，不是政府经费预算；再次，其所做的事情基本上是私事——与上司的师爷、长随、书吏沟通或拉关系，帮主人打探上司的好恶动向、代主人（每逢节庆时）向上司送礼、帮上司添置家具装修房舍、贿赂上司的师爷书吏以避免上呈报告被驳回。
                                　　<p>（三）运用了从前政治制度史研究者们易于忽视的大量丰富而生动的史料，揭示了书面规定的制度与实际生活中的制度之间的巨大差异，揭示了在许多行政场合法律并未被执行的严重事实，向我们展示了一套更加活生生的制度运作模式。
                                　　<p>瞿老在本书中运用的史料广泛，是我所读过的著作中罕见的。包括各类官员或幕友记载自己为政或辅政经历、体会的札记、笔记、杂录或自己奏疏、公牍、判牍的汇编，包括各类民间人士编撰的野史、述闻，包括各类衙门或官员编制的统计册簿、办公指南手册，包括各地方编制的地方志、赋役全书等等；当然还包括为了各种各样的具体政事问题的解决而发布的皇帝诏谕、朝廷则例。这些中就具体个别具体行政问题而发者，也是易于被从前的政治制度史著作所忽视的。我粗略统计，瞿老在本书中共引证了中文史料370种（此外还参考了西文著作资料66种，日文著作25种），其中引用最多的是官员或幕友的笔记、杂记类，有《学治臆说》、《学治体行录》、《学仕录》、《刑幕要略》、《庸吏庸言》、《从政遗规》、《从政录》、《病榻梦痕录》、《不慊斋漫存》、《公门要略》、《办案要略》、《樊山判牍》等160余种；引用各种册簿、全书或办公指南手册者，如《鄂省丁漕指掌》、《各行事件》、《海州交代驳册》、《阜邑款目簿》、《律法须知》、《补注洗冤录》、《门务摘要》、《牧令须知》、《晋政辑要》、《钱谷备要》、《荒政备览》、《审看拟式》、《河南赋役全书》、《缙绅全书》、《户部漕运全书》、《津门保甲图说》、《浙省仓库清查节要》、《湘阴县图记》、《六部成语注解》等31种；引用《安徽通志》、《长汀县志》、《番禺县志》、《东平州志》、《华阳县志》等地方志68种；引用各种政书、律令、条例、诏谕汇编如《大清会典事例》、《大清十朝圣训》、《定例汇编》、《江苏省例》、《吏部铨选则例》、《粤东省例新纂》、《责成州县条规》、《钦颁州县事宜》等23种；引用《漫游野史》、《河北采风录》、《海虞贼乱志》、《金坛见闻记》、《嘉定屠城记》等野史15种；还引用了其他史料或著作70余种。一本正文字数仅仅13万字（汉译字数）的著作，注释引用史料或著作460余种，注释多达1685条之多，注释文字达15.2万多字。这的确有些让我们叹为观止了。在政治制度史类著作（他们通常主要是引用正史、律例）中引用如此丰富多彩的札记、手册、野史、方志类史料，这还是我见到的第一次。
                                　　<p>瞿老引用如此广泛丰富的实际政治生活史料，当然不是为了炫示史料丰富，而是一位娴熟于社会学研究方法以及受美国实证主义哲学深刻熏陶的历史学者，自自然然地用来作为总结阐明社会生活中实际存在和运作的政治制度的充分必要依据。瞿老的研究，让我们看到了文字上的制度与社会生活中实际运行的制度之间的巨大差异。
                                　　<p>瞿老首先在本书各章的分析中让我们看到，清代地方政府的行政是由中央统一行政法典严格控制或调整的。这些法典非常详密，格外追求一致、准确、服从和集权。但是，在一个幅员辽阔民族众多的大国里，这种单调统一的规范也必然带来操作上的巨大困难且导致效率非常低下。法律条文过于严苛、僵化，不允许州县官作出个人判断或创设规则；也不给悬殊的地方性差异留下变通的余地，从而妨碍了州县官根据本地的任何特殊情况调整行政方法。这样一来，必然导致地方实际政治制度向表面上合法实际上非法，或者虽然非法但多少有些情理依据的方向发展。从这一视角出发，瞿老要求我们将考察视野超出法律法令的范围。他说：“法律法令总是关于政治行为的考查资料的一部分，因为它们规定并在某些情况下制约了官吏们及其管辖下的人民的行为。但是，对一个政治体制的研究，如果仅凭据法律法令，总是不全面不充分的。法律法令并不总是被遵守，文字上的法与现实中的法经常是有差距的。因为这一缘故，我力图超越法律及行政典章来勾划实际运作中的地方政府之轮廓。”[18] 
                                瞿老告诉我们：“许多法律法规并未真正被实施，或多或少流于形式。这一问题几乎在行政的各个方面都显露出来。举例而言，关于书吏衙役的服务期限问题，及关于衙门陋规问题的法条就是如此。但这并不意味着官员及其僚属可以随心所欲。如果规范某些程序的正式规定无法操作时，他们就不得不遵循成规。对成规的任何改动都可能遭到人们的反对。因此，全体衙门成员都渐渐形成了一套自己乐意且当地百姓也（勉强）接受的行为规矩。”这样发展的结果，就会造成这样的情形：“国家和公众看作越轨或腐败的行径，也许被（官吏们）看作遵循行业性约定俗成的行为规范（行规）而已。”[19]
                                　　<p>地方政府行政中的实际作法与法律规定有着明显差异，这种实际作法成为当时地方行政制度的实际构成部分，或对朝廷制定的行政制度构成修正或补充，其情形在瞿老的书中用了较多的篇幅进行揭示和分析。这体现在以下许多方面。
                                　　<p>例如，在第一章中，瞿老告诉我们：虽然《清会典》、《吏部则例》中明确规定佐贰官不得受理诉讼，州县官也不得让佐贰官受理诉讼，违者要受降级调用的处分。但是，实际上，许多州县官都允许其佐贰官受理一些轻微诉讼案件，以期方便他们捞到一些额外收入，也没有谁受处分。在第二章中，瞿老介绍，虽然《大清律例》明文禁止地方官员在迎送上级官员过境时大事铺张并致送金钱礼物，但现实中这种奢侈招待和大肆送礼（包括给上司的随从人员送礼）几乎成了惯例；法律明确禁止官员向百姓摊索或以低于市价的官价“购买”生活用品或其他财物，但现实中以官价向百姓购物也成了惯例，一些廉洁有名的州县官公然在自己的书中主张保留这一惯例。在第三章中，我们看到，法律禁止以任何形式买卖书吏职位，但是皇帝也无可奈何地感叹，继任书吏向离职书吏交一笔购买岗位的价金（“缺底”）几乎成了牢不可破的惯例。在第四章中，我们看到，法律禁止设挂名衙役或额外增雇衙役，违者州县官降二级调用；但是即使是廉洁有能的官员如刘衡等人也承认这种规定不可能兑现，并说增雇的做法只能“瞒上不瞒下”；关于书吏衙役的服务期，法律规定分别是五年和三年。期满继续留任者，州县官要受惩处。但事实上，许多书吏衙役期满后改名换姓留在岗位上，也没有什么处罚。在第五章，我们看到，法律禁止州县官向上司驻地派驻家人，但事实上派驻“坐省家人”和“坐府家人”专事给上司操办房舍装修采买季节性消费品并进行沟通联络成了惯例，已经无人反对了。在第六章，我们看到，法律禁止雇佣本地人为幕友，但事实上官员们不执行此一禁令。在第七章，我们看到，法律规定州县“自理词讼”必须在20日之内审结，违者罚俸或降级。但是，官员们常常无视这一期限，随意拖延。我们还看到，法律规定了命盗重案破案及捕获罪犯的几个严格期限，最后期限届满尚未破案者要受降职处罚。然而各省督抚们常常是在最后期限届满之前就将该州县官调任他职，以解除实际降职的危险。在第八章，我们看到，《户部则例》明确规定，在花户以铜钱代替银两交纳赋税时，必须按照省督抚（依市场一般兑换率）确定的折算率交纳，擅自抬高折算率盘剥百姓的官员将受惩罚。但是，事实上，各地官员们无视规定比率，而按高出市场42%-75%的比率收取铜钱，并且不被视为违律，未遭禁止。在漕粮的征收中也是如此，花户必须多交40%-50%的粮食作为“耗米”，远远超出朝廷规定的“耗米”加收额度，但也几乎成为惯例；法律规定不得强迫花户用银钱代替交纳漕粮，但官员们则无视禁令，总是强迫百姓用远高于市场粮价的比例交钱代替漕粮……。
                                　　<p>这类情形不胜枚举。瞿老的书中随处可见这样的举证和分析，这些丰富的实例向我们展示了一套丰富多彩的实际存在行政制度。这些情形，当然不仅仅是对国家成文制度的破坏，实际上可以看成是对那些制度的更加务实的补充和完善。因为即使是破坏也是有明确限度的，这些限度为州县官和百姓共知共守。瞿老说：“朝廷对陋规的失控，……这并不意味着州县官们可以随意征收陋规费。收费额度不得不受当地所有的人都了解的习惯的规约和限制。交费人愿意交纳与习惯性目的及数量相符的陋规，但如果官员或衙门中人需索过高或擅立名目，他们会拒绝交纳。”在某些地方，“人们总是按照一个固定的换算率交纳赋税，哪怕市上的换算率已经发生了变化。如果州县官擅自提高换算率，就会引起骚乱。显然，习惯是使陋规保持在某一限度之内的力量；聪明的州县官只好简单地依惯例办事。”[20] 
                                这些实际惯例尽管没有书面规定的制度那么美妙，但却是现实中形成的实际有效制度，或者构成了无论如何努力也难以革除的实际有效的坏制度（这时可能就不便用好坏来评价了）。这无疑使我们更进一步认清了中国古代地方政治制度的真实。
                               <h> <center>三</center>
                                　　<p>瞿老在本书中所进行的政治制度史研究，有以上三个方面的出色创新，是一种“行为分析”类型的研究。这与从前偏重于政治行为的结果分析或偏重静态制度（文物典章）的分析的政治制度史研究有着典型的区别。瞿老说：“所有行为分析必须放到特定的情境中进行，也就是说，我们必须按照任何行为在具体社会和政治条件中实际显示的情形来思考分析这些行为。从这种意义上讲，在特定政治环境中的中国官僚的行为方式，一般说来也应阐明中国行政管理及官僚行为的一般规律。”[21]瞿老的研究，已经通过州县地方官员在特定社会和政治条件下遵守或超越法律执行职务的行为方式的分析，出色地阐释了中国古代实际行政制度或官僚行为模式的一般模式或规律。这一阐释势必对中国政治法律制度史的研究产生重要的影响。
                                　　<p>关于这本书的理论贡献，哈佛大学出版社在出版按语中说：
                                　　<p>“这是第一本系统、深入研究清代地方政府的专著。此前从未有过类似的著作，不论是中文、日文还是西文的。这本书系统分析了清代州县官的职能及其运作，包括：征税，司法，长随和幕友的使用，对于书吏和衙役的依赖，以及公堂或衙门内的办事程序等。作者通过各种手本和札记，同时参考大量的官方资料，全面考察了在清代地方政府正式体制中的非正式人事因素的运作，特别考察了地方精英或乡绅对政府管理过程的参与，深入考察了地方利益对政府政策的影响，为我们提供了迄今为止最为完整的关于中国地方行政运作的图解。”原按语说，在本书出版之前，“关于中国政府的整个传统，关于它长于权衡利害的大量做法和设计（置），一直没有人以现代的方法进行过分析”，而该书的出版已经初步弥补了这一缺陷，它正是以现代方法分析中国古代政府传统的典范。[22]这里分析的是清代地方政府，实际上也是汉代以后二千余年间中国地方政府的一般模式或缩影。
                                　　<p>美国《亚洲研究学报》认为《清代地方政府》“是一本极为重要的书，其目的在于描述、分析并解释清代州县地方政府的结构和功能；作者也希望此书有助于政治制度的比较研究，并且为官僚政治与行政学（研究）提供资料。他的目的和希望都出色地达到了。”[23]
                                　　<p>《美国历史评论》第68卷第2期（1963年）也发表专文评论《清代地方政府》一书。评论说，在中国政府及行政的研究领域中，“此书前进了一大步。由于对资料拥有广泛的知识，并具有洞悉内幕的见解，他提出了关于中国最低层政府的第一部有意义和可靠的研究。”[24]
                                　　<p>海外学者评论本书贡献或引用本书成果的情形甚多，来不及一一细述。我在这里也用了万把字的篇幅介绍评论这本书。由于我对社会学研究方法不熟悉，也许会对瞿老在书中的更重要的成就或贡献视而不见。不过，我的这篇文章的主旨并不是要评价这本书（我自认为没有资格评价），而是浅谈一些阅读体会，给年轻的读者一些参考而已。当然，作为读者和译者，我也感到瞿老这本书也有不足。比如书中没有关于佐杂官员（僚属官员）法律地位和在地方行政中的地位的专章研究，不能不说是一个缺憾。一个州县的佐贰官有州同、州判、县丞、主簿等，有吏目、典史等“首领官”，有巡检、驿丞、税课司大使、仓大使、闸官、河泊所官等“杂职官“，可能有几人、十几人乃至几十人之多，他们都是朝廷任命的有官阶品级或虽未入流但有专司的官员。瞿老在本书中的篇章安排（仅在第一章“州县政府”概说中有千多字的简介），易于给人们造成一种误解：为数不少的佐杂官员，似乎仅仅是州县官的无足轻重的徒附；虽然有官员身份，但其在地方行政中的实际作用影响没有不具官员身份的幕、仆、书、役四者那样值得关注，或者说还不如这四类人。这大概是不完全符合历史的。应该说，州县地方政府是由州县官和他们的五种（而不是四种）助员（佐杂官、幕友、书吏、长随、衙役）共同组成。全面系统研究佐杂官员的法律地位及其在地方行政中的实际作用模式，也是非常重要的。其他方面的缺憾或许也能找到。但是，这些缺憾应该说是一本正文仅13万字的著作所难免的。我们总不能指望这么一本小篇幅的书面面俱到地阐明清代地方政府和行政中的一切吧。
                                　　<p>２００３年１０月２５日改定。
                                --------------------------------------------------------------------------------
                                <l>该书于1962年由美国哈佛大学出版社（harvard university 
                                press.）出版。现由范忠信、晏锋、何鹏翻译为中文，列入法律出版社“法学研究生精读书系”于2003年出版。
                                <l>
                                参见韦庆远《中国政治制度史》，中国人民大学出版社１９８９年版，目录；王汉昌《中国古代政治制度史略》，人民出版社１９８５年版，目录，１－７页；张晋藩《中国古代政治制度》，北京师范学院出版社１９８８年版，目录；储考山等《中国政治制度史》，上海三联书店１９９３年版，目录，１－９页。
                                <l> 张晋藩《中华法制文明的演进》，中国政法大学出版社１９９９年版，目录。
                                <l> 
                                参见杨鸿年、欧阳鑫《中国政制史》，安徽教育出版社１９８９年版；左言东《中国政治制度史》，浙江古籍出版社，１９８６年版，７２－７４页，２９９－３００页。
                                <l> 
                                例如，有的著作主张：“政治制度史史料除应注意到二十四史各志、《文献通考》以外，还可以利用一些‘正史以外的’资料，如‘历代会要’等”。再没有提到其他更加能反映当时古代实际运作中的政治制度真相的史料。其实，历代会要也有正史性质，因为多系据正史辑录。参见李明晨《中国古代政治制度史纲》，中国政法大学出版社１９９０年版，引言，３－５页。
                                <l>
                                瞿同祖《清代地方政府》，范忠信等译，法律出版社２００３年版，６０页，９０－９２页，１１９－１２２页；１５１－１５３页；１８９页，２１１－２１４页，２３３页，２４３－２４７页，２６０页，２６１页，２７９２８０页。
                                <l> 〔清〕丁日昌辑《牧令书辑要》卷一引汪氏《居官通论》。
                                <l> 〔清〕丁日昌辑《牧令书辑要》卷二引谢氏《居官致用》。
                                <l> 〔清〕黄宗羲《明夷待访录·原法》。
                                <l> 〔清〕顾炎武《亭林文集·郡县论一》。
                                <l> 瞿同祖《清代地方政府》，第３３５页。
                                <l> 瞿同祖《清代地方政府》，第４页。
                                <l> 瞿同祖《清代地方政府》，第４０－５７页。
                                <l> 瞿同祖《清代地方政府》，第２－３页，３３３－３３８页。
                                <l>〔清〕 丁日昌辑《牧令书辑要》卷二引汪氏《用亲不如用友》。
                                <l> 〔清〕方大湜《平平言》卷二。
                                <l> 瞿同祖《清代地方政府》，第二章第三节，特别见第４７页。
                                <l> 瞿同祖《清代地方政府》，引言，第２页。
                                <l> 瞿同祖《清代地方政府》，结论，第３３６页。
                                <l> 瞿同祖《清代地方政府》，第二章第三节，第５０－５１页。
                                <l> 瞿同祖《清代地方政府》，引言，第１－２页。
                                <l> 瞿同祖《清代地方政府》，封底，哈佛出版社原版按语。
                                <l> Journal  of  Asian  Studies, 
                                Vol.23,1963,p.59。
                                <l>American History Review,Vol.68:2,1963，p.35。
                                  
                                　　
