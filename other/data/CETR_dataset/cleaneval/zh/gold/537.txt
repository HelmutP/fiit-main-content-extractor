URL: http://www.cp.com.cn/scrp/bookdetail.cfm?iBookNo=929&sYc=1-1
                      <p>书名：     语言疚
                      <p>ISBN：     7-100-03663-1/H·931
                      <p>作者：     华劭 著
                      <p>开本：     32开
                      <p>装订：     平
                      <p>字数：     300 千字
                      <p>定价：     ￥23.00
                      <p>版印次：   1-1
                      <p>责任编迹  冯华英
                      <p>出版日期： 2003-06-01
                      
   　　<p>语言是非常复杂的现象。对此，各种学派众说纷纭，每种学说似乎精彩与偏
   颇并存，既使人增进知识，又给人留下遗憾。看来，从不同角度、不同层次，以
   不同方法去剖析语言的各个层面、各类单位和各种功能，从而接近或者说逼近所
   研究语言现象的本质和全貌，也许不失为一种合理的主张。作者受工作性质和知
       识水平限制，难于广泛论述各家学说，只能以语言研究和言语研究为经
   纬，把语言学中一些重要的问题编织在一起，不敢奢望构筑科学体系，只是试图
                           摸索出探讨课题的路数。
   　　<p>这本书是作者在讲课稿的基础上撰写的。1985年前后，作者受命为研究生开
   设“普通语言学”。虽然上世纪五十年代作者在前苏联学过这方面的课程，但一
   些知识已被遗忘，另一些则显得陈旧，只得结合我国俄语教学界的实际情况，自
   编教材，从头做起。具体做法是以从语言研究到言语研究为主线，围绕若干重要
   课题，广泛搜集选择材料，介绍不同观点，找出问题要害，确立研究方向。当然
   ，也谈一些自己的观点，与学生共析疑义。由于这些课题对认识语言、从事俄语
   研究和教学都很重要，“普通语言学”这门课程得到了好于预期的效果，部分讲
                       稿内容，也随着毕业生流传校外。
   　　<p>在行将退下讲坛之前，在领导、同事和学生的鼓励与敦促下，作者花了一年
   多的时间，重新编写。其中，删掉了两章，即“话语的实际切分——主位与述位
             ”及“言语的心理活动过程——话语的产生与理解”。原
   因是关于前一问题，我已发表数篇论文，对其新的发展，来不及概括、反映；对
        后一问题，则坦言认识不够深入，有待继续研究。其余各章均是对原
   稿加工整理而成的。成书后，李锡胤教授通阅，并提出一些宝贵意见。李红儒同
                   志参与全书校勘工作，付出了辛勤的劳动。
      　　<p>作者从事俄语教学与研究已整整五十年了。我们这一代人可以说已经
   尽力了，作者在1949年学俄语时，用的竟是日本人八杉真利编的词典和俄侨乌索
   夫编的语法。半个世纪之后，我国俄语教学与研究事业取得了长足的进步。但也
   毋庸讳言，受历史条件的限制，我们所做的贡献还不大。我国语言学，包括俄语
   语言学，与世界先进水平还有不小差距。对年轻的后继者，我们寄予厚望。若这
         本书能对他们在今后前进道路上有所帮助，作者就算达到目的了。
                 　　<p>最后，作者对鼓励、支持本书出版的黑龙江
   大学以及俄语系和俄语语言文学研究中心的领导，校内外同行，商务印书馆的编
                             急硎境现康母行弧
         　　　　　　　　　　　　　　　　　　　　　　　　　华　　劭
      　　　　　　　　　　　　　　　　　　　　　　于哈尔滨　黑龙酱笱
      　　　　　　　　　　　　　　　　　　　　　　俄语语言文学研究中心
       　　　　　　　　　　　　　　　　　　　　　　　　2001年7月20日
   　　<p>本书从语言研究的对象、性质、语言符号的诸多层面、语言的发展、篇章语
   言学范畴内的特质等方面，把语言和言语结合起来，给语言学的整体研究打开了
    一扇新的窗口，是高校中、外文语言学系科以及个大语言学读者的必读书目。
                   <p>第一章　语言学研究的对象　　语言与言语
                      　　<l>一　语言学研究的对象物和对象
                            　　<l>二　不同的语言观
                        　　<l>三　言语和语言的相互关系
                        　　<l>四　划分语言和言语的意义
                    <p>第二章　语言的符号性质　　能指与所指
                             　　<l>一　什么是符号
                         　　<l>二　语言符号的结构要素
                          　　<l>三　语言符号的特殊性
                <p>第三章　语言符号系统是层级装置　　单位与层次
                             　　<l>一　层级与层次
                    　　<l>二　语言系统中基本功能单位的层次
                         　　<l>三　本体层次与分析层次
                      　　<l>四　表达层面与内容层面的层次
                 <p>第四章　确定语言单位　　线性单位与集合单位
                        　　<l>一　为什么要确定语言单位
                   　　<l>二　在言语的可见层次上分离线性单位
                　　<l>三　区别、同一、线性单位与集合单位的关系
                      　　<l>四　各基本集合语言单位的组成
               <p>第五章　语言单位间的关系　　组合关系与聚合关系
                              　　<l>一　组合关系
                              　　<l>二　聚合关系
                       　　<l>三　组合和聚合的联系与对照
                    　　<l>四　组合关系与聚合关系的广泛运用
                    <p>第六章　语言的系统性质　　单位与结构
                             　　<l>一　语言的系统
                          　　<l>二　语言系统中的单位
                          　　<l>三　语言系统中的结构
                      <p>第七章　语言的发展　　共时与历时
                             　　<l>一　共时与历时
                         　　<l>二　语言发展变化的动因
                        　　<l>三　关于语言发展的规律性
                 <p>第八章　超符号层次单位的研究　　语义与句法
                　　<l>一　语言学研究把重点转向高层次的语言单位
                       　　<l>二　从形式到意义的句子研究
                       　　<l>三　从意义到形式的句子研究
                   　　<l>四　从语义与句法的结合切入句子研究
                          　　<l>五　句子的语义与转换
                         　　<l>六　衍生的复杂语义结构
                <p>第九章　从语用角度研究话语　　说话人与受话人
                            　　<l>一　什么是语用学
             　　<l>二　言语行为中的核心结构要素——说话人与受话人
                              　　<l>三　言语行为
                          　　<l>四　语句中增生的意思
              <p>第十章　篇章(话语)语言学　　篇章的关联性与整体性
                       　　<l>一　作为研究对象的连贯话语
                             　　<l>二　篇章的单位
                        　　<l>三　篇章单位之间的关联性
                        　　<l>四　篇章及其单位的整体性
            <p>第十一章　语句中名词的指称　　逻夹灾赋朴胗镉眯灾赋
                     　　<l>一　名词的指称、词义和句法功能
                             　　<l>二　指称与逻辑
                             　　<l>三　指称与语用
          <p>第十二章　语言的社会属性　　作为社会现象与文化现象的语言
                          　　<l>一　什么是社会语言学
                             　　<l>二　语言与社会
                             　　<l>三　语言与文化

   　　<p>华劭，1930年出生，祖籍湖北浠水。1951年毕业于哈尔滨外国语专门学校。
   50年代曾就读于莫斯科大学语文系。长期从事语言理论的教学与研究，主攻普通
   语言学、俄语语法学，并涉及语义学、语用学。著有《现代俄语语法新编(句法)
   》，改编苏联科学院新编的《俄语语法》(句法)。参与编写和编撰国家统编教材
   《现代俄语通论》、《大俄汉词典》，曾发表过数十篇学术论文，其中一部分被
                            收入《华劭论文选》。
       　　<p>华劭现为黑龙酱笱Ы淌凇⒉┦可导师、吉林大学兼职教授，任经
                           教育部审批成立的黑龙江
   大学俄语语言文学研究中心学术委会员主任。1990年在莫斯科获得世界俄语语言
                      文学教师联合会颁发的普希金奖章。
