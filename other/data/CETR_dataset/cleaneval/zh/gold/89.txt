<h> ICQ狂聊记录(二)
<p>   作者出版申明   　　由于工作的原
   因，平常接触外国朋友非常多，积累了一些所谓的经
   验，就迫不及待想与众网友分享。

<p>   　　在这里要向各位网友“老实交待”的是，实际上这个专栏中的对话并不完全
   是和Nikky一人的聊天记录，其中也把与另外一些外国朋友的聊天记录也“暴力
   ”地嫁接了进来，最主要的原
   因是我感觉别人也有不少可以学习的语言点。但有一个可以向大家保证的是和我
   谈话的都是地道的美国人士，语言方面的地道性可以三包，接下来的任务就是如
   何让别人相信通过ICQ聊天也能增强英语的实战能力了…
   (注：本专栏会随着我的聊天进程而不断有新内容加入的，敬请关注！)
   
<h>               四、网恋、摇滚和电影
 
<p>   我们不象外界说的那样优秀，但也没有他们说的那样差------这是第四次跟她在
   网上见面，很多时候我不是一个话很多的人，但这里碰上了一个能够把谈话继续
   下去的外国朋友，也许是我的幸运吧。
<h>   ·她突然问我了一个我经常想但又不知道答案的问题

<p>   你有没有可能在网上爱上一个你以前从来没有遇到的人？如果有人突然这么问你
   你会怎么回答？
   
<h>   ·电影和盗版

<p>   对吉姆.卡维泽是非常喜欢，他的眼睛里有一种忧郁，不是装出来的，他表演的
   时候已经完全沉浸在电影的气氛当中，我是真的很喜欢他的演技，甚至要盖过拉
   丁天后，那个在美国家喻户晓的性感的女明星。
   
<h>                五、聊聊中美两国股市的情况 
   
<p>   这一次见面话题不知怎么就扯到股市上去了，可能跟Nikky的专业有关吧，这方
   妫我是个半瓶子水。
<h>   ·中国股市情况也好不到哪去

   我们可以从侧面来看待这种情况：当平均指数由最高点下落50%时，有的股票可
   芟碌90%，有的是100%，有的是200%，当个股从100跌到50时，下挫幅度为50%，
   但对于从50升到100的股票来说，涨幅就是100%了，我们要想持平的话还要走很
   长一段路。 
   
<h>                六、宠物--最好的朋友
<p>   这次见面持续的时间不是很长，Nikky有点头痛。
<h>   ·它是公的还是母的/is it a he or she?
   
<p>   我的宠物，一只小狗，我最亲爱的朋友，对我很忠心，在我生命中占有很重要的
   位置。有时候，它比别的任何人都了解我，当我真正感到孤独的时候，它总是坐
   在那里，看着我。

<h>   ·愿下次见面的时候你会好起来

<p>                         那你应该休息好，睡觉前喝点热水，会感觉好一点的。

<h>                七、工作和兴趣 
<p>   工作、生活和兴趣，我们永远在追求它们的和谐统一。
<h>   ·我要把我的兴趣和工作融合起来

<p>   除非你喜欢高尔夫，否则就不要去打，要不你是不会成功的。对工作也是如此…
   
   
<h>   ·生活中会有很多你不喜欢的事情，这就是生活

<p>   在中国，有个崔永元是我喜欢的中央电视台节目主持人之一，但他的访谈对象不
   是什么名人，都是些很普通的老百姓，但这个节目很精彩，感人，而且幽默。

<h>   ·耐心点，试着把事情做的更好

<p>   你有没有这样的感觉，当你真的很努力很努力工作的时候，别人却对你的成绩说
   不。
   
<h>   ·希望有一天中国也能在世界上有如此的地位

<p>   美国在全球炯弥惺橇斓甲饔茫如果美国炯貌缓玫幕埃整个世界的经
   济也不会太好，希望有一天中国也能在世界上有如此的地位。
 
<h>  八、现在就给你打电话么 

<p>   这是我第一次听见Nikky的声音，是录在留言机里面的，非常清晰，但语速很快
   不知道下次跟她直接说话的时候会不会一样的快。
<h>   ·你对别人的议论会怎么看
   
<p>   在自寻烦恼的时候，有些话的确可以起到意想不到的作用，使人解脱，比如：do
   n’t trouble trouble until trouble troubles you.

<h>   ·如果你能来中国，我给你做免费导游

<p>   如果你真来的话，提前告诉我一声，我会盼望着和你相见的那一天。
   
<h>   ·如果我现在给你打电话你会怎么办

<p>   我等着你的电话，但如果你不想打的话，你是不用打的。
   
   
<h>                九、因为爱所以怀念 
<p>   这一次我们都谈到了各自的爷爷，记忆勾起了我们对往事和亲人的怀念。
<h>   ·讲述对爷爷的记忆
 
<p>   你不必说对不起，我的爷爷一直体弱多病，所以去世的时候也是一种解脱，但我
   还是很怀念他。
 <h>  ·孩子和教育
<p>   我会在他们足够大的时候，比如说10岁的时候告诉他们一些事情，毒品呀，性呀
   等等，他们对这些事情是懵懂的，告诉他们以后就可以提高他们的辨别力了。
<h>   ·歌手兼作曲人Kenny和他的歌
<p>   Nikky谈到了她很喜爱的一个歌手兼作曲人Kenny，而我则闻所未闻，我一点也不
   为自己的孤陋寡闻感到沮丧，因为我知道这其实就是文化的差异。
<h>   ·Heather and Her First Step
<p>   我仍然记得她迈出第一步时的情形　我想我是多么幸运目睹这一切。
   
<h>   十、虽然有分歧，但是我们仍然能够和平相处
<p>   每次和美国人谈到有关政治性的话题时，总会或多或少的产生一些分歧，此乃正
   常，我们受到的新闻舆论和导向是不一样的，但这次和Nikky稍微触及美国即将
   撂倒伊揽说男卸，感觉其实分歧并不大。
<h>   ·想告诉她，现在的中国发展真的很快
<p>   有一种情况经
   常遇到，那就是没有来过中国的外国人总是想当然的把中国想的很落后，很贫穷
   ，这个Nikky的前任男朋友就对在中国看到的4星级和5星级饭店感到惊讶，原
   来中国还有这么好的星级饭店呀，而且价钱也不贵。
<h>   ·我们分歧并不大
<p>   美国的检查员在伊揽说比徊皇芑队，人家的领土你凭什么来检查？
<h>   ·中美之间的关系
<p>   Nikky说，“我认为中美两国现在很好，不管是克林顿政府还是现在的布什政府”。 

    
