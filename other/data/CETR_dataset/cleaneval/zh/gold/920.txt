﻿URL: http://www.tuku4u.com/modules.php?name=Sections&sop=viewarticle&artid=2065



                            

   <h>第八章
   <h>(3461 个字)
   <h>(167 次阅读)    友好打印 
   　　<p>汤玛斯医生往后靠在椅背上，用修长优雅的手摸摸浓密黑亮的头发。他的年
   纪很轻。外表看来虽然很不成熟，但是他对路克患风湿的膝部的诊断，几乎和一
   星期以前哈理街那位专家的诊断完全一样。
   　　<p>“多谢你了，”路克说：“既然你觉得电疗有效，我就安心多了，我还不希
   望这种年纪就变成跛子。”
   　　<p>汤玛斯医生孩子气地一笑，说：“我想不会有什么危险，菲仕威廉先生。”
   　　<p>“啊，你让我安心多了，”路克说：“我本来想去找一位专家，可是现在我
   相信用不着了。”
   　　<p>汤玛斯医生又微笑道：“要是你觉得那样比较放心，还是去看看为好。无论
   如何，听听专家的意见总不会有错。”
   　　<p>路克迅速说：“人在这些方面往往很容易害怕，你一定了解这一点吧？我常
   常想，医生应该会觉得自己像个术士——对病人来说，他就像魔术师一样。”
   　　<p>“信心往往占了很重的分量。”
   　　<p>“我知道，‘医生说’好像已境闪舜表权威的话。”
   　　<p>汤玛斯医生耸耸肩，幽默地说：“要是病人都明白这一点就好了。”又说：
   “你正在写一本有关法术的书，不是吗？菲仕威廉先生。”
   　　<p>“咦！你怎么知道？”路克有点装腔作势地惊呼。
   　　<p>汤玛斯医生似乎觉得很好玩，“哦，亲爱的先生，像这种地方，消息传播得
   非常快，因为实在没什么好聊的话题。”
   　　<p>“不过也许会被人过分夸大，改天你说不定又听说起在召唤鬼魂，并且和恩
   多的女巫在比赛法力呢。”
   　　<p>“奇怪，你怎么会这么说？”
   　　<p>“为什么奇怪？”
   　　<p>“因为有人谣传说你已菊倩焦汤米·皮尔斯的鬼魂”
   　　<p>“皮尔斯？皮尔斯？就是那个从窗口掉下去的小男孩？”
   　　<p>“是的。”
   　　<p>“这——怎么会呢？——对了，我跟那位律师提过——他姓什么——是艾巴
   特吧？”
   　　<p>“对，故事就是从他那里传出来的。”
   　　<p>“难道说我已臼挂晃煌纺岳渚驳穆墒ο嘈攀澜缟嫌泄砘甏嬖诹寺穑俊
   　　<p>“这么说，你本身相信有鬼魂了？”
   　　<p>“听你的口气，你好像不相信，是吗？医生，不、不能说我真的‘相信有鬼
   魂’，不过我确实知道有些人突然奇怪的死亡或者暴死。可是我最有兴趣的还是
   跟暴死有关的各种迷信——例如被谋杀的人不会在坟墓里安息，还有凶手如果去
   摸被害的死者，死者的血就会流个不停。不知道这些传说是怎么来的？”
   　　<p>“很奇妙，”汤玛斯医生说：“不过我相信现在已经
   没什么人记得这些了。”
   　　<p>“当然比你想象中要多，不过我想这里也没有什么人被人谋杀，所以很难判
   断。”
   　　<p>路克说话的时候带着微笑，眼睛仿佛很随便地看着对方的脸，但是汤玛斯医
   生似乎仍旧非常镇定，也对他报以微笑。
   　　<p>“是的，我想我们这儿已经
   ——嗯，很多很多年——没有凶杀案子。起码我这辈子都没听说过。”
   　　<p>“是啊，这地方非常安详平静，不会有什么暴行，除非——有人把那个叫汤
   米什么的小男孩从窗口推下去。”路克微笑着说。
   　　<p>汤玛斯医生又带着他那充满孩子气欢乐的自然微笑说：
   　　<p>“很多人都恨不得扭断那孩子的脖子，不过我想还不至于真的有人会从窗口
   把他推下去。”
   　　<p>“他好像非常顽皮，也许有人觉得除掉他是义不容辞，替大家服务的事。”
   　　<p>“可惜这种理论只能偶尔引用一下。”
   　　<p>“我一直觉得，连续除掉好多人会对地方上有益，”路克说：“我不像一般
   英国人那么尊重人命，我觉得任何阻敖步的人都应该除掉。”
   　　<p>汤玛斯医生用手伸进美丽的短发中摸摸头，说：“不错，可是谁
   又有资格做裁判呢？”
   　　<p>“学科学的人就有资格，”路克说：“那个人必须心胸正直，头脑灵活，有
   高度专业知识——譬如说医生之类。说到这一点，我倒觉得你本身就是很好的裁
   判。”
   　　<p>“判决哪些人不该活下去？”
   　　<p>“是的。”
   　　<p>汤玛斯医生摇摇头，说：“我的工作是使不适合活下去的人变得适合活下去
   。我承认，在大部分情形下，这是件很辛苦的工作。”
   　　<p>“可是我们还是不妨来讨论一下，”路克说，“就拿已故的海利·卡特来说
   　”
   　　<p>汤玛斯医生尖声道：“卡特？你是说‘七星’的老板？”
   　　<p>“对，就是他。我不认识他，可是我堂妹康威小姐提过他的事。他好像是个
   十足的大恶棍。”
   　　<p>“噢，”对方说：“不错，他嗜酒如命，虐待太太，欺负女儿，爱跟人吵架
   ，又爱乱骂人，跟这里大部分人吵过架。”
   　　<p>“换句话说，世界上没有他这个人会更好？”
   　　<p>“我想可以这么说。”
   　　<p>“事实上，要是有人从背后把他推进河里，那个人可以说是为了大家着想才
   下手的了？”
   　　<p>汤玛斯医生冷档厮怠！澳闼说的这些手段是不是你曾经
   在——是马扬海峡吧？——用过呢？”
   　　<p>路克笑道：“嗯，不，这只是我的构想，不是真有这种事。”
   　　<p>“嗯，我也觉得你不像天生的杀人凶手。”
   　　<p>“告诉我——我很想知道——你有没有碰到过你觉得像杀人凶手的人？”
   　　<p>汤玛斯医生尖声道：“奇怪！你居然会问这种问题！”
   　　<p>“是吗？我想医生一定见识过各种奇怪的人物，譬如说，他一定会比别人提
   早发现杀人狂的早期症状。”
   　　<p>汤玛斯有点生气地说：“这完全是外行人对杀人狂的看法，以为他一定会拿
   着刀到处乱跑，嘴边不时吐些白沫
   。我不妨告诉你，杀人狂也许是世界上最难看出的病症。从外表上看，他也许和
   平常人完全一样，也许是个很容易受惊的人，也许他会告诉你他有些敌人。可是
   除此之外什么迹象都没有，一点也不讨人厌。”
   　　<p>“真的？”
   　　<p>“当然是真的。有杀人狂的疯子，常常认为自己是为了自卫才杀人。不过当
   然啦，有很多杀人凶手就像你、我一样正常。”
   　　<p>“医生，你这话可让我觉得坐立不安了！想想看，改天你也许会发觉我曾经
   一声不响地杀过五、六个人呢。”
   　　<p>汤玛斯医生微笑道：“我觉得不大可能，菲仕威廉先生。”
   　　<p>“是吗？彼此彼此，我也不相信你杀过五、六个人。”
   　　<p>汤玛斯医生愉快地说：“你没把我职业上的失败例子算在内。”
   　　<p>两人都笑了起来，路克站起来道别，用抱歉的口气说：
   　　<p>“对不起，打扰了你好久。”
   　　<p>“噢，没关系，我不忙，卫栖梧是个很健康的地方。真高兴能跟外地来的客
   人聊聊。”
   　　<p>“不知道　”路克没往下说。
   　　<p>“什么事？”
   　　<p>“‘康威小姐要我来找你看病时，曾靖嫠吖我，你实在非常…
   ∴牛医术实在很高明。我在想，你留在这种小地方会不会觉得太埋没自己的才
   闪耍俊
   　　<p>“噢，能从小地方着手也是一个好的开始，能得到很宝贵的狙椤！
   　　<p>“但是你不可能一辈子就这样待在乡下不求发展。听说你的已故对手汉伯比
   医生就没什么野心，一直安安分分，很满足地在这里行医。我想他在这里一定住
   了很多年了吧。”
   　　<p>“事实上他一辈子都住在这里。”
   　　<p>“听说他很正派，就是太顽固了点。”
   　　<p>汤玛斯医生说：“有时候他的确很难相处，对新设备很不信任，不过对老派
   的内科医生来说，他倒是位很好的先进。”
   　　<p>“听说他留下一个漂亮的女儿。”路克用戏弄的口气说。
   　　<p>他很有趣地看着汤玛斯医生白皙的面孔胀得通红，并且说：“嗯——嗯——
   是吧！”
   　　<p>路克用亲切的眼光看看他，很希望能把他从自己的嫌疑名单上除掉，一会儿
   ，后者恢复了正常，忽然说：“谈到犯罪，如果你对这方面有兴趣，我可以借你
   一本书，是从德文芬牍来的，克鲁哈玛写的《自卑感与犯罪》。”
   　　<p>“谢谢你。”路克说。
   　　<p>汤马斯医生伸手从书架上找出那本书，说：“就是这一本，其中有些很惊人
   的理论。虽然只是理论，倒也蛮有意思的。例如‘法兰克福屠夫’孟兹海的早年
   生活，喜欢杀人的小保姆安娜·海伦等，都非常有意思。”
   　　<p>“好像她杀了十多个托她照顾的小孩之后，别人才发现事情真相。”路克说
   。
   　　<p>汤玛斯医生点点头，“对，她的性格很惹人同情——她非常爱孩子，每个孩
   子死的时候，她真的都悲痛欲绝。这种心理实在很叫人惊讶。”
   　　<p>“这些人居然能逍遥法外那么久，真奇怪。”
   　　<p>这时他已经
   走到门口阶梯上了，汤玛斯医生送他出门，说：“也没什么好奇怪的，其实你知
   道，容易得很。”
   　　<p>“什么东西容易得很？”
   　　<p>“逍遥法外啊，”他又露出孩子气的迷人微笑，“只要小心点可以了，聪明
   人一定会非常小心，不留下任何痕迹。这就够了。”他又笑笑，然后走进屋里。
   　　<p>路克站在门口看着阶梯发呆。医生的微笑中有一种谦卑的意味，他们谈话当
   中，路克一直觉得自己像个完全成熟懂事的大人，而汤玛斯医生却仿佛是个年轻
   无邪的少年。但是此刻，他却有一种完全相反的感觉，医生的微笑就像一个大人
   对聪明淘气的孩子的那种纵容的微笑。
   
