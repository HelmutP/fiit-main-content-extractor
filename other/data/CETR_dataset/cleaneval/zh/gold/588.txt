

   <h>科学地认识校长培训中五个关系
<p>湖北教育学院副教授　王世忠
   　　<p>校长培训作为人力资源开发与管理的一个重要组成部分，其地位和作用已越
   来越为更多的中小学办学实践所证明：一个好的校长就是一所好学校。但在如何
   组织开展中小学校长培训以及通过培训所要达到的目标等问题认识上，总是局限
   于领导重视、经
   费投入和制度保证方面，很少能从校长培训自身的义务与权利、管理与服务、不
   足与潜力、灵活与多样以及训练、教育与发展等方面着手。为此，我们必须科学
   认识并处理好校长培训五个关系，以构筑良好的校长培训机制。
   <h>校长培训义务与权利的关系
   　　<p>以往，我们把培训作为学校的一项任务来布置，如今，培训必须考虑和满足
   学校自身发展的需求。人力资源开发部门应向有关教育行政部门和学校的管理人
   员灌输这样一种观念：校长参加培训，既是课程改革需要，也是个人成长发展之
   需。在许多场合，教育培训机构组织的培训似乎成了布置给教育行政部门或学校
   的“课外作业”，但与此同时，又有许多校长利用业余时间自费报名参加社会上
   举办的培训。究竟是培训信息不能有效传播，还是教育行政部门和学校负责人对
   培训认识不足？也许培训内容与校长发展的联系不紧密是一个方面，但校长是否
    懂得参加培训是自己应有的权利，也是一个重要方面。
   <p>由于这些认识问题未很好地理顺，因而在教育干部培训管理方面，尽管绞尽
     脑汁
   解决参训校长出勤和遵守纪律问题，但一些校长可能“三天打鱼两天晒网”，一
   些校长无视培训规章制度的存在，我行我素，许多参训校长把培训部门提出的要
   求，理所当然看成是“你们的要求”，而从未想过校长培训究竟因为什么而得以
    存在。
   <p>应当说，教育干部培训在学校人力资源的效能提升上和校长自身发展所起的
   作用，已越来越显著。一些学校的校长由于缺乏培训而成为只拥有熟练技能的“
   简单劳动的技术操作工”，而另一些学校的校长由于重视参训，他们的劳动价值
   随学校的发展而得以成倍增加。所以，作为教育干部培训工作者，有责任也有义
   务让每一个校长明白：关心并参与培训，不仅仅是学校单方面的事，它还意味着
   <h>校长如何把握未来发展的主动权。
    校长培训管理与服务关系
　<p>现代教育干部培训的部门，多是作为学校的管理部门来设立的。作为现代人
 力资源开发与管理的一个重要组成部分，培训部门虽有管理校长培训经
   费、发布培训信息以及进行培训效果评价的职能，但更重要的是为各学校和每一
  个校长提供培训服务。
   　　<p>培训的基础是培训需求、教育行政部门需求和校长需求，所以，教育干部培
   训工作者，必须主动向各教育行政部门及不同层次的校长征求培训需求，才有可
   能制订出切实可行的培训计划。培训内容的设计也应来自参训人员不同工作岗位
 上的工作感受。
 <h>校长培训不足型与潜力型的关系
  <p>在不足型与潜力型关系的逻辑
   对照中，涉及一个培训价值分析的问题。这种价值分析的过程，按照现代校长工
   作表现识别理论，通常包括以下几个步骤：
   　　<p>首先，区别校长是“不会做”还是“不愿做”。如果是“不愿做”，就不是
    培训所能解决的问题，而应从工作环境、领导方法、发展机会等方面找砸颉
   　　<p>其次，对于“不会做”的校长，也不应马上安排参加培训，而要找出他不会
  做的原
   因，也许是因为某种客观条件制约其业绩和表现，所以，不妨在帮助他排除客观
    障碍
   后，考虑为其设计或选择相应的培训课程，进行针对性培训，帮助他从不会到会
   。在培训阶段，应以鼓励为主，必要时可破除把考核作为鞭策或监督校长培训的
   手段的传统做法。
   　　<p>教育干部培训一方面必须注重校长的需求满足，另一方面还必须注重投入与
   产出的效益均衡性。我们要把更多的培训机会给那些有发展前途或有进取心和危
   机感的校长，而不是把有限的培训资金投入到不想或不愿做好本职工作的校长身
   上。培训不是万能的，培训只是帮助校长教育管理能力由弱到强，让优秀的更加
   优秀，让一般的能充分发挥出其潜力，以更称职地工作在校长岗位上。
   <h>校长培训训练、教育与发展的关系
   　　<p>从校长培训整体来看，校长培训包括三个层次的内容，即训练、教育和发展
   。训练是知识和技术的掌握，校长学习后直接用于工作，使管理工作绩效得到改
   善和提高；教育是校长满足未来工作、知识、技能需求的预备性学习；发展则是
   规划人的一生，开发人的潜能，并使校长个人成长与学校发展相互促进的过程。
   　　<p>由于三者的目标不同，故其培训的内容也不同。校长岗位培训的目标是直接
   提高校长的管理工作技能，其突出强调培训内容与当前工作的针对性和实用性。
   因此，在确定训练目标及其内容时，培训单位应对学校工作及校长进行全面分析
   ；另一方面要充分考虑教育行政主管人员的意见，因为主管人员最了解本部门所
   需及校长技能的欠缺。若在设计训练方案时，让教育主管人员参与设计，可使训
    练既符合校长的实际，又能满足学校工作的需求，达到训练的最佳效果。
   　　<p>教育和训练虽然都是以校长为对象，培训性质却迥异。一般来说，教育较训
   练所花的时间要长一些，培训目标则是针对校长未来工作所需的知识和技能加以
   培养，在学习上比较重视观念和理论教育。训练则不同，所用的时间较短，而且
   内容偏重务实的应用，以便训练学习结束后，即可运用在实际工作当中。所以教
   育目标与内容的确定较为复杂，学校必须具有与学校中长远发展战略相适应、与
   校长生涯路线相吻合的人才资源开发预测与规划，才能明确未来教育的目标和内
   容，才能使学校和校长双方的培训投资效益得到最大化。
   　　<p>发展也是校长培训的重要内容之一，它的作用是树立新理念，以开发校长潜
   能。每个人身上都蕴藏着巨大的不可估量的潜能，而且这种潜能通过培训可大大
   地开发出来，使每个人的工作更加卓越。因此，要依据人的发展特性，科学地确
   定校长培训目标及内容，有计划地、有效地通过培训，开发校长们的潜能。这种
    培训将成为校长人才资源开发培训的一大走势。
    　<h>校长培训灵活性与多样性的关系
   　　<p>中小学校长培训属于成人教育类型，它具有学习的从属性和实用性特点，这
   决定了校长教育培训形式的灵活性。校长培训可根据市场经
   济和现代教育制度的实际需要，以及员工自身的特点，灵活地选择形式，制订教
    育培训计划，调整教育培训专业与内容，为学校培养合格的校长。
   　　<p>校长培训的多样化首先表现在参训校长的多样化上。在某种程度上说，从校
   长、副校长到作为校长后备力量的中层管理者，都应该是这种教育培训的接受者
   。这些人员在年龄、文化水平、管理能力、职务、技能需求等方面都有很大的不
   同，应采取不同的教育培训方法。其次，是教育培训内容的多样化。对于普通教
    育来说，长期以来已经
   形成了标准学科、系统传授知识的培养范式。但就校长教育培养而言，则是职务
   (岗位)需要什么就教什么，校长缺什么就补什么。例如新任校长需要加强基本职
   业道德观念，以及基本岗位技能和基础管理理论素养的培训；校长提高培训则立
   足于拓宽知识面，加快知识更新速度上；而骨干校长的研修应使他们尽可能更多
   地掌握本专业的国内外最新信息。
    <p>综上所述，校长培训从宏观层面看，培训的总体目标必须根据社会、经
   济和教育发展目标以及当前校长培训的水平与能力制定。从微观层面看，校长培
   训是为本教育行政部门、本学校服务的，是为提高学校管理水平进而达到学校工
   作目标而实施的。就校长本身言，通过培训使校长从学校管理的“物为本”转为
     “知为本”“人为本”，以及培养校长的教育新理念、增强战略管理意识。

                                    　　

                                                               中国教育报

                                    　　
     _________________________________________________________________

                    中国教育和科研计算机网版权与免责声明
   ①凡本网未注明稿件来源的所有文字、图片和音视频稿件，版权均属本网所有，
                       任何媒体、网站或个人未颈就协
         议授权不得转载、链接、转贴或以其他方式复制发表。已颈就协
   议授权的媒体、网站，在下载使用时必须注明"稿件来源：中国教育和科研计算
                       网"，违者本网将依法追究责任。
                                     ②
   本网注明稿件来源为其他媒体的文/图等稿件均为转载稿，本网转载出于非商业
   缘慕逃和科研之目的，并不意味着赞同其观点或证实其内容的真实性。如转载
             迳婕鞍嫒ǖ任侍猓请作者在两周内速来电或来函联系。
     _________________________________________________________________

                          相关业务问题与建议请联络
             Copyright(c) 1994-2003CERNIC,CERNET 京ICP备020072
    关于假冒中国教育网的声明 | 版权所有：中国教育和科研计算机网网络中心
