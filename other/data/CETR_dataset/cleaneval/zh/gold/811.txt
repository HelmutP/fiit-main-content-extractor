URL: http://www.nmg.xinhuanet.com/zt/2005-04/26/content_4129474.htm

       <p>１８８９年７月１４日，由各国马克思主义者召集的社会主义者代表大会，
   在法国巴黎隆重开幕。这次大会上，法国代表拉
   文提议：把１８８６年５月１日美国工人争取八小时工作制的斗争日，定为国际
   无产阶级的共同节日。与会代表一致同意，通过了这项具有历史意义的决议。从
   此，“五一”国际劳动节诞生了。
       <p>为什么要把这一天定为国际劳动节呢？这还得从１９世纪８０年代的美国工
   人运动说起。
       <p>当时，美国和欧洲的许多国家，逐步由资本主义发展到帝国主义阶段，为了
   刺激经
   济的高速发展，榨取更多的剩余价值，以维护这个高速运转的资本主义机器，资
   本家不断采取增加劳动时间和劳动强度的办法来残酷地剥削工人。
       <p>在美国，工人们每天要劳动１４至１６个小时，有的甚至长达１８个小时，
   但工资却很低。马萨诸塞州一个鞋厂的监工说：“让一个身强力壮体格健全的１
   ８岁小伙子，在这里的任何一架机器旁边工作，我能够使他在２２岁时头发变成
   灰白。”沉重的阶级压迫激起了无产者巨大的愤怒
   。他们知道，要争取生存的条件，就只有团结起来，通过罢工运动与资本家作斗
   争。工人们提出的罢工口号，就是要求实行八小时工作制。
       <p>１８７７年，美国历史上第一次全国罢工开始了。工人阶级走向街头游行示
   威，向政府提出改善劳动与生活条件，要求缩短工时，实行八小时工作制。罢工
   不久，队伍日渐扩大，工会会员人数激增，各地工人也纷纷参加罢工运动。
       <p>在工人运动的强大压力下，美国国会被迫制定了八小时工作制的法律。但是
   ，狠毒的资本家根本不予理睬，这项法律只不过是一纸空文，工人们仍然是生活
   在水深火热之中，倍受资本家的折磨。
       <p>忍无可忍的工人们决定将这场争取生存权利的斗争，推向一个新的高潮，准
   备举行更大规模的罢工运动。
       <p>１８８４年１０月，美国和加拿大的八个国际性和全国性工人团体，在美国
   芝加哥举行一个集会，决定于１８８６年５月１日举行总罢工，迫使资本家实施
   八小时工作制。
       <p>这一天终于来到了。５月１日，美国２万多个企业的３５万工人停工上街，
   举行了声势浩大的示威游行，各种肤色，各个工种的工人一齐进行总罢工。仅芝
   加哥一个城市，就有４．５万名工人涌上街头。这下，美国的主要工业部门处于
   瘫痪状态，火车变成了僵蛇，商店更是鸦雀无声，所有的仓库也都关门并贴上封
   条。当时在罢工工人中流行着一首“八小时之歌”，歌中唱道：
       <p>“我们要把世界变个样，
       <p>我们厌倦了白白的辛劳，
       <p>光得到仅能糊口的工饷，
       <p>从没有时间让我们去思考。
       <p>我们要闻闻花香，
       <p>我们要晒晒太阳，
       <p>我们相信：
       <p>上帝只允许八小时工作日。
       <p>我们从船坞、车间和工场，
       <p>召集了我们的队伍，争取
       <p>八小时工作，八小时休息，
       <p>八小时归自己！”
       <p>激昂的歌声唱出了工人的心声，唱出了全世界无产者的共同愿望，也感染了
   广大的群众，他们纷纷声援工人的罢工运动，将罢工运动推向新的高潮。
       <p>罢工运动所表现的巨大力量使政府当局和资本家极为恐慌，他们不甘心答应
   工人的条件，便露出他们狰狞的一面。５月３日，芝加哥政府当局终于撕下了“
   民主”的假面具，用暴力镇压工人。他们组织罢工破坏者在警察的保护下混进工
   人的罢工队伍，故意制造混乱，以此为借口，当场开枪打死六个工人。这一暴行
   ，激起了全市工人的极大愤慨，他们决心为死难的工人兄弟们报仇！
       <p>第二天晚上７点，３０００多名工人聚集芝加哥秣市广场，怀着沉痛的心情
   ，为死难工友们举行追悼会，声讨政府当局的暴行。
       <p>会议秩序井然，快１０点钟时，大会即将结束，突然，有２００名武装警察
   冲进了会场。一名警官大声叫道：
       <p>“散开！快散开！不准在这里聚众闹事！”
       <p>工人们纷纷抗议道：“为什么不准我们开追悼大会？”“你们故意制造血案
   ，还不许我们为死难者默哀，还有天理没有？！”
       <p>“少费话？我们奉令驱散非法集会！如果不听劝告，我们将用武力解决！”
   这位警官蛮横地叫道。
       <p>“举行集会是我们的自由，你们无权驱散！”工人们拒理力争。
       <p>警官见势不妙，举枪叫道：“弟兄们，把这帮穷工人立即驱赶出去！”
       <p>如狼似虎的警察马上冲进广场，抡起棍子，朝工人头上、身上打去，工人们
   奋起反抗，广场顿时大乱。
       <p>“轰！”突然响起了一阵强烈的爆炸声。原
   来，这是埋伏在工人队伍中的资本家走狗，故意向人群投掷的一颗炸弹。四名工
   人当场倒卧在血泊之中，几名警察也炸死炸伤了。
       <p>这是资本家故意制造的事端，以给政府当局大规模镇压工人一个借口。这下
   ，政府立即调动大批军警，疯狂向工人队伍开枪，有２００多名工人被打死打伤
   ，更多的工人被逮捕起来。
       <p>随之，资产阶级的报纸大造舆论，诬蔑提出八小时工作制的人是“投掷炸弹
   者”。
       <p>政府当局顺手推舟，把八名工人领袖交付法院，诬告他们犯了杀人罪。
       <p>陪审团主要是由大工厂的工头组成，而出庭作证的，则是被警察局收买的工
   贼。
       <p>结果，８月２０日，法院判处七人绞刑，一人１５年徒刑。工人不服，向州
   最高法院上诉，裁决是维持原
   判。后又向联邦最高法院上诉，他们根本就不受理此案。
       <p>这下，激起美国各地工人群众的强烈抗议，他们联合起来，纷纷举行抗议集
   会。但政府当局仍然于次年１１月杀害了其中四名工人领袖，三名改判无期徒刑
   。一名则死于狱中。美国芝加哥工人的鲜血，燃起了全国工人斗争的烈火，并迅
   速漫延到欧洲和其它大洲。全世界的工人阶级纷纷举行罢工运动，与万恶的资本
   家做殊死的斗争。
       <p>在世界进步舆论的广泛支持下，尤其是全世界工人运动的斗争下，美国政府
   终于在一个月后，宣布实施八小时工作制。美国工人运动终于取得初步的胜利。
       <p>为纪念美国工人１８８６年５月１１日的罢工运动，为推动整个世界工人阶
   级的斗争热情，就把这一天定为国际无产阶级的共同节日。在以后的每年５月１
   日，各国的工人阶级都举行大规模的示威游行，以争取自身的权利。
    
