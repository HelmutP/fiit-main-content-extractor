URL: http://www.ctiforum.com/expo/2005/iccchina2005/iccchina2005.htm
   <h>2005中国国际呼叫中心与客户关系管理大会
   <p>主办: 中国信息化推进联盟客户关系管理专业委员会
   <p>承办: CTI论坛 (www.ctiforum.com)
   <p>邪: 　优百斯管理咨询有限公司
   　　　 　美国呼叫中心管理学院
   　　　 　美国电话营销学院
   　　　 　韩国呼叫中心情报研究所
   　　　 　台湾客服中心发展谢
   <p>时间： 2005年4月13日15日
   <p>地点： 中国 上海 光大会展中心 (交通位置图)
   个人免费参会注册  厂商参展  展会服务  组委会声明
   　　　 　·参展厂商名录 　部分参会代表单位名录
   　　　 　·AnyTouch统一CTI应用平台新版本发布会

                                  <h>展会报道

   　 　 　
   　 　 <p>·"呼叫中心管理者之夜"沙龙 (AVAYA特别支持)[4月14日]（图片）
   　 　 <p>·专题演讲 [4月14日]（图片）
   　 　 <p>·2005中国最佳呼叫中心及CRM系列奖项颁奖仪式"暨开幕晚宴 [4月13日]
   　 　 <p>·展厅参观 [4月13日]（图片）
   　 　 <p>·主题演讲 [4月13日]（图片）
   　 　 <p>·呼叫中心现场参观 [4月12日] （图片）
   　 　<p>·Mary Pekas专题讲座 [4月11日] （图片）

   [iccchina2005t-3.gif]

        <h>2005年4月11日 星期一 (Mary Pekas专题讲座名额已满，报名截止)
             <h>美国电话营销学院总裁 Mary Pekas专题讲座 (在线报名)
        ·致参加“对话式软推销线索资格确认流程”讲座课程学员的一封信
                             <p>09:00am – 04:30pm

   <p>要点： 
   <l>电话营销项目选择
   <l>项目实施选择
   <l>人员安排选择及流失
   <l>营销中心的员工培训选择
   <l>技术挑战与机遇

                                 <p>预期收获：
   <l>学习和掌握美国电话营销学院（TII）的注册品牌产品“对话式软推销线索资
               袢啡狭鞒蹋–SS Lead Qualification Process）”
              <p>说明：

   <l>现场芬 <l>中英文讲义

           <h>2005年4月12日 星期二 (呼叫中心参观名额已满，报名截止)
                          <p>第一参观路线 <p>第二参观路线
               <p>09:30am – 11:30am <p>世纪新元 <p>招商银行信用卡中心
                         （CCCS认证五星级呼叫中心）
                             　　 　　 　　 <p>午餐
                             <p>02:00pm – 04:00pm

   <p>盛大网络

                                  <p>平安财险

   <p>为保证参观活动顺利进行，组委会可能会对参观路线做出适当调整。

                                  <h>会议日程

                            <h>2005年4月13日 星期三
                             <p>09:15am – 09:25am

                                <p>大会开幕致辞

                             <p>09:30am – 10:15am

   <p>K1主题演讲：奥迪坚亚太区总裁 Gilbert Hu
   ——北美呼叫中心市场的新趋势 

                             <p>10:15am – 11:00am

   <p>K2主题演讲：Avaya CRM高级顾问 毛熠星
   ——联络中心最佳实践

                             <p>11:00am – 11:15am
                                    <p>茶歇
        <p>11:15am – 12:00am <p>K3主题演讲：美国呼叫中心管理学院（ICMI）
                         总裁，CEO 布赖隆た死锔＠
                         ——高效呼叫中心管理的秘密

                                    <p>休息
                             <p>01:15pm – 02:00pm

   <p>K4主题演讲：上海贝尔阿尔卡特
   业务发展纠 袁军
   ——强大的阿尔卡特IP联络中心 

                    <p>02:00pm – 02:45pm <p>K5主题演讲：华为
                             <p>02:45pm – 03:00pm
                                    <p>茶歇
    <p>03:00pm – 03:45pm <p>K6主题演讲：益昂通讯亚太区总裁Troy Lynch (林卓悦)
                      ——正在改变今日呼叫中心的新趋势
                    <p>03:45pm – 04:30pm <p>K7主题演讲：Intel
                       INTEL融合通信中国区纠 林竞宇
                     ——基于开放融合通信标准的呼叫中心
                             <p>06:00pm – 08:30pm
        <p>"AVAYA—2005中国最佳呼叫中心及CRM系列奖项颁奖仪式"暨开幕晚宴
                      (在线报名) (名额已满，报名截止)

                                                          <h>2005年4月14日 星期四
                                                              　
                                  <h>运营管理
                            <p>商业呼叫中心运营专场
                                 <h>技术与应用
                               <p>IP呼叫中心专场
                                  <h>行业研究
                              <p>呼叫中心建设专场
                                  <p>09:30–
                                 10:10 <p>T211
                            太维公司总裁 李宝民
                   ——世界级服务水平的整体实施方法 <p>T221
                             汇卓科技顾问 夏方
                     ——IP多点分布式呼叫中心--汇卓CIC
                                    <p>T231
             台湾德鸿科技CALL CENTER事业处 技术支援部纠 黄忠悉
         ——witness solution 台湾呼叫中心人员绩效优化管理应用实例
                                  <p>10:10–
                                 10:50 <p>T212
          Witness Systems：全球高级营销总监 Jose S. Rosenbaum 先生
                      ----呼叫中心排班优化及管理 <p>T222
                       奥迪坚技术服务纠 Samuel Qiang
                         ——IP分布式呼叫中心 <p>T232
                                  佳讯飞鸿
                    ——平台模式的CTI应用，期待您的见证
              ——佳讯飞鸿AnyTouch统一CTI应用平台新版本发布会

   <p>10:50–11:10 　　 　　 　　 　　 <p>茶歇

                                  <p>11:10–
                                   11:50

   <p>T213
   臺晨头中心發展f
   理事長 褐菊\
   ——呼叫中心產I谳與增值服 

                                    <p>T223
                         CosmoCom中国区总纠 马闽雄
         ——The IP Contact Center - 在今天已是现实可行的技术 <p>T233
                              Sagatori公司CEO
                                西蒙.克瑞斯
                    ——建设一个新的呼叫中心和“THEM2”
                                    　　
                                    <p>休息

                                  <h>运营管理
                              <p>运营管理标准专场
                                 <h>技术与应用
                             <p>外拨及电话营销专场
                                  <h>行业研究
                                <p>金融行业专场
                                  <p>13:30–
                                 14:10 <p>T214
                     招商银行总行个人银行部总纠 刘建军
        ——提升呼叫中心价值，构建招商银行国际化标准的服务体系 <p>T224
                          Avaya CRM高级顾问 毛熠星
                       ——通过主动联络创造价值 <p>T234
                     浦东发展银行 产品开发部总纠 蒋瞳
                                  <p>14:10–
                                 14:50 <p>T215
                             优百斯管理咨询公司
                               认证总监 范军
              ——CCCS标准：艚兄行墓娣痘运营管理的方向标 <p>T225
                   eSOON 商业模式咨询顾问\资深总监 李⒃
                ——打造未来的客服中心,先进运营狙榉窒 <p>T235
                                  华胜天成
                 ——全方位IT解决方案，助力客服中心未来发展

   <p>14:50–15:30　　 　　 　　 　　<p>茶歇

                                  <p>15:30–
                                   16:10
                                    <p>T216
              一汽-大众销售有限责任公司 客户服务部 部长 荆青春
                     ——一汽４笾凇靶”CRM铸就客户忠诚
                                    <p>T226
                           博雅思顾问总监 苏厚昌
                         ——电话营销活动管理 <p>T236
                       韩国三星生命（人寿保险）高永根
                             ——面向开放市场的
                            保险业客服对策研究 
                                  <p>16:10–
                                 16:50 <p>T217
                      广东网通分公司客服中心主任 安新
                    ——以CCCS标准为工具，提升网通服务 
                                    <p>T227
                       美国电话营销学院总裁Mary Pekas
           ——美国电话营销产业25年的演变及世界范围的新趋势 <p>T237
                        中国平安财产保险股份有限公司
                            客户服务总监 胡海波
                       ——呼叫中心如何为保险主业服务
                                  <p>18:30–
   20:30

   <p>· "呼叫中心管理者之夜"沙龙 (AVAYA特别支持)

                            <h>2005年4月15日 星期五
                                    　　
                                  <h>运营管理
                                 <h>技术与应用
                             <p>电信与增值服务专场
                                  <h>行业研究
                            <p>市场及产业链研究专场
                                  <p>09:30–
                                   10:10

   <p>T311
   TCL移动通信有限公司
   营销中心 副总纠
   肖 锋
   ——以顾客为中心锻造产品、服务和流程 

                                    <p>T321
                    上海南康科技有限公司 技术总监 林哲明
               ——ITACATI“电访专家”-专业的外呼系统解决方案
                                    <p>T331
                   四方科润通信有限公司常务副总裁 黄宇辉
                        ——企业建立呼叫中心新视野 
                                  <p>10:10–
                                   10:50

   <p>T312
   贝塔斯曼直接集团中国区首席运营官
   艾弥尔

                                    <p>T322
                    CosmoCom 亚太区总监 Rayman Wong 先生
     ——CCOD业务存在的理由? 电信运营商和服务提供商新的业务增长点 <p>T332
                    HOLLYCRM（合力金桥软件） 付总裁 卢毅
                        ——政府与企业的服务制胜之道
                   <p>10:50–11:10 　　 　　 　　 　　 <p>茶歇
                                  <p>11:10–
                                   11:50

   <p>T313
   中国光大银行95595服务中心处长 刘丽伟
   ——呼叫中心在光大银行的应用与创新 

                                    <p>T323
                           盛大网络副总裁 王静颖
                     ——盛大客服中心人力资源管理 <p>T333
                           CTI论坛市场总监 秦克旋
                       ——2005呼叫中心产业发展前瞻 
                                    　　
                                    <p>休息
                                  <p>13:30–
                                   15:30
                                  <p>专家论坛

                            <h>2005年04月14日15日

   <p>美国呼叫中心管理学院总裁 布赖隆た死锔＠甲ㄌ饨沧 (在线报名)

   <h>题目：高效呼叫中心管理的基本知识与技能

                                  <p>课程内容

   <p>展览大厅开放时间：2005年4月13日1:30pm —— 4月15日 03:30pm
   <p>注：以上日程内容，将根据参展厂商提交资料的进展情况随时更新，敬请留意。
