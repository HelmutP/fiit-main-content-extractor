

 "

 


Hartbeespoort Dam News








 
	 
		
			

			

		
		
			
			Hartbeespoort's First Online Newspaper
		
		
			
			
			
			Home
			
			
			
			
			This Weeks News
			
			
			
			
			Editorial
			
			
			
			
			
			
			
			
			
			
			
			
			
			Letters
			
			
			
			Services
			
			
			
			Contacts
		
	
 
 
	
		
			
			 
			 
			 After almost nine months families in 
			Hartbeespoort are still unable to get prepaid electricity meters 
			from the Local Municipality of Madibeng.  

			
			 
			Still no prepaid meters available 
			 
			After almost nine months residents of 
			Hartbeespoort are still not able to access prepaid electricity for 
			their homes. Earlier this year Kormorant reported on the nine 
			families who had tried since the beginning of the year to buy these 
			meters from the Local Municipality of Madibeng after they decided to 
			switch their electricity provision over to this system.  
			The nine families had, at that stage, spoken to the municipality on 
			numerous occasions and even offered to pay in advance should meters 
			become available to ensure that they get one, but to no avail. The 
			municipality indicated that there were no meters available and that 
			they cannot accept the families? money in advance as they could not 
			say when meters will be available again.  
			At that stage, a representative for the families, Debbie van 
			Jaarsveld, told Kormorant that the different members of the families 
			had visited both the Hartbeespoort and Brits municipal offices on a 
			regular basis and soon discovered that all of the families had the 
			same problem. They decided to work together in the hope that as a 
			group they would be able to resolve the problem. 
			The group contacted neighbouring municipalities that had meters 
			available but was told that they could buy meters from them but that 
			the meters would then not be on Madibeng?s system and they would not 
			be able to buy electricity units locally.  
			Their next step was to contact the company that supplied the meters 
			to the municipality with the same result.  
			The families then wanted to know if they could not buy the meters 
			and then take it to the municipality to be put on the local system. 
			The municipality?s response at that stage was that they were aware 
			of the problem and hoped that it would be speedily resolved.  
			This past week it came to light that after another several months 
			have passed there are still no meters available at the municipality. 
			With the same result to enquiries - that the municipality cannot say 
			when meters would be available again.  
			According to information the reason for the unavailability of 
			prepaid electricity meters is that the municipality has reportedly 
			not paid the supplier and that the supplier insists that the amount 
			of two million rand be paid before new meters will be supplied. 
			Money previously paid for meters by consumers was allegedly used for 
			purposes other than paying the supplier.  
			Mr. Tumelo Tshabalala, communication officer for the Local 
			Municipality of Madibeng, said in response to renewed enquiries that 
			the supplier that residents have been speaking of is not the 
			supplier of meters to Madibeng. He said that the demand for 
			electricity meters in Madibeng is very high and once the stock 
			arrives it is sold out immediately. 
			?One other contributing factor is that the manufacturing process is 
			not as fast as one would expect, it takes long so most of the time 
			we have to wait for the shipment. We are expecting another shipment 
			of 500 meters soon,? Tshabalala said. 
			 
			Back to Top                                                              
			Back to Home 

			
			 16 October 2008 

		
		
			
			

		
	
 





