

n
""Leading the Blind,"" his study of nineteenth century European
guidebook travel, the writer Alan
Sillitoe traces the emergence of
guidebooks like Baedeker's and Murray's to the increasing popularity of
travel among those outside of the
strictly leisured classes.
 
 

 
 

 

 

Rebecca Cooney





Overview
  � Taking the Old Baedeker Into the Next Century
 
 
This Week's Articles
   � Sidewalk
 
  � New York Today
 
  � Digital City
 
  � Citysearch.com
 
  � Center for Land Use Interpretation
 
 
 


 ""Tourists,"" as these newly itinerant classes would be derisively
called, were the first hardy souls to
use the guidebooks, which Sillitoe called ""educated dogs which led
the blind."" They were essential to understanding the character and mores of foreign places for those travelers unprepared by their upbringing.
 
  The hazards were many. In
France, Murray's guide warned, the
civilized traveler ought to eschew
dining at the common table at inns,
for the company was less than desirable: ""Without denying that there
are exceptions among these gentry,
it is impossible to have sojourned in
France for any time without the conviction that a more selfish, depraved,
and vulgar, if not brutal, set does not
exist, and gentlemen will take good
care not to encourage their approaches, and to keep a distance
from them.""
 
  In Switzerland, one guide counseled, buying a memento is a challenge as imposing as the Matterhorn: ""There is hardly a country in
Europe which has so complicated a
currency as Switzerland; almost every canton has a coinage of its own,
and those coins that are current in
one canton will not pass in the next.""
In Italy, train travel made for a less
arduous journey, but one still had to
be wary of brigands and malaria.
 
  Compared with this litany of risk,
the concerns of today's guidebooks
seem petty.
 
 Tourism is now the
world's No. 1 industry, and much of
travel seems like an assembly-line
process: automated car rental, last-minute E-fares, global A.T.M. card
access.
 
 Instead of brigands, the most
intimidating obstacle can be something like getting frequent flier credit for that last leg of the trip to Caracas on Avianca.
 
  Still, the increased frequency of
travel and the increased travel population make information all the more
important.
 
 Travelers on the Grand
Tour of the Continent might have had
time to take in events as they discovered them, but today's weekend continent-hoppers need up-to-the-minute
information.
 
 With new restaurants
opening (and closing weekly), what
hope does even an annually updated
guidebook have of charting New
York?
 
  It is not surprising then that city
guides emerged as an early, much-touted presence on the Internet. With
a seemingly infinite amount of space
for listings, an ability to perform
searches under any number of criteria, and, increasingly, the option of
purchasing tickets for events, the Internet city guides seem to offer a
guidebook, the Yellow Pages, the daily newspaper, and even the rancor of
public discourse all rolled into one,
a tribune for our time.
 
  There are limitations, however.
 
  For one, keeping track of all that information still requires people to enter it, and many online city guides
are less current than they suggest.
Another limitation is the guides'
scope: not every guide covers every
city. Citysearch, for example, has a
Danish version and is planning a
Phoenix edition, but lacks Chicago in
between. (The Internet, however, has
spawned myriad quirky, low-budget,
guides that are produced locally.)
 
  Yet whatever their weaknesses,
the sites have become a sort of daily
guidebook to an increasingly complicated life. To test them, I attempted
to navigate each site in the hope of
finding one of the 53 Alfred Hitchcock films being shown this month at
the Museum of Modern Art and, for
later in the day, a local video store
for something more current.
 
  In my estimation, the best way of
experiencing a city is merely wading
into the human flow and letting the
city's own rhythms and contours carry one along, with stimulation of the
senses the only destination. But for
those who favor the cultural equivalent of global positioning systems,
these latter-day guide dogs have
learned a few new tricks.
 
 

