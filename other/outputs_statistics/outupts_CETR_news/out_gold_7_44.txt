

 Dell Inc. on Friday said it was investigating why one of its notebooks exploded into flames at a conference in Japan this week.
 
 
Pictures of the flaming laptop were published this week on the tech site  The Inquirer. The computer was on a table and no one was hurt.
 
 
A spokesman for the Round Rock, Texas, computer maker confirmed that the computer was bought from Dell, but declined to give any further details.
 
 
""We're aware of it, and we're digging into the details,"" the spokesman said. ""There's an investigation going on right now. When something like this happens, we want to know why.""
 
 
The Inquirer, which is published by VNU Business Publications, quoted an eyewitness who said the computer produced ""several explosions for more than five minutes."" The fire was put out with fire extinguishers.
 
 
Defective notebook batteries are known to have the potential of overheating and even bursting into flames."
 

