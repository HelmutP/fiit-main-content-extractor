

 Most customers won't make the jump to Oracle Corp.'s Fusion applications for years, but the software maker says it has begun working with companies to make the transition. 
 
 
Oracle expects to complete the Fusion applications suite in 2008. Moving to the next-generation platform built on service-oriented architecture (SOA) means closely monitoring and mapping out business processes, and training and data-migration schedules, John Wookey, Oracle's senior vice president of applications development told TechWeb.
 
 
""We map the how-tos as part of the processes,"" Wookey said, explaining that Oracle has made available migration guides to customers. The guides provide details on mapping out the technology to ease the transition, ""so if you paid your vendors like this, here is how you do exactly the same thing,"" he said. 
 
 
Oracle also formed a Fusion steering committee, a subset of a CIO advisory board for JD Edwards, PeopleSoft, Oracle and Siebel customers. The group works together, building business cases to determine the best time to make the transition. 
 
 
Meanwhile, Oracle will release Oracle E-Business Suite 12.0, PeopleSoft Enterprise 9.0 and JD Edwards EnterpriseOne 8.12 this year, with ongoing improvements scheduled for the JD Edwards World product every 12 to 15 months.
 
 
Oracle's focus on industry-specific features, similar to SAP AG and Microsoft Business Solutions, has led to ""huge"" investments in a JD Edwards EnterpriseOne 8.12 tool called Plant Manager dashboard. Capabilities will include metrics on how to manage specific manufacturing operations. 
 
 
Wookey also said Oracle has made investments in business activity monitoring (BAM), which provides real-time analytics to build interactive dashboards and alerts for monitoring business services and processes.
 
 
Oracle plans to deliver enhanced BAM technology within call-center tools in the next PeopleSoft Enterprise 9.0 release. The application suite will have enhanced customer relationship management (CRM) features. Companies will have the option to manage data from call-center activity and service-level agreements as information changes.
 
 
The software giant also continues to eye open source technology following the purchase of Sleepycat on Feb. 14 for an undisclosed sum. It was Oracle's fourth acquisition this year. 
 
 
Wookey said healthcare is an industry that could benefit from open source technology. A perfect place for an open source initiative would solve the problem based on ""how messaging uses specific medical terminologies, which is a big problem just speaking the same language,"" he said, ""We've been talking with large vendors and standards bodies in the healthcare space about if Oracle were to contribute would other companies get on board."" 

