

     By Mark Potter  

    
     LONDON, Feb 9 (Reuters) - Europe's scepticism over attention deficit hyperactivity disorder (ADHD) is starting to lift, opening up a market that could grow to $1 billion-a-year by 2012-13, British drugmaker Shire Plc (SHP.L: Quote, Profile, Research) believes.  

    
     Shire, whose Adderall XR is the top-selling treatment for ADHD in the United States, has not even tried to get the drug approved for sale in Europe as psychiatrists have been divided on the existence of the condition, let alone how to treat it.   

    
     But Chief Executive Matthew Emmens told Reuters that  attitudes were changing and Shire planned to launch two of its next generation of ADHD treatments, skin patch Daytrana and Vyvanse, a successor to Adderall XR, in Europe in 2008 and 2009 respectively.  

    
     In particular, he was encouraged by two so-called ""opinion leader"" meetings of psychiatrists in Europe last year.  

    
     ""For the first time, we're seeing a willingness to not only accept it (ADHD) as a disorder, but to recommend treatment in Europe, in particular for adults,"" he said in an interview.  

    
     The U.S. market for treating ADHD, which is characterised by impulsive behaviour and short attention spans, is worth about $3.5 billion a year and growing. In Europe, the market is virtually non-existent.   "
 

