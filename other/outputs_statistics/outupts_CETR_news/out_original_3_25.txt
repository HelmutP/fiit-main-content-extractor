

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
FROM THE FOXHOLE TO THE FIRE | By V.A. MUSETTO | New Movies | Upcoming Movies | Ratings


























 
     
    
       
       
          
             	
                            
             
               NYC Weather 79 ° CLEAR              
          
          
             
          
          
             
               Sunday, August 26, 2007 
               
                   Last Update: 
		02:25 AM EDT                   
               
             
             
             
             
                     
             


			Recent
			30 days+
			The Web
			

           
       

       
            
            
			 
				Story Index
				Summer of '77
			 
			
            
            
            
           
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Movies
            Make Us Rich
            Real Estate
            Travel
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
            Intern Stories
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
Pulse HomeMoviesMusicFoodFashionTheaterHealthTravelComics &amp; GamesHoroscopeDatingWeddings
 

	 
		Movies Home
		Oscars Home
	 
	

	 
		Fashion
		Fashion Week
		Fashion Week Photos
	 
	      
       
           
          
              
             
               
                               
                   
                      FROM THE FOXHOLE TO THE FIRE     
                      
                   
                   By V.A. MUSETTO              
				  
				  
				   

 
                  
				  
				   

                                    Rating:  March 21, 2007 -- NEARLY a year after being named best feature at Tribeca, Tristan Bauer's "Blessed by Fire" is making its theatrical debut. 
 
  The Argentinian export honors the unseen victims of the 1982 Falklands war, those who survived the battlefield only to die years later at their own hands. 
 
  It has been estimated that hundreds of veterans on both sides of the war - in which Argentina and Britain battled for 74 days over possession of the windy and barren Falkland Islands, located off the southeast coast of South America - committed suicide after going home. 
 
  "Blessed by Fire" opens two decades after the guns fell silent. Esteban (Gaston Pauls) receives a frantic call from the estranged wife of a foxhole buddy, Alberto (Pablo Ribba). 
 
  After years of post-war depression, Alberto has tried to kill himself with a "cocktail" of drugs and booze. 
 
  Seeing his comatose buddy in a hospital bed, suspended between life and death, Esteban finds his mind wandering back to wartime. 
 
  The battlefield sequences unfold with surreal horror, while the human bonding in the foxholes emerges tenderly. 
 
  On the downside, Bauer - who makes no pretense about where his heart lies - tacks on a melodramatic coda that lessens the momentum of an otherwise praiseworthy film. 
 
 BLESSED BY FIRE  In Spanish and English, with subtitles. Running time: 103 minutes. Not rated (war violence). At the Two Boots Pioneer, Avenue A and Third Street, East Village. 
 
                    
                  

             
             
                


 
 UNRELEASED MYSTERIES 
 FIGHTING MAD 
 PERSIAN SHRUG 
 HYDE AND SEEK  
 

 
 MARY FLOPPINS 
 GRETA TAKES A SHOWER 
 TANGLED UP IN BOB 
 MORE 
 
                 
 

 
                              			     
                    
				 
					






 
	 
	    
		 
		  			 DATING 
		  			 JOBS 
		  			 AUTOS 
		  			 REAL ESTATE 
		  			
		 
	 
	     
 				 
             
          
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
           
            sign in
            subscribeprivacy policyENTERTAINMENT HEADLINES FROM OUR PARTNERSrss 
          
       
    
     
    
       NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007 NYP Holdings, Inc. All rights reserved. 
  	   
 


