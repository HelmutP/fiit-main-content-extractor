


 NEW YORK (Reuters) - Fund manager Brandes Investment Partners LP said on Friday it intended to vote against billionaire investor Carl Icahn's proposed $2.31 billion buyout offer for Lear Corp. (LEA.N: Quote"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     Investing Home 
     News 
     Markets 
     Industries 
     Stocks 
     Funds 
     ETFs 
     Ideas &amp; Screening 
     
 
   1:25PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      Investing
      
      &gt;
      News
      
      &gt;
      Hot Stocks
      
      &gt;
      Article
 
 
 
       Brandes says to vote against Icahn Lear buyout 
       Fri Feb 9, 2007 6:34PM EST 
      
     

      
					 
						  Market View  
						 
							
								
									LEA.N
								
								
									Last:
									
								
								
									Change:
									
								
								
									Revenue (ttm):
									
								
								
									EPS:
									
								
								
									Market Cap:
									
								
								
									Time:
									
								
								
									
										
									
								
							
							   
							 Stock Details 
							 Company Profile 
							 Analyst Research 
						 
					 
				 
			
 
	

  
    
  

 


 
     
       Company News 
     
     
       
UPDATE 2-Lear nears completing interiors unit sales to Ross
 
       
US CREDIT-Lear debt halts rally, Icahn stokes leverage fear
 
       
UPDATE 5-Icahn makes $2.43 bln takeover bid for Lear Corp.
 
       
CHRONOLOGY-Lear Corp.'s ongoing financial struggles
 
       
Fitch may cut Lear Corp.'s ratings on buyout offer
 
       
 
 
			
      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     NEW YORK (Reuters) - Fund manager Brandes Investment Partners LP said on Friday it intended to vote against billionaire investor Carl Icahn's proposed $2.31 billion buyout offer for Lear Corp. (LEA.N: Quote, Profile, Research), which the auto parts maker has accepted. 

    


 Brandes said it would vote against the deal on behalf of its clients who own 2.85 million shares in Lear, or about 3.9 percent of total shares outstanding, because it believes Icahn's offer is below Lear's long-term fair value. 

    



    

       
       
       
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


   
			 
		
				Next Article:

			Holiday toy sales lead Hasbro to strong 4th qtr
 
			   
		  

 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 
     
  Merger Talk  
 
  
 Backing Blackstone 
 Top U.S. office landlord Equity Office reaffirmed its support for a takeover offer from private equity firm Blackstone, despite a higher rival bid.  Full Article  
 
 Private equity team considers Sainsbury bid 
 Deals of the Day: Read the latest M&amp;A headlines 
 
 

 
   

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
			 
	 

	 
		 
                     
                    Industry News
                     
                     
				 
		
		 
				
						

				 
					Oil, Bernanke to sway stocks
12:11pm EST
				 
				 
		 
Stocks drop as more home loans go bad; Micron warns
 
		 
Brandes says to vote against Icahn Lear buyout
 
		 
				
					More Industry News...
			 
		 
		 

 

 

 
	 
    


 
 	 Top Company Searches on Reuters.com 
 

 
 
 1. AAPL
 
 2. SIRI
 
 3. GOOG
 
 4. GE
 
 5. MSFT
 
 
 

   6. CSCO
 
   7. INTC
 
   8. GS
 
   9. SI.N
 
 10. MOT
 
 
 

 

 
   



	 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















