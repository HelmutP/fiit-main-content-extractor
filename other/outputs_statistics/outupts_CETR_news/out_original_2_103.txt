

 "






//W3C//DTD XHTML 1.0 Transitional//EN""
	""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;


 
	Bloggers bucking the Brotherhood in Egypt - International Herald Tribune
	
		
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	

	
	
			
	
	
	
	
	
	
	
	
	
	
	
	
	

		
		
			
	
	
	
		
	
	
	
	
	
	
	
	



	



 
	
	
	 
 

 target=""_blank""&gt; width=""768"" height=""90"" border=""0"" alt="""" /&gt;
 
 
	
	

	
	 
		  
		  
		 Africa &amp; Middle East 
		 
			
			
			
		 
	 	
	
	
	
	
		

 
	  
	 
		 
			 iht.com 
 Business 
 Culture 
 Sports 
 Opinion 
		 
	 
	 
		 
			 AMERICAS 
 EUROPE 
 ASIA/PACIFIC 
 AFRICA/MIDDLE EAST 
   
 TECH/MEDIA 
 STYLE 
 HEALTH 
		 
	 
	 
		 
			 TRAVEL 
 PROPERTIES 
 BLOGS 
 DISCUSSIONS 
 SPECIAL REPORTS 
 AUDIONEWS 
		 
	 
	  
	 Morning home delivery - save up to 72% 
	 
		 
		
			
			 
				 
				SEARCH
				 
			 	
			 
			Advanced Search
			 
		
	 
 

	
	

	
	 
		
		 
		
			

			
			
			
			
			
			

			
			
			
			
			
			
				
			
				
				 
					
				            
				            
					
					
						 LETTER FROM EGYPT 
					
					 

					
					Bloggers bucking the Brotherhood in Egypt 
					
					
											
			            
						
			            
						
			            
						 
							  
							 
By Daniel Williams Bloomberg News 
							 Published: October 28, 2008 
							  
						 
			            
						
										
											
						
			                

 
	  
	 
		
		   E-Mail Article 
		  
		
		
		 
			 
				
   Listen to Article

			 
			  
		 
		
		
		   Printer-Friendly 
		  
		
		
		   3-Column Format 
		  
		
		
		   Translate 
		  
		
		
		   Share Article 
		  
		
		
		 
			 
  
 
			 
  Text Size 
		 

		  
		 
			

			
		 
	 
	  
 



							
							
			                 CAIRO: Abdel Moneim Mahmoud once organized student elections, collected donations and educated chicken breeders about the dangers of bird flu as an operative for the Muslim Brotherhood. That all ended after he criticized Egypt's controversial Islamic political group on his blog, Ana-Ikhwan (I Am Brotherhood).  
 Mahmoud, 28, condemned its opposition to women and Christians holding high office in Egypt, including the presidency. He also questioned its slogan, ""Islam is the answer"" - a rallying cry of associated groups and imitators across the Islamic world - for implying that religious scripture should be the primary criterion for political action.  
 Brotherhood officials told Mahmoud to stop blogging or drop out of the organization. While he suspended active participation, he still considers himself a member. He is also unrepentant.  
 ""The youth of the Muslim Brotherhood used to listen and obey,"" he says. ""Some leaders don't like it, but we don't keep quiet.""  
 Mahmoud is part of a new generation of Islamic-oriented bloggers in the Middle East whose willingness to air internal matters online has created as much of a stir as their opinions, says Diaa Rashwan, an analyst here at Al-Ahram Center for Political and Strategic Studies.  



 
	
 
     Today in Africa &amp; Middle East 
      
     
	 Aid disrupted in eastern Congo as rebels advance and populace flees 
 
  
 
	 Damascus closes U.S. school and culture center after border raid 
 
  
 
	 Iraqi court convicts U.S. soldiers' killer 
 
  

 

	
	
	 
		

	 
	
 


 ""What's new is this launching pad used by young people: the blog,"" he says. ""They are loyal to the Brotherhood, but believe in open debate.""  
 Until recently, political blogging in Egypt was largely the domain of secular democracy activists who reported on strikes and torture and promoted protests against the 26-year rule of President Hosni Mubarak. Brotherhood bloggers took their cue from those campaigners, says Mahmoud, a reporter for the independent, non-Islamic newspaper Al Dustour.  
 Two years ago, some Brothers, mainly in their 20s, began detailing the arrests and torture by the police of their own members. Some also turned to criticizing the Brotherhood itself. They released details of a draft political platform now being discussed internally that includes the ban on women and Christians leading a Muslim-majority country.  
 ""We don't think these activities are harmful,"" says Magdi Saad, 30, marketing manager for a real-estate company who runs the blog Yalla Mesh Mohem (It Doesn't Matter). ""We think we put a human face on the Brotherhood. The leaders were shocked.""  
 Discipline has long been the watchword for the 80-year-old group. Public airing of internal debates was considered off limits, and membership lists and training information are veiled from public view, partly because Brothers have been perpetually subject to imprisonment.  
 During part of its history, members preached violent struggle against the government. In 1974, under the influence of what was then its younger generation, it disowned bloodshed, except in the case of armed action against the U.S.-led occupation in Iraq and by Palestinian groups including Hamas against Israel.  
 The Brotherhood is a model for Hamas and other Islamic political organizations such as the Islamic Action Front in Jordan. Unlike Hamas, it is not on the list of terrorist organizations compiled by the U.S. State Department.  
 Although Egypt's government legally bars the Brotherhood from politics, it is the country's largest opposition force. The group, which estimates its membership at more than one million, won 88 of 454 parliamentary seats in Egypt's 2005 elections by running candidates as independents.  
 Mahmoud and Saad have been jailed on occasion for their involvement in the Brotherhood, but they seem unafraid that their public exposure on the Web puts them in further danger. During interviews in public places, they spoke without looking over their shoulders. Such conversations were considered risky, even in private, a few years ago.  
 ""The government knows who we are anyway,"" Mahmoud says.  
 Mustafa al-Naggar, 29, a dentist, says: ""The government is happy to characterize us as a secretive organization; we don't want to play that game."" In his Waves in a Sea of Change blog, he wrote, ""It is not shameful to revise our ideas or change our positions."" He also has suggested that members as young as 30 be considered in selecting Brotherhood leaders, instead of 40, which is now the case.  
 The Brotherhood acknowledges that blogging has created a division of opinion in the organization. Abdel Moneim Aly el-Barbary, a physician and high official, monitors the bloggers and estimates their number at about 150. He says he belongs to the faction that supports them; still, he wants them to avoid attacking personalities, be polite and keep their critiques positive.  
 It is better for members to air disagreements than let them fester in private, says Barbary, 55, who adds that restrictions should apply only to sensitive organizational issues such as finances. He also says blogging permits Brotherhood officials to see what the rank-and-file is thinking, since the leaders are frequently jailed and meetings of more than five people generally require permission under Egyptian law. ""We have to adapt to modern times,"" he says.  
  1  |  2  Next Page
 

							
							
			            	
							
								
								
								
								
						
							
						 
						
										
					
						 
							 
								
								 
							 
						 	
					
					
				    
				      
				    											
					
					 
						  
						 Back to top 
						 
Home  &gt;  Africa &amp; Middle East
 
					 
			

			
			 
			
			
			
				 
		 IHT.com Home » 
		 Latest News 
		  
		 
			 
				  
				 Regis Duvignau/Reuters 
			 
			 
				 France weighs big changes in drinking laws 
				 Winemakers complain that the government, egged on by killjoys, wants to make it harder to sell and drink wine. 
			 
			 
In Opinion: David Brooks: The behavioral revolution
 
		 
		 
			  
			 
				More Headlines 
				 
					 Both candidates court Pennsylvania 
					 Aid disrupted in eastern Congo as rebels advance and populace flees 
					 Damascus closes U.S. school and culture center after border raid 
					 Experts in economic malfunction 
				 
			 
			  
		 
	 
						
			
			
		
		 
		
	    
		
		 
			
						
			
			 
				  
				 
					 Video 
					 See all videos » 
				 
				 
					  
					  
					
					 
					
						 	
						
						 
							  
							 
								 Pirates hold ship 
								 Somali pirates continued their seizure of a Ukrainian cargo ship as American warships cornered them.  
							 
						 
						

						
						 
							  
							 
								 New commander in Iraq 
								 The U.S. military command in Iraq changes hands from General David Petraeus to General Ray Odierno.  
							 
						 
						

						
						 
							  
							 
								 The risks of desert farming 
								 Israel is at the forefront of desert farming, but even the world's most high-tech farms can't control the weat... 
							 
						 
						

						
						 
							  
							 
								 Gorillas in the Congo Republic 
								 A survey by the Wildlife Conservation Society in the Congo Republic has discovered a large population of Weste... 
							 
						 
						

						
						 
							  
							 
								 The Week Ahead - Aug. 4, 2008 
								 The IHT's managing editor, Alison Smale, discusses the week in world news. 
							 
						 
						

						
						 
							  
							 
								 Strong in Sudan 
								 President Bashir of Sudan, accused of crimes in Darfur, is finding support among former enemies. 
							 
						 
						

						
						 
							  
							 
								 Dancing in Darfur 
								 Facing charges of genocide, President Omar Hassan al-Bashir of Sudan staged a public relations campaign.  
							 
						 
						

						
						 
							  
							 
								 Farming the Sahara Desert 
								 Egypt is relying on an ambitious agricultural project to reinvigorate national food production. 
							 
						 
						

						
						 
							  
							 
								 Hezbollah celebrates release 
								 The militant group held a huge celebration after the release of five prisoners from Israel.  
							 
						 
						

						
						 
							  
							 
								 Mourning on 'Day of the Iraqi Child' 
								 While security is improving in Iraq many families are still grieving their losses. 
							 
						 
						
						 							
					 					
				 
				  
			 
			
	        
	        
				
									 
						
						 



 
						
					 
									
				
			
				
	       
			
			 
	 
  
  
 
	
	 
		 Most E-Mailed 
		 

		 
			  
			 24 Hours 
			 | 
			 7 Days 
			 | 
			 30 Days 
		 

 
	
 
	 1. 
	 The mysterious cough, caught on film 
 
 


 
	 2. 
	 Rising yen worries Japan 
 
 


 
	 3. 
	 DA: criminal charges possible in boy's Uzi death 
 
 


 
	 4. 
	 Court finds Niger guilty of allowing girl's slavery 
 
 


 
	 5. 
	 David Brooks: The behavioral revolution 
 
 


 
	 6. 
	 Volkswagen shares jump and short-sellers pounce 
 
 


 
	 7. 
	 Alaskans turning against Stevens after verdict 
 
 


 
	 8. 
	 Bet on a rebound and another fall 
 
 


 
	 9. 
	 Oil's stunning retreat: How long can it last? 
 
 


 
	 10. 
	 White House explores aid for merger between GM and Chrysler 
 
 


 

 
	
 
	 1. 
	 Endorsement: Barack Obama for U.S. president 
 
 


 
	 2. 
	 Partying helps power a Dutch nightclub 
 
 


 
	 3. 
	 Financial crisis has one beneficiary: The dollar 
 
 


 
	 4. 
	 Greenspan 'shocked' that free markets are flawed 
 
 


 
	 5. 
	 Woman arrested for 'killing' her virtual husband 
 
 


 
	 6. 
	 Want to be heard in India? You'd better form a militia 
 
 


 
	 7. 
	 Multitasking can make you lose ... um ... focus 
 
 


 
	 8. 
	 South Korea pushes to dissolve 'the old way' of business culture 
 
 


 
	 9. 
	 In Europe, crisis revives old memories 
 
 


 
	 10. 
	 A manga adventure through the world of wine 
 
 


 

 
	
 
	 1. 
	 Equestrian stripped of Beijing result for doping 
 
 


 
	 2. 
	 Indians complete trade with Brewers 
 
 


 
	 3. 
	 Moggi among 25 sent to trial for match-fixing 
 
 


 
	 4. 
	 A champion not honored in her own land 
 
 


 
	 5. 
	 Wells Fargo, not Citi, to buy Wachovia 
 
 


 
	 6. 
	 Fortis' Dutch operations nationalized 
 
 


 
	 7. 
	 6 seek to unseat scandal-plagued La. congressman 
 
 


 
	 8. 
	 Bulgarian corruption troubling the European Union 
 
 


 
	 9. 
	 Iceland is all but officially bankrupt 
 
 


 
	 10. 
	 Taking a hard look at a Greenspan legacy 
 
 


 

	 
	 
  
  
 
 
			
			
			
			

					
	 				    
		 
			  
			 
			
				iht.com/energy
							
		  	 
		   						
		 
			
			 
			
	   						
	 
					
		 Oil's stunning retreat: How long can it last? 
	
		
		 More from Energy: 
	
			
			 
				
					 Reducing carbon emissions no easy task for Europe 
				
				
				
					 How U.S. candidates differ on energy policy 
				
				
				
					 Russia's oil boom: Miracle or mirage? 
				
				
																

	     
		
		     
	 				
		

			
			
			
						
			
			
				
				
				
				
				
	        
		 
		
	 
	

	
	
		 
	
	 
		 
			
			 Contact iht.com 
			  
			
				  	
				 
Close this window or Send us another message
 			
			
		 
	 	
	
	 
		 
			  
			 
				
					
					 Search 
									
			 
		 
		
		 
			 
				 News: 
				 
					 
						 Americas 
						 | 
						 Europe 
						 | 
						 Asia - Pacific 
						 | 
						 Africa &amp; Middle East 
						 | 
						 Technology &amp; Media 
						 | 
						 Health &amp; Science 
						 | 
						 Sports 
					 
				 
			 
			
			 
				 Features: 
				 
					 
						 Culture 
						 | 
						 Fashion &amp; Style 
						 | 
						 Travel 
						 | 
						 At Home Abroad 
						
						 | 
						 Blogs 
                                                 | 
                                                 Reader Discussions 
						 | 
						 Weather 
					 
				 
			 
			
			 
				 Business: 
				 
					 
						 Business with Reuters 
						 | 
						 World Markets 
						 | 
						 Currencies 
						 | 
						 Commodities 
						 | 
						 Portfolios 
						 | 
						 Your Money 
						 | 
						 Funds Insite 
					 
				 
			 
			
			 
				 Opinion: 
				 
					 
						 Opinion Home 
						 | 
						 Send a letter to the editor 
						 | 
						 Newspaper Masthead 
					 
				 
			 
			
			 
				 Classifieds: 
				 
					 
						 Classifieds Home 
						 | 
						 Properties 
						 | 
						 Education Center 
					 
				 
			 
			
			 
				 Company Info: 
				 
					 
						 About the IHT 
						 | 
						 Advertise in the IHT 
						 | 
						 IHT Events 
						 | 
						 Press Office 
					 
				 
			 
			
			 
				 Newspaper: 
				 
					 
						 Today's Page One in Europe 
						 | 
						 Today's Page One in Asia 
						 | 
						 Publishing Partnerships 
					 
				 
			 
			
			 
				 Other Formats: 
				 
					 
						 iPhone 
						 | 
						 IHT Mobile 
						 | 
						 RSS 
						 | 
						 AudioNews 
						 | 
						 PDA &amp; Smartphones 
						 | 
						 Netvibes 
						 | 
						 IHT Electronic Edition 
						 | 
						 E-Mail Alerts 
						 | 
						 Twitter 
					 
				 
			 
			
			 
				 More: 
				 
					 
						 Daily Article Index 
						 | 
						 Hyper Sudoku 
						 | 
						 IHT Developer Blog 
						 | 
						 In Our Pages 
					 
				 
			 
			
		 
	 
	
	 
		 
			
			 
				Subscriptions 
				Sign Up  |  Manage
			 
		 

		 
			 
				 Contact Us 
				 | 
				 Site Index 
				 | 
				 Archives 
				 | 
				 Terms of Use 
				 | 
				 Contributor Policy 
				 | 
				 Privacy &amp; Cookies 
			 
		 
		 Copyright © 2008 the International Herald Tribune All rights reserved 
	 		
 
	
	








        

























