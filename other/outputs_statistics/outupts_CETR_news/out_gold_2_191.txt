

 "Arizona: escaping the US elections
 
 In an isolated corner of Arizona, you can find peace, ranchers and absolutely no TV, says Stanley Stewart 
	

 










	
  




 

 
 
 

 
 
	

Stanley Stewart 

 
 
  






	
 


 
I had only been in Tombstone half an hour when the shooting started. 
Apparently, it was the Republicans and the Democrats at it again. 
 
 
This was just the kind of thing I had come to Arizona to escape. 
 
 
Like most people in this year of interminable electioneering, I had had enough 
of politicking, of spin and soundbites, of poll figures and attack ads. 
 
 
I was heading deep into the southeastern corner of the state in the hope of 
escape. I was looking for a quieter America, for a ranch somewhere with 
wide, empty landscapes, and cowboys with few opinions beyond yup and nope. 
When I stopped for lunch in Tombstone, I hadn’t expected to be detained by a 
gunfight on Main Street. 
 




 
	

	
 
 
 

  Going native in the USA
 

 Oregon’s annual Nez Perce Indian gathering is a Native American ritual, a pilgrimage and a knees-up 
 
 
	
	
 
 

  Obama v McCain fever in Washington
 

 It’s an election battle with global implications, and the front line is Washington, DC. Rob Ryan is in the thick of it 
 
 
	
	
 
 

  Six of the best US holidays
 

 If planning the trip of a lifetime Stateside, where would six Times writers and US specialists go? 
 
 
	

 
 
 Related Internet Links 
 
 Read more than 500 USA travel articles 	
 
 
 


 
 
 Related Links 










 
 
Obama v McCain fever in Washington
 
 


 
 

	



 
 
 


	
 
Trouble had been brewing all morning. There had been heated words outside the 
Bird Cage Theatre. Threats had been exchanged at Big Nose Kate’s. Now, Wyatt 
Earp and his brothers, Virgil and Morgan, with old friend Doc Holliday, were 
advancing down Main Street in spangly waistcoats. The Clan-ton gang had been 
spotted at the OK Corral. A crowd drifted after them. “There’s going to be 
shooting,” a fat man from Ohio helpfully informed his fat wife. 
 
 
To some people, this was the Gunfight at the OK Corral, in which the Clantons 
are gunned down in a hail of blanks every day at 2pm. To others, it is part 
of America’s political feud. The Earp brothers were the hired guns of the 
Republican hierarchy, the owners of the big mining and cattle operations who 
wanted to make Tombstone “safe for investment”. The Clantons, humble cowboys 
who wanted to preserve a bit of economic space for freelance prospectors and 
small independent ranchers, were aligned with the Democrats. You can see how 
much has changed in 127 years. 
 
 
I didn’t wait for the final body count. I was a man on a mission. I climbed 
back into the hire car, a big beast appropriately named Bronco, and spun 
away down an empty highway. Telegraph poles flashed by. Yellow hills rolled 
into empty distance. On the radio, crackling with static, a man was singing, 
“All my exes live in Texas, and that’s why I hang my hat in Tennessee.”
 
 
In Douglas I stopped for a coffee at the Gadsen Hotel. It is the town’s only 
moment of glamour, built a century ago for cattle barons. In the lobby, a 
grand double staircase rises past a vast spread of Tiffany stained glass. 
One night, a drunken Pancho Villa is said to have ridden his horse up these 
stairs, firing into the ceiling as he went. You can still see the chipped 
marble on the seventh step. 
 
 
Pancho probably didn’t read English – which explains why he missed the sign on 
the door of the Saddle &amp; Spur lobby bar: “No firearms or weapons of any 
kind”. The bar was empty but for the barman, who had fallen asleep in front 
of a “wanted” poster. On the television in the corner, people were waving 
flags and banners while John McCain, the Arizona senator, was giving them 
the thumbs up. I turned the set off and stepped next door into the diner for 
“All the coffee you can drink – one dollar”. 
 
 
BACK IN the Bronco, I followed Highway 80. It ran like a drawn line through 
the empty grasslands of the San Bernardino Valley. In an hour’s driving I 
saw two other cars, neither of them sporting political bumper stickers. From 
time to time, distant homesteads appeared, set back a mile or so from the 
road, tucked into folds in the long, yellow hills. It was remote country. My 
mobile couldn’t get a signal. Hopefully, I was beyond the reach of Fox News 
as well. 
 
 
Price Canyon Ranch lies at the end of a long dirt road in the foothills of the 
Chiricahua Mountains. It was just the kind of place I was looking for. I 
wanted a real working ranch, not a dude ranch with an infinity pool and a 
spa and a yoga class. I wanted somewhere that felt like the West, somewhere 
comfortable but rustic, not a citified luxury resort where you expected to 
find the horses on the sun loungers, sipping martinis. 
 
 
Price Canyon has 10 guest rooms elegantly decorated in western style with 
hardwood floors and Navajo rugs. One of the old barns has been beautifully 
converted into a large central lounge with a stone fireplace, deep leather 
sofas, a library of western books and a dining area where meals are produced 
by the wonderful Fred Tullis, a painter-turned-chef. If food is the heart of 
a home, Fred and his generous country meals are the heart and soul of Price 
Canyon."
"8868","
Stanley Stewart 

 
 
 
  






	
 


 
I had only been in Tombstone half an hour when the shooting started. 
Apparently, it was the Republicans and the Democrats at it again. 
 
 
This was just the kind of thing I had come to Arizona to escape. 
 
 
Like most people in this year of interminable electioneering, I had had enough 
of politicking, of spin and soundbites, of poll figures and attack ads. 
 
 
I was heading deep into the southeastern corner of the state in the hope of 
escape. I was looking for a quieter America, for a ranch somewhere with 
wide, empty landscapes, and cowboys with few opinions beyond yup and nope. 
When I stopped for lunch in Tombstone, I hadn’t expected to be detained by a 
gunfight on Main Street. 
 




 
	

	
 
 
 

  Going native in the USA
 

 Oregon’s annual Nez Perce Indian gathering is a Native American ritual, a pilgrimage and a knees-up 
 
 
	
	
 
 

  Obama v McCain fever in Washington
 

 It’s an election battle with global implications, and the front line is Washington, DC. Rob Ryan is in the thick of it 
 
 
	
	
 
 

  Six of the best US holidays
 

 If planning the trip of a lifetime Stateside, where would six Times writers and US specialists go? 
 
 
	

 
 
 Related Internet Links 
 
 Read more than 500 USA travel articles 	
 
 
 


 
 
 Related Links 










 
 
Obama v McCain fever in Washington
 
 


 
 

	



 
 
 


	
 
Trouble had been brewing all morning. There had been heated words outside the 
Bird Cage Theatre. Threats had been exchanged at Big Nose Kate’s. Now, Wyatt 
Earp and his brothers, Virgil and Morgan, with old friend Doc Holliday, were 
advancing down Main Street in spangly waistcoats. The Clan-ton gang had been 
spotted at the OK Corral. A crowd drifted after them. “There’s going to be 
shooting,” a fat man from Ohio helpfully informed his fat wife. 
 
 
To some people, this was the Gunfight at the OK Corral, in which the Clantons 
are gunned down in a hail of blanks every day at 2pm. To others, it is part 
of America’s political feud. The Earp brothers were the hired guns of the 
Republican hierarchy, the owners of the big mining and cattle operations who 
wanted to make Tombstone “safe for investment”. The Clantons, humble cowboys 
who wanted to preserve a bit of economic space for freelance prospectors and 
small independent ranchers, were aligned with the Democrats. You can see how 
much has changed in 127 years. 
 
 
I didn’t wait for the final body count. I was a man on a mission. I climbed 
back into the hire car, a big beast appropriately named Bronco, and spun 
away down an empty highway. Telegraph poles flashed by. Yellow hills rolled 
into empty distance. On the radio, crackling with static, a man was singing, 
“All my exes live in Texas, and that’s why I hang my hat in Tennessee.”
 
 
In Douglas I stopped for a coffee at the Gadsen Hotel. It is the town’s only 
moment of glamour, built a century ago for cattle barons. In the lobby, a 
grand double staircase rises past a vast spread of Tiffany stained glass. 
One night, a drunken Pancho Villa is said to have ridden his horse up these 
stairs, firing into the ceiling as he went. You can still see the chipped 
marble on the seventh step. 
 
 
Pancho probably didn’t read English – which explains why he missed the sign on 
the door of the Saddle &amp; Spur lobby bar: “No firearms or weapons of any 
kind”. The bar was empty but for the barman, who had fallen asleep in front 
of a “wanted” poster. On the television in the corner, people were waving 
flags and banners while John McCain, the Arizona senator, was giving them 
the thumbs up. I turned the set off and stepped next door into the diner for 
“All the coffee you can drink – one dollar”. 
 
 
BACK IN the Bronco, I followed Highway 80. It ran like a drawn line through 
the empty grasslands of the San Bernardino Valley. In an hour’s driving I 
saw two other cars, neither of them sporting political bumper stickers. From 
time to time, distant homesteads appeared, set back a mile or so from the 
road, tucked into folds in the long, yellow hills. It was remote country. My 
mobile couldn’t get a signal. Hopefully, I was beyond the reach of Fox News 
as well. 
 
 
Price Canyon Ranch lies at the end of a long dirt road in the foothills of the 
Chiricahua Mountains. It was just the kind of place I was looking for. I 
wanted a real working ranch, not a dude ranch with an infinity pool and a 
spa and a yoga class. I wanted somewhere that felt like the West, somewhere 
comfortable but rustic, not a citified luxury resort where you expected to 
find the horses on the sun loungers, sipping martinis. 
 
 
Price Canyon has 10 guest rooms elegantly decorated in western style with 
hardwood floors and Navajo rugs. One of the old barns has been beautifully 
converted into a large central lounge with a stone fireplace, deep leather 
sofas, a library of western books and a dining area where meals are produced 
by the wonderful Fred Tullis, a painter-turned-chef. If food is the heart of 
a home, Fred and his generous country meals are the heart and soul of Price 
Canyon. 
 

