

 December 11, 2006 -- STEVE Solomon doesn't exactly break any new comedic ground in "My Mother's Italian, My Father's Jewish &amp; I'm in Therapy!" Dealing with such topics as his elderly, hard-of-hearing parents, his sexually unresponsive wife who "uses a smoke alarm as a meat timer" and such contemporary irritants as airport security and foreign-born cabbies, he's mining territory explored by every comic from here to the Catskills.  
  Fortunately, he overcomes the familiarity of his largely generic material with his unaffected delivery, perfect comic timing and ability to craft amusing observations about even the most mundane topics.  
  "Every Jewish holiday boils down to this: They tried to kill us, God saved us, let's eat" is a typical example of his humor, which more often than not recalls Alan King minus the anger. Indeed, the sweetness of Solomon's persona goes a long way toward overcoming the feeling you've heard every joke before. 

