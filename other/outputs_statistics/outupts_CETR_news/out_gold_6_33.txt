

 Eight days ago, it was just an idea in Phil Gant's head. On Sunday, ""This Is History"" was a full-blown 30-second Super Bowl commercial. We're talking about a commercial that actually ran during the Super Bowl, not the pregame. 
 
 
 
 
 
 
 
 Gant had been a creative director at Element 79 for all of a week when he went to chief creative officer Dennis Ryan's office Jan. 29 to tell him about a concept for a commercial celebrating a historic moment in Super Bowl annals: the first time African Americans would be head coaches of teams in the big game. 
 
 
Gant, who had been chief creative officer at BBDO/Chicago, wanted to do a commercial showing groups of people in different settings watching the game. As the commercial progressed, we would see that all the people were African Americans. A message would appear on screen asking ""Who's winning?,"" followed by the answer ""We all are,"" before a final reminder to ""Enjoy the game."" 
 
 Ryan was immediately taken with the concept. Within an hour, Ryan had sold the spot to Element 79 client Frito-Lay. That left Gant just days -- about four, give or take -- to get the spot made and approved by Frito-Lay, CBS and the NFL. 
 
 





Ryan tapped another Element 79 talent, creative director Scott Smith, to direct the commercial. On Jan. 30, Gant and Scott looked at 150 local actors and cast 40 for the commercial. On Wednesday they scouted and selected several sites on Chicago's South Side where they would film. On Thursday a film crew shot the commercial. 
 
 Footage was delivered to the Whitehouse edit facility as quickly as it was shot, while CBS Super Bowl commentators Jim Nantz and Phil Simms in Miami kindly agreed to record simulated football game voiceover copy that would seem to be emanating from television sets in the commercial, and Joel Corelitz at Underscore Music quickly whipped up an original piece of music. 
 
 By midnight Thursday, Gant and Smith had a rough cut of ""This Is History."" By midday Friday, several cuts later, Frito-Lay had signed off on the finished commercial. By Saturday morning, CBS and the NFL had given their approval as well. 
 
 All that remained was to decide where in the big game the spot would run. Smith first heard word it might air during the most prestigious first break after kickoff, but that wound up not being the case. ""This Is History"" in fact, aired near the end of the first half. 
 
 And the commercial turned out to be a welcome moment of quiet dignity in what was mostly a parade of spots going for laughs.  		
 





 
 
 Super Bowl of Advertising has swirled into mediocre territory   
	   			

		
 


















 
		
		
		



The 2007 Super Bowl of Advertising is over. And none too soon. 

By all accounts, including a number of regular Joe viewers we heard from who watched the advertising for the first time on Sunday, the work this year was a huge letdown. 
 

Viewers had every right to feel disappointed.  
 

After weeks of carefully manipulated pre-Super Bowl hype surrounding the advertising, what viewers witnessed Sunday night was, for the most part, a stunningly mediocre lineup of commercials that could, in truth, have been seen during prime time just about any night of the week. 
 

What the 2007 Super Bowl of Advertising would seem to suggest is that, slowly but surely, the entire event is being scaled back. 
 

The big ideas are now significantly less big, when they exist at all. And the production values less impressive. In fact, one Doritos commercial created by a consumer -- another depressing trend that made big headlines in the lead-up to the Super Bowl -- reportedly cost all of $12 to make. And it still got lots of publicity. We can just imagine how many advertisers are now asking themselves why they need to spend upward of $1 million to make a Super Bowl ad, if a few dollars will suffice. 
 

We hope all advertisers don't immediately succumb to the notion that less automatically means more. That wasn't what motivated the thinking behind what we now call the Super Bowl of Advertising. Think, instead, ""1984."" That commercial was an iconic moment in advertising, thanks to Apple, which put it on the air. And decades later, ""1984"" remains a benchmark spot against which all Super Bowl advertising can be judged. 
 

Sadly, very little television advertising Sunday night came even remotely close to matching the breakthrough splendor or heft of ""1984.""  Thankfully, Coca-Cola and its ad agency Wieden + Kennedy in Portland, Ore., showed that some who make advertising still believe in thinking big and executing on a grand scale. 
 

But we're not surprised there was precious little of that kind of advertising. Most advertisers, after all, no longer believe in the integrity of the television commercial. For them, it's all happening online, and that's where their attention is focused. If they must be on the Super Bowl, they'd much rather pull off a stunt that generates publicity and online traffic, rather than deliver a great commercial. 
 

So we leave behind this 2007 Super Bowl of Advertising. Will there be a renaissance in 2008? We wouldn't bet on it. We suspect we have seen the future in this 2007 lineup. It is a sadly diminished future, to be sure. 

