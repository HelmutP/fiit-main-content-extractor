














	
 



 
 
 The  Network 
 Tech News 
 Product Reviews 
 Business Intelligence 
 Security 
 Storage 
 VoIP 
 IT Management 
 Business 
 
 
 
 
IT News from 
 
 
Enterprise Product Reviews from 
 
 
Enterprise Software News from 
 
 
IT Security News from 
 
 
Enterprise Storage News From 
 
 
VoIP News from 
 
 
IT Management Insights from 
 
 
Business News from 
 
 	
 
	





 
 


 


 
 
 Register Now! 
 Benefits 
 Login 
 
 


 

 
 
 
  
 


SEARCH 


  
 
 
  
 

 


 
 
 
 Blogs 
 News 
 Mobile 
 Software 
 Security 
 E-Business &amp; Management 
 Networking 
 Hardware 
 Careers 
 White Papers 
 Newsletters 
 
 

 



 
 

October 26, 2005 (6:12 PM EDT)








 SMBs Worry About IP Telephony Security 



 
By
W. David Gardner
, TechWeb Technology News
 
 A survey of small and medium businesses has found that many are leery of the security of IP telephony systems and favor traditional telephony systems. 
 
 
Some 300 SMBs with 20-500 employees were interviewed and only 48 percent of them said they trust IP telephony security today while 76 percent said they trust the security of traditional service providers. The survey was conducted for the Computing Technology Industry Association (CompTIA). Results were released Wednesday.
 
 
Curiously, 31 percent of the SMBs polled said they expect to have greater confidence in IP telephony systems in the next 12 months.
 
 
In a prepared statement, John Venator, the trade association’s president and chief executive officer, noted that the survey indicates product vendors and solutions providers with security expertise can gain competitive advantage with SMB customers.
 







Bitty Browser (JavaScript required)



 
 
 
 Email This Story 
 Print This Story 
 Reprint This Story 
 Tag in Del.icio.us 
 
Digg This 
 Networking Technology News 
 More TechWeb News 
 Bookmark this site! 
 
 
 


 
 
 
 
   Try TechWeb's RSS Feed! 
(Note: The feed delivers stories from TechWeb.com only, not the entire TechWeb Network.)
 















NETWORKING WHITE PAPERS AND REPORTS


Secure Information Sharing for Layer 2 Networks
 
Recent improvements in both technology and hardware have made Layer 2 WAN services a viable, scalable and cost effective alternative to traditional WAN services. While the networking advantages of Layer 2 WAN services have been met with approval, there are some security deficiencies with Layer 2. These issues are discussed in this paper.
 
 
Complete Guide for Securing MPLS Networks
 
MPLS provides a virtual network to the enterprise over the service provider's network. The service provider's MPLS network is shared by multiple enterprise networks and/or used by the service provider as an MPLS and/or Internet backbone. But “virtual” does not imply “secure”, and it is not free from the risk of losing data.
 
 
Two-Factor Enterprise Authentication: An Overview
 
Cybercrime is a serious threat to networked assets, and the first step to deal with it is to ensure you know who is accessing your network. This paper deals with the issues surrounding network security, authentication and identity management, and the use of two factor authentication methods for securing access.
 
 
Two-Factor Enteprise Authentication for Mac OS X
 
With the proliferation of deployed workforces, users requiring network access from a variety of devices, and the sheer value and volume of online ‘assets’, the need for strong, reliable network protection has never been greater. This paper deals with the issues surrounding security, authentication and identity management in Mac OS X networks.
 
 



 
More like this &gt;&gt;
 

















 
 


 

BIGFIX Presents: Ray Hopewood, the Godfather of Enterprise Security
 
 

Minimize Threats with ProCurve ProActive Defense
 
 

ECM: The value of content standardization
 
 

Control costs and increase productivity with MFPs
 
 

How resilient is your organization? Learn how to assess your IT risk
 
 


  
  
 
 


 
 
 Related White Papers 
 
  
  
  
 

 	
 
CAREER CENTER
 
 
 
 Ready to take that job and shove it? 
 
Open | Close
 
 
 
 
 
 SEARCH 
 
 


 
Function:  

Information Technology
Engineering

 
 
 
Keyword(s): 

 
 
 
State:
 
 

 
 
 
Post Your Resume
 
 
Employers Area
 
 
News &amp; Features
 
 
Blogs &amp; Forums
 
 
Career Resources 
  
Browse By:  
State | City 
 
 
 

 SPONSOR 
 
 

 
 
 RECENT JOB POSTINGS 
 
 

 

Featured Jobs:
 

BreakthroughIT seeking Project Manager in Groton, CT
 
  

Monsanto seeing IT Transportation and Optimization Analyst in St. Louis, MO
 
  

Princeton Financial Systems seeking Business Analyst 4 in Princeton, NJ
 
  

CSAA seeking IT Analyst IV in Glendale, AZ 
 
  

DisplaySearch seeking IT Project Manager in Austin, TX
 
  
For more great jobs, career-related news, features and services, please visit our ""Career Center.
 
 

 
 
 CAREER NEWS 
 
 
Five Rules For Bringing Your Real-Life Business Into Second Life 
 If you're thinking about establishing yourself in Second Life -- or are wondering whether you should -- we've got five rules that will help your new venture be a success. 
 
 
 
Hiring Report: Northrop Grumman Hiring On Wide Range Of Software Engineers  
 The global defense and tech company is seeking tech professionals skilled in Web site development, general software development, database administration, digital manufacturing, SAP/ABAP, complex CAD/CAM and PLM activities. 
 
 
 
 
 More Articles From Our Career Center 
  
 
 

Advertisement









 


 
  
 


 

TechSearch for more articles on:
Computing Technology Industry Association
 CompTIA
 survey
 SMBs
 small and medium businesses
 IP telephony
 traditional telephony systems
 John Venator
 trade association

 
 
 

  
 
 
 Specialty Resources 

 
 

How organizations can protect their sensitive information
 
 

IT Service Continuity Process Roadmap
 
 

5 Ways to Tame Corporate Data
 
 

Predictive Analytics to the Rescue
 
 

Best practices for IP address management
 
  
 
 VIEW ALL BRIEFING CENTERS 
 
  
  
 
 
 
 Featured Microsite 
 

 
  
  

 
 
 Related Links 
  
 
FCC Vote On AT&amp;T-BellSouth Merger Stays On 'Hold'
 
 
Google's Holiday Wish May Come True: An Online Clone Of Microsoft Office
 
 
NYC Preps For Information Sharing
 
 
Skype's North American Calling Plan Is Now $30 A Year
 
 
FCC Continues Its Deliberations On The AT&amp;T-BellSouth Merger
 
 
IT Job Market Best In Five Years, Survey Finds
 
 
  
  
 
 
 

 Microsites 
 Featured Topic 






&lt;a href="" http: target="" _top&gt;&lt;img src="" http: width="125" height="125" border="0"&gt;&lt;/a&gt;





 Additional Topics 
 
 

Small Business Chucks the Paper - Learn More
 
 

Online Collaboration: Training, Networking, Innovating
 
 

  
 
 
 Crush The Competition 
 TechWeb's FREE e-mail newsletters deliver the news you need to come out on top. 




  
  
 
 
 Techencyclopedia 
 Get definitions for more than 20,000 IT terms. 




  
  

 
 
 Techwebcasts 
 Editorial and vendor perspectives 

Come up to speed on KVM-over-IP solutions: Download the free buyers guide
 

Come up to speed on KVM-over-IP solutions: Download the free buyers guide
 
 
  
  
 


 
 
 Vendor Resources 
 

Safari Online Books closes gap between innovation and publication.
 

Ease the SMB Storage Burden
 
 
  
  
 


 
 
 Focal Points 
 

SMBs: Taking backup and storage beyond tape. Download the free white paper.
 

Vista Security: Is it Worth It? View the Productcast Now
 
 
  
  
 



 
 
  
	
 

  
  
 
 

 
 

  


 
 


 
  
 
 

 Free Newsletters 
 TechEncyclopedia 
 TechCalendar 
 Opinion 
 Research 
 Careers &amp; Workplace 
 Webcasts 
 About Us 
 Contact Us 
 
 Site Map 
 Mobile Tech News 
 Software Tech News 
 Security Tech News 
 E-Business Tech News 
 Management Tech News 
 
 Networking Tech News 
 Hardware Tech News 
 
  

 
 
 InformationWeek 
 Network Computing 
 Financial Technology Network 
 Wall Street &amp; Technology 
 Bank Systems &amp; Technology 
 
 Insurance &amp; Technology 
 IT Pro Downloads 
 Intelligent Enterprise 
 Byte and Switch 
 Dark Reading 
 bMighty 
 Small Biz Resource 
 
 Byte.com 
 VoipLoop 
 CollaborationLoop 
 Blackhat 
 
Interop 
 
 
  

 
 


 
  


 
 
 Terms of Service 
 
Copyright ©2007 CMP Media LLC
 
 Privacy Statement 
 Your California Privacy Rights 
 Media Kit 
 Feedback 
 
  
 
 











