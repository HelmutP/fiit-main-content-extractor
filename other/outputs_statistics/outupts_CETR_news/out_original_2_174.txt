

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
	Bottom line boost with Tiger Woods on the tee | The Australian
	
	
	
		
	
			
		 
			
		 
			
		 
			
		 
			
		 
			
		 
			
		 
			
		 
	
		
			
		  
	
	
			
		

	 
		
		 
			 
				 Skip to: 
				 Content 
				 Site Navigation 
				 Footer 
				 Site Search 
				 Site Map 
				 News Network Navigation 
			 

		 

		
				 
					 

					 

					 

				 

			
	  
			
	 
		 
			 
NEWS.com.au |  
			 
Fox Sports |  	
			 
Newspapers |  			
    		 
CareerOne |  
             
carsguide |  
             
TrueLocal |  
             
Real Estate |  
             MySpace AU 
			
		 
				
		
				
	
			   
		
			
			
			
			
			Search for:
			
			
			
		
		
	 
			  			

	 

	
	
			 
		
		 
				
				
				

			Network Highlights:
 

		 
			 
		  
	 

	  

		
		 
			 
				 The Australian 
			 

			 
				 
				 
					 

					 

					 

				 

			 
			 

			
		 
		  October 29, 2008 02:15am AEDT
			| Make this site your homepage
		 			
		 


	
		 

	  
			
	
		 
			
		
					 THE AUSTRALIAN 
				
						 
							 
					
								 Breaking News 
							
								 The Nation 
							
								 The World 
							
								 Climate 
							
								 Features 
							
								 Opinion 
							
								 Blogs 
							
								 Sport 
							
								 
									 
							
										 AFL 
									
										 Rugby League 
									
										 Rugby Union 
									
										 Turf 
									
										 Cricket 
									
										 Soccer 
									
										 Golf 
									
										 Tennis 
									
										 Motorsport 
									
										 Opinion 
									
										 Swimming 
									
										 International 
									
										 Athletics 
									
									 
								 
							
								 Science &amp; Nature 
							
								 Lifestyle 
							
								 The Arts 
							
								 Travel 
							
								 Health 
							
								 Defence 
							
								 Weather 
							
							 
						 
					
					 Business 
				
					 Australian IT 
				
					 Higher Education 
				
					 Politics 
				
	
		 

	
	  

		
			
			 
				
				 Bottom line boost with Tiger Woods on the tee 
			
				
		    
		       
			     
			      
                 
               

            

		
			 
			
		  
			
			 
				
		
		 
			
			 
				 
					 
						Font Size:
						Decrease
						Increase
					 
					 
						Print Page:
						Print
					 
				 
			 
		
		 

		
	  
					
		  Brent Read
			| October 29, 2008
		   
	  
						
	 BARELY a summer goes by without Tiger Woods being ambitiously eyed off by those in charge of the domestic tour. Thus far, Woods' prohibitive appearance fee, rumoured to be anywhere from $4 million to $6m, has precluded any Australian tournament luring the world's best player. 
	
	 But hope springs eternal and yesterday, at the Australasian PGA's launch in Melbourne, two of this country's best players spoke effusively of the prospect of Woods returning to Australia once he recovers from the knee surgery which has sidelined him since his remarkable US Open win.  
 "Having him in a tournament and doing it the right way would have an incredible knock-on effect," former US Open champion Geoff Ogilvy said.  
 "Economists have done studies of the future economic impact when he actually plays a golf tournament and it's massive. It's really worth getting him here if we can and making it work.  
 "The Australian public need to see him playing because he's special."  
 While the chances of Woods returning to Australia remain remote, his influence is as strong as ever on the domestic scene and its players.  
 Stuart Appleby, a good friend of the 14-time major winner, has begun studying Woods in the hope it will improve his game. The Victorian is an eight-time winner on the US Tour but believes mimicking Woods can only be beneficial.  
 "I use Tiger as a great role model for the things he says, the way he acts and the way he plays the game," Appleby said.  
 "The more I study what he does, the more I learn about him."  
 Such as? "I asked him a question about his whole fist-pumping thing," he said.  
 "If I did that, adrenalin would be coursing through my veins and I would be so jacked up it wouldn't help my game.  
 "He said, 'it makes me concentrate more'."  
 As well as Appleby, Ogilvy believes he and his fellow professionals can glean plenty from the world No1, particularly when it comes to the mental side of the game.  
 "Physically he does everything pretty well," Ogilvy said.  
 "Adam (Scott) definitely drives the ball better than Tiger. His complete game ball-striking-wise is better than Tiger. But Tiger's short game is ridiculous. His belief is superb.  
 "Most pros can hole 20-footers, not many do it on the 72nd hole of the US Open on bad greens. He does."  
 American Paul Goydos, who once supplemented his earnings on the golf course by working as a substitute teacher, and South African Tim Clark lead the overseas contingent hoping to continue Ogilvy's barren run in Australia, where he has failed to win as a professional. 
	
  

				 

				  
					 
						 Story Tools 
					 

					 
						 
							
								 Share This Article 
								 Email To A Friend 
							  
						 
			 
				 Share This Article 
				 From here you can use the Social Web links to save Bottom line boost with Tiger Woods on the tee to a social bookmarking site. 
				 
					 del.icio.us 
					 Digg it 
					 Furl 
					 Netscape 
					 Yahoo MyWeb 
					 StumbleUpon 
					 Google 
					 Technorati  
					 BlinkList 
					 Newsvine 
					 ma.gnolia 
					 reddit 
					 Windows Live 
					 Tailrank 
					 Slashdot 
				 
			 

	 
	 
	    Email To A Friend 
	   
	       * Required fields 
	      
	       
	          
	            Recipient's Name:* Required
	            
	          
	          
	            Recipient's Email:* Required
	            
	          
	       
	       
	          
	            Your Name:* Required
	            
	          
	          
	            Your Email:* Required
	            
	          
	       
	       
	          
	            Email Format:*
	            
	               HTML
	               Text Only
	            
	           
	        
	        
	           
	             Your Comments:* Required
	             
	           
	        
	       
	           Information provided on this page will not be used for any other purpose than to notify the recipient of the article you have chosen. 
	          
	    
	  

	  

				 

				
	
	
	
  

			
			
			 
				 
		
		 
			 
				 Latest Sport 
			 

			 
				
		 
			
			 Indian hot air works for Ponting 
		
			 Hayes defends out-of-favour jockey 
		
			 Burgess  gets nod for tour  opener 
		
			 Gidley withdrawal leaves Roos exposed 
		
			 Passion is waning in the stands 
		
			 Bottom line boost with Tiger on tee 
		
		 
	
			 

		 

			
	 
	
					
					
	    
			 
				 
Video More Video
 
			 

			 
			   Inline video player has not loaded - you can still &lt;a href="" http:&gt;go directly to the video player&lt;/a&gt;
			 

	 

	
					
				
					
					 
						
				 
					 

					 

					 

				 

			
					 

					
				
					 
						 
				 
					 

					 

					 

				 

			
					 

				
					
					 
					   
				 
					 

					 

					 

				 

			
					 

					
				 
		 
			 
				 RSS Feeds 
			 

			 
				 
					 Opinion 
					 The Nation 
					 Business 
					 Sport 
					 Media 
					 Travel 
					 Higher Ed 
					 Defence 
					 The Arts 
					 Aviation 
					 Property 
					 Editor's Choice 
				 

			 

			 
				 More Feeds | What is this? 
			 

		 


  

			
			 
				 
	 
		 In The Australian Today 
	 
	
			 
				 
					 
					    Business 
					 
					
					
		 
			
			
			 Wall Street rallies nearly 900 points 
			 Rob Curran  US stocks roared higher, with the Dow index rising nearly 900 points in its second-biggest point gain in history. 
	     


	
					 
			        
		 
			
			 'Fear Gauge' tumbles on Wall St surge 
		
			 Volkswagen: world's biggest company 
		
		 
	
					 
				 

			 
		
	 
			 
				 
					 
					    Australian IT 
					 
					
					
		 
			
			
			 Maccas to provide 'clean' net service 
			 Karen Dearne  MCDONALD'S plans to launch ""family friendly"" WiFi services in store so its 1.45 million daily customers can surf the web for free. 
	     


	
					 
			        
		 
			
			 Sealed: WestNet snags Telstra ADSL2+ deal 
		
			 Internet: Google considers local centre 
		
		 
	
					 
				 

			 
		
	 
			 
				 
					 
					    Media 
					 
					
					
		 
			
			
			 Ten Unveils Sport Channel 
			 Jane Schulze  Channel Ten will launch a new digital 24 hour sports multi-channel early next year. 
	     


	
					 
			        
		 
			
			 CMH Upbeat on Pay-TV 
		
			 Austar heads for 20pc earnings growth 
		
		 
	
					 
				 

			 
		
	 
			 
				 
					 
					    Higher Education 
					 
					
					
		 
			
			
			 Chubb slams funding for duds 
			 Luke Slattery  ANU boss Ian Chubb will issue a warning that the nation risks a slide in global rankings if it fails to concentrate research funds. 
	     


	
					 
			        
		 
			
			 Waiting for Labor to deliver on VSU 
		
			 Upgrade sought in campus facilities 
		
		 
	
					 
				 

			 
		
	 

 
	 
		 Also in The Australian 
	 

	
			 
				 
					 
					    The World 
					 
					
					
		 
			
			
			 Kim ill but still on the ball: Aso 
			  NORTH Korean leader Kim Jong-il is probably in hospital but is still capable of making decisions, Japanese PM Taro Aso said. 
	     


	
					 
			        
		 
			
			 Bid to ward off Musharraf return 
		
			 Taiwan's diplomatic shift brings hope 
		
		 
	
					 
				 

			 
		
	 
			 
				 
					 
					    The Nation 
					 
					
					
		 
			
			
			 Maccas to provide 'clean' net service 
			 Karen Dearne  MCDONALD'S plans to launch ""family friendly"" WiFi services in store so its 1.45 million daily customers can surf the web for free. 
	     


	
					 
			        
		 
			
			 Aussies working longer hours than most 
		
			 Navy orders its sailors to plug leaks 
		
		 
	
					 
				 

			 
		
	 
			 
				 
					 
					    Opinion 
					 
					
					
		 
			
			
			 PM's rating will sustain him for now 
			 Paul Kelly, Editor-at-large  THOUGH Kevin Rudd's blanket bank guarantee is a blunder, its populist appeal should not be underestimated. 
	     


	
					 
			        
		 
			
			 Janet Albrechtsen: Europe worse off than the US 
		
			 Stephen Kirchner: Beware political saviours 
		
		 
	
					 
				 

			 
		
	 
			 
				 
					 
					    Lifestyle 
					 
					
					
		 
			
			
			 Marketing guru is impressed to dress 
			 Sharon Krum, New York  For Darryl Nipps, senior vice-president of a marketing company, volunteering at fashion week is a perfect fit. 
	     


	
					 
			        
		 
			
			 Multi-channelling risks 'media ghettos 
		
			 Fashion's changing faces 
		
		 
	
					 
				 

			 
		
	 
  

		 

	 

	
		 
			 
			 
Register | 
			 
Subscribe | 
			 
Online Archive | 
			 
Mobile/PDA | 
			 
Digital Edition | 
			 
Our Staff | 
			 
Contact Us | 
			 
Advertise With Us | 
			 
Sponsorship | 
			 
Terms &amp; Conditions | 
			 
Privacy Policy | 
			 
Accessibility | 
			 
Help | 
			 Sitemap 
			 
			 Copyright 2008 News Limited. All times AEDT (GMT + 11). 
		 

		
		
				 
					 

					 

					 

				 

			
				
					  
						
			
	  
		
		
	
	
	
	
  
	 




