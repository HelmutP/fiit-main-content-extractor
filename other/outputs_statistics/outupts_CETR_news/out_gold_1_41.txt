

 Do research before renting out house
				
				
				 
 August 20, 2007 
				
				

				 


If your house won't sell right now, are you going to rent it out? Before you become a landlord, research federal, state and local laws about tenant/landlord rights. Have an attorney prepare the lease agreement, and make sure that all tenants sign it. 
 


Ruth Spencer, Local 4 News 

