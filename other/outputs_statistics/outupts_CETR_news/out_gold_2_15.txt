

 "Retirement: Terry Brown, diocese of Malaita
		 
 
		 EPISCOPAL LIFE 
		 Oct  1, 2008 
					 Bishop Terry Brown of the Diocese of Malaita officially retired Aug.18 as spiritual leader of one of the largest dioceses in the Church of Melanesia in the Solomon Islands.   Born in the U.S., he moved to Canada and was later consecrated and installed as the fourth bishop of Malaita in 1996.  During his tenure as bishop ?much has been done and fulfilled in the areas of basic Christian education and strengthening the local church, ministry training, support for local church?s and diocesan programs and self support,? a diocesan news release said.  The first Canadian bishop to serve in Melanesia, Bishop Brown worked to improve ecumenical relations in Malaita province and has been involved in liturgical development and worship through the Liturgy and Worship Commission.   He first visited the Solomon Islands as a missionary and lecturer at Bishop Patteson Theological College in 1976. He lectured for six years before returning to Canada in 1981 to become a project desk officer for the Asia and Pacific Mission office based in Toronto."
"1664","EPISCOPAL LIFE
		 
 Oct  1, 2008 
					 Bishop Terry Brown of the Diocese of Malaita officially retired Aug.18 as spiritual leader of one of the largest dioceses in the Church of Melanesia in the Solomon Islands.   Born in the U.S., he moved to Canada and was later consecrated and installed as the fourth bishop of Malaita in 1996.  During his tenure as bishop ?much has been done and fulfilled in the areas of basic Christian education and strengthening the local church, ministry training, support for local church?s and diocesan programs and self support,? a diocesan news release said.  The first Canadian bishop to serve in Melanesia, Bishop Brown worked to improve ecumenical relations in Malaita province and has been involved in liturgical development and worship through the Liturgy and Worship Commission.   He first visited the Solomon Islands as a missionary and lecturer at Bishop Patteson Theological College in 1976. He lectured for six years before returning to Canada in 1981 to become a project desk officer for the Asia and Pacific Mission office based in Toronto. 

