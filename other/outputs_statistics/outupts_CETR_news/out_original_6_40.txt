

 //W3C//DTD HTML 4.01 Transitional//EN""&gt;

 
 Exploring the minds of millionaires  :: CHICAGO SUN-TIMES :: Terry Savage






	
	
	













    Back to regular view     Print this page
 





 



 








 




Your local news source ::       Select a community or newspaper »
 


	
        
		
	 	
		



Select a community 
------------------
Algonquin
Alsip
Antioch 
Arlington Heights
Aurora
Bannockburn
Barrington 
Barrington Hills
Batavia
Beecher
Bellwood
Berkeley
Blue Island
Bolingbrook
Broadview 
Buffalo Grove
Burr Ridge
Calumet City
Cary 
Chesteron, Ind.
Chicago
Chicago - Albany Park
Chicago - Avondale
Chicago - Belmont Cragin
Chicago -  Bucktown
Chicago - Dunning
Chicago - Edgebrook
Chicago - Edgewater
Chicago - Edison Park
Chicago - Jefferson Park
Chicago - Harlem-Irving
Chicago - Lakeview
Chicago -  Logan Square
Chicago - News Star
Chicago - North Center
Chicago - North Town
Chicago - Norwood Park
Chicago - Portage Park
Chicago - Ravenswood
Chicago - Rogers Park
Chicago - Roscoe Village
Chicago - Sauganash
Chicago - Skyline News
Chicago -  Ukranian Village
Chicago - Uptown
Chicago -  Wicker Park
Chicago Heights
Clarendon Hills
Country Club Hills
Crete
Crestwood
Crown Point, Ind.
Darien
Deerfield
Deer Park
Des Plaines
Dolton
Downers Grove
Elburn
Elgin
Elk Grove
Elmhurst 
Elmwood Park
Evanston 
Flossmoor
Forest Park
Fox River Grove
Fox Valley
Frankfort
Franklin Park 
Gages Lake
Gary, Ind.
Geneva
Glen Ellyn
Glencoe 
Glenview 
Glenwood
Golf 
Grayslake
Green Oaks
Gurnee
Harwood Heights
Harvey
Hawthorn Woods
Hazel Crest
Highland Park
Highwood
Hillside
Hinsdale 
Hobart, Ind.
Hoffman Estates
Homer Glen
Homer Twp.
Homewood
Indian Head Park
Inverness 
Island Lake
Joliet
Kenliworth
Kildeer
La Grange
LaGrange Highlands
LaGrange Park
Lake Barrington 
Lake Bluff 
Lake Forest 
Lake in the Hills 
Lake Villa 
Lake Zurich
Lansing
Lemont
Libertyville
Lincolnshire
Lincolnwood
Lindenhurst 
Lisle
Lockport
Long Grove
Lowell, Ind.
Manhattan
Markham
Matteson
Maywood 
Melrose Park 
Merrillville, Ind.
Midlothian
Mokena
 Montgomery
Morton Grove
Mount Prospect 
Mundelein 
Naperville
New Lenox
Niles
Norridge
North Barrington 
Northbrook 
Northfield
Northlake
Northwest Ind.
Oak Brook 
Oakbrook Terrace 
Oak Forest
Oak Lawn
Oak Park
Oakwood Hills
Olympia Fields
Orland Hills
Orland Park

 Oswego 
Palatine 
Palos Heights
Palos Hills
Palos Park
Palos Twp.
Park Forest
Park Ridge 
Plainfield

Portage, Ind.
Prospect Heights 
Richton Park
River Forest
River Grove
Riverwoods
Rolling Meadows 
Rosemont
Round Lake
Sauk Village
Schaumburg 
Schererville, Ind.
Schiller Park 
Silver Lake
Skokie 
South Barrington 
South Elgin
South Holland
St. Charles
Steger
Stone Park 
Thornton Twp.
Tinley Park/Southland
Tinley Park
Tower Lake
University Park
Valparaiso, Ind.
Venetian Village 
Vernon Hills 
Wadsworth
Wauconda 
Waukegan
West Lake Co., Ind.
West Proviso
Westchester 
Western Springs
Wheaton
Wheeling
Wildwood
Willowbrook 
Wilmette
Winnetka
Worth
Worth Twp.

 Yorkville 







Select a STNG publication or site
  
---------------------------------------------------
 DAILY PUBLICATIONS 
---------------------------------------------------
	Chicago Sun-Times	
	The Beacon News	
	The Courier News	
	The Daily Southtown	
	The Herald News	
	Lake County News-Sun	
	The Naperville Sun	
	Post-Tribune	


  
---------------------------------------------------
 SEARCH CHICAGO 
---------------------------------------------------
Autos
Homes
Jobs


  
---------------------------------------------------
 NEIGHBORHOOD CIRCLE 
---------------------------------------------------
 Montgomery
 Oswego 
 Yorkville 

  
---------------------------------------------------
 ENTERTAINMENT 
---------------------------------------------------
Centerstage
 Roger Ebert


  
---------------------------------------------------
 WEEKLY &amp; SEMIWEEKLY PUBLICATIONS 
---------------------------------------------------
	Algonquin Countryside	
	Antioch Review	
	Arlington Heights Post	
	Barrington Courier-Review	
	Booster: Lake View, North Center,
        Roscoe Village, Avondale Edition	
	Booster: Wicker Park, Bucktown,
	      Ukrainian Village Edition	
	Buffalo Grove Countryside	
	Cary-Grove Countryside	
	Deerfield Review	
	Des Plaines Times	
	Edgebrook-Sauganash Times-Review	
	Edison-Norwood Times-Review	
	Elk Grove Times	
	Elm Leaves	
	Evanston Review	
	Forest Leaves	
	Franklin Park Herald-Journal	
	Glencoe News	
	Glenview Announcements	
	Grayslake Review	
	Gurnee Review	
	Highland Park News	
	Hoffman Estates Review	
	Lake Forester	
	Lake Villa Review	
	Lake Zurich Courier	
	Libertyville Review	
	Lincolnshire Review	
	Lincolnwood Review	
	Morton Grove Champion	
	Mount Prospect Times	
	Mundelein Review	
	News-Star	
	Niles Herald-Spectator	
	Norridge-Harwood Heights News	
	Northbrook Star	
	Oak Leaves	
	Palatine Countryside	
	Park Ridge Herald-Advocate	
	Proviso Herald	
	River Grove Messenger	
	Rolling Meadows Review	
	Schaumburg Review	
	Skokie Review	
	Skyline	
	The Batavia Sun	
	The Bolingbrook Sun 	
	The Doings Clarendon Hills Edition	
	The Doings Elmhurst Edition	
	The Doings Hinsdale Editon	
	The Doings La Grange Edition	
	The Doings Oak Brook Edition	
	The Doings Weekly Edition	
	The Doings Western Springs Edition	
	The Downers Grove Sun	
	The Fox Valley Villages Sun	
	The Geneva Sun	
	The Glen Ellyn Sun	
	The Homer Sun	
	The Lincoln-Way Sun	
	The Lisle Sun	
	The Plainfield Sun	

	The St. Charles Sun	
	The Star: Chicago Heights Area	
	The Star: Country Club Hills, Hazel Crest
	The Star: Crete, University Park, Beecher	
	The Star: Frankfort, Mokena	
	The Star: Homer Glen, Lockport, Lemont	
	The Star: Homewood, Flossmore, Glenwood,
        Olympia Fields	
	The Star: New Lenox, Manhattan	
	The Star: Oak Forest, Crestwood, Midlothian	
	The Star: Oak Lawn, Palos &amp; Worth Townships	
	The Star: Orland Park, Orland Hills	
	The Star: Park Forest, Matteson, Richton Park	
	The Star: South Holland, Thorton Township	
	The Star: Tinley Park	
	The Wheaton Sun	
	Times Harlem-Irving	
	Times Jefferson Park, Portage Park,
       Belmont-Cragin Edition	
	Vernon Hills Review	
	Wauconda Courier	
	Wheeling Countryside	
	Wilmette Life	
	Winnetka Talk	





























 
 







 



 






 
 

 

    
 








 

 
 

 
 

 suntimes.com
 Member of the Sun-Times News Group 
 



 

 
 


Traffic  �  
Weather:  

""WHEW!""


		
 
 
 


Search » 

 

 
 Site
 STNG 




 

  
 

� Subscribe 
� Easy Pay 
� Reader Rewards 
� Customer Service 
� Email newsletters 


 

 

 

 

Home
 | 
News
 | 
Commentary
 | 
Sports
 |  
Business 
 | 
Entertainment
 | 
Classifieds
 | 
 Columnists
 | 
Lifestyles
 | 
Ebert
 | 
Search
  | 
Archives
 | 
Blogs
   | 
  RSS 
 
   




























 










  









 
 
  
  
  
  
    
 Terry Savage 
    
    
     Recent Columns 
  	  
     Savage Q &amp; A 
  	  
     Column Archive 
  	  
     E-mail Terry Savage 
  	  
     Biography 
  	   
  	
  
    Business 
    
    
     Archive 
  	  
     Appointments 
  	  
     Technology 
  	  
     Currency 
  	  
     Futures 
  	  
     Personal finance 
  	  
     Conrad Black on Trial 
  	  
     Portfolio 
  	  
     Real Estate 
  	  
     Stock market 
  	  
       
  	  
     Auto News 
  	  
     Sally Duros 
  	  
     Dan Jedlicka 
  	  
     Real Estate and Homelife 
  	  
     Chicago Innovation Awards 
  	  
     Made in Chicago 
  	  
     What's my line? 
  	   
  	
  	 
  
 	

  

 


    
   
   

    Columnists     
      	 



 Casual Friday 

  

 Robert Feder 

  

  

 Lewis Lazare 

 Ted Pincus 

 David Roeder 

 Terry Savage 

 Brad Spirrison 
 
	  
	 
  	   
	
	
	  
  
	  	
  


 

  

 

 


 





 


 








 






 






 
Terry Savage :: 



printer friendly »     
email article »  



 
 
 







 





 







  
 



 







 

  







 



 

 
VIDEO ::    MORE »
  
 
 
 

 
 
 

  
 
TOP STORIES ::
  
 
 NEWS 











Dry out, power up   


  
 BUSINESS 











W.W. Grainger adds new stores to area 


 
 SPORTS 











Bears are well grounded   


 
 ENTERTAINMENT 











Do celebs get off easy?   


 
 LIFESTYLES 











Glamorama: Coyote pretty  


 




 




 

  





 
 
 















	
	 
	
	
			  
				



 
		 
 Exploring the minds of millionaires 
		 
		
		
 
		
		

		 
 August 7, 2001 


		 BY TERRY SAVAGE SUN-TIMES COLUMNIST 
			
					
						
						





Do you really want to be a millionaire?  

    Silly question.  Of course, almost everyone would answer yes. But what would you, or should you, do to become one?  

    Should you quit your job and start day-trading technology stocks? Drop out of school to found an Internet company? Go on a game show or volunteer to marry someone you've never met? Those seem to be the most public routes to becoming an overnight millionaire.

    The Savage truth is that there are long odds against any of the above. In spite of--or perhaps because of--the media hype, these dreams of instant wealth remind me of inner city kids shooting hoops and dreaming of becoming an NBA All-Star. Sure, one or two or even 20 might achieve stardom, but we can all predict the wasted lives of the others who don't value an education and hard work. The same might be said of today's dot-com generation, hoping that venture capital and an IPO will be their path to mega-riches.

    How do millionaires really create wealth, and how do they live and think about and manage their wealth?  

    A new book, The Millionaire Mind, by Thomas Stanley, who wrote the best-selling The Millionaire Next Door, explores the lifestyles and attitudes of some of the estimated 3 million millionaires in America. What Stanley finds might surprise you.

    First, he sorted out those who were ""balance sheet"" millionaires, and those who simply lived an affluent lifestyle, while burdened with debt.  

    Balance sheet millionaires tended to own their homes without a mortgage, while those who merely live a wealthy lifestyle carry jumbo loans. Millionaires with assets of between $2 million and $5 million live, on average, in homes that are valued at $355,000--based on the IRS database figures, not exactly the mansion most expect. And the majority have not moved in the last 10 years.

    The millionaires in Stanley's survey tend to have started businesses, and built their wealth by finding a profitable niche. They say they love what they do and are motivated by building the business, not by building wealth. 

    They live comfortable lifestyles but are not wasteful. In a fascinating example, most of the millionaires in the survey report they buy expensive shoes, but almost all have them resoled. For the most part, they remain married to supportive and responsible spouses, who run economically productive households--from clipping coupons to buying household supplies in bulk.

    Bottom line: They spend less than they earn.

    When it comes to investments, these millionaires look to the stock market primarily as a place to grow capital, once their businesses have matured. They are investors, but not speculators in the markets. They rarely visit a casino, and almost never buy lottery tickets.  

    Of course, you might figure they don't need to speculate, since they're already wealthy. But perhaps these qualities are the reason they got wealthy in the first place.

    There's one other surprising component of this survey. Most of these millionaires were not at the top of their classes in school, nor did they score the highest on SATs.  

    To the contrary, many were only average students or had been told by their teachers that they'd never succeed. As a result, they developed a determination and resilience that helped them in business, and they learned to compensate for lack of academic skill by learning leadership through sports.  

    As you can tell by the sports reference--and the resoled shoes--the overwhelming number of  millionaires in this survey were men.

    While I don't want to spoil your enjoyment of this book, I do want to reveal the two characteristics that all the self-made millionaires had in common: They think differently from the crowd and they have a strong belief in themselves.

    Now, it could be argued that all the millionaires in Stanley's survey were ""old"" millionaires--at least that they made their millions in the old, pre-Internet economy. That's how they showed up on the IRS and Census Bureau databases that were used to build his survey information.  

    Do you really believe the basic rules of creating--and keeping--wealth have changed just because technology is changing the way we communicate, shop and plan? That's quite a leap.

    So while there are plenty of opportunities to become a millionaire, you might want to be guided by the values that have worked in the past.

 Yes, you might be one of the very few techno-geniuses who creates a new technology that is appreciated by the market. Or you might be employed by or invest in one of those new companies. But in the long run, the lifestyle attributes of the millionaire mind seem universally useful in coming out ahead in the long run. And, after all, that's how you really keep score--over the long run.

    And that's the Savage truth. 

 

Terry Savage is a registered investment adviser for stocks and commodities and is on the board of directors of McDonald's Corp. and Devon Energy Corp. Send questions via e-mail at savage@suntimes.com. Her third book, The Savage Truth on Money, recently was published by John Wiley &amp; Sons Inc.










		
 


















 


				











 
 




  

  













 	



 








 
 


  
 suntimes.com:  Send feedback | Contact Us | About Us | Advertise With Us |  Media Kit |  Make Us Your Home Page  
Chicago Sun-Times: Subscribe | Customer Service
 | Reader Rewards |   Easy Pay |  e-paper | P.M. Edition |  Online Photo Store  
Affiliates:  jump2web |  RogerEbert.com |  
SearchChicago -  Autos | 
SearchChicago -  Homes | 
SearchChicago - Jobs | 
NeighborhoodCircle.com |  Centerstage    Partner: NBC5.com


   






 


 
















  
? Copyright 2007 Sun-Times News Group | Terms of Use and Privacy Policy   
 


Member of the Real Cities Network



 























 




