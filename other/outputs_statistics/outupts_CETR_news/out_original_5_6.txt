


 By Mark Felsenthal 


 WASHINGTON (Reuters) - Federal Reserve Chairman Ben Bernanke looks set to tell Congress this week that the U.S. economy is sound, but that the central bank is still concerned that inflation, while moderating, may not fu"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


  
 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     Top Business News 
     Banking &amp; Financial 
     Consumer Products 
     Health 
     Tech, Media &amp; Telecom 
     Small Business 
     
 
   1:26PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      Business
      
      &gt;
      Article
 
 
 
       Bernanke to say economy sound, inflation a risk 
       Sun Feb 11, 2007 12:44PM EST 
      
     

     
 
	

  
    
  

 


      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Mark Felsenthal 

    


 WASHINGTON (Reuters) - Federal Reserve Chairman Ben Bernanke looks set to tell Congress this week that the U.S. economy is sound, but that the central bank is still concerned that inflation, while moderating, may not fully come to heel. 

    


 In two days of testimony on the Fed's semiannual monetary policy report on Wednesday and Thursday, Bernanke can boast that the central bank's interest-rate strategy appears to have put the economy on track for a ""soft landing"" in which growth slows just enough to keep inflation at bay. 

    


 At the same time, he will caution lawmakers -- and financial markets -- that a low-inflation outcome is not yet assured, and that the central bank stands ready to raise benchmark interest rates above their current 5.25 percent level to squelch bubbling price pressures if necessary. 

    


 The Fed has held overnight borrowing costs steady since June, when it put in place the last of 17 straight rate increases. 

    


 After their last meeting on January 30-31, Bernanke and his colleagues indicated concern that a tight labor market might make it difficult to bring inflation down. The U.S. jobless rate stood at a historically low 4.6 percent in January. 

    


 ""With the low unemployment rate, he remains concerned that core inflation might pick up again after having slowed a bit recently,"" said Maury Harris, chief U.S. economist for UBS in New York. 

    


 'UPBEAT ASSESSMENT'  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Industry News
                     
                     
				 
		
		 
				 
					Bernanke to say economy sound, inflation a risk
12:44pm EST
				 
				 
		 
Tokyo bourse to tie up with LSE: report
 
		 
G7 warns markets against yen-shrinking bets
 
		 
				
					More Industry News...
			 
		 
		 

 

 


 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Insights 
                     
				 
		
		 
				
						

				 
					Wireless industry to focus on growth at trade show
Fri Feb 09, 2007 01:56PM EST
				 
				 AMSTERDAM (Reuters) - Riding the wave of demand for mobile communications in developing countries and finding new ways to make money from maturing markets will be preoccupations at the world's top wireless trade show, 3GSM, in Barcelona. 
                
                    Full Article  
         
		 
		 

 

 

 
	 
    


 
 	 Top Company Searches on Reuters.com 
 
 
 
 1. AAPL
 
 2. GOOG
 
 3. MSFT
 
 4. GE
 
 5. SIRI
 
 
 
   6. CSCO
 
   7. DELL
 
   8. INTC
 
   9. SI.N
 
 10. MOT
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















