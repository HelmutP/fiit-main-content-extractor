

 "//W3C//DTD XHTML 1.0 Transitional//EN""
 	""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;
















 
	
	Charities across Canada squeezed as donations dry up
 	
 	
	
	
	
	



	
	
	









	 
 
	 
		 
			

			  
			 
				 
					 News 
					 Sports 
					 Entertainment 
					 Radio 
					 TV 
					 My Region 
				 
			 
			 
				
				
			 
			
		 
	 
	 
		 
 Member Centre: Login | Sign Up
 
 
 
		 
	 
 
	 
		 
	 

 
	 
		 
			 Home 
			 Canada Votes 
			 World 
			 
Canada
				 
					 Nunavut Votes 
                                         British Columbia 
					 Calgary 
					 Edmonton 
					 Manitoba 
					 Montreal 
					 N.B. 
					 N.L. 
					 North 
					 N.S. 
					 Ottawa 
					 P.E.I. 
					 Saskatchewan 
					 Thunder Bay 
					 Toronto 
				 
			 
			 Health 
			 
Arts &amp; Entertainment
				 
					 Music 
					 Film 
					 Television 
					 Books 
					 Media 
					 Art &amp; Design 
					 Theatre 
				 
			 
			 Technology &amp; Science 
			 Money 
			 Consumer Life 
			 Diversions 
			 Weather 
			 Your Voice 
		 
	 
 
		
		 
			 
				 Story Tools: E-MAIL  | PRINT  |  Text Size: S M L XL | REPORT TYPO | SEND YOUR FEEDBACK
 
				 
					



 Charities across Canada squeezed as donations dry up 

 Last Updated: 

Tuesday, October 28, 2008 |  2:26 PM ET

 

 

CBC News
 

				 
				 
					

 Officials with Canadian charities say the wobbly economy is squeezing the number of donations and endowments they typically receive as need continues to rise across the country. 
 The Vancouver Foundation, which gives out endowments to charities, expects to give 25 per cent less to charities, according to spokeswoman Catherine Clement. 
 ""We know given the market conditions and trying to be reasonably cautious that we're probably looking at a $10- to $15-million decline in the amount of money that we put back out in the community,"" she said.   
'The thing that people don't seem to realize is but for a paycheck, they could be the next person on our list that needs help.'???Barb Warren, Salvation Army
   Meanwhile at the grassroots level, charities including the Salvation Army say donations are slow in coming. 
 Barb Warren, a Dartmouth-based manager with the Salvation Army, said she's urging people to continue giving. 
 ""The thing that people don't seem to realize is but for a paycheck, they could be the next person on our list that needs help,"" she said. 
 Similarly, Mike MacDonald of the Upper Room Food Bank in Charlottetown says more people are having a hard time making ends meet. 
 ""We have seen an increase of over six per cent in just over the last number of weeks,"" he said. 
 But Michael McKnight, a spokesman for the United Way, said despite the economic turmoil, Canadians will find a way to help each other. 
 ""Our history shows and research shows that when times are tough, donors tend to dig a little deeper into their pockets recognizing there are people around them who need some additional support,"" he said.  



				 
				     
				 Story Tools: E-MAIL  | PRINT  |  Text Size: S M L XL | REPORT TYPO | SEND YOUR FEEDBACK
 
				 
			 
			 
				



 
	 Related 
	
		
		 
			 Internal Links 
			 
				
					
								 IN DEPTH: The Bottom Line 
						
				
					
								 IN DEPTH: Giving to charities 
						
				
					
								 Rent hikes will hit hard, charities say 
						
				
					
								 Fuel cost forcing cuts at charities 
						
				
			 
		 
		
	
	
		





	
	
 





				
				 
					 Money Headlines 
					 
	 
		 Top Headlines 
		 Recommended 
		 Commented 
	 
 
 
	 
		
 

	 
Markets make massive closing gains 
 
	 A late-session massive buying spree propelled the S&amp;P/TSX composite index to its fourth-largest point gain ever and matched its third biggest gain in terms percentage points. 

	 
Auto parts makers 'out of cash' and 'going out of business,' group says  
 
	 Canadian auto parts makers ""are out of cash"" and need fast, low-interest loans from the federal and Ontario governments to survive, the Toronto-based trade group that represents them says. 

	 
Charities across Canada squeezed as donations dry up 
 
	 Officials with Canadian charities say the wobbly economy is squeezing the number of donations and endowments they typically receive as need continues to rise across the country.  

	 
GM, Chrysler want $10 billion US bailout to help them merge: report 
 
	 The U.S. government is said to have been asked for about $10 billion US to support a merger between General Motors and Chrysler, and Ontario auto parts makers are reported to be seeking as much as $1 billion Cdn in government aid. 

	 
Iceland hikes interest rates 6 percentage points to boost currency 
 
	 Iceland raised its trend-setting lending rate by 50 per cent Tuesday in a desperate attempt to attract foreign cash and boost its ailing currency.   

 
	 
	 
		     
	 
	 
		     
	 
 
				 
				 
	 Money Features 
	

 

 
 FINANCECentral banks 
 Why central bankers are suddenly sexy 
 
 






 
   
     FeatureMoney Matters  
     Falling dollar has Canadian NHL teams worried 
      
  
  





 
				

 
	 People who read this also read … 
	 
	
 

				 
 
     Continue Article 
     
         
         
     
    
 
 
				 
	 In the Blogs 
	 
		 Most Blogged about CBC.ca Articles  
		 
 Thousands face infection risk after syringes reused at Alberta hospital 
 No laughing matter: Satirical magazine Frank folds 
 U.S. authorities uncover skinhead plot to assassinate Obama 
 

		 Full list of articles » 
	 
	 
		
			Search Technorati
			
			
		
		
		 Note: CBC does not endorse and is not responsible for the content of external sites - link will open in new window 
	 
 



	

				 
 
 
				
			 
		 
		 
	 Top CBCNews.ca Headlines 
	 
		 
			 CBCNews.ca 
			 World 
			 Canada 
			 Health 
			 Arts &amp; Entertainment 
			 Technology &amp; Science 
			 Money 
			 Consumer Life 
			 Diversions 
			 Your Voice 
		 
		 
			 
	 Headlines 
	
 

	 
McKenna rules out Liberal leadership bid  
 
	 Frank McKenna, a former New Brunswick premier, said Tuesday he will not run for the leadership of the federal Liberal party, putting an end to mounting speculation about his political future. 

	 
Williams apologizes, takes 'full responsibility' at cancer inquiry  
 
	 Premier Danny Williams apologized Tuesday for any grief and anguish that Newfoundland and Labrador breast cancer patients have suffered because of flawed laboratory tests.  

	 
Harper calls first ministers meeting for Nov. 10 
 
	 Prime Minister Stephen Harper has called a first ministers meeting to discuss the impact of global economic turmoil on Canada. 

	 
Markets make massive closing gains 
 
	 A late-session massive buying spree propelled the S&amp;P/TSX composite index to its fourth-largest point gain ever and matched its third biggest gain in terms percentage points. 

	 
Harper has no excuse to run a deficit: Paul Martin 
 
	 The Conservative government has no excuse if it goes into a deficit after inheriting a $12-billion surplus from the Liberals just two years ago, former prime minister Paul Martin said Tuesday in Toronto while promoting his book, Hell or High Water. 

 
 
 
	 News Features 
	
 
		 
	 
 
	 
	 
	 
	

	

 
	 
		  
 News 
 Sports 
 Entertainment 
 Radio 
 TV 
 Digital Archives 
  
		  
 About CBC 
 Canadian Broadcasting Centre 
 CBC Merchandise and Shop 
 Educational Resources 
 Tapes, Transcripts and Image Assets 
 Archive Sales 
 Jobs 
 Production Facilities 
 Independent Production 
 International Sales 
 Advertising with CBC 
  
		  
 Privacy 
 Terms Of Use 
 Ombudsman 
 Reuse &amp; Permissions 
 Other Policies 
 Help 
 Site Map 
 CBC Member Centre 
 Contact Us 
  
		  
		  
	 
	 Copyright © CBC 2008 
 
	















	

 
 





