

ike
millions of Americans, I 
hoarded each yellow-bordered 
issue of National Geographic as 
though it were an heirloom. Indeed, 
when I was growing up in the late 
1940's, a neighbor owned every issue 
from the beginning -- 1888. Since 
Playboy magazine wouldn't be invented for a few years, his son and I 
spent hours thumbing through the 
pages to marvel over pictures of 
bare-breasted women on remote Pacific isles, among other natural wonders, of course. 
   
 

 
 

 







 
This Week's Articles
  � The Complete National Geographic""
 
  � ""Powers of Ten Interactive""
 
  � ""Betye Saar: Digital Griot""
 
 



So I felt guilty when, during a 
move eight years ago, I dumped my 
35 years of National Geographic on a 
charity rummage sale. (It was that 
or give up my jazz LP collection.) 
And guiltier still when the magazine 
commissioned me to do a story on 
vanishing migratory songbirds, 
sending me to Jamaica and Costa 
Rica, among other places, on the 
kind of expense budget I could never 
have imagined.
 
       Now that awful load of guilt has 
been lifted with the arrival on my 
desk of  The Complete National Geographic on CD-ROM: 109 years (issues since 1888 and through 1997) on 
31 disks in a slipcase (bright yellow, 
of course) that would hold less than 
three years of the real thing. If that's 
still too much space, the 
collection is available 
on four DVD's. 
 
  The National Geographic Society boasts 
that the collection includes every cover 
(1,247), every page 
(190,000), every article 
(9,300), every photograph and illustration 
(180,000) and every advertisement, all 
scanned directly from 
the original magazines. 
The loose fold-out maps 
are missing, gathered 
in a CD-ROM package 
of their own. The society's archivists, we are 
told, had to turn to used-book stores, garage 
sales and libraries to 
find the necessary five 
copies of rare issues for 
scanning. In total, they  
assembled 6,236 magazines that occupied 90 
feet of shelf space. 
 
  Those are the gee-whiz facts of the 
project, but there are other issues. 
The society maintains that because 
the CD-ROM archive consists of exact images of pages as they were 
originally published rather than representing further editorial use, no 
additional payments to writers and 
photographers are necessary. This 
stance has angered some of the magazine's contributors.
 
  Nonetheless, after running the 
easy installation program, I loaded 
the appropriate CD and, with considerable pride, checked  out my article. 
After all, how many writers get a 
chance to do even one piece for the 
world's best-known magazine?
 
  And the pictures look great! The 
magazine's pages are displayed a 
spread at a time, and the scans were 
optimized to favor the photographs. 
My words, however, were faint and 
barely legible, even on a 17-inch monitor. But after you click on a spread 
to zoom in and then maximize it to 
full-screen size, you will find toolbar 
buttons that darken the text (along 
with the images) in three incremental steps to make it more readable. 
 
  There is also the option of printing 
a spread, one page at a time, in the 
exact size of the original magazine. 
Using an Epson Photo 700 ink-jet 
printer, I tried both glossy and flat 
photo-quality paper at settings of 360, 
720 and 1,400 dots per inch with consistently disappointing results: the 
type was faint and badly smudged, 
and the sharp photographic detail 
you see on screen  was lost in a smear 
of pixels. Moreover, because the 
CD's were created from single pages 
removed from bound magazines, every spread is divided by a vertical 
black stripe. Photographs that cross 
over from one page to the next are 
essentially split in half.   
 
 
 
  Searching the 109-year archive, 
however, yields split-second results. 
Entering ""South Pacific"" returned, 
among other citations, a July 1948 
story called ""Pacific Wards of Uncle 
Sam,""  the stuff  of boyhood memories 
in brilliant Kodachrome colors. 
 
   But browsing is the best way to 
follow the National Geographic's 
evolution from a scholarly scientific 
journal with a conservative, dark-brown cover and no photographs into 
a legendary publication that today 
reaches 50 million adults and children each month.
 
  For example, the publication had a 
longstanding policy of printing ""only 
what is of a kindly nature about any 
country or people."" But a browser 
will notice that the policy changed in 
the 1970's with the coverage of issues 
like environmental problems.
 
      Jumping from issue to issue with a 
click of the mouse doesn't quite equal 
the experience of flipping through 
the pages of a real magazine. And 
reading the stories and picture captions seriously strains the eyes, even 
at the darkest type setting. But the 
fast and efficient CD-ROM search 
engine beats hunting through a shelf-load of magazines for a vaguely remembered article. And The Complete National Geographic reminds 
us that the magazine itself, for all its 
perceived and sometimes imperious 
faults, is a national treasure. Even if 
the yellow-bound issues sometimes 
end up in a Dumpster. 
 
     
 Les Line is a freelance science writer, an author and a former editor in 
chief of Audubon magazine.  
 
  
 
   

