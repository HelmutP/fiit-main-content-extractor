

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
  
  Sunday Tribune
  
  
  


  
  
  
  
  
  
  
  
  




   
     
      IOL News
       | 
      IOL Sport
       | 
      IOL Business
       | 
      IOL Jobs
       | 
      IOL Entertainment
       | 
      IOL Travel
       | 
      IOL Motoring
       | 
      IOL Property
       | 
      IOL Parenting
       | 
      IOL Classifieds
       | 
      More 
     
   
      
           
     
        
       
          
         
	  News |
	  Opinion |
	  Entertainment |
	  Sport
         
       
     

     

     

       
      
       
         Unisa: Malema isn't studying here 
         28 October 2008, 15:06 
         
           Related Articles 
           
             
               Malema's grim report card is 'real' 
             
           
           
         
         
	  ANC Youth League President Julius Malema has never registered for law or any other degree at the University of South Africa (Unisa), the institution said on Tuesday. 
 
Spokesperson Doreen Gough said the institutions had no record of Malema's admission for any degree studies. 
 
""He was in fact registered in Unisa's access or non-formal programmes and has not made any further progress."" 
 
Unisa was responding to a report in The Star on Friday, which published a copy of his matric certificate and indicated that he had ""successfully registered"" for a part-time law degree at the university. 
 
According to the certificate, Malema matriculated at the age of 21, scraping through with an H for Mathematics and a G for Woodworking, both on the standard grade. He got Es for Sepedi and Afrikaans, both on higher grade, an F in Geography, a D in History and a C in second language English, all on the higher grade. 
 
ANC Youth League spokesperson Floyd Shivambu denied the results were real, saying: ""As far as we are concerned, these results are not a reflection of reality."" 
 
He declined to give Malema's ""real results"", saying the ANCYL was ""not focused on the individual but the collective"". - Sapa									
         
         
	   EMAIL STORY
	     
	   EASY PRINT
	     
	   SEARCH
	     
	   NEWSLETTER
	 
	  
       

       


       
         
          
            Search Sunday Tribune:
            
            
            
          
	   
            login |
            subscribe
	   
	 
	     
     	      
             
               Ramaphosa shows his true colours 
              Businessman and ANC leader Cyril Ramaphosa, once hailed as the heir apparent to the presidency, has warned dissidents against leaving the ruling party, reminding them of the "scars" he bore during the seven years he served under Thabo Mbeki.
               
                 Party girls drug, rob pub patrons 
                 Girls' tragic vigil at slain mom's side 
                 Stowaway's ordeal 
               
             
			 
Subscribe today &amp; get full access to our Premium content 
 
            
              
             

 

 
	
             
           

           
              
             
               
                 Today's Front Page 
                 Browse By Page 
                 Past Front Pages 
                 Supplements 
                 Subscribe Now 
               
             

		

           

            
             
            
           

         

         
         
           About Sunday Tribune 
           
             
               About Us 
               Advertising 
               Booking Deadlines 
               Contact Us 
               Readership 
               Terms &amp; Conditions 
             
           
           Sister Newspaper Sites 
           
             
               Cape Argus 
               Cape Times 
               Daily News 
               Isolezwe 
               Post 
               Pretoria News 
               Sun. Independent 
               Sunday Tribune 
               The Independent on Saturday 
               The Mercury 
               The Star 
             
           
          
         
                 
 

         
           Online Services 
           
             
               Accessories 4 Outdoors 
               Audio, TV, GPS &amp; PS3 etc 
               Books, CDs and DVDs 
               Car Hire 
               Car Insurance 
               Car Insurance for Women 
               Cars Online 
               Compare Insurance Quotes 
               Education 
               Gold-Reflect who you are 
               Home Loans 
               Lease Agreements 
               Life Insurance 
               Life Insurance for Women 
               Medical Aid 
               Offer to Purchase 
               Online Shopping 
               Play Euro &amp; UK Lotteries 
               Property Search 
               Residential Property 
             
           
           Mobile Services 
           
             
               IOL On Your Phone 
               Mobile Games 
               Personalise Your Phone 
             
           
         

       


       
          
          
         

            
                
                
                

            
            Date Your Destiny
            
                 
           
            
                
                
                
                 

               
                  
                
                 
                  I'm a 24 year old woman looking to meet men between the ages of 18 and 30.
                
              
              
           
           
         
      
 






  
    

		
			
			
			
			 
				
					Select Province &amp;
					Eastern Cape
					Free State
					Gauteng (PWV)
					KwaZulu Natal
					Mpumalanga (E. Tvl)
					North West
					Northern Cape
					Northern Province
					Western Cape
					------------------
					Botswana
					Lesotho
					Malawi
					Namibia
					Swaziland
					Zimbabwe
					Other Country (Not in SA)
				
			 
		
	
  
  
	
  
  
      
  
  
    
		 
			A 
			B 
			C 
			D 
			E 
			F 
			G 
			H 
			I 
			J 
			K 
			L 
			M 
			N 
			O 
			P 
			Q 
			R 
			S  
			T 
			U 
			V 
			W 
			X 
			Y 
			Z
		 
	
  
  
      
  
	 
       

 
         
           
              
             
               IOL JOBS 
               
                 Building a new future through passion  
                 Your performance is always measured  
                 Jobs database aids another young hopeful  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL TRAVEL 
               
                 More than a can of worms  
                 I've got you under my skin  
                 Unwind under the African sky  
               
             
            
           
         
       
       
         
           
              
             
               IOL TECHNOLOGY 
               
                 Nokia gives away smartphone technology  
                 Thailand to block websites insulting royals  
                 Intel to invest in solar venture  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL MOTORING 
               
                 'Hamilton prime to beat my title record' - Schumie  
                 Two new reasons for watching A1GP this season  
                 UK show warning for JIMS trade participants  
               
             
            Sign up for our FREE newsletter
           
         
       
       
         
           
              
             
               IOL PARENTING 
               
                 SA's shocking teen sex stories revealed  
                 Japanese parents try to marry off children  
                 Travel a major cause of accidental deaths  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL SPORT 
               
                 De Villiers isn't gambling on Pienaar, Smit  
                 Torres still hamstrung  
                 Pakistan fails to secure Windies Tests  
               
             
            
           
         
       
       
         
           
              
             
               IOL ENTERTAINMENT 
               
                 Chemistry between blonde trio will be electric  
                 'He either buys or rents his own jet'  
                 Christian signs up for Strange role  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               BUSINESS REPORT 
               
                 Stocks claw back lossed despite running out of steam  
                 Farming enterprises on BEE road  
                 Busa woos Chinese investors  
               
             
            Sign up for our FREE newsletter
           
         
		
		

		
	
		
		
       
       
       
RSS Feeds  |  Free IOL headlines  |  Newsletters  |  IOL on your phone  |  Group Links  
       
         
          © 2008 Sunday Tribune &amp; Independent Online (Pty) Ltd. All rights reserved. 
          Reliance on the information this site contains is at your own risk.  
          Please read our Terms and Conditions of Use and Privacy Policy. 
		  Independent Newspapers subscribes to the South African Press Code that prescribes news that is truthful, accurate, fair and balanced. If we don't live up to   the Code please contact the Press Ombudsman at 011 788 4837 or 011 788 4829.
         
         

		
		
		 
       
		  
		
		
       
     

   
  

 


  






