

 "Prominent bystander killings in Canada

 
 Last Updated: 

Tuesday, October 28, 2008 | 11:52 AM ET

 

 

CBC News
 

				
				 
					

 Dec. 26, 2005   Jane Creba, 15, was shot to death on Dec. 26, 2005, as two groups of people exchanged gunfire in a crowd of holiday shoppers on Yonge Street.  Two rival groups of men start shooting at each other around dinnertime on Toronto's busy Yonge Street on Boxing Day 2005. Jane Creba, a 15-year-old girl out shopping with her sister, is struck by a bullet and killed. Six other people on the street are wounded. 
 Two young offenders and seven adults would later face charges in the shooting. At the trial of one of the youths, identified only as J.S.R., a police officer who was at the scene would describe it as ""sheer pandemonium."" 
 ""There were hundreds of people running everywhere. It was complete mayhem,"" Const. Brian Callanan testified.     CBC Story: 'Panic, mayhem' on Yonge Street after Creba shot: witness     
   Oct. 19, 2007   Six men are found dead in what police described as a gang-style slaying on the 15th floor of an apartment building in Surrey, B.C. 
 RCMP would later describe two of the men ??? Chris Mohan, 22, of Surrey and Edward J. Schellenberg, 55, of Abbotsford ??? as innocent bystanders who were caught ""in the wrong place at the wrong time."" Schellenberg was a gas fireplace repairman who was working in the building that day. Police did not say why Mohan was at the apartment. 
 The RCMP said the killings were related to drugs and gang activity. ""The nature of the murders leaves us with the opinion this incident is not a random event,"" said Chief Supt. Fraser MacRae of the Surrey RCMP detachment.     CBC Story: 2 men 'in the wrong place at the wrong time' in Surrey slayings: police
     
  Jan. 12, 2008   Two men are kicked out of the Brass Rail strip club on Yonge Street in Toronto about 1:30 a.m. The men later return to the front of the club and fire shots at a crowd outside. John O'Keefe, who was heading home from the Duke of Gloucester Pub, also on Yonge Street, is hit and killed. 
 Toronto police Det. Sgt. Dan Nielsen says the two men kicked out of the strip club tried to shoot a bouncer and O'Keefe just got in the way. 
 Awet Zekarias, 22, and Edward Paredes, 23, are arrested just hours after the shooting. Both men are from Scarborough.     CBC Story: Passerby shot, killed on Yonge Street     
  Hou Chang Mao was fatally wounded by a stray bullet while stacking fruit at a Toronto grocery store.  (Toronto Police Service)  Jan. 17, 2008   Hou Chang Mao, 47, is killed by a stray bullet as he is stacking fruit at a grocery store on Gerrard Street East in Toronto's Chinatown East during the evening rush hour. 
 Police would later say they are frustrated by a language barrier and a lack of co-operation from members of the Chinese community. 
 Police release images from security video cameras of a silver Nissan Maxima and of two black men whom they describe as persons of interest.      CBC Story: Police canvas neighbourhood looking for witnesses to East Chinatown killing      
 Bailey Zaveda, 23.     (Toronto Police Service)  Oct. 25, 2008   Two men start fighting in a crowd of a dozen people outside the Duke of York tavern on Toronto's Queen Street East. One of the men starts shooting at the other, and hits him and four other people. One of them, Bailey Zaveda, 23, is killed. 
 Toronto police later issue a Canada-wide arrest warrant for Kyle Weese, 24, of no fixed address, for second-degree murder in connection with the shooting. Toronto police Det.-Sgt. Gary Giroux describes Weese as a ""very violent person"" with a ""very extensive criminal record."""
"2505","CBC News


				 
 
				 
					

 Dec. 26, 2005   Jane Creba, 15, was shot to death on Dec. 26, 2005, as two groups of people exchanged gunfire in a crowd of holiday shoppers on Yonge Street.  Two rival groups of men start shooting at each other around dinnertime on Toronto's busy Yonge Street on Boxing Day 2005. Jane Creba, a 15-year-old girl out shopping with her sister, is struck by a bullet and killed. Six other people on the street are wounded. 
 Two young offenders and seven adults would later face charges in the shooting. At the trial of one of the youths, identified only as J.S.R., a police officer who was at the scene would describe it as ""sheer pandemonium."" 
 ""There were hundreds of people running everywhere. It was complete mayhem,"" Const. Brian Callanan testified.     CBC Story: 'Panic, mayhem' on Yonge Street after Creba shot: witness     
   Oct. 19, 2007   Six men are found dead in what police described as a gang-style slaying on the 15th floor of an apartment building in Surrey, B.C. 
 RCMP would later describe two of the men ??? Chris Mohan, 22, of Surrey and Edward J. Schellenberg, 55, of Abbotsford ??? as innocent bystanders who were caught ""in the wrong place at the wrong time."" Schellenberg was a gas fireplace repairman who was working in the building that day. Police did not say why Mohan was at the apartment. 
 The RCMP said the killings were related to drugs and gang activity. ""The nature of the murders leaves us with the opinion this incident is not a random event,"" said Chief Supt. Fraser MacRae of the Surrey RCMP detachment.     CBC Story: 2 men 'in the wrong place at the wrong time' in Surrey slayings: police
     
  Jan. 12, 2008   Two men are kicked out of the Brass Rail strip club on Yonge Street in Toronto about 1:30 a.m. The men later return to the front of the club and fire shots at a crowd outside. John O'Keefe, who was heading home from the Duke of Gloucester Pub, also on Yonge Street, is hit and killed. 
 Toronto police Det. Sgt. Dan Nielsen says the two men kicked out of the strip club tried to shoot a bouncer and O'Keefe just got in the way. 
 Awet Zekarias, 22, and Edward Paredes, 23, are arrested just hours after the shooting. Both men are from Scarborough.     CBC Story: Passerby shot, killed on Yonge Street     
  Hou Chang Mao was fatally wounded by a stray bullet while stacking fruit at a Toronto grocery store.  (Toronto Police Service)  Jan. 17, 2008   Hou Chang Mao, 47, is killed by a stray bullet as he is stacking fruit at a grocery store on Gerrard Street East in Toronto's Chinatown East during the evening rush hour. 
 Police would later say they are frustrated by a language barrier and a lack of co-operation from members of the Chinese community. 
 Police release images from security video cameras of a silver Nissan Maxima and of two black men whom they describe as persons of interest.      CBC Story: Police canvas neighbourhood looking for witnesses to East Chinatown killing      
 Bailey Zaveda, 23.     (Toronto Police Service)  Oct. 25, 2008   Two men start fighting in a crowd of a dozen people outside the Duke of York tavern on Toronto's Queen Street East. One of the men starts shooting at the other, and hits him and four other people. One of them, Bailey Zaveda, 23, is killed. 
 Toronto police later issue a Canada-wide arrest warrant for Kyle Weese, 24, of no fixed address, for second-degree murder in connection with the shooting. Toronto police Det.-Sgt. Gary Giroux describes Weese as a ""very violent person"" with a ""very extensive criminal record."" 
 

