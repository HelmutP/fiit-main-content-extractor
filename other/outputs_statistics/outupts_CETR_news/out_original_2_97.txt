

 "        
//W3C//DTD XHTML 1.0 Strict//EN""
        ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd""&gt;


 

						    
			    				'Hate preachers' must prove they renounce extremism or face UK ban |
				Politics |
				guardian.co.uk
	
		

			
		




								
			
		
		
		
	
		
					
				
		
		
				









 

    
    		
	
					
				  
			

    		
	
	
	
		  
	

 
	 
		 Jump to content [s] 
				 Jump to site navigation [0] 
		 Jump to search [4] 
		 Terms and conditions [8] 
	 
 
 

	 
		 

 Sign in 
 Register 

 Text larger
 
 smaller 

 




					
						



     


	        
    
                
        
        
	
    

     

			
	 
	 
					
			 
				
		
	
	
		Search: 
		
		
			
		
			guardian.co.uk
			
							Politics
						Web
			
		
		
		
		
	
		
	 
		    

	 
		 
			
																													 
						News
					 
							
																								 
						Sport
					 
							
																								 
						Comment
					 
							
																								 
						Culture
					 
							
																								 
						Business
					 
							
																								 
						Money
					 
							
																													 
						Life &amp; style
					 
							
																								 
						Travel
					 
							
																								 
						Environment
					 
							
																													 
						Blogs
					 
							
																													 
						Video
					 
							
																													 
						Jobs
					 
							
																																		 
						A-Z
					 
									 
	 


					 			
									    
	
	  
		 
										 News 
							 Politics 
							 Terrorism policy 
					 
	 



							 
			 
		
		
 


 
 	    


	 
					    


				
		        


        



	    




  	   	        
   	   	   	   	
      	       
  	   	        
   	   	   		
	
		
        
            

		
				
		 
			
							 'Hate preachers' must prove they renounce extremism or face UK ban 
						
							 Jacqui Smith measures will make it easier to exclude those who want to come to Britain to stir up racial or religious hatred 
				
					 
			 



     
      	    
  
	        

	    
        




	
 
	    			 
											                	        	        	        	            Deborah Summers and Alan Travis
				 
							 
			  guardian.co.uk,
			 
	  			  			 Tuesday October 28 2008 12.16 GMT 
	  			  				
		
	 

 

	
			 Foreign-born ""preachers of hate"" and other violent extremists will have to prove they have publicly renounced their views or be refused entry to Britain under tough new rules unveiled today. 
 The measures, set out in a written ministerial statement by Jacqui Smith, the home secretary, will make it easier to exclude those who want to come to the UK to stir up religious or racial hatred. 
 Such preachers will be named and shamed, and the exclusion list will be shared with other countries to help them, the Home Office said. 
 Smith said: ""Through these tough new measures I will stop those who want to spread extremism, hatred and violent messages in our communities from coming to our country. 
 ""Coming to the UK is a privilege and I refuse to extend that privilege to individuals who abuse our standards and values to undermine our way of life.""    The new rules will: 
 ? create a presumption in favour of exclusion in respect of all those who have engaged in fostering, encouraging or spreading extremism and hatred, 
 ? provide that where an individual claims to have renounced their previous extremist views or actions the burden of proof is on them to demonstrate that this is so and that this has been publicly communicated, 
 ? introduce changes that will allow the government to exclude nationals of the European Economic Area, and their families, from the UK, before they travel to this country, if they constitute a threat to public security or policy 
 ? work with other agencies and community groups to improve the evidence base underpinning our exclusion decisions to ensure that we identify those who pose the greatest threat to our society, 
 ? consider in all future cases whether it would be in the public interest to disclose that an individual has been excluded with a presumption to inform the public, and 
 ? make greater use of UK watch lists to ensure that individuals who might fail to be excluded, should they seek to come to the UK, are identified, and any future visa application is considered with full regard to previous extremist activities.    The move will cover anti-abortionists, animal rights extremists and neo-Nazis as well as extremist clerics. 
 Since 2005, 230 people have been banned on grounds of national security and for ""unacceptable types of behaviour"", which include fomenting or glorifying terrorism and inciting hatred. Of those, 79 were excluded for unacceptable behaviour. 
 The most high-profile individuals to face exclusion orders include Omar Bakri Mohammed, who was banned in the wake of the July 7 terrorist attacks in the London transport system, and the Egypt-born cleric Yusuf al-Qaradawi, banned by the Home Office this year after being described as ""dangerous and divisive"" by David Cameron, the Tory leader. 
 The Home Office anticipates that the new rules will see a larger number of people barred from Britain. 
 The ban rests on the government's legal powers to tackle violent extremism and incitement to religious and racial hatred. It does not permit somebody to be banned for simply holding disagreeable political views but will apply to those who advocate illegal means to further them. 
 An MI5 report leaked to the Guardian this year on the current state of radicalisation and violent extremism in Britain noted that the influence of extremist clerics had ""moved into the background"" in recent years. 
	
	
 


    
  	
	
  
 




		
 
		
		    



	 
	
							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
		
			 
			
			
			larger | 
			smaller		 
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
					
					
					 
			
		 
												 	
										
										
											 	
																			Contact the Politics editor politics.editor@guardianunlimited.co.uk 
											 
										
						
												 
				 
				 
	


			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 

		    

	
	 
	
							
					 Politics 
					
						 
			 
									Terrorism policy 							 
		 
									
					 UK news 
					
						 
			 
									UK security and terrorism 							 
		 
									
					 World news 
					
						 
			 
									Global terrorism ·							 
			 
									Religion ·							 
			 
									Islam 							 
		 
					
							
		
 


		    			 
			 
																										 
						 More on this story 
					 
																																															 
							 
								    
		    	
	
	
								Smith to ban 'preachers of hate'
							 
						 
																																																	 
							 
								    
		    	
	
	
								Inayat Bunglawala: The shame game
							 
						 
																					 
		 
	

		    


	
		
		
				
	


 

	
	
		
	    



	 

							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
			 Article history 
		
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
					
					
					 
			
		 
												 	
										
										
											 	
																			Contact the Politics editor politics.editor@guardianunlimited.co.uk 
											 
										
						
												 
				 
				 
	


			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 
	     
	 
		 
			 About this article 
            Close
		 
		        	        			
					   	   	   
 			 
				 'Hate preachers' must prove they renounce extremism or face UK ban 
		   				   	 						This article was first published on guardian.co.uk on Tuesday October 28 2008. It was last updated at 12.16 on October 28 2008.							 
			 
 


  	    	    
	    	 
									 Related information follows Ads by Google 
						    



	
 




	 




				  
 

 
	
	
      	    	    
	    




     


	        
    
                
        
        
	
    

     


	        				
 	
 


	    
			 
		 Latest news on guardian.co.uk 
	

		 
			    
	                         Last updated less than one minute ago 
            
		 
		 
							 
					 News 
					 Man sentenced to 30 years for Kercher murder 
				 
							 
					 Sport 
					 Newcastle beat West Brom 
				 
							 
					 Politics 
					 IMF needs more funds, says PM 
				 
					 
	 

	        


 
															 Free P&amp;P at the Guardian bookshop 
																					
	 
					
							 
							 
					
						
					
				 
				 
					 
						 Duel 
						 ?17.99 with free UK delivery 
					 
				 
			 
					
							 
							 
					
						
					
				 
				 
					 
						 American Future 
						 ?20.00 with free UK delivery 
					 
				 
			 
			 
			 
																			 
													Browse more politics books
											 
																 
													Buy books from the Guardian Bookshop
											 
									 
	 


	     
	 
		 Find your MP 
		
			
			Enter name of the MP
		
		
			Enter postcode or placename
		
	 Or browse the map | About this search 

	 
 



	    

	
		
 
	
	
		
	 
		 
			 Sponsored features 
		 
		 	
						            
    
                
        
        
            
		 
		 	
						            
    
                
        
        
        
		 
	 
 


	    	
		
			
	
		 

	
			  
	
		
	    



	  
		
			
											
										
									
				 
		UK
	 
				
		
			
											
										
						
				 
		USA
	 
				
			 

	 
			 
			
						
													
			 UK 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Press Officers
 
												 mod.
												whitehall, london.
												?24,725- ?32,253 (incorporating Recruitment &amp; Retention Allowance ?2,500 London RRA + 4% London Weighting and DGMC GCN London Allowance ?3,000).
								 
			 
					 
 
				Child Protection Secretary
 
												 surrey county council.
												surrey.
												?18,270 - ?21,339.
								 
			 
					 
 
				CONSERVATIVE GROUP RESEARCH OFFICER
 
												 oldham metropolitan borough council.
												level 3, civic centre, west street, oldham.
												?28,919 - ?31,606 p.a. (pro rata).
								 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
			 
			
						
													
			 USA 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Choir Director/Organist
 
								 singing for our "heritage" services and related... regularly with director of worship &amp; arts and others to plan music for heritage services. position is part... .
																oh.
												 
			 
					 
 
				Busy, private, single specialty ENT practice is looking for a BE/BC otolaryngologist
 
								 stunning scenery, big-city excitement, and small-town charm; music, arts, history and heritage ... it all awaits in missouri. live in a growing and vital city... .
																mo.
												 
			 
					 
 
				Dean, School of Education
 
								 its catholic heritage, as expressed in the motto "teach me goodness, discipline, and knowledge." through an education rooted in the liberal arts, sjfc prepares... .
																ny.
												 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
		 
	


	
	
		
	
	

	
	 




      	    	    
	    			



     


	        
    
                
        
        
	
    

     

	


      	      	
      	 

			    
	    


	
		
		
		
		 
							 Related information 
				    

	
	 
	
							
					 Politics 
					
						 
			 
									Terrorism policy 							 
		 
									
					 UK news 
					
						 
			 
									UK security and terrorism 							 
		 
									
					 World news 
					
						 
			 
									Global terrorism ·							 
			 
									Religion ·							 
			 
									Islam 							 
		 
					
		
		
 

				
			
						 
			
																					 
													
												
					 
								 
															
											Haltemprice and Howden byelection: meet the candidates
						 
 	
							    

										                    		Jul 9 2008: 
		                    								 David Davis resigned as shadow home secretary and MP for Haltemprice and Howden in order to draw attention to the government's civil liberties policies. But he is not the only candidate standing in today's byelection
					 	 
					 						 							 							
															More galleries
													 	
												
													
							 

						 
			
													 
				Aug 29 2008 
										 
															
						    

						Samia Rahman: Racial and religious profiling is counterproductive, as Adam Khan knows only too well.
				  
																		 
				Jun 3 2008 
										 
															
						    

						New plan to tackle violent extremism
				  
																		 
				Feb 7 2008 
										 
															
						    

						Controversial Muslim cleric banned from Britain 
				  
																		 
				Feb 4 2008 
										 
															
						    

						Straw announces inquiry into claims MP was bugged
				  
							 

						 
			
																					 
													
												
					 
								 
															
											David Davis resigns over 42-days vote
						 
 	
							    

										                    		Jun 12 2008: 
		                    								 'I will argue against the slow strangulation of fundamental British freedoms by this government': The shadow home secretary, David Davis, resigns over the issue of detention without charge
					 	 
					 						 							 							
															More video
													 	
												
													
							 

			
		 
		
				
	


		




 
		 			
			 
License/buy our content |  
			 
Privacy policy |  
			 
Terms &amp; conditions |  
			 
Advertising guide |  
			 
Accessibility |  
			 
A-Z index |  
			 
Inside guardian.co.uk blog |  
			 
About guardian.co.uk |  
			 Join our dating site today 
		 
		
			
			 		
			
			 guardian.co.uk © Guardian News and Media Limited 2008 
			
						
			 
			
	
	    

	


	
		
			Go to: 
			
			
						
												
								
																														guardian.co.uk home
									
								
																	UK news
									
								
																	World news
									
								
																	Comment is free blog
									
								
																	Newsblog
									
								
																	Sport blog
									
								
																	Arts &amp; Entertainment blog
									
								
																	In pictures
									
								
																	Video
									
								
									
								
																	Archive search
									
								
																	Arts &amp; entertainment
									
								
																	Books
									
								
																	Business
									
								
																	EducationGuardian.co.uk
									
								
																	Environment
									
								
																	Film
									
								
																	Football
									
								
																	Jobs
									
								
																	Katine appeal
									
								
																	Life &amp; style
									
								
																	MediaGuardian.co.uk
									
								
																	Money
									
								
																	Music
									
								
																	The Observer
									
								
																	Politics
									
								
																	Science
									
								
																	Shopping
									
								
																	SocietyGuardian.co.uk
									
								
																	Sport
									
								
																	Talk
									
								
																	Technology
									
								
																	Travel
									
								
																	Been there
									
								
									
								
																	Email services
									
								
																	Special reports
									
								
																	The Guardian
									
								
																	The Northerner
									
								
																	The Wrap
									
								
									
								
																	Advertising guide
									
								
																	Compare finance products
									
								
																	Crossword
									
								
																	Events / offers
									
								
																	Feedback
									
								
																	Garden centre
									
								
																	GNM Press Office
									
								
																	Graduate
									
								
																	Bookshop
									
								
																	Guardian Ecostore
									
								
																	Guardian Films
									
								
																	Headline service
									
								
																	Help / contacts
									
								
																	Information
									
								
																	Living our values
									
								
																	Newsroom
									
								
																	Notes &amp; Queries
									
								
																	Reader Offers
									
								
																	Readers' editor
									
								
																	Soulmates dating
									
								
																	Style guide
									
								
																	Syndication services
									
								
																	Travel offers
									
								
																	TV listings
									
								
																	Weather
									
								
																	Web guides
									
								
																	Working for us
									
								
									
								
																	Guardian Weekly
									
								
																	Public
									
								
																	Learn
									
								
																	Guardian back issues
									
								
																	Observer back issues
									
								
																	Guardian Professional
									
														
			
		
	

 

 
	    


	    





	  





	    
	









     


	        
    
                
        
        
	
    

     




