

 April 8, 2007 -- AUGUSTA - If there's a crack in the invincibility that surrounds Tiger Woods, the one knock against a game that has produced four Masters victories and 12 major championships, it's the fact he hasn't come from behind to win a major. Yet.  
 Yes, it's petty, but a fact nonetheless, and haters mention it all the time. Woods has won all 12 of his majors after owning or sharing the lead after 54 holes. Anytime he has trailed, he has watched someone else go on to victory.  
 He has a chance to change that today when a most unusual Masters begins its final round with Woods trailing leader Stuart Appleby of Australia by one stroke.  
 If Woods, who is 3-over-par for the tournament, needs any extra incentive today, maybe he'll feed off the whispers that he's a great front-runner but terrible closer. But my guess is he'll be too busy trying to figure out how to navigate 18 more holes at brutally difficult Augusta National.  
 Usually, the Masters is about the players: Woods going for his third consecutive major; Phil Mickelson (at 6-over) trying to retain his Green Jacket; and Appleby (2-over) trying to become the first Australian to win the Masters.  
 But this tournament is about the course and its menacing greens. Par is not good here. It's outstanding. And even-par was what Woods shot yesterday despite a bogey-bogey finish.  
 "If you make 18 pars, you're going to move up that board," he said of his goal for today.  
 This is how difficult Augusta National played on a cold, windy day yesterday. Woods was fourth when he finished his round, but found himself in the final group, as all except for Appleby tumbled below him. 

