

 By KIERAN CROWLEY and ERIKA MARTINEZ             
				  
				  
				   
 

 
                  
				  
				   

                                    August 8, 2007 -- An NYPD cop's wife was busted for shooting his service weapon through their Long Island bedroom wall during a domestic dispute, police said.  
  The 9mm slug exited the bedroom of their Levittown home and lodged in a neighbor's wall.  
  On Saturday night at 7:30, Carolann Adduci, 44, and her husband, Vincenzo, a cop assigned to the Transit Borough Queens Task Force, were arguing, said Nassau Sgt. Anthony Repalone.  
  Carolann Adduci, a legal secretary, "fires his 9 mm Glock service weapon into the bedroom wall," said Repalone. There were no injuries.  
  Mrs. Adduci was charged with reckless endangerment, a misdemeanor.  
  Officer Adduci, a 16-year veteran, declined to comment yesterday. He was placed on modified assignment for failure to safeguard his weapon and faces possible NYPD Command Discipline, a source said. 

