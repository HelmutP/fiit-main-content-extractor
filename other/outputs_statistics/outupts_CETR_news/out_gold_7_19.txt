

 Three U.S. Senators -- including the duo that pushed through the CAN SPAM Act -- have introduced legislation designed to put an end to spyware, adware, and other invasive software from being secretly installed on computers.
 
 
 
 
Sen. Ron Wyden (D-Ore.), Conrad Burns (R-Mont.), and Barbara Boxer (D-Calif.) last week introduced the SPYBLOCK (Software Principles Yielding Better Levels of Consumer Knowledge) Act, which would require a user's consent before software's installed and demand uninstall procedures for all downloadable software.
 
 
 
 
Wyden and Burns were the co-sponsors of the CAN-SPAM Act, which aims to limit spam, and went into effect in January 2004.
 
 
 
 
Spyware, adware and other hidden programs piggyback on downloaded software, such as file-sharing applications, typically secretly. Such software tracks Web usage, puts up pop-up ads, or even changes the browser's home page; it's often difficult to detect or uninstall without special utilities.
 
 
 
 
""The Internet is a window on the world, but spyware allows virtual Peeping Toms to watch where you go and what you do on the Internet,"" said Wyden in a statement.
 
 
 
 
""Computer users should have the same amount of privacy online as they do when they close the blinds in the windows of their house,"" added Burns in the joint statement. ""But computers are being hijacked everyday as users unknowingly download unwanted and deceitful programs.""
 
 
 
 
SPYBLOCK would require all software to generate on-screen dialog boxes to tell users that clicking ""OK"" will trigger the download of a program; demand strict disclosure if software creates pop-ups, collects information and sends it elsewhere over the Internet, or modifies a computer's settings; and prohibit programs that steer users to counterfeit Web sites.
 
 
 
 
The bill would be enforced by the Federal Trade Commission (FTC) and state attorneys general, and would include provisions for injunctions and civil fines.
 
 
 
 
SPYBLOCK will be referred to the Senate Committee on Commerce, Science and Transportation; all three senators are members of the committee. 

