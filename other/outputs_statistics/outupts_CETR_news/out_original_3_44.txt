

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
HIRE &amp; LOWER AS TOP COPS SING THE BLUES | By JAMIE SCHRAM | New York News | New York City News | NY News


























 
     
    
       
       
          
             	
                            
             
               NYC Weather 75 ° CLOUDY              
          
          
             
          
          
             
               Sunday, August 26, 2007 
               
                   Last Update: 
		08:45 AM EDT                   
               
             
             
             
             
                     
             


			Recent
			30 days+
			The Web
			

           
       

       
            
           
			 
				Story Index
				Summer of '77
			 
			
            
            
            
            
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Movies
            Make Us Rich
            Real Estate
            Travel
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
            Intern Stories
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
   News Home
   Columnists
   Local
   National
   International
   Weird But True
   NYPD Blotter
	   Traffic &amp; Transit
   Lottery
 

	 
		Hopstop
		MTA Advisories
	 
	      
       
           
          
              
             
               
                               
                   
                      HIRE &amp; LOWER AS TOP COPS SING THE BLUES     
                      
                   
                   By JAMIE SCHRAM 

					

                         
	                 
		 
			Has warned of crisis."" WIDTH=""223"" alt=""""&gt;
		  		
		 
			Loading new images...
		 
	
		 
			
		 
	      	 

	
              
				  
				  
				   

 
                  
				  
				   

                                    June 5, 2007 -- The NYPD will fall more than 2,000 recruits short of the target for its next academy class, and several precincts have already suffered substantial drops in manpower, police brass testified yesterday.  
  At a City Council hearing, Rafael Pineiro, the department's chief of personnel, said he anticipated hiring between 700 and 800 recruits for the Police Academy class on July 9. The department had set a hiring goal of more than 3,000 cops for that class.  
  He also noted that there are just under 36,000 officers on the force, a decrease of 5,000 from October 2000, when the department reached a high of nearly 41,000 officers.  
  "We have serious problems," Pineiro said, adding that the difficulty in attracting new recruits boils down to the low starting salary of $25,100.  
  The NYPD is trying to manage an expected rise in attrition rates in 2010, as cops hired as part of the Safe Street, Safe City program 20 years earlier become eligible for retirement.  
  Police Commissioner Ray Kelly has also blamed the low starting salary for a looming manpower crisis.  
  Patrick Lynch, president of the Patrolmen's Benevolent Association, also testified at the hearing and said the situation has reached a "crisis point."  
  "Specifically, the NYPD is unable to recruit and retain enough qualified candidates to staff the precincts and commands in the city at a level that is safe," Lynch said.  
  As examples, Lynch pointed to the considerable drop in patrols and staffing rates in the 111th Precinct in Bayside, Queens, and at the 28th Precinct in Harlem.  
  At the 111th, which covers 9.4 square miles and has 116,000 residents, Lynch said it's "simply not enough" to have two or three patrol cars to cover such a large area.  
  In the 28th, Lynch said, there are currently 188 officers compared with 252 in 2000.  
   
  Critics say the police union is itself responsible for the low starting pay, accusing it of selling out its own recruits' salary in order to get raises for existing members. Union leadership denies the charge.  
  Peter Vallone Jr., chairman of the council's Public Safety Committee, echoed Lynch's view on the department's staffing troubles, saying, "It is a crisis waiting to happen."  
  The meeting grew testy when Vallone asked NYPD Deputy Chief John Gerrish, "Do you know of any other police force in any major city since 9/11 that has decided that the best strategy would be to downsize?"  
   
  Stunned, Gerrish responded, "I can't answer that question."  
  Then Vallone took aim at Pineiro, noting that in December 2004, in connection with arbitration proceedings, Pineiro testified that the NYPD did not have a recruiting problem.  
  But Pineiro fired back with statistics showing that there was a "precipitous drop" in the number of people applying for the NYPD exam after the contract introducing the $21,500 starting pay was announced in 2005.  
  For example, Pineiro said, there were 35,039 applicants in October 2004, 28,737 applicants in February 2005 and 29,027 applicants in June 2005.  
  When the contract was announced on June 27, 2005, there were just 21,124 applicants in October that year, 23,275 applicants in February 2006 and 21,000 in June 2006. 
  jamie.schram@nypost.com 
   
                    
                  

             
             
                


 
  PBA SUES TO BOO$T '9/11 AIR VICTIM' 
  CADETS BATTLE ON HOME FRONT 
  SPIDEY CENTS SET A RECORD  
  FINEST COPS, LOWEST PAY -   
 

 
 PSYCHO SPIN-OUT 
 SOCIAL STAR'S MA IN 'POOR'  HOUSE 
 N.Y. FORECLOSE FRENZY 
 MORE 
 
                 
 

 
                              			     
                  
				 
					






 
	 
	    
		 
		  			 DATING 
		  			 JOBS 
		  			 AUTOS 
		  			 REAL ESTATE 
		  			
		 
	 
	     
 				 
             
          
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
           
            sign in
            subscribeprivacy policyNEWS HEADLINES FROM OUR PARTNERSrss 
          
       
    
     
    
       NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007 NYP Holdings, Inc. All rights reserved. 
  	   
 


