

 "        
    //W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
























Now for the real test - Mail &amp; Guardian Online: The smart news source







 



 
	 
   
   
   


Click here?









    

    



	
	
	
	
	
	

   
    










   
   
 

    	
        	
            	

					


					
					


					
					
					
                
				 
 Make this your home page
				 
				

			   
			   
			   
			   
			   

			
			
                
            
      
     
     
    	
        	
        		
                	
                    	
                           
                            Home
                            
                                                        
                            News
                            
                            
                                                        Opinion
                            

                                                        Business
                            

                                                        Sport
                            
							                             Arts
                            
                                                        Leisure
                            




                            The Guide



                                                          Special reports
                            
                                                        Blogs
                            
							                             Topics A-Z
                            
							



JOBS | CARS | DATING | PROPERTY | SHOPPING





        		
                    	
													
                        	                            National
                            Africa
                            World
                            And in other news...
                            Zapiro
                            Weather
							Madam &amp; Eve
                            Photos
                            
                            
                            
                             

							  


                             
                             
                                                     
                    
                

                    	
                        	

							
																		 
										LOG IN
									 
																
                            

							SIGN UP
                            


                            MY DASHBOARD
                            
                        
                    
                
        	

        


     



     
    
	
		
			EMAIL
			
			PASSWORD
			
            
 Remember me?
			
			Forgot password?
			Create a new account
		
	
    
	 

	
		
			 
THE SMART NEWS SOURCE | Oct 29 2008 00:48 | LAST UPDATED Oct 29 2008 00:48 
			
			 
			 ABOUT THE NEW SITE | CONTACT US | STORY TIP-OFFS    
			 
			

 

			
		
	

 


		
			
			 SECTIONS 
			

    
     
     National 
     Africa 
     World 
	 Opinion    
     Business 
     And in other news... 
     Zapiro        
     Madam &amp; Eve        
     Motoring    
     Technology        
      
     


    
        
    
     Sports        
     Arts        
     The Guide 
     Editorials 
     The Weekly Wrap 
     Thought Leader 
     Tech Leader 
     Sports Leader 
     SA blogs 
	 Travel         
     



			
			
			 SERVICES 
			       
     Subscribe to newspaper 
     Download digital edition    
     Advertise with us 
    
     Competitions!        
     Win books!        
     Headlines for your site 
     News on your desktop    
     Get the weather            
     
			
			
			 ABOUT US 
			    
     About us 
     History of M&amp;G 
     CSI projects 
     Contact us    
     
			
			
			 OTHER 
			    
     Mobile sites 
     Podcasts    
     RSS Feeds 
     
			
		
		
	

	 








	
		
			 
				News | National | Education			 
		
		
	

 







 
	
    	
		    		
			 Now for the real test 
			 
						THABO MOHLALA
						
             | 				JOHANNESBURG, SOUTH AFRICA									-
						Oct 24 2008 06:00
			 
            
         
     
 
 
	
		 2 comment(s) | Post your comment
	
 
 
 

	
		
		 ARTICLE TOOLS 

	
	
		
		Add to Clippings
	
	
		
		 Email to friend
	
	
		
		Print 
	
	
		
  MORE TOOLS 

	
	
		
		Add to Facebook
	
	
		
		Add to Muti
	
	
		
		Add to del.icio.us
	
	
		
		Blog Reactions
	
	
		
		
	
		
		
  MAP 

	
	
		

    

     
        The map will replace this text. If any users do not have Flash Player 8 (or above), they'll see this message.
     
    
 	





		
	
	
 


 

 
 
 

	
	Next week a new form of matric exam commences -- and tensions among learners, teachers and parents were heightened by recent blunders around trial maths papers. This has left many wondering whether the department of education will deliver a credible final exam. Penny Vinjevold, deputy director general for further education and training, set out to put Thabo Mohlala?s mind at rest.	
	 
	 
	
	
This marks the first year in which grade 12 learners would be sitting for their National Senior Certificate examinations since the introduction of the new curriculum, how ready are you for this challenging operation?  
Administratively, we have been preparing for these exams since 2005, we started to plan how we would build up to give both confidence and to improve our ability to examine and distribute and to mark the exam papers. We appointed [exam] panels and so since 2006 we have been training our exam panels who set grade 10,11 and 12 papers. In 2006 a sample of schools wrote grade 10 and in 2007 all grade 11 learners wrote the exams at the end of last year set by our papers.  
Then at the beginning of 2008 grade 12 exemplar papers set by the same panel saying were released to schools so that teachers and learners could see what the exams would look like at the end of the year. We have already recruited 35 000 markers and started training them together with invigilators. We made sure we recruit more senior markers with lot of experience. Our exam capturing system and back up systems are ready to release the results at the end of December.  
The National Department of Education DoE has made available exemplar papers to all learners. But the similarities between the exemplar and trial papers amount to what has been called ""coaching"" or ""rigging"".  
No. Exams are set in completely different ways. Good examinations and good assessment practices throughout the world are not about putting obstacles in front of the children?you don't try to deliberately trip them up. What good exam does is test how much do children know, it doesn't try to hide the rules of the game from them. So all we've said was we want to give you the format so that you don't get surprised or frightened or get nervous during the exams. The exam itself would not be predictable. Secondly we made sure there are sets of questions that are problem-solving, questions that have high cognitive demand. In previous years children had passed papers to work through. We believe it is our duty to provide our learners with lots of examples of what the paper might look like. So we don't see this as coaching at all.  
There are concerns that the exam timetable provides too little time between papers.  
We looked at this very carefully. In the past exams ran over five weeks and we lost the whole of October for teaching. Parents and teachers complained that the learners were often at home for five days between one exam and the next and in fact that this is much worse to the learners. Because they start to lose concentration and some get up to mischief or some would have forgotten what they were taught. So our view is that we needed to start in November and in that way we recover the whole month for teaching. We tried to do some good things like making sure that maths is written in the morning and we also interspersed the subjects with languages.   
Change of content in subjects like physical science and visual arts were introduced late in the year and this has caused confusion among learners and teachers as they have already gone through those sections. 
It was never an external examination subject. It has always been an internally examined subject. But our strong provincial and national moderation team will look at the quality of the tasks that the learners have done during the year. Our moderators would make sure the quality of the internal assessment is acceptable standard. We have responded to teachers who said there was an overload in some subject like physical science and that 90% of the content would be examined while those aspects that cannot be examined would be dealt with through assessment.   
In the case of the arts, our panel of advisors alerted us to the fact that quite a large amount of one particular artist [Steven Cohen]'s work was unsuitable for school study. We received complaints from parents saying that the work bordered on pornography. We felt it would be irresponsible of the department [DoE] to leave the artist whose work was unsuitable as part of our recommended list, so we removed. But teachers who felt they wanted to teach the artist could do so as long as parents are consulted.  
Life orientation would not be externally examined, yet this subject can make a learner pass or fail. Could DoE and Umalusi give guarantees that there would be a uniform standard in this subject across all the provinces and schools? 
It was never an external examination subject. It has always been clear that it is an internally examined and contextually based subject. But we do have a strong provincial and national moderation team who go around and look at the quality of the task that the learners have done during the year. Those that are not up to scratch either because it is not of quality or the work was never handed in could make a learner fail the subject. In November our team of moderators will be going through the nine provinces to make sure the quality of the internal assessment is of acceptable standard.  
It is rumoured that the actual pass rate for last year's matric exam was 50% against the public figure of 65%. Is this true and how do you justify such a massive increase? 
The rumour is definitely incorrect. Learner performance is measured using carefully and thoughtfully constructed question papers using the best expertise available. After the paper is set and moderated by independent expert it is then presented to Umalusi for final approval. Therefore after the examination is written and the question papers marked, Umalusi reviews the marks for each subject and compare the marks obtained in the current examination to previous examinations.    
 CONTINUES BELOW 
 
 

Click here?

 
  
If there are no compelling reasons why marks in current examination should be higher or lower than previous, Umalusi adjust the marks, within strict boundaries, to be in line with the marks of previous history. The adjustment of the marks is an internationally acknowledged process to ensure that equivalent standards are maintained from year to year. The principle was applied in the Senior Certificate in public exam and has been in use as early as 1933.   
During the marking of the exam papers markers rely on memorandums, which have to be fairly broad as to accommodate different answers learners may give. Will the markers be able to consider answers that are correct albeit different to those in the memorandum? 
That is why the markers we have chosen are teachers who have produced high results in previous years. In other words, we are going for best teachers who understand the curriculum best. We have intensive memorandum discussion sessions where chief markers from all provinces may sit the whole day to discuss relevant points like looking at possible and alternative answers learners may give. They then take the memo back and work through the papers and if anything comes out of that marking they then feed that back in so that everybody knows if there is an open-ended question all the alternatives answers the children could put forward are considered.  
There are reports that in the Northern Cape only 42 schools were selected to sit for the trial examinations and this created anxiety among learners who did not take part in the exercise, why is this so? 
These schools obtained under-60% pass rate in 2007, so they had to take part in the compulsory trial exams. We have to protect the children. The exams were available to all schools but we didn't want to impose it on schools that have already set their own exams and who have been getting between 80%-90% pass rates.   
Why can't you supply the public with raw marks as opposed to Umalusi's adjusted marks. Is this not misleading to the public, universities and the learners themselves? 
The raw marks are those from the written examination only, which are then presented to Umalusi for their quality assurance as explained earlier. After they have been approved the marks are then combined with the internal assessment marks, which includes school based assessment (orals and practical marks), to arrive at the final marks per learner.  
Can this year's matriculants enroll with top universities overseas without taking on additional courses first or writing entrance tests? What system has our matric exam being benchmarked against? 
The National Senior Certificate (NSC) allows candidates an admission to university either for higher certificate, diploma, or Bachelor's degree studies. Candidates need to satisfy specific pass requirements for each of these areas of study and some universities may have additional requirements for admission. The NSC has been benchmarked with Scottish Qualification Authority, Cambridge International Examinations and the Board of Studies, New South Wales.   
These bodies evaluated the scope and content of [our] curriculum and exemplar question papers. They evaluated question papers of ten major subjects including: accounting, English, first additional language, economics business studies, history, geography, physical science, life sciences, mathematics and mathematical literacy. The three assessment bodies concluded that the subjects are internationally comparable.  
There is now no standard or high-grade differentiation, which was meant to accommodate learners of different abilities. How will they now be catered for? 
That is a very good question. What we've done is to ask our examiners to make sure that 30% of questions are standard grade level. In other words, you shouldn't fail this year if you'd have passed last year. So there is a good bunch of questions that are for standard grade level.   
Then the level goes up quite steeply to 50%, which is higher grade. We have made sure that in every question paper there are difficult questions right up to 80%-90% level just to make sure there is differentiation. Our exams have been structured to make sure we've high level of differentiation in the papers.  
What are the penalties for staff who leak exam papers and what will happen to exam cheats? 
Irregularities that occur during the conduct of the NSC's examination are managed in terms of the various applicable and relevant regulations. Staff members who are involved in leaking the exam question paper will be dealt with in accordance with the prescribed regulations and this can result in a dismissal. The matter may also be referred to police for a possible criminal charge.   
In cases where there are reports of alleged irregularities, the matter is investigated and will be handled by the Provincial Examination Irregularities Committee. In the event that it is established that a learner gained unfair advantage over others, his or her marks will be declared null and void and the candidate may be barred from writing the examination for a maximum period of 3-5 years.   
Finally in light of the recent slip-up with regard to preliminary mathematics paper one and mathematical literacy paper, for which you apologized, what assurance can you give to learners, teachers and parents who might still be harbouring doubts about the integrity and credibility of the forthcoming national examinations? 
The first point is that those exams were not set by DoE's national panel. They were not internally and externally moderated. And the only thing that went wrong was that the memorandum was not checked. And it was only in mathematics.   
So I think we got out the memorandum immediately within 48 hours.But I still want to re-assure people that that [trial exams] was not set by the national panel and what I've done after that was to be absolutely sure that we have checked, re-checked and double checked the papers and memorandums and of course there would be memorandum discussion sessions that I spoke about earlier.	
	
	





	 TOPICS IN THIS ARTICLE 

	
		
									
				 Tags 
				 
								  education system 
								 matric exams 
								 
			
												
				 People 
				 
								 Penny Vinjevold 
								 
			
			
		

	


 
	
	
	 



 Article comments 
 







 
		In most first-world countries, where ""best practice"" is applied and whence SA has drawn its ambitious OBE models, the pass mark is 50% or, in some cases, even higher. In SA it's still stuck as low as 33%. 
 
Then again, in those first-world countries the pass rate is also roughly 50% -- approximating the pass rates of first-year university courses. Half of all matric candidates fail. That's seen as quite ""normal"".  
 
And that is why their school-leaving certificates are widely respected all over the world as being sufficiently rigorous. South African school-leaving/university-entrance certificates are widely regarded as a bit of a joke when great rafts of ordinary state schools routinely churn out their 100% pass rates year after year.  
 
 
Jon Low on October 27, 2008, 5:31 am 
 
		
 
		Q: There are concerns that the exam timetable provides too little time between papers.  
 
A: ?  In the past exams ran over five weeks ? . Parents and teachers complained ?  .  ? or some would have forgotten what they were taught. 
 
If they have forgotten what they learned between September and November I would hate to think about what they have forgotten by February when varsity starts.   
 
 
 
Brencis Price on October 25, 2008, 1:11 pm 
 
		


 



 
	 
		You must be logged in to comment. log in or sign up to comment
	 
 
 
	 
	click here to log in
	 
 






 

 


 	
	
		

			EMAIL
			
			PASSWORD
			

			
		
		
		 Forgot password? | Create a new account


		
	
    
 


				 Advertising  Links 

 

 

 
Selfmed Medical Aid Scheme is a registered and exceptionally stable ""open"" medical aid provider, with excellent reserves.  We live up to our motto: ""Simplicity, Sincerity and Security with Personal Service""
 

 
Nashua Electronics Audio, TV, GPS &amp; PS3 etc
  


 
Life insurance made simple No medical examinations. No HIV test. No application forms. Quote me now! 

 
Spin and win R1 000 000! Gamble and win huge jackpots from the comfort of your own home. Click here to get R100 FREE! 



 
Looking for a reliable IT supplier? Shop at PCMall.co.za for the best prices on all PC and technology equipment, notebooks, etc 


 
Private Property listings List your property with us or search for a property. No estate agent's commission 


 
Cape Town. Covered. Daily. Fly with Emirates starting from AED 4 000 

 

 		



 


 



Click here?

 



    

   
    
 
 LATEST ARTICLES IN THIS SECTION 
               
			  				 MDC questions Mugabe's will to seal deal 
								 Islamists stone Somali woman to death for adultery 
								 Urgent African summit called to rescue Zim deal 
								 Obama, McCain spar over economy 
								 Lekota, Shilowa extend invitation to SA public 
							 

 POPULAR ARTICLES IN THIS SECTION 


 
	 
	 24 Hours 
	 1 Week 
	 1 Month 
	 
	 
	  
			  				 Obama, McCain spar over economy 
								 Islamists stone Somali woman to death for adultery 
								 Shilowa: Insults will not stop us 
								 Zuma: ANC not concerned by 'angry people' 
								 Zim summit fails to break impasse on unity pact 
							 
	 
	 
	 
	 				 Chair collapse turns Nene into web celebrity 
								 DA calls on IEC to quell 'political violence' 
								 Jennifer Hudson's mother, brother killed 
								 YCL slams publication of Malema's matric results 
								 Troubled Dept of Home Affairs needs help 
							 
	 
	 
	 
	 				 NWU appalled by Facebook racists 
								 Three hospitals in Gauteng renamed 
								 Burundi's albinos flee witchdoctors 
								 Somalia welcomes Russian help in fighting piracy 
								 Pollution slowly killing world's coral reefs 
							 
	 

 







		
		 



 



 Your M&amp;G Online 

						
							
							 
							 SUBSCRIBE TO M&amp;G 
							
							 ON YOUR PHONE 

 ON YOUR DESKTOP 

 VIA PODCAST 
						 

							
							 
							 ON YOUR WEBSITE 

 VIA RSS FEED 

 IN DIGITAL PDF 

 PLACE AN AD 
						 

							
								





 

 











 

 
  
   
  
  
   
   
   
   

   
 READ OF THE WEEK 
      

    
 More on this book 
 More M&amp;G books 
 Previous books 
 Go to Exclusive 
 Win books 
 

   
  

  
   
  


 






 
  
   
  
  
   
   
   
   
 THIS WEEK'S PAPER 
   
 
 Subscribe 
 Subs promotions 
 Get digital edition 
 

   
   
  
  
   
  







 
 Advertisements 



	
	
	
	



	
	
	
	

  

 Advertising links 




 



 ACCOMMODATION 




 FIRST FOR WOMEN 

 ONLINE CASINO 
 

 LIFE INSURANCE 

 INTERNET SOLUTIONS 

 



 


 BUY TECHNOLOGY 


 MAP SEARCH 

 HOLIDAY FINDER 

 ONLINE SHOPPING 

 INSURANCE 

 






		















 
 
 


Make us your home page | Add our headlines to your site | Desktop news with NewsFlash

  



Contact us | About us | Story tip-offs | Online advertising | Subscriptions | Topics A-Z | Corrections | Mobile sites   










All material © Mail &amp; Guardian Online. Material may not be published or reproduced in any form without prior written permission.  Read the site privacy policy and Ispa code of conduct relating to our blog hosting.  RSS feed   

 
This site is proudly hosted by Internet Solutions. We are a member of the Online Publishers Association.     
  






  
 
 










	
 
	


Click here?





 



 

	
	
		

	


	

	
	
 













