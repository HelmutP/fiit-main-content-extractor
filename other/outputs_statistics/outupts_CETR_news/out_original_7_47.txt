














	
 



 
 
 The  Network 
 Tech News 
 Product Reviews 
 Business Intelligence 
 Security 
 Storage 
 VoIP 
 IT Management 
 Business 
 
 
 
 
IT News from 
 
 
Enterprise Product Reviews from 
 
 
Enterprise Software News from 
 
 
IT Security News from 
 
 
Enterprise Storage News From 
 
 
VoIP News from 
 
 
IT Management Insights from 
 
 
Business News from 
 
 	
 
	





 
 







 


 
 
 Register Now! 
 Benefits 
 Login 
 
 


 

 
 
 
  
 


SEARCH 


  
 
 
  
 

 


 
 
 
 Blogs 
 News 
 Mobile 
 Software 
 Security 
 E-Business &amp; Management 
 Networking 
 Hardware 
 Careers 
 White Papers 
 Newsletters 
 
 

 



 
 

July 27, 2005 (10:04 PM EDT)



 The Price Of Service 



 
By
Katherine Burger
, 
 
 It's no secret that insurance companies (as well as many other different types of businesses) can drive down the costs of providing customer service by automating it. Interactive voice response (IVR) and Web self-service technologies are a lot cheaper than live representatives, according to research from Gartner. The customer support costs per incident for Web self-service average 50 cents, and $1.85 for IVR, Gartner reports - that's compared to $4.50 for telephone-based incidents.
 
 
But not all tech-enabled customer interactions are cheap, the research reveals. The most costly form of customer support, according to Gartner, is Web chat, which averages $7.50 per incident, followed by automated chat (instant messaging) at $5.25 and basic e-mail at $4.50.
 
 
Add to this the fact that insurers that are sincere about providing true quality customer service and response must offer all (or most) of these options - since customers have multichannel expectations for interaction with their financial services providers - and it is clear that serving and retaining customers is a very costly proposition.
 
 
This issue of Insurance &amp; Technology's Connected Enterprise examines the evolution of service and customer-centricity in today's increasingly integrated and automated insurance enterprise. The challenge, as we report, is not about whether or not to use technology to improve and facilitate customer service and related interactions; it's about making the right choice about tools and partners, and then executing successfully on those choices and strategies.
 
 
Visit www.insurancetech.com/ConnectedEnterprise to learn more about the options and best practices. Additionally, a live webcast on Sept. 22 will provide insights into how leading companies are getting actual returns on their investments in service. 







Bitty Browser (JavaScript required)



 
 
 
 Email This Story 
 Print This Story 
 Reprint This Story 
 Tag in Del.icio.us 
 
Digg This 
  Technology News 
 More TechWeb News 
 Bookmark this site! 
 
 
 


 
 
 
 
   Try TechWeb's RSS Feed! 
(Note: The feed delivers stories from TechWeb.com only, not the entire TechWeb Network.)
 


















 
 


 

To see how Good Mobile Messaging 5 can help you achieve your goals, click
here.
 
 

Make Better Business Decisions, Improve Your Reporting BI
 
 

Strategies for gaining business by protecting the business
 
 

Maximize ROI through business process management and SOA
 
 

Download free white papers and research from TechWeb Briefing Centers 
 
 


  
  
 
 


 
 
 Related White Papers 
 
 

Regulatory Compliance and Critical System Protection: The Role of Mission-Critical Power ...	

 
 

Beyond Search: Measuring The Impact Of Your Search Engine Via Analytics	

 
 

A Product Demo Tip - Demonstrating a Solution vs. a Bunch of Products	

 
 

The Future of PR: How to Garner More Coverage and Stay Way Ahead of the Competition	

 
 

Enhancing Collaboration Between In-House and Agency Teams	

 

  
  
  
 

 	
 
CAREER CENTER
 
 
 
 Ready to take that job and shove it? 
 
Open | Close
 
 
 
 
 
 SEARCH 
 
 


 
Function:  

Information Technology
Engineering

 
 
 
Keyword(s): 

 
 
 
State:
 
 

 
 
 
Post Your Resume
 
 
Employers Area
 
 
News &amp; Features
 
 
Blogs &amp; Forums
 
 
Career Resources 
  
Browse By:  
State | City 
 
 
 

 SPONSOR 
 
 

 
 
 RECENT JOB POSTINGS 
 
 

 

Featured Jobs:
 

BreakthroughIT seeking Project Manager in Groton, CT
 
  

Monsanto seeing IT Transportation and Optimization Analyst in St. Louis, MO
 
  

Princeton Financial Systems seeking Business Analyst 4 in Princeton, NJ
 
  

CSAA seeking IT Analyst IV in Glendale, AZ 
 
  

DisplaySearch seeking IT Project Manager in Austin, TX
 
  
For more great jobs, career-related news, features and services, please visit our ""Career Center.
 
 

 
 
 CAREER NEWS 
 
 
Five Rules For Bringing Your Real-Life Business Into Second Life 
 If you're thinking about establishing yourself in Second Life -- or are wondering whether you should -- we've got five rules that will help your new venture be a success. 
 
 
 
Hiring Report: Northrop Grumman Hiring On Wide Range Of Software Engineers  
 The global defense and tech company is seeking tech professionals skilled in Web site development, general software development, database administration, digital manufacturing, SAP/ABAP, complex CAD/CAM and PLM activities. 
 
 
 
 
 More Articles From Our Career Center 
  
 
 

Advertisement




&lt;noscript&gt;
&lt;a href="" http:&gt;
&lt;img src="" http: border="0" width="336" height="280" alt="" click here&gt;&lt;/a&gt;
&lt;/noscript&gt;


 


 
  
 
 
 Specialty Resources 

 
 

Impact of Outsourcing on IT Professionals
 
 

Whitepaper: Dell's Scalable Enterprise Architecture
 
 

Strategies for gaining business by protecting the business
 
 

Increase productivity with content management
 
 

bMighty: IT Security
 
  
 
 VIEW ALL BRIEFING CENTERS 
 
  
  
 
 
 
 Featured Microsite 
 

 
  
  

 
 
 

 Microsites 
 Featured Topic 


 Additional Topics 
 
 

Bring your business into the paperless future 
 
 

Controlling Access to Information
 
 

  
 
 
 Crush The Competition 
 TechWeb's FREE e-mail newsletters deliver the news you need to come out on top. 




  
  
 
 
 Techencyclopedia 
 Get definitions for more than 20,000 IT terms. 




  
  

 
 
 Techwebcasts 
 Editorial and vendor perspectives 

Take the layered approach to safeguarding assets: Free white paper has details
 

Use supply and demand to bring IT and business into balance. Download this free white paper
 
 
  
  
 


 
 
 Vendor Resources 
 

Eliminate the effort from your Microsoft Vista migration
 

SMBs: Taking backup and storage beyond tape. Download the free white paper.
 
 
  
  
 


 
 
 Focal Points 
 

Come up to speed on KVM-over-IP solutions: Download the free buyers guide
 

Safari Online Books closes gap between innovation and publication.
 
 
  
  
 



 
 
  
	
 

  
  
 
 

 
 

  


 
 


 
  
 
 

 Free Newsletters 
 TechEncyclopedia 
 TechCalendar 
 Opinion 
 Research 
 Careers &amp; Workplace 
 Webcasts 
 About Us 
 Contact Us 
 
 Site Map 
 Mobile Tech News 
 Software Tech News 
 Security Tech News 
 E-Business Tech News 
 Management Tech News 
 
 Networking Tech News 
 Hardware Tech News 
 
  

 
 
 InformationWeek 
 Network Computing 
 Financial Technology Network 
 Wall Street &amp; Technology 
 Bank Systems &amp; Technology 
 
 Insurance &amp; Technology 
 IT Pro Downloads 
 Intelligent Enterprise 
 Byte and Switch 
 Dark Reading 
 bMighty 
 Small Biz Resource 
 
 Byte.com 
 VoipLoop 
 CollaborationLoop 
 Blackhat 
 
Interop 
 
 
  

 
 



&lt;noscript&gt;
&lt;a href="" http:&gt;
&lt;img src="" http: border="0" width="728" height="90" alt="" click here&gt;&lt;/a&gt;
&lt;/noscript&gt;

 
  


 
 
 Terms of Service 
 
Copyright ©2007 CMP Media LLC
 
 Privacy Statement 
 Your California Privacy Rights 
 Media Kit 
 Feedback 
 
  
 
 











