










 




 

 









 


 







  


 December 16, 1999 
 

 REVIEW 


 The Grand Adventure of Railroading
 

LIONEL TRAINS PRESENTS TRANS-CON
 (Knowledge Adventure; $30 for 
standard edition, $40 for centennial 
edition; Windows 95 and 98.)


 By  LES LINE 

ny train enthusiast will tell you 
that there is very little romance in modern railroading. 
Steam locomotives, those living, 
breathing behemoths, disappeared 
from the mainlines in the 1950's, replaced by efficient diesels with colorful corporate paint schemes but no 
soul. 
   
 

 
 

 

 







Players of Trans-Con must overcome obstacles in building 
either the westward Union Pacific or the eastward Central Pacific.
 




Then, one by one, the great railroads that laid a spider web of steel 
across North America in the 19th 
century fell by the trackside, victims 
of bankruptcies, mergers and consolidations. Conrail and Amtrak, for 
example, picked over the carcasses 
of the New York Central, Pennsylvania, Reading  and other Eastern companies, salvaging freight and passenger operations that looked profitable 
and abandoning the rest. 
 
           Only one famous line has managed 
to retain its historic identity: the 
Union Pacific, perhaps the greatest 
railroad of all. It is the Union Pacific 
and the long-ago Central Pacific 
Railroad that are the stars of Trans-Con, a computer game from the toy-train maker Lionel and Knowledge 
Adventure. Trans-Con takes players 
back to a time when railroading was 
a grand adventure and there was a 
thrilling race to lay track across the 
wild western half of the United 
States, linking the Atlantic and Pacific coasts of the young nation. 
 
               Trans-Con puts you in charge of 
building either the westward Union 
Pacific or the eastward Central Pacific  segment of the 1,776-mile transcontinental railroad, overcoming assorted obstacles to be the first to 
reach an arranged meeting point at 
Promontory, Utah, while foiling saboteurs and saving the golden spike 
from outlaws. 
 
  The game, the centennial version 
of which is packaged in a decorative 
tin box with an informative foldout 
map of the route and a tiny model of 
a wood-burning 1860's locomotive, is 
fun, educational -- and nonviolent. 
 
  The graphics and sound effects are 
entertaining, and Lionel hopes the 
game will interest computer-wise 
children in leaving their keyboards 
for a while to play with scaled-down 
trains that also come packed with 
high-tech electronics like realistic 
digital sounds and remote control. 
Grown-ups who already have their 
toy railroads will also find Trans-Con 
a challenge, especially if they click 
the Hard button. 
 
   Construction of the transcontinental railroad, which began in January 
1863 and ended with the driving of 
the golden spike at Promontory on 
May 10, 1869, was an amazing engineering feat for the time. 
 
  The Central Pacific had the shortest route -- 690 miles -- and a three-year head start because its rival was 
delayed by financial and engineering 
problems. But it needed the extra 
time since Central Pacific workers 
faced the formidable task of laying 
track over the Sierra Nevada. Much 
of the Union Pacific's 1,086-mile section crossed gently rolling plains. 
 
   In either case, you will have the 
help of two young, more or less fictional characters --  a 25-year-old inventor and chief engineer, Jack 
Casement (beginning in 1866, the 
Union Pacific construction was supervised by a Civil War general 
named John S. Casement), and his 
16-year-old sister, Sarah. She  is 
adept at foiling the mischief-makers 
and unmasking the mastermind 
(perhaps the shipping mogul Cornelius Vanderbilt or the Confederate 
president, Jefferson Davis) who is 
determined to stop the construction. 
 
        Context-sensitive help is available 
with a single click at any time, as is a 
Train of Thought Journal with page 
after page of accurate, easy-to-digest 
information on topics like railroad 
engineering, geography, wild animals, natural disasters and characters straight out of the history books. 
For instance, the Union Pacific's 
president, Thomas C. Durant, is described as ""a shady fellow who uses 
the money granted by the government to pay for a grand lifestyle."" 
 



 





 A contest that 
involves would-be rail 
barons, heroes and 
villains. 





 






 
 
         A ""money meter""  shows you how 
much is available for each stretch of 
track. Spend too much by making 
bad decisions or experiencing bad 
luck and your railroad will go bankrupt. Then you will have to start the 
section over. The first obstacle west 
of Omaha is the Elkhorn River, and 
you can choose one of three kinds of 
bridges to cross it: a deck plate 
($10,000), a wood trestle ($25,000) or 
an iron trestle ($50,000). Go with the 
cheapest option and it will be washed 
away in a huge storm. It is also a 
really good idea to reroute the track 
to bypass a Pawnee village's crop 
fields  and to keep a reserve for 
losses from a raging prairie fire or to 
find a way through an unexpected 
quicksand maze.
 
  I have just passed present-day 
Grand Island, Neb.,  where Union Pacific workers encountered Indians 
for the first time.   
 
 I figure I will 
reach the virtual Promontory by 
New Year's Day, if I manage to build 
a 126-foot-high, 700-foot-long bridge 
near Laramie, Wyo.,  and find enough 
timber for rails on the nearly treeless plains.
 
         As for the Central Pacific, the 
""steam-powered map"" in Jack Casement's rolling office shows that it 
has been built only part way to the 
summit near Truckee, Calif.,  where 
it will take a year to complete a 1,659-foot tunnel. I'm betting we'll win, 
even though the real race was essentially a tie.
 
  
 
  
 
  
 
  
 
  
 
  

 
  
 

 






 
 
  
  




 
 


Home | 
Site Index | 
Site Search  | 
Forums  | 
Archives | 
Marketplace
  
Quick News  | 
Page One Plus  | 
International  | 
National/N.Y.   | 
Business   | 
Technology  | 
Science   | 
Sports   | 
Weather  | 
Editorial    | 
Op-Ed | 
Arts   | 
Automobiles   | 
Books  | 
Diversions   | 
Job Market   | 
Real Estate  | 
Travel 
 
 
Help/Feedback  | 
Classifieds   | 
Services   | 
New York Today 
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















