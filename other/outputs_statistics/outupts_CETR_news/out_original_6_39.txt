

 //W3C//DTD HTML 4.01 Transitional//EN""&gt;

 
 Review investments as year starts  :: CHICAGO SUN-TIMES :: Terry Savage






	
	
	













    Back to regular view     Print this page
 





 



 








 




Your local news source ::       Select a community or newspaper »
 


	
        
		
	 	
		



Select a community 
------------------
Algonquin
Alsip
Antioch 
Arlington Heights
Aurora
Bannockburn
Barrington 
Barrington Hills
Batavia
Beecher
Bellwood
Berkeley
Blue Island
Bolingbrook
Broadview 
Buffalo Grove
Burr Ridge
Calumet City
Cary 
Chesteron, Ind.
Chicago
Chicago - Albany Park
Chicago - Avondale
Chicago - Belmont Cragin
Chicago -  Bucktown
Chicago - Dunning
Chicago - Edgebrook
Chicago - Edgewater
Chicago - Edison Park
Chicago - Jefferson Park
Chicago - Harlem-Irving
Chicago - Lakeview
Chicago -  Logan Square
Chicago - News Star
Chicago - North Center
Chicago - North Town
Chicago - Norwood Park
Chicago - Portage Park
Chicago - Ravenswood
Chicago - Rogers Park
Chicago - Roscoe Village
Chicago - Sauganash
Chicago - Skyline News
Chicago -  Ukranian Village
Chicago - Uptown
Chicago -  Wicker Park
Chicago Heights
Clarendon Hills
Country Club Hills
Crete
Crestwood
Crown Point, Ind.
Darien
Deerfield
Deer Park
Des Plaines
Dolton
Downers Grove
Elburn
Elgin
Elk Grove
Elmhurst 
Elmwood Park
Evanston 
Flossmoor
Forest Park
Fox River Grove
Fox Valley
Frankfort
Franklin Park 
Gages Lake
Gary, Ind.
Geneva
Glen Ellyn
Glencoe 
Glenview 
Glenwood
Golf 
Grayslake
Green Oaks
Gurnee
Harwood Heights
Harvey
Hawthorn Woods
Hazel Crest
Highland Park
Highwood
Hillside
Hinsdale 
Hobart, Ind.
Hoffman Estates
Homer Glen
Homer Twp.
Homewood
Indian Head Park
Inverness 
Island Lake
Joliet
Kenliworth
Kildeer
La Grange
LaGrange Highlands
LaGrange Park
Lake Barrington 
Lake Bluff 
Lake Forest 
Lake in the Hills 
Lake Villa 
Lake Zurich
Lansing
Lemont
Libertyville
Lincolnshire
Lincolnwood
Lindenhurst 
Lisle
Lockport
Long Grove
Lowell, Ind.
Manhattan
Markham
Matteson
Maywood 
Melrose Park 
Merrillville, Ind.
Midlothian
Mokena
 Montgomery
Morton Grove
Mount Prospect 
Mundelein 
Naperville
New Lenox
Niles
Norridge
North Barrington 
Northbrook 
Northfield
Northlake
Northwest Ind.
Oak Brook 
Oakbrook Terrace 
Oak Forest
Oak Lawn
Oak Park
Oakwood Hills
Olympia Fields
Orland Hills
Orland Park

 Oswego 
Palatine 
Palos Heights
Palos Hills
Palos Park
Palos Twp.
Park Forest
Park Ridge 
Plainfield

Portage, Ind.
Prospect Heights 
Richton Park
River Forest
River Grove
Riverwoods
Rolling Meadows 
Rosemont
Round Lake
Sauk Village
Schaumburg 
Schererville, Ind.
Schiller Park 
Silver Lake
Skokie 
South Barrington 
South Elgin
South Holland
St. Charles
Steger
Stone Park 
Thornton Twp.
Tinley Park/Southland
Tinley Park
Tower Lake
University Park
Valparaiso, Ind.
Venetian Village 
Vernon Hills 
Wadsworth
Wauconda 
Waukegan
West Lake Co., Ind.
West Proviso
Westchester 
Western Springs
Wheaton
Wheeling
Wildwood
Willowbrook 
Wilmette
Winnetka
Worth
Worth Twp.

 Yorkville 







Select a STNG publication or site
  
---------------------------------------------------
 DAILY PUBLICATIONS 
---------------------------------------------------
	Chicago Sun-Times	
	The Beacon News	
	The Courier News	
	The Daily Southtown	
	The Herald News	
	Lake County News-Sun	
	The Naperville Sun	
	Post-Tribune	


  
---------------------------------------------------
 SEARCH CHICAGO 
---------------------------------------------------
Autos
Homes
Jobs


  
---------------------------------------------------
 NEIGHBORHOOD CIRCLE 
---------------------------------------------------
 Montgomery
 Oswego 
 Yorkville 

  
---------------------------------------------------
 ENTERTAINMENT 
---------------------------------------------------
Centerstage
 Roger Ebert


  
---------------------------------------------------
 WEEKLY &amp; SEMIWEEKLY PUBLICATIONS 
---------------------------------------------------
	Algonquin Countryside	
	Antioch Review	
	Arlington Heights Post	
	Barrington Courier-Review	
	Booster: Lake View, North Center,
        Roscoe Village, Avondale Edition	
	Booster: Wicker Park, Bucktown,
	      Ukrainian Village Edition	
	Buffalo Grove Countryside	
	Cary-Grove Countryside	
	Deerfield Review	
	Des Plaines Times	
	Edgebrook-Sauganash Times-Review	
	Edison-Norwood Times-Review	
	Elk Grove Times	
	Elm Leaves	
	Evanston Review	
	Forest Leaves	
	Franklin Park Herald-Journal	
	Glencoe News	
	Glenview Announcements	
	Grayslake Review	
	Gurnee Review	
	Highland Park News	
	Hoffman Estates Review	
	Lake Forester	
	Lake Villa Review	
	Lake Zurich Courier	
	Libertyville Review	
	Lincolnshire Review	
	Lincolnwood Review	
	Morton Grove Champion	
	Mount Prospect Times	
	Mundelein Review	
	News-Star	
	Niles Herald-Spectator	
	Norridge-Harwood Heights News	
	Northbrook Star	
	Oak Leaves	
	Palatine Countryside	
	Park Ridge Herald-Advocate	
	Proviso Herald	
	River Grove Messenger	
	Rolling Meadows Review	
	Schaumburg Review	
	Skokie Review	
	Skyline	
	The Batavia Sun	
	The Bolingbrook Sun 	
	The Doings Clarendon Hills Edition	
	The Doings Elmhurst Edition	
	The Doings Hinsdale Editon	
	The Doings La Grange Edition	
	The Doings Oak Brook Edition	
	The Doings Weekly Edition	
	The Doings Western Springs Edition	
	The Downers Grove Sun	
	The Fox Valley Villages Sun	
	The Geneva Sun	
	The Glen Ellyn Sun	
	The Homer Sun	
	The Lincoln-Way Sun	
	The Lisle Sun	
	The Plainfield Sun	

	The St. Charles Sun	
	The Star: Chicago Heights Area	
	The Star: Country Club Hills, Hazel Crest
	The Star: Crete, University Park, Beecher	
	The Star: Frankfort, Mokena	
	The Star: Homer Glen, Lockport, Lemont	
	The Star: Homewood, Flossmore, Glenwood,
        Olympia Fields	
	The Star: New Lenox, Manhattan	
	The Star: Oak Forest, Crestwood, Midlothian	
	The Star: Oak Lawn, Palos &amp; Worth Townships	
	The Star: Orland Park, Orland Hills	
	The Star: Park Forest, Matteson, Richton Park	
	The Star: South Holland, Thorton Township	
	The Star: Tinley Park	
	The Wheaton Sun	
	Times Harlem-Irving	
	Times Jefferson Park, Portage Park,
       Belmont-Cragin Edition	
	Vernon Hills Review	
	Wauconda Courier	
	Wheeling Countryside	
	Wilmette Life	
	Winnetka Talk	





























 
 







 



 






 
 

 

    
 








 

 
 

 
 

 suntimes.com
 Member of the Sun-Times News Group 
 



 

 
 


Traffic  �  
Weather:  

""WHEW!""


		
 
 
 


Search » 

 

 
 Site
 STNG 




 

  
 

� Subscribe 
� Easy Pay 
� Reader Rewards 
� Customer Service 
� Email newsletters 


 

 

 

 

Home
 | 
News
 | 
Commentary
 | 
Sports
 |  
Business 
 | 
Entertainment
 | 
Classifieds
 | 
 Columnists
 | 
Lifestyles
 | 
Ebert
 | 
Search
  | 
Archives
 | 
Blogs
   | 
  RSS 
 
   




























 










  









 
 
  
  
  
  
    
 Terry Savage 
    
    
     Recent Columns 
  	  
     Savage Q &amp; A 
  	  
     Column Archive 
  	  
     E-mail Terry Savage 
  	  
     Biography 
  	   
  	
  
    Business 
    
    
     Archive 
  	  
     Appointments 
  	  
     Technology 
  	  
     Currency 
  	  
     Futures 
  	  
     Personal finance 
  	  
     Conrad Black on Trial 
  	  
     Portfolio 
  	  
     Real Estate 
  	  
     Stock market 
  	  
       
  	  
     Auto News 
  	  
     Sally Duros 
  	  
     Dan Jedlicka 
  	  
     Real Estate and Homelife 
  	  
     Chicago Innovation Awards 
  	  
     Made in Chicago 
  	  
     What's my line? 
  	   
  	
  	 
  
 	

  

 


    
   
   

    Columnists     
      	 



 Casual Friday 

  

 Robert Feder 

  

  

 Lewis Lazare 

 Ted Pincus 

 David Roeder 

 Terry Savage 

 Brad Spirrison 
 
	  
	 
  	   
	
	
	  
  
	  	
  


 

  

 

 


 





 


 








 






 






 
Terry Savage :: 



printer friendly »     
email article »  



 
 
 







 





 







  
 



 







 

  







 



 

 
VIDEO ::    MORE »
  
 
 
 

 
 
 

  
 
TOP STORIES ::
  
 
 NEWS 











Dry out, power up   


  
 BUSINESS 











W.W. Grainger adds new stores to area 


 
 SPORTS 











Bears are well grounded   


 
 ENTERTAINMENT 











Do celebs get off easy?   


 
 LIFESTYLES 











Glamorama: Coyote pretty  


 




 




 

  





 
 
 















	
	 
	
	
			  
				



 
		 
 Review investments as year starts 
		 
		
		
 
		
		

		 
 August 7, 2001 


		 BY TERRY SAVAGE SUN-TIMES COLUMNIST 
			
					
						
						
 

  
You get a chance to start over every year in your retirement investment account. There are no tax consequences. There's no one looking over your shoulder to laugh at your past mistakes or criticize your current choices. Once you switch from one fund to another, you can forget about losses or bad timing. It's a new year and a new chance to allocate your assets. 
 
But most people won't take that chance. Instead, they figure that if they've made one mistake, they're even more likely to make another if they switch. 
 
 
That's what's behind the refusal to review and, if necessary, to reallocate investments at the start of the year.  
 
The best way to reach your long-term goals is to make a plan--and stick to it. If that plan includes a regular monthly investment in a diversified stock fund, then the absolutely wrong thing to do is to stop investing when prices fall. You're losing the opportunity to buy more shares at lower prices. 
 
You have to take the long-term (15 to 20 years) view. 
 
You can avoid having to make tough decisions by simply choosing an index fund for most of your retirement plan, and taking smaller positions in the other equity alternatives. That's the kind of allocation you can stick with until you're getting close to retirement. 
 
If, on the other hand, you chose riskier alternatives such as momentum or technology funds for most of your retirement plan investments, it's time to take another look. 
 
These are not one-decision funds. You have to figure out when to get out, as well as when to get in. 
 
Having spent much time on the trading floors, I can assure you that even the best and boldest traders make mistakes--including some big ones. But the one thing successful traders and investors have in common is the discipline to re-evaluate past decisions in light of current facts. 
 
This year, the facts have changed.  
 
No matter what the label--recession, slowdown, soft landing--the economy is certainly in a different place than when we started last year. Not only have realities changed, perceptions have changed, as well. 
 
So whether inside your retirement plan, or in your trading account, you'll have to make some decisions. Now the big question you face is whether you'll go into this year with last year's guidelines--or whether you're willing to put it all behind you and move forward into the new reality. 
 
This is the perfect weekend to review your retirement plan investment choices. And there's plenty of help for the process.  
 
Many companies have come to recognize that they have a responsibility to help their employees find intelligent, impartial, and unbiased advice on choosing investments within a 401(k) plan. 
 
They're aligning with online advice providers such as mPower and FinancialEngines--services that are also available at a moderate cost to individual investors. 
 
Check out www.Financial Engines.com. Started by economic theorist William Sharpe, who won the Nobel Prize for Modern Portfolio Theory, this site uses a sophisticated computer program to advise investors on asset allocation. 
 
It asks you to securely input personal information about income, age and goals, and then recommends allocation between your retirement plan investment choices. The cost is only $14.95 per quarter for one account, slightly more if you want to include non-retirement assets such as your IRA or trading account. 
 
Chicago-based Morningstar offers its version of this asset-allocation service at www.Morning star.com. Just click on ""401k Advice,"" and the site will walk you through the process of setting up your account. 
 
The cost is $7.95 for a three-month period. 
 
So now you have no excuse for not reviewing your investments. It's not wrong to be wrong. It's only wrong to stay wrong!  
 
And that's the Savage Truth. 
 
Terry Savage is a registered investment adviser for stocks and commodities and is on the board of directors of McDonald's Corp. and Pennzoil-Quaker State Co. Send questions via e-mail to savage@suntimes.com. Her third book, The Savage Truth on Money, recently was published by John Wiley &amp; Sons Inc.  

 






		
 


















 


				











 
 




  

  













 	



 








 
 


  
 suntimes.com:  Send feedback | Contact Us | About Us | Advertise With Us |  Media Kit |  Make Us Your Home Page  
Chicago Sun-Times: Subscribe | Customer Service
 | Reader Rewards |   Easy Pay |  e-paper | P.M. Edition |  Online Photo Store  
Affiliates:  jump2web |  RogerEbert.com |  
SearchChicago -  Autos | 
SearchChicago -  Homes | 
SearchChicago - Jobs | 
NeighborhoodCircle.com |  Centerstage    Partner: NBC5.com


   






 


 
















  
? Copyright 2007 Sun-Times News Group | Terms of Use and Privacy Policy   
 


Member of the Real Cities Network



 























 




