

t was about a year after moving
to Paris that I first heard the
question pop up from friends and
relatives back home: ""Are you fluent yet?""
 

 
 

 

 

Archive Photos





Overview
  � Virtual Tete-A-Tetes With Frenchmen, Sans Attitude
 
 
This Week's Articles
   � 'French for the Real World'
 
  � 'Learn to Speak French'
 
  � 'French Your Way 2.0'
 
  � 'Berlitz French'
 
 
 



 
  No, I would answer in the patient,
slightly smug tone that expatriates
use with their less fortunate cousins
in the New World. Fluency in French,
I explained (yawn), would very likely be long in coming.
 
  And in truth, it was longer in coming than I had imagined.
 
 As the years
passed, the question ""Are you fluent
yet?"" became the more ominous ""So
you're fluent, of course."" Only the
answer remained the same: No. Proficient -- sure. Fluent -- pas de tout.
 
  So forgive me if, back in the States
after three and a half years in
France, I still get a bit touchy on the
subject of fluency.
 
  That, however, is precisely what
many French language-instruction
CD-ROM's on the market seem to be
promising.
 
 (""The fast and effective
way to all-around fluency,"" says the
cover of the Berlitz French program
from the Learning Company.) It is
hard, though, to imagine becoming
truly adept at a foreign language
without without being immersed in a
setting where the language predominates -- and having a live teacher to
coax you through the rough spots and
force you to concentrate.
 
  Which is not to say that many of
the French language products are
not helpful. The CD-ROM technology,
far less static than a book or tape,
seems well suited to helping people
learn a language or, at least, speak it.
You can hear people speak words
and phrases and repeat them as
many times as you wish.
 
  That is especially important for
French since, for all the beauty of the
language, it can be daunting to understand. CD-ROM's can present a
wide variety of written and spoken
quizzes to reinforce learning, and
they let you find out instantly if you
have made a mistake. Some of the
programs let you connect to Web
sites for further study, and all the
programs reviewed here feature native French speakers, not the case in
many North American classrooms.
 
  Another feature unique to language CD-ROM's is the ability to let
students speak and, on the more sophisticated programs, to evaluate, to
some extent, their attempts to do so.
Several programs come with microphones, of varying quality, that can
be easily plugged into the computer.
The simpler language programs
have you repeat a word or phrase,
then play it back, letting you compare it with the words of the native
speaker. The more elaborate ones,
using increasingly sophisticated
voice-recognition technology, actually assess your efforts.
 
  Still, users should be aware that
some programs are rather, er, forgiving in their assessments of accent
and word choice -- much more so
than, say, a certain people for whom
French is the native language. Moreover, whatever the benefits of CD-ROM's for honing your speaking
skills and teaching some basic vocabulary, they seem less useful for
teaching the backbone of any language: grammar.
 
  And in French grammar, especially, mistakes there will be. We are, after all, talking about a country where
people take a perverse pleasure in
the pitfalls of their own language and
avidly follow on television and in
newspapers accounts of regular
events in which a reigning French literary figure has contestants --
adults, mind you -- sit together in
public and try to take down correctly
a complicated dictation.
 
 
 

