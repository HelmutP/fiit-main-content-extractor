


 By Ellis Mnyandu 


 NEW YORK (Reuters) - Volatility could be the name of the game on Wall Street this week as rising crude oil prices revive inflation fears and stock investors await congressional testimony from the Federal Reserve's chairma"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     Investing Home 
     News 
     Markets 
     Industries 
     Stocks 
     Funds 
     ETFs 
     Ideas &amp; Screening 
     
 
   1:11PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      Investing
      
      &gt;
      News
      
      &gt;
      Hot Stocks
      
      &gt;
      Article
 
 
 
       Oil, Bernanke to sway stocks 
       Sun Feb 11, 2007 12:11PM EST 
      
     

      
					 
						  Market View  
						 
							
								
									CSCO.O
								
								
									Last:
									
								
								
									Change:
									
								
								
									Revenue (ttm):
									
								
								
									EPS:
									
								
								
									Market Cap:
									
								
								
									Time:
									
								
								
									
										
									
								
							
							   
							 Stock Details 
							 Company Profile 
							 Analyst Research 
						 
					 
				 
			
 
	

  
    
  

 


 
     
       Company News 
     
     
       
UPDATE 1-RESEARCH ALERT-Baird raises Cisco to outperform
 
       
UPDATE 1-RESEARCH ALERT-BofA cuts Cisco to neutral
 
       
UPDATE 1-RESEARCH ALERT-Citigroup downgrades Cisco
 
       
UPDATE1-RESEARCH ALERT-Goldman adds Cisco to conviction buy list
 
       
Cisco chief may announce India manufacturing unit
 
       
 
 
			
      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Ellis Mnyandu 

    


 NEW YORK (Reuters) - Volatility could be the name of the game on Wall Street this week as rising crude oil prices revive inflation fears and stock investors await congressional testimony from the Federal Reserve's chairman. 

    


 Investors hope that Ben Bernanke will speak plainly about how he sees the outlook for U.S. interest rates in the months ahead after recent data showing the economy is strong. 

    


 The outcome of the weekend's Group of Seven finance ministers' meeting will get attention early this week as investors consider its impact, if any, on foreign-exchange rates. 

    


 Oil climbed back above $60 a barrel last week as OPEC member Nigeria reduced exports and tensions grew between the United States and Iran over Iran's nuclear program. Temperatures below freezing persisted in much of the United States and spurred demand for heating oil and natural gas. 

    


 Stock trading this week could prove choppy if oil prices rise much further, analysts said. 

    


 ""Crude and housing are the wild cards for this market,"" said Harry Clark, president and CEO of Clark Capital Management Group in Philadelphia. ""If crude goes above $60 to $65 a barrel, that could give the market a correction."" 

    


 For the past week, stocks fell despite a spurt of deal news, solid profits from bellwethers such as Cisco Systems Inc. (CSCO.O: Quote, Profile, Research) and a sparkling market debut by Fortress Investment Group LLC (FIG.N: Quote, Profile, Research). The Dow Jones industrial average (.DJI: Quote, Profile, Research) fell 0.57 percent, the Standard &amp; Poor's 500 index (.SPX: Quote, Profile, Research) slipped 0.71 percent and the Nasdaq Composite Index (.IXIC: Quote, Profile, Research) dropped 0.65 percent.  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


  

 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 
     
  Merger Talk  
 
  
 Backing Blackstone 
 Top U.S. office landlord Equity Office reaffirmed its support for a takeover offer from private equity firm Blackstone, despite a higher rival bid.  Full Article  
 
 Private equity team considers Sainsbury bid 
 Deals of the Day: Read the latest M&amp;A headlines 
 
 

 
   

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
			 
	 

	 
		 
                     
                    Industry News
                     
                     
				 
		
		 
				
						

				 
					Oil, Bernanke to sway stocks
12:11pm EST
				 
				 
		 
Stocks drop as more home loans go bad; Micron warns
 
		 
Brandes says to vote against Icahn Lear buyout
 
		 
				
					More Industry News...
			 
		 
		 

 

 

 
	 
    


 
 	 Top Company Searches on Reuters.com 
 

 
 
 1. AAPL
 
 2. SIRI
 
 3. GOOG
 
 4. GE
 
 5. MSFT
 
 
 

   6. CSCO
 
   7. INTC
 
   8. GS
 
   9. SI.N
 
 10. MOT
 
 
 

 

 
   



	 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















