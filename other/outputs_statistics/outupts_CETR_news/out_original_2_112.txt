

 "

 


Hartbeespoort Dam News








 
	 
		
			

			

		
		
			
			Hartbeespoort's First Online Newspaper
		
		
			
			
			
			Home
			
			
			
			
			This Weeks News
			
			
			
			
			Editorial
			
			
			
			
			
			
			
			
			
			
			
			
			
			Letters
			
			
			
			Services
			
			
			
			Contacts
		
	
 
 
	
		
			
			   
			   

			
			 
			Watch your title deeds, owners warned 
			 
			Landowners in especially the rural areas of 
			Hartbeespoort should ensure that their land ownership deeds are 
			still in their names. This warning follows the discovery by one 
			resident in Rietfontein that the land, in a trust for the family, 
			had been sold to a developer from another province and that the deed 
			had already been registered in his name without the family?s 
			knowledge at the beginning of this year.  
			The resident, who asked to remain anonymous due to ongoing court 
			proceedings, wants to warn others that if they are not careful, the 
			same could happen to them. She told Kormorant that the family 
			discovered something was amiss when the developer turned up at their 
			property earlier this year, saying that he just wanted a look at his 
			land. He also warned that they would have to move as he planned to 
			develop the property. 
			This set in motion an investigation which led to the discovery that 
			their family trust shares the same surname in the trust name as 
			another one in another province, although it does not have the same 
			number. The other trust turned out to be insolvent and apparently a 
			big retail bank was attaching the property in this trust, although 
			what followed was done to the property belonging to the Rietfontein 
			family. The resident said that a copy of the deed of their property 
			in Rietfontein was procured and that this was used to first put the 
			land up for sale on auction where the land was sold for R100 000 to 
			an elderly woman in November last year. On the same day of the 
			auction it was again sold to the developer for over R1 million.  
			Because the Rietfontein family?s trust does not have a bond on the 
			property, the original deed is at the office of their lawyer and the 
			resident said that at no time was anyone at their property - not to 
			evaluate the property or for an auction.  
			?All of this was done without as much as a signature from either 
			myself or the other trustees. We managed to trace the elderly woman 
			who supposedly bought the property at the auction for R100 000. She 
			said that the address on the sales contract was incorrect and that 
			she has never heard of a transaction such as was described. The 
			sales contract itself has pen marks of information being changed by 
			hand, including sales dates, and does not even have the appearance 
			of a valid contract,? the resident said. She questioned how property 
			held in trust can be sold so easily without the trustees? signatures 
			and without their lawyers knowing.  
			The family is now caught in a legal battle with them demanding that 
			the property be transferred back to the family trust and the 
			developer demanding his money back.  
			?This has been a very difficult time and all we want is to have the 
			property back that rightfully belongs to us,? the resident said.  
			According to her she knows of another local landowner who has been 
			affected in the sane way and that is why she wants to warn others to 
			be careful. ?With the area developing at the pace that it is, it 
			seems that some will resort to anything to get a hold of land to 
			develop,? she said.  
			 
			Back to Top                                                              
			Back to Home 

			
			 23 October 2008 

		
		
			
			

		
	
 





