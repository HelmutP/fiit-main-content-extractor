

 The phenomenon continues! An astonishingly big audience for a basic cable network -- 17.2 million viewers -- tuned into Friday's Disney Channel debut of ""High School Musical 2."" 
 


That should make it -- easily -- the most watched show on any network this week, when the full Nielsen ratings are released later today. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


The details, based on Nielsen's overnight and thus preliminary tote: 


• ""HSM 2"" now is the most-watched basic cable telecast of all time in total viewers (17.2 million), topping the previous record holder, ESPN's ""Monday Night Football"" game on Sept. 23, 2006 (16.0 million), by 8%. 
 


• It now ranks as the most-watched TV telecast ever for children 6-11 (6.1 million), and the most-watched entertainment telecast ever (behind only Super Bowl in 2004) in the age 9-14 demographic with 5.9 million. 
 


• It ranked as the No. 1 basic cable movie of all time in total viewers (17.2 million), towering above the previous record holder, TNT's ""Crossfire Trail"" on Jan. 21, 2001 (15.5 million), by almost 2 million viewers, or by 38%. 
 


John Smyntek 
 No hanging chads, but backyard voting mishap 
 


Five beautiful backyards. Which is the most beautiful of all? 
 


We need you to decide. 
 


After a few days of technical difficulties with our online voting system the Free Press is ready for your vote. 
 


Again. 
 


To keep things fair, we junked the bad files and started over on Saturday morning. 
 


The new voting system allows you to vote only once from any computer. So if you have voted since Saturday, consider your job done. 
 


Votes can be cast through Friday. The winner will receive a $100 gift certificate to a local garden center. 
 


To see photos of the finalist backyards and to vote, go to www.freep.com/backyards. 
 


Free Press staff 

