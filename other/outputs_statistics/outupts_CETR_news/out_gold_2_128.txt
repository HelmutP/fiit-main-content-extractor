

 "Newcastle 2-1 West Brom: Joey Barton penalty helps Joe Kinnear to first win
	
	
	 
 
	  
	  
	 
	
	 
	
	By Mirror.co.uk
	
	
	28/10/2008
	
	
	 
	
	 
 	 
	
	 
	 Joey Barton marked his first start since being released from jail with a goal as Newcastle finally ended their wait for a second Barclays Premier League win of the season.  
	 Barton converted a ninth-minute penalty and the Magpies looked to have secured Joe Kinnear's first victory as interim boss when Obafemi Martins headed home a second three minutes before the break.  
	 However, substitute Ishmael Miller pulled one back for the visitors with 25 minutes remaining, and the home side had to scrap all the way to the whistle for the points.  
	 Barton was handed his first start for Newcastle since leaving prison as Joe Kinnear looked for his first Barclays Premier League victory at the third attempt.  
	 Barton, who last appeared at St James' Park on May 5, was included in place of the injured Nicky Butt in one of three changes to the side which lost 2-1 at Sunderland on Saturday. There were returns too for Argentinian winger Jonas Gutierrez and Spanish full-back Jose Enrique, with Geremi and Sebastien Bassong making way.  
	 West Brom boss Tony Mowbray made just one change to the side beaten 3-0 at home by Hull as midfielder Chris Brunt replaced striker Ishmael Miller.  
	 The Magpies were led out by keeper Shay Given who was asked to wear the armband in the absence of the injured Butt and regular skipper Michael Owen.  Kinnear's men went close with just seconds gone when Obafemi Martins, Danny Guthrie and Jonas combined to allow Damien Duff to play Shola Ameobi in, although his shot was hacked away by Paul Robinson.  
	 The Magpies threatened again with barely two minutes gone when Ameobi flicked on Given's clearance to Martins, whose shot looped up off Ryan Donk and forced Scott Carson to make a smart save.  
	 West Brom managed to re-group after a torrid opening, but they fell behind with nine minutes gone after Barton slid the ball into Ameobi's feet and Donk felled him inside the box.  
	 Referee Mike Dean pointed straight to the spot and Barton grabbed the ball to send Carson the wrong way and score his first goal at St James' and just his second for the club.  
	 Borja Valero scuffed an 11th-minute effort wide as the Baggies tried to respond, but Barton smashed a long-range drive past the post four minutes later after Duff had seen his effort blocked following a mazy run.  
	 The visitors were passing the ball confidently with Newcastle repeatedly conceding possession, but they lacked the cutting edge to trouble the Magpies in the final third.  
	 Although Mowbray's side were enjoying more than their fair share of the ball, the Magpies continued to look more dangerous when they were going forward, with Jonas and Martins in particular causing problems.  
	 Baggies striker Roman Bednar got in on the left with 26 minutes gone, but his cross was cut out by Steven Taylor.  
	 Former Middlesbrough midfielder James Morrison picked up possession 30 yards from goal seconds later as the home side were once again forced back, but his ambitious effort flew high over.  
	 Newcastle threatened repeatedly down the right with Jonas and full-back Habib Beye linking well, and it was the defender who sent in a cross towards Ameobi in the 29th minute, although the striker could not keep his header down.  
	 However, the visitors would have been back on level terms 10 minutes before the break had Given not been at his best.  
	 
	 Advertisement - article continues below » 
	 
	

















	











	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	


	 
	 
	 Robert Koren caught the home defence square when he turned Jonathan Greening's pass into Morrison's path, but the Irishman won the one-to-one battle to preserve his side's lead.  
	 They might have taken full advantage within two minutes when Jonas and Ameobi carved open the visitors' defence, but the chance fell to Beye, who skied his shot.  
	 But the second goal finally arrived three minutes before half-time when Beye went past Paul Robinson and crossed for Martins to power home a header off defender Jonas Olsson. 
	 The home side returned knowing that a third goal would kill off the visitors, and set about the task of finding it from the off.  They might have created an opening with just a minute of the half gone when Martins, who had been played in down the left by Duff, rounded full-back Gianni Zuiverloon but saw his cross cut out by Donk with Danny Guthrie arriving at pace.  
	 But the visitors had by no means given up and Given had to watch a long-range effort from Brunt fly past his right post after Robinson had played his team-mate into space on the edge of the box.  
	 Martins' pace was a constant thorn in West Brom's side, but when he raced away from Olsson to cross with 51 minutes gone Donk calmly chested the ball back to Carson.  
	 Mowbray made his first change three minutes later when he withdrew Brunt and sent on Miller in an effort to give his side more of a goal threat.  
	 However, the newcomer unwittingly spared Newcastle within two minutes when Morrison's goal-bound shot hit him and ran to safety.  
	 Morrison lifted another effort on the turn high over the bar as the Baggies continued to press with the Magpies looking uncomfortable at the back.  
	 The former Boro winger turned provider on the hour when he fed Miller, who cut inside before sending a right-footed effort wide of the post.  
	 But the striker made no mistake five minutes later when the home defence was caught square once again.  
	 Koren's perfectly-weighted pass completed wrong-footed Fabricio Coloccini and Miller easily rounded Given before sliding the ball into the empty net.  
	 Kinnear moved to shore up his midfield when he replaced the tiring Jonas with Geremi with 20 minutes remaining.  
	 Miller might have levelled three minutes later when Morrison's free-kick rebounded to him off the defensive wall, but Given made another important block.  
	 Martins made way for Spaniard Xisco as Kinnear attempted to introduce some freshness, but Koren blazed an 81st-minute shot high over with West Brom making a final push.  
	 Duff had a chance to ease the nerves with two minutes remaining after Guthrie and Ameobi combined to set him up, but his left-foot shot was blocked by Valero."
"5936","
	
	By Mirror.co.uk
	
	
	28/10/2008
	
	
	 
	
	 
 	 
	
	 
	 Joey Barton marked his first start since being released from jail with a goal as Newcastle finally ended their wait for a second Barclays Premier League win of the season.  
	 Barton converted a ninth-minute penalty and the Magpies looked to have secured Joe Kinnear's first victory as interim boss when Obafemi Martins headed home a second three minutes before the break.  
	 However, substitute Ishmael Miller pulled one back for the visitors with 25 minutes remaining, and the home side had to scrap all the way to the whistle for the points.  
	 Barton was handed his first start for Newcastle since leaving prison as Joe Kinnear looked for his first Barclays Premier League victory at the third attempt.  
	 Barton, who last appeared at St James' Park on May 5, was included in place of the injured Nicky Butt in one of three changes to the side which lost 2-1 at Sunderland on Saturday. There were returns too for Argentinian winger Jonas Gutierrez and Spanish full-back Jose Enrique, with Geremi and Sebastien Bassong making way.  
	 West Brom boss Tony Mowbray made just one change to the side beaten 3-0 at home by Hull as midfielder Chris Brunt replaced striker Ishmael Miller.  
	 The Magpies were led out by keeper Shay Given who was asked to wear the armband in the absence of the injured Butt and regular skipper Michael Owen.  Kinnear's men went close with just seconds gone when Obafemi Martins, Danny Guthrie and Jonas combined to allow Damien Duff to play Shola Ameobi in, although his shot was hacked away by Paul Robinson.  
	 The Magpies threatened again with barely two minutes gone when Ameobi flicked on Given's clearance to Martins, whose shot looped up off Ryan Donk and forced Scott Carson to make a smart save.  
	 West Brom managed to re-group after a torrid opening, but they fell behind with nine minutes gone after Barton slid the ball into Ameobi's feet and Donk felled him inside the box.  
	 Referee Mike Dean pointed straight to the spot and Barton grabbed the ball to send Carson the wrong way and score his first goal at St James' and just his second for the club.  
	 Borja Valero scuffed an 11th-minute effort wide as the Baggies tried to respond, but Barton smashed a long-range drive past the post four minutes later after Duff had seen his effort blocked following a mazy run.  
	 The visitors were passing the ball confidently with Newcastle repeatedly conceding possession, but they lacked the cutting edge to trouble the Magpies in the final third.  
	 Although Mowbray's side were enjoying more than their fair share of the ball, the Magpies continued to look more dangerous when they were going forward, with Jonas and Martins in particular causing problems.  
	 Baggies striker Roman Bednar got in on the left with 26 minutes gone, but his cross was cut out by Steven Taylor.  
	 Former Middlesbrough midfielder James Morrison picked up possession 30 yards from goal seconds later as the home side were once again forced back, but his ambitious effort flew high over.  
	 Newcastle threatened repeatedly down the right with Jonas and full-back Habib Beye linking well, and it was the defender who sent in a cross towards Ameobi in the 29th minute, although the striker could not keep his header down.  
	 However, the visitors would have been back on level terms 10 minutes before the break had Given not been at his best.  
	 
	 Advertisement - article continues below » 
	 
	

















	











	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	


	 
	 
	 Robert Koren caught the home defence square when he turned Jonathan Greening's pass into Morrison's path, but the Irishman won the one-to-one battle to preserve his side's lead.  
	 They might have taken full advantage within two minutes when Jonas and Ameobi carved open the visitors' defence, but the chance fell to Beye, who skied his shot.  
	 But the second goal finally arrived three minutes before half-time when Beye went past Paul Robinson and crossed for Martins to power home a header off defender Jonas Olsson. 
	 The home side returned knowing that a third goal would kill off the visitors, and set about the task of finding it from the off.  They might have created an opening with just a minute of the half gone when Martins, who had been played in down the left by Duff, rounded full-back Gianni Zuiverloon but saw his cross cut out by Donk with Danny Guthrie arriving at pace.  
	 But the visitors had by no means given up and Given had to watch a long-range effort from Brunt fly past his right post after Robinson had played his team-mate into space on the edge of the box.  
	 Martins' pace was a constant thorn in West Brom's side, but when he raced away from Olsson to cross with 51 minutes gone Donk calmly chested the ball back to Carson.  
	 Mowbray made his first change three minutes later when he withdrew Brunt and sent on Miller in an effort to give his side more of a goal threat.  
	 However, the newcomer unwittingly spared Newcastle within two minutes when Morrison's goal-bound shot hit him and ran to safety.  
	 Morrison lifted another effort on the turn high over the bar as the Baggies continued to press with the Magpies looking uncomfortable at the back.  
	 The former Boro winger turned provider on the hour when he fed Miller, who cut inside before sending a right-footed effort wide of the post.  
	 But the striker made no mistake five minutes later when the home defence was caught square once again.  
	 Koren's perfectly-weighted pass completed wrong-footed Fabricio Coloccini and Miller easily rounded Given before sliding the ball into the empty net.  
	 Kinnear moved to shore up his midfield when he replaced the tiring Jonas with Geremi with 20 minutes remaining.  
	 Miller might have levelled three minutes later when Morrison's free-kick rebounded to him off the defensive wall, but Given made another important block.  
	 Martins made way for Spaniard Xisco as Kinnear attempted to introduce some freshness, but Koren blazed an 81st-minute shot high over with West Brom making a final push.  
	 Duff had a chance to ease the nerves with two minutes remaining after Guthrie and Ameobi combined to set him up, but his left-foot shot was blocked by Valero. 
 
 

