

 "McCain and Obama enter final week
            
              
            
            
              
            
            Tuesday, 28 October 2008 17:38
          
           
 
            
 With one week to go in the most hardfought presidential election campaign in US history, Barack Obama and John McCain are vowing to fight to the finish for every last vote. 
 The White House rivals are holding competing rallies in the rust-belt state of Pennsylvania before splitting, with Republican John McCain fighting a rearguard action in North Carolina and the Democratic candidate heading to Virginia. 
 Watch both Pennsylvania rallies 
 
 Advertisement   
 Despite holding a robust poll lead nationally and in battleground states, Barack Obama is warning against complacency as he prepares to air a costly 30-minute 'infomercial' on major US networks tomorrow. 
 'Don't believe for a second this election is over,' the Illinois senator said yesterday in Pittsburgh, whose withered steelworks are symptomatic of Pennsylvania's industrial blight. 
 'We can't let up. Not now. Not when so much is at stake,' he added in what aides called his 'closing argument' to voters. 
 Mr Obama's caution appears to be justified by a new poll which suggested the race was tightening. 
 The Zogby poll of 1,202 likely voters saw Mr Obama's support shrink nearly a point to 49%, while Mr McCain fell 0.4 points to 44.7%. The number of undecided voters rose to 6.3%. 
 For John McCain, Pennsylvania and its swollen ranks of disaffected, white, working-class voters is must-win territory on 4 November, along with historically Republican bastions such as North Carolina and Virginia. 
 The Arizona senator, 72, vied to reignite fears of 'socialism' by citing a 2001 radio interview given by Barack Obama where he appeared to lament the failure of the 1960s civil rights movement to bring about greater financial equality. 
 'That is what change means for Barack the Redistributor: It means taking your money and giving it to someone else,"" he told a rally in Dayton, Ohio. 
 The challenge facing Mr McCain was underlined by his choice this late in the game to head to North Carolina, which has not voted for a Democratic White House hopeful since 1976 but is now a raging battleground.  
 Virginia is an even deeper shade of Republican red, having last backed a Democrat for the presidency way back in 1964. But Barack Obama has a double-digit poll lead there, and is hoping the forecasts could portend a landslide in his favour. 
 Against the backdrop of an increasingly ugly election campaign, US justice authorities said that two white supremacists had been arrested for threatening to assassinate the Democrat during a 'killing spree' of more than 100 African-Americans. 
 Daniel Cowart, 20, and Paul Schlesselman, 18, were arrested last week in Tennessee for possession of firearms, threats against a presidential candidate and conspiring to rob a gun store. 

 

