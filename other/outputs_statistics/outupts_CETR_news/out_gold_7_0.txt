

 Skype has announced a new class of cordless phone that does not require a running computer. The VoIP firm, a unit of eBay, said Philips Consumer Products and Netgear will produce the phones.
 
 
Introduced at the IFA 2006 World of Consumer Electronics show in Berlin, Thursday, the Philips VOIP841 is scheduled to be available by the end of the year. Netgear's entry is expected to be available in early 2007.
 
 
""We are broadening our reach to mass consumers by offering them the opportunity to communicate via Skype without having to be tied to the computer,"" said Stefan Oberg, Skype's general manager for desktop and hardware.
 
 
The firm said the phones will have Skype software installed enabling them to be used to make and receive traditional phone calls through landline connections. The firm uses proprietary software and eschews standard SIP.
 
 
The Philips and Netgear phones come with remote DECT base stations that plug into the broadband connection and traditional phone lines. With more than 100 million registered users and a few million callers often online simultaneously, Skype is attempting to leverage its position as the de facto VoIP leader with a blizzard of product and service announcements.
 
 
Skype-to-Skype callers talk to each other without charge and calls from Skype users to non-Skype users on landlines in the United States and Canada are free for a limited period.
 
 
Prices for the Philips and Netgear phones were not available Thursday. However, phones not adhering to the SIP standard are likely to be more expensive than SIP-certified phones. 
 
 
""Skype is closed,"" said Matthew Rosen of Fusion Telecommunications, which markets a SIP-based VoIP service called efonica. There are already hundreds of SIP phones on the market.""
 
 
Rosen, who is Fusion's president and chief executive officer, said many inexpensive SIP phones like Cisco Systems' Linksys models have been on the market for months. SIP phones are cheaper because they utilize standard software, he noted. 

