

 "Meredith Kercher: Rudy Guede guilty of murder - now foxy Knoxy will stand trial
	
	
	 
 
	  
	  
	 
	
	 
	
	
	
	
	28/10/2008
	
	
	 
	
	 
 	 
	
	 
	 Rudy Guede was tonight found guilty of killing Meredith Kercher, while former lovers American Amanda Knox and Italian Raffaele Sollecito will stand trial for the murder. 
	 Ivorian Guede was sentenced to 30 years in prison by a judge in Perugia in Italy. 
	 All three were accused of killing the 21-year-old Leeds University exchange student in a violent sex game in the city last November and have spent nearly a year behind bars awaiting today's decisions. 
	 The semi-naked body of Miss Kercher was found in her room in the cottage she shared with Knox and others on her year abroad. 
	 She had died of stab wounds after her throat was slit. 
	 Knox, 21, from Seattle - nicknamed Foxy Knoxy - and her former lover Sollecito, 24, are accused of killing Miss Kercher in a bungled sex game with Guede.  Lawyer Francesco Maresca, who represents Miss Kercher's family, said: ""We are very satisfied, even though this was a young man who faces a very heavy sentence."" 
	 The trial is set to begin on December 4. 
	 The key piece of evidence used by prosecutors to link Sollecito to the crime is a trace of his DNA on the clasp of Miss Kercher's bra. 
	 
	 Advertisement - article continues below » 
	 
	

















	











	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	


	 
	 
	 His lawyers argued this was due to accidental contamination during the investigation. 
	 They have also said that if Sollecito had removed the victim's bra there would be more traces of his DNA on it and they would not only be on the clasp. 
	 Prosecutor Manuela Comodi contested this yesterday by bringing her own bra into court to demonstrate how Miss Kercher's could have been removed, according to reports in the Italian press. 
	 Knox's parents Edda Mellas and Curt Knox were in Perugia while their daughter's fate was decided behind closed doors. 
	 Miss Kercher's family were also in the Umbrian hilltop town to hear the judge's decision. 
	 Miss Kercher's family is seeking 25 million euro (£19.3 million) in compensation for the ""grave tragedy"" they have suffered, their lawyer said last week.  While the claim currently applies only to Guede, Francesco Maresca said he would make a similar request if Knox and Sollecito were brought to trial. 
	 But Mr Maresca stressed the amount applied to all three cases collectively, calling it a ""symbolic request"". 
 

