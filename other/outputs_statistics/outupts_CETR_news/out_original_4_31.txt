









 

 














 




 
 
  
  







  


 April 8, 1999 
 

 LIBRARY / TITANIC CD-ROM'S
 


 The Titanic's Mystique in Digital Packages 



 By RICK ARCHBOLD 

hy does the sinking of the
R.M.S. Titanic on the night
of April 14, 1912, still rank
as the disaster to end all disasters?
Worse calamities have come and
gone while the Titanic continues to
hold sway. James Cameron's ""Titanic"" was only the latest in a long
line of movie treatments of the subject going back to ""Saved From the
Titanic,"" released a mere month after the disaster and starring a Titanic survivor, Dorothy Gibson.
 
 
 

 
 

 

 

Associated Press





Overview
  � The Titanic's Mystique in Digital Packages
 
 
This Week's Articles
   � Total Titanic: A Night to Remember 
 
  � James Cameron's Titanic ExplorerTotal Titanic: A Night to Remember
 
 
 



There have been so many books
about the Titanic and the people who
built her and sailed on her that even
students of the subject have lost
count. It has been said that the Titanic stands third in the English
words-in-print sweepstakes, after
the Bible and the Civil War.
 
  Of course, the Titanic story gained
currency with the discovery of the
wreck on Sept. 1, 1985, by a joint
French-American expedition led by
Robert Ballard and Jean-Louis Michel. Suddenly the famous ship was
among us. If we couldn't touch her,
we could see her, looking remarkably well, thank you very much, despite all those years away and all the
inevitable decay.
 
  But the story's enduring popularity cannot be explained away by the
discovery of the wreck. The most
common significance given to the
Titanic is technological: that the disaster was an example of technology
overreaching itself. Maybe that angle helps explain why the Titanic
story should have such mass appeal
as we near the new millennium. Like
the late Edwardians, we have placed
an enormous amount of faith in our
ability to transcend nature. Our Titanic will not be a luxury liner but
something quite different: a massive
power blackout or some other failure
of highly centralized energy- or communications-delivery systems. The
current Year 2000 mania has roots in
the same psychological soil as the
fascination with the Titanic shipwreck.
 
  I would argue that the real keys to
the Titanic's staying power are related to when the sinking happened and
how we heard about it. The disasters
we remember, that become part of
popular culture, all take place at a
pregnant historical moment, usually
as an era is coming to a close. The
Titanic sank as the Edwardian age
was drawing to a close. The Hindenburg, which probably ranks second
in the modern disaster popularity
contest, burned in 1937, just before
the outbreak of World War II.
 
  The other aspect of the technological importance of these disasters
has to do with mass communication.
Each of these 20th-century disasters
represents a sort of communications
first. The Titanic was the first major
disaster that people learned about
through the miracle of the wireless
radio.
 
 (Before wireless, a ship could
sink without anyone noticing until it
failed to show up at its port of destination.) The Hindenburg was the
first great catastrophe that people
saw in moving pictures.
 
 But the
Titanic is the first great disaster of
the electronic age. And still the
greatest.
 
 
 
 
 
Here are a few Titanic Web sites that feature more than just pictures 
of Leonardo DiCaprio: 
 
  
ENCYCLOPEDIA TITANICA:  www.rmplc.co.uk/eduweb/sites/phind
 A comprehensive site with passenger lists, ship plans and video. 
 
 
TITANIC HISTORICAL SOCIETY:  www.titanic1.org
 Features selections from the society's journal, photos and trivia.
 
  
TITANIC: www.titanicmovie.com
 This official site for the film has some footage of dives to the wreck  by 
James Cameron, the director.   
 
  
GEORGE BEHE'S TITANIC TIDBITS: ourworld.compuserve.com/homepages/Carpathia
 Created by a past official of the Titanic Historical Society, this site covers obscure information like the music played by the ship's band. 
 
  
TITANIC'S LOST SISTER www.pbs.org/wgbh/nova/titanic
 A site on the Titanic's sister ship Britannic, which was sunk  in November 1916.
 
    
 
 
 Rick Archbold is the co-author with 
Dana McCauley of ""Last Dinner on 
the Titanic: Menus and Recipes 
From the Great Liner."" He just put 
up the www.lastdinner.com Web site.
 
 
 
 
  
 
  
 

 






 
 
  
  




 
 


Home |
Site Index |
Site Search |
Forums |
Archives |
Marketplace
  
Quick News |
Page One Plus |
International |
National/N.Y. |
Business |
Technology |
Science |
Sports |
Weather |
Editorial |
Op-Ed |
Arts |
Automobiles |
Books |
Diversions |
Job Market |
Real Estate |
Travel
 
 
Help/Feedback |
Classifieds |
Services |
New York Today
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















