

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
STOCK SCAM LOSSES MAY EXCEED $100M | By RODDY BOYD | Business News | Financial | Business and Money
























 
  
 
  
  













				  

 
 
  
 
 

 
     
	 	
	
 

		 
		NYC Weather 83 ° MOSTLY CLEAR 		 
		
		
     
    
     
	         
    
     
		 
		Saturday, August 25, 2007  
		Last Update: 
		11:40 PM EDT 
		 
         
         
         
                 
              
         


			Recent
			30 days+
			The Web
			

      
 




       
            
            
			 
				Story Index
				Summer of '77
			 
			
            
            
           
            
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Make Us Rich
            Movies
            Travel
            NYP Home Page
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
	Business Home
	Columnists
	NYP Home
	Business News
	Stock Quotes
		Sunday Business
 


 
     
    
        
       
          
             
                STOCK SCAM LOSSES MAY EXCEED $100M     
                
             
             By RODDY BOYD 
	
		

	
		 
	                 
		 
			
		  		
		 
			Loading new images...
		 
	
		 
			
		 
	      	 

	
              

 
             

            
            December 16, 2006 -- The victims of a rogue Long Island financial adviser accused of pilfering more than $100 million went to court yesterday trying to prevent lenders from foreclosing on loans they say they were duped into getting.  
  Peter Dawson, a high-flying Huntington-based financial adviser, allegedly got access to millions of dollars from the pensions and mortgages of middle-class retirees from New York and Florida by promising huge returns with little risk.  
  But what they got instead, according to the Securities and Exchange Commission and an amended civil suit filed in federal court yesterday, were empty bank accounts and a growing pile of foreclosure notices.  
  Starting in the late 1990s, Dawson used connections among retirees, longtime friends and various Long Island church groups to enlist clients at his BMG Advisory firm.  
  According to Jacob Zamansky, who sued him three weeks ago, Dawson convinced clients to take out second mortgages on their homes, which were long paid off.  
  Dawson had the home-equity loan checks sent directly to him with the promise that he would generate returns above their interest costs.  
  Particularly vulnerable were older veterans of the armed forces who were wooed by Dawson's tales of his days as an ultra-elite Navy SEAL.  
  However, a source within the Navy said that there is no record of Dawson's service. Former employees of Dawson, as well as longtime neighbors, laughed at the notion.  
  "It's kind of hard to be in the [SEALs] when you are 300 pounds," said a former neighbor and bilked investor whose wife grew up across the street from Dawson. "I had no idea it was this bad," said Zamansky. "Old people, veterans, middle-income people are getting foreclosures. They were tricked into this - the guy didn't even invest the money."  
  Zamansky's amended complaint claims Dawson bilked $100 million from his alleged victims and seeks $100 million in damages.  
  In court yesterday, the Securities and Exchange Commission, asked for a restraining order against the assets of Dawson and his wife Lisa.  
  A source close to the investigation said the Nassau County District Attorney's office has concluded that much of the missing $50 million to $100 million is not in Dawson's accounts.  
  Dawson has been holed up in his house recovering from an early November suicide attempt, said this source.  
  Dawson's lawyer did not return a call seeking comment. 
 
                        
             
 
  

 
          
       
       
          


 
 WINNERS AND LOSERS 
 SEC HITS MONEY MANAGER DAWSON WITH FRAUD CHARGE 
 SCAM-BLED EGGS 
 SQUAB RULE  
 

 
 'RUNWAY' TO START LICENSING 
 LIKE A HOUSE OF CARDS 
 STARWOOD'S 'GRAND' PLAN 
 MORE 
 
           
             
            	
          
       
     

 
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
 
 
sign insubscribeprivacy policyBUSINESS HEADLINES FROM OUR PARTNERSrss 
 
 
 
  

 

 NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007NYP Holdings, Inc. All rights reserved. 
 

 


