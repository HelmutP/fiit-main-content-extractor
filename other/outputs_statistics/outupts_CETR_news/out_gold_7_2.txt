

 Hitachi Global Storage Technologies on Wednesday unveiled the largest enterprise hard drive yet, a 300GB unit tagged with the moniker of Ultrastar 10K300.
 
 
 
 
The company's touting the 10,000 revolutions per minute (rpm) drive as the biggest, brawniest drive yet for mission-critical applications such as online transaction processing, media streaming, and data analysis.
 
 
 
 
When they ship in volume during the second quarter, the drives will be available in both Ultra 320 SCSI and 2Gbps Fibre Channel interfaces.
 
 
 
 
The one-inch thick, 3.5-inch format drives will be available in smaller sizes than the top-end 300GB -- including 36-, 73-, and 146-gigabyte models.
 
 
 
 
Although some units are already shipping, Hitachi said that the Ultrastar was in the process of being certified by various original equipment manufacturers (OEMs), who will integrate the larger hard drive into their storage servers. 

