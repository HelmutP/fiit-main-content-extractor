

 "//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd""&gt;

 








        
            Borthwick cast in the leading role  -
            International, Rugby Union - The Independent

    
    
    
    
    
    
    
    
































	 

 
	 
      
     
         Skip Links 
         
             Skip to navigation 
             Skip to primary content 
             Skip to secondary content 
             Skip to tertiary content 
             Skip to footer 
         
     


     
	
		
			Brand, the BBC and bullying: Why is cruelty cool, asks Terence Blacker
	
 

 
         
                 Rugby Union 
                 
         
			
				
			
			3°
			
				
					London
				Hi 10°C / Lo 2°C
			
			 
	 
	
		
			Search
			
				Query:
				
					
			
			
			
			
			
			Go
			
		
	
 
 
 

 

    
        
            


                 Headlines 
             
                 
                    Terence Blacker 
                            When did bullying become acceptable? |
                        
                        
                        
                     
             
		
            

	
		
		Click here...
	


 
        
    

 



 
 Navigation 
 
 
News
 
 
Opinion
 
 
Environment
 
 
Sport
 
 
Athletics
 
 
Cricket
 
 
Football
 
 Premier League 
 News &amp; Comment 
 Football League 
 FA &amp; League Cups 
 International 
 European 
 Scottish 
 Transfers 
 
 
 
Golf
 
 
Motor Racing
 
 
Olympics
 
 
Racing
 
 
Rugby Union
 
 News &amp; Comment 
 UK Clubs 
 International 
 European Clubs 
 
 
 
Rugby League
 
 
Sailing
 
 
Tennis
 
 
Others
 
 
 
 
Life &amp; Style
 
 
Arts &amp; Entertainment
 
 
Travel
 
 
Money
 
 
IndyBest
 
 
Student
 
 
Offers
 
 from The Independent &amp; The Independent on Sunday 
 
 



 
 
    Home &gt; Sport
 &gt; Rugby
 &gt; Rugby Union
 &gt; International
 
 

 
		 
			

 
     Borthwick cast in the leading role  
	

 
            By Chris Hewett 
            Tuesday, 28 October 2008
         
     
                    
                                    
                                 
                     GETTY IMAGES 
                     Steve Borthwick was yesterday appointed England captain  
                      
                                     
                                        
                                         enlarge
                                     
                                 
                     



 
	 

		 
			 


			
					 Print
				
			 
			 
			
					 Email
				
			
 
		 
	 

		
			Search
			Search
			
			
			
			
			
			Go 
			 Independent.co.uk
			 Web
		
	

	 
		 Bookmark &amp; Share 
		 
			 
Digg It
 
			 del.icio.us 
       Facebook 
			 
Reddit
 
		 
		 What are these? 
	 
 
 
         
             
 Steve Borthwick left Bath for Saracens last summer ""in search of a new challenge"" – an explanation of questionable value, rather like the line routinely used by politicians about spending more time with the family. He certainly has a challenge now, though, the 29-year-old line-out specialist from Cumbria was appointed England captain yesterday by the new manager Martin Johnson, and as a result, he has been burdened with the heavy responsibility of doing for this red rose vintage what Johnson himself did for the World Cup-winning one of 2003. 

            
        
             
"I won't be giving Steve a specific amount of time in the job," 
  Johnson said. "I just want to support him as much as possible. He is a 
  good leader who prepares thoroughly for all his matches both at club and 
  international level; I know he will enjoy the work that comes with being 
  captain of England."
 
 
That latter point was something of a no-brainer, for Borthwick has long been 
  renowned as a model professional who dedicates every waking breath, not to 
  mention most of his sleeping ones, to making the best of himself. Danny 
  Grewcock, who partnered him at Bath, once described him as the 
  hardest-working lock he had ever encountered, and Johnson clearly sees 
  something of this in his man. 
 
 
However, Borthwick will have to be at his very best in terms of form, as well 
  as preparation, if England are to make progress between now and the first 
  week of December, when the draw for the 2011 World Cup in New Zealand will 
  be made on the basis of International Rugby Board rankings. They have four 
  Tests at Twickenham. The first, against a hotchpotch Pacific Islands team, 
  is eminently winnable, the second, against an Australian side, prospering 
  under the controversial new laws, will be awkward; the last two, against the 
  world champion Springboks and the Grand Slam hunting All Blacks come under 
  the "profoundly difficult" heading. Yet they will suffer some 
  sharp criticism if they lose more then once, especially in light of the 
  Rugby Football Union's despicable treatment of Johnson predecessor Brian 
  Ashton.
 
 
Johnson could not have placed his trust in a more ambitious individual. He 
  knew this when he asked Borthwick to lead the thankless two-match tour of 
  New Zealand last June – a tour the manager missed because his wife was 
  expecting – and nothing has happened in the interim to alter his view. It 
  was hardly the best England trip in history, what with heavy defeats, in 
  Auckland and Christchurch and local police aiming accusations of sexual 
  misconduct at a number of players, but the captain played a thoroughly 
  rotten hand as well as it was possible to play it, acting with considerable 
  dignity in desperate circumstances. Even had there been serious alternative 
  candidates for the captaincy, Borthwick earned Johnson's loyalty on that 
  tour. As it was, candidates were thin on the ground. Phil Vickery, appointed 
  by Ashton early in 2007, performed magnificently through the World Cup in 
  France and kept going manfully through a fraught Six Nations Championship 
  but his bruised front-rower's body is now screaming for rest. Two of his 
  colleagues in the Wasps pack, the young flankers James Haskell and Tom Rees, 
  had been touted as possible "bolt from the blue" options – shades 
  of Geoff Cooke's decision to appoint a youthful Will Carling to the role in 
  the late 1980s – but Haskell is struggling for form and Rees has enough on 
  his plate establishing himself as a world-class breakaway capable of mixing 
  it with the likes of Richie McCaw and George Smith, the outstanding 
  Antipodeans heading his way next month.
 
 
Ultimately Borthwick was the only sensible choice. If he fails, it will not be 
  for the want of trying.
 
 
Captains' log England's leaders since 2003
 
 
*Lawrence Dallaglio, Winter 2003: Unhappy second spell before the first of his 
  retirements.
 
 
*Jonny Wilkinson, Autumn 2004: Andy Robinson's first choice as captain. As 
  usual with Wilkinson, injury got in the way.
 
 
* Jason Robinson, Autumn 2004: the rugby league convert was no-one's idea of a 
  natural union captain and he ended up struggling.
 
 
* Martin Corry, Spring 2005: Robinson turned to him almost in desperation and 
  was rewarded.
 
 
* Phil Vickery, Spring 2007: After taking over as coach, Brian Ashton wanted a 
  captain of his own and picked the right mind.
 


        
         Interesting? Click here to explore further 
        

        
        
    
  

	
             
     Next Article In International 
 

        
         Print Article
     
     

         Email Article
        
     
     
     
     
    	
	
	Click here for copyright permissions
	
	 
	Copyright 2008 Independent News and Media Limited
     
    
 
 
     Also in this section 
     
         Gatland takes on the Southern Hemisphere 
             Borthwick named England skipper 
             Borthwick named England captain 
             The Woodward blueprint for international success 
             
 
 

 
	 
	 
	 Advertiser Links 
 
 
 
 	 
		        
	
	 
	        	 		   	 
		          
		   
	 
 
 
 
 

 


 
 

		
		 

			 
            
            	 
     EDITOR'S CHOICE 
     
         
         
                
                        
                                 
                Britain's swans stay in 'warm' Siberia
             
         
                
                        
                                 
                The Ten Best Alarm Clocks
             
         
                
                        
                                 
                Win exclusive X Factor tickets
             
         
                
                        
                                 
                Lawton: Keane idle amid mob rule
             
         
                
                        
                                 
                Our guide to being the perfect patient
             
         
                
                        
                                 
                Egypt blog lifts veil on match- making
             
         
                
                        
                                 
                Down's: A mother's right to choose
             
         
                
                        
                                 
                Unwanted and unloved but Alonso is on fire
             
         
                
                        
                                 
                Southern invasion? A fight at the opera
             
         
                
                        
                                 
                Mountain marathons: Danger IS the point
             
         
                
                        
                                 
                The real home of Antiguan cricket
             
         
                
                        
                                 
                Leicester: the theatre with no boundaries
             
         
                
                        
                                 
                The standup comics who don't tell jokes
             
         
     
 

 

		
			

	
		
		Click here...
	


			
		
	 

 

 
	
		
		Click here...
	


 

 



 

 
 
	
	 
		 ON THIS SITE 
		 
 
 Dating 
 Independent Offers 
 
 	 
 


 

 
 
	 
		 Advertiser Links 
		 
			   	   	   			
		            
		                   		 
		 
	 
 


 
 


			


 

	 

	 
					 
						
							
								Search
								
									Query:
									
								
								 Independent.co.uk
								 The Web
								
								
								
								
								Go
								
							
						
					 


					 
						 
							©independent.co.uk
							Legal Terms &amp; Policies
							 | 
E-mail sign-up
							 | 
RSS
							 | 
Contact us
							 | 
Syndication
							 | 
Work for INM
							 | 
Advertising Guide
							 | 
Group Sites
							 | 
London Careers
							 
					 
				 
			 











    



        
        
        





