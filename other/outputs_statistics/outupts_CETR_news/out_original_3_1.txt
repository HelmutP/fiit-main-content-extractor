

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
THE MONKEY KING |  | Online Resources For Classroom Teachers | Newspaper Activities | Newspapers


























 
     
    
       
       
          
             	
                            
             
               NYC Weather 84 ° SUNNY              
          
          
             
          
          
             
               Saturday, August 25, 2007 
               
                   Last Update: 
		06:15 PM EDT                   
               
             
             
             
             
                     
             


			Recent
			30 days+
			The Web
			

           
       

       
            
           
			 
				Story Index
				Summer of '77
			 
			
            
            
            
            
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Movies
            Make Us Rich
            Real Estate
            Travel
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
            Intern Stories
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
   News Home
   Columnists
   Local
   National
   International
   Weird But True
   NYPD Blotter
	   Traffic &amp; Transit
   Lottery
 

	 
		Hopstop
		MTA Advisories
	 
	      
       
           
          
              
             
               
                               
                   
                      THE MONKEY KING     
                      CHAPTER SEVENTEEN: WHAT HAPPENED TO MONKEY KING 
                   
                   

					

                         
	                 
		 
			
		  		
		 
			Loading new images...
		 
		 

	
              
				  
				  
				   

 
                  
				  
				   

                                    July 27, 2007 -- Adopted From the Classic Chinese Tale by Ji-li Jiang 
 Illustrated by Hui Hui Su-Kennedy 
 STORY SO FAR: Unrepentant, indestructible, and still a troublemaker, Monkey is given one chance to prove himself by the Great Buddha. 
 BUDDHA looked at Monkey and softly said, ?You say you can jump. If you can jump out of the palm of my hand in one somersault, I shall let you rule Heaven. But if you fail, you must accept my punishment. Do you agree??  
 Monkey could hardly believe his ears. ?I just told him I could jump one hundred and eight thousand leagues!? he said to himself. ?Perhaps Buddha is hard of hearing. He must be foolish indeed to make such a bet with me.? 
 ?If I do what you ask, will you keep your promise?? Monkey asked out loud.  
 ?Oh yes, we have many witnesses here.?  
 ?And this is an honest deal?? Monkey demanded.  
 ?As honest as I can be,? returned Buddha with a smile. ?Do you agree??  
 ?Of course I agree,? Monkey replied with a laugh. ?Any fool would.?  
 Calmly, Buddha held out his hand. Monkey climbed into his palm. He stretched his legs and arms and wiggled all his fingers and toes. Then, just as he had done so many years ago at the great waterfall, he crouched down, closed his eyes, took a deep breath and leaped with all his might.  
 After a long moment he landed on his feet, quite certain he had gone at least one hundred and eight thousand leagues away from where he had started.  
 After all, in front of him was an unfamiliar landscape, a boundless flat plain with five pillars soaring straight into the sky. Monkey walked about, looking for landmarks, but he couldn?t find anything telling him where he was.  
 ?Oh well,? he said, ?I must have jumped even farther than I thought! This must be the end of the world. But I suppose it would be a good idea to make a mark to prove I came so far.?  
 So Monkey plucked a hair from his chest and cried, ?Alalalatola!?  
 The hair became a paintbrush. Monkey dipped the brush in ink, approached one of the pillars, and wrote, ?Magnificent Monkey King was here.?  
 Then he cloudsoared back to Heaven, very pleased that he had won his bet with Buddha.  
 
VIEW FULL ARTICLE &gt;PAGE 1 2 CONTINUE READING &gt;                                     

             
             
                


 
 THE VILLAGE IN THE PARK 
 THE WONDERFUL WORLD OF ANTS 
 WHAT'S UP IN THE SKY 
 MORE 
 
                 
 

 
                              			     
                 
 The online center for youth volunteer opportunities in NYC.
     
				 
					






 
	 
	    
		 
		  			 DATING 
		  			 JOBS 
		  			 AUTOS 
		  			 REAL ESTATE 
		  			
		 
	 
	     
 				 
             
          
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
           
            sign in
            subscribeprivacy policyNEWS HEADLINES FROM OUR PARTNERSrss 
          
       
    
     
    
       NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007 NYP Holdings, Inc. All rights reserved. 
  	   
 


