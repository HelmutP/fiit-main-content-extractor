






 




 














 




 
 
  
  







  


 February 11, 1999 
 



 Tuneups for the Young Brain: Try This at Home 

 
 
 


  By MARGOT SLADE 

oing what comes naturally is
how many parents define
thinking, a kind of hard-wired
inborn process applied like a lens to
different subjects to bring them into
focus. In this way, we learn. But the
reality, educators have been discovering, is that thinking can be taught
and, indeed, must be taught.
 
 
 

 
 

 

 

Arthur Tilley/FPG





 
Overview
  � Tuneups for the Young Brain: Try This at Home
 
 
This Week's Reviews
   � Thinking Things Collection 3
 
  � Putt-Putt Enters the Race
 
  � Dr. Brain
 
  � Blue's Birthday Adventure
  
 


 ""Thinking is a kind of overlord of
academia,"" said Arthur Pober, an
educational psychologist who conducts workshops nationally on thinking skills. ""What many grown-ups
don't understand is that learning
math and reading, social studies and
science, music, woodworking, block-building or art doesn't teach youngsters how to think. Just the reverse.""
 
  Learning to think ""should be a
distinct component of the school curriculum and a distinct activity at
home,"" Dr. Pober said. He added,
""Children can then use those skills to
better understand other subjects.""
 
  Educators essentially agree that
thinking comprises a cluster of elements: memory; cognition, or learning; decision making, and problem
solving. Learning includes the ability
to understand information -- whether it is a letter, word, sentence or
concept, presented visually or aurally -- and to explain it to others. And
problem solving means being able to
move from Step A to B to C -- and
knowing how to be more creative,
solving problems when the original
plan does not work.
 
  In recent years, specialists in early childhood education and software
developers have recognized that the
computer is perfectly configured to
teach such things. Learning a skill
requires practice, reinforcement,
repetition and reintroduction in different forms and settings so that
children thoroughly understand and
master it before they lose interest. A
computer is good at all of those
things, and it doesn't get angry, can't
get impatient and never gets bored.
 
  Equipped with the right software,
the computer will give feedback in
the form of hints, praise and suggested alternatives. It will offer children
repeated opportunities to acquire,
practice and improve a skill. And it
will do so using a variety of settings
and variable levels of difficulty.
 
  How can parents determine whether a particular product is right for a
child? They cannot always thumb
through a brochure and do not necessarily have access to a software
company's Web site; even if they do,
the site may not offer a glimpse of
the software in question. Sometimes
the only guidance, then, is the box.
 
  The descriptive material should
tell parents the age group for which
the product was designed. It should
also describe the activity and the
types of skills being developed, educators say. Make sure the rating is
appropriate, if it has one. Check the
graphics: youngsters today have sophisticated taste in animation.
 
  Seek out software that offers children the opportunity to acquire and
practice skills in various ways and
allows parents or children to adjust
the level of play. But also look for
products, like those reviewed here,
that are fun and that develop thinking skills using familiar characters
from books or television or present
comfortable, homelike settings.
 
  There is one catch. No software
will be ""right"" if parents abandon
their children to it. Parents must be
present at the computer, said Dr.
Pober, who is the executive director
of the Entertainment Software Rating Board and devised the product
rating system used by the video-game and interactive software industries along with a new rating system for the Internet.
 
  ""Parents should at least be offering encouragement to their kids,"" he
said. ""But they will also want to
observe how their children learn,
what their children are responding to
so that they can create follow-up
games and activities the whole family can pursue."" It is parents, he
added, who must gauge a child's
mood and call it a day before fun
turns into a lesson in frustration.
 
 
   Exercising thinking skills should be a hands-on activity -- for parents. 
Here are some ways in which they can assist even older children without doing the thinking for them.  
 
 
 
 
Be realistic.Start at a game level that is comfortable. It can be tricky 
to lure frustrated youngsters back to a game.
 
 
Offer a hand. Younger children may not be physically capable of  clicking, dragging and dropping without help.
 
 
Don't give away the answer. Ask questions that hint at other  strategies.
 
 
Try some warm-ups. If children are unable to perform a certain activity, try to devise other activities  -- simple memory, measuring or sorting games, for example -- that will help them exercise the requisite 
skills.
 
 
Always ask why. If children seem to take a random approach to problem solving, ask how they arrived at a solution, prompting them to 
think about the way they think.
 
 
 

 
 
  
 

 






 
 
  
  




 
 


Home |
Site Index |
Site Search |
Forums |
Archives |
Marketplace
  
Quick News |
Page One Plus |
International |
National/N.Y. |
Business |
Technology |
Science |
Sports |
Weather |
Editorial |
Op-Ed |
Arts |
Automobiles |
Books |
Diversions |
Job Market |
Real Estate |
Travel
 
 
Help/Feedback |
Classifieds |
Services |
New York Today
 

 
Copyright 1999 The New York Times Company
 
 
 

 






