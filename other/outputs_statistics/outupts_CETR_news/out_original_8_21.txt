

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;



 
    
    
    
    Laddering can provide desired stability -- chicagotribune.com



    
     www.chicagotribune.com/business/yourmoney/sns-yourmoney-0415cruz,0,1780299.story 
    
     chicagotribune.com 
    
    
        THE SAVINGS GAME 
    
    
     Laddering can provide desired stability 
    
    
    
    
    
    
       
            By Humberto Cruz 
       
    
       
            Tribune Media Services columnist 
       
    
    
    
          April 15, 2007 
    
    
    
    
     
         
        
 
         It'll soon be decision time for my wife, Georgina, and me. Four short-term certificates of deposit totaling 10 percent of our fixed-income portfolio will come due during the next four months. 
 
The fact that we have CDs has surprised some readers who expect a financial columnist to seek investments with the potential for higher returns. And Georgina and I do, with a significant chunk of our portfolio in low-cost, broadly diversified stock mutual funds. 
 
But we also diversify with fixed-income investments and are pleased with how our short-term CDs, all for 13 months or less and yielding between 5.2 percent and 5.8 percent a year, have stabilized our portfolio while providing dependable income. 
 
We plan to replace the maturing CDs with longer-term fixed-income investments, either other CDs and/or investment-grade bonds with staggered maturities from 2013 through 2017. 
 
We already own fixed-income investments coming due every year from 2008 through 2012. 
 
While our strategy is not for everyone, the subject is timely given the subprime mortgage problems roiling the credit markets, the mixed signals about the economy and the billions of dollars coming due in short-term CDs. For most of us, the basic reason for fixed-income investments is not to chase yields or speculate on the direction of interest rates but to smooth out the volatility of a portfolio and/or create a stable and dependable source of cash flow. 
 
For that latter purpose, it's hard to beat ""laddering,"" the strategy of investing in an assortment of bonds (Bank CDs are just one form of bonds.) with different maturities. 
 
With laddering, if interest rates fall, bonds that mature soon will have to be reinvested at a lower rate. But the other bonds in the portfolio would continue to pay above-market rates. If interest rates rise, the total portfolio might pay a below-market return, but the maturing bonds can be reinvested at higher rates. 
 
""The point is to create a stable source of cash you can always rely on,"" said John Radtke, executive director of Incapital LLC, an underwriter and distributor of fixed-income securities based in Chicago and Ft. Lauderdale. 
 
In addition, ""having a small amount in cash or short-term CDs is probably not a bad idea"" now, Radtke said, considering their yields approach, match or sometimes even exceed those of longer-term high-quality bonds. With close to $100 billion in short-term CDs coming due this year in the United States, many banks are offering special rates on short-term certificates to entice customers to renew. 
 
If all your money is in short-term deposits, however, you run the risk that interest rates will drop by the time they mature. 
""If concerns about the housing industry persist, the likelihood of the Fed cutting rates is significant,"" Radtke said, a move that would lead banks to lower the interest rate on CDs. 
 
To counter that risk, Radtke suggests rounding off your fixed-income portfolio with intermediate- and longer-term bonds that are rated investment grade. The latter point is significant. If capital preservation and reliable income are your goals, you want to stay away from riskier high-yield or ""junk"" bonds, even if they tempt you with juicy yields. 
 
How many bonds to buy and how far to extend maturities will depend on individual circumstances, including how much money you have to invest, your age and need for cash flow. You also need to be aware of whether a bond can be ""called,"" or redeemed by the issuer before maturity. ""You want to customize the cash flow as much as possible,"" so you receive income when you need it, Radtke said. 
 
Humberto Cruz is a columnist for Tribune Media Services. E-mail him at yourmoney@tribune.com. 
     
    
     Copyright © 2007, Tribune Media Services 






