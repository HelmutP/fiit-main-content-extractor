

 Wanted: No. 3 hitter: Sheffield's injury may require deal
				
				
				 
 August 25, 2007 
				
				 BY JON PAUL MOROSI 
				
 FREE PRESS SPORTS WRITER 

				 


For months, Tigers manager Jim Leyland had one easy decision among the myriad he makes every day: In the third spot on his lineup card, he wrote Gary Sheffield's name. 
 


Now, Sheffield is out indefinitely with an injured right shoulder. Sean Casey hit third on Wednesday and Thursday, and Marcus Thames was scheduled to bat there in Friday's rain-delayed game against Roger Clemens. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


""We've got to find somebody to hit third,"" Leyland said. 


Leyland would like Sheffield to be that someone, but it's not clear when he will return. Tigers president/general manager Dave Dombrowski said Thursday that Sheffield could be back by the middle of next week. 
 


If the absence continues, though, Leyland acknowledged that it's possible the Tigers will acquire a hitter to fill in for Sheffield. 
 


The club has declined to provide specifics about Sheffield's condition, but Leyland said the results of a magnetic resonance imaging exam earlier this week were ""no different"" from earlier findings. 
 


Sheffield, 38, said earlier this month that his collarbone shifted out of place following a July 21 outfield collision with second baseman Placido Polanco, and that fluid collected in the AC joint at the top of his shoulder as a result. The Tigers had the best record in baseball (57-37) at the time of the collision, and are 11-22 since. 
 


If Detroit decides to trade for a hitter, the club would probably look to acquire someone from a noncontending team who (a) is not signed beyond this year, (b) has pennant race or postseason experience, and (c) could provide power and/or patience in a lineup that could use more of both. 
 


Toronto veteran Matt Stairs, whom the Tigers claimed on waivers last September, was blocked on waivers this year and is therefore unavailable via trade. San Francisco slugger Barry Bonds has cleared waivers, but the Tigers do not have interest in him. 
 


WATCHFUL EYE: Leyland said reliever Joel Zumaya would not pitch Friday night, after taking the loss (and throwing 30 pitches) Thursday afternoon. 
 


""I'm watching him close,"" Leyland said. ""His arm strength's not totally there yet, but we knew that. You don't know that his arm strength is going to be there for the rest of the year. 
 


""It might be. It might not. I'll still take my chances with him."" 
 


Zumaya said his right arm felt healthy but ""a little tender"" on Friday, after making his second appearance since missing more than three months after finger surgery. 
 


""I have it in me,"" Zumaya said, when asked about his renowned arm strength. ""I still have it in me. It's just three months, man. 
 


""The first time I picked up a baseball, my arm was just not the same. It was weak. It's coming along. I can feel it coming along, but to me it's just like the beginning of spring training."" 
 


DOWN TIME: After learning of his demotion to Triple-A Toledo on Thursday, reliever Zach Miner told the Free Press, ""I'm upset and disappointed, and I don't agree with it. ... even though I'm gonna respect it."" 
 


Leyland said Miner, who went 2-3 with a 3.86 ERA in 24 big-league games this year, must ""make some adjustments"" to reach his potential. 
 


""He's done as well as some other guys who are here, but it wasn't like he was setting the world on fire,"" Leyland said. ""I really like him. His stuff's good enough. But he's got to learn how to do some things."" 
 


When asked to elaborate, Leyland said: ""He's got to go after hitters more, use his pitches, and have more confidence in his sinker."" 
 


NOTEBOOK: Leyland, on Ramon Santiago's defensive ability: ""It's one of those things where, if you want to play him at shortstop on a more regular basis, then you've got to have more production elsewhere. But he can play shortstop."" 

