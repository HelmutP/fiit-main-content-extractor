









 




 

 









 


 







  


 August 19, 1999 
 

 LIBRARY / SCREENWRITING SOFTWARE
 


 Small-Screen Help for Budding Filmmakers 



 By  ERIC A. TAUB 

    omputer programs to help 
screenwriters have been 
around for more than a decade, since writers began dropping 
the I.B.M. Selectric typewriter in favor of the Apple Macintosh. But in 
the last few years, they have come to 
include not only  simple aides but also 
complex systems offering, like the 
hyperbolic movie industry itself, 
bloated promises that you'll soon be 
writing a blockbuster with  ""passionate themes"" and ""unforgettable 
characters.""
 
 
 
 

 
 

 

 

Michael O'Neill





Overview
  � Small-Screen Help for Budding Filmmakers
 
 
This Week's Articles
   � Writepro
 
  � Plots Unlimited
 
  � Dramatica Pro
 
  � John Truby's Blockbuster
 
 
 



Of all writers, those who create 
film and television scripts strike out 
the most. Scripts often sell for the 
wrong reasons: The writer is a 
friend of a producer. The writer has 
persuaded a star to participate. 
Projects that are universally  disliked are suddenly loved once money 
is committed.
 
   Screenwriters know that  even if 
their films are produced, most of 
what they write may never be heard 
or even read, buried under  a mountain of rewrites or hidden in a sea of 
descriptive action meant  to capture 
the imagination of one of the scores 
of people who will have to sign off on 
the sale.
 
  Unless you're part of the tiny fraternity of screenwriters who  can sell 
a script based on a two-sentence 
idea, you must create a story that a 
reader -- whether a low-level employee, studio executive, agent or 
star --  immediately finds compelling. You must quickly show  that the 
characters are complex and fresh, 
the situation unique and intriguing, 
the dialogue biting and germane.
 
  If you're looking for the equivalent 
of a digital dishwasher, a program 
into  which you can dump all your 
words and receive a clean, finished 
script, don't  waste your time. Writing programs do not evaluate  dialogue and  narrative. Anyone who has 
used a grammar checking tool in a 
word processing  program knows 
that until computers become as intuitive as HAL, it's an  impossible goal.
 
  Rather, all these programs try  to 
help the writer think more critically. 
Through varying  approaches, each 
tries to give  rules for character development, conflict,  plot and resolution, while  the user  enters information or answers  multiple-choice questions about the intended story. Some 
programs can be  mastered in a few 
minutes; others require  days.
 
  If you insist on  software that features the latest in computer graphics, choose  your program carefully. 
Several packages remain mired in a 
1980's style, using fixed, primitive 
screen layouts and built on inflexible 
databases that tend to crash.
 
     Has a screenwriting program, like 
a word processor, become a de rigueur tool for the budding filmmaker? Of course not. In fact, some of 
these programs seem to exist on disk 
for no other reason than that they 
can: a book version of the same material would often  suffice. However, 
if you find that some constant, critical prodding helps you think more 
rigorously, then each of the four programs  examined here is worth a 
look. All are available through the 
Writers Store in Los Angeles
(writersstore.com).
 
  

  
 
Related Sites 
  These sites are not part of The New York Times on the Web, and The Times has no control over their content or availability.   
 
 
 
   The Writers' Store  
 
 
  

 
 
  
  
 

 






 
 


Home | 
Site Index | 
Site Search  | 
Forums  | 
Archives | 
Marketplace
  
Quick News  | 
Page One Plus  | 
International  | 
National/N.Y.   | 
Business   | 
Technology  | 
Science   | 
Sports   | 
Weather  | 
Editorial    | 
Op-Ed | 
Arts   | 
Automobiles   | 
Books  | 
Diversions   | 
Job Market   | 
Real Estate  | 
Travel 
 
 
Help/Feedback  | 
Classifieds   | 
Services   | 
New York Today 
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















