

 Microsoft on Thursday unveiled a new low-end personal finance program that it's selling only electronically. It marks the first time Microsoft has used a download-only distribution model for its consumer software.
 
 
Priced at $19.99, Money Essentials includes fundamental finance tools that focus on online banking, including ones that automatically categorize expenses and generate e-bill reminders.
 
 
Microsoft also launched 2007 editions of the other SKUs in its Money family, ranging from the $49.99 Money 2007 Deluxe to the $89.99 Money 2007 Home &amp; Business.
 
 
Money Essentials, which runs on Windows XP SP2, can be  downloaded from the Microsoft Web site. The other editions can be downloaded or purchased at retail. 

