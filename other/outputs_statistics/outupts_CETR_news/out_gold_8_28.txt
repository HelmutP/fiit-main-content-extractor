
 Gov. George Ryan on Wednesday pardoned the four men exonerated by DNA evidence of the 1986 slaying and rape of Rush University medical student Lori Roscetti. 
 
""The governor, in the interest of fairness and justice, is pardoning these men who were wrongfully convicted. It's the right thing to do,"" said Ryan spokesman, Dennis Culloton. The signed pardons will be filed on Thursday, Culloton said. 
 
The pardons came just two days short of the 16th anniversary of Roscetti's slaying. The four men--Omar Saunders, Marcellius Bradford, Larry Ollins, and his cousin, Calvin--were convicted of the Oct. 18, 1986, killing after Chicago police said Bradford and Calvin Ollins confessed. 
 
Bradford pleaded guilty in return for a reduced sentence of 12 years, and the three others were sentenced to life in prison. Following disclosure of new evidence uncovered by the Tribune in May 2001 and the DNA tests that showed none of the four were involved, Cook County prosecutors vacated the convictions. 
 
Saunders and Larry and Calvin Ollins were released from prison last December after serving nearly 15 years behind bars. 
 
Less than two months later, two other men, Eddie Harris and Duane Roach, were charged with Roscetti's murder. DNA tests linked them to evidence left on Roscetti's clothing and both gave videotaped statements implicating themselves, authorities said. 
 
""If it weren't for the [Tribune's] reporting and the DNA tests, these men would never have gone free and the real killers would not have been caught,"" Culloton said. 
 
Notified of the pardon, Calvin Ollins, 30, who was 14 when he was arrested, said: ""Getting a piece of paper like this, it's one of the most important things. It proves to anyone that we were innocent of the case. To have it on paper--you can't argue with that."" 
 
Defense lawyer Kathleen Zellner, who had sought the DNA tests that exonerated the four men, filed clemency petitions for the four earlier this year. The request for pardons was not opposed by the Cook County state's attorney's office. 
 
""We're very grateful for Gov. Ryan for the expeditious handling of the clemency petition and the innocence of these men,"" Zellner said. ""The case stands as a painful reminder of all of the changes that need to be made within the Chicago Police Department. These four lives have been permanently damaged and altered because of the misconduct, and the Chicago police and no amount of money will ever be able to rectify the injustice done to these people or the years taken away from their lives."" 
 
Zellner said Ryan had pledged to help engineer legislation before the end of the year to provide funds so the four men can file with the Illinois Court of Claims, which compensates people wrongfully convicted. Under the law they are entitled to about $140,000 each. 
 
""The timing is this,"" Culloton said. ""We have to allow men who have suffered an injustice to get before the Court of Claims as soon as possible. That may require getting some funds from the General Assembly."" 
 
""This will help us go beyond our means in society,"" Calvin Ollins said. ""Nobody will hold anything against us. 
 
""This is something that we've been waiting for, we've been expecting. We're not going to get too carried away--this is something that will help us out over a short period of time until we deal with some bigger issues."" 
 
Ollins was referring to a lawsuit filed by Zellner against Chicago police, Cook County prosecutors and Chicago Police Crime Laboratory personnel that seeks damages for the wrongful convictions. 
 
""We're looking forward to basing our futures on that,"" Ollins said. 
 
Men rebuilding lives 
 
Ollins is taking courses at the College of DuPage and studying to be a firefighter, a career he had dreamed of as a youngster. ""It requires a certain frame of mind that I'm not accustomed to--going to school, and homework, and doing so much reading,"" he said. ""But I'm learning."" 
 
Efforts to reach Larry Ollins, Bradford and Saunders were unsuccessful. 
 
Zellner said Larry Ollins, 32, has started a day-care business with a girlfriend. Saunders, 33, is taking business courses at the College of DuPage, and Bradford has gone back to school as well. 
 
""They're doing okay,"" Zellner said. 
 
Roscetti, a native of Springfield, was last seen driving to her apartment near Racine Avenue and Lexington Street on the West Side about 1 a.m. on the day she was killed. Four hours later, a Chicago &amp; North Western Railway officer found her body on the access road that stretched from Loomis Street to Ashland Avenue between 16th Street and 15th Place, in the shadow of the ABLA Homes public housing development. 
 
Three months later, early on Jan. 28, 1987, Chicago police brought Bradford and Larry Ollins in for questioning. More than 15 hours later, police said Bradford confessed, implicating himself and Larry Ollins, as well as Calvin Ollins, who lived in Cabrini-Green. 
 
The motive, according to police, was robbery so Calvin Ollins could get bus fare home. Calvin Ollins was brought in for questioning, and several hours later the learning-disabled teen also confessed, police said. 
 
DNA reopens case 
 
The Roscetti case came under scrutiny in January 2001 after Zellner filed in court a report written by DNA expert Ed Blake, who described prosecution testimony by Pamela Fish, a Chicago police crime lab analyst, as ""scientific fraud."" 
 
Fish had testified that semen taken from Roscetti's body could have come from three of the defendants. But a series of new DNA tests, conducted at independent labs, showed semen on Roscetti's clothing and on a vaginal swab and hair fragments recovered from the scene were not from any of the four. 
 
A Tribune investigation published May 2, 2001, detailed how the case against the four was evaporating even before the initial DNA test results were known. Prosecutors had based much of their case on the confessions of Bradford and Calvin Ollins. 
 
Bradford told the Tribune he falsely confessed under police coercion to avoid a life sentence. Calvin Ollins said he confessed because police promised to let him go in return. 
 
Chicago police have denied the confessions were coerced. 
 
Other key witnesses recanted critical testimony in interviews with the Tribune. One said he testified falsely to try to cash in on a $35,000 reward; another said he testified for the prosecution to avoid being implicated. 
 
Harris and Roach are awaiting trial for the Roscetti slaying. In their statements, they said they were looking for a car to burglarize when they happened upon Roscetti returning from a night of studying. 
 
Prosecutors have said they will seek the death penalty for Roach and Harris. 
