

 "Worker accused of murdering manager 
 Lynn Shaw HERALD REPORTER 
 A FORMER worker at Trident Steel, who allegedly stabbed and killed his manager after a disciplinary hearing, appeared in the Port Elizabeth High Court yesterday. 
 Siyabonga Myosana, 29, of Motherwell, is accused of the murder of Alan du Preez on November 2 last year. He is also charged with assault on two other workers on the same day. 
  Myosana, Du Preez and other employees were in the boardroom for the outcome of an internal hearing against Myosana when the stabbing took place. It is alleged Myosana was asked to sign a document to confirm the outcome of the hearing. 
 According to the charge sheet, the accused then pulled out a knife and stabbed Du Preez in the neck. Myosana then made his way to the front entrance, but a security guard had locked the premises gates. 
 He was arrested when police arrived on the scene. It is alleged Myosana had threatened to kill Du Preez and other employees if he lost his job. 
  Thembela Luphondo, a worker who said he witnessed the stabbing, testified that Myosana was found guilty during the hearing. 
 ?He (Myosana) then got up and stabbed the deceased in the neck with a knife. 
 ?Everyone started running. He did not say anything before he stabbed the deceased and he was not provoked in any manner,? said Luphondo. 
 The trial resumes today."
"8045","Lynn Shaw HERALD REPORTER 
 A FORMER worker at Trident Steel, who allegedly stabbed and killed his manager after a disciplinary hearing, appeared in the Port Elizabeth High Court yesterday. 
 Siyabonga Myosana, 29, of Motherwell, is accused of the murder of Alan du Preez on November 2 last year. He is also charged with assault on two other workers on the same day. 
  Myosana, Du Preez and other employees were in the boardroom for the outcome of an internal hearing against Myosana when the stabbing took place. It is alleged Myosana was asked to sign a document to confirm the outcome of the hearing. 
 According to the charge sheet, the accused then pulled out a knife and stabbed Du Preez in the neck. Myosana then made his way to the front entrance, but a security guard had locked the premises gates. 
 He was arrested when police arrived on the scene. It is alleged Myosana had threatened to kill Du Preez and other employees if he lost his job. 
  Thembela Luphondo, a worker who said he witnessed the stabbing, testified that Myosana was found guilty during the hearing. 
 ?He (Myosana) then got up and stabbed the deceased in the neck with a knife. 
 ?Everyone started running. He did not say anything before he stabbed the deceased and he was not provoked in any manner,? said Luphondo. 
 The trial resumes today. 

