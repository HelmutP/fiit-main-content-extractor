


 KABUL (Reuters) - A U.S. soldier died of a gunshot wound on Sunday in the normally safe northern Afghan province of Balkh, which borders Uzbekistan, the U.S. military said in a statement. 


 No details were released other than to say the dea"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   12:59PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      U.S.
      
      &gt;
      Article
 
 
 
       U.S. soldier dies of gunshot wounds in Afghanistan 
       Sun Feb 11, 2007 9:40AM EST 
      
     

     
 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		U.S. News
     
   
   
		 
				Bernanke to say economy sound, inflation a risk

			 
		 
				House Iraq debate limited to troop increase: Hoyer

			 
		 
				Maine lesbian ""adoptee"" case tests legal bounds

			 
		 
				Obama sees new generation of leadership
  |  Video

			 
		 More U.S. News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     KABUL (Reuters) - A U.S. soldier died of a gunshot wound on Sunday in the normally safe northern Afghan province of Balkh, which borders Uzbekistan, the U.S. military said in a statement. 

    


 No details were released other than to say the death was being investigated by the U.S. military authorities. Combat deaths are rare in the province, the capital of which is the relatively prosperous city of Mazar-i-Sharif, near the Uzbek border. 

    


 Afghanistan is bracing for a major bout of spring fighting when the snows melt in a few months. Last year was the bloodiest since a U.S.-led invasion ousted the Taliban in 2001. 

    


 More than 4,000 people were killed in fighting in 2006, a quarter of them civilians and more than 170 of them foreign soldiers. 

    



    

       
       
       
 

       © Reuters 2007. All rights reserved.
       

   
			 
		
				Next Article:

			U.S. delays F-22 Raptor fighters arrival in Japan
 
			   
		 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				 
U.S.-led forces show evidence of Iranian arms in Iraq
 
				 BAGHDAD (Reuters) - U.S.-led forces in Iraq presented on Sunday what officials said was ""a growing body"" of evidence of Iranian weapons being used to kill their soldiers, as U.S. anger at Tehran's alleged involvement in the war rises. 
                
                    Full Article  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















