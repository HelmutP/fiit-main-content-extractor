

 Tamil Tiger air raid hits army camp
                
            
        
         

            
                
                
        
        
        
            
                
                    
                        
                            
                                 
 







The attack by the Tamil Tigers is said to have hit a power station [Reuters]






                            
                        
                    
                
                
                    
                     Fighters from the Liberation Tigers of Tamil Eelam (LTTE) have carried out an air raid against a Sri Lankan army camp in the northwest of the country, the defence ministry has said. 
 ""Two bombs were dropped by LTTE aircraft over a military camp,"" Brigadier Udaya Nanayakkara, an army spokesman, told the AFP news agency on Tuesday. 
                    
                
                
                    
                     There were no immediate details about damages or casualties, he said. 
 ""There was a loud noise coming from near the army camp,"" a resident in Mannar district told AFP by telephone. 
 ""The military was also firing anti-aircraft guns."" 
                    
                     The LTTE, or Tamil Tigers, last carried out an air and ground attack on a military camp in the northern town of Vavuniya in September, killing 11 soldiers. 
 Ten Tiger suicide commandos also died in that attack.  Minelle Fernandez, reporting for Al Jazeera from the Sri Lankan capital Colombo, said the raid hit an oil storage facility but that ""the authorities have yet to confirm the exact location"".  She said that the raid would seem like a coup for the Tigers, coming  after the military said they had reined in the fighters. 
 ""[It is] an indication that the rebels are still able to come in and hit at the heart of Colombo,"" she said.  Czech-made weapons     The LTTE is believed to have smuggled five Czech-built Zlin-143 aircraft on to the island in pieces, from which point they were re-assembled.  
 Satellite images have shown that they have more than one air-strip inside the areas they control. 
 The Tigers' air raid came as government forces stepped up their own air operations against the guerillas inside their de facto mini-state in the north. 
 The military has claimed the upper hand in the latest ground battles. 
 Sri Lankan troops are battling to capture the separatists' political capital of Kilinochchi in the north. 
 Troops are about 10 to 15km southwest of the town, according to Sri Lankan army maps. 
 Tens of thousands of people have died since the LTTE launched a separatist campaign in 1972 for a homeland for minority Tamils in the majority Sinhalese island's north and east. 



