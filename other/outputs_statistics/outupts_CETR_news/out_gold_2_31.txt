

 "Canadian films confront death

 
 

The Barbarian Invasions | The Event | My Life Without Me


 By Peter T. Chattaway

 
 ALAS, technical problems and a crowded schedule prevented me from posting regular updates during the Vancouver film festival like I had hoped. But now that the festival has come and gone -- and now that some of the films showcased there are opening in regular theatres -- it is possible to comment on some of the themes that emerged there.

 
 If one trend dominated Canadian films, it was death -- more specifically, terminal illness and the questionable moral choices that some characters make as they face their impending demises.

 
  This theme was there right from the opening gala, which featured a screening of The Barbarian Invasions (opens November 21), Denys Arcand's sequel to his Oscar-nominated 1986 classic The Decline of the American Empire. In the new film, R?my (R?my Girard), the serial adulterer of the earlier film, is now divorced and estranged from his children, but his friends and family reunite when he finds himself confined to a hospital bed with just a few months left to live.

  Christians may find the new film especially intriguing because it also brings back not one, but two, characters from Arcand's other widely praised film from the bygone past, namely 1989's Jesus of Montreal. But more on that in a moment.

 
 The key thing to remember about Arcand is that he was a historian before he became a filmmaker; he began his career in the movies by making documentaries for the National Film Board, one of which, on Quebec founding father Samuel de Champlain, got him into trouble for its alleged revisionism. Arcand has brought a historical perspective to his films ever since; in Comfort and Indifference, his documentary on the 1980 referendum on Quebec sovereignty, an actor dressed as Machiavelli and recited lines from The Prince, to show how the federal Canadian government used every trick in the book to keep its power over that province.

 
 This concern for historical context helps to explain why The Decline of the American Empire, a comedy about sex and upper-middle-class decadence, has the title that it does. Several of the film's characters are professors who have lost their faith in political causes, and the film begins with one historian telling a reporter that moral depravity is usually a sign that an empire is in decay; thus, there is a serious social point to be made beneath all the philandering depicted in the film.

 
 This is also the cultural backdrop against which Arcand's next Oscar-nominated film, about a troupe of actors who put on a revisionist passion play based on the 'historical Jesus', needs to be seen. Jesus of Montreal is about the search for meaning in a world where political ideology and crass hedonism have both led to a dead end. And, since Arcand had already identified modern America with ancient Rome in Decline, he now asked if an actor playing Jesus on the fringe (Quebec) of the fringe (Canada) of the American empire might be able to point us to some deeper meaning, the way that Jesus, a man on the fringe (Galilee) of the fringe (Palestine) of the Roman empire, once did.

 
 Daniel Coulombe (Lothaire Bluteau), the boat-rocking protagonist in Jesus of Montreal, died at the end of that film, partly because of the appalling conditions in a public hospital. And to judge from The Barbarian Invasions, things have gotten even worse in the intervening years -- during the opening credits, the camera follows a nun down a crowded corridor as she passes numerous patients and staff, wires dangling from the ceiling as technicians fumble about. The whole scenario reeks of the so-called Third World.

 
 And then we discover something surprising. The nun, it turns out, is Sister Constance Lazure (Johanne-Marie Tremblay) -- the very same woman who worked in a soup kitchen before joining the other actors in Jesus of Montreal. Constance appears in only a few scenes in The Barbarian Invasions, but her presence adds an interesting spiritual element to a story that is mostly about the despair with which a selfish man faces the end of his life.

 
 In her conversations with R?my, Constance asserts that there must be a God who can forgive the abominable crimes we commit, that embracing the mystery of life is what can save us, and so on. If any other actress had played this role, I might have thought Arcand -- whose films can be notoriously sarcastic at times -- was belittling the character's naivete. But there is nothing condescending in Arcand's treatment of Constance, and the fact that she represents a link to Jesus of Montreal, perhaps the least cynical and most compassionate film Arcand ever made, suggests that he respects her, even if he cannot quite agree with her.

 
 Throughout the film, Arcand juxtaposes personal mortality with other kinds of mortality, such as the death of nations and ideas. In one scene, an academic on TV compares the September 11 terrorist attacks to the invasion of barbarians that brought down the Roman empire -- no longer are the outsiders being held off in Germany or wherever, but now they have struck the centre of power itself. When R?my's son S?bastien (St?phane Rousseau) goes looking for heroin to ease the old man's pain, a narcotics officer makes a similar comment about dealers ""invading"" Canada from other countries. And so the empire is still in decline. 

 
 This is where the second Jesus of Montreal cameo comes up. In one brief scene that has little to do with the film's plot but resonates strongly with its themes, a priest takes Sebastien's girlfriend to an underground warehouse, where a lot of religious art is sitting in a state of decay, unwanted and unused. The priest laments the death of religion in Quebec, and he asks if the church can sell any of this stuff off -- and the priest in question is Father Leclerc (Gilles Pelletier), the compromised, jaded cleric who commissioned the ill-fated passion play in the earlier film.

 
 These are fascinating parallels, and they make an old favorite even more interesting than it already was. As for the death theme in general, The Barbarian Invasions is not a very hopeful film -- it is filled with sadness, confusion and regret -- but it comes by its grief honestly.

 
 The same could not be said for The Event (now playing), Maritime director Thom Fitzgerald's manipulative and sentimental tribute to euthanasia. The film stars Parker Posey as a New York cop who investigates the death of an AIDS victim (Don McKellar) who went out with a bang by throwing a party for his friends and family the night that he planned to kill himself.

  Fitzgerald, who made his debut six years ago with The Hanging Garden, an awkwardly structured film about a gay Nova Scotian who revisits the place where he attempted (and possibly committed) suicide when he was a boy, has certainly improved as a director. The Event, which also alternates between past and present, has a more natural flow than the earlier film and is also quite entertaining, despite its serious subject matter. It also helps that Fitzgerald has a talented cast at his disposal, including Olympia Dukakis as McKellar's mother and Sarah Polley (who played a similar role in The Hanging Garden) as the gay man's understanding sister.

 
 But the fact that the film knows exactly how to push our buttons -- audience members sniffled throughout the movie and stayed reverently in their seats as the final credits rolled, which is unusual for a fast-paced film festival -- just makes it all the more offensive that his film is such a blatant work of propaganda. Fitzgerald tut-tuts every single character who would dare to question the righteousness of suicide, and the film goes on and on about how much love it took for the gay man's friends to kill him at his request.

 
 And of course, there is the usual patina of religiosity to make it all acceptable -- like when the gay man's mother says things like, ""Sometimes the only way to protect a defenseless child is to let him go back to God."" 

 
 What makes the film even more disturbing is the way it rubs our noses in the idea that, as the cop's own aging mother puts it, ""Getting old sucks."" And when the cop asks if her now-deceased father ever expressed interest in killing himself during his last, bed-ridden days, the mother replies, ""If only he'd asked."" AIDS victims, in other words, are not the only people we should consider killing out of the goodness of our hearts. You cannot help but wonder what goes through an old actress's mind when she is given such lines, especially in service of a film that goes out of its way to romanticize death.

 
 After sitting through that film, it was almost a relief to see My Life Without Me (opens November 7), in which it is Sarah Polley's turn to play the terminally ill one. Produced by Pedro Almodovar and directed by Isabel Coixet, yet filmed in Vancouver with an international cast, this film has one of the more peculiar and eclectic collections of talent that I've seen in a while.

  Polley plays Anne, a 23-year-old woman who lives in a trailer in her mother's backyard with her loving husband (Underworld's Scott Speedman) and her two cute daughters, and who makes ends meet by cleaning blackboards at a university. Despite being so young, Anne discovers she has just a few months left to live, so she makes a checklist of things to do before she dies -- including visiting her father (Frida's Alfred Molina) in prison and getting another man (You Can Count on Me's Mark Ruffalo) to fall in love with her, just to see what it feels like -- and she keeps her condition a secret from her husband, children, and mother (Blondie's Deborah Harry) even as she tries to manipulate things so that they can all have a good life without her. (The film also marks a re-union of sorts for Pulp Fiction co-stars Amanda Plummer, who plays Anne's food- and diet-obsessed co-worker, and Maria de Medeiros, who plays a Milli Vanilli-obsessed hairdresser.) 

 
 My Life Without Me is more subtle and playful and thoughtful and inviting than The Event -- which may not be saying much -- and it's a better film overall, but once again, the film is about a person who responds to news of her own imminent demise by acting selfishly. What's more, Anne does not allow those who love her to become part of this moment in her life, but she deliberately keeps them out. The film treats death as an opportunity for self-actualization, not as an opportunity to share one another's burdens in truly loving relationships.

 
 In addition, Anne's voyage of self-discovery may benefit from a cultural double-standard -- would the filmmakers go as easy on a married man who seduced a woman behind his wife's back, knowing that this other woman's heart would be broken when he left her or died a few months from now, just because he had only a few months to live? Somehow I doubt it.

 
 One film that I unfortunately did not get to see, but which I imagine would perfectly fit alongside these other films, is Allan King's documentary Dying at Grace, which captures the last days and moments of five people at a Salvation Army palliative care unit in Toronto. By all accounts, it sounds like a haunting, intimate film, so it's probably worth keeping an eye open for it."
"2288","By Peter T. Chattaway

 
 ALAS, technical problems and a crowded schedule prevented me from posting regular updates during the Vancouver film festival like I had hoped. But now that the festival has come and gone -- and now that some of the films showcased there are opening in regular theatres -- it is possible to comment on some of the themes that emerged there.

 
 If one trend dominated Canadian films, it was death -- more specifically, terminal illness and the questionable moral choices that some characters make as they face their impending demises.

 
  This theme was there right from the opening gala, which featured a screening of The Barbarian Invasions (opens November 21), Denys Arcand's sequel to his Oscar-nominated 1986 classic The Decline of the American Empire. In the new film, R?my (R?my Girard), the serial adulterer of the earlier film, is now divorced and estranged from his children, but his friends and family reunite when he finds himself confined to a hospital bed with just a few months left to live.

  Christians may find the new film especially intriguing because it also brings back not one, but two, characters from Arcand's other widely praised film from the bygone past, namely 1989's Jesus of Montreal. But more on that in a moment.

 
 The key thing to remember about Arcand is that he was a historian before he became a filmmaker; he began his career in the movies by making documentaries for the National Film Board, one of which, on Quebec founding father Samuel de Champlain, got him into trouble for its alleged revisionism. Arcand has brought a historical perspective to his films ever since; in Comfort and Indifference, his documentary on the 1980 referendum on Quebec sovereignty, an actor dressed as Machiavelli and recited lines from The Prince, to show how the federal Canadian government used every trick in the book to keep its power over that province.

 
 This concern for historical context helps to explain why The Decline of the American Empire, a comedy about sex and upper-middle-class decadence, has the title that it does. Several of the film's characters are professors who have lost their faith in political causes, and the film begins with one historian telling a reporter that moral depravity is usually a sign that an empire is in decay; thus, there is a serious social point to be made beneath all the philandering depicted in the film.

 
 This is also the cultural backdrop against which Arcand's next Oscar-nominated film, about a troupe of actors who put on a revisionist passion play based on the 'historical Jesus', needs to be seen. Jesus of Montreal is about the search for meaning in a world where political ideology and crass hedonism have both led to a dead end. And, since Arcand had already identified modern America with ancient Rome in Decline, he now asked if an actor playing Jesus on the fringe (Quebec) of the fringe (Canada) of the American empire might be able to point us to some deeper meaning, the way that Jesus, a man on the fringe (Galilee) of the fringe (Palestine) of the Roman empire, once did.

 
 Daniel Coulombe (Lothaire Bluteau), the boat-rocking protagonist in Jesus of Montreal, died at the end of that film, partly because of the appalling conditions in a public hospital. And to judge from The Barbarian Invasions, things have gotten even worse in the intervening years -- during the opening credits, the camera follows a nun down a crowded corridor as she passes numerous patients and staff, wires dangling from the ceiling as technicians fumble about. The whole scenario reeks of the so-called Third World.

 
 And then we discover something surprising. The nun, it turns out, is Sister Constance Lazure (Johanne-Marie Tremblay) -- the very same woman who worked in a soup kitchen before joining the other actors in Jesus of Montreal. Constance appears in only a few scenes in The Barbarian Invasions, but her presence adds an interesting spiritual element to a story that is mostly about the despair with which a selfish man faces the end of his life.

 
 In her conversations with R?my, Constance asserts that there must be a God who can forgive the abominable crimes we commit, that embracing the mystery of life is what can save us, and so on. If any other actress had played this role, I might have thought Arcand -- whose films can be notoriously sarcastic at times -- was belittling the character's naivete. But there is nothing condescending in Arcand's treatment of Constance, and the fact that she represents a link to Jesus of Montreal, perhaps the least cynical and most compassionate film Arcand ever made, suggests that he respects her, even if he cannot quite agree with her.

 
 Throughout the film, Arcand juxtaposes personal mortality with other kinds of mortality, such as the death of nations and ideas. In one scene, an academic on TV compares the September 11 terrorist attacks to the invasion of barbarians that brought down the Roman empire -- no longer are the outsiders being held off in Germany or wherever, but now they have struck the centre of power itself. When R?my's son S?bastien (St?phane Rousseau) goes looking for heroin to ease the old man's pain, a narcotics officer makes a similar comment about dealers ""invading"" Canada from other countries. And so the empire is still in decline. 

 
 This is where the second Jesus of Montreal cameo comes up. In one brief scene that has little to do with the film's plot but resonates strongly with its themes, a priest takes Sebastien's girlfriend to an underground warehouse, where a lot of religious art is sitting in a state of decay, unwanted and unused. The priest laments the death of religion in Quebec, and he asks if the church can sell any of this stuff off -- and the priest in question is Father Leclerc (Gilles Pelletier), the compromised, jaded cleric who commissioned the ill-fated passion play in the earlier film.

 
 These are fascinating parallels, and they make an old favorite even more interesting than it already was. As for the death theme in general, The Barbarian Invasions is not a very hopeful film -- it is filled with sadness, confusion and regret -- but it comes by its grief honestly.

 
 The same could not be said for The Event (now playing), Maritime director Thom Fitzgerald's manipulative and sentimental tribute to euthanasia. The film stars Parker Posey as a New York cop who investigates the death of an AIDS victim (Don McKellar) who went out with a bang by throwing a party for his friends and family the night that he planned to kill himself.

  Fitzgerald, who made his debut six years ago with The Hanging Garden, an awkwardly structured film about a gay Nova Scotian who revisits the place where he attempted (and possibly committed) suicide when he was a boy, has certainly improved as a director. The Event, which also alternates between past and present, has a more natural flow than the earlier film and is also quite entertaining, despite its serious subject matter. It also helps that Fitzgerald has a talented cast at his disposal, including Olympia Dukakis as McKellar's mother and Sarah Polley (who played a similar role in The Hanging Garden) as the gay man's understanding sister.

 
 But the fact that the film knows exactly how to push our buttons -- audience members sniffled throughout the movie and stayed reverently in their seats as the final credits rolled, which is unusual for a fast-paced film festival -- just makes it all the more offensive that his film is such a blatant work of propaganda. Fitzgerald tut-tuts every single character who would dare to question the righteousness of suicide, and the film goes on and on about how much love it took for the gay man's friends to kill him at his request.

 
 And of course, there is the usual patina of religiosity to make it all acceptable -- like when the gay man's mother says things like, ""Sometimes the only way to protect a defenseless child is to let him go back to God."" 

 
 What makes the film even more disturbing is the way it rubs our noses in the idea that, as the cop's own aging mother puts it, ""Getting old sucks."" And when the cop asks if her now-deceased father ever expressed interest in killing himself during his last, bed-ridden days, the mother replies, ""If only he'd asked."" AIDS victims, in other words, are not the only people we should consider killing out of the goodness of our hearts. You cannot help but wonder what goes through an old actress's mind when she is given such lines, especially in service of a film that goes out of its way to romanticize death.

 
 After sitting through that film, it was almost a relief to see My Life Without Me (opens November 7), in which it is Sarah Polley's turn to play the terminally ill one. Produced by Pedro Almodovar and directed by Isabel Coixet, yet filmed in Vancouver with an international cast, this film has one of the more peculiar and eclectic collections of talent that I've seen in a while.

  Polley plays Anne, a 23-year-old woman who lives in a trailer in her mother's backyard with her loving husband (Underworld's Scott Speedman) and her two cute daughters, and who makes ends meet by cleaning blackboards at a university. Despite being so young, Anne discovers she has just a few months left to live, so she makes a checklist of things to do before she dies -- including visiting her father (Frida's Alfred Molina) in prison and getting another man (You Can Count on Me's Mark Ruffalo) to fall in love with her, just to see what it feels like -- and she keeps her condition a secret from her husband, children, and mother (Blondie's Deborah Harry) even as she tries to manipulate things so that they can all have a good life without her. (The film also marks a re-union of sorts for Pulp Fiction co-stars Amanda Plummer, who plays Anne's food- and diet-obsessed co-worker, and Maria de Medeiros, who plays a Milli Vanilli-obsessed hairdresser.) 

 
 My Life Without Me is more subtle and playful and thoughtful and inviting than The Event -- which may not be saying much -- and it's a better film overall, but once again, the film is about a person who responds to news of her own imminent demise by acting selfishly. What's more, Anne does not allow those who love her to become part of this moment in her life, but she deliberately keeps them out. The film treats death as an opportunity for self-actualization, not as an opportunity to share one another's burdens in truly loving relationships.

 
 In addition, Anne's voyage of self-discovery may benefit from a cultural double-standard -- would the filmmakers go as easy on a married man who seduced a woman behind his wife's back, knowing that this other woman's heart would be broken when he left her or died a few months from now, just because he had only a few months to live? Somehow I doubt it.

 
 One film that I unfortunately did not get to see, but which I imagine would perfectly fit alongside these other films, is Allan King's documentary Dying at Grace, which captures the last days and moments of five people at a Salvation Army palliative care unit in Toronto. By all accounts, it sounds like a haunting, intimate film, so it's probably worth keeping an eye open for it.       

