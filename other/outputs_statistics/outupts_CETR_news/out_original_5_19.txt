


 By Michael Steen 


 ASHGABAT (Reuters) - Turkmenistan voted for a new president on Sunday in its first contested election, but one virtually certain to be won by an aide to the country's former authoritarian leader who died in December. 
"" &gt;






  
    
    
    
    
    
    
  	
    
    


 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:30PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      International
      
      &gt;
      Article
 
 
 
       Turkmens vote for leader in first contested poll 
       Sun Feb 11, 2007 12:02PM EST 
      
     

      
		

		 

	
	 
		

		

		 


 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		World News
     
   
   
		 
				Kosovo leaders urge calm after clashes kill two

			 
		 
				Blast kills 4 at Somalia ceremony

			 
		 
				Turkmens vote for leader in first contested poll

			 
		 
				Israeli cabinet approves Jerusalem excavations

			 
		 More World News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Michael Steen 

    


 ASHGABAT (Reuters) - Turkmenistan voted for a new president on Sunday in its first contested election, but one virtually certain to be won by an aide to the country's former authoritarian leader who died in December. 

    


 A European parliamentarian who monitored proceedings said the poll was not free and fair, but echoed diplomats who said it might herald gradual change in the reclusive, gas-rich Central Asian state. 

    


 Six candidates were officially vying to replace President Saparmurat Niyazov, who ruled the former Soviet republic for two decades with an iron fist while building golden statues of himself and stamping his image on every part of daily life. 

    


 The six all come from his Democratic Party -- the only legal political grouping. All pledged to continue in his steps, but the candidate seen as bound to win was acting leader Kurbanguly Berdymukhamedov, who served for 10 years in Niyazov's cabinet. 

    


 Human rights groups have condemned the poll, prompted by Niyazov's death from a heart attack, as a sham that would consolidate ""a new dictatorship"" in the desert nation. 

    


 ""They may hardly be called elections and they were absolutely not free and fair,"" Portuguese member of parliament Joao Soares told Reuters after visiting polling stations under Foreign Ministry accompaniment. 

    


 ""But ... the fact that they are at least trying to do something that resembles a free election is a step forward.""  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				 
U.S.-led forces show evidence of Iran arms in Iraq
 
				 BAGHDAD (Reuters) - Officials of the U.S.-led coalition on Sunday showed what they said were examples of Iranian weapons used to kill 170 of their soldiers and implicated high-level Iranian involvement in training Iraqi militants. 
                
                    Full Article  |  Video  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















