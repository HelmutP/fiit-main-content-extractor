

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
  
  Daily News
  
  
  


  
  
  
  
  
  
  
  
  




   
     
      IOL News
       | 
      IOL Sport
       | 
      IOL Business
       | 
      IOL Jobs
       | 
      IOL Entertainment
       | 
      IOL Travel
       | 
      IOL Motoring
       | 
      IOL Property
       | 
      IOL Parenting
       | 
      IOL Classifieds
       | 
      More 
     
 
        
           
     
        
       
          
         
	  News |
	  Opinion |
	  Entertainment |
	  Sport
         
       
     

     

     

       
      
       
         Top matric's exam stress tips 
         28 October 2008, 16:39 
         
           Related Articles 
           
             
               Nationally set matric papers cause concern 
               Tips to beat exam stress 
               'Let them write in peace' 
             
           
           
         
         
	  By Rivonia Naidu and Lisa-Marie Els 
 
Sschool pupils in KwaZulu-Natal who are feeling the pressure of the imminent end-of-year examinations and are in need of some encouragement should look no further than the inspirational Wayne Fredericks. 
 
The Southlands Secondary ex-pupil came first in the province in the 2007 matric exams, achieving an incredible aggregate of 98,5 percent.  
 
Although such results are generally achieved by pupils at private schools, Fredericks showed millions around the country that all odds can be overcome and that determination is the real key to success.  
 
Writing additional language papers on Wednesday, on Thursday and Friday, the computer application technology practical on Thursday and the information technology practical on Friday, KZN matric pupils have begun their first matric exam under the new Outcomes Based Education curriculum.  
 
Pupils in other grades begin their exams from next week.   
 
""Every setback is a setup for a comeback,"" is a motto coined by Fredericks, who believes it is a mantra all pupils, in particular matrics, should take to heart.  
 
""I used it to inspire my friends during exams last year,"" he said, ""and we all passed, so I think it worked!"" 
 
Although his family lives in modest circumstances, Fredericks attributes his success to the way he was raised. 
 
""They inspired me to do well,"" he said, ""and taught me how to go about life."" 
 
Fredericks thinks the most important lesson he learnt was to compete against himself, rather than others. 
 
""Throughout my life I have worked at bettering myself,"" he said. ""At the end of every term I would look back at my results and aim to improve on them, so I would never compete against anyone else. If I did come first, then so be it. I was just trying to be the best I could be."" 
 
Fredericks said matric pupils should not be anxious about the new syllabus.  
 
""View it as your year and don't fixate on previous years,"" he advised.  
 
""Practise the sections that stand out as important, and you should be fine."" 
 
Now an electronic engineering student at UKZN, he believes revision was the cornerstone of his success, and advised this year's matrics to revise as much as possible. However, Fredericks believes that training the body is as important as honing the mind. 
 
""I exercised frequently and made sure I stayed off the junk food. I would also hang out with friends to relax,"" he said. 
 
Durban psychologists had some handy advice for pupils writing exams.  
 
They said it was important to ensure adequate rest beforehand; to eat healthy food, like fruit and vegetables; and to take breaks to clear the mind and destress.  
 
Psychologist Rita Suliman said once an exam was over, pupils should not immediately throw themselves into studying for the next one. She said pupils should prepare an exam kit containing all the stationery required to write the exams to avoid last-minute panic.  
 
Durban North psychologist James Sharratt said the first thing pupils should understand was that preparation was vital and that some measure of stress before an exam was normal. 
 
""But when the stress and anxiety becomes unbearable, and one can't concentrate, or if a pupil suffers from insomnia as a result, it is important he or she seek professional counselling,"" Sharratt said.  
 
""If pupils feel severely stressed, they must either tell their parents so they can get help, speak to their life orientation teachers, or call Lifeline for counselling,"" he said.  
 
He said those pupils who had additional stress at home should seek help from professionals to help them get through the exams. 
 
    This article was originally published on page 5 of  The Daily News on October 28, 2008  
         
         
	   EMAIL STORY
	     
	   EASY PRINT
	     
	   SEARCH
	     
	   NEWSLETTER
	 
	  
       

       


       
         
          
            Search Daily News:
            
            
            
          
	   
            login |
            subscribe
	   
	 
	     
     	      
             
               Nazi plot to kill Obama 
              Two white supremacists allegedly plotted to go on a killing spree, shooting and decapitating black people and ultimately targeting Democratic presidential candidate Barack Obama, federal authorities said yesterday.
               
                 50 arrested during Zimbabwe talks 
                 
Police lay down law  to parties
 
                 'Honour killing' of woman a ruse 
               
             
			 
Subscribe today &amp; get full access to our Premium content 
 
            
              
             

 

 
	
             
           

           
              
             
               
                 Today's Front Page 
                 Browse By Page 
                 Past Front Pages 
                 Supplements 
                 Subscribe Now 
               
             

		

           

            
             
            
           

         

         
         
           About Daily News 
           
             
               About Us 
               Advertising 
               Booking Deadlines 
               Contact Us 
               Readership 
               Terms &amp; Conditions 
             
           
           Sister Newspaper Sites 
           
             
               Cape Argus 
               Cape Times 
               Daily News 
               Isolezwe 
               Post 
               Pretoria News 
               Sun. Independent 
               Sunday Tribune 
               The Independent on Saturday 
               The Mercury 
               The Star 
             
           
          
         
                 
 

         
           Online Services 
           
             
               Accessories 4 Outdoors 
               Audio, TV, GPS &amp; PS3 etc 
               Books, CDs and DVDs 
               Car Hire 
               Car Insurance 
               Car Insurance for Women 
               Cars Online 
               Compare Insurance Quotes 
               Education 
               Gold-Reflect who you are 
               Home Loans 
               Lease Agreements 
               Life Insurance 
               Life Insurance for Women 
               Medical Aid 
               Offer to Purchase 
               Online Shopping 
               Play Euro &amp; UK Lotteries 
               Property Search 
               Residential Property 
             
           
           Mobile Services 
           
             
               IOL On Your Phone 
               Mobile Games 
               Personalise Your Phone 
             
           
         

       


       
          
          
         

            
                
                
                

            
            Date Your Destiny
            
                 
           
            
                
                
                
                 

               
                  
                
                 
                  I'm a 34 year old man looking to meet men between the ages of 27 and 36.
                
              
              
           
           
         
      
 






  
    

		
			
			
			
			 
				
					Select Province &amp;
					Eastern Cape
					Free State
					Gauteng (PWV)
					KwaZulu Natal
					Mpumalanga (E. Tvl)
					North West
					Northern Cape
					Northern Province
					Western Cape
					------------------
					Botswana
					Lesotho
					Malawi
					Namibia
					Swaziland
					Zimbabwe
					Other Country (Not in SA)
				
			 
		
	
  
  
	
  
  
      
  
  
    
		 
			A 
			B 
			C 
			D 
			E 
			F 
			G 
			H 
			I 
			J 
			K 
			L 
			M 
			N 
			O 
			P 
			Q 
			R 
			S  
			T 
			U 
			V 
			W 
			X 
			Y 
			Z
		 
	
  
  
      
  
	 
       

 
         
           
              
             
               IOL JOBS 
               
                 Building a new future through passion  
                 Your performance is always measured  
                 Jobs database aids another young hopeful  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL TRAVEL 
               
                 More than a can of worms  
                 I've got you under my skin  
                 Unwind under the African sky  
               
             
            
           
         
       
       
         
           
              
             
               IOL TECHNOLOGY 
               
                 Nokia gives away smartphone technology  
                 Thailand to block websites insulting royals  
                 Intel to invest in solar venture  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL MOTORING 
               
                 'Hamilton prime to beat my title record' - Schumie  
                 Two new reasons for watching A1GP this season  
                 UK show warning for JIMS trade participants  
               
             
            Sign up for our FREE newsletter
           
         
       
       
         
           
              
             
               IOL PARENTING 
               
                 SA's shocking teen sex stories revealed  
                 Japanese parents try to marry off children  
                 Travel a major cause of accidental deaths  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL SPORT 
               
                 De Villiers isn't gambling on Pienaar, Smit  
                 Torres still hamstrung  
                 Pakistan fails to secure Windies Tests  
               
             
            
           
         
       
       
         
           
              
             
               IOL ENTERTAINMENT 
               
                 Chemistry between blonde trio will be electric  
                 'He either buys or rents his own jet'  
                 Christian signs up for Strange role  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               BUSINESS REPORT 
               
                 Stocks claw back lossed despite running out of steam  
                 Farming enterprises on BEE road  
                 Busa woos Chinese investors  
               
             
            Sign up for our FREE newsletter
           
         
		
		

		
	
		
		
       
       
       
RSS Feeds  |  Free IOL headlines  |  Newsletters  |  IOL on your phone  |  Group Links  
       
         
          © 2008 Daily News &amp; Independent Online (Pty) Ltd. All rights reserved. 
          Reliance on the information this site contains is at your own risk.  
          Please read our Terms and Conditions of Use and Privacy Policy. 
		  Independent Newspapers subscribes to the South African Press Code that prescribes news that is truthful, accurate, fair and balanced. If we don't live up to   the Code please contact the Press Ombudsman at 011 788 4837 or 011 788 4829.
         
         

		
		
		 
       
		  
		
		
       
     

   
  

 


  






