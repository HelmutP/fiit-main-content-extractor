

 "
//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 





 Arizona: escaping the US elections | USA - Times Online  
 









 
 
 	




	




















 

 
 








 


 
 

 Free Pizza 
 Buy one pizza and get one free at PizzaExpress  
 
 
 
 
 

 Navigation - link to other main sections from here 
 
Skip Navigation  
 
 

 
 	

 

 

	
 
 
 
 
 
 

 

For women in the public eye the business of dressing is fraught with pitfalls 
 
	
 India Knight 
 
 
Send your views
 
 
  
 

 
 
 


 
 
 News 
 Comment 
 Business 
 Money 
 Sport 
 Life &amp; Style 
 Travel 
 Driving 
 Arts &amp; Ents 
 
 
 Archive 
 Our Papers 
 Site Map 
 
 
 
 
 Travel News 
 Where To Stay 
 Your Say 
 Business Travel 
 Best of Britain 
 Winter Sports 
 Travel Images 
 Good Spa Guide 
 
 
 


	
 


 
	
 
 	
 
	



Times Online	
Times Archive	





 

 	
 Where am I? 
Home
 
 
Travel
Destinations
USA
 
 

 
 


 

	
 
 


	
MY PROFILE
	
SHOP
	
JOBS
	
PROPERTY
	
CLASSIFIEDS

	
 
 
 
 

 

 


 
 
 
 
 

 

From The Sunday Times
 
 





 
 
October 26, 2008 
 
 
 Arizona: escaping the US elections 
 In an isolated corner of Arizona, you can find peace, ranchers and absolutely no TV, says Stanley Stewart 
	
 
 










	
  




 

 
 
 

 
 
	

Stanley Stewart 

 
 
  






	
 


 
I had only been in Tombstone half an hour when the shooting started. 
Apparently, it was the Republicans and the Democrats at it again. 
 
 
This was just the kind of thing I had come to Arizona to escape. 
 
 
Like most people in this year of interminable electioneering, I had had enough 
of politicking, of spin and soundbites, of poll figures and attack ads. 
 
 
I was heading deep into the southeastern corner of the state in the hope of 
escape. I was looking for a quieter America, for a ranch somewhere with 
wide, empty landscapes, and cowboys with few opinions beyond yup and nope. 
When I stopped for lunch in Tombstone, I hadn’t expected to be detained by a 
gunfight on Main Street. 
 




 
	

	
 
 
 

  Going native in the USA
 

 Oregon’s annual Nez Perce Indian gathering is a Native American ritual, a pilgrimage and a knees-up 
 
 
	
	
 
 

  Obama v McCain fever in Washington
 

 It’s an election battle with global implications, and the front line is Washington, DC. Rob Ryan is in the thick of it 
 
 
	
	
 
 

  Six of the best US holidays
 

 If planning the trip of a lifetime Stateside, where would six Times writers and US specialists go? 
 
 
	

 
 
 Related Internet Links 
 
 Read more than 500 USA travel articles 	
 
 
 


 
 
 Related Links 










 
 
Obama v McCain fever in Washington
 
 


 
 

	



 
 
 


	
 
Trouble had been brewing all morning. There had been heated words outside the 
Bird Cage Theatre. Threats had been exchanged at Big Nose Kate’s. Now, Wyatt 
Earp and his brothers, Virgil and Morgan, with old friend Doc Holliday, were 
advancing down Main Street in spangly waistcoats. The Clan-ton gang had been 
spotted at the OK Corral. A crowd drifted after them. “There’s going to be 
shooting,” a fat man from Ohio helpfully informed his fat wife. 
 
 
To some people, this was the Gunfight at the OK Corral, in which the Clantons 
are gunned down in a hail of blanks every day at 2pm. To others, it is part 
of America’s political feud. The Earp brothers were the hired guns of the 
Republican hierarchy, the owners of the big mining and cattle operations who 
wanted to make Tombstone “safe for investment”. The Clantons, humble cowboys 
who wanted to preserve a bit of economic space for freelance prospectors and 
small independent ranchers, were aligned with the Democrats. You can see how 
much has changed in 127 years. 
 
 
I didn’t wait for the final body count. I was a man on a mission. I climbed 
back into the hire car, a big beast appropriately named Bronco, and spun 
away down an empty highway. Telegraph poles flashed by. Yellow hills rolled 
into empty distance. On the radio, crackling with static, a man was singing, 
“All my exes live in Texas, and that’s why I hang my hat in Tennessee.”
 
 
In Douglas I stopped for a coffee at the Gadsen Hotel. It is the town’s only 
moment of glamour, built a century ago for cattle barons. In the lobby, a 
grand double staircase rises past a vast spread of Tiffany stained glass. 
One night, a drunken Pancho Villa is said to have ridden his horse up these 
stairs, firing into the ceiling as he went. You can still see the chipped 
marble on the seventh step. 
 
 
Pancho probably didn’t read English – which explains why he missed the sign on 
the door of the Saddle &amp; Spur lobby bar: “No firearms or weapons of any 
kind”. The bar was empty but for the barman, who had fallen asleep in front 
of a “wanted” poster. On the television in the corner, people were waving 
flags and banners while John McCain, the Arizona senator, was giving them 
the thumbs up. I turned the set off and stepped next door into the diner for 
“All the coffee you can drink – one dollar”. 
 
 
BACK IN the Bronco, I followed Highway 80. It ran like a drawn line through 
the empty grasslands of the San Bernardino Valley. In an hour’s driving I 
saw two other cars, neither of them sporting political bumper stickers. From 
time to time, distant homesteads appeared, set back a mile or so from the 
road, tucked into folds in the long, yellow hills. It was remote country. My 
mobile couldn’t get a signal. Hopefully, I was beyond the reach of Fox News 
as well. 
 
 
Price Canyon Ranch lies at the end of a long dirt road in the foothills of the 
Chiricahua Mountains. It was just the kind of place I was looking for. I 
wanted a real working ranch, not a dude ranch with an infinity pool and a 
spa and a yoga class. I wanted somewhere that felt like the West, somewhere 
comfortable but rustic, not a citified luxury resort where you expected to 
find the horses on the sun loungers, sipping martinis. 
 
 
Price Canyon has 10 guest rooms elegantly decorated in western style with 
hardwood floors and Navajo rugs. One of the old barns has been beautifully 
converted into a large central lounge with a stone fireplace, deep leather 
sofas, a library of western books and a dining area where meals are produced 
by the wonderful Fred Tullis, a painter-turned-chef. If food is the heart of 
a home, Fred and his generous country meals are the heart and soul of Price 
Canyon. 
 
 

 
 
Page 1 of 2
 
 
 



 Next Page

 
 
 
 

 
	
 
 





 



 
 

 
 
 


 
 Have your say 
 
	
 
 
 
 
 I have stayed at Price canyon ranch and it is everything you report and more. I travelled alone and had the best adventure! 
 
 My only disappointment with your article is the background - nothing like Price Canyon........ and the fact that everyone now knows about my secret hideaway!! 
 
 Jane Cartwright, Woodsetts, UK 
 	
 
  
 

 






	



 


 Have your say 
 
 

 
 

 
 
 

   
 
	

  
 
 


 
Please note the maximum number of characters is 300.
 
 
 
 * Name 
 

 
 
 * Email 
 

 
 
 
 
 * Town  
 

 
 
 Country 
 

 
 
 
 * Required  
 
 
 

 
 
 Remember me  
 
 
 
 
 Terms and conditions 
 
  
  
 
 
 
 




 

	


 
 
 Print 
 Email 


 Post to del.icio.us 
 Post to Fark 
 Post to Yahoo! 
 Post to Digg 
 
 
 

 
 
 
 

 
	
 Also in USA  
 
 
 
Should I get an electronic visa for the US now? 
 
America's scariest ghost town 
 
 
 
 

 Also in Destinations  
 
 
 
 	
 
 

 
 
 
 
 
 
 
 
 
 

	
	

//W3C//DTD HTML 4.0 Transitional//EN""&gt;




  



 





 
 
	




 

 Times recommends 
 
 
Thomson bans serial complainers from booking holidays 
 
Seven top city breaks for under £100 
 
Why does Heathrow need to scan my shoes three times? 
 
 
 
 










 

 	
 

 
 
 
 Travel Search 
 
 


 
 Destinations 
 Holiday Type 
 
 

 
 
 

 

 Destinations 
Skip Destinations
 
 

 













 
 

Select
Africa
Asia
Australasia
Caribbean
Europe
Latin America
Middle East
North America
South East Asia

 
 
 

 
 


 
 

 
 













 

 
 



 Holiday Type 
Skip Holiday Type
  
 
 
 Active 
 Breaks 
 Family 
 Spas &amp; Lifestyle 
 
 
 
 
 Wildlife 
 Specialist 
 Wintersports 
 
 
 
 
 Beach 
 Cruises 
 Luxury 
 
 
 
 






 
 

 
 
 

 
 
 
 



 

 
 
 Travel comment    
 
	



 
 
 

I chased a bear in Slovakia, and limped home: adventure is a mixed blessing


 


More...

 
 
 Simon Barnes 
 
 
 
 
Post a comment
 
 
 
 
 
 




 
 

 
best read features 
 


 
 
  
Fabulous 50: travel 2008, the best so far 
  
How to fly for free (plus charges) 
  
Britain's most haunted cottage 
  
The real James Bond in Jamaica 
  
Viv Richards' top 10 places in Antigua 
 

 
 
 






 
 
 
 
 
 traveller 	
 
  
 
 
 
Be inspired: exclusive gallery by Gusov	
  
 
 
 
 
 
 
 
 
 
 




 

 	
 
 
 
 
 
 
  British hotel reviews - search maps   

 



 
 Britain (362)  
 London (57)  
 South East (69)  
 South West (52)  
 East Anglia (29)  
 Middle England (53)  
 Northern England (45)  
 Wales (28) 
 Scotland (29)  
 
	
 
 
 
 
 

 
 





 

 	
 
 
 
 
 
 
 Inside Travel   

 


 
 A guide to Travel Specials   
 Bolt Holes: British hotels  
 Budget Travel  
 Camp, caravan, motorhome 

 My hols: celebrity travel 
 Mystery Guest hotel reviews 

 Wintersun 2008-09 
 	

 
 Business City Guides  
 Currency Converter 
 E-mail Travel Bulletin 
 Travel Insurance 
 
 
 
 
 
 

 
 





 

 
 
 
Holiday Type   
 
	
 
 


The Turks and the Brits pilfered ancient artefacts from Greece, which should be returned - the tourist trade relies on ruins


 


More...

 
 
 Warren, London 
 
 
 
 
Post a comment
 
 
 
 
 
 




 

 	
 
 
 
 
 
 
 We've answered your Top 10 questions on...  

 


 
 Spas  
 Low cost airlines  
 The world's greatest sites  
 Holiday money  
 Hotel complaints  
 Cruises  
 Honeymoons  
 Rail travel  
 UK family spas  
 Flying with kids  
 Passports  
 Booking flights  
 Travel health 
 Travelling solo 
 Baggage 
 Travel insurance 
 Stopovers 
 Car rental 
 
 
 
 
 
 

 
 




 
 
 




 

 	
 
 

 
 
 
 Travel Directory 
 
 
 
 
 
 Search for a holiday 

 




 

 

 
 

 
 
 

 e.g. Villa in Tuscany 
 
 
 
 
 
 
 
 
 
 
 


	
 
 



 
 

 
 



























 
 Focus Zone 
 
 
 
 
 
 
 
 	
	
 
 	
 Social Entrepreneurs: 
 The inside track on current trends in the charity, not for profit and social enterprise sectors 	
 
 
 
 
 
 

 
 	
 Asian Cuisine: 
 Explore your passion for food with the delights of Thai, Indian &amp; Chinese cooking 	
 
 
 
 
 
 

 
 	
 A Life More Streamlined: 
 In our new series, Tony Hawks takes a dry, wry look at modern life - junk mail, interminable meetings and snooty sales assistants 	
 
 
 
 
 
 

 
 	
 Triathlon Training: 
 Read the training tips and advice that helped our London Triathletes 	
 
 
 
 
 
 

 
 	
 James Bond: 
 Read our exclusive 100 Years of Fleming and Bond interactive timeline, packed with original Times articles and reviews  	
 
 
 
 
 
 

 
 	
 Business Travel: 
 The latest travel news plus the best hotels and gadgets for business travellers  	
 
 
 
 
 
 
     
 
 
 
 
 
 Social Entrepreneurs 
 Asian Cuisine 	
 A Life More Streamlined 	
 
 
 
 
 Triathlon Training 	
 James Bond 	
 Business Travel 	
 
 
 
 
 
 
 











 






 
 
 
Classifieds 
 
 
 
 
 


 
 
Cars 
 
 
Jobs 
 
 
Property 
 
 
Travel 
 
 
 

 
 
 
 
 

 


 Cars 
 
Skip Cars of the Week  
 
Volkswagen Touareg Altitude 
 
2007 
£30,000 
  
 
 
 
 
 
 
 
BMW M3 Coupe 
 
2008 
£44,950 
 
 
 
 
 
 
 
 
Mini Cooper Cooper S 
 
2005 
£10,245 
 
 
 
 
 
 
 
 
Car insurance 
 
Great car insurance deals online 
 
 
 
 
 
 
 
 
 
 Search for more cars and bikes 
 
 

 


 


 Jobs  
 
Skip Jobs of the Week 
 
Standing Counsel
 
 
Charity Commission 
London 
 
 
 
 
 
 
 
Head of Membership and professional records
 
c. £50,000 + benefits 
British Medical Association 
London 
 
 
 
 
 
 
 
Sales Director
 
£
75k + big bonus 
Lyle &amp; scott 
London with overseas travel 
 
 
 
 
 
 
 
Software Engineering and Business Analysis Manager
 
£54,054 - £66,950 
National Improvement Policing Improvement Agency 
London 
 
 
 
 
 
 
 
 Search more Jobs 
 
 

 


 


 Properties 
 
 
Great Dubai Investment Opportunities
 
 
From £89,950 
 
 
 
 
 
 
Rushmore Condo's New Luxury Condo's In Manhattan
 
Great Investment, River Views 
 
 
 
 
 
 
 
Tooting, London 
 
2 bed flat for rent 
£1080PCM  
 
 
 
 
 
 
Luxury Antiguan Beachfront House 90ft Moorings Plus 40ft Sunseeker 
 
Also avaliable for rent 	
$3.5 million  
 
 
 
 
 
 
 Search for more properties 
 
 

 


 


 Holidays 
 
Skip Travel of the Week  
 
Funway Holidays Int Inc
 	
Captivating Caribbean 
SAVE 25% at Sandals Resorts! 
 
 
  
 
 
 
 
3 nights 4* Crowne Plaza Center City
 	
Deps 01 Nov - 20 Dec 08  
£469 per person 
 
 
 
 
 
 
 
Advertise your holiday property online
 	
List your property with two leading travel websites 
 
 
 
 
 
 
 
 
Travel insurance 
 	
Great travel insurance deals online  
 
 
 
 
 
 
 
 
 Search for more holidays 
 
 

 



 
 
Place your advert now
 
 
 


 Search Ad Reference: 
 

 
 

 

 
 
 
 
 
   
 


 

 








 
 
 
 	
 



 
 
 
 Where am I? 
Home
 
 
Travel
Destinations
USA
 
 

 
 
 
 Contact us 
 Back to top 
 

 
 
  
 News 
 Comment 
 Business 
 Money 
 Sport 
 Life &amp; Style 
 Travel 
 Driving 
 Arts &amp; Ents 
  


 
	
 
 
 
	



Times Online	
Times Archive	





 

 
 
 



 

 	
 
 Contact our advertising team for advertising and sponsorship in Times Online, The Times and The Sunday Times. Globrix Property Search - search houses for sale and rooms and property to rent in the UK. Milkround Job Search - for graduate careers in the UK. Visit our classified services and find jobs, used cars, property or holidays. Use our dating service, read our births, marriages and deaths announcements, or place your advertisement. 
 Copyright 2008 Times Newspapers Ltd. 
 This service is provided on Times Newspapers' standard Terms and Conditions. Please read our Privacy Policy.To inquire about a licence to reproduce material from Times Online, The Times or The Sunday Times, click  here.This website is published by a member of the News International Group. News International Limited, 1 Virginia St, London E98 1XY, is the holding company for the News International group and is registered in England No 81701. VAT number GB 243 8054 69. 
   
  










 
 


 


 
 
 









 
 
 



