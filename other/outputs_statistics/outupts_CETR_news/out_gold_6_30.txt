

 LAKE FOREST -- The improvement in the Indianapolis Colts' run defense during the playoffs has been staggering. 
 Bears offensive coordinator Ron Turner knows where to give the credit. 
 ""Twenty-one,"" he said. 
 Bob Sanders is 21.  In Super Bowl XLI the Bears will find the  Colts have him up near the line of scrimmage. They've been doing this  since his return from a knee injury and  their run defense has gone from one of historically bad proportions -- allowing a league-worst 173 yards a game --  to one giving up 73.3 yards a game in the playoffs.  
 ""That's oversimplifying it and not giving enough credit to the rest of the guys, but I think that is the difference,"" Bears offensive coordinator Ron Turner said. ""Sanders came in and gave them some energy, gave them some confidence.  
 ""He flies around, he plays so hard, he plays so fast and he's such a good player. I think everyone else kind of picked up on that and they're all playing better."" 
 Defensive tackle Anthony ""Booger"" McFarland called it a case of responding to pressure. 
 ""I mean, you get backed into a corner, a lot of one-game seasons, you play well, you move on; you lose you go home, and I think that's the attitude we've taken,"" McFarland said. ""Guys are making plays. That's the bottom line."" 
 McFarland is another reason. He came over from Tampa in a trade in October for a second-round draft pick after Corey Simon went on injured reserve. Although he played under coach Tony Dungy with the Bucs in the same system, it took a while for him to fit into the Colts' defensive line hole. 
 ""I'm just a piece of the puzzle,"" he said. ""As you know, when you have a puzzle, and then you have a lot of pieces that fit together and when they fit together well, the puzzle looks good, but if some of the pieces are off, the puzzle is going to look bad. So I just look at myself as a piece of the puzzle."" 
 Nevertheless, the Colts did allow 375 yards and 191 yards rushing in two games against Jacksonville. So one new player and another one fitting in better hardly sees enough to explain the difference for a more stout Colts run defense. 
 ""What I've seen, too, is a lot of teams have kind of given up on the run in the second half,"" Bears running back Thomas Jones said.  
 The Bears are still impressed with what improvement they've seen. 
   
 An even eight 
  The Bears are sending their biggest contingent to the Pro Bowl since the 1985 season because guard Ruben Brown was named as a replacement for injured Philadelphia Eagles guard Shawn Andrews. ""I really don't know what to say right now,"" Brown said. ""I'm in the Super Bowl. The Pro Bowl is great. I've been there eight times prior to now and I'm happy that they thought of me to go. But the Super Bowl is really all I care about. I don't care about anything else."" 
 Brown made it from 1996-2003 with Buffalo. His addition gives the Bears eight players, although defensive tackle Tommie Harris won't play due to a season-ending hamstring injury. 
 The Bears had a team-record nine players from the 1985 team make the Pro Bowl.  
 Brendon Ayanbadejo, Lance Briggs, Robbie Gould, Devin Hester, Olin Kreutz and Brian Urlacher are the other Pro Bowl players. 
   
 Friends forever 
  Running back Cedric Benson described his relationship with Jones now as cool, and he didn't mean frigid. ""I mean, we're cool. There's no conflict or drama between the two of us,"" he said. ""I mean, everything's good. I mean, what is the perception?"" 
 The perception from what Benson had said earlier this season was hardly friendship. Jones was seen more as an obstacle.  
 However, Benson said it was more of a professional adversarial relationship than personal dislike. 
   
 Heating up 
  Bears weather was nice for the NFC championship, but players are elated to be going somewhere above freezing. ""I got up this morning, it was 14 degrees, and driving to work it just hit me -- man, we finally don't have to play in cold weather,"" Jones said. 

