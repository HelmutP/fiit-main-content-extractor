

 BY TERRY SAVAGE
			
					
						
						
Q: My son purchased a home last week from a friend of ours.  There was no realtor involved, but we did have an attorney.  Now the sewer line has to be replaced --  there is a break in it.   
   
 
The seller had it rodded in December, and it was done again two days ago because we had water backing up in the basement.  The man from the plumbing company came out both times and told us that the seller was aware of the problem and was told of it. 
   
 
Now we have a very costly problem on our hands.  The plumber did put it in writing on our bill what was told to him.  What happens with the line is that if there is a slight break, roots grow and the line constantly has to be cleaned out. This happens every three months or so. 
   
 
I was told that the seller is liable because he knew of the problem. Is this true? I can't believe we were taken like this from a ""friend."" 
   
 
A: What a problem.  Your only resource is to go back to your attorney -- hopefully he/she was a specialist in real estate transactions and included some warranty clauses in the bill of sale.  Otherwise you'll have to have the attorney check state law and see exactly how the law reads in your state on the protection of real estate buyers.  Do it immediately, because typically there would be short time period for recision of the contract -- but again this depends on state real estate law.   
   
 
I'm going to post this letter because it is the best reason I can think of to work through a broker, or at very least a real estate lawyer and to make sure you have followed all the proper procedures, including a professional home inspection, which would either have disclosed this problem or brought forth the disclosure of any repairs made in the recent past.   
   
 
Please let me know how this turns out, as I'll be interested to see what recourse you have -- if any. 
   
   
 
Q: How can I invest in commodities -- as one would do with any stock -- with specific regards to natural gas commodities? 
   
 
A: Well, first you don't invest in commodities futures -- you speculate. 
   
 
And you must understand the concept of leverage -- a very low margin down payment.  That means if the price goes against you, you could lose not only all the money you put up to purchase the futures contract, but you could receive a call to put up more money! 
   
 
To learn more about this particular energy contract, go to the website of the New York Mercantile Exchange (NYMEX). They'll give you more information on the futures contract, and you can contact a futures broker to make your trades. 
   
 
But remember, this is not an investment -- And in case you were thinking that you have a unique idea, the futures prices instantly price in expectations of higher prices in the future. That's their function!   
   
   
 
Q: I'm figuring my cost basis on some gifted stock, and in searching the Internet I came across your column.  It's my understanding that the cost basis for the recipient of gifted stock is the donor's cost basis, not the value of the stock at the date of the gift.  Is this not the case? 
   
 
I enjoy your show when you cover for Bob Brinker.  I'm also finishing the Savage Number.  Thank you for writing it. 
   
 
A: The cost basis of gifted stock is the donor's cost basis.  The sales price is the price at which you sell the stock, not the price at which you received the stock.  And I'm delighted you're enjoying the book. 

