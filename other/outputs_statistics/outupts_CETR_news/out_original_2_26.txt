

 "//W3C//DTD HTML 4.01 Transitional//EN""
   ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
Uzi recoils, questions echo - The Boston Globe














  
  
  
  
  




















     
      
       
         
           
 	     
    	      
               
                 
Local Search Site Search
 
                
                
                  
                  
                
               
	     
	   
          
           
             
               
              Home Delivery
               
               
                  

  
               
             
           
         
 
 Home 
 Today's Globe 
 News 
 Business 
 Sports 
 Lifestyle 
 A&amp;E 
 Things to do 
 Travel 
 Cars 
 Jobs 
 Homes 
 Local Search 
 

 
 Local 
 National 
 World 
 Campaign '08 
 Business 
 Education 
 Health 
 Science 
 Green 
 Obituaries 
 Special reports 
 Traffic 
 Weather 
 Lottery 
 

         
         

 
         

 
        

 
THIS STORY HAS BEEN FORMATTED FOR EASY PRINTING 
 
 
 
         
           
 
Home /
 
 
News /
 
 
Local /
 
 
Mass.  
 
         
         
        

         
          
 
  
 

 

    
 Uzi recoils, questions echo 
 Boy's death spurs hard look at laws for guns, children 



 
    
        By 
        David Abel
    
     
    
          Globe Staff
          
          /
          October 28, 2008
    
     
         
                 
                         
Email|
 
                         
Print|
 
                         
Single Page|
 
                         | 
                         
                  
                
                        Text size
                        –
                        +
                
         



 

 

 



 
 
 WESTFIELD  - As his father raised his camera, an 8-year-old boy aimed an Uzi at a pumpkin set up at a shooting event. Before his father could focus, the third-grader from Connecticut squeezed the trigger, and the high-powered weapon recoiled  and fatally shot the boy in the head. 
 The death of Christopher Bizilj at the Westfield Sportsman's Club Sunday has raised questions about how someone so young could be allowed to shoot an automatic weapon, which can fire hundreds of rounds in a minute. 
 The Hampden District Attorney's Office and officials from the federal Bureau of Alcohol, Tobacco, Firearms and Explosives are investigating. They did not return calls seeking comment. 
 State Representative Michael Costello, the Newburyport Democrat who co-chairs the Joint Committee on Public Safety and Homeland Security, said yesterday that he plans to draft a bill that would ban anyone younger than age 21 from firing an automatic weapon. 
 ""This isn't a knee-jerk reaction; it's a common sense reaction,"" he said. ""We should take swift action to provide some reasonable restrictions on this type of unreasonable practice. It's almost indescribable that within a year of leaving a booster seat, an 8-year-old can be holding a submachine gun."" 
 In a telephone interview yesterday, the boy's father, Dr. Charles Bizilj, said he stood 10 feet behind his son as a professional trained in using the 9-mm Micro Uzi machine gun stood beside the boy on Sunday afternoon. He said he doesn't think the shooting instructor was holding the weapon as his son pressed the trigger, as guides did with other children firing the weapon. 
 ""This accident was truly a mystery to me,"" he said. ""This is a horrible event, a horrible travesty, and I really don't know why it happened. I don't think it's relevant that [the instructor] wasn't holding the weapon."" 
 He said his son, who loved to hike and bike near his home in Ashford, had fired handguns and rifles for three years. But he said this was the first time he had fired an automatic weapon. 
 ""I gave permission for him to fire the Uzi,"" Bizilj said. ""I watched several other children and adults use it. It's a small weapon, and Christopher was comfortable with guns. There were larger machine guns with much more recoil, and we avoided those."" 
 Bizilj, the medical director of the emergency department at Johnson Memorial Hospital in Stafford Springs, Conn., said his son was ""very cautious, very well trained, and very much enjoyed firing."" 
 The Uzi was the first gun the boy had fired all day. ""It was something he was looking forward to for months,"" Bizilj said. 
 The annual Machine Gun Shoot and Firearms Expo is a two-day event. Police are investigating whether the Sportsman's Club and the group running the event were licensed. ""We haven't confirmed whether either have been licensed,"" said Westfield Police Lieutenant Hipolito Nuñez, who added that no charges had been filed as of yesterday. 

 
 
 The outdoor event was organized by COP Firearms &amp; Training, an Amherst company run by Pelham Police Chief Ed Fleury that organizes machine-gun shoots regionwide. 
 Neither officials at the Sportsman's Club nor Fleury returned calls yesterday. 
 The club boasted in an advertisement for the event posted on its Website that the $5 entry fee was waived for children under age 16 and there was ""no age limit or licenses required to shoot machine guns."" 
 ""It's all legal &amp; fun,"" the advertisement says. ""You will be accompanied to the firing line with a Certified Instructor to guide you. But You Are In Control  - ""FULL AUTO ROCK &amp; ROLL."" 
 Shooting targets for the event included vehicles, pumpkins, and ""other fun stuff we can't print here,"" according to the advertisement. 
 The boy was firing the weapon at an outside firing range and was wounded once in the head when the recoil forced the gun to rotate upward and backward, Nuñez said. The boy was pronounced dead at Baystate Medical Center in Springfield. No one else was injured. 
 State law requires anyone under age 18 to have parental consent and a licensed instructor to fire an automatic weapon. Otherwise, there is no minimum age to fire such a gun, Nuñez said. 
 ""We do not know at this time the full facts of this incident, and it's being investigated,"" said Nuñez, who said it was unclear how many bullets were fired. 
 Outside the gates of the club yesterday, Fran Mitchell, a member since 1956, said, ""Everybody is shaken up."" 
 He said the bullet struck the boy's lower jaw. He did not see the shooting but said he watched as the boy was taken away. ""He was crying, he was still awake. 
 ""It was just one of those things,"" he said. ""We've got a tremendous safety record."" 
 Asked if the club will now restrict children from firing weapons, he said, ""I don't know, the club is going to have to sit down and make some serious decisions. We will be a long time getting over this."" 
 Bob Greenleaf, a former executive board member and member for 44 years of the Sportsman's Club, said either the boy's father or the range officer should be held responsible for the boy's death. 
 ""To allow the boy to hold the gun and shoot it without anyone holding it was not only stupid but criminal negligence  - and it should be considered involuntary manslaughter,"" Greenleaf said. ""If it was really important for the child to hold the gun, the adult should have been holding the weapon. The child should have only been able to press the trigger."" 
 The Uzi is a 50-year-old weapon once used by the Israeli infantry and later adapted for use by commandos and secret service agencies around the world because it can be used in close quarters to fire rapidly. The Micro-Uzi, a smaller version of the original weapon, weighs 4.5 pounds and can fire 20 rounds per second. An Uzi has more recoil than a handgun but less than an assault rifle such as an M-16. Recoil -- the backward motion of a weapon after it's fired -- is greater for guns that fire rapidly and use larger caliber shells. 
 ""It's very difficult to control,"" said Greenleaf, who did not attend the gun expo and said it was the club's first death. ""We're lucky the gun went up instead of sideways. If it went sideways, we might have had 10 dead people."" 
 Matt Tencati of Orange attended the event but left shortly before the accident. He said he watched as instructors held similar weapons' magazines to keep them steady as youngsters fired. 
 ""That way, there was no way for the children to lose control of the gun,"" he said. ""This is a tragedy, but a tragedy can happen anywhere at any time. You can't legislate away tragedy."" 
 He and other gun enthusiasts yesterday questioned the rush to revise the law. 
 State Senator Stephen M. Brewer, a Barre Democrat who is also on the public safety committee, described the boy's death as a ""very rare and freaky incident."" 
 ""Before we take any action, we need to look at all the details,"" Brewer said. 
 But Wayne Sampson, executive director of the Massachusetts Chiefs of Police Association, said Christopher Bizilj's death exposes a ""deficiency in the current law."" He said the law should reflect the differences between a 15-year-old and an 8-year-old handling such a powerful weapon. 
 ""The problem is there isn't any restriction on the age of a person who can use a weapon,"" he said. ""I would not allow my 8-year-old to handle a weapon like this. I believe it's beyond their capacity, because of the velocity and recoil of the weapon."" 
 The death stunned friends and family of the boy. 
 ""He was the kid you would clone if you could,"" said Maureen Connolly, a teacher who had Christopher as a student in the first grade in Ashford. ""He was about as perfect as they come."" 
  Michael Levenson and Andrew Ryan of the Globe staff and Globe correspondent Gregory B. Hladky contributed to this story. 
 © Copyright 2008 Globe Newspaper Company. 
 
 





 
   
     
      1
     
     
      2
     
       
       
   
 





 

          
           

 
   
 
           
             
              
              
 

 RELATED COVERAGE 


 
 
Interactive Graphic: Muzzle climb effect on a Mini Uzi
 
 

 
Video Father of boy accidentally shot speaks
 | 
Discuss
 
 
 

              
             
                     
         
        
         
        
          
 

   
     
Email
 
     
Email
 
     Print 
     
Print
 
     
Single page
 
     Single page 
     
Reprints
 
     
Reprints
 
     
Share
 
     
Share
 
     
Comment
 
     
Comment
 
     
    
   
   
       
     
       
         Share on Digg 
         Share on Facebook 
         
 Save this article
 
         powered by Del.icio.us 
       
     
     
       
      
      
      
      
      
      
         
           
            Your Name
            
            Your e-mail address
            (for return address purposes)
            
            E-mail address of recipients
            (separate multiple addresses with commas)
            
            Name and both e-mail fields are required.
           
         
         
           
            Message (optional)
            
            
           
           
          Disclaimer:
          Boston.com does not share this information or keep it permanently, as it is for the sole purpose of sending this one time e-mail.
         
         
      
 
     
   
 



        
         
           

 			
         

        
        
       
       
        
 
         
           Advertisement 
           

 
        

       

       


        
          
           

          
           

  
          
       
       

       
 
   MOST E-MAILED 
   



   
    
    
     
    
    
        Surveillance of Wilkerson allegedly accepting bribes 
    
     
    
    
     
    
    
        Restaurant closed after dead deer found in kitchen 
    
     
    
    
     
    
    
        Fidelity reportedly may lay off up to 4,000 
    
     
    
    
     
    
    
        Embattled state senator faces corruption charges 
    
     
    
    
     
    
    
        Prize-winning noodle kugel 
    
     
    
    
     
    
    
        Autopsy: Toddler may have drowned 
    
     
    
    
     
    
    
        Uzi recoils, questions echo 
    
     
    
   

    See full list 
 
 
   Recommended Searches 
   
     
       
         Fidelity 
         Halloween 
         Microsoft news 
       
     
     
       
         Gun club tragedy 
         HVAC 
         Skin treatment 
       
     
     
   
   About this list 
   
   



 
     
     
     
 

         
   
 

 
 

 
 

 
 

 
 

 
 

 
 
 
   
   
     
       Home 
 | 
       Today's Globe 
 | 
       News 
 | 
       Business 
 | 
       Sports 
 | 
       Lifestyle 
 | 
       A&amp;E 
 | 
       Things to Do 
 | 
       Travel 
 | 
       Cars 
 | 
       Jobs 
 | 
       Homes 
 | 
       Local Search 
     
     
       Contact Boston.com   | 
       Help 
 | 
       Advertise 
 | 
       Work here 
 | 
       Privacy Policy 
 | 
       Newsletters 
 | 
       Mobile 
 | 
       RSS feeds 
 | 
       Make Boston.com your homepage 
     
     
       Contact The Boston Globe 
 | 
       Subscribe 
 | 
       Manage your subscription 
 | 
       Advertise 
 | 
       The Boston Globe Extras 
 | 
       The Boston Globe Store 
 | 
       ©  NY Times Co. 
     
   
 
   

 
 
       
     
















