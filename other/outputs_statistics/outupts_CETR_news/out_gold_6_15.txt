

 When Republican businessman Larry Warner asked to be put into an alcohol treatment program in when he enters federal prison, Warner was joining a growing trend of criminal defendants trying to participate in a program that can shave significant time off their prison sentence. 
 

Successfully completing the program can shave up to 1? years off prison time in some rare instances, experts said. Warner was sentenced to 41 months Wednesday. 
 

Federal authorities say they have seen an increase in recent years in the number of criminals convicted in federal court in Chicago asking to get the treatment. And that has attracted some controversy. 
 

In April, for instance, a federal judge rejected a request by a money launderer and tax cheat, Nick Boscarino, to try to get into the program. U.S. District Judge John F. Grady said he saw no evidence of alcoholism in Boscarino, a former friend of Rosemont Mayor Donald E. Stephens. 
 
 
 High dropout rate 

At the time, Assistant U.S. Attorney Stephen Andersson said: ""It used to be when it came to sentencing, people got religion. Now they tend to get alcoholism."" 

David Novak, a former federal convict who is a Utah-based consultant for criminal defendants seeking to get into the program, said Thursday that entry is highly competitive. For instance, there has to be a significant record of the person's alcoholism to be admitted, he said. 
 

The alcohol program is intensive with a high dropout rate but for those who complete it, the rewards are evident, including a quicker release to a halfway house from prison. 

