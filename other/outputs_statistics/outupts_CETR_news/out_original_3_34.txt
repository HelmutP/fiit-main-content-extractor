

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
THE LAST 'KING' | By ADAM BUCKMAN | TV Shows | TV Ratings | Television Shows
" is calling it quits.  The comedy series that gave the...""&gt;

























 
     
    
       
       
          
             	
                            
             
               NYC Weather 84 ° SUNNY              
          
          
             
          
          
             
               Saturday, August 25, 2007 
               
                   Last Update: 
		06:45 PM EDT                   
               
             
             
             
             
                     
             


			Recent
			30 days+
			The Web
			

           
       

       
            
            
			 
				Story Index
				Summer of '77
			 
			
            
            
            
            
            
           
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Movies
            Make Us Rich
            Real Estate
            Travel
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
            Intern Stories
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
   TV Home
   Listings
   Reviews
   Linda Stasi
   Starr Report
   Adam Buckman
   LIVE: The TV Blog
       
       
           
          
              
             
               
                               
                   
                      THE LAST 'KING'     
                      SERIES ENDS AT THE ALTAR 
                   
                               
				  
				  
				   

 
                  
				  
				   

                                    Rating:  May 14, 2007 -- THERE is no joy on Queens Boulevard tonight.  
  Or on Northern Boulevard, or Main Street, or Elmhurst Avenue.  
  "The King of Queens" is calling it quits.  
  The comedy series that gave the borough of Queens its very own Ralph Kramden - portly, blustery parcel deliveryman Doug Heffernan (Kevin James) - is signing off after nine seasons on CBS with a one-hour series finale.  
  The episode begins with the planned wedding of Arthur (Jerry Stiller) and Ava St. Clair (Lainie Kazan) and ends with a touching montage of great moments from some of the series' best episodes.  
  Along the way, Doug and wife Carrie (Leah Remini) will wrestle with each other over the future direction of their life together (while a drunken Doug will literally wrestle with almost everybody else, as you'll see if you watch tonight).  
  As those who have been following "The King of Queens" in its final episodes are already aware, the Heffernans' marriage is on the rocks following disagreements over whether to adopt a child and move from Queens to Manhattan, where an apartment has become available 15 years after the couple added their names to the building's waiting list.  
  Elsewhere, hapless Spence (Patton Oswalt) continues to rue his split with roommate/pal Danny Heffernan (Gary Valentine), brother of Doug.  
  Naturally, they're both guests at the wedding, along with series regulars Deacon Palmer (Victor Williams), who struggles to persuade Doug to put on his pants; Deacon's wife, Kelly (Merrin Dungey); Holly Shumpert (Nicole Sullivan), who is now pregnant, but still single; and family friend Lou Ferrigno (as himself).  
  Jerry Stiller's wife, Anne Meara, guest stars as Spence's mother, Veronica.  
  Since this hour is the last episode for "The King of Queens," the show's producers might have been tempted to pile on the sentiment.  
  Thankfully, they decided against that. Instead, the sentimentality is spread thinly, with no sacrificing of this show's stock-in-trade, which is, of course, comedy.  
  How will "The King of Queens" be remembered? Probably something like this: As this funny little sitcom that never ranked too high or too low in the ratings, but which persisted for nine seasons - and made millions upon millions of dollars in rerun syndication, where it is likely to live on for years to come.  
  "The King of Queens" Series finale  Tonight at 9 on CBS  
                                   
 

             
             
                


 
 HARRIS, 26, AND BELFORD, 27 
 DYING 'KING' SIGNALS DEATH OF INSULT SITCOMS  
 FULL CIRCLE  
 

 
 OPRAH TAKES ON NYC 
 MARY-JANE OLSEN 
 BOB GRANT COMEBACK 
 MORE 
 
                 
 

 
                              			     
                    
				 
					






 
	 
	    
		 
		  			 DATING 
		  			 JOBS 
		  			 AUTOS 
		  			 REAL ESTATE 
		  			
		 
	 
	     
 				 
             
          
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
           
            sign in
            subscribeprivacy policyrss 
          
       
    
     
    
       NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007 NYP Holdings, Inc. All rights reserved. 
  	   
 


