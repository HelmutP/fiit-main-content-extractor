

 "
 
 The First Perspective and The Drum - Aboriginal, First Nations and Native News in Canada, NW Ontario and Manitoba



























          
  


			 														 			         		 
Headlines   |              			        		 
Employment Listings   |           		         		 
Join Taiga News   |             			 				 Get daily news updates           			                            		 
About Us   |            		 
Contact Us   |            		 
Home            		 			 			 															




 


 





      Editorial    Ottawa Watch    Under Northern Sky    Obidiah  
     About Us    Contacts    Subscription Info    Home 
     Advertising Info    Employment ads 
 
  
   Previous Issues  
 
    PDF Doc:  1.2 meg  
 
 
  

                


 Editorial: A not-so-crazy idea whose time has come 
 September 26, 2008 - James Wastasecoot 






 
Somewhere in the bush...
 
 
John: I forgot to mention that I think I will run for parliament - next time around. 
Sarah: You're going to run for parliament! You? 
John: Yes, me. 
Sarah: You can't even stand straight. Do you think you'll still be around next time? If you're gonna run, why not this time? 
John: Well, it kind of caught me by surprise, this one. I'm not ready. 
Sarah: Yeah? Well, that's Cree planning isn't it. Just think if you got into parliament and you had to plan something. 
John: I'd have a secretary. 
Sarah: Well, how would you run anyway. First off, you're an Indian and second, you're broke. 
John: Yeah, well that's what I mean. I'm broke right now. 
Sarah: So we gotta go cut wood today sometime, might as well get started. 
John: Yeah. As for being Indian, you know there are Indians all over the place now. 
Sarah: Yeah, I know they're all over the place! That's the problem, they should be home and at work. Our Council is all over the damn place and we're stuck here. 
John: I mean they're doctors, lawyers and what not. We even got a judge now from this community. 
Sarah: Not doing us any good here. 
John: Yeah well, Lookit that Tina there. She's a Member of Parliament. If she can do it, so can I. 
Sarah: Well, at least she's there to remind those bigwigs we're still here and we're not going anywhere. 
John: The thing is she's in the wrong party. 
Sarah: What do you mean? 
John: Well, she's part of the system. She's part of the Liberal party of Canada and she has to toe the party line, right? I would run for the First Peoples National Party. 
Sarah: Yeah, well good luck to you. I'll cut wood. If you can get there, I'll vote for you. 
John: The thing is our own people don't see why this is so important. This is how the Bloc Quebecois got started. Nobody thought they would get anywhere and look at them now. 
Sarah: Yeah, I guess they're there to represent their language, their culture, their own people. 
John: Yeah, that's what they're there for. We could do the same thing but we need to elect at least one member to parliament. 
Sarah: So you've made up your mind, eh? You're going to run for The First Peoples Party? 
John: Well, you can never tell what's going to happen in parliament. Do you remember that Chuck Cadman fellow? 
Sarah: Yes, the independent MP whose vote resulted in a tie in the house of commons. 
John: Yeah, and the speaker had to break the tie and the Liberals were saved from an election. 
Sarah: So one vote made a big difference in that case. 
John: Yeah, and there was also Elijah Harper. He made an historic vote in the Manitoba legislature on the Meech Lake thing. His vote made a difference. 
Sarah: But do you think our people would support a First Peoples candidate? 
John: I think they'd be crazy not to. 
Sarah: Ok. So maybe you should run for parliament then. If somebody like Eugene Whelan could get elected, then why not you? 
John: Right. Why not me?
 
 


 

       Email this link 
   Printable version 
   Email comments 
  
  
  
 







Employment Listings   |   About Us   |   Contact Us   |   Webmaster   |   Home   








