

 "        
//W3C//DTD XHTML 1.0 Strict//EN""
        ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd""&gt;


 

						    
			    				US election: Obama and McCain fight for Pennsylvania as both target the swing state |
				World news |
				guardian.co.uk
	
		

			
				
	




								
			
		
		
		
	
		
					
				
		
		
				









 

    
    		
	
					
				  
			

    		
	
	
	
		  
	

 
	 
		 Jump to content [s] 
				 Jump to site navigation [0] 
		 Jump to search [4] 
		 Terms and conditions [8] 
	 
 
 

	 
		 

 Sign in 
 Register 

 Text larger
 
 smaller 

 




					
						



     


	        
    
                
        
        
	
    

     

			
	 
	 
					
			 
				
		
	
	
		Search: 
		
		
			
		
			guardian.co.uk
			
							World news
						Web
			
		
		
		
		
	
		
	 
		    

	 
		 
			
																													 
						News
					 
							
																								 
						Sport
					 
							
																								 
						Comment
					 
							
																								 
						Culture
					 
							
																								 
						Business
					 
							
																								 
						Money
					 
							
																													 
						Life &amp; style
					 
							
																								 
						Travel
					 
							
																								 
						Environment
					 
							
																													 
						Blogs
					 
							
																													 
						Video
					 
							
																													 
						Jobs
					 
							
																																		 
						A-Z
					 
									 
	 


					 			
									    
	
	  
		 
										 News 
							 World news 
							 US elections 2008 
					 
	 



							 
			 
		
		
 


 
 	    


	 
					    


				
		        


        



	    




  	   	        
   	   	   	   	
      	       
  	   	        
   	   	   		
	
		
        		        	        	                        
        	         
        		            		                		        		
        		        		                                                          
                                        
                                       
                                                    
	            	         
	            
            

		
				
		 
			
							 US election: Obama and McCain fight for swing state Pennsylvania 
						
				
					 
			 



     
      	    
  
	        

	    
        




        	
 
	    			 
											                	        	        	        	            Ewen MacAskill in Hershey and Suzanne Goldenberg in Chester, Pennsylvania
				 
							 
			  guardian.co.uk,
			 
	  			  			 Tuesday October 28 2008 18.52 GMT 
	  			  				
		
	 

 

			 
							
									   John McCain and Sarah Palin address a rally in Hershey, Pennsylvania. Photograph: Bradley C Bower/AP 
					 
	
			 Barack Obama demonstrated his pulling power by attracting thousands of people to an outdoor rally in torrential rain today in Pennsylvania, a state on which John McCain has staked almost all of his hopes of a surprise win next Tuesday's election. 
 Confirming Pennsylvania's pivotal position on the electoral map, McCain was also in the state, campaigning alongside his running-mate, Sarah Palin, who told a crowd of about 10,000: ""It is going to come down to the wire here."" 
 Although it was one of McCain's best performances in weeks, he is facing polls that put Obama consistently ahead - the latest has Obama on 51% to McCain on 41%. 
 He is also beset by continuing distractions over Palin. Amid persistent rumours of strains between his campaign team and her advisers, the Politico website quoted one of his aides, speaking anonymously, describing her as ""a whack job"". 
 With time running out for McCain, Obama today showed how difficult it will be for the Republican candidate to make up ground in the state. 
 In spite of ankle-deep mud and freezing rain, campaign workers estimated about 8,000 people waited for hours for him to arrive in Chester, on the outskirts of Philadelphia, huddled under umbrellas, draped in plastic ponchos and even wearing plastic carrier bags over their shoes. 
 ""This is an unbelievable crowd for this kind of weather,"" Obama told them when he stepped out in a dark rain jacket over jeans. ""If we see this kind of dedication on election day there is no way we are not going to bring change to America."" 
 Pennsylvania, which has voted for Democrats in every presidential election since 1988, is the only Democratic state where McCain is still strongly competing. 
 In spite of a commanding lead over McCain in the polls, Obama's main ally in the state, the governor, Ed Rendell, has been warning the campaign not to take Pennsylvania for granted. Although Obama is ahead overall, detailed polls show the several key counties have tightened in McCain's favour over the last week and the two are now in a dead heat. 
 McCain is also pinning his hopes on white, working-class voters reluctant to turn out for Obama for a variety of reasons, including race. But many of them are angry over the economy and the blame they attribute to the Republicans outweighs their own lingering prejudices. 
 McCain's rally was indoors, at the Giant Arena, in Hershey, the central Pennsylvania home of the chocolate bar, and, after days of only attracting derisory crowds, he almost filled the 12,000-seat ice hockey stadium. 
 McCain appeared to be reinvigorated and, after weeks of floundering, to have settled on a consistent message portraying Obama as a socialist and untested in a crisis. 
 Seeming to address the reports of tension between his and Palin's teams, McCain, standing only a few feet from her, said: ""When two mavericks join up we don't agree on everything, but it's a lot of fun."" 
 Areas like Hershey are where the election could be won or lost, with residents saying the town is fairly evenly divided between Republicans and Democrats. 
 To loud cheers from a boisterous crows, McCain said: ""We are going to fool the pundits and win Pennsylvania."" 
 McCain took a swipe at Obama over the million dollars he is spending on a half-hour paid-for television address tomorrow night, unprecedented in US political history. It will held before a World Series baseball game, delaying the start, to the irritation of some fans. 
 ""By the way, no one will interrupt the World Series for an infomercial when I'm president,"" McCain said. 
 In spite of a standing ovation, many in the audience had come to see not McCain but Palin, who received the loudest cheers. Some of the audience left after she has spoken and before McCain had a chance to begin. 
 In spite of Obama's turn-out today, there is a lot of resistance to him in places like Hershey. Kim Condran, 40, a health worker who was at the McCain rally, expressed confidence that McCain can still take Pennsylvania. ""Obama is not a truthful person. I don't think he is revealing everything in his past. He has too many connections to foreigners, the bad ones,"" she said. 
 McCain and Palin had a good turn-out given the event was early in the morning, that it was a working day and the rain was heavy. But McCain opted against putting to the test whether he is capable of drawing, like Obama, an outdoor crowd in heavy rain, cancelling such an event later in the day in Quakertown, Pennsylvania. 
 Pennsylvania's two major cities, Philadelphia and Pittsburgh are expected to vote heavily for Obama, while McCain hopes to wrap up the small towns and rural areas sandwiched between them. 
 Behind the scenes, the Obama camp is worried about long lines at polling stations and other disruptions in battleground states. 
 Voting rights advocates in Pennsylvania have filed law suits to demand that state election officials increase their supply of paper ballots, and have reserves of trained poll workers on hand. 
	
	
 


    
  	
	
  
 




		
 
		
		    



	 
	
							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
		
			 
			
			
			larger | 
			smaller		 
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
			



			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 

		    

	
	 
	
							
					 World news 
					
						 
			 
									US elections 2008 ·							 
			 
									Barack Obama ·							 
			 
									John McCain ·							 
			 
									Republicans ·							 
			 
									Democrats 							 
		 
					
								 
																			 More news 		
							 
				
		
 


		    			 
			 
																										 
						 More on this story 
					 
																						 
								     			

     
								             US election: Obama leads McCain by 19 points among early voters, Pew says
     
																	 Barack Obama is leading 53% to McCain's 34% among those who have already voted, according to poll 
																																																									 
 
							 
								    
		    	
	
	
								Michael Tomasky: Republican attacks don't stick
							 
						 
																																																	 
							 
								    
		    	
	
	
								McCain and Republicans desert Alaska senator
							 
						 
																																																			 
		 
	

		    


	
		
		
		
		 
				
			
						 
			
															 Related 
																			 
				Oct 27 2008 
										 
															
						    

						Desperate attacks at the last minute won't hurt Barack Obama
				  
																									 
				Oct 27 2008 
										 
															
						    

						US election: McCain stokes fears of Democratic dominance if Barack Obama wins election
				  
																									 
				Oct 23 2008 
										 
															
						    

						US election briefing: Sarah Palin shopped for shopping
				  
																									 
				Oct 22 2008 
										 
															
						    

						US election: Republican operative faces voter registration fraud charges
				  
							 

			
		 
		
				
	


 

	
	
		
	    



	 

							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
			 Article history 
		
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
			



			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 
	     
	 
		 
			 About this article 
            Close
		 
		        	        			
					   	   	   
 			 
				 US election: Obama and McCain fight for Pennsylvania as both target the swing state 
		   				   	 						This article was first published on guardian.co.uk on Tuesday October 28 2008. It was last updated at 19.51 on October 28 2008.							 
			 
 


  	    	    
	    	 
									 Related information follows Ads by Google 
						    



	
 




	 




				  
 

 
	
	
      	    	    
	    




     


	        
    
                
        
        
	
    

     


	        				
 	
 


	    

	    
	
			
	 
		 
			
											
									 Bestsellers from the Guardian shop 
								
				 
											
										
											 AeroGarden 
										
											 A miniature indoor garden that enables you to grow organic herbs, salad leaves etc. in just water and air.  
										
											 From: £119.99 
									 
				
				 
									 
																					 Visit the Guardian offers shop 
																																		 Buy eco, organic and fair trade products 
				 														 
								 
					
		 
	 
	

	    
			 
		 Latest news on guardian.co.uk 
	

		 
			    
	                         Last updated one minute ago 
            
		 
		 
							 
					 News 
					 Man sentenced to 30 years for Kercher murder 
				 
							 
					 Sport 
					 Newcastle beat West Brom 
				 
							 
					 World news 
					 Obama: 'We are one week from change - but it won't be easy' 
				 
					 
	 

	        


 
															 Free P&amp;P at the Guardian bookshop 
																					
	 
					
							 
							 
					
						
					
				 
				 
					 
						 Cook Your Own Veg 
						 ?16.99 with free UK delivery 
					 
				 
			 
					
							 
							 
					
						
					
				 
				 
					 
						 Henry 
						 ?25.00 with free UK delivery 
					 
				 
			 
			 
			 
																			 
													Browse the bestseller lists
											 
																 
													Buy books from the  Guardian Bookshop
											 
									 
	 


	    

	
		
 
	
	
		
	 
		 
			 Sponsored features 
		 
		 	
						            
    
                
        
        
            
		 
		 	
						            
    
                
        
        
        
		 
	 
 


	    	
		
			
	
		 

	
			  
	
		
	    



	  
		
			
											
										
									
				 
		UK
	 
				
		
			
											
										
						
				 
		USA
	 
				
			 

	 
			 
			
						
													
			 UK 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Senior Research Executive
 
												 reilly people.
												senior research executive is required for this maj….
												?Neg.
								 
			 
					 
 
				Lecturer/Senior Lecturer in Marketing/Marketing Re…
 
												 london metropolitan university.
												london (city).
												?30,000 to ?44,538 per annum.
								 
			 
					 
 
				Operations director
 
												 morgan hunt.
												our client, a prestigious professional membership….
												?50000 per annum.
								 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
			 
			
						
													
			 USA 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Director Of Sales
 
								 to the resort provides a visual of the rich heritage of quartz mountain 's past and present. quartz mountain arts and conference center is the model place for... .
																ok.
												 
			 
					 
 
				Open Positions
 
								 undergraduates, including heritage speakers, and be... unique educational mission. the school of arts &amp; sciences has a strong liberal arts tradition and serves a... .
																ny.
												 
			 
					 
 
				Permanent Surgery
 
								 beaches, and a strong maritime heritage. experience the vibrant culture with its live theatre, music camp, center for the arts, museums and galleries, and a... .
																mi.
												 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
		 
	


	
	
		
	
	

	
	 




      	    	    
	    			



     


	        
    
                
        
        
	
    

     

	


      	      	
      	 

			    
	    


	
		
		
		
		 
							 Related information 
				    

	
	 
	
							
					 World news 
					
						 
			 
									US elections 2008 ·							 
			 
									Barack Obama ·							 
			 
									John McCain ·							 
			 
									Republicans ·							 
			 
									Democrats 							 
		 
					
		
		
 

				
			
						 
			
																					 
													
												
					 
								 
															
											McCain and Obama in New York
						 
 	
							    

										                    		Sep 12 2008: 
		                    								  
 On the anniversary of 9/11, the two presidential candidates discuss the role of service in American life, at Columbia University 
					 	
					 						 							 							
															More video
													 	
												
													
							 

						 
			
													 
				Oct 21 2008 
										 
															
						    

						US election: John McCain's campaign backing away from Colorado
				  
																		 
				Oct 21 2008 
										 
															
						    

						US election briefing: Family matters
				  
																		 
				Oct 20 2008 
										 
															
						    

						Obama a friend of terrorists, say McCain phone calls to voters
				  
																		 
				Sep 14 2008 
										 
															
						    

						Palin uses Hillary to taunt Obama
				  
							 

						 
			
																					 
													
												
					 
								 
															
											New Hampshire - the election night in pictures
						 
 	
							    

										                    		Jan 9 2008: 
		                    								 The New Hampshire primary election results
					 	 
					 						 							 							
															More galleries
													 	
												
													
							 

			
		 
		
				
	


		




 
		 			
			 
License/buy our content |  
			 
Privacy policy |  
			 
Terms &amp; conditions |  
			 
Advertising guide |  
			 
Accessibility |  
			 
A-Z index |  
			 
Inside guardian.co.uk blog |  
			 
About guardian.co.uk |  
			 Join our dating site today 
		 
		
			
			 		
			
			 guardian.co.uk © Guardian News and Media Limited 2008 
			
						
			 
			
	
	    

	


	
		
			Go to: 
			
			
						
												
								
																														guardian.co.uk home
									
								
																	UK news
									
								
																	World news
									
								
																	Comment is free blog
									
								
																	Newsblog
									
								
																	Sport blog
									
								
																	Arts &amp; Entertainment blog
									
								
																	In pictures
									
								
																	Video
									
								
									
								
																	Archive search
									
								
																	Arts &amp; entertainment
									
								
																	Books
									
								
																	Business
									
								
																	EducationGuardian.co.uk
									
								
																	Environment
									
								
																	Film
									
								
																	Football
									
								
																	Jobs
									
								
																	Katine appeal
									
								
																	Life &amp; style
									
								
																	MediaGuardian.co.uk
									
								
																	Money
									
								
																	Music
									
								
																	The Observer
									
								
																	Politics
									
								
																	Science
									
								
																	Shopping
									
								
																	SocietyGuardian.co.uk
									
								
																	Sport
									
								
																	Talk
									
								
																	Technology
									
								
																	Travel
									
								
																	Been there
									
								
									
								
																	Email services
									
								
																	Special reports
									
								
																	The Guardian
									
								
																	The Northerner
									
								
																	The Wrap
									
								
									
								
																	Advertising guide
									
								
																	Compare finance products
									
								
																	Crossword
									
								
																	Events / offers
									
								
																	Feedback
									
								
																	Garden centre
									
								
																	GNM Press Office
									
								
																	Graduate
									
								
																	Bookshop
									
								
																	Guardian Ecostore
									
								
																	Guardian Films
									
								
																	Headline service
									
								
																	Help / contacts
									
								
																	Information
									
								
																	Living our values
									
								
																	Newsroom
									
								
																	Notes &amp; Queries
									
								
																	Reader Offers
									
								
																	Readers' editor
									
								
																	Soulmates dating
									
								
																	Style guide
									
								
																	Syndication services
									
								
																	Travel offers
									
								
																	TV listings
									
								
																	Weather
									
								
																	Web guides
									
								
																	Working for us
									
								
									
								
																	Guardian Weekly
									
								
																	Public
									
								
																	Learn
									
								
																	Guardian back issues
									
								
																	Observer back issues
									
								
																	Guardian Professional
									
														
			
		
	

 

 
	    


	    





	  





	    
	









     


	        
    
                
        
        
	
    

     




