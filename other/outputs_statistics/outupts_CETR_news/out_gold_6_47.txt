

 Who hasn't felt ripped off at some point -- or, worse, been ignored or tangled in red tape when you've tried to resolve a consumer problem? 
 
 These things happen. And usually, except in the case of outright scams, consumers and business owners have a mutual stake in getting the problem resolved. 
 
 That's why the Sun-Times is launching a new column, called The Fixer. Its aim: to help consumers and the targets of their wrath -- at least the honest ones -- try to wipe the slate clean. 
 
 But before The Fixer can help, you need to do a little work of your own. 
 
 Don't rant. Don't rave. Definitely don't be a wimp. 
 
 Do try to help yourself. Plaster a smile on your face, and go see the salesperson you dealt with. Or find the mechanic, talk with the contractor, or take a deep breath and call the customer-service number of the company you're dealing with. Assume the steely smooth on-screen persona of Clint Eastwood, and calmly, intelligently explain the problem. (It's a huge help to have a sheet of paper with the names, dates and specifics in front of you.) 
 
 ""Hostile and aggressive doesn't work. You just look weak and out of control,"" says Lynette Padwa, author of Say the Magic Words: How to Get What You Want from the People Who Have What You Need (Penguin, $14) -- advice that will resonate with any parent who's dealt with a screaming toddler. 
 
 Instead, say Padwa and other experts in the art of complaining, be friendly. Be confident. Assume that the person you're dealing with actually wants to help you. 
 
 Tell them specifically what you want -- being reasonable with what you request. (Hint: The airline that spilled in-flight coffee on your shoe isn't going to spring for round-trip tickets to Paris.) 
 
 It pays to be nice to customer-service employees, says T. Scott Gross, author of Positively Outrageous Service: How to Delight and Astound Your Customers and Win Them for Life (Warner, $18.95). ""They can like you and want to help you, or they can abhor you and stay away from you like the plague,"" Gross says. ""You want to get mad with them, not at them."" 
 
 If they can't solve it, go up the chain of command. Find the manager, then the district manager, then the district manager's manager, and so on. Repeat as necessary. 
 
 If that doesn't work, get out your pen. Write a concise letter -- preferably avoiding threats of bodily harm -- that lets the key business people know what the problem is and how you've tried to resolve it. Set a deadline -- two or three weeks; whatever you think seems reasonable -- to get things fixed, and say if they can't do it by then, you'll seek third-party assistance. Then, do it. 
 
 ""Don't write it off to experience,"" says Steve Bernas of the Better Business Bureau. ""That's what some of these, especially the scam artists, bank on."" 
 
 That's where The Fixer comes in. Each week, starting in mid-September, we'll run some of your letters and try to help solve your thorniest consumer problems. 
 
 Honest businesses are likely to talk. Scam artists will probably duck. Either way, it should be enlightening. 

