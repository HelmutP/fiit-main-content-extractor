










 




 

 









 


 




 
 
  
  







  


 December 23, 1999 
 
  REVIEW 


 Crazy, Sexy, Cool: Virtual Beauty Makeovers 

IStyle Personal Makeover
 
(Sierra Home; $39.95 before $10 
rebate; Windows 95 and 98.)
 
Cosmopolitan Virtual  Makeover 2 Deluxe 
 (Broderbund; $49.99; Windows 95, 98 
and NT 4.0.)
 
 
  By J. D. BIERSDORFER
 

 
  
 
  hen was the last time you 
got a bad haircut and were 
afraid to leave the house 
without a hat or headband until it 
grew out? Two competing beauty 
makeover programs are going head-to-perfectly-coifed-head in hopes of 
helping you avoid being a shut-in this 
season. 
 
   
 

 
 

 

 







IMAGINE - Cosmopolitan's Virtual 
Makeover 2 Deluxe, top,  lets users try different looks; iStyle, bottom, 
includes videos.
 




In one corner stands the challenger, iStyle Personal Makeover by 
Sierra Home, and in the other,  the 
latest version of Broderbund's Cosmopolitan Virtual Makeover Deluxe. 
Both programs let the user load a 
photograph of herself from a scanner 
or a digital camera  (or use  one of the 
many models' photographs included). Then the user digitally tries on a 
variety of hairstyles, cosmetics and 
accessories in the search for a new 
look. Each program will let you print 
out the resulting creations, and each 
provides Web-shopping links and detailed product information.
 
  Although iStyle isn't stamped with 
a recognizable editorial name, as its 
competitor is, it is still a robust program that is packed with features. 
The software's libraries contain 
more than 500  hairstyles, 800 cosmetic colors and 400 accessories to sample, including hats, jewelry and sunglasses. An interactive Beauty Consultant will give a survey to new 
users to help provide appropriate 
suggestions for colors and styles. 
Once the user has selected an electronic face to play with, new hair and 
accessories can be added  with a 
quick click. Makeup can be applied 
with a selection of on-screen brushes 
and pencils, but getting used to 
putting on eye liner with a computer 
mouse may take practice. Cosmetics 
from companies including Revlon, 
Almay and Philosophy are represented in the program's makeup bag. 
 
  iStyle also contains 24 short videos 
that show the user how to accomplish 
such feats as properly applying lipstick and blush, as well as maintaining one's eyebrows. There is a Tips 
section that contemplates issues like 
""Liquid Vs. Powder Foundation,"" 
and short video clips  advise the user 
on how to recreate specific looks 
from distinctive  periods in fashion 
like the 1960's and 1970's. The program is currently available with a 
$10 rebate through  January, and updated styles and products will be 
offered for downloading at its  Web 
site at www.istylemakeover.com. 
 
  Cosmopolitan's Virtual Makeover 
Deluxe, now in its second edition, 
also provides the user with 500 hairstyles, including some  with bridal 
veils. The program has 30 hair solutions for men as well, along with 
several mustache and beard options. 
The program offers 300 cosmetic colors (Cover Girl products are featured prominently) and 200  accessories like hats and sunglasses to sample. Users are prompted to participate in a short Beauty Quiz early on 
to help create a personal profile, and 
recommendations are offered 
throughout the program based on the 
profile. 
 
  Virtual Makeover retains much of 
the sassy flavor of Cosmopolitan's 
editorial voice (an example from the 
Beauty Quiz: ""What Look Do You 
Like Best: Sleek Sophisticate or Daringly and Devastatingly Sexy?""), 
and users can also print out their 
made-over images on Cosmopolitan 
covers. New styles can be downloaded from the product's Web site 
(www.virtualmakeover.com).
 
  The Web site also has details about 
various versions of the Virtual Makeover software offered at different 
prices. And Broderbund  offers a version called Essence Virtual Makeover aimed at African-American 
women.   
 
       Whichever personal makeover product you choose, you should 
keep in mind that what you see on 
your screen may not be the  true color 
of the cosmetic products in real life. 
Factors like background lighting, the 
quality of the personal image you use 
and even the type and color calibration of your computer's monitor may 
affect the look. 
 
  To run iStyle, users will need a 
computer running Windows 95 or 98, 
with a minimum 133-megahertz  Pentium-class processor, 16 megabytes 
of RAM and 45 megabytes of free 
hard drive space. Virtual Makeover 
2 Deluxe  requires a computer running Windows 95 or later,  with at 
least a 90-MHz Pentium-class processor, 16 megabytes of RAM and 40 
megabytes of free hard drive space. 
(Although  there are no Macintosh 
versions of  the programs,   they 
should both run adequately on Power 
Macs using Windows-emulation software like Virtual PC.)
 
  Both programs have their strong 
points and support from established 
beauty-product companies. While 
Virtual Makeover has the strength 
and reputation of Cosmopolitan magazine behind it, iStyle seems  better 
suited for the novice cosmetics user. 
But in the end, the decision may 
depend on one factor: brand loyalty.
 
  
 
   
Related Sites  These sites are not part of The New York Times on the Web, and The Times has no control over their content or availability. 
  
 
 
www.istylemakeover.com
 
 
 
www.virtualmakeover.com

  

 
 
  
 

 






 
 


Home | 
Site Index | 
Site Search  | 
Forums  | 
Archives | 
Marketplace
  
Quick News  | 
Page One Plus  | 
International  | 
National/N.Y.   | 
Business   | 
Technology  | 
Science   | 
Sports   | 
Weather  | 
Editorial    | 
Op-Ed | 
Arts   | 
Automobiles   | 
Books  | 
Diversions   | 
Job Market   | 
Real Estate  | 
Travel 
 
 
Help/Feedback  | 
Classifieds   | 
Services   | 
New York Today 
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















