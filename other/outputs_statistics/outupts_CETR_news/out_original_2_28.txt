

 "//W3C//DTD HTML 4.01 Transitional//EN""
   ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
McCain banking on Biden gaffe - The Boston Globe














  
  
  
  
  




















     
      
       
         
           
 	     
    	      
               
                 
Local Search Site Search
 
                
                
                  
                  
                
               
	     
	   
          
           
             
               
              Home Delivery
               
               
                  

  
               
             
           
         
 
 Home 
 Today's Globe 
 News 
 Business 
 Sports 
 Lifestyle 
 A&amp;E 
 Things to do 
 Travel 
 Cars 
 Jobs 
 Homes 
 Local Search 
 

 
 Local 
 National 
 World 
 Campaign '08 
 Business 
 Education 
 Health 
 Science 
 Green 
 Obituaries 
 Special reports 
 Traffic 
 Weather 
 Lottery 
 

         
         

 
         

 
        

 
THIS STORY HAS BEEN FORMATTED FOR EASY PRINTING 
 
 
 
         
           
 
Home /
 
 
News /
 
 
Politics  
 
         
         
        

         
          
 
  
 

 

    
 McCain banking on Biden gaffe 
 Ad highlights 'crisis' remark 



 
    
        By 
        Michael Kranish
    
     
    
          Globe Staff
          
          /
          October 28, 2008
    
     
         
                 
                         
Email|
 
                         
Print|
 
                         
Single Page|
 
                         | 
                         
                  
                
                        Text size
                        –
                        +
                
         



 

 

 



 
 
 WASHINGTON  - After absorbing weeks of criticism about Sarah Palin's qualifications to be vice president, John McCain's campaign is highlighting the most controversial statements of Joe Biden, arguing that the Democratic vice presidential nominee has provided some of the best evidence against Barack Obama's candidacy. 
 With only a week until Election Day, the McCain campaign is spending some of its limited resources on a television commercial that includes a recording of Biden warning that there will be an effort to purposefully ""test"" Obama with an international crisis. The narrator of the ad urges voters to ""listen to Joe Biden talking about what electing Barack Obama will mean."" 
 It is unusual for an ad focused on the words of a vice presidential nominee to be launched in the final stages of a campaign. But with so much attention on questions about Palin, along with the recent dust-up about the $150,000 wardrobe purchased  by the Republican National Committee for her use, the McCain campaign is trying to shift from defending Palin to going on the offensive against Biden. The campaign views it at a twofer, using Biden's words in an effort to undercut Obama. 
 ""We discovered he was making our argument for us, he was making the case against Barack Obama,"" said Ben Porrit, a spokesman for the McCain campaign. In addition to the television ad, the  McCain campaign is seeking to publicize what it calls Biden's greatest gaffes, including the Delaware senator's comment that an ad criticizing McCain's lack of computer skills was ""terrible"" and his comment that it is ""patriotic"" for wealthier people to pay higher taxes. 
 Even Obama seemed to be defensive after hearing Biden's comment that there would be a deliberate effort to test Obama, saying that Biden sometimes engages in ""rhetorical flourishes."" 
 The McCain campaign, which has complained for weeks that Palin is subject to tougher scrutiny than Biden, believes that the criticism of Biden has reached a critical mass. That view may have been reinforced when last weekend's edition of ""Saturday Night Live"" led off with a sketch that mocked Biden's comments, and did not include the usual Tina Fey impersonation of Palin. 
 The effort comes amid reports that Palin has tried to assert some independence from her handlers in the McCain campaign, such as her suggestion that the campaign criticize Obama for his association with his former pastor, the Reverend Jeremiah Wright, notwithstanding McCain's statement that he wouldn't make such an attack. She also disagreed with the campaign's decision to pull financial resources out of Michigan, and said the automated phone calls attacking Obama weren't helpful. McCain disputed reports of tension in the campaign. 
 A Biden spokesman said the campaign is not concerned about becoming a target of attack in the McCain campaign ad. ""The fact they are running this kind of advertisement this late in the election is an admission on their part that they can't talk about the number one issue before voters, which is the economy,"" Biden spokesman David Wade said. 

 
 
 The increased focus on Biden's gaffes has led to media reports that Biden is being cloistered from the media and the public, but Wade disputed that idea, saying that Biden yesterday did 10 television interviews, either live or via satellite, in battleground states. 
 In an interview last week on Florida television station WFTV, anchorwoman Barbara West irritated Biden by asking, ""How is Senator Obama not being a Marxist if he intends to spread the wealth around?"" Biden asked whether West was ""joking"" and his campaign subsequently said Biden's wife, Jill, would not make a planned appearance on the station. The McCain campaign said Biden was ""leveling attacks on local reporters who ask challenging questions."" Biden later criticized the anchor for asking ""ugly"" questions. 
 In running the advertisement about Biden's comments, the McCain campaign is returning to what it initially believed would be its strongest argument: that McCain has the experience to deal with an international crisis and that Obama has not been tested. But some political analysts question whether that argument has been undermined by the selection of Palin, who had virtually no foreign policy experience and did not meet a foreign head of state until she became a vice presidential candidate. 
 Kevin Madden, a former spokesman for former Massachusetts governor Mitt Romney, said Biden's comment about Obama being tested by an international crisis ""fit perfectly"" with the McCain campaign's closing argument that Obama is ""an untested leader, a risky choice."" But Madden noted ""some will say that the Palin pick complicated the ability of the campaign to draw a very distinct, very clear contrast on the readiness argument."" 
 Larry Sabato, director of the University of Virginia's Center for Politics, said yesterday that ""Biden has gotten off easy in this campaign""  in comparison with Palin. The reason, he said, is that Biden is ""the boring one,"" while McCain, Palin, and Obama ""are black holes that have sucked in all available light and media attention."" 
 While Sabato viewed Palin as a ""highly unwise, unprepared"" choice who has hurt McCain's chances, he said that Biden has stumbled frequently. 
 Biden, in a recent appearance on ""The Tonight Show With Jay Leno,"" said that he now refrains from speaking with supporters along rope lines at rallies out of concern that he will make an offhand comment that will be picked up by the media. ""I learned to just go, 'Mm, mm, mm, mm,' "" Biden said. 
 © Copyright 2008 Globe Newspaper Company. 
 
 





 
   
     
      1
     
     
      2
     
       
       
   
 





 

          
           

 
   
 
           
             
              
              
 

 MORE POLITICAL COVERAGE 


 
 
McCain calls liberals a threat to economy
 
 
Obama reprises call for change and unity
 
 
Canellos: Small donors fueling Obama's last lap
 
 
For pollsters, finding an accurate sample is art, science
 
 
Notebook: McCain attacks Obama on redistribution
 
 
McCain is banking on Biden gaffe
 
 
ATF breaks up plot to assassinate Obama
 
 
The final stretch: Rivals talk taxes, new politics
 
 
Mass. Democrats campaigning in swing states
 
 
Kerry, Beatty trade jabs on partisanship at final debate
 
 
 
 POLITICAL OPINION 


 
 
Derrick Z. Jackson: Time for a chat with automakers
 
 
H.D.S. Greenway: Character trumping experience
 
 
More Campaign '08 Coverage
 
 
 

              
             
                     
         
        
         
        
          
 

   
     
Email
 
     
Email
 
     Print 
     
Print
 
     
Single page
 
     Single page 
     
Reprints
 
     
Reprints
 
     
Share
 
     
Share
 
     
Comment
 
     
Comment
 
     
    
   
   
       
     
       
         Share on Digg 
         Share on Facebook 
         
 Save this article
 
         powered by Del.icio.us 
       
     
     
       
      
      
      
      
      
      
         
           
            Your Name
            
            Your e-mail address
            (for return address purposes)
            
            E-mail address of recipients
            (separate multiple addresses with commas)
            
            Name and both e-mail fields are required.
           
         
         
           
            Message (optional)
            
            
           
           
          Disclaimer:
          Boston.com does not share this information or keep it permanently, as it is for the sole purpose of sending this one time e-mail.
         
         
      
 
     
   
 



        
         
           

 			
         

        
        
       
       
        
 
         
           Advertisement 
           

 
        

       

       


        
          
           

          
           

  
          
       
       

       
 
   MOST E-MAILED 
   



   
    
    
     
    
    
        Surveillance of Wilkerson allegedly accepting bribes 
    
     
    
    
     
    
    
        Restaurant closed after dead deer found in kitchen 
    
     
    
    
     
    
    
        Fidelity reportedly may lay off up to 4,000 
    
     
    
    
     
    
    
        Embattled state senator faces corruption charges 
    
     
    
    
     
    
    
        Prize-winning noodle kugel 
    
     
    
    
     
    
    
        Autopsy: Toddler may have drowned 
    
     
    
    
     
    
    
        Uzi recoils, questions echo 
    
     
    
   

    See full list 
 
 
   Recommended Searches 
   
     
       
         Fidelity 
         Halloween 
         Microsoft news 
       
     
     
       
         Gun club tragedy 
         HVAC 
         Skin treatment 
       
     
     
   
   About this list 
   
   



 
     
     
     
 

         
   
 

 
 

 
 

 
 

 
 

 
 

 
 
 
   
   
     
       Home 
 | 
       Today's Globe 
 | 
       News 
 | 
       Business 
 | 
       Sports 
 | 
       Lifestyle 
 | 
       A&amp;E 
 | 
       Things to Do 
 | 
       Travel 
 | 
       Cars 
 | 
       Jobs 
 | 
       Homes 
 | 
       Local Search 
     
     
       Contact Boston.com   | 
       Help 
 | 
       Advertise 
 | 
       Work here 
 | 
       Privacy Policy 
 | 
       Newsletters 
 | 
       Mobile 
 | 
       RSS feeds 
 | 
       Make Boston.com your homepage 
     
     
       Contact The Boston Globe 
 | 
       Subscribe 
 | 
       Manage your subscription 
 | 
       Advertise 
 | 
       The Boston Globe Extras 
 | 
       The Boston Globe Store 
 | 
       ©  NY Times Co. 
     
   
 
   

 
 
       
     
















