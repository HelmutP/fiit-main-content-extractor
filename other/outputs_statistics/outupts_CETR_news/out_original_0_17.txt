

 //W3C//DTD HTML 4.0 Transitional//EN"" ""http://www.w3.org/TR/REC-html40/loose.dtd""&gt;

 
BBC SPORT | Cricket | Sangakkara inspires Sri Lanka win






    
    

















 












	

	 
	 
	 

 
	
		  
		 
 
  Home  
  News  
  Sport  
  Radio  
  TV  
  Weather  
  Languages  
 
 
 
		 
			
	
		
	 
	
	

		 
 
	 
 
	




 

	
    

 
			
			UK version
	International version
About the versions|Low graphics|Help
	 
 
 





    
        
    

			

 
	
		
	
	
	
		
			
				
					   
					
					 
					Sport Homepage
					
					 
				
				
			
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
			
				
					   
					 
					
					Cricket
					
					 
				
				
	
	
	
		
		
			
				
					 
						
						Latest scores
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Results
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Fixtures
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Tables
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Future tour dates
						
					 
					 --------------- 
				
			
		
	
	




	
	
	
		
		
			
				
					 
						England
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						International Teams
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Counties
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
			
		
	
	




	
	
	
		
		
			
				
			
		
	
	




	
	
	
		
		
			
				
			
		
	
	




	
	
	
		
		
			
				
			
		
	
	




	
	
	
		
		
			
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Women's cricket
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Skills
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Laws &amp; Equipment
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Get Involved
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						606
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Test Match Special
						
					 
					
				
			
		
	
	




	
	
	
		
		
			
				
					 
						Archive
						
					 
					
				
			
		
	
	





			
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	




	
	
	
		
		
	
	





	
 




    

		 
 
 --------------- 
	 Daily E-mail   
	 Mobiles 	 
 
 

 
 Fun and Games 
 Question of Sport 
 



	
	
 CHOOSE A SPORT 
	
            Select	
			----------	
			Football	
			Cricket
			Rugby U
			Rugby L
			Tennis
			Golf
			Motorspt
			Boxing
			Athletics
			Snooker
			Racing
			Cycling
			Disability Sport
			Other
			------
			N Ireland
			Scotland
            Wales
	



	  



 RELATED BBC SITES 
 
	 NEWS 
	 WEATHER 
 



	
		

        
        
            
                
                    
			
                        
                        
Last Updated: Sunday, 11 February 2007, 11:56 GMT

  

	


                        
    
         
            
                
                E-mail this to a friend
            
            
	       	
	 	Printable version
            
         
    
    


                        
                    
                
            
            
		
                
		
		
			
			 
				 
					Sangakkara inspires Sri Lanka win
				 
			 
		
		
    		
		

                
                    
                        
	
		
                    	
		
			One-day international, Rajkot: Sri Lanka 257-8 (50 ovs) bt India 252-9 (50 ovs) by 5 runs  

    
	
        
	    	 
			Scorecard
		 
	
        
	
    



 


	
		 

			
			 
				
				 Sangakkara hit his sixth one-day century 
			 
			
		
		
	

	


A century from Kumar Sangakkara guided  Sri Lanka to a memorable five-run victory against India in the second one-day international in Rajkot.
 
Put in, the tourists struggled to 55-3 before Sangakkara put on 108 in 22 overs with Tillakaratne Dilshan (56).
 
 
He struck 11 fours and four sixes to inspire a total of 257-8.
 
 
Sourav Ganguly and Sachin Tendulkar put on 100 to lead India towards victory but 11 were needed from the final over and two wickets fell.

 
 
Sri Lanka made one change to the side that endured a washed-out opener in Calcutta on Thursday, including Nuwan Kulasekara in place of fellow fast bowler Nuwan Zoysa.
 
 
The home side started well when Munaf Patel had the experienced Sanath Jayasuriya caught behind for nine and trapped Marvan Atapattu leg before for 15.
 
 


	
		 

			
			 
				
				 India's experienced batting duo put their team back on track 
			 
			
		
		
	

	


 
Meanwhile Sreesanth had both opener Upul Tharanga and skipper Mahela Jayawardene edging catches in the close cordon.
 
 
Sangakkara and Dilshan rebuilt the innings, cashing in on the absence of a third seamer after shoulder trouble forced Zaheer Khan to be sidelined 
 
 
He joined fellow pacemen Irfan Pathan (shoulder), Ajit Agarkar (fever) and batsman Yuvraj Singh (back) on the injury list.
 
 
Sangakkara's first four scoring shots were boundaries, and he and Dilshan both hit leg-spinner Anil Kumble for fours to complete fifties as they looked set for a big total.
 
 
Off-spinner Harbhajan Singh eventually bowled Dilshan with a 'doosra' delivery to end the partnership.
 
 

    
    
	 

	
            
            
                
		
                
                     
                     
	 
		 
		Sachin's dismissal was the turning point
		 
 	 




 
                
                     
                     
	 Mahela Jayawardene 


 
                
            
        
	
	
    
    




 
On 91, Sangakkara saw Patel drop a straightforward return catch, but went on to complete his hundred.
 
 
India's reply started badly when Robin Uthappa and skipper Rahul Dravid departed within the opening four overs.
 
 
Tendulkar and Ganguly appeared to have put their side on course for victory but both were dismissed within four overs.
 
 
India needed 24 from the final 30 balls with five wickets intact, but Dinesh Karthik was yorked for 31.
 
 
Jayawardene then took a stunning catch running back towards third man to oust Harbhajan Singh in the 48th over and 14 were needed from the final 12 balls.
 
 
Unusually Mahendra Singh Dhoni did not hit his first boundary until the 67th ball he faced, but then next ball, the penultimate delivery of the match, he sliced to deep cover where Farveez Maharoof took another spectacular running catch.
 
 
Jayawardene was delighted his team prevailed in a tense finale.
 
 
""It was a brilliant game and Sachin's [Tendulkar] dismissal was the turning point,"" he said. ""A couple of quick wickets and good fielding at the end won the match.""
   
 
 
		
                    	 
		
	
	
  
                        
                        
                    
                    
                    
			
			
                        





    

	
                 
                         
                                SEE ALSO
                         
                 
                
		
			
				 
					
	
		
			
			
			
			Rain ruins Calcutta one-day clash
			
		
		
			 
		
	

					
						
							
								08 Feb 07 | 
								Cricket
								
							
						
					
					
				 
			
				 
					
	
		
			
			
			
			Sehwag back for Sri Lanka series
			
		
		
			 
		
	

					
						
							
								03 Feb 07 | 
								Cricket
								
							
						
					
					
				 
			
				 
					
	
		
			
			
			
			Star bowlers rested by Sri Lanka
			
		
		
			 
		
	

					
						
							
								17 Jan 07 | 
								Sri Lanka
								
							
						
					
					
				 
			
				 
					
	
		
			
			
			
			Sri Lanka in India 2007
			
		
		
			 
		
	

					
						
							
								12 Nov 06 | 
								Future tour dates
								
							
						
					
					
				 
			
		
		 
	

	

 


     

	
    
	
		
			 
				 
					RELATED BBC LINKS:
					
				 
			 
			 
				
					 
						
							Your say - 606
							
						
					 
				
					 
						
							Cricket weather
							
						
					 
				
					 
						
							BBC Hindi Service
							
						
					 
				
					 
						
							BBC Sinhala Service
							
						
					 
				
					 
						
							BBC Asian Network Sport
							
						
					 
				
			 
			
		
	

	
		
			 
		
		
                         
                                 
                                        RELATED INTERNET LINKS:
                                        
                                 
                         
                         
                                
                                         
                                                
                                                        International Cricket Council
                                                        
                                                
                                         
                                
                                         
                                                
                                                        Sri Lanka Cricket
                                                        
                                                
                                         
                                
                                         
                                                
                                                        India Cricket
                                                        
                                                
                                         
                                
                         
                        
                                
                                         
                                                The BBC is not responsible for the content of external internet sites
                                         
                                        
                                
                        
                
	



 






                    
                
                
            
        
    


	 
         
	 
	BBC PRODUCTS AND SERVICES 
	Daily and weekly e-mails | Mobiles | Desktop Tools | News Feeds | Interactive Television | Downloads
	 
 
 
	 
 BBC Copyright Notice 
 MMVII 
 
	 Back to top ^^ 
 


        
      

	
	Sport Homepage
	 | 
	


	
	Football
	 | 
	


	
	Cricket
	 | 
	


	
	Rugby Union
	 | 
	


	
	Rugby League
	 | 
	


	
	Tennis
	 | 
	


	
	Golf
	 | 
	


	
	Motorsport
	 | 
	


	
	Boxing
	 | 
	


	
	Athletics
	 | 
	


	
	Snooker
	 | 
	


	
	Horse Racing
	 | 
	


	
	Cycling
	 | 
	


	
	Disability Sport
	 | 
	


	
	Olympics 2012
	 | 
	


	
	Sport Relief
	 | 
	


	
	Other Sport...
	
	  
  
    

         
	 
	
	Help | 
	Privacy &amp; Cookies Policy | News sources | About the BBC | Contact us
	 
 




 







         
	 




