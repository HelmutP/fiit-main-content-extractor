

 "Return fees for car leases 'absolutely outrageous', consumer warns

 
 Last Updated: 

Monday, October 27, 2008 |  8:12 PM ET

 

 

CBC News
 

				
				 
					

 Some drivers nearing the end of their car-leasing contracts may want to brace themselves for some unexpected big bills, a fellow consumer warns.   
'To be told that my car, which was in good condition, needed $4,250 worth of work is absolutely outrageous.'???Susan Scott, consumer
   Montreal-based Susan Scott said she was presented with a hefty bill after she returned her Chrysler mini-van to the dealership. 
 ""To be told that my car, which was in good condition, needed $4,250 worth of work is absolutely outrageous,"" Scott said. 
 ""I went through the roof."" 
 George Iny, president of the Automobile Protection Association, said many companies have been hit with the dropping value of previously leased vehicles. 
 ""They're trying to cover some of their losses on the re-marketing of the vehicles,"" he said, adding some consumers have been presented with bills weeks after their vehicles were returned and resold at auction.  
   
 
 
 'Chrysler should be ashamed of themselves, and many will remember this outrageous 'client service' when they go to purchase another car down the road. ' 
 ??? Liz/Toronto 
 
Add your comment
 
   
 

 New car leases used to account for nearly half of a dealership's business, but in a few months, that proportion has dropped to less than 20 per cent, according to auto analyst Richard Cooper of JD Power and Associates. 
 Auto Check Canada, a company that specializes in lease returns, calculated the repairs on Scott's van could be completed for half of what Chrysler estimated. 
 Ken Selcer, president of Auto Check Canada, said consumers are best off getting a second opinion. 
 ""Once they know that a third party is involved, [the dealerships] tend to back away,"" he said."
"2488","CBC News


				 
 
				 
					

 Some drivers nearing the end of their car-leasing contracts may want to brace themselves for some unexpected big bills, a fellow consumer warns.   
'To be told that my car, which was in good condition, needed $4,250 worth of work is absolutely outrageous.'???Susan Scott, consumer
   Montreal-based Susan Scott said she was presented with a hefty bill after she returned her Chrysler mini-van to the dealership. 
 ""To be told that my car, which was in good condition, needed $4,250 worth of work is absolutely outrageous,"" Scott said. 
 ""I went through the roof."" 
 George Iny, president of the Automobile Protection Association, said many companies have been hit with the dropping value of previously leased vehicles. 
 ""They're trying to cover some of their losses on the re-marketing of the vehicles,"" he said, adding some consumers have been presented with bills weeks after their vehicles were returned and resold at auction.  
   
 
 
 'Chrysler should be ashamed of themselves, and many will remember this outrageous 'client service' when they go to purchase another car down the road. ' 
 ??? Liz/Toronto 
 
Add your comment
 
   
 

 New car leases used to account for nearly half of a dealership's business, but in a few months, that proportion has dropped to less than 20 per cent, according to auto analyst Richard Cooper of JD Power and Associates. 
 Auto Check Canada, a company that specializes in lease returns, calculated the repairs on Scott's van could be completed for half of what Chrysler estimated. 
 Ken Selcer, president of Auto Check Canada, said consumers are best off getting a second opinion. 
 ""Once they know that a third party is involved, [the dealerships] tend to back away,"" he said. 
 

