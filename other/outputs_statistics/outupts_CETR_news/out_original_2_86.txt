

 "
//W3C//DTD HTML 4.0 Transitional//EN""&gt;

	 

	horticulture - australianhorticulture



	    
		
	

	
		
		
			
			
			
			
			
		
		
 

 
				
			
									
						
							
								
									yourguide - jobsguide - autoguide - ruralpropertyguide - farmonline
								
							
						
					
				
				
				    
                        
	
		
		
		
	
	
		
	


				    
				
				
					
					    
                        
                            
                                
                                    
                                
                            
                            
                                
                            
                            
                                
                                    
	                                    
		                                    
			                                    
				                                    
					                                    Wed, Oct 29 2008
				                                    
			                                    
		                                    
		                                    
			                                    
				                                    
					                                    
				                                    
				                                    
					                                    
				                                    
			                                    
		                                    
	                                    
                                    	
	                                    
		                                    	
			                                    
				                                    
					                                    
                                    						
							                                    Articles
								                                    
							                                    
							                                    Your Say
							                                    
							                                    Weather
							                                    
							                                    What's On
							                                    
							                                    Links
							                                    
							                                    Library
							                                    
							                                    Search
							                                    
							                                    							
						                                    
					                                    
				                                    
			                                    
		                                    
	                                    
	                                    
		                                    
			                                    
				                                    
					                                    
									                        
									                            
									                            
									                            Join Us
									                            
									                            Subscription
									                            
									                            Contact Us
									                        																													
					                                    
				                                    
			                                    
		                                    
	                                    
	                                    
		                                    
			                                    		
				                                    	
					                                    
						                                    	
							                                    																								
							                                    
									                                    Horticulture Online
								                                    
									                                    
Australasian Flowers 
								                                    
									                                    
Australian Horticulture 
								                                    
									                                    
Good Fruit &amp; Vegetables 
								                                    
									                                    
Irrigation and Water Resources 
								                                    
									                                    
Turfcraft 
								                                    						
							                                    	
						                                    
					                                    		
				                                    
			                                    
		                                    
	                                    
                                    
                                
                            
                            
                                
                            
                            
                        
					    
					
					
					
						    
						
                        


	
		
			
				
					
Hort students called to apply for scholarships
 
						
							By , Australian Horticulture
							 
							Edition 343, 6/08/2008 11:53:01 AM
						

				
				
					
						
							
								
									
										
											
																						
											
										
										
											
												
		
										
									
								
							
							
								
								
							
							
								
									
										
											
												
											
										
										
											
												
		
										
									
								
							
						
						TWO lucky horticultural students could find themselves in Canberra next year for the Urban Greenscapes Symposium and the Nursery and Garden Industry national conference and exhibition from February 17-19.  It is the first time such an offer has been made through the industry?s Certified Nursery Professional (CNP) program.   
 Proudly supported by Australian Horticulture and Ramm Botanicals, the scholarship offers students an early understanding of the benefits of ongoing professional development.  
 Some students may in fact already qualify as CNPs if they have been working and studying simultaneously. 
 To be eligible for the scholarship the student entering must be in their final year of study, working towards a qualification in horticulture (Certificate III or above).  
 The eligible qualification would specialise in retail nursery, production/wholesale nursery, arboriculture, parks and gardens, or the general horticulture qualification. 
 The CNP Scholarship program aims to demonstrate to our future leaders the industry?s professionalism and allows students to explore all the potential career paths that are on offer.   
 Further, it provides inspiration to students and exposes them to the benefits of participation in a program such as CNP. 
 All education institutions are encouraged to nominate a student for consideration by Nursery and Garden Industry Australia?s panel of judges.  The panel will identify a finalist for both regional and metropolitan areas in each state.  
 From the finalists, two national scholarships will be awarded - a regional scholarship and a metropolitan scholarship. 
 Nominations for this exciting opportunity close September 19, 2008. 
 For more information contact the CNP Coordinator on (02) 9876 5200 or via email at cnp@ngia.com.au 
 The CNP program is funded jointly by the Nursery Industry Levy and the Commonwealth Government via Horticulture Australia and is proudly supported by Australian Horticulture magazine and Ramm Botanicals. 
 Australian Horticulture will be following the program closely, with profiles on finalists and the ultimate scholarship recipients in coming issues of the magazine. 

				
				
					
						
						
							Comment on this article +
						
						
							
						
						
						
						
						
						
					
				
			
		
	





					
					
					
						
						
						
								
							
						
						
					
				
				
					
						
							
								
									Privacy information - Copyright © 2005 Rural Press Limited
								
							
												
					
				
			
		
	


