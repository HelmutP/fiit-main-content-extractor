

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
  
  Sunday Independent
  
  
  


  
  
  
  
  
  
  
  
  




   
     
      IOL News
       | 
      IOL Sport
       | 
      IOL Business
       | 
      IOL Jobs
       | 
      IOL Entertainment
       | 
      IOL Travel
       | 
      IOL Motoring
       | 
      IOL Property
       | 
      IOL Parenting
       | 
      IOL Classifieds
       | 
      More 
     
   
      
           
     
        
       
          
         
          News |           Opinion |           Entertainment          Sport         
       
     

     
     

       
      
       
         Four held for robbing supermarket 
         28 October 2008, 19:06 
         
           
         
         
	  Four men were arrested on Tuesday on armed robbery charges in Temba near Hammanskraal, the Gauteng department of community safety said. 
 
Spokespoerson Mandla Radebe said the men were arrested on charges of robbing a supermarket in Morokolong, Temba on October 21. 
 
The men were arrested in various houses and sections in Temba. 
 
A Striker shotgun and a .38 Special revolver were recovered. Thirty eight rounds of rifle ammunition and six 9mm pistols rounds were found during the arrests. 
 
The four men would appear before the Temba magistrate's court on Wednesday. 
 
Gauteng Traffic Police and the Temba SAPS Trio Task Team carried out the joint operation. - Sapa						
         
         
	   EMAIL STORY
	     
	   EASY PRINT
	     
	   SEARCH
	     
	   NEWSLETTER
	 
	  
       

       


       
         
          
            Search Sunday Independent:
            
            
            
          
	   
            login |
            subscribe
	   
	 
	     
     	      
             
               The Week Past - October 26, 2008 
              The Week Past - October 26, 2008
               
                 
Times are tough, but we're dreaming of a high-tech Christmas
 
                 
Top cop Selebi a frequent visitor to HQ
 
                 
The Week Ahead - October 26, 2008
 
               
             
			 
Subscribe today &amp; get full access to our Premium content 
 
            
              
             

 

 
	
             
           

           
              
             
               
                 Today's Front Page 
                 Browse By Page 
                 Past Front Pages 
                 Supplements 
                 Subscribe Now 
               
             

		

           

            
             
            
           

         

         
         
           About Sunday Independent 
           
             
               About Us 
               Advertising 
               Readership 
               Booking Deadlines 
               Contact Us 
               Terms &amp; Conditions 
             
           
           Sister Newspaper Sites 
           
             
               Cape Argus 
               Cape Times 
               Daily News 
               Isolezwe 
               Post 
               Pretoria News 
               Sun. Independent 
               Sunday Tribune 
               The Independent on Saturday 
               The Mercury 
               The Star 
             
           
          
         
                 
 

         
           Online Services 
           
             
               Accessories 4 Outdoors 
               Audio, TV, GPS &amp; PS3 etc 
               Books, CDs and DVDs 
               Car Hire 
               Car Insurance 
               Car Insurance for Women 
               Cars Online 
               Compare Insurance Quotes 
               Education 
               Gold-Reflect who you are 
               Home Loans 
               Lease Agreements 
               Life Insurance 
               Life Insurance for Women 
               Medical Aid 
               Offer to Purchase 
               Online Shopping 
               Play Euro &amp; UK Lotteries 
               Property Search 
               Residential Property 
             
           
           Mobile Services 
           
             
               IOL On Your Phone 
               Mobile Games 
               Personalise Your Phone 
             
           
         

       


       
          
          
         

            
                
                
                

            
            Date Your Destiny
            
                 
           
            
                
                
                
                 

               
                  
                
                 
                  I'm a 40 year old man looking to meet women between the ages of 25 and 41.
                
              
              
           
           
         
      
 






  
    

		
			
			
			
			 
				
					Select Province &amp;
					Eastern Cape
					Free State
					Gauteng (PWV)
					KwaZulu Natal
					Mpumalanga (E. Tvl)
					North West
					Northern Cape
					Northern Province
					Western Cape
					------------------
					Botswana
					Lesotho
					Malawi
					Namibia
					Swaziland
					Zimbabwe
					Other Country (Not in SA)
				
			 
		
	
  
  
	
  
  
      
  
  
    
		 
			A 
			B 
			C 
			D 
			E 
			F 
			G 
			H 
			I 
			J 
			K 
			L 
			M 
			N 
			O 
			P 
			Q 
			R 
			S  
			T 
			U 
			V 
			W 
			X 
			Y 
			Z
		 
	
  
  
      
  
	 
       

 
         
           
              
             
               IOL JOBS 
               
                 Building a new future through passion  
                 Your performance is always measured  
                 Jobs database aids another young hopeful  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL TRAVEL 
               
                 More than a can of worms  
                 I've got you under my skin  
                 Unwind under the African sky  
               
             
            
           
         
       
       
         
           
              
             
               IOL TECHNOLOGY 
               
                 Nokia gives away smartphone technology  
                 Thailand to block websites insulting royals  
                 Intel to invest in solar venture  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL MOTORING 
               
                 'Hamilton prime to beat my title record' - Schumie  
                 Two new reasons for watching A1GP this season  
                 UK show warning for JIMS trade participants  
               
             
            Sign up for our FREE newsletter
           
         
       
       
         
           
              
             
               IOL PARENTING 
               
                 SA's shocking teen sex stories revealed  
                 Japanese parents try to marry off children  
                 Travel a major cause of accidental deaths  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL SPORT 
               
                 De Villiers isn't gambling on Pienaar, Smit  
                 Torres still hamstrung  
                 Pakistan fails to secure Windies Tests  
               
             
            
           
         
       
       
         
           
              
             
               IOL ENTERTAINMENT 
               
                 Chemistry between blonde trio will be electric  
                 'He either buys or rents his own jet'  
                 Christian signs up for Strange role  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               BUSINESS REPORT 
               
                 Stocks claw back lossed despite running out of steam  
                 Farming enterprises on BEE road  
                 Busa woos Chinese investors  
               
             
            Sign up for our FREE newsletter
           
         
		
		

		
	
		
		
       
       
       
RSS Feeds  |  Free IOL headlines  |  Newsletters  |  IOL on your phone  |  Group Links  
       
         
          © 2008 Sunday Independent &amp; Independent Online (Pty) Ltd. All rights reserved. 
          Reliance on the information this site contains is at your own risk.  
          Please read our Terms and Conditions of Use and Privacy Policy. 
		  Independent Newspapers subscribes to the South African Press Code that prescribes news that is truthful, accurate, fair and balanced. If we don't live up to   the Code please contact the Press Ombudsman at 011 788 4837 or 011 788 4829.
         
         

		
		
		 
       
		  
		
		
       
     

   
  

 


  






