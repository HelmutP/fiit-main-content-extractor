

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
  
  Cape Times
  
  
  


  
  
  
  
  
  
  
  
  




   
     
      IOL News
       | 
      IOL Sport
       | 
      IOL Business
       | 
      IOL Jobs
       | 
      IOL Entertainment
       | 
      IOL Travel
       | 
      IOL Motoring
       | 
      IOL Property
       | 
      IOL Parenting
       | 
      IOL Classifieds
       | 
      More 
     
 
        
           
     
        
       
          
         
	  News |
	  Opinion |
	  Entertainment |
	  Sport
         
       
     

     

     

       
      
       
         Judgment date set for Pretoria murder 
         27 October 2008, 19:04 
         
           Related Articles 
           
             
               2 murder accused to learn fate next week 
               Justice waits for Odendaal murder accused 
             
           
           
         
         
	  Judgment will be delivered on Tuesday in the trial of two men accused of murdering Pretoria resident Kathy Odendaal. 
 
Prosecutor Petra van Basten argued in the Pretoria High Court on Monday that the evidence was overwhelming that Alfred Matlala, 23, and Tshepo Dithung, 29, killed Odendaal on October 16 last year. 
 
Odendaal's blood was found on the clothing of both accused. 
 
Van Basten argued that the court should also convict the two of attempting to rape the mother of two. 
 
She said the only logical conclusion from the way in which the victim was found spread-eagled and half-naked was that her attackers had not only stabbed and strangled her, but also tried to rape her. 
 
A security guard caught Dithung jumping over a fence at Odendaal's Lynnwood Manor home shortly after the murder. 
 
Three men who worked for a refuse removal company caught Matlala the same afternoon after seeing him robbing a woman in a Lynnwood street. 
 
Police found Odendaal's car keys and jewellery in his pockets. 
 
Matlala was also caught in possession of a firearm stolen three days  earlier from a house in the area, at which Dithung left his fingerprints behind. 
 
The firearm was allegedly used to murder Wilson Mapadahanya in a Lynnwood Street and to rob Odendaal's neighbours the same day. 
 
Matlala's blood was found at the scene of the robbery at Odendaal's neighbour, the Department of Trade and Industry's chief of staff Moosa Ebrahim. 
 
The three-year-old daughter of Ebrahim's domestic worker was shot in the back during the robbery. 
 
Van Basten asked the court to convict the accused on all 15 charges against them, including five of robbery relating to the attack at the Ebrahim home. 
 
She said although it had been one robbery, it was not a duplication of charges because all of the victims had been threatened or assaulted and then robbed separately. 
 
Defence advocate Herman Alberts said the state's case was not above criticism, although the evidence against his clients appeared to be overwhelming, 
 
He said it remained his instructions that his clients were innocent. 
 
Alberts criticised fingerprint and ballistic evidence linking his clients to various crimes. He said that in both instances the court was  faced with conflicting reports from experts. 
 
Acting Judge Chris Eksteen postponed the trial until Tuesday for judgment. - Sapa
         
         
	   EMAIL STORY
	     
	   EASY PRINT
	     
	   SEARCH
	     
	   NEWSLETTER
	 
	  
       

       


       
         
          
            Search Cape Times:
            
            
            
          
	   
            login |
            subscribe
	   
	 
	     
     	      
             
               Court rules Norman Simons was likely killer of six young boys 
              It was likely that Norman Afzal Simons was responsible for the deaths of six young boys counted among the victims of the notorious Station Strangler ...
               
                 Rand recovers, but still vulnerable 
                 1 000s flee Congo war 
                 Shilowa calls for political tolerance in run-up to formation of new party 
               
             
			 
Subscribe today &amp; get full access to our Premium content 
 
            
              
             

 

 
	
             
           

           
              
             
               
                 Today's Front Page 
                 Browse By Page 
                 Past Front Pages 
                 Supplements 
                 Subscribe Now 
               
             

		

           

            
             
            
           

         

         
         
           About Cape Times 
           
             
               About Us 
               Advertising 
               Booking Deadlines 
               Contact Us 
               Readership 
               Marketing 
               Terms &amp; Conditions 
             
           
           Sister Newspaper Sites 
           
             
               Cape Argus 
               Cape Times 
               Daily News 
               Isolezwe 
               Post 
               Pretoria News 
               Sun. Independent 
               Sunday Tribune 
               The Independent on Saturday 
               The Mercury 
               The Star 
             
           
          
         
                 
 

         
           Online Services 
           
             
               Accessories 4 Outdoors 
               Audio, TV, GPS &amp; PS3 etc 
               Books, CDs and DVDs 
               Car Hire 
               Car Insurance 
               Car Insurance for Women 
               Cars Online 
               Compare Insurance Quotes 
               Education 
               Gold-Reflect who you are 
               Home Loans 
               Lease Agreements 
               Life Insurance 
               Life Insurance for Women 
               Medical Aid 
               Offer to Purchase 
               Online Shopping 
               Play Euro &amp; UK Lotteries 
               Property Search 
               Residential Property 
             
           
           Mobile Services 
           
             
               IOL On Your Phone 
               Mobile Games 
               Personalise Your Phone 
             
           
         

       


       
          
          
         

            
                
                
                

            
            Date Your Destiny
            
                 
           
            
                
                
                
                 

               
                  
                
                 
                  I'm a 32 year old woman looking to meet men between the ages of 27 and 40.
                
              
              
           
           
         
      
 






  
    

		
			
			
			
			 
				
					Select Province &amp;
					Eastern Cape
					Free State
					Gauteng (PWV)
					KwaZulu Natal
					Mpumalanga (E. Tvl)
					North West
					Northern Cape
					Northern Province
					Western Cape
					------------------
					Botswana
					Lesotho
					Malawi
					Namibia
					Swaziland
					Zimbabwe
					Other Country (Not in SA)
				
			 
		
	
  
  
	
  
  
      
  
  
    
		 
			A 
			B 
			C 
			D 
			E 
			F 
			G 
			H 
			I 
			J 
			K 
			L 
			M 
			N 
			O 
			P 
			Q 
			R 
			S  
			T 
			U 
			V 
			W 
			X 
			Y 
			Z
		 
	
  
  
      
  
	 
       

 
         
           
              
             
               IOL JOBS 
               
                 Building a new future through passion  
                 Your performance is always measured  
                 Jobs database aids another young hopeful  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL TRAVEL 
               
                 More than a can of worms  
                 I've got you under my skin  
                 Unwind under the African sky  
               
             
            
           
         
       
       
         
           
              
             
               IOL TECHNOLOGY 
               
                 Nokia gives away smartphone technology  
                 Thailand to block websites insulting royals  
                 Intel to invest in solar venture  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL MOTORING 
               
                 'Hamilton prime to beat my title record' - Schumie  
                 Two new reasons for watching A1GP this season  
                 UK show warning for JIMS trade participants  
               
             
            Sign up for our FREE newsletter
           
         
       
       
         
           
              
             
               IOL PARENTING 
               
                 SA's shocking teen sex stories revealed  
                 Japanese parents try to marry off children  
                 Travel a major cause of accidental deaths  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL SPORT 
               
                 De Villiers isn't gambling on Pienaar, Smit  
                 Torres still hamstrung  
                 Pakistan fails to secure Windies Tests  
               
             
            
           
         
       
       
         
           
              
             
               IOL ENTERTAINMENT 
               
                 Chemistry between blonde trio will be electric  
                 'He either buys or rents his own jet'  
                 Christian signs up for Strange role  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               BUSINESS REPORT 
               
                 Stocks claw back lossed despite running out of steam  
                 Farming enterprises on BEE road  
                 Busa woos Chinese investors  
               
             
            Sign up for our FREE newsletter
           
         
		
		

		
	
		
		
       
       
       
RSS Feeds  |  Free IOL headlines  |  Newsletters  |  IOL on your phone  |  Group Links  
       
         
          © 2008 Cape Times &amp; Independent Online (Pty) Ltd. All rights reserved. 
          Reliance on the information this site contains is at your own risk.  
          Please read our Terms and Conditions of Use and Privacy Policy. 
		  Independent Newspapers subscribes to the South African Press Code that prescribes news that is truthful, accurate, fair and balanced. If we don't live up to   the Code please contact the Press Ombudsman at 011 788 4837 or 011 788 4829.
         
         

		
		
		 
       
		  
		
		
       
     

   
  

 


  






