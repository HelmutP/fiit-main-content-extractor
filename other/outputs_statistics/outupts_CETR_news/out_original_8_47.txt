

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;



 
    
    
    
    Hunting down local wines -- chicagotribune.com



    
     www.chicagotribune.com/news/opinion/columnists/chi-uncorked_local_22aug22,0,893797.column 
    
     chicagotribune.com 
    
    
    
     Hunting down local wines 
    
    
    	 Midwest vines yield options for locavores 
    
    
    
        Bill Daley 
       
       
    
    
    
    
    
          August 22, 2007 
    
    
    
    
     
         
        
 
         Does eating local extend to drinking local—local wine, that is? It will be interesting to see if there's an uptick in sales of Midwest wine next month as various sustainable food groups, including Chicago's Green City Market, ""challenge"" consumers to rely mostly on locally sourced foods.  Coincidently, September is the third annual Illinois Wine Month with various festivals, tastings and other activities planned to promote the state's wine industry. There are more than 68 wineries in Illinois producing some 500,000 gallons of wine a year, according to the Illinois Grape Growers and Vintners Association.  As with food, the question for locally minded Chicagoans may be how ""local"" is ""local"" wine. For many, ""local"" in terms of food means Illinois and neighboring states, especially Wisconsin, Indiana and Michigan. That works with wine—to a point. Not all wineries use locally grown grapes.  To be sure, look for the name of the specific county, state or wine region (called American Viticultural Area, or AVA) on the label. Most labeling regulations allow a certain percentage of ""outside"" grapes in the mix, 15 percent for a wine labeled with an AVA designation, 25 percent for a wine with a state or county appellation of origin. (The Feds have all the details. Go to the Wine Appellations of Origin page on the Web site of the Alcohol and Tobacco Tax and Trade Bureau, a division of the U.S. Department of the Treasury: ttb.gov/appellation.)  Illinois currently has one AVA, the Downstate Shawnee Hills. Wisconsin has the Lake Wisconsin AVA in the southeast corner of the state. Indiana has the four-state Ohio River Valley AVA running along its southern boundary and Michigan has four AVAs: Fennville, Lake Michigan Shore, Leelanau Penninsula and Old Mission Penninsula.  Michigan wines have arguably the highest and brightest profiles of any in the Midwest. Tom Benezra, wine director for Sal's Beverage World stores, said that Michigan wines are the most popular Midwestern wines there.  ""They are mostly simple, fruity wines, on the sweet side and low in alcohol for easy drinking,"" he said. ""Many Chicagoans have second homes or favorite get-away spots in Michigan where they go and often return having tasted one or more of these wines.""  Benezra said that Michigan riesling has potential for higher, ""possibly world-class"" quality, but limited production and Michigan's tight wine shipping laws are keeping them scarce around Chicago.  Tracy Lewis Liang, director of wine and spirits for Treasure Island Foods, also points to northern Michigan whites.  ""No decent reds,"" she insisted, ""but who cares when you've got a good riesling?""  I'd respectfully disagree there, especially since I still dream about the intense 2005 pinot noir produced by Brys Estate out on Old Mission Penninsula. And, after putting in a third turn as a wine judge for the Indy International Wine Competition in Indianapolis last month, I can tell you there are a number of notable wines made all over the region.  For example, a 2005 Norton from Downstate Mary Michelle Winery's Illinois Cellars line took top honors when the Good Eating staff matched with Chinese spare ribs a couple of weeks ago. I really liked the refreshing taste of a 2005 Traminette, a white wine made by Stone Hill Winery of Missouri; it was tops in its class at the Indy contest. And the upper tier line of L. Mawby's sparkling wines from Michigan's Leelanau has a panache akin to the Frenchiest of Champagnes.  ""Local"" may not be easy to define but the concept is certainly worth exploring, wherever it leads you across the vineyards of the Midwest.  wdaley@tribune.com     Bill Daley answers questions on wine, beer and spirits every Sunday in Q.    
     
    
     Copyright © 2007, Chicago Tribune 






