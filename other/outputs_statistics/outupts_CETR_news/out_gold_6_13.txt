

 Former media mogul Conrad Black may remain free on bond pending sentencing on his conviction for swindling the far-flung Hollinger International newspaper empire he once ran out of millions of dollars, a federal judge ruled Thursday. 
 Black, 62, who once renounced his Canadian citizenship to become a member of the British House of Lords, was found guilty last Friday by a federal jury of three counts of mail fraud and one count of obstruction of justice for spiriting documents out of his Toronto office in defiance of a court order. 



 



















 » Click to enlarge image
 





 A judge said Thursday Conrad Black may remain free on bond pending sentencing.  (AP)
 
 







	
	
	








 
 RELATED STORIES 

� Special section: Conrad Black on trial 

	
	

	

	

	













	
 		



 Black was acquitted of nine other counts ranging from tax fraud to the most serious charge -- racketeering. He was also acquitted of fleecing Hollinger shareholders through such perks as taking the corporate jet on a two-week vacation to the island of Bora Bora. 
 ''I find clear and convincing evidence that Mr. Black is not going to flee,'' U.S. District Judge Amy St. Eve said. 
 But she imposed strict travel limits on Black. He must remain in the Chicago area or in southern Florida, where he has a Palm Beach estate. He may not leave the United States. 
 The government had argued that Black posed a flight risk because he did not have enough money to post a bond large enough to keep him from running away, and he faced the possibility of a long jail term. 
 Prosecutors have said Black faces up to 30 years in jail when he is sentenced Nov. 30. Defense attorneys have said the actual sentence would likely be much less. 
 ''Simply put, Mr. Black faces the potential of spending the rest of his life in jail,'' prosecutors said in court documents. 
 Three other former Hollinger executives, John Boultbee, 65, of Victoria, British Columbia, Peter Y. Atkinson, 60, of Oakville, Ontario, and Mark Kipnis, 59, of Northbrook, Ill., were also convicted of fraud charges after the three-month trial that drew international media attention. 
 Edward Greenspan, Black's Canadian defense attorney, has promised an appeal on ''viable legal issues.'' 
 Hollinger International, based in Chicago and renamed Sun-Times Media Group Inc. last year, was at one time one of the world's largest publisher of community newspapers as well as the Chicago Sun-Times, the Daily Telegraph of London and Israel's Jerusalem Post. 

