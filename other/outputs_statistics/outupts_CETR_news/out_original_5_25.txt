


 WASHINGTON (Reuters) - Democrats will limit this week's Iraq debate in the U.S. House of Representatives to a resolution that disapproves of President George W. Bush's plan to increase troop levels, the House Democratic leader said on Sunday. 
"" &gt;






  
    
    
    
    
    
    
  	
    
    


 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:30PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      Politics
      
      &gt;
      Article
 
 
 
       House Iraq debate limited to troop increase: Hoyer 
       Sun Feb 11, 2007 12:39PM EST 
      
     

     
 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		Politics News
     
   
   
		 
				Gates recalls Cold War in push for NATO spending

			 
		 
				Empowered Democrats examine Bush administration

			 
		 
				Bush urges Congress to approve energy proposals

			 
		 
				Turkey says U.S. Armenian bill would hurt ties

			 
		 More Politics News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     WASHINGTON (Reuters) - Democrats will limit this week's Iraq debate in the U.S. House of Representatives to a resolution that disapproves of President George W. Bush's plan to increase troop levels, the House Democratic leader said on Sunday. 

    


 ""We're going to members with a full debate: Tuesday, Wednesday, Thursday and Friday to give their opinion and then to vote: do you agree with the president's proposal,"" House Majority Leader Steny Hoyer of Maryland said on NBC's ""Meet the Press."" 

    


 Republicans have pressed for a substitute measure that would prevent lawmakers from cutting off funds for the 21,500 additional troops included in Bush's plan to curtail rising violence in Iraq. 

    


 ""Why not allow Republicans to bring a resolution to the floor and let the House vote up or down on that resolution?"" asked House Minority Leader John Boehner, an Ohio Republican who appeared with Hoyer on the program. 

    


 But Hoyer said Democrats wanted to avoid the kind of procedural fight that recently tied the Senate in knots over how to debate the issue and forced senators to shelve the issue of a nonbinding resolution on Bush's strategy. 

    


 The Democrat said allowing a Republican measure was ""not necessarily our plan at this point in time."" 

    


 ""We want a very straightforward, clear answer to the question: do you support the president's escalation,"" he said. 

    


 Hoyer said the Republicans will get their chance within the next 30 to 45 days.  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				 
U.S.-led forces show evidence of Iran arms in Iraq
 
				 BAGHDAD (Reuters) - Officials of the U.S.-led coalition on Sunday showed what they said were examples of Iranian weapons used to kill 170 of their soldiers and implicated high-level Iranian involvement in training Iraqi militants. 
                
                    Full Article  |  Video  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















