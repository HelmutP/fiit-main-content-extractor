









 




 














 







  


 July 29, 1999 
 

 
LIBRARY / CROSSWORD CD-ROM'S
 


 Building a Crossword: Tools and Materials
 

 


  By ALICE KEIM 

 
 
 
 or some, crosswords are part
obsession and part self-torture,
a kind of mental steeplechase.
For others -- and I am one, even
though I am not particularly adept at
them -- they are merely a pleasant
diversion, an afternoon jog that stimulates the mind while exercising it.
 
 
 
 

 
 

 

 

Claire Yaffa for The New York Times





Overview
  � Building a Crossword: Tools and Materials
 
 
This Week's Reviews
   � Crossworks
 
  � Crossword Weaver
 
  � Crossword Studio 3.0
 
  � Crossword Magic
 
 
 



 
  For teachers and parents, crosswords can be a wonderful teaching
tool.
 
  Social studies teachers create
puzzles based on historical facts or
geographical details. Language-arts
teachers use crossword puzzles to
test vocabulary or reading comprehension. The crosswords can also be
used by parents and teachers to create puzzles with holiday or other
themes. Birthday parties can include
puzzles with facts about the guest of
honor, like ""friend of Beavis (8 letters).""
 
  Since it can be hard to make a puzzle work in the crossword's characteristic grid of black and white
squares, software can be a great
help. Many of the available programs will let users import graphics
or customize the layout to produce
crossword-based cards and invitations (guests must complete the puzzle to uncover the details of the
event). Many programs also let users put their puzzles on the Web.
 
  With many programs to choose
from, you should be clear about what
you are looking for before you purchase. Some programs excel in layout and personalization, while others
include large dictionaries to help you
come up with word options. Some
programs are geared more toward
helping you solve puzzles than toward creating them. Most create
puzzles that can be solved on a computer or printed out and worked on
paper.
 
  Keep in mind that no program can
be brilliant or clever for you. While
the program can provide the basic
design, the real genius of crossword
creation is ultimately up to the creator.
 
 
 Alice Keim, a teacher, is the technology coordinator for the Horace Mann 
Lower School in the Bronx.
 

 
  
 

 






 
 
  
  




 
 


Home |
Site Index |
Site Search |
Forums |
Archives |
Marketplace
  
Quick News |
Page One Plus |
International |
National/N.Y. |
Business |
Technology |
Science |
Sports |
Weather |
Editorial |
Op-Ed |
Arts |
Automobiles |
Books |
Diversions |
Job Market |
Real Estate |
Travel
 
 
Help/Feedback |
Classifieds |
Services |
New York Today
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















