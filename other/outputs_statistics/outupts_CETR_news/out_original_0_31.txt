

 //W3C//DTD HTML 4.0 Transitional//EN"" ""http://www.w3.org/TR/REC-html40/loose.dtd""&gt;

 
BBC NEWS | Europe | France's Royal unveils manifesto






    
    












 


BBC NEWS / EUROPE
 
Graphics Version | Change to UK Version | BBC Sport Home

 


    


	
	
	
	
		
			
		      	
		       	News Front Page
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Africa
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Americas
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Asia-Pacific
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Europe
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Middle East
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	South Asia
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	UK
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Business
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Health
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Science/Nature
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Technology
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Entertainment
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Video and Audio
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	
		
			
		      	
		       	Have Your Say
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	



    


 

Sunday, 11 February 2007, 15:43 GMT

 France's Royal unveils manifesto 




	

	
		
	


The Socialist candidate in France's presidential election, Segolene Royal, has launched her manifesto in Paris. 
 
She announced a 100-point platform with a strong emphasis on social programmes, promising a higher minimum wage and the construction of more low-rent housing.
 
 
Ms Royal has been criticised for delaying the release of her platform until just 10 weeks before the first round of elections.
 
 
She has fallen behind her right-wing rival Nicolas Sarkozy in opinion polls.

 
 
Ms Royal unveiled her ""presidential pact"" in front of a cheering crowd of Socialist Party delegates who frequently broke into chants of ""Segolene, president!""
 
 
""I feel today I can propose to you something more than a platform,"" she said.
 
 
""A pact of honour, a presidential pact that I propose to everyone, the most vulnerable and the strong, those who have been our supporters all along and those who have not, because France needs all its people.""
 
 
Many of the 100 points already feature in the Socialist Party's election programme released last year.
 
 
Others, such as proposals to set up citizens' juries to evaluate the work of the National Assembly and military-style boot camps for young offenders are new ideas.
 
 
'Listening phase'
 
 
Ms Royal said she wanted to boost the monthly minimum wage from 1,250 euros ($1,625, ?834) to 1,500 euros, and build 120,000 low-rent homes every year. 
 
 


	

	
		
	

Benefits for the unemployed would be raised and pensions increased for low-income retirees. 
 
 
Ms Royal has defended her decision to delay the release of her manifesto, saying she had been in a ""listening phase"" - gathering ideas from the people of France via the internet and thousands of public meetings across the country.
 
 
Segolene Royal was France's political star of the second half of last year in France. 
 
 
She had a meteoric rise thanks to her crowd-pleasing campaigning skills and trounced two more senior party figures in the Socialist domination for April's election. 
 
 
But since the New Year things have begun to unravel, reports Hugh Schofield. There have been gaffes, signs of internal dissent and Nicolas Sarkozy has surged ahead in the polls. 

 
 
 


    
        
            
                E-mail this to a friend
            
        
    

 


	
                
                        
                                Related to this story:
                        
                
                 
		
			
				
					
	
		
			
			
			
			Royal renews 'dirty tricks' plea
			
		
		
	

					
						
							
								(27 Jan 07 | 
								Europe
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Quebec 'gaffe' causes Royal grief
			
		
		
	

					
						
							
								(23 Jan 07 | 
								Europe
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Battle Royal looms for French change
			
		
		
	

					
						
							
								(17 Nov 06 | 
								Europe
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			France's Royal promises changes
			
		
		
	

					
						
							
								(17 Nov 06 | 
								Europe
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Royal battles for nomination
			
		
		
	

					
						
							
								(08 Nov 06 | 
								Europe
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			French rivals hold last TV debate
			
		
		
	

					
						
							
								(08 Nov 06 | 
								Europe
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Profile: Segolene Royal
			
		
		
	

					
						
							
								(17 Nov 06 | 
								Europe
								)
							
						
					
					 
				
			
		
		
	

	

 




    
     
        RELATED INTERNET LINKS 
        
            French presidency 
        
            French government 
        
            French Socialist Party 
        
            Segolene Royal think-tank 
                    	
        The BBC is not responsible for the content of external internet sites
        
    

 
 




SEARCH BBC NEWS: 



 

    


	
	
	
	
		
			
		      	
		       	News Front Page
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Africa
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Americas
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Asia-Pacific
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Europe
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Middle East
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	South Asia
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	UK
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Business
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Health
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Science/Nature
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Technology
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Entertainment
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Video and Audio
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	
		
			
		      	
		       	Have Your Say
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	



    

 
 
NewsWatch | Notes | Contact us | About BBC News | Profiles | History
 
^ Back to top | BBC Sport Home | BBC Homepage | Contact us | Help | ?




 

