

 "
					Entire magazine read in newsagency									
								 

					
						
				
							
				
					
			
						
			
			
		
					
				
					Friday, 01 August 2008				
			
					
			
				 
 
 
 A devastated newsagency in the wake of the intrusion 
 Despite the clear presence of a sign informing customers it
is "not a lending library", Erskineville Newsagency was visited by a no-good freeloader
last Tuesday afternoon. The offender, a non-descript mid-20s man in a business
suit, made no pretence of purchasing the August issue of RALPH, choosing instead to peruse the publication while standing in
the Men's Lifestyle section. 

 
Powerball syndicate member Aidan Cleveland witnessed the thrifty Caucasian male's entry. "He made a beeline straight for the rack," he said. "I
hadn't seen him in here before, but he knew his way around the place like he
was a local."  
 
 
New Idea fan
Janice Hanbury, who was selecting a giraffe-themed birthday card for her
three-year-old niece at the time of the intrusion, watched as the man read the
entire issue, from the contents all the way through to the What
I Know About... section on the back page. 
 
 
Adding insult to injury, the tight-fisted man even scanned
the magazine's ads, chuckling at a risque phone-sex offering. "He certainly enjoyed the issue," said Hanbury. "If he'd paid for it, he
would've really got his money's worth."  
 
 
Newsagent Ian Jameson, who earns several dollars from each
sale of RALPH, was powerless to act: "Normally
I would've walked over and ask if he needed any help, but I was busy selling
scratchies. By the time I thought to confront him, he was three-quarters of the
way through it. I just had to cross my fingers and hope he'd buy a tin of Eclipse
mints on his way out."
 
 
"He didn't," Jameson added.  
 
 
RALPH editor Santi
Pintado has used the opportunity to declare a victory over rival FHM. "This just goes to prove we're
still number one when it comes to providing quality reading material for Aussie
blokes. Even if they don't want to pay for it."
 
 
In the wake of the transaction-free invasion, Jameson has
vowed to keep an eye out for any reappearance of the rogue reader. "I've added
another strongly worded sign about purchasing things you plan to read, right
near the RALPH slot. Next time he'll think
twice before blocking my aisles with his shiftless, content-stealing carcass."  
 
 
The magazine-reading man was later spotted listening to CDs
on the complimentary headphones at HMV. 




