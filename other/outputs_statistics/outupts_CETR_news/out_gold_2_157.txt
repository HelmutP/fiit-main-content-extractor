

 "Campaign Fatigue? The Real Challenge Is Yet to Come
	 
 It's been a long and hard-fought campaign, leaving many too exhausted to cheer the winner through the gates. But despite the presumed victory, warns SPIEGEL ONLINE blogger Peter Ross Range, the real battle awaits Obama on the other side of Inauguration Day. 
	     
 

 
 
 
	
			 
		  
				 REUTERS 
		 Obama has waged a good fight on the way to the presidency. But he'll need a lot of energy for the fights yet to come. 
		 		
 I?m tuning out. I?m already shifting to post-campaign mode. 
 This morning?s Washington Post included what looked like a compelling piece by Anne Applebaum, one my favorite semi-conservative columnists. Didn?t read it. Another by liberal wordsmith Richard Cohen on how Sarah Palin was the invention of right-wing journalists on a cruise ship to Alaska? Didn?t read it. Still another by a former Republican US senator on why he's declaring his support for Obama? Clearly an important piece. Didn?t read it. 
 Campaign fatigue -- and a sense that, finally, the deal is done -- is catching up with me. This is what happens when you?ve followed an election with brutal intensity for so many months. Since January, I?ve spent several hours every morning digesting the flood of reports and commentary on this amazing election. It was addictive. I couldn?t stop. But then, a few days ago, I began going into sensory overload. Now, it seems, my systems are shutting down.  
 And I know the reason: I?ve finally accepted the idea that Barack Obama is -- improbably -- going to be our next president. After weeks and months of warning how easily the election could flip, I?m letting my guard down. My defenses have been up so long that my arms hurt. 
 
 
 
 RELATED SPIEGEL ONLINE LINKS 
			 
		 
		 
 
	The Lone Ranger: Is Political Activism Cool Again? (10/27/2008) 
 
 
 
	The Lone Ranger: Obamaphiles Worry About a McCain Comeback (10/24/2008) 
 
 
 
	The Lone Ranger: Democrats Dream of Historic Realignment (10/23/2008) 
 
 
		 
	 
I know this is exactly what I am not supposed to do. A lot can happen in six or seven days. ?No time to unpanic,? ur-blogger Mickey Kaus wrote on Slate.com. Obama knows it, too. Every day he warns the faithful not to stop the relentless labor of soliciting votes, putting up campaign signs, promising rides to the polling places -- ?not for one day or one minute or one second,? he said on Tuesday. 
 Still, a turning point has been reached. There?s a feeling that everything's over but the shouting. ?Coverage Of Election Now Lacks Suspense,? the New York Times noted Tuesday morning. ?We?re not saying that it?s over?? says a TV anchor person. ?But?.?  
 That ?but? is the giveaway.  
 After a period of concentrating on dreary policy pronouncements, the new ""Superman of American politics"" is closing with the inspirational rhetoric that lifted him so many months ago. Obama still touches on pocketbook issues, including tax cuts for the middle class and attacks on the oligarchs who brought on the financial collapse. But he ends with the preacher-like call and response that thrilled his early supporters. ?All fired up!? he shouts. And the crowd responds: ?All fired up!? ?Ready for change!? ?Ready for change!?  
 
 
 
 
 NEWSLETTER 
 
Sign up for Spiegel Online's daily newsletter and get the best of Der Spiegel's and Spiegel Online's international coverage in your In-  Box everyday. 
 
 

 
 
 
 
Reverting to the messianic Obama appears to be a beautifully orchestrated end game. It rekindles memories of what brought the young politician onto the national stage in the first place. Once again, he?s the Great Uniter, the New American, Future Man.
 If all goes according to plan, I'll be able to breathe easier and enjoy the ride for a few days -- or maybe only for a few hours. Then I?ll start worrying about how in God?s name President Obama --even with an overwhelmingly Democratic Congress -- will deal with a set of problems, foreign and domestic, whose magnitude are more daunting than even those faced by Franklin Roosevelt during the worst times of the Depression. Roosevelt had no wars on his doorstep.  
 Obama has asked for the keys to the kingdom, and now he almost has them. Let?s hope he knows how to use them. 
 

