

 LAUSANNE, Switzerland---- The race for the 2016 Summer Olympics is officially open. 
 
 The IOC launched the bid process Wednesday, inviting the world's 203 national Olympic committees to submit candidate cities. 
 
 Initial applications must be submitted to the International Olympic Committee by Sept. 13. Official bid files are due by Jan. 14, 2008. 
 
 Chicago, Tokyo and Rio de Janeiro, Brazil, have already been endorsed by their national Olympic committees as 2016 candidates. Other likely contenders include Rome; Madrid, Spain; and Prague, Czech Republic. 
 
 The IOC executive board will select the bid city finalists in June 2008. After an evaluation process, the full IOC will pick the host city on Oct. 2, 2009, in Copenhagen, Denmark. 

