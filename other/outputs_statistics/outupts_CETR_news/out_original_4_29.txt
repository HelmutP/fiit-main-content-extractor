









 




 














 




 
 
  
  







  


 May 6, 1999 
 

 LIBRARY / MOUNTAINEERING SITES
 


 Mountain Climbing as a Spectator Sport
 

 
 
 


  By PETER DIZIKES 

 
 
 
  rmchair mountaineering is a
time-honored tradition among
those seeking vicarious thrills
from the comfort of their own homes.
In the last few years, however, it has
been supplemented by a new occupation: desktop mountaineering, the
practice of following a climbing expedition on the Web as its members
try to reach the summit of one of the
world's greatest peaks. And May is
the start of the peak climbing season.
 
 
 

 
 

 





 
  Overview  
  � Mountain Climbing as a Spectator Sport
 
 
  This Week's Articles  
   � The Mountain Zone
 
  � Nova Online: Lost on Everest -- the Search  for Mallory and Irvine
 
  � Everest Internet Experiment 1999
 
  � Everest News 
 
  � BBC Online: Everest Online

 
 
 


 Mount Everest, for instance, has
seen a huge increase in foot traffic in
the 1990's. But record numbers of
people have also paid a virtual visit
to Everest, the world's highest
mountain, in the last few years. Some
20 expeditions (and at least 150
climbers) are trying Everest this
year -- some have already reached
the summit -- and more than a dozen
have their own Web sites, with journal-style dispatches, photos, route
maps, audio and video clips, and
thumbnail Everest histories. Far
from being a remote adventure,
high-altitude mountaineering is now
a spectator sport.
 
  It wasn't always this way, of
course.
 
 Sir Edmund Hillary, of New
Zealand, and Tenzing Norgay, a
Sherpa guide, were the first climbers
to reach Everest's summit, on May
29, 1953.
 
 The news of the feat, by a
British expedition, reached the public on June 2 (the day of Queen Elizabeth's inauguration, by happy coincidence) -- and the news moved that
fast only because The London Times
had sent a correspondent, James
Morris, to Nepal.
 
 
 
  Times have changed: James Morris now writes as Jan Morris, and
when Everest is climbed today, the
public knows about it before the
climbers are off the mountain.
Climbers radio from the top down to
base camp, where team members
call home on satellite phones. Then
expedition Web sites quickly post updates that read like breaking news,
even if there is a little lag time.
 
  Because of the Web, desktop
mountaineers now get a taste of the
unfolding anxiety and uncertainty
faced by real mountaineers. Take
Carolyn Hahn, a writer living in New
York, who, along with other family
members, uses the Web to follow the
feats of her brother Dave, a professional climber who has reached the
Everest summit and sent witty Web
dispatches back from the mountain
in each of the last two years.
 
  ""I only know what's on the Web,""
she said. ""When two climbers died on
his route last year, we got an e-mail
saying he was O.K.
 
 But otherwise, I
just read the same updates as everyone else.""
 
  The Web would be well suited to
accounts of climbing feats even without this immediacy. Good sites include abundant photographs (most
can be enlarged with a click) and
other visual effects, combining them
with text in creative ways to give
readers more of that elusive you-are-there feeling than can be had while
curled up in an armchair with a book
or magazine. With so many sites,
though, it may not be clear where to
begin. Here are a few ways to make
the mountains come to you.
 
 
 
   
Related Sites  These sites are not part of The New York Times on the Web, and The Times has no control over their content or availability.  
 
 
""Into Thin Air,"" a book by Jon Krakauer  about the 1996 climbing disaster on Mount Everest, spent a long time atop the best-seller list, and the 
Imax film ""Everest"" enjoyed a long run in theaters. If you want to find 
out more about the disaster, in which five people died, here are a few 
Web sites related to it and to other climbs in 1996: 

 
 
A LOOK BACK AT EVEREST 
www.outsidemag.com:80/places/features/everest.html
 
 
Updates from 
Scott Fischer's expedition, Jon Krakauer's original magazine article 
and more, from Outside Online. 
 
  
THE EVEREST ASSAULT STORY
 
peacock.nbc.com/everest/main.html#home
 
  Sandy Hill Pittman was criticized for her role in the 1996 disaster, but her site is well done.
 
  
EVEREST SUMMIT JOURNAL COMPENDIUM
 
members.xoom.com/Everest96/links.htm
 
  A thorough listing of articles 
and interviews related to the 1996 climbing year.   
 
 
 
  
RISK ONLINE
 www.risk.ru
 
  This English version of a Russian site contains the best single-expedition Web site I've ever seen, in the Mountaineering section 
under the heading Togliatti K2 1996. It is the story of a Russian team 
that climbed K2 (the world's second-highest mountain) in the summer 
of 1996, told from the perspectives of two climbers. Lavishly illustrated 
with stunning photos and video. 
 
 
 Peter Dizikes is a freelance writer 
and recreational climber who lives in 
New York.  

 
 
  
  
 

 






 
 


Home |
Site Index |
Site Search |
Forums |
Archives |
Marketplace
  
Quick News |
Page One Plus |
International |
National/N.Y. |
Business |
Technology |
Science |
Sports |
Weather |
Editorial |
Op-Ed |
Arts |
Automobiles |
Books |
Diversions |
Job Market |
Real Estate |
Travel
 
 
Help/Feedback |
Classifieds |
Services |
New York Today
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















