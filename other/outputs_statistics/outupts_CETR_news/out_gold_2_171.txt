

 "Unisa: Malema isn't studying here
         
 28 October 2008, 15:06 
         
           Related Articles 
           
             
               Malema's grim report card is 'real' 
             
           
           
         
         
	  ANC Youth League President Julius Malema has never registered for law or any other degree at the University of South Africa (Unisa), the institution said on Tuesday. 
 
Spokesperson Doreen Gough said the institutions had no record of Malema's admission for any degree studies. 
 
""He was in fact registered in Unisa's access or non-formal programmes and has not made any further progress."" 
 
Unisa was responding to a report in The Star on Friday, which published a copy of his matric certificate and indicated that he had ""successfully registered"" for a part-time law degree at the university. 
 
According to the certificate, Malema matriculated at the age of 21, scraping through with an H for Mathematics and a G for Woodworking, both on the standard grade. He got Es for Sepedi and Afrikaans, both on higher grade, an F in Geography, a D in History and a C in second language English, all on the higher grade. 
 
ANC Youth League spokesperson Floyd Shivambu denied the results were real, saying: ""As far as we are concerned, these results are not a reflection of reality."" 
 
He declined to give Malema's ""real results"", saying the ANCYL was ""not focused on the individual but the collective"". - Sapa 

