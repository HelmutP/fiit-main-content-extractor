










 




 














 







  


 November 18, 1999 
 

 REVIEWS/SOFTWARE
 


 Exploring Worlds on CD-ROM's
 

nteractive CD-ROM's have largely
been ignored in the frenzy of the Internet land grab, left to collect dust
on computer store shelves, but intriguing educational titles are still
being released. The descriptions below are drawn from articles that appeared in Circuits this year.

 
BLUE'S BIRTHDAY ADVENTURE
 
(Humongous Entertainment; $29.99;
Macintosh Power PC, System 7.1 or
later, and Windows 95 and 98; for
ages 3 to 6.)
 
  All the genius of ""Blue's Clues,""
the much acclaimed preschool television program, has made its way
into this clever think-along CD-ROM.
Children help the dog named Blue
and her owner, Steve -- our human
guide through a cut-paper landscape
-- prepare for Blue's birthday party.
Children work through nine activities, each of which can be set to a
different skill level.
 
  This is a thoughtful piece of software. Decisions are reviewed, clues
are repeated and praise borders on
the lavish. And when all the clues are
in, everyone repairs to the Thinking
Chair to consider alternative solutions. Smart. (Feb. 11)
 
 
 
 
JAMES CAMERON'S
TITANIC EXPLORER
 
(Fox Interactive; $29.98; Windows
95 and 98, Power Macintosh.)
 
 
 
 This three-disc package is more
than just a video of the popular movie. A timeline using archival photographs, documents and brief clips
from the movie, with narration, provide a nice visual and oral summary
of the ship's history.
 
  The software is at its best when
feeding the insatiable appetite for
Titanic trivia. There is a modest but
fascinating archive that includes a
complete passenger and crew list,
builders' plans for every deck and
biographies of many passengers and
crew members. (April 8)
 
 
POWERS OF TEN INTERACTIVE
 
(Pyramid Media; $79.95; Windows
95 and 98, and Macintosh 7.1 and
later.)
 
 
 
 Powers of Ten Interactive is dedicated to expanding the ideas of the
husband-wife design team of Charles
and Ray Eames. Charles Eames is
best known for his furniture -- the
plywood chairs and chaise longue he
did for the Herman Miller company.
But with Ray, he was also a multimedia innovator. Together, they created about 88 short films and pioneering multiscreen and multipanel
exhibits.
 
  There are many wonderful pieces
here, like a short video that takes the
viewer up a road, across a meadow
and into the living room of the
Eames house in Los Angeles, a landmark construction of off-the-shelf industrial parts.
 
  Navigating the Powers of Ten disc,
however, proved to be a challenge.
The program slaps together portraits of thinkers on time and space,
from Descartes to Einstein, with bits
of science and a history of the Eameses' work thrown in: exactly the
sort of needless complexity it was
the genius of the Eameses to cut
through. (May 13)
 
 
BODY VOYAGE
 
(Southpeak Interactive; $24.95;
Windows 3.1, 95, 98 and NT, and Mac
7.0 or later.)
 
 
 
 This eerie tour of the human body
is about as close as you are likely to
get to the atmosphere of a Renaissance dissection. A computer artist
selectively reconstructed the digital
images of a cadaver (a Texas murderer) into colored cross-sections
and airy three-dimensional renderings. The viewer can twirl the entire
skeleton on its axis, peel off layers of
skin and muscle to reveal the organs
underneath and pan along the various planes of the body as if peering
from a tiny helicopter traveling over,
under and through the body's terrain.
 
 
 
  Body Voyage is not the place to
learn rigorous anatomy. But there is
plenty of ambience. (July 8)
 
 
 
 
 
  DICK HYMAN'S
CENTURY OF JAZZ PIANO
 
(JSS Music; $49.95, Home version;
$99.95, Pro version; Windows 3.1 and
later, and Macintosh 7.0 and later.)
 
 
 
 Dick Hyman, the jazz pianist and
historian, has spent his career educating audiences about the great figures of prewar jazz. As a result, the
videos, études, biographical nuggets
and diabolically difficult quiz on this
two-disc set are weighted toward the
early giants of jazz piano.
 
  One large section is given over to
Art Tatum, where Hyman details runs and intervals, analyzing
Tatum's style; in ""Ragtime to
Stride"" and other segments, he emphasizes how successive styles built
on what came before, like how the
role of the left hand gradually
changed from ragtime into stride to
the sophisticated post-stride of, say,
Teddy Wilson and finally to the jabbing bebop minimalism of Bud Powell.
 
 
 
  Hyman is not offering beginner piano lessons here. But for pianists or even vaguely interested casual musicians with a bit of jazz knowledge, Hyman gracefully connects the historical dots.
 
 (Aug. 5)
 
 
 
 
 
  COUNTRY.COM'S CENTURY OF COUNTRY
 
(Starworks; $29.98; Windows 3.1, 95
and 98.)
 
  Billed as ""the world's first interactive country music encyclopedia,""
this disc is designed to appeal, so say
its creators, to both casual and academic fans. They may be half-right.
 
 
 
  If you want to hear Sleepy LaBeef
discuss his debt to rock 'n' roll, this is
the place. If you want a video clip of
Hank Williams, you will be disappointed, as is the case for almost
every other major star.
 
  The primary focus of Century of
Country is its encyclopedia. One outstanding feature is a searchable database of awards, record labels,
management companies, birthdays,
even fan clubs. But the CD-ROM has
one overriding weakness: its silence,
odd for a multimedia disc about one
of America's great indigenous art
forms. (Aug. 5)
 
 
 
 
THE COMPLETE NATIONAL GEOGRAPHIC
 (National Geographic/Mindscape;
$149.95 for CD-ROM, $199.95 (DVD-ROM); Windows 3.1 and later,
Macintosh 7.5 and later.)
 
 
 
 
 
  The National Geographic Society
boasts that the collection includes
every cover (1,247), every page
(190,000), every article (9,300), every
photograph and illustration (180,000)
and every advertisement to appear
in its pages, all scanned directly
from the original magazines.
 
  And the pictures look great. The
magazine's pages are displayed a
spread at a time, and the scans were
optimized to favor the photographs.
The words, however, are often faint
and barely legible, even on a 17-inch
monitor.
 
  Searching the 109-year archive
yields split-second results. But
browsing is the best way to follow
National Geographic's evolution
from a scholarly scientific journal
with a conservative, dark-brown cover and no photographs into a legendary publication that today reaches 50
million adults and children each
month. (May 13)
 
 
 
 
 
 
APPALACHIAN TRAIL
 
(Maptech Windows; $39 for each CD-ROM, $139 for set of four; Windows
95, NT and 98.)
 
 
 
 The Appalachian Trail -- 2,160.3
miles, from Springer Mountain in
Georgia to Baxter Peak in Maine --
is no walk in the woods. Hikers who
travel a section of the trail make up
the bulk of the users, and they are the
ones who would benefit most from
this set of CD-ROM's.
 
 
 
  The discs offer plenty of maps,
photographs and descriptions. The
guides include trail mileage, elevation ascents and descents, locations
of shelters and sources of water.
There is also a description of what a
hiker will encounter. A 2.3-mile
stretch of the trail in Virginia near
Hawksbill Mountain in Shenandoah
National Park, for example, includes
the altitude at the summit (4,050 feet,
the highest point in the park), the
mountains visible from the trail and
the places to find the three-toothed
cinquefoil, which blooms in late
spring. (Sept. 30)
 
 
 
  THE SUPREME COURT'S GREATEST HITS
 (Northwestern University Press;
$29.95; Macintosh and Windows 95
and 98.)
 
 
 
  The Supremes have a compilation
disc, but don't expect to hear Diana
Ross on this one. Instead, this CD-ROM platter, produced by Northwestern University Press, is
crammed with more than 70 hours of
often passionate legal debate that
was recorded on tape in the nation's
highest court.
 
 
 
  Included on the compilation are
memorable cases like Roe v. Wade,
New York Times v. Sullivan and Miranda v. Arizona. The recordings,
which date from 1955, can be heard
in their entirety. The CD-ROM also
includes pictures and biographies of
all the justices who have served
since 1957 and background information on a range of opinions that cover
issues like race and freedom of the
press. (Sept. 2)
 
 
 
 
FAMILY TREE MAKER DELUXE VERSION 7.0
 
(Broderbund; $89.99; Windows 95
and 98.)
 
 
 
  If you are one of those people who
has been meaning to compile your
family history since ""Roots"" was
broadcast in 1977, help is here. Broderbund's latest version of Family
Tree Maker contains a whopping 20
CD-ROM's of genealogical information, and the program promises access to more than a billion names.
 
  In addition to templates for your
own family tree, the deluxe version
offers the digitized Social Security
Death Index and an International
Marriage Records Index, plus discs
containing American military
records, land records and 900 years
of birth records for the United States
and Europe. Users also receive a
free four-month subscription to GenealogyLibrary.com, a Web site with
more than 2,000 rare books, census
microfilm records and online databases to search.


 
  
 

 






 
 


Home |
Site Index |
Site Search |
Forums |
Archives |
Marketplace
  
Quick News |
Page One Plus |
International |
National/N.Y. |
Business |
Technology |
Science |
Sports |
Weather |
Editorial |
Op-Ed |
Arts |
Automobiles |
Books |
Diversions |
Job Market |
Real Estate |
Travel
 
 
Help/Feedback |
Classifieds |
Services |
New York Today
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















