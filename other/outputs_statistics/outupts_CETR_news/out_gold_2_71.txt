

 "GIRLS ALOUD ON THEIR BRAND NEW SOUND
	
					
		
		 
 
		
			 
			
							
			 
			
						 
				ABOVE: Nic and Sarah in the Playlist office with Kim Dawson			 
						
			  		
		 
		
			
					
			 
			 
				28th October 2008			 
			 
				
								 By Nicola &amp; Sarah of Girls Aloud
 
								
							 
			
						 				 
					Your Shout  ( 0 )
				 
			 
						 


		 WE?VE been a pop band for six years but only now do we feel like we?re hitting our stride. 
		 
	
	
	
			
		
	
		 
The Promise is our fourth No 1 single but it feels extra special this time.                                                                                                          In the past we?ve delivered some big tunes that defined our changing style.                                                                                                             Sound Of The Underground, Biology, Love Machine ? every song was so different and made a big impression.                                                                                                            But the last few singles, tracks like Can?t Speak French, all had a very similar vibe.                                                                                                            So now, coming back with a Sixties sound in The Promise, it?s like: ?Bang, we?re back!?                                                                                                           It feels like another Biology for us, something really fresh again, and we love the fans? reaction.                                                                                                           This year has been a great one for girls in pop ? we love the emergence of talented young artists like Adele.                                                                                                            She doesn?t have to put on a big show to impress, it?s all about her incredible voice and songs.                                                                                                           We like Duffy too but Solange Knowles is the one we really admire right now ? she?s another one who?s playing around with the whole Sixties sound.                                                                                                           R&amp;B man Ne-Yo is another artist we love and we would work with him at the drop of a hat.                                                                                                            The songs he?s written for Rihanna and Leona Lewis are amazing and his lyrics really touch us.                                                                                                            And lately we?ve been rediscovering Nineties club classics like Snap?s Rhythm Is A Dancer and UK garage anthems by artists like Craig David.                                                                                                            Whatever happened to that scene? It rocked."
"3569","By Nicola &amp; Sarah of Girls Aloud
								
							
			
						 
 				 
					Your Shout  ( 0 )
				 
			 
						


		 WE?VE been a pop band for six years but only now do we feel like we?re hitting our stride. 
		 
	
	
	
			
		
	
		 
The Promise is our fourth No 1 single but it feels extra special this time.                                                                                                          In the past we?ve delivered some big tunes that defined our changing style.                                                                                                             Sound Of The Underground, Biology, Love Machine ? every song was so different and made a big impression.                                                                                                            But the last few singles, tracks like Can?t Speak French, all had a very similar vibe.                                                                                                            So now, coming back with a Sixties sound in The Promise, it?s like: ?Bang, we?re back!?                                                                                                           It feels like another Biology for us, something really fresh again, and we love the fans? reaction.                                                                                                           This year has been a great one for girls in pop ? we love the emergence of talented young artists like Adele.                                                                                                            She doesn?t have to put on a big show to impress, it?s all about her incredible voice and songs.                                                                                                           We like Duffy too but Solange Knowles is the one we really admire right now ? she?s another one who?s playing around with the whole Sixties sound.                                                                                                           R&amp;B man Ne-Yo is another artist we love and we would work with him at the drop of a hat.                                                                                                            The songs he?s written for Rihanna and Leona Lewis are amazing and his lyrics really touch us.                                                                                                            And lately we?ve been rediscovering Nineties club classics like Snap?s Rhythm Is A Dancer and UK garage anthems by artists like Craig David.                                                                                                            Whatever happened to that scene? It rocked. 

