

 "Judgment date set for Pretoria murder
         
 27 October 2008, 19:04 
         
           Related Articles 
           
             
               2 murder accused to learn fate next week 
               Justice waits for Odendaal murder accused 
             
           
           
         
         
	  Judgment will be delivered on Tuesday in the trial of two men accused of murdering Pretoria resident Kathy Odendaal. 
 
Prosecutor Petra van Basten argued in the Pretoria High Court on Monday that the evidence was overwhelming that Alfred Matlala, 23, and Tshepo Dithung, 29, killed Odendaal on October 16 last year. 
 
Odendaal's blood was found on the clothing of both accused. 
 
Van Basten argued that the court should also convict the two of attempting to rape the mother of two. 
 
She said the only logical conclusion from the way in which the victim was found spread-eagled and half-naked was that her attackers had not only stabbed and strangled her, but also tried to rape her. 
 
A security guard caught Dithung jumping over a fence at Odendaal's Lynnwood Manor home shortly after the murder. 
 
Three men who worked for a refuse removal company caught Matlala the same afternoon after seeing him robbing a woman in a Lynnwood street. 
 
Police found Odendaal's car keys and jewellery in his pockets. 
 
Matlala was also caught in possession of a firearm stolen three days  earlier from a house in the area, at which Dithung left his fingerprints behind. 
 
The firearm was allegedly used to murder Wilson Mapadahanya in a Lynnwood Street and to rob Odendaal's neighbours the same day. 
 
Matlala's blood was found at the scene of the robbery at Odendaal's neighbour, the Department of Trade and Industry's chief of staff Moosa Ebrahim. 
 
The three-year-old daughter of Ebrahim's domestic worker was shot in the back during the robbery. 
 
Van Basten asked the court to convict the accused on all 15 charges against them, including five of robbery relating to the attack at the Ebrahim home. 
 
She said although it had been one robbery, it was not a duplication of charges because all of the victims had been threatened or assaulted and then robbed separately. 
 
Defence advocate Herman Alberts said the state's case was not above criticism, although the evidence against his clients appeared to be overwhelming, 
 
He said it remained his instructions that his clients were innocent. 
 
Alberts criticised fingerprint and ballistic evidence linking his clients to various crimes. He said that in both instances the court was  faced with conflicting reports from experts. 
 
Acting Judge Chris Eksteen postponed the trial until Tuesday for judgment. - Sapa 

