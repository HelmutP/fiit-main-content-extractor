

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 
  
  Sunday Tribune
  
  
  


  
  
  
  
  
  
  
  
  




   
     
      IOL News
       | 
      IOL Sport
       | 
      IOL Business
       | 
      IOL Jobs
       | 
      IOL Entertainment
       | 
      IOL Travel
       | 
      IOL Motoring
       | 
      IOL Property
       | 
      IOL Parenting
       | 
      IOL Classifieds
       | 
      More 
     
   
      
           
     
        
       
          
         
	  News |
	  Opinion |
	  Entertainment |
	  Sport
         
       
     

     

     

       
      
       
         Red revolution 
         24 October 2008, 07:50 
         
           Related Articles 
           
             
               Scolari wary of Liverpool threat 
               How the mighty have fallen 
               Five-star Chelsea stay top 
               Torres injury blow for Liverpool 
             
           
           
         
         
	  The growing belief that Liverpool are equipped to end their long wait for a Premier League title faces its sternest examination so far when they meet a flying Chelsea side at Stamford Bridge on Sunday.  
 
Chelsea are top on goal difference from Rafael Benitez's side, although the emphatic nature of their recent performances in the league contrasts sharply with Liverpool's.  
 
Last weekend, Liverpool scrambled a 3-2 victory at home to Wigan Athletic after trailing twice while in their previous game they battled back from 2-0 down to beat Manchester City. Give Chelsea a lead and they are unlikely to hand out second chances.  
 
Luiz Felipe Scolari has barely put a foot wrong since taking over at Stamford Bridge from the dour Avram Grant. The Brazilian sent a patched-up side out at Middlesbrough last weekend and they came home with a resounding 5-0 victory.  
 
They have scored 19 goals and conceded just three in eight matches and where Jose Mourinho's title-winning teams were powerful and attritional, Scolari's version is playing with a swagger and no little panache.  
 
Benitez has been criticised in the past for a poor Premier League points return against the big three of Chelsea, Manchester United and Arsenal, although this season's 2-1 defeat of United at Anfield was thoroughly deserved.   
 
The Spaniard twice got the better of Mourinho's Chelsea in the Champions League but last season the sides met five times, including another two-legged European semifinal, and Liverpool failed to record a victory.  
 
However, Scolari identified this week as Chelsea's toughest of the season so far with Liverpool's visit following so closely after Wednesday's 1-0 Champions League win over AS Roma.  
 
Liverpool will badly miss striker Fernando Torres, absent from their 1-1 draw at Atletico Madrid with a hamstring injury, and the emphasis will be on Robbie Keane and Dirk Kuyt to try to end Chelsea's four and a half year unbeaten home league record.  
 
However, the belief that even without Torres they are now real contenders is shared by Luis Garcia, whose goal helped Liverpool beat Chelsea in the 2005 Champions League semifinal.  
 
""I have watched and you can see the team growing. If they continue in this way they could win the league,"" Garcia, now with Atletico, told Liverpool's website.  
 
""When I first joined, Chelsea were far better than us, but the gap is closing and Liverpool can now win the league.""  
 
Fourth-placed Arsenal, fresh from romping to 5-2 win at Fenerbahce on Tuesday, visit West Ham United on Sunday.  
 
Manchester United, who are hitting their stride after a sluggish start and are fifth with a game in hand, travel to struggling Everton on Saturday. Third-placed Hull City aim to continue their dream start at West Bromwich Albion. 
 
    This article was originally published on page 30 of  The Star on October 24, 2008  			
         
         
	   EMAIL STORY
	     
	   EASY PRINT
	     
	   SEARCH
	     
	   NEWSLETTER
	 
	  
       

       


       
         
          
            Search Sunday Tribune:
            
            
            
          
	   
            login |
            subscribe
	   
	 
	     
     	      
             
               Ramaphosa shows his true colours 
              Businessman and ANC leader Cyril Ramaphosa, once hailed as the heir apparent to the presidency, has warned dissidents against leaving the ruling party, reminding them of the "scars" he bore during the seven years he served under Thabo Mbeki.
               
                 Party girls drug, rob pub patrons 
                 Girls' tragic vigil at slain mom's side 
                 Stowaway's ordeal 
               
             
			 
Subscribe today &amp; get full access to our Premium content 
 
            
              
             

 

 
	
             
           

           
              
             
               
                 Today's Front Page 
                 Browse By Page 
                 Past Front Pages 
                 Supplements 
                 Subscribe Now 
               
             

		

           

            
             
            
           

         

         
         
           About Sunday Tribune 
           
             
               About Us 
               Advertising 
               Booking Deadlines 
               Contact Us 
               Readership 
               Terms &amp; Conditions 
             
           
           Sister Newspaper Sites 
           
             
               Cape Argus 
               Cape Times 
               Daily News 
               Isolezwe 
               Post 
               Pretoria News 
               Sun. Independent 
               Sunday Tribune 
               The Independent on Saturday 
               The Mercury 
               The Star 
             
           
          
         
                 
 

         
           Online Services 
           
             
               Accessories 4 Outdoors 
               Audio, TV, GPS &amp; PS3 etc 
               Books, CDs and DVDs 
               Car Hire 
               Car Insurance 
               Car Insurance for Women 
               Cars Online 
               Compare Insurance Quotes 
               Education 
               Gold-Reflect who you are 
               Home Loans 
               Lease Agreements 
               Life Insurance 
               Life Insurance for Women 
               Medical Aid 
               Offer to Purchase 
               Online Shopping 
               Play Euro &amp; UK Lotteries 
               Property Search 
               Residential Property 
             
           
           Mobile Services 
           
             
               IOL On Your Phone 
               Mobile Games 
               Personalise Your Phone 
             
           
         

       


       
          
          
         

            
                
                
                

            
            Date Your Destiny
            
                 
           
            
                
                
                
                 

               
                  
                
                 
                  I'm a 24 year old woman looking to meet men between the ages of 26 and 30.
                
              
              
           
           
         
      
 






  
    

		
			
			
			
			 
				
					Select Province &amp;
					Eastern Cape
					Free State
					Gauteng (PWV)
					KwaZulu Natal
					Mpumalanga (E. Tvl)
					North West
					Northern Cape
					Northern Province
					Western Cape
					------------------
					Botswana
					Lesotho
					Malawi
					Namibia
					Swaziland
					Zimbabwe
					Other Country (Not in SA)
				
			 
		
	
  
  
	
  
  
      
  
  
    
		 
			A 
			B 
			C 
			D 
			E 
			F 
			G 
			H 
			I 
			J 
			K 
			L 
			M 
			N 
			O 
			P 
			Q 
			R 
			S  
			T 
			U 
			V 
			W 
			X 
			Y 
			Z
		 
	
  
  
      
  
	 
       

 
         
           
              
             
               IOL JOBS 
               
                 Building a new future through passion  
                 Your performance is always measured  
                 Jobs database aids another young hopeful  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL TRAVEL 
               
                 More than a can of worms  
                 I've got you under my skin  
                 Unwind under the African sky  
               
             
            
           
         
       
       
         
           
              
             
               IOL TECHNOLOGY 
               
                 Nokia gives away smartphone technology  
                 Thailand to block websites insulting royals  
                 Intel to invest in solar venture  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL MOTORING 
               
                 'Hamilton prime to beat my title record' - Schumie  
                 Two new reasons for watching A1GP this season  
                 UK show warning for JIMS trade participants  
               
             
            Sign up for our FREE newsletter
           
         
       
       
         
           
              
             
               IOL PARENTING 
               
                 SA's shocking teen sex stories revealed  
                 Japanese parents try to marry off children  
                 Travel a major cause of accidental deaths  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               IOL SPORT 
               
                 De Villiers isn't gambling on Pienaar, Smit  
                 Torres still hamstrung  
                 Pakistan fails to secure Windies Tests  
               
             
            
           
         
       
       
         
           
              
             
               IOL ENTERTAINMENT 
               
                 Chemistry between blonde trio will be electric  
                 'He either buys or rents his own jet'  
                 Christian signs up for Strange role  
               
             
            Sign up for our FREE newsletter
           
           
              
             
               BUSINESS REPORT 
               
                 Stocks claw back lossed despite running out of steam  
                 Farming enterprises on BEE road  
                 Busa woos Chinese investors  
               
             
            Sign up for our FREE newsletter
           
         
		
		

		
	
		
		
       
       
       
RSS Feeds  |  Free IOL headlines  |  Newsletters  |  IOL on your phone  |  Group Links  
       
         
          © 2008 Sunday Tribune &amp; Independent Online (Pty) Ltd. All rights reserved. 
          Reliance on the information this site contains is at your own risk.  
          Please read our Terms and Conditions of Use and Privacy Policy. 
		  Independent Newspapers subscribes to the South African Press Code that prescribes news that is truthful, accurate, fair and balanced. If we don't live up to   the Code please contact the Press Ombudsman at 011 788 4837 or 011 788 4829.
         
         

		
		
		 
       
		  
		
		
       
     

   
  

 


  






