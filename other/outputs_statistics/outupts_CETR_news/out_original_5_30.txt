


 By Alfred Cang 


 SHANGHAI (Reuters) - Once considered a symbol of the decadent West, Valentine's Day is becoming big business in newly affluent China. 


 Nowhere more so than in Shanghai, China's showcase city for the economic reforms"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:24PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      In Depth
      
      &gt;
      Article
 
 
 
       China's rich spend big to celebrate Valentine's Day 
       Sun Feb 11, 2007 2:20AM EST 
      
     

     
 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		In Depth News
     
   
   
		 
				African amputees seek victory, pride through soccer

			 
		 
				High School Musical takes over the world

			 
		 
				Rightwing warrior Falwell has eyes on 2008

			 
		 
				Afghan cleric takes Islamic battle to the airwaves

			 
		 More In Depth News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Alfred Cang 

    


 SHANGHAI (Reuters) - Once considered a symbol of the decadent West, Valentine's Day is becoming big business in newly affluent China. 

    


 Nowhere more so than in Shanghai, China's showcase city for the economic reforms of the last three decades, a financial hub which is once more rediscovering its glory pre-World War II days when it was known as the Paris of the East. 

    


 This Valentine's Day, Shanghai banker Richard Fan will be buying his wife a 40,000 yuan ($5,146) Cartier wrist watch. 

    


 ""I think it's a better gift than some 10,000 or 20,000 yuan ($1,300-$2,600) meal,"" said Fan, 37. 

    


 ""A gift you can use daily looks much more concrete,"" he added, blithely. 

    


 The watch's price tag is 12 times more than the average Chinese farmer earns in a year. 

    


 Among Valentine's Day gift ideas on offer in Shanghai is a $1,000 wine-and-dine package that includes limousine transfers, personal butlers and candle-lit dinners at private concerts.  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				 
U.S.-led forces show evidence of Iran arms in Iraq
 
				 BAGHDAD (Reuters) - Officials of the U.S.-led coalition on Sunday showed what they said were examples of Iranian weapons used to kill 170 of their soldiers and implicated high-level Iranian involvement in training Iraqi militants. 
                
                    Full Article  |  Video  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















