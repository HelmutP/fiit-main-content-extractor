

 When Don Dubin told me, ""Fishing is my life,"" I was pretty sure I'd come to  the right guy. 
 
Then, when he showed me his basement, I was hooked. 
 
The idea was to find an expert to take me shopping for the basic gear it  takes to catch a fish (or even two or three). I wanted to be able to recommend  what you need to introduce a kid to the joys of doing something besides  sitting in front of a screen pushing buttons on a controller. 
  

                    

                    

                    
                        
                    

                    
                     
                        

                        
                             
                                 Related Links 
                                 
                                     
													
													
														


    Hate worms? Reel in this fishing gear - VIDEO
    
















													
												 
                                 
                             
                        

                        
                        




    
        
            
        
    

                        

                        
                         
                            
                                
                            
                         
                        
                     
                    

                     
And who better to turn to than a man who has converted the entire basement  of his suburban Lincoln-wood house into a fishing museum? The place is  literally wall-to-wall fishing, including hundreds of lures, dozens of mounted  prize catches and an aquarium filled with the living objects of his passion:  blue gill, crappie, bass and such. 
 
A retired salesman, Dubin, 68, now can devote all of his time to his  obsession, which also includes prize-winning fish taxidermy and wood carving  as well as lecturing about fishing and fisheries. And, of course, actually  going fishing -- which he does two or three days a week. 
 
Just this year, Dubin was named a ""Legendary Angler"" and enshrined in the  National Fresh Water Fishing Hall of Fame in Hayward, Wis. He's clearly ideal  to help put together a Fishing Starter Kit. 
 
The very first thing Dubin told me about fishing gear is that you don't  have to spend much money to be successful. Good news, since I'd already  decided I didn't want to shell out more than $50. 
 
By the time we got together, Dubin had compiled a checklist so detailed,  with so many footnotes, that it scared me. If he puts that kind of effort into  a shopping venture, I can only imagine the planning that goes into his pursuit  of the elusive muskie. The fish doesn't stand a chance. 
 
Dubin is a big believer in the smaller, locally owned fishing store  because, he says, that's where you can get the most important item on his  list: good advice. 
 
We drove over to FishTech (formerly Ed Shirley's) in Morton Grove, where I  told co-owner Jim Gillen about my budget and my personal hang-up: I really  didn't want to touch a worm. 
 
Both Gillen and Dubin assured me I could catch fish without ever touching  live bait or -- more ick factor -- putting my finger down a fish's throat to  retrieve the hook. They said artificial bait works fine and there's a tool, a  forceps or ""hook-out,"" that you can use to  ...  get the hook out. 
 
First stop: the pole. Dubin showed me a couple of variations on a simple,  lightweight 10-footer, a size that's easy for kids to manage too. Pointing to  the bamboo and graphite versions, he pronounced, ""This is as effective a way  to catch a fish as any ever made."" 
 
Save the rod-and-reel purchase for after you've mastered the simpler  fishing pole, he advises. With a reel, there's a high chance of snagging the  line and ""getting disgusted."" And, ""If you get disgusted right off the bat,  you'll drop fishing."" Not a good option. 
 
Then what? Some hooks, fishing line, a bobber (it dips down to let you know  you've hooked a fish), split shot (little lead balls to weigh down the line)  and artificial worms and fake minnows for bait. 
 
Hoping I could convince somebody to come fishing with me, I bought two  poles (the bamboo and the graphite ones). The whole shebang -- including two  different sizes of hooks -- cost me $48.61 including tax. 
 
I wish I could tell you that Dubin and I then went for Montrose Harbor,  dropped our lines and caught a tasty perch dinner. Instead, we headed back to  his house where I learned that the thing I needed most -- just like told me at  the outset -- was advice. 
 
I had two poles and a paper bag filled with fishing tackle, and I had not a  clue how to even tie a hook on the line. (It's not that easy. You can barely  see the line, which is the idea as far as the fish is concerned.) If you don't  have a Hall of Famer to help you, read the instructions on the packaging the  hooks come in. 
 
It took close to an hour for (the supremely patient) Dubin to string all of  this stuff together to make it fishing-ready. Which is why he recommends  shopping at a smaller shop where you can get individual instruction on things  as basic as attaching the line to the pole -- or as complicated as where  they're biting. 
 
- - - 
 
The basics 
 
Here's what I bought at FishTech, 5802 W. Dempster Ave., Morton Grove;  847-966-5900 
 
1. Bamboo pole (with a hook, bobber, split shot and line), Danielson: $4.99 
 
2. Graphite telescoping pole, Zebco: $19.99 
 
3. Bobber: $1.19 
 
4. Hooks, 10, Aberdeen, size 8 (smaller): $1.99; hooks, 10, Aberdeen, size  6: $1.99 
 
5. Fishing line, Berkley Trilene, 8 lb., 110 yards: $2.99 
 
6. Artificial bait, 3 Mini Mites: $3.49 
 
7. Split shot, Water Gremlin: $1.09 
 
8. Forceps: $2.99 
 
9. Artificial worms, Berkley Gulp! red wigglers: $3.99 
 
Total (including tax): $48.61 
 
You'll also need  ... 
 
10. A fishing license is required for those over 15 (Illinois residents,  $13; non-residents, $24); scissors or a knife to cut the line; and swivels  (safety pin-like devices that make it easy to take the hook off the line and  the line off the pole). 
 
And the next step, 11. Rod and reel, Abu Garcia, $24.99. 
 
More resources: 
 
""Fishing Chicago 2007"" and ""Outdoor Notebook"" (free at bait and tackle  shops); ifishillinois.org; dnr.state.il.us; fishcba.com; henrysports.com. 

