


dictionary's main job is to
answer questions, not to provoke them.
 
 

 
 

 

 








NEW APPROACH - The Encarta World English Dictionary shows a Frida Kahlo painting but none from Picasso.
 




But even before
installing the Encarta World English
Dictionary, I was arguing with the
claims printed on its box. Microsoft
is marketing this product as ""the
world's first global English dictionary."" Oh? What about the Oxford
English Dictionary, that 20-volume,
40,000-page, continually updated
record of the English language as
spoken everywhere in the world (and
available, incidentally, both in its entirety and in abridged form on CD-ROM)?
 
  Let's go to promotional claim
No. 2: ""Created for the new age of
global communication."" I couldn't
argue with that.
 
 It was immediately
apparent from the print version, released in July, that this dictionary is
multicultural in tone and content to
an extent that is, indeed, unprecedented.
 
 That attitude (and latitude)
is even more pronounced in the audiovisually empowered CD-ROM.
The idea of an absolutely up-to-the-moment, sex- and race-sensitive dictionary with built-in Roget's Thesaurus, Computer Dictionary, Style
Guide, Almanac and Book of Quotations is a good one. But how does
such an ambitious project work in
actual use?
 
  I asked Joan Cohen, an educational
therapist in New Haven, to try out
the dictionary while writing one of
her case studies.
 
 She found that it
responded quickly, and she was impressed by the convenience of being
able to switch rapidly between her
text and the dictionary or resources
like the thesaurus.
 
 
 
  Taking a break from her report,
she fed the dictionary a few tough
words that her students might have
to look up: pangaea, archaea and
eukarya.
 
 All of them are clearly defined.
 
 Then, scrolling down the options under Bill Clinton, she decided
to investigate a few quotes.
 
 She liked
the wittiness demonstrated not only
by the choices but also by their presentation.
 



 





 An Encarta project 
takes multiculturalism 
to unprecedented 
lengths.





 






 
 
 ""There have been no scandals in this adminstration"" is filed
under Delusions.
 
 Returning to her
work, Ms. Cohen looked up ""shut
down."" Its little red megaphone icon
was missing, which meant that she
could not tune in its spoken pronunciation. She wondered if other words
are without sound (the packaging
touts the dictionary's audio pronunciations).
 
  I didn't find any more missing
megaphones, but there were other
slip-ups.
 
 I was surprised to see (and,
of course, hear) the no longer acceptable pronunciation of Mi'kmaq (an
Indian tribe of eastern Canada) as
""mik mak,"" instead of the now preferred ""meeg maw."" Definitions are
deliberately simplified (""English
has never been easier"" is part of the
marketing strategy), but they sometimes border on the simplistic. Some
etymologies are unsatisfying.
 
 The citation for ""arugula,"" for instance, is
""origin uncertain: probably from dialectal Italian."" Close, but why not
explain that ""arugula"" is the southern Italian pronunciation of the
standard Italian ""rucola""?
 
  At other points, the question that
kept coming to mind was, ""What
were they thinking?"" It may be more
fashionable, but it hardly makes
sense to show a Frida Kahlo painting
and a Diego Rivera painting but not a
Picasso or a Matissse, to give dictionary entries for Barbara Walters
and Oprah Winfrey but not for Walter Cronkite and Mike Wallace, to
offer a dozen quotes from Maya Angelou and none from Joseph
Brodsky, the Russian-born Nobel
Prize laureate and former United
States poet laureate. And why is New
Deal defined here but not New Frontier? (Great Society comes up, but
only in passing, in a quote from Lyndon B. Johnson.)
 
  Then there's the dictionary's annoying preachiness. It is one thing to
point out that Canadians and Mexicans may take offense when ""America"" is used to refer to only the
United States. (In fact, I wish that all
the politicians who speak ad nauseam of ""the American people"" would
take note of that.) But littering definitions with the word ""offensive"" --
it shows up 20 times each in explanations of two very popular expletives
-- is, well, offensive.
 
  I did enjoy browsing here, much as
I love wandering the densely worded
pages of print dictionaries. While
searching for Tchaikovsky, for instance, I spotted ""TCBY Treats"" on
the word list.
 
 It turned out to be 28th
on a chart of Top 50 Franchises in the
United States, one of numerous interesting charts that came up.
 
 Clicking
on ""tonality"" in Multimedia produced a line from ""My Country 'Tis
of Thee,"" presented in both scored
and recorded form in three keys: C,
F sharp and A flat. That was fun.
 
 
 
  But I also kept encountering shortcomings in using the program. For
instance, among the Multimedia selections (other search categories include Dictionary, Thesaurus, Almanac, Quotations, Computer Dictionary, Sounds, Images and the presumably all-encompassing Everything),
I came across ""wangga music of
aboriginal Australia."" I clicked on it
and found that I could listen to a few
bars.
 
 But had I entered the program
wanting to look up ""wangga music,""
having read or heard of it somewhere, and had I then taken the
logical first step and searched for it
under ""Everything,"" I would not
have found it.
 
 
 
  ""Everything,"" it turns out, does
not search Multimedia, Sounds or
Images.
 
 Two other drawbacks are
that Quotations, at least when arrived at via Everything, bunches together quotes both from the person
and about the person, which can be
confusing. And under Dictionary, different meanings for the same word
are often grouped in separate places,
which not only complicates the process of printing out a full word profile but increases the odds of overlooking the definition needed.
 
  After several hours of browsing, it
became clear that even in the area of
pop culture, a major attraction
(there can't be too many other general use dictionaries that have the
entry Marvin Gaye), some things are
amiss and others are simply missing
-- any references at all, for instance,
to klezmer and Latin music.
 
  I asked my 12-year-old son, Jonathan, to check out his area of expertise, rap music, which I thought
would surely be a hot area for globally minded lexicographers.
 
 He was
disappointed to find that Will Smith
turned up as only a name on several
lists of music awards.
 
 But then he
noticed that there was a quotation.
 
 It
turned out to be: ""Here lies Will
Smith -- and, what's something rarish,/ He was born, bred, and hanged,
all in the same parish."" Jonathan
panicked for a second until he realized that this was an earlier Will
Smith.
 
 
 
  So what were they thinking? I'm
afraid that to a large extent it was,
""How fast can we get this product
into the stores?""
 
  That might be the way to get ahead
in software marketing, but not in the
dictionary game.
 
 
 
 
 
   

