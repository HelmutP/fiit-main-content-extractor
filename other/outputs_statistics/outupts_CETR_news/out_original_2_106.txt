

 "






//W3C//DTD XHTML 1.0 Transitional//EN""
	""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;


 
	You can take the vines out of Burgundy, but will they make better wine? - International Herald Tribune
	
		
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	

	
	
			
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
		
	
	
	
	
	
	
	
	



	



 
	
	
	 
 

;?""&gt; target=""_blank""&gt;;?""&gt; width=""768"" height=""90"" border=""0"" alt="""" /&gt;
 
 
	
	

	
	 
		  
		  
		 Travel &amp; Dining 
		 
			
			
			
		 
	 	
	
	
	
	
		

 
	  
	 
		 
			 iht.com 
 Business 
 Culture 
 Sports 
 Opinion 
		 
	 
	 
		 
			 AMERICAS 
 EUROPE 
 ASIA/PACIFIC 
 AFRICA/MIDDLE EAST 
   
 TECH/MEDIA 
 STYLE 
 HEALTH 
		 
	 
	 
		 
			 TRAVEL 
 PROPERTIES 
 BLOGS 
 DISCUSSIONS 
 SPECIAL REPORTS 
 AUDIONEWS 
		 
	 
	  
	 Morning home delivery - save up to 72% 
	 
		 
		
			
			 
				 
				SEARCH
				 
			 	
			 
			Advanced Search
			 
		
	 
 

	
	

	
	 
		
		 
		
			

			
			
			
			
			
			

			
			
			
			
			
			
				
			
				
				 
					
				            

 
	
	 Gary Pisoni in the Santa Lucia Highlands, California, is one of the more successful ""suitcase"" winemakers who has cloned vines from Burgundy.   (Ian Shaw/Cephas) 	 
 


				            
					
					
					 

					
					You can take the vines out of Burgundy, but will they make better wine? 
					
					
											
			            
						
			            
						
			            
						 
							  
							 
By Eric Asimov  
							 Published: October 23, 2008 
							  
						 
			            
						
										
											
						
			                

 
	  
	 
		
		   E-Mail Article 
		  
		
		
		 
			 
				
   Listen to Article

			 
			  
		 
		
		
		   Printer-Friendly 
		  
		
		
		   3-Column Format 
		  
		
		
		   Translate 
		  
		
		
		   Share Article 
		  
		
		
		 
			 
  
 
			 
  Text Size 
		 

		  
		 
			

			
		 
	 
	  
 



							
							
			                 You heard the one about the suitcase clones, no? It goes like this: In the black of night a guy sneaks into a famous Burgundy vineyard - let's say La Tâche, but it could just as easily be Le Musigny or Clos de Bèze. He takes some cuttings of pinot noir vines, wraps them in wet cloth and smuggles them back to California. He propagates the vines and, voilà! He's got grand cru pinot noir.  
 Dubious? It supposedly happens all the time - the smuggling part, at least - if we are to believe the marketing for dozens of American wineries. Their promotional materials tell the story of the suitcase clones, or the brand-name version, Samsonite clones. In some variations, it was a friend of a friend who obtained the clones. Either way, vineyards all over the West Coast associate themselves in their marketing with Burgundy's greatest.  
 Such stories may excite gullible consumers who are looking for something, anything, to distinguish one of the myriad pinot noirs from another.  
 But the truth is that the origin of a vine, whether from a clone boldly swiped from Domaine de la Romanée-Conti or meekly purchased from the local nursery, is at best meaningless. The grand cru association is a little like picking up a guitar like one Jimi Hendrix used and expecting ""Purple Haze"" to burst out. Fat chance.  
 And by the way, it is illegal to import agricultural material without proper quarantining.  



 
	
	 
		 Multimedia 
		  
		
		 
			  
			 
Blog: Eric Asimov's 'The Pour' » View
 
		 
		  
		
	 
	
	
 
     Today in Travel &amp; Dining 
      
     
	 For foodies, Cartagena is now on the map 
 
  
 
	 You can take the vines out of Burgundy, but will they make better wine? 
 
  
 
	 Frequent-flyer status is no promise of service 
 
  

 

	
	
	 
		

	 
	
 


 Yet the continued fascination with suitcase clones, and with the arcane issue of grape clones in general, hints at the desperation of consumers to gain some sense of control over where their wine dollars are going. The more we know about the clonal selections, soil composition, rootstocks, trellising techniques, pruning methods and degree days, the better we can guess what's going to be in the bottle, right?  
 To an extent, yes, but even well-informed wine drinkers have a difficult time making sense of many of the technical details of winemaking, especially when it comes to clones. So let's take a closer look at clones and the actual role they play in what's in your glass, regardless of their origin.  
 Vines grow grapes because they want to reproduce the old-fashioned way, by enticing birds or other critters to eat the sweet fruit, a natural means of transporting the seeds to a new location for planting. Such methods prove inefficient to meet human needs.  
 The scourge of phylloxera, for one thing, makes it impossible for most vinifera grapevines to grow on their own roots. This makes growing from seeds cumbersome, so instead growers propagate vines from cuttings of parent plants.  
 The time-honored technique was a mass selection, in which growers would take cuttings from many different vines. The result was a diverse vineyard that produced grapes of many varied characteristics, particularly if that grape was pinot noir, which is somewhat genetically unstable and mutates far more easily and frequently than, say, cabernet sauvignon or syrah. This is why most suitcase clone tales are about Burgundy and pinot noir.  
 Many growers in Burgundy still believe a mass selection is the best way to plant a vineyard. Since many if not all of the great Burgundy vineyards are mass selections, the folly of filching a few dozen or even a few hundred cuttings is clear: it can't approach the diversity in the original site.  
 Meticulous growers used only particular vines for their cuttings. Perhaps these vines were the healthiest or produced the most flavorful grapes. Short-sighted growers might have singled out the most vigorous vines. Either way, by narrowing the clonal selection they were emphasizing their preferred characteristics.  
 By the late 20th century, scientists had grown expert at isolating clones that produced particular aromas and flavors, that were early ripening or slow to mature or were resistant to disease or produced wine dark in color.  
 In Dijon, France, a series of pinot noir clones became available with such designations as 113, 114 and 115, which were not only free of grape viruses but also emphasized the aromas and flavors of red fruits like cherry and raspberry, and 667, 777 and 828, which were reminiscent of darker fruits.  
 Regardless of the attention paid to suitcase clones, these Dijon clones have become the dominant selection among California pinot noir growers, particularly recently, when the number of acres of pinot noir planted in California has almost doubled, to 29,191, (about 11,800 hectares) in 2007 from 15,514 in 1999.  
 An over-reliance on these clones has troubled some wine writers, like Matt Kramer of Wine Spectator and Allen Meadows of Burghound.com, who have singled them out as one reason that so many California pinot noirs taste the same and lack complexity. Both writers, in fact, used the same word: boring.  
  1  |  2  Next Page
 

							
							
			            	
							
								
								
								
								
						
							
						 
						
										
					
					
				    
				      
				    											
					
					 
						  
						 Back to top 
						 
Home  &gt;  Travel &amp; Dining
 
					 
			

			
			 
			
			
			
				 
		 IHT.com Home » 
		 Latest News 
		  
		 
			 
				  
				 Regis Duvignau/Reuters 
			 
			 
				 France weighs big changes in drinking laws 
				 Winemakers complain that the government, egged on by killjoys, wants to make it harder to sell and drink wine. 
			 
			 
In Opinion: David Brooks: The behavioral revolution
 
		 
		 
			  
			 
				More Headlines 
				 
					 Both candidates court Pennsylvania 
					 Aid disrupted in eastern Congo as rebels advance and populace flees 
					 Damascus closes U.S. school and culture center after border raid 
					 Experts in economic malfunction 
				 
			 
			  
		 
	 
						
			
			
		
		 
		
	    
		
		 
			
						
			
			 
				  
				 
					 Video 
					 See all videos » 
				 
				 
					  
					  
					
					 
					
						 	
						
						 
							  
							 
								 Bosporus boat ride 
								 Is a trip to Istanbul complete without a boat ride on the Bosporus? 
							 
						 
						

						
						 
							  
							 
								 Airport shoe scanner 
								 An Israeli firm develops a scanner that keeps passengers from having to remove shoes at security checks. 
							 
						 
						

						
						 
							  
							 
								 Frugal Traveler: The Grand Tour 
								 The Frugal Traveler, Matt Gross, recaps his 13-week tour of Europe and discovers that the Old World still hold... 
							 
						 
						

						
						 
							  
							 
								 Frugal Traveler: Edinburgh 
								 After three months of constant adventure, the Frugal Traveler, Matt Gross, brings his European journey to a cl... 
							 
						 
						

						
						 
							  
							 
								 Frugal Traveler: The Dutch-Belgian border 
								 The Frugal Traveler, Matt Gross, explores the strangely divided border towns of Baarle-Nassau and Baarle-Herto... 
							 
						 
						

						
						 
							  
							 
								 Frugal Traveler: Harz Mountains, Germany 
								 The Frugal Traveler, Matt Gross, escapes civilization for the forests of Germany's Harz Mountains, said to be ... 
							 
						 
						

						
						 
							  
							 
								 Frugal Traveler: Gdansk, Poland 
								 The Frugal Traveler, Matt Gross, discovers street theater and historic neighborhoods in the Baltic Sea town of... 
							 
						 
						

						
						 
							  
							 
								 UrbanEye: A Coney Island Weekend 
								 Melena Ryzik reports from Coney Island, which hosts the Siren Music Festival, a volleyball tournament and the ... 
							 
						 
						

						
						 
							  
							 
								 Frugal Traveler: Bucharest 
								 The Frugal Traveler, Matt Gross, travels by train to Bucharest and discovers the city's semi-underground cultu... 
							 
						 
						

						
						 
							  
							 
								 No-Bake Blueberry Cheesecake 
								 Mark Bittman makes no-bake blueberry cheesecake bars. 
							 
						 
						
						 							
					 					
				 
				  
			 
			
	        
	        
				
									 
						
						 

;?""&gt; target=""_blank""&gt;;?""&gt; border=""0"" alt="""" /&gt;
 
						
					 
									
				
			
				
	       
			
			 
	 
  
  
 
	
	 
		 Most E-Mailed 
		 

		 
			  
			 24 Hours 
			 | 
			 7 Days 
			 | 
			 30 Days 
		 

 
	
 
	 1. 
	 The mysterious cough, caught on film 
 
 


 
	 2. 
	 Rising yen worries Japan 
 
 


 
	 3. 
	 DA: criminal charges possible in boy's Uzi death 
 
 


 
	 4. 
	 Court finds Niger guilty of allowing girl's slavery 
 
 


 
	 5. 
	 David Brooks: The behavioral revolution 
 
 


 
	 6. 
	 Volkswagen shares jump and short-sellers pounce 
 
 


 
	 7. 
	 Alaskans turning against Stevens after verdict 
 
 


 
	 8. 
	 Bet on a rebound and another fall 
 
 


 
	 9. 
	 Oil's stunning retreat: How long can it last? 
 
 


 
	 10. 
	 White House explores aid for merger between GM and Chrysler 
 
 


 

 
	
 
	 1. 
	 Endorsement: Barack Obama for U.S. president 
 
 


 
	 2. 
	 Partying helps power a Dutch nightclub 
 
 


 
	 3. 
	 Financial crisis has one beneficiary: The dollar 
 
 


 
	 4. 
	 Greenspan 'shocked' that free markets are flawed 
 
 


 
	 5. 
	 Woman arrested for 'killing' her virtual husband 
 
 


 
	 6. 
	 Want to be heard in India? You'd better form a militia 
 
 


 
	 7. 
	 Multitasking can make you lose ... um ... focus 
 
 


 
	 8. 
	 South Korea pushes to dissolve 'the old way' of business culture 
 
 


 
	 9. 
	 In Europe, crisis revives old memories 
 
 


 
	 10. 
	 A manga adventure through the world of wine 
 
 


 

 
	
 
	 1. 
	 Equestrian stripped of Beijing result for doping 
 
 


 
	 2. 
	 Indians complete trade with Brewers 
 
 


 
	 3. 
	 Moggi among 25 sent to trial for match-fixing 
 
 


 
	 4. 
	 A champion not honored in her own land 
 
 


 
	 5. 
	 Wells Fargo, not Citi, to buy Wachovia 
 
 


 
	 6. 
	 Fortis' Dutch operations nationalized 
 
 


 
	 7. 
	 6 seek to unseat scandal-plagued La. congressman 
 
 


 
	 8. 
	 Bulgarian corruption troubling the European Union 
 
 


 
	 9. 
	 Iceland is all but officially bankrupt 
 
 


 
	 10. 
	 Taking a hard look at a Greenspan legacy 
 
 


 

	 
	 
  
  
 
 
			
			
			
			

					
	 				    
		 
			  
			 
			
				iht.com/energy
							
		  	 
		   						
		 
			
			 
			
	   						
	 
					
		 Oil's stunning retreat: How long can it last? 
	
		
		 More from Energy: 
	
			
			 
				
					 Reducing carbon emissions no easy task for Europe 
				
				
				
					 How U.S. candidates differ on energy policy 
				
				
				
					 Russia's oil boom: Miracle or mirage? 
				
				
																

	     
		
		     
	 				
		

			
			
			
			
 
	 
	 
Blogs: Globespotters 
	  
	  
	 
		Urban advice from reporters who live there.  
		» Take a look
	 
	 
 
 

			
			
			
				
				
				
				
				
	        
		 
		
	 
	

	
	
		 
	
	 
		 
			
			 Contact iht.com 
			  
			
				  	
				 
Close this window or Send us another message
 			
			
		 
	 	
	
	 
		 
			  
			 
				
					
					 Search 
									
			 
		 
		
		 
			 
				 News: 
				 
					 
						 Americas 
						 | 
						 Europe 
						 | 
						 Asia - Pacific 
						 | 
						 Africa &amp; Middle East 
						 | 
						 Technology &amp; Media 
						 | 
						 Health &amp; Science 
						 | 
						 Sports 
					 
				 
			 
			
			 
				 Features: 
				 
					 
						 Culture 
						 | 
						 Fashion &amp; Style 
						 | 
						 Travel 
						 | 
						 At Home Abroad 
						
						 | 
						 Blogs 
                                                 | 
                                                 Reader Discussions 
						 | 
						 Weather 
					 
				 
			 
			
			 
				 Business: 
				 
					 
						 Business with Reuters 
						 | 
						 World Markets 
						 | 
						 Currencies 
						 | 
						 Commodities 
						 | 
						 Portfolios 
						 | 
						 Your Money 
						 | 
						 Funds Insite 
					 
				 
			 
			
			 
				 Opinion: 
				 
					 
						 Opinion Home 
						 | 
						 Send a letter to the editor 
						 | 
						 Newspaper Masthead 
					 
				 
			 
			
			 
				 Classifieds: 
				 
					 
						 Classifieds Home 
						 | 
						 Properties 
						 | 
						 Education Center 
					 
				 
			 
			
			 
				 Company Info: 
				 
					 
						 About the IHT 
						 | 
						 Advertise in the IHT 
						 | 
						 IHT Events 
						 | 
						 Press Office 
					 
				 
			 
			
			 
				 Newspaper: 
				 
					 
						 Today's Page One in Europe 
						 | 
						 Today's Page One in Asia 
						 | 
						 Publishing Partnerships 
					 
				 
			 
			
			 
				 Other Formats: 
				 
					 
						 iPhone 
						 | 
						 IHT Mobile 
						 | 
						 RSS 
						 | 
						 AudioNews 
						 | 
						 PDA &amp; Smartphones 
						 | 
						 Netvibes 
						 | 
						 IHT Electronic Edition 
						 | 
						 E-Mail Alerts 
						 | 
						 Twitter 
					 
				 
			 
			
			 
				 More: 
				 
					 
						 Daily Article Index 
						 | 
						 Hyper Sudoku 
						 | 
						 IHT Developer Blog 
						 | 
						 In Our Pages 
					 
				 
			 
			
		 
	 
	
	 
		 
			
			 
				Subscriptions 
				Sign Up  |  Manage
			 
		 

		 
			 
				 Contact Us 
				 | 
				 Site Index 
				 | 
				 Archives 
				 | 
				 Terms of Use 
				 | 
				 Contributor Policy 
				 | 
				 Privacy &amp; Cookies 
			 
		 
		 Copyright © 2008 the International Herald Tribune All rights reserved 
	 		
 
	
	








        

























