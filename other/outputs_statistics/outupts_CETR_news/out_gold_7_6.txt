

 The market for analytical software designed to measure various business departments is expected to increase to $2.5 billion in 2007 from $1.4 billion last year, a market research firm said Monday. 
 
 
 
 
Revenue from so-called enterprise performance management analytics will increase at a compound annual growth rate of 11.9 percent over the next five years, ARC Advisory Group said.
 
 
 
 
EPM software is designed to measure the performance of key business departments, including finance; sales and marketing; operations such as supply-chain management; and human resources.
 
 
 
 
While ARC projects solid growth for the software, it warns that vendor hype may dampen future revenue.
 
 
 
 
""ARC has seen other software solutions that were primarily sold to CEOs and CFOs and promised strategic differentiation,"" ARC analyst Steve Banker said in a statement. ""In the early years, those over-hyped markets grew very fast. Then, the word got out that suppliers were over-promising and under-delivering. A sharp market contraction resulted. The fear is that the EPM segment may share the same fate.""
 
 
 
 
Following its five-year project, ARC expects the EPM growth rate to slow to less than the rate of inflation. 

