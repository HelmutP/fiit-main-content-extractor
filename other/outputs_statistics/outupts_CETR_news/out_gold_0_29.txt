

 If you are driving through Switzerland, south to Italy, you are likely to take the route via the charming town of Lucerne and that means driving through the Sonnenberg tunnel. 
 
 
Those tunnels around Lucerne can be quite irritating, especially in fine weather. Just as you are enjoying a spectacular view of the lake and the mountains, you are plunged into darkness. 
 
 
But when you get to the Sonnenberg, make sure your eyes adjust, and take a closer look, for this is much more than a tunnel. In here is the world's largest nuclear shelter. 
 
 
Under Swiss law, local governments are required to provide shelter spaces for everyone, and in the early 1970s Lucerne was short  by several thousand. The new Sonnenberg motorway tunnel, just being built, seemed a neat solution: kit it out as a nuclear shelter as well and it could hold 20,000 people.
 
 
    
    
	 

	
            
            
                
		
                
                     
                     
	 
		
		The Sonnenberg, in theory, is able to withstand a one megaton nuclear bomb, as close as half a mile away
		 	 




 
                
            
        
	
	
    
    




 ""Actually we got the idea from you British,"" explains Werner Fischer, the local civil protection chief, as he shows me around. ""Londoners used the underground as shelter during the blitz."" 
 
Well maybe, but believe me, there are things in the Sonnenberg that you will never find down the Finchley Road tube station.
 
 

'Engineering feat' 
 

 
 

It starts with the doors, which are a metre and a half thick (5ft), and weigh 350 tonnes each. The Sonnenberg, in theory, is able to withstand a one megaton nuclear bomb, as close as half a mile away. 
 
 

	
		 

			
			 
				
				 The shelter was designed to be self-sufficient  
			 
			
		
		
	

	


One megaton is 70 Hiroshimas. That means the Sonnenberg residents would have emerged to a world reduced not to smoking rubble, but to ash. 
 

Inside, the tunnel is a surreal monument to neutral Switzerland's desire to survive a total war which would, naturally, have been started and waged by someone else. 
 
 

Every eventuality has been thought of. 
 
 
There are vast sleeping quarters, with bunk beds four layers deep. There is an operating theatre, a command post, and as Mr Fischer points out, a prison. ""Just because there's a nuclear war outside doesn't mean we won't have any social problems in here,"" he says. 
 
 
 
 
    
    
	 

	
            
            
                
		
                
                     
                     
	 
		
		Some of my friends have private ones in their own houses, used, these days, mostly to store wine or skis.
		 	 




 
                
            
        
	
	
    
    




 


There were even, it is rumoured, plans for a post office, until someone asked the obvious question ""when the world outside is burning, who would you write to? What would the address be, not to mention who would deliver your letter?""
 
 

Then there are the coloured lights, indicating whether it is night or day outside. Obviously the country which produces the world's top watches would not like to lose track of time. 

 
 


There are some truly impressive feats of engineering: the air filters, designed to supply those 20,000 souls with 192 cubic metres each of non-radioactive air every day, are indeed breathtaking. So large, the hall they are housed in has the dimensions of a medieval cathedral.
 
 

Shelter choice

 
 

But while the Sonnenberg may be the biggest shelter, it is by no means the only one.
 
 

	
		 

			
			 
				
				 Many shelters are now being used a storage spaces 
			 
			
		
		
	

	



In fact, there are over a quarter of a million of them in Switzerland, because, 17 years after the end of the Cold War, the policy of providing shelters for the entire population still stands. 
 
Some of my friends have private ones in their own houses, used, these days, mostly to store wine or skis. My house, though does not have one. 
 
 

An anxious telephone call to my local civil protection office brings a reassuring answer. ""Actually your community has 40% overcapacity in shelters,"" I'm told. 
 
 
It turns out that, should the unthinkable happen, I have got a luxury of choice. I can settle into a cosy neighbourhood shelter designed for 10. Sounds good, as long as my family and the neighbours we get on with get there first. 
 
 

Or, there is a larger shelter, beneath the local fire station, which those without private shelters would share with the firemen. I can see it is not going to be the easiest of decisions. 
 
 

And down on the main street of my village, new houses are going up, the bulldozers are digging remarkably deep and  blast resistant concrete is arriving by the tonne. 
 
 
But why add an estimated 4% to the house price, just to carry on preparing for a threat that has gone away? 
 
 

	
		 

			
			 
				
				 Until the law changes, bunkers will continue to be dug 
			 
			
		
		
	

	


 

Karl Widmer, deputy director of Switzerland's civil defence department, looks a little sheepish when I put this to him. 
 
 

""We asked ourselves this question,"" he admits. ""But then we thought, we've built all these things, so let's just carry on. And there could be new threats around the corner.""
 
 

""What threats exactly?"" snorts a Social Democrat member of parliament. ""Bird flu? Terrorism? An underground bunker won't protect against that. It's time we stopped this nonsense, all we're doing is building very expensive wine cellars.""
 
 

Later this year, the Swiss government will decide whether to continue the shelters-for-all policy, but this week, sirens right across Switzerland were tested, and the population had to check their bunkers were up to scratch.
 
 

The monstrous Sonnenberg shelter though, is being gradually dismantled. But not because it has finally been deemed unnecessary: no, no, the real problem is those 350 tonne blast doors. When they were tested, they would not shut.

 
 





From Our Own Correspondent was broadcast on Saturday, 10 February, 2007 at 1130 GMT on BBC Radio 4. Please check the programme schedules  for World Service transmission times. 

