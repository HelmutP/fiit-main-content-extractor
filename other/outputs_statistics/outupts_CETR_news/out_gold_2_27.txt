

 "Grieving father says he gave son, 8, permission to fire Uzi
						
						
						 
 

October 27, 2008 02:18 PM


    Email|

Comments (629)|

    Text size
    –
    +
  
 
						
						 

						 
						
						 
						
						  

 By David Abel and Andrew Ryan, Globe Staff, and Matthew P. Collette, Globe Correspondent 

 Dr. Charles Bizilj stood 10 feet behind his son this weekend at a ""Machine Gun Shoot"" in Westfield, where the third grader aimed an Uzi at a pumpkin in the distance. 
 

 
 
Flier from Westfield Sportsman?s Club 

 
As Bizilj reached for his camera, the boy clutched the gun in his arms and squeezed the trigger. The Uzi flipped backwards and 8-year-old Christopher Bizilj fatally shot himself in the head. 

 ?It was all a blur,? Dr. Charles Bizilj said this afternoon in a telephone interview. ?I?m still in the grieving process.? 

 Christopher was accompanied by a trained professional as he held the 9-mm Micro Uzi machine gun at the Westfield Sportsman's Club Sunday afternoon, but Bizilj said he doesn?t think the shooting guide was holding the weapon as his son pressed the trigger. 

 ?This accident was truly a mystery to me,? he said. ?This is a horrible event, a horrible travesty, and I really don?t know why it happened. I don?t think it?s relevant that he wasn?t holding the weapon.? 

 He said his son, a third grader who loved to hike and bike, had experience firing handguns and rifles. But he said this was the first time he had fired an automatic weapon.  

 ""I gave permission for him to fire the Uzi,? Bizilj said. ?I watched several other children and adults use it. It?s a small weapon, and Christopher was comfortable with guns. There were larger machine guns with much more recoil, and we avoided those.? 
 Bizilj, the medical director of the emergency department at Johnson Memorial Hospital in Stafford Springs, Conn., said that his son was ?very cautious, very well trained, and very much enjoyed firing.? 

 When his son pressed the trigger Sunday, it was the first gun he had fired all day. ?It took about an hour to get there, and it was something he was looking forward to for months,? Bizilj said. 

 The annual Machine Gun Shoot and Firearms Expo is a two-day event. Police are investigating whether the Westfield Sportsman?s Club and the group running the event were licensed. ?We haven?t confirmed whether either have been licensed,? said Westfield Police Lieutenant Hipolito Nu?ez.  

 The sportsman's club boasted in an advertisement for the event posted on its website that the $5 entry fee was waived for children under age 16 and there was ""no age limit or licenses required to shoot machine guns.""  

 ""It?s all legal &amp; fun,"" the advertisement says. ""You will be accompanied to the firing line with a Certified Instructor to guide you. But You Are In Control ? ""FULL AUTO ROCK &amp; ROLL."" 

 Shooting targets for the event included vehicles, pumpkins, and ""other fun stuff we can?t print here,"" according to the advertisement.  

 Christopher Bizilj was firing the weapon at an outside firing range and was wounded once in the head when the recoil forced the gun to rotate upward and backward, Nu?ez said. The boy was taken to Baystate Medical Center in Springfield. He was pronounced dead at the hospital with one gunshot wound to the head. No one else was injured. 

 State law requires anyone under age 18 to have parental consent and a licensed instructor to fire an automatic weapon. Otherwise, there?s no minimum age to fire such a gun, Nu?ez said.  

 ?We do not know at this time the full facts of this incident, and it's being investigated,"" Nu?ez said. 

 The event at the club was organized by C.O.P. Firearms &amp; Training, an Amherst company that, according to its website, organizes machine gun shoots throughout New England. Officials from that group also could not be reached.  

  
(David Molnar / The Republican of Springfield) 

 Devin Connery of Lunenburg, Mass., fired a Heckler &amp; Koch UMP on Sunday at the Machine Gun Shoot and Firearms Expo at the Westfield Sportsman?s Club. This photograph was taken before 8-year-old Christopher Bizilj apparently lost control of an Uzi and shot himself in the head."
"2097","By David Abel and Andrew Ryan, Globe Staff, and Matthew P. Collette, Globe Correspondent 

 Dr. Charles Bizilj stood 10 feet behind his son this weekend at a ""Machine Gun Shoot"" in Westfield, where the third grader aimed an Uzi at a pumpkin in the distance. 
 

 
 
Flier from Westfield Sportsman?s Club 

 
As Bizilj reached for his camera, the boy clutched the gun in his arms and squeezed the trigger. The Uzi flipped backwards and 8-year-old Christopher Bizilj fatally shot himself in the head. 

 ?It was all a blur,? Dr. Charles Bizilj said this afternoon in a telephone interview. ?I?m still in the grieving process.? 

 Christopher was accompanied by a trained professional as he held the 9-mm Micro Uzi machine gun at the Westfield Sportsman's Club Sunday afternoon, but Bizilj said he doesn?t think the shooting guide was holding the weapon as his son pressed the trigger. 

 ?This accident was truly a mystery to me,? he said. ?This is a horrible event, a horrible travesty, and I really don?t know why it happened. I don?t think it?s relevant that he wasn?t holding the weapon.? 

 He said his son, a third grader who loved to hike and bike, had experience firing handguns and rifles. But he said this was the first time he had fired an automatic weapon.  

 ""I gave permission for him to fire the Uzi,? Bizilj said. ?I watched several other children and adults use it. It?s a small weapon, and Christopher was comfortable with guns. There were larger machine guns with much more recoil, and we avoided those.? 
 Bizilj, the medical director of the emergency department at Johnson Memorial Hospital in Stafford Springs, Conn., said that his son was ?very cautious, very well trained, and very much enjoyed firing.? 

 When his son pressed the trigger Sunday, it was the first gun he had fired all day. ?It took about an hour to get there, and it was something he was looking forward to for months,? Bizilj said. 

 The annual Machine Gun Shoot and Firearms Expo is a two-day event. Police are investigating whether the Westfield Sportsman?s Club and the group running the event were licensed. ?We haven?t confirmed whether either have been licensed,? said Westfield Police Lieutenant Hipolito Nu?ez.  

 The sportsman's club boasted in an advertisement for the event posted on its website that the $5 entry fee was waived for children under age 16 and there was ""no age limit or licenses required to shoot machine guns.""  

 ""It?s all legal &amp; fun,"" the advertisement says. ""You will be accompanied to the firing line with a Certified Instructor to guide you. But You Are In Control ? ""FULL AUTO ROCK &amp; ROLL."" 

 Shooting targets for the event included vehicles, pumpkins, and ""other fun stuff we can?t print here,"" according to the advertisement.  

 Christopher Bizilj was firing the weapon at an outside firing range and was wounded once in the head when the recoil forced the gun to rotate upward and backward, Nu?ez said. The boy was taken to Baystate Medical Center in Springfield. He was pronounced dead at the hospital with one gunshot wound to the head. No one else was injured. 

 State law requires anyone under age 18 to have parental consent and a licensed instructor to fire an automatic weapon. Otherwise, there?s no minimum age to fire such a gun, Nu?ez said.  

 ?We do not know at this time the full facts of this incident, and it's being investigated,"" Nu?ez said. 

 The event at the club was organized by C.O.P. Firearms &amp; Training, an Amherst company that, according to its website, organizes machine gun shoots throughout New England. Officials from that group also could not be reached.  

  
(David Molnar / The Republican of Springfield) 

 Devin Connery of Lunenburg, Mass., fired a Heckler &amp; Koch UMP on Sunday at the Machine Gun Shoot and Firearms Expo at the Westfield Sportsman?s Club. This photograph was taken before 8-year-old Christopher Bizilj apparently lost control of an Uzi and shot himself in the head. 
 

