

The detention of a senior Iraqi health ministry official accused of funding sectarian death squads is the latest move in a continuing, but still low-key offensive against Shia militia groups.
 
Iraqi and American special forces seized the deputy health minister, Hakim al-Zamili, early on Thursday after breaking into his Baghdad office - an arrest US officials describe as the most significant so far, following a series of raids in recent months.
 
 
The US military alleges he was involved in running kickback schemes which had provided millions of dollars to elements of the Jaysh al-Mahdi, or Mehdi army, the militia loyal to the radical anti-US Shia cleric Moqtada Sadr.
 
 
Death squads linked to the militia have been blamed for kidnapping and murdering thousands of Sunnis over the past year.
 
 
There is also concern about the activities of the Badr Brigades, which is linked to the largest party in the Shia coalition at the heart of the Iraqi government,  although its leaders claim it has reformed.
 
 
'Change of heart'
 
 
It is for such reasons that the government has been accused in the past of turning a blind eye to militia activities.
 
 
But the prime minister, Nouri Maliki, is now promising a harder line under the new Baghdad security plan - which has been getting off to a fitful start in recent weeks.
 
 
US commanders believe there has been a change of heart.
 
 
""We are now seeing a political willingness to confront the militias which we did not see last year"" said one US military official, although he insisted American forces were not just going after the Mehdi army but ""anyone breaking the law"".
 
 


	
		 

			
			 
				
				 The US says Mr Zamili is implicated in several deaths 
			 
			
		
		
	

	


 
But there is no sign of this turning a broad offensive against the Shia militias that some have called for.
 
 
Many Iraqi politicians - especially in the Shia parties that dominate the government - fear a wider crackdown would provoke far worse violence.
 
 
They point out that many Shias in Baghdad look to militias for security in the face of continuing bomb attacks on their communities by Sunni insurgents.
 
 
As a result, the approach continues to be one of ""targeted raids"" against individuals by US and Iraqi units.
 
 
Concern over crackdown
 
 
Last month, the US military announced it had 16 senior Mehdi Army figures in custody, as a result of raids over the previous six weeks.
 
 
US officials describe this latest arrest as the most significant carried out so far.
 
 
But although Iraqi forces are involved in these operations, it is clear the Americans are still in the lead.
 
 
The Iraqi troops involved in detaining the deputy health minister are under the control of the US Special Forces Command, an American spokesman admitted.
 
 


	
		 

			
			 
				
				 US soldiers have taken the lead in anti-militia operations 
			 
			
		
		
	

	


 
Concern about the potential impact of a wider crackdown on the Mehdi army and other Shia militias was underlined in a report released today by an Iraqi think tank with close government ties.
 
 
""Dismantling Iraq's militias"" by the Baghdad Institute for Public Policy Research says the emphasis should be on tackling the security vacuum which has driven many to support for such groups.
 
 
""Military confrontation with the militias at this stage would create a bloodbath"", Ahmed Shames, one of the report's authors told the BBC, "" when people see the militias as providers of security and of services.""
 
 
US approach working?
 
 
In the Mehdi Army stronghold of Sadr City in northeast Baghdad, the militia has long been involved in organising supplies of cooking gas and power, as well as protection.
 
 
It is too early to gauge the impact of the arrest of the deputy health minister. Nor is it clear what will happen.
 
 
But the Mehdi army does appear to be feeling the pressure as a result of this targeted US approach  - with many of its members now keeping their weapons out of sight and many of its leaders having left the capital.
 
 
There are fears it could simply be biding its time though, waiting for the pressure to relent. 

