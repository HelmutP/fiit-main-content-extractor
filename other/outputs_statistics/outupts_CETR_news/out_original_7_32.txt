














	
 



 
 
 The  Network 
 Tech News 
 Product Reviews 
 Business Intelligence 
 Security 
 Storage 
 VoIP 
 IT Management 
 Business 
 
 
 
 
IT News from 
 
 
Enterprise Product Reviews from 
 
 
Enterprise Software News from 
 
 
IT Security News from 
 
 
Enterprise Storage News From 
 
 
VoIP News from 
 
 
IT Management Insights from 
 
 
Business News from 
 
 	
 
	





 
 


 


 
 
 Register Now! 
 Benefits 
 Login 
 
 


 

 
 
 
  
 


SEARCH 


  
 
 
  
 

 


 
 
 
 Blogs 
 News 
 Mobile 
 Software 
 Security 
 E-Business &amp; Management 
 Networking 
 Hardware 
 Careers 
 White Papers 
 Newsletters 
 
 

 



 
 


Techweb - Synopsys Introduces Industry's First 90 Nanometer USB 2.0 On-The-Go PHY and Extends Its Hi-Speed USB PHY to 90 Nanometer Node



Press Releases Unedited news and product information from vendors.  Synopsys Introduces Industry's First 90 Nanometer USB 2.0 On-The-Go PHY and Extends Its Hi-Speed USB PHY to 90 Nanometer Node August 30, 2004 (1:00 PM EST) PRNewswire 




MOUNTAIN VIEW, Calif., Aug. 30 /PRNewswire-FirstCall/ -- Synopsys, Inc. , the world leader in semiconductor design software, today announced the availability of the DesignWare(R) USB 2.0 On-The-Go (OTG) PHY (Physical Layer) Core targeted to TSMC's 90 nanometer (nm), 130-nm and 180-nm processes as well as an extension of the Hi-Speed USB 2.0 PHY Core product line to the 90-nm process node. The DesignWare USB 2.0 OTG PHY is the industry's first 90-nm USB OTG PHY Core. The DesignWare USB 2.0 OTG PHY is interoperability tested and jointly certified with Synopsys industry leading digital USB cores, thus providing a complete drop-in solution with lower cost, form factor and risk.
 These 90-nm product introductions complement Synopsys' certified Hi-Speed USB 2.0 PHYs currently in volume production on TSMC's 180-nm and 130-nm processes. The new USB PHYs, which are complete physical layer interface cores compliant with the USB 2.0 OTG standard that provide an integrated solution that has been implemented in silicon and tested with Synopsys' leading family of digital USB intellectual property (IP) cores, including the DesignWare Hi- Speed OTG controller, Host Controller and Device Controllers. 
 The new OTG PHY handles HNP (host negotiation protocol) and SRP (session request protocol), which are the OTG-specific differences between the Hi-Speed 2.0 and the new OTG standard. Available targeted to TSMC's 180-nm, 130-nm and 90-nm processes, the OTG solution is based on the Synopsys USB 2.0 PHY that is already certified and shipping in volume. 
 USB OTG is a supplement to the USB 2.0 specification that increases the capability of existing mobile devices and USB peripherals by adding host functionality for connections to USB peripherals. Many of the new peripherals connected to PCs and laptops are portable devices. As these portable devices increase in popularity, there is a growing need for them to communicate directly with each other when a PC is not available. The OTG supplement addresses this need for mobile inter-connectivity and defines a way for portable devices, through only one mini-connector, to connect to supported USB products in addition to the PC. 
 ""With the introduction of our USB 2.0 OTG PHY, designers can speed integration and reduce risk when designing a complete digital and mixed-signal OTG solution targeted to TSMC's most popular foundry processes,"" said Guri Stark, Synopsys' vice president of Marketing, Solutions Group. ""For designers at the leading edge of system design, our Hi-Speed USB 2.0 PHY and USB 2.0 OTG PHY in TSMC's 90-nm process enable them to achieve USB 2.0 connectivity based on our USB-certified PHY architectures proven at 180-nm and 130-nm. Our complementary DesignWare USB PHY and digital IP solutions give designers a one-stop shop and help them lower design risk with proven, interoperable solutions."" 
 Pricing and Availability The DesignWare USB OTG PHYs and Hi-Speed USB 2.0 PHYs are available now for TSMC's 180-nm, 130-nm and 90-nm processes. These PHYs are also available for porting to additional foundry processes. Contact Synopsys for pricing information. 
 About DesignWare Cores DesignWare Cores provide system designers with silicon proven, digital and analog connectivity IP for some of the world's most recognized products including communications processors, routers, switches, game consoles, digital cameras, computers and computer peripherals. The DesignWare IP family includes industry leading connectivity IP Cores and Verification IP (e.g., USB 1.1, USB 2.0, USB 2.0 PHY, USB 2.0 OTG, USB 2.0 OTG PHY, PCI, PCI-X(R), PCI Express(TM), Ethernet...), AMBA(TM) on-chip bus (logic, peripherals, verification IP) and microcontrollers (8051, 6811). Provided as synthesizable RTL source code or in GDS format, these cores enable designers to create innovative, cost-effective systems-on-chip and embedded systems. Synopsys provides flexible licensing options for the DesignWare Cores. Each Core can be licensed individually, on a fee-per-project basis or users can opt for the Volume Purchase Agreement, which enables them to license all the cores under one simple agreement. For more information on DesignWare IP, visit http://www.synopsys.com/designware.
 About Synopsys Synopsys, Inc. is the world leader in electronic design automation (EDA) software for semiconductor design. The company delivers technology-leading semiconductor design and verification platforms and IC manufacturing software products to the global electronics market, enabling the development and production of complex systems-on-chips (SoCs). Synopsys also provides intellectual property and design services to simplify the design process and accelerate time-to-market for its customers. Synopsys is headquartered in Mountain View, California, and has more than 60 offices located throughout North America, Europe, Japan and Asia. Visit Synopsys online at http://www.synopsys.com/.
 Synopsys and DesignWare are registered trademarks of Synopsys, Inc. All other trademarks or registered trademarks mentioned in this release are the intellectual property of their respective owners.  CONTACT: Troy Wood of Synopsys, Inc., +1-650-584-5717 or twood@synopsys.com; or Julie Crabill of Edelman, +1-650-429-2732 or, for Synopsys, Inc.
Web site: http://www.synopsys.com/





  





Email This Story 





Print This Story 





See Complete List Of Vendor News 


















 
 


 

To see how Good Mobile Messaging 5 can help you achieve your goals, click
here.
 
 

Patch-and-pray doesnt work. Learn how Virus Throttling can rapidly contain attacks.
 
 

Mobilized Solutions Guide: Find and compare solutions for your business
 
 

Top Requested White Paper Categories from TechWeb White paper Library
 
 

SMB and Enterprise solutions for protecting the data center 
 
 


  
  
 
 


 
 
 Related White Papers 
 
 

Regulatory Compliance and Critical System Protection: The Role of Mission-Critical Power ...	

 
 

The Future of PR: How to Garner More Coverage and Stay Way Ahead of the Competition	

 
 

Enhancing Collaboration Between In-House and Agency Teams	

 
 

What Journalists Want to See on Your Public Relations Web Site	

 
 

Five Secrets for Media Relations Success in a PR 2.0 World	

 

  
  
  
 

 	
 
CAREER CENTER
 
 
 
 Ready to take that job and shove it? 
 
Open | Close
 
 
 
 
 
 SEARCH 
 
 


 
Function:  

Information Technology
Engineering

 
 
 
Keyword(s): 

 
 
 
State:
 
 

 
 
 
Post Your Resume
 
 
Employers Area
 
 
News &amp; Features
 
 
Blogs &amp; Forums
 
 
Career Resources 
  
Browse By:  
State | City 
 
 
 

 SPONSOR 
 
 

 
 
 RECENT JOB POSTINGS 
 
 

 

Featured Jobs:
 

BreakthroughIT seeking Project Manager in Groton, CT
 
  

Monsanto seeing IT Transportation and Optimization Analyst in St. Louis, MO
 
  

Princeton Financial Systems seeking Business Analyst 4 in Princeton, NJ
 
  

CSAA seeking IT Analyst IV in Glendale, AZ 
 
  

DisplaySearch seeking IT Project Manager in Austin, TX
 
  
For more great jobs, career-related news, features and services, please visit our ""Career Center.
 
 

 
 
 CAREER NEWS 
 
 
Five Rules For Bringing Your Real-Life Business Into Second Life 
 If you're thinking about establishing yourself in Second Life -- or are wondering whether you should -- we've got five rules that will help your new venture be a success. 
 
 
 
Hiring Report: Northrop Grumman Hiring On Wide Range Of Software Engineers  
 The global defense and tech company is seeking tech professionals skilled in Web site development, general software development, database administration, digital manufacturing, SAP/ABAP, complex CAD/CAM and PLM activities. 
 
 
 
 
 More Articles From Our Career Center 
  
 
 

Advertisement









 


 
 
 

 
 
 Specialty Resources 

 
 

IT Governance and Risk Management Resource
 
 

IT Service Continuity Process Roadmap
 
 

Best Practices for Deploying Wireless Messaging
 
 

Best Practices for BPM with SOA to Maximize ROI
 
 

Expert tips on next-gen IP address management
 
  
 
 VIEW ALL BRIEFING CENTERS 
 
  
  
 
 
 
 Featured Microsite 
 

 
  
  

 
 

 Microsites 
 Featured Topic 


 Additional Topics 
 
 

New Document Imaging Solutions from Kodak
 
 

Get CMP's Free ITIL Best Practices Playbook
 
 

  
 
 
 Crush The Competition 
 TechWeb's FREE e-mail newsletters deliver the news you need to come out on top. 




  
  
 
 
 Techencyclopedia 
 Get definitions for more than 20,000 IT terms. 




  
  

 
 
 Techwebcasts 
 Editorial and vendor perspectives 

Ease the SMB Storage Burden
 

Curb power and heat in the data center. Download free white paper.
 
 
  
  
 


 
 
 Vendor Resources 
 

Enterprise mobility whitepaper: How to unshackle your mission-critical applications
 

Ease the SMB Storage Burden
 
 
  
  
 


 
 
 Focal Points 
 

Ensure a good mobile experience for IT and end users.
 

SMBs: Taking backup and storage beyond tape. Download the free white paper.
 
 
  
  
 



 
 
  
	
 

  
  
 
 

 
 

  



 
  
 
 

 Free Newsletters 
 TechEncyclopedia 
 TechCalendar 
 Opinion 
 Research 
 Careers &amp; Workplace 
 Webcasts 
 About Us 
 Contact Us 
 
 Site Map 
 Mobile Tech News 
 Software Tech News 
 Security Tech News 
 E-Business Tech News 
 Management Tech News 
 
 Networking Tech News 
 Hardware Tech News 
 
  

 
 
 InformationWeek 
 Network Computing 
 Financial Technology Network 
 Wall Street &amp; Technology 
 Bank Systems &amp; Technology 
 
 Insurance &amp; Technology 
 IT Pro Downloads 
 Intelligent Enterprise 
 Byte and Switch 
 Dark Reading 
 bMighty 
 Small Biz Resource 
 
 Byte.com 
 VoipLoop 
 CollaborationLoop 
 Blackhat 
 
Interop 
 
 
  

 
 


&lt;noscript&gt;&lt;a href="" http: target="" _blank&gt;&lt;img border="" src="" http:&gt;&lt;/a&gt;&lt;/noscript&gt;
 
  


 
 
 Terms of Service 
 
Copyright ©2007 CMP Media LLC
 
 Privacy Statement 
 Your California Privacy Rights 
 Media Kit 
 Feedback 
 
  
 
 











