


 By Jason Szep 


 CONCORD, New Hampshire (Reuters) - Democratic presidential contender Hillary Rodham Clinton on Saturday attacked President George W. Bush for arrogance and incompetence in Iraq but faced tough questions over her own vote t"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:29PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      U.S.
      
      &gt;
      Article
 
 
 
       Hillary Clinton faces tough questions over Iraq 
       Sat Feb 10, 2007 11:12PM EST 
      
     

      
		

		 

	
	 
		

		

		

		 


 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		Top News
     
   
   
		 
				Obama sees new generation of leadership
  |  Video

			 
		 
				Olmert non-committal on Palestinian unity pact

			 
		 
				Hillary Clinton faces tough questions over Iraq

			 
		 
				Taliban prepare for spring offensive in Afghan south

			 
		 More Top News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Jason Szep 

    


 CONCORD, New Hampshire (Reuters) - Democratic presidential contender Hillary Rodham Clinton on Saturday attacked President George W. Bush for ""arrogance and incompetence"" in Iraq but faced tough questions over her own vote to authorize the war. 

    


 On her first visit in a decade to the state that helps kick off the 2008 White House race, Clinton told voters in New Hampshire that Iraq was a challenge because of ""the arrogance and incompetence of our administration in Washington."" 

    


 At a town hall meeting of about 300 people in the city of Berlin, the New York senator was asked by one participant to repudiate her 2002 Senate vote for a measure that cleared the way for the March 2003 invasion. 

    


 ""Knowing what we know now, I would never have voted for it,"" she responded. ""I gave him the authority to send inspectors back in to determine the truth. I said this is not a vote to authorize pre-emptive war."" 

    


 Later at a high school gym packed with about 3,000 people in the state capital, Concord, she was asked if she wanted to ""have it both ways"" by calling for the war's end after voting for the measure five years ago. 

    


 ""I do not believe that most of us who voted to give the president authority thought he would so misuse the authority we gave him,"" she replied. ""He said he was going to the United Nations to put inspectors in. He did, but then he didn't let them complete their mission and he rushed to war."" 

    


 Clinton, an opponent of Bush's plan to send 21,500 more U.S. troops to Iraq, is the early Democratic front-runner and received enthusiastic applause and standing ovations in both New Hampshire cities.  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				
						

				 
Jakarta flood clean-up could take months
 
				 mud and debris from flood-damaged homes on Sunday after days of relatively dry weather, but for many it could be one or two months before they can actually move back into their houses. 
                
                    Full Article  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















