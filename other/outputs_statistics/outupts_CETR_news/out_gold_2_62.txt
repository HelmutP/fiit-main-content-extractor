

 "Frank McKenna won't run for Liberal leadership



 
 Updated Tue. Oct. 28 2008 6:08 PM ET 

 CTV.ca News Staff 





 Frank McKenna, the popular former New Brunswick premier and U.S. ambassador, announced Monday that he will not run for the leadership of the federal Liberal Party of Canada.  
 
 ""Although I have been deeply moved by expressions of support for me from across the country, I have not been persuaded to change my long-standing resolve to exit public life for good,"" the 60-year-old McKenna said in a news release.  
 
 "" My only regret is that I cannot honour the expectations of friends and supporters who have shown enormous loyalty to me.""  
 
 McKenna served as New Brunswick's premier for 10 years, coming to power in 1987, winning all 58 seats in the legislature.  
 
 McKenna has repeated many times he has no desire to re-enter politics, especially at the federal level.  
 
 He said in Monday's news release that the challenge of winning a leadership race, restoring the health of the party and returning a Liberal majority government requires a longer time commitment than he is prepared to make.  
 
 ""There will be an ample number of well-qualified candidates to do this important work,"" he said.  
 
 Despite considerable urging from politicians to run, there were those in the party that feared his decade-long absence from politics would be a liability.  
 
 ""We remember what happened to John Turner. He was away for a while, he came back, the expectations were very high and he didn't meet them,"" said CTV parliamentary correspondent Roger Smith. ""Some people thought that could happen to McKenna as well.""  
 
 Turned resigned from politics in 1976 and was convinced to return after Pierre Trudeau retired almost a decade later. In 1984, Turner took a gamble, calling an early election and suffered an overwhelming defeat.  
 
 McKenna's struggle with the French language is also seen by some as a liability.  
 
 ""Certainly after the language difficulties of Mr. Dion, I'm sure the Liberal Party will be looking for someone as fluently bilingual as possible,"" said Smith.  
 
 McKenna was appointed deputy chairman of T-D Bank Financial Group in May 2006. He has also served on the boards of many national companies and also as Canada's ambassador to the U.S. He resigned that position after Stephen Harper's Conservatives came to power in 2006.  
 
 Liberal MPs Michael Ignatieff and Bob Rae were frontrunners in the 2006 Liberal leadership race that Dion eventually won. Both will likely play the same role this time around, although neither has officially declared they are running this time around.  
 
 New Brunswick MP Dominic LeBlanc, the son of former governor general Romeo LeBlanc, indicated to the Globe and Mail on Monday that he would run, although he has not official announced his candidacy. Because LeBlanc is also from New Brunswick, many Liberals took this as a sign that McKenna would not be running.  
 
 A leadership convention would likely take place next May in Vancouver, where the party already has convention space booked for what was scheduled to be its biennial policy conference."
"3272","CTV.ca News Staff 





 Frank McKenna, the popular former New Brunswick premier and U.S. ambassador, announced Monday that he will not run for the leadership of the federal Liberal Party of Canada.  
 
 ""Although I have been deeply moved by expressions of support for me from across the country, I have not been persuaded to change my long-standing resolve to exit public life for good,"" the 60-year-old McKenna said in a news release.  
 
 "" My only regret is that I cannot honour the expectations of friends and supporters who have shown enormous loyalty to me.""  
 
 McKenna served as New Brunswick's premier for 10 years, coming to power in 1987, winning all 58 seats in the legislature.  
 
 McKenna has repeated many times he has no desire to re-enter politics, especially at the federal level.  
 
 He said in Monday's news release that the challenge of winning a leadership race, restoring the health of the party and returning a Liberal majority government requires a longer time commitment than he is prepared to make.  
 
 ""There will be an ample number of well-qualified candidates to do this important work,"" he said.  
 
 Despite considerable urging from politicians to run, there were those in the party that feared his decade-long absence from politics would be a liability.  
 
 ""We remember what happened to John Turner. He was away for a while, he came back, the expectations were very high and he didn't meet them,"" said CTV parliamentary correspondent Roger Smith. ""Some people thought that could happen to McKenna as well.""  
 
 Turned resigned from politics in 1976 and was convinced to return after Pierre Trudeau retired almost a decade later. In 1984, Turner took a gamble, calling an early election and suffered an overwhelming defeat.  
 
 McKenna's struggle with the French language is also seen by some as a liability.  
 
 ""Certainly after the language difficulties of Mr. Dion, I'm sure the Liberal Party will be looking for someone as fluently bilingual as possible,"" said Smith.  
 
 McKenna was appointed deputy chairman of T-D Bank Financial Group in May 2006. He has also served on the boards of many national companies and also as Canada's ambassador to the U.S. He resigned that position after Stephen Harper's Conservatives came to power in 2006.  
 
 Liberal MPs Michael Ignatieff and Bob Rae were frontrunners in the 2006 Liberal leadership race that Dion eventually won. Both will likely play the same role this time around, although neither has officially declared they are running this time around.  
 
 New Brunswick MP Dominic LeBlanc, the son of former governor general Romeo LeBlanc, indicated to the Globe and Mail on Monday that he would run, although he has not official announced his candidacy. Because LeBlanc is also from New Brunswick, many Liberals took this as a sign that McKenna would not be running.  
 
 A leadership convention would likely take place next May in Vancouver, where the party already has convention space booked for what was scheduled to be its biennial policy conference. 

