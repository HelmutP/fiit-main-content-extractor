

 //W3C//DTD HTML 4.01 Transitional//EN""&gt;

 
 Project Midway  :: CHICAGO SUN-TIMES :: 






	
	
	













    Back to regular view     Print this page
 





 



 








 




Your local news source ::       Select a community or newspaper »
 


	
        
		
	 	
		



Select a community 
------------------
Algonquin
Alsip
Antioch 
Arlington Heights
Aurora
Bannockburn
Barrington 
Barrington Hills
Batavia
Beecher
Bellwood
Berkeley
Blue Island
Bolingbrook
Broadview 
Buffalo Grove
Burr Ridge
Calumet City
Cary 
Chesteron, Ind.
Chicago
Chicago - Albany Park
Chicago - Avondale
Chicago - Belmont Cragin
Chicago -  Bucktown
Chicago - Dunning
Chicago - Edgebrook
Chicago - Edgewater
Chicago - Edison Park
Chicago - Jefferson Park
Chicago - Harlem-Irving
Chicago - Lakeview
Chicago -  Logan Square
Chicago - News Star
Chicago - North Center
Chicago - North Town
Chicago - Norwood Park
Chicago - Portage Park
Chicago - Ravenswood
Chicago - Rogers Park
Chicago - Roscoe Village
Chicago - Sauganash
Chicago - Skyline News
Chicago -  Ukranian Village
Chicago - Uptown
Chicago -  Wicker Park
Chicago Heights
Clarendon Hills
Country Club Hills
Crete
Crestwood
Crown Point, Ind.
Darien
Deerfield
Deer Park
Des Plaines
Dolton
Downers Grove
Elburn
Elgin
Elk Grove
Elmhurst 
Elmwood Park
Evanston 
Flossmoor
Forest Park
Fox River Grove
Fox Valley
Frankfort
Franklin Park 
Gages Lake
Gary, Ind.
Geneva
Glen Ellyn
Glencoe 
Glenview 
Glenwood
Golf 
Grayslake
Green Oaks
Gurnee
Harwood Heights
Harvey
Hawthorn Woods
Hazel Crest
Highland Park
Highwood
Hillside
Hinsdale 
Hobart, Ind.
Hoffman Estates
Homer Glen
Homer Twp.
Homewood
Indian Head Park
Inverness 
Island Lake
Joliet
Kenliworth
Kildeer
La Grange
LaGrange Highlands
LaGrange Park
Lake Barrington 
Lake Bluff 
Lake Forest 
Lake in the Hills 
Lake Villa 
Lake Zurich
Lansing
Lemont
Libertyville
Lincolnshire
Lincolnwood
Lindenhurst 
Lisle
Lockport
Long Grove
Lowell, Ind.
Manhattan
Markham
Matteson
Maywood 
Melrose Park 
Merrillville, Ind.
Midlothian
Mokena
 Montgomery
Morton Grove
Mount Prospect 
Mundelein 
Naperville
New Lenox
Niles
Norridge
North Barrington 
Northbrook 
Northfield
Northlake
Northwest Ind.
Oak Brook 
Oakbrook Terrace 
Oak Forest
Oak Lawn
Oak Park
Oakwood Hills
Olympia Fields
Orland Hills
Orland Park

 Oswego 
Palatine 
Palos Heights
Palos Hills
Palos Park
Palos Twp.
Park Forest
Park Ridge 
Plainfield

Portage, Ind.
Prospect Heights 
Richton Park
River Forest
River Grove
Riverwoods
Rolling Meadows 
Rosemont
Round Lake
Sauk Village
Schaumburg 
Schererville, Ind.
Schiller Park 
Silver Lake
Skokie 
South Barrington 
South Elgin
South Holland
St. Charles
Steger
Stone Park 
Thornton Twp.
Tinley Park/Southland
Tinley Park
Tower Lake
University Park
Valparaiso, Ind.
Venetian Village 
Vernon Hills 
Wadsworth
Wauconda 
Waukegan
West Lake Co., Ind.
West Proviso
Westchester 
Western Springs
Wheaton
Wheeling
Wildwood
Willowbrook 
Wilmette
Winnetka
Worth
Worth Twp.

 Yorkville 







Select a STNG publication or site
  
---------------------------------------------------
 DAILY PUBLICATIONS 
---------------------------------------------------
	Chicago Sun-Times	
	The Beacon News	
	The Courier News	
	The Daily Southtown	
	The Herald News	
	Lake County News-Sun	
	The Naperville Sun	
	Post-Tribune	


  
---------------------------------------------------
 SEARCH CHICAGO 
---------------------------------------------------
Autos
Homes
Jobs


  
---------------------------------------------------
 NEIGHBORHOOD CIRCLE 
---------------------------------------------------
 Montgomery
 Oswego 
 Yorkville 

  
---------------------------------------------------
 ENTERTAINMENT 
---------------------------------------------------
Centerstage
 Roger Ebert


  
---------------------------------------------------
 WEEKLY &amp; SEMIWEEKLY PUBLICATIONS 
---------------------------------------------------
	Algonquin Countryside	
	Antioch Review	
	Arlington Heights Post	
	Barrington Courier-Review	
	Booster: Lake View, North Center,
        Roscoe Village, Avondale Edition	
	Booster: Wicker Park, Bucktown,
	      Ukrainian Village Edition	
	Buffalo Grove Countryside	
	Cary-Grove Countryside	
	Deerfield Review	
	Des Plaines Times	
	Edgebrook-Sauganash Times-Review	
	Edison-Norwood Times-Review	
	Elk Grove Times	
	Elm Leaves	
	Evanston Review	
	Forest Leaves	
	Franklin Park Herald-Journal	
	Glencoe News	
	Glenview Announcements	
	Grayslake Review	
	Gurnee Review	
	Highland Park News	
	Hoffman Estates Review	
	Lake Forester	
	Lake Villa Review	
	Lake Zurich Courier	
	Libertyville Review	
	Lincolnshire Review	
	Lincolnwood Review	
	Morton Grove Champion	
	Mount Prospect Times	
	Mundelein Review	
	News-Star	
	Niles Herald-Spectator	
	Norridge-Harwood Heights News	
	Northbrook Star	
	Oak Leaves	
	Palatine Countryside	
	Park Ridge Herald-Advocate	
	Proviso Herald	
	River Grove Messenger	
	Rolling Meadows Review	
	Schaumburg Review	
	Skokie Review	
	Skyline	
	The Batavia Sun	
	The Bolingbrook Sun 	
	The Doings Clarendon Hills Edition	
	The Doings Elmhurst Edition	
	The Doings Hinsdale Editon	
	The Doings La Grange Edition	
	The Doings Oak Brook Edition	
	The Doings Weekly Edition	
	The Doings Western Springs Edition	
	The Downers Grove Sun	
	The Fox Valley Villages Sun	
	The Geneva Sun	
	The Glen Ellyn Sun	
	The Homer Sun	
	The Lincoln-Way Sun	
	The Lisle Sun	
	The Plainfield Sun	

	The St. Charles Sun	
	The Star: Chicago Heights Area	
	The Star: Country Club Hills, Hazel Crest
	The Star: Crete, University Park, Beecher	
	The Star: Frankfort, Mokena	
	The Star: Homer Glen, Lockport, Lemont	
	The Star: Homewood, Flossmore, Glenwood,
        Olympia Fields	
	The Star: New Lenox, Manhattan	
	The Star: Oak Forest, Crestwood, Midlothian	
	The Star: Oak Lawn, Palos &amp; Worth Townships	
	The Star: Orland Park, Orland Hills	
	The Star: Park Forest, Matteson, Richton Park	
	The Star: South Holland, Thorton Township	
	The Star: Tinley Park	
	The Wheaton Sun	
	Times Harlem-Irving	
	Times Jefferson Park, Portage Park,
       Belmont-Cragin Edition	
	Vernon Hills Review	
	Wauconda Courier	
	Wheeling Countryside	
	Wilmette Life	
	Winnetka Talk	





























 
 







 



 






 
 

 

    
 








 

 
 

 
 

 suntimes.com
 Member of the Sun-Times News Group 
 



 

 
 


Traffic  �  
Weather:  

""WHEW!""


		
 
 
 


Search » 

 

 
 Site
 STNG 




 

  
 

� Subscribe 
� Easy Pay 
� Reader Rewards 
� Customer Service 
� Email newsletters 


 

 

 

 

Home
 | 
News
 | 
Commentary
 | 
Sports
 |  
Business 
 | 
Entertainment
 | 
Classifieds
 | 
 Columnists
 | 
Lifestyles
 | 
Ebert
 | 
Search
  | 
Archives
 | 
Blogs
   | 
  RSS 
 
   




























 










  









 
 
  
  
  
  
    
 
    
     
  	
  
    Bears vs. Colts 
    
    
     Main Page 
  	  
     Fan Guide 
  	  
     News &amp; Notes 
  	  
     Advertising &amp; Media 
  	  
     Season Recap 
  	  
     Where to watch  
  	  
     History 
  	   
  	
  
    Chicago Bears 
    
    
     Roster 
  	  
     History 
  	  
     Depth Chart 
  	  
     Playoff Statistics 
  	  
     Statistics 
  	  
     Schedule/Results 
  	  
     Box Scores 
  	  
     Team Information 
  	   
  	
  
    Indianapolis Colts 
    
    
     Roster 
  	  
     History 
  	  
     Depth Chart 
  	  
     Statistics 
  	  
     Playoff Statistics 
  	  
     Schedule/Results 
  	  
     Box Scores 
  	  
     Team Information 
  	   
  	
  
    Columnists 
    
    
     Greg Couch 
  	  
     Jay Mariotti 
  	  
     Mike Mulligan 
  	  
     Carol Slezak 
  	  
     Rick Telander 
  	   
  	
  
    March to Miami 
    
    
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
      
  	  
     Buy Bears Prints 
  	  
      
  	  
      
  	  
      
  	  
      
  	   
  	
  	 
  
 	

  

 


    
   
  
  	   
	
	
	  
  
	  	
  


 

  

 

 


 





 


 








 






 






 
 :: 



printer friendly »     
email article »  



 
 
 







 





 







  
 



 







 

  







 



 

 
VIDEO ::    MORE »
  
 
 
 

 
 
 

  
 
TOP STORIES ::
  
 
 NEWS 











Dry out, power up   


  
 BUSINESS 











W.W. Grainger adds new stores to area 


 
 SPORTS 











Bears are well grounded   


 
 ENTERTAINMENT 











Do celebs get off easy?   


 
 LIFESTYLES 











Glamorama: Coyote pretty  


 




 




 

  





 
 
 















	
	 
	
	
			  
				



 
		 
 Project Midway 
		  Fashion students give Bearswear a superchic makeover  
		
		
 
		
		

		 
 February 1, 2007 


		  BY LESLIE BALDACCI   Staff Reporter    
			
					
						
						
 Call it ""Project Midway."" The concept: Give Bears fans a little fashion with their passion. We asked fashion design students from The Illinois Institute of Art-Chicago to transform a selection of boxy, generic Bears T-shirts and stocking caps into something more -- more fun, more unusual, more stylin' -- that women would love to wear to the biggest party of the year on Super Sunday. 
 
 
In a matter of hours, they transformed $20 shirts and $10 stocking caps into one-of-a-kind jock frocks. The best part? Yes, fans, you can try this at home. 
 
 lbaldacci@suntimes.com 
 
 



 
 



















 » Click to enlarge image
 





 The Illinois Institute of Art-Chicago fashion design students participate in our challenge to transform a selection of boxy, generic Bears T-shirts and stocking caps into something more.  (Dom Najolia/Sun-Times)
 
 







PHOTO GALLERY 

	


		 

� Stylin? Bears fashions 
 
	
	
	










	
	

	

	

	













	
 
 		



 CORTNEY DUVAL, 21 
FRESHMAN FROM THE GOLD COAST 
 Duval is a football fan ""and a big fan of hoodies."" She chose a large-sized, short-sleeved, navy blue ""2006 Conference Champions"" T-shirt (""I really like the Bear logo"") and went to work. 
 
 First she turned the shirt inside out and cut along the side and underarm seams, removing about three inches from the sleeves, sides and bottom. She re-sewed the seams, creating a much smaller shirt. She cut football-shaped pockets from an orange knit cap; added shoulder pad-motif accents and a waistband using pieces of striped jersey rib knit ($5 at a fabric store). Finally, she added a gray fleece hood, cut away a section of the front and added big orange laces. 
 
 ""It reminds me of 'Sporty Spice,'"" said Duval. 
 
 
 
 JASMINE JONES, 21 
SENIOR FROM THE WEST SIDE 
 Jones chose the shirt with the simplest design, a navy blue, long sleeved, size XL, with a classic orange ""C"" on the front. Jones cut around the crew neck, leaving it attached for about six inches at the front. She cut off the sleeves, following the curve of the C down the sides, under the arm and straight across the back. (The shoulders, sleeves and upper back of the shirt -- still in one piece -- became a shrug.) She made a second cut, under the bust, all the way around. She cut a three-inch band of orange satin and attached to the halter top. She loosely gathered, then attached, the shirt bottom to the orange band to complete her halter dress. 
 
 
 
 JESSICA OLIVERII, 25 
SENIOR FROM NORTHBROOK 
 Oliverii turned a gray ""NFC Champions"" shirt into a drawstring tube top. She cut across the top, attached a band she salvaged from a navy blue shirt, and ran an orange drawstring to snug the top. She sewed darts for a custom fit. At the bottom, she added a panel of stretchy orange fabric. Another scrap of navy shirt became a scarf, with orange feathers for a girly accent. ""It's a jock frock,"" said Oliverii, ""a trendy tunic."" 
 
 
 
 PRIYA PANDEY  
INSTRUCTOR FROM NAPERVILLE 
 Pandey, who teaches clothing construction and pattern making, cut a tiny tank top from a large shirt, and had enough fabric left over to whip up a pleated mini-skirt. The waistband was a knit hat in a former life. 
 
 ""I have a 13-year-old daughter, and she's going to a Super Bowl party,"" Pandey said. ""I'm keeping her in mind."" 
 
 Her daughter Pallavi models the outfit. 
 
 The seventh-grader's verdict: ""It's cute."" 
 
 
 
 JENNIFER WRIGHT, 22 
SENIOR FROM LINCOLN PARK 
 Wright cut a scoop neck and trimmed it with a stretchy band of blue sequins. ""You can wear it off the shoulders, or off one shoulder,"" she said. She added orange ribbon stripes to the sleeves, then made a deep cut down the back and laced it up like a football. Last, she added an orange ruffle at the bottom. 
 
 
 
 PAULA ADAMS, 23 
SENIOR FROM LOGAN SQUARE 
 Adams took a long-sleeved XL shirt and turned it into an empire-waist tunic with leg warmers. First she cut off the sleeves, removed the crew neck and cut down the front to form a v-neck. Then she cut the shirt in half under the logo. She cut a three-inch rim from a Bears knit stocking cap and attached it to the shirt top. She attached the bottom half of the shirt to the other edge of the band. White laces completed the tunic. She attached two pieces from the stocking cap to the sleeves. Voila: leg warmers! 
 
 ""This is trendy but casual,"" Adams said. 
		
 


















 
 


				











 
 




  

  













 	



 








 
 


  
 suntimes.com:  Send feedback | Contact Us | About Us | Advertise With Us |  Media Kit |  Make Us Your Home Page  
Chicago Sun-Times: Subscribe | Customer Service
 | Reader Rewards |   Easy Pay |  e-paper | P.M. Edition |  Online Photo Store  
Affiliates:  jump2web |  RogerEbert.com |  
SearchChicago -  Autos | 
SearchChicago -  Homes | 
SearchChicago - Jobs | 
NeighborhoodCircle.com |  Centerstage    Partner: NBC5.com


   






 


 
















  
? Copyright 2007 Sun-Times News Group | Terms of Use and Privacy Policy   
 


Member of the Real Cities Network



 























 




