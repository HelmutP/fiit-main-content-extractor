

 DENVER -  The word went out from American Red Cross headquarters to 900 chapters across the country last week: Stop taking donations of anything but money for U.S. troops fighting in the Persian Gulf. 
 
 ""Enough already"" is a hard message to convey when so many people want to give some tangible sign of support--be it as humble as toothpaste or hand lotion--to troops deployed in the desert. 
 
  But military and relief organizations, inundated with donations from individuals, church groups, schools and community organizations in the past few weeks, are asking people to refrain from sending care packages to the troops, unless they're friends or family members. 
  

                     
                        
                        
							
                            
                                

                                

                                
                                     
                                        
                                         
 
                                                        
	                                                        

� Laurie Goering talks about her coverage of the Iraq war 

� Archives 


April 30, 2003

                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        
America at War




                                                        
                                                        

	                                                
 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        



� Latest Tribune photos






 � 
Photo archives 



                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        
 
Tribune war dispatches


                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        










� Archives











                                                        
                                                        

	                                                
 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        



� Archives 




                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        

� Day-by-day summary of the war with Iraq







                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
                                                        

	                                                 
												    
	                                                    
												   	     
													     
														     Quote 
														     ""The new ruler of Iraq is going to be an Iraqi. I don't rule anything."" 
													     
													     
													    
                                                     
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        


� Diplomatic efforts

 � World reaction

 � Casualties

 � Iraq reaction

 � Midwest reaction

 � Protests

 � Military

 � Homefront

 � Iraq's future

 � Media

 � Business

 � Technology

 � At random

 � Commentary







                                                        
                                                        

	                                                
 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        


� March archives

 � February archives

 � January archives

 � December archives

 � November archives

 � October archives

 � September archives





                                                        
                                                        

	                                                
 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        

Because of biological and chemical threats, the military no longer delivers mail marked for ""Any Soldier"" or ""Any Servicemember."" Other options:

 E-mail:
 OperationDearAbby.net
 Defend America

 Donate:
 Operation Uplink
 USO Care Package

 Volunteer:
 Veterans Affairs 




                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                         

� Saddam profile

 � Iraqi leadership

 � Iraqi military

 � Iraqi history

 � About Iraq






                                                        
                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
	                                                        
These links will take you outside chicagotribune.com
 
� U.S. Dept. of State


 � Military, demographics and diplomacy 

 � Iraq: World FactBook


 � Geneva Conventions


 � Maps of Iraq

 





                                                        
                                                        

	                                                 
 
                                     
                                

                            
                        

                        
                        




    
        
            
        
    

                        

                        
                         
                            
                                
                            
                         
                        
                     


                     
 Military supply lines in the Persian Gulf can't accommodate the volume of material from well-wishers, officials said. 
 
 ""It's putting a tremendous strain on the ability of the military postal system to deliver mail from soldiers' loved ones,"" said Mark Saunders, a U.S. Postal Service spokesman. 
 
 There is no way to sort mail from family and friends and give it higher priority, he said. 
 
 ""People think one little package won't hurt,"" said Carrie Lee of Georgia, who runs Sgt. Mom's, a Web site for military families. ""But they don't realize thousands of other people are all thinking the same thing, and it's really bogging the mail system down."" 
 
 The Red Cross initiated its moratorium after being overwhelmed by donations of everything from sunscreen to lip gloss to CDs. 
 
 ""We have enough stuff to maintain uninterrupted distribution to service members for at least the next four months,"" said Stacey Grissom, a Red Cross spokeswoman. 
 
 Mail system already taxed 
 
 Anxiety over possible terrorist attacks adds to an already burdened mail system. 
 
 After Sept. 11, 2001, and the subsequent anthrax scares, the military stopped taking letters or packages addressed to ""any service member,"" a common practice in times of war. Now, only carefully screened items sent to specific service members are accepted. 
 
 In the U.S., every granola bar or box of tissues sent to troops has to be sealed in original packaging and checked for tampering. Overseas, Grissom said, ""there's nowhere to store this stuff. And you have to have people to open all these boxes and distribute the items."" 
 
 Military personnel in the desert already have enough to handle, she said. 
 
 Declining generous offers isn't easy and sometimes can be awkward. 
 
 In Maine, Red Cross officials had to turn down a company that wanted to donate a truckload of cookies that would have been too expensive to ship to the region. In Summerville, S.C., 250 care packages donated by local schoolchildren came close to being rejected by Charleston Air Force Base because they weren't addressed to specific service members. 
 
 In Colorado, several church groups and Scout troops recently organized donation drives for local National Guard members, only to find out that ""we can't send these things overseas for them,"" said Capt. Holly Peterson, spokeswoman for the Colorado National Guard. 
 
 ""Sometimes people do get frustrated,"" she said. 
 
 Instead of giving material goods, Peterson suggested, people can write checks to military relief societies, e-mail the troops, or donate time or services, such as baby-sitting or tutoring, to military families. 
 
 Persistent donors 

