

 //W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd""&gt;

 

BUSINESS NEAR YOU: Colorful Birmingham shop offers gifts, home accessories































  















 


	 
	
	
		 
			  
		
		
		
		 
		
	
	 

	 
	
	
		 
		
			 
				 Freep.com 
				 • 
				 Weather 
				 • 
				 Jobs 
				 • 
				 Cars 
				 • 
				 Real Estate 
				 • 
				 Apartments 
				 • 
				 Shopping 
				 • 
				 Classifieds 
				 • 
				 Dating 
			 

		 
		
		 
		
		
			
				 
					 
						 Emails 
						 RSS 
						 Text Me Freep 
						 Mobile 
					 
					 
						 Subscribe to the Free Press 
						 Manage My Account 
						 Place an Ad 
						 Contact Us 
					 
				
					 
					
					
Search Detroit:
	
			All
			Recent News
			Business Directories
			Classifieds
			Cars
			Jobs
			Shopping
		
	

	

	
					 
				
				 
		
			  
			
			 
			
		
		 
	
	
	 
	
			 
		 
LOCAL NEWS
		 
			 Front 
			 Community 
			 Metro 
			 Detroit 
			 Wayne 
			 Oakland 
			 Macomb 
			 Michigan 
			 Roadwork &amp; Traffic 
		 
	
		 
	
		 
NATION/WORLD
	
		 
			 Front 
			 USA Today 
			 AP Wire 
		 
	
		 
	
		 
SPORTS
	
		 
			 Front 
			 Scores 
			 Lions/NFL 
			 Red Wings/NHL 
			 Tigers/MLB 
			 Pistons/NBA 
			 Shock/WNBA 
			 U-M 
			 MSU 
			 Other Colleges 
			 Preps 
			 Outdoors 
			 Olympics 
			 Auto Racing 
			 Golf 
			 Fan Shop 
		 
	
		 
		
		 
ENTERTAINMENT
	
		 
			 Front 
			 Movie Showtimes 
			 Arts 
			 Movies 
			 DVD 
			 Music 
			 Names &amp; Faces 
			 Dining Out 
			 Nightlife 
			 Casinos 
			 TV &amp; Radio 
			 Video Games 
		 
	
		 
		
		 
BUSINESS
	
		 
			 Front 
			 Michigan 
			 Nation/World 
			 My Market Watch 
			 Air Travel 
			 Technology 
			 Real Estate 
		 
	
		 
		
		 
AUTOS
	
		 
			 Front 
			 Auto Reviews 
			 Auto Shows 
			
		 
	
		 
		
		 
FEATURES
	
		 
			 Front 
			 Lifestyles 
			 Books 
			 Fashion 
			 Food 
			 Gardening 
			 Health 
			 Comics 
			 Twist 
			 Yak's Corner 
			 Pets 
			 College Guide 
		 
	
		 
		
		 
TRAVEL
	
		 
			 Front 
			 USA Today 
		 
	
		 
		
		 
OBITUARIES
	
		 
			 Front 
			 Death Notices 
		 
	
		 
		
		 
OPINION
	
		 
			 Front 
			 Editorials 
			 Letters 
			 Editorial Cartoons 
			 Bloggers 
			 Columnists 
			 Forums 
		 
	
		 
		
		 
TWIST

	
		 
		
	 
		
	 
	
	 
	
	
		 
			
			



						 
			
				
				 
				
				 
  
				 
				 
				 Homepage 
				 
				
				 
 Forums 
				 RSS 
 
				
				 
				
				 
				 Email This Story 
				 Printable Format 
				 
				
			
			 
			
			 
			
				
				

			
				
			
			
			
			
			
		
		 
		
								
		
				

				

				
				
				
				
				
		
				
				
				
			
			
			 
			 SHOPPING 





 Find the best sales and deals at major retailers near you. 
Start Shopping! 



 






 Free Press Bookstore 
 Click here for sports books and posters, sports wearables, cookbooks and more! 




 
			 
			
			 
			
			
				 MORE ON FREEP.COM 
				
				 Most Popular: 
						
 
						
							 DETROIT 9, NEW YORK 6 (11 INNINGS): THE LONGEST WIN: Guillen's 3-run HR beats Yanks after huge rain delay 
						
							 Photo Gallery - Tornados, storms rake metro Detroit 
						
							 Homes damaged, streets flooded after tornadoes, storms rake area 
						
							 RAIN DELAY FUN: Fans wait 8-plus hours; kids stay up late; lots of beverages served 
						
							 TODD JONES: What players do during rain delays 



 


				 Latest Updates: 
					
					 
						
	
	

	
	 
Kyle O'Neill's game blog as Lions take on Manning - 7:10 pm
 
	

	
	 
Tigers continue up-and-down play - 7:05 pm
 
	

	
	 
Sheriff says suspected drugs found in rapper's home - 5:28 pm
 
	

	
	 
Workouts to go - 5:13 pm
 
	
 
					 			
				
			
			 
			
			
			 


		
		
		 
		
		 
		
		
			 
			
				
				 
				 
					 Freep.com 
										 » 
					 Community 
					 » 
					 CFP Birmingham 		
				 
				 
				
								
				
								 BUSINESS NEAR YOU 
				 Colorful Birmingham shop offers gifts, home accessories 
				
				
				 August 19, 2007 
				
				 BY CHRIS KUCHARSKI 
				
 FREE PRESS STAFF WRITER 

				 


Company: Fuchsia Frog, 320 E. Maple, Birmingham. 
 


Owner: Meg Ferron of Bloomfield Hills. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


Background: The store opened in May 2003. 


Ferron's mother came up with the name. ""We both loved the sound of it,"" Ferron said. 
 


Ferron had periodically worked in retail, and she wanted a creative environment. 
 


""I grew up surrounded by beautiful things,"" Ferron said. 
 


Its niche: The store carries a range of gift items, from home accents to baby clothes. Some of its most popular items are Marye-Kelley decoupage frames that can be personalized for any occasion. The Marye-Kelley line also offers monogrammed wine coasters. 
 


The store has a baby registry and carries the Zutano line of baby clothing, which is popular because the soft cotton pieces come in a wide variety of prints and colors that can be continually mixed and matched. 
 


Items for the home include scented candles by Seda France, Sabre knives and serving pieces, and unique message pillows. 
 


Gift wrapping is available for an additional cost. 
 


Hours: 10 a.m.-6 p.m. Monday-Friday, 10 a.m.- 5 p.m. Saturday, closed Sunday. 
 


For more information: Call 248-203-6550. 
				
				
				
				
				
				
				 
				
				
				.
 
 

 

	 
		 
Post a Comment
 
	 

 

 

 


 This article does not have any comments associated with it 


 

 

				
				 
					
				
			
			 
		
			 
		
		
		 
	
	
	 
	
	 
	
		
		 
			 Site index 
		 
		
		
		
			
 News 
				 
					 Front 
					 Community 
					 Metro 
					 Detroit 
					 Wayne 
					 Oakland 
					 Macomb 
					 Michigan 					
					 Nation/World 
					 Roadwork &amp; Traffic 
					 Weather 
					 Obituaries 
					 Death Notices 
				 
			
			
			
 Sports 
				 
					 Front 
					 Scores 
					 Lions/NFL 
					 Red Wings/NHL 
					 Tigers/MLB 
					 Pistons/NBA 
					 Shock/WNBA 
					 U-M 
					 MSU 
					 Other Colleges 
					 Preps 
					 Outdoors 
					 Olympics 
					 Auto Racing 
					 Golf 
				 
			
			
			
 Entertainment 
				 
					 Front 
					 Arts 
					 Movies 
					 DVD 
					 Music 
					 Names &amp; Faces 
					 Dining Out 
					 Nightlife 
					 Casinos 
					 TV &amp; Radio 
					 Video Games 
				 
			
			
			
 Business 
				 
					 Front 
					 Michigan 
					 Nation/World 
					 My Market Watch 
					 Air Travel 
					 Technology 
					 Real Estate 
				 
				

			
 Autos 
				 
					 Front 
					 Auto Reviews 
					 Auto Shows 
					
				 
			
			
			
 Features 
				 
					 Front 
					 Lifestyles 
					 Travel 
					 Books 
					 Fashion 
					 Food 
					 Gardening 
					 Health 
					 Comics 
					 Yak's Corner 
					 Pets 
					 Twist 
				 
			
			
			
 More 
				 
					 Print Edition 
					 Subscribe 
					 Contact Us 
					 Send a News Tip 
					 Freep Bookstore 
					 Order a Photo 
					 Jobs In Journalism 
					 Text Me Freep 
					 Mobile 
					 Submit a Photo 
					 Photo Galleries 
					 Videos 
				 
			
			
			
 Opinion 
				 
					 Front 
					 Editorials 
					 Letters 
					 Editorial Cartoons 
					 Bloggers 
					 Columnists 
					 Forums 
				 
			

			
			
 Classifieds 
				 
					 Front 
					 Find a Job 
					 Find a Car 
					 Find a Home 
					 Find an Apartment 
					 Contests 
					 Shopping 
					 Dating 
					 Pets 
					 Place an Ad 
					 How to Advertise 
				 
			
		
		
		
	
	 
	
	 
	
		
		 
		
			 
				 Partners:  Jobs: CareerBuilder.com
 
				 • 
				 Cars: Cars.com
 
				 • 
				 Apartments: Apartments.com
 
				 • 
				 Shopping: ShopLocal.com
 
			 
			 
				 Customer Service 
				 Terms of Service 
				 Send feedback 
				 Subscribe Now 
				 Jobs In Journalism 
			 
			
			 Copyright ©2007 the Detroit Free Press. All rights reserved. Users of this site agree to the Terms of Service and 
			Privacy Policy/Your California Privacy Rights (Terms updated March 2007) 
			
			 
				 USA Today 
				 USA Weekend 
				 Gannett Co. Inc. 
				 Gannett Foundation 
			 
		
		 
		
	
	
	
		
	
	 


 
























