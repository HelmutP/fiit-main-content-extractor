

 August 16, 2007 -- Excerpted from "Radicalization in the West: The Homegrown Threat," a report prepared by senior NYPD intelligence analysts Mitchell D. Silber and Arvin Bhatt for Police Commissioner Ray Kelly.
 
  RADICALIZATION 
 
  
 
  Ideology: Jihadist or jihadi-Salafi ideology is the driver that motivates young men and women, born or living in the West, to carry out "autonomous jihad" via acts of terrorism against their host countries. It guides movements, identifies the issues, drives recruitment and is the basis for action. 
 
  * This ideology has served as the inspiration for numerous homegrown groups including the Madrid March 2004 bombers, Amsterdam's Hofstad Group, London's July 2005 bombers, the Australians arrested as part of Operation Pendennis in late 2005 and the Toronto 18 arrested in June 2006. 
 
  Process: An assessment of the various reported models of radicalization leads to the conclusion that the radicalization process is composed of four distinct phases: 
 
  * Stage 1: Pre-Radicalization 
 
  * Stage 2: Self-Identification 
 
  * Stage 3: Indoctrination 
 
  * Stage 4: Jihadization 
 
  Each of these phases is unique and has specific signatures. All individuals who begin this process do not necessarily pass through all the stages; many stop or abandon this process at different points. 
 
  Individuals who do pass through this entire process are quite likely to be involved in the planning or implementation of a terrorist act 
 
  
 
  PHASES OF RADICALIZATION 
 
  
 
  Pre-radicalization: [This] is the point of origin for individuals before they begin this progression. It is their life situation before they were exposed to and adopted jihadi-Salafi Islam as their own ideology. 
 
  * The majority of the individuals involved in these plots began as "unremarkable" - they had "ordinary" jobs, had lived "ordinary" lives and had little, if any criminal history. 
 
  Self-identification: Individuals, influenced by both internal and external factors, begin to explore Salafi Islam, gradually gravitate away from their old identity and begin to associate themselves with like-minded individuals and adopt this ideology as their own. The catalyst for this "religious seeking" is a cognitive opening, or crisis, which shakes one's certitude in previously held beliefs and opens an individual to be receptive to new worldviews. 
 
  There can be many types of triggers that can serve as the catalyst including: economic (losing a job, blocked mobility), social (alienation, discrimination, racism - real or perceived), political (international conflicts involving Muslims), personal (death in the close family) . . . 
 
  Indoctrination: An individual progressively intensifies his beliefs, wholly adopts jihadi-Salafi ideology and concludes, without question, that the conditions and circumstances exist where action is required to support and further the cause. That action is militant jihad. This phase is typically facilitated and driven by a "spiritual sanctioner." 
 
  * While the initial self-identification process may be an individual act, as noted above, association with like-minded people is an important factor as the process deepens. By the indoctrination phase this self-selecting group becomes increasingly important as radical views are encouraged and reinforced. 
 
  Jihadization: Members of the cluster accept their individual duty to participate in jihad and self-designate themselves as holy warriors or mujahedeen. Ultimately, the group will begin operational planning for the jihad or a terrorist attack. These "acts in furtherance" will include planning, preparation and execution. 
 
  * While the other phases of radicalization may take place gradually, over two to three years, this jihadization component can be a very rapid process, taking only a few months, or even weeks to run its course. 

