

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;


		 
		
		
		Newcastle 2-1 West Brom: Joey Barton penalty helps Joe Kinnear to first win - mirror.co.uk
	
			
			
			
			
			
			
			
		
		
		

		
		

		

	    
		

		
		
	
	
	

			
		
			
		
			
		
			
	
	

	

		
	
		
		 


		

		
	 
	







 
	
		 
 
 
  FIND LOVE - Mirror Dating 
  CLICKETY-CLICK - Bingo 
  STAY HEALTHY - MirrorDiets 
  POOLS - £120k jackpot 
  WASTE TIME - Play Games 
  PLAY - Mirror Lotto 
 
 

 


	
	
	
	
	
	
	
	
	

	
		
	
	

		 
			 
				 
				  
					
						London
						
						Min:1°C
						Max:10°C
					
				 
				 
					
						
					
				 
				 
					
						
						
					
				 
	
		
		
	
		

			 
			 
				
				
					 
				
					
						
					
					mirror.co.uk
				
					 
				
			 
	 
	 
	
				
			
			
			
				
				
				
			
		 
		 
			 
				 
					
						 
							 Search 
							  
							  
							 
RSSSite MapTags
 
						 
					
				  
	
	 
		 
			 

	

			 
				Home
 
		
			 
				News
 
		
			 
				Sport
 
		
			 
				Celebs
 
		
			 
				TV &amp; Entertainment
 
		
			 
				Life &amp; Style
 
		
			 
				Advice
 
		
			 
				Opinion
 
		
			 
				Fun &amp; Games
 
		
			 
				Video
 
		
			 
				Mobile
 
		
			 
		  
		
		 

			
			 		

		
		
		 
	 Sunday Mirror 
 Tell us your story 

	 Mirror Photos archive 
 Mirror.co.uk Lite 
 

			  

			
			 
				 
		
					 Top Stories 
				
					 Latest News 
				
					 Columnists 
				
					 News Forums 
				
					 Weird World 
				
					 Weather 
				
					 City 
				
					 Technology 
				
					 Pictures 
				
				 
			  
		
			
			 
				 
		
					 Latest 
				
					 Football 
				
					 Olympics 
				
					 Rugby 
				
					 Pictures 
				
					 Forums 
				
					 Racing 
				
					 Cricket 
				
					 Motorsport 
				
					 Columnists 
				
					 More 
				
					 Pools 
				
					 Fantasy Football 
				
				 
			  
		
			
			 
				 
		
					 3am 
				
					 Celebs News 
				
					 Latest Celebs 
				
					 Columnists 
				
					 Celebs On Sunday 
				
					 Celebs Forums 
				
					 Pictures 
				
				 
			  
		
			
			 
				 
		
					 X Factor 
				
					 X Factor blog 
				
					 Strictly Come Dancing 
				
					 Strictly blog 
				
					 TV 
				
					 Columnists 
				
					 Film 
				
					 DVD 
				
					 Music 
				
					 Video Games 
				
					 Books 
				
				 
			  
		
			
			 
				 
		
					 Horoscopes 
				
					 Sex &amp; Health 
				
					 Fashion &amp; Beauty 
				
					 Real Life 
				
					 Mirror Shopping 
				
					 Dating 
				
					 Dieting 
				
					 Kids &amp; Family 
				
					 Science &amp; Environment 
				
				 
			  
		
			
			 
				 
		
					 Dr Miriam 
				
					 Sex Doctor 
				
					 Money 
				
					 Holidays 
				
					 Homes 
				
					 Motoring 
				
					 Gardening 
				
					 Mirror Investigates 
				
					 More Advice 
				
				 
			  
		
			
			 
				 
		
					 Blogs 
				
					 Forums 
				
					 X Factor 
				
					 Strictly Come Dancing 
				
					 Football Spy 
				
					 3pm 
				
					 Dr Miriam 
				
					 Science &amp; Environment 
				
					 Mirror Investigations 
				
				 
			  
		
			
			 
				 
		
					 Bingo 
				
					 Lotto 
				
					 Free Games 
				
					 Crosswords 
				
					 Sudoku 
				
					 Cartoons 
				
					 Competitions 
				
					 Fantasy Football 
				
					 Casino 
				
				 
			  
		
			
			 
				 
		
				 
			  
		
			
			 
				 
		
					 Features 
				
					 FAQs 
				
				 
			  
		
		  
	 
	
	

			 

		 

		 
 
	 
			
			 
		 
	
				 
		
				
						Home
				
			
				
						Sport
				
			
				
						Football
				
			
				 
		 
	
		 
 
	 
	
			
				
				
				
			
		 
 
	 Newcastle 2-1 West Brom: Joey Barton penalty helps Joe Kinnear to first win 
	
	
	 
	  
	  
	 
	
	 
	
	By Mirror.co.uk
	
	
	28/10/2008
	
	
	 
	
	 
 	 
	
	 
	 Joey Barton marked his first start since being released from jail with a goal as Newcastle finally ended their wait for a second Barclays Premier League win of the season.  
	 Barton converted a ninth-minute penalty and the Magpies looked to have secured Joe Kinnear's first victory as interim boss when Obafemi Martins headed home a second three minutes before the break.  
	 However, substitute Ishmael Miller pulled one back for the visitors with 25 minutes remaining, and the home side had to scrap all the way to the whistle for the points.  
	 Barton was handed his first start for Newcastle since leaving prison as Joe Kinnear looked for his first Barclays Premier League victory at the third attempt.  
	 Barton, who last appeared at St James' Park on May 5, was included in place of the injured Nicky Butt in one of three changes to the side which lost 2-1 at Sunderland on Saturday. There were returns too for Argentinian winger Jonas Gutierrez and Spanish full-back Jose Enrique, with Geremi and Sebastien Bassong making way.  
	 West Brom boss Tony Mowbray made just one change to the side beaten 3-0 at home by Hull as midfielder Chris Brunt replaced striker Ishmael Miller.  
	 The Magpies were led out by keeper Shay Given who was asked to wear the armband in the absence of the injured Butt and regular skipper Michael Owen.  Kinnear's men went close with just seconds gone when Obafemi Martins, Danny Guthrie and Jonas combined to allow Damien Duff to play Shola Ameobi in, although his shot was hacked away by Paul Robinson.  
	 The Magpies threatened again with barely two minutes gone when Ameobi flicked on Given's clearance to Martins, whose shot looped up off Ryan Donk and forced Scott Carson to make a smart save.  
	 West Brom managed to re-group after a torrid opening, but they fell behind with nine minutes gone after Barton slid the ball into Ameobi's feet and Donk felled him inside the box.  
	 Referee Mike Dean pointed straight to the spot and Barton grabbed the ball to send Carson the wrong way and score his first goal at St James' and just his second for the club.  
	 Borja Valero scuffed an 11th-minute effort wide as the Baggies tried to respond, but Barton smashed a long-range drive past the post four minutes later after Duff had seen his effort blocked following a mazy run.  
	 The visitors were passing the ball confidently with Newcastle repeatedly conceding possession, but they lacked the cutting edge to trouble the Magpies in the final third.  
	 Although Mowbray's side were enjoying more than their fair share of the ball, the Magpies continued to look more dangerous when they were going forward, with Jonas and Martins in particular causing problems.  
	 Baggies striker Roman Bednar got in on the left with 26 minutes gone, but his cross was cut out by Steven Taylor.  
	 Former Middlesbrough midfielder James Morrison picked up possession 30 yards from goal seconds later as the home side were once again forced back, but his ambitious effort flew high over.  
	 Newcastle threatened repeatedly down the right with Jonas and full-back Habib Beye linking well, and it was the defender who sent in a cross towards Ameobi in the 29th minute, although the striker could not keep his header down.  
	 However, the visitors would have been back on level terms 10 minutes before the break had Given not been at his best.  
	 
	 Advertisement - article continues below » 
	 
	

















	











	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	


	 
	 
	 Robert Koren caught the home defence square when he turned Jonathan Greening's pass into Morrison's path, but the Irishman won the one-to-one battle to preserve his side's lead.  
	 They might have taken full advantage within two minutes when Jonas and Ameobi carved open the visitors' defence, but the chance fell to Beye, who skied his shot.  
	 But the second goal finally arrived three minutes before half-time when Beye went past Paul Robinson and crossed for Martins to power home a header off defender Jonas Olsson. 
	 The home side returned knowing that a third goal would kill off the visitors, and set about the task of finding it from the off.  They might have created an opening with just a minute of the half gone when Martins, who had been played in down the left by Duff, rounded full-back Gianni Zuiverloon but saw his cross cut out by Donk with Danny Guthrie arriving at pace.  
	 But the visitors had by no means given up and Given had to watch a long-range effort from Brunt fly past his right post after Robinson had played his team-mate into space on the edge of the box.  
	 Martins' pace was a constant thorn in West Brom's side, but when he raced away from Olsson to cross with 51 minutes gone Donk calmly chested the ball back to Carson.  
	 Mowbray made his first change three minutes later when he withdrew Brunt and sent on Miller in an effort to give his side more of a goal threat.  
	 However, the newcomer unwittingly spared Newcastle within two minutes when Morrison's goal-bound shot hit him and ran to safety.  
	 Morrison lifted another effort on the turn high over the bar as the Baggies continued to press with the Magpies looking uncomfortable at the back.  
	 The former Boro winger turned provider on the hour when he fed Miller, who cut inside before sending a right-footed effort wide of the post.  
	 But the striker made no mistake five minutes later when the home defence was caught square once again.  
	 Koren's perfectly-weighted pass completed wrong-footed Fabricio Coloccini and Miller easily rounded Given before sliding the ball into the empty net.  
	 Kinnear moved to shore up his midfield when he replaced the tiring Jonas with Geremi with 20 minutes remaining.  
	 Miller might have levelled three minutes later when Morrison's free-kick rebounded to him off the defensive wall, but Given made another important block.  
	 Martins made way for Spaniard Xisco as Kinnear attempted to introduce some freshness, but Koren blazed an 81st-minute shot high over with West Brom making a final push.  
	 Duff had a chance to ease the nerves with two minutes remaining after Guthrie and Ameobi combined to set him up, but his left-foot shot was blocked by Valero.   
	    
	 
	
	 

	
	
	 
	
	 
	
	 

	 
	Print
 

	 
	Send
 

	 
	
	
	Bookmark &amp; Share
	
 

	
	 
	 
	
	 
	

	
	 
	
	 
	
	 

	 Fark 

	 Delicious 

	 
	
	Facebook
 

	 Digg 

	
	 
	 
	
	 

		 
  
	 
	More Top Football Stories
	 
	 
	 
	 
	 
Barnsley 0-0 Bristol City: Moore is merrier
	 
	 
Birmingham 3-1 Sheffield Wednesday: The Blues brothers
	 
	 
Blackpool 2-2 Crystal Palace: Evatt so sorry for that goal, dad
	 
	 
Charlton 1-1 Burnley: Toddy is just the tonic
	 
	 
Coventry 1-1 Derby
	 
	 
Norwich 2-1 Doncaster
	 
	 
	 
	
	 
	  

 
 
	
			
				
				
				
			
		 

	
	
	
	 
		 Sponsored Links 
			 
				
				
		 
	 
		 
 
	 
	
		 
 
	
	 
		
		 

			 


	
			
			
				Print
			
			
			
		
			
		
	

 

			 


	
			
			
			
			
				Send
			
		
			
		
	

 

			 


	
			
			
			
			
		
			
			
			
			Bookmark &amp; Share
			
			
		
	

 

			 Fark 

			 Delicious 

			 
	
	Facebook
 

			 Digg 

			
		 
	 
	
 
	 
	 Related Galleries 
 
	 


	
	
	 
	 
 
	 


	
	
	 
	 
		  See more galleries  
		
	 
	
	 
	 Related Stories 
						
				                  			
					 	
			
						 
							Premier League preview: Arsenal v Tottenham
										
						 
				
						 
							Premier League preview: Aston Villa v Blackburn
										
						 
				
						 
							Premier League preview: Newcastle v West Brom
										
						 
				
						 
							Opinion: The moronic minority at Sunderland v Newcastle show why we're all treated like kids
										
						 
				
						 
							The moronic minority at the Tyne-Wear derby show why we're all treated like kids
										
						 
				
						 
							Fan hatred will make Joey Barton a better player, says Newcastle boss Joe Kinnear
										
						 
							
					 		
				 
			
	 
	
	 
	 Related Tags 
						
				                  			
					 	
			
						 
							joey barton, 
										
						 
				
						 
							newcastle, 
										
						 
				
						 
							west brom
										
						 
							
					 		
				 
			
		  (What's this)  
		
	 
	
	
	
	 
	
			
				
				
				
			
		 
 
	
			
				
				
				
			
		 

		 
 
		
			Who will win Wednesday's north London derby?
		
		 

 

		 
 
 
	 
	
			
				
				
				
			
		 
 
	
			
				
				
				
			
		 
 
	
			
				
				
				
			
		 

		  
	 
	Most Popular
	 
	 
	 
	 
	 
	

	
	

	Jonathan Ross faces backlash on 'joke' phone messages to Fawlty Towers' Andrew...
	 
	
	
	Jonathan Ross faced a storm of criticism last night over his crude phone...
	 
	 
	

	
	

	X Factor favourite Diana Vickers talks of growing rivalry in the house
	 
	
	
	X Factor finalist Diana Vickers has revealed that tensions are getting high in...
	 
	 
	

	
	

	Angelina Jolie applauds British movie talent at Hollywood Film Festival Awards...
	 
	
	
	Angelina Jolie flew in for her humanitian work in Afghanistan to make a...
	 
	 
	

	
	

	Leon Jackson video exclusive: X Factor winner give George Sampson dance lessons
	 
	
	
	X Factor winner Leon Jackson has been making a documentary about his new life...
	 
	 
	

	
	

	Opinion: The moronic minority at Sunderland v Newcastle show why we're all...
	 
	
	
	Want to know why you can't drink alcohol while watching football? Look no...
	 
	 
	

	
	

	Juande Ramos wants £7.5million compensation from Tottenham Hotspur
	 
	
	
	Spurs are left facing a total compensation bill well in excess of £15m...
	 
	 
	

	
	

	High School Musical couple Zac Efron and Vanessa Hudgens in romantic Hawaii...
	 
	
	
	No wonder Vanessa Hudgens looks so pleased with herself…
	 
	 
	

	
	

	3,000 more Post Offices face axe over Royal Mail government contract
	 
	
	
	Gordon Brown has been warned at least 3,000 more post offices may shut if...
	 
	 
	

	
	

	Gordon Brown to spend millions in bid to prevent job meltdown
	 
	
	
	Gordon Brown yesterday defended his multi-billion pound plan to spend his way...
	 
	 
	

	
	

	Fern Britton exclusive: This Morning host stung by ""You have arms like a...
	 
	
	
	It was a tactless, throwaway remark – but one that was to have a long...
	 
	 
	   
	 
	 
	  

		 
	Premier League Table
	
	
	Team
	P
	GD
	PTS
	
	
	
	
	Liverpool
	9
	8
	23
	
	
	Chelsea
	9
	15
	20
	
	
	Hull
	9
	3
	20
	
	
	Arsenal
	9
	12
	19
	
	
	Aston Villa
	9
	6
	17
	
	
	Man Utd
	8
	8
	15
	
	
	Portsmouth
	9
	-4
	14
	
	
	Man City
	9
	9
	13
	
	
	Sunderland
	9
	-1
	12
	
	
	West Ham 
	9
	-2
	12
	
	
	Blackburn Rovers
	9
	-6
	12
	
	
	Middlesbrough
	9
	-7
	10
	
	
	West Brom
	9
	-7
	10
	
	
	Everton
	9
	-6
	9
	
	
	Wigan
	9
	-2
	8
	
	
	Fulham
	8
	-2
	8
	
	
	Bolton Wanderers
	9
	-4
	8
	
	
	Stoke
	9
	-8
	7
	
	
	Newcastle United
	9
	-7
	6
	
	
	Tottenham
	9
	-5
	5
	
		
	 
 

  
  
Arsenal
 
  
Aston Villa
 
  Blackburn Rovers 
  
Bolton Wanderers  
  
Chelsea
 
  
Everton
 
  
Hull City
 
  
Fulham
 
  
Liverpool
 
  
Manchester City
 
  
  
 
Manchester United
 
  
Middlesbrough
 
  
Newcastle United
 
  
Portsmouth
 
  
Stoke City
 
  
Sunderland
 
  
Tottenham Hotspur
 
  
West Brom
 
  
West Ham United
 
  
Wigan Athletic
 
  
 

			
			 
				
				 Recommended Searches 
					 
						
						   
					 
			 
		 		
			  
		
			 
 
	 
	 
	
			
				
				
				
			
		 
		 
 
			
			 
		Site Map
	
						
						
							 
						
							
							 
								 
					
						Advice
					
						 
					
						 
					
						Dr Miriam
					
						 
					
						 
					
						Gardening
					
						 
					
						 
					
						Holidays
					
						 
					
						 
					
						Homes
					
						 
					
						 
					
						Mirror Investigates
					
						 
					
						 
					
						Money
					
						 
					
						 
					
						More Advice
					
						 
					
						 
					
						Motoring
					
						 
					
						 
					
						Sex Doctor
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Celebs
					
						 
					
						 
					
						3am
					
						 
					
						 
					
						Celebs Forums
					
						 
					
						 
					
						Celebs News
					
						 
					
						 
					
						Celebs On Sunday
					
						 
					
						 
					
						Columnists
					
						 
					
						 
					
						Latest Celebs
					
						 
					
						 
					
						Pictures
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Fun &amp; Games
					
						 
					
						 
					
						Bingo
					
						 
					
						 
					
						Cartoons
					
						 
					
						 
					
						Casino
					
						 
					
						 
					
						Competitions
					
						 
					
						 
					
						Crosswords
					
						 
					
						 
					
						Fantasy Football
					
						 
					
						 
					
						Free Games
					
						 
					
						 
					
						Lotto
					
						 
					
						 
					
						Sudoku
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Life &amp; Style
					
						 
					
						 
					
						Dating
					
						 
					
						 
					
						Dieting
					
						 
					
						 
					
						Fashion &amp; Beauty
					
						 
					
						 
					
						Horoscopes
					
						 
					
						 
					
						Kids &amp; Family
					
						 
					
						 
					
						Mirror Shopping
					
						 
					
						 
					
						Real Life
					
						 
					
						 
					
						Science &amp; Environment
					
						 
					
						 
					
						Sex &amp; Health
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Mobile
					
						 
					
						 
					
						FAQs
					
						 
					
						 
					
						Features
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						News
					
						 
					
						 
					
						City
					
						 
					
						 
					
						Columnists
					
						 
					
						 
					
						Latest News
					
						 
					
						 
					
						News Forums
					
						 
					
						 
					
						Pictures
					
						 
					
						 
					
						Technology
					
						 
					
						 
					
						Top Stories
					
						 
					
						 
					
						Weather
					
						 
					
						 
					
						Weird World
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Opinion
					
						 
					
						 
					
						3pm
					
						 
					
						 
					
						Blogs
					
						 
					
						 
					
						Dr Miriam
					
						 
					
						 
					
						Football Spy
					
						 
					
						 
					
						Forums
					
						 
					
						 
					
						Mirror Investigations
					
						 
					
						 
					
						Science &amp; Environment
					
						 
					
						 
					
						Strictly Come Dancing
					
						 
					
						 
					
						X Factor
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Sport
					
						 
					
						 
					
						Columnists
					
						 
					
						 
					
						Cricket
					
						 
					
						 
					
						Fantasy Football
					
						 
					
						 
					
						Football
					
						 
					
						 
					
						Forums
					
						 
					
						 
					
						Latest
					
						 
					
						 
					
						More
					
						 
					
						 
					
						Motorsport
					
						 
					
						 
					
						Olympics
					
						 
					
						 
					
						Pictures
					
						 
					
						 
					
						Pools
					
						 
					
						 
					
						Racing
					
						 
					
						 
					
						Rugby
					
						 
					
						
							 
							
								 
								 
							
						
						
							
							 
								 
					
						TV &amp; Entertainment
					
						 
					
						 
					
						Books
					
						 
					
						 
					
						Columnists
					
						 
					
						 
					
						DVD
					
						 
					
						 
					
						Film
					
						 
					
						 
					
						Music
					
						 
					
						 
					
						Strictly blog
					
						 
					
						 
					
						Strictly Come Dancing
					
						 
					
						 
					
						TV
					
						 
					
						 
					
						Video Games
					
						 
					
						 
					
						X Factor
					
						 
					
						 
					
						X Factor blog
					
						 
					
						
							 
							
						
						
							
							 
								 
					
						Video
					
						 
					
					 
				 		
			  
		
			 

		 
 
 
 About Us 
 Contact Us 
 For the record 
 Privacy Statement 
 Terms and Conditions 
 
 
 Listen to  
 the Mirror 

 
 

 
		 
		 
		 
				 
			
		


