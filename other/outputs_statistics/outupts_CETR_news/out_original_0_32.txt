

 //W3C//DTD HTML 4.0 Transitional//EN"" ""http://www.w3.org/TR/REC-html40/loose.dtd""&gt;

 
BBC NEWS | Entertainment | The Doors celebrate Grammy honour






    
    












 


BBC NEWS / ENTERTAINMENT
 
Graphics Version | Change to UK Version | BBC Sport Home

 


    


	
	
	
	
		
			
		      	
		       	News Front Page
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Africa
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Americas
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Asia-Pacific
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Europe
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Middle East
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	South Asia
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	UK
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Business
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Health
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Science/Nature
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Technology
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Entertainment
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Video and Audio
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	
		
			
		      	
		       	Have Your Say
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	



    


 

Sunday, 11 February 2007, 11:33 GMT

 The Doors celebrate Grammy honour 




	

	
		
	


Iconic rock group The Doors and folk singer Joan Baez have received Grammy awards for lifetime achievement.
 
They were among the acts celebrated by the US Recording Academy in recognition of careers that changed music. 
 
 
Also honoured were psychedelic rockers The Grateful Dead, soul band Booker T and The MGs, jazz legend Ornette Coleman and opera singer Maria Callas.
 
 
The lifetime awards were presented on the eve of the main Grammy ceremony, where Mary J Blige leads nominations.

 
 
The Doors' guitarist Robby Krieger said the band's late singer, Jim Morrison, would have been ""very honoured"" to be recognised.
 
 
""People think he was anti-establishment, but in reality he wanted to be bigger than the Beatles,"" he said.
 
 
Tributes
 
 
Surviving members of The Grateful Dead, whose lead singer Jerry Garcia died in 1995, also paid tribute to their former bandmate.
 
 



    
        
            
             
 
            
            
                    


	"President Bush is the best publicity agent I've ever had"



            
                    


	 Joan Baez 

            
             
            
                    
            
            
        
    

""I wish the rest of my brothers in the band could be here,"" said Bill Kreutzmann, one of the group's two drummers.
 
Booker T. Jones, whose band epitomized the Memphis soul sound, thanked his family ""for keeping me alive all these years.
 
 
""It's been a difficult thing to do,"" he said.
 
 
He and other musicians from his group made reference to band-member Al Jackson, who died from a gunshot in 1975.
 
 
Saxophonist Ornette Coleman, one of the main innovators in the 1950s free jazz movement, also used his speech to muse about the meaning of life and death.
 
 
""How do we kill death since it kills everything?"" he asked. ""You don't have to die to kill and you don't have to kill to die.""
 
 
Bush tribute
 
 
Protest singer Baez, who was a key figure in the anti-Vietnam war movement. closed the ceremony by paying tribute to US President George W Bush.
 
 
""President Bush is the best publicity agent I've ever had,"" she said.
 
 
 ""People always ask me to compare then versus now. It's very much like a re-run."" 
 
 
Other prizes handed out at the ceremony included the Trustees award, presented to Stax Records co-founder Estelle Axton, theatrical composer Stephen Sondheim, and New Orleans-based engineer Cosimo Matassa.
 
 
The award recognises outstanding contributions to the recording industry in a non-performing capacity.
 
 
Recording engineer David M. Smith and the Yamaha Corporation, which manufactures a variety of recording equipment and musical instruments, each won a Technical Grammy award.
 
 
 
 
 
  

 
 
 


    
        
            
                E-mail this to a friend
            
        
    

 


	
                
                        
                                Related to this story:
                        
                
                 
		
			
				
					
	
		
			
			
			
			Grammy Awards: Main nominations
			
		
		
	

					
						
							
								(09 Feb 07 | 
								Entertainment
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Mary J Blige leads Grammys list
			
		
		
	

					
						
							
								(07 Dec 06 | 
								Entertainment
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Police 'set to reveal world tour'
			
		
		
	

					
						
							
								(08 Feb 07 | 
								Entertainment
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Shakira to play at Grammy Awards
			
		
		
	

					
						
							
								(08 Feb 07 | 
								Entertainment
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			Police to reunite for Grammy gig
			
		
		
	

					
						
							
								(30 Jan 07 | 
								Entertainment
								)
							
						
					
					 
				
			
				
					
	
		
			
			
			
			U2 steal Grammys glory from Carey
			
		
		
	

					
						
							
								(09 Feb 06 | 
								Entertainment
								)
							
						
					
					 
				
			
		
		
	

	

 




    
     
        RELATED INTERNET LINKS 
        
            Grammy Awards 
                    	
        The BBC is not responsible for the content of external internet sites
        
    

 
 




SEARCH BBC NEWS: 



 

    


	
	
	
	
		
			
		      	
		       	News Front Page
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Africa
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Americas
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Asia-Pacific
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Europe
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Middle East
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	South Asia
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	UK
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Business
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Health
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Science/Nature
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Technology
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Entertainment
			
			 | 
		
		
	
	
	




	
	
	
	
		
			
		      	
		       	Video and Audio
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	
		
			
		      	
		       	Have Your Say
			
			 | 
		
		
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	




	
	
	
	



    

 
 
NewsWatch | Notes | Contact us | About BBC News | Profiles | History
 
^ Back to top | BBC Sport Home | BBC Homepage | Contact us | Help | ?




 

