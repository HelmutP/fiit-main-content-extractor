

 BAGHDAD (Reuters) - Officials of the U.S.-led coalition on Sunday showed what they said were examples of Iranian weapons used to kill 170 of their soldiers and implicated high-level Iranian involvement in training Iraqi militants. 

    


 A senior defense official from the U.S.-led Multinational Force in Baghdad told a briefing that 170 coalition troops had been killed by Iranian-made roadside bombs known as explosively formed penetrators (EFPs) that he said were smuggled into Iraq. 

    


 Officials showed reporters fragments of what they said were Iranian-manufactured weapons, including one part of an EFP -- which is strong enough to penetrate the armor of an Abrams tank -- and parts of 81 mm and 60 mm mortar bombs. 

    


 The United States accuses Iran of fanning violence in Iraq by giving sophisticated bomb-making technology, money and training to militant Shi'ite groups, some of whom have links with Iraq's Shi'ite-led government. Iran denies the allegation. 

    


 The three defense officials spoke on condition they not be identified. 

    


 Washington has hardened its rhetoric over Iran's alleged role in the war in Iraq and tension has been growing between the two arch-foes over Tehran's nuclear plans. 

    


 ""The weapons had characteristics unique to being manufactured in Iran...Iran is the only country in the region that produces these weapons,"" the senior defense official said.   

