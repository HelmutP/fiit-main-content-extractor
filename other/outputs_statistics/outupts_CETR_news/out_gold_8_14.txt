

 HAVANA -  Without saying a word, teenager Yamile Cruz seats herself at the grand piano, places her slender fingers on the keyboard and, after a brief pause, unleashes a torrent of sound one might expect from a virtuoso twice her age and size. 
 
 Though the battered, perpetually out-of-tune instrument -- made ages ago in Moscow -- sounds as if its strings might pop at any moment, though the din of traffic outside rushes in through open windows on a sweltering afternoon, the 16-year-old has tuned everything out to turn in a masterful performance of Saint-Saens' Piano Concerto No. 2. 
 
  Similar scenes are unfolding in practically every chaotic corner of the sprawling Amadeo Roldan Conservatory, a fabled institution in Central Havana that has trained some of Cuba's most celebrated musicians. Guitarists, drummers, flutists, percussionists, pianists, you name it, they're making music well beyond their years in the classrooms and hallways and sun-drenched courtyard of a three-story walk-up that looks as if it hasn't had a new coat of paint since Fidel Castro took power, at the end of 1959. 
  

                     
                        
                        
							
                            
                                

                                

                                
                                     
                                        
                                        	 Related links 
										
                                         
 
                                                        
                                                        
									   					    
	                                                        


    
    The Beat of Cuba





Photos










                                                        

	                                                 
												    
	                                            
                                        
                                            
	                                                 
                                                        
                                                        
									   					    
	                                                        


    'I may never get to the heart of jazz'
    
















                                                        

	                                                 
 
                                     
                                

                            
                        

                        
                        




    
        
            
        
    

                        

                        
                         
                            
                                
                            
                         
                        
                     


                     
 Yet in this decaying building, and others like it scattered across this impoverished city, Cuba is producing musicians of Herculean technique, many of whom have applied their intensive classical training to the art of jazz -- and thus have come to tower over their counterparts around the world. Though the roots of Afro-Cuban music run deep in Havana, to the slave trade of centuries past, the last two generations have yielded larger-than-life jazz players whose mastery of their instruments and exalted level of musicianship enables them to conquer audiences wherever jazz is played. 
 
 Exactly why Cuban jazz musicians sound consistently brilliant may be a mystery to the outside world, but in Havana it is no secret: After Castro forged his alliance with the former Soviet Union in the early 1960s, the island quickly saw an influx of Russian and Soviet-bloc music teachers. They brought with them techniques that had been producing monumental classical virtuosos since the 19th Century. And though the fall of the USSR in the 1990s meant that financial support from Moscow virtually vanished, the Russian methods had become integral to Cuban music education and remain so to this day. 
 
 The merger of Cuban musical tradition and rigorous Soviet teaching has produced some of the greatest jazz players of the past 40 years. Yet for every Chucho Valdes and Gonzalo Rubalcaba, who have broken through the United States' long-running embargo of Cuba to win acclaim in the U.S. and beyond, uncounted others feel doomed to a lifetime of obscurity. Unable to bring their gifts to the international marketplace because they have great difficulty getting into the States, where most of the world's musical stars are merchandised, the Cuban giants languish well outside the spotlight. 
 
 These musicians -- some old and tired of battling against the effects of the embargo, others young, poor and frustrated by their inability to make contact with U.S. listeners -- are creating a music as complex and profound as anything available in jazz today. But they were not featured in the ""Buena Vista Social Club"" film, which in 1999 popularized a small group of aged Cuban musicians, and few listeners outside Cuba get to hear today's exceptional players. 
 
 With political tensions between Washington and Havana on the rise during the past couple of years, and with the U.S. granting entry visas only sparingly to Cuban musicians, the Cuban jazz artists realize that their prospects are getting worse. 
 
 ""If Cuban musicians would have a chance to enter the great distribution of the United States, we could show everyone that some of the most important music in the world is being made here, in Havana,"" says the esteemed Cuban bandleader Jorge Gomez. Gomez has toiled for 30 years in the Cuban music industry but was allowed into the United States to perform just once, in 1986. 
 
 ""Without us, the world is losing one of the most important roots of music."" 
 
 As well as some of the most formidable jazz artists working today. 
 
Coming of age 
 
 Ernan Lopez-Nussa, a 44-year-old jazz pianist revered in Havana but virtually unknown in the U.S. (despite a brief tour in 1999), can see himself in the faces of the gifted youngsters making music at the Amadeo Roldan Conservatory. 
 
 It was in this building, near the intersection of two dusty, noisy roads in Central Havana, that Lopez-Nussa spent more than a decade studying the harmonic intricacies of Bartok, the technical idiosyncrasies of Liszt and the keyboard poetry of Chopin. Though Lopez-Nussa, like the students today, spent mornings studying math, science and literature, in the afternoons he pored over musical scores, practiced ear-training exercises and polished his technique at the piano. 
 
 He didn't realize it at the time, but Lopez-Nussa was coming of age at an extraordinary moment in Cuban culture, the mid-1960s and '70s, when the ancient traditions of folkloric Cuban music were being galvanized by the techniques of Soviet musicians who had been sent to the island by the Kremlin. Suddenly, students who attended the Amadeo Roldan Conservatory were being taught not only the noble works of Cuban composers such as Roldan and Ernesto Lecouna but also drilled in the ferociously difficult keyboard exercises of Anton Rubinstein, Nikolai Rubinstein and other giant Russian pedagogues. To Lopez-Nussa, this Cuban-Soviet training seemed the most natural thing in the world, but it was unprecedented in Cuban history and laid the groundwork for a musical revolution yet to come. 
 
 ""I cannot say that we students loved being here -- we certainly never loved this building,"" says Lopez-Nussa, who's greeted with hugs and kisses from students and teachers alike as he makes an impromptu visit to the conservatory, which trains pre-university students. 
 
 ""Even now, the conditions are terrible, the pianos are terrible, the noise from outside is terrible, the practice rooms are filled with distractions. 
 
 ""To tell you the truth, I do not understand how the kids here learn to make music in such a difficult place. 
 
 ""Yet somehow we forget about everything else and concentrate on the music."" 
 
 In truth, the children have no choice, their teachers placing the same demands upon them as had been made on such earlier Russian supervirtuosos as Vladimir Horowitz, Sviatoslav Richter, Emil Gilels, David Oistrakh, Mstislav Rostropovich and scores more. 

