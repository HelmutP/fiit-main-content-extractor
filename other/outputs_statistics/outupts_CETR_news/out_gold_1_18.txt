

 By NICHOLAS J. COTSONIKA 
				
 FREE PRESS SPORTS WRITER 

				 


The clock is ticking on running back Kevin Jones and his recovery from a severe foot injury. Will the Lions return him to practice or keep him on the physically unable to perform list? 
 


""We're going to have to make a decision soon here,"" coach Rod Marinelli said. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


""It's going to be with doctors. We've got to use their medical knowledge.""  Jones is evaluated medically every Monday. He hopes to return for the regular-season opener Sept. 9 at Oakland. Martz has said he is optimistic Jones will be back based on the way Jones has been working out.  But the injury is tricky.  ""He's working hard and cutting, but it's not like cutting in pads,"" Marinelli said. ""That type of injury's just not clean. ... We've got to be right.""  If Jones is going to return for the opener -- or early in the regular season -- he needs to start practicing soon so he can see can get into game shape and see how the foot reacts.  If the Lions keep him on the PUP list, he must miss the first six weeks of the season. After that, there is a three-week window in which the Lions can return him to practice. Once they return him to practice, they have three weeks to put him on the active roster or injured reserve.
