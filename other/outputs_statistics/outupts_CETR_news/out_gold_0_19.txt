

Scientists say they have mapped the most important genes that put people at risk of type 2 diabetes, offering hope that a test could be delivered.
 
The findings could explain up to 70% of the genetics of the disorder which affects over 1.9 million UK people. 
 
 
Family history is a major risk factor for the condition, along with obesity.
 
 
The Imperial College London team, working with Canadian colleagues, found four points on the gene map linked to a person's diabetes risk, Nature reports.

 
 
Gene screen
 
 
One of the genetic mutations they identified, after scanning nearly 400,000 mutations, could potentially explain the cause behind type 2 diabetes. 
 
 
The mutation was in a particular zinc transporter, known as SLC30A8, which is involved in regulating insulin secretion. 
 
 
It may be possible to treat some diabetes by mending this transporter, the researchers say. 
 
 

    
    
	 

	
            
            
                
		
                
                     
                     
	 
		
		We can create a good genetic test to predict people's risk of developing this type of diabetes
		 	 




 
                
                     
                     
	 Researcher Professor Philippe Froguel 


 
                
            
        
	
	
    
    




 
They compared the genetic make-up of 700 people with type 2 diabetes and a family history of the condition, with 700 diabetes-free people. 
 
 
Once they found the four culprit regions they confirmed their findings by checking the genetic make-up of 5,000 more people with type 2 diabetes and a family history of the condition. 
 
 
Future hope
 
 
Researcher Professor Philippe Froguel, from Imperial College London, said: ""Our new findings mean that we can create a good genetic test to predict people's risk of developing this type of diabetes.
 
 
""If we can tell someone that their genetics mean they are predisposed towards type 2 diabetes, they will be much more motivated to change things such as their diet to reduce their chances of developing the disorder. 
 
 
""We can also use what we know about the specific genetic mutations associated with type 2 diabetes to develop better treatments.""
 
 
The researchers now plan to study the genes they identified more closely. 
 
 
Dr Iain Frame of Diabetes UK said: ""We have known for some time that family history plays a part in whether or not someone might develop type 2 diabetes.
 
 
""Whilst it is still early days, the results of this research look promising as they could potentially help the early identification of people with a genetic risk. 
 
 
""In the longer term they could perhaps lead to better treatments for people with the condition."" 

