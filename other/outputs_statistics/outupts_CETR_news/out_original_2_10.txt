

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

   
  	










	
	
Colombia Wants the World to Recognize Its Passion - WSJ.com
  
     
 



 

				 
				 
					 
						
					 
					 
						
					 
					 

						
					 
					 
						
					 
					 
						More
						 

							 
								BigCharts
							 
							 
								Virtual Stock Exchange
							 
							 
								FiLife.com
							 
							 
								WSJ Asia
							 
							 
								WSJ Europe
							 
							 
								WSJ Portuguese
							 
							 
								WSJ Spanish
							 
							 
								WSJ Chinese
							 
							 
								Financial News Online
							 
							 
								Far Eastern Economic Review
							 
						 
					 
				 

				 
					 
					
						
							
							
								 
								   
							
						   
						 

						 
						   SEARCH 
						 

								 
								 
							
							
						
					
					 
			 
	 



 
    
     
      
         
         
      

      
        Welcome,  Logout
        
        
          
          My Account 

          My Online Journal 
          Help 
          Message Center ( new) 
          
        

     


     

     


     
    
    

      
         
            Today's Paper
         
         Video 
         Columns 
         Blogs 

         Graphics 
         Newsletters &amp; Alerts 
         New!&amp;nbspJournal Community 
      
      
      

    
      Subscriber Log In

     

       Subscriber Log In 
      

        
             
            User Name
            
			 

             
            Password
            
			 
            
			 
			Log in
            
			 
             
              

              Remember me
              Forgot your password?

              

             

        
      

     
   

      

    
 
 
      
      Home   U.S.   World   Business   Markets   Tech   Personal Finance   Life &amp; Style   Opinion   Careers   Real Estate   Small Business   
      
 
 
 
 QUICK LINKS :  
 
 
Wall Street Crisis
 
 
Campaign 2008
 
 
Heard on the Street
 
 
Market Data
 
 
Management
 
 
Health
 
 
Tech Report
 
 
Crunchonomics
 
 
 
 Asia   Europe   Earnings   Economy   Health   Law   Management   Media &amp; Marketing   

         
                 
         
           
              
              
              More Industries
                                       
        
           
             
                 
                       
                                     
                
                
             
             
                Accounting 
 Advertising 
 Airlines 
 Autos 
 Banking 
 Chemicals 
 Computer Hardware 
 Computer Software 
 Consumer Products 
 Defense &amp; Aerospace 
 Energy 
 Broadcasting &amp; Entertainment 
 Financial Services &amp; Insurance 
 Food &amp; Tobacco 
 Hospitality 
 Industrial Goods &amp; Services 
 Internet 
 Marketing &amp; Strategy 
 Media Agencies 
 Metals &amp; Mining 
 Paper &amp; Forest Products 
 Pharmaceutical &amp; Biotech 
 Real Estate 
 Retail 
 Semiconductors 
 Telecom 
 Transportation 
 Utilities 
 
                            
		     	         
        Columns &amp; Blogs     
 

 
 
 
 
 
 
 
Dow Jones Reprints: This copy is for your personal, non-commercial use only. To order presentation-ready copies for distribution to your colleagues, clients or customers, use the Order Reprints tool at the bottom of any article or visit
www.djreprints.com
 

See a sample reprint in PDF format.
Order a reprint of this article now

 
 



 
 
 
 
 
 ADVERTISING 
 OCTOBER 27, 2008 
 









 Colombia Wants the World to Recognize Its Passion
 
 Latin American Nation Trots Out a New Slogan, Seeking to Rehabilitate Its Brand After Years of Violence and Corruption 
 By MATT MOFFETT
 
 
 
 
 
    Article 
 Comments 
 

 more in Media &amp; Marketing » 
 
 
 
 
 


     
 
 
 In 1996, when a Colombian government official first approached marketing consultant David Lightle about a campaign to improve the nation's image, Mr. Lightle looked around Bogota and told him the truth: ""Don't waste your money,"" he recalls saying. 
 The country, he says, ""was a mess."" Colombia was convulsed by violence from leftist guerrillas, and its president, Ernesto Samper, had been stripped of his U.S. visa amid evidence that drug traffickers had financed his election campaign. 
 When Colombia called Mr. Lightle again in 2004, the view from Bogota had changed for the better. Colombia's tough-minded President Alvaro Uribe had developed a close alliance with Washington, and had unleased a military offensive that had the guerrillas reeling. 
   

Colombia es Pasión A 'Colombia is Passion' keychain, one of hundreds of products bearing the campaign's heart-shaped logo. 
   
 Working with Colombia's export, tourism and investment promotion agency, Proexport, Mr. Lightle devised a campaign called Colombia es Pasión, or ""Colombia is Passion."" Now, the public-private partnership behind the campaign operates two stores in Bogota that offer hundreds of products featuring the campaign's heart-shaped logo. 
 Nearly 250 companies have licensed the logo, which also appears inside boxes of Colombia's exported roses and on the tail of one of the planes flown by national carrier Avianca. There's even a ""Colombia is Passion"" bicycling team, which boasts the world champion among cyclists younger than 23 years old. 
 Eager to highlight their advantages, and play down their faults, Colombia and a growing number of countries are using branding strategies to set themselves apart in the global marketplace. But even as country branding campaigns proliferate, some critics say that such efforts often amount to mere sloganeering and question how much they actually enhance a country's reputation. 
 Simon Anholt, a British author and consultant, is credited with coining the phrase ""nation brand"" in 1996. He says it summed up a simple observation that ""it's the responsibility of good governments to be, in effect, brand managers."" 
 Now, he says, advertising and marketing entrepreneurs have so distorted the concept, that he sometimes is sorry he brought it up. ""It's a lame dog following me the past 15 years, and I've spent that time trying to shoot the damn thing,"" says Mr. Anholt, who edits the quarterly journal Place Branding and Public Diplomacy. 
 Rebranding requires sweeping societal transformations, he says, not just clever public relations. He says South Africa rebranded when it ended apartheid; Ireland when it became a prosperous nation, rather than a mass producer of immigrants; Slovenia when it embraced democracy, joined the European Union and showed that a historically unstable part of Eastern European could be different. 
 If a country does undertake fundamental changes, marketing can complement them, Mr. Anholt acknowledges. After the death of longtime dictator Francisco Franco in 1975, Spain went from an economically hobbled backwater to a thriving democracy. Spain helped sell the change to tourists with a colorful logo and the slogan ""Everything Under the Sun."" 
 But campaigns based strictly on marketing can fall flat. A decade ago, a push by the British government of Tony Blair to adopt the catchphrase ""Cool Britannia"" flopped because the slogan was too narrow, says Thomas Cromwell, president of East West Communications in Washington. ""Cool Britannia,"" first applied to a British resurgence in pop music and fashion, didn't characterize British companies and was ill-suited to attacting older tourists, he says. 
 María Claudia Lacouture, Colombia's general manager for country image, says her country is ripe for rebranding because the popular perception of Colombia has lagged far behind the improving reality. Kidnappings and assassinations, once frequent occurrences, are now comparatively rare. President Uribe's strategy to take the war to the Marxist guerrillas has culminated this year in the death of three of the insurgency's top leaders. 
 Ms. Lacouture asks why people don't talk more about Colombia's dazzling variety of birds and orchids, the classic colonial architecture of Cartagena, or cultural titans like novelist Gabriel García Marquéz and painter and sculptor Fernando Botero. 
 ""Colombia is Passion"" is designed to help change the conversation about Colombia, she says. 
 Colombia is working with a limited budget, having spent just $5 million on the campaign since its inception. Even so, its logo won an American Graphic Design Award in 2006, and the ""Colombia is Passion"" Web page averages 77,000 monthly visits. 
 Tourism and foreign investment are rising, though Ms. Lacouture doesn't claim that the campaign is responsible for all of the progress. 
 Kokoriko, a big Colombian chain of chicken restaurants has rolled out a ""Colombia is Passion"" meal, given away CDs tied to the campaign and adopted ""Passion Made Into Flavor"" as its own slogan. ""Our customers love it,"" says Guillermo Beltran, Kokoriko's marketing director. 
 Not everybody is sold on ""Colombia is Passion."" A well-designed campaign ""should be rooted in [qualities] that distinguish Colombia,"" says East West's Mr. Cromwell. ""You could just as easily say 'Bolivia is passion' or 'Venezuela is passion.' "" 
 He says Colombia should have adopted something closer in spirit to the Juan Valdez coffee campaign, which features a sombrero-clad Colombian farmer who has become an international icon. 
 Write to Matt Moffett at matthew.moffett@wsj.com 

     
     
 
	

	 
		 Copyright 2008 Dow Jones &amp; Company, Inc. All Rights Reserved 
		 This copy is for your personal, non-commercial use only. Distribution and use of this material are governed by our Subscriber Agreement and by copyright law. For non-personal use or to order multiple copies, please contact Dow Jones Reprints at 1-800-843-0008 or visit 

		 www.djreprints.com 
	 
	
 
 
 More In Media &amp; Marketing  
      
       
         Email 
         Printer Friendly 
         Order Reprints 
         

 	
 

 
         
           

            Share:
           
           
             
               
                 
                   Yahoo! Buzz 
                 

               

             
            
             
               
                 
                   MySpace 
                 
               

             

             
               
                 
                   Digg 
                 
               

             
            

           
         
           
       
     

 


 
 
 


 
 
	 
	 
	 
		 Sponsored by 




	 
 
 

    
 
 
  	
	  Back To   
 
 
 
  	
	 Back To 
	  
	  MSN Money Homepage  
	  MSN Money Investing  
 

 
 
 




 
 
	 
  	   Email Newsletters and Alerts 
 The latest news and analysis delivered to your in-box. Check the boxes below to sign up. 
		
           
				WSJ.com Email Features
	
 
					

This Week's Most Popular

				 
 
					

On the Editorial Page

				 


            
                More Information on WSJ Features
                 
                    Also send me information about more WSJ Features
                    
                 
            
	            
            
 		  		 
					The email address  is already associated with another account. Please enter a different email address:
				 
		    

		   
				Newsletter Signup
				 
					Enter Your Email
					
				 
				 
					Sign Up
	
					SIGN UP
				 
			
		

	 
	  	
	 
	   Thank you !  You will receive  in your inbox 
	 	
	 
  	    Terms and Conditions 
	    Go to Email Center 
	 
   
  
     
      null 
     
  	   Email Newsletters and Alerts 
 The latest news and analysis delivered to your in-box. Check the boxes below to sign up. 
		
			
				WSJ.com Email Features
 
					

                      
                        Media &amp; Marketing Edition


				 
 
					

                      
                        News Alert


				 
 
					

In Today's Paper

				 

			
				Submit
	            
	             
				  	 
						The email address null is already associated with another account. Please enter a different email address:
				  	 
					 
						Enter Your Email
											
					 
				 

				 
					Sign Up
					SIGN UP
				 
			
				
		 
			 
New! To sign up for Keyword or Symbol Alerts click here.
 
	
			 To view or change all of your email settings, visit the Email Setup Center. 
		 
     
    
     
       Thank you ! You will receive  in your inbox. 

       
	     Manage Email preferences 
	   
     
  
 


 





 
 

 


 
 
 
Video
 
 
 

previous

 
 
 
 
 

next

 
 
 
 

 

 
Media Web Minute: Bono as Pundit
 0:43 
 

 

 
Smiley on Obama
 6:06 
 

 

 
Media Web Minute: 'Barbarians"" still relevant today
 0:44 
 
 
 
  More in Media &amp; Marketing 
 
 				
	 Ad Networks Vacate Web Space
 
 
	 				
	 Newspapers' Circulation Drops 4.6%
 
 
	 
	 
 
 Most Popular 
 
    Read 
 Emailed 
 Video 
 Commented 
 

 
 
 				
	1. Opinion : Arthur B. Laffer: The Age of Prosperity Is Over
 
 
	 				
	2. Opinion : Robert Carroll: Almost Everyone Would Do Better Under the McCain Health Plan
 
 
	 				
	3. Obama vs. McCain: It's Your Money
 
 
	 				
	4. Opinion : The Europeanization of America
 
 
	 				
	5. Financial Storm Hits Gulf
 
 
	 
	 
 
 
 				
	1. Opinion : Arthur B. Laffer: The Age of Prosperity Is Over
 
 
	 				
	2. Opinion : Robert Carroll: Almost Everyone Would Do Better Under the McCain Health Plan
 
 
	 				
	3. Obama vs. McCain: It's Your Money
 
 
	 				
	4. Opinion : The Europeanization of America
 
 
	 				
	5. Opinion : Fred Smith: Washington Is the Problem
 
 
	 
	 
  
 
1. Hatin' Palin 
 
 
2. Cluttered Cubicles Go Lean With 5S Rules 
 
 
3. The Next Real Estate Boom 
 
 
4. Palin's Star 
 
 
5. A Liberal Supermajority 
 
  
 
 
 
1.
 
How Detroit Drove Into a Ditch80 comments
 
 
 
2.
 
Obama vs. McCain: It's About Your Money75 comments
 
 
 
3.
 
Wealth Gap Is Focus Even as It Shrinks41 comments
 
 
 
4.
 
Even the Oracle Didn't Time It Perfectly36 comments
 
 
 
5.
 
Bankruptcy Fears Rise   As Chrysler, GM Seek Federal Aid30 comments
 
 
 
 
 
   
     Most Read Articles Feed 
     Most Emailed Feed 
     Most Popular Video Feed 
     Most Commented Feed 
   

   
 
 
 
 




    
 
 
 
 
 
 
 
 
 
	 
		 Email 
		 Printer Friendly 
		 
			Share:

			 

				 
					 
						Yahoo Buzz
						
					 
					
					 MySpace 

					 Digg 
					
				 
			 
		 
		 
			
			Text Size

			
		 
		   
	 
 

      
       
         Email 
         Printer Friendly 
         Order Reprints 
         

 	
 

 
         
           

            Share:
           
           
             
               
                 
                   Yahoo! Buzz 
                 

               

             
            
             
               
                 
                   MySpace 
                 
               

             

             
               
                 
                   Digg 
                 
               

             
            

           
         
           
       
     

 
 
 
 




 
 
	 
  	   Email Newsletters and Alerts 
 The latest news and analysis delivered to your in-box. Check the boxes below to sign up. 
		
           
				WSJ.com Email Features
	
 
					

This Week's Most Popular

				 
 
					

On the Editorial Page

				 


            
                More Information on WSJ Features
                 
                    Also send me information about more WSJ Features
                    
                 
            
	            
            
 		  		 
					The email address  is already associated with another account. Please enter a different email address:
				 
		    

		   
				Newsletter Signup
				 
					Enter Your Email
					
				 
				 
					Sign Up
	
					SIGN UP
				 
			
		

	 
	  	
	 
	   Thank you !  You will receive  in your inbox 
	 	
	 
  	    Terms and Conditions 
	    Go to Email Center 
	 
   
  
     
      null 
     
  	   Email Newsletters and Alerts 
 The latest news and analysis delivered to your in-box. Check the boxes below to sign up. 
		
			
				WSJ.com Email Features
 
					

                      
                        Media &amp; Marketing Edition


				 
 
					

                      
                        News Alert


				 
 
					

In Today's Paper

				 

			
				Submit
	            
	             
				  	 
						The email address null is already associated with another account. Please enter a different email address:
				  	 
					 
						Enter Your Email
											
					 
				 

				 
					Sign Up
					SIGN UP
				 
			
				
		 
			 
New! To sign up for Keyword or Symbol Alerts click here.
 
	
			 To view or change all of your email settings, visit the Email Setup Center. 
		 
     
    
     
       Thank you ! You will receive  in your inbox. 

       
	     Manage Email preferences 
	   
     
  
 
 
   
     Ask a Question 
     
       
        Journal Community
       
     
   
   
     Ask a question about anything you choose, and let readers from the Journal Community answer it. 
	 Please fill out all required fields. 
     Please enter a question. 
	 The Question description should not be more than 2000 characters. 
	 Please choose a category 
	 Please choose a SubCategory 
     The language you used does not comply with community standards. Please re-enter. 

     
       
         
          
            Enter your question*
          
         
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
      
      
	   
         
           
            

              Choose a Category*
                            Companies
                            Education
                            Health &amp; Wellness
                            Industries
                            Investing

                            Life &amp; Style
                            News
                            Nonprofits &amp; Philanthropy
                            Personal Finance
                            Politics

                            Science &amp; Environment
                            Small Business
                            Technology
                            Workplace &amp; Career
                            Other

                          
          
         
         
          
            Sub-category
          
         

       
     
     
       
        
           
            
            Notify me when I receive answers

           
        
       
       
         
           
            Ask
           

         
       
     

     
       *Required 
     
   
 
 
 
 
Video
 
 
 

previous

 
 
 
 
 

next

 
 
 
 

 

 
Media Web Minute: Bono as Pundit
 0:43 
 

 

 
Smiley on Obama
 6:06 
 

 

 
Media Web Minute: 'Barbarians"" still relevant today
 0:44 
 
 
 
 




 
     
 
 
  

     
       
         Journal Community 
       
       
         
           close window 
         
       
     
     
       
         Hello 
 
       
       
         Your question to the Journal Community Your comments on articles will show your real name  and not a username.Why? 

         Why use your real name? The Journal Community encourages thoughtful dialogue and meaningful connections between real people. We require the use of your full name to authenticate your identity. The quality of conversations can deteriorate when real identities are not provided. 
		
		 
	         Please enter your first and last name 
	         
	           
	             
	              First name: 
	             
	
	           
	           
	             
	              Last name: 
	             
	           
	         		
		 
				
         
           
            
               
                
               

            
           
           
             Create a Journal Community profile to avoid this message in the future. (As a member you agree to use your real name when participating in the Journal Community) 
           
         

         
           
             Post 
             Cancel 
           
           
             Privacy Policy 

             Community Rules 
           
         
       
     
	
	 
       
         Notice:  
 
       
       
         Your participation access with Journal Community has been disabled due to violation of Journal Community Guidelines. 
		
		 If you feel you have reached this status change in error, please contact TBD@wsj.com 
        
       
     
		
    
 
 
     
        
          back to top
           

           
             
                   
                       Search News, Quotes, Companies

                    
                    Search News, Quotes, Companies
                    
                     
                    
                    Search
                    Search

                     
                   
             
           
           
           
         

     
     
           
Log In or Subscribe to access your WSJ.com Account 
     
       
         WSJ.com Account: 

         

           My Account 

           Billing Information 
         
       
       Help &amp; Information Center: 

       

           Help 
           Customer Service 
           Contact Us 
           New on WSJ.com 
           Tour the new Journal 

       
     

     
       About: 
       

           News Licensing 

           Advertising 

           Conferences 

           About Dow Jones 
           Privacy Policy - Updated 
           Subscriber Agreement &amp; Terms of Use - Updated 

           Copyright Policy 

           Jobs at WSJ.com 

       
     

     
       WSJ.com: 

       

           Site Map 
           Home 

           U.S. 
           World 
           Business 

           Markets 

           Market Data 

           Tech 
           Personal Finance 

           Life &amp; Style 

           Opinion 

           Autos 
           Careers 

           Real Estate 
           Small Business 
           Corrections 

       
     

     
       Tools &amp; Formats: 

       
           Today's Paper 

           Video Center 
           Graphics 
           Columns 
           Blogs 
           Alerts 
           Newsletters 

           Mobile 
           Podcasts 
           RSS Feeds 
           Journal Community 
           Message Center 
           Forums 

       

     

     

       
Digital Network
 
       
           WSJ.com 

           Marketwatch.com 
           Barrons.com 

           AllThingsD.com 

           FiLife.com 
           BigCharts.com 

           Virtual Stock Exchange 
           WSJ Asia 

           WSJ Europe 
            Foreign language editions:  
           WSJ Chinese 
           WSJ Portuguese 

           WSJ Spanish 
       

     
    

Copyright ©2008 Dow Jones &amp; Company, Inc. All Rights Reserved
 

 
 
 










