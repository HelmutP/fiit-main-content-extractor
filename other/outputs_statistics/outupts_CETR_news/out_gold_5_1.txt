

 MUNICH, Germany (Reuters) - Defense Secretary Robert Gates on Sunday dismissed an attack on U.S. foreign policy by Russian President Vladimir Putin as the blunt talk of an old spy and said it was vital to keep working with Moscow. 

    


 In a speech which one U.S. senator said smacked of Cold War rhetoric, Putin told a security conference in Munich on Saturday the United States was making the world a more dangerous place by pursuing policies aimed at making it the ""one single master"". 

    


 A White House spokesman said it was ""surprised and disappointed"" by the comments and some Europeans said it was a wake-up call from a tougher Russia, newly empowered by a sharp rise in the prices of its oil, gas and metals exports. 

    


 But despite their concerns, the White House and Gates underlined the need for cooperation with Moscow. 

    


 ""Many of you have backgrounds in diplomacy or politics,"" Gates, a former CIA director, told the same Munich conference. 

    


 ""I have, like your second speaker yesterday (Putin), a starkly different background -- a career in the spy business. And, I guess, old spies have a habit of blunt speaking. 

    


 Gates raised concerns about Russian arms transfers and its ""temptation to use energy resources for political coercion"" which he said could threaten international stability.   

