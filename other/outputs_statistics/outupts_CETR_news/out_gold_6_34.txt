

 ASPEN, Colo. -- A one-day lift ticket to Aspen during peak ski season will cost you $87 at the window, up $5 from last season. 
 



That walk-up price tops the $85 Vail charged last season for single-day tickets and gives Aspen the title of priciest lift ticket -- at least for now. 
 



Visitors who plan ahead and buy multi-day tickets can save. 
 



For instance, Aspen Skiing Co. said it would offer seven-day passes allowing holders unrestricted access to Snowmass, Aspen Mountain, Aspen Highlands and Buttermilk throughout the season for $259 for adults or $199 for students. It must be purchased in person in Colorado at specific spots to be announced this fall. 

