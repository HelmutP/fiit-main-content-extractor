

 "Four held for robbing supermarket
         
 28 October 2008, 19:06 
         
           
         
         
	  Four men were arrested on Tuesday on armed robbery charges in Temba near Hammanskraal, the Gauteng department of community safety said. 
 
Spokespoerson Mandla Radebe said the men were arrested on charges of robbing a supermarket in Morokolong, Temba on October 21. 
 
The men were arrested in various houses and sections in Temba. 
 
A Striker shotgun and a .38 Special revolver were recovered. Thirty eight rounds of rifle ammunition and six 9mm pistols rounds were found during the arrests. 
 
The four men would appear before the Temba magistrate's court on Wednesday. 
 
Gauteng Traffic Police and the Temba SAPS Trio Task Team carried out the joint operation. - Sapa 

