

 BY RICK TELANDER Sun-Times Columnist
			
					
						
						
As ESPN prepares an ''Outside the Lines'' segment on the premise that the 10-2 Bears might be one of the most underappreciated and overly criticized teams in recent years, one thought comes to mind: 
 
 What if coach Lovie Smith is right? 
 
 Smith appears to be the fly in the ointment right now, the leak in the rowboat, the turnip in the cornfield, the babbler in church. 
 
 Trust me, he says, ''Rex is our quarterback.'' 
 
 A Christmas T-shirt made up with that slogan would be perfect for Smith to wear indefinitely, perhaps with holly flourishes and little elves on the sleeves, because it would be cheery and answer every question that comes his way. 
 
 How a coach can avoid at least wavering when his starting quarterback throws a 1.3 rating his way is a mystery. 
 
 But it is true that because Smith clings to Grossman as this team's starter, no matter how low the kid sinks, the recent NFC North championship was barely noticed by anybody in town. 
 
 It's all negative. 
 
 Yes, the Bears won the division with Grossman at quarterback. But it seems a trained -- nay, an untrained -- monkey could hang on to this defensive beast and ride it to victories. 
 
 Yes, Grossman looked magnificent in early games this season. But he suddenly has run out of confidence, developing skills, luck. 
 
 And when that happens in the NFL, coaches always look to the sideline for a replacement -- at least until the magic returns. If it does. 
 
 Grossman short and slow But here are two problems Grossman presents -- and always has: He is not tall, and he is not mobile. 
 His strong arm can't work when defenders the size of, say, the Miami Dolphins' Jason Taylor have their hands high in the air. 
 
 And Rex has almost zero escapability. Incredible as it sounds, Grossman's longest run this season is two yards. He has 19 carries for minus-17 yards total. 
 
 Yeah, a lot are kneel-downs. But sometimes a quarterback has to use his feet for yardage. 
 
 Sudden increments of rushing gain, even if small, can have a huge impact on game management. 
 
 Remember New England Patriots quarterback Tom Brady getting those critical 11 yards against the Bears on Nov. 26 -- and juking Brian Urlacher in the process? 
 
 Cleveland Browns backup quarterback Derek Anderson scampered 33 yards to set up the Browns' winning field goal in overtime Sunday against the Kansas City Chiefs. 
 
 And battered, old Jeff Garcia's runs were crucial to the Philadelphia Eagles' upset Monday of the Carolina Panthers. 
 
 A quarterback who always is standing where defenders expect him to be, who doesn't even test the open field, is an easy quarterback to attack and a hard one to protect. 
 
 We're not talking about cloning Bobby Douglass, who ran for 968 yards in 14 games in 1972. But I'm betting even Brian Griese might look to run once in a while. 
 
 Michael Vick might have rushed for 200 yards Sunday if he had been inserted for Grossman and the Vikings defended him the same way. 
 
 There is nothing new about Grossman's immobility. It's just that now he needs something fresh in his arsenal, and he has nothing. 
 
 Perhaps even more disturbing is that full-blown winter nears, and the Bears, if they keep home-field advantage, will play four more games at home en route to the Super Bowl. 
 
 Grossman said his cold-weather gloves, the frigid ball and swirling, icy winds didn't affect his play Sunday. 
 
 That's noble. But I disagree. 
 
 I think Grossman does have trouble on frozen tundra -- as the Vikings' Brad Johnson did -- and he's screwed without being able to run. 
 
 Smith's legacy on the line Grossman returns Monday to St. Louis, where he broke his ankle during the 2005 preseason. He has blown past the injury jinx this season, and the controlled climate at the Edward Jones Dome might do wonders for his game. 
 But assuming a Super Bowl run, five of the Bears' next six games will be played outdoors, many of them in bad weather. 
 
 It is almost admirable what Smith is doing, showing a loyalty that holds through disaster, demise and demeaning. But it also might be nuts. 
 
 It might show that Smith is a terrific defensive coordinator and a solid football guy but lacks the flexibility to make the difficult and critical decisions that a championship coach must make. 
 
 You see gamblers in Las Vegas double-down again and again to end their losing streaks. Then they're broke. 
 
 Smith can't believe that Grossman isn't the person he used to be, not the blessed quarterback to lead the way. 
 
 Smith thinks if he brings in Griese, he will have lost his essence, his insight, his focus, his team. 
 
 In a very real sense, Smith is betting his future as a coach on that belief, for this is the year for the Bears and this decision will be his legacy. 
 
 No coach should make decisions based on popular sentiment. Follow fans' desires, and chaos is your co- pilot. 
 
 But this quarterback thing has Chicago on mental lockdown. 
 
 And the only balm is a long shot: What if Smith is right? 

