

 "






//W3C//DTD XHTML 1.0 Transitional//EN""
	""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;


 
	The top adviser at Obama's side - International Herald Tribune
	
		
	
	
		
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	

	
	
			
	
	
	
	
	
	
	
	
	
	
	
	
	

		
		
			
	
	
	
		
	
	
	
	
	
	
	
	



	



 
	
	
	 
 

 target=""_blank""&gt; width=""768"" height=""90"" border=""0"" alt="""" /&gt;
 
 
	
	

	
	 
		  
		  
		 Americas 
		 
			
			
			
		 
	 	
	
	
	
	
		

 
	  
	 
		 
			 iht.com 
 Business 
 Culture 
 Sports 
 Opinion 
		 
	 
	 
		 
			 AMERICAS 
 EUROPE 
 ASIA/PACIFIC 
 AFRICA/MIDDLE EAST 
   
 TECH/MEDIA 
 STYLE 
 HEALTH 
		 
	 
	 
		 
			 TRAVEL 
 PROPERTIES 
 BLOGS 
 DISCUSSIONS 
 SPECIAL REPORTS 
 AUDIONEWS 
		 
	 
	  
	 Morning home delivery - save up to 72% 
	 
		 
		
			
			 
				 
				SEARCH
				 
			 	
			 
			Advanced Search
			 
		
	 
 

	
	

	
	 
		
		 
		
			

			
			
			
			
			
			

			
			
			
			
			
			
				
			
				
				 
					
				            

 
	
	  Barack Obama, left, with David Axelrod, his chief strategist, whom he met 16 years ago and who said of the nominee, ""He is always at his best when we're at our worst.""  (Ozier Muhammad/The New York Times) 	 
 


				            
					
					
					 

					
					The top adviser at Obama's side 
					
					
											
			            
						
			            
						
			            
						 
							  
							 
By Jeff Zeleny  
							 Published: October 27, 2008 
							  
						 
			            
						
										
											
						
			                

 
	  
	 
		
		   E-Mail Article 
		  
		
		
		 
			 
				
   Listen to Article

			 
			  
		 
		
		
		   Printer-Friendly 
		  
		
		
		   3-Column Format 
		  
		
		
		   Translate 
		  
		
		
		   Share Article 
		  
		
		
		 
			 
  
 
			 
  Text Size 
		 

		  
		 
			

			
		 
	 
	  
 



							
							
			                 CHICAGO: The senator calls before bedtime.  
 The cellphone in David Axelrod's shirt pocket comes to life, sometimes before midnight, sometimes after. If the ring tone is ""Signed, Sealed, Delivered I'm Yours"" by Stevie Wonder, he steps away from the dinner table or the barstool. It almost certainly means that Senator Barack Obama is on the line, ready for another turn of a rolling conversation the two men have been having almost every day for what has been a remarkable two years.  
 ""When the phone rings at 11, I have a pretty good sense of who it's going to be,"" Axelrod said. ""He does a lot of thinking and working late at night.""  
 As Obama, the Democratic presidential nominee, heads into the final days of his race for the White House, an ever-widening sphere of aides surrounds him. But almost none is as responsible for his current station as Axelrod, whose title of chief strategist only hints at the extensive role he has played in the senator's evolution: friend, adviser and confidant, always at the elbow of this candidate.  
 In many ways, Axelrod is a classic example of the Washington political consultant (even though he lives in Chicago and says he has no intention of moving to the capital if Obama wins). He has been making advertisements and offering advice for candidates for mayor, senator and president for a generation, since quitting his job as a newspaper reporter in 1984. He has a particular specialty in helping black candidates appeal to white electorates.  



 
	
	 
		 More Coverage 
		  
		
		 
			  
			 Elections 2008 
		 
		  
	 
		
	
 
     Today in Americas 
      
     
	 Experts in economic malfunction 
 
  
 
	 McCain says Alaska senator should resign 
 
  
 
	 New to campaigning, but Michelle Obama is no longer a novice 
 
  

 

	
	
	 
		

	 
	
 


 But Axelrod, in this client-consultant relationship, appears to be something different, with a personal investment in Obama's success that is obvious in the distress marked on his face whenever the candidate comes under attack.  
 Every politician has a guardian angel, and every presidential hopeful has a right-hand dispenser of wisdom. Yet in the trio of top strategists around Obama, including Robert Gibbs, a senior communications adviser, and David Plouffe, the campaign manager, it is Axelrod who has been at Obama's side the longest and has the most interwoven relationship with him.  
 ""Although he is as tough as they come, he's actually not a mercenary,"" Obama said in an interview. ""He actually believes in what we're doing, which actually makes him a bad consultant when he doesn't believe in the candidate. And he's a great consultant when he believes.""  
 Oh, Axelrod believes. But since the moment he met Obama in 1992? That would be revising history.  
 Their relationship began when Bettylu Saltzman, a Democratic activist in Chicago, suggested that the two men should become acquainted. She was a volunteer in Illinois for Bill Clinton's first presidential campaign, and Obama was leading a voter-registration drive.  
 Impressed, Saltzman predicted to her friends that Obama would be the first black president. So she thought he should know the city's best-known political strategist. (Told about the story recently, Axelrod conceded remembering few specifics about their meeting and said, ""We went out to lunch, maybe?"")  
 ""I certainly put the bug in his ear,"" said Saltzman, proudly recalling those days 16 years ago when she urged Axelrod to take time to meet the young lawyer. ""They are sort of a yin-and-yang personality. David can be so much more volatile than Barack.""  
 She added, ""He's calmed David down a lot.""  
 It took several years after that first meeting for Axelrod to agree to take Obama on as a client. But since then they have become linked in a way that is more than just professional.  
 ""He is always at his best when we're at our worst; that's sustained us through some difficult times,"" Axelrod said. ""This is unusual for a national campaign, but everybody likes each other.""  
 When campaigns struggle, candidates often substitute their top players by showing them the door or by layering them with other strategists. But neither is the case with Obama. The team that surrounded him two years ago as he made his decision to run for president is the same group with him at the finish line.  
 If Clinton had James Carville and President George W. Bush had Karl Rove, it could well be said that Obama has Axelrod. For advice on how to navigate the politics of race. For suggestions on how to calibrate his message. And for comedic relief.  
 As Obama was practicing his acceptance speech at the Democratic convention, at the very hour that a Denver stadium was filling up with supporters, a knock sounded on the door of his hotel suite. He was at an emotional high point of the address, the moment he declared, ""Enough!"" But he was near the door, so he answered it.  
  1  |  2  Next Page
 

							
							
			            	
							
								
								
								
								
						
							
						 
						
										
					
						 
							 
								
								 
							 
						 	
					
					
				    
				      
				    											
					
					 
						  
						 Back to top 
						 
Home  &gt;  Americas
 
					 
			

			
			 
			
			
			
				 
		 IHT.com Home » 
		 Latest News 
		  
		 
			 
				  
				 Regis Duvignau/Reuters 
			 
			 
				 France weighs big changes in drinking laws 
				 Winemakers complain that the government, egged on by killjoys, wants to make it harder to sell and drink wine. 
			 
			 
In Opinion: David Brooks: The behavioral revolution
 
		 
		 
			  
			 
				More Headlines 
				 
					 Both candidates court Pennsylvania 
					 Aid disrupted in eastern Congo as rebels advance and populace flees 
					 Damascus closes U.S. school and culture center after border raid 
					 Experts in economic malfunction 
				 
			 
			  
		 
	 
						
			
			
		
		 
		
	    
		
		 
			
						
			
			 
				  
				 
					 Video 
					 See all videos » 
				 
				 
					  
					  
					
					 
					
						 	
						
						 
							  
							 
								 The Week Ahead 
								 The IHT's managing editor, Alison Smale, discusses the week in world news. 
							 
						 
						

						
						 
							  
							 
								 The Week Ahead  
								 The IHT's managing editor, Alison Smale, discusses the week in world news. 
							 
						 
						

						
						 
							  
							 
								 Global View: Final presidential debate 
								 The IHT's managing editor discusses European reactions to the third McCain-Obama faceoff. 
							 
						 
						

						
						 
							  
							 
								 The Week Ahead - Oct. 13, 2008 
								 The IHT's managing editor, Alison Smale, discusses the week in world news. 
							 
						 
						

						
						 
							  
							 
								 Global View: Second presidential debate 
								 The IHT's managing editor discusses European reactions to the second McCain-Obama faceoff.  
							 
						 
						

						
						 
							  
							 
								 Road to November: Colorado 
								 Both campaigns have sunk significant resources into getting electoral votes in the state. 
							 
						 
						

						
						 
							  
							 
								 Global View: Vice-presidential debate 
								 The IHT's managing editor, Alison Smale, discusses European reactions to the Biden-Palin faceoff. 
							 
						 
						

						
						 
							  
							 
								 Voters' voices 
								 San Francisco is the first stop on a trip to peel back conventional notions of the American electorate.  
							 
						 
						

						
						 
							  
							 
								 The McCain Campaign style going forward 
								 With just a month to go, how will the campaign deploy John McCain and Sarah Palin accross the states it is tar... 
							 
						 
						

						
						 
							  
							 
								 Global View: First presidential debate 
								 The IHT's managing editor, Alison Smale, discusses international reactions to the first Obama-McCain debate. 
							 
						 
						
						 							
					 					
				 
				  
			 
			
	        
	        
				
									 
						
						 

 target=""_blank""&gt; border=""0"" alt="""" /&gt;
 
						
					 
									
				
			
				
	       
			
			 
	 
  
  
 
	
	 
		 Most E-Mailed 
		 

		 
			  
			 24 Hours 
			 | 
			 7 Days 
			 | 
			 30 Days 
		 

 
	
 
	 1. 
	 The mysterious cough, caught on film 
 
 


 
	 2. 
	 Rising yen worries Japan 
 
 


 
	 3. 
	 DA: criminal charges possible in boy's Uzi death 
 
 


 
	 4. 
	 Court finds Niger guilty of allowing girl's slavery 
 
 


 
	 5. 
	 David Brooks: The behavioral revolution 
 
 


 
	 6. 
	 Volkswagen shares jump and short-sellers pounce 
 
 


 
	 7. 
	 Alaskans turning against Stevens after verdict 
 
 


 
	 8. 
	 Bet on a rebound and another fall 
 
 


 
	 9. 
	 Oil's stunning retreat: How long can it last? 
 
 


 
	 10. 
	 White House explores aid for merger between GM and Chrysler 
 
 


 

 
	
 
	 1. 
	 Endorsement: Barack Obama for U.S. president 
 
 


 
	 2. 
	 Partying helps power a Dutch nightclub 
 
 


 
	 3. 
	 Financial crisis has one beneficiary: The dollar 
 
 


 
	 4. 
	 Greenspan 'shocked' that free markets are flawed 
 
 


 
	 5. 
	 Woman arrested for 'killing' her virtual husband 
 
 


 
	 6. 
	 Want to be heard in India? You'd better form a militia 
 
 


 
	 7. 
	 Multitasking can make you lose ... um ... focus 
 
 


 
	 8. 
	 South Korea pushes to dissolve 'the old way' of business culture 
 
 


 
	 9. 
	 In Europe, crisis revives old memories 
 
 


 
	 10. 
	 A manga adventure through the world of wine 
 
 


 

 
	
 
	 1. 
	 Equestrian stripped of Beijing result for doping 
 
 


 
	 2. 
	 Indians complete trade with Brewers 
 
 


 
	 3. 
	 Moggi among 25 sent to trial for match-fixing 
 
 


 
	 4. 
	 A champion not honored in her own land 
 
 


 
	 5. 
	 Wells Fargo, not Citi, to buy Wachovia 
 
 


 
	 6. 
	 Fortis' Dutch operations nationalized 
 
 


 
	 7. 
	 6 seek to unseat scandal-plagued La. congressman 
 
 


 
	 8. 
	 Bulgarian corruption troubling the European Union 
 
 


 
	 9. 
	 Iceland is all but officially bankrupt 
 
 


 
	 10. 
	 Taking a hard look at a Greenspan legacy 
 
 


 

	 
	 
  
  
 
 
			
			
			
			

					
	 				    
		 
			  
			 
			
				iht.com/energy
							
		  	 
		   						
		 
			
			 
			
	   						
	 
					
		 Oil's stunning retreat: How long can it last? 
	
		
		 More from Energy: 
	
			
			 
				
					 Reducing carbon emissions no easy task for Europe 
				
				
				
					 How U.S. candidates differ on energy policy 
				
				
				
					 Russia's oil boom: Miracle or mirage? 
				
				
																

	     
		
		     
	 				
		

			
			
			
						
			
			
				
				
				
				
				
	        
		 
		
	 
	

	
	
		 
	
	 
		 
			
			 Contact iht.com 
			  
			
				  	
				 
Close this window or Send us another message
 			
			
		 
	 	
	
	 
		 
			  
			 
				
					
					 Search 
									
			 
		 
		
		 
			 
				 News: 
				 
					 
						 Americas 
						 | 
						 Europe 
						 | 
						 Asia - Pacific 
						 | 
						 Africa &amp; Middle East 
						 | 
						 Technology &amp; Media 
						 | 
						 Health &amp; Science 
						 | 
						 Sports 
					 
				 
			 
			
			 
				 Features: 
				 
					 
						 Culture 
						 | 
						 Fashion &amp; Style 
						 | 
						 Travel 
						 | 
						 At Home Abroad 
						
						 | 
						 Blogs 
                                                 | 
                                                 Reader Discussions 
						 | 
						 Weather 
					 
				 
			 
			
			 
				 Business: 
				 
					 
						 Business with Reuters 
						 | 
						 World Markets 
						 | 
						 Currencies 
						 | 
						 Commodities 
						 | 
						 Portfolios 
						 | 
						 Your Money 
						 | 
						 Funds Insite 
					 
				 
			 
			
			 
				 Opinion: 
				 
					 
						 Opinion Home 
						 | 
						 Send a letter to the editor 
						 | 
						 Newspaper Masthead 
					 
				 
			 
			
			 
				 Classifieds: 
				 
					 
						 Classifieds Home 
						 | 
						 Properties 
						 | 
						 Education Center 
					 
				 
			 
			
			 
				 Company Info: 
				 
					 
						 About the IHT 
						 | 
						 Advertise in the IHT 
						 | 
						 IHT Events 
						 | 
						 Press Office 
					 
				 
			 
			
			 
				 Newspaper: 
				 
					 
						 Today's Page One in Europe 
						 | 
						 Today's Page One in Asia 
						 | 
						 Publishing Partnerships 
					 
				 
			 
			
			 
				 Other Formats: 
				 
					 
						 iPhone 
						 | 
						 IHT Mobile 
						 | 
						 RSS 
						 | 
						 AudioNews 
						 | 
						 PDA &amp; Smartphones 
						 | 
						 Netvibes 
						 | 
						 IHT Electronic Edition 
						 | 
						 E-Mail Alerts 
						 | 
						 Twitter 
					 
				 
			 
			
			 
				 More: 
				 
					 
						 Daily Article Index 
						 | 
						 Hyper Sudoku 
						 | 
						 IHT Developer Blog 
						 | 
						 In Our Pages 
					 
				 
			 
			
		 
	 
	
	 
		 
			
			 
				Subscriptions 
				Sign Up  |  Manage
			 
		 

		 
			 
				 Contact Us 
				 | 
				 Site Index 
				 | 
				 Archives 
				 | 
				 Terms of Use 
				 | 
				 Contributor Policy 
				 | 
				 Privacy &amp; Cookies 
			 
		 
		 Copyright © 2008 the International Herald Tribune All rights reserved 
	 		
 
	
	








        

























