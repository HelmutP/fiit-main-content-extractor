














	
 



 
 
 The  Network 
 Tech News 
 Product Reviews 
 Business Intelligence 
 Security 
 Storage 
 VoIP 
 IT Management 
 Business 
 
 
 
 
IT News from 
 
 
Enterprise Product Reviews from 
 
 
Enterprise Software News from 
 
 
IT Security News from 
 
 
Enterprise Storage News From 
 
 
VoIP News from 
 
 
IT Management Insights from 
 
 
Business News from 
 
 	
 
	





 
 



&lt;noscript&gt;
&lt;a href="" http:&gt;
&lt;img src="" http: border="0" width="728" height="90" alt="" click here&gt;&lt;/a&gt;
&lt;/noscript&gt;

 


 
 
 Register Now! 
 Benefits 
 Login 
 
 


 

 
 
 
  
 


SEARCH 


  
 
 
  
 

 


 
 
 
 Blogs 
 News 
 Mobile 
 Software 
 Security 
 E-Business &amp; Management 
 Networking 
 Hardware 
 Careers 
 White Papers 
 Newsletters 
 
 

 



 
 

February 24, 2006 (1:49 PM EST)







 
 Gaming, Celebrity Sites Nastiest Web Neighborhoods 




 
Page 2 of 2
  

 
By
Gregg Keizer
, TechWeb Technology News
 
 Levy and Gribble also tested the same neighborhoods to see how many drive-by downloads each launched. Drive-bys are considered the most nefarious method of installing adware and spyware onto user's PCs, since they leverage browser bugs to automatically (or nearly automatically) download and install the software onto hard drives.
 
 
The worst neighborhood for drive-by downloads was, hands-down, made up of sites that offered pirated copies of software, games, music, and movies. In October, Levy and Gribble found 6.5 percent of all pirate domains conducting drive-by downloads. Adult (2 percent), celebrity (3.9 percent), and games (3.3 percent) followed.
 
 
Amazingly, those numbers were significantly lower than the ones gathered during an earlier sweep in May 2005, when 16.6 percent (about one in six) pirate sites did drive-bys, and 9 percent of adults sites used the practice to install spyware.
 
 
""It's difficult to attribute this to a specific cause,"" cautioned Levy and Gribble, who offered several possible reasons for the decline, ranging from greater adoption of anti-spyware tools to civil lawsuits filed against spyware and adware purveyors.
 
 
""The percent of executables that were infected with spyware caught us by surprise,"" said Levy. ""We didn't expect it to be that high.""
 
 
""What important here is that overall, one in 25 domains has infectious files,"" added Gribble.
 
 
Fortunately, they're concentrated in some neighborhoods, and nowhere to be seen in others.
 
 
The Levy/Gribble paper can be downloaded in PDF format from the University of Washington's Web site.
 
 




Before
 
 
Page 2 of 2
 







Bitty Browser (JavaScript required)



 
 
 
 Email This Story 
 Print This Story 
 Reprint This Story 
 Tag in Del.icio.us 
 
Digg This 
  Technology News 
 More TechWeb News 
 Bookmark this site! 
 
 
 


 
 
 
 
   Try TechWeb's RSS Feed! 
(Note: The feed delivers stories from TechWeb.com only, not the entire TechWeb Network.)
 















SECURITY WHITE PAPERS AND REPORTS


Auditing: What You Need to Know
 
All companies must go through a formal auditing process to ensure they're meeting various compliance demands. In theory, this exercise will help them understand where their security holes are and how to make appropriate improvements. But how do companies ensure their auditors understand specific IT security issues and requirements? We find out.
 
 
Using QUALYSGUARD to meet SOX compliance &amp; IT control objectives
 
As a guideline to achieve SOX compliance, the SEC has mandated that organizations use a recognized internal control framework—specifically the recommendations of the Committee of the Sponsoring Organizations of the Treadway Commission (COSO). This document shows how CobIT provides the actionable framework for compliance with COSO.
 
 
Regulatory Compliance and Critical System Protection: The Role of Mission-Critical Power and Cooling in Data Integrity and Availability
 
This white paper addresses the regulatory compliance issues that impact business continuity planning and how mission-critical power, cooling, and monitoring strategies support business continuity.
 
 
Keeping Up Your SOX Compliance - And Turning IT into a High Performer by Improving Change Control
 
Learn how to sustain ongoing SOX compliance efforts by recognizing &amp; implementing the IT controls that deliver long-term competitive advantages as well as meeting SOX requirements. This paper provides guidance to improve your Sarbanes-Oxley program efforts and highlights key information on Tripwire and to the many ways we can support your efforts.
 
 



 
More like this &gt;&gt;
 

















 
 


 

Top ten search terms from the TechWeb TechEncyclopedia
 
 

Convergence: Preparing the Enterprise Network
 
 

Strategies for gaining business by protecting the business
 
 

Managing Energy Use in the Data Center
 
 

Download free white papers and research from TechWeb Briefing Centers 
 
 


  
  
 
 


 
 
 Related White Papers 
 
 

Regulatory Compliance and Critical System Protection: The Role of Mission-Critical Power ...	

 
 

Beyond Search: Measuring The Impact Of Your Search Engine Via Analytics	

 
 

A Product Demo Tip - Demonstrating a Solution vs. a Bunch of Products	

 
 

The Future of PR: How to Garner More Coverage and Stay Way Ahead of the Competition	

 
 

Enhancing Collaboration Between In-House and Agency Teams	

 

  
  
  
 

 	
 
CAREER CENTER
 
 
 
 Ready to take that job and shove it? 
 
Open | Close
 
 
 
 
 
 SEARCH 
 
 


 
Function:  

Information Technology
Engineering

 
 
 
Keyword(s): 

 
 
 
State:
 
 

 
 
 
Post Your Resume
 
 
Employers Area
 
 
News &amp; Features
 
 
Blogs &amp; Forums
 
 
Career Resources 
  
Browse By:  
State | City 
 
 
 

 SPONSOR 
 
 

 
 
 RECENT JOB POSTINGS 
 
 

 

Featured Jobs:
 

BreakthroughIT seeking Project Manager in Groton, CT
 
  

Monsanto seeing IT Transportation and Optimization Analyst in St. Louis, MO
 
  

Princeton Financial Systems seeking Business Analyst 4 in Princeton, NJ
 
  

CSAA seeking IT Analyst IV in Glendale, AZ 
 
  

DisplaySearch seeking IT Project Manager in Austin, TX
 
  
For more great jobs, career-related news, features and services, please visit our ""Career Center.
 
 

 
 
 CAREER NEWS 
 
 
Five Rules For Bringing Your Real-Life Business Into Second Life 
 If you're thinking about establishing yourself in Second Life -- or are wondering whether you should -- we've got five rules that will help your new venture be a success. 
 
 
 
Hiring Report: Northrop Grumman Hiring On Wide Range Of Software Engineers  
 The global defense and tech company is seeking tech professionals skilled in Web site development, general software development, database administration, digital manufacturing, SAP/ABAP, complex CAD/CAM and PLM activities. 
 
 
 
 
 More Articles From Our Career Center 
  
 
 

Advertisement




&lt;noscript&gt;
&lt;a href="" http:&gt;
&lt;img src="" http: border="0" width="300" height="600" alt="" click here&gt;&lt;/a&gt;
&lt;/noscript&gt;


 


 
  
 


 

TechSearch for more articles on:
spyware
 adware
 drive-by download
 University of Washington
 Henry Levy
 Steven Gribble
 game
 gaming
 celebrity
 adult
 pirate
 piracy
 wallpaper
 screensaver
 executable

 
 
 

  
 
 
 Specialty Resources 

 
 

Business Continuity Self-Assessment Tool
 
 

Systems Management for the Scalable Enterprise
 
 

Increase Mobile Lotus Notes Users Productivity 
 
 

Best practices in imaging consolidation
 
 

FREE Subscription to DB2 Magazine and Newsletter
 
  
 
 VIEW ALL BRIEFING CENTERS 
 
  
  
 
 
 
 Featured Microsite 
 

 
  
  

 
 
 Related Links 
  
 
US Department of Defense Computer Used To Defraud Government
 
 
Sturdier Botnets Mean More Spam In 2007
 
 
Yahoo Fixes Messenger Flaw
 
 
Microsoft Patches Windows XP Wireless, Tells No One
 
 
Microsoft Patches IE 7 Phishing Filter To Boost Speed
 
 
Senators Threaten To Repeal Real ID Act Unless Changes Are Made
 
 
  
  
 
 
 

 Microsites 
 Featured Topic 






&lt;a href="" http: target="" _top&gt;&lt;img src="" http: width="125" height="125" border="0"&gt;&lt;/a&gt;





 Additional Topics 
 
 

Become a paperless office
 
 

Availability Management Process Roadmap
 
 

  
 
 
 Crush The Competition 
 TechWeb's FREE e-mail newsletters deliver the news you need to come out on top. 




  
  
 
 
 Techencyclopedia 
 Get definitions for more than 20,000 IT terms. 




  
  

 
 
 Techwebcasts 
 Editorial and vendor perspectives 

Ensure a good mobile experience for IT and end users.
 

Data protection for the distributed business: Free white paper has details.
 
 
  
  
 


 
 
 Vendor Resources 
 

Ease the SMB Storage Burden
 

Ease the SMB Storage Burden
 
 
  
  
 


 
 
 Focal Points 
 

Come up to speed on KVM-over-IP solutions: Download the free buyers guide
 

Safari Online Books closes gap between innovation and publication.
 
 
  
  
 



 
 
  
	
 

  
  
 
 

 
 

  


 
 


 
  
 
 

 Free Newsletters 
 TechEncyclopedia 
 TechCalendar 
 Opinion 
 Research 
 Careers &amp; Workplace 
 Webcasts 
 About Us 
 Contact Us 
 
 Site Map 
 Mobile Tech News 
 Software Tech News 
 Security Tech News 
 E-Business Tech News 
 Management Tech News 
 
 Networking Tech News 
 Hardware Tech News 
 
  

 
 
 InformationWeek 
 Network Computing 
 Financial Technology Network 
 Wall Street &amp; Technology 
 Bank Systems &amp; Technology 
 
 Insurance &amp; Technology 
 IT Pro Downloads 
 Intelligent Enterprise 
 Byte and Switch 
 Dark Reading 
 bMighty 
 Small Biz Resource 
 
 Byte.com 
 VoipLoop 
 CollaborationLoop 
 Blackhat 
 
Interop 
 
 
  

 
 



 
  


 
 
 Terms of Service 
 
Copyright ©2007 CMP Media LLC
 
 Privacy Statement 
 Your California Privacy Rights 
 Media Kit 
 Feedback 
 
  
 
 











