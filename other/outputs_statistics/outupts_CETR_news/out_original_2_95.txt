

 "        
//W3C//DTD XHTML 1.0 Strict//EN""
        ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd""&gt;


 

						    
			    				Profiles: Meredith Kercher murder suspects |
				World news |
				guardian.co.uk
	
		

			
		




								
			
		
		
		
	
		
					
				
		
		
				









 

    
    		
	
					
				  
			

    		
	
	
	
		  
	

 
	 
		 Jump to content [s] 
				 Jump to site navigation [0] 
		 Jump to search [4] 
		 Terms and conditions [8] 
	 
 
 

	 
		 

 Sign in 
 Register 

 Text larger
 
 smaller 

 




					
						



     


	        
    
                
        
        
	
    

     

			
	 
	 
					
			 
				
		
	
	
		Search: 
		
		
			
		
			guardian.co.uk
			
							World news
						Web
			
		
		
		
		
	
		
	 
		    

	 
		 
			
																													 
						News
					 
							
																								 
						Sport
					 
							
																								 
						Comment
					 
							
																								 
						Culture
					 
							
																								 
						Business
					 
							
																								 
						Money
					 
							
																													 
						Life &amp; style
					 
							
																								 
						Travel
					 
							
																								 
						Environment
					 
							
																													 
						Blogs
					 
							
																													 
						Video
					 
							
																													 
						Jobs
					 
							
																																		 
						A-Z
					 
									 
	 


					 			
									    
	
	  
		 
										 News 
							 World news 
							 Meredith Kercher 
					 
	 



							 
			 
		
		
 


 
 	    


	 
					    


				
		        


        



	    




  	   	        
   	   	   	   	
      	       
  	   	        
   	   	   		
	
		
        
            

		
				
		 
			
							 Meredith Kercher murder suspects 
						
							 Profiles of the three suspects in the killing of the British student in Perugia last November 
				
					 
			 



     
      	    
  
	        

	    
        




	
 
	    			 
											                	        	            Tom Kington in Perugia
				 
							 
			  guardian.co.uk,
			 
	  			  			 Tuesday October 28 2008 14.31 GMT 
	  			  				
		
	 

 

	
			 All three suspects have denied killing the 21-year-old Leeds university student. Two of the suspects, Amanda Knox and her ex-boyfriend Raffaele Sollecito, were due to learn today whether they would face a murder trial. The third suspect, Rudy Guede, had opted for a fast-track murder trial, which was expected to conclude today. 
 Amanda Knox 		    
	        	        
	        	        				
					Photograph: Reuters
				
	        	    
	
	



 The 21-year-old, Jesuit-educated student from Seattle has been described by American friends as a kind, warm, intelligent girl, far removed from the sexual predator and loudmouth she was depicted as by Meredith Kercher's friends after she arrived in Perugia to study last year.  
 A keen football player and climber in Seattle, one American university friend, Andrew Cheung, said he would trust his life with Knox. She was, however, not popular with all of her peers in Perugia; one student, Conti von Hirsch, said he had found her ""brash, over-talkative and likely to laugh hard at her own jokes"". 
 During her months in jail in Italy, Knox has reportedly started to learn Chinese and continued to play the guitar, picking up the lyrics to Italian pop songs. Her favourite TV programme in jail is reportedly Big Brother. 
 Raffaele Sollecito 		    
	        	        
	        	        				
					Photograph: Reuters
				
	        	    
	
	



 The 24-year-old Italian from the southern city of Bari is the son of a doctor and was staying in Perugia with a well-to-do family. 
 The prosecutor Giuliano Mignini has said that Sollecito was warned by a tutor at college about watching violent pornography. Sollecito's lawyers have said he was at home using an interactive cartoon website at the time Kercher was killed last November.  
 The prosecution says his DNA was found on Kercher's bra clasp; his lawyers claim the bra was evidentially contaminated because of police mistakes.   
 Since his arrest, he has kept busy in jail, gaining a degree in information technology and signing up for a second degree. Between occasional games of five-a-side football he has been reading Dostoevsky and firing off letters to a local newspaper to complain about the way the press has picked his character apart. ""What does it matter if the various characters in these events suffer?"" he wrote in one letter. ""The important thing is to find a scoop and to cast as many shadows and suspects as possible."" 
 For Sollecito, his fate is now in the hands of his lawyers, and God. ""Every time I feel lonely and sad I think of He who is wishing me well,"" he wrote. ""It is He who watches me from above and is there to console me in difficult times."" 
 Rudy Guede 		    
	        	        
	        	        				
					Photograph: EPA
				
	        	    
	
	



 The 21-year-old was born in Ivory Coast and brought to Perugia as a child by his father, but later had to fend for himself after his father returned to Africa. 
 He managed to find accommodation with a wealthy Perugia family, and picked up work. He also played basketball, and was a member of the city's team in 2004 and 2005.  
 But prosecutors have plotted a decline in his fortunes beginning last year, accusing him of three break-ins, including one on September 2 when he allegedly entered the house of the Perugia bar owner Christian Tramontano. The bar owner said he confronted a man he later identified as Guede, who threatened him with a knife before fleeing.  
 Some locals have described Guede as a regular in the city's bars and clubs and claimed he had a reputation for chasing women. But his lawyer Walter Biscotti has said Guede ""is a boy just like many others his age"". 
	
	
 


    
  	
	
  
 




		
 
		
		    



	 
	
							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
		
			 
			
			
			larger | 
			smaller		 
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
			



			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 

		    

	
	 
	
							
					 World news 
					
						 
			 
									Meredith Kercher ·							 
			 
									Italy 							 
		 
									
					 UK news 
					
					
							
		
 


		    			 
			 
																										 
						 More on this story 
					 
																						 
							 
								    
		    	
	
	
								Man sentenced to 30 years for Kercher murder
							 
						 
																																																	 
							 
								    
		    	
	
	
								Background: The Meredith Kercher case
							 
						 
																																																																															 
							 
								    
		    	
	
	
								Family recalls 'beautiful, intelligent, caring' Meredith
							 
						 
																																																						 
							 
								    
		    	
	
	
								Timeline of the case
							 
						 
																					 
		 
	

		    


	
		
		
				
	


 

	
	
		
	    



	 

							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
			 Article history 
		
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
			



			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 
	     
	 
		 
			 About this article 
            Close
		 
		        	        			
					   	   	   
 			 
				 Profiles: Meredith Kercher murder suspects 
		   				   	 						This article was first published on guardian.co.uk on Tuesday October 28 2008. It was last updated at 16.18 on October 28 2008.							 
			 
 


  	    	    
	    	 
									 Related information follows Ads by Google 
						    



	
 




	 




				  
 

 
	
	
      	    	    
	    




     


	        
    
                
        
        
	
    

     


	        				
 	
 


	    
	
			
	 
		 
			
											
									 Bestsellers from the Guardian shop 
								
				 
											
										
											 BlueTinum internet Radio 
										
											 Designed in Denmark this radio uses Wi-Fi to access over 10,000 free world-wide broadcasts. 
										
											 From: £99.95 
									 
				
				 
									 
																					 Visit the Guardian offers shop 
																																		 Buy eco, organic and fair trade products 
				 														 
								 
					
		 
	 
	

	    
			 
		 Latest news on guardian.co.uk 
	

		 
			    
	                         Last updated one minute ago 
            
		 
		 
							 
					 News 
					 Man sentenced to 30 years for Kercher murder 
				 
							 
					 Sport 
					 Newcastle beat West Brom 
				 
							 
					 World news 
					 Obama: 'We are one week from change - but it won't be easy' 
				 
					 
	 

	        


 
															 Free P&amp;P at the Guardian bookshop 
																					
	 
					
							 
							 
					
						
					
				 
				 
					 
						 It's All Greek to Me 
						 ?12.99 with free UK delivery 
					 
				 
			 
					
							 
							 
					
						
					
				 
				 
					 
						 Big Necessity 
						 ?12.99 with free UK delivery 
					 
				 
			 
			 
			 
																			 
													Browse the bestseller lists
											 
																 
													Buy books from the  Guardian Bookshop
											 
									 
	 


	    

	
		
 
	
	
		
	 
		 
			 Sponsored features 
		 
		 	
						            
    
                
        
        
            
		 
		 	
						            
    
                
        
        
        
		 
	 
 


	    	
		
			
	
		 

	
			  
	
		
	    



	  
		
			
											
										
									
				 
		UK
	 
				
		
			
											
										
						
				 
		USA
	 
				
			 

	 
			 
			
						
													
			 UK 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				YOUNG CARERS SUPPORT WORKER
 
												 family action.
												kensington &amp; chelsea.
												?21,099 - ?23,437 p.a. pro rata inc. ILW Salary Review Pending.
								 
			 
					 
 
				Regional Development Officer
 
												 development trusts association-1.
												east of england: flexible - within the region.
												?26,095 to ?29,454.
								 
			 
					 
 
				Advertising Account Director for leading magazine…
 
												 meet the real me ltd.
												as advertisement director , you will be responsibl….
												?50K plus benefits.
								 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
			 
			
						
													
			 USA 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Southeastern, NC Opportunity
 
								 north carolina. duplin county has a strong agriculture heritage and a promising future. it operates the arts council that is responsible for coordinating and... .
																nc.
												 
			 
					 
 
				Spanish - Assistant/Associate Professor
 
								 undergraduates, including heritage speakers, and be... mission. the school of arts and sciences has a strong liberal arts tradition and serves a diverse... .
																ny.
												 
			 
					 
 
				Food &amp; Beverage Director - The R. David Conference Center
 
								 will possess a culinary arts or bachelor's degree in... places to work! imagine working at a highly rated heritage property... at a resort surrounded by stunning... .
																nc.
												 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
		 
	


	
	
		
	
	

	
	 




      	    	    
	    			



     


	        
    
                
        
        
	
    

     

	


      	      	
      	 

			    
	    


	
		
		
		
		 
							 Related information 
				    

	
	 
	
							
					 World news 
					
						 
			 
									Meredith Kercher ·							 
			 
									Italy 							 
		 
									
					 UK news 
					
					
		
		
 

				
			
						 
			
																					 
													
												
					 
								 
															
											Kercher family to face suspects in Italian court
						 
 	
							    

										                    		Sep 16 2008: 
		                    								  
 Meredith Kercher's sister, Stephanie, and parents, John and Arline, prepare to face the three people accused of fatally stabbing and sexually assaulting the British student 
					 	
					 						 							 							
															More video
													 	
												
													
							 

						 
			
													 
				Oct 28 2008 
										 
															
						    

						Timeline: The Meredith Kercher case
				  
																		 
				Oct 28 2008 
										 
															
						    

						Kercher bra carries DNA of all three suspects, says lawyer
				  
																		 
				Jul 12 2008 
										 
															
						    

						Italy student murder: Three charged with Kercher killing
				  
																		 
				Jan 12 2008 
										 
															
						    

						New scientific evidence in Kercher murder
				  
							 

						 
			
																					 
													
												
					 
								 
															
											Inside Italian Gypsy camps
						 
 	
							    

										                    		Aug 17 2008: 
		                    								 Photographer Robin Hammond travels to the Roma camps of Naples in Italy to document the people and their plight against discrimination from the rest of the country
					 	 
					 						 							 							
															More galleries
													 	
												
													
							 

			
		 
		
				
	


		




 
		 			
			 
License/buy our content |  
			 
Privacy policy |  
			 
Terms &amp; conditions |  
			 
Advertising guide |  
			 
Accessibility |  
			 
A-Z index |  
			 
Inside guardian.co.uk blog |  
			 
About guardian.co.uk |  
			 Join our dating site today 
		 
		
			
			 		
			
			 guardian.co.uk © Guardian News and Media Limited 2008 
			
						
			 
			
	
	    

	


	
		
			Go to: 
			
			
						
												
								
																														guardian.co.uk home
									
								
																	UK news
									
								
																	World news
									
								
																	Comment is free blog
									
								
																	Newsblog
									
								
																	Sport blog
									
								
																	Arts &amp; Entertainment blog
									
								
																	In pictures
									
								
																	Video
									
								
									
								
																	Archive search
									
								
																	Arts &amp; entertainment
									
								
																	Books
									
								
																	Business
									
								
																	EducationGuardian.co.uk
									
								
																	Environment
									
								
																	Film
									
								
																	Football
									
								
																	Jobs
									
								
																	Katine appeal
									
								
																	Life &amp; style
									
								
																	MediaGuardian.co.uk
									
								
																	Money
									
								
																	Music
									
								
																	The Observer
									
								
																	Politics
									
								
																	Science
									
								
																	Shopping
									
								
																	SocietyGuardian.co.uk
									
								
																	Sport
									
								
																	Talk
									
								
																	Technology
									
								
																	Travel
									
								
																	Been there
									
								
									
								
																	Email services
									
								
																	Special reports
									
								
																	The Guardian
									
								
																	The Northerner
									
								
																	The Wrap
									
								
									
								
																	Advertising guide
									
								
																	Compare finance products
									
								
																	Crossword
									
								
																	Events / offers
									
								
																	Feedback
									
								
																	Garden centre
									
								
																	GNM Press Office
									
								
																	Graduate
									
								
																	Bookshop
									
								
																	Guardian Ecostore
									
								
																	Guardian Films
									
								
																	Headline service
									
								
																	Help / contacts
									
								
																	Information
									
								
																	Living our values
									
								
																	Newsroom
									
								
																	Notes &amp; Queries
									
								
																	Reader Offers
									
								
																	Readers' editor
									
								
																	Soulmates dating
									
								
																	Style guide
									
								
																	Syndication services
									
								
																	Travel offers
									
								
																	TV listings
									
								
																	Weather
									
								
																	Web guides
									
								
																	Working for us
									
								
									
								
																	Guardian Weekly
									
								
																	Public
									
								
																	Learn
									
								
																	Guardian back issues
									
								
																	Observer back issues
									
								
																	Guardian Professional
									
														
			
		
	

 

 
	    


	    





	  





	    
	









     


	        
    
                
        
        
	
    

     




