

 Call it ""Project Midway."" The concept: Give Bears fans a little fashion with their passion. We asked fashion design students from The Illinois Institute of Art-Chicago to transform a selection of boxy, generic Bears T-shirts and stocking caps into something more -- more fun, more unusual, more stylin' -- that women would love to wear to the biggest party of the year on Super Sunday. 
 
 
In a matter of hours, they transformed $20 shirts and $10 stocking caps into one-of-a-kind jock frocks. The best part? Yes, fans, you can try this at home. 
 
 lbaldacci@suntimes.com 
 
 



 
 



















 » Click to enlarge image
 





 The Illinois Institute of Art-Chicago fashion design students participate in our challenge to transform a selection of boxy, generic Bears T-shirts and stocking caps into something more.  (Dom Najolia/Sun-Times)
 
 







PHOTO GALLERY 

	


		 

� Stylin? Bears fashions 
 
	
	
	










	
	

	

	

	













	
 
 		



 CORTNEY DUVAL, 21 
FRESHMAN FROM THE GOLD COAST 
 Duval is a football fan ""and a big fan of hoodies."" She chose a large-sized, short-sleeved, navy blue ""2006 Conference Champions"" T-shirt (""I really like the Bear logo"") and went to work. 
 
 First she turned the shirt inside out and cut along the side and underarm seams, removing about three inches from the sleeves, sides and bottom. She re-sewed the seams, creating a much smaller shirt. She cut football-shaped pockets from an orange knit cap; added shoulder pad-motif accents and a waistband using pieces of striped jersey rib knit ($5 at a fabric store). Finally, she added a gray fleece hood, cut away a section of the front and added big orange laces. 
 
 ""It reminds me of 'Sporty Spice,'"" said Duval. 
 
 
 
 JASMINE JONES, 21 
SENIOR FROM THE WEST SIDE 
 Jones chose the shirt with the simplest design, a navy blue, long sleeved, size XL, with a classic orange ""C"" on the front. Jones cut around the crew neck, leaving it attached for about six inches at the front. She cut off the sleeves, following the curve of the C down the sides, under the arm and straight across the back. (The shoulders, sleeves and upper back of the shirt -- still in one piece -- became a shrug.) She made a second cut, under the bust, all the way around. She cut a three-inch band of orange satin and attached to the halter top. She loosely gathered, then attached, the shirt bottom to the orange band to complete her halter dress. 
 
 
 
 JESSICA OLIVERII, 25 
SENIOR FROM NORTHBROOK 
 Oliverii turned a gray ""NFC Champions"" shirt into a drawstring tube top. She cut across the top, attached a band she salvaged from a navy blue shirt, and ran an orange drawstring to snug the top. She sewed darts for a custom fit. At the bottom, she added a panel of stretchy orange fabric. Another scrap of navy shirt became a scarf, with orange feathers for a girly accent. ""It's a jock frock,"" said Oliverii, ""a trendy tunic."" 
 
 
 
 PRIYA PANDEY  
INSTRUCTOR FROM NAPERVILLE 
 Pandey, who teaches clothing construction and pattern making, cut a tiny tank top from a large shirt, and had enough fabric left over to whip up a pleated mini-skirt. The waistband was a knit hat in a former life. 
 
 ""I have a 13-year-old daughter, and she's going to a Super Bowl party,"" Pandey said. ""I'm keeping her in mind."" 
 
 Her daughter Pallavi models the outfit. 
 
 The seventh-grader's verdict: ""It's cute."" 
 
 
 
 JENNIFER WRIGHT, 22 
SENIOR FROM LINCOLN PARK 
 Wright cut a scoop neck and trimmed it with a stretchy band of blue sequins. ""You can wear it off the shoulders, or off one shoulder,"" she said. She added orange ribbon stripes to the sleeves, then made a deep cut down the back and laced it up like a football. Last, she added an orange ruffle at the bottom. 
 
 
 
 PAULA ADAMS, 23 
SENIOR FROM LOGAN SQUARE 
 Adams took a long-sleeved XL shirt and turned it into an empire-waist tunic with leg warmers. First she cut off the sleeves, removed the crew neck and cut down the front to form a v-neck. Then she cut the shirt in half under the logo. She cut a three-inch rim from a Bears knit stocking cap and attached it to the shirt top. She attached the bottom half of the shirt to the other edge of the band. White laces completed the tunic. She attached two pieces from the stocking cap to the sleeves. Voila: leg warmers! 
 
 ""This is trendy but casual,"" Adams said. 

