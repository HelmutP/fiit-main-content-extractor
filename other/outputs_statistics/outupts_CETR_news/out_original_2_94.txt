

 "        
//W3C//DTD XHTML 1.0 Strict//EN""
        ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd""&gt;


 

						    
			    				Rudy Guede sentenced to 30 years for murder of Meredith Kercher |
				World news |
				guardian.co.uk
	
		

			
		




								
			
		
		
		
	
		
					
				
		
		
				









 

    
    		
	
					
				  
			

    		
	
	
	
		  
	

 
	 
		 Jump to content [s] 
				 Jump to site navigation [0] 
		 Jump to search [4] 
		 Terms and conditions [8] 
	 
 
 

	 
		 

 Sign in 
 Register 

 Text larger
 
 smaller 

 




					
						



     


	        
    
                
        
        
	
    

     

			
	 
	 
					
			 
				
		
	
	
		Search: 
		
		
			
		
			guardian.co.uk
			
							World news
						Web
			
		
		
		
		
	
		
	 
		    

	 
		 
			
																													 
						News
					 
							
																								 
						Sport
					 
							
																								 
						Comment
					 
							
																								 
						Culture
					 
							
																								 
						Business
					 
							
																								 
						Money
					 
							
																													 
						Life &amp; style
					 
							
																								 
						Travel
					 
							
																								 
						Environment
					 
							
																													 
						Blogs
					 
							
																													 
						Video
					 
							
																													 
						Jobs
					 
							
																																		 
						A-Z
					 
									 
	 


					 			
									    
	
	  
		 
										 News 
							 World news 
							 Meredith Kercher 
					 
	 



							 
			 
		
		
 


 
 	    


	 
					    


				
		        


        



	    




  	   	        
   	   	   	   	
      	       
  	   	        
   	   	   		
	
		
        
            

		
				
		 
			
							 Man sentenced to 30 years for Kercher murder 
						
							 Amanda Knox and Rafaelle Sollecito also to stand trial for Kercher murder, judge rules 
				
					 
			 



     
      	    
  
	        

	    
        




        	
 
	    			 
											                	        	        	            Lee Glendinning, Mat Smith and agencies
				 
							 
			  guardian.co.uk,
			 
	  			  			 Tuesday October 28 2008 21.45 GMT 
	  			  				
		
	 

 

			 
							
									   Raffaele Sollecito, Amanda Knox and Rudy Guede. Photographs: AP and EPA 
					 
	
			 Rudy Guede has tonight been sentenced to 30 years in prison for the murder of British student Meredith Kercher, an Italian judge has ruled. 
 The judge at the court in Perugia also ruled that there was sufficient evidence available for Amanda Knox and Rafaelle Sollecito to stand full trial for Kercher's murder. 
 Knox, 21, a US student, and Sollecito, 24, an Italian IT graduate, were accused of having killed Kercher in the course of a sex game that went wrong, alongside Rudy Hermann Guede, 21. 
 Kercher's semi-naked body was found on November 2 in her bedroom in the cottage she shared with Knox. Her throat had been cut. 
 Lawyer Francesco Maresca, who represents Miss Kercher's family, said: ""We are very satisfied, even though this was a young man who faces a very heavy sentence."" 
 For the past two months, Guede, from Ivory Coast, has been involved in a fast-track trial at his own request on charges of murder and attempted sexual assault. He has the right to appeal the sentence. 
 At the same time, judge Paolo Micheli listened to evidence from lawyers for Knox and Sollecito in pre-trial hearings to decide whether the pair should face trial over the student's murder. 
 Knox and Sollecito have been held in jail since shortly after the murder and the judge has tonight indicated that he would not grant their requests for house arrest. The trial is set to begin on December 4.  
 It is now close to a year since the British student was found dead, two months after she arrived in the Umbrian hilltop town to begin a one-year Erasmus programme studying modern history, political theory and the history of cinema. 
 The prosecution said the three were taking part in a Halloween-inspired sex party, in which Kercher had been forced to participate. 
 They said Knox had stabbed Kercher in the throat while Sollecito held her down and Guede tried to sexually assault her. 
 Guede admitted being in the house, saying he was in the bathroom when Kercher was attacked and that he rushed into the bedroom to try to rescue her, but he was scared and immediately fled the scene. 
 Sollecito said he was in his own apartment in Perugia and that he does not remember if Knox spent part or all of that night with him. 
 Knox initially told investigators she was in the house when Kercher was killed, and covered her ears when she heard screams. Later, she said she was not in the house. 
 Much of the prosecution case centred on DNA evidence, which the defence argued was inconclusive or contaminated. 
	
	
 


    
  	
	
  
 




		
 
		
		    



	 
	
							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
		
			 
			
			
			larger | 
			smaller		 
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
			



			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 

		    

	
	 
	
							
					 World news 
					
						 
			 
									Meredith Kercher ·							 
			 
									Italy 							 
		 
									
					 UK news 
					
						 
			 
									Crime 							 
		 
					
								 
																			 More news 		
							 
				
		
 


		    			 
			 
																										 
						 More on this story 
					 
																																															 
							 
								    
		    	
	
	
								Background: The Meredith Kercher case
							 
						 
																																																	 
							 
								    
		    	
	
	
								Who's who in the Kercher case
							 
						 
																																																						 
							 
								    
		    	
	
	
								Family recalls 'beautiful, intelligent, caring' Meredith
							 
						 
																																																						 
							 
								    
		    	
	
	
								Timeline of the case
							 
						 
																					 
		 
	

		    


	
		
		
				
	


 

	
	
		
	    



	 

							 Printable version 
		
			 Send to a friend 
		
			 Share 
		
			 Clip 
		
			 Contact us 
		
			 Article history 
		
	
 

	    


 
	 
		 
			 Email 
			Close
		 
		 
		
				        	       		        
	        			
				
					 
						Recipient's email address
					 
					
									
				
					 
						Your name
					 
				   
				
				
					 
						Add a note (optional)
						 
					 
				   
				
				 
					
				 
				 Your IP address will be logged 
			
		 
	 
 
	    


 
     
     
             Share 
            Close
     
    
       
        Digg
       
       
        reddit
       
        
        Google Bookmarks
       
       
        Yahoo! My Web
       
       
        del.icio.us
       
       
        StumbleUpon
       
       
        Newsvine
       
       
        livejournal
       
       
        Facebook
       
       
        BlinkList
       
    
    
    
 
	    

 
	 
		 
			 Contact us 
            Close
		 
		
		 
			    
			



			 
				 
					                        Report errors or inaccuracies: userhelp@guardian.co.uk
                    				 
				 
											Letters for publication should be sent to: letters@guardian.co.uk
									 
			 
		 
		 
			 
				 
					If you need help using the site: userhelp@guardian.co.uk
				 
				 
					Call the main Guardian and Observer switchboard:  +44 (0)20 7278 2332
				 
				 
					 
						 
							Advertising guide
						 
						 
							License/buy our content
						 
					 
				 
			 
		 
	 
 
	     
	 
		 
			 About this article 
            Close
		 
		        	        			
					   	   	   
 			 
				 Rudy Guede sentenced to 30 years for murder of Meredith Kercher 
		   				   	 						This article was first published on guardian.co.uk on Tuesday October 28 2008. It was last updated at 21.45 on October 28 2008.							 
			 
 


  	    	    
	    	 
									 Related information follows Ads by Google 
						    



	
 




	 




				  
 

 
	
	
      	    	    
	    




     


	        
    
                
        
        
	
    

     


	        				
 	
 


	    
	
			
	 
		 
			
											
									 Bestsellers from the Guardian shop 
								
				 
											
										
											 Paint Runner 
										
											 A revolution in paint rolling: no more bending down to charge the roller or balancing with a tray on a ladder!  
										
											 From: £29.99 
									 
				
				 
									 
																					 Visit the Guardian offers shop 
																																		 Buy eco, organic and fair trade products 
				 														 
								 
					
		 
	 
	

	    
			 
		 Latest news on guardian.co.uk 
	

		 
			    
	                         Last updated one minute ago 
            
		 
		 
							 
					 News 
					 Man sentenced to 30 years for Kercher murder 
				 
							 
					 Sport 
					 Newcastle beat West Brom 
				 
							 
					 World news 
					 Obama: 'We are one week from change - but it won't be easy' 
				 
					 
	 

	        


 
															 Free P&amp;P at the Guardian bookshop 
																					
	 
					
							 
							 
					
						
					
				 
				 
					 
						 White Tiger 
						 ?12.99 with free UK delivery 
					 
				 
			 
					
							 
							 
					
						
					
				 
				 
					 
						 Big Necessity 
						 ?12.99 with free UK delivery 
					 
				 
			 
			 
			 
																			 
													Browse the bestseller lists
											 
																 
													Buy books from the  Guardian Bookshop
											 
									 
	 


	    

	
		
 
	
	
		
	 
		 
			 Sponsored features 
		 
		 	
						            
    
                
        
        
            
		 
		 	
						            
    
                
        
        
        
		 
	 
 


	    	
		
			
	
		 

	
			  
	
		
	    



	  
		
			
											
										
									
				 
		USA
	 
				
		
			
											
										
						
				 
		UK
	 
				
			 

	 
			 
			
						
													
			 USA 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Program Manager SNF Therapy Jobs
 
								 cheese.its cities offer arts and culture rivaling the country's big cities. you'll find historic neighborhoods that reflect a strong heritage and festivals that... .
																sc.
												 
			 
					 
 
				superb practice opportunity adjacent to/10 minutes from
 
								 adjacent to the diverse cosmopolitan offerings of arts and culture, delicious cuisine and dynamic music... french and mexican heritage
 
 the area has a semi... .
																la.
												 
			 
					 
 
				Language Arts Teacher
 
								 county certified vacancies: position: language arts teacher (full-time; standard) summary description... and cultural heritage. selects instructional goals... .
																nv.
												 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
			 
			
						
													
			 UK 
						
									
							    

 
	



		
		    			      
						
			
			
			
					
	
	
						
		 
					 
 
				Regional Development Officer
 
												 development trusts association-1.
												east of england: flexible - within the region.
												?26,095 to ?29,454.
								 
			 
					 
 
				Policy Executive
 
												 postgraduate medical education &amp; training board.
												london, waterloo (se1).
												?28,370 - ?33,380 per annum.
								 
			 
					 
 
				Telesales Executive - Travel Show
 
												 dragonfly.
												telesales executive - travel show.
												?20000 - ?22000 per annum + Bonus.
								 
			 
				 	
	


     
				        Browse all jobs
     
	
 


						
		 
		 
	


	
	
		
	
	

	
	 




      	    	    
	    			



     


	        
    
                
        
        
	
    

     

	


      	      	
      	 

			    
	    


	
		
		
		
		 
							 Related information 
				    

	
	 
	
							
					 World news 
					
						 
			 
									Meredith Kercher ·							 
			 
									Italy 							 
		 
									
					 UK news 
					
						 
			 
									Crime 							 
		 
					
		
		
 

				
			
						 
			
																					 
													
												
					 
								 
															
											My neighbourhood
						 
 	
							    

										                    		Oct 14 2008: 
		                    								  
 The Joseph Rowntree Foundation asked young people to draw a map of where they feel safe or afraid in their neighbourhoods 
					 	
					 						 							 							
															More galleries
													 	
												
													
							 

						 
			
													 
				Jan 12 2008 
										 
															
						    

						New scientific evidence in Kercher murder
				  
																		 
				Dec 1 2007 
										 
															
						    

						Suspects in Kercher killing told they must stay in prison
				  
																		 
				Nov 26 2007 
										 
															
						    

						Kercher murder suspect says he will lead police to real killer
				  
																		 
				Nov 25 2007 
										 
															
						    

						Knox 'has no contact with reality'
				  
							 

						 
			
																					 
													
												
					 
								 
															
											Kercher family to face suspects in Italian court
						 
 	
							    

										                    		Sep 16 2008: 
		                    								  
 Meredith Kercher's sister, Stephanie, and parents, John and Arline, prepare to face the three people accused of fatally stabbing and sexually assaulting the British student 
					 	
					 						 							 							
															More video
													 	
												
													
							 

			
		 
		
				
	


		




 
		 			
			 
License/buy our content |  
			 
Privacy policy |  
			 
Terms &amp; conditions |  
			 
Advertising guide |  
			 
Accessibility |  
			 
A-Z index |  
			 
Inside guardian.co.uk blog |  
			 
About guardian.co.uk |  
			 Join our dating site today 
		 
		
			
			 		
			
			 guardian.co.uk © Guardian News and Media Limited 2008 
			
						
			 
			
	
	    

	


	
		
			Go to: 
			
			
						
												
								
																														guardian.co.uk home
									
								
																	UK news
									
								
																	World news
									
								
																	Comment is free blog
									
								
																	Newsblog
									
								
																	Sport blog
									
								
																	Arts &amp; Entertainment blog
									
								
																	In pictures
									
								
																	Video
									
								
									
								
																	Archive search
									
								
																	Arts &amp; entertainment
									
								
																	Books
									
								
																	Business
									
								
																	EducationGuardian.co.uk
									
								
																	Environment
									
								
																	Film
									
								
																	Football
									
								
																	Jobs
									
								
																	Katine appeal
									
								
																	Life &amp; style
									
								
																	MediaGuardian.co.uk
									
								
																	Money
									
								
																	Music
									
								
																	The Observer
									
								
																	Politics
									
								
																	Science
									
								
																	Shopping
									
								
																	SocietyGuardian.co.uk
									
								
																	Sport
									
								
																	Talk
									
								
																	Technology
									
								
																	Travel
									
								
																	Been there
									
								
									
								
																	Email services
									
								
																	Special reports
									
								
																	The Guardian
									
								
																	The Northerner
									
								
																	The Wrap
									
								
									
								
																	Advertising guide
									
								
																	Compare finance products
									
								
																	Crossword
									
								
																	Events / offers
									
								
																	Feedback
									
								
																	Garden centre
									
								
																	GNM Press Office
									
								
																	Graduate
									
								
																	Bookshop
									
								
																	Guardian Ecostore
									
								
																	Guardian Films
									
								
																	Headline service
									
								
																	Help / contacts
									
								
																	Information
									
								
																	Living our values
									
								
																	Newsroom
									
								
																	Notes &amp; Queries
									
								
																	Reader Offers
									
								
																	Readers' editor
									
								
																	Soulmates dating
									
								
																	Style guide
									
								
																	Syndication services
									
								
																	Travel offers
									
								
																	TV listings
									
								
																	Weather
									
								
																	Web guides
									
								
																	Working for us
									
								
									
								
																	Guardian Weekly
									
								
																	Public
									
								
																	Learn
									
								
																	Guardian back issues
									
								
																	Observer back issues
									
								
																	Guardian Professional
									
														
			
		
	

 

 
	    


	    





	  





	    
	









     


	        
    
                
        
        
	
    

     




