

 WASHINGTON -- Former House Speaker J. Dennis Hastert (R-Ill.) on Friday is expected to announce he will not seek a 12th term. 
 
 
Hastert, 65, the longest-serving GOP speaker, signaled last May he was contemplating retiring from the House when chief of staff Mike Stokke told the Sun-Times a Hastert retirement was a ""possibility."" Hastert, who lives in Plano, has virtually stopped all fund-raising and has kept a low profile since giving up the speaker's gavel last January when the Democrats took control of the House. 
 
 Hastert has asked his supporters to attend a Friday announcement at the Kendall County Courthouse in Yorkville. 
 
 



 
 

























	
	
	








 RELATED STORIES 

� Sweet blog: Hastert to announce he won't run again 

	
	

	

	

	













	
 		



His expected departure from the 14th District, which includes parts of Aurora, Elgin and DeKalb, creates a rare open seat and triggers a wide-open primary in both parties. 
 There is no obvious favorite GOP son or daughter for Hastert to tap and it is not clear that Hastert cares to engineer his successor. Only state Sen. Chris Lauzen (R-Aurora) has filed candidate papers. Waiting for Hastert's decision are Geneva Mayor Kevin Burns and ice cream mogul and investment fund executive Jim Oberweis. 
 
 Three Democrats are already in the race in this nominal Republican district: businessman and scientist Bill Foster; Jonathan Laesch, the 2006 Democratic nominee, and attorney Jotham Shepard. 
 
 Hastert was persuaded to run again in 2006 by President Bush, who said he wanted Hastert to serve as speaker through his second term -- not figuring the Republicans would lose power. Bush earlier this month asked Hastert to lead an official delegation to the funeral of former Japanese Prime Minister Kiichi Miyazawa. 
 
 Hastert, a former Yorkville wrestling coach, was first elected to Congress in 1986 after serving in the Illinois House. Hastert won his 11th term in 2006 with 59.79 percent of the vote, running with a cloud over him because of the Mark Foley page scandal. 
 
 Hastert's political war chest was drained because of large legal bills related to defending himself in the scandal, where the Florida congressman got caught sending suggestive messages to young men. Hastert ended up testifying before an ethics panel probing what House leaders knew when about warnings over Foley's conduct. 

