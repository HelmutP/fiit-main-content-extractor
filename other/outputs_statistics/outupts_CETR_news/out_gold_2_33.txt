

 "The Fighting Temptations

 
 HOLLYWOOD studios usually don't pay much attention to Christian audiences, so it's always a little flattering when one of them tries to curry our favour. But to judge from some of the films they send our way, they still don't understand us very well. The latest case in point: The Fighting Temptations.

 
 Paramount Pictures seems to think it has a winner in this film, a comedy about a down-on-his-luck hustler who finds some sort of redemption while directing a black gospel choir, so the studio has bought ads in Christian newspapers and shown the film at a Christian trade convention. The film features Christian rapper T-Bone in a small role, and Relevant, a Christian pop-culture magazine, recently gave the film front-cover treatment by profiling the faith of co-star Beyonce Knowles.

 
 But what about the film itself? Alas, it has pretty much no interest in faith or spirituality, not even of the watered-down Hollywood kind that we see in movies like Bruce Almighty; and rather than find some sort of harmony between secular and sacred forms of music, the film ultimately reduces praise and worship to just another form of entertainment.

 
 In addition, the film, directed by Jonathan Lynn (Yes Minister, Nuns on the Run) from a script by TV scribes Elizabeth Hunter (ER) and Saladin K. Patterson (Frasier), is awfully contrived and mostly unfunny.

 
 Cuba Gooding Jr., still trying hard after all these years to prove that the Oscar he won for his role in Jerry Maguire was no fluke, plays Darrin Hill, a compulsive liar who loses his job in (what else?) advertising when his boss discovers he fibbed about his credentials. In the midst of his financial woes, Darrin hears that his aunt his died, and that she has left him a substantial inheritance -- but in order to collect it, he must go down to Georgia and lead her church's choir to victory at a gospel competition.

 
 And so he goes, lying all the way. Darrin tries to woo people into the choir by claiming to be a big-shot music producer who can give them a shot at fame and fortune once the competition is over; amazingly, several people fall for this, one of them apparently even promising to sleep with him for a chance at the big time. But Darrin has no idea how to direct the choir, so he gets help from Lilly (Knowles), a sultry nightclub singer who is scorned by the church's resident legalist because she is a single mom. Of course, a perfunctory romance between Darrin and Lilly ensues.

 
 And speaking of legalists, there is nothing the movies like better than a judgmental believer who can be exposed in the end as the hypocrite she really is. The target of our ire here is Paulina Pritchett (LaTanya Richardson), the pastor's bossy sister and a staunch enforcer of church rules.

 
 Most of the time, the reverend (Wendell Pierce) meekly gives in to his sister's demands; when she says all choir members must be baptized, he goes and baptizes all the people who have been brought in from outside the church, including the prison convicts, apparently without bothering to see if any of them have expressed any sort of faith. But he gets his revenge in the end, when he reveals that Paulina is not a widow but is, in fact, a divorcee whose ex-husband has found himself a hotter, sexier wife.

 
 And that's pretty typical of the film's overall tone. In its own way, The Fighting Temptations affirms the notion that we are all sinners, but rather than suggest ways to overcome that, it just asks us to wink at the sin and possibly even revel in it a little; people are welcomed into the church not to have a life-transforming experience, but to make church more entertaining. And perhaps that's to be expected when a church choir's whole raison d'etre is not worship, but winning a talent contest.

 
 Granted, some of the songs in this film are pretty good, but just as the choir should be about more than the music, so too should the film. Likewise, as moviegoers, our Christian witness should be about more than proving our demographic clout. It is gratifying, I guess, to see that we are now a desirable niche audience, but we should be asking a whole lot more from the films we see than what The Fighting Temptations offers, regardless of where these films may have been promoted."
"2319","The Fighting Temptations

 
 HOLLYWOOD studios usually don't pay much attention to Christian audiences, so it's always a little flattering when one of them tries to curry our favour. But to judge from some of the films they send our way, they still don't understand us very well. The latest case in point: The Fighting Temptations.

 
 Paramount Pictures seems to think it has a winner in this film, a comedy about a down-on-his-luck hustler who finds some sort of redemption while directing a black gospel choir, so the studio has bought ads in Christian newspapers and shown the film at a Christian trade convention. The film features Christian rapper T-Bone in a small role, and Relevant, a Christian pop-culture magazine, recently gave the film front-cover treatment by profiling the faith of co-star Beyonce Knowles.

 
 But what about the film itself? Alas, it has pretty much no interest in faith or spirituality, not even of the watered-down Hollywood kind that we see in movies like Bruce Almighty; and rather than find some sort of harmony between secular and sacred forms of music, the film ultimately reduces praise and worship to just another form of entertainment.

 
 In addition, the film, directed by Jonathan Lynn (Yes Minister, Nuns on the Run) from a script by TV scribes Elizabeth Hunter (ER) and Saladin K. Patterson (Frasier), is awfully contrived and mostly unfunny.

 
 Cuba Gooding Jr., still trying hard after all these years to prove that the Oscar he won for his role in Jerry Maguire was no fluke, plays Darrin Hill, a compulsive liar who loses his job in (what else?) advertising when his boss discovers he fibbed about his credentials. In the midst of his financial woes, Darrin hears that his aunt his died, and that she has left him a substantial inheritance -- but in order to collect it, he must go down to Georgia and lead her church's choir to victory at a gospel competition.

 
 And so he goes, lying all the way. Darrin tries to woo people into the choir by claiming to be a big-shot music producer who can give them a shot at fame and fortune once the competition is over; amazingly, several people fall for this, one of them apparently even promising to sleep with him for a chance at the big time. But Darrin has no idea how to direct the choir, so he gets help from Lilly (Knowles), a sultry nightclub singer who is scorned by the church's resident legalist because she is a single mom. Of course, a perfunctory romance between Darrin and Lilly ensues.

 
 And speaking of legalists, there is nothing the movies like better than a judgmental believer who can be exposed in the end as the hypocrite she really is. The target of our ire here is Paulina Pritchett (LaTanya Richardson), the pastor's bossy sister and a staunch enforcer of church rules.

 
 Most of the time, the reverend (Wendell Pierce) meekly gives in to his sister's demands; when she says all choir members must be baptized, he goes and baptizes all the people who have been brought in from outside the church, including the prison convicts, apparently without bothering to see if any of them have expressed any sort of faith. But he gets his revenge in the end, when he reveals that Paulina is not a widow but is, in fact, a divorcee whose ex-husband has found himself a hotter, sexier wife.

 
 And that's pretty typical of the film's overall tone. In its own way, The Fighting Temptations affirms the notion that we are all sinners, but rather than suggest ways to overcome that, it just asks us to wink at the sin and possibly even revel in it a little; people are welcomed into the church not to have a life-transforming experience, but to make church more entertaining. And perhaps that's to be expected when a church choir's whole raison d'etre is not worship, but winning a talent contest.

 
 Granted, some of the songs in this film are pretty good, but just as the choir should be about more than the music, so too should the film. Likewise, as moviegoers, our Christian witness should be about more than proving our demographic clout. It is gratifying, I guess, to see that we are now a desirable niche audience, but we should be asking a whole lot more from the films we see than what The Fighting Temptations offers, regardless of where these films may have been promoted.

 
 -- Peter T. Chattaway"
"2319","HOLLYWOOD studios usually don't pay much attention to Christian audiences, so it's always a little flattering when one of them tries to curry our favour. But to judge from some of the films they send our way, they still don't understand us very well. The latest case in point: The Fighting Temptations.

 
 Paramount Pictures seems to think it has a winner in this film, a comedy about a down-on-his-luck hustler who finds some sort of redemption while directing a black gospel choir, so the studio has bought ads in Christian newspapers and shown the film at a Christian trade convention. The film features Christian rapper T-Bone in a small role, and Relevant, a Christian pop-culture magazine, recently gave the film front-cover treatment by profiling the faith of co-star Beyonce Knowles.

 
 But what about the film itself? Alas, it has pretty much no interest in faith or spirituality, not even of the watered-down Hollywood kind that we see in movies like Bruce Almighty; and rather than find some sort of harmony between secular and sacred forms of music, the film ultimately reduces praise and worship to just another form of entertainment.

 
 In addition, the film, directed by Jonathan Lynn (Yes Minister, Nuns on the Run) from a script by TV scribes Elizabeth Hunter (ER) and Saladin K. Patterson (Frasier), is awfully contrived and mostly unfunny.

 
 Cuba Gooding Jr., still trying hard after all these years to prove that the Oscar he won for his role in Jerry Maguire was no fluke, plays Darrin Hill, a compulsive liar who loses his job in (what else?) advertising when his boss discovers he fibbed about his credentials. In the midst of his financial woes, Darrin hears that his aunt his died, and that she has left him a substantial inheritance -- but in order to collect it, he must go down to Georgia and lead her church's choir to victory at a gospel competition.

 
 And so he goes, lying all the way. Darrin tries to woo people into the choir by claiming to be a big-shot music producer who can give them a shot at fame and fortune once the competition is over; amazingly, several people fall for this, one of them apparently even promising to sleep with him for a chance at the big time. But Darrin has no idea how to direct the choir, so he gets help from Lilly (Knowles), a sultry nightclub singer who is scorned by the church's resident legalist because she is a single mom. Of course, a perfunctory romance between Darrin and Lilly ensues.

 
 And speaking of legalists, there is nothing the movies like better than a judgmental believer who can be exposed in the end as the hypocrite she really is. The target of our ire here is Paulina Pritchett (LaTanya Richardson), the pastor's bossy sister and a staunch enforcer of church rules.

 
 Most of the time, the reverend (Wendell Pierce) meekly gives in to his sister's demands; when she says all choir members must be baptized, he goes and baptizes all the people who have been brought in from outside the church, including the prison convicts, apparently without bothering to see if any of them have expressed any sort of faith. But he gets his revenge in the end, when he reveals that Paulina is not a widow but is, in fact, a divorcee whose ex-husband has found himself a hotter, sexier wife.

 
 And that's pretty typical of the film's overall tone. In its own way, The Fighting Temptations affirms the notion that we are all sinners, but rather than suggest ways to overcome that, it just asks us to wink at the sin and possibly even revel in it a little; people are welcomed into the church not to have a life-transforming experience, but to make church more entertaining. And perhaps that's to be expected when a church choir's whole raison d'etre is not worship, but winning a talent contest.

 
 Granted, some of the songs in this film are pretty good, but just as the choir should be about more than the music, so too should the film. Likewise, as moviegoers, our Christian witness should be about more than proving our demographic clout. It is gratifying, I guess, to see that we are now a desirable niche audience, but we should be asking a whole lot more from the films we see than what The Fighting Temptations offers, regardless of where these films may have been promoted.

 
 -- Peter T. Chattaway 

