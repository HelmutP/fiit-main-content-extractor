

 July 12, 2007 -- Mayor Bloomberg's announcement of a contract settlement with the NYPD's sergeants union marks a big breakthrough for the city - and the union - on several levels.  
  Most important, the 27.5 percent hike in pay after six years will provide a strong incentive for cops to aspire to the sergeant rank, pass the test - and stick with the city long after they do.  
  That will do a great deal to strengthen and stabilize the department over the long term.  
  The deal also sets a rough, but quite workable, template for a comparable settlement with the Patrolmen's Benevolent Association.  
  Lack of a PBA contract has strained the city's ties with cops and led to severe recruiting woes that have left the department dangerously understaffed.  
  It's also created hardship for folks who deserve better: New York's Finest, who put their lives on the line every day for the city.  
  (Think of Officer Russel Timoshenko, who was left fighting for his life, and Officer Herman Yan, who was lucky to escape with just wounds, after lowlife gunmen attacked them from ambush early Monday morning.)  
  The sergeants' contract's unusually long term - six years - is a big plus, too.  
  For one thing, it will lend some certainty to City Hall's out-year budget forecasts.  
  For another, it'll deprive 2009 mayoral candidates of a cheap way to buy union support with taxpayer dollars - which in all likelihood would have been the case if the deal were set to expire around election time.  
  Under the agreement, sergeants - a key cog in the city's chief anti-crime machine and the first level of NYPD management - will finally be able to make a decent living for themselves.  
  When the deal is fully phased in, veteran sergeants will be able to earn as much as $103,000 a year - plus overtime (hardly chicken feed).  
  New sergeants will get base starting pay of $73,000, beginning next year. That's up almost 20 percent from the $61,000 under the expired contract.  
  That big jump, by the way, corrects the foolish deal struck last time - in which rookie sergeants, like rookie cops, got ridiculously low salaries to offset higher pay for veterans.  
  The low pay has created huge recruiting, retention and mobility problems for the NYPD.  
  The city's challenge now is to forge a deal with the PBA - one that similarly addresses the pathetically low $25,100 starting pay for new cops.  
  The deal with the sergeants - who are every bit as tenacious as the cops at the bargaining table - shows that a comparable settlement with the PBA is entirely conceivable, though the sides are now in arbitration proceedings.  
  Meanwhile, hats off to Mayor Mike &amp; Co. - and to Ed Mullins, the sergeants union president.  
  Mullins, especially, showed a great deal of leadership in concluding the talks.  
  The upshot is very good news for all New Yorkers. 

