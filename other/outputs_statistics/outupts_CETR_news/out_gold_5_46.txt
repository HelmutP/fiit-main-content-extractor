

 CONCORD, New Hampshire (Reuters) - Democratic presidential contender Hillary Rodham Clinton on Saturday attacked President George W. Bush for ""arrogance and incompetence"" in Iraq but faced tough questions over her own vote to authorize the war. 

    


 On her first visit in a decade to the state that helps kick off the 2008 White House race, Clinton told voters in New Hampshire that Iraq was a challenge because of ""the arrogance and incompetence of our administration in Washington."" 

    


 At a town hall meeting of about 300 people in the city of Berlin, the New York senator was asked by one participant to repudiate her 2002 Senate vote for a measure that cleared the way for the March 2003 invasion. 

    


 ""Knowing what we know now, I would never have voted for it,"" she responded. ""I gave him the authority to send inspectors back in to determine the truth. I said this is not a vote to authorize pre-emptive war."" 

    


 Later at a high school gym packed with about 3,000 people in the state capital, Concord, she was asked if she wanted to ""have it both ways"" by calling for the war's end after voting for the measure five years ago. 

    


 ""I do not believe that most of us who voted to give the president authority thought he would so misuse the authority we gave him,"" she replied. ""He said he was going to the United Nations to put inspectors in. He did, but then he didn't let them complete their mission and he rushed to war."" 

    


 Clinton, an opponent of Bush's plan to send 21,500 more U.S. troops to Iraq, is the early Democratic front-runner and received enthusiastic applause and standing ovations in both New Hampshire cities.   

