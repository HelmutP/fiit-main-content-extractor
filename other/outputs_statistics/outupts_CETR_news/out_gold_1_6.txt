

 By MIKE WENDLAND 
				
 Convergence Editor and Technology Columnist 

				 


Is black the new green?  There's a growing online movement to get Internet developers to design their Web sites with black backgrounds instead of white or light colors. The rationale is that a black background with white or contrasting text uses markedly less energy than conventional black text on white background designs.  But is it true? Yeah. Sort of. Sometimes, with some monitors. And therein lies a heated controversy. 
 


At the heart of the dark Web movement is an Australian-based site called Blackle, which quotes estimates by a computer energy-savings expert named Mark Ontkush that if Google was designed to have a a predominately black background it would save 750 million watt hours of energy a year. 
 
 
Blackle has no connection to Google. It just displays searches from Google in gray text on an all-black screen. 
 

Since Blackle went online a few months ago, a host of similar sites have cropped up, all claiming that the black backgrounds they use require less energy on the older CRT (Cathode Ray Tube) monitors that are still in widespread use.

No one is exactly sure how many of those old CRT units are still around, though estimates range that they make up 20% to 25% of all the monitors in use in the world. 
 

Most newer monitors today are LCD (Liquid Crystal Display) flat screens and are much more energy efficient. But because they use a backlight that is always on, it makes little difference what color the screen is. 
 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


Yet, even so, proponents of the dark Web say that every little step helps.

""We believe that there is value in the concept because even if the energy savings are small, they all add up,"" says a statement on the Blackle site.

 ""Secondly we feel that seeing Blackle every time we load our Web browser reminds us that we need to keep taking small steps to save energy.""

Not so fast, says the real Google, which has now come out against the idea. 

Bill Weihl, Google's Green Energy Czar, says that while CRT monitors indeed are energy hogs and a black background may slightly help to reduce energy consumption, the problem is in the other 75% of monitors that are not CRTs.

""We applaud the spirit of the idea, but our own analysis as well as that of others shows that making the Google homepage black will not reduce energy consumption,"" says Weihl on the official Google blog.

""To the contrary, on flat-panel monitors (already estimated to be 75% of the market), displaying black may actually increase energy usage."" 
 

Google also cites a study that shows the amount of energy saved by a black background on CRT screens is only about half as much as Blackle claims.  
 

But tell that to the Net's greenies. 
 

There are now nearly a dozen other search sites with black screens, called such things as Earthle, Trek Black, BlackWebSearch and Greenergle. There are also black screen versions of the Google site in German and Spanish. 
 

Ontkush, the environentalist who unleashed all this controversy with his first post back in January, said he picked Google to illustrate his theory only because it gets so much traffic.  Other sites could benefit from a color adjustment, too, he suggests. And white text on a black background, which many people say hurts their eyes and is hard to read, may not be the best combination after all. 
 

""The white on black palette was probably just adopted from the paper printing world,"" wrote the Boston-based Ontkush in a Q&amp;A on his http://ecoiron.blogspot.com ecoIron blog.

 ""There have been several studies on what the best color scheme is. I've heard white on gray, green on yellow, white on green, green on black, amber on black, and white on black. I think the jury is still out."" 
 

While the Net seems divided on just how useful all this color tinkering may be, there are some practical steps that consumers could take with their own systems that really do save energy. Here's some suggestions: 
 
?             Turn down the brightness of your monitor. The brighter it is, the more energy it uses.

?	Adjust the power settings of your computer to shut down the hard drive or turn off the monitor when in not in use after a prolonged period of inactivity. 

?	Turn the whole system off at night. Don't forget to turn off your printer, too. 
 
?	Unplug your battery charger when your laptop is fully charged. These chargers continue to draw power when plugged in, even though the battery is topped off.

Finally, if you do want to make the on-screen background dark, you can do so yourself. From the Tools settings on the popular Web browsers Internet Explorer and Firefox, there are options to change the font and page background colors, thus letting you make the whole Web as dark... or green... as you want. 

