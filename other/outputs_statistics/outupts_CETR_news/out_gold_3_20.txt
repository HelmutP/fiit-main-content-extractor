

 By MARC BERMAN
	
		

	
		 
 
	                 
		 
			WILTING ROSE? It is becoming increasingly clear that Jalen Rose is not in Isiah Thomas' plans for his 10-man rotation."" WIDTH=""223"" alt=""""&gt;
		  		
		 
			Loading new images...
		 
		 

	
 
             

 
             
                        October 16, 2006 -- The shrinking role of Jalen Rose is among the most sensitive issues Knicks president/coach Isiah Thomas must navigate as he tries to return harmony to a fractured locker room.
 
 It is becoming increasingly clear Rose is not in Thomas' plans for his 10-man rotation, unless camp standout Quentin Richardson is traded.
 
 Another possibility, if Rose becomes disgruntled, is the team reaching a buyout on the $16.9 million left on his expiring contract. However, Rose's pact is a pawn at the trade deadline, even though Thomas said he won't trade Rose because the club is no longer in adding-to-the-payroll mode.
 
 It was hardly an endorsement for Rose when Thomas responded Saturday night after the Knicks' last-second victory over the Sixers to a question Rose's role. In what looked like an obligatory audition, Rose started, played the first nine minutes, did not take a shot and was benched the rest of the game. Rookie Mardy Collins started the second half at small forward.
 
 "You really do need a 15-man roster," Thomas said. "At some point in time every single guy on the roster will have his chance to contribute. It's a team game, not an individual game. The most important thing is at the end of the game you hear the music going and all the guys laughing and joking, going on the plane to the next city and trying to put on a good show."
 
 Will Rose be laughing and joking after a series of DNPs? He's got a huge ego, and was insulted at questions in Charleston about possibly not playing.
 
 Rose did not dress in Friday's preseason opener, with a team official saying Thomas was saving him for a Philly start. "I'm an elder statesman on this team," Rose said. "My role on this team is to be a solid contributor, fit in and win games. Do whatever Isiah asks me to do. It's not up to Jalen to say it's preseason, I need 40 minutes."
 
 Thomas put a happy face on Rose's benching. 

