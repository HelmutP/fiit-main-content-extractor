
 This week's column is a collection of odds and ends from a tech reporter's inbox. Let's start with the odd. 
 
 Everything iPod 
 
The iPod continues to push our culture forward in significant ways. No longer is bringing reading material to the bathroom enough; now you can have a soundtrack. 
 
 Just pop your iPod into this dock built in to the toilet-paper holder. It comes with moisture-free speakers but is powered by batteries. So don't spend too much time lost in reverie. 
 
 The iCarta Stereo Dock with Bath Tissue Holder from Atech Flash is available at Amazon.com for $80. 
 
 You'll find this must-have accessory listed with other items that customers have tagged ""Amazon Oddities."" 
 
 Another oddity: ""Knitting With Dog Hair: Better A Sweater From A Dog You Know and Love Than From A Sheep You'll Never Meet"" (St. Martin's Griffin, 1997). At $12, the book is a bargain for lovers of dogs that shed. 
 
 (Tags are chic on the Web these days. You tag items with keywords, such as ""birthday,"" ""Chicago"" or ""oddities,"" to make content easier to organize and search. More interesting, it is the users who put tags on content, not the site operators.) 
 
 Fan power 
 
 Maybe you can spin a basketball on a finger, but can you spin how people view your game? 
 
 That's what the National Basketball Association is attempting with its deal to create a channel on YouTube.com for highlights fans like to post. 
 
 Remember when a young MJ flew through the air with hair? You can see such clips again and again on YouTube. Instead of suing YouTube for copyright violations, the NBA is trying to get a grip on content that already has spun out of its control. 
 
 As of this writing, there were about 35 videos on the NBA channel. A search of the term NBA highlights yielded more than 2,300 videos on the rest of YouTube. 
 
 Another effort from the league also taps into what's hot on the Web these days: social networking. At www.nba.com/fanvoice, fans can create their own page, include their favorite highlight clips, post pictures from games--including an eyeful of Eva Longoria attending San Antonio Spurs games--and engage in the usual bashing of other fans. 
 
 But in its beta format, Fan Voice doesn't look like a winner. 
 
 Why? On the second page of member profiles I scrolled through last week, three of the 12 members were writers for NBA.com. On the third page, I found two more writers, and on the fourth page of profiles the member ID's looked like this: NBA(underscore)Admin4, NBA(underscore)Admin6 and so forth. 
 
 Let's hear it for the people. 
 
 Snap judgment 
 
 With the elusive goal of getting people to make prints from camera phones, Walgreen Co. is taking an interesting, albeit not complete, step. 
 
 Imaging from mobile phones is improving. Several high-end phones offer 3.2 megapixels of resolution, with 2 megapixels making headway into many low- to mid-priced phones. 
 
 That doesn't compete with the 6-plus megapixels popping up on new digital cameras, but even 2 megapixels is enough for a passable print. Indeed, I've printed several nice images from phones, and I've even included some in photo books I've made through Shutterfly.com. 
 
 But most people don't bother getting images off a camera phone. One reason: It's not that easy. 
 
 Yes, you can plug some camera phones into a printer to make prints. Or some models allow you to send images from a phone via Bluetooth to a printer. 
 
 But what if you could walk into your local Walgreens and pick up those pictures an hour after shooting them, much like with images from a digital camera? 
 
 Now you can. 
 
 With the Pictavision Teleprints (www.pictavision.com) software application downloaded onto your phone, you can send camera images to a nearby Walgreens via the Web and then pick them up an hour later. Or have them mailed to your home. 
 
 But you can't walk into a Walgreens and take out a phone's memory card and pop it into a print machine. 
 
 That could come soon, as the Deerfield-based drugstore chain explores how to make prints from whatever gadget customers bring in to stores, a spokeswoman said. 
 
 For the moment, Walgreens is furthering its efforts to offer online photo services so prints can be picked up at any of its stores across the country. 
 
 The sell is convenience. 
 
 Have some great shots from your kid's birthday party, but Grandpa couldn't make it in from Florida? Send the prints to the Walgreens by his house. 
 
 There is risk for Walgreens. 
 
 The New Jersey firm providing the camera phone service, Exclaim, also has deals with many online photo sites, including Flickr.com, Dotphoto.com and Kodak (Kodakgallery.com). 
 
 You can print from these sites, too, and, based on my cursory scan of pricing, you'll pay more at Walgreens--19 cents a print compared with 12 cents from Dotphoto, for instance. 
 
 Hence, it could be a tough sell for Walgreens because it's a ""me too"" product in a market full of Web-savvy consumers. The only reason to choose Walgreens would be if you need those prints in an hour. If they can show up at Grandpa's house two days later, you'll save money elsewhere. 
 
 But it's a step closer, at least. 
