

 PC maker Lenovo Group Ltd. on Monday left open the possibility of a recall stemming from a smoking laptop that sent sparks flying at Los Angeles International Airport.
 
 
Lenovo is investigating the Sept. 16 incident that involved a T43 Thinkpad containing the same Sony battery that was the center of recalls last month by Apple Computer and Dell. Sony was assisting in the investigation of the latest incident.
 
 
The cause of the laptop overheating had not been determined, so Lenovo had not decided whether a recall would be necessary. Nevertheless, the possibility was left open.
 
 
""We'll do whatever is in the best interest of our customers and public safety,"" Lenovo spokesman Ray Gorman said.
 
 
The Thinkpad contained the same Sony battery  that prompted Apple and Dell to recall millions of batteries. The recalls followed reports of the batteries overheating and sometimes bursting into flames. Dell's recall of 4.1 million batteries was the largest consumer electronics recall in history. Apple recalled 1.8 million batteries.
 
 
In the Lenovo case, the notebook owner noticed the laptop smoking while boarding an aircraft, Gorman said. He then returned to the terminal, where the computer started to spark. There was no indication that the laptop had been subjected to any unusual conditions.
 
 
In the Dell and Apple recalls, Sony acknowledged that a defect in the battery could present a fire hazard. 

