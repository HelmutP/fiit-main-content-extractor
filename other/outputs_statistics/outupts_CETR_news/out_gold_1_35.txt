

 By BOB CHRISTIE 
				
 Associated Press Writer 

				 


PHOENIX -- Deputies searching the home of rapper DMX during an investigation into claims of animal cruelty found about a half-pound of suspected illegal narcotics, the Maricopa County sheriff said Saturday. 
 


No charges have been filed or arrests made. Sheriff Joe Arpaio said the investigation into alleged animal cruelty was ongoing, and the suspected drugs were being tested to confirm their content. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


Friday's search was prompted by reports that pit bulls kept by the rapper at the home in rural north Phoenix were not being given enough food or water. A dozen pit bulls were seized, the bodies of three dogs were dug up in the yard and a variety of assault-style weapons were taken from the home, Sheriff Joe Arpaio said.  DMX, whose real name is Earl Simmons, was not at home and his lawyer said he hasn't been there for two months.  Arpaio said there was no indication the dogs were used in fights.  Reached Saturday, attorney Murray Richman said he had no knowledge of any narcotics. The dogs were looked after by a caretaker and ""all sorts of people"" had been staying at the home, Richman said.  ""Why doesn't the sheriff call me?"" Richman said. ""I spoke to him yesterday -- and rather than make these allegations in the press why doesn't he call me?""  Arpaio said witnesses placed the 36-year-old rapper at the home within the past three weeks, but Richman insisted that he has not been there for 2 months.  The weapons are being checked to see if they are legal, the sheriff said. Richman said Simmons has no felony convictions that would prohibit him from owning weapons.  In 2002, Simmons pleaded guilty to animal cruelty, disorderly conduct and possession of drug paraphernalia in New Jersey. Police said they found pipes for smoking crack cocaine, a pistol and 13 pit bulls at his home in 1999.  His albums include ""It's Dark and Hell Is Hot,"" ""Flesh of My Flesh, Blood of My Blood"" and ""Year of the Dog ... Again.""
