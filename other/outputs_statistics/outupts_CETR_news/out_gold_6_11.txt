

 In Julio Lugo we trust.							My fantasy team, once in last place, has begun to make its move. Ryan Howard's return to elite status has helped, as has Carlos Zambrano's. A shrewd trade or two -- including getting Howard when he was hitting under .200 and picking up Raul Ibanez for Vladimir Guerrero -- has given me hope. To take that final step, however, I'll need an additional boost. That's where Lugo comes in.

 
 

Lugo is one of a handful of players who have been tossed onto the waiver wire by frustrated owners tired of waiting for them to produce. Lugo has been languishing there, along with Pat Burrell and Stephen Drew. It's hard to blame owners for dumping these underperforming players, but I'll do it anyway. Unless one thinks that Burrell and Lugo are done or that Drew was all hype, these are exactly the kinds of players you don't want to drop. The law of averages tell us that what goes down must come up -- and vice versa. So a .200-hitting Lugo, so long as he keeps his job, is likely to hit much higher than that the rest of the way. 
 

Do players' skills erode? Sure. But in most cases, guys who can hit eventually will find their stroke if given the chance to continue playing. Burrell and Lugo seem to be getting that opportunity. Can Burrell produce better than Casey Blake the rest of the way? I think he can. Can a resurgent Lugo do more than Kaz Matsui in the final two months? I'm guessing yes. 
 
 
 Waiver-wire gems 

Julio Lugo, SS, Red Sox: I've never been a huge fan of his because I think of him as one of those guys who puts up numbers beyond his talent level. That said, when I see a guy with 20-plus steals available for the taking, I jump at the opportunity. Lugo was hitting below .200 through June, and owners decided to try players such as Troy Tulowitzki or Jason Bartlett in his place. Benching Lugo was understandable, but dumping him was a big mistake. Lugo is hitting nearly .400 with five steals and 13 RBI in July and is likely to keep producing in the final two months. It might be too late to grab him up, but check your waiver wire to see if he's available. 

Pat Burrell, OF, Phillies: Are there holes in Burrell's swing? Absolutely. Can he be pitched to? Sure. But is he better than a .200 hitter with a handful of home runs? You better believe it. Owners got spooked when Burrell was benched for a handful of games in late June, back when he was hitting about .210 with seven homers. You don't drop a guy like this unless you're sure he's finished. He isn't. He's hitting a cool .395 with four homers and 13 RBI in July, and he looks like a sure bet to surpass 20 homers for the seventh consecutive season. Enjoy the power surge, faithful owners who swiped him in early July. 
 
 
 Waiver-wire lemons 

Austin Kearns, OF, Nationals: When a guy goes more than 50 games without a homer, you have to wonder whether he ever will regain his stroke. He finally hit one July 22, but Kearns hasn't exactly set the world on fire after a promising rookie season in 2002, when he hit .315 with 13 homers. The fact is, he never has driven in 90 runs or hit more than 24 homers. That makes him a below-average slugger on a team with little protection in the lineup, should he suddenly start hitting. Pick up promising young slugger Matt Kemp of the Dodgers instead and let some other owner wait for Kearns to start hitting. 

Marcus Giles, 2B, Padres: The younger of the Giles brothers has had a disappointing season, but let's be real for a moment: What were owners expecting? Giles really made a name for himself with one strong fantasy season in 2003 (.316 average, 21 homers, 69 RBI, 14 steals). Since then, he has been an average middle infielder in terms of fantasy production. He was far off my radar in drafts this year because he was moving to San Diego, a tough park for hitters. He's hitting .240 with four homers and eight steals. Even if he straightens it out, we're talking about a .260-10-60 guy. Don't you have better things to do? As a fantasy owner, you sure do. 
 
 
 Quick hits 

Forget the lackluster home-run total; Yankees outfielder Bobby Abreu has absolutely won back the trust of fantasy owners with 22 RBI in the first 19 games of July. That's as many RBI as he had in May and June combined. ... No one has been better in the last month than Giants right-hander Tim Lincecum. In his last five starts, the rookie is 3-0 with a 1.05 ERA and 41 strikeouts in 34 1/3 innings. Yes, he's that good. ... It's unclear when Rockies closer Brian Fuentes will return from the disabled list, but Manny Corpas is clearly a better-than-average replacement. Since he was put in the closer role, Corpas has allowed only two hits in eight innings and twice has struck out the side in the ninth. He is 5-for-5 in save chances and is worth a flier.
