

 Several of the Cubs' clubhouse TVs were tuned to the final innings of the Milwaukee Brewers' game Sunday against the Cincinnati Reds, with the interest peaking when reliever David Weathers faced Prince Fielder for the final out of the Reds' 7-6 victory. 
 

That added a half-game to the Cubs' lead in the National League Central, putting them a full game ahead of the Brewers before their Sunday night game against the St. Louis Cardinals, which was postponed in the bottom of the third inning. 
 

But a few Cubs were watching another game as Minnesota Twins ace Johan Santana struck out 17 Texas Rangers in eight innings. 
 

One of them was Kerry Wood, the last pitcher to have 20 strikeouts in a game, in 1998. 
 

''Woody was excited and pumping his fist,'' pitcher Scott Eyre said. ''He wanted him to get to 20.'' 
 

Santana was lifted in the ninth for Joe Nathan, who finished the game. 
 

Wood is a relief pitcher now and getting comfortable in that role, even as manager Lou Piniella is finding out how to best employ the former starter. 
 

''Woody's been [warming] up the last few days, and I don't usually do that and not use them,'' Piniella said of bypassing Wood on Friday and Saturday. ''I usually apologize to the pitcher in that case.'' 
 

Piniella has said Wood needs a longer time to warm up, but Wood said that's no longer the case. 
 

''I've been ready after 10 or 12 pitches,'' he said Sunday. 
 

Piniella was working off an experience in Denver a week ago when Ted Lilly was reaching a high pitch count against the Colorado Rockies. Wood was told to warm up in the fifth, but the inning ended quickly with a double play. That didn't give him enough time to get loose, so Lilly pitched the sixth inning, as well. 
 

But Wood said he has been able to get loose more quickly since then. 
 

Piniella said he would have used Wood on Sunday night if necessary. 

