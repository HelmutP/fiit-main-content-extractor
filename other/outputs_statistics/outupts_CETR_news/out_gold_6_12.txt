

 RAMAPO, N.Y. -- The thieves who kicked in the door of an empty house last month weren't after any ordinary valuables -- they wanted the 19th-century home's decorative wooden mantelpieces. 
 The mantel bandits made off with seven of the valuable fireplace frames, each worth at least $1,000, police said Friday. 
 ''This definitely was organized,'' Ramapo police Detective Keith Schwartz said. 
 The mantels, crafted between 1810 and 1820, were taken on July 19 or 20 from a property known as the Smith House, which is part of a large estate. Developer Ramapo Land Co. plans to build luxury housing in the area but preserve the Smith House, President Jack O'Keeffe said. 
 Ramapo town historian Craig Long suggested the mantel thieves might have mistakenly thought they were saving the pieces from potential demolition. 
 ''Hopefully, they didn't do this for economic gain and will return the mantels,'' he said. 
 The mantels vanished about three months after another historical heist -- the theft of an antique U.S. atlas from a Rockland County Historical Society building in New City. The 1823 book was recovered, and a former curator was charged with larceny in May. She has pleaded not guilty. 

