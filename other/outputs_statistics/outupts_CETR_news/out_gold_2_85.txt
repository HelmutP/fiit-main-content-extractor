

 "EDUCATION: Tasmanians celebrate first certificates 
						
							By Rosalea Ryan, Australasian Flowers
							 
							Edition 234, 27/10/2006 2:47:13 PM
						
				
				 

					
						
							
								
									
										
											
																						
											
										
										
											
												
		
										
									
								
							
							
								
								
							
							
								
									
										
											
												
											
										
										
											
												
		
										
									
								
							
						
						TASMANIA?S first wave of floristry graduations took place in Melbourne recently when six students from the Apple Isle received certificates from Marjorie Milner College, Surrey Hills. College principal, Greg Milner, congratulated the students on being part of the first group from their state to complete Certificate II or III in Floristry. 
 The college began operating in Tasmania in autumn 2005 with block-teaching sessions in Launceston and, later, Hobart. 
 Previously, students from Tasmania were given government assistance to fly to the mainland for training in the Victorian education system. 
 ?There are 18 apprentices across Tasmania in training at present,?? Milner said. 
 ?Previously the highest number at any one time was five.?? 
 In all, 28 students received their Certificate II in Floristry on the night: Sandra Buckles, Jocelyn Campbell, Connie Comport, Maria Fasano, Teresa Goodall, Kerryn Ineson, Carly Jenner, Kylie Kay, Sally Lees, Elizabeth Leversha, Amanda Lidgerwood, Bethany Morgan, Emma Mortlock, Antony Reid, Jeanette Richardson, Ashlee Rogerson, Veronica Strangis, Karen Stronell, Cheryl Sutcliffe, Ceni Tasci, Karly Whitford and Melinda Whitham of Victoria and Tasmanians Emily Flaherty, Jacqueline Streets, Heidi Webber, Caroline Wisby, Kathleen Wisby and Kylie Woodhouse. 
 Svetland Abramova, Courtney Barton, Teresa Goodall, Kerryn Ineson, Sally Lees, Andrea May, Amanda McLaren, Debbie Plunkett, Antony Reid, Kyle Robertson, Karen Stronell and Melinda Whitham were awarded Certificate II in Retail Operations. 
 Sixteen students were due to graduate from the Certificate III in Floristry course but a 17th name was added to the list after Sally Lees completed her workload on the day of the ceremony. 
 She joined Svetlana Abramova, Sally Campbell, Danielle Ferguson, Emily Flaherty, Vicki Halabarec, Steffanie Harito, Christina Kidman, Debbie Maselli, Debbie Plunkett, Lorraine Reeves, Kyle Robertson, Brooke Robinson, Lauren Standfield, Wilhelmina Watson, Melinda Whitham and Caroline Wisby in receiving her certificate. 
 Certificate III in Retail Supervision was awarded to Kyle Robertson. Irene Brockwell, Phuong Le and Lauren Peck received their Certificate IV in Floristry. 
 Courtney Barton?s win in the WorldSkills Australia final in April was among the year?s highlights for the college, Milner said. 
 ?Courtney had two perfect scores with two of her items,?? Milner said. 
 ?In the past three WorldSkills this college has had two firsts, a second and a fourth.?? 
 Milner praised the achievements of students in competitions in Melbourne in the past 12 months and thanked their families and employers for encouraging the students to take part. 
 ?Without their empoyers? continuing support these apprentices would not be able to compete,?? he said. 
 Other accomplishments in recent months included first placings to Sarah Jenner of Kenney?s Flowers, Warragul, Victoria, and Natalie Tyson of Style By Nature, Port Melbourne, Victoria, in the intermediate and advanced sections, respectively, at this year?s Melbourne International Flower and Garden Show, and Hannah Symons? win in the Interflora Florist of the Future (Victoria/Tasmania) competition. Symons is apprenticed at Buds In Bloom in Traralgon, Victoria. 
 Encouragement awards recognising enthusiasm for training, participation in competitions and potential in floristry were presented to Kylie Kay and Emily Flaherty.  
 he college prize for most outstanding first-year apprentice went to Sarah Jenner, while Courtney Barton, of Stonleigh Flowers, Wodonga, Victoria, and Natalie Tyson tied for the second-year award. 
 Vicki Halabarec, of Nikolina?s Florist in Leongatha, Victoria, was named most outstanding student and top third-year apprentice. 
 Halabarec joined Marjorie Milner teacher, five-time Interflora Designer of the Year and past Interflora World Cup competitor Lyndell Parker, and Phillip Roberts, a national demonstrator, teacher and director of In Full Bloom, South Melbourne, to demonstrate a range of floristry techniques."
"4089","
							By Rosalea Ryan, Australasian Flowers
							 
							Edition 234, 27/10/2006 2:47:13 PM
						 

				
				
					
						
							
								
									
										
											
																						
											
										
										
											
												
		
										
									
								
							
							
								
								
							
							
								
									
										
											
												
											
										
										
											
												
		
										
									
								
							
						
						TASMANIA?S first wave of floristry graduations took place in Melbourne recently when six students from the Apple Isle received certificates from Marjorie Milner College, Surrey Hills. College principal, Greg Milner, congratulated the students on being part of the first group from their state to complete Certificate II or III in Floristry. 
 The college began operating in Tasmania in autumn 2005 with block-teaching sessions in Launceston and, later, Hobart. 
 Previously, students from Tasmania were given government assistance to fly to the mainland for training in the Victorian education system. 
 ?There are 18 apprentices across Tasmania in training at present,?? Milner said. 
 ?Previously the highest number at any one time was five.?? 
 In all, 28 students received their Certificate II in Floristry on the night: Sandra Buckles, Jocelyn Campbell, Connie Comport, Maria Fasano, Teresa Goodall, Kerryn Ineson, Carly Jenner, Kylie Kay, Sally Lees, Elizabeth Leversha, Amanda Lidgerwood, Bethany Morgan, Emma Mortlock, Antony Reid, Jeanette Richardson, Ashlee Rogerson, Veronica Strangis, Karen Stronell, Cheryl Sutcliffe, Ceni Tasci, Karly Whitford and Melinda Whitham of Victoria and Tasmanians Emily Flaherty, Jacqueline Streets, Heidi Webber, Caroline Wisby, Kathleen Wisby and Kylie Woodhouse. 
 Svetland Abramova, Courtney Barton, Teresa Goodall, Kerryn Ineson, Sally Lees, Andrea May, Amanda McLaren, Debbie Plunkett, Antony Reid, Kyle Robertson, Karen Stronell and Melinda Whitham were awarded Certificate II in Retail Operations. 
 Sixteen students were due to graduate from the Certificate III in Floristry course but a 17th name was added to the list after Sally Lees completed her workload on the day of the ceremony. 
 She joined Svetlana Abramova, Sally Campbell, Danielle Ferguson, Emily Flaherty, Vicki Halabarec, Steffanie Harito, Christina Kidman, Debbie Maselli, Debbie Plunkett, Lorraine Reeves, Kyle Robertson, Brooke Robinson, Lauren Standfield, Wilhelmina Watson, Melinda Whitham and Caroline Wisby in receiving her certificate. 
 Certificate III in Retail Supervision was awarded to Kyle Robertson. Irene Brockwell, Phuong Le and Lauren Peck received their Certificate IV in Floristry. 
 Courtney Barton?s win in the WorldSkills Australia final in April was among the year?s highlights for the college, Milner said. 
 ?Courtney had two perfect scores with two of her items,?? Milner said. 
 ?In the past three WorldSkills this college has had two firsts, a second and a fourth.?? 
 Milner praised the achievements of students in competitions in Melbourne in the past 12 months and thanked their families and employers for encouraging the students to take part. 
 ?Without their empoyers? continuing support these apprentices would not be able to compete,?? he said. 
 Other accomplishments in recent months included first placings to Sarah Jenner of Kenney?s Flowers, Warragul, Victoria, and Natalie Tyson of Style By Nature, Port Melbourne, Victoria, in the intermediate and advanced sections, respectively, at this year?s Melbourne International Flower and Garden Show, and Hannah Symons? win in the Interflora Florist of the Future (Victoria/Tasmania) competition. Symons is apprenticed at Buds In Bloom in Traralgon, Victoria. 
 Encouragement awards recognising enthusiasm for training, participation in competitions and potential in floristry were presented to Kylie Kay and Emily Flaherty.  
 he college prize for most outstanding first-year apprentice went to Sarah Jenner, while Courtney Barton, of Stonleigh Flowers, Wodonga, Victoria, and Natalie Tyson tied for the second-year award. 
 Vicki Halabarec, of Nikolina?s Florist in Leongatha, Victoria, was named most outstanding student and top third-year apprentice. 
 Halabarec joined Marjorie Milner teacher, five-time Interflora Designer of the Year and past Interflora World Cup competitor Lyndell Parker, and Phillip Roberts, a national demonstrator, teacher and director of In Full Bloom, South Melbourne, to demonstrate a range of floristry techniques. 



