

 Three-quels save summer
				
				
				 
 August 19, 2007 
				
				 BY TERRY LAWSON 
				
 FREE PRESS COLUMNIST 

				 


It's all over but the clean-up, really, though it may take the EPA to oversee the removal of the toxic goo left by the big-budget waste dump that is ""The Invasion,"" the remake of the ""Invasion of the Body Snatchers"" that opened Friday. 
 


The last major studio picture of the summer season arrives Friday, and while it is an original title, ""The Nanny Diaries"" bears such a marked resemblance to ""The Devil Wears Prada"" that it is being marketed as its twin sister. That it happens to be well-made is almost an afterthought. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


The movie summer of 2007 may be most remembered for its ""three-quels,"" with the third installments of ""Shrek,"" ""Pirates of the Caribbean,"" Spider-Man"" ""Rush Hour"" and ""Ocean's 13."" All did only half their job -- luring customers, but failing to bring them back for repeat viewings. 


The sole three-peat that satisfied moviegoers and critics alike was ""The Bourne Ultimatum,"" which, like the ""Harry Potter"" series, pleased the faithful by getting stronger and better as it went along. 
 


As for the sequels, ""Hostel II,"" ""Daddy Day Care,"" ""28 Weeks Later"" and ""Evan Almighty"" -- they all belly-flopped. ""Fantastic Four: the Rise of the Silver Surfer"" and the resurrection of the ""Die Hard"" series made their $150 million and were quickly forgotten, although they did rake in enough overseas coin to make the studio dream of future installments. 
 


Meanwhile, audiences that had long been loyal to any CGI comedy with cute animals showed little appreciation for the tried and true formulaics of ""Surf's Up,"" far preferring the originality of ""Ratatouille"" and the hand-drawn crudeness of ""The Simpsons Movie"" over wiseacre penguins. 
 


The only special-effects-driven spectacular to truly entertain and accomplish the trick of making us go ""wow"" was, of all things, ""Transformers,"" directed by Michael Bay, whose name might still be synonymous with loud, empty and dumb if we didn't have Brett Ratner (""Rush Hour 3"") to kick around. 
 


And neither the timeliness nor star power of Angelina Jolie was enough to turn the excellent ""A Mighty Heart"" into what is now a contradiction in terms, a serious summer hit. As for the breakout independent film of the season, ""Waitress""? It just didn't happen. 
 


So it was an altogether sad season, right? 
 


Not for Hollywood. 
 


Not only did the R-rated comedies ""Knocked Up"" and ""Superbad"" generate profitable word-of-mouth buzz, but all those three-quels, boosted by ""Harry Potter,"" ""Die Hard"" and ""The Simpsons,"" made so much dough on their first-weekends that on paper, at least, this was Hollywood's hottest summer ever. 
 


According to movie biz bible ""Variety,"" the projected $3.35-billion take is more than $250 million ahead of the previous best of show, summer 2004. 
 


Try to look on the bright side: Maybe ""Spider-Man 4"" or ""Shrek 4"" will be an improvement, as opposed to a family obligation. And then, of course, there's always fall. 

