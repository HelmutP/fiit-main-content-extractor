

 //W3C//DTD HTML 4.01 Transitional//EN""&gt;

 
 Black to remain free on bond  :: CHICAGO SUN-TIMES :: Conrad Black on Trial






	
	
	













    Back to regular view     Print this page
 





 



 








 




Your local news source ::       Select a community or newspaper »
 


	
        
		
	 	
		



Select a community 
------------------
Algonquin
Alsip
Antioch 
Arlington Heights
Aurora
Bannockburn
Barrington 
Barrington Hills
Batavia
Beecher
Bellwood
Berkeley
Blue Island
Bolingbrook
Broadview 
Buffalo Grove
Burr Ridge
Calumet City
Cary 
Chesteron, Ind.
Chicago
Chicago - Albany Park
Chicago - Avondale
Chicago - Belmont Cragin
Chicago -  Bucktown
Chicago - Dunning
Chicago - Edgebrook
Chicago - Edgewater
Chicago - Edison Park
Chicago - Jefferson Park
Chicago - Harlem-Irving
Chicago - Lakeview
Chicago -  Logan Square
Chicago - News Star
Chicago - North Center
Chicago - North Town
Chicago - Norwood Park
Chicago - Portage Park
Chicago - Ravenswood
Chicago - Rogers Park
Chicago - Roscoe Village
Chicago - Sauganash
Chicago - Skyline News
Chicago -  Ukranian Village
Chicago - Uptown
Chicago -  Wicker Park
Chicago Heights
Clarendon Hills
Country Club Hills
Crete
Crestwood
Crown Point, Ind.
Darien
Deerfield
Deer Park
Des Plaines
Dolton
Downers Grove
Elburn
Elgin
Elk Grove
Elmhurst 
Elmwood Park
Evanston 
Flossmoor
Forest Park
Fox River Grove
Fox Valley
Frankfort
Franklin Park 
Gages Lake
Gary, Ind.
Geneva
Glen Ellyn
Glencoe 
Glenview 
Glenwood
Golf 
Grayslake
Green Oaks
Gurnee
Harwood Heights
Harvey
Hawthorn Woods
Hazel Crest
Highland Park
Highwood
Hillside
Hinsdale 
Hobart, Ind.
Hoffman Estates
Homer Glen
Homer Twp.
Homewood
Indian Head Park
Inverness 
Island Lake
Joliet
Kenliworth
Kildeer
La Grange
LaGrange Highlands
LaGrange Park
Lake Barrington 
Lake Bluff 
Lake Forest 
Lake in the Hills 
Lake Villa 
Lake Zurich
Lansing
Lemont
Libertyville
Lincolnshire
Lincolnwood
Lindenhurst 
Lisle
Lockport
Long Grove
Lowell, Ind.
Manhattan
Markham
Matteson
Maywood 
Melrose Park 
Merrillville, Ind.
Midlothian
Mokena
 Montgomery
Morton Grove
Mount Prospect 
Mundelein 
Naperville
New Lenox
Niles
Norridge
North Barrington 
Northbrook 
Northfield
Northlake
Northwest Ind.
Oak Brook 
Oakbrook Terrace 
Oak Forest
Oak Lawn
Oak Park
Oakwood Hills
Olympia Fields
Orland Hills
Orland Park

 Oswego 
Palatine 
Palos Heights
Palos Hills
Palos Park
Palos Twp.
Park Forest
Park Ridge 
Plainfield

Portage, Ind.
Prospect Heights 
Richton Park
River Forest
River Grove
Riverwoods
Rolling Meadows 
Rosemont
Round Lake
Sauk Village
Schaumburg 
Schererville, Ind.
Schiller Park 
Silver Lake
Skokie 
South Barrington 
South Elgin
South Holland
St. Charles
Steger
Stone Park 
Thornton Twp.
Tinley Park/Southland
Tinley Park
Tower Lake
University Park
Valparaiso, Ind.
Venetian Village 
Vernon Hills 
Wadsworth
Wauconda 
Waukegan
West Lake Co., Ind.
West Proviso
Westchester 
Western Springs
Wheaton
Wheeling
Wildwood
Willowbrook 
Wilmette
Winnetka
Worth
Worth Twp.

 Yorkville 







Select a STNG publication or site
  
---------------------------------------------------
 DAILY PUBLICATIONS 
---------------------------------------------------
	Chicago Sun-Times	
	The Beacon News	
	The Courier News	
	The Daily Southtown	
	The Herald News	
	Lake County News-Sun	
	The Naperville Sun	
	Post-Tribune	


  
---------------------------------------------------
 SEARCH CHICAGO 
---------------------------------------------------
Autos
Homes
Jobs


  
---------------------------------------------------
 NEIGHBORHOOD CIRCLE 
---------------------------------------------------
 Montgomery
 Oswego 
 Yorkville 

  
---------------------------------------------------
 ENTERTAINMENT 
---------------------------------------------------
Centerstage
 Roger Ebert


  
---------------------------------------------------
 WEEKLY &amp; SEMIWEEKLY PUBLICATIONS 
---------------------------------------------------
	Algonquin Countryside	
	Antioch Review	
	Arlington Heights Post	
	Barrington Courier-Review	
	Booster: Lake View, North Center,
        Roscoe Village, Avondale Edition	
	Booster: Wicker Park, Bucktown,
	      Ukrainian Village Edition	
	Buffalo Grove Countryside	
	Cary-Grove Countryside	
	Deerfield Review	
	Des Plaines Times	
	Edgebrook-Sauganash Times-Review	
	Edison-Norwood Times-Review	
	Elk Grove Times	
	Elm Leaves	
	Evanston Review	
	Forest Leaves	
	Franklin Park Herald-Journal	
	Glencoe News	
	Glenview Announcements	
	Grayslake Review	
	Gurnee Review	
	Highland Park News	
	Hoffman Estates Review	
	Lake Forester	
	Lake Villa Review	
	Lake Zurich Courier	
	Libertyville Review	
	Lincolnshire Review	
	Lincolnwood Review	
	Morton Grove Champion	
	Mount Prospect Times	
	Mundelein Review	
	News-Star	
	Niles Herald-Spectator	
	Norridge-Harwood Heights News	
	Northbrook Star	
	Oak Leaves	
	Palatine Countryside	
	Park Ridge Herald-Advocate	
	Proviso Herald	
	River Grove Messenger	
	Rolling Meadows Review	
	Schaumburg Review	
	Skokie Review	
	Skyline	
	The Batavia Sun	
	The Bolingbrook Sun 	
	The Doings Clarendon Hills Edition	
	The Doings Elmhurst Edition	
	The Doings Hinsdale Editon	
	The Doings La Grange Edition	
	The Doings Oak Brook Edition	
	The Doings Weekly Edition	
	The Doings Western Springs Edition	
	The Downers Grove Sun	
	The Fox Valley Villages Sun	
	The Geneva Sun	
	The Glen Ellyn Sun	
	The Homer Sun	
	The Lincoln-Way Sun	
	The Lisle Sun	
	The Plainfield Sun	

	The St. Charles Sun	
	The Star: Chicago Heights Area	
	The Star: Country Club Hills, Hazel Crest
	The Star: Crete, University Park, Beecher	
	The Star: Frankfort, Mokena	
	The Star: Homer Glen, Lockport, Lemont	
	The Star: Homewood, Flossmore, Glenwood,
        Olympia Fields	
	The Star: New Lenox, Manhattan	
	The Star: Oak Forest, Crestwood, Midlothian	
	The Star: Oak Lawn, Palos &amp; Worth Townships	
	The Star: Orland Park, Orland Hills	
	The Star: Park Forest, Matteson, Richton Park	
	The Star: South Holland, Thorton Township	
	The Star: Tinley Park	
	The Wheaton Sun	
	Times Harlem-Irving	
	Times Jefferson Park, Portage Park,
       Belmont-Cragin Edition	
	Vernon Hills Review	
	Wauconda Courier	
	Wheeling Countryside	
	Wilmette Life	
	Winnetka Talk	





























 
 







 



 






 
 

 

    
 








 

 
 

 
 

 suntimes.com
 Member of the Sun-Times News Group 
 



 

 
 


Traffic  �  
Weather:  

""WHEW!""


		
 
 
 


Search » 

 

 
 Site
 STNG 




 

  
 

� Subscribe 
� Easy Pay 
� Reader Rewards 
� Customer Service 
� Email newsletters 


 

 

 

 

Home
 | 
News
 | 
Commentary
 | 
Sports
 |  
Business 
 | 
Entertainment
 | 
Classifieds
 | 
 Columnists
 | 
Lifestyles
 | 
Ebert
 | 
Search
  | 
Archives
 | 
Blogs
   | 
  RSS 
 
   




























 










  









 
 
  
  
  
  
    
 
    
     
  	
  
    Business 
    
    
     Archive 
  	  
     Appointments 
  	  
     Technology 
  	  
     Currency 
  	  
     Futures 
  	  
     Personal finance 
  	  
     Conrad Black on Trial 
  	  
     Portfolio 
  	  
     Real Estate 
  	  
     Stock market 
  	  
       
  	  
     Auto News 
  	  
     Sally Duros 
  	  
     Dan Jedlicka 
  	  
     Real Estate and Homelife 
  	  
     Chicago Innovation Awards 
  	  
     Made in Chicago 
  	  
     What's my line? 
  	   
  	
  	 
  
 	

  

 


    
   
   

    Columnists     
      	 



 Casual Friday 

  

 Robert Feder 

  

  

 Lewis Lazare 

 Ted Pincus 

 David Roeder 

 Terry Savage 

 Brad Spirrison 
 
	  
	 
  	   
	
	
	  
  
	  	
  


 

  

 

 


 





 


 








 






 






 
Conrad Black on Trial :: 



printer friendly »     
email article »  



 
 
 







 





 







  
 



 







 

  







 



 

 
VIDEO ::    MORE »
  
 
 
 

 
 
 

  
 
TOP STORIES ::
  
 
 NEWS 











Dry out, power up   


  
 BUSINESS 











W.W. Grainger adds new stores to area 


 
 SPORTS 











Bears are well grounded   


 
 ENTERTAINMENT 











Do celebs get off easy?   


 
 LIFESTYLES 











Glamorama: Coyote pretty  


 




 




 

  





 
 
 















	
	 
	
	
			  
				



 
		 
 Black to remain free on bond 
		 
		
		
 
		
		

		 
 July 19, 2007 


		 
			
					
						
						
 Former media mogul Conrad Black may remain free on bond pending sentencing on his conviction for swindling the far-flung Hollinger International newspaper empire he once ran out of millions of dollars, a federal judge ruled Thursday. 
 Black, 62, who once renounced his Canadian citizenship to become a member of the British House of Lords, was found guilty last Friday by a federal jury of three counts of mail fraud and one count of obstruction of justice for spiriting documents out of his Toronto office in defiance of a court order. 



 



















 » Click to enlarge image
 





 A judge said Thursday Conrad Black may remain free on bond pending sentencing.  (AP)
 
 







	
	
	








 
 RELATED STORIES 

� Special section: Conrad Black on trial 

	
	

	

	

	













	
 		



 Black was acquitted of nine other counts ranging from tax fraud to the most serious charge -- racketeering. He was also acquitted of fleecing Hollinger shareholders through such perks as taking the corporate jet on a two-week vacation to the island of Bora Bora. 
 ''I find clear and convincing evidence that Mr. Black is not going to flee,'' U.S. District Judge Amy St. Eve said. 
 But she imposed strict travel limits on Black. He must remain in the Chicago area or in southern Florida, where he has a Palm Beach estate. He may not leave the United States. 
 The government had argued that Black posed a flight risk because he did not have enough money to post a bond large enough to keep him from running away, and he faced the possibility of a long jail term. 
 Prosecutors have said Black faces up to 30 years in jail when he is sentenced Nov. 30. Defense attorneys have said the actual sentence would likely be much less. 
 ''Simply put, Mr. Black faces the potential of spending the rest of his life in jail,'' prosecutors said in court documents. 
 Three other former Hollinger executives, John Boultbee, 65, of Victoria, British Columbia, Peter Y. Atkinson, 60, of Oakville, Ontario, and Mark Kipnis, 59, of Northbrook, Ill., were also convicted of fraud charges after the three-month trial that drew international media attention. 
 Edward Greenspan, Black's Canadian defense attorney, has promised an appeal on ''viable legal issues.'' 
 Hollinger International, based in Chicago and renamed Sun-Times Media Group Inc. last year, was at one time one of the world's largest publisher of community newspapers as well as the Chicago Sun-Times, the Daily Telegraph of London and Israel's Jerusalem Post. AP 		
 


















 


				

 Copyright 2007 Associated Press. All rights reserved. This material may not be published, broadcast, rewritten, or redistributed. 
 











 
 




  

  













 	



 








 
 


  
 suntimes.com:  Send feedback | Contact Us | About Us | Advertise With Us |  Media Kit |  Make Us Your Home Page  
Chicago Sun-Times: Subscribe | Customer Service
 | Reader Rewards |   Easy Pay |  e-paper | P.M. Edition |  Online Photo Store  
Affiliates:  jump2web |  RogerEbert.com |  
SearchChicago -  Autos | 
SearchChicago -  Homes | 
SearchChicago - Jobs | 
NeighborhoodCircle.com |  Centerstage    Partner: NBC5.com


   






 


 
















  
? Copyright 2007 Sun-Times News Group | Terms of Use and Privacy Policy   
 


Member of the Real Cities Network



 























 




