

 PISCO, Peru -- The government sent the army Saturday to stop looting in earthquake-shattered Peru, where tens of thousands lacked fresh water and families huddled in makeshift shelters. 
 

In a soccer stadium in the port city of Pisco, more than 500 people rushed a lone truck that ran out of packets of crackers, candy and toilet paper, screaming that they hadn't eaten and accusing rescue workers of keeping supplies for themselves. 
 

As many as 80 percent of the people in quake-hit urban areas may not have clean water, and many rural communities still haven't been reached to assess the damage, said Dominic Nutt, part of an emergency assessment team in Peru for the aid agency Save the Children. 
 

''The situation is probably worse than first imagined,'' Nutt said. 
 

President Alan Garcia sent 1,000 troops to stop the looting. ''We're going to establish order, regardless of what it costs,'' he said. 
 

Defense Minister Allan Wagner said the death toll from Wednesday's magnitude-8 quake had risen to 540, up from the previous figure of 510. 

