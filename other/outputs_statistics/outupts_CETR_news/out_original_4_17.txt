









 




 

 









 


 




 
 
  
  







  


 July 22, 1999 
 

 LIBRARY/PHOTOGRAPHY THEN AND NOW
 


 Imaging: From Daguerreotypes to Satellites 

  


  By  TOM VANDERBILT 

 
   ne of technology's recurring effects is to offer new ways of seeing. In the 18th century, travelers used a mirror called a ""Claude glass"" to enhance the painterly aspects of what they saw as ideal landscapes. The  stereoscope, invented in 1832 by the British scientist Charles Wheatstone, was an attempt to convey three-dimensional imagery using two lenses.   
 
 
 
 
 

 

 




 
 Overview
 
  � Imaging: From Daguerreotypes to Satellites
 
 
This Week's Articles
   � Satellite Image Web Sites
 
  � National Climatic Data Center
 
  � Photos on the Library of Congress Web Site
 
  � Stereoviews
 
 
 



 In the early 20th century, air travel led to  aerial panoramas, used by military planners in World War I  to map battlegrounds. Perhaps the most revelatory image of all was the first photographs of our planet from space. There was the  Earth, truly seen, for the first time, as a single object.   
 
       Given that the Internet is  becoming the dominant medium through which we ""see"" the world, it  seems appropriate that it should also prove to be a storehouse for any number of  previous and current technologies for seeing the world.   
 
 We cannot only view  reproductions of stereographic images or turn-of-the-century panoramas; we can tap into one of the myriad satellites now orbiting  Earth to get a personalized, localized version of a photo from space.
 
    Geographically,  the world is no  smaller than before, but through the Internet -- the latest trompe l'oeil technological apparatus -- we make it seem so. 
 
  

 
 

 

Irving Underhill





An early-20th-century view of the Brooklyn Bridge, from a collection that can be seen on the Library of Congress's American Memory Web site.
 




Sometimes a computer image does not suffice, however, and there are qualities of texture and tactility that only printed photographs can offer. Fortunately for that, there are a  number of sites that not only store a variety of photographs taken throughout  the history of the medium but also allow the viewer to order reproductions.   
 
  
 
  
 
  
   Tom Vanderbilt is the author of The Sneaker Book: Anatomy of an Industry and an Icon.""
 
  
 
  
 

 






 
 
  
  




 
 


Home | 
Site Index | 
Site Search  | 
Forums  | 
Archives | 
Marketplace
  
Quick News  | 
Page One Plus  | 
International  | 
National/N.Y.   | 
Business   | 
Technology  | 
Science   | 
Sports   | 
Weather  | 
Editorial    | 
Op-Ed | 
Arts   | 
Automobiles   | 
Books  | 
Diversions   | 
Job Market   | 
Real Estate  | 
Travel 
 
 
Help/Feedback  | 
Classifieds   | 
Services   | 
New York Today 
 

 
Copyright 1999 The New York Times Company
 
 
 

 

















