

 "HMV ad thanking artists puzzles industry insiders

 
 Updated Tue. Oct. 28 2008 2:01 PM ET 
 The Canadian Press 


 Toronto -- Sometimes a thank you is just a thank you, HMV Canada president Humphrey Kadaner said Tuesday, amid talk within the music industry about the company's full-page ad in Billboard magazine campaigning against so-called retail exclusives.  
 The ad appears in the U.S. trade magazine's current issue and is styled as a thank you letter to artists who ""have recently said no to retail exclusives in Canada."" The list includes AC/DC, Bryan Adams, Christina Aguilera, Genesis, John Mellencamp, the Police, Guns N' Roses and Anne Murray.  
 In the United States, Wal-Mart is the only store selling AC/DC's new album, ""Black Ice,"" and Best Buy has acquired the exclusive rights to sell the long-awaited CD ""Chinese Democracy"" by Guns N' Roses, expected to be released in late November.  
 But Canadian music fans will be able to find both albums pretty much anywhere CDs are sold, Kadaner said in an interview.  
 ""Five years ago, the first major exclusive was announced by the Rolling Stones (for the ""Four Flicks"" DVD) and since then we've been working hard to educate artists and record companies that Canada's (marketplace is) different and Canadian consumers don't really like these exclusives,"" he said.  
 ""Now there are two major releases coming out that were announced as exclusives in the U.S. and not in Canada. And we thought it's about time we ought to let more artists and managers know we appreciate all the efforts.""  
 While Kadaner insists the ad was simply meant to be a public thank-you note, it does allude to HMV's reputation for fighting back against artists who have signed exclusive deals with other retailers in Canada.  
 HMV temporarily pulled products by Alanis Morissette, Bob Dylan, Elton John and the Rolling Stones off its shelves in retaliation for retail exclusives.  
 Some members of the Canadian music industry expressed skepticism about the HMV ad and whether it was just a thank you or a veiled threat.  
 Industry watcher Larry Leblanc speculated that the ad might have been planned as a pre-emptive threat against labels considering an upcoming exclusive, given that the industry is always searching for ways to make money.  
 ""I think (retail exclusives) are going to come back, quite frankly,"" said Leblanc, who publishes an industry newsletter.  
 ""In this day and age they're looking for money under rocks essentially, anywhere there's revenue to be found.""  
 Leblanc said HMV's ploy to boycott merchandise of artists who sign exclusives has worked well, particularly with Rolling Stones products, because the strategy got a lot of media attention and probably cost the company very little in lost revenue.  
 ""The dirty little secret is the Rolling Stones (albums) don't sell -- they didn't lose any sales because the Rolling Stones don't sell a lot of product,"" said Leblanc.  
 Tim Baker, the head buyer for Canadian chain Sunrise Records, agreed and said most of the artists who typically opt for retail exclusives do so because their prospects of good sales are uncertain.  
 ""The majority of these bands who are doing this, how do I put this, are past their sell-by date for the most part,"" Baker said, although he added that AC/DC doesn't fall in that camp.  
 ""You'll see it again with more aging acts. I think you'll probably find it's (done by) some bands that still think that they're somewhat relevant, like the Rolling Stones, when in actual fact they're probably not.""  
 Kadaner wouldn't say who was hurt more by the lost sales after his company's past boycotts, HMV or the artists.  
 ""We absolutely gave up sales but we believed we're in the consumer business and if we satisfy our consumers on this overall issue we don't mind losing some sales in the short term,"" he said.  
 ""Do I think artists all of a sudden went broke because they weren't getting revenue from sales at HMV in Canada? No. But I just believe there is less hurt and more education in the process.""  
 Making the AC/DC album a retail exclusive in Canada through Wal-Mart ""was discussed but not seriously considered,"" said the company's director of corporate affairs, Kevin Groh.  
 Wal-Mart Canada is not fundamentally opposed to the concept of retail exclusives but chose not to go down that route in this case, Groh said.  
 Baker said he was also a bit puzzled by HMV's ad and wasn't sure whether to take it at face value or not.  
 ""I'm not disagreeing with (the message), but I'm a little bit confused as to what the motivation is here,"" he said.  
 ""Their motivation could very well be fairly magnanimous...I'm not going to say that I think there's an ulterior motive here. I just don't know."""
"3286","The Canadian Press 


 Toronto -- Sometimes a thank you is just a thank you, HMV Canada president Humphrey Kadaner said Tuesday, amid talk within the music industry about the company's full-page ad in Billboard magazine campaigning against so-called retail exclusives.  
 The ad appears in the U.S. trade magazine's current issue and is styled as a thank you letter to artists who ""have recently said no to retail exclusives in Canada."" The list includes AC/DC, Bryan Adams, Christina Aguilera, Genesis, John Mellencamp, the Police, Guns N' Roses and Anne Murray.  
 In the United States, Wal-Mart is the only store selling AC/DC's new album, ""Black Ice,"" and Best Buy has acquired the exclusive rights to sell the long-awaited CD ""Chinese Democracy"" by Guns N' Roses, expected to be released in late November.  
 But Canadian music fans will be able to find both albums pretty much anywhere CDs are sold, Kadaner said in an interview.  
 ""Five years ago, the first major exclusive was announced by the Rolling Stones (for the ""Four Flicks"" DVD) and since then we've been working hard to educate artists and record companies that Canada's (marketplace is) different and Canadian consumers don't really like these exclusives,"" he said.  
 ""Now there are two major releases coming out that were announced as exclusives in the U.S. and not in Canada. And we thought it's about time we ought to let more artists and managers know we appreciate all the efforts.""  
 While Kadaner insists the ad was simply meant to be a public thank-you note, it does allude to HMV's reputation for fighting back against artists who have signed exclusive deals with other retailers in Canada.  
 HMV temporarily pulled products by Alanis Morissette, Bob Dylan, Elton John and the Rolling Stones off its shelves in retaliation for retail exclusives.  
 Some members of the Canadian music industry expressed skepticism about the HMV ad and whether it was just a thank you or a veiled threat.  
 Industry watcher Larry Leblanc speculated that the ad might have been planned as a pre-emptive threat against labels considering an upcoming exclusive, given that the industry is always searching for ways to make money.  
 ""I think (retail exclusives) are going to come back, quite frankly,"" said Leblanc, who publishes an industry newsletter.  
 ""In this day and age they're looking for money under rocks essentially, anywhere there's revenue to be found.""  
 Leblanc said HMV's ploy to boycott merchandise of artists who sign exclusives has worked well, particularly with Rolling Stones products, because the strategy got a lot of media attention and probably cost the company very little in lost revenue.  
 ""The dirty little secret is the Rolling Stones (albums) don't sell -- they didn't lose any sales because the Rolling Stones don't sell a lot of product,"" said Leblanc.  
 Tim Baker, the head buyer for Canadian chain Sunrise Records, agreed and said most of the artists who typically opt for retail exclusives do so because their prospects of good sales are uncertain.  
 ""The majority of these bands who are doing this, how do I put this, are past their sell-by date for the most part,"" Baker said, although he added that AC/DC doesn't fall in that camp.  
 ""You'll see it again with more aging acts. I think you'll probably find it's (done by) some bands that still think that they're somewhat relevant, like the Rolling Stones, when in actual fact they're probably not.""  
 Kadaner wouldn't say who was hurt more by the lost sales after his company's past boycotts, HMV or the artists.  
 ""We absolutely gave up sales but we believed we're in the consumer business and if we satisfy our consumers on this overall issue we don't mind losing some sales in the short term,"" he said.  
 ""Do I think artists all of a sudden went broke because they weren't getting revenue from sales at HMV in Canada? No. But I just believe there is less hurt and more education in the process.""  
 Making the AC/DC album a retail exclusive in Canada through Wal-Mart ""was discussed but not seriously considered,"" said the company's director of corporate affairs, Kevin Groh.  
 Wal-Mart Canada is not fundamentally opposed to the concept of retail exclusives but chose not to go down that route in this case, Groh said.  
 Baker said he was also a bit puzzled by HMV's ad and wasn't sure whether to take it at face value or not.  
 ""I'm not disagreeing with (the message), but I'm a little bit confused as to what the motivation is here,"" he said.  
 ""Their motivation could very well be fairly magnanimous...I'm not going to say that I think there's an ulterior motive here. I just don't know."" 

