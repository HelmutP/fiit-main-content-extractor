


 By Kristin Roberts and Madeline Chambers 


 MUNICH, Germany (Reuters) - Defense Secretary Robert Gates on Sunday dismissed an attack on U.S. foreign policy by Russian President Vladimir Putin as the blunt talk of an old spy and said it was v"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:15PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      U.S.
      
      &gt;
      Article
 
 
 
       Gates dismisses Putin remarks as blunt spy talk 
       Sun Feb 11, 2007 9:28AM EST 
      
     

      
		

		 

	
	 
		 

 

   
          Video  
             
          
			Russia's Putin slams US policies 
			Play Video
		 
         

 


 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		Top News
     
   
   
		 
				Obama sees new generation of leadership
  |  Video

			 
		 
				Olmert non-committal on Palestinian unity pact

			 
		 
				Hillary Clinton faces tough questions over Iraq

			 
		 
				Taliban prepare for spring offensive in Afghan south

			 
		 More Top News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Kristin Roberts and Madeline Chambers 

    


 MUNICH, Germany (Reuters) - Defense Secretary Robert Gates on Sunday dismissed an attack on U.S. foreign policy by Russian President Vladimir Putin as the blunt talk of an old spy and said it was vital to keep working with Moscow. 

    


 In a speech which one U.S. senator said smacked of Cold War rhetoric, Putin told a security conference in Munich on Saturday the United States was making the world a more dangerous place by pursuing policies aimed at making it the ""one single master"". 

    


 A White House spokesman said it was ""surprised and disappointed"" by the comments and some Europeans said it was a wake-up call from a tougher Russia, newly empowered by a sharp rise in the prices of its oil, gas and metals exports. 

    


 But despite their concerns, the White House and Gates underlined the need for cooperation with Moscow. 

    


 ""Many of you have backgrounds in diplomacy or politics,"" Gates, a former CIA director, told the same Munich conference. 

    


 ""I have, like your second speaker yesterday (Putin), a starkly different background -- a career in the spy business. And, I guess, old spies have a habit of blunt speaking. 

    


 Gates raised concerns about Russian arms transfers and its ""temptation to use energy resources for political coercion"" which he said could threaten international stability.  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				
						

				 
Jakarta flood clean-up could take months
 
				 mud and debris from flood-damaged homes on Sunday after days of relatively dry weather, but for many it could be one or two months before they can actually move back into their houses. 
                
                    Full Article  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















