

 "ANC left 'cold' after resignations
         
 28 October 2008, 17:40 
         
           Related Articles 
           
             
               More top members quit ANC 
               Shikota, ANC members clash 
               The art of the possible? 
               Dexter's letter of resignation 
             
           
           
         
         
	  Senior ANC member Phillip Dexter and former ANC Women's caucus parliamentary chairperson Kiki Rwexana quit the ANC on Tuesday. 
 
They told reporters in Cape Town they would take part in the national convention called by Terror Lekota and Mbhazima Shilowa in Gauteng this weekend. 
 
Dexter recently resigned from the SA Communist Party. 
 
Rwexana is also a former deputy secretary general of the ANC Women's League (ANCWL), and is the first sitting ANC MP to resign from the party and from parliament. 
 
Dexter said he was leaving the ANC with a great deal of sadness, but the ANC had undergone a ""transformation"" since about 2004. 
 
Personal ambition, revenge, and vindictiveness had become the main driving force of the movement. 
 
""Dishonesty has really become the order of the day. Most alarming has been the peddling of many lies by current ANC leaders in recent times to justify political positions and decisions, and in particular, the election or removal of people to and from political office."" 
 
The current ANC leadership was at the forefront of undermining the Constitution, Dexter said. 
 
Rwexana said she too was sad to leave the party, which had been her second home for a long time. 
 
However, ""today the ANC is very cold...It depends on which side you are supporting. 
 
""The ANC we used to know is no more. The ANC that taught democracy in South Africa is no more...that taught respect to each, respect to the elderly, is no more. 
 
""There's hatred and anger in the ANC [now]. If you have a different view, you are no longer regarded as a member of the ANC. You are regarded as someone who is anti-ANC,"" she said. 
 
Speaking at the same briefing, Shilowa said work for the convention was progressing well and it was hoped to release the programme and list  of speakers on Thursday. 
 
All political parties in parliament had been invited to attend, as well as business, trade unions, and individuals, he said. 
 
Dexter also rejected suggestions he was a ""counter-revolutionary"" and the like. 
 
During the two terms he served on the ANC national executive committee (NEC), a number of terms on the SACP central committee, and on trade union executives, he never once felt that because he differed,  his position was in jeopardy or that he was going to be excluded in any  way from the organisation. 
 
""The first time that happened to me was when a number of individuals  spoke to me about who they wanted to elect as a leader in the [ANC's] Polokwane conference. 
 
""And I differed with them. I can tell you the day it happened and everything that happened after that day. 
 
""Because from that day onwards, I experienced nothing but people insulting me, accusing me of being a counter-revolutionary, labelling me, attacking me and spreading rumours about me. 
 
""And why, because I disagreed about this issue of electing somebody."" 
 
Dexter said it was a matter of public record that he was one of the people who disagreed with former president Thabo Mbeki on, among other things, macro-economic policy, and HIV and Aids. 
 
""Never once did he or anybody around him, suggest to me that there was no space for me in the organisation. In fact, to give credit, he would engage me."" 
 
Dexter said it was ironic that many of the people who today held leadership positions were those who used to ""hound me because we were accused of being anti-Thabo Mbeki"". 
 
""Those very same people today are the 100 percent Jacob Zuma people, who today, turn around and say Thabo Mbeki did this wrong and that wrong. 
 
""They never once raised those issues when he was apparently doing those things."" 
 
He also dismissed accusations that Lekota was ""anti-democratic"" when  he was ANC chair. 
 
In fact, Lekota had been too democratic and allowed people to speak ""forever"" at meetings. 
 
""So these kinds of claims really, honestly, I think you must take them with a pinch of salt,"" he said. - Sapa 

