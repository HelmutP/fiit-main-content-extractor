

 LONDON (Reuters) - The number of households around the world with high-definition television (HDTV) will treble over the next five years as viewers switch to its clearer, more vivid picture, according to a report on Friday. 

    


 The transition to HDTV has been called a landmark move for the industry, similar to the shift from black-and-white television to color. 

    


 According to Informa Telecoms and Media, the number of homes taking the product will jump to 151 million worldwide by 2011 from 48 million at the end of 2006 when an estimated 1.2 billion households had a television. 

    


 The report said some 58 percent of HD homes were currently found in the United States and 20 percent in Japan, with Britain, Canada, China and Germany also high on the list. 

    


 ""The falling price of high-definition sets has really caught the public's imagination, and consumer uptake is impressive,"" Adam Thomas, the report's author said. 

    


 But he also said some customers were disappointed with the product as, on some services, there is not always enough content to watch. 

    


 The report said it expected this to change and highlighted the situation in the United States, Japan and Australia where governments had set deadlines for broadcasters to deliver a quota of programming in HD. 

    


 ""Maybe it's time for that practice to become more widespread,"" Simon Dyson, senior media analyst at Informa, said.   

