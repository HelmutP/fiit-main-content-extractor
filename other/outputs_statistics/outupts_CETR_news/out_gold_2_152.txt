

 "Man sentenced for Wexford attack
            Tuesday, 28 October 2008 16:05
          
           
 
            
 A 26-year-old man whose attempted rape of a woman left her so badly beaten her friends could not recognise her has been jailed for five years at the Central Criminal Court. 
 The judge said the attack would have continued had the attacker not been caught in the act by friends of the victim. 
 Emil Klinczyk with an address at Main Street, Bunclody, was found guilty by a jury in July of attempted rape, sexual assault and assault causing harm to the woman in Wexford on 10 July 2006. 
 
 Advertisement   
 The woman was walking home alone when she was pulled into a driveway and attacked. 

 

