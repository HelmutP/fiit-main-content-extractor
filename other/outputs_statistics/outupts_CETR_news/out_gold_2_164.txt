

 "Girl raped in Cullinan robbery
         
 28 October 2008, 20:00 
         
           
         
         
	  A 17-year-old girl was raped when a gang of four men robbed a house in Boekenhoutskloof in Cullinan on Tuesday, Gauteng police said. 
 
Sergeant Marinda Stoltz said the four armed men gained entry to the plot house around 4am on Tuesday morning. 
 
The men held the family of five at gunpoint and tied up four members but took a seven-year-old boy with them as they ransacked the house of electronic equipment and computers. 
 
The men loaded the stolen goods into the family's Renault. 
 
One of the men then took the young woman into another room where he raped her. The men fled in the stolen Renault, which was recovered in Mamelodi on Tuesday morning. 
 
The young woman sustained light injuries during her ordeal. The rest of the family were not injured during the robbery. 
 
A case of house robbery and a case of rape were being investigated. No arrests have been made, said Stoltz. - Sapa 

