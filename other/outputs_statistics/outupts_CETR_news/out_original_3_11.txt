

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
INN CROWD OUTLANDISH | By STEVE CUOZZO | Food and Wine | Restaurant Reviews | Guide

























 
  
 
  
  













				  

 
 
  
 
 

 
     
	 	
	
 

		 
		NYC Weather 83 ° MOSTLY CLEAR 		 
		
		
     
    
     
	         
    
     
		 
		Saturday, August 25, 2007  
		Last Update: 
		11:10 PM EDT 
		 
         
         
         
                 
              
         


			Recent
			30 days+
			The Web
			

      
 




       
            
            
			 
				Story Index
				Summer of '77
			 
			
            
            
            
           
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Make Us Rich
            Movies
            Travel
            NYP Home Page
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
Pulse HomeMoviesMusicFoodFashionTheaterHealthTravelComics &amp; GamesHoroscopeDatingWeddings
 

	 
		Movies Home
		Oscars Home
	 
	

	 
		Fashion
		Fashion Week
		Fashion Week Photos
	 
	


 
     
    
        
       
          
             
                INN CROWD OUTLANDISH     
                WAVERLY IN 'PREVIEW': WHERE LOOKS ARE ALMOST EVERYTHING 
             
            
	
		

	
		 
	                 
		 
			
		  		
		 
			Loading new images...
		 
	
		 
			
		 
	      	 

	
 
             

 
             
             
            January 10, 2007 -- THE other night at Waverly Inn &amp; Garden, the world's most crowded "not open yet" restaurant, the waiter didn't know if truffles with $55 macaroni-and-cheese were black or white. Is this any way to launch the alleged new "center of Gotham's café society universe"? 
 Welcome to the most annoying new place to eat in a long time. The service is clunky, the phone number secret. And despite raking in the dough for two months, it's in "previews" - meaning it ducks the press while incubating a colony of apparently beer-averse boldfaces (it's Heineken or nothing). 
 As even Somali urchins by now know, Waverly Inn - sans the "Ye" of earlier iterations - is the pet beanery of Vanity Fair Editor Graydon Carter, a partner with Sean Macpherson, Eric Goode and chef John DeLucie. 
 It yearns to be the throne room of a revivified West Village dining/schmoozing scene that will soon include Keith McNally's hot-button Morandi. 
 But so far, Waverly Inn is behaving more like "Beatlemania," the Broadway musical that dodged critics by running perpetually in "previews" without ever actually opening. 
 A manager bristles when I ask about taking pictures: "We are not giving anybody rights to photograph. Our Edward Sorel murals aren't completely finished." Meanwhile, the joint basks in the shill-shine of media/fashion/arts types craving exposure in Vanity Fair and writers praying for jobs there. 
 And yet, for all the Inn's insufferable mystique, it flatters the zone where James Dean and Bob Dylan raised their collars to the wind. It's hard to stay mad at an eatery so pretty, so cozy, and so romantically evocative of a Village salon era that never was. 
 Inside, the snoots disappear. The waiters are as warm as they are green. You almost forgive a dull-as-toast menu that boasts of using "reverse osmosis" water. 
 Getting a table is actually a cinch. Just call Carter up and ask him for one! Or, for those not on a first-name basis, walk in and ask for a reservation the next night. This one scored me a good, front-room table, and then one next to a kitchen door in the garden. 
 A weeknight visit found long-haired characters out of Jack Kerouac's day, but no sign of Ellen Barkin or Ron Perelman, caught by Page Six in an on-premises water-tossing episode. 
 Sunday night was another story. "Everybody looks famous," a friend marveled. Mr. VF himself held down a U-shaped booth. Carolina Herrera preened at a nearby table. 
 The Waverly occupies the same turf it did for generations until it closed in 2005 - a ramble of rickety, low-ceilinged rooms with fireplaces and a skylit atrium with ivied walls and a tree thrust through the roof. 
 But now there are velvet trim, red banquettes, marvelously mellow lighting and those witty Sorel murals of literary caricatures. Prime areas got the Russian Tea Room treatment with giant booths for power-noshing. 
 With glam dripping from the sconces, Waverly Inn doesn't need the best food on the block. Even so, its menu comes over as comprehensively plodding. 
 Despite the notorious mac-and-cheese - with truffles artlessly shaved atop oversize elbows and gummy cheddar - most dishes are reasonably priced ("large plates" $15-$36, most under $25). But $10 seems too much for an untrimmed, steamed artichoke with leaves so sharp, they draw "owws." 
 Of course, Waverly Inn isn't about the food. But it still needs one thing to be loved: a sign that says "Open." 
 steve.cuozzo@nypost.com  
 
            
          
 
       
       
          


 
 GRAND 'STAND' 
 THE INN CROWD 
 HAUTE SPOTS  
 ELLEN GIVES RON A DRENCHING  
 

 
 UPPER BEST SIDE 
 NOT ANOTHER WHISTLE STOP 
 GOOAALL 
 MORE 
 
           
             
            	
          
            






 
	 
	    
		 
		  			 DATING 
		  			 JOBS 
		  			 AUTOS 
		  			 REAL ESTATE 
		  			
		 
	 
	     
        
     

 
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
 
 
sign insubscribeprivacy policyENTERTAINMENT HEADLINES FROM OUR PARTNERSrss 
 
 
 
  

 

 NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007NYP Holdings, Inc. All rights reserved. 
 

 


