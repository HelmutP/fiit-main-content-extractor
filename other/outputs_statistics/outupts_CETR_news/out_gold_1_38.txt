

 Hurricane strengthens as it roars into Mexico
				 
 With death in its wake, Dean puts fear into Yucat?n 
				
				 August 21, 2007 
				
				 BY DAVID OVALLE, JACQUELINE CHARLES and MARTIN MERZER 
				
 MCCLATCHY NEWSPAPERS 

				 


TULUM, Mexico -- Tourists fled the Mayan ruins, shack dwellers in remote areas sought sturdier refuge and oil field workers turned off the spigots Monday night as a savage Hurricane Dean launched its attack on Mexico's Yucat?n Peninsula. 
 


A top-rank Category 5 storm, poised to escalate its assault early today under cover of darkness, Dean menaced a tourist region called the Mayan Riviera, the city of Chetumal and one of the world's most crucial oil operations. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


Its work was done in Jamaica, where at least two people died, many houses were shattered or flooded and the cleanup was under way. 


Now, it was Mexico's turn. Rain arrived about 5 p.m., the leading edge of genuine trouble. 
 


""We'll take them out by force,"" Tulum Mayor Jorge Luis Cordoba Pech said of anyone who resisted evacuation. Many residents of the coastal town live in tin-and-wood shanties. ""We can't let them lose their lives."" 
 


At the same time, Mexico's state-run Pemex oil company hurriedly ordered 18,000 workers to abandon offshore oil rigs, suspending production at all 407 wells and drilling operations. 
 


Forecasters said Dean's ferocious core would crash into the Yucat?n early today. As the storm approached land, the wind already screamed at 160 m.p.h., 4 m.p.h. above the Category 5 threshold. 
 


In Jamaica, which avoided a direct hit, officials reported extensive -- but possibly not catastrophic -- damage. It included collapsed houses, destroyed roofs, heavy flooding and impassable roads in many parishes. 
 


The road connecting Kingston to its airport was transformed into a sea of sand, an obstacle course of boulders and downed power lines. That airport remained closed, though the airport in Montego Bay reopened late Monday. 
 


It could have been much worse in Jamaica. Dean was blamed for nine deaths in Jamaica, Haiti, the Dominican Republic, St. Lucia and Dominica. Four people were missing in the Dominican Republic and presumed dead. 
 


""There always seems to be somebody looking out for Jamaica,"" said Vernon Thompson, 56, who lives in the Caribbean Terrace neighborhood, near Kingston's airport. ""No matter how bad things are, we always seem to come out quite well."" 
 


For residents of the tiny Cayman Islands, the news was better Monday: Dean's vicious eye wall and other hurricane winds bypassed them, veering to the south, though the islands still were subjected to heavy rain, strong gusts and 16-foot waves. 
 


Dean was expected to push diagonally across the Yucat?n, roaring over populated areas, ancient Mayan ruins, jungles and the western state of Campeche. Among places at risk: Chetumal on the border with Belize and the towns of Tulum, Punta Allen, Mahahual, Felipe Carrillo Puerto and Los Limones. 

