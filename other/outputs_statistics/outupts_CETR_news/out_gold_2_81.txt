

 "Grants fill fiscal gap for Aussie exporters 
						
							By PAUL LE LACHEUR, GrapeGrowers &amp; Vignerons
							 
							Edition 350, 29/09/2008 2:38:23 PM
						
				
				 

					
						
							
								
									
										
											
																						
											
										
										
											Braydun Hill Vineyard's Carol Bradley-Dunn says the EMDG is not just for big producers.
												
		
										
									
								
							
							
								
								
							
							
								
									
										
											
												
											
										
										
											
												
		
										
									
								
							
						
						TO enter any new market, most wine exporters need financial assistance for research and preparation. That's the current wisdom, especially for small to medium enterprises. This begs the question: what about companies that have been active in export markets for a long period of time? The answer is simple: no matter whether it is a new exporter, an existing one or one that has been exporting for a number of years, it pays to get to know the style of export assistance grants that are available. 
 According to Daniel O'Connor, chief executive officer of Xennex, a company specialising in grant applications, time and money spent during the 'pre grant application' stage yield the best results. In addition, it will alleviate the pressure of misinformation, and can be powerful in signposting obstacles before they become fatal to an application. 
 There are two main grants available and they vary in purpose. The Export Market Development Grant is administered by Austrade and is the Federal Government's principal financial assistance program. It has been tailored to suit companies that need encouragement to develop export markets, and reimburses up to 50 per cent of expenses incurred on eligible export promotion activities above a $15,000 threshold. The money needs to be spent first and then a grant claimed from Austrade at the beginning of the next financial year. For someone's first grant (seven grants is the maximum), they may claim expenses incurred over the past two financial years. 
 Seven categories of promotional activities are eligible: overseas representatives and marketing consultants; marketing visits; communications; free samples; trade fairs, seminars and in-store promotions; promotional literature and advertising; and overseas buyers. 
 First time applicants are advised by Austrade to register for the EMDG, then access free, personalised coaching sessions throughout the year to prepare an application correctly, ensuring applicants get the best out of the scheme. 
 Another export assistance scheme is known as the Market Access Program. Designed to suit smaller exporters, it is run by the South Australian Government and sources funds up to $5000 per grant. Again, 50pc of expenditure is reimbursed. 
 Details: www.austrade.gov.au/exportgrants/howtoapply"
"4063","
							By PAUL LE LACHEUR, GrapeGrowers &amp; Vignerons
							 
							Edition 350, 29/09/2008 2:38:23 PM
						 

				
				
					
						
							
								
									
										
											
																						
											
										
										
											Braydun Hill Vineyard's Carol Bradley-Dunn says the EMDG is not just for big producers.
												
		
										
									
								
							
							
								
								
							
							
								
									
										
											
												
											
										
										
											
												
		
										
									
								
							
						
						TO enter any new market, most wine exporters need financial assistance for research and preparation. That's the current wisdom, especially for small to medium enterprises. This begs the question: what about companies that have been active in export markets for a long period of time? The answer is simple: no matter whether it is a new exporter, an existing one or one that has been exporting for a number of years, it pays to get to know the style of export assistance grants that are available. 
 According to Daniel O'Connor, chief executive officer of Xennex, a company specialising in grant applications, time and money spent during the 'pre grant application' stage yield the best results. In addition, it will alleviate the pressure of misinformation, and can be powerful in signposting obstacles before they become fatal to an application. 
 There are two main grants available and they vary in purpose. The Export Market Development Grant is administered by Austrade and is the Federal Government's principal financial assistance program. It has been tailored to suit companies that need encouragement to develop export markets, and reimburses up to 50 per cent of expenses incurred on eligible export promotion activities above a $15,000 threshold. The money needs to be spent first and then a grant claimed from Austrade at the beginning of the next financial year. For someone's first grant (seven grants is the maximum), they may claim expenses incurred over the past two financial years. 
 Seven categories of promotional activities are eligible: overseas representatives and marketing consultants; marketing visits; communications; free samples; trade fairs, seminars and in-store promotions; promotional literature and advertising; and overseas buyers. 
 First time applicants are advised by Austrade to register for the EMDG, then access free, personalised coaching sessions throughout the year to prepare an application correctly, ensuring applicants get the best out of the scheme. 
 Another export assistance scheme is known as the Market Access Program. Designed to suit smaller exporters, it is run by the South Australian Government and sources funds up to $5000 per grant. Again, 50pc of expenditure is reimbursed. 
 Details: www.austrade.gov.au/exportgrants/howtoapply 



