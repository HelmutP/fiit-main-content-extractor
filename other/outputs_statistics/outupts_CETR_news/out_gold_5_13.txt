

  (adds background, share reaction)  

    
     PARIS, Feb 9 (Reuters) - French drugs group Sanofi-Aventis (SASY.PA: Quote, Profile, Research) said on Friday a California court had ruled against it in a case pitting it against Amphastar (AMPR.O: Quote, Profile, Research) and Teva (TEVA.O: Quote, Profile, Research) over the best-selling Lovenox drug.  

    
     Sanofi said it was reviewing its options and intended to continue vigorously to defend its intellectual property rights.  

    
     The company is battling with the generic rivals over the patent of its bloodthinner Lovenox.  

    
     The French drug maker, faced with a slew of generic threats, hoped the trial would address the issue of intent which was unresolved in April when a U.S. Court of Appeals ruling reversed a lower court decision that had invalidated the Lovenox patent.  Following Sanofi's appeal victory, the case returned to the U.S. district court of California. Lovenox is Sanofi's best-selling drug, with sales of 2.14 billion euros ($2.75 billion) in 2005.  

    
     The generic drugmakers are trying to sell cheaper Lovenox copies in the United States even though the patent expires in 2012. Lovenox is also under attack from Momenta Pharmaceuticals (MNTA.O: Quote, Profile, Research) and Sandoz, owned by Switzerland's Novartis (NOVN.VX: Quote, Profile, Research).  

    
     Lovenox is a complex mixture of ingredients based on live organisms and Sanofi has said that part of the drug cannot be characterised and therefore cannot be copied.    

