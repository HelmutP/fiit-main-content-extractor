

 "//W3C//DTD HTML 4.0 Transitional//EN""&gt;  

   VOA News - US Considers Talks With Some Afghan Taliban Elements 





 
 













  
  

     
  
 
 	
	

	
	
	
	 
  
  A trusted source of news and information since 1942  

  

 
	
		Text Only
		
	
	
	 

	
Search  
	
	
	
	
	&gt;)"" name=""submit""&gt;
	


	


 
	
	

 VOICE OF AMERICA 

 VOA Home 
 VOA English 
 Regions/Topics 
 Subscribe to E-mail 
 Select Language 
 About VOA 	
		
		
	 

	
	
		
		      LatestNewscast  News NowLive  VOA AfricaLive   Africa Americas Asia Europe Middle East U.S.A.  American Life Health &amp; Science Entertainment News Analysis Special Reports  Shows by Name VOA News Blog T2A Chat  VOA Mobile Podcasts Webcasts Correspondents Broadcast Info  Articles in  Special English Pronunciations  Read Editorials    

		
		
	   

	
	US Considers Talks With Some Afghan Taliban Elements
			
			
				
	By David Gollust 
				 
				
				Washington 
				
					28 October 2008 
					
				
				 
			
		
		
		 
		
		
		 State Department officials say the United States is considering contacts with elements of Afghanistan's militant Islamic Taliban movement, as part of a broad Bush administration review of the conduct of the Afghan war. The review is expected to be completed after the U.S. presidential election. VOA's David Gollust reports from the State Department.  Officials here say that while no decisions have been made, the policy review could lead to a direct U.S. dialogue with what are termed ""reconcilable"" elements of the Taliban.  The Bush administration began an urgent review of Afghanistan policy earlier this month, in the face of what U.S. military officials say is a mounting insurgent threat in Afghanistan by the Taliban, elements of al-Qaida, and other Islamic extremists.  The Afghan government of President Hamid Karzai has been engaged in contacts with relatively moderate Taliban elements brokered by Saudi Arabia.   The United States, while supporting the Saudi-led dialogue, has thus far spurned direct talks itself.  But a senior State Department official who spoke to reporters here Tuesday said the idea of U.S contacts with some Taliban factions is ""certainly something that has been discussed as part of the review.""  The official said American outreach to Sunni tribes in Iraq, initially hostile to the U.S.-led intervention in that country, helped turn them against al-Qaida and to sharply reduce violence in that country.  At a news briefing, State Department Deputy spokesman Robert Wood said a similar U.S. overture - to Taliban elements willing to reconcile with the Kabul government - is a possible outcome of the review:  ""I'm not saying that that's not a possibility. What I'm saying is that there is a review ongoing and we're looking at a wide range of aspects of our policy, to try to improve our ability to battle extremism in Afghanistan, and help the Afghans do their jobs as well,"" said Wood.  Wood said any direct U.S. contact with the Taliban would be in the context of a broader reconciliation process which he said ""has to be something that the Afghans themselves take ownership of, and lead.""   The senior official who spoke here said any Taliban elements that take part would have to meet the Karzai government's terms, and renounce violence, pledge allegiance to the Afghan constitution and become part of the political process.  It is unclear whether any Taliban factions would be willing to make such commitments, and U.S. officials say they do not envisage talks with the movement's radical senior leadership.  The Wall Street Journal, which reported the potential dialogue Tuesday, quoted one U.S. official as saying the United States will never sit at the table with the fugitive leader of the Taliban, Mullah Omar.  The idea of outreach to less-extreme Taliban elements has been publicly endorsed by U.S. Army General David Petraeus, the former Iraq commander who this week becomes head of the U.S. Central Command which includes responsibility for Afghan operations.  
   
		
		

		
		
		
		
		 
		
		
					
					
					
					E-mail This Article 
					
		 			
		
					
					
					
					
					Print Version
					 
			
 
   
		
		
	    
		
		

		 



  
Related Stories
 
 

				
				
				
				

				
				Pakistan, Afghan Leaders Seek Talks With Taliban Factions 
				

				
				
				
				

				
				Afghanistan, Pakistan, US Military Forces Cooperate in Border Region 
				

				
				
				
				

				
				Australia to Investigate Deaths of Rejected Afghan Asylum Seekers 
				

 

 



  
Top Story
 
 
  
	
   
	
   
   
   

   
   		Economy Remains Focus in Final Days of US Election Campaign      		
   		
   	
   

   
	  
 


 



  
More Stories
 
 
  
	 
 
 
 
 
 
 Global Stocks Gain Despite Dire US Economic Forecasts 
 
 
 

  	 
 
 
 
 
 
 Iceland says it Needs $6 Billion Bailout   
 
 
 

  	 
 
 
 
 
 
 Pakistani, Afghan Delegates Agree to Talks With Taliban   
 
 
 

  	 
 
 
 
 
 
 US Considers Talks With Some Afghan Taliban Elements 
 
 
 

  	 
 
 
 
 
 
 Iraq Proposes Changes to US Troop Pact 
 
 
 

  	 
 
 
 
 
 
 Zimbabwe Opposition Says Ruling Party Not 'Genuine' in Power-Sharing Deal   
 
 
 

  	 
 
 
 
 
 
 Rebels Advance in Eastern Congo   
 
 
 

  	 
 
 
 
 
 
 Israeli Parties Call for February 10 Election 
 
 
 

  	 
 
 
 
 
 
 Afghanistan, Pakistan, US Military Forces Cooperate in Border Region   
 
 
 

  	 
 
 
 
 
 
 North Korea Threatens to Turn South Into 'Debris'   
 
 
 

  	 
 
 
 
 
 
 South Korean 'Goose Dads' Feel Pinch of Crisis-Driven Currency Swing   
 
 
 

  	 
 
 
 
 
 
 Academic Program Gives Boost to Rwandan Coffee Sales   
 
 
 

  	 
 
 
 
 
 
 US Hispanic Voters Favor Obama, But Turnout Will Be Key   
 
 
 

  	   
	  
 
					
					
		
				
	  
	  
	    
		    FAQs   |  Terms of Use &amp; Privacy Notice   |  Broadcasting Board of Governors   |  Site Map   |  Link to Us    |  Contact Us  

		
	  
	
	
     
  





  
  
  
 
  
 
 


