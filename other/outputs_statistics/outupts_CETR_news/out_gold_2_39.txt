

 "Safin may retire aftertwo 'difficult' years

		
	
	
	
				
	 
 
	October 28, 2008		Edition 1
		 

	 
		
	 

	 PARIS: The regular tennis season entered the final straight yesterday at the Paris Masters, where a dozen players are chasing the three remaining slots at next month's lucrative ATP season-ender in Shanghai.
 
 With 18 of the top 20 players in the world appearing in the City of Light - including Roger Federer, who gave organisers a fillip by confirming he would be on board as he bids to land his first Masters title of the year - French fans expected a feast.
 
 They also hoped that, with the top players getting a bye to round two, they would see three-times Paris champion Marat Safin serve up a tasty hors d'oeuvre against Argentinian qualifier Juan Monaco.
 
 
 Instead, Safin gave them a dogs breakfast of a performance - losing absymally 6-0 7-6 (7-4) in one hour and 19 minutes. Seven double faults and 46 unforced errors told their own sad story and Safin admitted he couldn't wait to start his holiday in Miami.
 
 ""I just don't know what happened. I just couldn't find my rhythm. I didn't take my chances,"" said the two-times Grand Slam winner, who turns 29 in January.
 
 Safin said he would consider his future after two almost non-stop years. ""I have had two difficult years and no vacation - pretty intensive. If I feel like I want to continue to play, I will -  if not, it will be over."" - Sapa-AFP 

