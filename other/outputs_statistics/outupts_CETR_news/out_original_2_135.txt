

 "//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

 




	
	?Biomaterial? artifical heart ready by 2011
	 
	
	
	
	
	
	
	
	
	

	
	
	


 
	
	  
	 
			 
				 National Post 
				 Canada.com Network 
				 
					
						Browse the Canada.com Network
						
						
							Home
							News
							Sports
							Entertainment
							Health
							Travel
							Obituaries
                            Celebrating
							Contractors
						
						
						
							National Post
							Victoria Times Colonist
							The Province (Vancouver)
							Vancouver Sun
							Edmonton Journal
							Calgary Herald
							Regina Leader-Post
							Saskatoon StarPhoenix
							Windsor Star
							Ottawa Citizen
							The Gazette (Montreal)
							DOSE
							Vancouver Island Newspapers
							VANNET Newspapers
						
						
						
							Global TV
							Global National
							Global BC
							Global Calgary
							Global Edmonton
							Global Lethbridge
							Global Saskatoon
							Global Regina
							Global Winnipeg
							Global Ontario
							Global Quebec
							Global Maritimes
							E!
							CHEK NEWS
							CHCA NEWS
							CHCH NEWS
							CJNT Montreal
							TVTropolis
							X-treme Sports
							Specialty Channels
						
					

				 
				 
					Shop | Find a Career | Find a Car | Find a Home
				 
				
			 

		 National Post 

		 
 
 
   
     
       
        Home
       
     
     
       
        News
       
     
     
       
        Opinion
       
     
     
       
        Arts
       
     
     
       
        Life
       
     
     
       
        Sports
       
     
     
       
        Cars
       
     
     
       
        Multimedia
       
     
     
       
        Your Post
       
     
     
       
        NP Network Blogs
       
     
     
       
        Financial Post
       
     
   
   
     
       
        Movies
       
     
     
       
        TV Listings
       
     
     
       
        The Ampersand
       
     
     
       
        Shinan Govani
       
     
     
       
        Painting Competition
       
     
   
 

  

 

		 
		 
			 
 

                                   
                        
                        
                        
				    
				    
				    
				    
				    
                    
                        
                         

			 
			 
				 Hot Topics 
				 
					 RBC Paint 
					 Footprint 
					 Cars 
				 
				 
					 Politics 
					 U.S. Election 
					 Sun Destinations 
				 
			 

		 

	 
	
	
		   Search  
	
	

	
	 Home / Arts 
	

	
	 
		 
			
 
 
 ?Biomaterial? artifical heart ready by 2011 
 Agence France-Presse?
        
            Published:?Tuesday, October 28, 2008 
 
 
 Related Topics 
 
  Cardiology  
  Clinical Trials  
  Alain Carpentier  
 
  
 
 
 Story Tools 
 
  -+ Change font size
              
  Print this story  
  E-Mail this story  
 
 
 
 Share This Story 
 
  Facebook  
  Digg  
  Stumble Upon  
  More  
 
 
 
 Story tools presented by 
 
 
 
  A fully implantable artificial heart designed to overcome the worldwide shortage of transplant donors will be ready for clinical trial by 2011, the French professor behind the prototype said yesterday. Leading heart transplant specialist Alain Carpentier, head of the European research team behind the project, said the prosthetic heart was ready to be manufactured and should be ready for human use ?within two and half years.? The prototype was developed in association with a team of aerospace engineers. Shaped like a real heart, with the same blood flow rhythms, the prototype uses the same technology as prosthetic heart valves already used around the world. Made from chemically treated animal tissues, these ?biomaterials? are designed to avoid rejection by the patient?s immune system or blood clotting, a recurrent problem with existing artificial hearts, Carpentier said.  
 
 
 
 
 Close 
 
 Presented by 
 
 

 
 
 Get the National Post newspaper delivered to your home 
 

			
			 
			  
			 
				 
					 Reader Discussion 
 

 

				 
				 
 

 

				 
			 

			 
			
			 
 

 
 More From National Post 
 
  Anxiety disorders linked to high blood pressure National Post - Monday, Oct. 27, 2008  
  Diagnosis issue putting women at risk: doctor National Post - Monday, Oct. 27, 2008  
  Diets heavy in fried food and animal fats account for 35% of
all heart attacks National Post - Tuesday, Oct. 21, 2008  
  Posted Sports NP National Post - Saturday, Oct. 18, 2008  
  NHL needs to look deeper National Post - Thursday, Oct. 16, 2008  
 
 
 
 More From the Web 
  
  Total artificial heart to be ready by 2011: research team Tehran Times, Iran - Tuesday, Oct. 28, 2008  
  Missile technology makes artificial heart The Age, Australia - Tuesday, Oct. 28, 2008  
  'Full' artificial heart implant BBC News - Tuesday, Oct. 28, 2008  
  Now, an artificial heart that beats Times of India - Tuesday, Oct. 28, 2008  
  Artificial heart beats its rivals and fools experts Independent, Ireland - Tuesday, Oct. 28, 2008  
  
 
 
  First fully artificial heart ready for human trials 'within two and a half years' Daily Mail, UK - Monday, Oct. 27, 2008  
  Scientist: Artificial human heart trials by 2011 Guardian, UK - Monday, Oct. 27, 2008  
  French company aims to create artificial heart Reuters - Tuesday, Oct. 28, 2008  
 
 Powered by Inform 
 
 
 

			 
			
		 
		
		 
			 
			
 




 
  Global News
 
 
   
    
      Previous
    
    
      Next
    
   
   
     
      
        
      
       
        Tonight on Global National, Canada's Most-Watched National Newscast: Oct 28
       
       The day's news headlines from Global National's Kevin Newman for Tuesday, October 28, 2008. 
     
     
      
        
      
       
        Medical marijuana ruling
       
       Mon, Oct 27 - Ottawa's effort to restrict the supply of medical marijuana has been denied.  Kevin Newman reports. 
     
     
      
        
      
       
        Lost &amp; found
       
       Mon, Oct 27 - For centuries, the original stage where Shakespeare's dramas played out was lost...until now.  Tara Nelson reports. 
     
     
      
        
      
       
        Big drop
       
       Mon, Oct 27 - CNS correspondent David Akin reports on the markets taking a tumble towards the end of the day. 
     
     
      
        
      
       
        Our government, our economy
       
       Mon, Oct 27 - Jacques Bourbeau has the results of our exclusive poll on how Canadians feel about how the government and our troubled economy. 
     
   
  
 
 


			 
			 
			 Most Popular 
			 
				 
					  National Post  
					  Financial Post  
					  E-Mailed NP  
				 
				 
 

 Subscribe to Feed 
 
  Flaherty poised to be 'Deficit Jim'  
  Obama poses a danger to Canada: former U.S. ambassador  
  Video gamers high earners, athletic and dating: studies  
  Chinese-made chocolate coins recalled  
  Polls leave Senator Dewey-eyed  
 
 


				 
				 
				 
			 
			

			 
			 
		 
 

                        
                        
				    
				    
				    
				    
				    
                    
                        
                         

		 

			 
			 
			 
				 NP Network Blogs 
				 
					 
						  National Post  
						  Financial Post  
					 
					 
 News for your mobile device | Subscribe to Feed 
 
 
   
     
      The Ampersand
     
     
      Mixed Media: Venice coming to Ontario (maybe), the £60-million locked gallery, operas and orchestras strapped
     
     Tue, Oct. 28, 2008 · 5:43 PM ET by Adam McDowell 
     
      More
     
     
					Filed under: 
					Mixed Media, Art and Culture | 
					Read More | 
					Comments (0) 
   
   
     
      Full Comment
     
     
      Neil Hrab: Eternal struggle against western infidels proves wearing on Ahmadinejad's health
     
     Tue, Oct. 28, 2008 · 5:30 PM ET by Kelly McParland 
     
      More
     
     
					Filed under: 
					Full Comment, Neil Hrab | 
					Read More | 
					Comments (0) 
   
   
     
      The Appetizer
     
     
      Making Love in the Kitchen, episode 7: Freezing With Fritzi
     
     Tue, Oct. 28, 2008 · 5:22 PM ET by Brad Frenette 
     
      More
     
     
					Filed under: 
					nutrition, Making Love in the Kitchen | 
					Read More | 
					Comments (0) 
   
   
     
      Posted Toronto
     
     
      Remake reveals 1930s murals (and Wi-Fi too) 
     
     Tue, Oct. 28, 2008 · 5:09 PM ET by Rob Roberts 
     
      More
     
     
					Filed under: 
					Kuitenbrouwer | 
					Read More | 
					Comments (0) 
   
   
     
      Full Comment
     
     
      With McKenna out, these two are probably pretty darned happy
     
     Tue, Oct. 28, 2008 · 4:35 PM ET by Ronald Nurwisah 
     
      More
     
     
					Filed under: 
					Canadian politics | 
					Read More | 
					Comments (2) 
   
   
     
      Full Comment
     
     
      Steve Janke: Lloyd Axworthy advocates 'Stop Harper' alliance of opposition parties
     
     Tue, Oct. 28, 2008 · 4:25 PM ET by Kelly McParland 
     
      More
     
     
					Filed under: 
					Full Comment, Steve Janke, Canadian politics | 
					Read More | 
					Comments (2) 
   
   
     
      Posted Toronto
     
     
      Is school?s Warriors logo offensive?
     
     Tue, Oct. 28, 2008 · 4:25 PM ET by Rob Roberts 
     
      More
     
     
					Filed under: 
					Q&amp;A | 
					Read More | 
					Comments (1) 
   
   
     
      Posted Toronto
     
     
      Teen stabbed at Don Mills school in fight ?over a pair of gloves?
     
     Tue, Oct. 28, 2008 · 3:53 PM ET by Chris Boutet 
     
      More
     
     
					Filed under: 
					Crime | 
					Read More | 
					Comments (0) 
   
   
     
      Full Comment
     
     
      FC Abroad: Even opponents spare some sympathy for disgraced Ted Stevens
     
     Tue, Oct. 28, 2008 · 3:33 PM ET by Kelly McParland 
     
      More
     
     
					Filed under: 
					Full Comment, U.S. Politics, Araminta Wordsworth, FC Abroad | 
					Read More | 
					Comments (0) 
   
   
     
      Posted
     
     
      Precriminations: GOP begins McCain campaign blame game well before voting day
     
     Tue, Oct. 28, 2008 · 3:26 PM ET by Shane Dingman 
     
      More
     
     
					Filed under: 
					U.S. Politics, u.S. election | 
					Read More | 
					Comments (0) 
   
   
     
      The Ampersand
     
     
      Today is World Animation Day
     
     Tue, Oct. 28, 2008 · 3:00 PM ET by Brad Frenette 
     
      More
     
     
					Filed under: 
					Art and Culture | 
					Read More | 
					Comments (0) 
   
   
     
      Posted Sports
     
     
      World Series: Game 5 finish delayed until Wednesday
     
     Tue, Oct. 28, 2008 · 2:52 PM ET by Jim Bray 
     
      More
     
     
					Filed under: 
					Baseball, MLB | 
					Read More | 
					Comments (0) 
   
   
     
      The Ampersand
     
     
      The Kurt Cobain guide to a successful business
     
     Tue, Oct. 28, 2008 · 2:39 PM ET by Brad Frenette 
     
      More
     
     
					Filed under: 
					Music | 
					Read More | 
					Comments (0) 
   
   
     
      The Ampersand
     
     
      This just in: Frank Magazine dead again
     
     Tue, Oct. 28, 2008 · 2:24 PM ET by Adam McDowell 
     
      More
     
     
					Filed under: 
					Magazines | 
					Read More | 
					Comments (0) 
   
   
     
      Full Comment
     
     
      Ottawa forces a bad idea on Toronto in the name of environmental purity
     
     Tue, Oct. 28, 2008 · 2:00 PM ET by Kelly McParland 
     
      More
     
     
					Filed under: 
					Full Comment, Kelly McParland, Canadian politics | 
					Read More | 
					Comments (8) 
   
   
     
      Posted Sports
     
     
      NBA Preview: Pacific Division
     
     Tue, Oct. 28, 2008 · 2:00 PM ET by Eric Koreen 
     
      More
     
     
					Filed under: 
					Basketball, NBA | 
					Read More | 
					Comments (0) 
   
   
     
      Full Comment
     
     
      John Ivison: Cotler denies floor-crossing rumours
     
     Tue, Oct. 28, 2008 · 12:50 PM ET by Kelly McParland 
     
      More
     
     
					Filed under: 
					Full Comment, John Ivison, Canadian politics | 
					Read More | 
					Comments (2) 
   
   
     
      Full Comment
     
     
      David Akin: Cabinet speculation -- The Rookies
     
     Tue, Oct. 28, 2008 · 12:37 PM ET by Kelly McParland 
     
      More
     
     
					Filed under: 
					Full Comment, david akin, Canadian politics | 
					Read More | 
					Comments (0) 
   
   
     
      The Ampersand
     
     
      Ode to a stripper, by Nickelback
     
     Tue, Oct. 28, 2008 · 12:37 PM ET by Adam McDowell 
     
      More
     
     
					Filed under: 
					Music | 
					Read More | 
					Comments (4) 
   
   
     
      Posted
     
     
      Prince Charles says climate the real crisis
     
     Tue, Oct. 28, 2008 · 12:33 PM ET by Karen Hawthorne 
     
      More
     
     
					Filed under: 
					World, Environment | 
					Read More | 
					Comments (2) 
   
 
 


					 
					 
				 
				
			 

			 
			 
 




 
  Video
 
 
   
    
      Previous
    
    
      Next
    
   
   
     
      
        
      
       
        Hudson's nephew's body found
       
       Police confirm they have found the body of Academy Award-winning actress Jennifer Hudson's nephew 
     
     
      
        
      
       
        Campaign follies
       
       Despite the polls tightening, the Palin factor appears to be turning against the Republican Party 
     
     
      
        
      
       
        Bullets in the rafters
       
       Brett Gundlock takes a look at the secret club hidden in the rafters of Toronto?s Union Station ?- one the the mayor would like to evict 
     
     
      
        
      
       
        Urban fishing
       
       Tyler Anderson takes a look at a unique way to bring a bit of the outdoor adventure to innercity children 
     
     
      
        
      
       
        The Talibe Children of Senegal
       
       Tyler Anderson follows the talibe children, sent by their parents to live with and beg for their marabout' 
     
     
      
        
      
       
        Dusk to Dawn
       
       National Post's photographers explore Toronto after sunset 
     
     
      
        
      
       
        40-Something
       
       Tyler Anderson takes a look at an unusual retirement home for ex-HIV test patients 
     
     
      
        
      
       
        Faces in the field
       
       NP Video: An eccentric sculptor has turned his surroundings into the biggest tourist attraction in northern Ontario's Burks Falls 
     
     
      
        
      
       
        A better you
       
       NP Video: The Post and our experts show three women ways to a better you in our new series 
     
     
      
        
      
       
        Graffiti
       
       From Montreal to Vancouver, their names are written in paint, marker and carved into walls, benches and windows -- and all manner of public and private spaces 
     
     
      
        
      
       
        Little Kenadie
       
       Kenadie Joudin Bromley is four-years-old and weighs 10 lbs. She's a primordial dwarf 
     
     
      
        
      
       
        The Abaya Monologues
       
       On the streets with Danielle Crittenden as she gets a taste of life in conservative Islamic dress 
     
   
  
 
 


			 
			 
				 
					 More National Post 
					 
						 
NewsRSS
 
 

 
   
     
      Toronto stocks soar 7% in late rally
     
   
   
     
      Trudeau top pick for Liberal leader: poll
     
   
   
     
      McKenna drops out of Liberal leadership race
     
   
 
 

					 
					 
						 
Financial PostRSS
 
 

 
   
     
      North American stocks rally on bargain hunting
     
   
   
     
      Epic closes flagship hedge fund
     
   
   
     
      Housing market 'tracking' U.S. boom, bust
     
   
 
 

					 
					 
						 
SportsRSS
 
 

 
   
     
      No joy in mudville: World Series Game 5 postponed until Wednesday
     
   
   
     
      NBA Preview: How do you top that?
     
   
   
     
      Mitchell wants Raptors to have 'principles'
     
   
 
 

					 
					 
						 
ArtsRSS
 
 

 
   
     
      He sees stars, and they're all Canadian
     
   
   
     
      New Nickleback song bares itself
     
   
   
     
      The 1967 Casino Royale farce was a little too shaken ? and stirred
     
   
 
 

					 
				 

				 
 

                        
                        
				    
				    
				    
				    
				    
                    
                        
                         

				 

			 
			 
				 Promotion 

				 
					 
						 
 

 Stock Challenge 

  

 
  Click for a chance at your share of $150,000 in prizes
 
 

						 
					 
					 
						 
 

 FP Stock Widget 

  

 
  Stay up to date and take your stock portfolio anywhere
 
 

						 
					 
				 

			 

		 
		
	 
	
	 
		
		
			
				
				
				
				
			
		
	 

	
	 
	 
		
		
	 

	 
		 Services: Privacy | Terms | Contact us | Advertise with us | FAQ | Copyright and permissions | Today's paper | Digital paper | Newsletter | YourPost | News Feeds | Subscription services | Site map 
		 National Post: Home | News | Opinion | Arts | Life | Sports | Cars 
		| Multimedia| Your Post | NP Network Blogs 
		 Financial Post: Home | Analysis | Trading Desk | Markets | Money | Small Business | Careers | Reports | FP Magazine 
	 
	 
		 
			National Post and Financial Post are Part of the Canada.com Network 
					
						Browse the Canada.com Network
						
						
							Home
							News
							Sports
							Entertainment
							Health
							Travel
							Obituaries
                            Celebrating
							Contractors
						
						
						
							National Post
							Victoria Times Colonist
							The Province (Vancouver)
							Vancouver Sun
							Edmonton Journal
							Calgary Herald
							Regina Leader-Post
							Saskatoon StarPhoenix
							Windsor Star
							Ottawa Citizen
							The Gazette (Montreal)
							DOSE
							Vancouver Island Newspapers
							VANNET Newspapers
						
						
						
							Global TV
							Global National
							Global BC
							Global Calgary
							Global Edmonton
							Global Lethbridge
							Global Saskatoon
							Global Regina
							Global Winnipeg
							Global Ontario
							Global Quebec
							Global Maritimes
							E!
							CHEK NEWS
							CHCA NEWS
							CHCH NEWS
							CJNT Montreal
							TVTropolis
							X-treme Sports
							Specialty Channels
						
					

		 
	 
	 © 2008 The National Post Company. All rights reserved. Unauthorized distribution, transmission or republication strictly prohibited. 
	 
	
 

					
					
					
					
					
				




