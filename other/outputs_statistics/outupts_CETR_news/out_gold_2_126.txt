

 "The infantilisation of politics
			 
 
						FERIAL HAFFAJEE: COMMENT
												-
						Oct 21 2008 00:00
			 
            
         
     

 
	
		 34 comment(s) | Post your comment
	
 
 
 

	
		
		 ARTICLE TOOLS 

	
	
		
		Add to Clippings
	
	
		
		 Email to friend
	
	
		
		Print 
	
	
		
  MORE TOOLS 

	
	
		
		Add to Facebook
	
	
		
		Add to Muti
	
	
		
		Add to del.icio.us
	
	
		
		Blog Reactions
	
	
		
		
	
		
		
  MAP 

	
	
		

    

     
        The map will replace this text. If any users do not have Flash Player 8 (or above), they'll see this message.
     
    
 	





		
	
	
 


 

 
 
 

	
	If there's one thing driving me into the arms of the ANC --Mark2, it's Youth League president Julius Malema and his ilk. And I'm not alone.	
	 
	 
	
	
As he announced UDI last week, former ANC chairperson Terror Lekota was applauded only when he verbally klapped Malema, asking why we should be harangued and frightened by children.  
He's so right.  
In the post-Polokwane universe it is the rantings of Malema, which have scarred the body politic. From his injunction to kill for [ANC president Jacob] Zuma to his loose invocation of the phrase ""We are willing to lay down our lives for ?"" and his lax approach to the independence of the judiciary, the young man with the dead eyes has, for too many months, been allowed to bully this nation. His understanding of power is not that it is a stewardship granted by citizens to leaders, but that it is a force to be unleashed across the land like a disciplining whip. 
  
""Nobody,"" he has said, ""will tell the youth league what to do."" At other times he has sounded like a foot soldier of Pol Pot about to usher us counter-revolutionaries into re-education camp. If you think this is an overstatement, remember this is the young man who took students out of school (when the statistics show they need every classroom minute they can get) to march against the Constitutional Court judges. And nobody told him where to get off!   
Some may not take him terribly seriously, but his view that former president Thabo Mbeki should be axed carried the day against more sensible calls to leave the lame duck in office where he could do no harm.Look now. Mbeki's probably a strategist behind the ANC split which is no mere splinter, as the occupiers of headquarters at Luthuli House seem to think. And for that the ruling party has got Malema and his masters to blame.   
Who are his masters? Zuma is the uber-chief, who uses the young man as a battering ram; in turn, he has resisted serial requests by the national executive committee of the ANC to rein in Malema, who has become an enormous liability for the party.  
This is not a treatise against young leadership, but against the infantilisation of our political discourse. Ours is a country, the founding father of which symbolised, in his early days, the value of young thought and strategic radicalism. Nelson Mandela and his generation blew like fresh winds through the ANC of their time, demanding more change more quickly. By contrast, Malema's rhetoric is mere flatulence unleashed across the land.  
And like haemorrhagic fever it's extremely infectious and potentially deadly. Over in the red corner is the Young Communist League (YCL), which competes daily with the ANCYL to see who can take our debate down another notch or two. Our revise sub-editor, Jo Tyler, was so incensed by the nonsense in the press release the young communists put out after Lekota announced he was leaving that she's given them a free lesson in proofreading.   
And if you thought that press release was an aberration jotted down in a hurry, look at this one. Tutu and Ndungane: kindly leave politics to politicians, the YCL recommended to the former archbishops Desmond Tutu and Njongonkulu, who were in the trenches when these little 'uns were still in nappies.    
 CONTINUES BELOW 
 
 

Click here?

 
  
The cautionary words to these two great men are "" ? yet another old and tired Anglicanism of a Christian movement theory wanting to tower large in competition with churches like Roman Catholic Church. Both have used their leadership in the clergy; to behave like politicians and to make commentary on any societal issue regardless of its relevance to religion.""  
So put that in your cassock and smoke it, say the hotheads. The good thing is that we now no longer have to treat this kind of rhetoric with any seriousness and we can take our votes and make the X elsewhere."
"5834","FERIAL HAFFAJEE: COMMENT
												-
						Oct 21 2008 00:00
			
 
            
         
     

 
	
		 34 comment(s) | Post your comment
	
 
 
 

	
		
		 ARTICLE TOOLS 

	
	
		
		Add to Clippings
	
	
		
		 Email to friend
	
	
		
		Print 
	
	
		
  MORE TOOLS 

	
	
		
		Add to Facebook
	
	
		
		Add to Muti
	
	
		
		Add to del.icio.us
	
	
		
		Blog Reactions
	
	
		
		
	
		
		
  MAP 

	
	
		

    

     
        The map will replace this text. If any users do not have Flash Player 8 (or above), they'll see this message.
     
    
 	





		
	
	
 


 

 
 
 

	
	If there's one thing driving me into the arms of the ANC --Mark2, it's Youth League president Julius Malema and his ilk. And I'm not alone.	
	 
	 
	
	
As he announced UDI last week, former ANC chairperson Terror Lekota was applauded only when he verbally klapped Malema, asking why we should be harangued and frightened by children.  
He's so right.  
In the post-Polokwane universe it is the rantings of Malema, which have scarred the body politic. From his injunction to kill for [ANC president Jacob] Zuma to his loose invocation of the phrase ""We are willing to lay down our lives for ?"" and his lax approach to the independence of the judiciary, the young man with the dead eyes has, for too many months, been allowed to bully this nation. His understanding of power is not that it is a stewardship granted by citizens to leaders, but that it is a force to be unleashed across the land like a disciplining whip. 
  
""Nobody,"" he has said, ""will tell the youth league what to do."" At other times he has sounded like a foot soldier of Pol Pot about to usher us counter-revolutionaries into re-education camp. If you think this is an overstatement, remember this is the young man who took students out of school (when the statistics show they need every classroom minute they can get) to march against the Constitutional Court judges. And nobody told him where to get off!   
Some may not take him terribly seriously, but his view that former president Thabo Mbeki should be axed carried the day against more sensible calls to leave the lame duck in office where he could do no harm.Look now. Mbeki's probably a strategist behind the ANC split which is no mere splinter, as the occupiers of headquarters at Luthuli House seem to think. And for that the ruling party has got Malema and his masters to blame.   
Who are his masters? Zuma is the uber-chief, who uses the young man as a battering ram; in turn, he has resisted serial requests by the national executive committee of the ANC to rein in Malema, who has become an enormous liability for the party.  
This is not a treatise against young leadership, but against the infantilisation of our political discourse. Ours is a country, the founding father of which symbolised, in his early days, the value of young thought and strategic radicalism. Nelson Mandela and his generation blew like fresh winds through the ANC of their time, demanding more change more quickly. By contrast, Malema's rhetoric is mere flatulence unleashed across the land.  
And like haemorrhagic fever it's extremely infectious and potentially deadly. Over in the red corner is the Young Communist League (YCL), which competes daily with the ANCYL to see who can take our debate down another notch or two. Our revise sub-editor, Jo Tyler, was so incensed by the nonsense in the press release the young communists put out after Lekota announced he was leaving that she's given them a free lesson in proofreading.   
And if you thought that press release was an aberration jotted down in a hurry, look at this one. Tutu and Ndungane: kindly leave politics to politicians, the YCL recommended to the former archbishops Desmond Tutu and Njongonkulu, who were in the trenches when these little 'uns were still in nappies.    
 CONTINUES BELOW 
 
 

Click here?

 
  
The cautionary words to these two great men are "" ? yet another old and tired Anglicanism of a Christian movement theory wanting to tower large in competition with churches like Roman Catholic Church. Both have used their leadership in the clergy; to behave like politicians and to make commentary on any societal issue regardless of its relevance to religion.""  
So put that in your cassock and smoke it, say the hotheads. The good thing is that we now no longer have to treat this kind of rhetoric with any seriousness and we can take our votes and make the X elsewhere.
 

