

So will the 43rd Munich Security Conference be remembered as the start of a new Cold War? 
 
That is probably the single most important question to emerge from this long weekend of speeches and private chats among the world's most powerful. 
 
 
Certainly Russian President Vladimir Putin's strident speech stands out from the crowd. 
 
 
In it, to recap, he strongly criticised the US and its European allies, with his harshest criticism reserved for Washington. 
 
 
The US had, he said, overstepped its borders in every way, seeking to impose its will on the world. 
 
 
'No new Cold War'
 
 
But Washington has clearly decided to politely brush aside Mr Putin's remarks rather than to escalate tensions with Russia. 
 
 
Making his first appearance at the conference as the new US Defence Secretary, Robert Gates tried to deflate President Putin's attack by poking a bit of fun at their past careers as spies.
 
 
""As an old Cold Warrior,"" he said, ""one of yesterday's speakers almost filled me with nostalgia for a less complex time. Almost.
 
 
"" Many of you have backgrounds in diplomacy or politics. I have, like your second speaker yesterday, a
starkly different background - a career in the spy business.
 
 
""And, I guess, old spies have a habit of blunt speaking"" 
 
 
Mr Gates bluntly said there was no new Cold War and that the US certainly did not want one. 
 
 


	
		 

			
			 
				
				 Mr Putin said the US overstepped its borders in every way 
			 
			
		
		
	

	


Rather, he said, it sought Russia's partnership.
 
Overall though, Mr Gates's performance will surely be remembered for it being very unlike his predecessor Donald Rumsfeld - the man the conference used to love to hate. 
 
 
Clearly hoping to reach out to America's European allies he acknowledged that the US had made mistakes in the last few years and that it needed to work on restoring its reputation as a force for good in the world. 
 
 
He even pointedly poked fun at his predecessor.
 
 
""Over the years, people have tried to put the nations of Europe and of the Alliance into different categories,"" he said. 
 
 
""And I am told that some have even spoken in terms of 'old' Europe versus 'new',"" referring to comments made by Mr Rumsfeld in the run-up to the Iraq invasion.
 
 
It got a good laugh.
 
 
But back to that Cold War question. Towards the end of the conference Russian Defence Minister Sergei Ivanov defended his boss, insisting President Putin had not intended to be confrontational.
 
 
After that the mood appeared to soften. But there is no doubt there have been mutterings here about a chill in relations between and Russia and the West - though maybe not a new Cold War. 

