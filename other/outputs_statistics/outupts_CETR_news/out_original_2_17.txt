

 "//W3C//DTD HTML 4.0 Transitional//EN""&gt;

 
	



	
	

















	Anglican Journal: Moratoria may pose ?difficulties?
	
	









		
			
				
					
						
						
  About these ads 

					
				
				

			
		
		
			
		
		
			
				
					
						
						08
						


					
				
			
			
		
		
			

LATEST ISSUE | ARCHIVES | SUBSCRIPTION CHANGES | CONTACT US








		
		
			
				
					 October 28, 2008 
					
Home &gt; 2008 &gt; October 2008

					
FONT SIZE: A A A A

				
				
					
						 Canada 
						 Council of General Synod 
						 General Synod 2007 
                         Anglican Journal Daily 
						 House of Bishops 
						 Residential Schools 
						 World 
						 Africa 
						 Asia / Pacific 
						 Europe 
						 United Kingdom / Ireland 
						 Middle East 
						 United States 
						 Central / South America 
						 Anglican Communion 
						 World Council of Churches 
						 Opinion &amp; editorial 
						 Editorial 
						 Letters 
						 Concerning Lutherans 
						 Culture 
						 Books 
						 Film 
						 Music 
						 Television 
						 Theatre 
						 Spotlight 
						 Lambeth 2008 
						 HIV / AIDS 
						 PWRDF 
						 Sexuality debate 
						 Obituaries 
						 Bible readings 
					   E-Mail updates 
						 Diocesan partners 
						 Masthead 
						 News feeds RSS 
						what is RSS?
						 Classified Ads 
						Employment 
						Bed &amp; Breakfasts 
						Conferences 
						Anniversaries 
						
						
 
 About these ads



					
					
				

			            
	
		
		
			


 
		 Moratoria may pose ?difficulties? 
		 Clarity sought about expectations 
		 Marites N. Sison staff writer 
		 Oct  1, 2008 
					 

contributed

Archbishop of Canterbury Rowan Williams attempts to bridge both sides of the issue. Related Stories 
 Oct  1, 2008 - Dealing with Lambeth moratoria  
 
 
 The Archbishop of Canterbury, Rowan Williams, has acknowledged that, while ?a strong majority? of bishops present at the Lambeth Conference agreed on the need for moratoria on same-sex blessings and on cross-border interventions, they were aware of the ?conscientious difficulties this posed for some.?   Archbishop Williams said ?there needs to be a greater clarity about the exact expectations and what can be realistically implemented.? Many Canadian bishops have expressed concern about the lack of clarity regarding the moratoria (please see related story, page 1.)   In a letter sent to bishops of the Anglican Communion on Aug. 26, Archbishop Williams said that many bishops, especially the newer ones, said that ?they had been surprised by the amount of convergence they had seen.? He added: ?And there can be no doubt that practically all who were present sincerely wanted the Communion to stay together.?   Archbishop Williams said that, while it ?remains to be seen? how the sense of belonging together ?will help mutual restraint? when dealing with the divisive issue of sexuality, ?it can be said that few of those who attended left without feeling they had in some respects moved and changed.?   The final document released by bishops at the Lambeth Conference, held July 16 to Aug. 3, was also ?an attempt to present an honest account? of what was discussed and expressed in so-called indaba groups, said Archbishop Williams. He said that, while the document, called Reflections, was not a formal report, it nonetheless identified what bishops considered to be priorities in the Anglican Communion.   ?There was an overwhelming unity around the need for the church to play its full part in the worldwide struggle against poverty, ignorance and disease,? he said. ?The Millennium Development Goals were repeatedly stressed, and there was universal agreement that both governmental and non-governmental development agencies needed to create more effective partnerships with the churches.?    

			

 		

		
			
	
		

		
					
					
						
						
					 
 About these ads

						   
 
 Print this article Respond to this story Reader's Opinions 
 
 
						
						  Anglican Journal does not endorse and is not responsible for the content of external sites. External links will open in a new window 
						
 

						
					
								
			
		
		
			Copyright 1998- 2008 |   Latest Issue | Archives | Contact Us | Search | Privacy Policy | Writers' guidelines

  
		
			
		
	
	
	











