

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
ZEKE DEBUT RIGHT ON 'Q' | By MARC BERMAN | New York Knicks
























 
  
 
  
  













				  

 
 
  
 
 

 
     
	 	
	
 

		 
		NYC Weather 80 ° CLOUDY 		 
		
		
     
    
     
	         
    
     
		 
		Sunday, August 26, 2007  
		Last Update: 
		12:30 AM EDT 
		 
         
         
         
                 
              
         


			Recent
			30 days+
			The Web
			

      
 




       
            
            
			 
				Story Index
				Summer of '77
			 
			
           
            
            
            
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Make Us Rich
            Movies
            Travel
            NYP Home Page
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
       
 
   Sports Home
   Columnists
   
      New York Teams
   
   
      More Sports
   
   Bettor's Guide
   Send a Letter
   
      Sports Blogs
   
 

 
Yankees
Mets
Giants
Jets
Knicks
Nets
Rangers
Islanders
Devils
Liberty
 


 
MLB
NFL
NBA
NHL
NCAA Football
NCAA Basketball
Golf
Tennis
Racing
WNBA
 


		 
			The Back Page
			Hardball
			Yankees
			Mets
			Knicks
			Rangers
			NHL
			Hondo
			
			Giants
			Jets
			College Sports
			Fantasy Tracker
			Where's Kernan?
			College Sports
		 
		


 
     
    
        
       
          
             
                ZEKE DEBUT  RIGHT ON 'Q'     
                RICHARDSON PACES KNICKS TO 3-OT WIN 
             
             By MARC BERMAN 
	
		

	
		 
	                 
		 
			
		  		
		 
			Loading new images...
		 
	
		 
			
		 
	      	 

	
 
             

 
             
                        November 2, 2006 -- MEMPHIS - Their fourth-quarter choke of a 19-point lead was as despicable as anything Larry Brown's team did last season. 
 
  But throughout Isiah Thomas' first game as Knick head coach, throughout three overtimes, one Knick always stood tall, the one Knick who was to blame as much as anyone for last season's 23-59 disgrace. 
 
  With the Knicks down one, an exhausted Quentin Richardson - capping a 31-point, nine-rebound night - calmly sank two free throws with 11.9 seconds left in the third OT to propel the Knicks to a ghastly 118-117 victory over the Grizzlies at Fedex Forum. 
 
  With Stephon Marbury, Steve Francis and Eddy Curry all fouling out, Richardson played 56 minutes, made 10 of 13 shots, all five of his 3-pointers and was 6-of-6 from the foul line. 
 
  "At the end of the day, the biggest thing is we won," Richardson said in the weary but jubilant locker room. "As crazy and wacky as it was, you look back at it and say we won." 
 
  It was such a perfect night that Richardson even made the final game-winning defensive stop after sinking the free throws. He harassed Mike Miller into a missing a driving runner in the lane with one second left, with gritty David Lee (10 points, 13 boards) pulling down the ferocious rebound to end the madness and avoid an embarrassment. 
 
  "I told Isiah in the huddle, there's no way they're getting a tip-in to win this thing," Lee said. 
 
  At 1-0, the victory marks the first time the Knicks have been over .500 since Jan. 5, 2005, when they were 16-15 under Lenny Wilkens. The Knicks, who get the Hawks tomorrow in Atlanta, had lost four straight season openers under three different head coaches. 

 
 
VIEW FULL ARTICLE &gt;page 1 2 CONTINUE READING &gt;             

             
  

 
          
       
       
          


 
 SURGERY SETS BACK JEFFRIES 
 ROSE MAY BE ON THE WAY OUT 
 STEPH NURSES SORE FOOT 
 ISIAH 'BALKS' 
 

 
 KNICKS MULL NICHOLS' FUTURE 
 STEPH SAYS DON'T FEAR KG, CELTS 
 KNICKS' LEE ENJOYING TIME WITH TEAM USA 
 MORE 
 
           
             
            	

          
          

 THE BACK PAGE 

 JOEL SHERMAN?S HARDBALL 

 WHERE'S KERNAN? 

 METS 

 YANKEES 

 KNICKS 

 RANGERS 

 NHL 

 HONDO 


 NETS 

 FANTASY TRACKER 
        
     

 
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
 
 
sign insubscribeprivacy policySPORTS HEADLINES FROM OUR PARTNERSrss 
 
 
 
  

 

 NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007NYP Holdings, Inc. All rights reserved. 
 

 


