








 



 






 

 
 

 
 

 
 

 
 

 
 



 

 


Dogs bark (Or how to infiltrate your newsroom with good writing) 







By DICK HUGHES 

Salem Statesman Journal 
 





You're back in the same old grind. So how do you push good writing?

How do 

you nudge your editors, and the rest of the newsroom, along? Here are

21 

ideas from other newsrooms, compiled for a National Writers' Workshop. You can use many on your own. Some need

editors' 

backing (which is always helpful). Choose what's best for your

situation. 

Most cost little money, so the boss can't say, ""We can't afford that.""

But 

remember, dogs bark, so don't expect them to purr -- not to begin

with, at 

least. 
 







 1. Form an ongoing writers group. Give one another the support, ideas

and 

constructive suggestions you don't always get from your editors.

 
 2. Set up short-term, four- to seven-week writer circles, with a 

leader/mentor and three to seven writers who get together each week to

critique one another's work. (This might lead to a regular writers

group.)

 
  3. Make copies of the best tips from National Writers Workshop

sessions. 

Pass them out. Hold a brown bag discussion about what you learned.

Invite 

the editors. Do this whenever someone attends an outside workshop or 

convention.

 
  4. Buy a tape (or ask your publication to) of your favorite writing 

workshop or speaker - one that translates well to audio. Listen to it

when 

you're commuting, exercising or pretending to be transcribing an

interview. 

Loan it to colleagues.

 
  5. Convince your newsroom to start a writers' library, and promote

it. If 

the money-handling folks are sluggish, start your own. Shop used

bookstores 

and library book sales. Pick up good books about writing and examples

of 

great writing. (Keep your receipts for the day when the light bulb

goes off 

and the editors reimburse you. Heck, give the editors a book. Tell

them you 

know how much they value good writing.) Clear off a desk or shelves in

the 

middle of the newsroom for the library. Tell people to bring in the

best 

article, book or magazine story they've read. Encourage staff members

to 

browse and borrow.



 
  6. Trade places. Offer to work a few days on the copy desk or a

different 

beat. You'll gain new perspectives -- it's a cheap break from your

regular 

job -- and you'll share some ideas on writing.



 
  7. Get on the mailing list for writing newsletters. (Jack Hart at The

Oregonian puts out one of the best, ""Second Takes."")



 
  8. Buy ""Coaching Writers"" by Roy Peter Clark and Don Fry. Read it.

Share a 

copy with your editor. Take him or her to lunch or coffee to talk

about it. 

Want to make sure you have the boss' attention? Offer to pick up the

tab 

(save the receipt).



 
  9. Find a writing coach -- an editor, colleague or friend who's good

at 

listening and asking the right questions and who can help you stay

sane.



 
  10. Set aside time each day, even a few minutes, to work on something

special.



 
  11. Bring cookies or pizza for the copy desk or editors. (That'll

surprise 

them.) Get them to talk about what drives them crazy, and excites

them, 

about the writing in your newsroom.



 
  12. Talk up good writing. Compliment colleagues when you see a great

lead 

or story. Get articles on writing/reporting from the Poynter Institute

website, the Freedom Foundation and other journalism organizations. 

Photocopy the best ones and hand them out with a personal note.

(""After your 

story on ..., I thought you'd be interested in this."") Put notes about

writing, or a daily writing tip, on a bulletin board, in a staff

newsletter 

or e-mail, or in a ""Whatsup"" discussion file in your computer system.



 
  13. Rotate critiques led by reporters, copy editors and other staff 

members, not just editors. Include interns and new staff members, who

have 

fresh views. Make it a short daily oral or written critique, or ask

them to 

review a week's work and focus on an area of their choice.



 
  14. Blanket the board. Post examples of good work from your

publication and 

from other ones. Line the hall leading to the bathrooms with your and

your 

competitors' editions. Ask vacationing staff members to bring back

papers 

from around the country. Post them, along with your edition from the

same 

day, so people can examine the writing, story play, etc.



 
  15. Get your team, bureau or newsroom to set up monthly brown bag

sessions 

with local experts on subjects to build reporting expertise -- how

local 

budget laws work, how to read annual reports, how to use the local

library 

to research stories, a look at guns confiscated by cops.



 
  16. Set up monthly visits to news-making sites for copy editors, and

other 

writers, so they experience the sights and sounds of what's in the

news. 

(They'll be more likely to respect -- and sharpen -- the sense of

place in 

your stories.)



 
  17. Add a section on writing -- tips, good examples, links -- on your

newsroom website or start your own. Get a local college or writers 

organization to participate.



 
  18. Take stories and write them based on different structures --

inverted 

pyramid, first person, narrative/chronological, bumper sticker, point

of 

view, book chapters, radical clarity.



 
  19. Work with management (you need their support and their dollars to

make 

this fly) to launch a newsroom writing program. Bring in speakers from

other 

media (use your NWW contacts), colleges, libraries and the community.

Set up 

discussions by speakerphone with the nation's top writers; many will

give up 

45 minutes to talk about writing. Create a staff-led monthly awards

program 

to honor excellent writing and other good work.



 
  20. Tithe the time. Ask each editor to commit a specific amount of

his or 

her time each week to training staff members, including developing

good 

writing.



 
  21. Reward good behavior. Tell editors when they've done an

especially good 

job of coaching or editing. Thank copy editors for a wonderful

headline or 

great catch on an error. Tell the story.









 
Top





 



 

 
 





  

April 4-5 	

Portland, Ore. 



Wilmington, Del. 
 



April 18-19 

Charlotte, N.C. 

Hartford, Conn.

 
 

April 25-26 

Orange County, Calif. 

Orlando, Fla. 
 



May 30-31 

Colorado Springs, Co. 
 



June 6-7 

Twin Cities, Minn.

	 
 

	

 
 

	

 

A narrative writer's glossary

 
 

Advice for editors on working with narratives

 
 

Narratives: 2 types



  






Content &amp;copy copyright 1998 Detroit Free Press. All rights reserved.






