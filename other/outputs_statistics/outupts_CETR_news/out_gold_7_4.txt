

 Microsoft is considering a release of security updates on CD for users of its older operating systems who lack broadband connections to the Internet, according to an e-mail sent to prospective beta testers on Tuesday.
 
 
 
 
The e-mail invites beta testers who are using Windows 98, Windows 98 Second Edition, and Windows Millennium to join a test of a CD that will include aa pertinent critical security patches found on the WindowsUpdate Web site.
 
 
 
 
""This security update CD will be of special benefit to customers with slow Internet connections and for those customers who typically do not visit the Microsoft Web site to download updates for their computers,"" the e-mail read.
 
 
 
 
Microsoft did not disclose a possible release date for the security update CD, but beta user applications will be accepted only until December 5.
 
 
 
 
The Redmond, Wash.-based developer has been criticized by some analysts for relying exclusively on the Web-based WindowsUpdate service, which users with slow dial-up connections often ignore because of the large file sizes of security fixes. 

