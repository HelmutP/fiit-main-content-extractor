

 "
//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt; 



 Rudy Guede guilty of Meredith Kercher murder, Amanda Knox faces trial - Times Online 







 
  


 
 
 


 
  
 Free Pizza 
 Buy one pizza and get one free at PizzaExpress 
  
 
 
 
 Navigation - link to other main sections from here 
 
Skip Navigation  
 
 
 
 
 
 
 
  
  
 
Having a £600 handbag is like having a crush on The Joker in Batman
 
 Caitlin Moran 
 
  
 
 
 
 
 
 
 News 
 Comment 
 Business 
 Money 
 Sport 
 Life &amp; Style 
 Travel 
 Driving 
 Arts &amp; Ents 
 
 
 Archive 
 Our Papers 
 Site Map 
 
 
 
 
 UK News 
 World News 
 Politics 
 Environment 
 Weather 
 Tech &amp; Web 
 Times Online TV 
 Photo Galleries 
 Topics 
 Mobile 
 RSS 
 
 
 
 

 

 
 
 
Times Online
Times Archive
 

 
 Where am I? 
Home  
NewsWorld NewsEurope News  

 
 
 
  

MY PROFILE
SHOP
JOBS

PROPERTY
CLASSIFIEDS
  
 
 

 

 
   
 
 
 

From The Times
 
 


 
 
October 28, 2008 
 
 Rudy Guede guilty of Meredith Kercher murder, Amanda Knox faces trial 
 

 
 
  
       
 
 
 
  

Richard Owen in Perugia
 
  

 
 Kercher
murder: the three versions 
 
An immigrant who turned his back on his adoptive family and drifted into a
life of petty crime was tonight found guilty and sentenced tonight to 30
years' jail for the murder and sexual assault of the British student
Meredith Kercher, who was killed in Perugia one year ago.
 
 
In a separate ruling, Amanda Knox, Ms Kercher's American flatmate, and Ms
Knox's former Italian boyfriend Raffaele Sollecito, who have been in custody
ever since the crime, were ordered to stand trial on 4 December for Ms
Kercher's death.
 
 
The pair are also accused of murder, sexual assault and the theft of Ms
Kercher's cash. Rudy Guede was cleared of theft.
 
 
  Related Links  
  
 
 
Knox 'at scene of Kercher death'
 
 

  
Meredith Kercher's murder: the three versions
  

  
‘Foxy Knoxy framed me,’ says drifter
  






  

 
 
 
 
Guede, 21, who was arrested after fleeing to Germany and extradited to Italy,
had opted for a fast-track trial in the hope of a reduced sentence if found
guilty. The prosecution had asked for a life term. However in Italy a life
term often means thirty years in practice. Walter Biscotti, Guede's lawyer,
told The Times he would lodge an immediate appeal.
 
 
Ms Kercher's parents, John and Arlene, her sister Stephanie and brother Lyle
were in court in Perugia to hear the ruling by the judge, Paolo Micheli. The
family had also attended the opening of the pre-trial hearings for Ms Knox
and Mr Sollecito in September, when they said they hoped for justice for the
daughter and announced they would seek 25 million euros in compensation.
 
 
Commenting on Guede's conviction, Francesco Maresca, the Kercher family's
lawyer said: ""We are very satisfied, even though it means this young man now
faces a very heavy sentence.""
 
 
Judge Micheli, who delivered his ruling after eleven hours of deliberation,
said he would decide today whether to grant a request by the defence for the
pair to be released from prison and placed under house arrest while awaiting
trial.
 
 
In the closing stages of the pre-trial hearings Giuliano Mignini, the
prosecutor, rejected claims police had contaminated DNA evidence placing all
three suspects at the scene of the crime.
 
 
The prosecution alleged that the three took part in a drug-fuelled Hallowe’en
sex party in which Ms Kercher, from Coulsdon, Surrey, was an unwilling
participant and which ended with her death. She was found on November 2 last
year half naked and with her throat cut under a duvet in the bedroom of the
cottage she shared with Ms Knox. The pathologist said she had died a “slow
and agonising death”.
 
 
According to a prosecution reconstruction Guede and Mr Sollecito held Ms
Kercher down after she had been forced to her knees, while Ms Knox stabbed
her in the throat. Much of the pre-trial process has centred on the DNA
evidence, which forms the crux of the prosecution case.
 
 
Mr Guede admitted he was at the cottage on the evening of the crime, but
claimed he was innocent and that Ms Knox and Mr Sollecito had sought to
frame him for the murder. His lawyers had argued there was nothing to link
him to the presumed murder weapon, a kitchen knife that had traces of the
DNA of Ms Knox and Ms Kercher but not of Guede.
 
 
  
Page 1 of 2
  
 

 Next Page
 
 
 
 

 
 
 

 
 
 Print 
 Email 
 Post to del.icio.us 
 Post to Fark 
 Post to Yahoo! 
 Post to Digg 
 
 
 
  
 
  
 Also in Europe News  
  
 
Rudy Guede guilty of Meredith Kercher murder, Amanda Knox faces trial 
 
Russian muder suspect confesses after falling in love with officer 
 
Imprint of famine seen in genes of Second World War babies 60 years on 
  
 
 
 Also in World News  
  
 
Ivory from 10,000 elephants on sale amid fears of new slaughter  
 
Iraq demands all US troops out by 2011 
  
 
 

 
 
 
 
  
   
 
//W3C//DTD HTML 4.0 Transitional//EN""&gt;
  

 
 

 
 Times Recommends 
 
 
Mound Bayou buys into Obama's hope 
 
Scientists make artificial heart 
 
Biblical age mines could be King Solomon’s 
 
 
 
 


 
 
Soyuz returns
 
 

 
 
 Space tourist tycoon Richard Garriott floats back to earth 
 
 Slide Show 
 
 



 
 
Queen in Europe
 

 
 
 
 The Queen visits Slovakia 
 
 Slide Show 
 
 



   
 
 
 Paris weblog 
 
  
 
 
 
Sarkozy, the natural leader of Europe
 
 
 
 
 
 
   
 


 
 
  
 
 Times Archive: Victory in Europe 
 
  
 
 
 
The war with Germany is over
 
 
 
 
  Explore Times Archive: War and revolution  
 
 
  
 
 
 
 

 
  
 Comment    
 
 
 

It is useless to say nationalism and ethnic tribalism have no place in international relations of the 21st century

 
More... 
  Christopher Meyer  
 
  Post a comment  
  
 
 

 
 
Pictures of the Day
 
 
 
 
 A selection of the best images from around the world 
 
 Slide Show 
 
 
 
  



   
 



    
 
 Comment Central 
 
  
 
 
 
The morning after: Why Republicans won't matter
 
 
 
 
  Daniel Finkelstein's blog  
 
 
    
 


 
 
 
 
 
 

 
 MOST READ 
 MOST COMMENTED 
 MOST CURIOUS 
 
 

 
 
   
 
 
 Most Read 
Skip Most Read   Today 
 
 Nuclear-powered passenger aircraft 'to... 
 Who's the greatest? The Times US... 
 Scientists develop artificial heart that... 
 Alex Ferguson: why I don't pray - and other... 
 

 
 
 MOST COMMENTED 
Skip Editor's Pick   Today 
 
 Passports will be needed to buy mobile phones 
 Nouriel Roubini: I fear the worst is yet to... 
 eBay buyer faces libel action after leaving... 
 Politicians attack BBC as Ofcom launches... 
 

 
 
 MOST CURIOUS 
Skip Most Curious  
 Today 
   
Children 'want adult supervision on internet'   
   
Jordan copper mines from biblical times...   
   
Swans delay migration to stay in warmth of...   
   
Lavatory owner feeling flush after...   

 
 
 
 
   
 
 

 
 
 
   



 
 Focus Zone 
 
 
   
  
  
 
 Social Entrepreneurs: 
 The inside track on current trends in the charity, not for profit and social enterprise sectors 
 
  
  
  
 
 Asian Cuisine: 
 Explore your passion for food with the delights of Thai, Indian &amp; Chinese cooking 
 
  
  
 

 
 
 A Life More Streamlined: 
 In our new series, Tony Hawks takes a dry, wry look at modern life - junk mail, interminable meetings and snooty sales assistants 
 
  
  
  
 
 Triathlon Training: 
 Read the training tips and advice that helped our London Triathletes 
 
  
  
  
 
 James Bond: 
 Read our exclusive 100 Years of Fleming and Bond interactive timeline, packed with original Times articles and reviews  
 
  
  
  
 
 Business Travel: 
 The latest travel news plus the best hotels and gadgets for business travellers  
 
  
   
     
 
 
 
  
 Social Entrepreneurs 
 Asian Cuisine 
 A Life More Streamlined 
  
  
 Triathlon Training 
 James Bond 
 Business Travel 
  
 
 
 
 
 

 
  
  
 Special 
 
Fleming &amp; Bond
 
 Exclusive 100 year timeline 
 
  
 
 

 
  
  Services  
 
 
  
 Business City Guides 
 Free Money Guides 
 IFA Search 
  
  
 Fantasy Football 
 Business Directory 
 Free Credit Report 
  
 
  
 

 
  
  
 
 Business City Guides 
 Overseas contacts and local business information 
 
  
 
 


 
  
  
 Related Feature 
 Life: Streamlined 
 Tony Hawks' wry look at life 
 
  
 
 


 
  
  
  
 Andrew Davidson meets Peter Sands 
 
  
 
 


 
  
  
 
 Credit Crunched? 
 Our Credit Clinic has free help and advice 
 
  
 
 

    
  Popular Searches on Times Online  
 
 
 
 books  
| 
 chess  
| 
 credit crunch 
| 
 currency converter  
| 
 fashion  
| 
 football  
| 
 formula 1  
| 
 london film festival  
| 
 mortgages  
| 
 pensions  
| 
 podcasts  
| 
 property  
| 
 recipes  
| 

 redundancy calculator  
| 
 savings  
| 
 sudoku  
| 
 us election  
| 
 wine  
 
 
 
  Shortcuts to help you find sections and articles  
 
    
 
 
  
Classifieds 
  
 
 
 
 
 
Cars 
 
 
Jobs 
 
 
Property 
 
 
Travel 
 
 
 
 
 
 
 
 
 
 Cars 
 
Skip Cars of the Week   
Volkswagen Touareg Altitude  
2007 
£30,000   
 
 
 
 
 
 
BMW M3 Coupe  
2008 
£44,950   
 
 
 
 
 
 
Mini Cooper Cooper S  
2005 
£10,245   
 
 
 
 
 
 
Car insurance  
Great car insurance deals online    
 
 
 
 
 
  Search for more cars and bikes  
 

 
 
 Jobs 
 
Skip Jobs of the Week  
Standing Counsel  
Charity Commission 
London  
 
 
 
 
 
 
Head of Membership and professional records 
c. £50,000 + benefits 
British Medical Association 
London  
 
 
 
 
 
 
Sales Director 
£
75k + big bonus 
Lyle &amp; scott 
London with overseas travel  
 
 
 
 
 
 
Software Engineering and Business Analysis Manager 
£54,054 - £66,950 
National Improvement Policing Improvement Agency 
London  
 
 
 
 
 
  Search more Jobs  
 

 
 
 Properties 
 
 
Great Dubai Investment Opportunities  
From £89,950  
 
 
 
 
 
Rushmore Condo's New Luxury Condo's In Manhattan 
Great Investment, River Views   
 
 
 
 
 
Tooting, London  
2 bed flat for rent 
£1080PCM   
 
 
 
 
 
Luxury Antiguan Beachfront House 90ft Moorings Plus 40ft Sunseeker  
Also avaliable for rent 
$3.5 million   
 
 
 
 
  Search for more properties  
 

 
 
 Holidays 
 
Skip Travel of the Week   
Funway Holidays Int Inc 
Captivating Caribbean 
SAVE 25% at Sandals Resorts!   
 
 
 
 
 
3 nights 4* Crowne Plaza Center City 
Deps 01 Nov - 20 Dec 08 
£469 per person   
 
 
 
 
 
Advertise your holiday property online 
List your property with two leading travel websites    
 
 
 
 
 
Travel insurance  
Great travel insurance deals online     
 
 
 
 
  Search for more holidays  
 

 
 
 
Place your advert now 
 
 

 Search Ad Reference: 
  
  

 
 
 
 
 
   
 
 


 
 
 
 
 

 
  
 Where am I? 
Home  
NewsWorld NewsEurope News
  

  
 
 Contact us 
 Back to top 
 
  
  
 News 
 Comment 
 Business 
 Money 
 Sport 
 Life &amp; Style 
 Travel 
 Driving 
 Arts &amp; Ents 
  

  
 
 
Times Online
Times Archive
 

  
 
 
 
 
 Contact our advertising team for advertising and sponsorship in Times Online, The Times and The Sunday Times. Globrix Property Search - search houses for sale and rooms and property to rent in the UK. Milkround Job Search - for graduate careers in the UK. Visit our classified services and find jobs, used cars, property or holidays. Use our dating service, read our births, marriages and deaths announcements, or place your advertisement. 
 Copyright 2008 Times Newspapers Ltd. 
 This service is provided on Times Newspapers' standard Terms and Conditions. Please read our Privacy Policy.To inquire about a licence to reproduce material from Times Online, The Times or The Sunday Times, click  here.This website is published by a member of the News International Group. News International Limited, 1 Virginia St, London E98 1XY, is the holding company for the News International group and is registered in England No 81701. VAT number GB 243 8054 69. 
   
  










 
 

 

   





  

