

 July 25, 2007 -- Lowering cholesterol as much as possible may reduce the risk of heart disease, but with a price: taking it too low could raise the risk of cancer, U.S. researchers reported yesterday.  
  Patients who took statin drugs to lower their cholesterol had a slightly higher risk of cancer, although the study did not show that the statin drugs themselves caused the cancer.  
  The researchers found one extra case of cancer per 1,000 patients with the lowest levels of LDL - low density lipoprotein, or so-called bad cholesterol - compared to patients with higher LDL levels.  
  Dr. Richard Karas and his colleagues at Tufts University School of Medicine in Boston looked at the records of 41,173 patients for the study, which was published in the Journal of the American College of Cardiology. 

