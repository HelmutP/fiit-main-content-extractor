

 "
			NO AMOUNT OF SPIN CAN EVER MAKE LABOUR THE PARTY OF MIDDLE BRITAIN		
	



	 
 
		 
						
						   
			 
				Jack Straw: Rounded on liberal prison reformers			 
						
				
			
					 
	 
	
	 
		 
			Tuesday October 28,2008		 
	 
	 
				 
			By Macer Hall
		 
			 
	 	 
		 
			
			Have your say(7)
		 
	 
		 
		 BENEATH his ?getting on with the job? veneer, Gordon Brown has now embarked on his most?calculated spin operation yet.

 
	 





			 
				 
 Allies insist the Prime Minister?s response to the world financial crisis has transformed him from a busted flush to a political contender once again. But while the ?serious man for serious times? is portrayed as focusing on the economy virtually round the clock, Labour?s high command is ruthlessly implementing a ?save Gordon? strategy. 
   
 Take a look at the recent pronouncements of senior ministers: there is a co-ordinated drive to sound tough on the issues that the Government has repeatedly flunked during 11 years in office. 
   
 Jack Straw yesterday rounded on liberal prison reformers.  
   
 The Justice Secretary claimed soppy do-gooders who put the rights of inmates before those of victims drove him ?nuts?. He conveniently forgot his Government?s promotion of early release for thugs, non-custodial punishments and the risible Human Rights Act.  
   
 Suddenly Labour is claiming to be ?tough on crime? all over again, just days after the truth of the violent crime explosion was laid bare. 
   
 Mr Straw?s remarks on crime fit into a pattern of behaviour among ministers. Last week newly-promoted Immigration Minister Phil Woolas promised tougher border controls and hinted at capping population at 70 million. He went too far for Home Secretary Jacqui Smith?s liking and was humiliatingly withdrawn from BBC1?s Question Time. 
   
 The strategic intention, however, behind the immigration policy fiasco was obvious. Ministers wanted to signal that Labour ? having presided over a record migrant influx ? has turned tough on border controls at last. 
   
 A further example illustrates exactly which section of the electorate Labour?s strategy is targeting.  
   
 Earlier this year the Government made a spectacular U-turn by ditching heavy-handed enforcement of metric weights and measures. Skills Secretary John Denham announced that prosecutions of shopkeepers and stallholders selling their wares in pounds and ounces were regarded as ?not in the public interest?. So after years of persecuting so-called ?metric martyrs?, the Government wants to convince us it has surrendered to common sense. 
   
 With a small gesture of retreat, ministers hoped to portray themselves as siding with the middle class for once. 
   
 Crime, immigration and  petty bureaucracy are issues that have most rankled with Middle Britain under Labour.  
   
 It is no coincidence ministers are engaged in a series of stunts to neutralise their toxicity. Middle-income swing voters in marginal constituencies are crucial if Labour is to avoid meltdown at the next election. So a strategy designed to address those simmering frustrations has been formulated at Number 10 ? and the economic crisis has given Labour an opportunity to deploy its forces in pursuing that goal.  
   
 Brown can play the statesman bestriding the international stage while his Cabinet henchmen make soothing noises about tackling the more prosaic everyday irritations endured by middle-class voters. 
   
 Brown himself signalled that a major spin offensive to cosy up to Middle Britain was ready to roll in his Labour conference speech. ?On the side of hard-working families is the only place I?ve ever wanted to be; and from now on it?s the only place I ever will be,? he vowed. 
   
 At the time, that claim to be ?on your side? was viewed by critics as the Prime Minister?s most audacious subterfuge yet. 
   
 But Brown knows no other way of operating politically. Indeed, his strategy is a return to the classic New Labour approach he helped devise. It is about using the language of the centre ground to implement the policies of the Left. 
   
 With Peter Mandelson back at the Cabinet table and Alastair Campbell lurking in the background, it is clear that a concerted drive to spin Labour into contention at the next election is well underway. 
   
 Yet their strategy is fraught with dangers. While Brown assumed office claiming to have dispensed with the media manipulation of the Tony Blair era, he has been caught out time and again. The latest confusion over immigration only exposed a typical gap between Labour?s rhetoric and reality. And other attempts at addressing the niggling irritations experienced by many families have been equally unsuccessful. 
 
                                           
 Earlier this year Downing Street declared an end to punitive taxes on excessive household rubbish, only for the hated scheme to be hastily resurrected to fit in with environmental directives imposed by Brussels. 
   
 But Brown?s claim to be ?on the side of hard-working families? is most likely to be exposed as a sham by the continuing growth of taxation. Over the past decade, those same families have been hammered by ever-growing stealth taxes on fuel, pensions, property and inheritance. 
   
 Now the Prime Minister intends to hike state spending to ever more stratospheric levels in the hope of reversing the slide into recession. In the immediate term, he plans massively increased Treasury borrowing to pay for grandiose public works schemes totalling up to ?100billion. But that debt will have to be repaid, with interest. And that can only mean a fresh onslaught of tax robbery far outstripping anything even Brown has got away with in the past. 
   
 He may dream of siding with hard-working families but that is hardly possible while he is rapaciously raiding their incomes, savings and pensions once again. 
   
 On the side of Middle Britain? With Brown and his gang as friends, who needs enemies?"
"3974","
			By Macer Hall
		
			 
	 	 
		 
			
			Have your say(7)
		 
	 
		 
		 BENEATH his ?getting on with the job? veneer, Gordon Brown has now embarked on his most?calculated spin operation yet.

 
	 





			 
				 
 Allies insist the Prime Minister?s response to the world financial crisis has transformed him from a busted flush to a political contender once again. But while the ?serious man for serious times? is portrayed as focusing on the economy virtually round the clock, Labour?s high command is ruthlessly implementing a ?save Gordon? strategy. 
   
 Take a look at the recent pronouncements of senior ministers: there is a co-ordinated drive to sound tough on the issues that the Government has repeatedly flunked during 11 years in office. 
   
 Jack Straw yesterday rounded on liberal prison reformers.  
   
 The Justice Secretary claimed soppy do-gooders who put the rights of inmates before those of victims drove him ?nuts?. He conveniently forgot his Government?s promotion of early release for thugs, non-custodial punishments and the risible Human Rights Act.  
   
 Suddenly Labour is claiming to be ?tough on crime? all over again, just days after the truth of the violent crime explosion was laid bare. 
   
 Mr Straw?s remarks on crime fit into a pattern of behaviour among ministers. Last week newly-promoted Immigration Minister Phil Woolas promised tougher border controls and hinted at capping population at 70 million. He went too far for Home Secretary Jacqui Smith?s liking and was humiliatingly withdrawn from BBC1?s Question Time. 
   
 The strategic intention, however, behind the immigration policy fiasco was obvious. Ministers wanted to signal that Labour ? having presided over a record migrant influx ? has turned tough on border controls at last. 
   
 A further example illustrates exactly which section of the electorate Labour?s strategy is targeting.  
   
 Earlier this year the Government made a spectacular U-turn by ditching heavy-handed enforcement of metric weights and measures. Skills Secretary John Denham announced that prosecutions of shopkeepers and stallholders selling their wares in pounds and ounces were regarded as ?not in the public interest?. So after years of persecuting so-called ?metric martyrs?, the Government wants to convince us it has surrendered to common sense. 
   
 With a small gesture of retreat, ministers hoped to portray themselves as siding with the middle class for once. 
   
 Crime, immigration and  petty bureaucracy are issues that have most rankled with Middle Britain under Labour.  
   
 It is no coincidence ministers are engaged in a series of stunts to neutralise their toxicity. Middle-income swing voters in marginal constituencies are crucial if Labour is to avoid meltdown at the next election. So a strategy designed to address those simmering frustrations has been formulated at Number 10 ? and the economic crisis has given Labour an opportunity to deploy its forces in pursuing that goal.  
   
 Brown can play the statesman bestriding the international stage while his Cabinet henchmen make soothing noises about tackling the more prosaic everyday irritations endured by middle-class voters. 
   
 Brown himself signalled that a major spin offensive to cosy up to Middle Britain was ready to roll in his Labour conference speech. ?On the side of hard-working families is the only place I?ve ever wanted to be; and from now on it?s the only place I ever will be,? he vowed. 
   
 At the time, that claim to be ?on your side? was viewed by critics as the Prime Minister?s most audacious subterfuge yet. 
   
 But Brown knows no other way of operating politically. Indeed, his strategy is a return to the classic New Labour approach he helped devise. It is about using the language of the centre ground to implement the policies of the Left. 
   
 With Peter Mandelson back at the Cabinet table and Alastair Campbell lurking in the background, it is clear that a concerted drive to spin Labour into contention at the next election is well underway. 
   
 Yet their strategy is fraught with dangers. While Brown assumed office claiming to have dispensed with the media manipulation of the Tony Blair era, he has been caught out time and again. The latest confusion over immigration only exposed a typical gap between Labour?s rhetoric and reality. And other attempts at addressing the niggling irritations experienced by many families have been equally unsuccessful. 
 
                                           
 Earlier this year Downing Street declared an end to punitive taxes on excessive household rubbish, only for the hated scheme to be hastily resurrected to fit in with environmental directives imposed by Brussels. 
   
 But Brown?s claim to be ?on the side of hard-working families? is most likely to be exposed as a sham by the continuing growth of taxation. Over the past decade, those same families have been hammered by ever-growing stealth taxes on fuel, pensions, property and inheritance. 
   
 Now the Prime Minister intends to hike state spending to ever more stratospheric levels in the hope of reversing the slide into recession. In the immediate term, he plans massively increased Treasury borrowing to pay for grandiose public works schemes totalling up to ?100billion. But that debt will have to be repaid, with interest. And that can only mean a fresh onslaught of tax robbery far outstripping anything even Brown has got away with in the past. 
   
 He may dream of siding with hard-working families but that is hardly possible while he is rapaciously raiding their incomes, savings and pensions once again. 
   
 On the side of Middle Britain? With Brown and his gang as friends, who needs enemies? 
 
 

