

 Federal authorities should focus on enforcing existing gun laws rather than passing new ones to reduce crime, the leading Republican candidates for U.S. Senate said. 
 
 In response to a Tribune survey on criminal justice and 2nd Amendment issues, the candidates all said the constitution guarantees the right to own firearms and states should decide whether citizens should be allowed to carry concealed weapons. 
 
  ""We need better enforcement of existing gun laws, and no further restrictions upon the 2nd Amendment,"" state Sen. Steve Rauschenberger (R-Elgin) said, summing up his position on gun laws, and encapsulating those of his rivals. 
  

                     
                        
                        
							
                            
                                

                                

                                

                            
                        

                        
                        




    
        
            
        
    

                        

                        
                         
                            
                                
                            
                         
                        
                     


                     
 Rauschenberger is facing former investment banker Jack Ryan, retired Air Force Gen. John Borling, Glenview businessman Andy McKenna Jr. and dairy owner James Oberweis in the March 16 primary for the seat of retiring U.S. Sen. Peter Fitzgerald (R-Ill.). 
 
 Though the five leading contenders generally agree on matters of gun control, their priorities differ on other matters. 
 
 McKenna, for instance, said he supports ""tougher criminal penalties for white-collar crime,"" in cases exemplified by businesses such as Enron, WorldCom and Tyco International, all of which have been wracked by executive scandals in recent years. 
 
 ""We should send a clear signal to corporate insiders that we will not tolerate self-dealing or unfair practices that undermine both the investments and the confidence of individual investors,"" McKenna said. 
 
 Oberweis, who has made illegal immigration a key issue in his campaign, said he supports the Clear Law Enforcement for Criminal Alien Removal Act, known by the acronym CLEAR. Sponsored by Rep. Charlie Norwood (R-Ga.), the act would encourage local law enforcement to ""investigate, apprehend, detain or remove aliens."" 
 
 Critics, however, say the law would result in a fundamental change in Americans' right to freedom of movement, essentially requiring for the first time in the nation's history that U.S. citizens carry proof of their citizenship. 
 
 In a previous survey, all the candidates expressed some reservations about the USA Patriot Act. Alone among the candidates, however, Rauschenberger pointed to portions of the act as ripe for repeal. 
 
 ""Provisions of the Patriot Act that grant the federal government the power to deny a U.S. Citizen [his] 6th Amendment right to counsel ought to be repealed,"" Rauschenberger said. 
 
 Though agreeing with the other contenders that better enforcement is more necessary than additional criminal statutes, Ryan asserted that making the criminal justice system more fair requires a deeper examination of whether the nation's laws match the legal system's goals. 
 
 ""There are few areas of American life that lack regulation by the federal government, even as it pertains to criminal statues,"" Ryan said. ""Increasing or decreasing existing laws would negligibly impact the overall safety of our communities and our country. Examining the operation of our system in practice under the legal constructs we have erected would likely bear more fruit if the objective is to get closer to the goals envisioned."" 
 
 One of the most vigorous criminal justice debates in recent years has centered on the death penalty. Indicted former Illinois Gov. George Ryan--who is not related to Jack Ryan, and whose indictment has cast a pall over the state's GOP--helped to put the issue front-and-center by commuting the death sentences of all Illinois Death Row inmates. 
 
 Jack Ryan said he generally opposes the use of the death penalty at the federal level, though McKenna said he supports it only in ""limited cases."" McKenna and Oberweis, cited terrorism as a reason for executing someone. 
 
 Despite Ryan's opposition to the death penalty, only McKenna said he was in favor of a moratorium on it, ""if a similar pattern of injustice as existed in Illinois can be demonstrated at the federal level."" 
 
 On another hot-bottom issue for some conservative voters, the candidates all indicated they are friendly to the idea of allowing people to carry concealed firearms, which Ryan said now is a right under the laws of 37 states. All the candidates except Rauschenberger stated that they believe the issue should be decided by states, while Rauschenberger stated he has voted in favor of such laws in Illinois. 
 
 ""The evidence is clear in the states that do have these laws that violent crime goes down, not up,"" Borling said, alluding to recent studies that are roundly criticized by gun-control advocates. ""When criminals know there may be a level playing field, they are less likely to want to play."" 
 
 All the candidates also said they would support a proposal to ban federal suits against gun-shop owners who unknowingly sell firearms to criminals. Ryan said shop owners should not be held liable if they ""did not conspire in any way to facilitate criminal activity."" 
 
 On the War on Drugs, which some conservative Republicans consider a misguided effort akin to Prohibition, Borling was largely critical of the current approach to decreasing the availability and use of drugs, calling it ""ineffective and wasteful."" 
 
 Most of the candidates generally supported the federal government's efforts but offered suggestions for better pursuing drug interdiction. 
 
 Federal ""judges should be given more discretion in the application of the federal sentencing guidelines for first-time, non-violent, small possession drug offenders,"" Rauschenberger said. 

