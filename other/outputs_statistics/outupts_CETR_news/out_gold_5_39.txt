

 DALLAS (Reuters) - His influence may be diminished but his zeal is undaunted. Evangelist Jerry Falwell is on a mission to keep a like-minded Republican in the White House and get at least one more conservative judge on the Supreme Court. 

    


 Despite his years in the trenches of America's culture wars, Falwell -- who founded the Moral Majority political movement in 1979 and helped propel the rise of the religious right -- said a major victory in his broader crusade to restore the country's moral righteousness has so far eluded him. 

    


 With abortion still legal, prayer banned in public schools and pornography rife, he sees a long struggle ahead. For now, he is focusing on voter registration drives and rallying the faithful with his eyes on the twin prizes of the 2008 presidential election and control of the Supreme Court. 

    


 Some of his statements -- he famously blamed gays and lesbians for provoking the September 11 attacks -- have eroded some of his conservative support base. But he remains a rallying figure on the far U.S. right. 

    


 ""I think we got the social and moral issues on the front burner. But while we have made progress ... we have not won any of the battles yet,"" Falwell told Reuters in an interview. 

    


 ""It is a long road back. We are at least one U.S. Supreme Court Justice short of a socially conservative court,"" Falwell said on the sidelines of an evangelist conference in Dallas. 

    


 By a long road back Falwell was referring to his youth in the 1930s and 1940s -- a period he feels brought out the best in a strong nation that adhered to ""old fashioned values"".   

