
 "CPA 10th Congress: work and achievements of the party  

Peter Symon  

Thousands of members and supporters of the Communist Party have played an enormous part in the progressive reforms that have been achieved in Australian society since the Party was first formed 85 years ago.  

The Party has always been on the side of the working people and some of its earliest members were trade union leaders who struggled hard to improve workers? conditions and wages which at that time, in the 1920s, were very poor. Since British colonisation Australia has been a capitalist country with the employers out for maximum profits.  

The depression years  

The first big spurt of growth in Party membership occurred during the Depression years of the 1930s. Up to 30 percent of the workforce were unemployed. Workers were forced to roam the country in search of work. Many were evicted from their homes when they could not pay the rent and the Party led many anti-eviction fights and helped the families in whatever way they could.  

The scene of families barricading themselves in their homes while supporters gathered in the street and taunted the bailiffs, and the police carried out the evictions were repeated many times over. Generally, however, it was the owners of property, backed up by the state, who won out over the justified anger of the families and their supporters.  

Unemployed Workers? Movement  

It was in this period that the Unemployed Workers? Movement came into existence. Initially there was no dole payment and no food handouts either. It was the bitter struggles of the organised unemployed that eventually forced governments to make a handout of rations to stave off starvation. The Unemployed Workers? Movement also campaigned for a rent allowance, work at full award wages and no evictions.  

The Depression also stimulated a growing militancy in the workforce in the face of a concerted employer offensive. It was as a result of the work of Party activists among the workers that the Party gained respect and provided leadership. This was particularly so on the coalfields where communists were elected to the leadership of the Miners? Federation. Rank and file committees began to be established in some workplaces.  

In an effort to unite militant workers in the trade union movement, the Militant Minority Movement (the MMM) was formed. The response of the capitalist class to the collapse of the economy in the years of the Great Depression was to step up their suppression of the working class. The trade unions and all progressive organisations were violently attacked.  

Extreme right-wing, fascist elements set up the New Guard which attacked workers? meetings and the demonstrations of the unemployed. CPA members were in the forefront of the struggle against the New Guard attacks.  

Nazi goon squads  

When Hitler and his Nazi party came to power in Germany in 1933 it became clear that Nazism was the ""terrorist dictatorship"" of the big monopoly companies and that they intended to violently suppress every progressive organisation. The Nazi goon squads roamed the streets of German cities attacking meetings of workers, breaking strikes and smashing up the offices of progressive organisations. Nazism was virulently racist, not only directed against the Jews but all other non-Aryan races. Nazism also meant war and Hitler declared his aim to be world domination.  

In the 1930s and ?40s tens of thousands of people joined in the worldwide movement against war and fascism.  

It was the time of the Spanish Civil War and the formation of the International Brigade, made up of volunteers from many countries around the world. Its members travelled to Spain to defend the elected republican government which was under attack from the Franco fascists. Australian communists were among those who enlisted in the International Brigade to fight fascism with arms in hand.  

During the course of World War II, about 4000 members of the Communist Party were in the armed forces. One of those was Vic Williams from Perth who is an honoured member of the Party to this day. Vic is a working class poet and a chronicler of the struggles of farmers. Both the publication of progressive literature and the plight of small farmers were important areas of activity of Party members.  

The smashing of fascism created a new situation in the world. The Soviet Union, which played the foremost part in the military defeat of fascism, emerged as a great world power with enormous prestige. New socialist countries came into existence following the war and in 1949, the Chinese people?s revolution was crowned with victory.  

This panicked the leaders of the Western imperialist countries and they launched a political, propaganda, economic and military crusade against the socialist states. It became known as the Cold War. An economic blockade was imposed on all the socialist countries and billions of dollars were spent to undermine them.  

Ban the bomb  

Opposition to the Cold War and the build up of nuclear weapons became a main issue in this period. The ""Ban the Bomb"" movement ? to stop the testing of nuclear weapons and to eliminate all stocks of nuclear weapons ? was supported by millions of people around the world. Members of the Communist Party played a leading role in the various peace organisations.  

It was a period of intense anti-communism. In Australia every strike struggle was portrayed as a ""communist plot"", communists were alleged to be spies for the Soviet Union, communists were said to stand for dictatorship as against the ""democratic"" system of the Western countries.  

As a part of this anti-Communist campaign the Western governments and their agencies worked hard to undermine and split the communist parties from within. A whole range of ideas were propagated. Sometimes these ideas appeared as being ""super-militant"" and ""super-revolutionary"". Others claimed that capitalism had changed and now had a ""human face"" and that existing democratic institutions cleared the way for easy change in the future to a socialist society.  

These ideas became quite widespread and were behind the various splits that took place in the communist movement of Australia. To this extent our opponents succeeded. These same false ideas and illusions played a major role in the break-up of the Soviet Union and the other socialist countries of Eastern Europe. However, the anti-Communists were never able to kill off Marxist ideas which guide the Communist Parties ? the same ideas that underlie the current ""Political Resolution"" and Party Program which will be finalised by the Party Congress over the weekend of October 1, 2, and 3 this year.  

The CPA?s members campaigned against the introduction of the GST which by its very nature is an unfair tax. We campaigned for the maintenance of Medicare as a free and universal health system.  

We opposed the program of privatisation of publicly owned enterprises and government services that has been implemented by both Labor and Liberal governments. The CPA opposes the sale of Australian resources to foreign corporations. More and more of the economy is being placed under the control of the big transnational corporations.  

We are now engaged in extremely important campaigns against the Howard government?s IR legislation, the full privatisation of Telstra, terrorism and ASIO laws, and the voluntary student union legislation. These government measures are to be rammed through parliament and backed by severe attacks on democratic rights. The extreme right-wing, conservative attacks on working class and progressive organisations is once again being stepped up.  

Socialism better  

We have continued to maintain that socialism is a better economic, political and social system than capitalism. It is a system that is committed to meet the needs of the working people and not the selfish interests of exploiters. It means jobs, more democracy and public ownership, environmental protection, better education and health services, more for young people and real economic and social equality for women. Instead of the individualism propagated by capitalism, socialism brings a cooperative society into existence.  

The Party has all these years of campaigning and struggle to its credit and many conditions at work and in society were achieved. Our Party?s campaigns and activities will continue.  

It is on this background of years of struggle and achievement that the 10th Congress of the Communist Party of Australia is being held."
"3105","Peter Symon  

Thousands of members and supporters of the Communist Party have played an enormous part in the progressive reforms that have been achieved in Australian society since the Party was first formed 85 years ago.  

The Party has always been on the side of the working people and some of its earliest members were trade union leaders who struggled hard to improve workers? conditions and wages which at that time, in the 1920s, were very poor. Since British colonisation Australia has been a capitalist country with the employers out for maximum profits.  

The depression years  

The first big spurt of growth in Party membership occurred during the Depression years of the 1930s. Up to 30 percent of the workforce were unemployed. Workers were forced to roam the country in search of work. Many were evicted from their homes when they could not pay the rent and the Party led many anti-eviction fights and helped the families in whatever way they could.  

The scene of families barricading themselves in their homes while supporters gathered in the street and taunted the bailiffs, and the police carried out the evictions were repeated many times over. Generally, however, it was the owners of property, backed up by the state, who won out over the justified anger of the families and their supporters.  

Unemployed Workers? Movement  

It was in this period that the Unemployed Workers? Movement came into existence. Initially there was no dole payment and no food handouts either. It was the bitter struggles of the organised unemployed that eventually forced governments to make a handout of rations to stave off starvation. The Unemployed Workers? Movement also campaigned for a rent allowance, work at full award wages and no evictions.  

The Depression also stimulated a growing militancy in the workforce in the face of a concerted employer offensive. It was as a result of the work of Party activists among the workers that the Party gained respect and provided leadership. This was particularly so on the coalfields where communists were elected to the leadership of the Miners? Federation. Rank and file committees began to be established in some workplaces.  

In an effort to unite militant workers in the trade union movement, the Militant Minority Movement (the MMM) was formed. The response of the capitalist class to the collapse of the economy in the years of the Great Depression was to step up their suppression of the working class. The trade unions and all progressive organisations were violently attacked.  

Extreme right-wing, fascist elements set up the New Guard which attacked workers? meetings and the demonstrations of the unemployed. CPA members were in the forefront of the struggle against the New Guard attacks.  

Nazi goon squads  

When Hitler and his Nazi party came to power in Germany in 1933 it became clear that Nazism was the ""terrorist dictatorship"" of the big monopoly companies and that they intended to violently suppress every progressive organisation. The Nazi goon squads roamed the streets of German cities attacking meetings of workers, breaking strikes and smashing up the offices of progressive organisations. Nazism was virulently racist, not only directed against the Jews but all other non-Aryan races. Nazism also meant war and Hitler declared his aim to be world domination.  

In the 1930s and ?40s tens of thousands of people joined in the worldwide movement against war and fascism.  

It was the time of the Spanish Civil War and the formation of the International Brigade, made up of volunteers from many countries around the world. Its members travelled to Spain to defend the elected republican government which was under attack from the Franco fascists. Australian communists were among those who enlisted in the International Brigade to fight fascism with arms in hand.  

During the course of World War II, about 4000 members of the Communist Party were in the armed forces. One of those was Vic Williams from Perth who is an honoured member of the Party to this day. Vic is a working class poet and a chronicler of the struggles of farmers. Both the publication of progressive literature and the plight of small farmers were important areas of activity of Party members.  

The smashing of fascism created a new situation in the world. The Soviet Union, which played the foremost part in the military defeat of fascism, emerged as a great world power with enormous prestige. New socialist countries came into existence following the war and in 1949, the Chinese people?s revolution was crowned with victory.  

This panicked the leaders of the Western imperialist countries and they launched a political, propaganda, economic and military crusade against the socialist states. It became known as the Cold War. An economic blockade was imposed on all the socialist countries and billions of dollars were spent to undermine them.  

Ban the bomb  

Opposition to the Cold War and the build up of nuclear weapons became a main issue in this period. The ""Ban the Bomb"" movement ? to stop the testing of nuclear weapons and to eliminate all stocks of nuclear weapons ? was supported by millions of people around the world. Members of the Communist Party played a leading role in the various peace organisations.  

It was a period of intense anti-communism. In Australia every strike struggle was portrayed as a ""communist plot"", communists were alleged to be spies for the Soviet Union, communists were said to stand for dictatorship as against the ""democratic"" system of the Western countries.  

As a part of this anti-Communist campaign the Western governments and their agencies worked hard to undermine and split the communist parties from within. A whole range of ideas were propagated. Sometimes these ideas appeared as being ""super-militant"" and ""super-revolutionary"". Others claimed that capitalism had changed and now had a ""human face"" and that existing democratic institutions cleared the way for easy change in the future to a socialist society.  

These ideas became quite widespread and were behind the various splits that took place in the communist movement of Australia. To this extent our opponents succeeded. These same false ideas and illusions played a major role in the break-up of the Soviet Union and the other socialist countries of Eastern Europe. However, the anti-Communists were never able to kill off Marxist ideas which guide the Communist Parties ? the same ideas that underlie the current ""Political Resolution"" and Party Program which will be finalised by the Party Congress over the weekend of October 1, 2, and 3 this year.  

The CPA?s members campaigned against the introduction of the GST which by its very nature is an unfair tax. We campaigned for the maintenance of Medicare as a free and universal health system.  

We opposed the program of privatisation of publicly owned enterprises and government services that has been implemented by both Labor and Liberal governments. The CPA opposes the sale of Australian resources to foreign corporations. More and more of the economy is being placed under the control of the big transnational corporations.  

We are now engaged in extremely important campaigns against the Howard government?s IR legislation, the full privatisation of Telstra, terrorism and ASIO laws, and the voluntary student union legislation. These government measures are to be rammed through parliament and backed by severe attacks on democratic rights. The extreme right-wing, conservative attacks on working class and progressive organisations is once again being stepped up.  

Socialism better  

We have continued to maintain that socialism is a better economic, political and social system than capitalism. It is a system that is committed to meet the needs of the working people and not the selfish interests of exploiters. It means jobs, more democracy and public ownership, environmental protection, better education and health services, more for young people and real economic and social equality for women. Instead of the individualism propagated by capitalism, socialism brings a cooperative society into existence.  

The Party has all these years of campaigning and struggle to its credit and many conditions at work and in society were achieved. Our Party?s campaigns and activities will continue.  

It is on this background of years of struggle and achievement that the 10th Congress of the Communist Party of Australia is being held. 
