


 By Parisa Hafezi 


 TEHRAN (Reuters) - Iranian President Mahmoud Ahmadinejad marked the 28th anniversary of Iran's revolution on Sunday pledging to pursue the country's nuclear program but announcing no new atomic work that would have riled "" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:16PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      U.S.
      
      &gt;
      Article
 
 
 
       Iran says wants to stay within nuclear rules 
       Sun Feb 11, 2007 1:13PM EST 
      
     

      
		

		 

	
	 
		

		

		

		 

 

   
          Video  
             
          
			Iran 'won't suspend atomic work' 
			Play Video
		 
         

 


 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		Top News
     
   
   
		 
				Obama sees new generation of leadership
  |  Video

			 
		 
				Olmert non-committal on Palestinian unity pact

			 
		 
				Hillary Clinton faces tough questions over Iraq

			 
		 
				Taliban prepare for spring offensive in Afghan south

			 
		 More Top News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
     By Parisa Hafezi 

    


 TEHRAN (Reuters) - Iranian President Mahmoud Ahmadinejad marked the 28th anniversary of Iran's revolution on Sunday pledging to pursue the country's nuclear program but announcing no new atomic work that would have riled the West. 

    


 Ahmadinejad, under pressure at home to tone down speeches his critics say have helped push Iran toward international isolation, said he would keep within international regulations but still ruled out a UN demand to suspend uranium enrichment. 

    


 Iran has until February 21 to halt uranium enrichment, a process that can make fuel for power stations or, if greatly enriched, material for warheads. A UN sanctions resolution passed in December threatened further measures if Iran refuses. 

    


 ""We are ready for talks but will not suspend our activities,"" Ahmadinejad told hundreds of thousands of Iranians in Tehran's Azadi (Freedom) square to mark the 1979 Islamic revolution, saying suspension would be ""humiliation"". 

    


 Iran's chief nuclear negotiator, Ali Larijani, met European Union foreign policy chief Javier Solana in Germany on Sunday to discuss the row. Solana said after the meeting no deal had been reached but possible solutions were being explored. 

    


 The United States, which has stepped up pressure on Iran by sending a second aircraft carrier to the Gulf, has been adamant it would not accept anything short of full suspension. 

    


 French Foreign Minister Philippe Douste-Blazy also said the international community's demand was ""exceedingly clear"".  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				
						

				 
Jakarta flood clean-up could take months
 
				 mud and debris from flood-damaged homes on Sunday after days of relatively dry weather, but for many it could be one or two months before they can actually move back into their houses. 
                
                    Full Article  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Dixie Chicks could bring political edge to Grammys
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















