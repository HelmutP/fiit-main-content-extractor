

 "
//W3C//DTD XHTML 1.0 Transitional//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd""&gt;

   
    RT? News: World's fattest man ties the knot
    
    
    
    
    
    
    
    
    

















  
  
     
       
	 
		 
			 Mobile Edition 
			 RT? Extra 
			 A-Z 
		 
	 
	 
		 
			 
RTE.ie  
			 
News  
			 
Sport  
			 
Business  
			 
Entertainment  
			 
Television  
			 
Radio  
			 
RT? Aertel  
			 Performing Groups 
			 
Den?TTV
 
			 
RTÉ Guide  
			 
About RT?  
		 
	 
	 
		 
			 
Fashion  
			 
Food  
			 
Motors  
			 
Travel  
			 
Property  
			 
Jobs  
			 Shop 
			 
Weather  
			 
Money  
			 
RT? Mobile  
			 
Live TV  
			
		 
	 
	 
		
			
				
					
					

					
					

					
					

					Search Web
					
					

					Search RT?.ie
					
					

				
			
		
	 
 

       
        		   
		     
			News
		     
   
             
               
                Home
 
               
                Full News Index
 
               
TV Programmes
				   
					 One News 
					 Six One News 
					 Nine News 
					 Prime Time  
					 Prime Time Investigates 
					 Questions &amp; Answers 
					 The Week In Politics 
					 Nationwide 
					 Pobal 
					 Capital D  
					 One To One 
					 News on Two 
					 Oireachtas Report 
					 Euro Report 
					 News2Day 
					 Nuacht 
				   
			   
               
Radio Programmes
				   
					 Morning Ireland 
					 News At One 
					 Drivetime 					
					 World Report 
					 This Week 
			
				   
			 
						 US Election 
			
               			  
                Special Reports
	                           
	               RT? News China 
      		        Global Food Crisis 
      			 Climate Change 
      			 Lisbon Treaty 
			 US Election 
			 Home Features 
		       World Features 
	                Past Special Reports 
				 
			   	
			   
                Webchats
	                           
			 Robert Shortt 
			 Election Debate 				
	               Aine Lawlor 				
	               George Lee 				
	               Fergal Bowers 				
	               Bryan Dobson 
	               Miriam O'Callaghan 
	               Charlie Bird 
				 
			   	
			 
 
			   Budget 2009
 	              
			   Nuacht
 			  
			 		  
			   
                RT?.ie/live Schedule 
 
			   
                Weather
 
			   
                Using Audio/Video
 
               
                Email Services
 
               
                Contact Us
 
               
                News Feeds
 
               
                Text Alerts
 
             			
         
		  			
          

		 
		 
		     
		      
		        Archive
		      
		     
    		   
	
		
			&lt;&lt;
			
			&gt;&gt;
		
		
			Mo
			Tu
			We
			Th
			Fr
			Sa
			Su
		
		
			
			
			
			
			
			
			
		
		
			
			
			
			
			
			
			
		
		
			
			
			
			
			
			
			
		
		
			
			
			
			
			
			
			
		
		
			
			
			
			
			
			
			
		
		
			
			
			
			
			
			
			
		
	
	
   


          

	 
           
          

         
           
            World
           
           
             World's fattest man ties the knot 
            Monday, 27 October 2008 11:57
           
           
            
 Manuel Uribe, the world's fattest man, has tied the knot in a TV ceremony the US Discovery Channel dubbed 'My Big Fat Mexican Wedding'. 
 Despite shedding 230 kilograms (570 pounds) from 590 kilograms early on this year, Mr Uribe, 43, had to be carried by a crane on his bed, where he has been confined for years, to the makeshift altar at a venue 30 minutes from his home. 
 The ceremony was televised by Discovery as part of a special, exclusive program titled after the 2002 US movie 'My Big Fat Greek Wedding', with other reporters from Japan, Germany and Argentina in tow.  
 
 Advertisement   
 No other cameras or cellphones were allowed inside. 
 'They're married. Now everybody's clapping their hands and congratulating the couple,' Monterrey Mayor and wedding witness Cristina Diaz said. 
 Uribe has been suffering from morbid obesity since 1992. He was gripped by suicidal thoughts until February of this year when he undertook a drastic diet. 
 He and his wife, Claudia Solis, first met four years ago at the deathbed of a friend of his who weighed 250 kilograms (551 pounds). 
 The wedding reception had a 'low-calorie banquet' with meat, cream of mushroom and buttered vegetables. 

           
          		 
			
				
			
			 
				printable version
			 
			
				
			
			 
				share this
			 
		 

         
         
           
             
              
              
               
Manuel Uribe &amp; Claudia Solis Tie the knot  
             
           
           
	

	 
	   Most Popular Stories 
	   Top Stories 
	 

	 
		 
		   News 
		   Sport 
		   Business 
		   Entertainment 
		 
	
		 
		 
	 

	

 

          
 
	
 	



 
   
      LIVE TV
   
   
	
 
   
     Now: 
     
      Prime Time
       
      21:35 Tuesday 28 October 
     
   
   
     Next: 
     
      The Podge and Rodge Show
       
      22:50 Tuesday 28 October 
     
   
   	
   
	
 


     
       
         Television Programmes 
		
			[select show]
			- One News
			- Six One News
			- Nine News
			- Prime Time
			- Prime Time Investigates
			- Questions &amp; Answers
			- The Week In Politics			
			- News on Two			
			- Election Programmes
			- Nationwide
			- Pobal
			- Capital D
			- Oireachtas Report
			- One to One
			- Euro Report
			- Nuacht
			- news2day
		
       
		
       
         Radio Programmes 
		
			[select show]
			- Morning Ireland
			- News At One
			- Drivetime					
			- World Report
			- This Week
		
       
     


           
   RT?.ie News Highlights 
   
     
       
        Is No One Listening?
       
      
        
      
       
 Ever been messed around by a service provider? Prime Time Investigates wants to hear from you. Click here for details  
       
        
          
        
       
     
     
     
     
       
        Time to Buy?
       
      
        
      
       
 RT? Money explains what the housing measures in the Budget mean for first-time buyers 
       
        
          
        
       
     
     
     
     
       
        Jump The Slump
       
      
        
      
       
 Find out how some people manage to keep the 'pep in their step' despite the economic gloom 
       
        
          
        
       
     
     
     
     
       
        Dublin Mountain Rambles
       
      
        
      
       
 The Dublin Mountains Partnership is encouraging people to use the hills and mountains outside the capital more 
       
        
          
        
       
     
     
     
   
 
         
         ? 
       
       
 
	 © RTÉ 2008 
	 RT? Commercial Enterprises Limited, Registered in Dublin,  Registration Number:155076.  
	   Registered Office: Donnybrook, Dublin 4, Ireland.  
	 Terms &amp; Conditions - RTÉ Privacy Statement - TV Licence - Contact Us
	    - Sports photos provided by Inpho 
		Wire services provided by Reuters, APTN and PA 
	    
	   
 
 	
 
     
     

    



 

     

	



 

     

	






 



     
	 
		
		
		
	 

	 
		
		
		
	 

	
 



    
  

