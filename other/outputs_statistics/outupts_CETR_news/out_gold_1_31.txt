

 Former Pistons and Lakers coach passes
				
				
				 
 August 24, 2007 
				
				 FREE PRESS NEWS SERVICES 
				
 

				 


SPOKANE, Wash. -- Former Pistons coach Butch van Breda Kolff, a happy-go-lucky nonconformist who from 1951 through 1994 coached more than 1,300 college, professional and high school games, died Wednesday in Spokane, Wash. He was 84. 
 


He held 13 head coaching jobs, and for one season, at age 61, he coached a high school team. 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


""I've had some good jobs that I've left, or they fired me,"" he once said. ""At the time, I thought it was the right thing for me to do. Whether it turned out right later, who cares?"" 


Van Breda Kolff was fired after coaching the Los Angeles Lakers to the 1968 and '69 NBA Finals (both losses to the Boston Celtics) and joined the Pistons for the '69-70 season. He was 82-92 as Pistons coach over two-plus seasons, never reached the playoffs and was let go after a 6-4 start in '71-72. 
 


He posted a 482-272 coaching record in 28 college seasons, and was 287-316 in 10 seasons as a professional coach. 
 


His coaching style never changed. When he was 71 and coaching his final season, the New York Times described him as the ""animated, nonstop-gesticulating, chair-kicking, sideline-pacing, expletive-spewing Butch of days gone by."" But his teams were well-schooled, emphasizing teamwork, a patient offense and a tough defense. 
 


Van Breda Kolff played for Princeton and coached at his alma mater in 1962-67. He played professionally for the New York Knicks in 1946-1950, averaging 4.7 points a game. 
 


New York: Former Knicks captain Allan Houston told USA Today he is ready to make a comeback, two years after he retired because of knee problems. Houston, 36, said that two teams have contacted him and that playing for the Knicks isn't out of the question. 
 


San Antonio: The Spurs signed French power forward Ian Mahinmi, 20, whom they drafted with the 28th selection in the first round in 2005. 
 


Seattle: The NBA fined Sonics co-owner Aubrey McClendon $250,000 two weeks after he said his group didn't buy the team to keep it in Seattle. McClendon, an Oklahoma City tycoon, said he hoped to move the team there. 
 


FIBA Americas tournament: Samuel Dalembert scored 18 points, six in the final 2:05, and Canada beat Venezuela, 80-73, in Las Vegas. The Philadelphia 76ers' center, who was born in Haiti and recently became a Canadian citizen, also grabbed eight rebounds and blocked five shots. ... Former Piston Carlos Arroyo had 16 points and eight assists for Puerto Rico in a 108-67 win over Panama. ... Carmelo Anthony scored 17 points and Kobe Bryant added 14 for the U.S. in a 112-69 win over Venezuela late Wednesday night. 

