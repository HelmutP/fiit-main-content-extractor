
 You get a chance to start over every year in your retirement investment account. There are no tax consequences. There's no one looking over your shoulder to laugh at your past mistakes or criticize your current choices. Once you switch from one fund to another, you can forget about losses or bad timing. It's a new year and a new chance to allocate your assets. 
 
But most people won't take that chance. Instead, they figure that if they've made one mistake, they're even more likely to make another if they switch. 
 
 
That's what's behind the refusal to review and, if necessary, to reallocate investments at the start of the year.  
 
The best way to reach your long-term goals is to make a plan--and stick to it. If that plan includes a regular monthly investment in a diversified stock fund, then the absolutely wrong thing to do is to stop investing when prices fall. You're losing the opportunity to buy more shares at lower prices. 
 
You have to take the long-term (15 to 20 years) view. 
 
You can avoid having to make tough decisions by simply choosing an index fund for most of your retirement plan, and taking smaller positions in the other equity alternatives. That's the kind of allocation you can stick with until you're getting close to retirement. 
 
If, on the other hand, you chose riskier alternatives such as momentum or technology funds for most of your retirement plan investments, it's time to take another look. 
 
These are not one-decision funds. You have to figure out when to get out, as well as when to get in. 
 
Having spent much time on the trading floors, I can assure you that even the best and boldest traders make mistakes--including some big ones. But the one thing successful traders and investors have in common is the discipline to re-evaluate past decisions in light of current facts. 
 
This year, the facts have changed.  
 
No matter what the label--recession, slowdown, soft landing--the economy is certainly in a different place than when we started last year. Not only have realities changed, perceptions have changed, as well. 
 
So whether inside your retirement plan, or in your trading account, you'll have to make some decisions. Now the big question you face is whether you'll go into this year with last year's guidelines--or whether you're willing to put it all behind you and move forward into the new reality. 
 
This is the perfect weekend to review your retirement plan investment choices. And there's plenty of help for the process.  
 
Many companies have come to recognize that they have a responsibility to help their employees find intelligent, impartial, and unbiased advice on choosing investments within a 401(k) plan. 
 
They're aligning with online advice providers such as mPower and FinancialEngines--services that are also available at a moderate cost to individual investors. 
 
Check out www.Financial Engines.com. Started by economic theorist William Sharpe, who won the Nobel Prize for Modern Portfolio Theory, this site uses a sophisticated computer program to advise investors on asset allocation. 
 
It asks you to securely input personal information about income, age and goals, and then recommends allocation between your retirement plan investment choices. The cost is only $14.95 per quarter for one account, slightly more if you want to include non-retirement assets such as your IRA or trading account. 
 
Chicago-based Morningstar offers its version of this asset-allocation service at www.Morning star.com. Just click on ""401k Advice,"" and the site will walk you through the process of setting up your account. 
 
The cost is $7.95 for a three-month period. 
 
So now you have no excuse for not reviewing your investments. It's not wrong to be wrong. It's only wrong to stay wrong!  
 
And that's the Savage Truth. 
 
Terry Savage is a registered investment adviser for stocks and commodities and is on the board of directors of McDonald's Corp. and Pennzoil-Quaker State Co. Send questions via e-mail to savage@suntimes.com. Her third book, The Savage Truth on Money, recently was published by John Wiley &amp; Sons Inc. 
