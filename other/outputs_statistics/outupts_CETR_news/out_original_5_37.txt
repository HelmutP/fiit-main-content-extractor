


  By Dean Goodman 


  LOS ANGELES (Reuters) - The Grammys could take a rare step 
into the political arena on Sunday if the Dixie Chicks win the 
coveted album-of-the-year award, as observers of the music 
industry's biggest honors are predic"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     U.S. 
     International 
     Business 
     Small Business 
     Markets 
     Politics 
     Entertainment 
     Technology 
     Sports 
     Oddly Enough 
     
 
   1:29PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      News
      
      &gt;
      Entertainment
      
      &gt;
      Article
 
 
 
       Dixie Chicks could bring political edge to Grammys 
       Sun Feb 11, 2007 10:19AM EST 
      
     

      
		

		 

	
	 
		 


 
	

  
    
  

 

 
	 




						
						
						 
	 
     
	 
		 
		Entertainment News
     
   
   
		 
				Death permeates Grammy lifetime achievement awards

			 
		 
				Actor ""daunted"" by Mandela role in prison drama

			 
		 
				Anna Nicole's cause of death still a mystery
  |  Video

			 
		 
				Moore pins 'Wild Hope' on folk-flavored songs

			 
		 More Entertainment News... 
 
 
 
   



      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
      By Dean Goodman 

    


  LOS ANGELES (Reuters) - The Grammys could take a rare step 
into the political arena on Sunday if the Dixie Chicks win the 
coveted album-of-the-year award, as observers of the music 
industry's biggest honors are predicting. 

    


  The Texas country trio is reveling in its new outsider 
status after picking a fight with President George W. Bush in 
2003, and watching its popularity plummet as the 
Republican-leaning country music establishment snubbed the 
chart-topping group. 

    


  The Chicks' first album since the brouhaha, ""Taking the 
Long Way,"" yielded five Grammy nominations, including three for 
the wry single ""Not Ready to Make Nice."" The acclaimed album 
was the ninth-biggest seller in the United States last year, 
even if its commercial success fell short of its predecessors. 

    


  Grammy voters love underdogs and comeback stories, which 
could help the Dixie Chicks add to their collection of eight 
Grammys. But politically charged music arising from such 
turbulent times as Vietnam and Watergate has traditionally been 
ignored at awards time. 

    


  Indeed, for a liberal-leaning recording industry full of 
rebels and other creative misfits, there has been little 
acknowledgment in recent years that an unpopular conservative 
president is embroiled in a war that has cost the lives of more 
than 3,120 U.S. military personnel and over 50,000 Iraqis. 

    


  ""I think people are paranoid,"" former Grateful Dead member 
Mickey Hart told Reuters. ""I think that if they speak out, they 
think they're gonna get whacked by the government. It's pretty 
oppressive now. Look at the Dixie Chicks. They got whacked."" 

    


  Not exactly ""whacked"" Mafia-style. Chicks' Singer Natalie 
Maines ignited the controversy when she said during a London 
concert that she was ashamed to hail from the same state as 
Bush.
  Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
    


 
 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   

 
	 
    
 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Today On Reuters 
                     
				 
		 
				 
U.S.-led forces show evidence of Iran arms in Iraq
 
				 BAGHDAD (Reuters) - Officials of the U.S.-led coalition on Sunday showed what they said were examples of Iranian weapons used to kill 170 of their soldiers and implicated high-level Iranian involvement in training Iraqi militants. 
                
                    Full Article  |  Video  
			 
		 
Business:
			Oil, Bernanke to sway stocks
 
		 
Entertainment:
			Covington graduates from 'Idol' to Nashville
 
		 
Oddly Enough:
			Court upholds urinal attack conviction
 
		 
Technology:
			Number of HDTV homes to treble by 2011: report
 
		 
		 

 

 

 
	 
    

 
 	 Popular Searches on Reuters.com 
 
 
 
 1. Mantova
 
 2. Oil
 
 3. Iran
 
 4. Anna Nicole Smith
 
 5. Bird Flu
 
 
 
   6. Astronaut
 
   7. Lukashenko
 
   8. Iraq
 
   9. Global warming
 
 10. Rare shark
 
 
 
 

 
   

 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















