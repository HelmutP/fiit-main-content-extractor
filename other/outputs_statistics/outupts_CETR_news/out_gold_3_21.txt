

 August 22, 2007 -- A small-time ex-heavyweight boxer lured a young woman to his friend's upper Manhattan apartment, where he raped and sodomized her, authorities said yesterday.  
  Barry Lineberger, 42, of Harlem, approached the 21-year-old victim, whom he did not know, on an Upper East Side street at about 7:20 p.m. Aug. 5, law-enforcement sources said. Lineberger allegedly talked the woman into coming to the apartment by telling her there was something there he wanted to give her.  
  Instead, the hulking former boxer, who has weighed as much as 247 pounds for fights, fondled, raped and sodomized the woman, authorities said. Lineberger was arrested Saturday on charges of rape and sexual abuse.  
  Lineberger accumulated a record of 4 wins, 9 losses, winning two fights by knockout, according to the Web site BoxRec.com. He last fought in 2005.  
  His lawyer, Stephen Pokart, said the victim failed to identify his client.  
  "She has not yet said he did it," he said. "And she was shown a picture of him and she did not pick him out." 

