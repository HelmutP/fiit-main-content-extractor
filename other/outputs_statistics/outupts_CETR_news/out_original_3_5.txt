

 //W3C//DTD HTML 4.01 Transitional//EN"" ""http://www.w3.org/TR/html4/loose.dtd""&gt;

 
WALKING ON EIRE | By VENISE DOUGLAS | Entertainment News | Celebrity News | Arts &amp; Entertainment


























 
     
    
       
       
          
             	
                            
             
               NYC Weather 82 ° CLEAR              
          
          
             
          
          
             
               Saturday, August 25, 2007 
               
                   Last Update: 
		10:05 PM EDT                   
               
             
             
             
             
                     
             


			Recent
			30 days+
			The Web
			

           
       

       
            
            
			 
				Story Index
				Summer of '77
			 
			
            
            
            
           
            
            
          
               
               
               
          
          
            Post An Ad
            Marketplace
            Autos
            Dating
            Jobs
            Real Estate
          
          
            Sports Blogs
            Fashion
            Harry Potter
            Movies
            Make Us Rich
            Real Estate
            Travel
            Tempo
            TV
            Video Games
            Health: IVF
            Weight Loss
            Intern Stories
          
          
            Past Editions
            Archives
            AvantGo
            Classroom Extra
            Coupons
            E-Edition
            Past Editions
            Liberty Medals
            
            Parade Magazine
            Site Map
            Special Sections
            Story Index
            Tempo
            Weekend Guide
          
         
             
       
           
          
              
             
               
                               
                   
                      WALKING ON EIRE     
                      DISCOVERING IRELAND?S BEAUTY ON FOOT 
                   
                   By VENISE DOUGLAS 

					

                         
	                 
		 
			
		  		
		 
			Loading new images...
		 
		 

	
              
				  
				  
				   

 
                  
				  
				   

                                    March 16, 2007 -- I'M walking up the Hill of Tara, Ard Ri Eireann, the seat of the High Kings of Ireland in County Meath. The wind is up and the air is moist with the light mist the Irish call "rain." And of course, the mossy mounds where my ancestors lie buried are emerald green.
 
 I've been walking all day and I'm exhausted but exhilarated, too. The ancient stones here tell stories, especially to solitary walkers who have time to listen.
 
 I take a deep, deep breath once I get to the top. It feels as if all of Ireland is at my feet (feet are called "shank's mare" over here). Walking is an art form in Ireland and I'm just beginning to realize I want to see every water-colored
masterpiece the island has to offer. Back down the hill, I stop at a traditional pub where thirsty walkers go for a Guinness or two.
 
 There's a fiddler playing traditional music in the corner. A few other American-Irish pilgrims like me are chatting with the barman. Someone is speaking Irish at the table behind me - the lovely lilt of the language entrancing me. I order a light golden whiskey (no water, please) and take out my walking journal. Where else will the open road take me?
 
 Walking tours are a fabulous way to see this country.
 
 On guided walking tours, such as ones given by Country Walkers, you can expect to walk with groups that are limited to under 20 people. Country Walker's Irish trips, for instance, accommodate only up to 18 participants. There are two guides on each trip so that no matter what your pace, you're accompanied, while your luggage is transported to your next lodging by van.
 
 On these trips, the terrain tends to be even, with mild elevations. "Our age range on the Ireland tours goes from 30s right up to 70 years of age," says Carolyn Walters Fox, Country Walker's Marketing Director. "A traveler who is used
to walking about three miles a day at home should feel comfortable with the pace."
 
 
VIEW FULL ARTICLE &gt;PAGE 1 2 CONTINUE READING &gt;                                   

             
             
                


 
  
  
  
 MORE 
 
                 
 

 
                              			     
                  
				 
					






 
	 
	    
		 
		  			 DATING 
		  			 JOBS 
		  			 AUTOS 
		  			 REAL ESTATE 
		  			
		 
	 
	     
 				 
             
          
 
	
	 
		News
		 
			 Local News 
			 National News 
			 International News 
			 News Columnists 
			 Weird But True 
			 NYPD Daily Blotter 
			 Liberty Medals 
			 Traffic &amp; Transit 
			 Lottery 
			 Classroom Extra 
		 
	 	
	 
		Sports
		 
			 Yankees 
			 Mets 
			 Giants 
			 Jets 
			 Knicks 
			 Nets 
			 Rangers 
			 Islanders 
			 Devils 
			 Sports Blogs 
			 Columnists 
			 
Bettor's Guide
			 
 Horse Racing Picks 
			 Post Line 
		 
	 	
	 
		Page Six
		 
			 Cindy Adams 
			 Liz Smith 
			 Braden Keil 
			 Michael Riedel 
			 Celebrity Photos 
			 Celebrity Sightings 
			 Page Six Magazine 
			 Delonas Cartoons 
		 
 
		Business
		 
			 Business Columnists 
			 Real Estate 
			 Stock Quotes 
		 
	 	
	 
		Entertainment
		 
			 Movies 
			 Movies Blog 
			 Oscars 
			 Food 
			 Fashion 
			 Fashion Blog 
			 Music 
			 Theater 
			 Health 
			 Travel 
			 Travel Blog 
			 Horoscope 
			 Weddings 
			 Dating 
			 Weekend Guide 
			 Comics &amp; Games 
			 Post Game Report 
			 Tempo 
		 
	 	
	 
		Post Opinion
		 
			 Editorials 
			 Oped Columnists 
			 Letters 
			 Books 
			 Ramirez Cartoons 
			 Send a Letter 
		 
 
		TV
		 
			 Linda Stasi 
			 Starr Report 
			 Adam Buckman 
			 Reviews 
			 TV Listings 
			 LIVE: The TV Blog 
		 
	 	
	 
		Classifieds
		 
			 Cars 
			 Dating 
			 Jobs 
			 Real Estate 
			 Marketplace 
			 Place an Ad 
		 
 
		Miscellaneous
		 
			 Contests 
			 Coupons 
			 Media Kit 
			 Parade Magazine 
			 RSS 
			 Special Sections 
			 Privacy Policy 
			 Terms of Use 
		 
	 	
	 
		User Services
		 
			 Contact Us 
			 FAQ 
			 Daily Newsletter 
			 Home Delivery 
			 Avant Go 
			 E-Edition 
			 Archives 
			 Back Issues 
			 Reprints 
			 Story Index 
			 Last 30 Days 
		 
	 	
	
           
            sign in
            subscribeprivacy policyENTERTAINMENT HEADLINES FROM OUR PARTNERSrss 
          
       
    
     
    
       NEW YORK POST is a registered trademark of NYP Holdings, Inc. NYPOST.COM, NYPOSTONLINE.COM, and NEWYORKPOST.COM are trademarks of NYP Holdings, Inc. 
 Copyright 2007 NYP Holdings, Inc. All rights reserved. 
  	   
 


