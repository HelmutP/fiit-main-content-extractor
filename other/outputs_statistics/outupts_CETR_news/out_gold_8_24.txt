

 ""You've got to ask yourself one question: Do I feel lucky? Well? Do ya, punk?"" 
 
Those words were uttered by Inspector Harry Callahan, played by Clint Eastwood, in the film ""Dirty Harry."" He was telling serial killer Scorpio to guess whether the gun Callahan was pointing at him had any bullets left. 
 
A simplified ""Do you feel lucky, punk?"" is to this day emblazoned on computer mouse pads, posters and T-shirts, which represents remarkable staying power for a 1971 film. 
  

                     
                        
                        
							
                            
                                

                                

                                
                                     
                                        
                                        	 Related links 
										
                                         
 
														
														
															
															


    
    Andrew Leckey



Photo












														

													 
													
												
										
											
													 
														
															



Aug. 19: Nardelli must get rolling fast at Chrysler 

Aug. 12: Starbucks, McDonald's have wide-ranging battle 

Aug. 5: A short attention span can bring long suffering 

July 29: Whole Foods saga: Think before you type 

July 22: Company of the burp endures barely a blip 

July 15: Packaging can't hide serious quality issues 

July 8: Subrime home loans are far from wholesome 


July 1: Once-sunny outlook turns cloudy at Yahoo! 

 

June 17: Cross-marketing yields unusual combinations 

June 10: Curse of too much debt at too young of an age 

June 3: Staying on top proves to be harder than ever 

May 27: Big tobacco rolls on, despite a hazy climate 

May 20: Investing gaffes offer many valuable lessons 

May 13: The 1980s revisited: Insider trading is back 

May 6: Some patience, please, after change at the top 

April 29: Siemens' history takes back seat to its troubles 

April 22: Global warming fight ripples across business 

April 15: CEOs are being judged on more than results 

April 8: Some aren't savoring chocolate's darker side 

April 1: Spotlight on rich is warming--or searing 

March 25: Paranoia, fear creep deep within business 

March 18: Ford giving up bond with its Aston Martin 

March 11: Whatever the reason, markets are hardly dull 

March 4: Marriages of convenience rule corporate kingdom 

Feb. 25: Firms ignore advice, then comb for answers 

Feb. 18: Fox soon to make business news a 3-way race 

Feb. 11: Nowadays, a penny saved is an accomplishment 

Feb. 4: A little found money can really mean a lot 

Jan. 28: Rise and fall of oil has its ups and downs 

Jan. 21: Going private may not serve the public good 

Jan. 14: Democratic rule in congress won't blunt power of the fed 

Jan. 7: Ford's view on the danger of inflation was, in fact, a winner 

Dec. 31: Not all execs, or ex-execs, share spirit of giving 

Dec. 24: Age is just a number--except at retirement time 

Dec. 17: Do you feel confident? Ready to go shopping? 

Dec. 10: Bucking a trend: Dollar coins on the way 

Dec. 3: Warehouse club firm finds trouble in bulk   
 


														
														

													 
													
												
										
											
													 
														
															 
 Main page 

 Advice on investing $10,000 in 2007 

 Regulators urge government to beef up 401(k) oversight 

 Charitable donations cut taxes, spread cheer 

 It's not just about double-digit returns 

 For many investors, tweaks may be wise 

 The savings
  game 

 The Leckey
  file 

 Getting
  started 

 Spending
  smart 



 Taking stock 

 Assess your risk tolerance 

 The week ahead 
														
														

													 
                                     
                                

                            
                        

                        
                        




    
        
            
        
    

                        

                        
                         
                            
                                
                            
                         
                        
                     


                     
Fast forward to recent holiday shopping seasons and a similar question regularly posed to law-abiding consumers:
""You consumers have got to ask yourself one question: Do I feel confident? Well? Do ya, consumers?"" 
 
Investors, economists and retailers wait with bated breath for the answer. 
 
Many times in my reporting career I stood in front of stores asking people how much they intended to spend on holiday gifts based on their relative confidence. Responses were whatever anyone felt they needed to say to get me out of their face. 
 
But, of course, there are more scientifically conducted measures of the pessimism-versus-optimism game. 
 
The Conference Board consumer confidence index and the University of Michigan consumer sentiment index are monthly surveys of U.S. households, though their methodologies differ. 
 
With final figures released toward the end of the month, they're among the main economic indicators that gain media coverage. Interest in these measures is based on the belief that our attitudes toward the future will impact our spending patterns and the overall economy. 
 
Investors may care the most of all, because a drop in the consumer sentiment is often followed by an increase in bond prices and decline in their yields. That's because lackluster consumer spending may prompt the Federal Reserve to cut short-term interest rates to stimulate the economy. That will make existing higher-yielding fixed-rate investments more valuable. 
 
Stocks, especially retailing stocks, move quickly whenever confidence or sentiment numbers surprise. 
 
Whether due to the troubled housing market or general economic worries, we haven't been quite as perky lately as some experts might hope. Such a shame: Isn't it our patriotic duty to buy major household appliances with gusto and wax enthusiastic about our future paychecks? 
 
Tricky business, this determination of how we feel deep down inside. Because in our heart of hearts, many of us just plain don't know. We have our good days, we have our bad days. 
 
Linking all of this to our pocketbooks must be quite a challenge for those U.S. households entrusted with helping determine just how all of us feel. 

