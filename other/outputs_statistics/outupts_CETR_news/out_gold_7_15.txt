

 Levy and Gribble also tested the same neighborhoods to see how many drive-by downloads each launched. Drive-bys are considered the most nefarious method of installing adware and spyware onto user's PCs, since they leverage browser bugs to automatically (or nearly automatically) download and install the software onto hard drives.
 
 
The worst neighborhood for drive-by downloads was, hands-down, made up of sites that offered pirated copies of software, games, music, and movies. In October, Levy and Gribble found 6.5 percent of all pirate domains conducting drive-by downloads. Adult (2 percent), celebrity (3.9 percent), and games (3.3 percent) followed.
 
 
Amazingly, those numbers were significantly lower than the ones gathered during an earlier sweep in May 2005, when 16.6 percent (about one in six) pirate sites did drive-bys, and 9 percent of adults sites used the practice to install spyware.
 
 
""It's difficult to attribute this to a specific cause,"" cautioned Levy and Gribble, who offered several possible reasons for the decline, ranging from greater adoption of anti-spyware tools to civil lawsuits filed against spyware and adware purveyors.
 
 
""The percent of executables that were infected with spyware caught us by surprise,"" said Levy. ""We didn't expect it to be that high.""
 
 
""What important here is that overall, one in 25 domains has infectious files,"" added Gribble.
 
 
Fortunately, they're concentrated in some neighborhoods, and nowhere to be seen in others.
 
 
The Levy/Gribble paper can be downloaded in PDF format from the University of Washington's Web site. 

