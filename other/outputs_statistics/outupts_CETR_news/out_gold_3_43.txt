

 June 2, 2007 -- New Yorkers have lost a friend most never knew they had.  
  Warren Anderson, a former state Senate majority leader and one of the last of New York's so-called Rockefeller Republicans, died yesterday after a brief illness. He was 91.  
  Anderson, of Binghamton, led the Senate from 1973-88, and was instrumental in crafting the legislation that helped bring New York City back from near-bankruptcy in 1975. In addition, he labored tirelessly to provide the upstate and suburban support necessary for the bailout bills to become law.  
  He was a man of personal honor, a rare quality in a calling better noted for expediency.  
  Anderson served his constituency, and his state, with distinction. 

