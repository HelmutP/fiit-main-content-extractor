

 "Russia Capitalizes on New World Disorder
	 
 By Gerhard Sp?rl 
	 The war in the Caucasus is a truly global crisis. Russia's action against the western-looking Georgia testifies to an extreme craving for recognition and is reminiscent of the Cold War. It reveals the reality of the chaotic new world order -- a result of the failures of President Bush's foreign policy. 
	     
 

 
 
 
	
			 
		  
				 AP 
		 Ossetian soldiers on top of a tank enter Tskhinvali next to a giant portrait of Russian Prime Minister Vladimir Putin. 
		 		
 When the German Foreign Minister Frank-Walter Steinmeier describes the overall situation in the world, he likes to refer to what he calls a ""new complexity"" of circumstances. Yet things were already complex before the war in the Caucasus region, which has its roots in the 19th century more than in the 21st, but now we have been deprived of one small piece of ignorance. Who would have even bothered to try and pinpoint South Ossetia on the map or to carefully differentiate it from North Ossetia before the conflict? And this is supposed to be a world crisis?
 But it is one indeed, because the crisis has given oil and gas producer Russia an alibi for cleaning up along its borders in places like Georgia, where the United States and NATO were beginning to exert their influence. It is a world crisis, because this wounded ex-superpower decided, some time ago, that it was going to put an end to a phase of humiliation and losses, of NATO and American expansion.  
 People took to the streets in the Baltic states, and the Polish president traveled to Georgia to participate in a show of solidarity among the weak, among countries with a long historical memory of what Russia can do to the weak. It is no coincidence that Eastern Europeans suspect that the West is hedging its bets, as it did in 1938, 1956, 1961 and 1968, in loud silence and inactive appeasement. Their illusions are suddenly dropping away like autumn leaves. 
 
 
 
 
 REPRINTS 
 
 

 
Find out how you can reprint this SPIEGEL ONLINE article in your publication.
 
 
 
Part of the truth is that the United States had rather relished treating Russia and its then president, Vladimir Putin, as yesterday's superpower and leader. US President George W. Bush withdrew from the Anti-Ballistic Missile (ABM) Treaty and invented a missile shield in the Czech Republic and Poland. The revolutions in Ukraine and Georgia, reverberations of the revolutionary fall of 1989, were made possible by the gracious assistance and coaching of American foundations and think tanks. There was nothing wrong with this approach, but America, the overwhelmingly superior superpower, was petty enough to gloat over its achievements.
 
A Touch of the Old Cold War
 
 John McCain, who hopes to become the 44th US president, has come up with the spectacular idea of establishing a league of democracies that would address the world's problems whenever the United Nations is gridlocked, in other words, whenever there is an important issue on the table. If this league existed today, would intervention forces already have been deployed to the Caucasus? And now McCain has come up with the no less original idea of excluding Russia from the golden circle of G8 nations. Does anyone have any other bright ideas on how to punish the miscreant? 
 
 
 
 RELATED SPIEGEL ONLINE LINKS 
			 
		 
		 
 
	The Caucasus Crisis: 'The US Has More Important Concerns Right Now' (08/14/2008) 
 
 
 
	'Massacres on our Doorstep': Europe Agrees to Monitor Georgia Cease-Fire (08/13/2008) 
 
 
 
	Fragile Cease-fire: Russian Army Clears Out Georgian Army Bases (08/13/2008) 
 
 
 
	Blame Game?: EU Looks for Common Response on Russia (08/13/2008) 
 
 
 
	Ceasefire in Georgia: Putin Outmaneuvers the West (08/12/2008) 
 
 
 
	Russia's Position in the Caucasus: How Tight Should the Bear Hug? (08/12/2008) 
 
 
		 
	 
The new complexity consists of the fact that a few opportunities were missed after 1989, such as the chance to develop a resilient relationship among the European Union, NATO and Russia. Before long, our only concern will be over whether we should in fact entrust the Russia, as uninterested in democracy as they are, with our energy security.
 It is true that there is a touch of the old Cold War to August 2008. And yet it is also true that the month's events constitute only a subcategory of the larger complexity in which the world finds itself today. The United States is the common denominator. On the one hand, it had no qualms about tormenting Russia, and yet it is incapable of coming to Georgia's aid. It was also apparently unable to dissuade the Georgian president from embarking on his adventure. CNN is so enamored of Georgian President Mikhail Saakashvili that he is constantly asked to appear on the news network for interviews, so that he can instill his view of things -- of Georgia on the road to democracy, and of Russia succumbing to revanchism -- in Americans, to the delight of the White House. 
 
A Lopsided Multipolarity
 
 The world ceased to be a unipolar place when the Iraq war began. When the neocons used the word unipolarity, they were referring to the idea that the world's sole superpower, thanks to its military superiority, could assume that it was entitled to the role of global cop, and that the world must bend to its will, whether it wanted to or not.  
 Now a new technical term has come into circulation: multipolarity. It means that a number of powers can do as they please, without punishment, and no one can do much about it. China can do as it pleases with Tibet, the Uyghurs and its dissidents, and it can buy its energy where it pleases. India can sign a nuclear treaty with the United States, and can then vacillate between choosing to ditch the agreement and keep it in place. Iran can decide to become a nuclear power and then wait to see what happens, to see whether Israel and the United States, for example, will issue empty threats of air strikes while Russia and China obstruct the superpower in the UN Security Council whenever it calls for effective resolutions. 
 
 
 
 
 NEWSLETTER 
 
Sign up for Spiegel Online's daily newsletter and get the best of Der Spiegel's and Spiegel Online's international coverage in your In-  Box everyday. 
 
 

 
 
 
 
But the new multipolarity is lopsided. America is still the power without which nothing works -- whether it be sensible or senseless. China is moving in its own orbit and is unlikely to move forward as quickly as it had hoped until recently. It's easier to win gold medals than establish a stable world power by combining capitalism with communism. India is drifting along behind China, struggling with its own domestic problems and unable to decide whether it should throw in its lot with China or the United States.
 And Russia? It has a tremendous craving for recognition and a ludicrous amount of money. That money could be put to great use, to develop a nation, for example. That would be a goal that made sense. In the long run, Putin will have to stop playing the bare-chested macho man, the great loner who couldn't care less about alliances and world opinion. 
 And so the world finds itself in a state of new complexity. It's a nice, inoffensive term, one difficult to criticize. Things are already tremendously in flux. But aren't things always in flux, sometimes more, sometimes less? In 1957, the new British Prime Harold Macmillan was asked what would determine the course of his government. ""Events, my good man, events,"" he replied. Yesterday there was Iraq, today there is the Caucasus, and the Palestinian problem never seems to go away.  
 What will tomorrow bring?"
"7469","By Gerhard Sp?rl 
	 The war in the Caucasus is a truly global crisis. Russia's action against the western-looking Georgia testifies to an extreme craving for recognition and is reminiscent of the Cold War. It reveals the reality of the chaotic new world order -- a result of the failures of President Bush's foreign policy. 
	     
 

 
 
 
	
			 
		  
				 AP 
		 Ossetian soldiers on top of a tank enter Tskhinvali next to a giant portrait of Russian Prime Minister Vladimir Putin. 
		 		
 When the German Foreign Minister Frank-Walter Steinmeier describes the overall situation in the world, he likes to refer to what he calls a ""new complexity"" of circumstances. Yet things were already complex before the war in the Caucasus region, which has its roots in the 19th century more than in the 21st, but now we have been deprived of one small piece of ignorance. Who would have even bothered to try and pinpoint South Ossetia on the map or to carefully differentiate it from North Ossetia before the conflict? And this is supposed to be a world crisis?
 But it is one indeed, because the crisis has given oil and gas producer Russia an alibi for cleaning up along its borders in places like Georgia, where the United States and NATO were beginning to exert their influence. It is a world crisis, because this wounded ex-superpower decided, some time ago, that it was going to put an end to a phase of humiliation and losses, of NATO and American expansion.  
 People took to the streets in the Baltic states, and the Polish president traveled to Georgia to participate in a show of solidarity among the weak, among countries with a long historical memory of what Russia can do to the weak. It is no coincidence that Eastern Europeans suspect that the West is hedging its bets, as it did in 1938, 1956, 1961 and 1968, in loud silence and inactive appeasement. Their illusions are suddenly dropping away like autumn leaves. 
 
 
 
 
 REPRINTS 
 
 

 
Find out how you can reprint this SPIEGEL ONLINE article in your publication.
 
 
 
Part of the truth is that the United States had rather relished treating Russia and its then president, Vladimir Putin, as yesterday's superpower and leader. US President George W. Bush withdrew from the Anti-Ballistic Missile (ABM) Treaty and invented a missile shield in the Czech Republic and Poland. The revolutions in Ukraine and Georgia, reverberations of the revolutionary fall of 1989, were made possible by the gracious assistance and coaching of American foundations and think tanks. There was nothing wrong with this approach, but America, the overwhelmingly superior superpower, was petty enough to gloat over its achievements.
 
A Touch of the Old Cold War
 
 John McCain, who hopes to become the 44th US president, has come up with the spectacular idea of establishing a league of democracies that would address the world's problems whenever the United Nations is gridlocked, in other words, whenever there is an important issue on the table. If this league existed today, would intervention forces already have been deployed to the Caucasus? And now McCain has come up with the no less original idea of excluding Russia from the golden circle of G8 nations. Does anyone have any other bright ideas on how to punish the miscreant? 
 
 
 
 RELATED SPIEGEL ONLINE LINKS 
			 
		 
		 
 
	The Caucasus Crisis: 'The US Has More Important Concerns Right Now' (08/14/2008) 
 
 
 
	'Massacres on our Doorstep': Europe Agrees to Monitor Georgia Cease-Fire (08/13/2008) 
 
 
 
	Fragile Cease-fire: Russian Army Clears Out Georgian Army Bases (08/13/2008) 
 
 
 
	Blame Game?: EU Looks for Common Response on Russia (08/13/2008) 
 
 
 
	Ceasefire in Georgia: Putin Outmaneuvers the West (08/12/2008) 
 
 
 
	Russia's Position in the Caucasus: How Tight Should the Bear Hug? (08/12/2008) 
 
 
		 
	 
The new complexity consists of the fact that a few opportunities were missed after 1989, such as the chance to develop a resilient relationship among the European Union, NATO and Russia. Before long, our only concern will be over whether we should in fact entrust the Russia, as uninterested in democracy as they are, with our energy security.
 It is true that there is a touch of the old Cold War to August 2008. And yet it is also true that the month's events constitute only a subcategory of the larger complexity in which the world finds itself today. The United States is the common denominator. On the one hand, it had no qualms about tormenting Russia, and yet it is incapable of coming to Georgia's aid. It was also apparently unable to dissuade the Georgian president from embarking on his adventure. CNN is so enamored of Georgian President Mikhail Saakashvili that he is constantly asked to appear on the news network for interviews, so that he can instill his view of things -- of Georgia on the road to democracy, and of Russia succumbing to revanchism -- in Americans, to the delight of the White House. 
 
A Lopsided Multipolarity
 
 The world ceased to be a unipolar place when the Iraq war began. When the neocons used the word unipolarity, they were referring to the idea that the world's sole superpower, thanks to its military superiority, could assume that it was entitled to the role of global cop, and that the world must bend to its will, whether it wanted to or not.  
 Now a new technical term has come into circulation: multipolarity. It means that a number of powers can do as they please, without punishment, and no one can do much about it. China can do as it pleases with Tibet, the Uyghurs and its dissidents, and it can buy its energy where it pleases. India can sign a nuclear treaty with the United States, and can then vacillate between choosing to ditch the agreement and keep it in place. Iran can decide to become a nuclear power and then wait to see what happens, to see whether Israel and the United States, for example, will issue empty threats of air strikes while Russia and China obstruct the superpower in the UN Security Council whenever it calls for effective resolutions. 
 
 
 
 
 NEWSLETTER 
 
Sign up for Spiegel Online's daily newsletter and get the best of Der Spiegel's and Spiegel Online's international coverage in your In-  Box everyday. 
 
 

 
 
 
 
But the new multipolarity is lopsided. America is still the power without which nothing works -- whether it be sensible or senseless. China is moving in its own orbit and is unlikely to move forward as quickly as it had hoped until recently. It's easier to win gold medals than establish a stable world power by combining capitalism with communism. India is drifting along behind China, struggling with its own domestic problems and unable to decide whether it should throw in its lot with China or the United States.
 And Russia? It has a tremendous craving for recognition and a ludicrous amount of money. That money could be put to great use, to develop a nation, for example. That would be a goal that made sense. In the long run, Putin will have to stop playing the bare-chested macho man, the great loner who couldn't care less about alliances and world opinion. 
 And so the world finds itself in a state of new complexity. It's a nice, inoffensive term, one difficult to criticize. Things are already tremendously in flux. But aren't things always in flux, sometimes more, sometimes less? In 1957, the new British Prime Harold Macmillan was asked what would determine the course of his government. ""Events, my good man, events,"" he replied. Yesterday there was Iraq, today there is the Caucasus, and the Palestinian problem never seems to go away.  
 What will tomorrow bring? 
 
 

