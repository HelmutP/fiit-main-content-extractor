


     By Mark Potter  
     LONDON, Feb 9 (Reuters) - Europe's scepticism over attention deficit hyperactivity disorder (ADHD) is starting to lift, opening up a market that could grow to $1 billion-a-year by 2012-13, British drugmaker S"" &gt;






  
    
    
    
    
    
    
  	
    
    


 
 

   
  	 
  	    
			
 



 
   


    
    
     
    	
     
		 
            
			 
                
                     
                    
                
             
			 
			 
				 
					Login
				 
				 
					My Profile 
					 
					Logout
				 
			 
            
		 
    


  
 

 
	 
		 
	 
     
    
	 
 
   

 
	 
		
 
	

  
    
  

 

 
 
 

 


 
  	 Go to a Section: 
     Top Business News 
     Banking &amp; Financial 
     Consumer Products 
     Health 
     Tech, Media &amp; Telecom 
     Small Business 
     
 
   1:21PM EST, Sun 11 Feb 2007
 
  You are here: 
  
    Home
    
      &gt;
      Business
      
      &gt;
      Health
      
      &gt;
      Article
 
 
 
       Shire to target Europe as ADHD market opens up 
       Fri Feb 9, 2007 7:53AM EST 
      
     

     
 
     
       Company News 
     
     
       
UPDATE 2-Shire sees pick up in hyperactivity drug market
 
       
Shire says extended release ADHD drug effective
 
       
Shire says US FDA approves Lialda for colitis
 
       
Shire sues Andrx for patent infringement
 
       
Shire CEO sees U.S. ADHD market growth accelerating
 
       
 

 
	

  
    
  

 

 
					 
						  Market View  
						 
							
								
									SHP.L
								
								
									Last:
									
								
								
									Change:
									
								
								
									Revenue (ttm):
									
								
								
									EPS:
									
								
								
									Market Cap:
									
								
								
									Time:
									
								
								
									
										
									
								
							
							   
							 Stock Details 
							 Company Profile 
							 Analyst Research 
						 
					 
				 
			
      
      

      

		 

    
       

    
     
        Email This Article |
        Print This Article |
        Reprints
 
     
        [-]
        Text
        [+]
     

		
      

    
     By Mark Potter  

    
     LONDON, Feb 9 (Reuters) - Europe's scepticism over attention deficit hyperactivity disorder (ADHD) is starting to lift, opening up a market that could grow to $1 billion-a-year by 2012-13, British drugmaker Shire Plc (SHP.L: Quote, Profile, Research) believes.  

    
     Shire, whose Adderall XR is the top-selling treatment for ADHD in the United States, has not even tried to get the drug approved for sale in Europe as psychiatrists have been divided on the existence of the condition, let alone how to treat it.   

    
     But Chief Executive Matthew Emmens told Reuters that  attitudes were changing and Shire planned to launch two of its next generation of ADHD treatments, skin patch Daytrana and Vyvanse, a successor to Adderall XR, in Europe in 2008 and 2009 respectively.  

    
     In particular, he was encouraged by two so-called ""opinion leader"" meetings of psychiatrists in Europe last year.  

    
     ""For the first time, we're seeing a willingness to not only accept it (ADHD) as a disorder, but to recommend treatment in Europe, in particular for adults,"" he said in an interview.  

    
     The U.S. market for treating ADHD, which is characterised by impulsive behaviour and short attention spans, is worth about $3.5 billion a year and growing. In Europe, the market is virtually non-existent.   Continued... 

    

       
       
      
         
 

       © Reuters 2007. All rights reserved.
       

 
	 
     
	 
		 Reuters Pictures 

		 
			

			 Editors Choice: Best pictures from the last 24 hours. View Slideshow 
		 

	 
 
 
   


 
	 
     
  Also on Reuters  
 
 
  
""Unabomber"" wants libraries to keep his papers
 
 
 VIDEO: Virgin boss Richard Branson offers climate prize"" /&gt; 
VIDEO: Virgin boss Richard Branson offers climate prize
 
 
  
Disgraced evangelist's therapy fuels ""gay debate""
 
 
 

 
   


 


 

 
 
 

 


 

 


   
      
 
	

  
    
  

 

 
	 

	 
		 
                     
                    Industry News
                     
                     
				 
		
		 
				 
					Sanofi loses Lovenox patent, no generics just yet
Fri Feb 09, 2007 03:40PM EST
				 
				 
		 
Shire to target Europe as ADHD market opens up
 
		 
BioMimetic Therapeutics says offering priced at $17.15/shr
 
		 
				
					More Industry News...
			 
		 
		 

 

 


 
	 

	 
		 
                     
                    Most Viewed Articles 
                     
				 
		 
				Putin says U.S. wants to dominate world

			 
		 
				Sex museum makes HIV lessons fun

			 
		 
				Where holey hosiery is a holy horror
  |  Video

			 
		 
		 

 

 

 
	 

	 
		 
                     
                    Insights 
                     
				 
		
		 
				
						

				 
					Wireless industry to focus on growth at trade show
Fri Feb 09, 2007 01:56PM EST
				 
				 AMSTERDAM (Reuters) - Riding the wave of demand for mobile communications in developing countries and finding new ways to make money from maturing markets will be preoccupations at the world's top wireless trade show, 3GSM, in Barcelona. 
                
                    Full Article  
         
		 
		 

 

 

 
	 
    


 
 	 Top Company Searches on Reuters.com 
 
 
 
 1. AAPL
 
 2. GOOG
 
 3. MSFT
 
 4. GE
 
 5. SIRI
 
 
 
   6. CSCO
 
   7. DELL
 
   8. INTC
 
   9. SI.N
 
 10. MOT
 
 
 
 

 
   



	 



 

     Reuters.com: 
    Help  | 
    Contact Us  | 
    Advertise With Us  | 
    Mobile  | 
    Newsletters  | 
    RSS  | 
    Widgets  | 
    Interactive TV  | 
    Labs  | 
    Site Index 

     Reuters Corporate: 
		Copyright  | 
		Disclaimer  | 
		Privacy  | 
		Products &amp; Services  | 
		Professional Products Support  | 
		About Reuters  | 
    Careers 

  	   

  	 International Editions:
  	
  	Arabic  | 
  	Argentina  | 
  	Brazil  | 
  	Canada  | 
  	Chinese Simplified  | 
  	Chinese Traditional  | 
  	France  | 
  	Germany  | 
  	India  | 
  	Italy  | 
  	Japan  | 
  	Latin America  | 
  	Mexico  | 
  	Russia  | 
  	South Africa  | 
  	Spain  | 
  	United Kingdom  | 
  	United States 

       
     Reuters is the world's largest international multimedia news agency, providing investing news, world news, business news, technology news, headline news, small business news, news alerts, personal finance, stock market, and mutual funds information available on Reuters.com, video, mobile, and interactive television platforms. Reuters journalists are subject to the Reuters Editorial Handbook which requires fair presentation and disclosure of relevant interests. 

   

 





















