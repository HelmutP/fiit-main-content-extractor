

 Michigan developer's newest star strategy title to come in the fall
				
				
				 
 August 22, 2007 
				
				 BY HEATHER NEWMAN 
				
 FREE PRESS GAME WRITER 

				 


Michigan doesn't have too many game developers, but those we do have are the type of folks you can really rally behind: ""Galactic Civilizations II,"" the turn-based strategy game put together by Stardock Entertainment out in Plymouth, has taken home a pile of great reviews. The first expansion pack, ""Dark Avatar,"" was the top-rated game of 2007 across all platforms on GameRankings.com. 
 


Now they're producing the second, and last, expansion for the series: ""Twilight of the Arnor,"" set to bow this November for $29.95. To play it, you'll need either the Gold Edition of the original game or the original plus ""Dark Avatar."" 
 


 

	
		
			 Advertisement 
		
			
			
			
		
	


For more details on what the new expansion will include, check out www.galciv2.com/twilight, and be assured that you'll be seeing more coverage here. 


Also announced today: SEGA is releasing a new ""Sonic,"" this one for the Wii and PlayStation 2. ""Sonic Riders: Zero Gravity"" will hit stores in February 2008. The company promises you'll actually be able to control gravity, adding to the already breakneck pace of ""Sonic"" games. You'll get evolving vehicles and multiplayer modes, too, along with all-new playable characters that SEGA isn't specifying just yet. 
 


In a case of ""New! The features you were asking for five years ago!"", Electronic Arts announced today that ""FIFA Soccer 08,"" which launches October 9 in this country, will finally include the ability to suit up with 10 other friends online and each play a position on a single team.  That feature will only be available on Xbox 360 and PlayStation 3, and will be offered only as a free download - six to eight weeks after the game goes on sale. Oh well, what's a few more weeks when you've been waiting for years? 

