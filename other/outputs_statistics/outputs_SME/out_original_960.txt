
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Veríte predpovediam ultrapesimistu Petra Staněka? 

        
            
                                    24.10.2008
            o
            15:19
                        |
            Karma článku:
                12.90
            |
            Prečítané 
            43459-krát
                    
         
     
         
             

                 
                    Prečo sa médiá toľko spoliehajú na ekonóma, ktorý v kľúčových predpovediach už toľkokrát zlyhal?
                 

                 
 SME – Peter Žákovie/sme.sk
    Finančná kríza môže trvať aj desať rokov  BRATISLAVA. Kríza na svetových finančných trhoch nebude krátka a potrvá ešte osem až desať rokov. Tvrdí to ekonóm Slovenskej akadémie vied a poradca premiéra Peter Staněk. Hlavný problém súčasnej situácie na trhoch nie je podľa neho v hypotekárnom bankovníctve, ale v extrémnom zadlžení obyvateľstva.      Peter Staněk sa za posledný mesiac stal jedným z najcitovanejších ekonómov v našich médiách. Čiastočne určite aj preto, že na rozdiel od väčšiny analytikov je schopný na rovinu povedať  konkrétne a zároveň hrozivé predpovede. Novinári to majú radi, dobre to články (cez ich titulky) predáva - ako v horeuvedenom príklade.     Lenže aké spoľahlivé sú vlastne Staněkove negativistické predpovede? Pri analýze jeho vyjadrení ku kľúčovým ekonomickým otázkam minulých rokov (daňová reforma, vstup SR do EÚ) sa zdá, že tento bývalý štátny tajomník financií poslednej Mečiarovej vlády, neskôr ekonomický expert Strany demokratického stredu/STRED (Ivan Mjartan) a súčasný premiérov poradca je v predpovediach vysoko neúspešný.     11.6.2002, Nový deň  Slovo má PETER STANĚK, ekonóm, autor knihy Globalizácia, EÚ a Slovensko  * Je deklarovaný dátum vstupu, rok 2004, reálny?     - Spomínané reformy budú realizované v období 2003 - 2005 a budú znamenať dosť vyčerpávajúci proces pre EÚ. Ak to hodnotím z tohto hľadiska, musím súhlasiť s poslancom Európskeho parlamentu H. P. Martinom o tom, že najskôr si musí EÚ urobiť poriadok doma a potom zrejme prijme ďalšie krajiny. Takže vstup do EU vidím skôr v rokoch 2005 - 2006.     Skutočnosť: Do EÚ sme vstúpili  1.mája 2004.    12.8.2002, STV  P. MITKA, Price Waterhouse Coopers, privatizačný poradca vlády::  "K cene Slovenských elektrární sa v tejto etape nebudeme vyjadrovať z toho dôvodu, že my sami ešte potrebujeme dokončiť ocenenie."  Peter STANĚK, Ústav slovenskej a svetovej ekonomiky SAV:  "V dnešnej situácii potrebujeme za každú cenu príjmy z privatizácie elektrárne, pretože tie diery v rozpočte sú obrovské, tak toto si tak isto zistí aj budúci investor aj privatizačný poradca. Toto všetko vedie k tomu, že pravdepodobne tá cena nebude nejaká veľká a skôr bude tlačená rádovo nie na miliardy, ale na menšie sumy."     Skutočnosť: Enel kúpil SE od štátu za 31 mld Sk.    3.6.2003, Národná Obroda   Daňovú reformu včera kritizoval aj Peter Staněk z Ústavu slovenskej a svetovej ekonomiky. Obavy vyjadril najmä zo zvyšovania daní z nehnuteľností a z negatívneho vplyvu rastu spotrebných daní. Reforma podľa neho môže znamenať zánik tretiny malých a stredných podnikov.      Skutočnosť: Počet malých a stredných podnikov v SR (definovaných ako podniky do 250 zamestnancov), ku koncu roka       2003   2004   2005     58 367   70 553  79 364      Zdroj: Štatistický úrad SR      16.7.2003, Nový deň  Príjmy reálne porastú až o tri roky  Slovo má PETER STANĚK, ekonóm-analytik a pracovník Ústavu slovenskej a svetovej ekonomiky SAV  "budúci rok bude náš vstup do EÚ prekrytý dosahmi daňovej reformy. Žiaľ, v tejto fáze pôjde o predstih cien pred rastom príjmov. Príjmy môžu reálne začať rásť o nejaké tri roky."     Skutočnosť: Reálna zmena čistých príjmov priemernej domácnosti (%, ku koncu roka): rast príjmov mínus rast cien     2004     2005   2006     +0,5     +5,2  +7,7      Zdroj: Štatistický úrad SR      1.8.2003, Nový deň  Zvyšovanie cien nemožno stotožniť s nijakou serióznou reformou, povedal nám včera ekonomický analytik zo Slovenskej akadémie vied Peter Staněk. Daň za súčasný daňový chaos budeme platiť ešte päť až desať rokov. Reči ministra financií Ivana Mikloša o tom, ako nám porastú reálne mzdy, nemožno brať vážne, pripomína uznávaný ekonomický expert. Výsledkom bude ďalší obrovský prepad domácej kúpyschopnosti a to položí mnoho malých, stredných, ale aj väčších firiem. V tomto kontexte pôsobí trápne, ak vláda hovorí o tristomiliónovej pomoci na podporu malých aj stredných podnikateľov. "Ide iba o chaotické látanie rozpočtových dier a nesystémové, zúfalé naháňanie dodatočných prostriedkov na prefinancovanie eurofondov," pripomína P. Staněk.    Skutočnosť: Reálna zmena priemernej mzdy v roku (%)       2004     2005   2006     +2,5     +6,3  +3,3      Zdroj: Štatistický úrad SR     9.12.2003, Hospodársky denník   Nastane väčší pokles dopytu  Pozitíva daňovej reformy kompenzujú viaceré negatívne opatrenia     Podľa P. Staněka vysoká sadzba DPH bude negatívne pôsobiť na niektoré ďalšie snahy zahraničných investorov prísť na územie SR. V oblasti domáceho kúpyschopného dopytu obyvateľstva je možné, vzhľadom na kumulatívny dopad viacerých reforiem - penzijnej, sociálnej, daňovej atď., očakávať výrazné zvýšenie výdavkov domácností. Vo väzbe na dôsledky vstupu do EÚ v oblasti štandardizácie, certifikácie a ekologizácie výrobkov sa dá očakávať aj výrazné zvýšenie výdavkov firiem. To bude znamenať výrazné obmedzenie vnútorného domáceho dopytu. Keď sa k tomu pridá skutočnosť, že pokračuje deregulácia cien energií, minimálne pre roky 2004, 2005 a sčasti aj pre rok 2006, je možné očakávať pokles reálneho dopytu tak z hľadiska domácností, ako aj firiem.     Skutočnosť: Konečná spotreba domácností – reálna zmena       2004     2005   2006     +3,8     +7,2  +6,3      Zdroj: Štatistický úrad SR     Hrubé investície firiem - reálna zmena:        2004     2005   2006     +5,0     +17,5  +8,2      Zdroj: Štatistický úrad SR       29.4.2006, Nový Čas,  Cena za liter benzínu a nafty sa zvýšila o ďalších 60 halierov a na niektorých čerpacích staniciach tak prekročila 42-korunovú hranicu!...Cena benzínu u nás by v lete mohla byť na úrovni 45 korún za liter,“ predpovedá ekonomický analytik Peter Staněk, podľa ktorého letné ceny ovplyvní najmä hurikánová sezóna v USA a ich rastúci záujem o ropu z Európy.    Skutočnosť: Najvyššia týždenná priemerná cena v roku 2006 dosiahla 42,60 Sk koncom júla.      A bonus na záver:    22.7.2005, Hospodárske noviny,  Keď hovorí analytik, nemusí to byť expert    Za odvážne a často nekompetentné považuje vyjadrenia analytikov bánk k rozličným ekonomickým otázkam ich kolega zo Slovenskej akadémie vied Peter Staněk. "Ak majú analytici úctu k realite a chápu ju v širokom kontexte, tak by mali byť podstatne opatrnejší a nepoužívať len niektoré ekonomické makročísla a selektívne vybrané údaje," upozorňuje Staněk.  ... A ako je možné, že analytici, ktorí sa podľa Staněka vyjadrujú nekompetentne, pracujú vo významných inštitúciách? "O stratégii bánk, ktoré u nás pôsobia, sa nerozhoduje na Slovensku, ale v zahraničí. Keď niekto pracuje v renomovanej banke, ešte nemusí poznať súvislosti vývoja ekonomiky. Spolupracoval som s týmito inštitúciami a viem o ich analytickej úrovni. Preto nehovorme, že ten, kto tam robí, je automaticky super. Ak sú banky také vynikajúce, prečo majú problémy s klasifikovanými úvermi?" zdôrazňuje analytik SAV.      Slotova cesta vrtuľníkom od súkromnej firmy: Či šlo o nezákonný dar pre predsedu koaličnej strany bude prešetrovať parlamentný výbor, oznámili v stredu a štvrtok hlavné slovenské médiá. Slotovi má hroziť pokuta až milión korún. Ako ma upozornil čitateľ, o šetrení kauzy z hlavných slovenských médií neprekvapujúco neinformovali akurát STV (opäť napr. na rozdiel od Slovenského rozhlasu) a TA3 (vrtuľník patril firme Kmotríka, majiteľa televízie).   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (113)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




