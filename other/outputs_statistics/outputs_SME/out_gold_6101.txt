

  
 Dielo nenechá mnohých očami len tak povrchne po ňom prejsť. M. Lopuchinová zaujme až pritiahne poloúsmevom, melancholickým pohľadom, ohybom a mäkkosťou tela, či celkovou zmesou zmyselnosti a ľahostajnosti. Niektorí Rusi ju pre záhadnosť výrazu nazývajú „ruskou" Monou Lisou. Portrét v roku 1885 inšpiroval k veršom básnika Jakova Polonského (1819-1898).
 
 К портрету Она давно прошла, и нет уже тех глаз, И той улыбки нет, что молча выражали Страданье - тень любви, и мысли - тень печали. Но красоту ее Боровиковский спас. Так часть души ее от нас не улетела, И будет этот взгляд и эта прелесть тела К ней равнодушное потомство привлекать, Уча его любить, страдать, прощать, молчать. 
   

