
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Menyhertova
                                        &gt;
                Nezaradené
                     
                 Bezdomovec 

        
            
                                    21.6.2010
            o
            8:10
                        (upravené
                21.6.2010
                o
                8:50)
                        |
            Karma článku:
                13.08
            |
            Prečítané 
            1321-krát
                    
         
     
         
             

                 
                    Je ich veľa. A ani  nevieme,  koľko ich vlastne v skutočnosti je.
                 

                  Vídame ich všetci, hlavne vo veľkých mestách a na verejných priestranstvách. Často spia na lavičkách , či žobrú peniažky pri východe z kostola. Byť bez domova nieje zrejme med lízať. Uznávam ale, že mnohí z nich majú leví podiel na svojom nešťastí a takýto život im aj vyhovuje..........   Stretla som človeka bez domova, ktorý sa odlišoval od ostatných. Bol to taký neštandartný  bezdomovec. Videla som ho, ako si kupuje mydlo, toaletný papier, hygienické vreckovky, jednorázové žiletky, lacný šampón a  bežné potraviny. Jupí, konečne nákup bez povestného "čuča". Nemal ale pri pokladni dosť peniažtekov. Nechýbalo mu veľa, bolo to zopár centov. Pokladníčka zavrela kasu a povedala: "Choďte, je to vporiadku."   On sa slušne poďakoval a odišiel.   O pár minút som ho opäť uvidela. Ale nebol to už celkom on. Bol prezlečený, celkom čistý, oholený. Na krku mal zavesenú identifikáciu predajcu časopisu Nota bene.  Keď som podišla k nemu, usmial sa.   Neviem, a ani nechcem vedieť aké je to žiť bez domova. Obdivujem ale ľudí, ktorých ani takéto nešťastie nepoložilo na lopatky. Takých, ktorý napriek nepriazni osudu sa snažia v tomto svete nájsť svoje vlastné miesto.   Napríklad aj ako predavači Nota bene.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Mužolapka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Rytmus a jeho kecy....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            Závisť, alebo prehnané ego?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O  cintoríne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Menyhertova 
                                        
                                            O delení ministerstiev...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Menyhertova
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Menyhertova
            
         
        menyhertova.blog.sme.sk (rss)
         
                                     
     
        som človek s chybami.....
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    971
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Výnimočná
                                     
                                                                             
                                            Timea Keresztényiová :Sama mama
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lara Fabian
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




