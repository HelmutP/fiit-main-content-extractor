

 Nebola to úplne zhoda okolností, že sa u mňa zišli, ale náhoda zohrala istú rolu. Jeden Koniec som mal už rozčítaný, ten Cortázarov, keď som nečakane v knižnici naďabil na druhý. Prosto som si ho musel vziať. A aké boli? 
 
Julio Cortázar: Konec hry 
 
Moje prvé stretnutie s Cortázarom bolo veľmi pozitívne. Výherci boli román, Konec hry je súbor poviedok zasadených s nostalgickým nádychom do Buenos Aires a argentínskeho vidieka. A pre mňa osobne to bolo tentoraz menej pútavé, či skôr menej vypointované. Zatiaľ čo v románe mohol autor pomaly rozvinúť rôzne motívy do tej miery, ako si zasluhujú, na konci väčšiny zaradených poviedok som si hovoril: "No a čo? Čo ďalej? Niečo mi ušlo?" 
 
Zo všetkých na mňa najlepšie zapôsobili dve, Bakchantky a titulný Konec hry. Podstatu "hry" prezrádzať nebudem, ale z Bakchantiek som vybral jeden odstavec. Postačí vedieť, že v poviedke ide o predstavenie vidieckeho symfonického orchestra, ktorý má veľmi ambiciózneho dirigenta: 
 
Článek doktora Palacína jsem si přečetl o přestávce po Mendelssohnovi a Straussovi, kteří Mistrovi přinesli nadšené ovace. Když jsem se procházal foyerem, jednou či dvakrát jsem zauvažoval, jestli provedení opravňuje k tak bouřlivým reakcím posluchačů, kteří, pokud vím, rozhodně nejsou příliš velkorysí. Ale výročí bývají branou hlouposti a já jsem usoudil, že lidí závislí na mistrovi nejsou s to ovládnou své emoce. V baru jsem potkal doktora Epifaníu s rodinou a chvíli sa s nimi bavil. Dívky byly rudé a vzrušené, obklopili mě jako kdákající slepičky (připomínají všelijakou drůbež), aby mi řekli, že Mendelssohn byl úžasný, že to byla hudba jako ze sametu a z organtýnu a božsky romantická. Člověk by mohl poslouchat nokturno celý život a scherzo znělo, jako by ho hráli ruce víl. Bebě se víc líbil Strauss, protože je to silný, opravdu německý Don Juan, s těmi lesními rohy a pozouny, ze kterých jí naskakovala husí kůže - což mi přišlo překvapivě doslovné. 
 
Sústo pre kritikov snobizmu alebo pre kritikov maskulinizmu? 
 
Dušan Mitana: Koniec hry 
 
Mitana bol na pomerne dlhom zozname autorov, ktorých si plánujem prečítať hneď, ako na nich zase niekde naďabím. (Dnes už druhýkrát toto slovo... aký je základný tvar? Ďabiť?) Zase, lebo pred pár rokmi som od neho čítal súbor poviedok Slovenský poker. 
 
Koniec hry je román a vydaný bol pôvodne približne o jednu dekádu skôr ako Poker. Tento časový interval v tvorbe niektorých spisovateľov prakticky nič neznamená, ale v tomto prípade hovoríme o rozdiele zásadnom. Poker 1993, Koniec 1984. Prvá už pár rokov po revolúcii, druhá ešte v hlbokom socíku.  
 
Nečudo, že štýl knihy, ktorá mi pripomenula napr. knižnú podobu Nevery po slovensky, je tvrdo realistický. Žiadne snové pasáže, žiadne fantazírovanie, normálna prechádzka po Bratislave, kolegovia, práca, rodina. 
 
Teda, aby som nezavádzal. Fantazírovania je dosť, nie je to však fantasy, ide o normálne úvahy človeka o konaní ostatných, najmä v prípade, že tí ostatní konajú zvláštne z nudy, zo zábavy, z núdze alebo z dôvodu orwellovského doublethinku. Súčasnú verziu (pochopiteľne) nemohol v roku 1984 vydať. (Trochu symbolické...) 
 
Konkrétnejšie k deju možno povedať, že hlavným hrdinom je televízny režisér, ktorý má zložité rodinné pomery. Príde k nešťastiu a dotyčný sa musí vyrovnať so situáciou, ktorú neplánoval. Koná v súlade so svojou povahou a presne podľa indícií, ktoré mu jeho okolie poskytuje. Problém: snáď všetko, o čom je nejako presvedčený, sa ukáže byť ináč. Akoby s ním hrali čudnú hru. Ako skončí? 
 
Citátov bude viac a to som už zopár vyradil. 
 
 
 
 
Na tému tvorivá práca v kolektíve a pre iných: 
 
Čím dlhšie robil túto robotu, pri ktorej sa musel ustavične vyrovnávať s množstvom nepredvídateľných problémov a pseudoproblémov, tým väčšmi bol presvedčený, že to najľahšie na jeho profesii je - sama tvorivá práca; aby sa k nej vôbec dostal, najdôležitejšie bolo nedať sa znechutiť. 
 
Kto tu tvorivo pracuje a nepozná to, nech sa hlási... v diskusii, Žinčicu necháme Žinčicom. 
 
 
 
Na tému odbornosti novinárov: 
 
Robila redaktorku v kultúrnej rubrike populárneho obrázkového týždenníka. Písala o výtvarníctve, o hudbe, o filme, o divadle, o literatúre - o všetkom: mala na to tú najlepšiu kvalifikáciu: v ničom sa poriadne nevyznala; vyštudovala novinárstvo. Festivaly, vernisáže, koncerty, premiéry - špecializovala sa najmä na rozhovory. Helena Barlová: záruka duchaplného, zasväteného interview. 
 
Akoby nestačilo, že sa po tomto výbere citátu ku mne chrbtom otočia všetci moji známi, ktorí sú vyštudovanými novinármi. Ešte na mňa budú pozerať krivo aj moji známi, ktorí robia rozhovory. :-D 
 
 
 
Na tému zrady: 
 
Ty si chceš vziať ženu, ktorej strýko zapredal vlasť za tridsať strieborných? 
 
Sme v ČSSR. Zaujímavé, že sa socialisticky uvedomelí oháňali biblickou metaforou. Akoby ju okopírovali od podaktorých dnešných národnostne uvedomelých. 
 
 
 
Na tému kúpeľňovú: 
 
... premýšľal pozerajúc na Helenine kefy, hrebene, šampóny, vlasové tužidlá a natáčky. Koľko času venovala svojim vlasom! Ja tvoju lastovičku, to ale bolo maželstvo! S prekvapením si uvedomil, že väčšina spomienok na Helenu sa mu spája s kúpeľňou, maskovaním a kefovaním. 
 
Myslím, že najmä to posledné v manželstve vôbec nie je nezvyčajné, tak neviem, čo na tom hrdinovi prišlo prekvapivé. Eh? 
 
 
 
Sonda do duše premysleného alkoholika: 
 
Keď už pre nič iné, budem piť aspoň preto, aby som Ti urobil natruc, povedal, a ay jej vrátil štatistický výpočet výstražných varovaní, so škodoradostným zadosťučinením argumentoval: Čo sa ti vlastne nepáči, som ako každý iný občan, prečo by som sa mal odlišovať? Veď si len prečítaj v novinách oficiálne štatistiky - každým rokom stúpa spotreba alkoholických nápojov. Tóno pije preto, lebo má veľkú zodpovednosť a potrebuje sa uvoľniť, Dušan preto, lebo nemá nijakú zodpovednosť, a tak sa môže uvoľňovať hocikedy, Jožo pije preto, lebo nemôže robiť to , čo chce, Dežo zase preto, lebo môže robiť všetko, čo len chce, Pišta pije preto, že chce pred niečím uniknúť, Milan zasa preto, lebo chce dostihnúť niečo, čo mu ustavične uniká, Karol pije, lebo málo zarába, Jano zasa preto, lebo nevie, čo robiť s toľkými peniazmi, a tak má keždý dôvod, aby si vypil, a hladina alkoholu v krvi národa stúpa. Vieš, koľko je to promile? Len si predstav, že by odrazu celý národ prestal piť. To by bola normálna sabotáž. O koľké miliardy by sme ochudobnili štátnu pokladnicu? Nie, moja milá, my alkoholici prinášame veľkú obeť v záujme všeobecnej prosperity. 
 
Ako hovoria iní klasici, keď sa objaví nejaký hnusný abstinent, dostane vidly do zadku a všetko bude v poriadku. 
 
 
 
A aby som ešte raz parafrázoval, ak to nie je jasné: Mitana je v našej knižnici vítaná. 
 
 
 
 

