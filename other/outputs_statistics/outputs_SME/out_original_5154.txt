
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Roy
                                        &gt;
                Všetkým
                     
                 Kto sa zastane Tatier, ak nie my sami? 

        
            
                                    24.4.2010
            o
            12:40
                        (upravené
                24.4.2010
                o
                13:24)
                        |
            Karma článku:
                6.86
            |
            Prečítané 
            995-krát
                    
         
     
         
             

                 
                    Vlky, medvede, kamzíky, ostatné zvieratá, rastliny a stromy nemôžu prísť do Bratislavy protestovať pred Úrad vlády proti primitivizmu, neodbornosti a neľudskosti, ktoré nám vládnu v politickom rozhodovaní. Nemôžu prísť na Námestie SNP stanovať a povedať, že nechcú žiť v zúženom životnom priestore, že sa nechcú sťahovať vyššie a vyššie a nemôžu ani povedať, že "Pre Boha, ľudia, veď sa zobuďte, sme tu pre vás!". Opäť si myslíte, že váš jedinečný, unikátny hlas, ktorý nemôže nik nahradiť, nič nezmôže? Že je to len kvapka v mori, že vy ste len malý pán alebo malá pani? Áno, je to "len" kvapka v mori - extrémne dôležitá, však!
                 

                 Tým, ktorí si myslia, že ich hlas nemá nijakú váhu, nič nerozhodne, ničomu nenapomôže, treba pripomenúť dve veci. Po prvé, uvedomte si, a je to práve v tomto predvolebnom období veľmi viditeľné, že hlas každého jedného z nás má (pre politikov) momentálne cenu zlata. Pozrite sa ako sa oň trasú. O každý jeden, o každú jednu kvapku - v mori.   Po druhé, treba si uvedomiť, že protesty tohto druhu, akými sú občianske protesty proti zonácii Tatier, proti stupídnemu zákonu o vlastenectve, protesty proti vykrádaniu štátu zo strany vládnej moci, protesty proti zlému a neefektívnemu manažovaniu krajiny majú výlučne charakter kvapky v mori - a preto je úplne zásadné, aby tých kvapiek bolo čo najviac. Veľa! Aby tie kvapky nezostávali doma, nesedeli len pasívne pri televízore a nadávali na to, ako veci nie sú na poriadku.   Kvapky patria do mora, nie pred televízor! Kvapky patria na námestie (SNP), pred prezidentský palác, pred parlament. Kvapky patria do ulíc. Tam sa dá vytvoriť more, ktoré dokáže potopiť Zlo - hlúposť, primitivizmus, vulgárnosť, chamtivosť, nenažranosť, neodbornosť, násilie .... Ale, ako som povedala, musí ich byť viac, veľa a musia si to správne načasovať. Nesmú (v čase) prepásť príležitosť.!   A nezabúdajme na to, že Život si hlas kvapiek pamätá! Má inú pamäť, než človek.   ___________________________________   Ja som si to odbila včera - v rámci poobedného, podvečerného venčenia nášho psíka - Dastíka. Vzala som dieťa a psa a vybrali sme sa do podvečerných bratislavských ulíc, obchodov, obchodíkov, nákupného centra, až sme napokon skončili na Námestí SNP.   Najskôr sme zabrúsili do obchodu so žonglovacími potrebami - kúpiť nové, hliníkové paličky a doplniť zásoby šnúry na žonglovanie s Diabolom, ktorého čaru moje dieťa momentálne úplne prepadlo. Nakukla som aj do vedľajšieho obchodíka. Prilákali ma ručne robené šperky vystavené vo výklade. Aj keď mali vážne skvelé ceny, nekúpila som napokon nič - "hriešne" peniaze som už totiž oplieskala na žonglovanie a tak som sa pre tento krát vzdala toho krásneho modrého, točeného náhrdelníka. Ale neplakala som - veď, čo by jedna matka neurobila pre svoje dieťa!   Nasledovala krátka, príjemná prechádzka podvečerným mestom. Užíval si ju aj náš Dastík, oňuchávajúc a kropiac každý druhý roh, stĺp, strom... Jeho špecialitkou je aj "polievanie" kvetov z kvetinárstva, vyložených na zemi - napr. v pasáži na Grosslingovej. Tu však musím povedať, že poučená minulými skúsenosťami, dávam veľký pozor, a nedovolím Dastíkovi čúrať len tak, mírnix-dírnix, kam sa mu zachce.   Zastavili sme sa v nákupnom centre - v banke, vybavili zopár finančných transakcií, odľahčili účet o úhradu hypotéky a nasýtili hladné žalúdky. Všetci traja - Dastík pod stolom, ja so synom - ako slušní a civilizovaní občania - za stolom.   Čakala nás posledná zastávka - Námestie SNP a jeho malé stanové mestečko. Taká malá Pohoda - ako to pri pohľade na rozložené stany za sochou komentoval môj syn. Alebo inak - akási prírodná obývačka - ležíte, spíte si, podriemkávate (na zemi, pod stanom), kým na obrazovke pred vami, beží posledný film. Aj na námestí sa premietalo. Práve dávali dokumentárny film o tom, čo je to les.   Myslíte si, že viete, čo je to les? Tiež som si kedysi myslela, že viem - kým som nečítala texty Juraja Lukáča o tom, čo je to skutočný LES? Že to nie sú len ľuďmi do radu (aj keď nahusto) nasadené stromy - to je len ľudský paškvil lesa! V skutočnosti človek nedokáže stvoriť Les! Preto naň nemá ani siahať! A keď - má sa to robiť citlivo, s hlbokým zvážením následkov, dôsledkov, reakcií!   ___________________________________   Les je životný priestor. Je to náväznosť, životná závislosť organizmov, stromov, zvierat od tých najmenších až po obrov (aj bobrov). Je to niečo úplne jedinečné, unikátne a dokonalé, čo sa buduje stovky, tisícky rokov a vytvára dokonale funkčný, samoreprodukujúci, samoočisťujúci sa mechanizmus - ktorý sa bez zásahu ľudskej ruky zaobíde veľmi dobre. Sú to obranné mechanizmy, ktoré ho chránia, ktorých pôsobenie sa však prejaví až v horizonte mnohých rokov - nie v horizonte štyroch volebných rokov sociálno-demokratickej a inej vlády, ako si niektorí odborníci za priame zásahy do prírody myslia.   Tuším sa to nazýva aj Biotop. Alebo Ekotop? V každom prípade, Eintopf - sa to nenazýva.   ___________________________________   Podpísala som petíciu proti necitlivému a nepotrebnému návrhu zonácie Tatier a proti zbytočným postrekom drevín. Stretla som kamaráta - Marcela (Dávida). Podebatovali sme o tom, či má zmysel voliť Most-Híd a prekrúžkovať Ondreja Dostála čo najvyššie alebo SaS - či Richard Sulík dokáže ustáť Pravdu vždy, nielen, keď sa mu to hodí, keď to bude potrebovať. Ako sa ukázalo, KDH nie je naša volebná destinácia. Okrem toho, že nie sú cestou výbušnín v batožinách a iných podobných sociálno-demokratických sajrajtov - nie sú ani cestou so štipkou invencie, nápaditosti a iskry. SDKÚ vyšlo z debaty ako núdzová voľba - ale len pre kamoša. Pre mňa nie.   Prišiel čas pobrať sa domov. Dastík, napriek tomu, že mu na akcii padla do oka fešná fenka - vlčiačka - a on nie a nie sa od nej odlepiť, necítil sa tam dobre. Vadil mu hluk z potlesku mnohých ľudí, reagujúcich na spíkera a program.       Dnes sa s vami, priatelia, rozlúčim pozdravom:   Nech nám to kvapká! (veď už aj zo SAV kvaplo!)       Vaša kvapka!                                                         P.S. Tak, a teraz - hurá, sobota volá! Oblasť neplatenej a neocenenej práce nepočká. Hor sa, teda, do prania, čistenia, upratovania, umývania, starostlivosti o deti, starých, chorých i zvieratá!   .... alebo si dám radšej ešte jednu kávu?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            O Zaujatí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Situácia je fakt lepšia: Už sa len Klame, Kradne a Kupujú hlasy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Ako sme volili v porodnici alebo O "prave nevolit"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Prochazka &amp; Knazko, alebo Nevolim zbabelcov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Nemozem uz citat spravy zo Slovenska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Roy
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Roy
            
         
        zuzanaroy.blog.sme.sk (rss)
         
                                     
     
         V posledných rokoch ma najviac oslovili myšlienky a knihy Anselma Grúna - nemeckého benediktína, a slovenských feministiek z ASPEKTu. Píšu o tom, kde nám to drhne (v živote) a ako z toho von. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    152
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1606
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Foto: Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Verše bez veršov
                        
                     
                                     
                        
                            Letters to Animus
                        
                     
                                     
                        
                            Môj pes Dasty
                        
                     
                                     
                        
                            Budúci prezident - aký si?
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Alternatívne školy - Bratislavský kuriér
                                     
                                                                             
                                            Môj Prvý pokus - Démonizácia Waldorf. pedagogiky v slov.médiách
                                     
                                                                             
                                            Odkaz Sorena Kierkegaarda
                                     
                                                                             
                                            Tomáš Halík: O výchove a vzdelávaní
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Joni Mitchell - absolútna nádhera: Both Sides Now
                                     
                                                                             
                                            Anselm Grün: Zauber des Alltäglichen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dr.Christiane Northrup - Womens Wisdom
                                     
                                                                             
                                            Sloboda Zvierat
                                     
                                                                             
                                            Anselm Grün
                                     
                                                                             
                                            Tomáš Halík
                                     
                                                                             
                                            Dominik Bugár - Fotograf, kolega - rodič
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       "Suma bola zaplatená pánovi dekanovi, ktorú pán dekan schoval do zásuvky"
                     
                                                         
                       David Deida - "Intimní splynutí - Cesta za hranice rovnováhy"..
                     
                                                         
                       All inclusive a piesok na zadku. Je toto dovolenka?
                     
                                                         
                       Moja Pohoda
                     
                                                         
                       Lož na kolesách: Pád Lancea Armstronga
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Gentlemani na Wimbledone
                     
                                                         
                       Druhá šanca pre človeka
                     
                                                         
                       Facka Ficovi
                     
                                                         
                       Mantra národa.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




