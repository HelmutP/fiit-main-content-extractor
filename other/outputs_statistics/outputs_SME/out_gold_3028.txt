

 Pred siedmimi rokmi sme „ukrajinskú" Veľkú noc prestopovali v Írsku. Pamätám si, ako som stála v hostelovej izbe a česala si mokré vlasy. Cez okno otvorené dokorán vletel vrabec a dlhú chvíľu sa pokojne vyhrieval na slnkom ožiarenom koberci. Ako keby som tam ani nebola. Stála som ako obarená. Predtucha. Keď som zavolala domov, mama mi už len potvrdila to, čo som už sama tušila... 
 Na pohrebe som nebola. Boli sme asi tisíc kilometrov ďaleko a letenky sa nedali posunúť na skorší termín. Za dedkom som teda išla až v lete. Malá obec v ukrajinských Karpatoch, asi 270 kilometrov na východ od Užhorodu. Päť hodín autom po rozbitých cestách alebo osem hodín trmácania v starom autobuse. 
 Na cintoríne je tráva vyše kolien - ako na poli, kde ju dedko každé leto kosil. Sadáme si okolo hrobu, babka vyberá chlieb, slaninu a vodku. Podľa zvyku máme len jeden pohár. Prvý vždy vylejeme na hrob - dedkovi, potom už koluje z rúk do rúk. Mamin brat, chlap ako hora (keď si vypije, bojí sa ho celá dedina), sa rozplače ako malé dieťa. Spomíname. 
 Dedka si čoraz viac pamätám podľa mozaiky fotiek, ktoré visia zarámované ako obrazy v dome starých rodičov. Na jednej stene ikony svätých, na druhej - zožltnuté rodinné fotografie. Všetky ozdobené vyšívanými ručníkmi. 
 Obrázok asi štrnásťročného chlapca, ktorý drží za uzdu koňa. Vychudnutá tvár mladíka v pracovnom tábore v Nemecku. Mladý vojak. Tridsiatnik s krásnou ženou v kroji. Babka si ho vraj vôbec nechcela zobrať, sobáš dohodol sám s jej otcom. Mala iného nápadníka a tvárila sa, že to ani neprežije - nepomohlo. Nikdy som tomu príbehu nechcela uveriť, lebo ich vzťah bol pre mňa vždy vzorom skutočnej lásky. Pamätám si aj to, ako dedko s nami - štyrmi vnučkami - hrával každé leto futbal. Ako nás učil otčenáš, hrával nám na fujare a ako sme raz spolu zablúdili v lese, ktorý poznal ako vlastné topánky. Zostali po ňom kalendáre, ktoré vyrábal pre celú rodinu a niekoľko hrubých zošitov s poznámkami, ktoré si snáď niekedy prečítam. Ale nielen to... 
 Stmieva sa. Fľaša je už takmer dopitá. Ujo začína spievať, babka a mama sa pridávajú. Smutné huculské piesne. V diaľke sa zažínajú ohne. Lúčime sa a odchádzame do tmy. 
 .................................... 
 Na Veľkú noc zvykneme spomínať na tých, čo odišli. Lebo život a smrť si nikdy nie sú tak blízko. Huculi vtedy chodia na hroby, s jedlom a pitím. Pohanské zvyky sa tu tak úzko prelínajú s kresťanskými, že je veľmi ťažké ich navzájom odlíšiť. Nerozsypať soľ, nehodiť chlieb na zem, nepodávať veci cez prah... Zdanlivo nezmyselné povery, ktoré poznám už odmalička. Sú vo mne tak hlboko zakorenené, že sa nad nimi ani nezamýšľam. Cítim, že jednoducho majú svoj zmysel. A keď mám podať niečo cez prah, inštinktívne urobím ešte jeden krok dopredu. Aby nás ten prah nikdy nerozdelil. 
   
 






 

