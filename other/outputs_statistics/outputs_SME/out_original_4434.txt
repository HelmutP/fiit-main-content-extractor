
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Novák
                                        &gt;
                Domáce dianie
                     
                 Pravda o diaľniciach 

        
            
                                    12.4.2010
            o
            19:22
                        (upravené
                13.4.2010
                o
                14:06)
                        |
            Karma článku:
                7.48
            |
            Prečítané 
            2945-krát
                    
         
     
         
             

                 
                    V poslednom čase sa často krát stretávame s kritikou výstavby diaľnic na území Slovenska prostredníctvom PPP projektov. Pre menej informovaných to znamená výstavbu diaľnic s primárnym financovaním zo súkromných zdrojov a následným splácaním určenej sumy staviteľovi štátom.
                 

                 
  
   Pokiaľ by túto výstavbu najviac nekritizovalo SDKÚ DS bolo by všetko v poriadku, lebo sám nie som zástancom tohto riešenia, keďže samotná výstavba sa určite predraží, ale treba sa pozrieť na celú genézu výstavby diaľnic na Slovensku. Hneď po revolúcii tu už bolo niekoľko projektov, ktoré naznačovali akými trasami by mali diaľnice na Slovensku viesť a ako to už býva začalo sa s výstavbou najzaťaženejších trás. Najväčší rozmach výstavby bol za vlády V. Mečiara, avšak hneď po nastúpení M. Dzurindu do vlády sa rázne seklo s celou výstavbou. Nie je vôbec pravda to, čo vehementne všade tvrdí, že sa podieľal na zvýšenej podpore výstavby, ale presne tento človek sa postaral, že výstavba sa úplne zastavila na celom území Slovenska. A presne tu vidím ten moment, kedy sa celá výstavba najviac predražila, keďže od vtedy cena 1 km vzrástla o 300 percent. Je pravdou, že druhá Dzurindova vláda začala s postupným uvoľňovaním finančných tokov do výstavby diaľnic no je tiež pravdou že práve súčasná vláda vláda stavia diaľnice viac ako 3x rýchlejšie!              Presne kvôli prerušeniu výstavby v rokoch 1998-2002, sa všetko predražilo jednak o nové konania ohľadom vyvlastňovania pozemkov, nové merania a dokumentácie a najviac zvýšené náklady jednotlivým firmám, ktoré pre štát stavali.   Jediné možné riešenie, ktoré sa v súčasnosti akejkoľvek vláde ponúkalo, bola výstavba prostredníctvom PPP projektov, keďže pokiaľ sme chceli odstrániť rozdiely medzi jednotlivými regiónmi bolo nutné čo najrýchlejšie dostavať všetky úseky. Pokiaľ ide o predraženie výstavby, tak vláda  stanovila pomocou dodatkou maximálny limit navýšenia ceny o 10 % obstarávacej ceny.  Aj keď je známe, že štát bude platiť niekoľko miliónov ročne za výstavbu, je to určitou zárukou jednak pre investorov, existujúce firmy, a obyvateľov , že do ich regiónov budú vďaka lepšej infraštruktúre plynúť nové investície. To znamená väčší prílev príspevkov do štátneho rozpočtu a predovšetkým posilnenie práceschopnosti regiónov s najvyššou mierou nezamestnanosti. Jednoducho, najdrahšia diaľnica je tá ktorá nieje postavená.   Preto, by som tento krok nazval skôr ako nutné zlo, za ktoré si nemôžeme sami. Zodpovednosť nesú iní..  konkrétni  ľudia  z vlády pána Dzurindu, ktorí dnes vykrikujú že ide o zlodejstvo....                     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (25)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Novák 
                                        
                                            Pravda o diaľniciach 2, alebo Kto teraz dáva dvakrát dáva....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Novák 
                                        
                                            "Blede modrá"univerzita
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Novák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Novák
            
         
        romannovak.blog.sme.sk (rss)
         
                                     
     
        Pracujem v oblasti stavebníctva, no zároveň sa pohybujem v oblasti školstva,...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3972
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Domáce dianie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bodaj by si páni z EU vytiahli konečne hlavy odtiaľ, kde ich majú
                     
                                                         
                       Stehno veľryby
                     
                                                         
                       Základné pravidlá predpovedania budúcnosti vo volebnej kampani
                     
                                                         
                       Moja skúsenosť s očkovaním
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Ficova a Kiskova úžera
                     
                                                         
                       Fico neprisľúbil, že ďalšia kampaň bude z jeho strany férová
                     
                                                         
                       Nie je podpis, ako podpis
                     
                                                         
                       Mestský policajt sa mi vyhrážal použitím zbrane
                     
                                                         
                       Čo môže zlepiť Smer a KDH? Kotlepidlo
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




