

 
Ľudia nie sú
rovnakí, a rovnaké nie sú ani zdravotné poisťovne. Hodnotenie zdravotných
poisťovní je prirodzeným prvkom v zdravotných systémoch s viacerými zdravotnými
poisťovňami. Ak by sa totiž zdravotné poisťovne od seba neodlišovali, nebol by
tu ani priestor pre ich pluralitné fungovanie.
 
 
Health Policy Institute (HPI) vypracoval zozbieral dáta o
fungovaní zdravotných poisťovní na Slovensku a navzájom ich porovnal. A ľaľa: poisťovne sa medzi sebou dosť výrazne
líšia! Napriek názoru, že "niektoré poisťovne sú všetky rovnaké". Ktorá je teda lepšia?
 
 
Na takúto
otázku nejestvuje jednoznačná odpoveď. Každý totiž vníma priority odlišne, pre niekoho
je napríklad dôležitejšia kvalita, pre iného dostupnosť. Niekto ocení, že
poisťovňa platí lekárom načas, iný sa uspokojí so zľavou na okuliarové rámy.
 
 
Podľa ratingu HPI je najlepšou zdravotnou poisťovňou na
Slovensku Dôvera nasledovaná Unionom. Nie každý 
sa však s HPI musí stotožniť v tom, ktoré parametre zdravotnej poisťovne
sú dôležitejšie a ktoré menej.
 
 
Práve preto
pripravilo Sme.sk  spolu s HPI možnosť urobiť si svoj vlastný, individuálny rating
zdravotných poisťovní. Projekt beží na adres poistovne.sme.sk. Stačí, ak podľa vlastných priorít
pridelíte váhu jednotlivým porovnávaným oblastiam a ukazovateľom. Vyberte si to,
čo považujete za dôležité a zistíte, ktorá poisťovňa najlepšie vyhovuje vašim
požiadavkám.
 
 
Tomáš Szalay  
 
 
P. S. Pár
metodických poznámok:
 
 
	 
	Aby
	ste nemali priveľa klikania, prednastavené sú váhy podľa HPI, ktoré však
	samozrejme môžete meniť. 
	 
	Odporúčanie:
	neignorujte úplne finančné ukazovatele zdravotných poisťovní. Problémy v nich
	bývajú predzvesťou problémov pri poskytovaní zdravotnej starostlivosti a
	platobných podmienkach. Napríklad Európska zdravotná poisťovňa skončila vlani v
	strede ratingu, a to najmä vdaka veľkorysým zmluvám s poskytovateľmi a ponukou
	nadštandardných služieb. Keby boli mali finančné ukazovatele vlani v ratingu vyššiu
	váhu, EZP by skončila horšie. 
	 
	Za
	inšpiráciu ďakujeme projektu glassbooth.org (zistite, kto je vám názorovo bližší,
	McCain alebo Obama!). 
 

