

 
  Ľudské bytosti toho vydržia naozaj veľa. Človek dokáže zaťať zuby a znášať bolesť, strach aj úzkosť, preniesť sa cez ťažké životné situácie, tragédie, úmrtia blízkych. Vie sa  naučiť existovať a takmer plnohodnotne fungovať aj bez zraku, bez sluchu, či na vozíčku. Čo je však pre človeka asi najväčšia rana, je strata vlády nad sebou samým. Keď sa telo jednoducho vplyvom veku a chorôb opotrebuje, prestane počúvať príkazy z mozgu, až vypovie službu úplne. 
 
 
  Hovorím o ľuďoch na sklonku života, ktorým ostala už iba posteľ, zašpinená plienka, túžba po uhasení smädu, zmiernení bolesti, či pohľade na známu tvár. Najhoršie je, ak dotyčnému ostane kúsoček zdravého rozumu, ak má zachovanú schopnosť rozhodovať sa. Vtedy si svoj bezútešný stav uvedomuje a okrem samotného utrpenia z choroby, ho zožiera aj pocit poníženia a zahanbenia.  
 
 
  Hovorím o kvantách našich seniorov, ktorých si zdravotnícke zariadenia prehadzujú ako horúci zemiak. Čaká sa len nato, kedy ten "otravný" dedo, ktorému stále niečo vadí, chytí zápal pľúc a prestane otravovať navždy. 
 
 
  Investovať do ekonomicky neproduktívneho obyvateľstva dnes totiž u nás nie je v móde. Peniaze treba napumpovať tam, kde sa dá očakávať uzdravenie, navrát do práce, či aspoň dlhodobé prežívanie. Veď kto by už dnes, v dobe klonovania, počul tie starecké stony? Iba ten, kto chce. 
 
 
  Myslím si, že hlavná chyba nie je v už existujúcich zariadeniach. Chyba je v tých neexistujúcich. Liečebne pre dlhodobo chorých, alebo hospice, nie sú nato, aby sa tam starčekovia a starenky dusili vankúšmi, či podľa holadského "humánnejšieho" modelu zabíjali injekciami. Podstata je umožniť im dôstojné dožitie života na tejto zemi. 
 
 
  Naozaj si myslíme, že je to také nepodstané? Chcú títo ľudia tak veľa? Nezaslúžia si to?   
 
 
 A uvedomujeme si, že ten vysušený, stonajúci úbožiak s dušou na jazyku, žobroniaci, aby mu niekto ovlažil pery, môžem byť jedného dňa pokojne aj ja sám? 
 
    

