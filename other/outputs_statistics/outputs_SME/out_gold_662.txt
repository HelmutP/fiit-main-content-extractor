

 
 
 
Toto je môj osobný názor, toto je môj postoj k cirkvi.
Toto je citát mňa samého z jednej z mnohých diskusií s veriacim
človekom. Scenár je takmer vždy ten istý. Ja tvrdím, že cirkev nám do hláv
tlačí kaleráby, že klame, podvádza a snaží sa nás všetkých presvedčiť
o REÁLNEJ a FYZICKEJ existencii Boha, Satana, neba a pekla,
anjelov a čertov. Na to ma vždy veriaci intelektuál vysmeje, chrstne mi do
tváre myšlienky novotomizmu, kresťanskej filozofie, atď. atď.
 
Áno, priznávam, toto všetko poznám. Že s kresťanskou
filozofiou a novotomizmom ani tomizmom nesúhlasím, je už druhá, momentálne
nepodstatná, vec. Je však paradoxné, že o týchto veciach som sa dozvedel
na hodinách NOSky na strednej škole. Nie v kostole, na birmovných
stretnutiach ani na katechizme. Tam nás učili a vždy nám vtĺkali do hlavy
to, že pod zemským povrchom je peklo, horí tam oheň a zlé duše sa tam
pražia. Rozprávky? Moje výmysly? Tento krát mám v rukách hmatateľný dôkaz.
 
 
 
 
 
Začnime od toho, že z úcty ku svojim rodičom a pre
pokoj ich duše som súhlasil, že sa tento rok zúčastním sviatosti birmovania.
Nebudem tu rozoberať to, či je alebo nie je správne nútiť svojho 17 ročného
syna robiť to. Ja som sa rozhodol spraviť to najmä pre to, aby som mal konečne
pokoj, aby som doma nemusel počúvať výčitky a aby som (hoci takýmto možno
až zvráteným spôsobom) spravil svojim rodičom radosť. To nechajme bokom. 
 
 
Na poslednom stretnutí birmovancov si naša skupina vypočula
CD s názvom „Svedectvo o pekle“. Pri počúvaní sa mi skutočne
zastavoval rozum, s otvorenými ústami som počúval a nevedel som, či
sa mám smiať, alebo plakať.
 
 
Dnes som sa skontaktoval so svojou animátorkou (ženou, ktorá
vedie moju skupinu a pripravuje nás na birmovku) a pokrytecky sa
tváril, že sa ma posledné stretnutie dotklo, oslovilo ma a chcel by si
nahrávku ešte raz v pokoji vypočuť. Dohodol som sa s ňou
a prišiel si po ňu k nej domov. Tu som ju skopíroval do svojho
počítača a uložil na internetový server. Tak, aby som úžasný zážitok
z jej vypočutia umožnil i všetkým Vám, najmä však veriacim, ktorí sa
mi vždy smiali, keď som o cirkvi tvrdil to, čo som o nej vždy tvrdil.
Páčisa: 
 
 
 
 
 
http://rapidshare.com/files/117815544/svedectvo_o_pekle.rar
 
 
 
 
 
(Ide o pomerne veľký súbor (niečo vyše 50
megabajtov) preto som ho uploadol na rapidshare.com. Každý s minimálnymi
znalosťami angličtiny bude schopný stiahnuť ho odtiaľ. Stačí klaknúť na FREE,
potom počkať nejaký čas, odpísať verification code (možno budete musieť odpísať
iba tie znaky, pri ktorých bude symbol mačičky, nie psíka, pozor na to)
a môžete sťahovať. Je to praktické najmä pre rýchly download tohto
serveru)
 
 
(Upozornenie! Súbor „Svedectvo o pekle“ je voľne
šíriteľný, NEJDE o warez!)
 
 
 
 
 
Obsahuje štyri stopy.
 
 
1.- Intro. Lacné zvukové efekty a blábol autorky.
 
 
2.- Svedectvo. Muž, Američan, v televíznej talkshow
hovorí o svojom zážitku, kedy ho (podľa jeho rozprávania) Ježiš vzal na
vyše 20 minút priamo do pekla. Opisuje ho ako miesto reálne existujúce cca.
2000km (on však udáva na metre presné údaje) pod zemským povrchom. Hovorí
o celách, démonoch ako skutočných bytostiach, neznesiteľnom teple, ohni,
priepastiach kde sa ľudia piekli v plameňoch a démoni nad ich okrajmi
stáli a zhadzovali dnu tých, čo sa snažili vyštverať nahor. Plné scén ako
z lacného rádoby hororu, celé rozprávanie by som hodnovernosťou prirovnal
k blábolom vodcu Vesmírnych Lidí.
 
 
3.- Ak ma predchádzajúca časť načala, táto ma zabila. Muž
číta článok údajne uverejnený v istom fínskom časopise, kde ruský vedec
tvrdí, že na sibíri robili vrt do hĺbky asi 2000 metrov a prekopali sa
priamo do pekla. Spustili tam mikrofón a počúvali miliardy ľudí kričať.
Nasleduje pasáž, ktorá má zrejme byť danou nahrávkou kričiacich ľudí. Ja to
odhadujem na stanicu moskovského metra.
 
 
4.- Outro, záverečný blábol autorky.
 
 
 
 
 
Túto nesmierne poučnú nahrávku odporúčam naozaj každému.
Oplatí sa to počuť. Oplatí sa zamyslieť sa nad cirkvou a nad tým, čo sa
nám snaží nahovoriť. Jediné, čo ma desí je fakt, že najmenej tri štvrtiny
členov mojej prípravnej skupiny tomu, čo počuli, do slova a do písmena
uverili. Ako inak to nazvať, čím to je, ak nie vymývaním mozgov?
 
 
 
 
 
Samozrejme, odkaz pre mojich veriacich priateľov a oponentov
- už sa teším, ako spôsobom pokryteckým a vám vlastným, znova raz zahodíte
horúci zemiak a zvalíte vinu na konkrétneho bláznivého kňaza. Poviete, že
je to prípad jednotlivca, nie celej cirkvi. To je však iba pokrytecké zvalenie viny na jediného človeka, pričom sa takéto a podobné veci dejú denne na mnohých miestach. Toto je len špička ľadovca,
jedna z mála informácií, ktoré sa dostávajú na povrch (i keď iba na
sme.blogu, no je to najviac, čo JA dokážem spraviť). Tých konkrétnych
a vraj ojedinelých zlyhaní kňazov a duchovných sa už nakopilo toľko,
že si dovoľujem zovšeobecňovať a tvrdím to, čo som vždy tvrdil: Cirkev je
zvrátená organizácia, obyčajná sekta, snažiaca sa vymývať nám mozgy, aby
s nami mohla manipulovať.
 
 
Za pravdu mi dáva história, milióny konkrétnych prípadov zo
sveta, ale najmä osobné skúsenosti. A to nielen s jedným duchovným.
 

