
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Miluj blížneho svojho... 

        
            
                                    8.6.2010
            o
            6:19
                        |
            Karma článku:
                7.36
            |
            Prečítané 
            731-krát
                    
         
     
         
             

                 
                      Naozaj dlho som premýšľal, ako sa pokonám s nápadom zaujať stanovisko k veciam, čo už naozaj dlho trápia svetové kresťanstvo a hľadal som voľajaký, na Slovensku dobre zmapovaný priestor, kde by sa hneď nemuselo vyrukovať s násilím, pedofíliou, homosexualitou a zneužívaním nevinných na iné spôsoby, lebo akurát to je priestor viac ako nebezpečný. Ľudia všetko s tým súvisiace akceptujú zle, cirkev samotná nedokázala dostatočnú sebareflexiu a hoci sa hriechy už dávno pomenovali, rozčarovanie ľudského zlyhania nikoho nectí. No potom ma napadlo pomôcť si osobnosťou, s ktorou som sa osobne stretával, ba podieľali sme sa na plnení  rovnakých úloh a bol som presvedčený, že je to dobrý kňaz, vynikajúci biskup a dobrý človek, ale... veď čítajte!
                 

                     Korešpodencia medzi súčasným trnavským arcibiskupom Róbertom Bezákom a jeho predchodcom Jánom Sokolom, ktorý sa v dobrom zdraví a pohode dožil penzijného veku, by mala byť podľa akýchkoľvek kritérií seriózna, dobroprajná a nanajvýš korektná, lebo veď uznáte, špičky našej klerikálnej hierarchie svoj život žijú podľa presne daných kánonov a na očiach kresťanskej i všeobecne občianskej verejnosti. Ak už nič iné, tak desať Božích prikázaní a predovšetkým to z titulku tohto príspevku by malo mať charakter striktného a nemenného rozmeru, lebo raz darmo, takéto celebrity sú jednoznačnými reprezentantmi a každé zlyhanie sa stáva viac ako len precedensom!   Lenže, tentoraz sa udialo niečo celkom mimoriadne, niečo, čo všetky očakávania zvalilo ako domček z karát a Slovensko sa dočkalo potvrdenia starej pravdy, že naozaj nie je zlato všetko to, čo sa blyští. Obsah listu sa skladal z výhrad a želaní, ktoré si Ján Sokol ani vo svojej nedávnej nedotknuteľnosti a neomylnosti nemôže nevšímať a už vôbec nie ich ignorovať, lebo z pozície výkonu arcibiskupskej moci sa jeho každodenný život upravuje celkom radikálne, ba vlastne, čo lepšie vystihuje súhrn príkazov súčasného šéfa trnavskej arcidiecézy, doslova minimalizuje.   Má zákaz slúžiť bohoslužby a keďže sa na budove diecézneho úradu vymenili všetky zámky, od ktorých samozrejme až doteraz vlastnil kľúče, má vstup dovolený iba vtedy, ak na to dostane osobný súhlas arcibiskupa Bezáka. A akosi naviac , čo je pre mňa želaním na úplnej hranici poníženia, sa Ján Sokol dožil výhrady, presne definovanej strohými slovami, aby so súčasným arcibiskupom nejedával v jednej miestnosti! Je to teda niečo na spôsob toho starodávneho „rozvodu od lôžka i stola“ a spolu s odporúčaním, aby sa odsťahoval z blízkosti diecézneho úradu, je to jasná degradácia celoživotných zásluh .   Do môjho života vstúpil Ján Sokol niekoľkokrát celkom osobne. Po prvý raz vtedy, keď sa po roku 1989 začalo budovať cirkevné školstvo a nie vždy to bolo jednoduché, lebo občas sa staré krivdy mali tromfnúť novými a vtedy som jeho veľkorysé uznávanie faktov oceňoval, no potom som sa dozvedel o jeho činnosti na Sereďskej fare, kde úzko spolupracoval s najvyššími straníckymi špičkami priamo z Bratislavy a sklamanie i rozčarovanie a veru aj hnev prišli potom, keď sa prevalila aféra jeho spolupráce s ŠTB. Jasné, človek, ktorého prezentácia a post v akejkoľvek spoločnosti má taký kredit, akým je kňaz našej najrozšírenejšej cirkvi, takéto zlyhanie nemôže ustáť. Prehral a nie je ničím iným než stroskotancom a bolo nanajvýš interesantné, ako dlho sa darilo definitívny koniec toho zdanlivo dobre prežitého a národu prospešného života kamuflovať a zahmlievať.   Až teraz je tu teda ten deň zúčtovania a okrem toho doteraz jasného a poriadne nechutného zlyhania sa národ dozvedá veci, ktoré súvisia s netransparentným hospodárením s financiami a keďže to predstavuje sumu viac ako 55 miliónov korún, vôbec sa arcibiskupovi Bezákovi nedivím, že nedostatky uvádzal výsledkami auditu, pátraním po dokumentoch a vyslovením všetkých už citovaných výčitiek!   Bič teda predsa len pleskol a daj Bože, aby to nebolo neskoro a aby sa žiadna snaha očistiť zdiskreditované meno cirkevného preláta už nikto nesnažil vylepšovať. V cirkvi, aj v tej katolíckej, je akurát teraz ten čas, keď je odvaha vysvetľovať hriechy a naprávať ich a som si istý, že nielen tie, napríklad írske, ale aj tie naše, si to celkom zaslúžia. Spravodlivosť by mala byť jednoznačná, adresná a presne vykonateľná a to veru aj v prípade Jána Sokola, ktorého životné peripetie vôbec neznesú označenie, že bol dobrý a spravodlivý muž. Človek človeku bratom je pekné, miluj blížneho svojho, ako seba samého celkom určite tiež, ale okrem odpúšťania a lásky je na svete aj spravodlivosť a tá nech ľudstvu dominuje.       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




