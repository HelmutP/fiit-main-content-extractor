
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Oskar Bardiovsky
                                        &gt;
                Nezaradené
                     
                 Mediomorfóza sveta 

        
            
                                    23.4.2010
            o
            10:08
                        |
            Karma článku:
                1.78
            |
            Prečítané 
            747-krát
                    
         
     
         
             

                 
                    Konečne zaujímavá kniha o médiách, a to dokonca od slovenského autora. Mediomorfóza sveta je intelektuálne náročné čítanie.
                 

                     Autor sa zameral najmä na to, ako sa zmenil film  - ako reprezentant najvýznamnejšieho obrazového média svojej doby - a s ním (ako aj v dôsledku jeho pôsobenia) aj spoločnosť. V podstate je to úvaha o význame obrazovej informácie v súčasnej spoločnosti a o zmenách, ktoré priniesli moderné media.   Kniha je zaujímavá aj tým, že vychádza takmer výlučne z literatúry, ktorá nie je strikne vedecká. Hlavnými zdrojmi úvah B. Malíka sú memoáre režisérov a rozhovory s nimi,  knihy filozofov 19. a 20. storočia (a výnimočne aj z iných období), a úvahy filozofov komunikácie ako boli napríklad Flusser, McLuhan alebo Baudrillard.    Samozrejme, niektoré tvrdenia autora netreba brať doslova, ide často skôr o metafory , zveličenia či zjednodušenia, naznačujúce limity filozofickej analýzy.    Súčasne však autor filozoficko-psychologicky objasňuje mnohé nedávne a súčasné mediálne javy ako sú napríklad reality show či televízne seriály.        Na záver len dve myšlienky zo posledných strán knihy:        “…hľadanie exkluzivity končí uniformizáciou a banalizáciou a tiež premenou nenormálneho a úchylného na normu.” (s.143).       “Anulovanie lineárneho času kultúrou technických obrazov, a teda aj anulovanie existenciálneho rozmeru smrti, je aj smrťou človeka ako človeka.” (s.153).               Branislav Malík, Mediomorfóza sveta. Filozofické, antropologické, sociálne a politické aspekty súčasných médií. Bratislava: IRIS, 2008 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Oskar Bardiovsky 
                                        
                                            Čierne slnko geopolitiky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Oskar Bardiovsky 
                                        
                                            Problem Doroty Nvotovej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Oskar Bardiovsky 
                                        
                                            Morálne hodnoty denníka Sme
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Oskar Bardiovsky 
                                        
                                            Nepodstatné táranie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Oskar Bardiovsky 
                                        
                                            Plytké úvahy o slovenskej politike
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Oskar Bardiovsky
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Oskar Bardiovsky
            
         
        bardiovsky.blog.sme.sk (rss)
         
                                     
     
        a tak si tu obcas kritizujem a kritizujem a kritizujem...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    36
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1827
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Podvody na bratislavskom magistráte
                     
                                                         
                       Načo sú nám kontroly? Načo, pán Trebuľa?
                     
                                                         
                       Vám neodpovieme. Ste len e-mail
                     
                                                         
                       Poburuje ma, ak televízie ukazujú utrpenie matky pri mŕtvom dieťati
                     
                                                         
                       Prečo Vám kapitálová životná poistka nikdy nezarobí?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




