
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Edita Vyšná
                                        &gt;
                Nezaradené
                     
                 Čo možno nájsť pri ceste 

        
            
                                    24.6.2010
            o
            10:40
                        (upravené
                24.6.2010
                o
                12:19)
                        |
            Karma článku:
                5.49
            |
            Prečítané 
            980-krát
                    
         
     
         
             

                 
                    Prečo na Slovensku musia byť také tupé bilboardy? Ak aj majú nápad, tak sú často vulgárne, príliš a nevhodne šteklivé, urážlivé. Reklama a marketing musí byť, rovnako ako politické kampane, to je jasné. Čím viac Fica v jednom meste (v tom mojom sa drží na jednom s naším primátorom, ešte aj teraz, nech vidno, kto za koho kope), tým lepšie. Klamať priamo pri frekventovaných komunikáciách, ešte sa aj podsvietiť, predsa treba. Okolie cesty však možno poňať aj inak.
                 

                 
  
       Nemyslím, že niekde majú takú hustotu bilboardov ako na Slovensku. Či sa radi dívame na bilboardy, kde si dievča sťahuje nohavičky, alebo na bilboard pre "národovcov", ktorých má osloviť slogan "Aby sme sa zajtra nečudovali, keď bude slovenská vlajka zeleno-červeno-biela", máme ich všade. Asi ešte v Českej republike sú na tom podobne. Tie politické tam sú (áno, tiež ešte stále tam sú) takisto skutočne obohacujúce..Najmä na umretie vyzerajúci Mirek Topolánek s heslom „Jsem unavený muž" (na obr. v perexe). Dobrý ťah konkurencie. Antikampaň namiesto vlastnej kampane, aby si nikto nevšimol, že ju vlastne máme o ničom.   Oproti konzumu a politikárčeniu, ktoré tu my máme, by som chcela vyzdvihnúť myšlienku bilboardov v Nórsku (obrázok ). V rôznych, asi troch variantách, je zobrazený jeden v bielom a jeden jedinec v čiernom ..Ten v čiernom objíma toho bieleho v štýle „Som tvoj záchranný pás". Upozorňujú na nebezpečenstvo na cestách. Sú to jediné bilboardy pri cestách, ktoré v Nórsku nájdete..A sú krásne zvládnuté a majú krásnu symboliku. Poviete si, načo by tam aj boli reklamné bilboardy, veď po nórskych cestách prejdú dva autá za deň...To je skoro pravda..pri niektorých...Ale potom načo by tam bol aj tento bilboard?- Cieľovou skupinou je život vodičov a ich spolujazdcov, je jedno, koľko ich danou cestnou komunikáciou prejde. Len aby prešli v zdraví.   A ešte jednu super vec by som rada predostrela. Tabule v Austrálii- Diaľkové návesti s humorným nádychom "Please arrive alive" (Prosím dojdite živí), ktoré vám držia palce, aby ste v pohode došli ešte tých 39 km, keď už idete 1000...Nič o tom, že tam nájdete bufet s Coca Colou..Iba to, že prosím, držte sa a príďte živí/ý.                     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            Teach First
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            Dobrá kampaň nevládnej organizácie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            ani tak..a hlavne nie po francúzsky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            bude lepšie-a..už je!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            Nevedecky a nadšene detsky o faune Austrálie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Edita Vyšná
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Edita Vyšná
            
         
        editavysna.blog.sme.sk (rss)
         
                                     
     
         rada píšem.. o veciach, ktoré ma zaujali, oslovili, nahnevali. neznamená to, že to bude niekto ďalší považovať za hodné opisu;) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    137
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    919
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            enviro
                        
                     
                                     
                        
                            rozjímanie
                        
                     
                                     
                        
                            USA
                        
                     
                                     
                        
                            šport
                        
                     
                                     
                        
                            Brusel
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prezident...náš sused
                     
                                                         
                       Ľúbime ťa, prepáč ...
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




