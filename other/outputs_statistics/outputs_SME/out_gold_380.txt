

 
Sniper ( slovensky ostreľovač) je niekedy bežnými ľudmi braná ako mýtiská postava (podobne ako SWAT, Delta force ci Specnaz). Ide však o človeka, i keď nie úplne bežného. Sniper musí byť inteligentný trpezlivý a mať množstvo znalostí. A byť dobrý strelec. 
 
 
No v prvom rade nie je len dobrý strelec ale hlavne znalec balistiky. Dôvod? nuž skúste na 100 metrov trafiť 0,33l plechovku. Nebude stačiť ani tá najlepšia puška a ďalekohľad. Ďaľším článkom je znalosť svojej zbrane a streliva. Plus znalosť ľudskej anatómie. 
 
 
Prečo? Nuž pomaly (relatívne) letiaci projektil  má síce nižšiu energiu dopadu, môže však z dôvodu rotácie poškodiť telo vážnejšie než rýchla guľka. Pomalé projektyli sa rýchlejšie vyklonia z dráhy a odovzdajú svoju pohybovú energiu okoliu. Sniper však potrebuje presnosť a vysoký dostrel. K tomu môže mať cieľ nepriestrelnú vestu. Takže musí poznať zbraň a potrebuje si vybrať presný druh munície vhodný na zásah. 
 
 
Vo vojenských konfliktoch dochádza k stretu do 200 metrov (väčšinou v poli okolo 150-200 v mestskej aglomerácií do 100 metrov). Sniper operuje na 2-3 krát väčšiu vzdialenosť. V extrémnych alebo špecifických prípadoch do 1-1,5 kilometra. Na takú vzdialenosť nie je vhodné bežné strelivo.
 
 
Takže najznámejšie mýty o sniperoch:
 
 
čo výstrel to smrteľný zásah (One shot - One kill) - nikto nie je dokonalý ani špecialisti. Táto rovnica teoreticky platí do 500-600 metrov (pol kilometra!) ale netreba zabúdať, že nie každý zásah je smrteľný. Vojenský sniperi sa snažia usmrtiť, policajný vyradiť s činnosti a na to niekedy stačí zásah do ramena či nohy. 
 
 
Sniper je obyčajný zabiják - nuž je to povolanie ako každý iný vojak či policajt. Má akurát lepšie skóre v počte zásahov 
 
 
Je vyslovene samotár - nuž tak to je veľký omyl. Skoro vždy sú dvaja, strelec a výpočtár (zároveň chráni snipera) ktorý sleduje smer vetra, okolie a upozorňuje strelca. 
 
 
Akákoľvek automatická puška je vhodná pre snipera - Nuž vačšina sniperov si vyberá práve poloautomatické pušky. Sú spoľahlivejšie a hlavne väčšinou chcú špeciálne. Ako je SVD (Sniperskaja Vintovka Dragunova) slávny Dragunov, HK(Heckler-Koch) PSG-1, HK SR-9 (športová puška),  CZ 700, Barrett M82 a podobne.
 
 
  
 
 
Najjednoduchšie to má v boji - Nuž dovolím si taktiež nesúhlasiť. V minulosti bežný vojak bol zajatý. Vo Vietname kde si Američania spravili hanbu boli bežne vojaci zajatý a následne vymenený. Od prvej svetovej vojny však sniper bol zabitý pri zajatí a tak to je vcelku aj dnes.  
 
 
Každý lepší strelec môže byť sniper - Nuž aj áno aj nie. Ako som uviedol vyššie nejde len o streľbu. Robia zároveň pozorovania, musí vedieť čo sa deje na bojisku a podobne. Existujú tri oblasti ktoré sniper musí zvládnuť - streľba, bojový výcvik (poľný), stratégia a taktika. Streľba je jasná, poľný výcvik mu umožní prežiť a pohybovať sa v boji. vie podľa neho odhadnúť čo by sa mohlo stať. No a ako taktik a stratég vie podať informácie na velenie alebo vie odhadnúť potencionálny cieľ. U policajných sniperov je to trochu iné. Vie sa pohybovať v meste, nájsť vhodnú pozíciu a pokiaľ má možnosť streľby musí odhadnúť správny okamih výstrelu.
 
 
Sniper sa niekam uloží a je to - nuž tu nejde ani o mýtus ako úplnú hlúposť. Dôležité je aby bol správne nasadený. inak stráca svoj význam a stačí poslať a riskovať život radového vojaka.  
 
 
Nemá na bojisku konkurenciu a nepriateľa - nuž najväčším nepriateľom snipera je druhý sniper. Terorista či obyčajný vojak väčšinou snipera ani nenájdu. Iný sniper však pozná možnosti a premýšľa inak.
 
 
Každá puška pre snipera má tlmič výstrelu - no ide skôr o výjnimku. Tlmič je účinny iba u strely, ktorá opúšťa hlaveň zbrane a nedosahuje rýchlosť zvuku. Ide o samopaly (SMG SubMachine  Gun)i keď i tam existujú výjnimky. Pre porovnanie guľka z Dragunova má rýchlosť 830 m/s, z PSG-1 868 m/s obe sú to sniperky. Samopal HK MP-5 od 315 (SD verzia s tlmičom) do 400 m/s, CZ vz 51 (Škorpión) 320 m/, oba sú SMG. Útočné pušky M-16 930 m/s, AK-47 710m/s a CZ SA-58 705 m/s. Pre úplnosť rýchlosť zvuku je 344 m/s. Sniperky s tlmičom sa používajú skôr občasne kvôli krátkemu dostrelu a nízkej účinnosti. 
 
 
Každý už raz niekoho zastrelil - Taktiež hlúposť. Mnohí sniperi nikdy neboli a snáď ani nebudú v ostrom nasadení 
 
 
Sniper stráca v mieri význam - nie je to pravda. Slúžia ako zaisťovanie, prevencia či ochrana objektov z vysokou dôležitosťou 
 
 

 
 
Vo vojnách umierajú ľudia, často pre absolútnu hlúposť, v mieri sa nájde dosť situácií kde sniper tiež nájde svoje uplatnenie. Napríklad ochrana vládnej schôdzky. Väčší počet ich bol nasadený napríklad pri schôdzke M. Dzurindu a A. Šarona 11. 11.
2003.  
 

