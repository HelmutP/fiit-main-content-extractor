
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Lisinovič
                                        &gt;
                Z mojej papierovej dielne
                     
                 Skutočnosť a model: Modrý kostolík v Bratislave 

        
            
                                    6.5.2010
            o
            11:23
                        |
            Karma článku:
                13.65
            |
            Prečítané 
            5000-krát
                    
         
     
         
             

                 
                    Je celý modrý a stojí na Bezručovej ulici v centre mesta. Rímskokatolíckemu kostolu sv. Alžbety nik nepovie inak ako Modrý kostolík. Do mojej zbierky mi pribudol aj jeho papierový model...
                 

                    Kostol bol postavený v rokoch 1910-1913 v secesnom štýle. Projektoval ho budapeštiansky architekt Edmund Lechner.         V blízkosti kostola je Gymnázium Grösslingová. Zaujímavosťou je, že kostol sa pôvodne nachádzal v jeho areáli a slúžil ako školská kaplnka.               Sv. Alžbeta, ktorej je kostol zasvätený, bola uhorská svätica a dcéra Ondreja II. z kráľovského rodu Arpádovcov. Jej relikvie sú uložené v kostole.         Vystrihovačka Modrého kostolíka vyšla v tohtoročnom februárovom čísle časopisu Fifík. Celkom jednoduchá stavba vhodná aj pre modelárov - začiatočníkov má rozsah dve strany A4.            Na záver niekoľko fotografií hotového kostola. Predstavuje môj štvrtý dokončený model tento rok.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Skutočnosť a model: Grassalkovichov palác v Bratislave
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Putovanie po slovenských Kalváriách (194) - Klokočov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Lisinovič
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Lisinovič
            
         
        lisinovic.blog.sme.sk (rss)
         
                        VIP
                             
     
         Študent študujúci v škole života. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    519
                
                
                    Celková karma
                    
                                                6.42
                    
                
                
                    Priemerná čítanosť
                    2265
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šaštín
                        
                     
                                     
                        
                            Slovenské Kalvárie
                        
                     
                                     
                        
                            Z mojej papierovej dielne
                        
                     
                                     
                        
                            reportáže z modelárskych akcií
                        
                     
                                     
                        
                            v prírode
                        
                     
                                     
                        
                            ulicami miest
                        
                     
                                     
                        
                            železnice a MHD
                        
                     
                                     
                        
                            motorizmus
                        
                     
                                     
                        
                            viera
                        
                     
                                     
                        
                            extra
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj brat
                                     
                                                                             
                                            Moja manželka:)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            railtrains.sk
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




