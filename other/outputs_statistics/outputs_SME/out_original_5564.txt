
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Wiezik
                                        &gt;
                Súkromné
                     
                 Pomoc Poľane (tak trochu ako v Krylovej piesni) 

        
            
                                    30.4.2010
            o
            16:35
                        (upravené
                25.6.2010
                o
                14:44)
                        |
            Karma článku:
                9.16
            |
            Prečítané 
            1680-krát
                    
         
     
         
             

                 
                    Aj capko záhradník by zostal zahanbene stáť v kúte. Krátka informácia o záchranných krokoch pre prales na Poľane, z dielne Generálneho riaditeľstva Lesov SR.
                 

                 Neteší ma poukazovať na rôznych típkov alá Vojtek a ich fundované výplody, navyše si myslím, že podobných ukážok profesnej nespôsobilosti a diletantizmu je okolo nás toľko, že ľudia, sa aj napriek kolosálnemu rozmeru (a žiaľ praktickému dopadu) takýchto prešľapov, k nim postupne stávajú necitliví. Blbosť, podobne ako porno, postupne zovšednie, hlavne keď sa s ňou stretávate príliš často.   Avšak ako zakaždým predtým aj teraz musím. Musím ukazovať prstom a kričať. Hľa, takto si štátni lesníci predstavujú pomoc Poľane! Link, k predmetnej správe nájdete tu.      
          Začali prípravy na záchranu porastov na Zadnej Poľane (V Banskej Bystrici, 29.apríla 2010)      Legislatíva Slovenskej republiky upravuje povinnosti obhospodarovateľov lesov v záujme zabezpečenia ich priaznivého stavu hneď dvojnásobne:   1. Z pohľadu hospodárenia musí hospodár zamedziť rozširovaniu podkôrnikovej kalamity   2. Kvôli ochrane životného prostredia musí zachrániť európsky biotop - smrekový porast     
   
 „My nemáme záujem ťažiť na Zadnej Poľane drevo pre zisk, len musíme zabezpečiť, aby sa lykožrút nešíril z V. pásma ochrany prírody do okolitých hospodárskych lesov a tiež aby nezničil biotopy, vďaka ktorým je Zadná Poľana taká vzácna." - komentuje prípravné práce Ing. Peter Morong, rozvojovo technický riaditeľ š.p. LESY SR.     V utorok 27. apríla 2010 sa na pracovnom stretnutí v Kriváni dohodli zástupcovia Štátnej ochrany prírody, š.p. LESY SR a Lesoochranárskej služby na tomto postupe:   Tri pracovné skupiny zložené zo zástupcov všetkých zúčastnených organizácií vypracujú do 14.mája 2010 komplexný materiál s podrobným popisom každej napadnutej jednotky lesného porastu na Zadnej Poľane, aj s analýzou suchého porastu, vyznačením živých chrobačiarov, návrhom opatrení a technológie odkôrnenia a ťažby.   Následne sa 17.mája 2010 stanoví podľa naliehavosti záchrany smrekového porastu presný harmonogram odkôrnenia a ťažby. „Dúfam, že sa nebudeme sedemnásteho baviť o tom, či je výnimka Ministerstva právoplatná alebo nie. Chceme sa dohodnúť, kde začne realizácia záchrany aj s použitím motorových píl." pripomína Morong.    Ostáva vyriešiť ako sa naloží s vyťaženým a odkôrneným drevom. V záujme uplatňovania enviromentálnych postupov navrhuje š.p. LESY SR, aby v niektorých porastoch bolo vyťažené a očistené drevo ponechané v poraste. Chcú preto žiadať Ministerstvo životného prostredia o dotáciu na záchranu smrekových biotopov."   Vzhľadom na to, že je von tak krásne (a ja chcem k tej kráse prispieť svojou prítomnosťou), len pár malých postrehov k tejto zvesti:   Legislatíva nehovorí nič o európskom biotope, a smrekový porast nie je biotop. Žiaľ som presvedčený, že formulácia v 2. bode nie je preklepom.   Zamedzenie šíreniu lykožrúta z V. stupňa do okolia je činnosť záslužná. Neviem ale na základe čoho, by ju niekto niekedy mal vykonávať inde ako v ochrannom pásme. Tu musí isť o zabránenie šíreniu, a nie o asanáciu priamo v piatom stupni - ten musí zostať nedotknutý. Zasahovanie v piatom stupni, je v rozpore s proklamovanými úmyslami, priamo ničí biotopy, likviduje prales.   Nechápem, ako sa môže ŠOP SR zodpovedná za ochranu prírody, dohodnúť na postupe, ktorého cieľom je aktívna asanácia stromov v pralese.   Nechápem, ako môže zamestnanec štátneho podniku Ing. Morong bagatelizovať to, že mieni zasahovať do pralesa v piatom stupni na základe pochybnej výnimky. Neviem, ako môže vôbec pomyslieť na použitie píl v pralese a piatom stupni, bez toho aby vedel, či je výnimka právoplatná alebo nie?!   Náznak o možných dotáciách na "záchranu smrekových biotopov" dávam do pozornosti len preto, že veľmi aktuálne poukazuje na trend, ktorý logicky nastupuje z dielne na profit zameraných manažérov, po tom, ako ťažené drevo nepredstavuje lukratívny sortiment. Náklady na takúto "pre biotopy prospešnú" asanáciu, nie sú nízke, a už vôbec nie, keď by mali byť štátnou zákazkou financovanou MŽP SR. Upozorňujem, že takáto a podobné požiadavky zo strany lesníkov budú stále častejšie. V skutočnosti vždy šlo o peniaze, zaodeté do populizmu a ľúbivých rečí o pomoci prírode.   Na záver už len toľko. Aj na konci deväťdesiatych rokov sa pár šikovných snažilo na Poľane ťažiť. Neuspeli. Vtedajšia zmätená snaha lesníkov stroskotala na omnoho menšom odpore, než na aký sa musia pripraviť v súčasnosti. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Desať rokov postkalamitného vývoja
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Desať rokov úžasnej divočiny v Tichej a Kôprovej doline
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Vetrová kalamita - verzia 2014
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Čo pytliak, to poľovník - kríza predátorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            130 vlkov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Wiezik
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Wiezik
            
         
        wiezik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Pracujem na Fakulte ekológie a environmentalistiky, TU vo Zvolene. Presadzujem fungujúcu ochranu prírody na Slovensku. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    52
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5130
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            moja stránka
                                     
                                                                             
                                            Rozhovor v Pravde
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            F.W.M. Vera - Grazing Ecology and Forest History
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio Rock
                                     
                                                                             
                                            Miles Davis - Bitches Brew
                                     
                                                                             
                                            Jaga Jazzist
                                     
                                                                             
                                            Tom Waits
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Michal Wiezik
                                     
                                                                             
                                            wildlife.sk
                                     
                                                                             
                                            alternatívne lesníctvo z Čiech
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       10 rokov po ...
                     
                                                         
                       Nie len blondínky, ale aj brunetky
                     
                                                         
                       Vlčie hory
                     
                                                         
                       Čo pytliak, to poľovník - kríza predátorov
                     
                                                         
                       Názorná ukážka vplyvu človeka na vysušovanie krajiny
                     
                                                         
                       Zimné OH a vzorový alibizmus Ministerstva životného prostr. S.R.O.
                     
                                                         
                       Nehaňte vlka
                     
                                                         
                       Bude Slovensko ešte niekedy tým Švajčiarskom?
                     
                                                         
                       Popíjač koly sa nám všetkým smeje do tváre
                     
                                                         
                       Kto stojí za Cynickou obludou ?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




