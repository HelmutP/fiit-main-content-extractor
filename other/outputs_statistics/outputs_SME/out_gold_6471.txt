

 Chladný morský vietor veje Mužovi do tváre. Sedí, s nohami skrčenými k telu, na kolenách položené ruky skrížené cez seba a na nich opretá brada. Oranžová obloha s ohnivo bielym slnkom sa mu odrážajú v očiach. Vo vetre občas zakvíli čajka. Muž len ticho sedí, ani sa nepohne. Vietor sa mu hrá s vlasmi. Z prístaviska dole pod ním nepočuť žiadne zvuky. Akoby tam život ani neexistoval. Veľké majáky sú uhasené, kamenné budovy sa zdajú dočista prázdne. Jediný zvuk ktorý počuť je dunenie vetra o žiarivo biele útesy a masívne údery vĺn o pobrežie. Šeptanie mora a vzduchu dolieha priamo k nemu až hore na bralá. Akoby sa tie dva živle ticho zhovárali čo každý z nich plánuje robiť keď sa rozdelia... 
 Je sám. V Šedistých prístavoch... 
 



 
   
   
 
 
 
   
 Srdcervúco kričí. Zúfalstvo ho premáha. Zrazu cíti zvláštny pocit. Tma pomaly ustupuje ako sa prvé slnečné lúče predierajú cez vrcholky zasnežených veľhôr. V prvej chvíli len posmrkáva a necháva stekať slzy po zmučenej tvári. Lúče pomaly stekajú po tráve priamo k nemu. Pozrie sa dole do náručia, kde zviera telo dievčaťa ktoré nadovšetko miluje. To ledva dýcha... Smrť ju ťahá za ruku. Môže to cítiť. Pozrie hore... Ešte je skoro na to aby vyšlo slnko... Je to vôbec slnko? Lúče už má na tvári a v tom zasiahnu jeho oči. Zakryje si ich rýchlym pohybom ruky. V svetle sa objavil Anjel. Dievča začalo dýchať... 
   
   
 



 
   
   
 
 
 
   
 Stáli tam spolu. Pozerali jeden na druhého. Všetci štyria. Ani jeden z nich nevedel čo má povedať. Oblaky nad nimi sa posúvali celkom rýchlo. Vietor začal fúkať silnejšie a obidvom dievčatám začali povievať vlasy, no ani jedna nepohla pohľadom. Iba mierne privreli oči ako im do nich vietor fúkal. Slnko pomaly klesalo ponad horizont. Chlapci, stojac taktiež nepohnute, boli ticho. Jeden z nich sa pohľadom naklonil k zapadajúcemu Slnku. Dievčatá upreli pohľad na profil jeho tváre. Chvíľu tak stáli. Druhý chlapec zahmkal. Ten čo pozeral na oblohu sa nakoniec ozval prvý. Dievčatá prikývli. Druhý chlapec len nehnute stál a pozeral, tentoraz do zeme. Nakoniec sa všetci otočili a bez slova sa každý vybrali iným smerom. Bez rečí, bez sĺz. Už nikdy sa nestretli... 
   
   
 



 
   
   
   

