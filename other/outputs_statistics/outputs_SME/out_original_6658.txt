
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Šimčík
                                        &gt;
                Británia
                     
                 Tribute bands 

        
            
                                    17.5.2010
            o
            9:01
                        (upravené
                17.5.2010
                o
                9:46)
                        |
            Karma článku:
                4.11
            |
            Prečítané 
            681-krát
                    
         
     
         
             

                 
                    Britské reinkarnované verzie klasikov rocku alebo hudobní zombies
                 

                 Pubrocková scéna ma v Británii hlbokú tradíciu a aj keď elektronická hudba ovláda diská, živá hudba má stále svoje publikum. V praxi to vyzerá asi tak, že keď sa v sobotu vyberiete do nejakého pubu, nie väčšieho ako Vaša lokálne pohostinstvo alebo bar,  v rohu hrá lokálna kapela známe rádiové hity od The Rolling Stones, Beatles, Neilla Younga, Oasis alebo aj Madonny, Anastacie alebo Lady Gaga. Takmer vždy výborne ozvučení, fenderami a gibsonami vyzbrojení hudobníci dokážu niekedy spracovať tieto skladby do úrovne porovnateľnej s originálom. Tí menej úspešní šak tiež neostávajú bez potlesku, veď pri úrovni pitia v Británii po desiatej večer je pre zákazníkov každá možnosť si zakričať svoj rádiový hit vítaná.   Niektoré kapely však zachádzajú oveľa ďalej a na počesť svojím veľkým vzorom zakladajú revivalové kapely. Hrajúce ich repertoár a napodobnujúce ich oblečenie, pohyby a charakter sa niekedy stávajú originálnejśími ako sú originály sami. Ponúkajúc obecenstvu iba to čo chcú - veľké hity a svojích obľúbencov zmrazených v tom najlepšom období sú obľúbení ako medzi divákmi, tak aj medzi kritikmi, označujúc ich zábavnejší ako originál.   Nájdite štyri rozdiely                                   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Ako Maťo Ďurinda reggae hral
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O retre
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O hmle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            O nechcených vianočných darčekoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Šimčík 
                                        
                                            Keď jeden chýba
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Šimčík
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Šimčík
            
         
        simcik.blog.sme.sk (rss)
         
                        VIP
                             
     
        Agropunk, vraj aj Mod, stratený ako working class hero na východoslovenskom vidieku.  Rád chodím na futbal, koncerty, do pubu, kostola a z času na čas aj na disco :)

"Myslím, teda slon"


  
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    186
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Názory
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Subkultúry
                        
                     
                                     
                        
                            Británia
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Čo sa domov nedostalo
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Roger Krowiak
                                     
                                                                             
                                            Rudolf Sloboda - Do tohto domu sa vchádzalo širokou bránou
                                     
                                                                             
                                            Subculture - The Meaning Of Style
                                     
                                                                             
                                            F.M. Dostojevskij - Idiot
                                     
                                                                             
                                            NME
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Arctic Monkeys
                                     
                                                                             
                                            The Gaslight Anthem
                                     
                                                                             
                                            Slovensko 1
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            BBC Radio 2
                                     
                                                                             
                                            BBC Mike Davies Punk Show
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tatran Prešov
                                     
                                                                             
                                            Bezkonkurenčný Dilbert
                                     
                                                                             
                                            flickr
                                     
                                                                             
                                            discogs
                                     
                                                                             
                                            ebay.co.uk
                                     
                                                                             
                                            Banksy
                                     
                                                                             
                                            Kids And Heroes
                                     
                                                                             
                                            Roots Archive
                                     
                                                                             
                                            West Ham United
                                     
                                                                             
                                            NME Radio
                                     
                                                                             
                                            denník Guardian
                                     
                                                                             
                                            denník Pravda
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




