
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Rusina
                                        &gt;
                Camino Santiago de Compostela
                     
                 3. etapa do Compostely alebo Palmová nedeľa a moja prvá kríza 

        
            
                                    8.4.2010
            o
            23:14
                        (upravené
                9.4.2010
                o
                2:35)
                        |
            Karma článku:
                9.85
            |
            Prečítané 
            2075-krát
                    
         
     
         
             

                 
                    Noc bola suchá, ráno tiež veľmi pekné a pohodlie v stane úžasné. Vôbec sa mi nechce vstávať, veď je nedeľa. Síce neskôr, ale nakoniec sa premáham. Raňajkujem včera kúpené žemličky s nutelou. Ešte že som ju kúpil dvojfarebnú, lebo teraz by mi už šla hore krkom. Všetko zbaliť mi zaberá pomerne dosť času. Prvé kroky vedú len tak lesom. Potom nachádzam dáku cestu a vydávam sa po nej. Priorita dnes je nedeľná Palmová omša.
                 

                    Po čase si uvedomujem, že slnko je na nesprávnej strane. Ráno by malo byť po mojej pravici a je presne naopak. V lese som to musel domotať a nabral som úplne opačný smer na juh. Ani prvé domy mi nepomáhajú, lebo ako to tu býva zvykom, osady nie sú poriadne značené. Hlavne, že kostolná veža oznamuje zvonkohrou celú hodinu.  Horko –ťažko nachádzam živú dušu a pýtam sa, kde vlastne som. Je to jednoduché, stačí len nastrčiť mapu a oni vám ju vrátia s prstom na jednom mieste so slovom „aqui“, teda v preklade „tu“.  Dozvedám sa, že som v Cervães. Fakt sa vraciam. Otáčam to. Druhý krát prechádzam poniže kostola, ktorý sa týči na pahorku nad dedinou. Tentokrát je melódia zvonkohry mimoriadne košatá. Napadá ma, že možno je to zvolávanie na omšu. Otravujem ďalšieho obyvateľa Cervães. „Faz favor“ (tak upútate pozornosť, že dačo potrebujete)“direcçao?“ (smer) a jednoducho v mape ukážem na susednú dedinu, kam sa chcem dostať. Začne mi to vysvetľovať, našťastie pri tom aj gestikuluje. Ešte sa pýtam: „Eucaristia agora?“ „Si, agora.“ „Obrigado,“ zaďakujem. Takže môj predpoklad bol správny. Stúpam teda na pahorok ku kostolu. Už tam postávajú farníci, ale nehrnú sa dovnútra. Znova sa pýtam „Eucaristia agora?“ „Si, si,“ dostávam odpoveď. Nehrnú sa, tak si ani ja nehrniem a sadám si na múrik námestia pred kostolom v stromoradí olivovníkov. Pozorujem, že čakajúci farníci odtrhajú z olivových stromov ratolesti. Jasné, treba dačím privítať Ježiša pri vhcode do Jeruzalema. Podľa ich vzoru si odtrhám aj ja. Nejako dlho sa čaká. Vchádzam do chládku kostola. Stále čakám. Zo soboty na nedeľu sa menil čas a ja som si ho pre zachovanie môjho biorytmu neprenastoval, čiže slnko mi naďalej bude zapadať o 19:00 (aj časy budem uvádzať v svojom čase), ale všetci tu čakajú. Možno sú len taký dochvíľny (alebo predchvíľny), čo mi teda na portugalskú náturu nesedí ako som ju spoznával vyše mesiaca v Porte. Zrejme je toto jeden z rozdielov medzi mestom a vidiekom.   Konečne vítame s olivovými ratolesťami príchod Ježiša do Jeruzalema. Z pašií v portugalčine vyrozumievam, že sa jedná o udalosti: Ježiš pred Pilátom.   Interiér kostola ma zaujal už pri čakaní a záujem ma neprešiel až do konca omše. Svätostánok sa nachádza v ihlanovitom priestore pod dvoma fakt obrovskými balvanmi. Skvelý nápad. Samozrejme, všetko ostatné je pokryté košatými ornamentami, ako to majú Portugalci radi. Napadá ma, že to bude určite originálne riešenie interiéru kostola v Portugalsku a ja som to objavil v podstate omylom.   Po omši obedujem zvyšky želiem s nutelou a vyrážam s olivovou ratolesťou na batohu. Nedarí sa mi ale vymotať z Cervães. Táto dedina sa pre mňa stáva zakliata. Pýtam sa ľudí, ale každý akoby mi ukazoval úplne odlišný smer. Nakoniec, a to je už na mne asi vidieť zúfalstvo, si to nechávam veľmi podrobne vysvetliť od jedného pána, a darí sa mi vymaniť z Cervaes. To už mám nachodených 17 000 krokov (okolo 13 kilometrov) a stále som južnejšie od môjho nocľahu. Toľko platím za navigačné chyby.  Na mojej google mape z Igreja Nova nevedie žiadna cesta na sever, ale objavujem čosi lesné. Je tam síce nápis, že zákaz vstupu, riziko vzniku požiaru, najmä júni až septembri. Hovorím si, že rizikový mesiac nieje a je ešte stále dosť vlhko po tom predvčerajšom daždi. A hlavne chcem ísť na sever. Ak to aj nieje správna cesta (po ránu už nie som tak sebavedomý v navigácii) tak aspoň stojí za to. Pekná príroda, slnečno, nedeľný výlet. Niektoré stromy sú skutočne odspodu trocha sčernané od ohňa.  Týmto opustenými lesmi kráčam od poludnia, až do štvrtej kde vstupujem do dediny Arcozelo. Už som celkom vyhladnutý a vysmädnutý, nemám totiž už ani kvapku a ani nič pod zub. Chcelo by to teraz poriadny nedeľný obed. Pri odbočke na N201 nachádzam „Churrascaria“, čo je typ potravinového obchodu, kde sa dá pomerne lacno kúpiť na prírodno opečené mäso. Vstupujem dnu, ale ďalej ako po prah sa mi nedarí dostať. Táto situácia obsluhujúcu predavačku mimoriadne živo rozosmiala. Ja nechápem a snažím sa znova prekonať prah s rovnakým neúspechom. „Guarda-chuva, guarda-chuva!“ hovorí pomedzi smiech. Začínam chápať. Môj dáždnik, ktorý v túto slnečnú nedeľu odpočíva priviazaný k boku batohu a výrazne nad ním vyčnieva, mi v tom bránil. Vtom mi príde vtipné to slovo „guarda-chuva“, ktorého význam som akurát pochopil. Už som vedel, že „chuva“ je dážď, veď zatiaľ mi ho za pobyt v Portugalsku bolo nadelené štedro, a „guarda“ je ľahko pochopiteľné, chrániť. Dažďochránič. Nechávam si zabaliť pol kuraťa naprírodno opečené. Pýtam si ešte vodu do mojich dvoch prázdnych 1,5 litroviek. Vysvetľuje mi, že cez cestu (teda N201) je „bom agua“. Idem sa teda tam a pri východe zopakujem rovnakú chybu s dáždnikom. Predavačka sa rovnako živo rozosmeje. Usádzam sa na prameni na druhej strane cesty a zjem takmer celé kura len tak bez ničoho. Dva kúsky si odkladám na neskôr.   Pozerám do mapy. Nezostáva mi nič iné, ako kráčať po N201. Poriadne sa napijem, ale fľaše si naplním len do slabej polovice (teda poloprázdna). Budem kráčať cez civilizáciu, teda s vodou nebude problém.  Vyrážam. Slnko ma už opaľuje z ľavej strany. Píšťaly ma začínajú pobolievať. Pôvodne som mal v úmysle tento deň poňať pohodovejšie, jednak je nedeľa a jednak tretí deň býva kritický. Vedomie rannej straty ma však ženie ďalej. Po niekoľkých kilometrov je to doslova nezlúčiteľné s chôdzou. Vyhŕňam si gate a zisťujem, že mám na nich poriadny opuch a kde sa končí ponožka, tam je pekný zárez. Sťahujem si ponožku nižšie a pociťujem náhlu úľavu. Výborne, neohrozene kráčam ďalej. Paličky zo stanu, ktoré som premenoval na trekingové paličky mi teraz výrazne pomáhajú. Chcem sa dostať aspoň pred mesto Ponte de Lima. Pôvodne som chcel byť dnes za ním. Chýba mi ranných 17 000 krokov. Ešte túto zákrutu. Ešte túto. A tamtú. Zastavujem pred jedným Snack Barom s túžobným pohľadom naň a moje odhodlanejšie Ja mi zatiaľ dohovára, nech na to ani nemyslím. Vtom zvnútra víde pani a volá na mňa: „Entrada“. Odhodlanejšie Ja je v oslabení. Vstupujem a pýtam si úplne nelogicky „pão“, teda chleba, ktorý by som za normálnych okolnosti v Snack Bare nepýtal.  Pani odchádza do vedľajšej miestnosti a po chvíli mi skutočne prináša pão a v ňom niekoľko plátkov šunky a syra. K tomu mi ešte pridáva prepasírovanú marhuľu na zapitie. Pochopil som, že som sa znova dostal na miesto, kde pestujú sympatie k pútnikom. Ešte si u nej natankujem vodu do plna a na cestu mi pridáva veľký kus karamelu na cmúľanie.  Po tejto večeri už ďaleko necestujem. Tesne za Snack Barom je fliačik lesíka, kde si nachádzam čistinku. Je to pomerne skoro, pol siedmej, ešte pred západom slnka. Kráčať sa mi už ale naozaj nedá. Dnes sa slávnostný vstup do Ponte de Lima nekoná. Olivovou vetvičkou si aspoň vyzdobujem strop stanu. Zaspávam najedený po slabých 32 000 krokoch, počuť N201 a v ďialke zvonkohru.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            10. etapa do Compostely alebo kto nemá v hlave, ten má v nohách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            9. etapa do Compostely alebo ako som sa neskoro v noci doplazil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            8. etapa do Compostely alebo pôstne putovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            7. etapa do Compostely alebo posledná večera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Rusina 
                                        
                                            6. etapa do Compostely alebo osud premočených smradlavých ponožiek
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Rusina
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Rusina
            
         
        martinrusina.blog.sme.sk (rss)
         
                                     
     
        Momentálne erazmus študent v Porte (Portugalsko)na fakulte Belas Artes
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2020
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Camino Santiago de Compostela
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




