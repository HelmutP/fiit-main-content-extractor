

 
„Spí", odpovedala prekvapujúco pokojne. „Vrátil sa
z ďalekého Vietnamu. Bol tam chudáčik takmer týždeň, musel tam jesť len
ryžu a pomaly vlastnoručne budovať už zrušenú (rozumej zbytočnú,
pozn. ivans) ambasádu. A navyše, vieš si, Ivan predstaviť",
prešla do osobného tónu, čo ma prekvapilo. Že by si Robó uložil moje meno do
mobilu? Fíha, veď ja sa pomaly stávam rodinným priateľom Ficoóvcov. „Tak vieš
si predstaviť", pokračovala, „že on tam letel pravidelnou linkou, ani si
nemohol na tej dlhej ceste topánky vyzuť a nôžky povystierať, aby náhodou
niekomu nezasmrdelo, ó!"
 
 
Chcel som oponovať, že celé jeho komančovanie už smrdí veľmi.
A nechutne. A že jeho „Ficoó Reisen" ma už nebaví, nech si cestuje po
Vietname, Bielorusku, Kube, Líbyi a Venezuele za svoje, že ja to zo
svojich daní odmietam platiť a že....Ale pani F. prerušila moje myšlienky
náhlou zmenou tónu hlasu, kedy dosť rozhorčene zvolala: „A to som musela ešte
so Silviou (rozumej Glendová, hovorkyňa, pozn. ivans) napísať
text platenej správy, aby sa národ dozvedel, čo si môj Robkoó vytrpel na tej
ceste komerčnou linkou. Div, že som tú správu nemusela platiť zo svojho. Ale
Silvi mi povedala: „nechaj to tak, ja to celé zacvakám z rezervy šéfa, tá
je bohatá a ľahučko sa prekračuje a míňa a míňa...To si národ
nevšimne!" „Chichi, nie je tá Silvinka rozkošná?", položila rečnícku otázku
pani Ficoóva a nečakala ani na odpoveď a mlela, mlela a mlela
ďalej. Ach to bude ale účet, blyslo mi hlavou o 10 minút, kedy som si
stihol s prepáčením aj odskočiť. Mal by som ju nejako prerušiť, asi
zabudla, že volám ja a tiež asi aj s kým volá, nakoľko som
v slúchadle začul otázku: „A čo myslíš, mám si k tomu novému
červenému kostýmu kúpiť klobúčik, ako mala tá anglická kráľovná na bankete? Veď
vieš ktorý, videla som jej ho v televízii." Skoro zo mňa vyhŕklo: Ty ťapa,
veď ste tam mali ísť obaja osobne! Čo ste robili, keď ste tam nešli?!, ale len
som niečo zašomral. Ale pani Ficoóva si odpovedala sama: „Vieš, mali sme
tam ísť aj my, ale nemohla som sa odtrhnúť od upratovania a prala som. Aj
žehlenia bolo veľa. Ani Róbertoóvi sa nechcelo, bolela ho hlava z II.
piliera. A začala ho aj z prvého. Stále premýšľa akoby to celé
vyriešil?! Hlavu mu ide chudáčikovi rozdrapiť, aj som mu musela dať Ibalgin
400ku. A aj ten Shooty, Schutz so Štulajterom  (kde je Sýkora, pozn. ivans)
stále do neho rýpu! Že je majster demagógie, že chce znárodňovať, no uznaj
Gitka(?!), je toto možné?! Veď on chce ľuďom len dobre!!"
 
 
Gitka?! Nechápal som! Veď ona asi zabudla s kým volá
a myslí si, že trkoce so svojou priateľkou. Juj, zľakol som sa. Čo keď mi
začne rozprávať nejaké rodinné pikošky?! Asi by som mal položiť. Ale keď som
začul v diaľke Roboóv rozospatý hlas, ako sa pýta: „Kto volá? Luky? (rozumej Lukašenko, prezident Bieloruska,, pozn. ivans).
S kým voláš, drahá?", tak som zbystril. Konečne je hore. Konečne mu to
vytmavím. Poviem mu rovno do očí, že ako môže takto verejne klamať
a zavádzať občanov. Že už sa príliš nafukuje bublina jeho preferencií a keď
bude priveľká, môže ľahko prasknúť. Nech si uvedomí, že je predsedom vlády SR
v roku 2008, že sme v NATO a EÚ, že nie je generálnym tajomníkom
ÚV KSS a nie sú 80te roky. A že som ho nevolil a nikdy nebudem!
A spolu so mnou ho nevolilo ďalších 74% oprávnených voličov! A že
nesie zodpovednosť za celú republiku a že.....
 
 
 
 
 
Ale prúd mojich nevyslovených výčitiek prerušila pani
Ficoóva, keď zašvitorila: „Drahá, už musím končiť. Róbertó sa už vyspinkal.
Ideme leštiť valašku s hviezdou na čepeli. Veď vieš ktorú, už som ti
minule u kaderníčky o nej hovorila. Daroval mu ju osobne Luky so slovami:
„MOLODEC, ty charašo zbijaješ!" A dal mu BOZK na čelo. Fúj, to bolo
nechutné. Už som sa zľakla, že mu dá „brežnevovku" na ústa?! No uznaj, nebolo
by to hrozné?! Ale už musím naozaj končiť, zavolaj aj na budúce! Papá!"
A kým stihla položiť, započul som ju ako volá do spálne: „Ty môj zbojníček,
ty môj hrdina, čo bohatým berie a chudobným(rozumej svojím, pozn.
ivans) dáva, už som hneď pri Tebe aj s tvojou obľúbenou
valaškou....." 
 
 
KLAP. Položila. Konečne ticho. Úff, to bolo! Vystrel som
zmeravenú ruku a pošúchal stŕpnuté ucho. Hotovo! Nuž takto som ja
v jednu nedeľu volal Robovi Ficoóvi!
 
 
 
 
 
Možno ste še pobavili! To by bolo fajn. Ale až taká sranda
to nie je. Stačí si ozaj prečítať 20tu stranu SME z pondelka 27.10.2008
a ono nás ten humor prejde. V takom Bielorusku by som za takýto
srandablog išiel do väzenia. V takom Rusku by som si ho ani nedovolil
napísať. Ak sa milý bloger (rozumej, volič, občan)
nespamätáme, tak v našom širokorozchodnom 
Slovenrusku v roku 2011 nám dá FICO K.O. Neveríš?! Minsk aj Moskva
sú „zatracene" blízko! Jedinou nádejou je, že snáď sa mlčiaca väčšina
spoluobčanov preberie a knokaut
mu zasadíme my! Je to v našich rukách a najmä hlavách! Ako hovorí na
citovanej strane novín Lajos Grendel: 4444 0568789 26483 42457 122 61 8965  431 4
 
 
 
 
 
p.s. a keď už som z tej strany SME využil všetky
podnety, tak aj článok V tieni finančnej krízy sa nás môže dotknúť, keď by
došlo k najhoršiemu a vládna trojka by nás, svojich odporcov, nedajbože,
pozatvárala  :-). Stratili by sme volebné
právo do europarlamentu. Neviete ako dlho chodí pošta z Ilavy do
Štrasburgu?
 
 
 
 
 
p.s.2  dúfam, že Petit
Press a Sme nebudú robiť problémy s autorskými právami, cé
v krúžku patrí Sme, ja som len jednu noc nemohol poriadne spať a tak
som čítal a potom písal....
 
 

 

