
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Roy
                                        &gt;
                Všetkým
                     
                 O prezidentovi 

        
            
                                    7.6.2010
            o
            20:57
                        (upravené
                7.6.2010
                o
                22:01)
                        |
            Karma článku:
                5.03
            |
            Prečítané 
            524-krát
                    
         
     
         
             

                 
                    Pozerám, čítam - populárny nemecký prezident Horst Kohler odstúpil. V podstate pre "banalitu". Pre slová. Zle zvolené slová. Pre urýchlenú, dostatočne neuváženú reakciu. Je zrejmé, že nie v každej krajine má slovo tak nízku hodnotu ako je tomu u nás. Svoje pôvodné vyhlásenie sa snažil najskôr korigovať, opraviť, dovysvetliť. Neuspel. Napokon vyjadril ľútosť nad tým, že jeho formulácia mohla vyvolať nedorozumenie a odstúpil. Odstúpil populárny, obľúbený prezident, proreformný politik, uznávaný odborník (bývalý šéf Medzinárodného menového fondu), človek, ktorý dokázal Nemcom vrátiť dôveru v post nadstraníckeho prezidenta, ktorý je schopný v súlade s jeho (existujúcim) vlastným názorom skritizovať opozíciu i koalíciu, keď si myslí, že treba. Odstúpil prezident - osobnosť. Ako je na tom náš - veľkou časťou spoločnosti - neobľúbený, nepopulárny, ako odborník a človek-osobnosť, neuznávaný prezident? A ako sme na tom my s ním?
                 

                 Nemeckú verejnosť a nemeckú opozíciu pobúrilo vyjadrenie Horsta Kohlera k misii nemeckých vojakov v Afganistane, s ktorou nesúhlasí viac ako 60% obyvateľov Nemecka. Nesúhlas vyvolali prezidentove slová o tom, že v krajnom prípade je vojenské nasadenie oprávnené aj v prípade obrany ekonomických záujmov.   Kultivované a (nielen ekonomicky) vyspelé Nemecko je dnes voči násiliu už dostatočne scitlivenou spoločnosťou. Aj voči násiliu legitimizovanému ekonomickými záujmami. (Nuž, mali sa z čoho poučiť.)   Odstúpil uznávaný, rešpektovaný a populárny politik - prezident - človek. A kultivovanému Nemecku privodil šok.   Vyspelý a scitilivený svet nám ukazuje, že slová predsa len sú dôležité a neradno sa s nimi zahrávať. Nielen kôli ohrozeniu vlastnej kariéry. Ide predsa o viac.   ____________________________________   Náš neobľúbený, nepopulárny, ako osobnosť-človek a politik nikým relevantným (a aj veľkou časťou spoločnosti) neuznávaný prezident - si pokojne hovie v predzidentskom paláci bez jediného chmúrneho mráčika na prezidentskom čele, bez jedinej vtieravo-nepríjemnej myšlienky v predzidentskej hlave. Na odstúpenie či demisiu, samozurejme, nemyslí. Netrápi sa. Nemá prečo!   Veď on predsa Nič nepovedal!   ____________________________________   Nuž, neostáva mi iné, ako dať mu tentoraz za pravdu. Je to tak, má pravdu - za jeho posledných šesť (či koľko) prezidenských rokov naozaj nepovedal Nič!   NIČ poriadne!       A nekultivované (rozumej - dostatočne nescitlivené) Slovensko nie je v šoku ani náhodou. A čo je horšie - nie je dokonca ani nemilo prekvapené!       Do zaplakania, priatelia! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            O Zaujatí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Situácia je fakt lepšia: Už sa len Klame, Kradne a Kupujú hlasy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Ako sme volili v porodnici alebo O "prave nevolit"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Prochazka &amp; Knazko, alebo Nevolim zbabelcov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Nemozem uz citat spravy zo Slovenska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Roy
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Roy
            
         
        zuzanaroy.blog.sme.sk (rss)
         
                                     
     
         V posledných rokoch ma najviac oslovili myšlienky a knihy Anselma Grúna - nemeckého benediktína, a slovenských feministiek z ASPEKTu. Píšu o tom, kde nám to drhne (v živote) a ako z toho von. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    152
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1606
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Foto: Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Verše bez veršov
                        
                     
                                     
                        
                            Letters to Animus
                        
                     
                                     
                        
                            Môj pes Dasty
                        
                     
                                     
                        
                            Budúci prezident - aký si?
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Alternatívne školy - Bratislavský kuriér
                                     
                                                                             
                                            Môj Prvý pokus - Démonizácia Waldorf. pedagogiky v slov.médiách
                                     
                                                                             
                                            Odkaz Sorena Kierkegaarda
                                     
                                                                             
                                            Tomáš Halík: O výchove a vzdelávaní
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Joni Mitchell - absolútna nádhera: Both Sides Now
                                     
                                                                             
                                            Anselm Grün: Zauber des Alltäglichen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dr.Christiane Northrup - Womens Wisdom
                                     
                                                                             
                                            Sloboda Zvierat
                                     
                                                                             
                                            Anselm Grün
                                     
                                                                             
                                            Tomáš Halík
                                     
                                                                             
                                            Dominik Bugár - Fotograf, kolega - rodič
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       "Suma bola zaplatená pánovi dekanovi, ktorú pán dekan schoval do zásuvky"
                     
                                                         
                       David Deida - "Intimní splynutí - Cesta za hranice rovnováhy"..
                     
                                                         
                       All inclusive a piesok na zadku. Je toto dovolenka?
                     
                                                         
                       Moja Pohoda
                     
                                                         
                       Lož na kolesách: Pád Lancea Armstronga
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Gentlemani na Wimbledone
                     
                                                         
                       Druhá šanca pre človeka
                     
                                                         
                       Facka Ficovi
                     
                                                         
                       Mantra národa.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




