

   
 Prázdno, tmavo, ticho 
 nič, 
 to všetko, ako 
 bič. 
   
 Len jedny stopy. 
 Len jedny ruky. 
 Len jedno srdce. 
 A výkrik - Dosť! 
   
 Veď to všetko má sa 
 znásobiť, 
 len jedno, neznamená nič. 
   
 Už nechcieť vidieť len jej obrázky, 
 čo sú ako film. 
 Veď ten predsa skončí. 
   
 Už nechcieť počuť len jej zvuky, 
 čo sú ako pieseň. 
 Veď tá predsa skončí. 
   
 Už nechcieť len o nej snívať, 
 sen je prízrak, 
 len závan, čo z viečok nadránom odletí. 
 Veď ten predsa skončí. 
   
 Naplniť. 
 Naplniť prostú túžbu ľudskú. 
 Chcieť snáď tak veľa? 
   
 Alebo čakať? 
 Čakať, že raz sa naplní, 
 že nie je ako Godot, 
 ktorý už zajtra...určite zajtra 
 ... 
 A NEPRÍDE. 
   

