
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Kravčík
                                        &gt;
                www.theglobalcoolingproject.co
                     
                 Program radikálneho zníženia povodňových rizík 

        
            
                                    17.5.2010
            o
            8:35
                        (upravené
                17.5.2010
                o
                8:51)
                        |
            Karma článku:
                9.02
            |
            Prečítané 
            2819-krát
                    
         
     
         
             

                 
                    Vytvorenie 50.000 pracovných príležitostí pre roky 2010 - 2013 s cieľom dosiahnuť radikálne zníženie povodňových rizík a strát, zvýšenie ochrany pôdneho fondu a hospodársky rozvoj, postavený na zvyšovaní zásob vodných zdrojov v Slovenskej krajine o 250 mil. m3 ročne.
                 

                     Úvod   Slovensko trápia povodne i suchá, ktoré v posledných rokoch spôsobujú  škody vyjadrené v stámiliónoch Euro a ktoré majú mať podľa predpovedí  stúpajúcu tendenciu. K tomu sa pridáva nastupujúca hospodárska kríza,  ktorá popri inom prináša nezamestnanosť. Prináša však i možnosť presunúť  uvoľnené pracovné sily na riešenie skôr uvedených problémov, ako sa  o to úspešne pokúsili v čase Veľkej hospodárskej krízy v tridsiatych  rokoch 20.storočia v USA v rámci programu New Deal. Z úst svetových  lídrov môžeme stále častejšie počuť snahu o zopakovanie princípov New  Deal-u a využitie skúseností z neho.   Myšlienky tohto druhu sa objavujú aj u nás. Rada združenie miest  a obcí Slovenska na svojom zasadnutí dňa 27. a 28.1.2009 schválila zámer  zostaviť a predložiť na najbližšie rokovanie Rady ZMOS komplexný  materiál o možnostiach a príspevku miestnej územnej samosprávy  k hospodárskemu rastu a ekonomickej stabilite Slovenska, tzv. komunálny  balíček. Vláda SR a ZMOS dňa 13. februára 2009 prijali Memorandum o  spolupráci pri riešení dopadov finančnej a hospodárskej krízy na  slovenskú spoločnosť. Memorandum sa zameriava na definovanie oblastí  spolupráce, prijímanie a plnenie opatrení ekonomického a spoločenského  charakteru smerujúce k eliminovaniu dopadov finančnej a hospodárskej  krízy. Vláda SR sa zaviazala v čo najširšej miere podporovať miestny a  regionálny rozvoj a ZMOS sa zaviazalo podporovať zamestnávanie občanov  aj cestou vytvárania sociálnych podnikov a pripraviť ekonomický program  miest a obcí pre rozvoj zamestnanosti. Z pohľadu ZMOS sa javí užitočné  vytvárať v období krízy cielene nové pracovné miesta na komunálnej  úrovni v oblastiach, ktoré znižujú regionálne disparity, zvyšujú  konkurencieschopnosť obcí a miest a neohrozujú trhové prostredie.  K týmto oblastiam patrí  aj obnova krajiny, ochrana životného prostredia  a oblasť protipovodňovej prevencie.   Americký New Deal v 30-ych rokoch 20.storočia pozostával z množstva  programov a iniciatív na federálnej, národnej i miestnej úrovni,  s rozličným trvaním, nákladmi a počtom zapojených ľudí. Viaceré z nich  možno podrobiť kritike z hľadiska okamžitej efektívnosti. Prezident  Roosevelt si toho bol vedomý, avšak napriek tomu dával prednosť ponuke  pracovných príležitostí, pretože priamu peňažnú pomoc v nezamestnanosti  jednotlivcom považoval za „drogu nenápadne ničiacu ľudského ducha."  S odstupom času možno povedať, že programy nielen dali zamestnanie  miliónom ľudí, ale zanechali po sebe zdravšiu krajinu s vysadenými  lesmi, rybníkmi, hrádzkami či terasami, ktorej benefity cítiť podnes.       Ochrana vodných zdrojov a protipovodňová prevencia   Tento projekt  sa  inšpiruje tou časťou programu New Deal, ktorý bol  zameraný na ochranu prírodných  zdrojov a na protipovodňovú prevenciu.  Dodáva však i súvislosti, ktoré sú aktuálne pre Slovensko. Tie netreba  pracne definovať. Existujú dokumenty na vládnej i mimovládnej úrovni,  ktoré sa zhodujú v analýze súčasného stavu i v tom, čo treba na ochranu  prírodných zdrojov a v oblasti protipovodňovej prevencie vykonať.   V materiáloch vlády SR[1] sa  súčasný stav väčšiny vodných tokov a ich povodí charakterizuje  nasledovne:      významne sa znížila prirodzená retenčná schopnosť povodí azrýchlil  sa povrchových odtok vkrajine, vdôsledku čoho sa zvýšilo riziko  afrekvencia výskytu povodní,    znížila sa prirodzená samočistiaca schopnosť vodných tokov, čo sa  následne prejavilo na kvalite povrchových apodzemných vôd,    dochádza kznižovaniu výdatnosti akznehodnocovaniu zdrojov pitnej  vody,    urýchľuje sa erózia a tým nastáva znižovanie úrodnosti pôdy (najmä  vhorských apodhorských oblastiach),    nastalopodstatné obmedzenie ekologických funkcií vodných tokov atým  dochádza k degradácii prirodzených vodných amokraďových ekosystémov.      Veľmi podobné hodnotenie možno nájsť v strategických materiáloch ZMOS  z posledného obdobia.[2] Dokumenty ZMOS-u v neposlednom rade konštatujú, že medzi príčinami  vzniku povodní je znížená schopnosť zadržať dažďovú vodu z dôvodu  poškodenia krajiny odstránením vodozádržných a protieróznych prvkov  (odlesnenie, vysušenie močiarov, rozoranie medzí, zlikvidovanie remízok,  atď.). Tieto faktory spôsobujú zmeny odtokových pomerov a zvyšujú  riziko lokálnych povodní. Rýchly odtok dažďových vôd zároveň prispieva  k odvodňovaniu krajiny so širokým spektrom negatívnych dopadov, od  znižovania zásob podzemných vôd až  po zmeny mikroklímy na odvodnených  územiach.   V otázke riešenia súčasnej neradostnej situácie Koncepcia  vodohospodárskej politiky SR do roku 2015 definuje stratégiu  revitalizácie povodí: „presadzovať princíp hospodárenia  s vodou na  celej ploche povodia, teda aj mimo korýt a vodných nádrží a vytvárať tak  priestor na širšie uplatnenie biotechnických, agrotechnických a  lesníckych opatrení... za účelmi zlepšenia retenčnej schopnosti."  Zvýšenie retenčnej schopnosti povodí sa viackrát nachádza aj v tej časti  toho istého dokumentu, ktorá definuje úlohy v oblasti ochrany pred  povodňami. Zvýšenie retenčnej schopnosti územia sa má vykonať s cieľom  „zvýšiť potenciál podzemných vôd a obmedziť  vznik povodňových situácií  technickými opatreniami (budovanie nádrží, rybníkov, poldrov,  prehrádzok)."   Vzácny konsenzus medzi vládnymi materiálmi a materiálmi ZMOS-u  v hodnotení situácie a v otázkach potrebných opatrení sa však razom ruší  pri hodnotení ich realizácie. ZMOS kritizuje investičnú neaktuálnosť  a inovatívnu zaostalosť zodpovedných inštitúcií, ktorá pri  nerešpektovaní Koncepcie vyúsťuje do kontraproduktívnych opatrení: „Na  jednej strane sa v koncepcii deklaruje význam plošnej ochrany územia  a potreba integrovaného manažmentu povodia, na strane druhej sa v nej  navrhujú investičné opatrenia, ktoré ako parciálne riešenia síce dočasne  môžu riešiť lokálnu potrebu, ale zároveň zvyšujú výskyt povodní  v povodí ako celku..."[3]   Inými slovami, zodpovedné inštitúcie ignorujú plošné opatrenia na  zvýšenie retencie povodia. V súčasnosti preferovaný spôsob  protipovodňovej prevencie spočíva nie v plošných, ale v líniových  opatreniach. Vykonávajú sa najmä v úpravy tokov a ochranných hrádzí na  dosiahnutie čo najrýchlejšieho odvedenia vôd zvyšovaním prietočnosti  korýt našich riek. Koncentrácia na tento spôsob pri zanedbaní iných má  vážne nedostatky. Čo najrýchlejšie odvádzanie vôd zvyšovaním  prietočnosti korýt riek nerieši problém s povodňovou vlnou, ale ho  presúva na obce nižšie po toku a vystavuje ich ťažko riešiteľným  situáciám. V neposlednom rade je v rozpore s dobrou vodohospodárskou  praxou, pretože spôsobuje tiež nežiaduce zvýšené odvodňovanie povodia  počas celého roka, čím zhoršuje situáciu v čase sucha. Preto je v  rozpore s princípom integrovaného manažmentu vodných zdrojov.   Súčasná situácia potreby tvorby nových pracovných miest v oblastiach,  ktoré neohrozujú trhové prostredie, ponúka príležitosť na riešenie  toho, čo ZMOS nazýva „zjavné rozpory medzi deklarovanými príčinami  vzniku povodní, kompetenčnými možnosťami správcov povodí a nastavením  investičných aktivít v koncepcii vodohospodárskej politiky v oblasti  protipovodňovej ochrany."[4] Obciam,  ktoré márne čakajú na protipovodňové opatrenia štátu, tento projekt  ponúka jednoduché a relatívne lacné opatrenia na zachytenie povrchového  odtoku skôr, než sa spojí v ničivú riavu, ktorá sa už ťažko krotí.  Najlepšia medzinárodná prax protipovodňovej prevencie spočíva podľa  európskych expertov v trojstupňovom prístupe: v zachytení dažďovej vody  na mieste kde padá, v jej retencii a až nakoniec v jej odvádzaní.[5] Tento  projekt rieši najmä prvé dva kroky protipovodňovej prevencie, ktoré sú  u nás neprávom prehliadané a zanedbávané. Znamená posun ťažiska  opatrení, ktoré sú v súčasnosti izolované od príčin vzniku povodní, na  ich prevenciu. Projekt poskytuje zmysluplnú prácu pre tisíce ľudí  v blízkosti ich bydliska, zveľaďuje vodné zdroje a znižuje riziko  povodní. Elimináciou príčin vzniku veľkej časti povodní zvýšením  vododržnej kapacity územia sa počet, intenzita i dopad povodní výrazne  zníži. Má však i ďalšie pozitívne účinky, ako sú zmiernenie dopadov  zmien klímy, protierózna ochrana, zvýšenie biodiverzity, rozvoj nových  hospodárskych činností v krajine a ďalšie.   Protipovodňové a protierózne opatrenia   Ako bolo naznačené v predchádzajúcej časti, úprava toku v obci a nad  ňou nie vždy vyrieši problém pravidelných zátop ženúcich sa cez obec  v podobe masy kalnej vody, blata a kameňa. Voda tečie gravitačne cestou  najmenšieho odporu. Pri nedostatočnom odpore rýchlo naberá kinetickú  energiu berúc so sebou čiastočky pôdy, kameniva a organickej hmoty.  Splav, erózia a odnos pôdy je prejavom veľmi rýchlym, ale tvorba pôdy je  procesom tak veľmi pomalým, že o pôde možno hovoriť ako  o neobnoviteľnom zdroji. Naši predkovia chránili pôdu pred vodnou  eróziou všetkými možnými spôsobmi. Spomeňme si na rôzne terasy  v krajine, pásy trávy, krovín, či stromov obklopujúce polia. Ich účelom  bolo spomaliť tok vody, prípadne docieliť jej zastavenie a postupné  vsakovanie do pôdy.   „Zníženie plošnej a výmoľovej erózie a nadväzne nežiaduceho  transportu a akumulácie plavenín a splavenín po vodných tokoch" je  ďalšia zo strategických úloh Koncepcie vodohospodárskej politiky SR do  roku 2015, s ktorou plne súhlasí i ZMOS, avšak ktorej plnenie stagnuje.  Protierózne opatrenia a opatrenia na zvýšenie schopnosti povodia  zadržiavať vodu sú často totožné. Môžu mať rozličný charakter. Technické  opatrenia predstavujú napr. vsakovacie priekopy po vrstevniciach,  depresie, vsakovacie jamy, vodoholdingy, limany, drobné prehrádzky,  resp. stupne na vodných tokoch, bystrinách, v roklinách či v stržiach,  poldre, malé prietočné vodné nádrže, protipožiarne nádrže, rybníky, a  pod. Biotechnické opatrenia sú podobné, ale prekážku povrchovému odtoku  spájajú s použitím vegetácie - medzí, trávnatých pásov, pásov krovísk a  stromov, zatrávňovanie a zalesňovanie nevyužívaných plôch a pod. Medzi  technicky náročnejšie „stavby" možno považovať rôzne typy prehrádzok  v prirodzených tokoch, odtokových rigoloch, jarkoch, eróznych ryhách  a pod. V prirodzených tokoch sa veľmi osvedčujú  priečne stavby z dreva  alebo kameňa, ale i náročnejšie prehradenie toku stavebnými košmi -  gabiónmi, kde sa zadrží iba menšie množstvo vody, hrádza ostáva  priepustná pre prirodzený prietok, ale v čase zvýšených prietokov sa  voda zadrží, spomalí a tým sa zníži jej účinok.   Spomínané opatrenia sú väčšinou nenáročné na kvalifikovanú prácu a sú  blízke vidieckemu obyvateľstvu, ktoré býva postihnuté nezamestnanosťou  viac ako ľudia v mestách. Rozsah a typ opatrení protipovodňovej či  protieróznej prevencie, ako aj objem vodozádržných zariadení možno určiť  z objemu povrchového odtoku a/alebo špičkového prietoku vody z dažďa  (prípadne z topenia snehu) na základe faktorov ako sú výška zrážky,  plocha povodia a celého komplexu charakteristík povrchu. V tomto  programovom návrhu uvažujeme s vytvorením vodozádržných zariadení  s cieľovou kapacitou jednorazového zadržania cca 100 mil. m3 dažďovej vody na celom území Slovenska, ktoré by prispeli k systému  protipovodňovej prevencie obcí. Pri predpokladanom ročnom zvýšení zásob  vodných zdrojov na Slovensku o 250 mil. m3 (prostredníctvom  zvýšeného vsaku, výparu a akumulácie dažďovej vody v rôznej forme) možno  zároveň očakávať väčšiu stabilitu hydrologického režimu i zvýšenú  prevenciu pred suchom a požiarmi.   Záver   Program Slovenské vody pre ľudí vytvára priestor na realizáciu  pomerne jednoduchých a nenáročných prác nebývalého rozsahu. Ide o práce  v záujme ochrany pôdy, vodných zdrojov, ekosystémov, ale i celkového  zhodnotnenia územia, pretože územie bohaté na vodu, pôdu a vegetáciu  naberá na cene, podobne ako územie so nižším rizikom povodní.  Obyvateľstvo sa dokáže ľahko stotožniť s cieľmi projektu, ktoré na neho  pôsobia motivujúco. Zamestnávanie pracovníkov na prípravu, realizáciu a  údržbu protieróznych a vodozádržných opatrení v území vytvorí užitočnú  zamestnanosť, ktorá bude impulzom pre hospodársky a sociálny rast  v budúcnosti. Počas realizácie program navyše svojou výdavkovou stránkou  prispeje k rastu HDP o 0,25 % v každom roku realizácie.   Za kľúčového partnera programu považujeme mestá a obce, ktoré  najlepšie poznajú svoju situáciu či už vzhľadom na nezamestnanosť  a ľudské zdroje, alebo na potrebu obnovy a ochrany vodných zdrojov.  Mestá a obce tiež najviac znášajú následky povodní. V praxi môžu  uplatňovať také nástroje, ako zostavovanie štúdií a projektov na  zvyšovanie vodozádržnej schopnosti územia svojho sídla (intravilánu  alebo extravilánu) a znižovanie vodnej erózie pôdy. Susediace sídla,  združenia miest a obcí, ako aj regióny môžu vytvoriť a koordinovať  spoločný systém protipovodňovej prevencie a podporovať vytváranie  vlastných poradenských, informačných a kompetenčných centier.   Autori projektu veria, že realizované práce budú prinášať úžitok ešte  dlho i po prípadnom ukončení projektov tak, ako to bolo v prípade  podobných projektov v rámci amerického New Deal-u. Samotné zvyšovanie  retenčnej schopnosti územia a zadržiavanie dažďovej vody v ňom je bez  ohľadu na momentálnu ekonomickú situáciu súčasťou strategických vládnych  a mimovládnych materiálov. Je i súčasťou medzinárodne presadzovaných  princípov dobrého hospodárenia s prírodnými zdrojmi a podmienkou  environmentálnej a ekonomickej bezpečnosti. Momentálna ekonomická  situácia len vytvára priaznivejšie podmienky k tomu, čo treba konať  a k čomu sú príslušné inštitúcie zaviazané konať.       79. riadne zhromaždenie NEF Hospodársky klub, 24. február 2009       Iniciátori programu : Združenie Neformálne ekonomické fórum  Hospodársky klub (www.hospodarskyklub.sk),  MVO Ľudia a voda, (www.ludiaavoda.sk),   Združenia miest a obcí Slovenska, (www.zmos.sk)           Citát:   Prácami na výsadbe lesov a na protipovodňovej prevencii poskytujeme  pracovné príležitosti štvrťmiliónu nezamestnaných, najmä mladým mužom,  ktorí majú rodiny... zvyšujeme tým hodnotu našich prírodných zdrojov a  znižujeme značnú časť súčasného sociálneho napätia... a chránime tak  nielen prírodné, ale aj ľudské zdroje. Veľkou výhodou tejto práce je, že  je veľmi adresná a nevyžaduje veľa mechanizácie.   /F.D. Roosevelt, Rozhlasové posolstvo americkému  ľudu, 7.máj 1933/         [1] Koncepcia vodohospodárskej politiky SR do roku 2015   [2] Viď napr.: Stratégia ZMOS v oblasti protipovodňovej prevencie a ochrany  územia miest a obcí pred povodňami, 2007;  Zásady integrovaného  manažmentu vodných zdrojov na území obcí a ich povodí, ZMOS, 2008   [3] Stratégia ZMOS v oblasti protipovodňovej prevencie a ochrany územia  miest a obcí pred povodňami, 2007   [4] tamže   [5] Viď napr. dokument „Najlepšia prax protipovodňovej prevencie,  protipovodňovej ochrany a zmierňovania povodní," vypracovaný riaditeľmi z  oblasti vodného hospodárstva krajín EÚ v roku 2003         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Poloz na hrad!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Zavšivavená spoločnosť trollami s podporou SME?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Bratislava šiestym najbohatším regiónom EÚ. Západné Slovensko 239-tým…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Nemajú Boha pri sebe!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Kravčík
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Kravčík
            
         
        kravcik.blog.sme.sk (rss)
         
                                     
     
         Presadzujem a podporujem agendu „VODA PRE OZDRAVENIE KLÍMY“. Jej cieľom je posilnenie environmentálnej bezpečnosti prostredníctvom zodpovedného prístupu v ochrane prírodného a teda i kultúrneho dedičstva. Napĺňanie agendy, založenej na prijatí novej, vyššej kultúry vo vzťahu k vode, môže na Slovensku vytvoriť viac ako 100 tisíc a v Európe vyše 5 miliónov pracovných príležitostí. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    629
                
                
                    Celková karma
                    
                                                7.10
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Povodne
                        
                     
                                     
                        
                            Hladujúci potrebujú vodu
                        
                     
                                     
                        
                            Klimatická zmena
                        
                     
                                     
                        
                            VODA zrkadlo kultúry
                        
                     
                                     
                        
                            http://s07.flagcounter.com/mor
                        
                     
                                     
                        
                            Nová vodná paradigma
                        
                     
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            http://moje.hnonline.sk/blog/4
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Kandidáti za poslancov do EP
                                     
                                                                             
                                            Ladislav Vozárik
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            http://www.clim-past.net/2/187/2006/cp-2-187-2006.pdf
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.aktualne.centrum.cz/blogy/jana-hradilkova.php
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hospodarskyklub.sk
                                     
                                                                             
                                            ashoka.org
                                     
                                                                             
                                            bluegold-worldwaterwars.com
                                     
                                                                             
                                            holisticmanagement.org
                                     
                                                                             
                                            theglobalcoolingproject.com
                                     
                                                                             
                                            ludiaavoda.sk
                                     
                                                                             
                                            watergy.de
                                     
                                                                             
                                            waterparadigm.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nemajú Boha pri sebe!
                     
                                                         
                       Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                     
                                                         
                       Ako sa z jednej ochranárskej ikony stala obyčajná nula
                     
                                                         
                       10 rokov po víchrici v Tatrách stále v zákopoch
                     
                                                         
                       Primátor všetkých Košičanov?
                     
                                                         
                       Dnes zasadá Vláda v Ubli
                     
                                                         
                       Aspoň pokus o integráciu Rómov? Za primátora Rašiho? Zabudnite!
                     
                                                         
                       Róbert Fico je bezpečnostným rizikom pre Slovensko
                     
                                                         
                       Kto je zodpovedný za pád starenky do kanalizačnej šachty v Michalovciach
                     
                                                         
                       Zdravé Košice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




