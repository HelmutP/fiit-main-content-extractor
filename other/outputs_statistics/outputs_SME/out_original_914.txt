
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Petra Struková
                                        &gt;
                Nezaradené
                     
                 Fenomén menom Simpsonovci 

        
            
                                    2.10.2008
            o
            17:23
                        |
            Karma článku:
                10.75
            |
            Prečítané 
            7208-krát
                    
         
     
         
             

                 
                    Kto by ich nepoznal? Sú žltí, vtipní a obľúbení takmer na celom svete...jednoducho Simpsonovci! 
                 

                  Ako je možné, že skoro dvadsať rokov patria Simpsonovci k najúspešnejším seriálom? Čo je na nich také zvláštne? Na prvý pohľad možno nič...len to, že sú žltí:-) Táto žltá rodinka, spolu s ďalšími obyvateľmi Springfieldu si získala srdcia divákov závratnou rýchlosťou. Simpsonovci boli pôvodne len krátke, asi dvojminútové, skeče a svoju dnešnú podobu získali až po dvoch rokoch, teda v roku 1989. Simpsonovci v krátkych skečoch sa od tých neskorších dosť líšili, veď posúďte sami....                A prečo sú už toľko rokov stále populárni? Jedným z dôvodov môže byť to, že tvorcovia sa do tohto seriálu snažia premietnuť rôzne problémy dnešnej americkej (ale nielen americkej) spoločnosti. Je tu nastolená otázka homosexuality (Selma, sestra Marge, je lesbička a asistent pána Burnsa, Smithers, je gay), ďalej tu možno nájsť problém globálneho otepľovania, cirkvi, politikov a ich aféry, morálne problémy, ako alkoholizmus i drogy, vzťah k starým ľuďom, otázku národnostných menšín atď.     Autori Simpsonovcov radi zosmiešnia a vystrelia si z kohokoľvek a čohokoľvek a to im taktiež zabezpečilo úspech. V seriáli sa objavilo mnoho známych osobností, medzi ktorými nechýbali ani niektorí americkí prezidenti, slávni herci, speváci, či hudobné kapely a aj osobnosti z vedeckej oblasti, ako je napríklad Bill Gates. Z hercov spomeniem Aleca Baldwina, Kim Basinger, Elizabeth Taylor, Mela Gibsona. Z hudobného sveta to boli Rolling Stones, Aerosmiths, Green Day, Cypress Hill, Metallica, členovia The Beatles, Smashing Pumpkins, U2, 50 Cent a niekoľko ďalších známych mien.      Metallica            Tu je niekoľko zaujímavostí, týkajúcich sa Simpsonovcov: V USA je 121 Springfieldov, ale ten, v ktorom žije žltá rodinka, má vraj číslo 122. Homer, Lisa a Bart sú jediné postavy, ktoré v každej epizóde povedia aspoň jednu vetu. Hlavné postavy sú pomenované podľa členov rodiny Matta Groeninga, jeho otec a syn sa volajú Homer, jeho matka je Marge, sestry Maggie a Lisa a ďalší syn má meno Abraham. Meno Bart vzniklo z anglického slova BRAT, čo znamená fagan, nespratník. Bartove prvé slová boli Ay caramba! Lisa povedala Bart! a Maggie povedala Daddy!, ale nikto to nepočul. Homerova hláška D´OH sa stala súčasťou Oxfordského slovníka ako plnohodnotné slovo.                                                                                                                         Toto sú Simpsonovci, bez pochyby najslávnejšia americká rodina. Na televíznych obrázovkách je už takmer 20 rokov a ja verím, že vďaka originalite a zmyslu pre humor jej tvorcov to tak zostane naďalej. Pri tejto žltej rodine vyrástlo pár generácii a dúfam, že ich ešte zopár pri nich vyrastie... Mám ich rada:-)       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Struková 
                                        
                                            A čo ďalej?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Struková 
                                        
                                            Na čaji s budhistickým mníchom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Struková 
                                        
                                            Zmrzlinová mafia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Struková 
                                        
                                            Aj Slováci zažijú dúhu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Struková 
                                        
                                            Tak toto sa môže stať len na Slovensku...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Petra Struková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Petra Struková
            
         
        strukova.blog.sme.sk (rss)
         
                                     
     
        Milujem dobrý čaj, prechádzky v prírode, večery v čajovni, pohodu a pokoj...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1620
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako sa cíti človek po roku liečby na psychiatrii
                     
                                                         
                       Premiér Fico ruinuje Slovensko
                     
                                                         
                       Kotleba - had na Ficových prsiach
                     
                                                         
                       SMER buď nepozná realitu alebo vedome zavádza
                     
                                                         
                       Objavenie Oravy
                     
                                                         
                       Zachráňte blogera Mareka Albrechta!
                     
                                                         
                       Keď kráľ bulváru prehovorí...
                     
                                                         
                       Haló, páni Haulík a Baránek, aj ja som tu!
                     
                                                         
                       A čo ďalej?
                     
                                                         
                       Rozumiem vám, pán premiér, ale začať takto?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




