

 Popri regáloch sa prepletá vysoká štíhla postava. Neomylné držanie tela. Žieňa na ktoré som už dávno zabudla. Pred 36 rokmi sme sa delili v jednej malej nemocnici na pôrodnom oddelení o izbu. Sestrička v náručí nosila dva naše drobné uzlíčky. 
 Bolo to moje prvé dieťa. Dievčatko s tvárou ako biely sneh a modrajúcimi prstíkmi. Už vtedy som vedela že niečo nie je v poriadku. Moja susedka porodila svoju vytúženú tretiu dcérku po dvoch synoch. Pyšný otecko s dvomi synčekmi chodieval pozrieť ich malú sestričku. Kopa príbuzenstva, plno kytíc, nefalšované skutočné šťastie. 
 Moje dieťa zo skríženou transpozíciou veľkých ciev odišlo po mesiaci. Ešte veľmi dlho som si spomínala na šťastie iných matiek, ktoré majúc svoje zdravé deti oň neprišli. 
 A teraz po toľkých rokoch toto stretnutie. Pani ma vzácne spoznala a tak celkom prirodzene sme skončili na káve. Je zvláštne prestrihnúť toľké roky života...ako by to bolo včera. 
 Prirodzene ma zaujímalo ako sa má a aký osud mal ten jej malý uzlíček. 
 Ten tu  už nie je... 
  Dcérka zahynula pri havárii minulý rok. 
 Ostala po nej iba vnučka a zopár  predčastných ostrých vrások okolo úst. Synovia sú už ženatí a pomáhajú ako vedia.... 
   
   
 Nikomu nezáviď.... 
   
   

