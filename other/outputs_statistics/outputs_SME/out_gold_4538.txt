

 aaaaaaaaaaaaaaaaa 
 
 
 
  Sváčko to sú naši fyzici zo SAV, že vraj ešte nevideli štvrtý rozmer, nože dajte im okoštovať z toho vášho... 
 aaaaaaaaaaaa 
Rozhovor JÁRAY-Luby 

 Touto cestou predkladám širokej laickej i odbornej verejnosti, môj rozhovor s bývalým predsedom Slovenskej akadémie vied, SAV, pánom Štefanom Lubym: 
  
 aaa 
  
 Dr.h.c. prof. Ing. Štefan Luby DrSc.  
  
 Pán Luby je  dobrým pracovníkom SAV, je dobrý organizátor a propagátor vedy, je dobrý priateľ a kolega, je dobrý  vlastenec, má ešte mnoho tu nemenované ľudské cnosti, preto je obľúbený medzi zamestnancami SAV, ale aj v mieste jeho, trvalého i prechodného  pobytu.  
 Má len jednu malú ľudskú chybičku a to, že on je veľmi, veľmi hlúpym fyzikom, no pritom nie hlúpejším ako sú ostatní zamestnanci FÚ SAV, kde sa on momentálne prechádza, sedí, či leží, alebo vypisuje nejaké fyzikálne sprostosti.  
 Ajhľa alegória kolektívu, v ktorom sa pán Luby v súčasnosti pohybuje. 
   
 
 
   

   
 Priebeh rozhovoru je nasledovný: 
  JÁRAY. 
  Pán akademik SAV,  môžu sa vaše názory na fyziku, považovať za zhodné s názormi zamestnancov FÚ SAV? 
  -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby.  
 Áno. 
  
   JÁRAY. 
 Súhlasíte so mnou, že výsledky z vyučovania fyziky na školách a univerzitách SR patria k najhorším zo všetkých vyučovaných predmetov? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby.  
 Áno. 
  
  JÁRAY.  
 Mohli by ste stručne uviesť príčiny tejto neblahej reality. 
  -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby 
 Jednoznačne ide o zlý spôsob výučby fyziky a to hlavne z toho dôvodu, že sami učitelia nie sú odborne pripravení vyučovať fyziku, ale aj preto, že sú slabo platení. Dostávajú málo eurá. 
  
  JÁRAY. 
 Tam kde fyziku vyučuje dobre platený odborník, tak tam s fyzikou nie sú problémy? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby. 
 Jednoznačne nie. 
  
  JÁRAY. 
 Stručne povedané, keby ste vy osobne vyučovali fyziku, tak vaši žiaci by nemali s fyzikou žiadne problémy? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby.  
 Nie. 
  
   JÁRAY. 
 Pán akademik SAV,  vyznávate skutočnosť, že gravitačná a zotrvačná hmotnosť telies vykazuje ekvivalenciu, že nikdy nevieme rozoznať kedy ide o gravitačnú a kedy o zotrvačnú hmotnosť telies pre prípady a = g? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby. 
 Áno. To tvorí základ fyziky, i celéj prírodovedy. 
  
  
  JÁRAY. 
 Aká veľká je podľa vášho názoru zotrvačná hmotnosť telies v inerciálnej, (zotrvačnej) sústave? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby. 
 Nulová. 
  
 JÁRAY. 
 Môže mať teleso nulovú gravitačnú hmotnosť? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby.  
 Nie. 
  
  JÁRAY. 
 Nestačí táto skutočnosť na to, aby neplatil princíp ekvivalencie zotrvačnej a gravitačnej hmotnosti telies? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 
  Luby. 
 Nie. 
 
  
 
 
  JÁRAY. 
 Viete vy o tom, že k tomu aby stlačená pružina v jednom a tom istom bode ostala stáť na jednom mieste, alebo sa zmršťovala, či rozťahovala, sú potrebné tri rôzne veľké parciálne tlaky, teda tri rôzne hmotnosti telies?  
 Viete vy o tom že existujú tri rôzne  hmotnosti telies? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby. 
 Úprimne sa priznám, že neviem. Ale to je najväčší nezmysel aký  som v živote počul.  

  
 
 
 JÁRAY. 
 Zmeral už niekedy nejaký zamestnanec FÚ SAV veľkosť tlakov pri stláčaní a rozpínaní sa pružiny v určitých, konkrétných bodoch? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Luby. 
 Načo? 

  
 
 
 JÁRAY. 

 Aby vedel  hodnoverne povedať, či je to pravda, že telesá majú tri rôzné hmotnosti, alebo nie.  
 
 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY---- 
 Luby. 
 Skončíme tento rozhovor. Čo tvrdíte to nikoho z vedcov nezaujíma. To nemôže byť pravda, to nebola pravda od Galilea, načo to preverovať teraz. To sú iba taľafatky a urážky statočných vedcov zo SAV. 

  
 
 
  JÁRAY. 

 V súčasnej, relativistickej fyzike sú dôležitejšie osobné dojmy fyzikov, alebo výsledky experimentálnych meraní?  
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----  
 Luby. 
 Keby bola pravda čo tvrdíte, to by svedčilo v prvom rade o tom, že okrem vás ani jeden fyzik v dejinách ľudstva nedokázal myslieť normálne.  
 Pravda bude ale taká, že vy nedokážete myslieť normálne, čiže tak ako myslia ostatní fyzici SAV i celého sveta. Preto aj výsledok vášho hypotetického experimentu, musí byť nenormálny.  
  
 JÁRAY. 
 Ja neprezentujem svoje "nenormálne" myšlienky, ale overené fakty, namerané počas pohybu pružiny, ktoré si môžu overiť aj zamestnanci FÚ SAV, keď sa práve nenudia, či nespia. Je to fyzikom v SAV zakázané? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----  
 Luby. 
 Je zakázané narušovať tvorivú prácu (svätý pokoj) zamestnancov FÚ SAV, rôznymi  taľafatkami. My vo FÚ SAV  vieme čo je pravda a čo nie, nám to nemusí nikto z ulice hovoriť. Pochopte už to konečne, my fyzici zo SAV sme v prevahe a presile. Aj prostý pracujúci, i nezamestnaní ľud stojí na našej strane.V demokracii rozhoduje opravde väčšina a nie akési experimentálné výsledky. 
  
 JÁRAY. 
 Takže podľa vás a fyzikov zo SAV, jedným, stálym, parciálnym tlakom, môžeme vyvolať tri rôzne interakcie v pružine?   
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----  
 Luby. 
 My vo FÚ SAV môžeme čo sa nám zachce, vy nemôžete nič !!! 
  
  
 Tento text organicky doplňujú nasledovné články opisujúce debilitu fyzikov SR: 
 http://jaray.blog.sme.sk/clanok.asp?cl=206142&amp;bk=5145  
 http://jaray.blog.sme.sk/clanok.asp?cl=215505&amp;bk=92788  
   
  

