
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľudovít Kaník
                                        &gt;
                POLITIKA
                     
                 Kto to zaplatí, socialisti? 

        
            
                                    16.5.2010
            o
            12:41
                        (upravené
                16.5.2010
                o
                18:14)
                        |
            Karma článku:
                11.93
            |
            Prečítané 
            3579-krát
                    
         
     
         
             

                 
                    Prúser spôsobený socialistickou politikou dopadá na celú Európu. Pôžičky nepomáhajú, na nevyhnutné škrty je málo sily a vôle. Euro sa otriasa v základoch. Všetko následkom klamstiev o sociálnom state. Slovensko zatiaľ ide po tej istej ceste.
                 

                 V mnohých krajinách Európy často  vládnu socialisti. Sú pre nich typické reči o sociálnom štáte, ale zároveň tiež  o tom, ako by sme mali dobiehať svet zvyšovaním našej konkurecieschopnosti. V praxi sa realizuje žiaľ len to prvé. Sociálny štát, ktorý v očiach a v chápaní socialistov znamená zväčša len rozdávanie, sľubovanie neba na zemi, rozširovanie štátnej byrokracie a neustáleho zadlžovania, končí absolútne nesociálnymi výsledkami. Vždy sa totiž tento socialistický zázrak života na dlh končí smutným a krutým vytriezvením, rastom chudoby, demonštráciami a nepokojmi v uliciach a prepadom hospodárskej výkonnosti.   Nie je to len problém Grécka. Všetci vieme, že problémy majú Španieli, Portugalci, Taliani, Maďari ale aj iné krajiny. Záchvat populistických sľubov totiž nezachytil len socialistov, ale v snahe udržať si voličov a moc sa do tejto sebevražednej súťaže pustili aj mnohé, za pravicu sa považujúce európske strany.   Najnovším dôkazov zhubnosti takejto politiky z tábora socialistov je španielsky premiér Zapatero. Socialista, ktorý tak ako Fico a jeho kolegovia rád rozdáva to, čo nevytvoril a zadlžuje krajinu, zistil, že stačí iba krôčik a aj Španielsko padne do priepasti bankrotu. Preto prichádza so škrtmi, prepúšťaním, znižovaním sociálnych vymožeností. Teraz ide opravovať to, čo sám kazil. Ďalší v poradí sú Portugalci, ktorí idú zvyšovať dane. Francúzi sa začínajú vyhrážať, že opustia eurozónu, pretože sa snažia zachrániť svoje banky, ktoré do Grécka, Španielska a Portugalska pumpovali peniaze. Na týchto príkladoch vidno, že celá ilúzia, tzv. sociálneho štátu na dlh je jedno obrovské nezodpovedné klamstvo, za pomoci ktorého sa síce dajú vyhrať voľby, ale krajina a jej obyvatelia za to neskôr kruto zaplatia. Gyurcsány, bývalý socialistický premiér Maďarska to priznal prvý, keď priznal o prístupe k občanom a voličom: „klamali sme ráno, klamali sme na obed, klamali sme večer".   Slovensko vďaka odvážnym reformám a dobrej hospodárskej politike predchádzajúcej vlády, začalo dobiehať vyspelé európske krajiny a zvyšovať životnú úroveň obyvateľov.   Žiaľ na scénu sa dostala iracionálna nekompetentná a škodlivá a deštrukčná politika Ficovej vlády, ktorá výrazne prehĺbila krízu na Slovensku, rekordným spôsobom nás všetkých zadlžila a nič sociálneho okrem omrviniek, ktorými si kupuje voličov, nespravila. A teraz podľa nej, máme prispievať navyše na všetkých tých, ktorí žili doteraz roky na dlh na základe nezodpovedných politík svojich vlád. Zrejme preto, lebo Fico vie alebo tuší, že jeho politika smeruje presne tam, kde je dnes Grécko, Španielsko, Portugalsko. Myslí si, že aj nám to potom niekto iný zaplatí. Nezaplatí. Zatiaľ máme platiť len my zdá sa, že to Grécku ani euru veľmi nepomôže. Tešia sa len veriteľské banky.   Neexistuje  perpétum mobile. Takéto politiky sťahujú celé Slovensko, ale aj iné krajiny, v ktorých sa vykonávajú, na hospodárske dno.  Euro sa nedá udržať ďalšími a ďalšími pôžičkami, ale hospodárskou politikou, ktorá podporuje rast, konkurencieschopnosť, tvorí hodnoty a nezaťažuje obyvateľov rastúcim dlhom. Takúto politiku sme na Slovensku mali do nástupu Ficovej vlády. Dnes sme na Gréckej ceste a ani zástupné problémy a hranie nacionalistickými kartami, ktoré tu rozohráva Fico, Orbán a Slota, nemôžu zakryť skutočné problémy Slovenska. Tými je vysoká nezamestnanosť, zhoršujúce sa podnikateľské prostredie, korupcia,  zle fungujúce súdnictvo.   My riešenia máme. Vieme, čo treba urobiť, aby sme znížili nezamestnanosť, obnovili hospodársky rast, potlačili korupciu a urobili poriadok na súdoch.  Už sme to raz dokázali. Najprv však musíme poraziť Fica vo voľbách.   Viaceré aktuálne témy preberáme na mojom Facebook profile www.facebook.com/ludokanik.   Pre tých, čo radi sledujú krátke správy som aj na Twitteri - www.twitter.com/ludokanik   Všetky informácie o mne sa sústreďujú na mojej webstránke -  www.kanik.sk       Ľudo Kaník   Kandidát do NRSR za SDKÚ-DS   Zdieľať   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (94)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Nestačí prejsť do 2. kola, treba zvíťaziť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Fico sa sám usvedčil z manipulovania štatistík
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Kam sa podelo 400 miliónov pri transakcii súvisiacej s SPP?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Čo môžete naozaj čakať od smeráckeho župana?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľudovít Kaník 
                                        
                                            Ktože to obviňuje z napomáhania Smeru? Ich koaličný partner? :-)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľudovít Kaník
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľudovít Kaník
            
         
        ludovitkanik.blog.sme.sk (rss)
         
                                     
     
        Poslanec NRSR za SDKÚ-DS. Exminister práce, sociálnych vecí a rodiny SR a dlhoročný predseda DS , ktorá sa pod jeho vedením po dlhoročnej spolupráci s SDKÚ integrovala na politickú stranu SDKÚ-DS, zodpovedný za reformu sociálneho systému a trhu práce. Pripravil a zrealizoval reformu zákonníka práce, dôchodkovú reformu, reformu systému sociálnych dávok a boj s čiernou prácou. Dnes aktívny člen SDKÚ - DS a zakladajúci člen občianskeho združenia SPORITEĽ na ochranu záujmov občanov, účastníkov dôchodkového sporenia v II. pilieri dôchodkového systému. 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    51
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2934
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            DÔCHODKY
                        
                     
                                     
                        
                            PRÁCA
                        
                     
                                     
                        
                            RODINA
                        
                     
                                     
                        
                            POLITIKA
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nestačí prejsť do 2. kola, treba zvíťaziť
                     
                                                         
                       "Manažér" Paška
                     
                                                         
                       Fico sa sám usvedčil z manipulovania štatistík
                     
                                                         
                       Kam sa podelo 400 miliónov pri transakcii súvisiacej s SPP?
                     
                                                         
                       Máme sa tváriť, že čierne stavby a krádeže pozemkov nevidíme?
                     
                                                         
                       Na čo prišli na ministerstve práce
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




