
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Štefancová
                                        &gt;
                Politika
                     
                 Pseudoliberalizmus 

        
            
                                    12.4.2010
            o
            23:02
                        (upravené
                22.1.2013
                o
                18:03)
                        |
            Karma článku:
                5.36
            |
            Prečítané 
            1133-krát
                    
         
     
         
             

                 
                    Slobodu by sme mali chápať takto: "Moja sloboda končí tam, kde sa začína tvoja sloboda." Koľkí "liberáli" dnes na plné ústa vykrikujú o slobode. Nazývajú sa liberálmi, väčšinou neprávom, pretože to, o čo sa snažia, má od slobody ďaleko.
                 

                     Pripomeňme si definíciu slova liberalizmus:   A political theory founded on the natural goodness of humans and the autonomy of the individual and favoring civil and political liberties, government by law with the consent of the governed, and protection from arbitrary authority. (zdroj: answers.com)   V skratke to znamená, že človek je autonómnou jednotkou a že spoločnosť podporuje občianske a politické slobody. Toto znamená liberalizmus. Slobodu jednotlivca. Nič také, ako dnešné mylné chápanie liberalizmu vo forme všetko staré je na nič, skúšajme nové veci, búrajme konvencie, poľavujme v morálke - nie je to zlé, je to liberálne a dobré. Je to pokrok.   Za prvé - určite si nemyslím, že by sme mali žiť ako v stredoveku, nové veci sú fajn, sú nesmierne potrebné pre rozvoj spoločnosti. Pokrok potrebujeme - ale nie ten zvrátený, hlásaný pseudoliberálmi.   Za druhé - nemyslím si ani, že všetci, ktorí sami seba za liberálov označujú, sú práve tí pomýlení jedinci. Nie všetci - len ich podmnožina.    A za tretie - nie o tých prvých dvoch bodoch som chcela, ale to len na úvod, na uvedenie do situácie.   Takže, teraz k veci: čo mi vadí na pseudoliberáloch.    Hlásajú slobodu. Vadí im, keď niekto potláča ich slobodu. A pritom sa sústavne snažia o potláčanie, napríklad aj mojej, slobody. Vezmime si taký útok pseudoliberálov na kresťanov, konkrétne na RKC. Každý deň čítam články, blogy, diskusie, kde si zopár jedincov potrebuje vyliať srdce, vravia, ako ich Cirkev obťažuje, ako im niečo nanucuje, píšu príklady z cirkevných škôl, kritizujú šírenie homofóbie alebo "stredoveký" názor proti "slobodnej voľbe ženy". Presne tak, RKC ich neskutočne obťažuje a sústavne sa ju preto snažia zdiskreditovať.    Ako však tá RKC reálne ovplyvňuje inak, alebo nijako, veriacich ľudí? RKC vás nenúti chodiť do kostola a počúvať Slovo Božie z úst kňazov, RKC vás nenúti prihlasovať vaše deti do cirkevných škôl, nenúti vás vychovávať vaše detí k averzii voči homosexuálom ani nikoho nenúti neísť na potrat. Nikoho k tomu nenúti, pretože je to inštitúcia založená na slobode. Na slobodnom vstupe do nej. (Prosím vás, teraz sem hlavne nikto nepleťte nejaké stredoveké križiacke výpravy, pretože to je niečo definitívne prekonané a irelevantné). Ale keď už sa človek rozhodne byť katolíkom, tak snáď so všetkým, čo tá inštitúcia hlása a čomu verí. A keď sa pre to rozhodne niekoľko miliónov ľudí, tak zvyšok by ich mal minimálne rešpektovať. Preto nepochopím praktiky určitých "liberálnych" skupín, ktoré nemajú v Spojených štátoch problém postaviť sa pred vchod do kostola, stavať sa veriacim do cesty a na malé deti, ktoré prichádzajú s rodičmi na Bohoslužbu, pokrikovať zvrhlé heslá a kritizovať pred nimi ich rodičov, pretože tí nepovažujú sodomiu za niečo, čo je v poriadku. Okrem iného je toto ich konanie postavené mimo zákon, pretože podobné protesty vo vzdialenosti menej ako 50 metrov od miest poberajúcich všeobecnú úctu, medzi ktoré patria aj kostoly, je nezákonné. Nepochopím, že títo pseudoliberáli a homosexuáli žiadajú od Cirkvi uznanie homosexuálnych manželstiev, pretože aj oni chcú údajne patriť do RKC. NAČO chcú do nej patriť, keď popierajú jej základné hodnoty??   Čo ma však najviac zaráža je situácia, ktorá sa pomaly stáva nekontrolovateľnou. V Británii sa napríklad na cirkevnej škole nemôže presadzovať, že potrat je nesprávna voľba, hoci je to jadrom katolíckej morálky - ochrana života od jeho počiatku až po prirodzenú smrť. Rovnako už nemôžu študentov presviedčať, že striedanie sexuálnych partnerov je zlé. A určite nemôžu vyučovať, že homosexualita je choroba.    Ale prečo?? Nenútia ten názor iným vyznaniam, nenútia ho ateistom, len chcú vychovávať študentov vo viere. V tej viere, kvôli ktorej sa ich rodičia rozhodli dať dieťa na cirkevnú školu.    Toto sa deje v dnešnom svete. Pod pozlátkom liberalizmu sa ľudia snažia presadzovať zvrhlé hodnoty a kritizovať morálku slušných občanov.       Ale viete, čo je na tom pravom liberalizme najkrajšie? Mám právo napísať, že homosexualita je choroba, mám právo napísať, že sodomia je zvrátenosť, mám právo napísať, že potrat je vražda. Nikoho nenútim mať ten istý názor, ale na základe liberálnej spoločnosti s prihliadaním na slobodné občianske a politické práva mám právo mať svoj názor, mám právo ho prejaviť a mám právo na rešpektovanie spoločnosťou. Aj keď je plná "liberálnych" slobodupotláčajúcich aktivistov, o ktorých rešpekt ani nie je hodné stáť.       A teraz - pustite sa do mňa. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (252)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Blaženosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Usmievať sa na svet
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Roman Holiday
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Teoretici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Prečo radšej SaS ako Smer
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Štefancová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Štefancová
            
         
        stefancova.blog.sme.sk (rss)
         
                                     
     
        som blázon, som romantička, som dieťa revolúcie, som žena s veľkým potenciálom, čo čaká na správnu chvíľu, som zvedavá, som živel, som originál :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1603
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            univerzita
                        
                     
                                     
                        
                            zo sveta
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keby tak Fico tušil
                     
                                                         
                       O čom sa písalo v novinách pred 100 rokmi
                     
                                                         
                       Otvorený list homosexuálnej aktivistke pani Hane Fábry
                     
                                                         
                       Chlapča v kuchyni
                     
                                                         
                       Pár čísel
                     
                                                         
                       Červené ruže a saudský Nový Rok...
                     
                                                         
                       Na rovinu: Deti z detských domovov nemajú šancu
                     
                                                         
                       Krupobitie
                     
                                                         
                       Súrodenci a láska nekonečná (alebo jej rôzne podoby...)
                     
                                                         
                       Staré diáre
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




