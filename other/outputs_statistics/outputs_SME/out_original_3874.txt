
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Feľbaba
                                        &gt;
                Súkromné
                     
                 Komu slúži právo ? a naša mentalita 

        
            
                                    3.4.2010
            o
            19:30
                        (upravené
                3.4.2010
                o
                19:50)
                        |
            Karma článku:
                4.71
            |
            Prečítané 
            893-krát
                    
         
     
         
             

                 
                    V poslednom 20-ročí sme boli svedkami toľkých udalostí a káuz, že môžeme v kľude túto éru   považovať  za priekopnícku . Hlavne čo sa týka Slovenska. Podstatné je však to, že dnes to už berieme ako „štandard“.   Áno, naša mentalita sa po roku 1989 rapídne zmenila a zmenilo sa aj právo. Doba nám dala šancu a dodnes si myslím, že sú medzi nami ľudia, ktorí sa márne chytajú za hlavu preto, akú mali šancu zbohatnúť. Tí, ktorí boli rýchlejší a dravší pochopili posolstvo novej doby a hľadali možnosť, ako rýchlo zbohatnúť. V 90- tých rokoch to bola móda a trendy bolo, ak ste to dosiahli tichučko, nečestne a hlavne spomínane rýchlo. Potom už iba nasledovalo to, kvôli čomu to väčšina ľudí robila a to ukázaním , že ja mam a ty nemáš. Myslím, že sa to dochovalo a netvrdím, že pred rokom 1989 to nebolo ! Asi je to súčasť našej mentality, ale ja sa však pýtam, kedy sa priblížime ku skutočným hodnotám a to podobným ako v západnej Európe? Verím, že je to proces, aj keď nie celkom sa to niekedy tak javí.
                 

                            Ak pokračujem ďalej, otázka v titule ostáva: „Komu slúži právo? "   Áno správne, nám :-) Právo „nám" malo slúžiť vždy a či už v dobách minulých, alebo  prítomných, bolo odôvodňované  jednak poriadkom a spravodlivosťou, ale aj sledovaním určitých  záujmov. Vniesť do toho trochu svetla  je relatívne a to hlavne z toho dôvodu, že zmysel „ sledovania určitých záujmov" sa dá vysvetliť rôzne, ale verím, že každý na základe svojho vnútorného racia, to pochopí správne. Ale ľudí to nezaujíma! Ľudia povedia, že „ to je vyššia sféra ľudí, ktorí si robia čo chcú a nič/nikto ich nezastaví" alebo „ čo my zmôžeme?" Škoda, že v našej spoločnosti sa ustálila otupenosť a vnútorné nariekanie. Nieje to predsa súčasť našej mentality, ale pomaly sa stáva. Trpíme, že nás niekto okráda, ale neurobíme nič! Tým sa stávame poddajnými. Je pravdou a plne sa stotožňujem s názorom sociológa Gabriela Tardeho, ktorý tvrdí , že aj štát vzniká vrodenou potrebou ľudí podrobovať sa a poslúchať. Podľa môjho názoru to funguje aj na nižšej báze, ako vo vzťahu občan- štát a to na báze majetná osoba- nemajetná osoba. Totižto, peniaze oslobodzujú a doplním ešte, že peniaze robia človeka.  Je to tak a oči si zakrývať nemusíme. Utopisticky si myslím, žeby sme všetci mali  zvýšiť snahu  peniaze získať. Naša spoločnosť by sa stala ľudskejšia a nikto by si v duchu nemohol myslieť: „ a teraz čumte a záviďte drahí priatelia!"               Ale ako dosiahnuť bohatú spoločnosť ? To je otázka, ktorej odpoveď sa hľadá ťažko, ale osobne by som sa zameral na naše zákony. Zákon je kľúčom k zmene, ale nie zas každý. Správny zákon rieši a predvída následky, nesprávny zas nerieši a odsúva problematiku. Niekedy je ťažké predvídať čo nastane. Obozretne pristupovať k zmenám sa dá a tie následky bývajú rôzne.  Zmena raz príde, ale viem, každý si zas a zas povie, že mentalita sa nezmení. A kebyže nie, tak máme možnosť sprísniť zákony. Predsa ak niečo nejde podobrotky, pôjde to po zlom. Ale o bohatej spoločnosti až nabudúce....  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Feľbaba
            
         
        felbaba.blog.sme.sk (rss)
         
                                     
     
        študent, ktorého trápia pomery v spoločnosti a písanie berie ako výzvu :-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    893
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            -
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




