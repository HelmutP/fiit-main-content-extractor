

 Preto varím ja. A šťastie a láska  a poruzumenie, je u nás samozrejmosťou ako jedlo na stole. Stáť v kuchyni, je pre mňa oddych a relax zároveň. Ako by  som písal  báseň, krásne a povznášajúce. Hádam preto som môjmu receptu dal názov. 
 STROGANOV S RYŽOU ALA POÉSIA 
 Čo k tomu potrebujeme? Najmä chuť čarovať v kuchyni 
 ale nebyť pritom Coprfield, 
 ale predsa. 
   
 Na STROGANOV S RYŽOU ALA POÉSIA 
 /pre dve osoby / 
 je potrebné 
 250 g ryže 
 olej, soľ, vegetu , 
 300 g kuracieho mäsa 
 dve veľké cibule, sladkú papriku 
 dve malé paradajkové pretlaky, kečup 
 tvrdý syr. 
   
 Začíname. 
   
 Naprv pokrájame na mierne / len tak akurát - nie malé nie veľké / podlhovasté kúsky. Cibuľu pokrájame na pol mesiačiky, posolíme a necháme slziť. No netreba pritom plakať, ale tešiť sa zo života. Za pár minút cibuľa pustí šťavu. Na rozpálenom oleji speníme poriadne cibuľku do mäkka, / za stáleho miešania pridáme mäso a pár minút /cirka 7 - minút /, takisto za neustáleho  miešania spečieme mäsko s cibuľkou. pridáme trocha sladkej  papriky. Vznikne krásna vôňa, varenie je naozaj poézia. Podlejeme vodou, pridáme pretlaky a kečup. Ochutíme vegetou tak, aby to malo grády / nie príliš slano / a s pokojou v duši Jany Kirschner varíme do mäkkúčka . 
 Nezabúdame  miešať. 
 Ryžu uvaríme vo vode, do ktorej sme dali trocha vegety. Ryžu necháme iba odstáť, nepreplachujeme. 
 Jedlo prestrieme a na ryžu postrúhame syr a navrch syra s ryžou dáme trocha zelenej vňate. 
 Svojej polovičke zaprajeme dobrú chuť, uvidíte ako Vás bude milovať. 
 U nás to funguje tak už päť rokov. Sme sýti a nekonečne šťastní. 
 Bon apetít 
   
   
   
   
   
   

