

  
 Sú to predpoklady a plány, je krásne o nich premýšľať, sú však veľmi vzdialené od tejto reality, keď mi soľ, ktorú prijímam do tela ničí srdce, keď sa ióny soli kúpu v mojej krvi, bublajú v komorách ako vriaca polievka a ja utekám po rovnej zelenej lúke, snažím sa pravidelne dýchať, cítim svoje srdce, cítim ho v ušiach, utekám a vravím si, že som silná, že som športovec a nad lúkou a nad mojou hlavou lietajú čierne vrany, utekám smerom k zasneženým horám a pred očami mám obraz bosého dieťaťa zo sna z minulej noci, dieťa stojí  nad Haďou riekou v štáte Idaho a na motúziku má uviazanú bielu kozu. 
 Dobehnem do cieľa a žiadostivo zatúžim po jarných búrkach a romantickom moknutí, po Friedrichovi Schillerovi a rozprávkovom jelenčekovi, zachce sa mi v búrke zachraňovať tulipány v črepníkoch, ktorým sa aj tak nedarí, ktoré zanedbávam a popri tom žiarlivo nakúkam do rozkvitnutých záhrad susedov. Chcem toho naraz veľa, chcem vanilkovú zmrzlinu, perníkovú chalúpku, chcem trpaslíkov a gýče. 
   
  
   
 Vietor rozfúka oblaky a búrka nepríde, ani len dažďa sa nedočkám, vrátim sa domov a ľahnem si  na posteľ s červeným jablkom v ruke, ležím tam s vystrašeným presoleným srdcom  a keď si odhryznem z jablka, tak mi ako Snehulienke kúsok zabehne, vyhŕknu mi slzy, plačem, mám zablatené nohy, som slaná a vôbec nie taká pekná ako Snehulienka. Mala by som vymyslieť výborný príbeh, no nevymýšľam, pozerám sa na knižku s čiernobielou Kunderovou fotkou na obálke a unesená jeho písaním mám chuť zabaliť to svoje, udusiť svoj vnútorný, veľmi jasný hlas a robiť niečo iné, niečo naoko jednoduchšie a pohodlnejšie. 
 Pozbierala som sa. Zmyla som zo seba špinu, v zapadajúcom slnku som na seba púšťala studenú vodu a moje vnútro revalo ako ranená zver, trvalo to krátko, proces bol osviežujúci a prospešný, vzduch voňal mandľami, otvorila som okno, nechala som slnko, nech mi svieti na chrbát a v tom svetle som sa otriasla ako pes, kvapky vody na kachličkách, kvapky vody na zrkadle. Po niekoľkých hodinách ma naplnil pokoj a viera v seba, mokrá, vlhká a celkom čerstvá som sa tešila z všeličoho, z bzučiacej muchy, ktorá mi robila večer spoločnosť, z rozmarínového alpského sirupu v čaji, z láskavého strýca Rafaela, ktorý mi navrhol, aby som si urobila herbár, aby som sušila v hrubých knihách pestrofarebné kvety a učila sa ich mená. Robím tak. Suším kvety a lupou pozorujem zmeny ich krehkých tiel, suším ich v Kunderových knižkách a keď nás nikto nevidí, šepkám mu do čiernobieleho ucha, že čoskoro, čoskoro pán Kundera, budeme viesť rovnocenný dialóg. 
   
 + Palmenhaus, Viedeň 
 + Hadia rieka, Idaho 
   

