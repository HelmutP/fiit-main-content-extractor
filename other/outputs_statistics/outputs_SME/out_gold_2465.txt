

 Luxus mať v byte alebo v dome vždy dostatok vody nie je výsadou posledných sto rokov, i keď by sme možno aj na Slovensku ešte niekde našli zopár domácnosti, ktoré po vodu chodia s vedrom k studni. Už pred stáročiami však privádzali vodu do  Ríma dodnes zachované slávne rímske aquadukty, a aj niektoré staré civilizácie v Amerike mali nielen dokonalú vodovodnú sieť, ale aj kanalizáciu. 
 Vodovody a ich súčasti však, samozrejme, prešli najmä v poslednom čase výrazným rozvojom. Zvýšila sa technická úroveň privádzania vody do domácnosti, aj technická dokonalosť a  dizajnérske spracovanie súčasnej vodovodnej techniky. 
 Rehabilitácia kohútikov? 
 Po boome v deväťdesiatych rokoch, keď pákové batérie z trhu takmer vytlačili kohútikové batérie, nastáva obdobie ich rehabilitácie. Každý dobrý predajca dnes má v sortimente nielen pákové a kohútikové batérie, ale aj senzorické batérie.  Keď sme sa odborníkov z veľkoobchodu s vodovodnými batériami Viva, spýtali či sú pákové batérie kvalitnejšie ako kohútikové, jednoznačne takúto myšlienku odmietli. Pri výbere sa teda môžete spoľahnúť výlučne na svoj vkus a špecializované predajne majú v ponuke tak veľa dizajnérsky rozmanitých kúskov,  že je skôr problém vybrať si. Ale ako to už býva, aj tu je nejaký ten  háčik... 
 Dôležitá je kvalitná kartuša 
 Kvalita batérie závisí najmä od jedného jej vnútorného prvku - od kartuše. V 70. rokoch minulého storočia bola vyvinutá keramická kartuša a odvtedy sa kvapkajúce vodovody (vraj) stali minulosťou.  Najkvalitnejšie kartuše sa vyrábajú na automatickej montážnej linke a testujú sa tlakom vzduchu. Okrem toho seriózni výrobcovia robia aj testy životnosti. Takto vyrobené batérie majú oveľa dlhšiu záručnú dobu ako štátom predpísané 2 roky - pri tých najkvalitnejších je to aj 7 rokov. 
 Na trhu sú však aj batérie, ktoré v sebe  ukrývajú plastovú kartušu. Ich cena je podstatne nižšia a úmerne s ňou klesá aj kvalita a životnosť batérie. Plast sa tlakom vody môže ľahko deformovať a kartuša sa začne kaziť alebo dokonca úplne prestane fungovať. 
 Ak chcete šetriť vodou 
 Novinkou na trhu je Click kartuša, ktorá pomáha regulovať teplotu teplej vody obmedzením pohybu  páky. Batérie vybavené Click kartušou sľubujú šetrenie vodou. 
 Mnohí majitelia pákových batérií nevedia, že keď otvoria batériu v strednej polohe tečie studená, ale aj teplá voda. Možno aj vy púšťate práve takto vodu mnohokrát denne - len aby ste napríklad niečo opláchli. Vždy pritom používate aj teplú vodu, hoci  ju vlastne nepotrebujte a zbytočne vám stúpajú náklady. Kartuše so systémom Click umožnia také nastavenie, že pri použití páky v strednej polohe bude tiecť iba studená voda. 
 Ak už nechcete zažívať nepríjemné horúce alebo studené šoky pri sprchovaní, zaujímavou možnosťou je kúpa termostatickej batérie.  Termostatické batérie regulujú teplotu a prietok vody, zamedzujú nepríjemnému kolísaniu a zabezpečujú tak vyšší komfort. Technológia týchto batérii umožňuje výrazné šetrenie vody, pretože odpadáva zdĺhavé nadstavovanie teploty vody. Okrem toho špeciálna tryska obmedzuje aj prietok vody. 
 Na čo nesmieme zabudnúť 
 Skôr  ako začnete používať novú batériu, mali by ste kúpiť a namontovať do prívodného potrubia mechanický filter. Tie sa na našom trhu predávajú s plastovým alebo s nerezovým sitkom - nerezové sitko má, samozrejme, dlhšiu životnosť. 
 Pýtate sa, prečo treba filtrovať vodu z verejnej vodovodnej siete? Pretože napriek úpravám obsahuje  množstvo prímesí ako je piesok, hrdza, zlúčeniny horčíka, chlóru, vápnika a tiež rôzne mikroorganizmy. A keď sa dostanú do batérie, môžu poškodiť kartušu. Mnohí predajcovia batérií vám ani neuznajú reklamáciu vodovodnej batérie ak nemáte namontovaný mechanický filter. Filtre pritom nemusia výrazne ovplyvniť  váš rozpočet, pretože ich cena sa pohybuje od 120 korún. 
 Už pri kúpe batérie sa nezabudnite informovať na sieť servisov. Drahšie značky ponúkajú servisné siete po celom území republiky a opravár zvyčajne príde k vám domov a opravu vykoná na mieste. Niektoré predajne však majú iný postup: zákazník prinesie  problematickú batériu naspäť a predajňa vybaví reklamáciu u výrobcu. Treba preto pozorne zvážiť, pre akú batériu sa rozhodnete, ale aj to, kde ju pôjdete kúpiť. 
 Rozmanitosť a nové riešenia 
 Dnes už snáď ani neexistuje oblasť, v ktorej by produkty nepodliehali módnym vplyvom - výrobcovia sa snažia neustále prinášať novinky  a zlepšenia. V oblasti vodovodných batérii v poslednom čase prenikli na náš trh sprchové panely. Tieto sú určené naj-mä do sprchovacích kútov. Sprchový panel okrem tradičného vybavenia pozostávajúceho z batérie, sprchovej hlavice obsahuje aj niekoľko trysiek, ktoré spríjemnia sprchovanie. Tieto panely ocenia najmä tí, ktorým sa do malej  kúpeľne nevošla vaňa i sprcha a rozhodli sa pre sprchovací kút. 
 Ďalším krokom vpred je možnosť vybrať si vaňové a sprchové batérie, ktoré sa montujú pod omietku. Na obložení vidieť iba regulačný ventil a ventil na spúšťanie vody. Účinnosťou sa tieto novinky nelíšia od klasických batérii a sú  zaujímavým riešením. 
 Senzorické alebo automatické vodovodné batérie sú síce na našom trhu dlhší čas, stretávame sa s nimi však väčšinou len v toaletách reštaurácií alebo veľkých nákupných centier. Práve do takýchto zariadení je tento typ batérie ideálny, pretože voda sa spúšťa bez  dotyku rúk. Ich sortiment je u nás obmedzený a v ponuke ich majú nie-ktoré kúpeľňové štúdiá. 
 Lacná batéria sa môže 
 doslova roztrhnúť 
 Trh na Slovensku ponúka vodovodné batérie v nezvyčajne širokom cenovom rozpätí a dizajnérskom vyhotovení. Nie je problém kúpiť batériu doslova za pár  stoviek. Podľa názoru odborníkov sú to však prevažne čínske výrobky, ktoré nemajú žiadne servisné zázemie. Majú aj pomerne krátku životnosť a stojí za to pouvažovať, či sa oplatí kúpiť ich alebo nie. Boli zaregistrované i prípady, že na takejto batérii tlak vody doslova roztrhol celý bok batérie a voda sa  prúdom valila von. A pár ušetrených stoviek možno nestojí za zničenú kúpeľňu či vytopených susedov. 
 Kúpou drahých značkových batérií získate istotu, že predajca vám poskytuje nielen dobré servisné zázemie, ale aj náhradné diely dlho potom, ako prestanú ten váš typ vyrábať. Dôležitý je i  fakt, že každá batéria prejde tlakovou skúškou. Tieto batérie sú však finančne pomerne náročné a preto nad ich kúpou väčšina rodín ani neroz-mýšľa - je nad ich finančné možnosti. 
 Najvýhodnejšia je preto stredná cenová hladina - tu sa spája finančná dostupnosť s dobrou kvalitou. Batérie strednej  cenovej triedy majú keramickú kartušu a mnohé sú vybavené aj Click kartušou. 
 Poslednou kategóriou sú lacné vodovodné batérie, ktoré podľa odborníkov majú nízku životnosť. V prípade závady je často jediným riešením vymeniť celú batériu (v čase trvania záruky), alebo kúpiť novú. 
 Preto  sa pri kúpe v prvom rade informujte, či kartuša vybranej batérie je plastová alebo keramická. Toto kritérium je pri kúpe najdôležitejšie. Spýtajte sa aj na servis a náhradné diely, tieto informácie nie sú zanedbateľné a už pri prvej poruche sa vám zídu. A všimnite si tiež, aké informácie a vedomosti má obsluhujúci personál a  nebojte sa žiadať aj podrobnejšie informácie, lebo iba s nimi si dokážete dobre vybrať z množstva batérií, ktoré ponúka trh. 
 Kde nakupovať 
 Ak ste sa teda už rozhodli pre kúpu batérie a predchádzajúce riadky vás prinútili uvažovať, kam sa vybrať, ponúka sa vám hneď niekoľko možností. 
 Solventnejším  zákazníkom, ktorí chcú nielen kvalitu, ale aj značku a zaujímavý dizajn, najlepšie poslúžia kúpeľňové štúdia. 
 Tým, ktorí hľadajú kvalitu za rozumnú cenu, ponúkajú železiarstva široký sortiment, kde je možnosť kúpiť lacnejšie, ale aj drahšie vodovodné batérie. A personál rovnako ako v  kúpeľňových štúdiách je profesionálne zdatný a schopný dobre poradiť. 
 Najhoršie skúsenosti sme mali v hypermarkete Baumax, kde nám nemal kto poradiť ani po štyridsiatich piatich minútach hľadania. Nakoniec sme sa dozvedeli, že v prípade poruchy treba priniesť batériu naspäť na výmenu. V ponuke tohto hypermarketu boli najlacnejšie, ale aj  drahšie, pravdepodobne kvalitné batérie, chýba tu však kvalifikovaný personál a zákazníkom sa nemá kto venovať.   n 


 Ako funguje termostatická batériaTermostaty používané v batériách sa líšia podľa technologického a materiálového riešenia, ktoré zmiešava vodu. Najrozšírenejším typom sú batérie,  ktoré pracujú na princípe vysoko rozťažného vosku. V závislosti od teploty sa rozťahuje alebo zmršťuje a tak zabraňuje poklesu tlaku a kolísaniu teploty. Ďalším typom sú teplovodivé plochy kapslovitého tesnenia, po ktorých prúdi voda, reagujú zmenou veľkosti a tým udržujú nastavenú teplotu vody. Existujú tiež kvapalinové termostaty,  kde kvapalina mení svoj objem pri zmene teploty. Posledným typom sú bimetalové termostaty, ktorých princípom je rozdielna rozťažnosť dvoch kovov.Prehľad cien batérií
 hypermarket
 železiarstvo
 kúpeľňové štúdio


 umývadlová batéria páková
 279 - 3 990 Sk
 665 - 3 050 Sk
 2 000 - 28 000 Sk


 umývadlová  batéria kohútiková
 2 690 - 2890 Sk
 776 - 1 800 Sk
 2 500 - 12 000 Sk


 vaňová batéria páková
 699 - 5 790 Sk
 963 - 5 300 Sk
 3 000 - 100 000 Sk


 vaňová batéria kohútiková
 3 390 - 3 610 Sk
 1 130 - 3 510 Sk
 3 500 - 60 000 Sk


 sprchová batéria páková
 690 - 2 950 Sk
 965 - 4 030 Sk
 3 000 - 20 000 Sk


 sprchová  batéria kohútiková
 1 690 - 1 890 Sk
 764 - 1 490 Sk
 3 000 - 20 000 Sk


 termostatická batéria vaňová
 5 390 Sk
 3 740 Sk
 od 8 733 Sk


 termostatická batéria sprchová
 2 150 - 3 290 Sk
 2 690 Sk
 od 7 500 Sk


 sprchový panel
 4 490 - 9 990 Sk
  
 od 33 000 Sk




