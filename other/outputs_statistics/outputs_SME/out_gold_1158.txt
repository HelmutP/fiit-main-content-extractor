

   
 
 
O chvíľu prišiel. Vo dverách stál vysoký chudý chlapec. Pozrela som do chodby. "Kde je Andy?" „Čo ma nepoznáte?" zamutoval výrastok. Na prvý pohľad veru nie. Kedysi viac než guľatý chlapec bol teraz štíhly. Dal si dolu bundu. Viac než štíhly. 
 
 
„Poď sa najesť," vravím mu. Odmieta, vraj jedol doma. Chalani sa zavrú do izby. Naložím na tanier koláčiky a idem za nimi. Položím pečivo pred Andyho v očakávaní žiariacich očí a bleskového útoku naň. Nič. „Držíš diétu?" „Veru tak," odpovedá. „Nepreháňaš to trochu?" narážam na jeho vychrtnutosť. „Nie, teraz sa cítim šťastný, ale ešte by som chcel trochu zhodiť. Začal som viac športovať," hrdo sa chváli chlapec. Spomenula som si, aké mal problémy kvôli hmotnosti. Trápil sa on, trápili sa jeho rodičia. Chlapec držal rôzne diéty, chodil do fitka, ale nič. Darmo však mama denne chystala šaláty a zdravú stravu! Chlapec si v školskom bufete za vreckové nakupoval keksíky, bagety a sladké pitivo. Alebo vymieňal s deckami mliečne desiaty z automatu za sladkosti. Keď bol niekedy u nás, nakúpil si chipsy, cukríky a kolu a keď odchádzal, s obavami sa pýtal, či ho nebonznem...
 
 
Zrazu aké zázračné schudnutie! A za krátky čas, veď naposledy bol u nás na jeseň a vtedy sa dogúľal. Bola som veľmi zvedavá, čo za diétu chlapec má. V čase povianočnom, keď sa určite nebudem môcť zapnúť do gatí, sa mi zíde.  
 
 
Nebolo sa treba pýtať. Na vysvetlenie mi stačil jeden pohyb a to, čo nasledovalo, len potvrdilo moju domnienku. 
 
 
Andy sa načiahol za jedným koláčikom. Na ruke mal náramok, vlastne skôr šnúrku. Červenú. Veľmi váhavo si z koláčika odhryzol a žul. Potom vstal a šiel do kuchyne. Tiekla voda, Andy sa napil, vrátil sa do izby a pochválil moje cukrárske umenie. 
 
 
Odišla som do kuchyne nakuknúť do smetného koša. Bol tam. Rozžutý vypľutý koláčik a aj ten nedojedený kúsok. Pekne rozmrvený, aby ho náhodou nebolo na prvý pohľad poznať. 
 
 
Test č. 1 - polievka. Chlapci sedia za stolom. Môj sa kŕmi, Andy pravdepodobne počíta mastné polievkové očká ako kedysi Ježiš. Len škoda, že mi aj tie peniažky nenechá. Namiesto nich nechá polievku, z ktorej raz ochutnal. „Nehnevajte sa, teta, ja takúto polievku nemám rád. Ale je dobrá, naozaj." To ti tak verím, ty vyziablina, veď minule si si dal tri taniere! 
 
 
Test č. 2 - Andyho obľúbené jedlo. Andy sa v ňom prple ako sliepka v hnoji. „Je to to horúce," znie argument. Asi máme s mojim mladým plechové papule, lebo my sme už dojedli, ale Andyho stále páli. Rozhodnem sa mu to uľahčiť a odchádzam od stola aj z kuchyne. Keď sa vrátim, Andy má zjedené. Kuk do smetiaka - nič. Pod stolom sa šťastne zalizuje pes. Vezmem nabok potomka a sondujem, kto to dojedol. Zaprisaháva sa, že Andy, ale ja som mama, vidím mu až do žalúdka a vidím, že v ňom leží Andyho šalát a jeho rezeň je isto - iste v žalúdku toho psa. Syn len mlčky prikývne a šepne „ale ma neprezraď, lebo by som bol zradca!" 
 
 
Test č. 3 - sladkosti. Ideme sa prejsť do mesta. Cieľ, o ktorom chlapci nevedia, je cukráreň. V čokoládovni je výnimočne prázdno. Môj teenager nestíha prehĺtať slinky. Sedíme, vyberáme z veľkého množstva dobrej (a aj poriadne drahej) čokoládky. Našu objednávku zaklincuje Andy. „Čaj." 
 
 
Vychvaľujem dobrú sladkú maškrtu a Andy sa lačne díva. Naprázdno prehĺta. Pýtam sa ho na jeho diétu. Vymýšľa všetko možné, ako nejedáva po šiestej, po obede už len ovocie a zeleninu, ako veľa športuje, behá, posiluje a ako rýchlo vyrástol... Aj sám vidí, že mu neverím. Spod rukáva mu vytiahnem červenú šnúrku. Nič sa nepýtam. „No dobre, som pro-ana," rezignuje. 
 
 
Objednávam mu čokoládu. Namáča do nej lyžičku, slastne ju oblizuje. Po chvíli začne hovoriť. O tom, ako sa mu v škole všetci smiali. Ako nemal kamarátov. O kamarátkach ani nehovoriac. O tom, ako mu liezli na nervy tie večné diéty, do ktorých ho rodičia tlačili, lebo aj keď schudol, bolo to pre nich málo. O tom, ako videl na ich tvárach nechuť a sklamanie, keď sa najedol. O tom, ako mu zaplatili fitko a tréningy, ale jeho to nebavilo. Nemal tam s kým chodiť, nemal s kým behať, nikto ho nepodporil, namiesto toho počúval, ako zase zlyhal, lebo nedodržal diétu. Prasa, guľa, tučko bombička, tučibomba, megero, tlstý Andy, tukoš, tukan... Až našiel na internete pro-ana stránky. Boli to síce stránky dievčenské a tie dievčatá - kostry sa mu vôbec nepáčili, ale - Andy našiel, čo potreboval. Baby sa tam podporovali, držali spolu, ak mala niektorá „krízu", ťahali ju z nej. Andy našiel spriaznené duše. Chcel iba schudnúť. Zapojil sa do diskusií a zrazu mal motiváciu. Vydržať, aby nesklamal nielen seba, ale aj tých, ktorí ho podporovali. Zdalo sa mu, že im na ňom záleží. Každé kilo dole oceňovali a uznanlivo mu gratulovali. Bol hviezda, lebo chlapcov anorektikov je málo. Zrazu sa s ním chceli dievčatá stretávať. A nielen tie z pro-ana fóra. Samozrejme, potom už aj spolužiaci sa pozerali naňho inak. Rodičia boli šťastní, že ich syn konečne začal na sebe pracovať. A Andy chudol. Chudol a chudol. Jedol čoraz menej, až prestal jesť takmer úplne. Jedno jedlo denne, tak znie jeho heslo. Jedno jablko, alebo jeden jogurt, alebo jeden pomaranč....a do toho veľa cvičenia a pitia. Keď sa nedá inak a musí niečo zjesť - na návšteve alebo v reštaurácii, pichne prsty do hrdla a obsah žalúdka skončí v záchodovej mise. Niekedy mu býva až zle. Našťastie, pro-ana stránky obsahujú návody na prekonanie všetkého....  Len nie na návrat späť.
 
 
Andy hovoril dlho, veľmi dlho. Slová prúdili ako rozvodnená rieka. Môj syn vypliešťal oči a nechápal, ako môže niekto dobrovoľne hladovať.  A mne bolo Andyho nevýslovne ľúto. Objala som ho, pohladila po strapatej hlave a on sa rozplakal. 
 
 
Sľúbil, že sa prizná rodičom.  Vie, že sám to nezvládne.  Vie, že sa musí ísť liečiť. 
 
 
Najdôležitejšie však je, že Andy chce. 
 

