
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Serbák
                                        &gt;
                Fotoreportáže
                     
                 Florencia 

        
            
                                    13.6.2010
            o
            14:40
                        (upravené
                13.6.2010
                o
                16:23)
                        |
            Karma článku:
                9.90
            |
            Prečítané 
            2884-krát
                    
         
     
         
             

                 
                    Toskánska metropola Florencia prekypuje umením a architektúrou, je jedným z najkrajších miest sveta. Láka čarom svojich spletitých uličiek i zákutí, námestíčkami, schodištiami a fontánami. Je kolískou renesancie a talianskych klasikov, pôsobili tu Dante, Machiavelli, Michelangelo, Galileo i Boccaccio. Nájdeme tu vrcholné výtvarné diela najslávnejších umelcov, ktorým dominuje socha Dávida od Michalangela.
                 

                 V piatok 4. júna sme prišli do Florencie hneď ráno, aby sme do večera stihli uzrieť čo najviac z tejto "Mekky" umenia. Zaparkovali sme v podzemných garážach pri železničnej stanici a namierili si to priamo do centra na námestie Plazza della Signoria k Dávidovi. Trhovisko sa tiahne od garáži až po baziliku San Lorenzo, ktorú sme spoznali podľa neomietnutej fasády. Je to farský kostol rodiny Medici, ktorá vládla Florencii takmer 300 rokov a je tu pochovaných vyše 50 jej členov. Vonkajšia fasáda tohto kostola zo 16. storočia sa mala obkladať bielym mramorom podľa návrhu Michalangela, no projekt sa nezrealizoval.      Po príchode na Plazza della Signoria ma ohromilo množstvo sôch na tomto námestí. Pred radnicou Palazzo Vecchio s 95-metrovou vežou Arnolfo dominuje Michalangelova socha Dávida. V roku 1504 Michalangelo Buonarroti dokončil pre svoje mesto túto 4,34 m vysokú sochu z bieleho mramoru, zobrazujúcu mladého židovského kráľa Dávida vo chvíli, keď sa rozhodol bojovať proti obrovi Goliášovi. V roku 1873 originál sochy premiestnili do galérie dell Accademia a jej miesto zaujala dokonalá kópia sochy z rovnakého mramoru.               Neďaleko Dávida stojí socha Hercules a Casus od Bandinelliho.         V blízkosti Palazzo Vecchio je aj Neptúnová fontána, ktorej hlavným tvorcom bol Bartolomeo Ammannati. Uprostred fontány je biela socha Neptúna a okolo sú traja Titóni.      Pár metrov od priečelia radnice je pod širokými oblúkmi na námestie otvorená, voľne prístupná galéria Loggia della Signoria, známa tiež pod názvom Loggia dei Lanzi. V nej sa nachádza množstvo sôch, z ktorých najznámejšia je Perseus s hlavou medúzy od Celliniho.                  Krátka ulička s vchodom do najslávnejšej florentskej galérie Uffízi vedie k najstaršiemu a najužšiemu mostu ponad rieku Arno Ponte Vecchio. Zlatníci tu sídlia od 16. storočia, kedy vojvoda Ferdinando I. de Medici z mosta vyhnal mäsiarov. Na prízemí mosta sú okrem zlatníkov aj stánky a obchodíky so suvenírmi,  poschodím mosta sú priamo prepojené paláce na oboch brehoch rieky Arno.            Neďaleko za mostom je šikmé námestie s palácom Pitti, v ktorom sa nachádza významná galéria a okrasné záhrady.         Pre nedostatok času sme palác Pitti nenavštívili, ale sme sa presunuli späť na pravý breh rieky Arno na Dómske námestie s očarujúcim Dómom (Duomo Santa Mária sel Fióre). Obdivujúc nádhernú mramorovú fasádu Dómu sme ho obišli okolo a zistili sme, že vstup do Dómu je zdarma, len si treba vystáť rad a hore na kupolu Dómu je treba vystáť ešte dlhší rad.                        Zistiac, že všetko sa vo Florencii za jeden deň vidieť nedá a záujmy máme rôzne, po krátkej porade sme si dohodli zraz pri Dávidovi o siedmej a náhradný pri aute o ôsmej hodine večer. Milan odišiel do galérie Uffízy, kde si vystál jedenapolhodinový rad a ja som sa postavil do radu na 4 lístky pre výstup na kopulu Dómu.      Kupodivo za polhodinu som bol na rade a mohli sme si za 6 eur na osobu vyšľapnúť po schodoch. "Ocenila" to najmä švagriná, ktorá má nejakú fóbiu z výšok a v minulosti sa už dvakrát vrátila spod Sninského kameňa, lebo bolo treba posledných 15 metrov vystúpiť po strmých schodíkoch. Pre mňa, starého turistu, zvyknutého prekonávať aj tisícmetrové stúpania, to bol naopak pôžitok a ten rozhľad zo strechy kupoly stál rozhodne za to. Z vnútornej strany kupoly sú v dvoch výškových úrovniach kruhové galérie. Najprv sa išlo po schodoch do výšky nižšej galérie, prešlo sa do vnútra, obišla sa kupola zvnútra do polovičky obvodu. Nasledoval výstup na strechu kupoly a pri zostupe vedie chodník polovičkou obvodu vrchnej vnútornej galérie a zostup po schodoch dole. Nevýhodou je, že vnútorné galérie chránia turistov pre prípadným pádom 2 metre vysokým, stokrát obchytaným špinavým plexisklom, takže sa z galérií dá fotiť iba strop kupoly.         Zdržali sme sa hore minimálne polhodinu, nafotili desiatky záberov, aj natočili nejaké videosekvencie.                           Naše ďalšie kroky viedli do slávnej Galleria Academie, do ktorej sa chodí hlavne kvôli originálu Dávida, ale aj ďalším Michalangelovým sochám. Po zakúpení vstupeniek po 8 eur a dôkladnej kontrole ako na letisku, sme vstúpili do priestrannej galérie s nedokončenými sochami Michalangela pre honosnú hrobku Júlia II., pápeža z rodu Medici. Michelangelo sa pustil do štyroch dvaatrištvrtemetrových blokov, ktoré mali byť súčasťou námetu otrokov na náhrobku Júlia II. Nemali to byť štyri osobitné postavy, ale časti jednotného celku: driemajúci Mladý obor, Prebúdzajúci sa obor, Atlas, ktorý drží na svojich pleciach celý svet a starý Bradatý obor. Michelangelo pracoval na otrokoch až do jari, kým ho pápež Lev a kardinál Giulio nepožiadali, aby s prácou prestal a vytesal sochy na náhrobky ich otcov. Michelangelo spočiatku odmietal, ale na pápežove naliehanie nakoniec súhlasil a sochy otrokov zostali nedokončené. Na konci oblúkovito zakončenej galérie sa prísne strážený skvie originál Dávida. Za jeho chrbtom je popri stene dlhá polkruhovitá lavička, na ktorej si obdivovatelia tejto jedinečnej Michalangelovej sochy radi posedia a v nemom úžase pozorujú dokonalé vypracované telo z bieleho mramoru. Aj ja som si pohovel vedľa Japonky, ktorá na lavičke zaspala. Škoda, že je v galérii prísny zákaz fotenia, to by boli zábery! Štyria strážcovia Dávida s očami ako ostriež, nerobia nič iné, len pozorujú turistov, či sa nepokúsia fotiť. Pri najmenšej manipulácii s foťákom sa vrhnú k nemu s krikom "nou foto, nou foto" a vlastným telom bránia svoj poklad pred zneuctením. Aj ja som mal foťák zavesený na krku a veľké pokušenie, foťák prešiel pred vstupom do galérie osobitným roentgenom, zrejme či v ňom nemám nejakú výbušninu, alebo čo. Takže fotky z Galleria Academie nebudú.   Ešte nám zvýšila chvíľa času, tak sme si zašli (už sme boli opäť všetci piati spolu) do Múzea vynálezov Leonarda da Vinciho. Sú tam zrealizované takmer všetky da Vinciho náčrty vynálezov od letiaceho stroja, až po bojový voz s rotujúcimi kosami pre kosenie nepriateľov. Tieto 2 vynálezy sú hneď pri vstupe, tie som nafotil, ďalej mi už fotiť nedovolili.         A tak sme sa plný dojmov z tretieho dňa talianského výletu pobrali večer na "našu farmu", kde si náš 51-ročný benjamínok Jaro pred spaním pri nočnom osvetlení zaplával v bazéne, nám starším sa zdalo byť už trocha chladno, tak sme sa zahriali pripraveným farmárovým vínom.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (19)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Drevené kostolíky pod Duklou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Zaujímavosti zo 100-ročných novín
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Čo písali pred 100 rokmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Nádherné a ľudoprázdne Tatry
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Serbák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Serbák
            
         
        serbak.blog.sme.sk (rss)
         
                                     
     
        Dôchodca s pestrou škálou záujmov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    232
                
                
                    Celková karma
                    
                                                8.50
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Cykloturistika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotoreportáže
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ako to vidím ja
                        
                     
                                     
                        
                            Storočné noviny
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martina Rúčková
                                     
                                                                             
                                            Alexandra Mamová
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                             
                                            Michal Májek
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Martin Štunda
                                     
                                                                             
                                            Dominika Sakmárová
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Zuzana Minarovičová
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Palo Lajčiak
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Branislav Skokan
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            geni.sk všetko pre váš rodokmeň
                                     
                                                                             
                                            stránka obce Úbrež
                                     
                                                                             
                                            fotogaléria Drahoša Zajíčka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zimný Šíp.
                     
                                                         
                       Pamätník čs. armády s vojnovým cintorínom na Dukle
                     
                                                         
                       Drevené kostolíky pod Duklou
                     
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Putovanie po slovenských Kalváriách (194) - Klokočov
                     
                                                         
                       Demokracia
                     
                                                         
                       Vianoce na dedine a vianočný jarmok
                     
                                                         
                       Zaujímavosti zo 100-ročných novín
                     
                                                         
                       Daniel
                     
                                                         
                       Od absolútnej nezávislosti k absolútnej straníckosti
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




