

   
  
   
 Twitter je pomerne nová forma mikroblogovania, ktorá sa stáva čoraz populárnejšou aj medzi bežnými ľuďmi (ak vás zaujíma niečo viac o histórií, pozrite Wikipediu). Aby som vám priblížil jeho poslanie, porovnal by som to s extra odľahčenou verziou Facebooku, kde by ste mali priestor na napísanie statusu a pridávanie priateľov, ktorý by tento status mohli vidieť a reagovať naň. Zoberiem to pekne od začiatku. 
   
 Po registrácii na tejto sieti získate svoju profilovú stránku, kde si v jednoduchých nastaveniach nastavíte všetko tak, ako vám pasuje. Princíp spočíva v tzv. "tweetovaní" (doslovne si to môžme preložiť ako štebotanie). Tweetom sa označuje to, s čim sa chcete podeliť so svetom. Systém je podobný ako na Facebooku. Na stránke mate priestor na písanie čohokoľvek, čo vás napadne. Čo považujem ja za výhodu, je obmedzenie dĺžky tweetu na 140 znakov. Môžete si to predstaviť ako "esemeskovanie na sieti". Jednoducho musíte byť stručný a výstižný, aby sa vám to, čo chcete povedať, vošlo do spomínanej dĺžky. Týmto sa vyhnete akémukoľvek spamu a naučíte sa byť výstižný, zaujímavý a kreatívny aby ste získali followerov. 
   
  
 Slovíčkom "follower" sa označuje človek, ktorý môže sledovať vaše tweety a reagovať na nich. Môžu to byť vaši kamaráti, známi alebo úplne neznámi ľudia, ktorých ste zaujali tým, čo píšete. 
   
 Potenciál tejto sociálnej siete môžu využiť hlavne ľudia, ktorý si napr. píšu blog a chcú si spraviť reklamu, prípadne komunikovať s čitateľmi. Možnosti sú obrovské, treba si len nájsť tu správnu cestu ako sa stať zaujímavým. 
   
 Takže teraz by ste už mohli vedieť základy a poznať pojmy tweet, tweetovanie, follower. 
   
 Keď už niekto niečo napíše, a vy by ste na to chceli reagovať, prípadne to zdielať, stretnete sa s pojmom "retweet" a "reply". Funkcia retweet slúži na zdieľanie tweetu, ktorý vyvesil niekto, koho sledujete. Dá sa povedať, že je to podobné ako zdieľanie na Facebooku. Slovíčko reply znamená v preklade odpoveď. Logicky si môžte odvodiť, že touto funkciou môžete pridať reakciu na napr. názor vašeho kamaráta, s ktorým nemusíte súhlasiť. Môžete použiť buď priamo tlačidlo na to určené, alebo manuálne. Odpoveď sa dáva v tvare "@pouzivatel" (po stlačení tlačidla sa to dá automaticky) . Takže ak je môj nick diffside, tak ak by ste mi chceli niečo napísať, alebo ma označiť vo vašom tweete, napíšete správu do ktorej pridáte aj tento tag (označenie) "@diffside".  
   
 Twitter je narozdiel od Facebooku do istej miery prispôsobiteľný. Môžte si nastaviť farby textu, okien, prípadne si vybrať pozadie alebo nahrať vlastné. Takto si ho môžete prispôsobiť presne tak, ako chcete a ako vás vystihuje. 
   
 Keď vás už omrzel Facebook a chcete skúsiť niečo nové, tak skúste Twitter, prípadne follownuť priamo mňa, a možno sa vám zapáči tak, ako množstvu spokojných používateľom po celom svete. Twitter ja zatiaľ dostupný v angličtine, taliančine, španielčine, francúzštine, nemčine a japončine no pripravuje sa preklad aj do iných jazykov, preto netreba vešať hlavu ak nerozumiete anglicky. Ovládanie je intuitívne, prehľadné a jednoduché a všetko je len otázkou zvyku a chuti. 
   
 Tak doztweetovania... :) 
   

