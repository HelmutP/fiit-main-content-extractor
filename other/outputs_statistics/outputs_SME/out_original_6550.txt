
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Kindl
                                        &gt;
                Nezaradené
                     
                 Potrebná studená sprcha pre nadnesené slovenské sebavedomie. 

        
            
                                    15.5.2010
            o
            15:01
                        (upravené
                15.5.2010
                o
                17:32)
                        |
            Karma článku:
                5.03
            |
            Prečítané 
            1553-krát
                    
         
     
         
             

                 
                    Slovensko - Dánsko 0:6! Prekvapenie? Možno pre fanúšika zaslepeného výsledkami našich hráčov ignorujúcich nedostatky v hernom prejave, ktoré boli viac ako evidentné vo všetkých troch našich doterajších zápasoch a naplno sa prejavili počas prvej tretiny proti húževnatým Dánom.
                 

                     Veľmi ma prekvapili reakcie slovenskej verejnosti na hru slovenských hokejistov počas úvodných troch zápasov na majstrovstvách sveta v Nemecku postupne proti Rusku, Bielorusku a Kazachstanu. Napriek tomu, že dva s týchto zápasov sme vyhrali a proti Rusku ustáli relatívne prijateľnú prehru 3:1, väčšina slovenských fanúšikov, z ktorých značná časť pred začiatkom šampoionátu neverila v dobré výkony našich hráčov, ostala omámená dobrými výsledkami bez ohľadu na priebeh zápasov, v ktorých bolo vidieť množstvo chýb v základných herných hokejových činnostiach. Tieto chyby sme dokázali ustáť len vďaka zázračným zákrokom nášho brankára a nemohúcnosti súperových útočníkov.   Tieto nedostatky ostali evidentne bez povšimnutia aj pre slovenských hokejistov, ktorých sebavedomé vyjadrenia pre médiá boli zaslepené výsledkami zápasov a tým, že dokázali otočiť aj zdanlivo stratené stretnutie proti Bielorusom. Podľa môjho názoru tieto všetky aspekty sa prejavili v prvej tretine proti Dánsku, keď naši hráči evidentne podcenili súpera a vykorčuľovali na ľad pod dojmom, že im bude stačiť tréningové tempo na zdolanie teraz už bývalého hokejového trpaslíka. Opak sa stal pravdou a dánski hokejisti vytrestali našich naivných hokejistov za ich ľahkovážny prístup k zápasu šiestimi gólmi v ich sieti v priebehu necelej prvej tretiny a posadili ich späť na zem, kde podľa mňa patria. Tento zápas schladil jednoznačne hlavy našich primadon a ukázal im, že bez srdca a bojovnosti sa zápasy na svetovej scéne hrať nedajú a ani nesmú, pretože nehrajú iba za seba, ale za celý národ, ktorý sleduje ich výkony a sklamanie takýchto rozmerov sa odpúšťa iba ťažko. Na druhej strane myslím, že práve takýto výsledok dokáže vyburcovať našich chlapcov  ku zlepšeným výkonom v ďalšom priebehu turnaja a dosiahnutia výsledkov, s ktorými po tomto výkone počítajú len tí najvernejší fanúšikovia slovenské hokeja.   Týmto sa zároveň snažím vyzvať slovenskú hokejovú verejnosť, aby nezanevrela na našich chlapcov kvôli jednému nevydarenému zápasu spôsobenému nedostatkom skúseností a nerozvážnosti, držala im palce ďalej a pevne dúfala, že si dnešnú príučku vezmú k srdcu a dokážu to pretaviť do výkonu v ich nastávajúcich zápasoch, najbližšie v pondelok o 16:15 SEČ proti Fínsku.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kindl 
                                        
                                            Fantastický gól Tomáša Hertla zosmiešnenie? Smiešne!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kindl 
                                        
                                            23 statočných pre misiu Afrika na svete
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kindl 
                                        
                                            Potrebná studená sprcha pre nadnesené slovenské sebavedomie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kindl 
                                        
                                            Po Rusoch na Bielorusov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kindl 
                                        
                                            Cesta do hlbín duše slovenského futbalového fanúšika
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Kindl
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Kindl
            
         
        kindl.blog.sme.sk (rss)
         
                                     
     
        Nechcem nudiť, pokúšam sa iba záujem vzbudiť.....
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    957
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




