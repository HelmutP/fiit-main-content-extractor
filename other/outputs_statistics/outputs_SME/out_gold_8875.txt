

   
 Približne dva mesiace pred voľbami 
   
 Dedinský rozhlas sa rozozvučí. Pred oznamom hrá vždy zaujímavá muzika. Tentoraz to je jedna z tých piesní, ktorých refrén sa nedá zabudnúť: „Daj si pivo značky Topvar!“ Nasleduje oznam, ktorý približne znie takto: „Vážení spoluobčania, v ten a ten deň sa koná stretnutie občanov na tej a tej akcii. Organizuje ju pán Fico a jeho strana. Zadarmo sa bude podávať guláš a pivo. Všetci ste srdečne pozvaní. Na akciu bude chodiť kyvadlová doprava, ktorá bude zadarmo.“ 
   
 Približne mesiac pred voľbami 
   
 Ten istý scenár, aký bol dva mesiace pred voľbami. 
   
 1. 6. 2010 - Deň detí 
   
 Vonku stále prší. Pomýšľať na dedinskú akciu, ktorá sa pravidelne organizuje v tento deň, je zbytočné. Akcia sa totiž vždy odohráva na dedinskom futbalovom ihrisku, kde je množstvo súťaží, do ktorých sa môžu zapojiť deti z dediny a okolia. Na konci akcie sa vždy rozdávajú ceny. Každé dieťa, bez ohľadu na poradie, dostane ešte balíček so sladkosťami a špekáčik, ktorý si bude môcť opiecť večer pri ohni. V rozhlase žiadny oznam o posunutí akcie na iný deň. Akcia sa nekoná. 
   
 Týždeň pred voľbami 
   
 Celú sobotu svieti slnko, počasie je ideálne. V rozhlase stále nič nehlásia. Akcia sa nekoná. 
   
 Pár dní pred voľbami 
   
 Objavujú sa falošné letáky, kde pán Fico vyzýva ľudí, aby volili SDĽ. Dedinský rozhlas promptne reaguje. Oznam znie asi takto: „Vážení spoluobčania, neverte letákom, ktoré ste aj vy mohli dostať do schránky. Pán Fico tieto letáky určite nepodpísal a ide o podvod.“ Hlásateľka ešte na koniec oznamu dodáva: „Ak naozaj chcete voliť ľavicovú stranu, tak voľte stranu pána Fica“ 
   
 12. 6. 2010 – deň volieb 
   
 Koná sa deň detí. Je krásne počasie, deti sa radujú. Prechádzam sa s mojou malou sestričkou po ihrisku, kde je množstvo súťaží pre deti. Všetci sa usmievajú a zabávajú. Starí posedávajú pri výčape, deti súťažia a pán starosta je na všetkých milý. Vládne priam rodinná atmosféra. Všetci majú v rukách balóniky, ktoré boli aj súčasťou jednej súťažnej disciplíny. Na balónikoch je logo a nápis SMERu. Všetko je v najlepšom poriadku.  
   
 13. 6. 2010 – deň po voľbách – už je vypálené, len to kopnúť do seba 
   
 Sedím na zástavke autobusu, dnes totiž cestujem domov. Zhodou okolností, ako to už na dedinách býva, je zástavka súčasťou terasy pred krčmou. Výsledky volieb sú známe a ja si čilujem spokojne na lavičke, kým nepríde môj autobus. Štamgasti pred krčmou sa rozprávajú.  
   
 Feri: Joži, počul si výsledky v rozhlase? Vyhrali sme.  
 Jožko: Ľudia tu nie sú sprostí. Vždy som to vedel.  
 Feri: Tak, tak... Len nevím, kto dal ten jeden hlas Maďarom. Nebola to stará Dostalová?  
 Jožko: Istotne ona. Vždy to bola stará pi*a maďarská... 
 Feri: A ten hlas tej cigánskej strane kto dal?  
 Jožko: To ti teda nevím. Kok*tskí cigáni, čo sa tlačá do politiky...je*ať ich do hlavy... 
 Feri: Hlavne, že sme vyhrali... 
 Jožko: Tak, tak... 
   
 Svorne si štrngli s poldecákmi čerešne a zapili hnusným pivom.  
   
 Jožko: Dobre sme vypálili... 
 Feri: Tak, tak.. 
   

