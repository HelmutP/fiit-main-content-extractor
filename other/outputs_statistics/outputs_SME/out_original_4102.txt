
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lucia Anďalová
                                        &gt;
                Nezaradené
                     
                 Gymnazisti pomáhali zachraňovať životy 

        
            
                                    7.4.2010
            o
            16:24
                        (upravené
                20.4.2010
                o
                16:55)
                        |
            Karma článku:
                3.10
            |
            Prečítané 
            685-krát
                    
         
     
         
             

                 
                    Je osem hodín ráno. Pred budovou Gymnázia na ulici Dominika Tatarku stojí zdravotnícke vozidlo. Úraz? Omyl. Auto patrí Národnej transfúznej službe SR. Je utorok 23.3. 2010, deň, kedy dostali študenti možnosť spraviť humánne gesto. Heslom akcie je „Darujte krv - zachráňte ľudský život"
                 

                 
Nielen zdravotníci brali celú vec s úsmevomLucia Anďalová, Tomáš Tkáč
   V najväčšej učebni gymnázia sa momentálne nachádza asi 50 potenciálnych darcov, z toho 42 tunajších študentov. Postupne prechádzajú lekárskou prehliadkou. U tých, ktorí vyhovejú, môžu zdravotné sestry prejsť k odberu.  „Vyzerá to na najvyššiu účasť vôbec," hrdo vraví Mgr. Roman Švarc, ktorý podujatie už niekoľkýkrát pomáhal organizovať. „Bolo by tu ešte o pätnásť ľudí viac, ale zúčastnili sa Valentínskej kvapky krvi minulý mesiac. Aj tak dnes asi padne školský rekord."  Prekvapilo aj hojné zastúpenie dievčat. Je ich síce menej ako chlapcov, zato oveľa viac, ako obyčajne.  Lucia zo 4.C je už po odbere. Kým sa osmelila, dlho váhala. „Naozaj som chcela prísť, ale mala som strach. Presvedčil ma až môj priateľ. Teraz som prvá z rodiny, ktorá darovala krv." Na otázku, aké sú jej bezprostredné pocity, odpovedala s úsmevom: „Je to úľava, keď už to mám za sebou. Asi pôjdem aj nabudúce."     O úľave ešte nemôže hovoriť oktávanka Eva. „Som tu od ôsmej... pustila som dvoch ľudí pred seba, lebo sa bojím," priznáva sa. O 9:16 si sadá na polohovateľné lôžko. Dostáva na rameno sťahujúcu pásku. Zdravotná sestra jej opatrne zaviedla ihlu a podala loptičku na stláčanie. O šesť minút je po všetkom. „Keď ma pichli, trochu to štípalo, ale teraz mi je fajn. Mám zo seba radosť," hodnotí Eva svoj výkon.  Rovnako dobre zostala naladená Soňa z hojne zastúpenej 4.B. „Nerobila som to prvý raz a plánujem darovať krv ešte veľakrát. Je to dobrý skutok, jednoduchý krok, ktorým pomôžem nejakému človeku." Existenciu mobilnej transfúznej jednotky plne oceňuje. „Hocikto nepríde sám do nemocnice. Tu v škole je to pre nás príjemnejšie a odhodlajú sa aj noví darcovia."   Medzi prítomnými neboli len gymnazisti. Našli sa medzi nimi dvaja zamestnanci školy a dvanásť ďalších dobrovoľníkov, napríklad Kežmarčan Marek zo Strednej priemyselnej školy v Poprade.  „Je to pre mňa prvý raz. Poriadne som sa bál, ale keď som videl, ako to zvládajú ostatní, strach zo mňa opadol a cítim sa dobre," vraví pritláčajúc si gázu na vpich.     Je 11:45 a zdravotnícky personál sa pomaly balí. Získali krv od 43 darcov z 56 prítomných, čo je neobvyklý úspech a školský rekord. Potvrdil to aj MUDr. Pavel Repovský, ktorý sa starostlivo pristavil pri každom, komu prišlo nevoľno, alebo sa zľakol. Pochválil študentov za ich odvahu. „Všetci boli veľmi statoční a vydržali, aj keď im to zo začiatku nebolo príjemné, a ani nik neodpadol."  Jeho rukami prešlo už veľa študentov. „Robíme to tretí rok. Ročne zvládneme 50 výjazdov po školách, dedinách a organizáciách. Odkedy pôsobíme, počet darcov sa zdvojnásobil na päť tisíc a tento rok sme si dali predsavzatie zvýšiť číslo na 6 - 7 tisíc."   Na otázku, čo sa bude diať s krvnými konzervami ďalej, odpovedal takto: „Na spracovanie krvi máme vlastné zariadenie. Z každej konzervy sa oddelia červené krvinky, plazma a biele krvinky s trombocytmi. Každé sa dajú inak použiť, teda jeden darca môže pomôcť hneď niekoľkým rôznym ľuďom. Červenými krvinkami sa nahrádza krvná strata pri úraze alebo operácii. Z plazmy sa, napríklad, získavajú lieky pre pacientov s poruchami zrážanlivosti krvi."     Nie je ťažké splniť kritériá pre darcu. Treba dosiahnuť vek 18 rokov, vážiť aspoň 50kg a byť minimálne mesiac zdravý. Krv sa nesmie darovať pri užívaní liekov, po piercingu, operácii, tetovaní, drastickej diéte, u žien počas menštruácie, tehotenstva a dojčenia. Z celkových piatich litrov najvzácnejšej tekutiny sa človeku odoberie len 400 ml. Túto stratu si telo čoskoro nahradí. V medicíne je však krv nenahraditeľná.  Za svoj vklad do krvnej banky si darcovia odniesli nielen stravný lístok a vitamíny, ale aj vedomie, že možno práve pomohli niekomu zachrániť život. Tento dobrý skutok si za múrmi Gymnázia budú môcť zopakovať v októbri. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Zombie medzi nami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Otvorený list MUDr. Štefanovi Paulovovi - Pi*a z h*ven sú bez viny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Nové polyfunkčné budovy v Poprade. Pre koho?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Protidrogová výchova na školách – vyhodené peniaze!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Anďalová 
                                        
                                            Zlatý život študentský 5: Ak nevytŕčaš z davu správne, si nepriateľ
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lucia Anďalová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lucia Anďalová
            
         
        andalova.blog.sme.sk (rss)
         
                                     
     
        Študujem žurnalistiku ale už sa zotavujem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    21
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1680
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Študentský život
                        
                     
                                     
                        
                            Poprad
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            WEBJOURNAL
                                     
                                                                             
                                            Prečo je štát zlý hospodár?
                                     
                                                                             
                                            Apokalypsa v denníku Nový čas
                                     
                                                                             
                                            Láska v čase internetu (toto zabolelo)
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Farby strachu (jediná slovenská hororová antológia)
                                     
                                                                             
                                            Eastlake: Hájili sme hrad
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            P*ča z h*ven
                                     
                                                                             
                                            Ľahká múza
                                     
                                                                             
                                            Theatre des vampires
                                     
                                                                             
                                            Sunn o)))
                                     
                                                                             
                                            Psyclon Nine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            judytha
                                     
                                                                             
                                            cookingchinchillas
                                     
                                                                             
                                            afinabul
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cyanide &amp; Happiness :)
                                     
                                                                             
                                            www.dolezite.sk
                                     
                                                                             
                                            www.slovakgothiccommunity.sk
                                     
                                                                             
                                            www.darklyrics.com
                                     
                                                                             
                                            www.cracked.com
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Otvorený list MUDr. Štefanovi Paulovovi - Pi*a z h*ven sú bez viny
                     
                                                         
                       Nové polyfunkčné budovy v Poprade. Pre koho?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




