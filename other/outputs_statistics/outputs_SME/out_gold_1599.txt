

   
 Reklamná kampaň bola viac než pôsobivá. Vtipná televízna reklama priam ľudí nabádala, aby vzali peniažky z výplaty a šli nakupovať. Hoci dnešná doba nákupom veľmi nepraje, ani ja som nezaváhala a vybrala sa na nákup do najbližšej predajne. 
   
 Do súťaže som sa rozhodla zapojiť prostredníctvom SMS a nevypisovať svoje údaje na pokladničný blok. Zhodou okolností som pri sebe nemala pero a čas ma poriadne tlačil, tak prečo by som neposlala SMS? Reklamný leták neupozorňoval na nič také, čo by ma prinútilo myslieť si, že som v časovej tiesni nekonala múdro, keď som nechcela v preplnenej predajni zháňať pero. Cena spätnej SMS bola 0,398 € . Bežného človeka by ani vo sne nenapadlo, že okrem tých pár centov prichádza aj o podstatnú šancu niečo vyhrať. 
   
 Veľké prekvapenie 
   
 Niekoľko dní po skončení súťaže som počas obedňajšej prestávky v práci surfovala po internete. Zo zvedavosti som do vyhľadávača zadala heslo „Kolesománia" a čakala, čo objavím. Bola som zvedavá, či už náhodou nenájdem zverejnené výsledky. Dočítala som sa, že na tie si ešte počkám do konca augusta. V poriadku, na tom nič zvláštneho nie je. Však sú i také súťaže, kde sa na výsledky čaká oveľa dlhšie. Mňa však upútala iná zvláštnosť. 
   
 V pravidlách stálo, že v prvom kole sa uskutoční žrebovanie výhier z pokladničných dokladov a z doručených SMS sa bude žrebovať až v druhom kole. Prvé kolo sľubovalo 3656 cien. Hralo sa o: 
 52 skútrov 
 102 cestovných kufrov 
 230 horských bicyklov 
 291 poukazov na nákup pohonných hmôt 
 2981 hudobných CD 
   
 Čítala som ďalej, pretože ma zaujímalo, o aké ceny budem súťažiť ja. Veď to vyzeralo tak, že v prvom kole rozdali takmer všetky ceny! Utešovala som sa faktom, že budem mať o niečo väčšiu šancu vyhrať auto, keďže ich nerozdali v prvom kole. Také autíčko by sa mi veru zišlo. No márne som sa tešila.  Aj druhé kolo robilo rozdiely medzi súťažiacimi. Teraz budem citovať časť zo šiesteho bodu pravidiel: 
 
 „Z pokladničných dokladov sa v druhom kole uskutoční žrebovanie o 5 x osobný automobil Volkswagen Polo a z doručených sms sa budú žrebovať výhry v nasledovnom súhrnnom počte: 
   
 - 1 x osobný automobil Volkswagen Polo 
 - 2 x skúter ADAMOTO Fighter 50 
 - 4 x cestovný kufor SAMSONITE 
 - 4 x horský bicykel CTM Axon 
 - 9 x poukaz na nákup PHM - Slovnaft 
 - 19 x hudobné CD 
   
 Kompletný zoznam výhercov bude zverejnený na stránke www.coop.sk od 31.8.2009." 
 
 Povedala by som, že pravdepodobnosť výhry toho, kto poslal SMS, je viac než mizerná.  Ako SMS súťažiaca hrám len o nejakých 39 cien. Táto číslica (v porovnaní s číslom 3661) vyzerá ako nemiestny žart a na svetlo sa vynára niekoľko otázok. 
   
 Ako je možné, že v dnešnej modernej dobe niekto uprednostní papierový box pred modernou elektronickou formou súťaženia? 
   
 Kto mi vysvetlí fakt, že ten, komu je ľúto vyhodiť pár centov za SMS, má väčšie šance vyhrať?  
   
 Ak už mala súťaž deliť súťažiacich na dve skupiny, prečo neboli výhry rozdelené na polovicu, alebo nejakým primeraným dielom?  
   
 Keď už COOP Jednota Slovensko vyhadzuje ročne toľké peniaze na výdaj letákov, prečo sa súťažiaci a verní zákazníci ani v jednom z nich nedozvedeli, že posielanie SMS je pre nich nevýhodné? Chceli na našich SMS zarobiť, lebo sa im 15€ za nákup videlo primálo? 
   
 Dnes má mobilný telefón takmer každý druhý človek. No povedzte, koľko ľudí má doma internet? 
 Pevne verím, že nie som jediná, kto si v tejto chvíli kladie podobné alebo rovnaké otázky. Na svete máme ďalšiu súťaž, ktorá nebola poriadne zorganizovaná a opäť sme sa nechali dobehnúť. Ostáva len dúfať, že sa nič podobné v budúcnosti nezopakuje, alebo sa pre istotu vyhýbať podobným podozrivým súťažiam. Jedno je však isté. Ak sa mi v blízkej budúcnosti zažiada čiastočne vyprázdniť obsah svojej peňaženky, v obchodnej sieti COOP Jednota to určite nebude. 
   

