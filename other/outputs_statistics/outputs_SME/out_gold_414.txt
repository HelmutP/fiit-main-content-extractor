

       Turecké uvítanie     Turecko sa ukázalo ako územie protikladov. 
 
     Už na letisku v Antalyi sme dostali teplotnú facku. Cesta autobusom k moru netrvala dlho. Moje občasné driemoty starostlivo preberal sprievodca. Každú vetu začínal slovami:“Liebe Gäste!“, samozrejme s tureckým prízvukom. Po celoročnej práci doma sme konečne zacítili ten zvláštny slaný vzduch zmiešaný s vôňou typickej tureckej kuchyne. Pred tým pusté pobrežie je dnes husto posiate novými, modernými hotelmi. 
 
 
      
 
 
Side     Pár dní sladkého polyhovania bohate stačilo. Zastávam názor, že každú krajinu treba spoznať v rámci možností.     Požičať si v Turecku auto nie je veľmi bezpečné. Istejšie je nasadnúť do dolmusu. Vo voľnom preklade to znamená preplnený, čo zväčša naozaj je.      Všetci turisti mali strach, pretože v čase nášho pobytu vybuchol dolmus s britskými turistami. Vyzbrojení myšlienkou(moja rodina a ja): čo sa má stať, stane sa, nasadneme k prešedivelému šoférovi. Po čase zisťujeme, že ide úplne iným smerom ako sme chceli. Nechápem síce ako, ale priviezol nás živých, len mierne „uvarených“ do turistického mestečka Side. Nie je to žiadne veľkomesto, veď má len 25 tisíc obyvateľov. Množstvo obchodíkov, kaviarní a dovolenkárov zapĺňalo ulicu, na ktorej koniec som nedovidela. Pred každým obchodom stáli tzv. „naháňači“. Musím povedať, že nepomáhalo ani veľmi priame vysvetľovanie, že o nič nemáme záujem. Ale pozvanie na dobrý turecký čaj či kávu sa neodmieta. Obchodník, ktorý neovláda angličtinu či nemčinu, nič nepredá. Medzi najkvalitnejšie tovary bezpochyby patria: zlaté šperky, kožené výrobky, čaje, sladkosti plné medu a orieškov a koberce. Kobercárne sú na každom kroku, oplatí sa vidieť, ako turecké ženy ručne tkajú koberce rôznorodých vzorov.               
 
 
Ochranné oko     Turci veria v ochranné oko, ktoré má čarovnú moc chrániť pred všetkým zlým. Nájdete ho všade, nad dverami obchodov, na palmách, v autách i na šperkoch. A odteraz mu trochu veríme aj my. Denne sa nám pripomína aj nad dverami mojej izby.      
 
 
Pamukkale     Byť v Turecku a nezažiť Pamukkale, je ako v Paríži nevidieť Eiffelovku. Viac ako 100 metrov vysoké vápencové terasy  v slnku úžasne žiaria. Turci mu dali nežnú prezývku „bavlnený zámok“. Prechádzať sa v tyrkysovo modrej teplej vode (35°C) a vnímať tú zvláštnu krásu bolo pre mňa zážitkom. Zážitkom, na ktorý sa len tak ľahko nezabúda, pretože Pamukkale je malým tureckým rajom na zemi.       
 
 
Antické návraty 
 
 
     Do Side sme sa ešte vrátili. Toto mestečko je jedinečné svojimi archeologickými pamiatkami. Je spojením moderny a antiky. Nádherné antické divadlo hrdo pozerá na pokojné more. A kto nevidel antické divadlo v Aspendos alebo antické mesto v Perges, určite by sa mal na tureckú rivieru vrátiť. Antické monumenty vnášajú do moderného Turecka kúsok histórie. 
 
 
        
 
 
Manavgatské vodopády 
 
 
     Na Turecku je zaujímavé nielen slané more, ale aj sladké vodopády v Manavgate či Kursunlu. V turečtine sa nazývajú Şelâlesi. Rieka Manavgat, na ktorej sa tieto vodopády nachádzajú, pramení v pohorí Taurus. Z terasy sme sledovali aj  skáčúcich odvážlivcov z najvyššieho bodu vodopádov. 
 
 
       
 
 
Turecká lýra     Viem, že nič netrvá večne, no tieto fantastické chvíle si budem určite pamätať. Nielen z fotiek, ale aj pri pohľade na mapu sveta na mojom pracovnom stole. Vždy zablúdim očami k Turecku.      Posledný deň som poverčivo hodila tureckú lýru do mora. Znamená to, že sa ešte vrátim. Kedy? To je už otázka do budúcnosti. 
 


