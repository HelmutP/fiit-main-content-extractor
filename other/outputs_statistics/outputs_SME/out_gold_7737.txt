

 Leukémia. Druhé slovo, ktoré nenávidím. Zobrala mi brata.Tak mladý, túžiaci po živote a statočný.Je to ešte len mesiac čo tu nie je , a je tu také strašné ticho. Kde sú časy keď bolo plno v dome? 
 Verte ľudia, že aj hádky detí sa dajú brať pozitívne. Z vlastnej skúsenosti vravím keď sa deti pohádajú a nezasiahnu do toho rodičia, zmieria sa sami. Nekričte na deti, berte to z pozitívnej stránky- i keď sa hádajú, sú to deti, ktoré máte a o ktoré sa dá tak ľahko prísť. Berte život tak, že si užívajte každý deň, berte deti na výlety, veď nikdy neviete dokedy tu budú prípadne dokedy tu budete vy. Ak sa im náhodou/prípadne vám niečo stane, aby ste si to nemuseli vyčítať. 
 Vo veľa veciach sa snažím nájsť pozitívum. Kde však nájdem pozitívum v tichu? Nikde? Niekedy je dobré mať ticho a svoje myšlienky, no nie na stálo. 
 Známy citát vraví: V tichu človek vo svojom vnútri stretne seba takého, aký je pred Bohom. (Pau del Rosso)- je to pravda. Ak je ticho, ostal len človek a jeho myšlienky, mnohé otázky na ktoré niet odpoveď a mnoho viet ktoré nie je komu povedať. 
 No našla som aspoň jedno pozitívum- človek má čas rozmýšľať. V Dnešnom uponáhlanom svete to dnes nik nestíha, Zastavte sa aj vy na chvíľku vo svojom uponáhlanom dni, skúste chvíľku porozmýšlať či by ste niekomu nechceli niečo povedať, niekomu poďakovať. Totiž zajtra už môže byť neskoro. 
 autorka: Nikoleta Zbrášová 

