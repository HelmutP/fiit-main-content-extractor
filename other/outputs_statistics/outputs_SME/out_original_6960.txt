
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Herman
                                        &gt;
                Nezaradené
                     
                 Zvončekom už odzvonilo 

        
            
                                    21.5.2010
            o
            14:38
                        |
            Karma článku:
                6.51
            |
            Prečítané 
            1565-krát
                    
         
     
         
             

                 
                    V bardejovskej bazilike už zvončekom naozaj odzvonilo. Nemyslím tým, čo stoja pred bazilikou , tým naozaj odzvonilo. Veľký Urban a menší Ján sú už len ako turistická atrakcia a na veži bijú srdcia nových bratov Urbana a Jána.
                 

                 
  
   Mám na mysli zvončeky, do ktorých sa vyberajú milodary. Dlhé roky, čo pamätám, sa milodary vyberali do klasických a pre cirkevný chrám primerane dôstojných, bordovo zamatových zvončekov so zlatou obrubou, nasadených na drevených rúčkach, aby vyberači mohli pohodlne vyberať aj od ľudí sediacich v prostriedku lavíc.            Pred pár týždňami sa to ale zmenilo. A s poľutovaním konštatujem, že k horšiemu a podľa mňa aj k nedôstojnému vyberaniu milodarov.            V chráme to len zašumelo, keď vyberači nastúpili na „plac" s nevkusnými plastovými lavórikmi, ktoré boli síce takmer vo vatikánskych farbách, ale skôr by sa hodili do kuchyne na rôzne šaláty, ako na výber milodarov. Nezanedbateľný bol aj zvuk vhadzovaných mincí, ktorý tvar plastu a jeho tvrdosť ešte viac znásobovali, a tak dosť nepríjemne rušili priebeh omše. Zdá sa že si túto skutočnosť uvedomili aj správcovia farnosti a nevydarené lavóriky nahradili iným kuchynským „nádobíčkom" , košíčkami na pečivo, z prútia, presne takými, aké sa predávajú  na jarmokoch alebo v ÚĽUV-e. Košíčky nadôvažok vystlali látkou, ktorá trocha tlmí zvuk mincí.            Neviem, prečo sa zaviedol takýto zlepšovák. Ak by staré zvončeky boli potrhané a nevyhovujúce, chápal by som to ako dočasné riešenie, kým sa zabezpečia nové, ale zničené určite neboli.   Preto mi vychádza len jedna možnosť. Je tu niečí, alebo niekoho zámer psychologicky vplývať na veriacich , aby sami videli ,kto a koľko milodarov oferuje na farnosť , aby sa tým hojnosť milodarov zvýšila. No posúďte sami, hodíte do košíčka so samými  „zlatými" mincami len medenú, alebo nebodaj neplatnú alebo zahraničnú mincu? Veď keď vás uvidia ,čo si o vás pomyslia? Bolo by načase aj tu, na východe Slovenska, zaviesť spôsob vyberania milodarov tak, aby sa nerušil priebeh omše. Stačí umiestniť pri  vstupe do kostola pokladničku a vec je jednoducho vyriešená. Nie len veriaci pri bohoslužbách, ale aj iní návštevníci baziliky by tak kedykoľvek mohli prispieť na prevádzku kostola, či iné potrebné zabezpečenie činnosti farnosti, na ktoré sa prispieva z milodarov.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            V Kauflande
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            Šče ne vmerla Ukrajiny ni slava, ni voľa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            K téme, na rovinu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            Ešte nie sú online
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            Nevyužitá šanca
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Herman
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Herman
            
         
        herman.blog.sme.sk (rss)
         
                                     
     
        Prakticky - realista,teoreticky -idealista v skutočnosti celkom obyčajný jedinec.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    170
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1616
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            žena v najlepších rokoch
                                     
                                                                             
                                            kňaz z Lunikova
                                     
                                                                             
                                            milovník gruzínskeho čaju
                                     
                                                                             
                                            lekár-špecialista
                                     
                                                                             
                                            patavedec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            zive
                                     
                                                                             
                                            slobodná encyklopédia
                                     
                                                                             
                                            počítače.sme.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




