
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Dérer
                                        &gt;
                Nezaradené
                     
                 Ako Boh s Einsteinom hádžu kockami (fyzika verzus náboženstvo) 

        
            
                                    20.1.2010
            o
            22:35
                        (upravené
                20.1.2010
                o
                22:45)
                        |
            Karma článku:
                11.78
            |
            Prečítané 
            3715-krát
                    
         
     
         
             

                 
                    Dobre si to rozmyslite. Ak kliknete na tento článok, rátajte s dávkou fyziky. Možno filozofie. A možno teológie. A to všetko od človeka, ktorý nie je profesionálny fyzik, filozof ani teológ. Na druhej strane, ak mi dáte šancu, môžete sa zúčastniť na jednej nie celkom banálnej úvahe.
                 

                 Chvíľu to vyzeralo, že svet je v poriadku. Ale čím viac bol v poriadku pre jedných, tým menej sa pozdával druhým.    Asi tak sto rokov dozadu mali fyzici dojem, že zákony prírody poznajú a ich veda je aspoň v hrubých rysoch uzavretá; stačí už len riešiť detaily. A kresťanskí filozofi mali problém. Nielen s Nietzschem, ktorý vtedy písal o smrti Boha, ale i s fyzikou. Ak je totiž celý beh sveta spleťou príčin a ich kauzálnych dôsledkov, potom to, čo nazývame náhodou, je len naša neznalosť. Nič nie je skutočnou náhodou, za všetkým sa skrýva (fyzikálna) zákonitosť, a keby sme ju poznali, vieme presne, čo sa kedy a kde stane. Počiatočné parametre nášho vesmíru by predurčovali vývoj na ľubovoľne dlhý čas dopredu. Každý detail, nielen zrážka hviezd, ale i každý pád suchého listu zo stromu, stretnutie dvoch mravcov, i dúha po daždi by boli presne naprogramované, predurčené vďaka neľútostnému determinizmu. Každý úsmev, nedopísaný a zahodený milostný list, splynutie dvoch tiel i boľavý zub. Deprimujúce.       Nik síce nedokázal veštiť budúcnosť, pretože nik nepoznal polohové súradnice a hybnosti všetkých častíc vesmírnej hmoty v okamihu po Big Bangu (ani kedykoľvek neskôr), ale samotné vedomie, že budúcnosť je predurčená, muselo pôsobiť frustrujúco. Veriaci fyzik riešil dilemu: ak náhodou existuje Boh, naozaj sa na nás od stvorenia sveta len nečinne díva, tak, ako sa my dívame na rybičky v akváriu? Načo potom stvoril svet, tak nudne predvídateľný? A načo sa veriaci modlia, ak Boh nezasahuje?        Naša predstavivosť zrejme nestačí na vysvetlenie Božích motívov a plánov, ale z  rýdzo ľudského hľadiska to skutočne vyzerá nelogicky: ak je Boh všemohúci a vševedúci, všetky počiatočné parametre nastavil a pozná, a tak pri svojej neobmedzenej predstavivosti vie presne, čo sa odohrá trebárs v roku 2014 o svätojánskej polnoci na rohu Palm Street a Garden Avenue, v chotári Hornej Marikovej, či kdekoľvek inde, kedykoľvek inokedy. Načo by sa s takým svetom namáhal? Všetko vie vopred, nič ho nemôže prekvapiť.       Pár mudrcov sa pokúšalo argumentovať slobodnou vôľou jednotlivcov, ale skeptici namietali, že i to môže byť ilúzia – čo ak je každé naše rozhodnutie, každé pohnutie mysle už zahrnuté v pôvodnom programe svorenia sveta? Potom si slobodu našich rozhodnutí len namýšľame.       Napokon to boli fyzici, ktorí zmenili chápanie sveta a nás v ňom. Kvantová fyzika zachránila predstavu, že nič nie je vopred rozhodnuté a nemenne dané. A ukázala možnosť, ako by mohol Boh zasahovať do svetového diania  a našich osudov, bez toho, aby musel konať zázraky (v zmysle flagrantného porušenia fyzikálnych zákonov), bez toho, aby sme si to vôbec všimli! Aby bolo jasné: Nevieme, či to robí. Fyzika nám ani nepovie, či Boh existuje. A ak, či koná a ovplyvňuje udalosti práve takto. Ale vieme, že takáto možnosť existuje.        Ako to funguje? S nevyhnutnou dávkou zjednodušenia začnime faktom, že veľmi malé objekty (napríklad atómy, elektróny, protóny, atď.) sa chovajú podivným spôsobom, ktorý dokáže popísať len kvantová fyzika, klasická je tu bezmocná. A kvantová fyzika hovorí: nemôžeme vystreliť časticu presne známou rýchlosťou a presne známym smerom. Nemôžeme s istotou očakávať, že dopadne (s ľubovoľnou presnosťou) tam a vtedy, ako sme to predpovedali či vypočítali. Pretože príroda sa zásadným spôsobom bráni a neumožňuje nám presne zistiť polohu a rýchlosť (resp. hybnosť) častice. Čím presnejšie zistíme (zameriame) jedno, tým menej presné bude druhé, a naopak (ako iste viete, odborne sa tomu hovorí Heisenbergov princíp neurčitosti, a je to základ celej kvantovej fyziky). Tento fenomén nemá nič spoločné s nedokonalosťou našej meracej techniky (ktorú by sme časom mohli vylepšiť). Nie, narazili sme na istú hranicu, a príroda nepustí. („Potiaľ smieš, ďalej nie.“) Výsledok je, že nevieme presne, kam častica dopadne – vieme len vypočítať pravdepodobnosť, s akou dopadne tam a tam, a s akou pravdepodobnosťou o kúsok vedľa. V praxi to znamená, že ak vystrieľame častíc naozaj veľa, na miesta vyššej pravdepodobnosti ich dopadne celkovo viac, na miesta malej pravdepodobnosti menej. Vieme teda predvídať, ako bude vyzerať terč po ukončení dlhotrvajúcej streľby, ak zostaneme pri tejto metafore – niekde bude ako rešeto, niekde skoro nedotknutý. Čo ale nedokážeme, je predpovedať osud jednej individuálnej častice; nevieme, kam dopadne jeden konkrétny projektil. Náhodnosť, ktorá je neodstrániteľná. A takto sa chová v mikrosvete všetko, nielen lietajúce častice.       Einstein s tým mal problém. Kládol si otázku, či Boh hádže kockou, aby rozhodol, kam dopadne konkrétna častica. Odpoveď nikdy nenašiel, a je možné, že leží mimo fyziky. Je dokonca možné, že na druhej strane pomyslenej hranice sedí osobne Boh a nastavuje skryté parametre tak, aby tento elektrón letel tam a tamten neutrón narazil sem. A nezistíme to. Celkom zábavná predstava. Alebo nás až zamrazí, vyberte si.       Na tomto mieste celej úvahy pozorný čitateľ asi namietne: „Dobre, točíme tu o nevypočítateľnom chovaní mikročastíc. Ale veľký súbor častíc sa vďaka zákonom štatistiky chová vypočítateľne. Napríklad ľudské telo obsahuje nejakých 1027 až 1028 atómov, to už je skutočne veľký súbor. Aký môže mať vplyv chovanie jednotlivého atómu či elektrónu v takmer nekonečnom mori štatistiky? Pre nás – makroskopické objekty – stále platí klasická fyzika!“   Odpoveď znie: nie celkom a nie vždy.        Existujú vo fyzike javy či procesy, najčastnejšie zvané turbulencie. Majú jedno spoločné: systém veľmi vzdialený od rovnovážnych pomerov, v ktorom platí, že veľmi malé príčiny môžu mať veľmi veľké dôsledky. Počasie je typický príklad – vieme pomerne spoľahlivo predpovedať, že trebárs v piatok podvečer budú na Východnom Slovensku búrky. Ale nevieme vôbec povedať, kam udrie prvý blesk. Nevieme, či sa z postupujúcej oblačnosti spustí prietrž mračien vo Veľkom Šariši, alebo lejak začne o pár minút skôr v nešťastných Jarovniciach. O tom, kde sa začne odvíjať špirála nejakého lavínovitého deja, rozhodujú nepatrné náhodné poruchy. Niekedy by asi stačilo, aby sa ten atóm či onen elektrón pohol inak a inde, a celý makroskopický výsledok je dramaticky odlišný. Pri pohľade na nejaké odplavené domy či stratené životy možno niekto spomenie vyššiu moc. A netuší, ako presne pomenoval príčinu. Vyššia moc, vo fyzickom i metafyzickom zmysle slova.       A je vám asi jasné, že počasie je len jeden z nepreberného množstva príkladov; turbulentné javy stretávame na každom kroku. Ak Boh existuje, stačí mu ovplyvniť zopár mikročastíc, a svet nebude tým, čím býval. Ani zázraky k tomu nebolo treba. Boh si nechal dostatočný manévrovací priestor.       Vôbec netvrdím, že takto to vo svete funguje. Nie je to z mojej strany nič viac, než myšlienková hra. Predpokladám, že fyzici a snáď i osvietenejší teológovia sa podobnými úvahami zaoberajú už dávnejšie, a určite na profesionálnejšej úrovni. Nezisťoval som, aký je súčasný state of the art podobných teórií. Jednoducho, zdala sa mi to byť zaujímavá téma. Ak uvážime, odkedy a s akým nasadením riešia niektorí ľudia otázku, či je fyzika zlučiteľná s náboženstvom, azda tento drobný príspevok do veľkej diskusie niekto ocení.        Poznámka: tento článok je postskriptom k mojej dávnej eseji o fyzike:   http://www.enigma.sk/e-knihy/tomas-derer-fyzika-ocami-laika           .             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (108)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Dojmy a pojmy fotoamatéra (1.časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Čo majú podľa mňa urobiť učitelia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Novembrové impresie po litri vína
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            O živote po živote (3.časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            O živote po živote (2.časť)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Dérer
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Dérer
            
         
        tomasderer.blog.sme.sk (rss)
         
                                     
     
        Chemik, euroskeptik, klimaskeptik, slobodomyseľný bezočivec a kacír, spochybňovač autorít, podozrivý zo sympatií k libertariánom. Súkromník slúžiaci zahraničnému kapitálu. Sodoma-Gomora? Čo už, nič lepšie o sebe mi nenapadá. To najlepšie na mne sú moja žena a moje deti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2117
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




