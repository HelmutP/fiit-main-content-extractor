
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marie Stracenská
                                        &gt;
                o kadečom
                     
                 Po kostole 

        
            
                                    23.5.2010
            o
            13:18
                        (upravené
                23.5.2010
                o
                13:24)
                        |
            Karma článku:
                13.03
            |
            Prečítané 
            1587-krát
                    
         
     
         
             

                 
                    Nedeľné ráno. Malé slovenské dedinky. Tety v tmavých kostýmoch, ujovia v klobúkoch. Dievčatko v ružových nohaviciach vedie za ruku mamka. Všetci idú rovnakým smerom, do centra predpoludňajšieho diania. Okrem nich nikde nikto. Auto stretnem len sporadicky. Je pekné sa takto skoro vychystať na cestu. Vlčie maky sa ukláňajú stále silnejúcemu slnku. Teplo fúka, ale len trochu. Počúvam zaujímavé slová z rádia, otvoreným oknom cítim vôňu začínajúceho leta.
                 

                 Už som skoro tam, ešte asi desať minút odhadujem. Idem malou dedinkou na Považí a teším sa na starú zrúcaninu. Och, čože?! Okamžite brzdím, vyskakujem z auta, automaticky zamykám a utekám ku krajnici na opačnej strane. Babička o paličke polo kľačí polo stojí a nevie sa pohnúť. V takomto teple na sebe čierny vlniak, hrubú šatku, ktorá jej cloní oči, v jednej ruke barla a v druhej krásne staromódna čierna kabelka, hrubé pančušky pod dlhou sukňou takmer nevidno. „Krivo sem stúpila. Ďakujem." Zhrbená starká mi siaha len niečo vyše pása. „Ďaleko bývate? Pomôžem vám domov." Chytím ju pod pazuchu a prekvapí ma ako rázne vykročí. „Z kostola idem. Krivo sem stúpila, nemohla sem vstat." Neviem, kam ideme, ale ďaleko to asi nebude. „Za kostolom do uličky a je to kúsok", ako by babička vedela, na čo myslím. Prejdeme asi päťdesiat metrov, keď zastane. „Dobre ideme?", začne sa obzerať. „Na jednej strane je plot kostolnej záhrady, na druhej múr", vysvetľujem. „Mna tu majú za takú, čo rozum stráca", vysvetľuje babička. „Ale asi dobre ideme. Počkajte. Sopel mám." Vreckovku urobenú z utierky vyťahuje spod útrob vlniaka. „Ale sem spadla! Aj hlavu mám rozbitú? Ale ma doma zas vyhrešia..." Pozriem sa na ňu lepšie, veru má oškreté čelo. Že som sa rovno nepozrela poriadne! A vlastne, prečo som ju nenaložila do auta a neodviezla? Pýtam sa seba a neviem odpovedať. Rýchlo sa to celé stalo. „Nech nabudúce idú do kostola s vami", navrhujem. „Ony už so starýma nechcú chodit." Kráčame pomaly, ani sa o mňa veľmi neopiera silou, skôr ma stíska. Dvakrát sa ma opýta, odkiaľ som. Sto metrov nám trvá asi desať minút. Ale veď babička si hádam pamätá, kde býva, verím. „Už sme tu, tu to je."   Otváram cudziu bránku, dom je veľký a pekne opravený. Ešte pár schodíkov. Dvere otvorí starší pán, asi syn. „Babka spadla pri kostole, pomohla som jej", vysvetľujem. Nedôverčivo na mňa pozrie."Ale to sa muselo už dávno stat, omša sa už dávno skončila." Ani nestihnem odpovedať. Už ho nezaujímam. „Kde si bola? Čo si zasa robila?! Načo si do teho kostola išla?" Rozkričí sa na babičku, „vyhreší" je na toto slabý výraz. Starká sa na vrchnom schode otočí: "Šak podte na olovrant." Vyjachcem niečo o aute a že sa ponáhľam. Chlapík si ma vôbec nevšíma, stále hučí a opakuje, že je babička hrozná a že sa to zasa stalo. Pripadám si nemiestne. Otáčam sa odchádzam, ešte začujem ďalšie mužove výčitky, pridám do kroku.   Sadám si späť za volant. Ruka, ktorou som ju podopierala pod pazuchou, mi vonia babkiným silným mydlom. Nič o nej neviem. Ani o jej rodine. Ale zaželám jej v duchu veľa šťastia. Mám pred sebou ešte kus pekného dňa. No, len či ho všetci budeme mať pekný... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Vianoce vo štvrtok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Nepoznám žiadne dieťa...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Ranná káva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Cez hrádzu zrýchlene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marie Stracenská 
                                        
                                            Poschodová
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marie Stracenská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marie Stracenská
            
         
        stracenska.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som žena. Novinárka, lektorka a konzultantka. Mama nádherných a šikovných dvojčiat. Manželka, dcéra, sestra, priateľka. Neľahostajná. Mám rada svet. Rovnako rýchlo sa viem nadchnúť, potešiť a rozosmiať ako pobúriť a nahlas rozhnevať. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    701
                
                
                    Celková karma
                    
                                                12.17
                    
                
                
                    Priemerná čítanosť
                    1779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o deťoch
                        
                     
                                     
                        
                            o vzťahoch
                        
                     
                                     
                        
                            o kadečom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Moja rozprávka
                     
                                                         
                       Zaťovia
                     
                                                         
                       Nápadník
                     
                                                         
                       Univerzitná nemocnica vyriešila problém Richterových prezervatívov
                     
                                                         
                       Obrovská stará dáma
                     
                                                         
                       Tutoľa máme kopčok
                     
                                                         
                       Bojovníčka
                     
                                                         
                       Vifon
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




