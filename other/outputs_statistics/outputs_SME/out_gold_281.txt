

 
Róbert Adamec včera v Paľbe len dokázal, že urobiť dobrú reportáž o probléme interrupcií je fakt ťažké. Zaujímavé je, že novinári pociťujú nutnosť robiť jasné závery napriek tomu, že fakty sú nejasné, veda nie príliš nápomocná a etika tiež nejednoznačná. Aspoň keby začali poctivým skúmaním faktov a argumentov, ktoré sa v diskusii objavujú. 
 
V reportáži Paľby  (druhý príspevok vo videu) sa rozoberajú viaceré veci, tak poďme postupne: 
 
1.    Bilbordová kampaň 
 
 
	Tou druhou stranou, ktorá vraj život nepodporuje má byť hlavne Spoločnosť pre plánované rodičovstvo. Avšak podľa tejto mimovládnej organizácie je kampaň Právo na život vedená fanaticky a zavádzajúco. Napríklad už tým, že neúmerne zväčšený jedenásťtýždňový plod na bilboardoch v skutočnosti pochádza z trinásteho týždňa tehotenstva, kedy zákon interrupciu nepovoľuje. Väčšina takýchto zákrokov sa údajne robí medzi 6.až 8. týždňom, kedy plod detské telíčko ničím nepripomína. 
 
 
Je to tak alebo nie? Paľba nedáva priestor autorom kampane na reakciu k obvineniu.
 
 
K otázke financovania kampane domácimi sponzormi (je to možné, ľahké, uskutočniteľné?) sa nevyjadruje niekto nestranný z tretieho sektora, ale rovno odporkyňa kampane: 
 
 
	Oľga PIETRUCHOVÁ, Spoločnosť pre plánované rodičovstvo 
	-------------------- 
	Viedla som niektoré kampane, napríklad kampaň Piata žena a viem ako ťažko je zohnať prostriedky. A ja jednoducho neverím, že nezištní slovenskí podnikatelia poskytli len tak prostriedky v takom vysokom množstve, ako je potrebné na túto kampaň. 
 
 
Adamec tak následne špekuluje, že skutočné financovanie by mohlo prichádzať zo zahraničia. Spomína aj Alianciu za život: 
 
 
	Druhým mužom v pozadí kampane by mohol byť aj Bruno Quintavalle z Veľkej Británie. V médiách bol vídavaný vydávaný po boku slečny Tutkovej. Aliancia pre život, ktorú Bruno vedie, by sa dala dokonca považovať za politickú stranu. Svojho času sa postarala o škandál, keď jej televízia BBC odmietla odvysielať volebné šoty s obrázkami potratených plodov. Celá vec sa nakoniec dostala až pred súd. 
 
 
Reportér mohol spomenúť, ako súd rozhodol: Aliancia pre život opakovane prehrávala  (napriek dočasnému víťazstvu v roku 2002). 
 
2.    Argumenty, kedy začína život 
 
 
	Róbert ADAMEC, redaktor 
	-------------------- 
	Každý z nás má plné právo vyjadrovať svoje náboženské a politické postoje. Čo je však na celej veci zarážajúce a podozrivé, Právo na život sa verejnosti prezentuje ako nenáboženská iniciatíva, postavená na vedeckých faktoch. Ale je to skutočne tak? 
	 
	Marek NIKOLOV, organizátor kampane PRÁVO NA ŽIVOT 
	-------------------- 
	Na Slovensku ľudia nevedeli pred tým, ako vyzerá dieťa po umelom potrate v jedenástom týždni od počatia a v podstate na tom obrázku bolo veľmi jasne a zreteľne vidieť, že sa jedná o dieťa. 
	 
	MUDr. Michal KLIMENT, PhD., Spoločnosť pre plánované rodičovstvo 
	-------------------- 
	Pravda je taká, že toto sú skutočne len náboženské pohľady, pretože my rešpektujeme aj vznikajúci život, ale musíme si uvedomiť, že vždycky vznikajú určité množné potencionálne konflikty so samotnou tehotnou ženou. A tak sa z nenáboženského hľadiska vníma vznik života ako proces, ako kontinuálny proces, ktorý končí narodením. 
	 
	Róbert ADAMEC, redaktor 
	-------------------- 
	V záujme väčšej objektivity sme okrem doktora Klimenta oslovili ešte inú vedeckú kapacitu. Profesor Krčméry je dokonca citovaný aj na internetovej stránke Práva na život a navyše je to veriaci človek. 
	 
	Prof. MUDr. Vladimír KRČMÉRY, DrSc, Dr.h.c., rektor Vysokej školy zdravotníctva a sociálnej práce Svätej Alžbety 
	-------------------- 
	Či je to plnohodnotný človek, plnohodnotné dieťa, viete plnohodnotné dieťa sa dá, hovorím nazvať vtedy, keď je už narodené a má všetky teda prejavy plnohodnotnosti. Ale to slovo plnohodnotný je slovo, ktoré má viacero významov, hej? Ak sa narodí napríklad dieťa s Downovým syndrómom, alebo poškodené, že len s jednou rukou, s fokoméliou, tak nenazývame ho plnohodnotné dieťa, pretože nie je plnohodnotné, niečo mu chýba. Určitá časť hodnoty chýba. Ale nemôžeme z toho extrapolovať, že by sme ho mohli usmrtiť. 
	 
	Róbert ADAMEC, redaktor 
	-------------------- 
	To samozrejme netvrdíme ani my. Chceme len vedieť, prečo majú organizátori kampane takú silnú potrebu skrývať svoje ideologické postoje za údajne objektívne vedecké fakty. Ak by prezentovali svoj názor ako čisto náboženský, v demokratickom a právnom štáte by asi príliš nepochodili. 
 
 
Táto časť o vede sa mi zdá celkom dobrá. Problém je, že Adamec nepovie, že rovnaký problém s vedeckou oporou má protistrana – ako úsudok, že pred narodením je život neplnohodnotný vedie k akceptácii interrupcií? Krčméry podáva aj zaujímavé prirovnanie – neplnohodnotný život má predsa kopec životov aj po narodení, a tam málokto umožňuje slobodu voľby rodičov. Tiež mi trochu vadí, že keď už rozprával s Krčmérym a prízvukoval svoj objektívny prístup, prečo sa novinár nespýtal, čo konkrétne teda vedia Krčméryho k stanovisku jasného odporcu interrupcií. 
 
3. Argumenty, či zákaz interrupcií v skutočnosti znižuje ich počet 
 
 
	Róbert ADAMEC, redaktor 
	-------------------- 
	... A nie je isté ani to, či by zákaz skutočne vyriešil problém interrupcií. 
	 
	MUDr. Michal KLIMENT, PhD., Spoločnosť pre plánované rodičovstvo 
	-------------------- 
	Ak niekto tvrdí, že sa na Slovensku nenarodilo milióny detí, s ktorými vlastne ohlupujú verejnú mienku, tak to nie je pravda, pretože vieme, že tam, kde sa potraty zakážu, tam existujú len dve možnosti. Buď ženy umierajú na ilegálne potraty, pričom pôrodnosť nestúpne, alebo ženy cestujú za potratmi tam, kde sú liberálnejšie prístupy a pôrodnosť takisto nestúpne. 
	 
	Jana TUTKOVÁ, organizátorka kampane PRÁVO NA ŽIVOT 
	-------------------- 
	Pozrite sa do krajín, kde je potrat zakázaný, pozrite sa do Írska alebo Malty, kde je úplne zakázaný, je to rapídne zníženie potratov v týchto krajinách. 
	 
	Oľga PIETRUCHOVÁ, Spoločnosť pre plánované rodičovstvo 
	-------------------- 
	Írky cestujú napríklad do Veľkej Británie, rovnako ako pred tým Portugalky, kým to nezlegalizovali do Španielska, alebo z Malty cestujú ženy na Sicíliu a všetci o tom vedia proste a tvária sa, že to nevidia. 
	 
	Róbert ADAMEC, redaktor 
	-------------------- 
	Aj takto to môže vyzerať, keď sa osobné stane politickým. Táto žena [ukážka foto] vykrvácala na následky neodborne vykonanej interrupcie, pretože v jej krajine bola zakázaná. 
 
 
Obe protistrany nechá novinár hovoriť pomimo seba. Je predsa vysoko pravdepodobné, že zákaz potratov povedie aj k celkovému menšiemu počtu potratov, ale aj k viac nelegálnym potratom, aj k väčšiemu počtu úmrtí žien. Ale podstata je nájsť dominantné efekty, porovnať ich s ostatnými a zvážiť, či zákaz vytvára lepšiu kombináciu efektov ako nezákaz. Z neznámych dôvodov sa Adamec sám prikláňa k liberálnemu postoju. 
 
Ku koncu reportér poukáže aj na katolíkov za slobodu voľby: 
 
 
	Róbert ADAMEC, redaktor 
	-------------------- 
	Avšak nie sú to iba občania bez náboženského vierovyznania, ktorí so zákazom interrupcií nesúhlasia. Na týchto fotografiách je zachytený pochod za životy žien v USA, na ktorom sa zúčastnilo aj združenie Katolíci za slobodnú voľbu. Toto združenie zastáva názor, že právo na život v tomto prípade nemajú len embriá. Interrupčná etika musí brať rovnaký ohľad aj na ženu. 
 
 
Aha, tak prečo pár minút predtým kampani vyčítal, že presadzuje svoje náboženské ideologické vnímanie, no zároveň sa „prezentuje ako nenáboženská iniciatíva“? Tiež neviem, nakoľko je tá skupina relevantná – určite by našiel aj ateistov za zákaz interrupcií, ak by sa mu to do reportáže hodilo... 
 
 
	Róbert ADAMEC, redaktor 
	-------------------- 
	Metódou Spoločnosti pre plánované rodičovstvo je hlavne sexuálna osveta a propagovanie hormonálnej antikoncepcie. Pomocou tejto metódy sa jej za posledných sedem rokov podarilo znížiť na Slovensku počet interrupcií až o sedemdesiat percent. 
 
 
Ale tie na obrazovke ukazované čísla ukazujú celkový pokles interrupcií.  Ako vieme, že osveta a antikoncepcia vysvetľujú celý pokles? Čo tak vývoj demografie alebo (ako napísal Martin Hanus v .týždni v inak obdobne nepresvedčivom článku) cena za interrupcie? 
 
Adamec na konci reportáže charakterizuje dve strany debaty takto: 

 
 
	Toto však nie je mýtický boj medzi kozmickými silami dobra a zla, ale oveľa reálnejší a historický známy zápas medzi zástancami represie a prevencie. Tí prví si myslia, že sa morálka dá ľuďom vnútiť prísnymi zákonmi. Tí druhí sú presvedčení, že to tak nefunguje. 
 
 
Dosť zlá charakteristika. Tí prví predsa tiež sú za prevenciu (typu abstinencia). A tí druhí predsa nevidia potraty nutne ako nemorálne, naopak, vidia to ako lepšie, morálne riešenie v krízových situáciách. A naopak, aj tí druhí snáď veria v represiu v prípade vrážd – neprotestujú, že máme taký bohatý trestný zákonník na všelijaké skutky. Ale interrupciu vraždou nenazývajú – nieže nie sú za represiu, ale nevidia, za čo by tak mala byť. 
 
Zaujímavé tiež je, že reportáž obsahuje dlhšie diskusné výmeny Tutkovej s Adamcom (a la Sudor), no pri protistrane sú to krátko nastrihané citáty, bez novinárovej oponentúry. 
Príspevok sa tak zaradil medzi kopec iných, ktoré nepočúvajú poriadne obe strany, respektíve nie sú voči nim rovnako kritické, a ktoré robia unáhlené nepodložené závery pri téme, ktorá sotva môže dospieť do jednoduchého a jednoznačného záveru.
 
 
 
 
 
Update 8.11: Róbert Adamec reaguje:
 
 
Vážený pán Šipoš, 
ďakujem Vám za spätnú väzbu. Upozorňuje ma, že moje reportáže nemusia byť pochopené. V prípade Vašich kritických poznámok som však nadobudol dojem, že to „nepochopenie“ bolo asi zámerné.  
Kým prejdem ku konkrétnym bodom Vašej kritiky, treba povedať, že ste reportáž posunuli do úplne inej významovej roviny. Ak ste reláciu Paľba sledovali od začiatku, iste Vám neušlo úvodné zahlásenie Paľa Fejéra, že sa relácia bude venovať odhaľovaniu dezinterpretácií. Tam bol jasne definovaný zámer obidvoch reportáží. Tá moja sa mala venovať kampani PRÁVO NA ŽIVOT. Cieľom teda nebolo „urobiť dobrú reportáž o probléme interrupcií“, ale ukázať, že sa tento problém zo strany organizátorov kampane podáva verejnosti príliš jednostranne.  
Z dôvodov (uvedených v reportáži) som ku kampani zaujal stanovisko, že bola vedená netransparentne, zavádzajúco a problematiku interrupcií postavila do čierno-bielej optiky. Poukázať na tieto skutočnosti  si okrem iného vyžadovalo, aby som organizátorom kampane kládol „nepríjemné“ otázky a oslovil najmä ľudí, ktorí majú na problém interrupcií iný názor. V tomto prípade to bola Spoločnosť pre plánované rodičovstvo, pretože je najväčším oponentom kampane. Ukázať „druhú stranu tej istej mince“, demytologizovať problém a načrtnúť možné následky zákazu interrupcií, o ktorý sa snažia organizátori kampane – to bol skutočný zámer mojej reportáže.  
Čo sa týka „obvinenia“ organizátorov kampane, že 11-týždňový plod na ich bilborde pochádza z 13-teho týždńa tehotenstva – treba povedať, že to nie je obvinenie v tom zmysle, že by bolo treba určiť, či je alebo nie je pravdivé. Podľa zákona sa totiž tehotenstvo počíta od prvého dňa poslednej menštruácie, ale k ovulácii, teda aj k možnému oplodneniu dochádza po dvoch týždňoch, čím nastáva onen dvojtýždenný posun pri určovaní veku plodu. Túto informáciu Vám môže potvrdiť každý gynekológ. Organizátori kampane uviedli na svojom bilborde údaj „interrupcia 11. týždeň“, nešpecifikovali však, či sa tento údaj vzťahuje na plod alebo na tehotenstvo. Ak sa vzťahuje na plod, bilbord zobrazuje (na Slovensku v podstate) nezákonnú interrupciu, ak na tehotenstvo, potom klame fotografia, pretože plod by bol v tomto prípade 9-týždňový a teda oveľa menší. V obidvoch prípadoch je bilbord zavádzajúci.  
K otázke financovania kampane sa predsa ako prvá vyjadrovala slečna Tutková a nie pani Pietruchová – tá len reagovala. Tak isto som dal Tutkovej priestor, aby reagovala na moje „špekulácie“ ohľadom ľudí v pozadí kampane (Gregg Cunningham, Bruno Quintavalle) a ich možného financovania bilbordov. Dokonca dvakrát. Najmä tá druhá reakcia je, myslím, dosť veľavravná. Napriek tomu som nerobil nijaké jasné závery. Nič z toho ste vo svojich poznámkach neuviedli. 
Celú časť o vede ste si zle vyložili najmä preto, že ste nepochopili (?) celkový zámer reportáže. Ani trochu som sa nemienil púšťať do určovania toho, kedy vzniká život. Tu predsa išlo o niečo úplne iné. Organizátori kampane interpretujú plod ako „dieťa“ a predkladajú to verejnosti ako vedecký fakt. Pokúsil som sa teda ukázať, že vo vedeckom svete v tejto veci nepanuje nijaký konsenzus.  
V časti, ktorá sa venovala otázke, či zákaz naozaj vyrieši problém interrupcií tvrdíte, že nechávam obidve protistrany hovoriť „pomimo seba“. Celý blok začína doktor Kliment – hovorí, že tam, kde sa potraty zakážu sa robia potraty ilegálne a ženy zomierajú na následky neodborne vykonávaných interrupcií alebo ženy cestujú do zahraničia. Na to reaguje Tutková a chce diváka presvedčiť, že v krajinách, kde je potrat zakázaný sa rapídne znižuje ich počet. Na to priamo reaguje Pietruchová, ktorá hovorí o potratovej turistike v krajinách, ktoré chvíľu predtým spomenula Tutková. Nasleduje komentár s fotkou ženy, ktorá vykrvácala na následky neodborne vykonanej interrupcie, pretože v jej krajine bola zakázaná a údaje Svetovej zdravotníckej organizácie, že takto ešte dnes zomiera 70 tisíc žien ročne a milióny si odnášajú trvalé následky. Na to som hneď dal priestor slečne Tutkovej, ktorá spochybnila tieto štatistiky atď. Aj tu ste celú vec posunuli do inej významovej roviny. Vôbec neriešim, čo je lepšie, a nepridávam sa na žiadnu stranu! Znovu len poukazujem na to, že organizátori kampane verejnosti účelovo selektujú informácie a zatajujú možné následky zákazu interrupcií. 
Komentár, v ktorom hovorím, že to nie sú len občania bez náboženského vierovyznania, ktorí so zákazom interrupcií nesúhlasia ste celý vytrhli z kontextu. Reagujem ním totiž na slová doktora Klimenta v predchádzajúcom synchróne (viď reportáž). Zaradil som ho práve preto, lebo si nemyslím, že základným rozdielom medzi prívržencami a odporcami zákazu interrupcií je IBA náboženská resp. nenáboženská orientácia. Na tomto mieste ste mi tiež vytkli, že si protirečím, pretože som vraj „pár minút predtým kampani vyčítal, že presadzuje svoje náboženské ideologické vnímanie, no zároveň sa prezentuje ako nenáboženská iniciatíva“. Nejde len o to, že sú organizátori kampane nábožensky orientovaní (veď iní nábožensky orientovaní ľudia sú za slobodu voľby!), ale že sa skrývajú za údajne objektívne vedecké fakty. A majú na to svoj dôvod (viď príslušný komentár v reportáži). 
V komentári, kde som použil údaje, zobrazujúce pokles počtu interrupcií na Slovensku predsa nešlo v prvom rade o skúmanie všetkých faktorov, ktoré na to vplývajú. Poukazoval som predovšetkým na rozdielnosť v názoroch oboch strán, týkajúcich sa riešenia problému interrupcií  a že metódy Spoločnosti už sú v praxi overené ako úspešné. Avšak, organizátori kampane nesúhlasia s preventívnymi metódami Spoločnosti v zmysle sexuálnej osvety a s hormonálnou antikoncepciou. Nasledovali synchróny, ktoré usvedčujú Tutkovú zo zavádzania. Tvrdí totiž, že „deti“ (tým myslí oplodnené vajíčka!) sú týmito hormónmi chemicky zabíjané. 
K poslednej kritickej poznámke: Skutočne som presvedčený, že tento spor je sporom medzi zástancami represie a prevencie. Represia predstavuje snahu niečomu zabrániť, prevencia znamená niečomu predchádzať. Zákaz interrupcií, o ktorý sa snažia organizátori kampane Právo na život môžme považovať za prevenciu len s veľkou dávkou fantázie. Keď totiž niekomu niečo zakazujete, tak mu vlastne zabraňujete v určitej činnosti. Zákon nedokáže predchádzať potratom, pretože (ako som ukázal v reportáži), potraty sa budú aj naďalej vykonávať a to nelegálne, pokútne atď.  
Na záver chcem len dodať, že som rád diskusii, ktorú ste otvorili svojimi kritickými poznámkami. Vždy je lepšie spolu hovoriť ako nehovoriť. 
S úctou 
Róbert Adamec
 
 
Update 12.11: Jana Tutková reaguje listom. 
 

