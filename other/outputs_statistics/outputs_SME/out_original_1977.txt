
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Šlinská
                                        &gt;
                Nezaradené
                     
                 Shina: Verím, že všetko, čo som zasiala, jedného dňa zožnem 

        
            
                                    22.12.2009
            o
            8:00
                        (upravené
                22.12.2009
                o
                13:38)
                        |
            Karma článku:
                10.64
            |
            Prečítané 
            3531-krát
                    
         
     
         
             

                 
                    Shina. Speváčka, textárka, hudobníčka. Zakladateľka a majiteľka vydavateľstva Slnko Records. Pri pohľade na ňu mám chuť parafrázovať Ivu Bittovú a napísať, že Shina je proste divná slečinka. V tom najlepšom slova zmysle. V jednej z piesní svojej kapely Longital spieva: Konečne si letel, tak ako si vždy chcel...Práve tento text mi vnukol nápad urobiť tento nie celkom tradičný rozhovor... 
                 

                 
foto: Ivi Rebová 
   Nikdy si nebola známa a nikdy si nebola vplyvná... Slnko records a Longital. Dá sa povedať, že ty už známa a vplyvná si. Akou cestou si musela prejsť, kým si to dosiahla?   Ja si tak nepripadám, môžem kľudne po meste chodiť električkou aj peši a motať sa kde chcem a nikto nevie, kto som. Mohla by som sa natlačiť do televízie, ale nikoho nezaujíma, čo si myslím, našťastie. Ďalej je ten text ale oveľa zaujímavejší : tvoj hlas je pretkaný silou, čo doňho dáš, keď ju máš. A dostať sa k tej sile, to je práca. Nemyslím tým silný hlas, ale skôr to čo spieva Lucia Piussi: slabým hlasom povedz silné vety. Musíš sa k tým silným vetám dostať, dostať sa k poznaniu, ktoré je v tých vetách silné. Musíš sa prepracovať k tomu, že nech si akákoľvek, máš tu svoje presné miesto, na ktoré sa musíš postaviť a na tom mieste stáť, svoju rolu zahrať perfektne - aj keby to bol záporný charakter.   Kúsky všetkého bývalého sa dnes vo mne skladajú... Páliš za sebou mosty?   Nepálim, ale upratujem a to veľmi poctivo, nemôžem povedať, že som v minulosti nechala niečo nevyriešené. A to sa spája aj s materiálnymi vecami. Zistila som, že keď sa motám na mieste, neviem čo ďalej, je dobré vyhodiť pár starých vecí a urobiť miesto novým. Poupratovať byt, napríklad, ale poriadne. Zbaviť sa vecí už len preto, že jednoduchý život sa menej zamotáva. Kedysi som dostala dobrú radu: Peniaze treba míňať, aby mohli prísť ďalšie. Tak niečo v tomto zmysle, týka sa to aj všetkého ostatného. A keď raz uzavriem nejaký vzťah, nikdy sa k nemu nevraciam.   Zostal smútok, čo sa nedá zniesť... Čo ti pomáha, keď si smutná?   Už nebývam smutná, nejako ma to prešlo.      Konečne si letel, tak ako si vždy chcel... Tvoje túžby. Splnené i nesplnené.   Mala som veľkú túžbu dlhé roky dostať hudbu, čo mi hrala v hlave do tohto sveta. Keď sa mi to podarilo, bolo to pri pesničke Keď vták Peng, dobre si to pamätám, mala som pocit, že som všetko dosiahla. Taký pocit pokoja, trhnutie a ticho. Trvalo to ale iba chvíľu, zostalo vo mne ešte veľa rôznych ambícií a tie ma ženú ďalej, musím im slúžiť, kým sa ich nezbavím, už akýmkoľvek spôsobom, dosiahnutím, rozpustením. Musím sa im venovať, lebo v nich je energia pohybu, ten pohyb ťa privádza do situácií, ktoré musíš zažiť, vedie ťa k ľuďom, ktorých musíš stretnúť, musíte si splatiť navzájom účty...  To je práca, čo tu mám.   Dnes som loď osamelá...    Toto je pre mňa silná téma, Osamelosť cítim stále, ale nie takú, že by okolo mňa nikto nebol, alebo by som mala núdzu o ľudí. Skôr ten spôsob, akým sa pozerám na svet, je zvláštny a keď to začnem používať, vidím, ako sa ľudia bránia, ako to nechcú počuť. V tom sa cítim sama, čo sa týka živých ľudí okolo mňa, ale zase nachádzam množstvo spriaznených duší v knihách, napríklad ľudia, ktorí sú ďaleko alebo dávno mŕtvi, ale náš vzťah je stále živý. V drôtoch to šumí, svetielka blikajú, správy prichádzajú.   Na nič nečakáme, zbavení hraníc vyrazíme... Tvoje vlastné hranice. Kde sú? Podľa čoho/koho si ich určuješ?   Dlhé roky som sa cítila veľmi obmedzená a mávala som aj pocity nedostatočnosti, že nikdy nič poriadne neurobím, že nedôjdem ani tam, kde už iní boli. A to je taká blbosť, s týmto zápasí veľa ľudí. Neuvedomujú si, že si sami stanovujú ciele, sami stanovujú hranice, sami tvoria svoj život. Sami sa rozhodujú, že budú nešťastní a chudobní a potom naozaj sú. Dávajú si nízko latku a stále dokola ju preskakujú. Napríklad muzikanti. Obľúbená nízko položená latka je: tým sa neuživíš a ja musím živiť rodinu, nemôžem sa venovať hraniu. Toto sa ale nedá oklamať, každého to dobehne a to v rôznom veku,  všetko čo mali urobiť a neurobili, napríklad nevyužili svoj talent, ktorý im bol daný na rozvíjanie.   Ani slobodná, ani uväznená, vezmem si len to, čo mám vnútri... Čo máš vnútri?   „Vnútri" je všetko. Vonku vidíme len odraz toho, čo je vnútri. Keď chcem vedieť ako som na tom, pozriem sa okolo seba, akých ľudí stretám, do akých situácií sa dostávam, všetko čo potrebuješ vedieť máš takto stále so sebou.      Netrvám na tom, čo som trvala predtým, otváram všetky dvere, čo boli pre mňa zavreté... Zmeny v tvojom živote. Vítaš ich?   Určite, ja si ich plánujem a potom vyrábam. Zmeny aj tak nastanú, akurát, keď si  k nim otočená čelom, vidíš ich prichádzať, môžeš sa pripraviť a využiť ich. Keď cúvaš a nechceš vidieť, stále do niečoho narážaš a čuduješ sa, aké náhody a aké hrozné veci sa ti dejú. Sú hrozné len preto, lebo keď ich nevidíš a vrazíš do nich, ublížiš si, potom sa urazíš - prečo zase ja? - a nakoniec sa ľutuješ.   Tak dlho sme žili v predstavách... Naivita, dobrá alebo zlá vlastnosť? Si naivná?   Neviem či je to naivita, čo mám. Napríklad ľuďom naozaj dôverujem, všetkým, ale dávam si pozor. Nepestujem si predsudky a keď nejaký nájdem, snažím sa ho rozpustiť.   A teraz už vieme byť slobodní... Čo pre teba znamená sloboda?   Mať pre seba všetok svoj čas a naložiť s ním podľa toho ako chcem,potrebujem.   A keď si neveríš, ako by som ti mohla pomôcť... Boli chvíle, keď si o sebe pochybovala? Keď si neverila, že to, čo robíš má zmysel?   Pochybnosti sú potvory, je najvyššie umenie zbaviť sa ich úplne, pracujem na tom.      Keď srdce velí choď a hlava vraví stoj... K čomu sa prikláňaš ty?   Srdce vždy vie, kadiaľ vedie cesta, hlava mu väčšinou bráni pustiť sa ňou. Niekde som počula, že na ceste srdca sa nedá zablúdiť. Tak to je presné. Ak máš vzťah so svojim srdcom vyrovnaný (vyladený), stačí sa vždy spýtať: Idem správne? Odpoveď je vždy jasná, akurát málokto ju naozaj chce počuť a málokto sa podľa nej riadi. Hlava vyrába pochybnosti.Toto delenie je také zvláštne, lebo nikto naozaj neverí, že by v srdci mohlo niečo také byť ako rozhodovacie centrum, a ani sa to nemyslí doslova. Skôr je to niečo, čo by sme asi mohli nazvať myseľ, ktorá má svoju plusovú časť a mínusovú. Alebo dobrého a zlého. Alebo toho, čo ťahá hore a toho, čo láka dole. Tu hrá srdce vždy toho správňáka, ktorý vyhráva. Znamená to, že keď svoje rozhodnutia robíš podľa srdca, to, čo najviac chceš, po čom naozaj túžiš  naozaj dostaneš, s podmienkou, že sa pre to rozhodneš,venuješ tomu všetku svoju energiu, nenechávaš si zadné vrátka, a tak ďalej.   Anjel môj, vždy pri mne stoj, tíš ma, vezmi si môj nepokoj... V čo veríš?   Verím, že všetko, čo som zasiala, jedného dňa zožnem, či dobré alebo zlé, a budem sa s tým musieť zmieriť, nech to bude čokoľvek.   Je nám nadelené, na čo si pomyslíme... Ty a sila myšlienky.   Nuž toto sa všeobecne podceňuje. Myšlienky, to, na čo celé dni myslíme, to, čo celé dni hovoríme, to sa stáva našou realitou. Keby sme boli schopní aspoň jeden deň sledovať všetky svoje myšlienky a reakcie s odstupom, všetko sa o sebe dozvieme. A dozvieme sa aj všetko to, čo sa nám ešte len stane. A z textov spevákov, ktorí ich spievajú mnohým ľuďom sa dozvieme, čo sa udeje im. My speváci spievame svoje pesničky aj tisíckrát pre seba, pre druhých, je v tom strašná sila, môže nás vyniesť hore, alebo zhodiť úplne dole. Je tlaková níž ... Jedna z najoblúbenejších piesní nášho národa, spevák, ktorý pravidelne klesá na svoje dno. A tisíce mu v tom pomáhajú a potom sledujú svoje klesanie.   Z dvoch polovíc celá ja, z dvoch polovíc celá ty, z dvoch polovíc celá... Shina a jej druhé ja. Existuje?   Už sme sa toho dotkli. Sú vo mne dve, jedna čo všetko vie a druhá, čo všetko môže urobiť. A tieto dve ideálne musia spolupracovať. Tá čo je dole stále hystericky volá tej hore (ešte že sú mobily) čo mám teraz robiť ?! ako sa mám rozhodnúť?! No a tá horé tichá a kľudná posiela dole správy, ale sama nemôže nič urobiť, može len počkať na otázku a odpovedať. Keď sa to dole zle vyvíja, posiela zhora rýchle depeše, potkýňa ma, kašle vo mne, nechá ma čo to zmeškať, strká mi do ruky časopisy, knihy sa otvárajú na zaujímavých miestach - skrátka snaží sa upútať moju pozornosť - no a keď sa ženiem bezhlavo ďalej a nepočúvam, pripúta ma k posteli (choroba je ideálna) a tam sa upokojím a zrazu jasne ten jej tichý hlas počujem.  A potom si zase spolu šteboceme, posielame sms-ky ...   Často sa na teba dívam, myslím si, si ten, s ktorým pôjdem ďalej... Ako vidíš svoju budúcnosť?   Jasnú, svetlú, takú akú si sama urobím, ona je už v dnešnom rozhodnutí zakódovaná.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Diagnóza: SAShE pozitív
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Martina Slováková, talentovaná individualistka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Hip-hop bez pózy?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Juraj Kušnierik: Slováci čítajú toľko, ako nikdy predtým
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Šlinská 
                                        
                                            Rudi Rus: Metal je výnimočné umenie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Šlinská
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Šlinská
            
         
        slinska.blog.sme.sk (rss)
         
                        VIP
                             
     
        music is my religion...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2904
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Francisco X. Stork- Marcelo objavuje skutočný svet
                                     
                                                                             
                                            Peter Stamm Približná krajina
                                     
                                                                             
                                            Stefan Beuse- Strieľame gumičkami do hviezd
                                     
                                                                             
                                            Monika Kompaníková- Piata loď
                                     
                                                                             
                                            Zuzana Mojžišová- Bon voyage
                                     
                                                                             
                                            Michaela Rosová- Hlava nehlava
                                     
                                                                             
                                            Chuck Palahniuk- Deník
                                     
                                                                             
                                            Amélie Nothomb- V ohromení a strachu
                                     
                                                                             
                                            Michael Hornburg- Břečka
                                     
                                                                             
                                            Boris Vian-Pěna dní
                                     
                                                                             
                                            Paolo Giordano- Osamělost prvočísel
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Thelma &amp; Clyde
                                     
                                                                             
                                            James Blake
                                     
                                                                             
                                            stateless
                                     
                                                                             
                                            zero 7
                                     
                                                                             
                                            koop
                                     
                                                                             
                                            tori amos
                                     
                                                                             
                                            lisa papineau
                                     
                                                                             
                                            dan bárta
                                     
                                                                             
                                            cat power
                                     
                                                                             
                                            bat fot lashes
                                     
                                                                             
                                            st germain
                                     
                                                                             
                                            feist
                                     
                                                                             
                                            ane brun
                                     
                                                                             
                                            lisa papineau
                                     
                                                                             
                                            tori amos
                                     
                                                                             
                                            radiohead
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Simča
                                     
                                                                             
                                            Baška
                                     
                                                                             
                                            Saša
                                     
                                                                             
                                            Samo
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Alenka
                                     
                                                                             
                                            Tomáš
                                     
                                                                             
                                            Daniela
                                     
                                                                             
                                            Anka
                                     
                                                                             
                                            Soňa
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            lemon
                                     
                                                                             
                                            sashe
                                     
                                                                             
                                            eFeMko
                                     
                                                                             
                                            voices
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




