

 Vtedy sa nebeské kráľovstvo bude podobať desiatim pannám, ktoré si vzali lampy a vyšli naproti ženíchovi. Päť z nich bolo nerozumných a päť múdrych. Nerozumné si vzali lampy, ale olej si so sebou nevzali. Múdre si vzali s lampami aj olej do nádob. Keď ženích neprichádzal, všetkým sa začalo driemať a zaspali. O polnoci sa strhol krik: "Ženích prichádza, vyjdite mu v ústrety!" Všetky panny sa prebudili a pripravovali si lampy. Tu nerozumné panny povedali múdrym: "Dajte nám zo svojho oleja, lebo naše lampy hasnú." Ale múdre odvetili: "Aby azda nebolo ani nám ani vám málo, choďte radšej k predavačom a kúpte si!" No kým išli kupovať olej, prišiel ženích a tie, čo boli pripravené, vošli s ním na svadbu a dvere sa zatvorili. Napokon prišli aj ostatné panny a vraveli: "Pane, Pane, otvor nám!" Ale on im povedal: "Veru, hovorím vám: Nepoznám vás. Preto bdejte, lebo neviete ani dňa ani hodiny. 
 Mt 25, 1-13 

