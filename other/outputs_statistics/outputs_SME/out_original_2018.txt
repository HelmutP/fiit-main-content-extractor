
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tatiana Melisa Melasová
                                        &gt;
                Všeobecné
                     
                 Možno aj burina môže byť užitočná 

        
            
                                    6.1.2010
            o
            9:56
                        |
            Karma článku:
                14.49
            |
            Prečítané 
            4798-krát
                    
         
     
         
             

                 
                    Práve som si pustila Beethovenovu 9. symfóniu, ktorá mimo iného obsahuje aj časť, s názvom „ Óda na radosť“. Mám to už opočúvané najmenej tisíc krát, lenže dnes je to naozaj výstižné. Aj keď tento úsek symfónie zaznel v mojom živote už veľakrát, nikdy som to tak nebrala. Nedokázala som sa radovať z toho, čo bolo naozaj dôležité. Prišlo mi to také normálne a nikdy som nepredpokladala, že by sa naozaj zlé veci, ktoré som zažila v mojom živote mohli skončiť aj katastrofou. Tou mojou, samozrejme.
                 

                 Bola som presvedčená, že keby jedno hovno letelo po oblohe, tak padne na moju hlavu. Až dnes som pochopila, že to je presne naopak. Od môjho narodenia som disponovala toľkým šťastím, že momentálne mám obavy, ako to bude po zvyšok môjho života. Či som si svoje šťastie už nevyčerpala. Často som mala na mále a vždy som sa z toho nejakým spôsobom vylízala.   Nebudem tu všetko opisovať, spomeniem asi iba to najhoršie. V sedemnástich som mala opuch mozgu, v dôsledku užívania drog. Navŕtali mi dieru do hlavy aj miechy. Ostala som ochrnutá. Sami lekári sa vyjadrovali dosť skepticky. Najprv, či prežijem a potom,  či sa vôbec ešte niekedy postavím. Postavila som sa a rozchodila to. Trvalé následky minimálne.   Druhý krát som ochrnula o pár rokov neskoršie po aplikovaní kraku intravenózne. Tiež som to rozchodila.   Viac krát ma museli v akútnom stave vykrízovať v umelom spánku a zlyhávali mi pre život dôležité orgány, takže ma museli kriesiť.   K tomuto všetkému musím pripočítať bezvedomie, otras mozgu, predávkovanie, životu nebezpečné zranenia, a tak ďalej. To všetko niekoľko krát, samozrejme v súvislosti s drogami a mojim životným štýlom. Veď nie nadarmo sa hovorí, že zlá burina nevyhynie.   Zlá burina sa presne na mňa hodí. Keď sa pozriem koľko perspektívnych, dobrých a úspešných ľudí „rastie“ navôkol, mňa naozaj možno označiť iba za burinu. Každý sa jej chce zbaviť a odstrániť ju nejakým definitívnym spôsobom. Ale ona sa aj napriek tomu nedá vykynožiť. Skôr alebo neskôr znovu vyrastie, hoci aj cez betón, či asfalt. A tak neostáva nič iné, len s ňou naďalej bojovať, alebo ju ignorovať. Až sa nájde niekto, kto v tej burine nájde aj niečo pozitívne. Okrem toho, že je to škodná rastlina, môže byť napríklad veľmi liečivá ak trebárs Púpava. Tá je dokonca aj krásna a pritom je to burina.   Som strašne rada, že práve ja patrím k takýmto burinám a dúfam, že budem mnohým ľuďom užitočná. Terajšie a dúfam aj budúce obdobie môjho života mi pomáha prekonávať vlastné životné peripetie, ktoré som si sama vytvorila, no môže to byť veľmi užitočné pre ostatných.  Zistili mi protilátky na hepatitídu C, no vírus mi nenašli. Neuveriteľné?! Možno áno, ale nie v mojom prípade. Isté je, že som tu chorobu už mala, lebo moje telo si vytvorilo protilátky. Po opakovaných testovaniach mojej krvnej vzorky môžem víťazoslávne prehlásiť, že: NEMÁM hepatitídu C a nie som infekčná.  Snáď som si to vysvetlila dobre. Pravdou však ostáva, že patrím k tým 20% šťastných jedincov, ktorí si dokázali sami vyliečiť smrteľnú chorobu, bez podania akýchkoľvek medikamentov. Teda, ak sa za lieky nepovažuje heroín, pervitín a iné svinstvá, ktoré som do seba cpala. (Preboha, ak niekto z vás trpí smrteľnou chorobou, neberte toto ako návod!)  Takže pečeň mi zatiaľ ostáva. To je dobré, myslím že ju ešte budem potrebovať pre svoj budúci život. Mám ďalší dôvod na radosť. Ako dobre, že Beethoven napísal niečo také ako je „Óda na radosť“. Aspoň to mám  čím osláviť. Aj keď si nemyslím, že to komponoval pre príležitosť, akou je tá moja. Ale radosť je radosť, nech je vnímaná akokoľvek. Pre mňa určite. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (112)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy o prostitúcii (3)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy o prostitúcií 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Moje fakty a úvahy  o prostitúcii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Verona
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tatiana Melisa Melasová 
                                        
                                            Deti a rodičia drogy spolu nezničia (aspoň u nás)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tatiana Melisa Melasová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tatiana Melisa Melasová
            
         
        melasova.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    8108
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Všeobecné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            DROGY-SOS
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




