
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Sýkora
                                        &gt;
                výplody strapatej hlavy
                     
                 Škrtám fica a vy? 

        
            
                                    1.6.2010
            o
            7:44
                        (upravené
                1.6.2010
                o
                7:31)
                        |
            Karma článku:
                12.22
            |
            Prečítané 
            2267-krát
                    
         
     
         
             

                 
                    Už dávno som neblogoval, prevalený krízovými starosťami v práci, ale aj vlastnou snahou o zmenu tréningu na maratón. Trošku k tomu prispelo aj obrovské znechutenie zo stavu spoločnosti, kedy som už nevládal ani „telefonovať" s naším pánom premiérom. V novinách SME som už čítal len ozaj vybrané komentáre a moje obľúbené víkendové Fórum. Nadpisy komentujúce našu SK realitu mi stačili. Ozaj ma dosť ubíjal posledný vývoj u nás. Z mojej letargie ma prebudili dve veci, posledné číslo časopisu .týždeň a posledné blogy Mateja. A ešte trošku aj nádejný závan volebných výsledkov od nášho západného suseda. Ale pekne po poriadku.
                 

                 Vážim si to, čo sa celkovo podarilo .týždňu a síce vybudovať na Slovensku týždenník s významom, ktorý presahuje naše hranice. Som predplatiteľ a na festivale Pohoda sa vždy teším aj na ich diskusie. Štefan Hríb a jeho nekompromisné úvodníky, často až za hranicou „normálnosti" plus jeho Lampy stoja (väčšinou) za to. Na druhej strane biznis je biznis a tak (ne)chápem reklamu na „Novú éru pod Tatrami" práve v tomto časopise ?! Ostatne nie som sám a redakcia zverejnila aj kritické ohlasy iných čitateľov. Aj tak je veľa dôvodov, že .týždeň je kvalita, ktorú beriem ako súčasť svojho života. Nehovoriac o tom, že reportéra Andreja Bána poznám osobne a fakt si ho veľmi vážim za to, čo robí !   Posledné číslo s preškrtnutým Ficom na obálke a vynikajúcim celým obsahom ma zobudil. FAKT KLOBÚK DOLE! Vrelo odporúčam kliknúť si na web, nájdete tam vtipné a výstižné videá Antona Srholca , Shootyho, Miša Kaščáka, Veca a Stana Dančiaka. Teší ma, že pri každom z nich je počet klikov nad desať tisíc, že konečne to rezonuje medzi ľuďmi. Plus v tomto čísle je aj vynikajúci rozhovor práve s pánom Srholcom, ktorý je plný skvelých myšlienok a posolstiev. Citujem jeho odpoveď na otázku o dôležitosti týchto volieb a či máme nádej, čo sa týka budúcnosti Slovenska: „Nechcem šíriť nejaký teľací optimizmus, ale vždy je nádej. Boli aj horšie časy a dostali sme sa z toho. Pri všetkom tom negatívnom vidím v našej spoločnosti prvky obrody a hľadania novej duchovnosti, ktorá už nie je na tradičných príkazoch a zákazoch. Je tu už veľa mladých ľudí, ktorým záleží na tom, aby sme boli slobodní a odmietajú náznaky novej totality. Ja stále žijem v nádeji. A aj keby som už umrel, tak umriem v nádeji a hrdý na to, že sme urobili veľký pokrok." Rozhovor stojí za to, rozhodne si ho nenechajte ujsť!   Nakoniec celý obsah od úvodníku, ktorý vysvetľuje rozhodnutie redakcie, prečo škrtli Fica až po slepé milicionárske náboje zábavného Mira v bodke Milana Lasicu je mimoriadne vydarený. Ak niekto verí, že Róbert Fico si zaslúži byť opäť premiérom, tak nech si prečíta toto číslo. Ak aj potom bude rozhodnutý dať hlas Smeru, tak nech si ešte prečíta posledné Matejove blogy, najmä tento. A potom, ak by predsa len mal záujem krúžkovať 17ku, je mi ho ľúto. Keď nič iné, tak kvôli jeho deťom a vnúčatám, samozrejme pokiaľ ich z akciovky Smer nezabezpečil do života. A ak aj materiálne, morálne nikdy!   Aby som nezostal len pri týchto slovách, prejdem aj k činom. Ráno obehnem stánky a kúpim zopár čísiel .týždňa do zásoby, aby som mal čo dať na prečítanie vo svojom blízkom okolí. Na pracovisku zriadim primeranú propagáciu škrtaniu fica a ideme na to. Proste je tu nádej, o ktorej hovorí pán Srholec. Je tu nádej, ktorú cítim z posledných Matejových blogov. Viem, samochvála smrdí, ale ja som na syna hrdý.   A na záver mám na Teba milý čitateľ môjho blogu prosbu. Nie je v tom nič osobné, ale pokiaľ sa ti chce, pomôž preklikať Mateja na výber sme.sk . Aj kvôli tomu, že sa snaží osloviť svojich rovesníkov - prvovoličov, ktorých hlasy budú potrebné v súboji s hlasmi našich ctených dôchodcov, pri všetkej úcte ku každému jednému z nich.   Proste škrtáme fica a čo vy?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Tri rohy jedenástka aneb Jarek a Róbert zažiarili v Prievidzi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Prečo by som nešiel do povstania ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Dobrovoľná brigáda v dnešnej dobe? Áno, podarila sa!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            Nech sa dielo podarí.....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Sýkora 
                                        
                                            42 do šťastia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Sýkora
            
         
        ivansykora.blog.sme.sk (rss)
         
                                     
     
        mierne unavený, ale vcelku optimistický štyridsiatnik, čo má rád prírodu, šport, čokoládu, modrú farbu a číslo 4 a riadi sa životným krédom "Rozhodujúci je človek"

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    128
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1573
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            o behu a inom športovaní
                        
                     
                                     
                        
                            Berkat - pomoc
                        
                     
                                     
                        
                            fotoreportáž
                        
                     
                                     
                        
                            cesty/necesty
                        
                     
                                     
                        
                            výplody strapatej hlavy
                        
                     
                                     
                        
                            rozhovory
                        
                     
                                     
                        
                            Pozorovania a minipríbehy
                        
                     
                                     
                        
                            starinky
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            nohavica/plíhal to je klasika
                                     
                                                                             
                                            Pink Floyd kapela mladosti je stále skvelá
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            nielen o behaní
                                     
                                                                             
                                            nielen o maratóne
                                     
                                                                             
                                            dobrá inšpirácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       7. ročník filmového festivalu Jeden svet v Prievidzi sa blíži
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




