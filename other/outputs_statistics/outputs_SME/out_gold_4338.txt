

 Na úvod dám to hnusné, aby som sa toho zbavil. 
 
Hľa, dieťa visiace  
na pupočnej šnúre 
jak v čase vianočnom 
ozdobné gule... 
 
 
Toto je jediné torzo môjho pokusu vyvolať hrôzu v mysli čitateľov na tému zrodu, neľudskosti voči detskému týraniu, možno aj interrupcie by sa tam našli, nehovoriac o zapojení kresťanskej morálky, vianočných reminiscencií v kontraste s hrôzami, ktoré sa dejú... Jednoducho angažovaná expresívna kritika. Medzičasom som sa stal otcom a trošku sa bojím na túto tému ďalej pokračovať, takže sa tomu vyhýbam, ako je to len možné. Desí ma, že by som mal vážne písať o neláske k deťom, už to dosť dobre neviem, stratil som odstup. Predtým som si myslel, že to chápem, dnes už je to nepochopiteľné.
 
 
Dosť bolo rozpakov, dávam niečo z mojej imaginárnej studnice o vzťahoch, asi nie na odľahčenie, ale to nevadí.
 
 
Vo vnútri tvojho 
zľadovateného srdca  
sa obnažený kĺžem   
a hľadám slzu odmäku. 
 
 
Nie, že by to pomohlo, 
veď aj hadie zuby plačú jed. 
 
 
No ak obeť neotráviš hneď,  
očakávaj stálu obetu. 
 
 
Budem obiehať  
vôkol tvojho srdca 
v krvnom obehu 
túžob milenca, 
ktorý prišiel na to, 
že sexu už viac niet. 
 
 
Dokonané. 
 
 
Pravda je, že sa mi veľmi páčia použité slová a obrazy spojené s ľadom, plačom, hadím jedom a obiehaní okolo srdca. Pravda však je aj to, že myšlienkovo to nie je práve konzistentné, Od pasáže s obetou by to chcelo dať niečo o vytrvaní, až sa srdce roztopí, vyplaví jedy a bude to happyend. Naproti tomu sa mi tam mimovoľne zjavil sex (Freud by sa potešil) a to ma celkom zaujalo (z profesionálnych dôvodov, preto je to prekvapivé, z iných dôvodov ma zaujíma vždy). Ako kontrast k láske by to nemuselo byť zlé, k tomuto sa určite ešte niekedy vrátim.
 
 
Nakoniec si dovolím uviesť jedno hrajkanie so slovíčkami ako príklad toho, keď to nejde.
 
 Tlačenka 
 
Vnútorný tlak 
vonkajších nátlakov 
ma vtlačil do tlače. 
 
 
Otlaky odolávajú 
pretlaku náplastí 
a tlačia ma do médií. 
 
 
Malo to byť o politikoch a ďalších osobách, ktoré bez ohľadu na cenu túžia po zviditeľňovaní. Zatiaľ sa mi to veľmi nepáči.
 
 
To by bolo všetko, ďakujem za trpezlivosť.
 

