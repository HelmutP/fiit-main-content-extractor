

 V priebehu posledného mesiaca ma oslovili kolegovia z Maďarska a Anglicka, aby som im preložil doručený dopis zo Slovenska. Ako hovoríme po anglicky: No problem. 
 Krátke pozadie, veľmi stručné: 
 Ak si zahraničná firma, prípadne fyzická osoba niečo kúpi na Slovensku, a je registrovaná ako platca DPH vo vlastnej krajine, tak si môže za určitých podmienok, v stanovenom čase uplatniť vrátenie DPH zo Slovenska. Nie je to jednoduchý proces a je zdĺhavý. 
 Pre celé Slovensko túto agendu vybavuje Daňový úrad Bratislava I.  
 List je napísaný gramaticky a vecne správne, ale bohužiaľ v Slovenčine. Takže všetci žiadatelia o vrátanie DPH z celého sveta dostávajú úradné dopisy v Slovenčine. Je to tak.  
 Na jednej strane chápem, že úradná komunikácia na Slovensku, by mala byť v Slovenčine, ale prečo šikanovať, zahraničných klientov. Daňový úrad Bratislava I, jedná o vrátení DPH hlavne so zahraničnými subjektmi,  a určite by uvítali, komunikáciu minimálne v Angličtine, čo by dokazovalo našu vyspelosť.  
 Veď Angličtinu alebo Nemčinu už vštepujeme našej omladine od Základnej školy a napísať štandardný dopis v angličtine, by mala byť rutina.  
 Neviem, čo na to povedať, pomôžem s prekladom, ale čo v krajinách kde nie je k dispozícii dobrák Slovák na preklad. 

