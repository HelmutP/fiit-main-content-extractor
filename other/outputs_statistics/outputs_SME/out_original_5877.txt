
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matúš Lazúr
                                        &gt;
                Malajzia
                     
                 Oáza pokoja v horách – Fraser`s Hill 

        
            
                                    17.5.2010
            o
            8:01
                        |
            Karma článku:
                4.59
            |
            Prečítané 
            1341-krát
                    
         
     
         
             

                 
                    Keď sa tropická horúčava nedá vydržať, jediným miestom v Malajzii kde sa dá voľne dýchať, sú hory. Po rušnom Genting Highlands v blízkosti hlavného mesta kde síce je chládok, ale zato atmosféra pripomína Las Vegas spolu s Disneylandom dokopy a 280 killometrov vzdialenom Cameron Highlands s čajovými plantážami, farmami a neustále preprachtými uličkami, zostávajú už len menej známe miesta, kde sa dá újsť.
                 

                 Jedným z nich je Fraser's Hill. Dostal som sa tam až na druhý pokus, ale miesto sa mi tak zapáčllo, že v rámci dlhšieho oddychu som si zvolil za základňu práve túto bývalú koloniálnu horskú rekreačnú stanicu.   Z bežných turistov na Frasers Hill nezablúdi takmer nikto.  Nachádza sa asi 100 kilometrov severovýchodne od Kuala Lumpur a ešte asi 40 kilometrov od najblžšieho mestečka Kuala Kubu Baru. Smerom hore ste už ale odkázaní na vlastnú dopravu. Niekedy vraj existoval aj autobus, no linka je zrušená na neurčitú dobu.      Ani samotná cesta hore nie je veľmi jednoduchá. Nekonečne sa kľukatiaca  strmá cesta, je síce dobre udržiavaná, ale veľmi úzka a neuveritešne kľukatá.  V závere cesty je dokonca brána, ktorá posledných 8 kilometrov reguluje dopravu v počas každej hodiny cez deň len jedným smerom.   To všetko spolu s veľmi málo domácimi obyvateľmi prispieva k tomu, že miesto je jedným slovom „čarovné". Taký pokoj a ticho, krásna príroda, kvetinové záhony pripomínajúce hociktoré kúpeľné mesto v Európe, sa len tak ľahko nenájdu. A všetko je pritom pravé, rovnako ako kamenné domy, čo zostali ako pozostatok bývalej koloniálnej slávy tohto miesta.  Turistický megapriemysel ešte toto miesto načisto nepohltil, aj keď chápadlá už načahuje...      Za svoju atmosféru vďačí toto miesto aj tomu, že tu vlastne nič nieje. Jediné malé námestíčko s atrakciou v podobe starých hodín obrastených popínavými rastlinami a kamennej budovy pošty patrí k najkrajším obrázkom z miestnej architektúry. Kamenné bungalovy sú roztrúsené po okolí, rovnako ako pár ubytovacích zariadení, golfové ihrisko, niekoľko turistických chodníkov, malé jazierko a  tržnica so stánkami ponúkajúcimi miestne špeciality.                                          Skrátka idylka. Je to jedno z mála miest v krajine, kde má človek pocit že tá exotika, sa dá aj dlhodobo vydržať...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            O troch vozoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Lovci a zberači
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Bratislava - Talin a späť 2. Kurská kosa.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Bratislava - Talinn a späť 1.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Spomienky na Baltik 3. Sopoty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matúš Lazúr
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matúš Lazúr
            
         
        matuslazur.blog.sme.sk (rss)
         
                                     
     
        Z ďalekého východu späť na blízkom západe.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    45
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1162
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Austrália
                        
                     
                                     
                        
                            Malajzia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O čase a bytí
                     
                                                         
                       Kráľom je otec
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       Zbohatlíci kolonizujú Petržalskú hrádzu. Polícia nekoná.
                     
                                                         
                       Niečo je zhnité v slovenskej pošte (porovnanie s rakúskou)
                     
                                                         
                       Aký je rozdiel medzi buzerantom a homosexuálom
                     
                                                         
                       Vďaka, pani chatárka, za otrasný víkend v Tatrách
                     
                                                         
                       Otec, odpusť mi...
                     
                                                         
                       O pracovnej sile (zamyslenie čakateľa v rade pred pokladňou).
                     
                                                         
                       Ako žiť?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




