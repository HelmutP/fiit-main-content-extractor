

 
Určite poznáte WD-40 a ak nie, je to mazadlo. Mazadlo takmer na všetko, ktoré kúpite skoro všade, od malého obchodíku, až po hypermarket, možno aj v zelenine . Ak ste niekedy videli Mr. Beana, mal ho nad umývadlom. Na čo, to neviem ale vraj to maže dobre. 
 
 
 
 
 
 
 
 
 Začnem netradične a to od konca.    
 
 
 
 
 
 
                                                                                                      Trnava 2.6.2008 
 
 
 
Vec: Oznámenie o úspešnosti ponuky – zaslanie 
          V súlade s vyhodnotením zákazky s nízkou hodnotou Vám oznamujeme, že Vaša ponuka na dodávku multifunkčného čistiaceho a konzervačného prostriedku WD40 v zmysle zoznamu položiek požadovaného tovaru bola neúspešná. 
 
 
Ďakujeme Vám za účasť a tešíme sa na ďalšiu spoluprácu 
(dokument vo formáte pdf si môžete pozrieť tu) 
 
 
 
Vravíte si nič zaujímavé, keby nebolo pár nezrovnalostí. 
 
 
Koncom mája 2008 sme dostali na firmu list od Krajského riaditeľstva Policajného zboru v Trnave, kde nás vyzývajú na predloženie ponuky. Išlo vlastne o prieskum trhu, ktorý mal byť vyhodnotený podľa kritérií : najnižšia cena a podmienkou bolo dodržanie počtu merných jednotiek a objemových balení. 
 
 
 
Ďalšia podmienka  vyzerala asi takto: 
 
 
Ak oslovený uchádzač predloží ponuku neúplnú, alebo nesplní niektorú z požiadaviek na jednotné spracovanie ponuky, nebude jeho ponuka hodnotená. 
 
V prípade, že akceptujete našu výzvu, spracujte prosím Vašu ponuku podľa požiadaviek uvedených vo výzve a v termíne do 5.6.2008 zašlite : 
 
 
elektronickou poštou na nižšie uvedenú adresu osoby určenej pre styk s uchádzačom alebo faxom. Podmienkou je súčasné zaslanie rovnopisu ponuky na adresu : 
 
 
Krajské riaditeľstvo Policajného zboru, Ekonomický odbor, Kolárová 31, 91702 Trnava 
 
 
Poštou alebo osobne, v uzavretej obálke, s uvedením obchodného mena a sídla uchádzača alebo miesta podnikania uchádzača s označením heslom  : 
 
 
 
„Súťaž  - WD 40 – NEOTVÁRAŤ“ 
 
 
 
Pomyslel som si trošku divné, poslať niekomu e-mail alebo fax a navyše uzatvorenú obálku? To majú páni policajti jeden fax asi v uzatvorenej miestnosti, ktorú otvoria až 6.6.2008 ráno? A pravdepodobne budú mať ošetrený aj ten príjem e-mailu?  Tušil som, že ako víťaz naša firma s tejto prapodivnej súťaže nemôže vzísť. No napriek tomu som  to poslal a čakal na vyjadrenie, ktoré  ste si mohli prečítať v úvode. List prišiel 6.6.2008, tak to si páni policajti dali na čas. Ale čo to nevidím v pravom hornom rohu dátum 2.6.2008. Tak to je gól, oni už vtedy vedeli, že sme neuspeli aj keď obálky mali prísť   do 5.6.2008. Vysvetlenie nepoznám ale napadá ma, že policajti otvárajú obálky priebežne ako chodia a na tie s vyššou cenou ihneď reagujú listom, takým ako sme dostali. Ale  to nám mohli poslať skôr, vypracoval by som ponuku novú. Alebo len niekto kto to otváral vedel komu má zavolať aby dal cenu nižšiu, čo i len o 1halier a poslal ju až posledný deň? Jedná sa o skutočne malý tender a ak sa to takto praktizuje pri tendroch kde cena sa pohybuje v tisícoch ako to vyzerá pri miliónoch? 
Otáznikov je tu dosť, odpovedať si musí každý sám. Ja viem jedno:  nie je dôležité zúčastniť sa ale vyhrať.  

 

