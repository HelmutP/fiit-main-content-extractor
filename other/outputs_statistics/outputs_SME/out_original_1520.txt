
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Najväčší babrák sedí na úrade vlády 

        
            
                                    9.7.2009
            o
            10:42
                        |
            Karma článku:
                18.28
            |
            Prečítané 
            52664-krát
                    
         
     
         
             

                 
                    Človeku, ktorý v živote nikdy nič poriadne nerobil sme zverili našu krajinu a tak to aj vyzerá. Premiér Robert Fico je vrcholne neschopný človek, ktorého tento národ zvolil na základe klamstiev a prázdnych sľubov za premiéra. Robert Fico možno dokáže viesť schôdzu SZM, ale určite nie Slovensko. Je to na zaplakanie.
                 

                 Dezolátny stav verejných financií   Ekonomika nemusí vždy len rásť, môže aj klesať a stagnovať a to, že tento rok budeme mať pokles HDP o 6,5%, nie je vina tejto vlády. Vina tejto vlády spočíva v tom, že ešte v októbri minulého roka, akoby sa vôbec nechumelilo, predpokladala 6,5 percentný rast HDP (číslo je teda rovnaké, len znamienko je opačné) a na základe neho naplánovala daňové príjmy vo výške 10 miliárd Eur. A keďže máme "sociálnu" vládu, boli výdavky plánované vo výške 11 miliárd Eur. Skutočnosť je ale taká, že príjmy budú možno 8 ale výdavky 12 a hrozí nám deficit, aký Slovensko ešte nikdy nemalo - 4 miliardy Eur.   Tento rekordný deficit pritom vôbec nemusel byť. Riadny hospodár by totiž nikdy nerátal s rastom ekonomiky, keď všetky okolnosti ukazujú na pokles. Rátal by s nižšími príjmami a plánoval by nižšie výdavky. Už v októbri 2008 som upozorňoval, že odhad nárastu 6,5% nie je reálny a že dôjde k výpadku daňových príjmov. Bohužiaľ, socani nevedia plánovať a nevedia hospodáriť a premiér Fico vedie túto krajinu rovno do bankrotu.   Pred tromi mesiacmi sme upozorňovali, že Slovensku hrozí deficit vyše 6% HDP a že treba prehodnotiť rozpočet a hlavne začať šetriť. Vtedy sa Róbert Fico tváril, že je všetko v poriadku, dnes už aj on priznáva deficit vyše 6% HDP. A čo napadne správnemu socanovi ako prvé? Šetriť? Kdeže. Ako prvé ho napadne zvýšiť dane. Socani totiž vždy vedeli kradnúť ľuďom slobodu, kradnúť im peniaze a hlavne vedeli rozdávať - z cudzieho.   A tak nám včera minister financií Ján Počiatek oznámil, že "Daňový systém, ktorý máme, je daňový systém do dobrého počasia..." a nevylúčil možnosť zvýšenia daní (samozrejme, že dávno vedia, že dane zvýšia, len potrebujú na to pripraviť národ). Dovolím si povedať, že o daniach viem viac než Ján Počiatek a tvrdím, že náš daňový systém je jeden z najlepších na svete. Samozrejme, že znesie vylepšenia, o ne som sa v roku 2007 pokúšal.   Daňový systém priniesol tejto vláde za prvých 30 mesiacov vládnutia o 150 miliárd Sk viac (po zohľadnení inflácie) ako vláde predošlej. A kde sú tie peniaze? Sú rozflákané a rozkradnuté. V tom je problém a nie v daňovom systéme.   Rozflákané   Tesne po nástupe vlády v roku 2006 zastavil Robert Fico privatizáciu Carga, z ktorej mohol mať náš štát 14 mld. Sk, dnes musí štát doplácať stratu 130 miliónov Euro. Nezmyselné sociálne podniky stoja desiatky miliónov Eur, šrotovné tiež. STV3, širokorozchodná trať, predražená rekonštrukcia hradu a opätovné zadlžovanie nemocníc sú ďalšími príkladmi rozflákaných peňazí.   Rozkradnuté   Pred očami nás všetkých ukradli 45 miliónov Eur alebo viac z emisii. Miliardový tender ohlásený na nástenke ministerstva Slovensko do nástupu Roberta Fica nezažilo. Vylúčenie uchádzačov o elektronické mýto z dôvodu, že sú lacní, je taký istý výsmech ako vybielené a vyčiernené zmluvy a podprahové a podlimitné tendre.   Takže, milí voliči SMERu: Naleteli ste na prázdne sľuby a klamstvá. Nie je to vaša vina, lebo ste nemohli vedieť, že všetky predvolebné reči boli len bláboly. Teraz to však viete. Viete, že celá sociálna politika SMERu je o tom, aby čím viac ľudí bolo sociálne odkázaných, inak by SMER nemal kto voliť. Viete, že táto vláda nevie plánovať a nevie hospodáriť. A viete, že premiér je vrcholne neschopný človek, ktorý v živote nič poriadne nerobil a vždy žil iba z cudzích peňazí. Keď ho zvolíte opäť, nesiete spoluzodpovednosť za nehorázne zadlžovanie a morálny úpadok našej krajiny. Vaše deti a vnúčatá vám to nikdy neodpustia.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (603)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Chmúrava
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            Milujú sa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




