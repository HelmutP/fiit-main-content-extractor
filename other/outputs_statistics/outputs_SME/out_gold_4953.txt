

 Práve som počúvala svedectvá o týchto udalostiach v rozhlasovom éteri na rádiu Lumen, keď sa môj zrak vryl do knižky, uloženej v rade kníh na vypolicovanej stene mojej pracovne. František Dlugoš: "Štefan Putanko svedok viery." V slove na úvod autor hovorí:" Sme dlžní osobnostiam, ktorým sme sa nestihli poďakovať." 
 Preto moja vďaka patrí  takýmto silným a odvážnym kňazom, vďaka ktorým  komunistická ideológia nedokázala zničiť Cirkev. Tým synom a dcéram slovenského národa, ktorí prežili útrapy väzení, vytrvali v ťažkých skúškach, v boji proti komunistickej totalite. Tí Slováci, ktorí dnes chvália a volia komunizmus a snažia sa o návrat totality, sú tí, ktorí majú krátku pamäť a nezaslúžia si slobodu. 
 Nech večný Veľkňaz Ježiš Kristus odmení horlivé a kňazské účinkovanie Štefana Putanka večným životom. Rovnako aj všetkých rehoľníkov, ktorí boli internovaní, vyvezení na technické práce do pracovných nútených táborov-koncentračných táborov pre kňazov v Mučeníkoch, Podolínci, v Hájku, v Kladne alebo inde, alebo boli väznení vo väzniciach v Leoploldove, Ilave, Pankráci alebo Mirove a podľahli tvrdým podmienkam. Vo svojich modlitbách vyprosujem pevné zdravie a dary Ducha svätého nášmu drahému kňazovi Štefanovi Janíkovi, ktorý aj napriek ťažkým rokom, ktoré prežil v PTP v Mučeníkoch  sa dožíva pekného veku a roky staroby prežíva v Nitre. 
 Sv. Ján Baptista Mária Vianney,  nebeský  patrón všetkých kňazov povedal: „Kňaz je láskou Kristovho srdca". Láska priam hrdinská láska bola úzko spätá s osobou Štefana Putanka až do konca jeho životnej kalvárie. Štefan Putanko a Štefan Janík sú  jednými z veľa nepokorených a nekompromisných kňazov, ktorých nezlomila ani Barbarská noc zla a silný program vládnucej strany, ktorý mal slúžiť k ateizácii spoločnosti. Títo naši kňazi svojim životom vydali cenné svedectvo živej viery. 
 Ďakujeme Vám za Vašu odvahu v boji s totalitným režimom pre pravdu a spravodlivosť! 
   
   

