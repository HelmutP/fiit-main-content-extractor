

 Posledný gitarový hrdina Slash nám naservíroval svoj sólový debut. Fanúšikovia a kritici už od ohlásenia prác na albume tŕpli v dychtivom očakávaní, čo tento velikán rockových pódií prinesie. Výsledok isto nie je sklamaním, no nadčasovým nesmrteľným platniam ako Appetite for Destruction či Use Your Illusion (obe od Guns N' Roses) sa nevyrovná. 
 Po prvom vypočutí mi v hlave prekvitali chválospevy na dokonalosť a vzbudil sa vo mne dojem, že som narazil na niečo veľké a prevratné. Bohužiaľ tento pocit zo mňa postupom času začal vyprchávať. 
 Niektoré piesne vo mne evokujú prvoplánovosť a páchnu umelinou. Prisladká balada Gotten s umraučaným Adamom Levinom (Maroon 5) pôsobí gýčovo. Dva zárezy Mylesa Kennedyho (Alter Bridge) Back from Cali a Starlight zaváňajú imitáciou interpretácie Roberta Planta z raných rokov Led Zeppelinu. Fergiena divoká Beautiful Dangerous vyzerá ako štetka, ktorá sa tlačí do postelí rádiovým staniciam. Cultovská Ghost Iana Astburyho chutí ako spálený odvar hard rocku. V mojich očiach tieto piesne zapadajú do tieňa šedého priemeru. 
 Album sa však môže pýši mnohými gurmánskymi kúskami. Ozzyho Crucify the Dead vyžaruje temnú sabbathovskú auru. Lemmy (Motörhead) v svižnej rockovej odrhavčke ospevuje svoju nesmrteľnosť. Z inštrumentálky Watch This budete len zízať ako Dave Grohl búši do bicích ako za čias Nirvany a Duff so Slashom zamorujú vzduch nekompromisnými tónmi. M. Shadows (Avenged Sevenfold) vám nedá ani vydýchnuť v priamočiarom Nothing to Say. Prekrásna vybrnkávačka Rocca De Luca s jemnými prvkami latina A Saint Is A Sinner Too pohládza srdce. Iggyho charizmatická We All Gonna Die je skvelá bodka na záver. 
   
 Slash zas raz dokázal genialitu svojho skladateľského umenia. V piesňach exceluje precítenými melódiami i robustnými elektrizujúcimi sólami, pri ktorých gitaru doslova znásilňuje. 
 Summa sumarum album vyznieva ako kolekcia hitov, medzi ktorými si každý milovník hudby príde na svoje. 
   

