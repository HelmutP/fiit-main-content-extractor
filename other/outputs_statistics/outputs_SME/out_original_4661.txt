
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Kravčík
                                        &gt;
                www.theglobalcoolingproject.co
                     
                 Rakúsko zavodňuje krajinu a my? Tunelujeme ju! 

        
            
                                    16.4.2010
            o
            1:02
                        |
            Karma článku:
                8.82
            |
            Prečítané 
            2203-krát
                    
         
     
         
             

                 
                    Začína sa hovoriť, že najväčším bohatstvom 21. storočia sa stala voda. Vo svete vznikajú vojnové konflikty kvôli vode. Zrejme my sme si to ešte nevšimli a stále máme pocit, že jej máme dostatok. Strategický zámer rezortu hospodárstva ťahať nové ropovodné potrubie cez Žitný ostrov len potvrdzuje ľahkovážnosť v rozhodovaní pri ochrane vody. Rusi uvoľnili zo štátneho rozpočtu 20 mld. USD na investovanie do vodných programov a pripravujú sa na distribúciu vody zo svojich oblastí bohatých na vodu do oblastí bez vody a mať z toho dobrý obchod. Rakúšania na začiatku 21. storočia začali zavodňovať poškodenú krajinu. A čo my? Nuž my usilovne pracujeme na tom, aby zo Slovenska čo najrýchlejšie odtiekla všetka dažďová voda, ktorá nás iba otravuje keď prší.
                 

                      Preto aj všetky peniaze z Eurofondov na ochranu vôd na Slovensku sa investujú iba a len na kanalizovanie vodných tokov, aby nedajbože neostala ani jedna dažďová kvapka na Slovensku. Dokonca existujú skupiny, ktore zosmiešňujú tých, ktorí sú proti vysušovaniu Slovenska. Až vzniká podozrenie, že niekto ich úmyselne za to platí, aby konflikt s vodou na Slovensku rástol.   Keďže Slovensko susedí s Rakúskom, poďme sa učiť od susedov, ako oni chránia vodu, ako posilňujú biodiverzitu i rozvíjajú technológie pre ochranu vôd. S láskavým dovolením prof. Michaela Steinera z Viedenskej univerzity ponúkam pre inšpiráciu fotografie  z projektov, realizované v partnerstve Federálneho ministerstva Rakúskych lesov (OBF) so Svetovým fondom na ochranu prírody (WWF) a Inštitútom ochrany prírody a krajinnej ekológie Viedenskej univerzity. Prvé projekty boli zrealizované začiatkom 21. storočia a za desať rokov sa zavodnilo niekoľko tisíc hektárov  poškodených lesov, ktorým sa až tak dobré darí, že 6 z nich je už zapísali do Ramsarských lokalít na ochranu mokradí.    Na Slovensku je to presne naopak. Príkladom sú Tatry, kde namiesto prekvitania prírody prekvitá konflikt medzi zainteresovanými. Príroda chradne pred očami a záujmové skupiny do zblbnutia sa hádajú, kto má pravdu. Ponúkam zopár fotografií z Rakúskych projektov.         Rakúsky inštitút pre ochranu prírody vymyslel štetovnicové hrádzky. Na začiatku ich ručne osádzali. Iste to bolá práca namáhavá a málo vykonná, o čom svedí obrázok.           Preto osadzovanie štetovnicových hrádzi zmechanizovali a tak sa ich osadzovanie urýchlilo.       Štetovnice osadené do bahna vydržia aj niekoľko desaťročí.       Spevňovanie štetovníc zvýšuje funkčnosť hrádzi. Na vhodných miestach osadzovali aj zrubové hrádze. V bahne sú hrádze praktický nezničiteľné, lebo sa zakonzervujú.       Hĺbka osadenia jednotlivých štetovníc je závislá na štruktúre zeminy a veľkosti frakcií, ktoré môžu brániť hlbšiemu zapichnutiu štetovníc.       Štetovnice sa horizontálne urovnávajú pílou, aby to malo aj estetický tvar s prepadom v prostriedku .       Stúpnutie hladín vody podporuje rast vegetácie.       Už po ročnej prevádzke štetovnice sa strácajú v krajine                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (33)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Poloz na hrad!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Zavšivavená spoločnosť trollami s podporou SME?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Bratislava šiestym najbohatším regiónom EÚ. Západné Slovensko 239-tým…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Nemajú Boha pri sebe!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Kravčík
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Kravčík
            
         
        kravcik.blog.sme.sk (rss)
         
                                     
     
         Presadzujem a podporujem agendu „VODA PRE OZDRAVENIE KLÍMY“. Jej cieľom je posilnenie environmentálnej bezpečnosti prostredníctvom zodpovedného prístupu v ochrane prírodného a teda i kultúrneho dedičstva. Napĺňanie agendy, založenej na prijatí novej, vyššej kultúry vo vzťahu k vode, môže na Slovensku vytvoriť viac ako 100 tisíc a v Európe vyše 5 miliónov pracovných príležitostí. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    629
                
                
                    Celková karma
                    
                                                7.10
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Povodne
                        
                     
                                     
                        
                            Hladujúci potrebujú vodu
                        
                     
                                     
                        
                            Klimatická zmena
                        
                     
                                     
                        
                            VODA zrkadlo kultúry
                        
                     
                                     
                        
                            http://s07.flagcounter.com/mor
                        
                     
                                     
                        
                            Nová vodná paradigma
                        
                     
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            http://moje.hnonline.sk/blog/4
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Kandidáti za poslancov do EP
                                     
                                                                             
                                            Ladislav Vozárik
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            http://www.clim-past.net/2/187/2006/cp-2-187-2006.pdf
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.aktualne.centrum.cz/blogy/jana-hradilkova.php
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hospodarskyklub.sk
                                     
                                                                             
                                            ashoka.org
                                     
                                                                             
                                            bluegold-worldwaterwars.com
                                     
                                                                             
                                            holisticmanagement.org
                                     
                                                                             
                                            theglobalcoolingproject.com
                                     
                                                                             
                                            ludiaavoda.sk
                                     
                                                                             
                                            watergy.de
                                     
                                                                             
                                            waterparadigm.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nemajú Boha pri sebe!
                     
                                                         
                       Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                     
                                                         
                       Ako sa z jednej ochranárskej ikony stala obyčajná nula
                     
                                                         
                       10 rokov po víchrici v Tatrách stále v zákopoch
                     
                                                         
                       Primátor všetkých Košičanov?
                     
                                                         
                       Dnes zasadá Vláda v Ubli
                     
                                                         
                       Aspoň pokus o integráciu Rómov? Za primátora Rašiho? Zabudnite!
                     
                                                         
                       Róbert Fico je bezpečnostným rizikom pre Slovensko
                     
                                                         
                       Kto je zodpovedný za pád starenky do kanalizačnej šachty v Michalovciach
                     
                                                         
                       Zdravé Košice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




