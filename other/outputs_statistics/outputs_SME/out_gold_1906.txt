

  
    
 Teodor Jozef Mousson (1887 Högyész – 1946 Trenčín), žil a tvoril v Michalovciach v rokoch 1911 – 1944. Do Michaloviec ho odsunula školská správa Rakúsko-Uhorskej monarchie čo sa mu v budúcnosti zmenilo na utrpenie. V roku 1918 totiž vzniklo Československo a jeho ako Maďarského učiteľa už nepotrebovali. Bol prepustený a preto často musel chytať do ruky štetec a farby. 
 Za druhej svetovej že vraj po krčmách predával obrazy po 50 korún. Dnes sa ich cena vyšplhá aj na stonásobok pôvodnej ceny. 
 Mousson napriek tomu aký mal život, býval pre Michalovčana na ozaj romantickom mieste. Nazývame to  miesto "Hrádok". Na mieste terajšej hvezdárne mal svoj dom. Vyššie je kaplnka - mauzóleum rodiny Stárayovcov - grófov z Michaloviec. Vrch oddávna nazývaný Hrádkom je opísaný už v encyklopédií o Rakúsko- Uhorsku svojho času ako vojensky strategické miesto. Z jednej strany že vraj nedobytné  a z druhej s dobrým prístupom. Jozef T. Mousson mal teda strategické bývanie. Videl na mesto i na okolie. Každé ročné obdobie skúmal pôsobenie slnečných lúčov, ich hru a snažil sa zachytiť realitu z akej vyžaruje samo svetlo. Svetlo, ktoré konzervuje prítomný okamžik, svetlo ktoré je v prítomnej chvíli chvíli tu, a zároveň niekde inde. Vitajte vo svete impresionizmu! 

   

   
 Michalovce J. T. Moussona  vtedy a dnes 
   
   
  
 Jeho dcéry? Kam kráčajú? V pozadí je Hrádok s kaplnkou na vrchu. Za ňou mal svoj dom. 
   
  
 Toto nie je určite miesto odkiaľ majster Mousson maľoval. Je to jediné ktoré sa približuje originálu na obraze. To pravé miesto je inde. Tak prečo som neodfotografoval ohyb Laborca s pozadím "Hrádku" z toho správneho miesta? Pretože sa nedalo. Mousson si postavil svoj maliarsky stojan zrejme tam kde je dnes hrádza pre reguláciu toku. Možno za jeho čias  sa tam dalo kúpať. Z tohto bodu hrádze by to sedelo, lenže pán Mousson nemal foťák a maľoval bez ohľadu na panoramatické videnie. Teda mne sa do foťáka nezmestil ohyb Laborca a zároveň hrádok v pozadí. Majster Mousson to dokázal. Možno vedome, možno nevedome ... 
 A prečo je tam dnes tak málo vody? No práve preto sa budovala pri Michalovciach vodná priehrada Zemplínska Šírava, aby sa Laborec nevylieval. A preto je dnes v Laborci miestami tak málo vody. Ale opravte ma, tiež mám pocit že som fotil nejaký kanál:-) 
   
  
 Trh s grécko-katolíckym kostolom v pozadí. A dzivky še chvaľa co novoho kupiľi:-) 
  
 No tak toto len hádam. Všimnite si ten oblúk pod strechou veže. To sa nepodobá. Lenže rímsko-katolícka veža o pár desiatok metrov ďalej je už úplne iná. Takže možno to ani Michalovce nie sú, alebo majster improvizoval ... 
   
  
   
  
 dnes je to budova ktorá patrí Zemplínskemu múzeu. Je  to dnes galéria, ale aby bol obraz úplný, patrí k tomu aj fotka nižšie: 
  
 Táto budova patrila miestnym grófom a slúžila ako sýpka. K tomu spomeniem že za druhej svetovej vojny po akciách partizánov v okolí a po zabití vinníkov okupantmi zobrali fašisti násilým niekoľko chlapov z vypálenej obce Vinné a okrem iného  mali vybrať práve túto sýpku. Dá sa do nej dostaňť cez okno od parku.:-) 
   
  
 Škola, židovská škola, za Slovenské štátu škola pre Nemecké deti. Myslím že hitlerjugend. Dnes (alebo včera?:-) Hostinec Tatra. V podstate je v tej budove krčma a predaj mäsa. 
   
  
 No nemám pravdu?:-) 
  
   
  
 V pravo kašteľ nevidno, nezmestil sa do záberu. Pán majster Mousson to dokázal.:-) 
   
    
 A ešte niektoré z obrazov J. T. Moussona 
 
  
   
   
  
   
   
  
   
  
   
  
   
  
   
  
   
 Miesto kde J. T. býval 
  
  Dnešná hvezdáreň na mieste majstrovho domu. 
   
  
 Tento múrik s plotom zámerne zachovali ako pozostatok z domu J. T. Moussona 
   
  
 Takúto pekne gotickú kaplnku mal hneď za domom ...  
   
 Vždy som chcel napodobňovať Mussonov impresionizmus. Jeho obrazy sú tak živé ... Nie je tam fotografická presnosť v zmysle precíznosti, ale je tam presnosť vnímania farieb mimo čas a priestor. A predsa sa mi fotografia reality zobrazí kdesi v mozgu ak sa dívam na obrazy majstra Moussona. Ale to len duch nahliadol pocit, ktorý inak nezachytíme, len ak cítime túžbu umelca po zobrazení krásy.  
  Nikdy som jeho impresie nedosiahol, vždy zostane pre mňa neprekonaný. Alebo možno raz? ....  
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   

