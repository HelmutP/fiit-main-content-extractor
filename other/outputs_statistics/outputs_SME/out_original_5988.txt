
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kotrčka
                                        &gt;
                Cestovanie a iné
                     
                 Fínsko, fínsky, fínske... 

        
            
                                    6.5.2010
            o
            18:39
                        |
            Karma článku:
                2.67
            |
            Prečítané 
            827-krát
                    
         
     
         
             

                 
                    Niečo o mojej obľúbenej krajine.
                 

                 Teda takto - obľúbených krajín mám viac - o čom svedčí aj nedávny blog o nákupe učebnice dánčiny. Ale Fínsko a fínske veci sú teraz takou mojou druhou láskou (prvú si nechám pre seba :-D )   Ani neviem, ako sa to stalo. Kedysi som sa o Fínsko nezaujímal, bral som ich ako "nazi" národ, teda u mňa spadali pod rovnakú skupinu antipatií ako Nemecko, Taliansko. No asi pred dvoma rokmi som postupne začal Fínsko a pridružené veci spoznávať bližšie.  Ako najlepšie spoznať krajinu? Cez jazyk, jasná vec. Tak som sa vrhol na fínčinu. Sakra ťažké a išlo mi to sakra pomaly. Perkele :-) Ale dá sa - a dovolím si tvrdiť, že keď sa človek naučí fínsky, nie je už žiade jazyk príliš ťažký. Teda ak si odmyslíme mimozemské jazyky ako arabčina a kórejčina :-) Po zdarnom zdolaní základov fínčiny som sa rozhodol, že skúsim spoznať nejakého rodeného Fína. Ešte lepšie Fínku :-) pre chlapa je to lepšia inšpirácia na učenie. A nebol by som to ja, keby som v dobe internetu nezačal budovať kontakt prostredníctvom klasickej pošty. Ale funguje to - a dokonca odpadá pokušenie nahádzať text do Google Translatora. Odporúčam.  Dokonca som sa týmto spôsobom dostal aj ku klasickým fínskym "pochúťkam" zvaným Salmiakki. Čo to je? Je to tradičná (aj keď je to sporné, či zhruba 100 rokov je už národná tradícia) "sladkosť", najčastejšie v podobe cukríkov, no salmiakki náplň sa nevyhne ani čokoládam a podobne. Sladko-slaná (teda skôr nijako-slaná) chuť, či dokonca varianta s extra slanou chuťou - proste skvelé prekvapenie pre neznalého. No po pár pokusoch to začne chutiť - prekvapivé, no je to tak. Aspoň u mňa.  Dobre, ale fínsko nie je len Salmiakki. Super krajinka, ktorú je dobré spoznať. Áno, priznávam sa, že ešte som tam nebol. Ale napchaný informáciami som už poriadne, možno viem o jednotlivých miestach aj viac ako bežný Fín :-) No dobre, možno nie, ale na Slováka viem dosť, iste podstatne viac, ako že hlavné mesto je Oslo (veru tak, Slováci si Fínsko často mýlia s Nórskom) a že je tam zima. Už fakt len nasadnúť na dopravný prostriedok a dogúľať sa na miesto činu.  Jaj, aby som nezabudol, na trénovanie jazyka je vhodné popočúvať originálny fínsky prejav - takže odporúčam fínsky rozhlas YLE, najmä okruhy Radio Suomi (asi netreba prekladať) a YLE Puhe (YLE Reč, Rozprávanie). Prípadne, ak sa chcete naučiť helsinský slang, tak okruh pre mladých YleX. Plus hudba, aktuálne "fičím" na Anne Eriksson (nie, ona nerobí telefóny, spieva).  Toľko o Fínsku odo mňa, zatiaľ nezasväteného. Ak budem vedieť (a zažijem) viac, tak sem pribehnem a budem referovať. Näkemiin. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Správy STV/RTV:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Červená tabuľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            02 je drahé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Dlhodobý problém 02
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Prečo nie je 02 môj hlavný operátor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kotrčka
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kotrčka
            
         
        kotrcka.blog.sme.sk (rss)
         
                                     
     
        25/184/72 - narodený v deň, keď má meniny Adolf, IT, hamradio a cyklofanatik...

 
  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    63
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1383
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            OpenSource
                        
                     
                                     
                        
                            Fotografovanie
                        
                     
                                     
                        
                            Cestovanie a iné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sex na pracovisku
                     
                                                         
                       Má Ficova vláda prepojenie na scientológov?
                     
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Privatizácia Smeru
                     
                                                         
                       Cargo – obrovské dlhy, vysoké platy a pokútne spory
                     
                                                         
                       Na ceste po juhovýchodnom Poľsku
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Počiatkov pochabý nápad: zadržiavanie turistov za 30 EUR
                     
                                                         
                       "Rómsky problém"
                     
                                                         
                       Rómska otázka – aké sú vlastne fakty?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




