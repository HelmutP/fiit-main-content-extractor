
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Draxler
                                        &gt;
                Politika
                     
                 Kto zostaví vládu po budúcich voľbách? 

        
            
                                    13.6.2010
            o
            11:53
                        (upravené
                9.1.2011
                o
                10:12)
                        |
            Karma článku:
                5.82
            |
            Prečítané 
            2590-krát
                    
         
     
         
             

                 
                    Slovensko si prežilo v zásade pekné, pokojné voľby. Samotná kampaň, naštastie, nebola aj napriek ostrejším momentom výrazne špinavá. A, ako to už na Slovensku býva, skôr zameraná na billboardy a tlačovky. Inak strany plávali v mediálnom mori krátkodobých reakcií a protireakcí. Televízne diskusie boli mimoriadne plytké. Ale určitá insitnosť politickej scény má výhodu v tom, že s výnimkou partajných kruhov a viac intelektuálne orientovaných skupín politiku ľudia vnímajú relatívne vyrovnane. Voľby boli skôr spoločenskou udalosťou a občianskym sviatkom.
                 

                    Navyše poskytli celkom príjemné divadlo, drámu až do samotného záveru. Ešte pred niekoľkými mesiacmi to vyzeralo na pohodlné pokračovanie vládnutia doterajšej koalície. Potom sa začal voľný pád dvoch satelitov Smeru, ktorý trval až do konečného sčítania hlasov. Výsledok: SNS sa takmer nedostalo do parlamentu a HZDS svoju púť na národnej scéne nateraz (a možno navždy) skončilo.   Končí tak jedna éra, Vladimír Mečiar bol napriek všetkým svojim chybám výrazným a v mnohých kľúčových momentoch aj zodpovedným politikom. Tak trochu romantik so slabosťou pre patetické gestá a silové hry. Jeho trochu prehnaná démonizácia súvisí aj s tým, že slovenskí intelektuáli si len hmlisto uvedomujú, ako spoločnosť v 90. rokoch vyzerala a aké intelektuálne kapacity boli k dispozícii. Vladimír Mečiar kočíroval HZDS, ktoré bolo v mnohých ohľadoch monštrom, ale tých možností zase tak veľa nebolo. Teraz už hádam ale ostane na zaslúženom politickom dôchodku.   Príjemným momentom volieb je úspech strany Most-Híd. Zo slovenského parlamentu sa stráca orbánovská SMK. Navyše, Béla Bugár si úspech zaslúži. Je jedným z mála naozaj vecných slovenských politikov a má okolo seba väčšinou veľmi kompetentných ľudí. A to, že Most predsa len nie je výhradne maďarskou stranou, je veľmi príjemným signálom na medzinárodnej úrovni aj dovnútra slovenskej spoločnosti.   Rozpačitejšie pôsobí vysoký výsledok novej strany SaS. Rozhodne platí, že trochu zlepšila kvalitu politického dialógu tým, že má materiály písané pomerne jasným jazykom, snaží sa komunikovať technicky. Na druhej strane povrchná vecnosť zakrýva mimoriadne zjednodušený pohľad na svet, ktorý títo „burziáni z Miletičky" majú. V tejto chvíli ale treba uznať, že zabojovali lepšie, ako si to mnohí, vrátane autora týchto riadkov, ešte nedávno predstavovali.*   Nepríjemným momentom volieb je vysoký výsledok Smeru - aj keď tu zrejme došlo k posunom v tom zmysle, že Smer mnohých svojich sympatizantov strácal a zároveň k nemu prechádzali doterajší voliči jeho satelitov. Strana si zaslúžila dostať po nose oveľa výraznejšie. Treba uznať, že médiá by Smer znášali pod zem aj v prípade, že by vládol oveľa zodpovednejšie. Strana síce išla do vlády evidentne nepripravená, ale mnohé kroky v začiatkoch vládnutia boli úplne legitímne. Napríklad zastavenie privatizácie či zákaz komerčnosti zdravotného poistenia. Postupom času ale biznismeni zo Smeru pri pohľade na plný stôl štátnych financií doslova zbesneli. A vláda naozaj nerobila nič dlhodobé, koncepčné pre krajinu. Strana papalášov sa zameriavala na arogantné gestá ako predvolebné umiestnenie megasochy „kráľa" Svätopluka na hradné nádvorie.   Nová vláda bude naozaj „zlepencom". A hneď na začiatku bude musieť riešiť zopár bonbónikov. Svoj postoj k záchrane Grécka. Alebo čo s nerealistickým a čiastočne nezmyselným receptom zvaným „odvodový bonus", ktorý je vlajkovou loďou SaS. Nová vláda bude musieť dramaticky upratovať verejné financie. Nebude to populárne.  Iveta Radičová, zrejme budúca premiérka, je navyše nešikovnou komunikátorkou. Mimochodom, jej predchodca  na straníckom poste, Mikuláš Dzurinda, bol veľmi zlým rečníkom. Hovoril takmer výhradne v zle zostavených vetách, plných balastu, bez pointy a ešte sa pritom neuveritelne pateticky tváril. Radičová je len čiastočným zlepšením a je len otázkou času, kedy začne občanov svojím roztekaným, pseudoštátnickym prejavom silnejšie iritovať.   Čo bude pri ďalších voľbách, ktoré pokojne môžu prísť aj predčasne? Dokáže sa Smer aspoň čiastočne zreformovať? Je to jedna z kľúčových otázok dneška. Zrejme ťažko. Vrchné etalóny strany sú prelezené mimoriadne bezškrupulóznymi ľuďmi. Ak by strana brala reči o „dôraznej obrane národnoštátnych záujmov" dôsledne, musela by niektorých svojich vysokých funkcionárov rovno postaviť k múru. Ale reformu si treba želať. Strana sa môže rýchlo dostať naspäť k moci. Pre krajinu by bolo veľmi prospešné, aby to už bol aspoň trochu iný Smer.   Strana môže ale tiež tvrdo doplatiť na odstavenie od moci a finančných tokov, ako aj na možné vyšetrovanie niektorých káuz.   Takže všetko je otvorené a Slovensko zrejme čaká obdobie nestability, ktoré môže vyústiť do dramatického preorania politickej scény. Každé nové riešenie, ťažiace z nespokojnosti občanov, treba postaviť na zodpovednej vnútrostraníckej práci a technokratickej príprave na vládnutie. To by malo byť poučením ostatných štyroch rokov.               *Tieto voľby pekne ukázali aj kvalitu predvolebných prieskumov. Keď MVK vo februári ukázala prudký nástup SaS, konkurenčný Focus, ktorý robil zber dát v rovnakom čase, ešte nič nezaznamenal. Išlo len o najvýraznejšiu ukážku toho, ako ktoré agentúry pracujú.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Čo sa stalo v Odese?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Vylomeniny amerických kongresmanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Niekoľko poznámok k Paškovmu obmedzovaniu novinárov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Balkánsky pop
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Draxler 
                                        
                                            Ruské béčkové akčáky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Draxler
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Draxler
            
         
        draxler.blog.sme.sk (rss)
         
                        VIP
                             
     
         Momentálne žije v Prahe, kde píše, skúma a vyučuje. Po rokoch strávených v západnej Európe je to príjemná zmena, aj keď to počasie by mohlo byť aj lepšie. Bloguje aj na http://blog.etrend.sk/juraj-draxler/. (Foto: European Alternatives Cluj) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    253
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3170
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Dôchodkové veci
                        
                     
                                     
                        
                            Cestopisy
                        
                     
                                     
                        
                            Praktické rady
                        
                     
                                     
                        
                            Impresie
                        
                     
                                     
                        
                            Ekonomické zamyslenia
                        
                     
                                     
                        
                            Moje alter ego
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Daňová debata (Filko, Marušinec, Draxler, Lehuta)
                                     
                                                                             
                                            Dôchodková debata na eTrende: Draxler, Filko, Chren
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Radůza
                                     
                                                                             
                                            Szidi Tobias
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomír Némeš
                                     
                                                                             
                                            Bobby (denník z ciest po Ázii)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Uniorbis - Svet univerzít
                                     
                                                                             
                                            Denník The Financial Times
                                     
                                                                             
                                            Denník The Guardian
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Balkánsky pop
                     
                                                         
                       List priateľke
                     
                                                         
                       Sanatórium Družba: V jednej z najčudnejších budov sveta
                     
                                                         
                       Načo sú dobré univerzitné rebríčky
                     
                                                         
                       Dodrží premiér Fico svoje vlastné pravidlá?
                     
                                                         
                       Som zdravotná sestra. Personál v slovenských nemocniciach? Otrasný
                     
                                                         
                       Aj zvieratá zomierajú lepšie ako pacienti v našich nemocniciach
                     
                                                         
                       Načo sú dobré univerzitné rankingy?
                     
                                                         
                       Harvard alebo nič
                     
                                                         
                       Naozaj potrebujeme viac policajtov, pán minister?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




