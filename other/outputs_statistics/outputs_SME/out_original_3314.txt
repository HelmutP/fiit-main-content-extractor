
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viera Šimkovičová
                                        &gt;
                Postrehy a komentáre
                     
                 Holokaust nie je minulosťou 

        
            
                                    25.3.2010
            o
            17:48
                        (upravené
                25.3.2010
                o
                18:42)
                        |
            Karma článku:
                8.00
            |
            Prečítané 
            1674-krát
                    
         
     
         
             

                 
                    25. marec má viacero prívlastkov. Je Dňom zápasu za ľudské práva, dňom, kedy si pripomíname bratislavský Veľký piatok z roku 1988, je i Dňom počatého dieťaťa. 25. marec je aj dňom pietnej spomienky na obete holokaustu. V prvom transporte, ktorý opustil popradskú železničnú stanicu 25. marca  1942 vo večerných hodinách, odviezli do koncentračného tábora v  Auschwitzi (Osvienčime) v Poľsku tisíc židovských dievčat z východného  Slovenska. V diskusiách o holokauste sa občas objaví názor, že je zbytočné zaoberať sa neustále udalosťou, ktorá patrí do dávnej histórie. Holokaust však nie je minulosťu.
                 

                 Táto historická trauma je každodennou realitou nie len pre tých, ktorí prežili peklo koncentračných táborov, ale aj pre milióny ďalších ľudí, nesúcich toto tragické dedičstvo vo svojom individuálnom či kolektívnom nevedomí. Tento fenomén nie je väčšine bežných ľudí veľmi známy. Vážne sa ním zaoberajú uznávaní odborníci. Podľa skúseností obetí a terapeutov, o ktorých sa môžeme dočítať napríklad na stránke českého bloggera Františka Kostlana, sa hrôzy holokaustu a šoa sa prenášajú medzigeneračne na druhú i tretiu generáciu obetí tejto traumy.   Úzkostné stavy, psychosomatické a onkologické ochorenia, poruchy prijímania potravy, bezdôvodný strach o vlastné deti, zážitky scén, ktoré vo svojom živote reálne nevideli – to všetko vychádza na povrch na terapeutických sedeniach. Potláčanie týchto zážitkov vedie k pocitu znecitlivenia, uzavretiu a izolovaniu traumy do „železnej skrinky“ odkiaľ ale musí raz, v záujme zachovania duševného aj telesného zdravia, vydolovaná prežitá, spoznaná všetka bolesť, strach a hrôza, ktorá sa s touto traumou spája.       Kolektívne nevedomie je podľa Junga sféru nevedomia, kde sa určité vzorce, zážitky a predstavy prenášajú medzi príslušníkmi určitej skupiny ľudí, rodiny, národa, i celého ľudského spoločenstva. Tento fenomén zrejme je jednou z možností vysvetlenia prenosu nespracovanej traumy z generácie na generáciu.   „Že člen európskej kultúrnej rodiny mohol dospieť až ku koncentračným táborom, vrhá povážlivé svetlo na všetkých ostatných.“ píše Jung. „lebo kto, konieckoncov, sme, aby sme mohli domýšľavo tvrdiť, že u nás by sa nič podobného nemohlo stať? ..S hrôzou sme si uvedomili, čoho všetkého je človek schopný a čoho by sme teda boli schopní aj my, a od tej doby v nás hlodá strašná pochybnosť o ľudstvu, kam patríme aj my.“   Prepracovať sa k tejto traume vôbec nie je jednoduché. Hovoria o tom aj  skúsenosti "detí  z fotografie"  „So sestrou sme o svojej traume nehovorili päťdesiat rokov“ hovorí autor  dokumentu a jedno zo zachránených detí. Prežili koncentračný tábor spoločne ako 8-10 ročné deti, a trvalo im paťdesiat rokov, kým, o tom boli schopní spolu rozprávať.   Okrem priamych obetí šoa a holokaustu, ktoré ich stále nosia buď zatvorené vo svojich „železných skrinkách“, alebo sa aktívne snažia s nimi konfrontovať, sú okolo nás  aj milióny potomkov druhej a tretej generácie, ktorí nosia túto traumu ukrytú  hlboko vo svojom nevedomí. Jednou z týchto mlčiacich skupín sú aj Rómovia na Slovensku. Rómsky holokaust-  porraimos, bol dlhé roky úplne tabuizovanou témou a dodnes nie je úplne zdokumentovaný. Aj druhá a tretia generácia Rómov nesie v sebe prenesené zážitky svojich predkov. Takmer každá rómska rodina má predka, ktorý bol počas holokaustu zavraždený. Nehovoria o tom, málokedy sa k tejto udalosti vracajú. Strach o seba a o svoje deti však majú. Možno i tu je jeden z koreňov ich súčasnej segregácie a nedôvery v "tých druhých". Nevedomky, ale bránia sa možnosti byť priradení k tejto skupine. Mojej priateľke v čase sčítania ľudu opakovane volala jej matka a úpenlivo ju prosila, aby sa nehlásila k rómskej národnosti. Je to náhoda alebo krutá skúsenosť prenesená cez generácie?   Holokaust ako trauma sa týka aj nás a našich predkov. Tí, ktorí mlčali, keď začali deportácie, ktorí bohatli a profitovali na arizáciách alebo na odmenách za ukrytie židovských spoluobčanov (nie všetci to robili nezištne), alebo tí, ktorí podporovali a podporujú fašistický slovenský štát, nesú zodpovednosť za svoje skutky, za svoje postoje i za svoje nekonanie. Hriechy otcov trestá Hospodin do štvrtého pokolenia, hovorí biblia.   Kolektívna zodpovednosť sa prenáša medzigeneračným prenosom aj na nás. Je možné, že následné hrôzy komunizmu boli následkom nášho nepriznaného hriechu, nenávisti a zášti, nesenej z generácie na generáciu už po storočia a prejavenej mlčanlivým súhlasom so „zbavovaním sa“ označeného nepriateľa. Prenasledovanie katolíckej cirkvi v komunistickom režime je možné vnímať v tomto kontexte ako následok jej podpory či neschopnosti čeliť nastupujúcemu a rozvíjajúcemu sa fašizmu. Snahy o to, aby bol Jozef Tiso blahorečený a považovaný za mučeníka, sú hlbokým nepochopením role, ktorú zohral vo vplyve na realitu nášho národa a tiež signálom ignorovania tieňa, ktorý si nesieme teraz už aj cez ďalšie generácie.   Hriech môže byť odpustený po tom, čo ho hriešnik oľutuje a rozhodne sa svoj život zmeniť. V božích očiach oľutovaný hriech prestáva existovať. Ale pozrieť sa pravde do očí nebýva ľahké. I keď sa hrdíme ako národ niekoľkými desiatkami ocenení „spravodlivý medzi národmi“ ktoré dostali ľudia, neváhajúci riskovať svoje životy pre záchranu prenasledovaných, je potrebné si uvedomiť, že tých, ktorí mohli niečo urobiť, a neurobili, je oveľa viac.   Prijať časť svojej zodpovednosti, identifikovať fakt, že to často bola zášť a chamtivosť, čo vyvolalo pocit, že to, čo sa deje, je v poriadku, že viera v boha môže ospravedlniť nenávisť a súhlas s vraždením, nájsť podstatu toho zlého v sebe aj cez pocit, že nás sa to netýka a že naše „železné skrinky“ ani neexistujú, to je úloha, ktorá stojí pred nami dnes. Uvedomiť si, že holokaust nie je minulosťou pre milióny ľudí, ktorých život denne poznačuje jeho strašné dedičstvo.   Nie, nehovorím o kolektívnej vine. Hovorím o ochote prijať zodpovednosť, ktorá nám, každému z nás prináleží. Spýtali sme sa svojich dedov, kde boli, čo robili a čo si mysleli keď sa to dialo? Vieme odkiaľ pochádza majetok, ktorý nadobudli naše rodiny v tom čase? Vieme spoznať, keď niekto hovorí a presadzuje názory, ktoré sú podobné tým, spred 65 rokov? Keď som sa spýtala jedného staršieho človeka z môjho bližšieho okolia, na to, čo vlastne bolo na židoch také zlé, čo ľuďom na nich prekážalo, povedal jednu vetu: „oni mali peniaze.“   Je zrejmé, že aj neustále návraty prejavov fašizmu a jemu podobných názorov nám pripomínajú súčasť nášho kolektívneho aj individuálneho tieňa, ktorú sme dodnes nespracovali. A načo by sme to mali robiť? Načo by sme sa mali zaoberať niečím podobným, vonkoncom nie príjemným a pohodlným? Pretože je tu šanca, ktorá nás môže posunúť ďalej, ako píše Jung: „Konfrontovať človeka s jeho tieňom znamená ukázať mu aj jeho svetlo.“    „Spása“ totiž neznamená, že je niekomu odňaté bremeno, ktoré nemienil niesť. Ako neznesiteľný je človek sám sebe, to prežíva len ten, kto je úplný.“    „Keby tak ľudia vedeli, aký to znamená zisk, keď človek nájde svoju vlastnú vinu, a aké zvýšenie duševnej úrovne!“   Opak môže znamenať problém, ktorý sa bude niesť do budúcnosti, na ďalšie generácie. Pre niektorých jednotlivcov ( a možno aj pre národ) môže mať aj fatálne dôsledky.   Gabi Neumann, autor spomínaného dokumentu v diskusii k filmu rozprával príbeh jedného zo svojich spolupútnikov, s ktorým sa mal niekoľkokrát stretnúť. Keď ich stretnutie už bolo takmer isté, dostal správu, že jeho kamarát náhle zomrel. Skúsenosti z Osvienčimu boli pre neho tak bolestné, že radšej, než by mal hovoriť o svojich zážitkoch, hoci s človekom, ktorý ich s ním zdieľal, zomrel, na stretnutie neprišiel.   Zdá sa to nepravdepodobné? „Ak sa dostaví osudovo požiadavka sebapoznania, a my sa jej vzoprieme, potom tento odmietavý postoj môže znamenať aj skutočnú smrť... Nevedomie má tisíce spôsobov, ako prekvapivo rýchlo ukončiť existenciu, ktorá nemá zmysel.“ uvádza Jung.   Aj okolo nás chodia ľudia, ktorí nesú živú ranu, možno pochovanú hlboko vo svojom vnútri, ktorá je ich vnútornou realitou a ovplyvňuje ich život. Vieme, že nesieme zodpovednosť. Či ju prijmeme a pomôžeme tak liečiť rany sveta, na ktoré máme dosah, je na každom z nás. Tak prijmeme výzvu, ktorú nám prináša táto požiadavka sebapoznania.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (63)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Milujú katolíci len tých, ktorí sú na kolenách?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Slovensko plné šťastných gayov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Rastú, keď sa nepozeráme
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Za plotom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Vyhodiť či nevyhodiť.. to je otázka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viera Šimkovičová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viera Šimkovičová
            
         
        vierasimkovicova.blog.sme.sk (rss)
         
                                     
     
        Veľkosť človeka sa nemeria iba výšinami, na ktorých spočinula  jeho noha, ale aj temnými roklinami a prepadliskami, ktoré dokázal zdolať pri ceste do výšin...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    62
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2056
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Postrehy a komentáre
                        
                     
                                     
                        
                            Životné cesty
                        
                     
                                     
                        
                            Príbehy
                        
                     
                                     
                        
                            Psychológia a spiritualita
                        
                     
                                     
                        
                            Rozprávkovo
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Neonacizmus nie je zlo
                                     
                                                                             
                                            Scott Peck: Lidé lži
                                     
                                                                             
                                            Temnota je proto aby bylo světlo
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Scott Peck: Odmítnutí duše
                                     
                                                                             
                                            Christopher Paolini: Brisingr
                                     
                                                                             
                                            Robert Fullghum: Ach jo
                                     
                                                                             
                                            Scott Peck: Svět který čeká na zrození: Návrat k civilizovanosti
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janette Maziniová
                                     
                                                                             
                                            Katarína Pišútová
                                     
                                                                             
                                            Miro Kocúr
                                     
                                                                             
                                            Vlado Schwandtner
                                     
                                                                             
                                            Zuzana Roy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Mokranovci
                                     
                                                                             
                                            Škola Fantázia
                                     
                                                                             
                                            Annwin
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zápisky o ľuďoch – v inom živote
                     
                                                         
                       Opentaní chlapci, bojovať!
                     
                                                         
                       Aké bude referendum o rodine? Drahé a posledné
                     
                                                         
                       Porozumenie medzi ateistami a veriacimi
                     
                                                         
                       Referendum o rodine: Slušní ľudia, ktorí vám ukradnú slobodu
                     
                                                         
                       Keď si Monika ugrgne z pravdy
                     
                                                         
                       Keď som stála nad hrobom
                     
                                                         
                       (Prečo) sú katolíci stádo?
                     
                                                         
                       Snahy o nápravu chýb v RKC v súlade s vierou a láskou
                     
                                                         
                       Varíme s medveďom - Kačacie prsia s gaštanovou omáčkou
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




