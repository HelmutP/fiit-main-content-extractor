
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriela Oremová
                                        &gt;
                Nezaradené
                     
                 Čo sa deje? 

        
            
                                    20.5.2010
            o
            20:56
                        (upravené
                20.5.2010
                o
                22:24)
                        |
            Karma článku:
                5.70
            |
            Prečítané 
            435-krát
                    
         
     
         
             

                 
                    Vonku je strašné počasie. Náš psík sa skrútil v pelechu a len občas jedným okom mrkne, či náhodou neodchádzame všetci preč z domu, nerád ostáva sám a spí ďalej. Hádam by prespal celé tieto usmrkané,chladné dni. Na človeka má toto daždivé obdobie podobný vplyv. Máme šťastie, nevytápa nás, hoci nám voda vymyla po záhrade množstvo brázd, katastrofy nás zatiaľ obchádzajú. Len nikto nevie, či z toho, čo sme zasadili aj niečo narastie. V záhrade a na dvore sa nič robiť nedá ,tak skúsim niečo vymyslieť dnu.
                 

                 Zapnem počítač a píšem blog. Vo chvíli, keď som takmer na konci, pripojenie zmizne a definitívne. Nič to, veď zajtra to snáď bude lepšie, zavolám dcére ako pokračuje s diplomovlou. Ale ani prezvoniť sa mi nedá, signál nemám ani na jedinú čiaročku.    Tak by som si mohla niečo pozrieť v televízii. Prepínam a prepínam, ale rýchlo ma to prestane baviť. Medzitým sa totiž spustil riadny lejak a ani jeden kanál nefunguje čisto. Istejšie bude všetko povypínať, zdá sa mi, že v diaľke počujem hrozivé hrmenie.   Zasiahol nás chvost strašnej búrky. Krúpy podierkovali aj tak dosť biedne listy rastliniek a dolu dvorom sa valí mútna voda. Východ je na tom určite horšie, temné olovené mraky na tej strane neveštia nič dobré. Naša pivnica je suchá a Ipeľ drží hrádza. Z tejto strany nám nehrozí nebezpečenstvo.   Ľudia v prestávkach medzi dažďovými vlnami vychádzajú skontrolovať svoje dvory a záhrady. So strachom pozerajú na tmavé nebo. Mnohí hovoria, že nič také ešte nezažili a že to má na svedomí sopka na Islande.   Čo sa to vlastne deje? Písala som už raz o tom, že len tak náhodou sa nič nestane. Určite aj na takéto extrémy existuje reálne vysvetlenie. Niečo som raz začula v televíznych novinách, ale nie od meteorológov, tí sa vyjadrujú  k problému opartne.   Zapálila som sviece presne tak, ako v časoch keď nebola televízia, internet, ani mobilné telefóny. Všade sa rozlieha hrobové ticho. Dokonca ani lietadlá nad nami nepočuť. Príroda im pristrihla krídla. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Skúsim
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Miesto pre život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Vôbec nechápem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Nečakaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Zázračná hruška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriela Oremová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriela Oremová
            
         
        oremova.blog.sme.sk (rss)
         
                                     
     
        Som presvedčená ,že ak človek do svojho zámeru vloží silu svojej duše a teplo svojho srdca, celé nebo sa spojí, aby mu pomohlo!

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




