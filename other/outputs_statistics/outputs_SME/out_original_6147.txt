
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Trnovec
                                        &gt;
                Nezaradené
                     
                 Deň matiek 

        
            
                                    9.5.2010
            o
            8:50
                        |
            Karma článku:
                7.66
            |
            Prečítané 
            1168-krát
                    
         
     
         
             

                 
                    Dnes je Deň matiek. Každý z nás má matku. Niekto má šťastie, že ju zažil a si ju pamätá, iný ju má živú a sa ju opatruje, iný je vo veku, kedy sa o neho ešte stará matka. Ale aj tí, ktorí ju majú vymazanú krutým osudom od detských dôb, majú matku.
                 

                     Dnes je Deň matiek.   Každý z nás má matku. Niekto má šťastie, že ju zažil a si ju pamätá, iný ju má živú a sa ju opatruje, iný je vo veku, kedy sa o neho ešte stará matka. Ale aj tí, ktorí ju majú vymazanú krutým osudom od detských dôb, majú matku.   Materstvo je dnes zaznávané a pri tom cenené. Možno práve preto treba hovoriť o materstve ako o dare, ktorý nemôže mať každá žena. Materstvo nie je samozrejmosť. Každý z nás vo svojom najbližšom okolí vidí, ako sa materstvo odmieta. Dôvody sa rôznia. Každý, kto sa z nás rozhodne neprijať dieťa, má na to svoj dôvod. Kto sa rozhodne prijať dieťa, má tiež preto dôvod. Dieťa nesmie byť hračkou v rukách rodiča. Dieťa má svoju dôstojnosť.   Niekedy sa prijíma dar nového života aj cez slzy, ktoré sa zvyknú premeniť v žiare životodarného slnka na drahokamy. Materstvo je darom, ale aj obetou. Prijíma sa s bázňou. Je zahalené tajomstvom spoločného úseku života, kde je dieťa bytostne naviazané na matku a pri tom je od počiatku osobité, originálne. Rozhodnutím mu slúžiť. Dieťa cíti, či sa prijíma s radosťou alebo nevôľou, či je milované alebo nechcené. Matka dáva základy novému človeku, ovplyvňuje jeho celú budúcnosť. To my, muži, nie sme schopní. Preto si ctím matky, preto ich oslavujem.   Mám veľké šťastie, že ma moja mamka prijala. Vo veľmi ťažkých časoch.   Mám veľké šťastie, že moja manželka bola ochotná sa stať matkou. Veľakrát.   Mám veľmi veľké šťastie, moje deti s manželkami sú ochotné stať sa matkami.   Všetkým za to veľká vďaka!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Je ešte Rakúsko katolícke?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Nepôjdem voliť! Prečo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Bojím sa otvoriť chladničku! Alebo kultúrny boj?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Strata zdravého rozumu, alebo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Trnovec 
                                        
                                            Diera v železnej opone
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Trnovec
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Trnovec
            
         
        trnovec.blog.sme.sk (rss)
         
                                     
     
         Som slobodný človek. Sloboda je okrem života najfantastickejší dar, ktorý som dostal. Denne stojím v nemom úžase pred zázrakom života, tohto tajomstva sa neviem nabažiť. Každý deň je DAR, za ktorý nestíham ďakovať. Predseda Klubu mnohodetných rodín, viceprezident FEFAF, člen administratívneho výboru COFACE. Teda písať budem o rodine. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    133
                
                
                    Celková karma
                    
                                                5.58
                    
                
                
                    Priemerná čítanosť
                    749
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Je ešte Rakúsko katolícke?
                     
                                                         
                       Nepôjdem voliť! Prečo?
                     
                                                         
                       Bojím sa otvoriť chladničku! Alebo kultúrny boj?
                     
                                                         
                       Strata zdravého rozumu, alebo?
                     
                                                         
                       Ako trestáme korupciu: Za miliónové tendre podmienka, za 10 kíl ryže 5 rokov natvrdo
                     
                                                         
                       Zmena ústavy je dobrou správou, cena je však vysoká
                     
                                                         
                       EÚ a tradičné hodnoty
                     
                                                         
                       Zajtra hrozí sedem kresiel pre SMER a s ním aj útok Bruselu na posledné zvyšky suverenity
                     
                                                         
                       10 dôvodov, prečo má Slovensko nádej
                     
                                                         
                       Kto je chudobný?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




