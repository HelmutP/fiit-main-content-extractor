
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Piršč
                                        &gt;
                Nezaradené
                     
                 V autobuse MHD 

        
            
                                    24.3.2010
            o
            4:38
                        (upravené
                19.3.2012
                o
                18:52)
                        |
            Karma článku:
                7.59
            |
            Prečítané 
            1778-krát
                    
         
     
         
             

                 
                    Každý, kto pravidelne cestuje domov z práce autobusom MHD, pozná to otupné ticho z únavy, ktoré nesú vo svojich mysliach cestujúci, ktorým pred chvíľou skončila pracovná doba. Muži i ženy apaticky nechávajú plynúť svoje myšlienky bez obmedzenia - pracovné povinnosti sa miešajú v hlave so súkromnými problémami.
                 

                     Do takéhoto zamĺknutého autobusu som nastúpil jedného letného popoludnia s kolegom, na ktorého som počkal pred bránami polygrafického závodu, kde pracoval, a ktorému sa krátko predtým skončila ranná šichta. Viezli sme sa do centra mesta, mali sme šťastie, že sa nám ušlo miesto na sedenie, lebo niektorí cestujúci sa mlčky držali tyče nad sebou a oddychovali po celodennej práci postojačky.   Na ďalšej zastávke pristúpili do autobusu, okrem ďalších zmorených tvári, aj dve mladé dievčatá, odhadol som ich vek na šestnásť - sedemnásť rokov, z ktorých čerstvá mladosť a bezstarostná prostoduchosť priam sálala.   Postavili sa do uličky vedľa mňa a jedna z nich si zrejme v zápale predchádzajúcej debaty neuvedomila, že v autobuse nie je taká rušná atmosféra ako vonku na autobusovom nástupišti, lebo hlasno pokračovala v popisovaní svojich nedávnych zážitkov, a tak odhaľovala svoje vnútro nielen svojej priateľke, ale aj prítomným spolucestujúcim. Veľká časť z nich vyrušená z uvažovania a snenia sa myšlienkami vrátila späť do prítomnosti a nechtiac sa započúvali do monológu čulej devy, ktorej tenký dievčenský hlas prerezával ospalé ticho ako v divadle:   „No a potom sme hrali karty - tri hore. Ja som si objednala ešte jednu vodku s džúsom, Lucka pohár vína a Lenka, jedna kamoška z inťáku, kolu. O šiestej prišiel do reštiky Karol a priviedol zo sebou Mariána. Vieš ktorého? Toho debila zo štvrtej be, čo tak šalie za Petrou. Karol si sadol k Lucke a Lenke a Marián ku mne. No a tak sme hrali karty piati. Ja som si zapálila cigu a ten debil Marián sa začal frajerčiť. Začal mi lichotiť, že mám pekné vlasy, či by som mu nedala potiahnuť šluka, a aby som mu dala svoje číslo. Debil! Akurát jemu by som dala moje číslo na mobil. Však ja mám svojho Peťa, nie? No a Peťo sa ma na druhý deň pýtal, kde som bola celý večer. Vieš, Peťo je dakedy riadne žiarlivý..."   Asi päťdesiatročný útly chlapík s briadkou, ktorý sedel na druhej strane uličky, pravdepodobne mal mimoriadne ťažký deň v robote, lebo už nevydržal počúvať jej hlasité výlevy, zdvihol sa prudko zo sedadla, poklepal mladú študentku po pleci, a keď sa k nemu prekvapene otočila, uvoľnil teraz on svoju ťarchu emócií dôrazným tónom:   „Počúvaj ma dobre! Na najbližšej zastávke vystúpiš!!! Ak okamžite neprestaneš tu predvádzať svoju obmedzenosť, pôjdem za vodičom a poviem mu, aby ťa vylúčil z dopravy! Kto ma, preboha, počúvať tie tvoje kraviny! Nikto v autobuse nie je zvedavý na tvoje ciťáky! Jasné?"   Nechám na čitateľovi, nech posúdi, či také rázne prerušenie reči cudzou osobou bolo vo verejnom dopravnom prostriedku adekvátne, ale mladé sebavedomie pôvabnú študentku rýchlo opustilo, lebo zvyšok cesty, kým sme všetci účastníci tohto príbehu nevystúpili na konečnej zástavke, už neprehovorila ani slovo.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Šéfova dcéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Hudobný automat – 2.časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Hudobný automat – 1.časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Papierová prilba
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Piršč 
                                        
                                            Silvester 1988
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Piršč
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Piršč
            
         
        pirsc.blog.sme.sk (rss)
         
                                     
     
        Foto: 10.9.2012









 
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bieda a charakter
                     
                                                         
                       Kvetografie
                     
                                                         
                       Led Zeppelin - Celebration Day
                     
                                                         
                       Racheli
                     
                                                         
                       Krása sveta
                     
                                                         
                       O Slovensku, ktoré nik nechcel
                     
                                                         
                       Splynutie ticha
                     
                                                         
                       spomienkovy
                     
                                                         
                       Ďakujem Ti, Vierka
                     
                                                         
                       Emily Dickinsonová: Srdce chce najskôr slasť, potom - zomrieť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




