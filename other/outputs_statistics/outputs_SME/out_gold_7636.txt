

 Vždy, keď prídem domov z mesta, po dlhšom pobyte medzi šumom krokov a hlavných tried, načúvam malým odtienkom toho nášho dialektu. Nastražím uši ako satelity, ktoré zachytávajú slová s úľubou. ,,To teda poriadne zlialo, všakže?" Nakláňa sa istý sused k všeobecne neobľúbenému šoférovi. 
 Nedbá, že je zakázané nakláňať sa za priehradku. A neobľúbený šofér mu s rezkým prízvukom, ale dosť rezignovane odpovedá: ,,Veru zlialo, zlialo!" 
 Ľudia sa nikdy nezamysleli nad tým, že jazdí tú istú trasu už dobrých tridsať rokov. Volant a rýchlostná páka sú životnými druhmi tohto šesťdesiatročného ufrflaného muža, ktorý nikdy nepocítil potrebu vyskúšať ich aj v úplne iných krajoch. 
  
   
  
   
  
 Vyskúšať iné cesty, vidieť iné kraje. Aj tak je všetko všade rovnaké, uisťuje sám seba, keď si už po tisícikrát oblieka tú istú bledomodrú košeľu. Aj tak ľudia všade žijú rovnako! Usmeje sa na svoju strhanú tvár do zrkadla a tá mu odpovedá smutným úškľabkom. 
 Vidieť tak už čoskoro nové polia a nové vrchy, stať sa na chvíľu Wesleym Martinom či Deanom, tých neodradí ani dážď či horúce letné popoludnia. Ešte aj benzín a decht im vonia ako jemný a nežný flakónik letiaci povetrím. Vonia, pretože je súčasťou ich strastiplnej cesty. Dobrovoľná výmena za istoty a pohodlia. 
 Rozpálený betón zasipí, keď ho schladí podvečerný dážď a potom starnúci stopári zaspávajú v mokrej a vysokej tráve. ,,Nemohol zaspať ešte hodinu. Ležal na chrbte a pozoroval bohaté strapce hviezd. Ani nie meter od neho zacvrlikal cvrček. Tráva bola vlhká, ale cítil slnkom nasýtené teplo jej podložia." 
  
 Objavy na cestách naberajú na seba rôzne podoby. Vidíme starý zamknutý kufor, od ktorého neexistuje kľúč. Predstavujeme si, že je vystlaný papierom podobným tapete a vo vnútri hrkúta tajomstvom. Skryté poštové holuby požadujúce doživotný trest za ľudskú ignoranciu. Ako poštári prenášajúci nákazy sa dostali na list neželaných. 
 Hrkútajúci kufor leží v tej istej priekope spolu s obrovským reflektorom, zeleným baloňákom, štrbavou drevenou komodou a korčuľami. Ich majiteľka, bývalá krasokorčuliarka, ktorá odmieta uveriť v globálne otepľovanie, teraz leží v ústave Bellevue. Ešte minulú zimu obdivovala rybničné rákosia vyrastajúce z ľadu, ale nápor apokalyptických informácií zlomil jej dôverčivú dušu. Ľady sa roztopili, korčule vyhodila. 
  
 Pri cestách stoja obrovské zhrdzavené ryby, pomníky ukazujúce náhodné smery chvostmi. Mýty tvrdia, že existujú od počiatku vekov, no my objavíme ich stvoriteľa. Čudáka žijúceho vo veži obrastenej zhrdzaveným železom. 
  
 Cez deň na svetlo nevychádza, až v noci sadí svoje výtvory. Žiť tak v starodávnom vodojeme, v okrúhlej miestnosti s podlhovastými a ďalekozrakými oknami, kde by bolo veľa miesta na vysoké regály plné kníh. 
  
 Odpočívajú, unavené a padlé ako Marinettiho futuristické ideály. Hladíme ich už iba očami. Relevantné dôkazy obviňujúce veľkomestá, ktoré kradnú ľudský dotyk. 
  
  
 V zasadacej miestnosti sedeli traja chlapi oblečení v tmavých sakách, červené kravaty svietili na ich bielych košeliach ako znamenie STOP, radšej nevstupovať. Jeden si znudene podopieral bradu, druhý zíval a tretí sústredene naprával obrázok uznávaného starostu na stene. Sem-tam niekto z nich akoby z povinnosti zaševelil: ,,Nech sa ten kapitalizmus už konečne skončí!" 
 O nič sa nepokúšali, nič neinovovali, len čakali na prvý máj, radostné sprievody a pečené klobásy. Začala som kričať nech sa preberú, nech niečo robia. ,,A čo by si chcelo robiť dievčatko?" Spýtal sa ma ten zívajúci a opäť roztvoril ústa dokorán. ,,Poďme sa aspoň rozprávať, debatovať, polemizovať!" Kričím na nich netrpezlivo. Ten, čo naprával obraz, si ma premeral a dôležito preriekol. ,,Aspoň nejaké klobásky si priniesla?" 
   

