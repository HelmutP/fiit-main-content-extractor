
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Petra Jamrichová
                                        &gt;
                Politika
                     
                 Pán minister školstva kandiduje do parlamentu? 

        
            
                                    12.5.2010
            o
            10:00
                        (upravené
                12.5.2010
                o
                10:19)
                        |
            Karma článku:
                24.18
            |
            Prečítané 
            6162-krát
                    
         
     
         
             

                 
                    Môj manžel volá dnešných druhákov na základných školách pokusnými králikmi. Ich dnešné učebnice sú totiž pracovné zošity. Je to moderný spôsob učenia, keď sa vypracovávaním jednotlivých úloh deti látku učia. Takže treba veľa hľadať v iných knihách, na internete a podobne. Taký druhák je však dosť malý na to, aby zvládol pracovať s internetom natoľko, aby si potrebné informácie našiel sám, bez pomoci rodiča.
                 

                 
  
   Takže pán minister školstva Ján Mikolaj, čo ste za svoje volebné obdobie urobili či neurobili v školstve, k tomu by sa vedeli vyjadriť i samotní učitelia. Z pohľadu rodiča toho druháka, ktorého doma chovám píšem:   V jednom z rozhovorov ste sa pán minister vyjadrili: "My kvôly urýchleniu predsa nepošleme na základnú školu zlú učebnicu. Dôležité je, aby boli naozaj dobré."   Sú vskutku výborné. Keď totiž unavená prídem domov z práce a najradšej by som si vyložila doma nohy, pretože celý deň zarábam a riadne si platím všetky dane i odvody, z čoho potom i Vy v parlamente žijete, čakajú ma tie "naozaj dobré" učebnice. Miesto toho, aby som si zašla na aerobic, spinning a či salsu, pri školských učebniciach je moje duševné a telesné zdravie úplne v háji.   Spomínam si, ako sme sa na prírodovedu učili stromy na skopírovaných A4 papieroch, na polročnú písomku, pretože kníh ani na polroka nebolo. Čierno - bielym stromom sme priraďovali čierno - biele listy a čierno - biele kvety. Pán minister, a Vám by sa v autoškole čierno - biele značky učili ako? Ja som nevedela rozoznať ani čierno - bielu čerešňu na tom papieri. Tú čerešňu, ktorá mi pod oknom v záhrade rastie, tú čerešňu, na ktorú sa z kuchyne dívam, tú čerešňu, ktorá práve odkvitla.   Tak ako doma odpočítavam dni do konca školských prázdnin, počas ktorých tie "naozaj dobré" učebnice so synom vypracovávať nemusím, tak odpočítavam volebné obdobie tejto "úžasnej vlády".   Prvého polroka  učebnice neboli. Staré učebnice už boli vyradené. Na nové učebnice, ktoré mali podľa Vás pán minister prísť na začiatku školského roka sa čakalo viac než pol roka. Ak by som si svoju prácu takto aj ja robila, už dávno by som nepracovala. Pán minister Mikolaj ste členom strany SNS. Nevychádzam z údivu, že v ďalších voľbách opäť na volebnej listine SNS do parlamentu pod číslom 3 kandidujete. Klamete a zavádzate. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (41)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Jamrichová 
                                        
                                            Aj taká bola MHD
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Jamrichová 
                                        
                                            Chcel byť hokejovým brankárom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Jamrichová 
                                        
                                            Študentské časy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Jamrichová 
                                        
                                            Ako som za "totáča" čižmy strihala
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Petra Jamrichová 
                                        
                                            Keď niečo chcete, musíte sa prihlásiť a "oné" na obed
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Petra Jamrichová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Petra Jamrichová
            
         
        jamrichova.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2750
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Mesto pod Urpínom
                        
                     
                                     
                        
                            O živote
                        
                     
                                     
                        
                            Cestopisy
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




