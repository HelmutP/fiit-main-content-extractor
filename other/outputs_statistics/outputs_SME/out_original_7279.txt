
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Samo Marec
                                        &gt;
                Slovákov sprievodca po kade&amp;ta
                     
                 Slovákov sprievodca po susedných krajinách 

        
            
                                    25.5.2010
            o
            20:38
                        (upravené
                4.6.2010
                o
                13:34)
                        |
            Karma článku:
                19.34
            |
            Prečítané 
            16160-krát
                    
         
     
         
             

                 
                    Začnime susedmi. Susedov máme radi, ale viac, ak ostávajú u seba. U nás ich máme radi iba keď prídu ako turisti a míňajú peniaze. Česi sa ešte radi hádžu pod lavíny. Ale predtým väčšinou tiež stihnú niečo minúť, takže nám to nevadí.
                 

                 Česi   Čechov máme radi a nielen preto, že nám odstreľujú lavíny. Sú naši bratia, teda okrem situácií, keď nie sú. To je väčšinou keď hráme proti nim hokej a futbal. A ešte keď ich prezident niečo povie. Ani vtedy ich nemáme radi, lebo ich prezident si občas aj niečo myslí a nám to pripomína, aké krásne by to bolo, keby si aj náš prezident občas niečo myslel. Ale smejeme sa im, že majú v parlamente komunistov. Alebo sme sa teda smiali, kým sme nezistili, že aj my máme, akurát utajených. Majú dobré pivo, ale my máme lepšie. Oni ho zase vypijú viac. Žerú rezne, čo im vymysleli Rakúšania, nosia ponožky v sandáloch a my k nim nosíme svojich Cigánov. Aj Necigánov, ale na nich sa nehneváme, keď sa vrátia. V hokeji a vo futbale sú lepší, chceme si síce myslieť, že nie a aj sa nám to páči, keď si to môžeme myslieť, ale nikdy nám to dlho nevydrží. Ale máme ich radi, lebo majú super knedle a sranda filmy. A ešte majú Bolka Polívku, on je tiež sranda, aj keď hovorí, že je Moravák. My mu neveríme, lebo je to skoro ako keď naši Maďari hovoria, že sú Maďari. To sa nám nepáči.       Maďari   Aj Maďarov máme radi, lebo aspoň máme na koho nadávať. Takého suseda vždy treba. Maďari nám ukradli Tokaj a my sme im za to ešte aj vystavali Budapešť. Občas si ju chceme zobrať naspäť, ale potom nám dôjde, že nám nikdy nepatrila. Aspoň hory nemajú. My máme. Ani dvojkríže nemajú, tak im občas nejaké postavíme tam, kde žijú, lebo oni žijú aj na Slovensku. Tu u nás ich tiež máme radi, dokonca čím ďalej, tým viac a čím ďalej od hranice, tým tiež viac. Ich premiér si často niečo myslí, presne ako český prezident, ale nám sa jeho myšlienky nepáčia, tak ho Maďarom nezávidíme. Ďalej sú Maďari sranda, lebo nám nerozumejú. My sme na nich naštvaní, lebo im tiež nerozumieme a to je nezvyk, lebo my si tu na okolí rozumieme s každým. Maďari majú aj diaľnice, ale to nie je žiadna frajerina, lebo na rovine ich dokáže postaviť každý. Majú Balaton, lenže keď ho vypustia, nestane sa nič. Keby sme my vypustili Gabčíkovo, bol by prúser na celú Európu, tak radšej nevyskakujú. Aj keď teraz vyskakujú, tak vyskakujeme aj my. Chcú nám zobrať veľa vecí, ale my si ich nedáme.       Poliaci   Poliakov máme radi, lebo majú lacné tenisky, mäso a syr. Nemáme ich radi, lebo nám ten syr ukradli. Aj Jánošika by nám chceli ukradnúť, ale toho si ubránime, lebo by nám nič neostalo. Okrem toho mali tiež prezidenta, ktorý rád hovoril, čo si myslí, ale toho už nemajú. Podobné veci sa im stávajú často, tak ich potom ľutujeme. Inak ich neľutujeme, lebo nám v Anglicku berú prácu. Na Poľsku je najlepšie to, že o ňom nič nevieme a je nám to úplne jedno. Nedávajú nám prácu ako Rakúšania ani mafiu ako Ukrajinci. Nechcú na nás zaútočiť ak Maďari, tak nás nezaujímajú. Inak tam všetci nosia kríže a ak nie, aj tak si to myslíme. Tiež si o nich myslíme, že sú to sedliaci, lebo je to fasa, keď si to môžeme myslieť. Nemajú hory, tak sa im smejeme, lebo aj v tom sme lepší. Majú more, ale je studené a nudné, tak aj preto sa im smejeme. Smiešne rozprávajú, za čo sa im tiež smejeme. Celkovo sa im smejeme veľa. Aj oni sa nám smejú, ale menej. Tak trochu nás obdivujú, asi nevedia, že máme komunistov v parlamente. A nemajú diaľnice. Ani euro, ale sú s tým spokojní.       Ukrajinci   Ukrajincov nemáme radi hlavne preto, že sú to Rusi. Oni tvrdia, že nie sú, ale my vieme svoje. Majú mafiu a lacné cigarety, oboje sa k nám dostáva nelegálne. Ukrajina je veľká, lebo tak vyzerá na mape, tak asi bude. Nevieme si predstaviť, aká až, ale asi dosť, tak tam nechodíme. Ak, tak len pre tie cigarety. Ukrajinky sú pekné, ale určite to nie je pravda. Ukrajinci pijú vodku. Aj my pijeme vodku, ale nie až toľko ako oni. Pijeme aj pivo, ale menej ako Česi a víno, ale menej ako Maďari. Zato keď máme chuť, pijeme to všetko naraz. Na Ukrajine nemajú cesty. Mali peknú premiérku, ale už ju nemajú. Veď ani my nemáme, tak prečo by mali byť lepší. Majú bane, my sme tiež mali, už nemáme, ale ani nám ich netreba, lebo máme automobilky. Automobilky sú viac fasa, lebo aj my sme viac fasa ako Ukrajinci. Majú sa strašne zle a to sa nám páči, lebo sa aspoň niekto má horšie ako my. Nie sú ani v Európskej Únii, tak sa na nich tak trochu smejeme. A tiež preto, že sú Rusi, ale to sme si už povedali. U nás nikto nechce byť Rus. My Rusov nemáme radi, teda okrem nášho premiéra.       Rakúšania   Rakúšanov máme radi, lebo nám dávajú prácu a majú fest oveľa viac diaľnic ako my. Tak naozaj ich ale radi nemáme, lebo sú oveľa bohatší, musíme u nich otročiť, majú viac diaľnic a vyššie hory. Rakúšania sú frajeri, ale aj tak sú to len Nemci. Myslia si, že vymysleli rezeň, lenže to je nič, my sme vymysleli unikátny mýtny systém. Mali takého pekného politika, čo veľa kričal a my takých u nás máme radi, ale už ho nemajú. Aj tak sme ho nemali radi. Rakúšania sú divní, lebo im nerozumieme a keď sa im už konečne naučíme rozumieť, aj tak sa s nami nechcú rozprávať. Nie sú dobrí v hokeji. Ani Poliaci, Ukrajinci a Maďari nie sú, furt nad nimi vyhrávame. Teda nad Maďarmi už zriedkavejšie a tesnejšie, ale vyhrávame. Rakúšania sú ale v hokeji takí zlí, že s nami radšej ani nehrajú. Chceli kúpiť naše letisko, ale sme im ho nepredali, lebo sme frajeri. Oni nie sú frajeri, lebo pijú iba pivo  a tiež tam majú veľa kadejakej hávede ako Turkov, Albáncov, Poliakov, Maďarov a Slovákov, čo ich všetci za chvíľu prevalcujú, takže by si mali dávať bacha a nefrajerovať. Čaká ich škaredá budúcnosť a aj preto ich máme radi. Škaredá budúcnosť je niečo, na čo sme u nás zvyknutí, tak sa s nimi cítime byť viac spriaznení.   Tak. Teraz už vieme, že svojich susedov máme radi. Nabudúce možno o nesusedoch.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (77)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Dear John, odchádzam z blogu SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako zostarnúť a (ne)zblázniť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Písať o tom, čo je tabu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Samo Marec 
                                        
                                            Ako v Rumunsku neprísť o ilúzie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Samo Marec
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Samo Marec
            
         
        samuelmarec.blog.sme.sk (rss)
         
                        VIP
                             
     
         Domnievam sa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    357
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    10791
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            An angry young man
                        
                     
                                     
                        
                            Dobré správy zos Varšavy
                        
                     
                                     
                        
                            Fero z lesa v hlavnom meste
                        
                     
                                     
                        
                            From Prishtina with love
                        
                     
                                     
                        
                            Kraków - miasto smaków
                        
                     
                                     
                        
                            Listy Karolovi
                        
                     
                                     
                        
                            Sama Mareca príhody a skúsenos
                        
                     
                                     
                        
                            Samo na Balkóne 2011
                        
                     
                                     
                        
                            Samo v Rumunsku
                        
                     
                                     
                        
                            Scotland-the mother of all rai
                        
                     
                                     
                        
                            Slovákov sprievodca po kade&amp;ta
                        
                     
                                     
                        
                            TA3
                        
                     
                                     
                        
                            Thrash the TV trash
                        
                     
                                     
                        
                            Univerzita Mateja a Bélu
                        
                     
                                     
                        
                            Veci súkromné
                        
                     
                                     
                        
                            Veci verejné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Čarovné Sarajevo
                                     
                                                                             
                                            Jeden z mála, čo za niečo stojí
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            O chvíľu budem čítať toto
                                     
                                                                             
                                            Spolčení
                                     
                                                                             
                                            Smutný africký príbeh
                                     
                                                                             
                                            Sila zvyku
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Michal!
                                     
                                                                             
                                            Aľenka
                                     
                                                                             
                                            Soňa je super (nepáčilo sa jej, čo som tu mal predtým)
                                     
                                                                             
                                            Vykorenený Ivan
                                     
                                                                             
                                            Chlap od Žamé
                                     
                                                                             
                                            Žamé
                                     
                                                                             
                                            Dievka na Taiwane
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




