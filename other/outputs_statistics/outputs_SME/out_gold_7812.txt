

 Nestačí, že som si k paušálu objednal aj internetové pripojenie Orange World a na telefóne som si spustil automatické nastavenie prístupových bodov. Internet mi nešiel. Volám teda 905, kde riešia všetky problémy. Chlapík na hot-line si otvoril formulár k môjmu telefónu a povedal mi, čo a ako mám nastaviť. Internet mi išiel asi 10 sekúnd a potom viac ani muk. 
 Po rôznych márnych pokusoch na druhý deň volám znova 905, tam už bol asi zbehlejší operátor a hovorí mi, že pre telefóny s Androidom (operačný systém na ktorom beží aj X10) radšej nech si zavolám 595 a z menu nech si vyberiem 3. No, dobre, keď nej 905 tak skúsme 595, však aj to sú len tri číslice. Keď si z menu vyberám pomoc pri nastavení telefónu, milý robot so ženským hlasom mi oznamuje, aby som volal odbornú pomoc 14905 a že ma to bude stáť len smiešnych 60 centov za minútu. No, vidím, že stačí vytočiť o dve číslice viac, a ceny rapídne stúpajú ;-) 
 Keďže som informatik, chlapík mi pomôže nastaviť veci za necelých 5 minút, tkaže k mesačnému poplatku 6 euro som si pripočítal ešte dobré 3 eurá za nastavenie. Ja viem, nie je to majland a takmer nič v porovnaní s cenou, ktorú som za telefón zaplatil (265 €). Za tie prachy by sa ale patrilo  dodať mobil, ktorý zapnem a funguje, zvlášť ak ide o typ, ktorý je priamo určený na pripojenie na internet (dodáva sa so zabudovanou aplikáciou na Facebook, Gmail, e-mail, navigáciou cez Google a pod.) Orange vrazí do reklamnej kampane ťažké prachy na X desiatku, ale keď vás už raz uloví, už si nedá tú námahu aby vám zabezpečil poriadnu podporu. 
 Dobre, keď už ste sa dostali až sem a vypočuli (vyčítali) ste si moje náreky, tak ako bonus získavate jenoduchý návod, ako nastaviť pripojenie Xperia X10 (a zrejme aj iných mobilov s Androidom) na internet cez Orange World službu. Je to takmer triviálne, stačí nastaviť jeden, jediný parameter. 
   
 N Á V O D 
 1. Otvorte si aplikáciu "Nastavenia" a postupne si vyberte: "Ovládanie bezdrôtových sietí" / "Mobilné siete" /  "Názvy prístupových bodov". 
 2. Stlačte (hardvérové) tlačidlo menu a vyberte si "Nový názov prístupového bodu" 
 3. Dajte mu nejaký názov podľa vlastného výberu (do políčka "Názov") a nastavte parameter "Názov prístupového bodu (APN)" - tam napíšte internet. 
 4. To je všetko. Uložte prístupový bod. (zase HW tlačidlo menu a "Uložiť") 
   
 Ak vám to pomohlo, zrejme ste ušetrili pár euro a spústu času :-) Ak sa mi chcete odvďačiť, hoďte do pokladničky v najbližšom kostole 1 € (len tretinová cena oproti Orange) ;-) ale hlavne prosím nevoľte Fica a tú bandu lumpov okolo neho! 

