
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lucia Tomišová
                                        &gt;
                Nezaradené
                     
                 Nenechám sa odradiť 

        
            
                                    10.5.2010
            o
            7:50
                        |
            Karma článku:
                5.14
            |
            Prečítané 
            1593-krát
                    
         
     
         
             

                 
                    Pri mojich posledných dvoch blogoch, kde som sa venovala téme pitnej vody, som pri diskusiách nadobudla pocit, že príliš iritujem čitateľov. Kladiem si tak otázku čím a prečo?  V  súlade s tým, čo mám napísané v profile, a za čo sa vôbec nehanbím, ani hanbiť nebudem, píšem o vode a vodách, ktoré tu máme a ktoré pijeme. Keďže sa pri svojej práci stretávam s množstvom odborníkov, ktorí sa téme vody a vôd venujú, píšem ďalej o tom čo ma zaujalo. Nevidím na tom nič zlé a v skutku netuším prečo to niektorých tak veľmi hnevá.
                 

                Nikdy som nepropagovala, žiadnu konkrétnu balenú vodu iba upozorňujem na rozdiely, ktoré medzi jednotlivými kategóriami vôd reálne existujú.  Som presvedčená o tom, že ak sa niekto o kvalitu toho čo pije zaujíma a má dostupné informácie, vie sa sám rozhodnúť čo má piť. Na rozdiel od vodární a ich kampane, ja hovorím o všetkých kategóriách vôd a nepodsúvam informáciu, že len jedna jediná je zdravá.  Keď píšem o tom, že pre dojčatá a malé deti vôbec, je voda z vodovodu nevhodná ak obsahuje viac ako 10mg/l dusičnanov, nepíšem to preto, že mojou úlohou je zvýšiť predaj dojčenských vôd. Iba upozorňujem na skutočnosť, že balené dojčenské vody spĺňajú prísnejší limit ako voda z vodovodu. Ak teda niekto býva v oblasti, v ktorej je voda s prekročeným limitom na dusičnany, nie je vhodné aby z nej pripravoval stravu pre svoje malé dieťa – dojča. Je to pravda a tú pravdu Vám potvrdí mnoho pediatrov a odborníkov z oblasti vody. Kampaň vodární bola, zdá sa, účinná. Mnohí z Vás si jednoducho myslia, že voda z vodovodu je to najlepšie riešenie pre náš pitný režim. V poriadku – neberiem Vám Váš názor, ale dovoľte mi mať môj – NIE JE. Vždy sa objaví množstvo odmietavých reakcií, že podporujeme ničenie životného prostredia prostredníctvom produkcie PET fliaš, ktoré sú vraj okrem toho škodlivé aj pre balené vody ako také. PET fľaše sú súčasť našich životov, vyžiadal si ich trh a zákaznícky komfort. Nie každý má to šťastie, že býva pri prameni minerálnej alebo pramenitej vody. A tvrdiť, že obaly z  PET materiálu, škodia baleným vodám – sú mýtom. Obaly na potraviny musia byť vyrobené z materiálu, ktorý zabezpečí ich kvalitu a vlastnosti. Samozrejme, že je potrebné dodržiavať skladové podmienky, platí to pri všetkých balených potravinách prečo by práve voda mala byť výnimkou? Pravidelné kontroly na všetkých stupňoch, čiže u výrobcov aj u samotných predajcov, v priebehu posledných rokov  nezistili ani jediný prípad ohrozenia kvality balenej vody z dôvodu prieniku látok z obalov.  Balené pramenité, prírodné minerálne, liečivé, alebo dojčenské vody pochádzajú vždy z podzemných zdrojov a sú prirodzene mikrobiologicky čisté. Predtým, ako sa z nich môže vôbec voda čerpať a plniť do fliaš, sa takéto zdroje musia tri a viac rokov sledovať, pričom počas tejto doby, voda musí vykazovať stálu kvalitu a zloženie. Celý proces kontroly takto dodávanej vody je prísnejší  v porovnaní s vodou z vodovodu,, pretože ide o potravinuy. A to nie je útok na vodu z vodovodu, ale fakt, ktorý sa okrem iného opiera aj o skutočnosť, že voda z vodovodu je primárne určená aj na iné účely než len na pitie. Áno, na Slovensku si  môžeme dopriať ten luxus, že sa umývame, polievame záhrady aj splachujeme pitnou vodou. Keďže je určená na hromadné zásobovanie obyvateľstva, musí byť zdravotne bezpečná. Aby takou ostala aj potom čo nám vytečie z kohútika, je nutné ju dezinfikovať chlórom.   Kto ju chce piť, nech ju pije. Keď však budeme naďalej počúvať, že táto (a žiadna iná voda) je najlepšia pre naše zdravie a zdravie našich detí, tak budeme počúvať nepravdu. Málo ľudí si napríklad uvedomuje, že garancia zdravotnej bezpečnosti vody z vodovodu, zo strany vodárenských spoločností, platí iba po prípojku do domového rozvodu. Málokto si možno uvedomuje, v akom stave sú vnútorné rozvody jeho domu a  v akom skutočnom stave sú aj samotné verejné rozvodné siete na vodu. Keď sa niekde stane havária (čo nie je jav neobvyklý), nazrite prosím do odkrytej jamy a pozrite sa, ako vyzerá vodovodné potrubie zvnútra. Pozrite sa napríklad občas na sitko, ktoré je súčasťou vášho kohútika a pozrite sa, čo všetko sa na ňom zachytí za pol roka, alebo rok. A niekedy aj za mesiac. Ak to niekto vníma ako útok na vodu z vodovodu a je skalopevne presvedčený, že je tým najlepším, čo môže piť, v poriadku.  Verím však, že sa nájdu aj takí ktorí si minimálne vymenia sitko vo svojej domácnosti a možno sa budú viac o kvalite toho čo pijú oni a ich deti informovať. Bavíme sa stále o vodách, ktoré sú určené na pitie, je ich na Slovensku mnoho a sú medzi nimi rozdiely. Škála je tak  široká, že si dokáže vybrať každý.  Dovolila som si, len upozorniť, že tí, ktorí si vybrať nemôžu (dojčatá a malé deti), by od nás mali dostávať tú vodu, ktorá je im určená.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Tomišová 
                                        
                                            Sleduj farbu svojho moču, radia Briti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Tomišová 
                                        
                                            Ktorá voda je najlepšia a najzdravšia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Tomišová 
                                        
                                            Dusičnany vo vode z vodovodu a  zdravie našich detí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Tomišová 
                                        
                                            Je chlór vo vode z vodovodu úplne neškodný?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Tomišová 
                                        
                                            Má byť voda z vodovodu v reštauráciách zadarmo?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lucia Tomišová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lucia Tomišová
            
         
        tomisova.blog.sme.sk (rss)
         
                                     
     
        Ako riaditeľka Asociácie výrobcov nealkoholických nápojov a minerálnych vôd, sa snažím vysvetľovať, ktorá voda, je kedy a prečo zdravá. Inak sa už 17 rokov venujem modernému tancu, ktorý je nielen zábavný, ale aj zdravý. Prakticky celý môj život sa točí okolo zdravia:-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3556
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




