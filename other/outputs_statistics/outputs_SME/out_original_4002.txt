
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marian Vojtko
                                        &gt;
                Svet v ktorom žijeme
                     
                 Divné praktiky T-COMu 

        
            
                                    19.4.2010
            o
            21:15
                        (upravené
                19.4.2010
                o
                21:23)
                        |
            Karma článku:
                6.25
            |
            Prečítané 
            1241-krát
                    
         
     
         
             

                 
                    Pred časom som napísal článok o falošných elektronických faktúrach, ktoré mi v mene T-COMu prišli e-mailom. Vec sa vyriešila: faktúry posielali priamo z T-COMu. Omylom! Aj sa ospravedlnili, ale veľa nevysvetlili. Ak Vás to zaujíma, čítajte ďalej.    T-COM však vyšiel v ústrety zákazníkom v zlepšení služieb. Ako? To, čo by ste vybavili za 10 minút v ich kancelárii, musíte vysvetliť 4 pracovníkom, ktorým sa dovolať nie je jednoduché, a ak sa už dovoláte, za spojenie na ich mobilné telefóny riadne platíte. Aj o tom sa ďalej dočítate.
                 

                 ELEKTRONICKÉ FAKTÚRY  Na e-mailovú adresu, pozostávajúcu z rovnakých troch písmen pred i za zavináčom mi opakovane prišla elektronická faktúra na vyše 21 eur. Nebolo tam uvedené, za čo mám vlastne zaplatiť. Ani meno pracovníka, ani žiaden kontakt, len upozornenie, že e-mail bol vygenerovaný automaticky, a že naň nemám odpovedať.  V diskusii k článku ktosi upozornil, že číslo účtu (boli tam uvedené dve) naozaj patrí T-COMu.  Potom na môj článok zareagoval jeden pán, ktorý dal veci v T-COMe do pohybu. Po niekoľkých telefonátoch s kompetentnými a zaslaní podozrivého e-mailu s faktúrou som dostal túto odpoveď:  Vazeny pan  Vojtko, na zaklade Vasho  podnetu prijateho dna 16.2.2010 vo veci zasielania mailovych faktur na Vasu  e-mailovu adresu, Vam oznamujeme nasledovne:   Po presetreni  vzniknutej situacie sme zistili, ze sme Vam zasielali faktury na Vasu e-mailovu  adresu .....  z dovodu nespravne zadanej identifikacie pre inu spolocnost.  Vasu e-mailovu adresu uviedol nespravne predajca pri zriadovani sluzieb T-Com  pre inu spolocnost. Po overeni u dotknutej spolocnosti a zisteni, ze nie su  vlastnikom e-mail adresy: ...., sme mailovu adresu .....  vymazali z  údajov dotycneho zakaznika dna 10.2.2010.    Za  vzniknutu situaciu sa Vam ospravedlnujeme.    Dakujeme Vam za  vyuzivanie sluzieb spolocnosti T-Com.  Ing. A...... .............. Centrum sluzieb  zakaznikom T-Com   O.K., vec vyriešená. Okrem kuriozity, že podnet prijali 16.2., ale už 10.2. na základe jeho prešetrenia urobili nápravu, stále tu zostáva zopár nezodpovedaných otázok:  Ako je možné, že si pracovník pri zriaďovaní služieb zákazníkovi vymyslí e-mailovú adresu, na ktorú mu majú byť zasielané faktúry?  Ako je možné, že vo faktúrach neuvádzajú, za aké služby fakturujú poplatok?  Prečo faktúra neobsahuje informáciu o tom, kto ju vystavil, a nie je v nej uvedený ani žiaden kontakt, pre prípad reklamácie?  Myslieť si o tom môžeme všeličo, ale bez faktov by to boli len špekulácie...    AKO JE TO SO SLUŽBAMI T-COMu ... V ÚSTRETY ZÁKAZNÍKOM?  1. Servis za platenú službu. Čo sme v Amerike? Keď som v lete minulého roka prevzal po 70 ročnom kňazovi faru na Záhorí, zistil som, že je tu jediný telefón, z ktorého volať je už problém. Vo faktúre som si všimol, že sa platí poplatok za "prenájom koncového zariadenia".   Tak som zavolal na číslo uvedené na faktúre. Dozvedel som sa, že farský úrad nevyužil možnosť odkúpenia telefónneho aparátu a preto sa zaň platí nájom. Keď som im povedal, že aparát je vo veľmi zlom stave, že sa z neho takmer nedá volať, tak mi povedali, že už vyše päť rokov je nárok na jeho výmenu.  Podľa mojich predstáv, by malo byť úplne samozrejmé, že slušná firma k platenej službe dodáva automaticky aj servis: aparát je po dobe životnosti = automaticky ho vymenia, veď si za to zákazník platí. Ale T-COM má asi iný názor: kým sa zákazník sám neozve, netreba si ho všímať, hlavne že platí faktúry včas!  K dobru pracovníka, s ktorým som vtedy telefonoval, musím poznamenať, že mi hneď ponúkol telefonický kontakt na technika, ktorý príde aparát vymeniť. (I keď si viem predstaviť aj taký servis platiacemu zákazníkovi, že toho technika kontaktujú oni sami...)  Poďakoval som za ponúknutý kontakt a povedal som, že ja chcem radšej prenájom aparátu zrušiť, lebo si chcem kúpiť bezdrôtový prenosný telefón. Kontaktný pracovník mi na to povedal, že to môžem, ale len osobne v ich obchodnej kancelárii, v mojom prípade v Senici. Opýtal som sa ešte, kde mám odovzdať starý aparát, keď skončí jeho prenájom? Odpoveď: Ten si už môžete kľudne nechať.  V obchodnej kancelárii ma vybavili k úplnej spokojnosti: službu zrušili, aj mi ponúkli z ich sortimentu telefónnych prístrojov. Opýtal som sa ešte, či je možné zriadiť službu zobrazenie čísla volajúceho. Odpovedali, že keď budem mať telefón, ktorý túto službu podporuje, na požiadanie mi ju okamžite aktivujú.  2. Služba zadarmo?    Bonus alebo figa borová? Išiel som na pravidenú návštevu starých a chorých, jednu pani som nenašiel... Pri ďalšej návšteve mi vravela, že bola odcestovaná, ale že mi nechala odkaz na záznamníku. Ale veď ja na telefóne žiaden záznamník nemám - oponoval som... Inokedy mi iná žena vravela, že mi telefonovala, ale ozval sa odkazovač.  Ako som o tom v prekvapení uvažoval, spomenul som si, že kedysi bolo v reklame T-COMu uvedené, že zriaďujú odkazovú službu. Pýtal som sa teda v obchodnej kancelárii, ako si môžem vypočuť odkazy, a ako sa vôbec dozviem, že mi boli zanechané nejaké odkazy? Odpoveď: K tomu je potrebné špeciálne koncové zariadenie, ktoré podporuje túto službu.  No pekne! Berú poplatok za prenájom zariadenia, ktoré je dávno po životnosti, ale pritom bez upozornenia zákazníkovi aktivujú službu, ku ktorej treba špeciálne zariadenie, ktoré neposkytnú, ani neponúknu. A zákazník ani netuší, že mu ľudia nechávajú odkazy...  3. Ako sa zo zákazníka stane len akýsi segment.   Ale vyšší!!! Keď už som bol v tej obchodnej kancelárii T-COMu, požiadal som o zrušenie tej podivnej odkazovej služby a o aktiváciu zobrazovania čísla volajúceho. Pracovníčka si vyžiadala môj občiansky a údaje o farskom úrade, na ktorý je telefón vedený.  Po chvíli však povedala: - Ale ja vám to spraviť nemôžem. Vy ste vyšší segment. - A to čo znamená? - pýtam sa prekvapený. Odpoveď: - Vy musíte všetky zmeny realizovať telefonicky cez vášho operátora. - A to je kto, a ako sa s ním skontaktujem? - To vám museli oznámiť, keď ste boli preradený. - Ale ja o žiadnom preradení neviem. Minulé leto, keď som tu bol, tak ste ma riadne vybavili. Prečo som bol teda preradený? - Museli ste o to požiadať... A keď nie vy, tak ten, kto vám platí telefón... Asi biskupský úrad... (?) - Ja som o to nežiadal, a telefón si platím sám. Biskupský úrad s tým nič nemá. ... A to už teda u vás nič nevybavím? - Nie, my vás obslúžiť nemôžeme. - Takže, to už sem chodiť nemusím? - Nie. - A ako teda mám kontaktovať toho, u ktorého môžem realizovať požadované zmeny? Po chvíľke hľadania v počítači povedala: - Vy patríte pánovi M. Na neho však nemáme kontakt. Dáme vám číslo na pani B., ona vám dá kontakt na pána M.  Pani B. som sa pokúšal dovolať na jej mobil niekoľko týždňov... Nevolal som síce každý deň, ale niekoľko ráz do týždňa, v rôznych časoch.  Konečne som sa jej dovolal. Keď som jej vysvetlil, čo vlastne chcem, spýtala sa: - A vy chcete pána M. alebo pána K.? - V obchodnej kancelárii mi povedali, že to má na starosti pán M. - Ale on je vedúci. Túto vec budete musieť riešiť s pánom K. Na toho ja kontakt nemám. Dám vám číslo na pána M. a on vám potom dá kontakt na pána K.   Ďalší týždeň som sa pokúšal dovolať na mobil pána M. Konečne sa mi to dnes podarilo! Povedal som mu, čo chcem, a už trochu podráždene sa ho spýtal, prečo ma preradili do toho segmentu? - To je preto, aby ste mohli všetko vybaviť pohodlne po telefóne a nemuseli cestovať do obchodnej kancelárie. Keď som sa ho spýtal, či je pohodlnejšie niekoľko týždňov vyvolávať a pátrať po príslušnom pracovníkovi, odpovedal: - Preradení ste boli k 1.2. Úlohou pána K. je vás kontaktovať a informovať vás o tom.  Takže bez môjho vedomia ma preradia, a za štvrť roka ma nestihnú o tom informovať! V slušnej spoločnosti by o takejto možnosti informovali vopred a opýtali by sa, či o to mám záujem...  Pán M. mi dal číslo mobilu na pána K., a zároveň povedal, že mu zavolá, aby sa mi pán K. ozval.  Za tri hodiny mi pán K. zavolal (takže tento telefonát som už platiť nemusel). Vysvetlil som mu, čo chcem a on sľúbil, že to vybaví.  Som zvedavý ako (po telefóne?), lebo ak som dobre rozumel, žiadosť o zmeny budem musieť podpísať... !   Pri takýchto praktikách si T-COM veľmi ťažko udrží zákazníkov. Zvlášť, keď teraz Orange prišiel s akciou "Domáca linka". Azda v T-COMe počítajú s tým, že tam, kde majú klasickú telefónnu ústredňu, či zakúpený drahší telefónny prístroj pre klasickú telefónnu linku, sú odkázaní len na nich, lebo na alternatívnych operátorov sa s tým zariadením nenapoja?  I ja - kebyže som pred pol rokom nekúpil súpravu prenosných telefónov pre pevnú linku, už by som to určite riešil inak... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Vojtko 
                                        
                                            XXXL - radikálne riešenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Vojtko 
                                        
                                            život XXXL
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Vojtko 
                                        
                                            Vodičom autobusu bez autoškoly?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Vojtko 
                                        
                                            O čom nás vlastne presviedča reklama?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marian Vojtko 
                                        
                                            Bloger - nevolič, čo ťa trápi?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marian Vojtko
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marian Vojtko
            
         
        marianvojtko.blog.sme.sk (rss)
         
                                     
     
         Keď bolo PMD 85 hitom, uchvátili ma počítače. Na istý čas sa mi stali profesiou... Napokon ma však uchvátil Niekto iný. 
 Občas blogujem aj tu: mojakomunita.sk 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    241
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1233
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Svet v ktorom žijeme
                        
                     
                                     
                        
                            dumky
                        
                     
                                     
                        
                            Kocúrkovo SR
                        
                     
                                     
                        
                            slovenčina
                        
                     
                                     
                        
                            Cirkev v súčasnosti
                        
                     
                                     
                        
                            financovanie cirkvi
                        
                     
                                     
                        
                            Modlitby
                        
                     
                                     
                        
                            Život Krista
                        
                     
                                     
                        
                            Dejiny Cirkvi
                        
                     
                                     
                        
                            Kristus a my
                        
                     
                                     
                        
                            Zaujimavosti o svätých a cirkv
                        
                     
                                     
                        
                            Myjava
                        
                     
                                     
                        
                            Spomienky
                        
                     
                                     
                        
                            XXXL
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




