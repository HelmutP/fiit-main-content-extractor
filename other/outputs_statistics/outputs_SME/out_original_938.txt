
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Vlachynský
                                        &gt;
                Cesty Univerzom
                     
                 Plavba Warlatie - PVC dobrodružstvo v troch dejstvách 

        
            
                                    13.10.2008
            o
            8:46
                        |
            Karma článku:
                9.07
            |
            Prečítané 
            2521-krát
                    
         
     
         
             

                 
                    Nápad vznikol spontánne behom pár minút, realizácia nakoniec trvala rok. Koncom tohoto leta však  bola konečne spustená na vodu a vyrazila na Váh, aj so štvorčlennou posádkou. Warlatia, koráb postavený z PVC fľaší a ďalších smetí.
                 

                  Ako som povedal, myšlienka prišla len tak, z jasného neba blesk, postaviť plavidlo z plastových fľaší a zopár dosiek. Žiaden ekologický protest, ani recesia, ani súťaž. Proste len tak. Fľaše sme zbierali doma, s prispením rodiny a kamarátov, naviac sme odbremenili Lido od ďalších pár sto kúskov (tety na vychádzke nás chválili, že zbierame smeti - vysvetlite im, že to je materiál na loď!). Po búrlivých brainstormingoch u Doda boli hotové návrhy. Konečný výsledok - okolo 600 fľaší s objemom cez 1000 litrov, odhadovaný výtlak takmer 900kg. Pre štvorčlennú posádku viac ako dosť. Projekt Warlatia začal.     Fáza prvá - stavba Warlatie                   Na stovky plastových fľaší bol krázny pohľad, škoda bolo do nich rýpať...       Záverečné núdzové doplnenie zásob, susedia prepáčia.       Tri fošne, chrbtová kosť celého plavidla.       Prvý pontón hotový!          Grafická vyzualizácia. Pôvodné nápady boli búrlivejšie, vrátanie klietky naplnenej fľašami.       Všetko bolo spočítané. Aspoň zhruba :) V detaile vidieť aj oporné body pre priečne spojnice.          Tri trupy hotové.       Fáza druhá - Plavba Warlatie          Favorit Frank mal tú česť viezť vzácny náklad ku Trenčínu.     Hrdinná posádka zostavuje loď.     Stavba paluby. Skladací prístrešok z papierových rúrok. Nebola to najlepšia voľba...       Posádka a jej vlajka. Chlapi z ocele.       Prvé metre plavby dopadli na jednotku. Prosím pohľad na palubu - nehostinné drevo bolo pokryté dvoma priklincovanými nafukovačkami a papierovými doskami.       Hill Billy - jedna z pohonno-smerovacích palíc.       Warlatia - no comment       Prvá skúška - strom krížom cez rieku zhrzil pozhŕnať palubu do vody a zakliesnený koráb by sme z tadiaľ už v nikdy nedostali. Pár metrov pred kontaktom sa nám podarilo hovado zatlačiť na breh.       Hadík. Kúsal. Snažil sa.       Prvý deň  bolo krásne počasie. O tom svedčí aj tento náhodný naháč vo Váhu.       Prvé táborisku. Môj návrh spať na lodi neprešiel.       Druhý deň. Zachmúrené pohľady posádky smerom k oblohe. Ešte sme nevedeli, že nás ten večer navštíví peklo. Ale tušili sme!                         Občasná búrka. Nič, čo by papierovo-igelitový prístrešok nezvládol. A medzitým sa hrali karty.                           Druhé táborisko pri Považanoch. Warlatia odpočíva potiahnutá na plytčinu a posádka pózuje. Až kým...Až kým nevypukla najprudšia búrka v okolí Piešťan za posledných 5 rokov. Stromy padali a blesky blikali kadenciou protileteckého delostrelectva. Stany smer neuznávali, tak sme sa krčili pod kúskom igelitu a nechávali sa otĺkať krúpami. Ešte stále to nebolo také zlé - kým sme náhle nezistili, že Warlatia aj s väčšinou našich vecí zmizla. Lepšie povedané - odplavila sa, vďaka prudko zvýšenej hladine. Naša ľoď nás zanechala! Bez rozmýšľania sme sa rozbehli dolu prúdom - chvílu plávajúc, občas sa predierajúc nepriechodným brehom (ak ste videli v noci počas búrky bežať naháča po roli medzi Považanmi a Potvoricami, bol som to ja). Po viac ako hodinovom plávaní sme tú  potvoru konečne dostihli a dotiahli na breh. Návrat pre veci, návrat k lodi, hodiny v búrke. O štvrtej ráno sme opäť zaľahli, premočený do poslednej nitky, so zážitkami hodnými nočnej bitky kdesi v Mekongu. Po pár hodinách sme sa opäť postavili na nohy a doplavili sa k Prúdom, kde sme Warlatiu zanechali zamaskovanú. Príjazd do Piešťan si vyžadoval lepšiu formu.      Fáza tretia - koniec Warlatie               Záverečná plavba sa odohrala o  niekoľko týždňov. Spolu s prizvanými hosťami nás bolo na palube sedem. Pre Warlatiu hračka.       Prejazd popod most vyvolal záujem u nemeckých turistov aj pospolitého ľudu.            Dlho sme zvažovali, čo s loďou urobiť. Poškodenie bolo minimálne, prišli sme asi o 10 fľaší.                    Napriek úvahám o jej zanechaní na brehu pre voľné užívanie sme ju nakoniec rozobrali - expedícia Warlatia sa skončila.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Vlachynský 
                                        
                                            Piešťanci v boji proti nariadeniu vyhrali prvú bitku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Vlachynský 
                                        
                                            Zatiahnu nás tajtrlíci do vojny?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Vlachynský 
                                        
                                            Gabura potvrdil, že sme sa nikam nepohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Vlachynský 
                                        
                                            Strana Neuveritelných Sedlákov (Slotu mám ja rád)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Vlachynský 
                                        
                                            Prepadnutý hlas a demokracia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Vlachynský
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Vlachynský
            
         
        vlachynsky.blog.sme.sk (rss)
         
                                     
     
        tuzemský filozofix, instantný repper, kvantový automechanik 
  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2643
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Jama umenia a dekadencie
                        
                     
                                     
                        
                            Zvrátené myšlienkové pochody
                        
                     
                                     
                        
                            Cesty Univerzom
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            INESS
                                     
                                                                             
                                            Eurokríza
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            štúdium v Británii
                                     
                                                                             
                                            Rats Get Fit!
                                     
                                                                             
                                            GoldHaven
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




