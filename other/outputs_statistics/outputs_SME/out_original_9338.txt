
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Karol Kaliský
                                        &gt;
                PRÍRODA
                     
                 Paralelné svety 

        
            
                                    22.6.2010
            o
            7:30
                        (upravené
                22.6.2010
                o
                0:10)
                        |
            Karma článku:
                14.84
            |
            Prečítané 
            2421-krát
                    
         
     
         
             

                 
                    Jeden by ani neveril, v akom svete to žijeme.  Teraz nemyslím na ten náš ľudský, kde nám (zdanlivo) vôbec nič nechýba. Veď povážte, ľudia sa nikdy v histórii nemali tak dobre ako teraz. Naši praprastarí otcovia ani nesnívali o autách, lietadlách, internete, teplej sprche, mobilnom telefóne, tone hračiek, prebytkoch jedla či pitia od výmyslu sveta. To, že žijeme na dlh a za náš komfortný a pohodlný život zaplatia už najbližšie generácie nám je šumafuk a ťažko to možno niekomu zazlievať. Ľahko sme si zvykli. Ale o tom som nechcel písať. Okrem nášho virtuálneho sveta peňazí a konzumu, ktorý tu existuje len nepatrnú chvíľku, je všade okolo nás skutočný svet, oveľa dokonalejší, svet ktorý je tu už tristo miliónov rokov a bude tu ešte dlho po nás. Ja sa občas do tohto sveta vyberiem nazrieť. Netreba si pritom vôbec drať nohy ani nachodiť desiatky kilometrov. Stačí len opustiť našu betónovú džungľu, zastať a zadívať sa okolo seba. Objavíte makrosvet, zázračný svet hmyzu.
                 

                                                                              

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Samozvaní ochranári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            10 rokov po ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            O čom sú (pre nás) Tatry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Neodrezávajte svojim miláčikom hlavy!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Vlčie hory
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Karol Kaliský
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Karol Kaliský
            
         
        kalisky.blog.sme.sk (rss)
         
                        VIP
                             
     
        lesník, trošku aj fotograf prírody (viac na www.wildlife.sk)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                12.48
                    
                
                
                    Priemerná čítanosť
                    4833
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            PRÍRODA
                        
                     
                                     
                        
                            FOTOGRAFIE
                        
                     
                                     
                        
                            NEZARADENÉ
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            V čase krízy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pokorný, Bárta: Něco překrásného se končí
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janci Topercer
                                     
                                                                             
                                            Juro Lukáč
                                     
                                                                             
                                            Mišo Wiezik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.arollafilm.com
                                     
                                                                             
                                            www.wildlife.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Desať rokov úžasnej divočiny v Tichej a Kôprovej doline
                     
                                                         
                       Vlčie hory
                     
                                                         
                       130 vlkov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




