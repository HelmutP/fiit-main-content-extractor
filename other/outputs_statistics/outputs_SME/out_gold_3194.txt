

 _ _ _ 
 „Mám tu priateľov, a pokoj a vyrovnanosť, po ktorých tak dlho túžim, nájdem skôr, keď sa na nejaký ten čas usadím na jednom mieste. A je tu more."  
   
 Znovu a znovu si čítam týchto pár slov. Hoci je to iba nepatrný, smiešny zlomok dennej dávky všetkých slov, chrliacich sa nepríčetne z človeka a do neho...cítim prudký pohyb existencie. Ozajstný slastný zážitok. 
   
 Von pučí jar. Svet sa rozjasňuje, ľudia v ľahkých kabátoch kúzlia úprimné úsmevy, na uliciach stretávam zaľúbené dvojice... 
   
 „Ale ja stále chodím s nepokrytou hlavou, 
 plnou trápenia." 
   
   
 „Prečo? Pozri na nádheru mesta. Nebuď smutná." 
   
 ...smutná nie som. Hoci, každý má na to právo. 
   
 Nie, nie som  smutná. Realita je subjektívna krása. Krása je realita. A ešte aká nádherná! Som jedným z miliárd ľudí.  Ako každý mám svoje spomienky. Svoju „psychologickú anamnézu", „úzkostné myšlienky", „negatívne jadrové schémy" , „škodlivé podmiené pravidlá", „maladaptívne reakcie", ... 
   
 A - ako každý - mám i toľko schopností, vzácnych spomienok, výnimočných ľudí v srdci, ušľachtilých pocitov, plánov a radostí. Preto sa veľmi snažím zabudnúť, myslieť, skúmať, pýtať sa, hľadať alternatívy, cestu von z bludných prstencov, tešiť sa, ľúbiť .... písať! Žiť! 
   
 „....no tak sa teda usmej! Pristane ti to!" 
   
 Ďakujem. Vo svojich básňach sa usmievam najkrajšie ako viem. 
   
 „...píšeme, píšeme, 
 posledná spodná sukňa noci je dávno popísaná 
 a nikto nevie čo je poézia" 
   
 ...a predsa to skúsim naznačiť, hoci na veľké veci definície nestačia. 
   
 Nemyslím si, že ozajstnej poézii ide o slová. Verše sú nepodstatné. Poznám veľa básnikov. Niektorí z nich napísali len pár basní a to zrejme ani jednu nikto okrem nich nečítal. Ba niektorí poéziu objektívne ani nevyznávajú...a predsa - sú to básnici. Velikáni poézie! Vidíte im to na očiach. Keď... 
   
 „...poézia je bolesť aj krása. 
 Len ľahostajnosť nepotrebuje poéziu. 
 Ale ľahostajnosť je choroba, na ktorú sa zomiera už za živa." 
   
 Môj drahý priateľ má more. Liek na ľahostajnosť. Má svoje myšlienky. Krásne lienky. Mám ich aj ja. Presne také, aké mi predkreslievala mamina, keď som bola malá. A ja som ich vždy nakreslila také veľké a škaredé. Tie jej boli útlunké a ... slobodné. Dnes viem podobné kresliť sama. Je to jednoduché, skúste to. Najlepšie sa kreslí do vzduchu (oproti slnku a rozihraným oblakom) a na ľudské líca. Lienky - myšlienky. Cestičky k sebe. 
   
 „Tak chodím ráno zbierať mušle po pláži 
 v záhyboch vĺn a ticha 
 hľadať zmienku o sebe" 
   
   
   
 (utorok 23.3.2010, Praha, Břevnov) 
   
   

