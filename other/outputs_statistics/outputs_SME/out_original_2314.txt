
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dávid Králik
                                        &gt;
                Aj také sa mi stalo
                     
                 A na záver "Hepáč" 

        
            
                                    19.2.2010
            o
            15:15
                        (upravené
                19.2.2010
                o
                15:32)
                        |
            Karma článku:
                9.54
            |
            Prečítané 
            2062-krát
                    
         
     
         
             

                 
                    som naivne očakával, ale vyvinulo sa to trošku inak.
                 

                 Vchádzam do triedy. Už prvý pohľad mi vraví, že sa čosi udialo. Rozbláznené decká zbierajúc zo zeme burizóny a jediac ich, sa prekrikujú, že ten a ten rozsypal tomu a tomu burizóny. Okamžite som zasiahol. "Vinníci zajtra prinesú nový balíček!" vyhlásil som rezolútne bez akéhokoľvek ďalšieho vyšetrovania. O čosi neskôr (zajtra) sa ukázalo, že "vinníci" síce vysypali spolužiakovi burizóny, no predchádzalo tomu nadávanie z jeho strany. Sadli sme si do kruhu a začali o celej situácii diskutovať. Dospeli sme spoločne k názoru, že nebolo správne počiatočné nadávanie, no ani následné riešenie chalanov (otvorene som priznal, že ani ja som neostal bez viny, lebo moje prvotné vyhlásenie bolo unáhlené, bez poznania celej skutočnosti). Burizóny sme si napokon rozdelili všetci. Nadôvažok sme si spravili hodinu etiky, kde sme opäť budovali tímového ducha. Po niekoľkých hrách - "Energizéroch" (niekedy by som tieto hry nazval pokojne i Emergizmérmi" :o) sme sa dostali k finálnej úlohe. Na vyhradenej ploche pred nami bolo trinásť ceruziek (v triede mám dvanásť žiakov, ale keďže sa cítim ako súčasť tímu, počet ceruziek bol 13). Vytiahol som z ruksaku balíček cukríkov. "Pre mňe sme jedna partia. Áno, každý z nás má nejaké chyby, no sme tím a podľa mňa, práve naše odlišnosti z nás robia skvelý tím. Každopádne, máte právo na svoj názor a tak máte aj vy teraz možnosť vyjadriť sa k tejto veci. Tu sú čokoládové cukríky. Pre každého z nás je tu jeden. No ak si niekto z nás myslí, že ktosi iný nepatrí do nášho tímu, vezme jednu ceruzku a povie jeho meno. Dieťa, ktoré bude označené spolužiakom, ako ten, kto nepatrí do našej triedy, nedostane cukrík."   Možno, ak by sme boli v americkom filme a hral by nejaký "doják", tak by všetky deti ostali potichu sedieť, usmievali by sa na seba a nikto by nevzal ceruzku a nepovedal meno svojho spolužiaka. Prišiel by "hepáč", teda chcem povedať Happy end a všetci by sme si padli do náručia a... Ale my sme neboli v americkom filme a ani hudba nehrala a tak sa "hepáč" nekonal. Pár detí, možno ešte pod vplyvom predchádzajúcich udalostí, vzalo ceruzky a povedalo: "Ten a ten sem nepatrí, lebo..."       Keď skončilo celé kolo, v ktorom mal každý možnosť vyjadriť sa, poprípade označiť spolužiaka, ktorý nepatrí do triedy, ujal som sa slova. "Pre mňa ste jedna trieda-partia," vravím a rozdávam čokoládové cukríky tým, ktorí nemajú v ruke žiadnu ceruzku. Tým, ktorí neoznačili nikoho, kto by mal opustiť našu triedu. Tým, ktorí dokázali prehrýzť krivdy, ktoré na nich iní napáchali (lebo detský kolektív vie byť neľútostný a na každom z nich sa nejaké tie krivdy skôr, či neskôr napáchali).   Ešte hodnú chvíľu zostali všetci prekvapene sedieť. Neviem, či pochopili, čo sa im snažil povedať a už vôbec neviem, či som konal správne a či moje konanie nebude kontraproduktívne, no pre mňa je to stále jedna trieda. Jedna partia. Jeden tím...   ..."a preto sa nikdy nepýtaj, komu zvonia, lebo vždy zvonia tebe..." 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Redakcia SME sa musela zblázniť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            To najlepšie nakoniec...už dnes
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Ignoranti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Čo už by mi len mohol ponúknuť prepážkový zamestnanec Tatra Banky?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dávid Králik 
                                        
                                            Možno máte dieťa vo veku 9 až 99 rokov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dávid Králik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dávid Králik
            
         
        davidkralik.blog.sme.sk (rss)
         
                                     
     
        Bývalý (m)učiteľ na I. stupni ZŠ, ktorý sa platonicky priatelí s Jonathanom Livingstonom, so Siddhárthom a hobitmi. Znamenie vodnára ho predurčuje k uletenosti všetkými smermi - hudba, literatúra, šport ...  




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    930
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3168
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja malá fikcia
                        
                     
                                     
                        
                            Viera ako ju cítim
                        
                     
                                     
                        
                            Aj také sa mi stalo
                        
                     
                                     
                        
                            Moje malé postrehy
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Život je krásny
                        
                     
                                     
                        
                            Malé gramatické okienko
                        
                     
                                     
                        
                            Logopedické okienko
                        
                     
                                     
                        
                            Rozprávky (aj) pre dospelých
                        
                     
                                     
                        
                            Milovaťžofiu
                        
                     
                                     
                        
                            Nedeľná ... veď viete : o )))
                        
                     
                                     
                        
                            Biblia očami laika : )
                        
                     
                                     
                        
                            ...cestou ma vanie...
                        
                     
                                     
                        
                            šerm
                        
                     
                                     
                        
                            moje hudobné výlevy
                        
                     
                                     
                        
                            Etika u mňa v triede :o)
                        
                     
                                     
                        
                            Moja poviedkovňa
                        
                     
                                     
                        
                            Pracujem ako divý :o)
                        
                     
                                     
                        
                            baška
                        
                     
                                     
                        
                            (pre mňa) zaujímaví ľudia
                        
                     
                                     
                        
                            na slovíčko
                        
                     
                                     
                        
                            Nápadník
                        
                     
                                     
                        
                            Súťaž krátkych videí
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Veľmi (ne)chutná hmyzia knižka
                                     
                                                                             
                                            Niektoré rany sú príliš hlboké a nikdy sa skutočne nezahoja
                                     
                                                                             
                                            (pre mňa) Môj najlepší rozhovor
                                     
                                                                             
                                            Logopédia už aj vo vašej škôlke? Prečo nie :)
                                     
                                                                             
                                            Have a nice Day(vid:)
                                     
                                                                             
                                            Kam odchádzam zo školstva
                                     
                                                                             
                                            Chceli by ste ísť do neba, potom vedzte...
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Najlepšia logopédka v Rači/ na svete
                                     
                                                                             
                                            FELIX
                                     
                                                                             
                                            iní
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




