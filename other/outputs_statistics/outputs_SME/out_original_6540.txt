
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s Medveďom - Slané lievance (dolky) 

        
            
                                    16.5.2010
            o
            12:00
                        (upravené
                17.7.2010
                o
                14:30)
                        |
            Karma článku:
                11.77
            |
            Prečítané 
            6260-krát
                    
         
     
         
             

                 
                    Ahojte, milí naši.  Lievancová sága začala v našom medveďom brlohu už asi pred tromi týždňami. Dostal som chuť na čerstvú prvú jarnú bryndzu a chcel som ju trochu nevšedne zakomponovať do nejakého dobrého jedla. No a napadli ma lievance. Sladké lievance nie sú u nás ničím výnimočným, ostatne jeden z našich starých receptov, fotografovaný ešte mobilom sú práve veleúspešné sladké lievance pripravované z kysnutého cesta. Deti, schválne si pozrite tú čítanosť.:)
                 

                    Slané lievance sme však u nás skúsili po prvýkrát. A ihneď sme sa do nich zamilovali až po naše medvedie uši. Takže vám dnes ponúkam tri druhy slaných lievancov, veď čo budeme trocháriť.   Lievance z kukuričnej múky s bryndzovou omáčkou   Schválne som ich urobil z kukuričnej múky, aby aj bezlepkáči videli, že lievance nemusia byť pre nich zakázanou neznámou. Použiť môžeme ako kukuričnú múku, tak aj kukuričnú krupicu. Dôležité je, aby zarobené cesto dlhšie odpočívalo, najlepšie cez noc. Kukuričná múka či krupica v ňom pekne napučí a cesto bude skvelé, hladké a bude pekne držať pokope. Obzvlášť pri kukuričnej múke som zvyknutý na mierne trpkú dochuť. Dobrá výraznejšia omáčka, či plnka v slanej či sladkej forme ju však dokonale prekryje.   Potrebujeme:   Na 4 porcie asi 0,5 l mlieka, 2,5 dcl kyslej smotanu, 30 dkg kukuričnej múky, 2 vajíčka, pol balíčka prášku do pečiva, soľ, kôpor  (čerstvý či sušený - nepreháňať s množstvom), 15 dkg bryndze, štipku soli a olej na vysmážanie.         Postup je veľmi jednoduchý. 0,5 l mlieka dobre zmiešame s kukuričnou múkou, práškom do pečiva, vajíčkami, kôprom a štipkou soli.      Zarobené cesto necháme aspoň 2-3 hodiny, ale najlepšie cez noc  odstáť. Ako som spomínal, kukuričná múka pekne nabobtná.         Na olejom dobre vytretom lievanečníku vysmážame krásne nadýchané lievance.         Vôbec nepotrebujeme kvasnice, stačí trocha prášku do pečiva a lievance sa pomaly vznášajú po kuchyni. :)         Vezmeme bryndzu a dobre ju vymiešame s kyslou smotanou. V prípade potreby ešte rozriedime trochou mlieka. Jednoduchou omáčkou potom lievance zalievame. Výborné sú aj ako jednoduché raňajky. Ostatne v amerických seriáloch pomaly iné na raňajky ani nerobia. Samozrejme na sladko a s javorovým sirupom.      Dobrú chuť.         Lievance ako príloha   Potrebujeme:   Na 4 porcie 0,5 l mlieka, 25 dkg hladkej múky, 2 vajíčka, pol balíčka prášku do pečiva, 2 v šupke uvarené zemiaky, soľ a olej na vysmaženie.         Opäť jednoduchý postup. Zemiaky ošúpeme, na jemno nastrúhame, pridáme mlieko, vajíčka, múku, prášok do pečiva, soľ a dobre metličkou vyšľaháme.      Necháme aspoň hodinu postáť.         A tu už smažíme naše lievance. Budú nesmierne jemné. Môžeme ich plniť všetkým možným od výmyslu sveta, alebo ich použiť ako prílohu napríklad k paprikášu z paprík, húb a sójových rezancov.         Deti, v paprikáši som použil čínske čierne huby (Judášove uši), ktoré sa mi veľmi neosvedčili, pre nevýraznú chuť a príliš tvrdú konzistenciu, ktorá je pre čínsku kuchyňu želateľná, ale v paprikáši pôsobila predsa len trochu rušivo. Presnejšie chutili tak trochu ako guma od slipov.:) V zozname ingrediencií preto odporúčam šampiňóny, ale určite by bola vhodná aj Hliva ustricová, ktorú však treba tiež veľmi dobre podusiť.   Takže potrebujeme:   25 dkg šampiňónov, 1 balíček sójových rezancov  (mäsožravci nahraďte čím chcete), 0,5 kg paprík (vhodné sú aj farebné,  ale tie sú teraz drahé, jak šľak), viazaničku mladej cibuľky, 0,5 l  smotany na varenie, olej, čierne mleté korenie, sladkú mletú papriku,  1-2 lyžice hladkej múky, kukuričnej múky, alebo Solamilu, slanú sójovú  omáčku a soľ.         Nakrájanú cibuľu necháme speniť na oleji, pridáme huby, na väčšie kúsky nakrájané papriky a sójové rezance. Tie sme samozrejme predtým prevarili v posolenej vode a precedili.      Pridáme sladkú mletú papriku, sójovú omáčku, prípadne trochu soli, mleté  čierne korenie, dobre podlejeme a dusíme do mäkka. Na záver vlejeme  smotanu dobre rozmiešanú s múkou, alebo Solamilom a ešte prevaríme.         Tak a tu máme našu prezentáciu. Poviem vám huby - nehuby, či uši - neuši zblajzli sme to ako keby sme boli po dlhej protestnej hladovke za záchranu morských sasaniek.      Poviem vám huby - nehuby, či uši - neuši, zblajzli sme to, ako keby sme boli po dlhej  protestnej hladovke za záchranu morských sasaniek. :)         Deti, skutočné mekice som na našom blogu ešte nerobil. Zato dnes vyskúšam už  ich druhú inšpiráciu. Toto je ta prvá. - je na spodku receptu.   Nuž a toto druhá.   Lievance na spôsob mekice:      Potrebujeme:       0,5 l jogurtu, 3 dcl mlieka, 20 dkg balkánskeho syra, 2 vajíčka, 20 dkg hladkej múky, pol balenia prášku do pečiva, soľ a olej na vysmážanie.         Postup jasný ako facka. Balkánsky syr nastrúhame, pridáme 3 dcl jogurtu a 3 dcl mlieka, 2 vajíčka, múku, prášok do pečiva a štipku soli. Dobre vymiešame a necháme aspoň hodinu postáť.         Smažíme krásne a jemné lievance. Od skutočných bulharských mekíc sa líšia len tým, že mekice sa vysmážajú vo väčšom množstve oleja (ako langoše) pričom sa hustejšie cesto nalieva do formy menších placiek do rozpáleného oleja. Mastné ale vynikajúce.         Naše lievance podávame so zvyšným jogurtom,      ale mimoriadne chuťovo zaujímavá kombinácia je  podávanie slaných mekíc - doliek potretých s dobrým sladkým džemom,  najlepšie z višní či iného sviežeho ovocia. Veľmi zaujímavá zmyselná  kombinácia slaného so sladko ovocným. Vrelo odporúčam vyskúšať.   Dobrú chuť a príjemnú nedeľu.          tenjeho         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (51)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




