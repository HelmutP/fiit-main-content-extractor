

 Asi teraz sklamem všetky politické očakávania od tohto článku. Pravdupovediac, v máji som oslávil len šestnásť rokov a o slovenskej politike mám asi taký prehľad, ako Iľko o zajtrajšom počasí... 
 Voľby sledujem často a vždy mám svojho favorita. Ešte ale nikdy som na to nešiel politickým rozmýšľaním. Keď sa volil prezident, môj favorit bol Gašparovič. Nie, že by som rozmýšľal, že prečo on a nie niekto iní. Skrátka, je to Slovanista! 
 Teraz, keby som volil, môžete len hádať, komu by som dal hlas. Som za srandu! Preto by som volil Paliho Kapurkovú (áno, aj SMK by bola sranda, ale aj sranda má svoje hranice)! Teraz si asi hovoríte, že „Mladý, ty keď dovŕšiš osemnásť, radšej ani voliť nechoď!”, ale kľud: zatiaľ to tak vnímam len preto, lebo nevolím - nemôžem. 
 Naozaj, načo sa zamýšľať nad tým, koho voliť, keď nemôžem? Naozaj ma udivuje, keď nejaký 13-ročný napíše dlhočízny článok alebo komentár o tom, koho treba voliť a koho nie. Samozrejme, aj ja sledujem, čo sa deje v politike, ale určite nebudem radiť niekomu, kto má tridsať rokov, že koho treba voliť. 
 Každopádne, na tieto voľby sa teším najviac. Naozaj, takto som sa ešte netešil na žiadne! Nie je to preto, že som zvedavý, koľko ľudí chce ukázať politikom zdvihnutý prst (Paliho Kapurková), alebo koľko ľudí chce beztrestne húliť marišku alebo je za eutanáziu, či potraty (SaS), alebo koľko ľudí sa chce vrátiť k starému-dobrému? (SDKÚ, SMER, či nebodaj KSS). Je to kvôli niečomu úplne inému. 
 V čase volieb totiž budem, ak sa všetko podarí, u priateľky. Konečne, po dlhšom čase. Hrozne sa teším, pretože si užijem ju a zároveň aj majstrovstvá vo futbale. Takže najlepšie voľby, aké som zažil. Aj keď sa to volieb netýka... Čo Vy plánujete robiť počas volieb? (Možno) Okrem občianskej povinnosti voliť? 

