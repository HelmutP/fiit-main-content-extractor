
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mirka Vávrová
                                        &gt;
                Alica v krajine zazrakov
                     
                 Nie je billboard ako billboard... 

        
            
                                    6.6.2010
            o
            22:27
                        |
            Karma článku:
                7.35
            |
            Prečítané 
            1558-krát
                    
         
     
         
             

                 
                    Posledné jazdy autobusom som si skutočne užila. V bezprostrednom okolí míňam na trase dom-práca a späť len niekoľko billboardov. Keď sa preto z času na čas preveziem cez pol mesta, nestačím sa diviť, aké rozmery nabrala naša predvolebná kampaň. Cestovanie v posledných dňoch bolo o to vzrušujúcejšie, že medzi desivo veľkými hlavami politikov som hľadala billboard ku kampani za dôstojný život ľudí s mentálnym postihnutím. Náš billboard.
                 

                 Bol to zvláštny pocit. Zmes hrdosti, radosti i obáv, či vás kampaň zaujme. Súperov má silných. Sľubujú trinásty dôchodok. Istotu v ťažkých časoch. Lepšie Slovensko. Ochranu a spoluprácu. Všetci svorne myslia na budúce generácie. Niektorí aj na dovolenky a naše mesto. Tí paranoidnejší nás varujú pred susedmi a parazitmi. Veľké hlavy politikov sa na nás usmievajú, zuby o poznanie belšie, tvár bez vrások i bez pórov, snedá a príťažlivá. Dôkladne premyslená štylizácia i výber kravaty či kostýmu. Uveriteľné je snáď len ich meno.   Naproti tomu stojí náš billboard. Zobrazuje človeka s mentálnym postihnutím, ktorý fandí Slovensku v blížiacich sa majstrovstvách sveta vo futbale. Radostne kričí Slovenskooo no nezabudne poznamenať, že i on je jeho súčasťou. Chce žiť s nami. Nielen vedľa nás. Nesľubuje sociálne dávky ani lepšiu budúcnosť pre ďalšie pokolenie. Uspeje so svojou nenáročnou túžbou stať sa súčasťou našej spoločnosti?   Včera som sedela v autobuse a hľadela von oknom. Keď som pred Patrónkou zazrela nášho fanúšika s jeho prostou správou smerom k verejnosti, oči mi zaslzili. Hrdosťou. Naša niekoľkomesačná ťažká práca priniesla svoje prvé sladké plody. S kampaňou len začíname a tých plodov čakajte ešte niekoľko. Čo nečakajte, sú sľuby. Nechceme kafrať do remesla našim majstrom politikom. Čo chceme, je váš záujem. Jednoduchý, ľudský. O ľudí okolo vás. Aj o tých nevyretušovaných, ktorí môžu vyzerať inak ako my. Sú iní. Sú preto horší?   12. júna sa okrem parlamentných volieb uskutoční aj Deň krivých zrkadiel. V rôznych mestách Slovenska sa budú konať rôzne akcie s mottom kampane za dôstojný život ľudí s mentálnym postihnutím: Sme iní, nie horší. Chceme žiť s vami, nie vedľa vás. Ak ste sa rozhodli podporiť kampaň politikov s priehrštím prázdnych sľubov svojou účasťou na voľbách, podporte aj nás. Ich kampaň pomôže najmä im samotným. Naša aj vám.   www.zpmpvsr.sk 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Letné prázdniny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Pani učiteľke s láskou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Pohľad späť (cez spätné zrkadlo)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Malé špongie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mirka Vávrová 
                                        
                                            Návraty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mirka Vávrová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mirka Vávrová
            
         
        mirkavavrova.blog.sme.sk (rss)
         
                                     
     
        Milujem a neúnavne študujem život.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    58
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1418
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Alica v krajine zazrakov
                        
                     
                                     
                        
                            la vita é bella..niekedy
                        
                     
                                     
                        
                            Svet detí (a ja v ňom)
                        
                     
                                     
                        
                            Lásky jednej rusovlásky
                        
                     
                                     
                        
                            Išla Mirka na vandrovku
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dobrá škola (časopis)
                                     
                                                                             
                                            English is easy, Csaba is dead
                                     
                                                                             
                                            Rešpektovať a byť rešpektovaný
                                     
                                                                             
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            30 seconds to Mars
                                     
                                                                             
                                            Passenger
                                     
                                                                             
                                            Emeli Sande
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mary - lebo píše ľahučko a krásne
                                     
                                                                             
                                            Sjuzi - lebo :)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Letné prázdniny
                     
                                                         
                       Pani učiteľke s láskou
                     
                                                         
                       Pohľad späť (cez spätné zrkadlo)
                     
                                                         
                       Malé špongie
                     
                                                         
                       Návraty
                     
                                                         
                       Klub anonymných strachoprdov
                     
                                                         
                       Jedna hodina, tri predsudky
                     
                                                         
                       50 centov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




