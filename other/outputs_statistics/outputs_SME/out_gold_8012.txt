

   
 „Ak si zoberieme štatistiky Eurostatu, tak sa priznajme, že v máji 2006, keď ste nám odovzdávali vládu, bola nezamestnanosť o 0,1 percenta vyššia ako je teraz,“ povedal premiér Robert Fico počas televíznej diskusie volebnej líderke SDKÚ-DS Ivete Radičovej. 
 V prvom rade si predseda vlády už zrejme nepamätá, že súčasná vláda nenastúpila v máji 2006, ale až začiatkom júla 2006. Docent práva Robert Fico si takisto evidentne nechce priznať, že ekonomika vykazuje istú zotrvačnosť a preto sa isto nedá tvrdiť, že „júnová nezamestnanosť je ešte vaša a júlová už naša“. Súčasná vláda totiž v roku 2006 do ekonomického nastavenia krajiny žiadny spôsobom nezasiahla. Keďže som na Právnickej fakulte študoval, dobre viem, že len veľmi málo študentov práva chápe ekonomické zákonitosti a ekonomické predmety im nerobili žiadne problémy. Fico k právnikom, ktorí rozumejú ekonomike, evidentne nepatrí. 
 Teraz k faktom.  Štatistický úrad SR dnes zverejnil údaj o tom, že miera nezamestnanosti na Slovensku v prvom štvrťroku tohto roka dosiahla 15,1 %. V druhom kvartáli 2006, kedy končila minulá vláda, bola miera nezamestnanosti na Slovensku podľa toho istého úradu 13,5 %. 
 Podľa metodiky Ústredia práce, sociálnych vecí a rodiny SR bola na Slovensku v apríli tohto roka miera nezamestnanosti 12,52 %. V júni 2006 vykázal rovnaký úrad mieru nezamestnanosti 10,36 %. 
 Pán premiér, prečo sa hanbíte pred ľuďmi priznať, že nie ste až taký Superman? Prečo nepriznáte, že v súčasnosti je miera nezamestnanosti na Slovensku vyššia, ako keď ste preberali vládu? Samozrejme, je to najmä kvôli svetovej kríze, ale do istej miery určite aj kvôli hospodárskej politike súčasnej vlády (Zákonník práce, minimálna mzda, zákon o službách zamestnanosti....) 
   

