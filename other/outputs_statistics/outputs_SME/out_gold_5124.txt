

 Mám mnoho priateliek, ktoré rezignovali v hľadaní lásky. Po tom, čo ich opustil, podviedol, nemiloval, alebo inak zdeformoval ich priateľ. Teraz pijú šálku kávy a vierohodne vyhlasujú : Normálni chlapi vymreli !!! 
 Mám kamarátov, ktorí sú i napriek značnej dávke sympatie, krásy, ambicióznosti stále samy. Za dôvod väčšinou dávajú prácu, málo času, alebo jednoducho nestretli tú pravú. Jednoducho každý, každého hľadá a nájdu len niektorí. 
 Neviem prečo to je tak, neviem či sme možno príliš náročný, príliš očakávame a hľadáme pre seba len to naj. Viem však, že pravý muži a pravé ženy nevymreli. Sedia niekde v útulnej kaviarni, s kamarátmi. Vyzerajú vyrovnane, majú zdravé sebavedomie a niekde v hĺbke svojho ja , túžia stretnúť človeka, ktorý by im rozumel. 
 Paradoxom toho je, že všetkých tých osamelých ,opustených ľuďí , niekto tajne miluje. Niekto o nich sníva a má rád ich spoločnosť. A možno nie tajne..možno o tom dotyční vedia, ale neriešia..Pretože hľadajú čosi "lepšie", "dokonalejšie". A o tom to je... O tom celom kolotoči, lások a nelások..o tom , že milujeme a nie sme milovaný, alebo naopak. 
 Keď budete sedieť v kaviarni s kamarátmi, skúste sa pozrieť k vedľajšiemu stolu..čo viete, možno tam bude rovnako vo vnútri opustená druhá polovička.. 
   
 Milujte sa.. 

