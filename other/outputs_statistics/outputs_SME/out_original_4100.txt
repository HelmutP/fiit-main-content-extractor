
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 O problémovej demokracii v Rusku televízne správy mlčali 

        
            
                                    7.4.2010
            o
            17:10
                        |
            Karma článku:
                13.18
            |
            Prečítané 
            24613-krát
                    
         
     
         
             

                 
                    Televízie nás včera skôr zabávali ako informovali o podstate fungovania Ruska
                 

                 
To s tým baľšim štátnikom bolo fakt dobré, baľšóje spasíba!foto SME/sme.sk - Vladimír Šimíček
   Sledujúc včerajšie televízne správy sa divák dozvedel, že prišiel politik z veľkej a dôležitej krajiny. Ale o čo menej sa mohol dozvedieť o stave domovského štátu hosťujúceho politika, o to viac dostával informácie o ňom so zbytočne veľkým rešpektom, a s priveľkým dôrazom na banality.  Napríklad:  Bratislava dnes víta vzácnu návštevu, ruského prezidenta Dmitrija Medvedeva (STV1). Aj napriek tomu, že ruský prezident Dmitrij Medvedev je nižšieho vzrastu - meria niečo vyše 160 centimetrov -  má povesť veľkého štátnika. Bez problémov sa totiž prispôsobí hostiteľskej krajine.(Markíza) Takmer 1000 ochrankárov a policajtov stráži hlavu Ruskej federácie počas návštevy na Slovensku. (TA3) K Medvedevovým koníčkom patrí aj plávanie a možno si tak bude chcieť zašportovať aj u nás. Z jedného brehu Dunaja na druhý by musel preplávať 5-krát, keďže denne zvládne 1,5 kilometra. (Joj) Z Ruska sa akreditovalo vyše 60 žurnalistov. (Markíza)   Samozrejme,  televízie nezabudli na jedno z najhorších žurnalistických klišé pri veľkých  udalostiach:    Bratislava je dnes jedným z najstráženejších miest na svete. Prišiel  Dmitrij Medvedev. (Markíza)  Bratislava sa od zajtra stane jedným z najstráženejších miest na svete.  (Joj)  Bratislava sa na 2 dni stane jedným z najstráženejších miest na svete.  (TA3)  Hoci to nie je taká sláva, ako pred piatimi rokmi, keď sa na Dunaji  stretol Bush s Putinom, aj tentoraz je Bratislava strážená ako oko v  európskej hlave. (Joj)   Čo sme sa naopak o návšteve ruského „veľkého štátnika“ nedozvedeli:     Rusko patrí k štvrtine najskorupomovanejších krajín sveta (Transparency International).   Rusko je v praxi niekde medzi demokraciou a medzi autoritárskym štátom v dolnej polovici krajín z pohľadu slobody. (EIU Democracy rankings)   Rusko patrí k osmine krajín s najhoršou slobodu médií na svete (Reportéri bez hraníc)   Rusko patrí medzi tretinu svetových krajín s najnižším priemerným vekom úmrtia. Muži sa v Rusku dožívajú v priemere 62 rokov (U.N.World Population Project)     O proteste aktivistov Amnesty International informovali v hlavných správach Markíza a STV stručne, len o uskutočnení protestu, nie o faktoch, na základe ktorých majú viacerí ľudia o Rusko obavy (ani v dnešných serióznych denníkoch nie je o tieto témy okrem SME záujem). Pritom keď naši politici či firmy robia s inou krajinou veľké dohody o spolupráci, je dôležité vedieť, o akej krajine je reč, ako sa tam robí politika a biznis. Televízie aj kvôli kváziakčným obrázkom sa viac zamerali na formality ako na podstatu.   Aby som nezabudol, objavil sa aj mimoriadne unikátne dokonalý živý vstup ako prvá (!) informácia včerajších Hlavných správ TA3:   Zuzana WENZLOVÁ, hlásateľka -------------------- Najsilnejší muž Ruska v najbližších chvíľa priletí na oficiálnu návštevu Slovenska. Pozornosť sa sústreďuje teraz najmä do prezidentského paláca, kde ho už čaká slovenský prezident Ivan Gašparovič. Na mieste je už aj náš kolega Peter Bielik. Peter, tak je už všetko pripravené na návštevu najsilnejšieho muža Ruska?  Peter BIELIK, redaktor -------------------- Áno samozrejme z našej strany je všetko pripravené. Počujeme sa trocha horšie, keďže za mnou hrá už vojenská kapela. No, ale na základe toho vidíme, že naozaj je pripravené všetko tak z našej strany, zo strany TA3 ako aj zo strany organizátorov tohto podujatia. Naozaj všetko dokonale. Zo strany TA3 preto, pretože na mieste máme až 3 kamery, ktoré o 19 hodine, keď je naplánovaný príchod ruského prezidenta budú na živo prenášať tento príchod a samotné prijatie. Mali by teda naši diváci mať hodnotný zážitok najmä z toho prijatia, pretože máme kameru umiestnenú tak tu na nádvorí ako aj na balkóne a v útrobách prezidentského paláca. Oproti ostatným médiám teda máme exkluzívne a mimoriadne postavenie a budeme vám môcť priniesť naozaj mimoriadne zábery. Čo sa týka samotnej návštevy, tá je po bezpečnostnej a samozrejme aj protokolárnej stránke naozaj pripravená dokonale.   Tak prvá správa dňa je, že TA3 si robí svoju prácu. A návšteva ešte nezačala a už je všetko podľa novinára dokonalé. Ani Gašparovičov hovorca (bývalý redaktor TA3) by to nepovedal lepšie.   Inak nepochybne prvú cenu za zahlásenie príspevku o návšteve ruského prezidenta získava TV Joj:   Prišli k nám Rusi. Opäť ako hostia, tentoraz však tanky nechali doma.      Opäť preferencie bez nových strán: Čitateľ ma upozornil na reportáž Anny Vojtekovej z TA3, ktorá z grafu preferencií podľa agentúr Focus a Polis nemenovala strany Most a SaS, teda 4. a 5. najpopulárnejšiu stranu. Okrem toho chýba aj HZDS, ktorá mala v prieskume Focusu nad 5%, hoci v Polise 4,4%. V tej istej reportáži vysielanej ešte včera poobede bola pôvodne tabuľka s číslami všetkých menovaných strán (viď reprofoto dole vľavo). Vo verzii pre Hlavné správy o 18:40 už komplet tabuľku nahradila tá o tri politické strany ochudobnená grafika. Obzvlášť pred voľbami by si na takéto veci mali televízie dávať pozor.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (145)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




