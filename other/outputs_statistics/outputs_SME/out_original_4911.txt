
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lubo Kužela
                                        &gt;
                Informatika
                     
                 Excel: Práca s textovými reťazcami v bunkách 

        
            
                                    21.4.2010
            o
            7:50
                        (upravené
                20.4.2010
                o
                14:02)
                        |
            Karma článku:
                10.93
            |
            Prečítané 
            5275-krát
                    
         
     
         
             

                 
                    Čo urobiť, ak máte v jednom stĺpci meno a priezvisko, ale potrebujete iba priezvisko? Riešením je napríklad skopírovanie priezviska do inej bunky. Niektoré funkcie práce s textovými reťazcami ukážem na jednoduchom príklade.
                 

                 1) V stĺpci A máme mená a priezviská. Funkcia LEFT() nám vyberie prvých 5 znakov z bunky A1:       Funkcia LEFT() teda vyberá požadovaný počet znakov z príslušnej bunky.   2) Na tomto príklade zistíme polohu medzery (prázdneho znaku) v bunke A1 pomocou funkcie FIND():      Funkcia FIND() vracia poradové číslo vybraného znaku v bunke. V tomto prípade je medzera v  bunke A1 ôsmym znakom z ľava.   3) Teraz môžeme spojiť funkie z príkladov 1 a 2. Zístíme polohu medzery, a vyberieme znaky v bunke po túto medzeru (t.j. meno)      Funkciou FIND() teda zistíme polohu medzeri a túto hodnotu použijeme ako parameter funkcie LEFT(). Tá nám vyberie prvých 8 znakov z bunky A1.   4) Počet znakov v bunke môžeme zistiť funkciou LEN(). Na tomto príklade zisťujeme počet znakov v bunke A1:      V tomto prípade obsahuje bunka A1 14 znakov (počíta sa aj medzera).   5) Ak chceme vybrať priezvisko, musíme použiť funkciu MID().   Syntax funkcie: MID(bunka;znak_1;znak_2)   bunka - označenie bunky z ktorej vyberáme znaky   znak_1 - poradové číslo prvého vybraného znaku v bunke   znak_2- poradové číslo posledného vybraného znaku v bunke   Nasledujúci príklad nám vyberie z bunky A1 znaky začínajúce medzerou, teda FIND(" ";A1), po posledný znak (poradové číslo znaku je zhodné s počtom znakov bunky A1 v stĺpci C)          V tomto prípade nám do bunky D1 skopírovalo aj medzeru. V prípade, že chceme kopírovať priezvisko bez medzery, musíme posunúť poradové číslo prvého kopírovaného znaku o jedna:   =MID(A1;FIND(" ";A1)+1;C1)   6) Poslední príklad ukazuje spájanie reťazcov. Spojí sa nám meno v stĺpci B a priezvisko v stĺpci D:      V tomto prípade spájame text bunky B1 s textom bunky D1.       Hotový príklad si môžete stiahnuť tu.   Poznámka: Na vytvorenie príkladov bol použitý Microsoft Office Excel 2007, SP2.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Excel: Načo je tlačidlo "Scroll lock"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Excel: Fotka ako komentár bunky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Predpovede pohybu akciového trhu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Filtrovanie pošty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Pár technických vtipov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lubo Kužela
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lubo Kužela
            
         
        kuzela.blog.sme.sk (rss)
         
                                     
     
         Pracujem ako konzultant v IT. Zaujímam sa o technológie a ekonómiu. 
 
V prípade záujmu pomôzem s Excelom, prípadne naučím vybranú oblasť alebo spracujem zadanie. 

  
makrá, kontingenčné tabuľky, grafy, reporty, import údajov, databázy, výpočty, prehľadávanie a analýza dát.. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    61
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2778
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Informatika
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Veda
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Denník ítečkára :))
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Steve Wozniak - iWoz
                                     
                                                                             
                                            Dan Ariely - Jak drahé je zdarma
                                     
                                                                             
                                            Randall Stross - Planeta Google
                                     
                                                                             
                                            Leander Kahney - Jak myslí Steve Jobs
                                     
                                                                             
                                            Nick Mason - Pink Floyd
                                     
                                                                             
                                            Liker - Tak to dela Toyota
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            RADIO_FM
                                     
                                                                             
                                            Julee Cruise
                                     
                                                                             
                                            Belle and Sebastian
                                     
                                                                             
                                            I AM X
                                     
                                                                             
                                            RADUZA
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            LenTak
                                     
                                                                             
                                            Pinus@Chicago
                                     
                                                                             
                                            EcoNoir
                                     
                                                                             
                                            Rado Baťo
                                     
                                                                             
                                            Marek Psenak
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            HN Online - Stlpceky
                                     
                                                                             
                                            Radio_FM - Playlist
                                     
                                                                             
                                            eTREND - Odbornejsie blogy
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




