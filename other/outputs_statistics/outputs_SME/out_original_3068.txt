
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jarmila Tvaruzkova
                                        &gt;
                Nezaradené
                     
                 Grand Canyon - zostup do pekla 

        
            
                                    22.3.2010
            o
            3:32
                        (upravené
                22.3.2010
                o
                4:11)
                        |
            Karma článku:
                6.33
            |
            Prečítané 
            1320-krát
                    
         
     
         
             

                 
                    V jedno marcové ráno som s rodičmi čakala na príchod autobusu v Grand Canyon Village. S batohmi na chrbtoch, s paličkami v rukách a s čapicou na hlave sme pozorovali ako sa začína rozvidnievať.
                 

                 
South Kaibab Trail/Grand CanyonJarmila Tvaruzkova
   Niečo po šiestej dorazil náš autobus smerom k South Kaibab Trail. Na zastávke vystúpil ešte postarší nemecký pár a vydali sme sa zľadovatelými serpentínami z rímsy do kaňonu. Večer predtým sme sa pýtali rangerky v centre pre návštevníkov na našu trasu - zísť a vyjsť Veľký Kaňon za jeden deň. Nedoporučujú. Nie sme predsa nejaké "béčka", ideme! Na začiatku cesty sme si trochu nadávali, že sme doma nekúpili tie turistické mačky. Tu boli drahé na našu škrobivú slovenskú náturu, čo sa dalo očakávať v strede jedného z najobľúbenejších amerických národných parkov. Pomocou paličiek sme úvodný ľadový terén našťastie zvládli a ďalej sme pokračovali po suchu.   Ranné slko sa hralo s farbami Veľkého Kaňonu a ja som sa zamilovala do tej oranžovočervenej zeme. Slnečné lúče spolu so zostupom nás rozohriali, tak sme schovali bundy do batohu a vybrali foťáky na zvečnenie tejto chvíle. South Kaibab Trail je jeden z mála chodníkov vo Veľkom Kanone, ktorý ide po hrebeni. Nie je tam tieň ani voda. Marcová teplota bola ale veľmi príjemná a my sme pokračovali v zostupe schodovitým chodníkom. Cestou sme stretli skupinu mladých ľudí, ktorí mali nižšie stanový kemp a opravovali chodník. Za dve a pol hodiny sme zišli až k rieke Colorado. Zvrchu vyzerala ako taký potok, zblízka vcelku rýchla blatová rieka. Dali sme prednosť niekoľkým výpravám mulov smerujúcim do práce nosiť ľudí do kaňonu(počula som, že cena okolo $300) a prešli sme cez most na druhú stranu rieky.   Veľký Kaňon zvrchu vyzerá ako veľká hlboká skalnatá diera. Po zostúpaní dole viac ako 1400 metrov k rieke sú to však obrovské kopce. Je tam teplejšie, rastú tam iné kvety, žijú tam iné živočíchy. Ten zostup je ako cesta do minulosti. Skaly na rímse sú oveľa mladšie ako ich praprarodičia dole v "pekle". Po chvíli rozjímania a jedenia pri rieke sme sa vydali na cestu späť.   Strieborným mostom sme druhýkrát prekročili rieku Colorado a napojili sa na Bright Angel Trail. Piesková cesta viedla kus popri rieke a juka palmách. Teplota by bola aj vhodná na opaľovanie sa, náš však čakal výstup na rímsu. Po asi dvoch míľach sme vošli do bočného kaňonu s potokom. Chládok a tieň bol vítanou útechou so zvyšujúcim sa sklonom chodníka. Šlapali sme v kuse až do Indiánskej záhrady ktorá je asi v polovici cesty. Chceli sme si tam nabrať vodu, ale nechtiac sme to prešli a už sme sa nechceli vracať. Dopriali sme telu trocha energie v podobe chleba, ovocia a orieškov ktoré nám skoro zjedla hladná okoloidúca veverička. Náš cieľ bol stále v nedohľadne, ale naše nohy už cítili prechodené kilometre.   Od vrchola nás delilo štyri a pol míle serpentín. Až teraz som si plne uvedomila výhodu "Camelbaku", plastového sáčku na vodu s hadičkou čúhajúcou z batohu. Stehná prestávali spolupracovať a vracali mi presilenie z atletiky minulý rok. Hrejivým pocitom pri srdci mi ostávalo to, že vrchol vidím, ale rieku nie. Hovorila som si, že keď to zvládnu moji rodičia, ja by som to mala vybehnúť ako srna. Aj to možno snáď čoskoro príde. Už sme stretávali čoraz viac ľudí s čoraz horšou výstrojou. Podaktorí boli schopní ísť niekoľko míľ strmým kopcom po slnku v sandáloch, bez vody a jedla. Posledná míľa a pol bola blatovosnehová, ale to už sme boli takmer v cieli. Zvládli sme to za osem a pol hodiny späť na rímsu. Unavení sme ešte zašli kúpiť známky a pohľadnice a výpravu sme zakončili krátkym behom na autobus do kempu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jarmila Tvaruzkova 
                                        
                                            Snow days alebo ďalší dôvod prečo sa tešiť zo snehu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jarmila Tvaruzkova 
                                        
                                            Salsa, Doritos, a ľad v džúse
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jarmila Tvaruzkova 
                                        
                                            Tam kde sa spája veda, architektúra a umenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jarmila Tvaruzkova 
                                        
                                            Dvojitý študent
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jarmila Tvaruzkova 
                                        
                                            Nie je USA ako USA
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jarmila Tvaruzkova
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jarmila Tvaruzkova
            
         
        jarmilatvaruzkova.blog.sme.sk (rss)
         
                                     
     
        som zvedavý človiečík, ktorý sa často pýta "A prečo...?"
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1200
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            škola, školička
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Physics and the Art of Dance
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




