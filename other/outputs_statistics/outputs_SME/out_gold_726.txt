

 
„Vaša milovaná pravicová vláda čo urobila pre deti – nič,“
vyhlásil v stredu na tlačovke premiér Fico.
 
 
Rozmýšľam, či mám napísať politicky korektne, že sa pán
premiér mýli, alebo politicky nekorektne, že ten boľševik trepe ako vždy
sprostosti. Takže prosím vás odkážte tomu pajácovi, že tá jeho „krajne
pravicová“ vláda zaviedla daňový
bonus, vďaka ktorému moja rodina minulý rok získala 26 280 Sk, ktoré
som ušetril na dani z príjmu. A za tieto ušetrené peniaze som si
nekúpil Jach&amp;Tu v Monaku, ale „investoval“ som ich do svojich detí.
 
 
A mimochodom, čo pre moje deti za prvé dva roky svojho
vládnutia urobila ľavicová vláda s akousi „silnou sociálnou
politikou“? Nuž zaviedla tzv. milionársku, teda skôr polmilionársku
daň (© Jozef Mihál), takže ak tento rok zarobím viac ako 498 000 Sk tak
moja rodina z toho nebude mať až tak veľa, lebo štátu odvediem vyššiu daň
za to, že som sa viac snažil a chcel svojej rodine vylepšiť ekonomickú situáciu.
A to sa v časoch ľavicovej vlády nemá. V časoch ľavicovej vlády
treba čakať, kým pán premiér a teta z ministerstva sociálnych vecí láskavo
rozhodnú o zvýšení prídavkov na deti o 100 Sk. Pokiaľ možno až
v druhej polovici volebného obdobia, aby to až tak veľa nestálo a aby
si to voliči pri najbližších parlamentných voľbách ešte pamätali.
 

