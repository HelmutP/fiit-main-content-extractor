
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alena Škutchanová
                                        &gt;
                Medické
                     
                 A naozaj pitvete? Článok druhý, pitvy po tretí krát. 

        
            
                                    10.5.2010
            o
            6:49
                        |
            Karma článku:
                11.46
            |
            Prečítané 
            2569-krát
                    
         
     
         
             

                 
                    Smrť a život sa nedajú oddeliť. Na stážach z gynekológie sme videli pôrod. Súdne lekárstvo sme mali tento rok ako posledné zo stáží.   CSI Martin. Všetky tie veci, čo v správach štvorčekujú, tentoraz pre nás bez štvorčekov.   Plurima mortis imago - mnohoraká je podoba smrti. (Článok nie je určený pre citlivé povahy)
                 

                 Pitvy k stavu medickému patria.   Niekedy to človek v pitevni ustojí a cvičenie skončí, ani nevie ako. (Ľudské telo, napríklad pre mňa bolo, je a bude zdrojom fascinácie. Mozog zvlášť.). Inokedy to bolí už len keď vidí fotky.   Súdnym lekárom sa pod ruky dostane každý, u koho nie je príčina smrti jasná, kto zomrel chirurgom na stole (mors in tabula) alebo sa vyšetruje, či jeho smrť nenastala chybným lekárskym postupom, každý, kto z tohto sveta zišiel násilím, či už vlastnou alebo cudzou rukou.   Už nie je doma, vo svojom oblečení, špinavý. Na pitevnom stole to už je človek vytrhnutý z kontextu, nahé telo na pracovnom stole. Po prvom nafotení umytý asistentom.   Výsledok je niekedy zarážajúci. Mladý chalan, príjemná tvárička, akoby spal. Až na jeden detail. Že si strelil do spánku. Vôbec, tie obrázky ľudí tak podobných tým, čo nám ukazujú chirurgovia a intenzivisti, medik naučený, že zachránili sme... a potom prásk! Na tretej fotke zo série zranenie, ktoré mladý muž (iný prípad ako prvý spomínaný) určite nerozchodil.   Nech si hovorí kto chce, čo chce o rovnocennom prístupe, ak je onou mŕtvolou starý spitina (z ktorého vnútorností po otvorení ešte tiahne), nebolí to ani zbla tak, ako keď z pozostatkov kričí, že je to dieťa alebo mladý človek. Na celom tom nie je najhoršia tá bezkontextová mŕtvola na pitevnom stole. Je to ten príbeh, ktorý ju tam dostal. Niekedy pracne skladaný najlepšími odborníkmi.   Situácie niekedy tak kontrastné, až sa javia pretkané istým zvláštnym poetičnom, svet inou optikou, tak pokrivenou, že na konci je niekedy dúha. Púpavové pole, bezoblačná jarná obloha. Uprostred poľa malé lietadlo zaryté čumákom do zeme, v ňom mŕtvy pilot. "Keď som tam prišiel, okolo stáli policajti a fajčili jednu od druhej. Keby to videl Svěrák, natočí o tom film."   Scenáre smrti, niekedy absurdné, niekedy priam zbytočné. Také, aké napíše len život so smrťou.   Panoptikum nášho slovenského minisveta. Postavy, postavičky, mafiánske vraždy a po rokoch vykopávané mŕtvoly. Biele kone, veľké zvieratá aj malé ryby, v smrti si všetci rovní navzájom. Psychopatickí vrahovia, domáce násilie, znásilnenia, sebevraždy jednoduché aj prešpekulované. To všetko často impregnované alkoholom. "Ja nerozprávam, čo eskimáci. To Slováci." Všetky tie neuveriteľné veci, čo si ľudia dokážu navzájom porobiť.  Sfetovaný stopár, pitva vodiča: "Zasadil mu ranu do krku. Potom ďalších  stodeväťdesiatpäť."   "Za svoj život uvidíte toľko mŕtvol, že vám z toho bude zle." Už teraz dosť na to, že sa mi síce po prednáške nesnívalo o mŕtvolách, ale prisahám, že zlé sny som mala. A pár dní som z toho bola riadne vytretá (výraz pre mňa pomerne dobre vystihuje špecifický smutný stav sprevádzajúci stretnutie s niektorými prípadmi).   Pre toto všetko patrí súdnym lekárom môj veľký obdiv. Skladajú z kúskov tiel príbehy, sú tí poslední, ktorí nechajú mŕtveho prehovoriť. A nesú si so sebou celým svojim životom tie príbehy a mená, niekedy aj mŕtvolný pach, ktorý sa nie vždy dá hneď osprchovať.   Dnes v zjemnenom a cenzúrovanom svete plnom vône najnovšieho  dezodorantu, módnych šatočiek a večnej mladosti je toto iná lekcia do  života.   Memento mori. Pamätaj na smrť.   Carpe diem. Uži deň.         (výroky v úvodzovkách sú pomerne presné citácie zachytené a zapísané na prednáškach a stážach)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Cisársky rez
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Anestézia na poslednej ceste
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Dotykoví
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Neprebudení
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Vymysli si niečo pekné na snívanie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alena Škutchanová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alena Škutchanová
            
         
        skutchanova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Lekárka - anestéziologička skautkapoviedkárkaa ešte všeličo možné
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    39
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2294
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Anestéziologické
                        
                     
                                     
                        
                            Intenzívne
                        
                     
                                     
                        
                            Medické
                        
                     
                                     
                        
                            Skautské
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Bydžovský - Akutní stavy v kontextu
                                     
                                                                             
                                            Forrest Carter - Škola Malého stromu
                                     
                                                                             
                                            Reif Larsen - Mapa mojich snov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Miro Tropko
                                     
                                                                             
                                            Lenka Rubensteinová
                                     
                                                                             
                                            Marek Gajdoš
                                     
                                                                             
                                            Ľubica Kočanová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje obrázky
                                     
                                                                             
                                            Moje osobné stránky
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ľudia, ktorí chodia po štyroch
                     
                                                         
                       Pochod za právo na rozhodnutie
                     
                                                         
                       Úsvit a kultúrne zákulisia vedy o sexe. Prípad masturbácie.
                     
                                                         
                       Polovačka na fotky
                     
                                                         
                       Dobrý lekár vždy zachraňuje...
                     
                                                         
                       Čo radí autobusár, kým radí
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       V takýchto chvíľkach, má človek chuť, stále začínať odznova
                     
                                                         
                       Uväznená mitochondria a zrod komplexného života
                     
                                                         
                       Vrkoč
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




