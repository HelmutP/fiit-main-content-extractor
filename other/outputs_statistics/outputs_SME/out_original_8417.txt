
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Špurek
                                        &gt;
                galéria u Maxa
                     
                 Pláň je tichá, sneh 

        
            
                                    9.6.2010
            o
            11:13
                        (upravené
                9.6.2010
                o
                11:21)
                        |
            Karma článku:
                2.18
            |
            Prečítané 
            236-krát
                    
         
     
         
             

                 
                    Nehmotne pribúda priestor. Je to niekde vzadu. Prechádzajú iné  zvieratá. Počasie je dobré. Breeze. Povieva jemný vánok. Počuť len  kĺzanie. Napredujeme. To je pozitívne. Ohliadam sa. Všetko je na svojom  mieste, tak dopredu. Popruhy všetko mocne zvierajú.
                 

                 Jedna hora, jeden ľad, jedno nepoznanie. Postupné opadanie hmly, kým  sa celkom rozjasní. Kým sa ma dotkne ruka, čo tu už bola. Sneh sa krúti  vo vzduchu a padá  v záveji, zaviaty a sladko unavený. A ja ním nie som.       Ľadová pláň je tichá.... jemný piesok, zrnká snehu bláznivo letia nad  rovným povrchom zeme.       Mohutná stena, zrezaná zo strany vetra, sa pokojne vypína predo mnou.  A bude tu, ak sa mi podarí vrátiť a vrátiť touto istou cestou. Je  bláznivé ísť tam, kam idem a chcem. Ale nejde možno o cieľ, ísť treba,  lebo žijeme. Už nerozmýšľam, vybral som sa, ostáva len belavá tajná  radosť prítomna.       Psi húpavo bežia. Sú pokojné. Sú silné, ženú sa vpred. Je to radostná  melódia prirodzenosti. Únik je forma víťazstva. Sentencia je matkou  pravdy. A zrnko snehu mi prevŕtava nos a nedarí sa mu, až kým neumrie  roztopením a necinkne na moje pery. Ženieme sa dopredu krajinou. Je snáď  menej krásna tým, že nie je členitá? Že nepreniká do údolí a vrchov?  Vidím v nej niečo celkom iné. Sneh radostne sviští. Asi takto je  v poriadku vesmír.       To, čo je za nami, už navždy zaostáva za dneškom. Roky sa sypú, sú  ako lavína. Ako kopa snehu, ktorý je malátny, neuchopiteľný, neforemný  po prudkom páde. Nikdy nevieme v tejto hre saní, kde vyvrcholí ten beh.  Či cieľ nebol len snovým bodom pôvodnej reality. Načisto platónsky  vzaté, víťazná idea, preexistencia, ktorá sa vyššie realizuje posibilným  bytím ako transpozíciou. Veci sú veľké. Ja si na nič netrúfam. Len  prosto obdivujem. Zadúša ma, že nedokážem vyjadriť množstvo krásy. Krásy  ma umýva od špiny, ktorej je tu tak hodne.              V ideálnom vesmíre Platóna a Boha tiež letím po vyvolenej ceste,  drobná chvíľa zhody objektu a ideálu v dynamickom jambe.       Sane letia, to stačí. Je to labutia plavba lamanšským prieplavom  snehu počas boja  v druhej svetovej. Unavený vojak, maličká súčiastka  seba samého, už navždy odtrhnutá od kolosu, unavene načúva mohutnému  hukotu vĺn. Jeho noha je zdrtená. Letí priestorom. Je plnosťou seba  samého. Práve s tou zničenou nohou. A Duchom, ktorý ho robí ním samým.       Sane sú pevné. Je úžasné môcť sa na ne spoľahnúť. Ale aj na psy. Sú  skúsené. Dôveryhodné. Je to napredujúci tým so svojím vodcom. Milujú ma.  A keď sa približuje biely medveď polárnej noci, dychtivo brešú. Ani  neviem, či viac túžia, aby prišiel, či aby ho odohnali. Ja ich  neodsudzujem, ale neprivolávam ich cestu. Pravda musí byť stotožnením  času. Plnosťou nepokrivenej radostnej sprchy úniku z pút hnusnej  civilizácie.       Je to žiadaný radostný návrat v snehovej pláni do pocitu horúceho  raja. Biblia má pravdu. Sama je skutočným patriarchom zamrznutým  v trópoch, uprostred zimy, nie nepodobných zápasu Homéra. Nie menej  pochybovačná ako Aischyles. Je horúca v tejto snehovej pláni. Ako vie  a nehovorí. Dáva tušiť. Nesiem ju, spočíva v saniach. A on je tu  naokolo. Zavše z diaľky počuť hrubý hlas ľadového obra, ako si spokojne  mrmle polárnu pieseň života.       Výprava je tajná. Výprava sa podarí.       Poviem svojej výprave, zabudol som na všetko.       Mocný nad ľadovým obrom mi je cesta, tvorca ľadového medveďa. Biblia  je môj horúci čaj v termoske a káva. Je to ústredná žila mojej  existencie uprostred bielej. A takto je všetko na svojom mieste pre  tento systém.       Bežia dôstojným taktom snežných polí ako biele kone v zelených  horkých stráňach. Dopĺňajú pravdu priestoru sebou. Ešte chvíľa,  rozrežeme vzduch a za ním uvidíme obzor letiacej pravdy zhora dolu. Ako  sa rozprestiera nad mojou zemou. Nie pre globálny záujem filozofie. Len  pre mňa. A preto si ju ani nevezmem. Len sa jej dotknem.                      + 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Super vec! Poďte to skúsiť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Svetlo Ježiša Krista premôže tmu! Určite!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Moje telo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Dojatie je mŕtve
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Cyril a Metod, Róbert Bezák
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Špurek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Špurek
            
         
        spurek.blog.sme.sk (rss)
         
                                     
     
        Kresťan. Človek fascinovaný Ježišom Kristom. Tvorca a konzument umenia. Doktorand...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    384
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            literatúra
                        
                     
                                     
                        
                            kresťanské umenie
                        
                     
                                     
                        
                            galéria u Maxa
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľudmila Onuferová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            des Jano
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




