

  Ak sa však tu zastavíme, podľa čoho spoznáme, že prebehol nejaký čas? Čo je pre nás referenčný pevný bod? Čas môže byť vlastne iba zdanie, lebo veď „situáciu 2“ porovnáme s predchádzajúcou iba na základe zmeny nej samotnej. Zdanlivo teda neexistuje bod/vec, ktorá by sa v našom chápaní času nemenila. 
   
 Táto úvaha smeruje k tomu, že čas určujeme ako zmenu stavu 1 na stav 2 a podľa našej pamäti, lebo si „pamätáme“, že „niečo“ bolo „predtým“ iné. Tu však ani slovo „predtým“ už nemôžeme použiť, veď bez času ako ho vnímame neexistuje „pred a potom“. 
 Všetko je akoby teraz. A toto „teraz“ je vlastne všade a teda trvá aj večne. Svet a jeho história môže byť v tomto zmysle len „spočítateľným“ súborom okamžikov, ktoré predstavujú teraz, ktoré trvá večne. 
 Zrazu však v tomto jednoduchom vnímaní všetkého sa akoby stráca význam žitia, prežívania chvíľ, veď potom nič čo robíme by nemohlo ovplyvniť budúcnosť a ako to je vlastne potom s Bohom a Jeho plánom spásy ľudí, s obetou Krista, s našou smrťou a zmŕtvychvstaním? 
 Možnože to, čo som zatiaľ spomínal je iba iný, vonkajší pohľad akoby z 21.-vej dimenzie a pokiaľ som ohraničený v 3D a žijem, tak ho ani nebudem schopný rozpoznať. Myslím si, že keď zomriem a ?dostanem sa k Bohu, tak pochopím to všetko a pravú pravdu. 
 Verím, že Boh je všemocný a nekonečný a práve v ňom sa nachádza poznanie a vlastne aj samotná existencia spomínaného „teraz.“ Veď v Ňom neexistuje viac včera, zajtra, čas vôbec, v Bohu je len „teraz“, ktoré je večné, nekonečné, nad rámec všetkého poznaného. 
 Ďalej potom už zrejme nebude mať zmysel ani poznávanie vecí a sveta okolo, lebo budeme žiť v samotnej jednote s Bohom, ktorá nás dokonale a večne naplní. Aby naša radosť bola úplná. 
 Aký má toto všetko súvis s mojím chápaním teraz a nekonečna? V zásade mi tento pohľad umožní viacej si uvedomovať, že všetko, čo žijem, robím, vidím a cítim má svoj nekonečný význam v Bohu, je to v Ňom obsiahnuté (Čo nie je od Boha?!). 
 A v tejto chvíli, keď preciťujem „teraz“ ako niečo, čo ostane naveky mi zrazu zmiznú akékoľvek ľudské pohľady a ostáva mi ponuka žiť v jednote s Bohom už teraz, dnes, v tomto momente. Ostane potom jediný pevný bod celého vesmíru, nášho času, okamihov „teraz“ a tým je Boh. 
 Človek potom už žije v Nebeskom kráľovstve, v Nebi. Ale vráťme sa opäť do fyzickej roviny a radšej konajme hneď skutky dobra, ako sa iba zamyslíme a dúfame v toto teraz-nekonečné equilibrium... 
   

