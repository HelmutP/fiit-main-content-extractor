

   
 Šestica hudobníkov hrá akustický jazz s punkovým nasadením. Pripravte sa na superrýchle sóla, skvelú zábavu a v jazze nevídanú interakciu s publikom, o ktorú sa stará agitátor Sacho s megafónom. 
 Dnes to bude skvelý žúr, ktorý poteší nielen fanúšikov jazzu ale aj tanečnej hudby. Po koncerte, ktorý sa začne okolo 9:30, bude zábava pokračovať špeciálnym setom britského DJa Paula Murphyho, jedného zo zakladateľov britskej acid-jazzovej scény. 
 Lístky na dnešný koncert sa dajú kúpiť za výhodnú predpredajovú cenu v sitei Ticketportál, alebo u Dr. Horáka či v Artfore. Ďalší Sushi Jazz bude už o dva týždne. V Nu Spirit Clube privítame Ryotu Nozakiho a jeho Jazztronik v plnej živej zostave. 
  

