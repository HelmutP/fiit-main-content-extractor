
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nora-Soul Slížová
                                        &gt;
                Moja Rodina
                     
                 Dnes máš narodeniny 

        
            
                                    9.2.2010
            o
            11:19
                        |
            Karma článku:
                7.42
            |
            Prečítané 
            1613-krát
                    
         
     
         
             

                 
                    Keby som to vtedy tušila, že je to naposledy, čo sme spolu, tak...
                 

                 Tak by som sa snažila vryť si všetko hlboko do pamäte. Zvečniť si tú chvíľu a stovky jej podobné. Zapamätať si každú vrásku na tvojej tvári, tvar každej mozole na tvojich rukách. Zadívať sa hlboko do tvojich oči, a navždy si zapamätať ich odtieň a lesk.    Čas nemilosrdne trhá listy z knihy mojich spomienok a ja mám obavy, že príde deň, keď už v nej neostane ani jedna strana, len nadpis. Ktorý, ostane posledným svedkom kapitoly, ktorá je pre mňa najcennejšie. Keby sa tak dala zapečatiť táto kniha spomienok, aby čas neporušil jej obsah, aby v nej nevybledla ani vôňa pitralonu, ani nezriedili sa počty spomienok na spoločné chvíle.    Viem, že sa to nedá. Panta rhei. Čas plynie a veci sa menia. Jedna vec sa však nemení, dnes mi chýbaš práve tak veľmi, ako pred dvadsiatimi rokmi.    Papa, všetko najlepšie k tvojim narodeninám. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Keď stojíme nad hrobom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Milujem oboch - a to rovnako
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Transformácia pesimistu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Dining Guide - Kde sa najesť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nora-Soul Slížová 
                                        
                                            Byť lepším človekom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nora-Soul Slížová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nora-Soul Slížová
            
         
        slizova.blog.sme.sk (rss)
         
                                     
     
         Človek, ktorý si chce nájsť čas pre uvedomenie si vlastných myšlienok, postojov, názorov. Pochopiť určitú vec, teóriu, či událosť, mi prináša osobný rast a vytúžený pocit štastia.   
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    409
                
                
                    Celková karma
                    
                                                4.81
                    
                
                
                    Priemerná čítanosť
                    1464
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moja Rodina
                        
                     
                                     
                        
                            Dining Guide
                        
                     
                                     
                        
                            Prúd myšlienok
                        
                     
                                     
                        
                            To čo ma potešilo
                        
                     
                                     
                        
                            To čo ma zarmútilo
                        
                     
                                     
                        
                            Vedeli ste, že...?
                        
                     
                                     
                        
                            o živote
                        
                     
                                     
                        
                            z vlaku
                        
                     
                                     
                        
                            Zvieracie
                        
                     
                                     
                        
                            readers diary
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Smutné ale pravdivé
                                     
                                                                             
                                            Suicide Read This First
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            C.G.Jung
                                     
                                                                             
                                            Fyzika I
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jesus Adrian Romero
                                     
                                                                             
                                            Fly To The Sky
                                     
                                                                             
                                            Rady svojich priatelov
                                     
                                                                             
                                            Hlas svojho srdca
                                     
                                                                             
                                            Svoje myšlienky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj prvý...(blog ktorý som čítala)
                                     
                                                                             
                                            Michal Patarák-Filozof v bielom plašti
                                     
                                                                             
                                            Kamilka- Úprimná duša
                                     
                                                                             
                                            Hirax
                                     
                                                                             
                                            DiDi- proste skvela
                                     
                                                                             
                                            Veronika Bahnová - Favorite Teacher
                                     
                                                                             
                                            Rolo Cagáň
                                     
                                                                             
                                            Jozef Klucho- Doktor bacsi
                                     
                                                                             
                                            Juraj Drobny-ODF- Velky drobec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Holos
                                     
                                                                             
                                            Forum - psychológia
                                     
                                                                             
                                            I Psychologia
                                     
                                                                             
                                            Macher z KE :)
                                     
                                                                             
                                            Bennett Pologe, Ph.D.
                                     
                                                                             
                                            Kokopelli forum
                                     
                                                                             
                                            Umenie na iný spôsob- Akupunktura
                                     
                                                                             
                                            Ambulancia klinického psychológa
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Blog na SME bol výnimočný, už nie je
                     
                                                         
                       Nenapísaná poviedka
                     
                                                         
                       Čestný občan Róbert Bezák
                     
                                                         
                       Ako som Jožka odviezla na psychiatriu (smutný príbeh s úsmevom )
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Obyčajná láska
                     
                                                         
                       Rande na slepo
                     
                                                         
                       Kvapka krvi
                     
                                                         
                       Bolesť.
                     
                                                         
                       Taká obyčajná autonehoda
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




