
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lýdia Koňárová
                                        &gt;
                Pro veritate cum caritate
                     
                 Ako Pavol Jantausch vzdelaním národné sebavedomie podporoval 

        
            
                                    22.6.2010
            o
            8:22
                        (upravené
                25.9.2010
                o
                7:55)
                        |
            Karma článku:
                5.03
            |
            Prečítané 
            960-krát
                    
         
     
         
             

                 
                    Dôstojnosť človeka    vyžaduje, aby sa neplahočil ustavične v bahne každodenných trampôt a    nekvákal so žabami výlučne o tom, čo budem jesť, čo budem piť, kde bývať    a čím sa odievať ? Nikto sa nepreceňuj a  nepokladaj za jediné   koliesko v hodinách. Musíš sa stále starať,  aby si bol zo dňa na   deň aspoň o mak lepším a dokonalejším.
                 

                 
 
       Pavol Jantausch sa narodí vo Vrbovom 27. júna 1870 ako tretie z piatich detí povrazníka Jozefa a Kataríny, rodenej Sudorovej.        "Sotva som mal päť rokov, už mi mamička dala "úrad". Varovať a opatrovať kvočku s kuriatkami. Upútala ma usilovnosť, starostlivosť a spravodlivosť kvočkina. Hrabala - kutala bez únavy a i najmenšieho chrobáčika deťom rozdelila, aby sa ušlo všetkým. Na šum ponad nami letiaceho vtáka zakrákorila, rozprestrela ochranné krídla a kuriatka boli hneď pod ňou. Skúšal som i ja krákoriť, ale márne.    Pes a náš kocúr, čo sa vždy so mnou vláčili, neboli v takej milosti u kvočky ako ja. Dunčo, keď videl, že mám krajec chleba v hrsti a rozdávam odrobinky, bezočivo sa miešal do hostiny. Maco zas namieril na najväčšie čierne kurča. No všetko som dobre doopatroval a za odmenu dostal nový kabát."           Študuje na gymnáziu v Trnave, Bratislave a Ostrihome, maturuje v Trnave 15. júna 1889.        "Odkedy sa náš mladý   pán organista oženil, stal sa neobyčajne láskavým k nám deťom v škole.   Často vybiehal zo školy, ako by si bol čosi zabudol doma. Bol taký   demokratický, že vždy sa nás opýtal, koho chceme za pozorníka. Fígľa sme   mali dosť, aby sme hlasovali na takých, čo najmenej budú žalovať.   Darilo sa nám. Palica odpočívala v kúte. Rohy nám rástli. Asi týždeň sme   obišli na sucho.    Vtedy  sme obdarili  dôverou Miša krstných, lebo sa nám zaveril, že nenapíše  nikoho na  tabuľu. Ledva pán organista vystrčil nohu, Mišo vyskočil na  stôl, že -  vraj - on je "generálom" a nás bude komandovať na vojnu. Viac  nám  nebolo treba. Strhnul sa taký treskot - pleskot, až sa triasli  steny. V  roztopašnosti sme otvorili okno a Joža Tenkých, ktorý čupel v  lavici,  lebo ho vraj brucho bolí, sme chceli vyhodiť. Vrešťal ani čo by  ho  drali.    Na zúfalé  volanie hneď boli pod oknami pani  organistka a pán organista, ako by ich  bol vystrelil. Pán organista v  ruke s pleskáčom, ktorým v kuchyni muchy  zabíjal, zjavil sa pri Mišovi,  ktorý chcúc skočiť zo stola, nohou  preboril kalamár na protokol a  padol pod stôl : "No, to je už škandál !"  O chvíľu sme už mali všetci  muchy povyháňané a sedeli sme ako hríby  pod trávou."    Doktorát teológie získa na Pázmaneu vo Viedni.    Za kňaza je vysvätený 21. septembra 1893, ako kaplán pôsobí v Topoľčanoch, Smoleniciach, Bojnej a  Bratislave - Blumentáli. Farárom v Dubovej pri Modre sa stáva 15. februára 1899, do Ludaníc ho preložia 15. marca 1906.   Otec mu radí: "Synku, preto býva vo farskom živote všetko  zhumpľované, lebo to mnohí majú pofušované i sami so sebou, i s Pánom  Bohom, i medzi sebou.    Tvojou úlohou bude, aby si ich privádzal k povedomiu, že musia  akosi ináč pliesť tú niť života. Nie je to maličkosťou, ak je v kúdeľ  pazdernatá a pomiešaná so slamou. Budeš mať čo vytriasať, hladiť a  čistiť."        Šíri čítanie kníh, učí pestovať ovocné stromy, vinice a racionálne obrábať pôdu.          "Ako dubovský farár rád som chodieval do veľkého "Pustáka", je to veľký kamenistý prieloh. Obdivoval som vyčnievajúce bralá a popri nich utešené kríčky, skalné klinčeky a od jari do zimy premieňavo kvitnúce kvietky. Neraz som navykutával spomedzi kamenia a vo vačkoch domov doniesol výbornú prsť do kvetníkov.    Raz mi v hlave skrsla myšlienka spraviť tam vinicu. Prekopávali sme, rúcali, roztĺkali, kyprili od novembra do apríla. V máji sa sadilo. Ľudia v lete híkali, keď videli, ako to rastie."    V Smoleniciach máva na "Duboch" besedy a cvičenia v slovenskom speve.   Píše do Kollárových Katolíckych novín, spolupracuje s evanjelickými farármi Zochom a Jamnickým. Zvolá s nimi v roku 1899 do Modry veľké zhromaždenie za národnostné práva. Sú stíhaní a potrestaní.    "Nebohý môj strýčko, keď mal ísť do Ameriky, navštívil ma na dubovskej fare; prišiel sa odobrať. Ako zarezaný sedel za stolom; slzy mu tiekli. Prehováral som ho, že mu bude lepšie, veď doma neokúsil iba otroctvo, biedu a sklamanie. "Ale predsa", vzdychol si bolestne, "bol som vo svojej vlasti !"                Svätej stolici trvá štyri roky, kým si uvedomí rozpad Rakúsko - Uhorska a vyjme územie Ostrihomskej arcidiecézy na Slovensku, tvoriacej 80% jej územia, spod právomoci ostrihomského arcibiskupa.   Pomaďarčenosť cirkvi na Slovensku je pri  vzniku ČSR totálna. Vysoké cirkevné úrady (biskupi, kanonici, dekani,  pápežskí kapláni a seminárni predstavení) vykonávajú Maďari alebo pomaďarčení  Slováci.        Ako inak. Uhorský minister náboženstva a školstva, gróf Apponyi v roku 1906 vyhlásil: "Učiteľom, ktorí nechcú z mládeže vychovať dobrých Maďarov, znemožním vyučovať. Pre každého občana štátu platí princíp, že v tomto štáte je pánom Maďar." V roku 1907 triumfuje: "Slovenský národ neexistuje."    Novú Trnavskú administratúru Svätá stolica zriadi a priamo si podriadi 29. mája 1922. Prvým apoštolským administrátorom pápež Pius XI. menuje ThDr. Pavla Jantauscha.           Za prvého slovenského biskupa je vysvätený 14. júna 1925.    Svätá stolica mu udelí titul biskup prienenský, podľa anticko - byzantského mesta Priéné. Okrem neho patrí 7 biskupom. Eligio Pietro Cosimu z čínskeho Šan-tungu (1865), Anthony Gaughrenovi z juhoafrického Orange River (1886), Franz Löbmannovi z nemeckého Meißenu (1915), Justo Rivas Fernándezovi zo španielskeho Santiaga de Compostela (1922), Antonio de Castro Mayerovi z brazílskeho Camposu (1948), Manuel Dos Santos Rochovi z portugalského Lisabonu (1949) a Jacques Le Cordierovi z Paríža (1956).           Vyberie si heslo Pro veritate cum caritate, S láskou pre dobro druhého k Pravde.   Privedie na Slovensko Saleziánov a v Šaštíne im odovzdá do správy faru s kostolom a časť kláštora 8. septembra 1924.    V roku 1925 prosí Svätú stolicu, spolu s ďalšími slovenskými biskupmi, aby splnila dlhoročné prianie slovenského národa a vyhlásila Sedembolestnú Pannu Máriu za patrónku Slovenska. Dekrétom zo dňa 22. apríla 1927 sa im želanie splní.        V roku 1924 pozve na Slovensko prvé sestry Premonštrátky, zakladajúce materské škôlky, detské domovy, domovy mládeže a domovy dôchodcov. V roku 1932 im posvätí vo Vrbovom prvý kláštor a v roku 1939 pri ňom založí ľudovú školu. Druhý kláštor pribudne v Dvorníkoch pri Hlohovci.                  Založí podporný Spolok Kolégia sv. Svorada na zakladanie, podporu a udržiavanie internátov. Predseduje mu desať rokov (1922 – 1932). Zakladajúci a doživotní členovia venujú 2 000 alebo 500 Kčs, riadni platia ročne po 20 Kčs.        "Ako štyria malí študentíci sme boli na strave v Trnave. Naša domáca mala s nami nemalú oštaru. Preberační a maškrtní sme boli ani mačky. Vyslovili sme jej jednohlasné ultimátum: Namiesto chleba dostávať denne po dvoch krajciaroch. Za jeden krajciar dával pekár dve žemličky, ženy na trhu holbu pukancov. Jablká, zelené figy, zemiakový cukor, pelendrek, sladké drevo, svätojánsky chlieb a čokoláda boli skoro zadarmo.    Za osem krajciarov som doniesol plný klobúk všeličoho. Usmievali sme sa víťazne a hostili ako na Štedrý večer. Na štvrtý deň sa dostavila kríza. Gustiho bolel žalúdok, Imrichovi sa točila hlava, Julko celý deň vzdychal a ja som si v škole za šuškanie vyjednal od Ferka kôrku z krajočka. Chlapci si robili z nás posmech, že sme slabí ako hlísty a nechceme ísť za pasy. Hanba sem - hanba tam, na piaty deň sme prosili domácu o krajec chleba."                              Prvé krídlo prvého moderného internátu na území Bratislavy, Svoradova, posvätí 1. mája 1928 za účasti ministra školstva Milana Hodžu. Polícia si nezabudne poznačiť do tajných fasciklov, že tie pochybné živly namiesto štátnej hymny spievajú Tomášikovu Hej, Slováci, ešte naša slovenská reč žije a mávajú slovenskými a pápežskými zástavami.        "Vlasť nám nesmie byť len veľkým kožuchom, ktorým by sme zakrývali svoju nahotu, azda i mravnú, a v ktorom by sme sa zohriali; ani nie len spoločnou misou, v ktorej sa nakŕmime.   Láska k vlasti je úprimná snaha občana, aby podľa svojich síl napomáhal blahobytu a keď treba, aby prinášal aj obety."           Vláda, diktujúca ideológiu jednotného československého národa, nemá záujem, aby  vznikla slovenská cirkevná provincia. Naťahuje čas. Zmluvu Modus   vivendi so Svätou stolicou podpíše až v roku 1928. Delimitačný plán provincie štátna a cirkevná komisia odošlú do Vatikánu po piatich rokoch.                V decembri 1934 posiela biskup Jantausch do Vatikánu žiadosť Posvätnej kongregácii pre semináre a univerzity o zriadenie Rímskokatolíckej bohosloveckej fakulty s tým, že československá vláda je ochotná otvoriť ju v rámci Univerzity Komenského v Bratislave. Na zimný semester v prvom študijnom roku sa 4. novembra 1936 zapíše 217 poslucháčov.            V roku 1938 otvorí Rímskokatolícke biskupské reálne československé gymnázium v Trnave.           Publikuje v časopisoch Duchovný pastier, Serafínsky svet, Tatranský orol, Trnavská rodina, Kultúra a Posol Božského Srdca. Ten posledný finančne podporuje viac ako 15 rokov.       Obnovuje vydávanie Katolíckych novín, je zakladajúcim členom Spolku sv. Vojtecha, posiela peniaze na misie.     Pápež Pius XI.  odčlení  Trnavskú administratúru od Ostrihomskej arcidiecézy a nové  diecézy Košickú a Rožňavskú od Jágerskej cirkevnej provincie   apoštolskou konštitúciou Ad ecclesiastici regiminis incrementum 2. septembra 1937.           Neskoro. Na 40 rokov  zostane  bezcenným zdrapom papiera.        Prvou Viedenskou  arbitrážou 2. novembra 1938 fašistické Nemecko a Taliansko dokopú Slovensko vzdať územia  na juhu a Podkarpatskej Rusi v prospech kamoša  Horthyho.       Po skončení vojny a  prevzatí kormidla komunizmus vyhlási náboženstvo, Marxove ópium ľudstva, spolu s Leninovým zahnívajúcim kapitalizmom, za najúhlavnejších triednych nepriateľov.           Keď   biskupovi Jantauschovi zomrie mladšia sestra Anna, stará sa o jej šesť detí. Dá ich  študovať alebo vyučiť  remeslu. Mariška a Štancka (Konštancia) sa stanú učiteľkami, Vladko knihárom, Paľko záhradníkom a Ferko mlynárom, no láka ho teológia. Vyštuduje ju v Ríme. Pred socialisticko - koncentračnou prevýchovou v uránových baniach utečie do Francúzska  a vzdeláva chlapcov v saleziánskom  lýceu Dona Bosca.        Okrem Paľka posiela do Prahy aj Jožka, na chemickú učňovku. Ten, aby strýkovi ušetril výdavky, sa dva roky stravuje vo vegetariánskej jedálni. Keď  komunisti vyhlásia kňazom vyhladzovaciu vojnu, kupuje im cestovné lístky a pomáha im cez Moravský Svätý Ján utiecť do Rakúska. Chytí ho ŠtB, štát mu skonfiškuje majetok, ženu s malými deťmi vyvezie na dedinu do nájmu k cudzím a jeho bachne na desať rokov do likvidačnej basy v Leopoldove.              Keď biskup Jantausch vážne ochorie, opatrujú ho sestry Premonštrátky. Zomiera 77-ročný v deň svojich menín a krstu, 29. júna 1947.            Želá si byť pochovaný skromne, vo svojom rodisku.       "Dôstojnosť človeka   vyžaduje, aby sa neplahočil ustavične v bahne každodenných trampôt a   nekvákal so žabami výlučne o tom, čo budem jesť, čo budem piť, kde bývať   a čím sa odievať ?   Nikto sa nepreceňuj a  nepokladaj za jediné  koliesko v hodinách.    Musíš sa stále starať,  aby si bol zo dňa na  deň aspoň o mak lepším a dokonalejším."   Slovenskú cirkevnú  provinciu zriadi, takmer 60 rokov po vzniku ČSR, pápež Pavol VI.  apoštolskými konštitúciami Praescriptionum  sacrosancti a Qui divino 30. decembra 1977.      zdroj citátov: kázne Pavla Jantauscha 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Tajná láska poetky Wallady, poslednej omejadskej princeznej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Zo Smrtnej hory fúka na Trygve Gulbranssena
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Pizza Clandestina s Vitom Corleone
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            De Syv Sostre, Sedem sestier
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Diego Velázquez - Las Hilanderas
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lýdia Koňárová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lýdia Koňárová
            
         
        konarova.blog.sme.sk (rss)
         
                                     
     
        ochranca prírody a pamiatok
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    54
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3598
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Chansons
                        
                     
                                     
                        
                            Bretónsko
                        
                     
                                     
                        
                            Capri
                        
                     
                                     
                        
                            Korzika
                        
                     
                                     
                        
                            Normandia
                        
                     
                                     
                        
                            Nórsko
                        
                     
                                     
                        
                            Paris
                        
                     
                                     
                        
                            Prado
                        
                     
                                     
                        
                            Pro veritate cum caritate
                        
                     
                                     
                        
                            Sardínia
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Škótsko
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Thassos
                        
                     
                                     
                        
                            Wien
                        
                     
                                     
                        
                            Zámky na Loire
                        
                     
                                     
                        
                            P&amp;#234;le-m&amp;#234;le
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Granada, Boabdil a kráľovná Aisha
                                     
                                                                             
                                            Meč z Toleda, tajný sen každého kráľa
                                     
                                                                             
                                            Diego Velázquez - Los Borrachos
                                     
                                                                             
                                            Vodný výťah pre lode vo Falkirku
                                     
                                                                             
                                            Siracusa, jedno z najslávnejších miest starovekého Stredomoria
                                     
                                                                             
                                            Córdobsky emirát a kalifova mešita
                                     
                                                                             
                                            Zámok Cheverny a Mona Lisa v oranžérii
                                     
                                                                             
                                            Zámok Chambord a dvojité schodisko Leonarda da Vinciho
                                     
                                                                             
                                            Zámok Ussé a Kráska spiaca v lese
                                     
                                                                             
                                            Zámok Blois a prekliaty básnik François Villon I.
                                     
                                                                             
                                            Zámok Blois, kabinet s jedmi a kredenc II.
                                     
                                                                             
                                            Aliki a pristávacia dráha pre mimozemšťanov
                                     
                                                                             
                                            Florencia, Catherine de Medicis a zámok Louvre
                                     
                                                                             
                                            Bayeux, Odon a tapiséria kráľovnej Matildy
                                     
                                                                             
                                            Perníkové chalúpky v Kerascoete
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




