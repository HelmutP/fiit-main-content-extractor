
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lucia Kramaričová
                                        &gt;
                Súkromné
                     
                 Turecká a arabská káva 

        
            
                                    28.10.2008
            o
            10:10
                        |
            Karma článku:
                11.94
            |
            Prečítané 
            15334-krát
                    
         
     
         
             

                 
                     
  Turecká káva v našich končinách znamená zaliať mletú zrnkovú kávu vrelou vodou. Žiadna extra pochúťka. Toto našské zjednodušenie nijako neprispelo k chuti tureckej kávy. Na Balkáne pod pojmom turecká káva bežne servírujú kávu urobenú v džezve, špeciálnej kovovej nádobke s dlhou rúčkou, v ktorej sa priamo zomletá káva s vodou varí. Takto pripravená káva chutí úplne inak, než iba zaliata.  
                

                 
  
    Naša rumunská lektorka bola prekvapená, keď videla spolužiakov, ako si zaliali kávu, čo to robia a dôrazne protestovala proti nazývaniu takej kávy tureckou. Súhlasím, zalievaná káva ma neoslnila a dlho som kávu vôbec nepila. Naozajstná turecká káva - v Grécku samozrejme nazývaná grécka káva - je pochúťkou pre labužníkov. Je silnejšia, hustejšia a delikátnejšia.  Turecká káva sa dá pripraviť doma aj bez klasickej džezvy, v odchodoch s domácimi potrebami dostať aj moderné džezvy z nerezu s plastovými rúčkami, alebo postačí aj obyčajný malý hrnček. Klasická džezva (nazývaná aj ibrik) má však výhodu v špeciálnom tvare, ktorý zadrží zomletú kávu v nádobe a do šáločky prenikne minimum usadeniny.       moderné a tradičné džezvy      Príprava tureckej kávy  Turecká káva by sa dala prirovnať k zákusku, pije sa z drobných šáločiek - nie z čajových dvojdecových šálok. Klasické džezvy sú vyrábané vo viacerých veľkostiach podľa počtu porcií, ktoré sa chystáme podávať. Počet porcií poznáme podľa čísla vyrazeného na dne typickej džezvy.  Primerané množstvo kávy (prax robí majstra) treba na jemno zomlieť - ideálne tesne pred prípravou kávy. Do džezvy naberieme vodu, pridáme pomletú kávu a prípadne aj cukor. Turecká káva chutí lepšie sladšia, aj ľuďom, ktorí sú zvyknutí piť zalievanú kávu nesladenú.          Kávu nenecháme vrieť, ale keď sa spení, stiahneme ju z horáka, počkáme, kým pena klesne - penu možno aj odobrať a rozdeliť do šáločiek. Spenenie opakujeme trikrát, chvíľu počkáme, kým sa mleté zrnká usadia a kávu z džezvy pomaly nalievame do šáločiek.       
              Nevyhnutným doplnkom každej kávy je pohár vody.  Do vody pred varením spolu s kávou a cukrom môžme pridať drvený kardamóm alebo mletú škoricu.       kardamómové zrno          podávanie tureckej kávy      Arabská káva  Príprava a podávanie arabskej kávy je obrad. Slušnosťou je ponúkať hosťovi nepárny počet šálok - samozrejme, pravou rukou. Arabská káva by sa nemala sladiť cukrom a dolievať mliekom. Podáva sa tradične s datlami, ktoré obsahujú veľa cukru.  Podľa tradičného obradu sa používajú nepražené kávové zrná, ktoré sa tesne pred prípravou qahwy opražia na kovovej panvici s dlhou rúčkou nad ohňom, vychladnuté sa nasypú do mažiara a podrvia. Arabská káva sa nevarí v džezve, ale v nádobe pripomínajúcej čajník, ktorá sa volá dallah.       dallah, rôzne typy, v popredí staršia, ktorá má ešte džezvovitý tvar                 na hrubo podrvená káva v mažiari       Dallah sa naplní vodou a tesne pred zovretím do vody nasypeme opraženú podrvenú kávu a podrvený kardamóm. Oheň, plyn alebo elektrický horák stiahneme na minimum a varíme. Približne desať minút. Ako pri tureckej káve, necháme zrná usadiť sa a dallu chytíme do ľavej ruky a polmesiacovitým hrdlom nalievame kávu do šáločiek. Podávame hosťom pravou rukou a sprava doľava. Kávu by mala podávať najmladšia osoba. Nalieva sa asi do polovice šáločky.      
    
  
       Saudi túto kávu pijú až prekardamómovanú, pomer kávy a kardamómu upravíme podľa chuti. Na začiatok je asi najlepšie začať s minimom kardamómu, má výraznú chuť. Okrem kardamómu možno použiť škoricu alebo šafran.        usadenina na dne šálky - minimum, na rozdiel od zalievanej "tureckej" kávy   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (42)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            Dvor pri usadlosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            PF
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            V starom dome - retro kúpeľňa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            Jar a začiatok leta v starej záhrade
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lucia Kramaričová 
                                        
                                            Od zimy do leta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lucia Kramaričová
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lucia Kramaričová
            
         
        kramaricova.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.milosch.sk/LuckaMilos 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    165
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3234
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            Aliquid de antiquitate
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            De universitate
                        
                     
                                     
                        
                            Cultus atque humanitas
                        
                     
                                     
                        
                            Animalia
                        
                     
                                     
                        
                            Železník
                        
                     
                                     
                        
                            Indológia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Zatepľovanie - ničenie historických fasád
                                     
                                                                             
                                            Bezhlavé zatepľovanie
                                     
                                                                             
                                            Ručná výroba thonetiek
                                     
                                                                             
                                            Statok bez modernizácie
                                     
                                                                             
                                            Panelákové bývanie
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Titus Popovici - Cizinec
                                     
                                                                             
                                            Učebnice sanskrtu - zas a znova
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            andy
                                     
                                                                             
                                            vlastnou rukou, mokopa
                                     
                                                                             
                                            výtvarník
                                     
                                                                             
                                            certikoviny
                                     
                                                                             
                                            denník z korfu
                                     
                                                                             
                                            brankárska stránka
                                     
                                                                             
                                            indo-európsky blog
                                     
                                                                             
                                            pamiatky
                                     
                                                                             
                                            anca din românia
                                     
                                                                             
                                            čosi z orientu
                                     
                                                                             
                                            archeológ martin
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




