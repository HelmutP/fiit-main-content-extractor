

 Cez víkend, hoci bolo takmer pred dažďom, som  si na bicykli „odskočil“ hádam len  tristo metrov za mesto a verte, rozkvitnuté poľné kvety ma veľmi potešili. Urobil som si ďalšie fotografie do môjho herbára. Dnes  ponúkam foto a pár informácií o kostihoji lekárskom, pakosti dvojsečnom a pažitke pravej (zo záhrady). 
 
    Z kostihoja lekárskeho sa na jar alebo v jeseni vykopáva koreň, ktorý pozdĺžne rozrežeme a usušíme. Obsahuje veľa hojivého slizovitého hlienu, triesloviny, živicu, cholín a iné látky. Droga má protizápalové a odhlieňovacie účinky. V Anglicku sa vraj kostihoj používa ako liek pri žalúdočných vredoch. 
 
    Na ďalšej snímke je rastlina z čeľade pakostovité  (Geraniaceae). Podobá sa na pakost smradľavý, ale ten je trochu menší a kvitne až v júni.  S najväčšou pravdepodobnosťou ide o pakost dvojsečný. Nie som si istý či je používaný v ľudovom liečiteľstve tak, ako pakost smradľavý, ktorý sa používa na rany. 
 
     Pažitku som odfotil v záhrade. Jej listy si môžeme napríklad nakrájať na chlieb s maslom. Obsahujú aj vitamín C. 
  
  . 
  
 Kostihoj lekársky 
 
  
 
 Pakost dvojsečný   
 
 
  
 
 
 Pažitka pravá 
  
 Snímky: Stano Orolin 
 
 

