

   
 Vždy som vedel že zaobchádzanie so zvieratami v Číne nie je ružové ale toto by mi nenapadlo ani v zlom sne. Po zhliadnutí niekoľko nechutných, zvrátených ale bohužiaľ pravdivých videí  neviem čo si mám myslieť o tomto národe. 
 Ako môže nejaká ľudská bytosť zaživa sťahovať zviera kričiace o pomoc z kože pre predaj kožušín? Ako ho môže nehať zatvorené v malej klietke a nechať ho pozerať sa na toto hrozné divadlo a dať mu hrozný pocit toho že ho to onedlho čaká? Ako môže zviera stiahnuté z kože nechať pohodené živé v krabici a ponechať ho dlhej a bolestivej smrti? 
 Príšerne som sa cítil aj vtedy, keď som pozeral video natočené na usporiadaní súťaže, kto rýchlejšie zabije a pripraví zviera na konzumáciu, aj za cenu že je živé. Nešlo o nejakú krytú súťaž, ale o masové podujatie na štadióne!!! 
 Bolo nechutné pozerať ako sa tam tie šikmooké prasce pozerajú a chichotajú nad rybou ktorá bola zaživa usmažená a uždibujú si z nej kúsky paličkami a ona ešte pritom žije, hýbe žiabrami..pozerajú na to svojou nehybnou tvárou bez súcitu a bez akejkoľvek mimiky.  
  O psíkoch zavretých v extra malých klietkach kde s nimi zaobchádzajú ako s neživým tovarom a porcujú ich tam polo živých ani nemusím hovoriť. Ani o psích farmách kde sa psi chovajú za cielom konzumácie v podmienkach ktoré nepotrebujú koment.         
 Áno, nesmierne ľutujem že som to videl lebo ma bolí že s tým nemôžem nič spraviť ale za to viem že Miletičovu dlho nenavštívim. Viem že nie všetci sú takí ale v tomto článku sa ani nesnažím byť objektívny lebo píšem čo práve cítim. 
   
 To ale nemení to fakt že podľa mňa sú Číňania krutým národom ktorí ani nie sú hádam ľuďmi, ale strojmi ktoré nemajú city a prežijú všade a všetko. 
 Jednoducho nechápem ako to môže nejaký človek spraviť a aj to že sa kruté zaobchádzanie so zvieratami sa u nich neeliminuje a je bežné k videniu. Pre mňa to boli surové zábery a pripadalo mi to ako bolestná mašinéria smrti bezmocných bytostí obkolesená prihlúplo sa pozerajúcimi Číňanmi. 
 Ak je v ich kultúre tradičné jedenia psieho mäsa, prosím! Ale zaživa sťahovať z kože či konzumovať zviera nie je podľa mňa normálne. 
 Ak je pravda že Čína sa raz stane svetovou veľmocou, tak dúfam že vtedy ja už nebudem žiť. 
   
 Pre tých čo chcú vidieť tie surové zábery, tu je link. 
 Ale vopred upozornuje že nie sú vhodné precitlivých ľudí..vlastne pre nikoho z nás. 
 http://cafe.loveme.cz/video/cinska-farma-na-kozesiny-sokujici-video/ 
 http://cafe.loveme.cz/video/jidlo-ktere-zije-sokujici-video/ 

