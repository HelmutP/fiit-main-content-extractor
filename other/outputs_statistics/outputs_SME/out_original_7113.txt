
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marcel Páleš
                                        &gt;
                Cesta za zmyslom života
                     
                 Zviaž svoj egoizmus a vykopni ho na mesiac 

        
            
                                    23.5.2010
            o
            14:05
                        (upravené
                18.9.2010
                o
                1:40)
                        |
            Karma článku:
                7.94
            |
            Prečítané 
            914-krát
                    
         
     
         
             

                 
                    Nedávno mi jedna známa opisovala zážitky zo svadby, na ktorú vzala svojho kolegu z práce. Všetci ich dávali dokopy, tak si povedala, že tomu dá šancu. Ostala však zarazená, keďže tento chlapík orientoval minimálne polovicu svojho prejavu na seba. Priznám sa i ja, že posledné dva roky jedno z mojich predsavzatí znie „krotiť svoje ego". Občas mi totiž ulieta nesprávnym smerom. Z globálneho hľadiska sa egocentrizmus môže podpísať pod nejednu katastrofu. No a Chuck Norris, i keď je novodobým supermanom, nebude stíhať zachraňovať celý vesmír.
                 

                 
  
   Áno, áno, drahí priatelia, i keď sa to nezdá, k novodobým chorobám patrí určite aj egoizmus. Populárny francúzsky filozof a sociológ G.Lipovetsky vo svojej knihe Éra prázdnoty hovorí: „dnes žijeme sami pre seba, nestaráme sa o svoje tradície, ani o tých, ktorí prídu po nás: upustili sme od zmyslu pre históriu rovnako ako od spoločenských inštitúcii a hodnôt.“ Aj preto môžeme súčasnej dobe pripísať atribút „egoistická“. Ak máte čo i len minimálnu záľubu v pozorovaní ľudí, myslím, že aj Vy poznáte aspoň jedného z Vášho okolia, ktorého ego predbieha v jeho podaní všetko ostatné. Neraz takto – mysliac predovšetkým na seba – koná podvedome každý z nás. Občas to na škodu nie je, no občas by sa dalo myslieť viac i na ostatný svet.    Každopádne je egocentrizmus súčasťou každého človeka. Vyplýva to aj z výskumov ľudskej psychiky MUDr. V.Savčenka. Ten výstižne opisuje, že „úlohou Ega je zabezpečiť rast a rozvoj človeka. Ego schvaľuje všetko, čo vedie k rastu človeka a odmieta všetko ostatné. Avšak, keď napríklad človek veľa je, neznamená to, že jeho pažravosť schvaľuje jeho Ego. Naopak. Keby počul hlas svojho zdravého Ega, nikdy by sa nepremenil na sviňu.“ Takýchto príkladov je mnoho. Odrazom toho, kam vedie nezdravý vývoj egoizmu, sú svetové vojny resp. ekologické problémy.    I keď si možno myslíte, že ako bežní smrteľníci ste primalí na to, aby ste ovplyvnili beh sveta a jeho budúcnosti, opak je pravdou. Človek je predsa bytosť sociálna, žije v spoločnosti, individuálne sa teda podieľa na jej vývoji. Všetci ľudia dokopy tvoria ako dieliky mozaiky jeden celistvý obraz. Takže aj moje konanie môže pomôcť k lepšiemu zajtrajšku, nielen môjmu, ale i ostatných existencií. Ak by tak uvažovali všetci, určite by sa dni budúcnosti niesli v odraze častého úsmevu.   Pri hádkach, konfliktoch, nedorozumeniach, krízach či akýchkoľvek sporoch – ak by človek svoje ego sýtil skôr láskavosťou a prosociálnym cítením vôbec, asi by sme si dokázali vypestovať na Zemi aspoň čiastočný raj. Je to ale možné budovať od malých skupín – napríklad v rodine, v školskej triede, na pracovisku a postupne i vyššie. Obzvlášť dôkladne by sme mali pristupovať v procese výchovy detí – ak do nich zasejeme láskavosť, budúcnosť môže priniesť krásne ovocie nielen pre nás, ale i pre ostatný svet.   Pripomínam aj výrok pápeža Benedikt XVI.: „ak sa ľudia budú pozerať iba na svoje vlastné záujmy, náš svet sa určite zrúti.“    A tak si hovorím, že nebolo by na škodu, pokúsiť sa krotiť svoj egoizmus, zviazať ho ako poštový balík a vykopnúť kamsi na mesiac. Poprípade, ak tam nevykopnete, tak ten egoizmus vykopnúť von zo svojho života aspoň na ten kalendárny mesiac. Držím Vám v tom palce. Sám sa o to pokúšam.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Je trápne byť citlivým človekom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Veľkým svetom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Obyčajné objatie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Ra-ta-ta-ta-ta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Páleš 
                                        
                                            Máte v sebe nainštalovaný balík láskavosti?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marcel Páleš
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marcel Páleš
            
         
        marcelpales.blog.sme.sk (rss)
         
                                     
     
        Okrem mnohého iného ma zaujímajú diskusie o zmysle života, rád presahujem hranice priemernosti, milujem čítanie kníh a tiež čítanie ľudí. Keďže rád nachádzam neobyčajné v obyčajnom, nájdete ma často strateného v pozorovaní kolobehu života. Soľou môjho života je umenie, korením priatelia a najsilnejšou prísadou nad všetko vyvýšená láska. Spomedzi priateľov si najviac cením Boha, preto mastretnete aj v božích chrámoch, kde vyhľadávam pokoj v duši.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    154
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1065
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zamyslenia
                        
                     
                                     
                        
                            Živý pozdrav z Márnice
                        
                     
                                     
                        
                            Idúc ulicou
                        
                     
                                     
                        
                            Cesta za zmyslom života
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Slová nad zrkadlom
                        
                     
                                     
                        
                            Boh
                        
                     
                                     
                        
                            Nočná návšteva
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Beatnici + absurdné divadlo
                                     
                                                                             
                                            J.P.Sartre, Ionesco
                                     
                                                                             
                                            Franz Kafka
                                     
                                                                             
                                            Haruki Murakami
                                     
                                                                             
                                            Mario Puzo
                                     
                                                                             
                                            Dan Brown, D. Dán
                                     
                                                                             
                                            Paolo Coelho
                                     
                                                                             
                                            Maxim E.Matkin
                                     
                                                                             
                                            Stephen King
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            relaxačné napr. Jaro Filip - Meditation for Piano
                                     
                                                                             
                                            Hip-hop najmä SK scénu
                                     
                                                                             
                                            Rieka života a podobné
                                     
                                                                             
                                            cigánske, napr: Kmeťoband, Diabolské husle
                                     
                                                                             
                                            trocha popu, rocku, punku, romantiky
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Brnčová Kristínka
                                     
                                                                             
                                            Janka Dratvová
                                     
                                                                             
                                            Danielka Nemčoková
                                     
                                                                             
                                            Danka Janebová
                                     
                                                                             
                                            Janka Regulyová
                                     
                                                                             
                                            Miška Urbanová
                                     
                                                                             
                                            Phillipa Marco
                                     
                                                                             
                                            Lienka Jamborová
                                     
                                                                             
                                            Pietruchova Oľga
                                     
                                                                             
                                            Peťuška Debnárová
                                     
                                                                             
                                            Čínske dievča-Chen Lidka Liang &amp;#26753;&amp;#26216;
                                     
                                                                             
                                            Kika Hatarova
                                     
                                                                             
                                            AnezaHviezdna
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            LumpArt - moderné umenie
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




