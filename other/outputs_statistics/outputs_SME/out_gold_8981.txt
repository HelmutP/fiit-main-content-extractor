

 „Keby sa nepriznám, možno by bolo všetko ako pred tým... Videl nás vôbec niekto?" vyčítal si Martin. Izbu zaplnilo ticho, ktoré prerušil až hlásny výkrik. 
 „Baška, vráť sa!" jeho naliehavé slová kričali. 
 Hlavu si spustil do prázdnych dlaní. „Prepáč. Už budem navždy len s tebou. Prosím, odpusť mi." 
 Myšlienky mu nedovolili spávať, trhali zo snov a prinášali únavu. Nechcel sa viac vidieť neverne. Z monotónneho obracania fľaše chradol, a tak ľahšie padal do mdlôb, v ktorých mal čas nepremýšľať. Rozchod ho natoľko zničil, že vo výpovedi videl jediné možné východisko. Rozhodol sa odísť. Odísť na okraj spoločnosti, kde si chcel pomôcť úplne sám. Desať spoločne prežitých rokov, zaznamenaných teraz už len v spomienkach, to však robilo takmer nemožným. Martin vstal. Rozhliadol sa a pozbieral kúsky potrhanej fotografie. Zapadali do seba tak, ako do seba nedávno zapadali aj ich ruky. 
   
 Prepadla som ženskému zlozvyku. Moje úspory sa pravidelne stávali obeťou zlej nálady liečenej v umelom svete obchodných centier. Nákupy mi vždy prinášali mohutnú dávku odreagovania sa a v spoločnosti priateľov mi vysýchali slzy rýchlejšie, ako by človek čakal. No bolesť ostávala, najmä vo chvíľach, keď som bola sama. 
 Ležala som v matkinom lone. Prikrývala sa paplónom a prosila, prečo nemôžem tento svet nevnímať tak, ako keď som sa narodila. Po tvári mi stekal štipľavý prúd sĺz, až premočil celú obliečku. Keby sa tak mohol odplaviť do nedovidených diaľav a vzal so sebou aj moje trápenie. A tvár rozžiaril tak, ako to vedel len Martin. Priniesol by mi čerstvú kyticu kvetov. Voňavú a veľkú, že by sme sa cez ňu nevideli. Jeho citlivosť a dôraz si ma vždy vedeli udobriť. 
   
 Všetko som zničila. 
 Seba i Martina... 
 Nikdy si neodpustím zamlčanú neveru, za ktorú som ho zbila. Priznal sa, no ja nie a nie. 
 Trp a trp! 
 Udierala som ho a vlastné prehltávala. Zlyhala som. 
   
 Slnko vystúpilo nad obzor, ja som vstala a išla sa naraňajkovať. Vo fľaši ostal ešte kúsok whiskey. Na stole ma čakala krabička cigariet. Pre ne ma matka odsudzovala. Nenávidela fajčiarov. Musela som sa skrývať jej prísnemu pohľadu a raňajšiu rutinu vykonávať zavčas rána. Už v detstve ma chválili sa výraznú predstavivosť. Vraj ju mám zužitkovať a skúsiť to s umením. Teraz ma však neutíchajúco prenasledovala a ja som ho videla všade. 
   
 Vo víne, ktoré si nikdy neodoprel... 
 V rádiu, ktoré mal tak rád... 
 V práčke, na ktorej sa mu to tak páčilo... 
 Vo vysávači, ktorý aj v nedeľu maskoval naše výkriky... 
 ...aj vo mne, v ktorej ho toľko ostalo. 
   
 Malo to miznúť, no zanechal vo mne príliš veľa. A ja som to všetko behom chvíle chcela zahodiť. Utiecť preč, zbabelo a bez riešenia. 
 S chuťou som vyfúkla cigaretový dym a zdvihla pohár k perám. Bol to práve matkin hlas, čo pohár rozbil. „Martin ti napísal." 
 Ani to množstvo sklených úlomkov nedokázalo konkurovať mojim pocitom. Vnútro sa mi roztrieštilo na niekoľko časti a ja som ako kvapka padla k zemi. Cítila som radosť z toho, že sa udobríme. Zradu, za ktorú som mohla. Beznádej z toho, čo má prísť. Bála som sa prečítať, čo napísal. 
   
 ahoj.cakam ta dnes na nasom mieste o 16:00 
 prosim prid 
   
 Po desiatich minútach hľadenia na display ma prebrali matkine zvedavé slová. „Čo chce ten sviniar?" 
 Zmĺkla som. 
 Skúsim to... 
   
 Napravím všetko, čo som pokazila... 
   
 Len predsa nie takto. 
   
 Zvyšok dňa som venovala svojmu zovňajšku. Zabudla som na to, čo sa stalo a myslela len na to, čo teraz príde. Obliekla som si nové šaty a navoňavkovala sa Martinovou obľúbenou vôňou. 
 V parku sa hral jesenný vánok s padajúcimi listami. I tie ležiace na zemi znenazdajky nadvihol a odniesol o pár stromov ďalej. Zastavila som na chodníku, ktorý ma mal zaviesť k Martinovi. V rukách som zvierala prázdno, ktorým okolie zaváňalo. Postupovala som ďalej s neistotou a prázdnymi dlaňami a pokúšala sa zhlboka dýchať. Sedel na neďalekej lavičke a mne sa podlomili kolená. Na čelo mi vystúpili kvapky potu a stres roztriasol ruky i hlas. Tak, ako keď sme sa prvýkrát pobozkali. 
 Ahoj Martin...ko. Tak som tu. 
 Bála som si prisadnúť a objať ho. 
 „Ahoj. Tak...predsa si nakoniec prišla?" 
 Martin, prepáč. Boli som ti neverná aj ja. V tú noc som bozkávala niekoho, koho si taktiež nepamätám...a pokračovalo to aj ďalej. Prepáč mi. Mrzí ma to. 
 Jeho tvárou prebehol náhly údiv a ja som vystrela ruky k jeho očiam, ktoré sa už-už chystali vypadnúť z jamôk. Neveril tomu, čo počuje. Či je to pravda alebo zošalel až natoľko, že trpí silnými halucináciami. Moje srdce tĺklo zmätene a dych sa zmenil na pravidelne nepravidelný. Nemohla som sa poriadne nadýchnuť, akoby sa moje pľúca náhle scvrkli. Strach umocnil nečakaný signál správy. Nemala som odvahu si ju prečítať. Nepísal mi znovu Martin? 
   
 Basi, dnes som nasla vasu spolocnu fotku.ste spolu zlati, drzte sa!!! 
   
 Na fotke som bola v Martinovom objatí. 
 Večer, ktorý si nepamätám. 
   
 S kým sme si to vlastne boli neverní? 
   
   
 Foto: Ľubica Martincová 

