

  
  
  
    aaaaaaaaaaaaaaaaaaaaaa 
 sssssssssssssssssssssssssssssss 
 sssssssssssssssssssssssssssss 
   
  
 Rozhovor JÁRAY-Dvurčneskij 
 Touto cestou predkladám širokej laickej i odbornej verejnosti, môj rozhovor s riaditeľom Matematického  Ústavu SAV, pánom : 
  
    
     
 prof. RNDr. Anatolijom Dvurčenskijom DrSc. - 
 Úvodom dovolím si predstaviť účastníka tohto rozhovoru: 
 Prof. RNDr. Anatolij Dvurčenskij DrSc. 
 Je riaditeľom Matematického ústavu SAV, ako aj vedec roka 2005. Rozhodol o tom nasledovný kolektív odborníkov zo SAV:  
 - 
 Jeho najdôležitejšie vedecké výsledky sú nasledovné:  Vyriešenie problému združených rozdelení pozorovateľných v kvantových logikách (spolu s doc. RNDr. Pulmanovou, DrSc.), Zovšeobecnenie a aplikácie Gleasovnovej vety v kvantových logikách Hilbertovho priestoru, tenzorový súčin diferenčných posetov, reprezentácia pseudo MV-algebier a pseudo efektových algebier pomocou unikátnych čiastočne usporiadaných grúp. 
 Slávny výrok Anatolija Dvurčenskijho: 
 „Matematik musí veľa vedieť, aby mohol málo povedať.“  
 -aaaaaaaaaaa 
 aaaaaaaaaaaaaaaaaaaaaaa 
 Priebeh rozhovoru je nasledovný: 
  JÁRAY. 
  Pán riaditeľ MÚ SAV, môže vaša osoba reprezentovať úroveň matematických poznatkov slovenských matematikov na najvyššej úrovni ? 
  -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Áno. 
 - 
   JÁRAY. 
 Súhlasíte so mnou, že výsledky z vyučovania matematiky na školách a univerzitách SR sú najhoršie zo všetkých vyučovaných predmetov? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Áno. 
 - 
  JÁRAY.  
 Mohli by ste stručne uviesť príčiny tejto neblahej reality. 
  -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Jednoznačne ide o zlý spôsob výučby matematiky a to hlavne z toho dôvodu, že sami učitelia nie sú odborne pripravení vyučovať matematiku.  
 - 
  JÁRAY. 
 Tam kde matematiku vyučuje matematický odborník, tak tam s matematikou nie sú problémy? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
  Nie. 
   
  JÁRAY. 
 Stručne povedané, keby ste vy osobne vyučovali matematiku, tak vaši žiaci by nemali s matematikou žiadne problémy? 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
  Nie. 
 - 
   JÁRAY. 
 Pán profesor, ja mám taký malý problém, nie je mi jasné akú mocninu majú celé čísla 2 a 3 v matematike. Mohli by ste mi to vysvetliť. 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 O čo vám ide. 
 -
 
  
 JÁRAY.
 Ak by čísla 2 a 3 mali spoločného nulového exponenta, tak by to vyzeralo nasledovne: 20;  30, čo by ale v konečnom dôsledku znamenalo, že čísla 2 a 3 sú v skutočnosti dve rovnaké čísla, zapísané rôznou formou, preto že platí 20 =  30 = 11; Lebo platí, že každé číslo umocnené na nultú, je číslo 11! 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 To môžem podpísať. 
 - 
 JÁRAY. 
 Pre prípad, žeby čísla 2 a 3 mali spoločné exponenty o hodnote 2, čiže 22; 32; oni by už nereprezentovali čísla 2 a 3, ale čísla 41 a 91. 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij. . 
 Aj to môžem podpísať. 
 - 
  JÁRAY. 
 Takže aby čísla 2 a 3, zachovali svoju predpokladanú matematickú hodnotu, mali by mať jedine exponenty 1; čiže musí platiť 21, 31. 
  Takže na záver by sa muselo konštatovať, že všetky celé matematické čísla ležiace na osi x, sú čísla s exponentom 1. 
 -----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij.  
 Vyplýva to z logiky matematiky. 
 - 
  JÁRAY. 
 N a teraz túto matematicko -  logickú skutočnosť, preverme v matematickej praxi a to na matematickej rovnici: 
 2 . 3 = 6  
 ---skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 V zmysle predchádzajúcich záverov, táto rovnica vyzerá nasledovne: 
 I.     20. 30  =   6(0+0)   =  11 
 Pre tento prípad je jedno čo s tými čísla urobíme, či neurobíme, výsledok toho súčinu je vždy číslo 11.  (10 = 11) 
 ----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 II.    21. 31  =   ?     
 Pre tento prípad s naznačeným  súčinom  21. 31  už nič nemôžeme robiť a tak nemôžeme ani tvrdiť, že výsledkom tohto súčinu je číslo 6, lebo ani v súčasnej matematike neplatí, že: 
 21. 31  = 62,  lebo 62 = 361  a nie 61.   
 Keby ale v matematike platilo, že:  
 21. 31  = 61 tak s matematikou by to bolo veľmi zlé. 
 ---skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Takže ako sa matematika dopracovala k tomu, že súčin bezexponenciálnych čísel 
  2. 3  = 6.  
 Aké exponenty musia mať čísla 2 a 3  aby výsledkom  ich súčinu bolo číslo 6 ? 
 ----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
 Dvurčenskij. 
 Úprimne sa priznám, že i keď som profesorom matematiky, ja to neviem. 
 - 
 JÁRAY. 
 Tak potom prečo sa  súčin 2 . 3 = 6,  učí na školách a univerzitách  ako pravda SR? 
 ----skromný JÁRAY-----múdry JÁRAY----- skromný JÁRAY-----múdry JÁRAY----- 
  
 Dvurčenskij.  
 Úprimne sa  priznám, že i keď som profesorom matematiky, ja to neviem. 
   
 Tento text organicky doplňujú nasledovné články: 
 http://jaray.blog.sme.sk/clanok.asp?cl=206648&amp;bk=42411  
 http://jaray.blog.sme.sk/clanok.asp?cl=215377&amp;bk=98564  
   

