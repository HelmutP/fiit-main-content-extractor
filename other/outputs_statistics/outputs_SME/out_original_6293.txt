
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Kotian
                                        &gt;
                Nezaradené
                     
                 To sú strašné kaleráby, povedal Bagír 

        
            
                                    11.5.2010
            o
            9:37
                        (upravené
                11.5.2010
                o
                11:44)
                        |
            Karma článku:
                5.97
            |
            Prečítané 
            1121-krát
                    
         
     
         
             

                 
                    To sú  strašné kaleráby, tohto drúka ste si zvolili sami? spýtal sa Bagír, lebo  ako dvaapolročný pes si posledné parlamentné voľby nepamätal, a radšej  prepol na TA3.
                 

                Tam zasa Anna Belousovová objavovala Ameriku v rómskej, teda cigánskej otázke, ale na Kužmovu výčitku, prečo nezahlasovala za jeho návrhy, keď ich teraz tak razantne presadzuje, vedome neodpovedala a typickým spôsobom, aký každému ženatému chlapovi pripomína jeho zbesnenú polovičku, odviedla tému celkom inam. To sa nedá počúvať, vyhlásil Bagír, ktorý v podobných chvíľach v jeho domácnosti prezieravo zaliezal do pelechu a stal sa tak objektívnym indikátorom rastu domáceho napätia, a pazúrikom prepol naspäť na verejnoprávny kanál – aspoň sa dozvieme, čo nás čaká, ospravedlňujúco povedal svojmu pánovi, ktorý mal tých kalerábov plnú hlavu a radšej si čítal fantasy o čarodejníkoch, ktorá mala k realite oveľa bližšie ako toto mimoriadne vystúpenie predsedu vlády v Slovenskej televízii. To žiadny z jeho voličov nevidí, ako bojuje proti gréckej predstave sociálneho štátu, ktorá je mu taká blízka, spýtal sa Bagír a ukázal na rešerše na stole, kde sa to podobnými názormi len tak hemžilo. Ja viem, nie je to nič objavné, píšu o tom v smečku aj v Trende, ale bojovať za stabilitu eura a pritom byť pri raste zadlženosti vlastnej krajiny ešte rýchlejší, ako boli Gréci, sa mi zdá natoľko zjavné, zaškeril sa pes, že by to mohli vidieť aj dospelí ľudia s právom voliť... Viem, viem, nemáš rád, keď sa vyťahujem nad vami, aj keď sú to voliči koalície, cúval Bagír nielen slovne a blížil sa k pelechu, pritom si sám neviem odomknúť ani len dvere a musím čakať, kým niekto so mnou pôjde von, priznával pes svoju submisívnu pozíciu, pričom jedným očkom pozoroval premiéra, ako sa snaží pôsobiť starostlivým dojmom, nevdojak pripomínajúc jeho predchodcu, ktorý si podobne strieľal z divákov už presláveným songom Neublížil som nikomu z vás... A ako rozumieš slovu zatiaľ, spýtal sa spoza dverí Bagír, keď počul, ako premiér upokojuje svojich insitných voličov, že „nikto od Slovenska nepožaduje žiadne pôžičky ani žiadnu finančnú účasť, pretože v danom okamihu, ak by sa tento mechanizmus aktivoval, by postačovalo, aby SR prevzala napríklad garancie“. Do dvanásteho júna napríklad?

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            7 dôvodov, prečo sme skončili druhí, alebo okom hokejového laika 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Aký veľký je prúser Richarda Sulíka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Súd s Tomom Nicholsonom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Kedy ak nie teraz?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Gorily verzus ovce
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Kotian
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Kotian
            
         
        robertkotian.blog.sme.sk (rss)
         
                        VIP
                             
     
        som novinár na voľnej nohe - odkedy som odišiel zo Sme.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2633
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




