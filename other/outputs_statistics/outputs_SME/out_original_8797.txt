
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Filip Holec
                                        &gt;
                Nezaradené
                     
                 Prekliatie anglických brankárov 

        
            
                                    14.6.2010
            o
            12:55
                        (upravené
                14.6.2010
                o
                19:54)
                        |
            Karma článku:
                3.23
            |
            Prečítané 
            637-krát
                    
         
     
         
             

                 
                    Od čias Petra Shiltona cez tragikomických Seamana či Robinsona až do Afriky. História reprezentácie Albiónu je popretkávaná skvelými futbalistami a takmer každý zo stoviek futbalistov s tromi levmi na hrudi bol zapamätateľný a výnimočný hráč.
                 

                 
  
   Takisto aj muži medzi tromi žrďami anglickej reprezentácie vchádzajú do dejín. Pravidelne však ide o ich neskutočné zaváhania. Či už to bol David Seaman v roku 2002, či Paul Robinson v kvalifikačnom zápase v Chorvátsku, alebo Scott Carson s tým istým súperom, všetci majú spoločné jedno. Ani jeden z nich je brankár svetovej extratriedy. Najnovšie však sklamal Rob Green. Brankár, pre ktorého bolo špecifické, že takmer nerobí chyby. Teraz čakám, čo vyvedie Joey Hart..Naozaj neviem, či je momentálne príjemné vstupovať do brány vo farbách Albiónu, keď už zrejme aj sama královná čaká na nejaký husársky kúsok anglického reprezentačného brankára. Nech už nosí akékolvek meno na chrbte.   Je veľmi jednoduché zhodiť prehru, či stratu bodov na brankára. Je jedno akú krajinu reprezentujete, vždy máte odlišný dres a preto ste vždy jednoduchším terčom. Anglickí reprezentační brankári však dávajú tejto psychologickej nevýhode ďalší rozmer. Neviem si vysvetliť prečo sú to vždy anglickí gólmani, ktorí sú zárukou smiešneho gólu vo vlastnej sieti. Nechápem, že práve brankári, ktorí mali doteraz povesť, ktorá zaručovala koniec čiernych časov na tomto poste sú zrazu opäť nočnou morou. Absolútne sa nesnažím zhodiť chybu na Roba Greena, práve naopak. V ďalšom vývoji konkrétneho zápasu ukázal svoju triedu už len tým, že dokázal striasť zo seba ťaživý pocit chyby, ktorý dokáže s brankárovou psychikou narobiť veľké veci. Angličania si teraz pravdepodobne búchajú hlavu o stenu, pretože opäť majú známeho strašiaka. Snažím sa len prísť na to, prečo je ten post u nich ešte špecifickejší. Viem, že odpoveď mi nedá nik a nedám si ju asi ani sám, ale momentálne musí byť pre akéhokoľvek anglického reprezentačného brankára zložité postaviť sa do brány. Nedôvera, ktorú musia všetci traja na šampionáte cítiť, im môže zlomiť väz.   Podľa môjho názoru práve tento aspekt stojí za krízou anglických brankárov. Ani jeden z menovaných nepatril medzi anglický priemer v domácej súťaži, všetci traja podávali štandartne kvalitné výkony. Špecifickosť brankárskeho postu je práve v psychológii a sebadôvere a preto by sa mali Angličania a anglickí fanúšikovia zamyslieť nad tým, či sami nestoja za zaváhaniami svojich mužov v rukaviciach. Pokiaľ totiž brankár necíti podporu a neverí si, nechytí nič. Akurát tak chrípku. Myslím, že aj náš Ján Mucha by vedel rozprávať o tom, ako vie brankár oplatiť prejavenú dôveru, ktorú dostal až v Legii Varšava. Zhrnuté a podčiarknuté: Akonáhle si anglická verejnosť bude slubovať od brankárov hrúbky, aj sa ich dočká. V kombinácii s loptou Jabulani je nesebavedomý brankár katastrofou. My môžme byť šťastní, že Janko Mucha je v obrovskej pohode a Angličania môžu smútiť pretože si sami vytvorili kult brankárskych skratov a miniel.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Holec 
                                        
                                            Karol, mám sa vrátiť domov?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Holec 
                                        
                                            Proletári všetkých krajín sa spojili. V 21. storočí. Na FMK.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Holec 
                                        
                                            R.I.P Slovenský šport
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Holec 
                                        
                                            SFZ- Tento vulgarizmus obsahuje futbal
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Holec 
                                        
                                            Spojila nás Afrika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Filip Holec
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Filip Holec
            
         
        holec.blog.sme.sk (rss)
         
                                     
     
        S futbalom som sa narodil, vyrastal som s ním a nikdy z neho nevyrastiem..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1174
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       List Karolovi: O bulvári a ľuďoch
                     
                                                         
                       List Karolovi: Ficovo posolstvo v Modrom z neba
                     
                                                         
                       Oplatí sa byť slušný?
                     
                                                         
                       Mesiac v špitáli - III.časť - 11.4. Deň D
                     
                                                         
                       Už neplačem
                     
                                                         
                       O tom, prečo ľudia hodnotia niečo, čomu sa nerozumejú
                     
                                                         
                       Vianoce: Láska v čase cholery
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




