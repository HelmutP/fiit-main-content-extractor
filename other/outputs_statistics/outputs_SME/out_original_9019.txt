
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Robíš mi kariéru, bomba! 

        
            
                                    17.6.2010
            o
            5:33
                        (upravené
                17.6.2010
                o
                5:39)
                        |
            Karma článku:
                3.87
            |
            Prečítané 
            908-krát
                    
         
     
         
             

                 
                      V stredu popoludní mi voľakto zazvonil presne vo chvíli, keď som si zasadol k televízii pozerať futbal z majstrovstiev sveta Španielsko – Švajčiarsko a priznám sa, nepotešil som sa. Nikto neavizoval návštevu, pošta už tu bola a susedov mám skôr domasedov, nuž som očakával niečo na spôsob tatárskeho vpádu. A stalo sa, telefónom sa od vchodu ozvalo – otvor, tu je Dano! Na ktorom to bývaš? A výťah chodí? Po chvíli už, vyškerený, spokojný, s fľašou vína v ruke vtrhol do obývačky, bez váhania obsadil pred televízorom najlepšiu lóžu a nariadil priniesť vývrtku a poháre. Nuž veru, hovorím, nemáme čo oslavovať, ten zápas sme včera zmrvili, ale keď ináč nedáš...
                 

                         A po chvíli som teda ponalieval, štrngli sme si a starý Davidík – je najradšej, ak ho titulujú akurát takto – mi oznámil. Nič neoslavujeme, ale urobil si ma slávnym a včera som si to vychutnával ako ešte nikdy v živote nič! Slávna bola aj šesťdesiatka, aj prvá prapravnučka, ale ten blog, to bola fantázia! Syn mi to rozmnožil, susedia s tým mávali ako na manifestácii a v Papučke, to je tá jeho domovská piváreň, tam to bolo ako v úli! To bolo vláda sem, vláda tam, Davidík povedal a tak aj bude, Dano povedz, ako ťa to napadlo a tak som musel vysvetľovať, čo je to škola života! Naraz ma brali vážne, opísal si to prvotriedne a ešte aj tá moja mi dnes dala päťeurovku! Tú si však odložím až keď budeme hrať s Talianmi, cítim, že bude remíza a aj tak postúpime, takže budem mať základ na oslavy.   Potom sme chvíľku pozerali, sedmička merlotu sa míňala, no ako sa ukázalo, akurát toto nebol dôvod Danovej návštevy. Vieš, oznámil mi, od rána nerozmýšľam nad ničím iným iba nad tým, ako sa tí komunisti nechcú poddať, ako ich drží tá vidina a posadnutosť sedieť v tej vláde a už som to chlapom vysvetľoval, že sú za tým tie strašné peniaze čo sú vo všetkých tých dohodnutých kšeftoch, lebo z nich kvapne aj im. A keď tam nebudú, tak kšeft dostanú iní a namiesto koruniek príde len figa borová, takže zostanú len oči pre plač! A predstav si, porozumeli, nikto sa neškriepil.   A akurát pred chvíľkou som si prečítal to SME, čo si mi poslal a tam, rovno na prvej stránke takými velikánskymi písmenami – vraj vláda bude bez komunistov, chcú to všetky štyri koaličné strany! Čo myslíš, dá sa to, však ty si vždy hovoril, že je to ťažká vec, lebo kto chcel voľačo znamenať, kto chcel byť chytrý, ten sa musel stať komunistom!   Nuž vysvetľoval som, pomáhal mi predovšetkým čas - už je tu sloboda viac ako dvadsať rokov a  veľa tých nových je naozaj mladých, keď tu „rodná a jediná už nepanovala“, dosť ich je veriacich, pre tých bolo samozrejmé vyznávať iné životné zásady a časť ich mala jednoducho šťastie, že vyrastala v rodinách, kde platila filozofia humánnych rozmerov. Starý Davidík sa tešil naozaj úprimne a oznámil mi, že toto ho dnes pri pive vôbec nenapadlo, ale tomuto budú zasa rozumieť všetci a keď už som ho preslávil včera ja, zajtra to urobí vo vlastnej réžii! Budú čumieť, budeme mať vládu bez jediného „partajníka s červeným pôvodom“ a to sa tiež bude musieť osláviť. Alebo vieš, čo. Však už sme to červené dopili, stále je nula nula, takže radšej pôjdem oslavovať už teraz. Aj tam je telka a tam môžem aj fajčiť, takže si to viac vychutnám. Budú sa diviť, hovorím ti, to bude zasa bomba – bez komunistov!   A bol preč – videl som z kuchynského obloka, ako si to namieril rovno do bistra oproti, kde celkom iste sedela jeho ekipa verných, pozerala futbal, pila pivo a meditovala. Majú sa na čo tešiť, starý Dano má tému naozaj nad iné. Bude im prorokovať, že nová vláda bude celkom nová, po komunistovi v nej nebude ani len smrad!   Predstavte si však, že ten nápad, akurát takúto udalosť pripomenúť prvoplánovo, ako vec hodnú zapamätania si, potom zaujal aj mňa. V Čičovej prvej vláde to boli vari všetci, v Mečiarových vládach veľa, aj v Moravčíkovej, ak si pamätáte, ba aj v Dzurindových, no a v tej Ficovej ich bolo zo šestnástich desať! A teraz nebude nikto – presne tak, ako o tom snívali členovia KDH od chvíle, keď ako politická strana vznikli. Úžasné a musím doznať, že starému Davidíkovi to na jazyku muselo zachutnať aspoň tak dobre, ako tá sedmička austrálskeho červeného. Na staré kolená robí kariéru, politickú, no bomba!         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




