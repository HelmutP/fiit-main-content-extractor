
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s Medveďom - Slávnostná večera pre dvoch 

        
            
                                    21.2.2010
            o
            12:00
                        (upravené
                20.2.2010
                o
                19:44)
                        |
            Karma článku:
                13.01
            |
            Prečítané 
            6623-krát
                    
         
     
         
             

                 
                    Minulý víkend som trávil doma sám. No teda doma. V sobotu v práci a v kine. No a v nedeľu som už od rána v prázdnom byte čakal na to, kedy sa Macko vráti domov. Bol som unavený, otrávený, a ak som si chcel zlepšiť náladu, musel som začať robiť niečo, čo ma aspoň trochu baví. Čo by ma tak mohlo baviť? A potom som si spomenul, že je Valentína a ja nemám pre Macka ani Margot tyčinku. A vtedy už bolo všetko jasné. Idem uvariť slávnostnú večeru.
                 

                 Prekutral som chladničku aj mrazničku, vyhádzal pol špajze, skočil som aj do večierky, v ktorej toho mávajú len málo a podarilo sa mi dať dokopy tento základný materiál.   Na predjedlo - Zeleninový šalát s chrumkavou salámou Kus ľadového šalátu, paradajku, papriku, nakladanú papriku, pár olív a konček z trvanlivej salámy.   Na hlavné jedlo - Princezná Karé na hrášku Kus vykosteného karé asi 40 dekagramov, dve lyžice sójovky, dve lyžice chilli omáčky a dve lyžice ustricovej omáčky, jeden mrazený hrášok a poriadne veľa cesnaku.   Na múčnik Jedno mrazené lístkové cesto, štyri decilitre mlieka, jeden zlatý klas, cukor práškový aj kryštálový a vo večierke predsa len mali smotanu na šľahanie a konzervované mandarínky.   No to už je materiál s ktorým sa predsa len dá pracovať. Teraz si treba rozplánovať čas tak, aby všetko bolo dokonale pripravené, keď sa Macko vráti, aby tá večera mohla šup šup frčať. Dnes to teda okrem základných receptov bude aj trochu o tom, ako si zorganizovať prácu keď pripravujeme viac chodov, aby sme neskončili s jazykom na veste.      Začneme s mäsom, nech má čas sa marinovať. Nakrájame ho na rezne široké ako prst a potrieme marinádou z chilli omáčky, sójovej omáčky a ustricovej omáčky. Soliť ho vôbec netreba, marináda je dosť slaná. Mäso uložíme do chladničky a ideme ďalej.      Zo štyroch decilitrov mlieka, dvoch lyžíc cukru a jedného prášku Zlatý klas uvaríme trochu tuhší puding, prelejeme ho do misy a necháme pomaly chladnúť.      Do hrnca dáme variť mrazený hrášok a aspoň jednu ošúpanú hlavičku cesnaku. Vodu jemne posolíme a keď začne vrieť, vypneme plameň a necháme ešte tak päť minút postáť.      Teraz nakrájame všetku zeleninu na šalát, premiešame ju a necháme odpočívať bez ochutenia v chladničke. Kúsok trvanlivej salámy nakrájame na drobné hranolčeky a tiež necháme odpočívať v chladničke.      Ideme na lístkové cesto. Rozkrojíme ho na dva kusy a každú časť vyvaľkáme na tenko. Asi dva až tri milimetre. Z každej časti ostrým pomúčeným nožom vykrojíme srdce. Jedno poriadne poprepichujeme vidličkou a druhé potrieme vajíčkom a poriadne posypeme kryštálovým cukrom.   Obe srdcia dáme do rúry vyhriatej na 170 stupňov. Pečieme, kým neskaramelizuje cukor, ktorým je posypané jedno srdce. Vtedy by už mali byť hotové obe časti.      Kým sa srdcia upečú, zlejeme hrášok a ponorným mixérom ho poriadne rozmixujeme na hladkú kašu. Ak treba, môžeme jemne dosoliť.      Takto presne vyzerá po upečení to srdce potreté vajíčkom a posypané cukrom. Pieklo sa asi desať minút. Obe srdcia necháme celkom vychladnúť a až potom s nimi manipulujeme.      V nádobe vyšľaháme smotanu na šľahanie s lyžicou cukru celkom do tuha a odložíme do chladničky.      Dokončíme múčnik. Na misu uložíme srdce z cesta, ktoré sme poprepichovali vidličkou, a tak nenarástlo. Na neho natrieme celkom vychladnutý puding. Obložíme mandarínkami z konzervy. Potrieme šľahačkou a priklopíme druhým srdcom. Odložíme do chladničky.      Šalát navŕšime v kôpke na tanier. Zatiaľ ničím neochutíme, len ho necháme, aby získal izbovú teplotu a nebol veľmi studený.   Týmto je celá príprava večere hotová a máme čas na prípravu seba a stola.   Ďalšie práce už robíme až vtedy, keď večera naozaj začne.      Najprv na panvici opražíme hranolčeky z trvanlivej salámy.      Keď pustia vlastný tuk, pridáme do panvice dva krajce chleba a z každej strany ich do chrumkava opražíme.      Šalát posypeme chrumkavou opraženou salámou, jemne osolíme, pokvapkáme citrónom a      ... pridáme k nemu topinku. Osviežujúce, šťavnaté a chrumkavé.      Keď je predjedlo zjedené, postavíme si na sporák grilovaciu panvicu, necháme ju rozohriať a namarinované mäso ogrilujeme z každej strany jednu minútu.   Celá príprava aj s rozohrievaním panvice nebude trvať dlhšie ako 5 minút.      Na tanier dáme kopček hráškovej kaše, pridáme voňavé mäso a kúsok citrónu na dochutenie. Ľahké, chutné a voňavé.      Na záver večere už len z chladničky vyberieme srdce, ostrým nožom narežeme porcie a s múčnikom sa presunieme do salónu. Sladké, chrumkavé a vzrušujúce.      Mackovi večera veľmi chutila a chválil sa kade tade, že aké mal on prekvapenie. A ja som sa skvele odreagoval a popri tom varení som zabudol na všetky nevyriešené problémy.   A že už je po Valentínovi? No a? Šak dobrú večeru si môžete dať hocikedy nie?   Tak dobrú chuť a veľa lásky.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (77)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




