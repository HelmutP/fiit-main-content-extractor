
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Šuraba
                                        &gt;
                Nezaradené
                     
                 Sobotný obed 

        
            
                                    8.5.2010
            o
            14:41
                        (upravené
                7.5.2010
                o
                17:30)
                        |
            Karma článku:
                4.61
            |
            Prečítané 
            1762-krát
                    
         
     
         
             

                 
                    Venujem Lucke, aby jej nebolo smutno.
                 

                 
  
       Chalani sa vrátili z dielne. Obaja mali špinavé ruky, spotené krky a červené oči. Boli v kováčskej dielni, kde si ostrili meče. Ja som sa vrátil z podzemnej knižnice, kde som pomáhal Dedkovi ukladať staré zväzky. Boli písané neviditeľným atramentom a voskom. Štípali žltým papierom. Vrátil som sa a sedím v kuchyni čakajúc na obed. Sobotný obed je stále pokrmom pre dušu, rovnako aj pre mňa.       Ako iste viete, všetci traja sme čarodejníci. Jeden z nich je rytier a má dobré srdce. Druhý z nich je kováč a má večný oheň. Ja som bardom, preto aj tie knihy, prechádzky s druidkou a pletenie púpav, či bozky pod čerešňou počas májovej noci. (Ale nebudem tu písať všetko.)       Aby sme si ich nazvali, jeden bude Milan a druhý Peter. Peter je rytierom, má veľmi ostrý meč a ohromnú silu. Milan je kováč s večným ohňom. Práve prišli z dielne, kde nám trom ostrili meče. Prezliekli si svoje košele a šli k rieke, umyť si brudné ruky.       Sedím pri kuchynskom obede, držím drevenú lyžicu. Som šťastný a čakám na obed. Dedko práve prichádza a prosí ma, či mu nepomôžem so sudom. Počas doobedia v knižnici som sa s ním zblížil. Rozprával mi, aby som si dával pozor na ľudskú závisť. Vytiahol mi nejaký zväzok a hovoril, že nech si to prečítam. Vraj to je o nejakom chlapcovi, ktorý bol námorníkom a vďaka ľudskej závisti prišiel o všetko. Tuším sa volal Dantés. Teraz celá kuchyňa vonia po žihľave, nakosenej ďateline a pive. Pri okne kvitne orgován a pripomína mi mladú druidku, s ktorou som objímal stromy.       Spoločne postavíme sud na stôl, je ohromne ťažký, preto použijem mágiu. Nie som nejaký paladín Karola Veľkého, preto sa ani neživím rukami. Počuť dupot čižiem na drevenej podlahe, chlapci sa vrátili. Majú v ruke raka, chytili ho keď si umývali zasvinené ruky.       Ale to už všetci štyria sedíme pri stole, máme pred sebou plné drevené krígle piva. Štrngáme si na svätú inkvizíciu, bez nej by sme nesedeli pri stole. Okrem toho jeme nakladaný syr, kyslé uhorky a tmavý chlieb. Dedko ho vzal ráno z pekárne, keď šiel ráno na trh. Popri tom zobral aj nejaké slané ryby, aby nám chutilo k pivu.       Popritom ako spolu debatujeme o všeličom, rak sa pečie na masti a Milan ho čarami obracia, aby bol chrumkavý.       „Čo budete robiť poobede?" Opýta sa nás Dedko, ktorý práve žuje nakladaný syr a splachuje ho pivom. .   „Mal by som ísť k Orlom, nech mi trochu porozprávajú o tunajšej inkvizícii." Ozýva sa Peťo, vyberajúc kyslú uhorku.   „Ja pôjdem s jednou rybárkou na prechádzku." Povráva Milan.       Všetci traja sa na neho pozrieme, div nám nezabehne. „Ty?" Spýtame sa ho zborovo a hľadíme naň ako Átrej na Falca.       „Áno, áno! Ja! Aj ja môžem ísť na rande!" Zlostí sa a červená sa ako ten rak, ktorého chytili pred chvíľou. Ale vie, že to nemyslíme v zlom.       „Čo budeš robiť ty?" Opýta sa ma Peter.   „Ja pôjdem za svojim Hnedým koňom." Hovorím mu.       Luskneme všetci traja prstom a pred nami stojí rak. Všetci sa ho dotýkame, jeme ho  a myslíme na to, ako je nám s teplým žalúdkom dobre. Sme šťastní, aj keď nás naháňa inkvizícia a nevieme, čo bude zajtra.       Vstávame z obeda, kosti z raka hodíme psovi a každý si ide po svojej ceste. Ja prichádzam do stajne, kde ma čaká môj ďalší priateľ. Natrhal som mu zopár zelených jabĺk, lebo tie milujeme obaja.       Toto je pre mňa sobotný obed. A nemusí tam byť ani upečený rak.       PS: A to ešte, keby som našiel v Dedkovej špajze klobásu a čalamádu.       Obrázky:       &lt;http://www.bcgamefisher.com/Chehalis%20river%201.jpg&gt; [cit. 2010-05-07]   &lt;http://es.homesandproperty.co.uk/handp/media/Old-Kitchen-350x300_958.jpg&gt; [cit. 2010-05-07]     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            O Košiciach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Návrat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Železničná krčma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Pierre Picaud
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Veľmi, veľmi málo o rytieroch okrúhleho stola
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Šuraba
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Šuraba
            
         
        suraba.blog.sme.sk (rss)
         
                        VIP
                             
     
        


 
geovisite


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    295
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1672
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Malý princ
                        
                     
                                     
                        
                            Českí herci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Sestra
                                     
                                                                             
                                            Najkrajšia milovníčka mačiek
                                     
                                                                             
                                            Moja zlatá Šárka
                                     
                                                                             
                                            Spolužiak-knihovník
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Veď kliknite:-)
                                     
                                                                             
                                            Blog nielen o bezpečnosti IT
                                     
                                                                             
                                            Kúpte si skriptá
                                     
                                                                             
                                            Jedna šikovná archivárka
                                     
                                                                             
                                            Nevinné duše troch kamarátov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sila / Slabosť (?)
                     
                                                         
                       Giethoorn - holandským mestečkom na člne
                     
                                                         
                       Základy Poa
                     
                                                         
                       Električkové zízanie
                     
                                                         
                       Víkend v Derbyshire: Chatsworth House
                     
                                                         
                       Krátko o ženách a meškaní
                     
                                                         
                       Gionatanove farby
                     
                                                         
                       Divadlo na jednotku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




