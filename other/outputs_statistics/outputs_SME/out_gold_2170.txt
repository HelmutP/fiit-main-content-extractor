

 Ako to bude vyzerať? 
 V pravom stĺpci každému blogerovi pribudne nový prvok "Čo ma zaujalo", takýto: 
  
 Odkedy to bude fungovať? 
 Od štvrtka 4. februára 2010. Texty, ktorým blogeri klikli na karmu pred týmto dátumom, sa zobrazovať NEBUDÚ - blogeri sa teda nemusia obávať, že sa spätne zobrazia ich kliky v čase, kedy ešte nevedeli, že bude takýto systém zavedený. 
 Som bloger, čo keď nechcem, aby ostatní videli, komu klikám na karmu? 
 Prihláste sa do blogu a kliknite na Nastavenia a tam zrušte zaškrtnutie položky "Zobrazovať na mojom blogu, ktoré články sa mi páčili a klikol som im na karmu." Okno z vášho blogu zmizne. 
 Som bloger, ako zistím, kto klikal na karmu pri mojich článkoch? 
 Nezistíte. Systém je postavený tak, aby si bloger nemohol preverovať klikačov na svoje články. Takisto nemožno nijako zistiť, ako kliká na karmu človek, ktorý nemá u nás aktívny blog. 
 ...tak a teraz konečne zistím, ktorý bloger si sám kliká na karmu! 
 Nie :-) Jediné články, ktoré sa v zozname obľúbených nezobrazia, ani ak im bloger na karmu klikne, budú jeho vlastné. 
 Máte ďalšie otázky? Píšte do diskusie. 

