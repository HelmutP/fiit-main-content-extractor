

 Pár dní po doručení rozsudku som ho zaslal všetkým členom vedeckej rady a akademického senátu UVLF v Košiciach. Na to som dostal nasledujúci e-mail od jedného z členov vedeckej rady, ktorému som najprv nechcel prikladať žiadnu váhu, no keďže má „hlbší zmysel", napokon som sa rozhodol ho zverejniť. 
  -----------------------------------------------SPRÁVA------------------------------------------------ 
 From: Dr. Ivan Holko [mailto:ivan.holko@gmail.com]  
 Sent: Thursday, May 27, 2010 11:15 AM 
 To: .......členovia VR a AS UVLF........ 
 Subject: Rozsudok 
  Vážení členovia akademickej obce UVLF. 
 V súvislosti s prípadom porušenia autorských práv MVDr. Máriou Kantíkovou, PhD., ktorý taktiež súvisí s udelením titulu PhD na Univerzite veterinárskeho lekárstva a farmácie v Košiciach, si dovoľujem zaslať Vám rozsudok Krajského súdu v Žiline zo dňa 30.3.2010. 
 Rozsudok je právoplatný. 
 Verím, že Vám táto informácia pomôže objektívne a bez predsudkov posúdiť celú záležitosť a prijať adekvátne stanovisko. 
 S pozdravom, 
 Ivan Holko 
  -------------------------------------------------ODPOVEĎ--------------------------------------------- 
  From: Kozak Marian &lt;kozak@uvm.sk&gt; 
 adresa pre odpoveďkozak@uvm.sk 
 To:"Dr. Ivan Holko" &lt;ivan.holko@gmail.com&gt; 
 cckozak@uvm.sk 
 Sent: 27. mája 2010 15:22 
 Subject:RE Rozsudok 
 Kolega, užívajte si tej „radosti z víťastva" i „dobrej reprezentácie"  veterinárneho stavu. 
 M.Kozák 
 A druhý krát  takéto veci si odpustíte zasielať na moju adresu. 
  -------------------------------------------------ODPOVEĎ--------------------------------------------- 
  From: Dr. Ivan Holko [mailto:ivan.holko@gmail.com]  
 Sent: Thursday, May 27, 2010 3:57 PM 
 To: kozak@uvm.sk 
 Subject: Pardón 
 Vážený pán profesor. 
 Stav poškodzujú v skutočnosti tí, čo páchajú nekalé praktiky a nie tí, čo na ne poukážu. 
 Je určite veľmi pohodlné diskutovať o týchto veciach v kuloároch a pred ich aktérmi sa milo usmievať. Ja som zvolil menej pohodlnú cestu. 
 Ale nerobím si ilúzie o uvažovaní ľudí zo starej socialistickej školy, ktorí si zamieňajú falošnú kolegialitu s ochranou stavu resp. sa za ňu alibisticky skrývajú. Čo ma ale skutočne mrzí je, že takíto ľudia vychovávajú budúcu elitu. 
 Ospravedlňujem sa za obťažovanie. 
 Ivan Holko 
  -------------------------------------------------ODPOVEĎ--------------------------------------------- 
  From: Kozak Marian &lt;kozak@uvm.sk&gt; 
 Mail to: odpoveďkozak@uvm.sk 
 To: "Dr. Ivan Holko" &lt;ivan.holko@gmail.com&gt; 
 cckozak@uvm.sk 
 Sent: 28. mája 2010 9:08 
 Subject: RE Pardón 
 Kolega, zatiaľ ste dokázali len neúctu a aroganciu, takže sa spamätajte keď Vy chcete vychovávať takýmto spôsobom spoločnosť ....... 
 Koniec diskuzie. 
 M.Kozák 
 Už som pred časom písal o tom ako univerzita kryje tento prešľap prominentky a všemožne sa snaží z nej urobiť obeť a zo mňa vinníka. 
 Napriek tomu, že sú svedkovia, napriek tomu, že dotyčná opísala preukázateľne aj od iných , napriek tomu, že napokon aj súd potvrdil jej vinu, sa od veľaváženého pána profesora, člena vedeckej rady, dočkám namiesto (aspoň) mlčania, výčitky a moralizovania o poškodzovaní nášho stavu a požiadavky, aby som ho neobťažoval (mimochodom na pracovnom kontaktnom e-maile člena vedeckej rady). 
 Nechcem hovoriť to, čo je asi jasné každému, ohľadom schopnosti sebareflexie, ktorá jediná udrží stav čistý. O tom, že tutlanie prehreškov, hlavne u prominentov, nikdy problémy nevyrieši, ale ich len prehĺbi. 
 Ide tu ešte o niečo iné. 
 Ak tí, ktorí by mali bdieť nad etikou akademickej obce nielenže nie sú schopní ustáť túto zodpovednosť, ale navyše osočujú a vinia tých, ktorí im nastavia zrkadlo, je to vážna vec. Vážny stav, ktorý si pokojne dovolím nazvať aj stavom celého vysokého školstva, ak nie aj celej našej spoločnosti. 
 Stále mi nedávalo zmysel, napriek prominentnému postaveniu páchateľky, prečo najlepšie hodnotená univerzita u nás dopustí tolerovanie niečoho, čo sa vo vyspelých štátoch považuje za akademický zločin. 
 Odpoveď mi dal jeden spolužiak, pôsobiaci na tejto univerzite: „Vieš, keby sme to mali takto brať, tak potom by sme v polovici prípadov museli odoberať tituly". 
 Asi na tom niečo bude, aj keď verím, že až o polovicu sa nejedná resp. ak aj, tak o tú „významnejšiu" polovicu - pánov riaditeľov, poslancov, ministrov, atď. To ale nijako neznižuje nezodpovednosť takéhoto konania a jedná sa o absolútne pošpinenie akademickej etiky a celé vzdelávanie sa tak stáva dehonestujúcou fraškou nielen pre protekčných, ale aj pre tých poctivých, ktorých čestne nadobudnutý titul zákonite devalvuje. 
 Takže už chápem, že kolegovia nechránia plagiátorku, oni chránia svoj domček s kariet 
   
 Je smutné, keď zmysel pre právo a poriadok už nie sú cnosťou, ale skôr handicapom ba priam neúctou až aroganciou. Bez diskusie. 

