
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Lisinovič
                                        &gt;
                ulicami miest
                     
                 Slovenské pamiatky (takmer) in memoriam 

        
            
                                    12.5.2010
            o
            15:20
                        (upravené
                11.5.2010
                o
                23:01)
                        |
            Karma článku:
                13.50
            |
            Prečítané 
            3649-krát
                    
         
     
         
             

                 
                    V našej republike máme množstvo vzorne vynovených architektonických pamiatok, ktoré sú ozajstnými skvostmi. Na opačnej strane sú však stavby, o ktoré sa nikto nestará a ktoré pomaly upadávajú do zabudnutia. Niektoré pripomenie tento článok. Pripravte sa na dosť smutné fotografie...
                 

                    Kaštieľ v Snine. Klasicistickú budovu postavili v roku 1781. Na oficiálnych stránkach mesta sa dá nájsť informácia, že v súčasnosti hľadajú mesto aj súkromný vlastník kaštieľa zdroje na jeho rekonštrukciu.            Renesančný kaštieľ v Trstíne. Postavený v roku 1685, rekonštruovaný v roku 1792.         Tzv. malý kaštieľ v Hanušovciach nad Topľou. Veľký kaštieľ mal viac šťastia, dnes je sídlom miestneho vlastivedného múzea. Písal som o ňom tu.         Na chvíľu opustíme kaštieľe a pozrieme sa na železnicu. Kruhová budova bývalého rušňového depa v Kútoch je dnes v zúfalom stave. Využívaná bola pravidelnou prevádzkou ešte v polovici 90. rokov 20. storočia. Neskôr tu bol depozitár historických rušňov. Pred desiatimi rokmi som sem chodil obdivovať pre mňa vtedy ešte neznáme parné rušne...         Takto vyzerá ukončenie mnohých lokáliek u nás. Pozrieme sa na dve, začneme v Plaveckom Mikuláši.      Kedysi sem chodili nocovať parné rušne miestnej lokálnej železnice zo Zohoru...            Nitrianske Pravno - konečná lokálky z Prievidze         Kaštieľ v Hlohovci         Kaštieľ v Seredi. Niekdajšie klasicistické sídlo Eszterházyovcov pustne od začiatku 90. rokov minulého storočia.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Skutočnosť a model: Grassalkovichov palác v Bratislave
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Miroslav Lisinovič 
                                        
                                            Putovanie po slovenských Kalváriách (194) - Klokočov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Lisinovič
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Lisinovič
            
         
        lisinovic.blog.sme.sk (rss)
         
                        VIP
                             
     
         Študent študujúci v škole života. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    519
                
                
                    Celková karma
                    
                                                6.42
                    
                
                
                    Priemerná čítanosť
                    2265
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šaštín
                        
                     
                                     
                        
                            Slovenské Kalvárie
                        
                     
                                     
                        
                            Z mojej papierovej dielne
                        
                     
                                     
                        
                            reportáže z modelárskych akcií
                        
                     
                                     
                        
                            v prírode
                        
                     
                                     
                        
                            ulicami miest
                        
                     
                                     
                        
                            železnice a MHD
                        
                     
                                     
                        
                            motorizmus
                        
                     
                                     
                        
                            viera
                        
                     
                                     
                        
                            extra
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj brat
                                     
                                                                             
                                            Moja manželka:)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            railtrains.sk
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




