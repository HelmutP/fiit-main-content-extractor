

 
  
 
 
Doteraz som vždy kupovala až slovenský preklad Harryho Pottera, pri
poslednej časti som sa z nedočkavosti rozhodla pre anglický originál. A
čítala som všade, kde sa dalo: cestou do práce i z práce, počas obeda, venčenia psa i
v posteli neskoro do noci. Ráno som vstávala nevyspatá a realitu okolo
seba vnímala len keď to bolo nutné...  
 
Každého, kto má Harryho príbehy v obľube, dokážu knihy o ňom
úplne pohltiť. Stačí pustiť fantáziu na špacír a ako mávnutím čarovného
prútika sa ocitnúť v inom svete. Na literatúre, filmoch a špeciálne na
fantastike mám najradšej to, čo v Deathly Hallows ústami Albusa
Dumbledora vyjadrila aj J.K. Rowlingová: "Of course it is happening
inside your head, Harry, but why on earth should that mean it is not
real?" ("Samozrejme, že sa to deje v tvojej hlave, Harry, ale prečo by
to malo znamenať, že to nie je skutočné?"). Tento výrok vo mne okamžite
zarezonoval, pretože jeho význam siaha ďaleko za hranice jeho konkrétneho určenia v tomto diele.  
 
 

 
 
Vzhľadom na informáciu samotnej J.K. Rowlingovej, ktorú ako "teaser" (čiže
dráždič) vypustila už dávno pred vydaním knihy, o smrti dvoch postáv, čitateľ v každej horšej situácii čaká, že niekto zomrie, či už z ústrednej
trojice, prípadne jej blízkeho okolia. Komu, ako alebo kedy sa to
stane, to vám nepoviem, rozhodne strata záujmu o dej nehrozí a
spomínaný autorkin sľub s tým nemá čo robiť. 
Celý román je
napnutý ako guma na trenírkach, ako sa vraví.
Rowlingová dupne na plyn hneď v úvode a nespustí z neho nohu až do
konca. Akčný dej striedajú kľudnejšie chvíľky, kedy sa nájde čas na
trochu racionálneho uvažovania a odpočinku pred ďalšou skúškou nervov,
no od začiatku sa zdá, že ich bude čoraz menej.  
Tentokrát už Harrymu a jeho priateľom nezostáva priestor na pocit bezpečia
medzi múrmi školy a hrozba je všadeprítomná. Smrťožrúti so svojím
Temným pánom v čele sú na love a prenasledujú každého, kto nie je
verný ich ideológii. 
 

Na čo sa ešte môžete tešiť?  
Na definitívne dospievanie hlavných postáv, ich učenie sa z vlastných
chýb, ktorých nebude málo i na nachádzanie odvahy vo chvíľach
najťažších. Na
podrobnosti zo života Albusa Dumbledora, ale napríklad aj Severusa
Snapa, či Harryho matky Lily, rodenej Evansovej. Ďalej
na skúmanie minulosti, hľadanie
súvislostí, detektívne pátranie po horcruxoch aj
rozlúštenie mnohých tajomstviev...  
A napokon na
konečné uzavieranie kruhu udalostí i vskutku šalamúnske zakončenie série, sprevádzané bombastickým finále.  
V neposlednom rade aj na viacero veľmi
dojímavých scén...  
 
 

 
 
Rowlingovej
štýl písania zachováva zvyky a štandard
predošlých častí heptalógie. Živé opisy aj dialógy, udržiavanie
prehľadnosti v akčných scénach, dobre zvládnutá psychológia postáv aj
Harryho vnútorných pochodov a bezchybne fungujúca spolupráca všetkých týchto prvkov. Nemožno jej uprieť obrovský talent, no všetky knihy naraz by som asi nezvládla, jej postupy sú predsa len dosť... jednoliate. Ňou vymyslený svet však milujem a som rada, že existuje v tejto podobe.
 
 

 
 
Anglický originál sa číta ako po masle,
zvládne ho každý pokročilý angličtinár, aj keď pripúšťam, že znalosť
mnohých slovných spojení je dôležitá, aby text nestrácal plynulú
súvislosť. Vzhľadom na to, že slovenské vydanie s názvom Harry Potter a dary smrti ohlásilo vydavateľstvo Ikar až na rok 2008, všetkým netrpezlivým čitateľom odporúčam pustiť sa do pôvodnej verzie. Ja osobne si zaobstarám aj tú slovenskú.  
 
 
Ilustrácie k americkému vydaniu HP7 © Mary GrandPré, perex foto © Bloomsbury, foto J.K.R. bohužiaľ neviem    
 

