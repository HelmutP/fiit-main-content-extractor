
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rasťo Gallo
                                        &gt;
                Nezaradené
                     
                 Výlety v okolí Košíc I - tipy na turistické atrakcie východu 

        
            
                                    9.6.2008
            o
            5:34
                        (upravené
                4.9.2011
                o
                10:26)
                        |
            Karma článku:
                10.59
            |
            Prečítané 
            25689-krát
                    
         
     
         
             

                 
                    Blíži sa nám leto, tak som si povedal, že by nebolo od veci hodiť sem zopár tipov na rodinné výlety v okolí (širšom) Košíc. Sám mám dvoch chlapcov vo veku 5 a 7 rokov, a dá to niekedy slušnú námahu vymyslieť zaujímavý (pre rodičov i deti) program na víkend. Uvedené destinácie sú vo väčšine fyzicky nenáročné, takže ich zvládnu i malé deti. Nebudem sa zmieňovať o úplne všetkom zaujímavom, čo sme v okolí vypátrali, skôr sa jedná o naše obľúbené miesta. Tak isto neuvádzam príliš veľa detailov, tie si každý nájde na nete.
                 

                   Hrady     -         hrady sú v našej rodine vôbec najobľúbenejším výletným cieľom, žiaľ, pomaly sa nám možné ciele vyčerpávajú, tak neviem, čo si počneme. V každom prípade je to vždy zážitok objavovať novú zrúcaninu, liepať sa po múroch, liezť do temných pivníc a rozprávať sa o pohnutých osudoch hradných obyvateľov. Milujem tieto výlety i preto, že to nie sú klasické túry, teda šlapanie od niekam niekam a nazad, ale túry, na konci ktorých vždy čaká nejaké prekvapenie, niečo čo možno hodinu dve skúmať. Keď navyše praje počasie, je to ozaj rozprávka. V okolí Košicoch je hneď niekoľko úplne najlepších zrúcanín ever.   1.      Turniansky hrad   2.      Hrad Slanec   3.      Kapušianský hrad   4.      Šarišský hrad   5.      Hrad Vinné   6.      Brekov   7.      Jasenov   8.      Zborov   Pekné hrady sú aj za hranicami v Maďarsku.       2. Rôzne atrakcie         -         Dukla - množstvo zbraní voľne vystavených okolo cesty, pamätník padlým. Tanky v Údolí smrti. Po ceste na Duklu odporúčam zastaviť sa vo Svidníku v múzeu, je tam vystavené všeličo možné týkajúce sa bojov. V areáli múzea je tiež vystavená ťažká bojová technika a lietadlo, deti môžu po všetkom liezť. V múzeu Vám premietnu film z päťdesiatych rokov o večnom priateľstve československej a sovietskej armády a vôbec, človek ma vo Svidníku celkovo pocit, že je niekde v roku 1970.  V tomto meste je ešte Skanzen ľudovej architektúry, netreba vynechať. Krátke video z Dukly:   http://www.vimeo.com/11969895   -         Opálová baňa pri Dubníku - uno fantastiko. Podľa sprievodcu je celková dĺžka chodieb baní asi 22 km. Prehliadka vedie na úseku asi 800 m dlhom, vyfasujete štýlové oblečenie (viď foto), baterku a ide sa. V bani je teplota asi 2 stupne, takže treba sa dobre obliecť. Od fundovaného sprievodcu sa dozviete množstvo zaujímavých informácií, uvidíte jazierko vody, ktoré obsahuje 10% kyseliny, môžete si priplatiť za hľadanie opálov na halde. Samotná cesta z Košíc vedie veľmi peknou krajinou (Olšany, Rozhanovce, Kecerovce - tu sa treba zastaviť na krátku prehliadku ruín kaštieľov).         -         Morské oko v pohorí Vihorlat - veľmi pekná prechádzka okolo jazera, ktoré je plné (fakt plné) rýb a neskutočné čisté.   -         Herľany - známy gejzír, treba si na internete nájsť info, kedy dotyčný tryská či eruptuje, či čo to robí.   -         Pomník zahynuvším slovenským vojakom KFOR v maďarskej obci Hejce. Jeden pomník je priamo v obci, pamätný kameň sa nachádza aj na mieste nehody v lese, je to asi 4 km dlhá túra.      -         ZOO v maďarskej Nyíregyháze - krásna ZOO, neporovnateľná s košickou. Cesta trvá z KE asi dve hodiny   -         Vykopávky v Nižnej Myšli - v poli za dedinou sú voľne prístupné miesta vykopávok starovekej kultúry. Treba si na nete čo to o tejto lokalite prečítať, deťom tak môžte zdanlivo nezaujímavé jamy v zemi zatraktívniť výkladom. Časť vykopávok je k videniu vo východoslovenskom múzeu.      -         Vodná nádrž Ružín - je možné prenajať si čln, v peknom počasí je to pre deti obrovský zážitok.          -         Ak už ste na Ružíne, neďaleko je vrch Sivec. My sme tam boli v zime, na dobytie vrcholu nám nestačili sily, ale po ceste sme objavili niekoľko zaujímavých jaskýň.   -         Plte v Červenom kláštore. Plavba plťou po Dunajci vedie nádhernou krajinou, krojovaný pltník podá výklad o miestnych zaujímavostiach. Pozor na miesto nástupu - plte štartujú iba z miest rôzne vzdialených od cieľa,  plavba potom môže trvať  hodinu ale aj tri a to už je pre deti neúnosné.   -         Múzeum Andyho Warhola v Medzilaborciach. Ak Andyho považujeme za svojho, tak je to s odstupom najslávnejší slovák všetkých čias. Pred jeho múzeom je nápaditá socha, múzeum samotné tiež veľmi pekné. Hoci pop-art ma nijak zvlášť neberie, treba v každom prípade vidieť.   -         Hájske vodopády -  sú to vlastne len také vodopádiky, ale dá sa k ním prísť až na dotyk, pre deti je táto interaktivita veľmi príťažlivá. V blízkosti je spomínaný Turniansky hrad.   http://www.mojevideo.sk/video/f505/hajske_vodopady.html       3. Múzeá, záhrady       -         Múzeí je v Košiciach len zopár, ale sú pri vhodnom podaní pre deti veľmi zaujímavé. Najmä Technické múzeum na Hlavnej malo úspech - s tetou sprevádzateľkou sme sa dohodli, že výklad nám nie je treba, takže sme brúsili po múzeu celkom sami, bola to taká dobrodružná výprava v štýle Indianu Džonsa. Za pozretie určite stojí i Východoslovenské múzeum s peknou zbierkou nerastov a hlavne s množstvom vypreparovaných zvierat. Zaujímavé sú i kosti mamuta a v budove oproti sú pekné expozície vykopávok, zbraní a všeličoho možného pre deti zaujímavého. Netreba tiež obísť košický zlatý poklad.   - múzeum letectva pri košickom letisku       -         Na takmer celodenný výlet vydá i návšteva múzea Solivar v Prešove. Je to v podstate zachovalá stará fabrika na ťažbu soli, treba vidieť v každom prípade.    -         Botanická záhrada - deti sa v lete dobre vyšalia vo vonkajšom areáli, máčajú sa v jazierkach, v potôčku a tak. Naposledy sme tam boli na výstave exotických motýľov, deti vytešené.   -         Samozrejme ZOO v Kavečanoch + atrakcie v podobe bobovej dráhy, trampolíny. Po ceste do Zoo treba odskočiť na vyhliadkovú vežu, pri ktorej sú aj zrúcaniny košického hradu.           Myslím že z uvedeného sa už voľajaký ten rodinný výlet dá naplánovať. Takže vypnúť televízor, naložiť deti do auta a davaj het.       P.S. Poteší ma, ak v diskusii uvediete nejaké ďalšie tipy, rád sa tam pozriem. Druhá časť tipov na výlety tu:   http://rastogallo.blog.sme.sk/c/205960/Vylety-v-okoli-Kosic-II.html     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Gallo 
                                        
                                            Aký bol Dobrý Festival?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Gallo 
                                        
                                            Čad a Slobodná Európa na Traktor Rock Feste 2012
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Gallo 
                                        
                                            Prešov + Košice = Poke Festival
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Gallo 
                                        
                                            Video: Finále MS v hokeji na košickom amfiteátri
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Gallo 
                                        
                                            Hudobná reportáž: Chiki Liki Tua a Sps get explode
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rasťo Gallo
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rasťo Gallo
            
         
        rastogallo.blog.sme.sk (rss)
         
                                     
     
        Tatranec žijúci v Košiciach, strojár, videoamatér.
http://www.youtube.com/user/TVMENGAART
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2901
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Poryvy
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako si to tie prasce americké dovoľujú!
                     
                                                         
                       Kto sa bojí rodiča číslo 2?
                     
                                                         
                       Otvorený list dekanovi PEVŠ k prednáške Štefana Harabina
                     
                                                         
                       terapeutický denník Michala Hvoreckého
                     
                                                         
                       Čierna biela technika
                     
                                                         
                       Prečo si ľudia na internete tak veľmi nerozumejú
                     
                                                         
                       Vo vlaku
                     
                                                         
                       Blbí Američania? A čo o svete vieme my?
                     
                                                         
                       Pochod za život, alebo keď je 70.000 priveľa a zároveň primálo
                     
                                                         
                       Paradise for people
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




