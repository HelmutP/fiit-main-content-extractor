
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Serbák
                                        &gt;
                Fotoreportáže
                     
                 Večerná Pisa 

        
            
                                    12.6.2010
            o
            6:03
                        (upravené
                12.6.2010
                o
                7:10)
                        |
            Karma článku:
                8.20
            |
            Prečítané 
            2095-krát
                    
         
     
         
             

                 
                    Do toskánskeho mesta Pisa sme sa presunuli vo štvrtok podvečer z "našej" farmy pri Altopascio po diaľnici za polhodinu. Zaparkovali sme na veľkom parkovisku v centre cca 200 metrov od Dómskeho námestia, kde sa nachádza svetoznáma šikmá veža. Môj prvý dojem z veže po vstupe na námestie cez bránu v hradbách bol ohromujúci. Tá majestátnosť a najmä šikmosť veže, hoci som ju už najmenej stokrát videl na obrázkoch a vo filmoch, naživo predčila moje očakávanie.
                 

                 Dómske námestie (volajú ho aj Piazza dei Miracoli - námestie zázrakov) je z dvoch strán obkolesené starými mestskými hradbami. Stojí tu Dóm, baptistérium a Šikmá veža so zvonicou. Vežu začali stavať v 12. storočí a jej základ tvorí kruhový kamenný prstenec, vysoký 3 metre o priemere 15 a pol metra. Po postavení 3. poschodia sa veža naklonila, preto jej výstavbu zastavili. O 100 rokov neskôr postavili ďalšie 3 poschodia, čím vznikla naklonená válcovitá veža s dverami na prízemí a galériami so stlporadím na 6 poschodiach. O ďalších 100 rokov na veži postavili zvonicu so 7 zvonami, naladenými na 7 rôznych tónov. Veža sa nakláňala kvôli mäkkému podložiu stále viac, až dosiahla odklon od zvislej osi 4 a pol metra. V roku 1993 bolo nainštalovaných do zeme na severnej strane veže 800 ton olovených dosiek ako protizávažie, čím sa veža stabilizovala.                     Na vežu sme sa bohužiaľ nedostali, púšťajú do nej len do ôsmej večer, prišli sme 5 minút po termíne. Prezreli sme si celé námestie a jeho detaily, potom sme sa išli navečerať do najbližšej pizérie s výhľadom na Dóm.                                 Dali sme si spolu 3 veľké pize rôzneho druhu po 8 eur a 1 morské potvory, po bratsky sme sa podelili a ochutnali zo všetkého. Smäd sme uhasili pivom po 5 a pol eura (ženy vínom) a vybrali sme sa na večernú prechádzku k rieke Arno.                              Bol teplý  príjemný večer, tak sme sa tam peknú chvíľu zdržali. Cestou na parkovisko sme sa ešte zastavili na Dómskom námestí. Bol som nemilo prekvapený, že slávna Šikma veža v Pise je veľmi slabo iluminovaná (nasvietená). V našej dedine na východnom Slovensku sú 3 kostoly a všetky sú iluminované.      Auto sme si vyzdvihli z parkoviska štvrťhodinu pred záverečnou (parkovisko zatvárajú o jedenástej večer). O polhodinku sme už boli v posteli a tešili sme sa na metropolu Toskánska Florenciu, na ktorú sme si vyčlenili celý nasledujúci deň. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Drevené kostolíky pod Duklou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Zaujímavosti zo 100-ročných novín
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Čo písali pred 100 rokmi
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Serbák 
                                        
                                            Nádherné a ľudoprázdne Tatry
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Serbák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Serbák
            
         
        serbak.blog.sme.sk (rss)
         
                                     
     
        Dôchodca s pestrou škálou záujmov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    232
                
                
                    Celková karma
                    
                                                8.50
                    
                
                
                    Priemerná čítanosť
                    2351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Turistika
                        
                     
                                     
                        
                            Cykloturistika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotoreportáže
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ako to vidím ja
                        
                     
                                     
                        
                            Storočné noviny
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Martina Rúčková
                                     
                                                                             
                                            Alexandra Mamová
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                             
                                            Michal Májek
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Ivan Rias
                                     
                                                                             
                                            Martin Štunda
                                     
                                                                             
                                            Dominika Sakmárová
                                     
                                                                             
                                            Jozef Kuric
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľubomír Nemeš
                                     
                                                                             
                                            Zuzana Minarovičová
                                     
                                                                             
                                            Janette Maziniova
                                     
                                                                             
                                            Palo Lajčiak
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Branislav Skokan
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            geni.sk všetko pre váš rodokmeň
                                     
                                                                             
                                            stránka obce Úbrež
                                     
                                                                             
                                            fotogaléria Drahoša Zajíčka
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zimný Šíp.
                     
                                                         
                       Pamätník čs. armády s vojnovým cintorínom na Dukle
                     
                                                         
                       Drevené kostolíky pod Duklou
                     
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Putovanie po slovenských Kalváriách (194) - Klokočov
                     
                                                         
                       Demokracia
                     
                                                         
                       Vianoce na dedine a vianočný jarmok
                     
                                                         
                       Zaujímavosti zo 100-ročných novín
                     
                                                         
                       Daniel
                     
                                                         
                       Od absolútnej nezávislosti k absolútnej straníckosti
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




