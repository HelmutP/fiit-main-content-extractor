

   
   
  
             Tu je niekoľko klenotov z dokumentu: 
             Steve Burg (Ex-Jew for Jesus) X Bill Maher 
             S. B.: (hovorí  o zázrakoch) Bol som na večierku ... a požiadal som chlapíka o pohár vody. A on povedal: „Vieš čo, tu máš pohár, vystrč ruku z okna a pomodli sa za dážď." Nepáčilo sa mi jeho správanie, povedal som dobre, vystrčil som ruku z okna a začalo pršať; pršať tak silno, že ľudia nemohli odísť z večierka. Pre mňa je to zázrak. Nemusíte tomu veriť. 
             Maher: Je to dosť chabé. ... Ak by pršali žaby, povedal by som, že na tom niečo je. Ale ono predsa prší. A potom prestane pršať. 
             S. B.: Kedy ste si naposledy priali, aby pršalo a ono začalo pršať do 10 sekúnd? 
             Maher : Neviem, nikdy som si neprial dážď. Ale, ak by som si veľmi želal, aby pršalo a začalo by pršať, nemyslel by som si, že je to preto, že som si to želal, ale preto, že jednoducho niekedy prší. 
             
             S. B.: Boh nemá toľko práce, aby nemohol človeka vypočuť, keď si s ním chce človek poklábosiť. 
             Maher:  Ak Santa Claus dokáže obraziť každý dom na svete... 
             S. B.: Neverím v Santu... 
             Maher: Samozrejme, že nie. Je to smiešne. 
             S. B.: Áno. 
             Maher: Jeden chlapík, ktorý lieta po celom svete a hádže dolu komínom darčeky ... to je smiešne. Ale jeden chlapík, ktorý počuje všetkých, ako k nemu niečo naraz šepkajú, to je iné kafe. 
             
             Maher:  Tento muž (Jonáš) žil v rybe po tri dni? 
             S. B.:  Zázrakom, áno. 
             Maher: Steve, Steve, Steve... 
             S. B.: Neveríte v zázraky, to neznamená, že zázraky neexistujú. 
             Maher: Samozrejme, že nie. Nemám 10 rokov. Človek nemôže žiť v rybe. 
   
             The Holy Land Experience, Orlando, Florida, predstaviteľ Ježíša v miestnom dramatickom predstavení: 
             Maher: Čo bol holocaust: Prečo to bolo dobré? 
             Predstaviteľ Ježíša: Viete, je to súčasťou Božiaho plánu. 
             Maher: Naozaj? A to by ste si mysleli, ak by ste boli jedným z tých, koho strčili do pece? 
             Predstaviteľ Ježíša: Viete čo, je to, ako by ste chceli tomu mravcovi vysvetliť, ako funguje televízia. Božie zámery sú oveľa vyššie ako tie naše, nie je spôsob, ako im porozumieť. 
   
             Mark Pryor, demokratický senátor za štát Arkansas 
             Senátor:  V podstate, vražda je nezákonná v každej krajine na svete. 
             Maher: Ale neprišli by sme na to aj bez náboženstva? Nemyslíte si, že by si ľudia nepovedali: „Viete čo, nezabíjajme sa a neberme si navzájom naše veci?" 
             Senátor: Neviem. ... Myslíte, že máme v DNA zakódované, že zabíjanie inej osoby je zlé? Nemyslím. 
             Maher: Naozaj? Potrebujete Boha, aby rozhodol, aby sme sa nezabíjali? 
             Senátor: Pozrite sa na primitívne kultúry. Tie boli neustále vo vojnovom stave. 
             
             Je vidieť, že obyčajný sedliacky rozum (common sense) a skúsenosť poľahky dostávajú vieru do úzkych. Vieroučné správy sú v problémoch pri jednoduchých otázkach a napr. pri snahe o vysvetlenie Božích zámerov ústia do ospravedlňovania holocaustu a pod. Môj skromný názor na vieru je ten, že by sme ju mali oholiť Occamovou britvou, presne ako Platónove fúzy. Pozrime sa napr. na jeden africký príbeh o vzniku sveta: Na počiatku všetkého bolo pusté, prázdne nebo, bez akýchkoľvek hviezd. Okrem neba bol veľký strom, ktorý sa týčil vo vzduchu. Vzduch mu bol matkou a živiteľom. Na počiatku všetkého bol aj vietor, ktorý sa preháňal sem-tam. Strom bol prameňom života pre bielych mravcov. Všetko opísané bolo ovládané nehmotnou a neviditeľnou silou, Slovom. Raz sa stalo, že vietor odfúkol jednu z vetví stromu a spolu s ňou sa do hĺbky zrútili aj všetky mravce na nej. Keď mravcom došla potrava, a teda listy na vetvičke, začali sa živiť vlastnými výkalmi a sami vytvárali ďalšie a ďalšie. Hromada výkalov rástla, až nadobudla veľkosti pôvodného stromu. Mravcom výkaly zachutili natoľko, že začali jesť len ne. Tak, ako jedli viac a viac výkalov, viac a viac ich aj produkovali. Výkalov bolo nakoniec toľko, že z nich vznikol obrovský guľatý svet. Atď, atď... (Zdroj: O želvách, lidech a kamenech: Mýty, legendy a pohádky černé Afriky. 1. vyd.  Praha 1999, s. 7-12). 
             Považujeme tento mýtus kmeňa Wapangwov z Tanzánie za pravdivú výpoveď o tom, ako vznikol svet? Nie? Prečo ale považujeme za pravdivú tú rozprávku o šiestich stvoriteľských dňoch, či hovoriacom hadovi, kráčaní po vode, premenách vody na víno, 3-dňovom pobyte Jonáša v rybe,... mi nie je jasné. Ja sa domnievam, že nie je žiaden kvalitatívny rozdiel medzi správou o vzniku sveta z mravčích výkalov kmeňa Wapangwov a správou o vzniku sveta, ako nám ju podáva Biblia. Obidva sa jednoducho mýlia. Prečo teda nečítať Bibliu ako rozprávku? Kde bolo tam bolo, za siedmimi horami a siedmimi morami, žil raz jeden ujo, ktorý žil v rybičke, za kamoša mal uja, ktorý chodil za kamarátmi po vode, teta Eva, ktorá vznikla z rebra jej eňo ňuňo kamoša Adama, keď sa nudila, chodila na pokec s hadom,... a všetci žili šťastne, až jedného dňa ujo Ježiško šiel na tretí deň po smrti do nebíčka. A rozprávky je koniec... 
             Možno je ťažké žiť vo svete, na ktorý nedohliada žiadna dobrotivá božská bytosť, vo svete, ktorý nikam nespeje, ktorý nemá žiaden zámer ani účel a v ktorom sa nedejú žiadne zázraky,... Stále si ale myslím, že je to oveľa poctivejšie, pretože je to ocenenie sveta takého, aký naozaj je. Jednoducho, tak, ako existujú jednorožci iba v rozprávkach, tak sa dá chodiť po vode, žiť v rybe, stvoriť svet za 6 dní božskou bytosťou,... iba v rozprávkach. Maher to vo svojom sranda-dokumente odhaľuje. 

