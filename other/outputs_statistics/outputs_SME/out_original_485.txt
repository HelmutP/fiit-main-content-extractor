
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Cimrák
                                        &gt;
                Nezaradené
                     
                 Štátna dotácia v Holandsku na sexuálne služby pre postihnutých 

        
            
                                    21.6.2005
            o
            10:58
                        |
            Karma článku:
                9.27
            |
            Prečítané 
            3299-krát
                    
         
     
         
             

                 
                    Nedávno som sa dopočul, že v Belgicku sa v parlamente diskutuje o finančnej podpore pre telesne a duševne postihnutých, ktorá by slúžila na pokrytie výdavkov na spoločníka. Takýto spoločník, resp. spoločníčka, príde do vášho bytu, strávi s vami pár hodín a ako sa návšteva ďalej vyvinie, záleží len na vás. Informácia, že by štát finančne podporoval takúto aktivitu, ma zaujala a chcel som sa presvedčiť, či to nie je len fáma.
                 

                Nezistil som nakoniec, či parlament o tejto téme naozaj rokuje, ale je pravdou, že niektoré okresné úrady skutočne poskytujú vyššie spomínanú finančnú pomoc. Ide o zaplatenie služieb tzv. dobrovoľníkov pracujúcich pre združenie SAR (voľný preklad - Združenie pre Pomoc Alternatívnym Vzťahom). Činnosť tejto organizácie je pozoruhodná.    
 SAR - podporuje alternatívne sexuálne vzťahy  SAR je združenie, ktoré podporuje ľudí s telesným alebo duševným postihnutím, pri hľadaní alternatívy sexuálneho vzťahu, ktorý sa v tejto skupine nadväzuje len veľmi ťažko. SAR sa snaží aj o informovanie obyvateľstva o problémoch v sexuálnom živote postihnutých. Pôsobí v Holandsku a v hraničných oblastiach Belgicka a Nemecka.   
 Ako to funguje?  Klient, ako aj jeho zástupca, môže zatelefonovať na kontaktné telefónne číslo, kde sa dozvie viac podrobností, ako SAR funguje. Počas rozhovoru požiadajú klienta o niektoré podrobnosti, okrem iného druh a mieru jeho postihnutia.  Tieto podrobnosti sú predložené jednému z dobrovoľníkov. Ten potom nadviaže kontakt s klientom a takto sa môžu dohodnúť na podrobnostiach ich stretnutia. 
  Priebeh stretnutia sa čo najviac prispôsobí možnostiam a schopnostiam klienta. Návšteva trvá v priemere hodinu a pol. Za jednu návštevu sa účtuje 73 euro. Pokiaľ je vzdialenosť medzi klientom a dobrovoľníkom väčšia ako 200km, účtuje sa zvýšená tarifa 86 euro. Je možné uskutočniť aj tzv. poznávaciu návštevu, za ktorú sa účtuje 23,50 euro. 
   
 Pár podrobností  Združenie SAR je nezisková organizácia a zamestnáva len niekoľko polo-dobrovoľníkov v riadení organizácie a telefonistu. Okrem toho spolupracuje s asi 15-timi ženami a mužmi, ktorí pracujú ako dobrovoľníci. Títo ľudia majú už väčšinou skúsenosti s opaterou telesne alebo duševne postihnutých ľudí. Podstupujú výberové konanie, ktorého cieľom je zabezpečiť spoľahlivosť a odbornosť.    
 Reakcie na činnosť SAR  Od založenia SAR v októbri 1982 sú skúsenosti s jeho činnosťou viac než pozitívne. Ženy ako aj muži využívajúci služby SAR sa cítia silnejší a sebavedomejší. Cítia sa istejšie pri nadväzovaní nových sociálnych kontaktov. Aj dobrovoľníci konštatujú, že návštevy majú svoj význam a nejde pri nich len o sexuálne uspokojenie. Vytvárajú sa týmto nové vzťahy.    
 Na záver  Namiesto hodnotenia činnosti SAR pridám len niekoľko otázok. Na niektoré z nich vlastne ani nemám ujasnenú odpoveď.     Je v poriadku, že štát poskytuje príspevky na zaplatenie služieb SAR?  
 Prečo to tí dobrovoľníci robia?  
 Je to prostitúcia alebo sociálna opatera postihnutých?  
 Ak by ste mali postihnuté dieťa, s minimálnou šancou nájsť si partnera, informovali ste ho alebo odporučili by ste mu služby SAR?  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (68)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             
 
  

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Cimrák 
                                        
                                            Bol som v Tatrách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Cimrák 
                                        
                                            O Mexiku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Cimrák 
                                        
                                            Transplantácia obličiek
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Cimrák 
                                        
                                            Miesto, kde chcem byť pochovaný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Cimrák 
                                        
                                            Kde stál Berlínsky múr?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Cimrák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Cimrák
            
         
        cimrak.blog.sme.sk (rss)
         
                        VIP
                             
     
        PhD študent momentálne pracujúci v Belgicku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    21
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2989
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Mexiko
                        
                     
                                     
                        
                            Belgicko
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marián Minárik
                                     
                                                                             
                                            Zuzana Karasová
                                     
                                                                             
                                            Miriam Novanská
                                     
                                                                             
                                            Peter Petiar Lachky
                                     
                                                                             
                                            Marianna Varjanová
                                     
                                                                             
                                            Michal Kostic
                                     
                                                                             
                                            Andrea Uhljarová
                                     
                                                                             
                                            Jolana Čuláková
                                     
                                                                             
                                            Han(k)a Tichá
                                     
                                                                             
                                            Slavo Nemsak
                                     
                                                                             
                                            Peter Jacko
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




