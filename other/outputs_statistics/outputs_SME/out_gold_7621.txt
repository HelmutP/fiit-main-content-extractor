

  
 Hneď potom, ako sa pokúsili vyburcovať ich k výkonu priamymi otázkami k projektu, ktorý som mala na starosti, však pochopili, že moje mozgové bunky utrpeli cez víkend priam učebnicovú porážku a deadline prezentácie evidentne nedodržím. Tu už nepomohli ani psie oči. 
   
 Utorok. 
 Ráno som sa postavila na váhu. Tento deň už nezachráni nič. A keby aj, výpis z účtu v mojej schránke tomu úspešne zabránil. 
   
 Streda. 
 Dnes mám druhý deadline na prezentáciu. Mám ciťák, že aj posledný. Celú noc som sa snažila ako fretka, ale keď mi o pol štvrtej ráno zamrzol comp aj s nesejvnutým projektom otvorila som si fľašu červeného a napísala šéfovi SMS, že mám črevnú chrípku. Keď sa mi cez obed comp konečne nakopol a ja som si skontrolovala maily, zistila som, že aj výpoveď. Samozrejme, tento mail som našla ako posledný, teda potom, ako som vybavila všetkých 15 ďalších, urobila a poslala 4 tabuľky a 2 interné minireporty. No nič, odídem s čistým štítom. 
   
 Štvrtok. 
 Bolí ma hlava. Moja vymyslená črevná chrípka sa v noci zákerne premenila na celkom reálnu opicu. Mám teda v pláne stráviť tento deň v posteli, keď v tom mi začne hulákať mobil. Zdvihnem a uškriekaný hlas personálnej riaditeľky z teraz už bývalej firmy opäť reaktivuje toho trpaslíka s krompáčom v mojej hlave, ktorého som len pred chvíľou úspešne umlčala ružovou tabletkou. Ževraj mám prísť ihneď vrátiť notebook, mobil, kľúče a čojaviemčo ešte. Seriem na ňu. 
   
 Piatok. 
 Vybrala som sa teda do firmy. V snahe využiť posledné kvapky výhod si lístok na MHD objednávam cez firemný mobil. Cestujúc sa z neho snažím vymazať všetky intímne, súkromné a inak poburujúce záležitosti a samozrejme omylom vymazávam aj potvrdzujúci SMS lístok! Revízor mi ako inak neverí...  
 Pozitívum dňa – hádku s predavačkou v potravinách vyhrávam a syrokrém po záruke mi nakoniec z nákupu stornovala. 
   
 Sobota. 
 Ráno o siedmej ma budí horlivý vŕtajúci sused. Keď konečne prestane, nahradí ho jeho manželka klepaním rezňov. Neva, nenechám si predsa pokaziť víkendový  deň a po takmer hodinovej kúpeli ladne schádzam dolu verejným schodiskom. Tu ma odchytí klepkajúca suseda a ťahá ma k nej do kúpelne ukázať „tie mokré fľaky“. Neviem, čo bola väčšia rana – vidieť jej obstarožného manžela iba v trenkách alebo zistenie, že z mojich kúpeľňových trubiek fakt zateká... 
 Našťastie večerný babský záťah do mesta začína dobre, telefón a doklady si zabúdam až nad ránom, v taxíku na ceste domov.  
   
 Nedeľa. 
 Keby som nebola single skončil by sa tento týždeň určite kopačkami! Budím sa s jednoznačným pocitom - ako dobre, že nemám partnera, či nebodaj manžela! A tak s presvedčením, že všetko zlé ma už postretlo a od pondelka začínam novú etapu života zostávam pre dnešok opäť v posteli. Pre istotu. 
   
 Večer si v hlave premietam môj „úspešný“ týždeň. Nie som však zronená, život mám pevne vo svojich rukách a verím, že od zajtra nastupuje biely pruh. Veď život je ako zebra – čierny pruh sa strieda s bielym – aj otec mi to kedysi hovoril.  
 Ráno cestou z Úradu práce, s boľavými nohami po hodinách státia v šore, vyhádaná s Pani Dôležitou, premočená od dažďa a blata spod kolies „ohľaduplných“ vodičov, hladná a s pocitom absolútnej neschopnosti a samoty som si spomenula, že to malo dovetok. Život je ako zebra, striedajú sa biele a čierne pruhy. Len raz za čas po tom čiernom nepríde hneď biely ale prekvapí riť....  
   

   

