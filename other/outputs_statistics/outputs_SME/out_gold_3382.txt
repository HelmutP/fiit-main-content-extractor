

 Podľa denníka Pravda došlo pri zverejnení tohto článku s exkluzívnymi informáciami k nedorozumeniu a k chybe v komunikácii v redakcii. Je skvelé, keď sa Pravda dokáža ze chybu čestne ospravedlniť. To mi navyše umožňuje tento denník aj jej redaktora Lukáša Milana pochváliť za prácu a uchopenie spomínanej kauzy. Ostatné oslovené médiá totiž o ňu neprejavili veľký záujem. Preto ma dvojnásobne mrzí, že som sa proti chybe denníka musel verejne ohradiť. 
 Mrzí ma však aj to, že Pravda pripisuje chybu svojmu redaktorovi. Z viacerých zdrojov z redakcie viem, že tento mal naopak od začiatku záujem ma ako spoluautora uviesť. Ide tiež o novinára, ktorý svojou prácou preukázal novinárske kvality (páči sa mi jeho práca) a nie je odkázaný na zamlčanie spolupráce. Keď v redakcii vznikne problém v komunikácii, ktorý sa ťažko identifikuje, začnú sa hľadať vinníci. Preto sa prihováram za úplné uzatvorenie celého prípadu bez ďalšieho riešenia. Inak budem mať z ospravedlnenia Pravdy tak trochu trpký pocit Pyrrhovho víťazstva. 
  
  

