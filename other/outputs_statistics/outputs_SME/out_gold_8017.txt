

 Ak si mám predstaviť nekonečno, predstavím si železničnú trať. Jeden pražec za druhým, krok za krokom až tam, kde sa stretajú koľaje, tam, kde sa len náznakom vlnia v tráve, kde medzi nimi rastie kvietie, žltý rozchodník a jasnočervený vlčí mak pri násypoch tratí a kde vlak chodí len raz za čas, aby sa dalo ísť po pražcoch malými krokmi jeden za druhým... Krajina lesov a skál a viaduktov a chatiek a táborísk... 
 Aj tá hudba tak znie, často v nej cítiť rytmus koľají a ciest. Jednoduchá, čistá, priama... 
 Prvé akordy, mohla som mať vtedy tak dvanásť a gitara sa mi nedala úplne naladiť už od výroby. Neohrabaný rytmus, hudobný hluch silne prevažujúci nad sluchom a magické C, G, D a A-moll a ešte pár tých ľahkých akordov, na ktorých sa dal ako - tak uhrať celkom solídny repertoár. Dajána, Montgomery, Buráky... Pesničky v začiatkoch tak ohrané, že teraz na ne prichádza čas riadne po polnoci. Z recesie a zaspomíať si. 
 Country je aj pre decká. 
 Na folk treba vyrásť. Na folk sa vyrastá vyrastaním. 
 Jasné, takmer všetky Nedvědovky a veľký kus Nohavicu sa dá uhrať na oných magických niekoľkých akordoch. Trochu skúsenejší sme prešli na Kamelot, tí ozaj báječní gitaristi pridali širokorozchodné akordy cez päť pražcov a sóla a všetko to, čo robí našu muziku jedinečnou, prečo majú folkové pesničky toľko variácii, že si ľudia niekedy nepamätajú, ako to vlastne znelo pôvodne. 
 Pár kamarátov, gitara, dve, tri... 
 Nadaní hudobníci, na ktorých sa vždycky tešíš až sa stretnete, ktorým by jeden aj spacák odniesol aj jedlo navaril, len aby prišli a hrali... Kopa ľudí a kopa hlasov tých nadaných aj tých menej, niekde na chate, pri ohni, alebo proste len tak, že je chuť. Pre mňa ľudia, pri ktorých som pochopila viachlas. Potom sa zrazu nájdem, že to nie je to ono spievať sám, že ten zvuk sa niekam stratil, keď tu nie je nikto, aby sa naše hlasy splietli a zneli. To pravô orechovô, hudobná extáza a tanec a vyznania, súznenie silné ako milovanie. 
 To do tých pesničiek asi zašili aj ich autori, cesty a táborák a kamarátov... Nekonečno leta a modrej oblohy a vzrušenie z rozkvitnutých vlčích makov pri cestách a tajomný pulz túlavých topánok a ľudské žiale a radosti lásky. 
 Najviac to nie je to ani tak o tom, o čom sa v tých pesničkách spieva, je to o tej nostalgii, o tom, že sme s folkom vyrástli, o textoch, ktoré zrazu vo svete zahltených veľkých dospelákov dávajú zmysel. V momente, keď pre tých pár tónov takmer stojí čas, keď sa z diaľky zídu starí kamaráti. Kamaráti, ktorí vyrastali s folkom. Tisíce spomienok, ktoré splynuli a po ktorých ostal už len pocit. Vracia sa s kažým stisnutím strún až to polí konce prstov a pri dohasínajúcom ohni obchádza chlad. 
 Autorov je veľa, aj keď tých slovenských poredšie a s trochu iným zvukom ako český folk. Spevník sa mení, ale tie najtverdšie folkové kúsky zostávajú. 
 Môžeš počúvať akú hudbu chceš, ale ak si hlboko vnútri tvrdý folkáč, vždy sa k nám vrátiš... 
   
  

