
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            milan šupa
                                        &gt;
                Nezaradené
                     
                 Vratké piliere kresťanstva! 

        
            
                                    20.5.2010
            o
            18:23
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            214-krát
                    
         
     
         
             

                 
                       Čo iné, ako práve Desatoro prikázaní možno považovať za základné piliere kresťanskej viery? Akou pevnou však môže byť mohutná stavba kresťanstva, ak sa základné piliere, na ktorých stojí a o ktoré sa opiera povážlivo kývajú, rúcajú a doslova rozsýpajú? Neveríte? Len sa teda v tejto súvislosti bližšie pozrime na dodržiavanie druhého prikázania, ktoré znie: „Nevezmeš meno svojho Pána nadarmo!“
                 

                            Čo iné, ako práve Desatoro prikázaní možno považovať za základné piliere kresťanskej viery? Akou pevnou však môže byť mohutná stavba kresťanstva, ak sa základné piliere, na ktorých stojí a o ktoré sa opiera povážlivo kývajú, rúcajú a doslova rozsýpajú? Neveríte? Len sa teda v tejto súvislosti bližšie pozrime na dodržiavanie druhého prikázania, ktoré znie: „Nevezmeš meno svojho Pána nadarmo!“          Pojem Boh je jedným z najvyšších a najsvätejších pojmov vo stvorení! Je to najposvätnejšie slovo, ktoré má byť vyslovované iba v stave najväčšej citovej vrúcnosti, akej je človek schopný a to či už vo chvíľach bolesti, alebo radosti. Každé jeho bezmyšlienkovité a povrchné vyslovenie je jeho strhnutím, zneuctením a znížením!           Žiaľ, v súčasnosti to došlo až tak ďaleko, že toto vznešené slovo sa stalo iba obyčajným zvratom v každodennej hovorovej reči. Slovným zvratom, vyslovovaným ľahkomyseľne a aj niekoľko desiatok krát za deň a to i ľuďmi, ktorí sami seba považujú za kresťanov.          Avšak práve v tomto prípade by malo dvojnásobne platiť ono známe, že menej je niekedy viac. Ľudia vo všeobecnosti a dokonca ani horliví kresťania nie sú si žiaľ vôbec vedomí toho, ako ťažko sa každodenne previňujú voči Stvoriteľovi práve prestupovaním druhého prikázania, požadujúceho nevyhnutnosť preukazovania úcty voči jeho menu.          Pre porovnanie si uveďme príklad z dávnej histórie židovského národa, v ktorom bola úcta k tomuto najposvätnejšiemu pojmu na oveľa vyššej úrovni, ako je tomu dnes. Pojem, vyjadrujúci meno Najvyššieho si židia z posvätnej úcty k nemu nedovolili v jeho skutočnom znení ani len napísať. Písali ho tak, že vynechávali samohlásky.          Toto meno smelo byť vyslovené najvyšším veľkňazom iba raz v roku a to v deň najvýznamnejšieho sviatku. V tej chvíli sa takmer celý národ, zhromaždený v chráme a v jeho nádvoriach vrhol na kolená.           Dnes je to už však žiaľ, iba prázdny slovný zvrat, pričom však mnohých kresťanov nemožno obviniť z nejakých zlých úmyslov. Väčšina z nich tak jedná iba z nevedomosti, povrchnosti a z osobnej, nedostatočnej hĺbky pochopenia významu a vážnosti druhého prikázania. Neznalosť zákona však neospravedlňuje! Ako v pozemskom zákonodarstve, tak i v prípade Desatora, ktoré by mal predsa každý kresťan poznať, chápať a dodržiavať!           Avšak práve preto, že u mnohých ľudí dochádza k porušovaniu druhého prikázania naozaj bez zlého chcenia, práve preto je potrebné ich na to upozorniť a stále upozorňovať, aby si už konečne začali na to dávať väčší pozor a aby si na svoju dušu prestali nakladať stále nové a nové bremená ťažkej viny.           Celá vec je naozaj veľmi vážna a nemožno ju zľahčovať!   Veď ak si uvedomíme, že jednotlivé prikázania Desatora sú usporiadané v poradí podľa dôležitosti, je prikázanie o nevyhnutnosti prejavovania úcty k menu Najvyššieho  druhým v poradí. A určite nie náhodou!          Skúsme sa teraz zamyslieť nad nasledovným príbehom: Na okraji dediny žil jeden figliar, ktorý si stále robil z ľudí iba žarty. Raz v noci začal len tak zo zábavy kričať: „Horí! Horí“ Na jeho krik sa zbehla celá dedina s úmyslom pomôcť mu v jeho nešťastí. On sa však na tom iba zabával.          No a považujúc to za naozaj dobrý vtip, urobil to po nejakom čase ešte raz. Opäť sa zhŕklo dosť ľudí, ktorí pribehli bez premýšľania a s ochotou pomôcť. Nebolo ich už tak veľa, ako pred tým, avšak všetci vyšli opäť iba na posmech.           Ako áno, ako nie, jedného dňa sa nášmu figliarovi naozaj chytil horieť dom. Najskôr sa požiar snažil uhasiť sám, ale keď videl, že je to nad jeho sily, začal zúfalo volať: „Horí! Horí!“          A už asi všetci tušíme, ako to dopadlo. Nikto mu neprišiel na pomoc a jeho dom i všetko čo bolo v ňom zhorelo.           O čom nám hovorí tento krátky príbeh? No predsa o tom, že keď nesprávnym a falošným spôsobom zneužívame nejaký pojem, nejaké slovo, musí nám to skôr, alebo neskôr zákonite privodiť škodu.           Ak teda zneužívame a v neuveriteľnej povrchnosti takmer každý deň niekoľko desiatok krát bezmyšlienkovite vyslovujeme jeden z absolútne najvyšších pojmov, aké vôbec jestvujú – posvätný pojem Boh, takáto ľahkomyseľnosť musí mať pre nás priam katastrofálne dôsledky. Sami sa tým totiž odrezávame od nesmiernej pomoci, podpory a posily, ktorej by sa nám muselo dostať vždy vtedy, keby sme vyslovili toto slovo s patričnou úctou, vážnosťou. To znamená veľmi uvážlivo, čiže iba vo chvíľach, keď sa je v hlbokej úcte k tomuto pojmu schopná plne zachvievať celá naša duša i všetko naše cítenie. Nech už sú to chvíle radosti a vďačnosti, alebo bolesti a prosby o pomoc.           Stráňme sa teda od tejto chvíle akéhokoľvek povrchného zneužívania toho najvyššieho a najsvätejšieho pojmu, aký vôbec jestvuje! Veď už celé stáročia a jedine ku nášmu duchovnému prospechu sa nás na to snaží upozorniť druhé prikázanie. V žiadnom prípade to nezľahčujme! Lebo napokon to, čo nám mohlo a malo priniesť nesmierny úžitok, pomoc a ochranu, práve to nám pri našej povrchnosti a bezmyšlienkovitosti prinesie nakoniec nesmiernu škodu. A je jedno, či sme kresťania, alebo ateisti! Ani ateistov totiž, presne tak, ako je tomu v pozemskom zákonodárstve, ich neznalosť Zákonov Božích v žiadnom prípade neospravedlňuje!        M.Š. spolupracovník časopisu „Pre Slovensko“   http://www.pre-slovensko.sk/     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        milan šupa 
                                        
                                            Spravodlivosť existuje!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        milan šupa 
                                        
                                            Duchovné poučenie z komunálnych volieb
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        milan šupa 
                                        
                                            Vzťah cirkevných vieroúk k absolútnej Pravde
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        milan šupa 
                                        
                                            Úvaha o poslednom súde
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        milan šupa 
                                        
                                            Kliatba ustavičného zahmlievania Pravdy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: milan šupa
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                milan šupa
            
         
        supa.blog.sme.sk (rss)
         
                                     
     
        Som človek, ktorému nie je ľahostajné všeobecné nepochopenie pravého zmyslu ľudského života na zemi a z tohto nepochopenia prameniace zlyhávanie osobných, ako aj celospoločenských vzťahov.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    351
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    265
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




