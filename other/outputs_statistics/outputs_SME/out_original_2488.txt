
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ľubomír Galko
                                        &gt;
                Nezaradené
                     
                 Špeciálny darček k MDŽ od SaS 

        
            
                                    8.3.2010
            o
            13:48
                        (upravené
                9.3.2010
                o
                1:52)
                        |
            Karma článku:
                14.40
            |
            Prečítané 
            6255-krát
                    
         
     
         
             

                 
                    Impulz k napísaniu tohoto blogu som dostal z konkrétneho príspevku od jedného diskutéra pod článkom na SME, v ktorom SaS predstavila svoju kandidátku do tohtoročných júnových volieb do NR SR. Uviedol svoju úvahu, že ženy sa do SaS "nejako nehrnú". Nuž, nie vždy kvantita odzrkadľuje kvalitu. Napriek tomu si dovolím tvrdiť, že to s nami nie je až také zlé, práve naopak.
                 

                 Premiér Fico aj so svojou družinou v týchto dňoch, samozrejme, opätovne a po koľký krát, za naše spoločné peniaze, obieha Slovensko a obdarúva ženy červenou ružou. Čítal som, že sem - tam síce povädnutou, ale to asi nevadí, v tej rýchlosti sa to zvyčajne stratí. Popri tom ich obsťastňuje trápnymi erotickými vtipmi, čím síce poburuje ženy ďalšie ale to mu zrejme nevadí tiež.   My v SaS máme obmedzený rozpočet, ako nová strana nemáme ani príspevky zo štátneho rozpočtu za voličské hlasy. Nemáme ani nakradnuté zo štátnej kasy z nejakých unikátnych nástenkových tendrov, predaja emisií, predražených mýt a podobných záležitostí a ani z tej štátnej kasy nehodláme v budúcnosti kradnúť.   Tak by som si dovolil symbolicky venovať v tento deň taký netradičný darček k MDŽ všetkým ženám (ale vlastne aj mužom). Nie sú to (zvädnuté) ruže, sú to ľalie. A sú to ľalie v krásnom rozkvete.       Je to päť skvelých a múdrych žien na našej kandidátke. Žien s čistou minulosťou a pevne verím, že aj s čistou budúcnosťou. Som presvedčený, že ani jedna z nich nebude :   - tlačiť falošné letáky a uverejňovať inzeráty podnecujúce národnostné nepokoje   - parazitovať na účelovo založených sociálnych podnikoch   - kryť poslancov, čo budú mlátiť Rómov a brániť ich trestnému stíhaniu   - mať predstavu, že ona nemusí pracovať, veď jej poslancovanie je predsa "poslanie"   - diletantskými rozhodnutiami pripravovať mamičky na materskej o možnosť privyrobiť si   - tváriť sa, že je bohyňa a nad ňu proste niet       Naopak:   Jana Kiššová - generálna managerka SaS - je tá, čo prejavila svoju osobnú statočnosť, keď ako členka ústrednej volebnej komisie nedovolila zamiesť kauzu vynášania volebných lístkov vo voľbách do VÚC, nedala sa zlomiť  a odmietla podpísať zápisnicu.   Lucia Nicholsonová ako bývalá novinárka si trúfla prežiť určitý čas v prostredí rómskych osád a je rozhodnutá zúročiť tieto skúsenosti ako timlíderka pri riešení problémov v oblasti sociálne odkázaných spoločenstiev.   Natália Blahová, odborne zdatná, láskavá a citlivá občianska aktivistka - a ďalšia naša tímlíderka. Asi ju v internetových kruhoch veľmi predstavovať netreba. Stačí si pozrieť jej publikovanie na webe. Sociálna oblasť je v neskutočne dobrých rukách.   Zuzana Aštáryová, pracovitá a entuziazmom nabitá okresná reprezentantka SaS pre Trenčín dokázala tú chiméru Trenčína ako absolútnej prevahy a bašty strany majiteľa "iba malej korzičky a tých dvoch ďalších" rozptýliť a získať z radov sklamaných bývalých priaznivcov toho zlodejského spolku neuveriteľné množstvo ľudí, ochotných popasovať sa v boji za lepšie Slovensko.   A Kristína Hájková  - naš mladý "benjamínek" - okresná reprezentantka pre Bratislavu V a zároveň aktívna konzultantka vo viacerých odborných tímoch má toho už dnes vo svojom mladom veku v hlave omnoho viac ako desať Zmajkovičových alebo Vaľových dohromady. Stačí si prečítať jej otvorený list petržalskému starostovi Ftáčnikovi pri voľbách do VÚC (v ktorých mimochodom získala 1.371 hlasov) alebo článok o predmanželských zmluvách.   Strana SaS nie je stranou na jedno použitie - na jedno volebné obdobie. Aj tieto naše kandidátky o tom svedčia. Som presvedčený, že o všetkých a aj mnohých ďalších, ktoré tu z priestorových dôvodov predstaviť nie je možné, budeme ešte mnoho a v dobrom počuť. O niektorých už teraz po júnových voľbách a o niektorých trebárs po 5,10, či 15 rokoch.   A aby bolo úplne jasné .....   Tak tieto (aspoň niektoré z nich), v to pevne verím, sú na odchode do zabudnutia :           A.Belousovová          J.Vaľová           R.Zmajkovičová    D.Gabániová       V.Tomanová       A tieto sú pripravené sa čestne uchádzať o priazeň voličov :               Jana                      Lucia                 Natália               Zuzana                Kristína       Tak milé kandidátky, ženy, dámy, slečny, dievčence (každá si doplňte, čo sa Vám hodí) zo SaS, pripravte sa na predvolebný boj. Som hrdý na nášu skvelú útočnú päťku. Ste tak dobrá päťka ako tá Demitrova na nedávno skončenej olympiáde.   A nielen tejto vyššie zmienenej päťke ale všetkým ženám na kandidátke SaS úprimne veľmi držím palce.   Zdroj foto : internet a archív SaS  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (86)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Alternatíva voči Ficovi existuje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Píše redaktorka Pittnerová za špinavé peniaze Smeru?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            "Kali" prenajíma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Bejby sú naďalej medzi nami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ľubomír Galko 
                                        
                                            Ten Sulík sa zase vyfarbil!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ľubomír Galko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ľubomír Galko
            
         
        lubomirgalko.blog.sme.sk (rss)
         
                        VIP
                             
     
         Exminister obrany a poslanec NR SR Podpredseda strany Sloboda a Solidarita Zakladajúci člen a člen Republikovej rady.  ---------------------------------------------------------- Mojim tvrdým bojom proti korupcii som sa stal vážnym problémom a nepriateľom pre mnoho nečestných ľudí, včítane politikov, naprieč celého politického spektra, ktorí doteraz radostne parazitovali na systéme                ------------------------------------------------------- Nikdy mi nešlo o koryto, ale pokiaľ mam možnosť, budem stále ťať do živého, odkrývať zlodejiny a upozorňovať na tú rozbujnelú chobotnicu aj na jej chápadlá. -----------------------------------------------------------  Pretože si myslím, že to stále má zmysel. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    164
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6774
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




