

 
K tomuto účelu výborné poslúži draft otvoreného listu klimatológa Jamesa Hansena z Columbia University a NASA britskému premiérovi Gordonovi Brownovi a nemeckej kancelárke Angele Merkelovej [v ňom argumentuje, prečo by politici nemali schváliť výstavbu nových uhoľných elektrární v týchto krajinách]. 
 
 
 
 
 

 
 

 
 
Aby sme mohli kvantitatívne posúdiť podiel jednotlivých štátov na globálnom otepľovaní, je nutné sa pozrieť na kumulatívne emisie oxidu uhličitého z fosílnych palív od začiatku priemyselnej revolúcie (Obr. 1): 
 
 

 
 

 

 
 
 

 

 
 
Z grafu je zrejmé, že najväčší podiel na množstve skleníkových plynov, ktoré je dnes vo vzduchu majú Spojené Štáty (27.5%), zatiaľ čo Čína si vedie ďaleko lepšie (8.2%) 
 
 
 
 
 
Treba však mať tiež na pamäti aj počet obyvateľov v danom štáte a je dobré sa pozrieť na „zodpovednosť“ jednotlivcov v danom štáte. V súčasnosti (rok 2006) je realita nasledovná (Obr. 2): 
 
 
 
 
 

 
 

 
 

  
  
 
 

 

 
 
Znovu vedie USA, a asi 5-násobne predstihuje Čínu v produkcii skleníkových plynov na jedného obyvateľa. 
 
 
  
 
 
No a nakoniec je vhodný, pri hodnotení viny za globálne otepľovanie, graf ukazujúci historický podiel jednotlivcov v daných štátoch na dnešných koncentráciách CO2 (Obr. 3): 
 
 
 
 
 

 
 

 
 

  
 
 

 
 

 
 
A teraz si porovnajte historickú zodpovednosť za globálne otepľovanie jedného obyvateľa USA a jedného obyvateľa Číny, poprípade Indie (!). Myslím, že názor si urobí každý sám. 
 
 
 
 
 
Na poslednom grafe vedie o malé precento pred USA Veľká Británia, ktorá je kolískou priemyselnej revolúcie a ako prvá začala vo veľkom spaľovať uhlie. Veľká Británia (a aj väčšina krajín EU) má však celkom iný a konštruktívny prístup k riešeniu problematiky klimatických zmien. 
 
 
 
 
 
Už len na dokreslenie situácie možno dodať dva fakty: 
 
 
 
 
 
i)     každá ďalšia vypustená molekula CO2 má na otepľovanie menší vplyv ako tá pred ňou, pretože závislosť teploty a koncentrácie CO2 je logaritmická. 
 
 
 
 
 
ii)    dopady klimatickej zmeny najviac postihujú a naďalej budú postihovať práve rozvojové krajiny, pretože nemajú dostatok informácií ani technológií, aby sa vhodne adaptovali a minimalizovali jej účinky. 
 
 
 
 
 
Je niekto, kto súhlasí s postojom USA v otázkach riešenia klimatických zmien?? 
 
 
 
 
 
Všetky grafy a väčšina informácií boli prebraté od Jamesa Hansena. Ďalšie jeho články si môžete v angličtine prečítať na jeho stránkach. 
 

