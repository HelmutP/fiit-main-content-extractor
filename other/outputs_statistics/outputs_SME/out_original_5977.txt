
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dagmar Rusnáková
                                        &gt;
                Nezaradené
                     
                 Ako som sa znova učila čítať 

        
            
                                    6.5.2010
            o
            16:09
                        (upravené
                6.5.2010
                o
                17:38)
                        |
            Karma článku:
                8.38
            |
            Prečítané 
            832-krát
                    
         
     
         
             

                 
                    Čítať a písať nás naučili naše drahé - zväčša panie - učiteľky keď sme mali 6 - 7 rokov. V dospelosti sa nám tieto zručnosti javia už ako plne zautomatizované, ale niekedy to aj s nimi dopadne ako v Modernej dobe  Ch. Chaplina. /Aj jeho osud bol asi predurčený, veď cha - cha hovorí jasnou rečou./ V naivnej viera sama v seba, aspoň do dnešných dní, že čítať viem teda určite a dokonca aj poriadne, som žila aj ja. K zmene môjho postoja prispeli - topánky.
                 

                     Nové, pohodlné, dcérami ofrflané /ale však nosiť ich budem ja, tak čo.../, cenovo prístupné - 23 éčok - ani veľa ani málo, už len kráčať prešovskými chodníkmi.   K prvému prekvapenému pootvoreniu očí došlo na 2. deň ich nosenia, keď sa mi začal odliepať podpätok, teda podlepenina podpätku. Poučená inými topánkovými príhodami som pietne vložila topánky do pôvodnej škatule a zo smutnou otázkou v očiach i na perách - čo s tým ďalej? - som  napochodovala do prešovského A..... v prešovskom M.... Pani za pultom na mňa lakonicky pozrela a jej veta: „Na to sa reklamná udalosť nevzťahuje." - ma ani vlastne neprekvapila.   K druhému otváraniu očí došlo asi o mesiac, keď som si všimla lichobežníkový tvar podrážky topánok. „Ak to takto pôjde ďalej, tak tieto boty nedožijú do jari", zhodnotil daný stav môj pragmatický manžel.   Stav bdenia a definitívneho otvorenia očí nastal do tretice, a to po zistení, že špička mojich trojmesačných topánok začína povoľovať. Pietne som utrela topánky od prachu, vložila ich do igelitovej tašky /pôvodná škatuľa už dávno bola ta -tam/ a vybrala sa zo smutnou otázkou v očiach i na perách - čo s tým ďalej?  do prešovského A..... v prešovskom M.... Pani za pultom na mňa lakonicky pozrela a jej veta: „Na to sa reklamná udalosť nevzťahuje." - ma opätovne neprekvapila. Naopak, bola som na seba hrdá ako sa vyznám, keď som jej odpovedala, že ja podrážky neriešim, ale tá špička je snáď už zaujímavá aj pre ňu. Pani znechutene prevrátila topánky v rukách a sucho skonštatovala: „Sú špinavé." Keď mi na moju vlezlú otázku: „Že kde?" povedala: „Na podrážke!" S dodatkom: „Nečítali ste reklamačný poriadok? Majú byť čisté a suché!"  Zalapala som po dychu. Predstava druhého odchodu z toho istého obchodu s tými istými topánkami a nevybavenej reklamácie ma naozaj dojala. Usmiala som sa na ňu a odchádzajúc som zaševelila: Nebojte sa, ja Vám tie topánky donesiem." Vychádzajúc z obchodu mi však skrsla báječná myšlienka. Som v Maxe, sú tu toalety, je tu voda a mydla! Ide sa na TO! To ste mali vidieť! Voda tiekla, ale tak s citom, toaletný papier suploval handru na utierane, mydlo zabezpečilo dokonalosť vzhľadu. Pamätajúc na vetu: S U CH É!  som na záver strčila podrážky pod sušiak na toalete. S pocitom dobre vykonanej práce som napochodovala do oného obchodu. Veta: „Tak som Vám doniesla tie topánky a majú čistulinkú podrážku!" vôbec nevyvolala frenetický potlesk. Pani na mňa pozrela pohľadom ošetrovateľa na psychiatrii a sucho skonštatovala: „Vy ste nepočúvali? Majú byť suché! V sáčku za mesiac splesnivejú a to by ste si ich potom vzali také nazad? Nečítate reklamačný poriadok?" A okom hodila na reklamačný poriadok pripojený k cenovke. Priznám sa, že písmenka veľkosti 7 - 8 na počítači mne, hrdej nositeľke dioptrií č. 4 veľa nepovedali a že som ho teda naozaj nečítala.   Uznáte, že po takejto argumentácii som naozaj nemohla urobiť nič iné, len zobrať topánky, bloček o zaplatení a s grimasou na tvári hovoriacou: Ja sa vrátim! odplávať z predajne. Doma som si zobrala lupu, pridala k nej okuliare a konečne si prečítala reklamačný poriadok. Naozaj sa tam písalo, že topánky sa vracajú čisté a suché a len taký lajdák, nepresne čítajúci ako ja nepochopil, že sa zaňho bude hanbiť jeho milá pani učiteľka, keď mu z tohto textu nedošlo, že aj podrážka je časť topánky.   Topánky mám zatiaľ doma. Už tretí deň mi schne podrážka, lebo naozaj nemám času na to, aby som to riskla ešte raz a doniesla ich zase domov. Či mi po mesiaci - lebo veď toľko môžu byť v reklamačnom šetrení - vrátia oné topánky, alebo peniaze, naozaj neviem. Čo ale určite viem, je, že hranica dôslednosti a neslušnosti až šikany je občas veľmi blízko a tiež, že z A..... si už fakt nikdy v živote nič nekúpim.   /Podobnosť s osobami a udalosťami naozaj nie je náhodná/     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            Sprievodca cestovateľa MHD na Slovensku - 1. kapitola
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            S mobilom na večné časy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            Prečo mám rada šarištinu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            Slovenskí gladiátori 21. storočia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            O Šindliari, ľuďoch a divadle
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dagmar Rusnáková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dagmar Rusnáková
            
         
        dagmarrusnakova.blog.sme.sk (rss)
         
                                     
     
        Som len taká malá poľnohospodárka v teréne.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    964
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jarek Nohavica: Darmoděj
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




