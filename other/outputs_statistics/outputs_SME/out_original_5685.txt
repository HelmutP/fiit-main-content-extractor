
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Bartkovský
                                        &gt;
                Nezaradené
                     
                 Bratislavské homosexuálne zvrátenosti? 

        
            
                                    2.5.2010
            o
            20:22
                        (upravené
                3.5.2010
                o
                8:07)
                        |
            Karma článku:
                5.42
            |
            Prečítané 
            1608-krát
                    
         
     
         
             

                 
                    22. mája 2010 sa v Bratislave má uskutočniť pochod homosexuálov - najväčšia akcia homosexuálov v histórii krajiny. Slovensko je zároveň poslednou krajinou Európskej únie, kde sa takýto pochod zatiaľ nekonal. Takúto akciu treba odmietnuť, lebo ohrozuje normálnu rodinu, mládež a deti. Nielen slovne odmietnuť, ale aj zakázať!
                 

                 Treba vyzvať primátora nášho hlavného mesta, aby urobil všetko preto, aby sa takáto zvrátená akcia vôbec nekonala.   Taktiež politikov...   Ján Pavol II. schválil isté úvahy sformulovaných Kongregáciou pre náuku viery ohľadom pomýlených homosexuálnych zväzkov. Tam sa uvádza: Ak sú všetci veriaci povinní postaviť sa proti právnemu uznaniu homosexuálnych zväzkov, katolícki politici - sú vzhľadom na zodpovednosť, ktorá im prináleží - viazaní touto povinnosťou ešte väčšmi... ide o povinnosť svedčiť pravde.       Čo je to vlastne homosexualita?   A treba homosexuálov odsúdiť alebo homosexuálne úkony?   Homosexualita je trvalá citová, erotická a sexuálna preferencia osôb rovnakého pohlavia. Jej nositeľ je sexuálne priťahovaný osobami rovnakého pohlavia. Homosexuálne vzťahy sú neprirodzené, pretože narušujú prirodzenosť, podstatu a dôstojnosť ľudskej sexuality, a preto ich nemožno schvaľovať; treba však rozlišovať medzi homosexualitou a homosexuálnym správaním. Homosexuál sa nemusí nevyhnutne správať homosexuálne; na druhej strane heterosexuál pod vplyvom zvedenia alebo sexuálneho experimentovania v puberte sa ojedinele môže dopustiť homosexuálneho činu.   Príčiny homosexuality nie sú dostatočne ujasnené; isté je, že jednou z príčin je zvedenie alebo psychické traumy v detstve. Terapia homosexuality je možná, ale úspechy sú pomerne skromné. Verejná podpora homosexuality oslabuje rodinu a ohrozuje zdravý vývoj ľudí, preto ju treba odmietnuť. (FÓRUM ŽIVOTA).       Rodina je v kríze   Požiadavky istých homosexuálov sú pre spoločnosť a svet neprijateľné. Niektorí homosexuáli túžia po registrovaných partnerstvách a dokonca aj v kostole, ako vidno pri protestoch v zahraničí, keď Katolícka Cirkev pochopiteľne takéto zvrátenosti odmieta, a ďalej túžia po adopcii detí a pod.   Rodina je základnou bunkou spoločnosti. Normálna, zdravá, prijateľná a v budúcnosti prospešná rodina bola, je a vždy bude tvorená mužom a ženou. Dieťa patrí do takejto normálnej rodiny.      A na Slovensku, v Európe a vo svete treba chrániť  tradičnú rodinu, a nie podporovať pomýlené homosexuálne akcie, ktoré ohrozujú rodinu a spoločnosť.       A čo sa týka pedofilných škandálov v Katolíckej Cirkvi...   Je zaujímavé, že v tomto Roku kňazov vyšli najavo mnohé sexuálne škandály. Média útočia na Katolícku Cirkev a pritom takéto prípady sú minimálne v porovnávaní tých škandálov, ktoré sa stali v rodinách, v školách (telocvikári),  nekatolíckych cirkvách, a inde. Ale samozrejme, že to vôbec neospravedlňuje.   Štátny sekretár Vatikánu kardinál Tarcisio Bertone vyvolal vraj rozruch keď vyhlásil, že pedofília nesúvisí s celibátom, ale s homosexualitou.      Psychiater Dr. Richard Fitzgibbons, ktorý má skúsenosti s terapiou kňazov, ktorí sexuálne zneužívali deti, dáva za pravdu kardinálovi Bertonemu a hovorí: „Výpovede kardinála Bertoneho sú dokázané výsledkami štúdie Johna Jaya a klinickými skúsenosťami a dôkazmi. Každý kňaz, ktorého som terapeuticky liečil pre sexuálne kontakty s deťmi, mal predtým homosexuálne vzťahy s dospelými."   Dr. Fitzgibbons je od r. 1988 riaditeľom istého poradenského centra v USA a poradcom vatikánskej Kongregácie pre klérus. Vo svojom „Liste katolíckym biskupom" z roku 2002 charakterizoval kňazov so sklonmi k sexuálnemu zneužívaniu, ako osoby, ktoré v detstve utrpeli ťažké zranenia z osamelosti, problémami so vzťahom k otcovi, odmietaním rovesníkov, chýbajúcim mužským sebavedomím a negatívnym obrazom seba alebo svojho tela. Ďalej tento psychiater uvádza: „Kňazi, ktorí majú sexuálne kontakty s maloletými... odmietajú skúmať si svedomie... aj prijať sviatosť pokánia".   Kňazom s homosexuálnymi sklonmi psychiater odporúča, aby sa informovali o pôvode sklonov a o možnostiach uzdravenia. On sám hovorí, že videl mnohých kňazov, ktorí sa stali svätejšími a šťastnejšími vo svojej službe, pretože sa vyliečila ich neistota, čo sa týka ich mužstva, pochádzajúca z detstva a mladosti, ako aj ich osamelosť a hnev a tým aj homosexuálne sklony.   Je to len problém kňazov?       Určite tých, čo propagujú, podporujú a schvaľujú takéto zvrátenosti (homosexuálne pochody, registrované partnerstvá, adopcie detí pre homosexuálov a lesby a pod.) pobúri tento môj článok... Ale nemyslím si, že je to namierené proti nim, ale proti homosexuálnym hriešnym požiadavkám...   Niektorí si myslia, že Cirkev odsudzuje homosexuálov.   Cirkev ich neodsudzuje... Je však proti hriechu, čo dobrovoľne niektorí vykonávajú!   „Nemalý počet mužov a žien má hlboko zakorenené homosexuálne sklony. Táto objektívne nezriadená náklonnosť je pre väčšinu z nich skúškou. Treba ich prijímať s úctou, súcitom a jemnocitom a vyhýbať sa akémukoľvek náznaku nespravodlivej diskriminácie voči nim. Aj tieto osoby sú povolané plniť vo svojom živote Božiu vôľu a, ak sú kresťanmi, spájať s Pánovou obetou na kríži ťažkosti, s ktorými sa môžu stretnúť v dôsledku svojho stavu" (Katechizmus Katolíckej cirkvi, 2358).       Kongregácia pre náuku viery predložila pápežovi Jánovi Pavlovi II. takéto úvahy, ktoré aj schválil: Cirkev učí, že rešpekt voči homosexuálnym osobám v nijakom prípade nemôže viesť k schvaľovaniu homosexuálneho správania alebo k právnemu uznaniu homosexuálnych zväzkov. Spoločné dobro vyžaduje, aby zákony uznávali, podporovali a chránili manželský zväzok ako základ rodiny, primárnej bunky spoločnosti. Právne uznanie homosexuálnych zväzkov alebo ich zrovnoprávnenie s manželstvom by znamenalo nielen schvaľovanie mylného správania, ktoré by sa následne mohlo považovať za model pre súčasnú spoločnosť, ale aj zahmlenie zmyslu základných hodnôt, ktoré patria do spoločného dedičstva ľudstva. Nie je možné, aby Cirkev pre dobro ľudí a celej spoločnosti takéto hodnoty nebránila.   Ostáva teda dúfať, že náš slovenský národ nikdy nedovolí, aby sa ničila tradičná rodina muža a ženy.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (276)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Dopisoval som si s europoslankyňou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Nech si taliansky umelec vystavuje svoju rodinu a nie pápeža!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            25. marec - Deň počatého dieťaťa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Kaddáfího koniec, Ficovo ticho
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Lesbičky prehrávajú, tak nech sa už vzdajú!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Bartkovský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Bartkovský
            
         
        bartkovsky.blog.sme.sk (rss)
         
                                     
     
        Chcem písať o súčasnom dianí...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Rodina
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zaujímavé blogy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            TK KBS
                                     
                                                                             
                                            Postoy
                                     
                                                                             
                                            Svet kresťanstva
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Dopisoval som si s europoslankyňou
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




