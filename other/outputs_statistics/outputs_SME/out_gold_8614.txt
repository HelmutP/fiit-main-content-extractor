

 
  Zaujal ma článok pani Oľgy Pietruchovej, podľa ktorej v .týždni "škrtli... aj súdnosť a zdravý rozum", keď zverejnili rozhovor s Charlesom Murrayom, ktorý za najzávažnejší trend pri posudzovaní budúcnosti označil, "že sa čoraz viac detí rodí ženám, ktoré nevstupujú do manželstva a deti nemajú otcov." 
 Keďže sa tejto téme odborne venujem (moja dizertačná práca), dovolím si vstúpiť do debaty.  
 Osoba a spoločenský jav 
          Je rozdiel hovoriť o nebezpečnosti slobodných matiek ako osôb a slobodnej matke ako sociálnom jave. Na otázku, prečo žena vychováva dieťa sama, bez otca dieťaťa alebo iného muža v domácnosti, sa možno pozerať hneď z niekoľkých pohľadov: 
 
 1. Osobný pohľad ženy. Na túto otázku nedá odpoveď sociológia a politika, ale koreň treba hľadať v osobnom rozhodovaní minimálne dvoch ľudí, ktoré môže odrážať celé spektrum motivácií, ktoré sú zčasti ovyplvnené výchovou (rodinou, kultúrou, školou, spoločnosťou...), ale sú najmä slobodným rozhodnutím dvojice, ktoré je viac alebo menej riadené rozumom (vyplyv citov, slepá láska, ale môže byť aj obyčajnú zaľúbenosť, klamstvo jedného z partnerov, podvod, z jednej strany len potreba vybiť sexuálny pud a pod). 
 2. Sociológia (a s ňou ďalšie spoločenské vedy) si všimla zaujímavý jav, ktorý pomenoval aj Murray. Totiž, že s problémové správanie mladých mužov má súvis s tým, že vyrastajú bez otca. Toto zistenie má hlbokú logiku. Mladý muž aj mladá žena potrebuje otca pre svoj stabilný vývin (mimochodom, tento dar priznáva aj autorka článku). Problém je, že sa kultúrne začína akceptovať ako fakt niečo, čo túto potrebu deťom popiera. Ak budem hovoriť o slobodnej matke v tejto rovine, budem hovoriť o spoločensko-psychologickom jave a nie o konkrétnej osobe, ktorej motivácia a zodpovednosť sa na tejto rovine nedá posudzovať. 
 
 Slobodná matka + slobodný otec = slobodné dieťa? 
          Pomenovanie slobodná matka je tak trochu zvláštne pomenovanie. Zdá sa, akoby bola viac slobodnou ako matka žijúca v manželstve. Lenže ono to tak celkom asi nebude. Nielenže má viac povinností a menej času na seba (pozor, teraz hovorím o sociologickej slobodnej matke, nie o konkrétnej žene), ale častokrát si tento stav sama nevolí. Jediné, čo tu možno nazvať slobodným, je jej stav v oficiálnych dokumentoch, prípadne jej vlastné rozhodnnutie. Má stopercentnú pravdu pani Pietruchová, ak tvrdí, že žena sama dieťa nesplodí. A tu sme pri koreni veci! Slobodná matka predpokladá "slobodného otca". A predpokladá nejakú slobodnú lásku... Naozaj slobodnú? Lásku určiite mimo celoživotného rozhodnutia jedného pre druhého, lásku, ktorá ešte nedozrela pre sexuálny akt..., hoci si to možno myslela.      
 Nezodpovedný otec a zodpovedná matka? 
          V tejto téme je kľúčom nezodpovedný otec. Lenže musíme priznať, že časť nezodpovednosti prislúcha aj matke. 
          Vrátim sa k predchádzajúcemu podnadpisu, ktorý som zvolil naschvál provokatívne. Nie, slobodní rodičia neplodia slobodné deti. Naopak, komplikujú im život! Opäť, nehovorím o Tebe, slobodná matka, ktorá to čítaš, možno so slzami v očiach, možno po dlhej ceste osobného dozrievania a hrdinských darov svojim deťom, ale o jave, ktorý ohrozuje deti a cez nich budúcnosť. Život jednotlivca sa nedá vyjadriť sociologickými dátami ani psychologickými poučkami. Život osoby je vďaka osobnej slobode ďaleko viac. Ale sú niektoré javy, ktoré dokazuje vývinová psychológia, ktoré významne ovplyvňujú vývin dieťaťa.       
 Neodsudzovať ale rozlišovať 
 Stačí si pozrieť práce skvelých psychológov Matějčeka a Langmeiera, práce psychoanalitikov (od Freuda, cez Eriksona až po Lacana). Dieťa potrebuje oboch rodičov! A nielen ako jednotlivcov, ale aj ich vzťah. Dieťa sa od raného detstva učí nielen láske ja-ty, ale vzťahu ja-ty-on. Otcovská postava mu ponúka ďalší model milujúcej osoby, ktorá mu otvorí nielen svet vzťahov, ale aj vnímanie zákona (Binasco, Lacan). K tomu nechcem zabudnúť pripomenúť, akou dôležitou osobou je otec pri porpore matky počas tehotenstva a ďalšieho spoločného života. 
 Ak sa potom hovorí o negatívnych spoločenských javoch, tie nie sú následkom toho, že žena je zlá, alebo chce škodiť spoločnosti. Chce sa tým povedať, že tieto deti od malička majú viac otázok a túžob, na ktoré bez muža v rodine pri nadľudskej snahe matky, nemôžu dostať dostatočnú odpoveď. 
  Naučiť sa milovať 
          Dieťa potrebuje zodpovedného otca... a zodpovednú matku. Som posledný, kto by odsúdil konkrétnu slobodnú matku. Otcovi by som však povedal svoje. A prečo nie aj matke? Áno, aj jej. Ale prvé, čo môžem urobiť je, hovoriť, že byť slobodnou matkou nie je pre dieťa to pravé orechové. Mladý muž potrebuje vidieť alebo aspoň počuť, že žena nie je objekt na vybitie svojej neuspokojenej sexuálnej túžby, že je osoba ako on. Dievča si zaslúži vidieť, aké je to byť milovanou mužom (svojím otcom k sebe, a vzťah otca a matky). Ak to nevidí v rodine, má to počuť inde. 
          Čo však dnes chalani počujú? Ja som presvedčený, že milovať sa človek musí naučiť. Lebo zaľúbenosť a láska nie je to isté, rovnako ako nie je to isté milovať sa pre zábavu a milovať sa ako osoby. 
 Kto chráni rozum? 
          Ak konzervatívci upozorňujú na jav slobodnej matky, pýtajú sa, čo sa s tým dá robiť. Ako pomôcť týmto rodinám, ženám a deťom? Ako tomu predchádzať? Viacero odborníkov upozornilo na fakt, že ak je štát príliš štedrým darcom pre slobodnú matku, mužovi to vysiela signál, že aj keď sa nepostará on, žena neostane bez podpory. A žiaľ, niektorým mužom to stačí (žiaľ!), hoci by nemalo. 
 Pár slov na záver 

 
   

          Na záver si dovolím uviesť pár poznámok z mojich výskumných skúseností: mladí muži (15-25 rokov), aj tí, ktorí pochádzajú z dysfunkčných rodín a detských domovov, chcú byť väčšinou otcami, a lepšími, ako ich vlastní otcovia. To je veľká nádej. Na druhej strane sa zdá, že niektorí z nich nevidia veľký súvis medzi ich budúcou rodinou a súčasnými vzťahmi so svojou partnerkou. Som presvedčený, že niekde tu je kľúč k tejto téme: aby muž vnímal v žene, s ktorou chodí vždy aspoň potenciálnu manželku. Ak ňou nie je, tak je to minimálne odkaz prehodnotiť vzťah. 

