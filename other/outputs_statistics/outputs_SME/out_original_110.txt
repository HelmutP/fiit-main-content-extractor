
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Juriš
                                        &gt;
                Nezaradené
                     
                 Registrácia živnosti na daňovom úrade bez pokuty 

        
            
                                    22.8.2007
            o
            9:00
                        |
            Karma článku:
                10.49
            |
            Prečítané 
            21672-krát
                    
         
     
         
             

                 
                    Registrácia živnostníka na daňovom úrade je veľmi jednoduchý úkon. Ak vie človek písať a čítať, tak nemusí mať obavy. Živnostníkovi zaberie čas max. 10 minút pri vypisovaní tlačiva prihlášky k registrácii daňovníka FO. V článku ponúkam drobný návod na úspešnú registráciu živnostníka na daňovom úrade.
                 

                  Fyzická osoba (v tomto prípade živnostník) po získaní živnostenského listu je podľa zákona č. 511/1992 Zb. v znení neskorších predpisov povinná zaregistrovať sa na daň z príjmov na miestne príslušnom daňovom úrade do 30 dní od nadobudnutia právoplatnosti oprávnenia na podnikanie.     Registrácia živnostníka na daňovom úrade je veľmi jednoduchý úkon. Ak vie človek písať a čítať, tak nemusí mať obavy. Živnostníkovi zaberie čas max. 10 minút pri vypisovaní tlačiva prihlášky k registrácii daňovníka FO. Túto prihlášku možno získať priamo na daňovom úrade alebo si ju možno vytlačiť zo stránky Daňového riaditeľstva SR na tejto linke.     Samotný úkon registrácie živnostníka spočíva vo vypísaní prihlášky k registrácii daňovníka FO, ku ktorej treba priložiť kópiu živnostenského listu a tieto „2 papiere" odovzdať na daňový úrad, buď osobne alebo poštou.     Aby ste predišli zbytočnej pokute vo výške minimálne 2000,- Sk za oneskorenú registráciu, treba túto prihlášku doručiť na daňový úrad do 30 dní od dátumu, ktorý je uvedený na živnostenskom liste v spodnej časti napravo vedľa jednotlivých činností OKEČ. POZOR dátum, ktorý je uvedený vpravo hore na pečiatke s nápisom „Rozhodnutie nadobudlo právoplatnosť", daňový úrad neakceptuje. Podstatné je to, od kedy sa môže daná činnosť (ohlasovacia živnosť) vykonávať.          Predná strana prihlášky k registrácii daňovníka FO (vzorovej vymyslenej osoby Heleny Ružičkovej ku ktorej patrí horeuvedený vymyslený živnostenský list), by mala byť takto vyplnená:     Dôležité údaje sú rodné číslo, meno a priezvisko, adresa, telefón alebo mobil a prevažujúca činnosť, ktorú bude daňovník vykonávať najviac (ak ich má na živnostenskom liste viac).                Na druhej strane prihlášky je dôležitý dátum vypísania prihlášky a vlastnoručný podpis daňovníka (živnostníka).      Ak na vyplnenej prihláške chýba jeden z týchto údajov, tak vám príde výzva z daňového úradu, aby ste chýbajúce údaje doplnili v uvedenej lehote, inak sa bude na vašu prihlášku hľadieť, ako keby nebola podaná. (Teda nebude vám pridelené daňové identifikačné číslo a môžete dostať pokutu).          Súčasťou prihlášky k registrácii daňovníka FO sú aj 3 prílohy. Najdôležitejšou prílohou je príloha 1, ktorá obsahuje kolonku Bankový účet, zástupcu v daňovom konaní a splnomocnenca pre doručovanie. V prípade, že ešte nemáte založený bankový účet a nemáte ani zástupcu v daňovom konaní ani splnomocnenca pre doručovanie, tak túto prílohu nevyplňujte. Ale majte na pamäti, že ak vám budú chodiť peniaze z podnikateľskej činnosti na bankový účet, ste povinný tento účet nahlásiť daňovému úradu do 15 dní.          Prílohu 2 a 2.1 je v podstate zbytočné vypisovať, keďže k prihláške sa prikladá kópia živnostenského listu, ktorá obsahuje predmet činnosti a všetky údaje týkajúce sa vydania živnostenského listu. (Archívne kapacity daňového úradu sú tiež obmedzené).  V prípade, že prílohy vypisujete, nazabudnite uviesť v pravom hornom rohu vaše rodné číslo alebo IČO. (aby sa vedelo identifikovať, komu tá príloha patrí, keby sa papiere pomiešali....).     Vyplnenú prihlášku s kópiou živnostenského listu môžete poslať poštou alebo osobne priniesť na daňový úrad. Ak prídete osobne, je dobré mať so sebou aj originál živnostenského listu, aby mohol daňový úradník overiť zhodnosť kópie s originálom živnostenského listu.     Výsledkom podania prihlášky k registrácii daňovníka FO je zaregistrovanie a pridelenie daňového identifikačného čísla (DIČ). Osvedčenie o registrácii a pridelení DIČ daňový úrad musí vystaviť podľa zákona v lehote do 7 dní od podania prihlášky. Osvedčenie sa štandardne posiela poštou v obálke do vlastných rúk, ale daňovník môže požiadať o osobné vyzdvihnutie osvedčenia na daňovom úrade.     PS Ak budete vypisovať prihlášku k registrácii podľa tohoto návodu, nemali by ste mať problém s registráciou na daňovom úrade. Tento návod platí aj pre registráciu samostatne hospodáriacich roľníkov, sprostredkovateľov, slobodné povolania alebo prenajímateľov nehnuteľností. (Prenajímatelia nehnuteľností uvedú na prednej strane prihlášky prevažujúcu činnosť "Prenájom vlastných nehnuteľností".)  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Friday's american &amp; oldtimer cars (Danubia, August 2013)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Zápas II. futbalovej ligy žien o prvé miesto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Bolek Polívka o slovensko-maďarských vzťahoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Kvitnúca jabloň s jablkami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Juriš 
                                        
                                            Veterný mlyn v Holíči
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Juriš
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Juriš
            
         
        marianjuris.blog.sme.sk (rss)
         
                                     
     
        Muškát žltý
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2938
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Urda - filatelia
                                     
                                                                             
                                            Anton Kaiser
                                     
                                                                             
                                            Ivka Vráblanská
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O hlasovacom stroji Smeru, piatej slobode a Ficových akademikoch
                     
                                                         
                       Friday's american &amp; oldtimer cars (Danubia, August 2013)
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




