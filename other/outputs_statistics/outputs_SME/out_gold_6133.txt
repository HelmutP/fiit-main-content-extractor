

 Počet obvinených poriadkových policajtov v roku 2008 oproti roku 2007 klesol o 18.(tabuľka 1). Dobrý výsledok, pomyslel som si. Zavolal som policajnému hovorcovi, aby mi odpovedal na jednoduchú otázku: "Čím to je, že obvinili menej policajtov?" Odpoveď bola asi v nasledovnom duchu: "Systémové opatrenia, lepšia vzdelanosť a prísnejšie kritériá výberu policajtov sú dôvodom, prečo stále menej poriadkových policajtov porušuje zákon." Pekná formulácia. 
 O rok neskôr sa však karta obrátila. Čísla z roku 2009 už také priaznivé neboli. V roku 2009 stúpol počet obvinených poriadkových policajtov oproti roku 2008 o 16.(tabuľka 2). Hovorca na otázku "čím to je, že počet obvinených policajtov stúpol," odpovedal s úsmevom a stoickým pokojom: "Viac obvinených policajtov nie je dôsledkom toho, že policajti páchajú viac trestných činov ako v minulosti. Dôvodom je, že oveľa dôslednejšie odhaľujeme nezákonné praktiky v policajnom zbore." Inými slovami, ak je z roka na rok viac alebo menej obvinených policajtov, výsledok je rovnaký: Stále lepší policajti páchajú menej trestných činov alebo policajní inšpektori sú dôkladnejší. Iná alternatíva neexistuje. Mali by zmeniť zákon o štátnej službe a hovorcom dať špeciálnu odmenu za kreatívnosť. 
 TABUĽKA 1 
 
 




 služby PZ  


 2007  


 2008 


 +/- 2007  




 služba poriadkovej polície 


 85 


 67  


 -18  





 TABUĽKA 2 
 
 




 služby PZ  


 2008  


 2009  


 +/- 2008  




 služba poriadkovej polície 


 67 


 83  


 +16  





   

