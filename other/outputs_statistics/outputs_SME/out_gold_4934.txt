

 Mračná tvár moju zahalili 
 a trpkosť očí modrý vodopád. 
 Kamene smoly šťastiu môjmu cestu zahatili 
 istotu chôdze porušil prudký pád. 
   
 Ooo aká trpkosť v duši,keď niekto sklame ťa 
 keď dôveru nalomí ako stisk čepele. 
 Keď ako nôž nečakane bodne ťa. 
   
 Dôveru tvoju nenavráti ti už nič, 
 ako západ slnka za hory sa zastrela. 
 Trpké lúče šľahajú ťa po tvári 
 so šťastím dnes len nezhody a sváry. 
   
 Tak nepýtaj sa,ako opäť pomôcť mi 
 viem len jednu odpoveď. 
 Dôveru moju stratenú,trpký žiaľ 
 obnoví len človek,ktorý ma tak sklamal. 
   
 Len slovo z jeho pier ozdraví mi mokvajúce rany 
 len jeho pohladenie odbremení všetky brány. 
 Len jeho prepáč rozžiari tú srdca izbietku 
 len jeho mám ťa rád odomkne zámok,začarovanú klietku. 
   

