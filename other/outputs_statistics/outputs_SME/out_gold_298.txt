

 
Kostol sv. Alžbety - tzv. Modrý kostolík (Bezručova ulica) začali stavať v auguste 1909 a vysvätili ho 11. októbra 1913. Je súčasťou komplexu gymnázia (Grösslingova ulica) a rímsko-katolíckej fary.  Pôvodne to mala byť iba kaplnka pre študentov katolíckeho gymnázia. Neskôr kostol mal byť symbolickým mauzóleom cisárovnej Alžbety, manželky Františka Jozefa I.. 
 
 
Gymnázium,  kostol i faru projektoval budapeštiansky architekt Edmund Lechner. Sú vybudované v architektonickom slohu secesie, resp. uhorskej odrody secesie. Komplex by mohol tvoriť malý ostrov secesného umenia Bratislavy. Žiaľ, zatiaľ sa zrejme našli finančné prostriedky iba na obnovu kostola a fary. Gymnázium, jeho vonkajšia a vnútorná secesná výzdoba, v súčasnosti chátra. 
 
 
Inšpiratívnym zdrojom secesie, umeleckého  štýlu  konca 19. a začiatku 20. storočia,  bola príroda a návrat k remeselnej výrobe. Secesná línia  je vyjadrená v plynulej vlniacej sa krivke.  Jej nenásilný pohyb na ploche je doplnený neobvyklou farebnosťou. Spolu s ornamentálnosťou a s využitím pravidelne opakujúcich sa prírodných motívov listov a kvetov sú typickými znakmi secesie. 
 
 
Skúsme spolu odhaliť prvky secesie Modrého kostolíku. 
 
 
 
 
 
 
 
 
Kostol sv. Alžbety - tzv. Modrý kostolík, je jednoloďový kostol s vežou a zvonicou. Má výraznú farebnú kombináciu modrej a bielej. Modrá je v secesii symbolom nadpozemského a biela symbolom nádeje. 
 
 
  
 
 

 
 
Hlavný vchod zdobí kruhová mozaika zobrazujúca sv. Alžbetu s ružami 
 
 
 
 
 
Vchod do farskej záhrady tvorí secesná brána v línii oblúka 
 
 
  
 
 
Steny budovy zdobia modré majolikové obkladačky. Jemná remeselná práca a neobvyklý materiál, v tomto prípade keramika, je súčasťou secesných stavieb. 
 
 
 
 
 
Modrá strecha kostola, veže aj výrazného kríža farebne ladí s výzdobou 
 
 
 
 
 
  
 
 
Ornamenty, ktoré tvoria výzdobu sú založené na opakujúcich sa líniách tvarov a prvkov, listov a kvetov. Pravý uhol je narušený jemným oblúkom. 
 
 
  
 
 
Vnútorná výzdoba aj farebnosť je dodržaná i vo vnútri kostola. 
 
 
 
 
 
Základným prvkom stropu je kvetina, listy maľované 
 
 
 
 
 
alebo v podobe štukovej výzdoby vystupujúcej z klenby stropu.  
 
 
  
 Lampu večného svetla zdobí zmenšená kópia uhorskej kráľovskej koruny 
 
 
 
 
 

 
 
Zo stropu visí secesný kovový luster, dominujúci strednej časti hlavnej lodi kostola  
 
 
  
 

 
Modré drevené lavice sú taktiež zdobené ornamentom s prírodným motívom štylizovaného listu a kvetu. 
 
 
  
 
 
V strednej časti je malý oltárik s Pannou Máriou 
 
 
  
 
 
Balkón, miesto pre organovú hudbu ako aj organ samotný je farebne aj ornamentálne zladený s výzdobou kostola. 
 
 
 
 
 
Vnútorná strana vstupnej brány je kombinácia dreva, skla a železa. 
 
 
 
 
 
Detail brány naznačuje dominantný znak secesie, vyjadrený v plynulej vlniacej sa krivke kovu s prírodným motívom vsadenej do skla a dreva.  
 
 
  
 
 
 Prehliadka kostolíka nateraz skončila. Je skutočne modrý a je v ňom zachytené umenie zvané secesia. Je to nádherné miesto pre očistu duše aj pre dotyk s neobvyklou krásou ľudského umu aj remeselnej práce. 
 

 
 
 

