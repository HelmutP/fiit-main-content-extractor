

 
 
  
Už sa v tom vyznám,
 nemá to význam, 
 vyznať sa v citoch  
 a vo vyznaniach. 
   
 To sa len priznám, 
 ku klišé k prizmám. 
 Chvíľu mám úspech 
 ty snáď povieš ach. 
   
 Zmuchlú sa látky, 
 zastonú latky, 
 látkový výbuch 
 a chuť na perách. 
   
 Kým hudbu púšťam 
 ty ma opúšťaš. 
 V citových púšťach 
 kvapku si váž. 
   
 Z neba Ti znesiem 
 to čo neznesieš. 
 Potom ťa púšťam 
 nech v pamäti máš: 
   
 telo sa prešlo, 
 hlavu to prešlo. 
 Ruka ti máva 
 ja necítim zášť. 

