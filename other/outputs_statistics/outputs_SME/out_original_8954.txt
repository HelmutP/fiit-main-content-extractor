
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Veselý
                                        &gt;
                Životný štýl
                     
                 Predsudky bránia v úspechu 

        
            
                                    17.6.2010
            o
            7:37
                        (upravené
                13.8.2010
                o
                10:54)
                        |
            Karma článku:
                7.89
            |
            Prečítané 
            1675-krát
                    
         
     
         
             

                 
                    Ak človek vopred označí svoje šance za beznádejné, je to predsudok. Je to ako samonaplňujúce sa proroctvo. Ak mu uveríte, tak ho tým splníte. Potom už sami vytvoríte podmienky, ktoré umožnia, aby sa ono „proroctvo“ naplnilo.
                 

                   „Chcel by som byť milionár, no keďže som z chudobnej rodiny, určite nemám šancu“. A tak sa ani nepokúsim uvažovať o tom, ako by sa to dalo. A tak sa ním nestanem.   „Chcel by som krásnu partnerku, ale mám krivý nos a žiadna pekná žena by ma nechcela“. Tak sa na ne ani nepozriem. A strácam šancu.     „Rád by som spravil kariéru, ale pri mojom šéfovi asi nemám žiadnu šancu“. Kašlem na to. Neskúsim to.   Takto myslíme často, lebo to na prvý pohľad vyzerá logicky. Niečo chcem, ale je tu prekážka. Keby nebolo prekážky, tak by som to mal. Vzdám sa však pokusov prekonať prekážku, pretože neverím, že to môžem zvládnuť.   Žijeme tak, ako chceme žiť   To, čo v živote mám, som si spôsobil ja sám. Prijať tento fakt je pre mnohých ľudí ťažké, hlavne, ak nie sú spokojní s tým, čo „v živote majú“. Ak totiž nie som spokojný s tým čo mám, je nepríjemné priznať si, že sa tak stalo mojím pričinením. Pohodlnejšie je obviniť niekoho iného.      Príklad. Jozef je už roky zamestnaný na zle platenom pracovnom mieste. Má pocit, že nízky plat a veľa práce sú okolnosti, ktoré on sám už nemôže nijako ovplyvniť. Ale v skutočnosti ich nielenže ovplyvňuje, ale priamo spoluvytvára. On nastúpil na zle platené miesto. On súhlasil s tým, že svoj životný čas bude predávať za málo peňazí. On súhlasil s tým, že po skončení školy sa už prestal vzdelávať. On nie je obeťou okolností. On tú situáciu akceptoval a potom celkom prestal hľadať inú možnosť. On prestal hľadať východisko zo situácie, s ktorou je nespokojný. On zostáva na zle platenom mieste a tým akceptuje nízky plat pravdepodobne aj do budúcnosti. To je jeho podpis ako autora vlastného života.   Príklad. Viera má 120 kíl a stredný vek na krku. Tvrdí, že nikdy nechcela byť tučná. Ale roky sa pozerala do zrkadla a videla, ako priberá. Súhlasila s tým, pretože keby jej naozaj záležalo na tom, aby bola štíhla, hľadala by účinný spôsob, ako priberanie zastaviť. To ona jedla koláče, klobásy a iné nevhodné jedlá. Vedela, že sú nevhodné. Je to všeobecne známe. Každým jedným sústom tak vydávala súhlas s tým, že v budúcnosti bude o niečo tučnejšia ako práve teraz.     Komplexy menejcennosti ako príčina neúspechu   Komplex menejcennosti je súbor pocitov ovplyvňujúcich správanie človeka.   To, ako žijeme, závisí od toho, ako sami o sebe zmýšľame. Stávame sa takými, za akých sa sami považujeme. My sme súčasťou aktuálneho diania, reality. Spoluvytvárame ju spolu s ostatnými ľuďmi a vecami. Ak sa považujeme za slabých, budeme ako slabí aj konať. Slabý neočakáva od seba veľa. A tak dostane málo, pretože sa uspokojí s málom.   Človek, ktorý má komplexy menejcennosti a chcel by napríklad schudnúť, uvažuje takto: „Chcel by som schudnúť, ale to ja určite nedokážem. Som nedisciplinovaný, ne­odo­lám čokoláde, a vôbec… cvičenie ma nebaví. Nemám silnú vôľu“. Asi takto uvažuje človek, ktorý si neverí. Podľa tohto sebahodnotenia sa neskôr aj zariadi. Bude sa podľa toho aj správať. Je preto dôležité, aké sebavedomie človek má.   Ak už vopred neverím v možný úspech, tak načo sa potom vôbec snažiť?   Keď si dovolíte myslieť o sebe, že ste neschopní, tým si sami sebe vystavujete potvrdenie, že budete konať, ako si zvyčajne počínajú neschopní. Opakovane sa tým len programujete na neschopnosť.   Iste aj vy poznáte ľudí, ktorí si každú chvíľu povedia výrok typu:     Nič sa mi nedarí.   Nemám pevnú vôľu.   Nikdy nedosiahnem to, čo chcem.   Som smoliar.   Často som chorý.   Neviem si nájsť dobrú prácu.     Takéto výroky sú celkom zrozumiteľné. Neraz s nimi začali už rodičia, alebo vychovávatelia, keď opakovane tvrdili:     Ty si ale nešikovný.   Z teba nič dobrého nebude.   Tak toto určite nedokážeš.   Kto by bol na teba zvedavý?     Je jedno, či takéto krátke výroky povieme nahlas, alebo si ich hovoríme len v duchu sami pre seba. Dokonca nie je dôležité ani to, či sú pravdivé. Všetky takéto hodnotiace výroky však programujú naše sebahodnotenie.     Ak sa správate podľa predstavy „som chudobný“, dostanete viac chudoby, dostanete viac neschopnosti.   Ak si často hovoríte, že ste človek, čo má stále málo peňazí, budete mať stále menej peňazí. Nepokúsite sa naučiť získať viac, alebo sporiť.     Ak máte pocit, že ste slabí, nebudete sa púšťať do ťažkých úloh. Potom si na život s malými nárokmi na seba zvyknete a vaše schopnosti ešte viac ochabnú.   A tak komplexy menejcennosti, alebo pochybnosti o sebe a svojich schopnostiach trvale regulujú vaše každodenné konanie. To sa neskôr zhmotní do menej kvalitného životného štýlu. Je to podobné, ako prírodný zákon. Platí trvale.   Nabudúce sa pozrieme na to, ako vykrmujeme svoj mozog. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Test: Okuliare na ochranu krčnej chrbtice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Ako nepribrať cez sviatky?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Kniha o chudnutí zadarmo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Ako sa pečie domáci chlieb - video
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Veselý 
                                        
                                            Chudnutie a chôdza - video
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Veselý
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Veselý
            
         
        vesely.blog.sme.sk (rss)
         
                        VIP
                             
     
         Píšem na témy chudnutie, práca, opatrovanie a občas posielam fotky z výletov. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    412
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5070
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Videoseriál chudnutie
                        
                     
                                     
                        
                            O chudnutí krátko, stručne
                        
                     
                                     
                        
                            Chudnutie v sebaobrane
                        
                     
                                     
                        
                            Recepty na varenie
                        
                     
                                     
                        
                            Odpočinkové články
                        
                     
                                     
                        
                            Firefox
                        
                     
                                     
                        
                            Recenzie kníh
                        
                     
                                     
                        
                            Prikázania pre úspešný web
                        
                     
                                     
                        
                            O písaní blogov
                        
                     
                                     
                        
                            The Bat! - Program na e-maily
                        
                     
                                     
                        
                            Iné omrvinky
                        
                     
                                     
                        
                            Hudobná skrinka
                        
                     
                                     
                        
                            Životný štýl
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Dukanova diéta
                                     
                                                                             
                                            Turistika
                                     
                                                                             
                                            Energetická hodnota potravín
                                     
                                                                             
                                            Reality
                                     
                                                                             
                                            Kalkulačka BMI a obezity
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Web o opatrovaní a pomoci
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Glykemický index
                                     
                                                                             
                                            Kam na výlet
                                     
                                                                             
                                            Recepty na varenie
                                     
                                                                             
                                            Najlepšie probiotiká
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




