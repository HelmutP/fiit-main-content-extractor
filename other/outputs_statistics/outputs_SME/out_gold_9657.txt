

 
 
 Slnovrat je oslavou príchodu krátkeho severského leta a bielych nocí, pod rúškom ktorých sa ujímajú moci rôzne sily – príroda, láska, viera, nadprirodzeno. Každý si ho vysvetľuje ináč a oslavuje, čo sa mu práve hodí. Slnovrat poodhaľuje vlastnosti obyvateľov tolerantného Švédska a ošiaľ zo svetla bielych noci je dostatočným dôvodom na ospravedlnenie rôznych vylomenín. Môj prvý slnovrat som pred niekoľkými rokmi zažil v Dalarne (kraj v strede Švédska), kde je podľa domácich slnovrat „najšvédskejší“. 
 Tak sa v tom vyznajte... 
 Korene sviatku, ktorým ľudia oslavovali najdlhší deň v roku, siahajú približne do roku 1400. Spočiatku bol sviatkom Jána Krstiteľa, ale moderná doba mu prisúdila svetský charakter. Oslavuje sa šesť mesiacov pred narodením Krista – teda pol roka pred Vianocami. Po správnosti by sa mal oslavovať 24. júna, no Švédi sú praktickí, a aby si mohli „midsommar“ - ako svojou rečou nazývajú slnovrat - riadne užiť a vychutnať, oslavujú ho cez víkend. V skutočnosti však začínajú už v piatok, kedy je im vďaka celonárodnému sviatku dopriaty deň pracovného pokoja a spolu s nedeľou majú na oslavu tri plné dni. Mnohí však žijú slnovratom už od štvrtka večera, kedy sa vydávajú na cestu do prírody, k priateľom a príbuzným. V Dalarne – „najtradičnejšej“ časti Švédska slnovrat oslavujú aj niekoľko týždňov. Tak sa v tom vyznajte... 
  
 Typická krajina stredného Švédska - lúky, lesy a červené drevené domy 
 Najtypickejšie zvyky, ktoré sa viažu k slnovratu, sú dodnes živé predovšetkým v Dalarne – srdci Švédska. Na rozkvitnutých lúkach zbierajú nádejeplné mladé dievčatá deväť druhov kvetov, ktoré si večer kladú pod vankúš a snívajú o svojom nastávajúcom. Vo Švédsku je zvykom, že venčeky, ktoré majú na hlavách, im pletú na znak náklonnosti chlapci. Nie je to prejav zženštilosti, súvisí to sčasti so spätosťou s prírodou a rovnoprávnosťou v právach i povinnostiach rodinného života. Aby sa niektorý z nich nemohol vyhovoriť, počas slnovratu sa v dennej tlači dokonca objavujú návody na pletenie venčekov. 
  
 Anička tiež nazbierala 9 druhov lúčnych kvetov... 
 V Dalarne u známych 
 V rozľahlom Švédsku je nepísaným pravidlom, že rodina sa schádza dvakrát za rok. Na Vianoce a na slnovrat. „Pôjdeš s nami k známym, majú chatu v Dalarne“, velí Anna-Katarina. Olof, Stina a ich synovia Per a Jan nás vítajú vo svojej usadlosti v dedine Skattungbyn. „Dedinou“ chápu Švédi zopár domov porozsievaných voľne po krajine, vzdialených často aj niekoľko kilometrov od seba. Dva drevené domy našich hostiteľov ležia v lese dobrých päť kilometrov od najbližších obydlí. Po našom príchode vztýčil Olof s hrdosťou zástavu. Rovnako ako jeho krajania k nej prechováva úctu. Modrá symbolizuje more, žltá slnko a kríž je znakom kresťanstva, čo spolu so silným vzťahom k prírode stelesňuje ich životný priestor. 
  
 Vyrážame zo Štokholmu na vidiek a spolu s nami tisíce Švédov 
  
 Po ceste si môžete kúpiť lososa v prístave, priamo od rybárky 
 Olof je jednou z duší slnovratu v Skattungbyne - riadi vztyčovanie slnovratového stožiara. „Každá dedina má svoj stožiar a súperia medzi sebou o ich výšku. Vence a ozdoby na ne pripravujú dievčatá, chlapci ich upevňujú a za prítomnosti dedinčanov ich vztyčujú. Stožiare sú symbolom plodnosti a úrody. Tancom  a veselými pesničkami o láske a prírode ďakujeme za príchod leta – obdobia plodnosti“, vysvetľuje. Po dni plnom tancov a spevu sa domorodci odoberú na kávu s koláčikmi. Káva je na severe v obľube, najmä v zime nie je výnimkou, ak jej denne vypijú aj desať šálok. V zime je tu na rozdiel od leta veľa tmy a vtedy o sebe vyhlasujú, že jej musia vypiť veľa, lebo z ponurého počasia upadajú do depresií. 
  
 Olof má už osemdesiat a slnovrat je jeho celoživotná srdcová záležitosť 
  
 U Olofa na chate. Nápis: "Choďte okolo, býva tu vtáčik". Vzťah k prírode v praxi... 
  
 Mládencom trvá vztyčovanie stĺpu aj pol hodinu 
  
 Muzikanti ako vystrihnutí z Bergmanovho filmu 
  
 Prekáravý tanec okolo slnovratového stĺpa patrí k najobľúbenejším 
  
 Či máš kroj a či nie, či si krpec, alebo starec, slnovrat treba osláviť 
  
 A ešte jeden detský vláčik... 
  
 Aj ja, malý "troll", oslavujem slnovrat! 
 Mycket dryckor, mycket flickor 
 „Veľa pijatiky, veľa dievčat“ - tak sa mládež po večeri v kruhu rodiny s obľubou povzbudzuje pred výjazdom na lesnú párty. Je polnoc, všade plno svetla a domorodcov, ktorí nalievajú otrasne sladký flaggpunč. Hoci „flagg“ je po švédsky zástava, necítiť v ňom ani more, ani slnko, o kresťanstve ani nehovoriac... „Takýchto Švédov si ešte nevidel, čo?“, reagujú známi na moje pohľady smerom k provizórnemu tanečnému parketu. Upozorňovali ma, že keď si uhnú, tak sú podarení. To sedí. Všade veľa pijatiky, aj veľa dievčat. Plavovlásky, chtivé lásky (neskúšal som, ale rýmuje sa to...) sú trošku roztrpčené nad svojimi nevládnymi hrdinami, o ktorých mali snívať s kvetmi pod vankúšom, a tak len sedia pri ohníkoch a vedú reči o všeličom možnom. Do rána ďaleko a do vytriezvenia tiež. Niektorí sa pokúšajú tancovať, no väčšina z nich má zjavné problémy udržať smer, hoci je bezvetrie. Sú skutočne podarení. Jeden z nich sa opiera oboma rukami o dom, akoby ho chcel odtlačiť. V tej chvíli je to zrejme jeho jediná opora v živote. Mocuje sa, ale dom nie a nie sa pohnúť... 
  
 Je pol druhej v noci, ale vidieť na kilometre ďaleko 
 Do rytmu! 
 Škandinávci majú radi lode už od čias Vikingov, stredovekých vládcov severu. Pomohli im dostať sa do iných častí Európy, ba aj ďalej. A čo ich potomkovia? Kto sa chce odviesť na vikingskom plavidle, môže to skúsiť na vodách Siljanu, kultového jazera Švédov, ležiaceho v srdci Dalarny. Tradičnú plavbu do kostola v meste Rättvik (Správny záliv) tu možno zažiť aj mimo sezóny slnovratu, a miestni ju úspešne udržujú pri živote. Kostolná pramica ponášajúca sa na vikingské plavidlo je ozdobená vetvičkami z brezy – na severe veľmi rozšíreného stromu. Stačí prísť do prístavu, odkiaľ sa každú nedeľu presne o desiatej vydáva táto pramica na dvojkilometrovú plavbu do kostola na druhom brehu. „To musíme skúsiť, ináč by náš slnovrat nebol skutočným slnovratom“, povedali sme si s Aničkou hrnúc sa do člna. S prekvapením zisťujeme, že je na nej ešte voľné miesto a dokonca nepýtajú ani korunky za turistickú atrakciu. Jediná podmienka ich súhlasu je, že po omši to spolu s nimi odveslujeme aj späť. Dvojnásobný zážitok, dvojnásobné potešenie na našej strane! 
 V lodi už sedí šestnásť Švédov, zväčša v krojoch. Berieme do rúk veslá a natešení mávame usmievavým turistom, ktorí prozreteľne ostali na brehu. Čln sa zásluhou vesiel tých, čo majú kroje, dostáva do pohybu a dvojica huslistov spúšťa veselé pesničky. Po pol kilometri začínam chápať úsmevy tých, čo ostali na brehu. Vždy sa nájde zopár naivných turistov, ktorí sa nechajú prehovoriť na veslovanie. Veď je to normálny šport! Na rukách vyduté žily, po niekoľkých minútach mám v dlaniach zo tri triesky. Navyše treba udržať rytmus, čo znamená trafiť sa v rade za sebou. Inak by sme sa poudierali, čo si nepraje nikto na palube. Medzi veslovaním sa pokúšam urobiť zopár fotografií a tak zaberám jednou rukou poslepiačky. Keď navigátorka zisťuje, že farba v našich tvárach chytá podozrivo tmavý odtieň a rytmus záberov prestáva byť hoden veľkosti sviatku, dáva pokyn na prestávku. Zdvihnutím vesiel akože zdravíme „pobrežníkov“, v skutočnosti však naberáme nový dych. 
  
 Dorazili sme k brehu a smerujeme na lúku, kde bude "open air" omša 
  
 Piknikové deky sú rozložené, omša môže začať 
 Napriek všetkému sme po vylodení ešte celkom použiteľní. Omša nie je v kostole, ale v prírode – na lúke. Duchovný rozpráva príbeh o človeku, ktorý bol veľmi bohatý, nie však šťastný, lebo nedokázal prežívať zázrak života, ten jedinečný zázrak prírody a ľudskej blízkosti. Mravný odkaz príbehu je, aby sme boli dobrí k iným, lebo všetko na tomto svete je pominuteľné a naháňanie sa za pominuteľnými záležitosťami nie je cestou ku šťastiu. Tou je nachádzanie zmyslu života každého z nás. Nás však opäť „nahnali“ na pramicu a keďže sme raz ľahkovážne dali slovo, usadáme k veslám na cestu späť, na druhý breh, do reálneho života. 
  
 Slnovrat je oslavou krátkeho severského leta. Hoci má voda iba 15 stupňov, deťom to nevadí... 
 Na záver jeden švédsky vtip, ktorý zrejme vymyslel nejaký turista, ktorý sa dal tiež nahovoriť na veslovanie. Viete prečo Švédi dávajú lodiam ženské mená? Lebo pred každým vyplávaním sa musia namaľovať... 
   

