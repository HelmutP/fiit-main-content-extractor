


 
Kôprovský štít sa nachádza
v západnej časti Vysokých Tatier. Najčastejším východiskom, odkiaľ turisti
začínajú s chôdzou, je tatranská osada Štrbské Pleso (súčasť päťtisícového
mesta Vysoké Tatry). Na Štrbské Pleso sa dá dostať viacerými spôsobmi: autom,
autobusom, ozubnicovou železnicou (ľudovo zubačkou) z Tatranskej Štrby
alebo tiež elektrickou energiou poháňanou železnicou z Popradu cez Starý
Smokovec. Čiže dostať sa na Štrbské Pleso je veľmi jednoduché a pohodlné.
Nadmorská výška Štrbského Plesa je 1355 metrov. Ak si za cieľ zvolíme napríklad
Kôprovský štít, ktorý má 2363 metrov, musíme vystúpiť ešte 1008 metrov. 
 
 

 
 
Veľký Kriváň je najvyšší vrch
pohoria Malá Fatra, štvrtého najvyššieho pohoria Slovenska. Častým východiskom
pri výstupe na Veľký Kriváň je osada Štefanová, ktorá má pravidelné autobusové
spojenie s Terchovou aj Žilinou. Jej nadmorská výška je 625 metrov. Ak
chceme vystúpiť na 1709 metrov vysoký Veľký Kriváň, potrebujeme sa dostať ešte
o 1084 metrov vyššie.
 
 
 
 
 
Zaujímavé je, že na zdanlivo
nedostupnejší Kôprovský štít treba vystúpiť o menej výškových metrov ako
na Veľký Kriváň. Je to naozaj tak, totiž o náročnosti výstupu rozhoduje skôr
rozdiel medzi najvyšším a najnižším bodom túry (tzv. prevýšenie), a nie
nadmorská výška cieľa. Môžeme tiež povedať, že dôležitá je v tomto prípade
relatívna výška a nie absolútna. Z tohto hľadiska je rovnako namáhavé
vystúpiť z 500 m n. m. do výšky 1500 m n. m., ako napr. z výšky 4000
m n. m. do výšky 5000 m n. m., síce absolútne nadmorské výšky sa líšia, ale
relatívne v obidvoch prípadoch stúpneme o 1000 výškových metrov
(uvažujeme len prekonaný výškový rozdiel, samozrejme vo výške 4000 m n. m. sa
nám asi pôjde horšie kvôli redšiemu vzduchu a podobne).
 
 
Okrem už spomínaného prevýšenia ako
rozdielu medzi najvyšším a najnižším bodom trasy môžeme prevýšenie
vyjadriť presnejšie tak, že sčítame prevýšenia na jednotlivých úsekoch, na
ktorých trasa buď len stúpa alebo len klesá. Potom dostaneme celkovú hodnotu
stúpania a hodnotu klesania (vo výškových metroch), pričom tieto hodnoty
sa rovnajú ak začíname aj končíme trasu v rovnakej nadmorskej výške. Tieto
hodnoty sú presnejšie, lebo ak začíname v nadmorskej výške napr. 500 m n.
m. a vystúpime na tri po sebe nasledujúce vrchy s výškami 1500 m n. m.,
tak prevýšenie je 1000 metrov, ale v skutočnosti stúpame až 3000 metrov
(postupne).
 
 
Vidíme teda, že samotná absolútna
nadmorská výška cieľa hovorí len veľmi málo o skutočnej náročnosti
výstupu. Viac informácii nám poskytne hodnota prevýšenia, celková hodnota
stúpania, ale náročnosť samozrejme ovplyvňujú aj faktory ako počasie (keď je veľká
zima alebo naopak prudké slnko, silný vietor alebo prší, ide sa horšie), terén
(po skalách alebo po blate sa ide horšie) a podobne.
 

