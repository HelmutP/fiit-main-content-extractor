

 Dnes som sa predieral starou cestou z Košíc do Prešova a pred obcou Budimír je poľnohospodárky lán asi 100 hektárov, oraný po spádnici od lesa ku štátnej ceste.  To, čo som tam videl, neveril som vlastným očiam. Dažďová voda z horných polí je splachovaná a zalieva údolia riek. Celé Slovensko je na nohách, dokonca sa k tomu odhodlala aj Vláda SR, aby vyriešila finančnú kompenzáciu poškodeným. Bolo by rozumnejšie a prospešnejšie pre postihnutých aby peniaze, ktoré Vláda rozhodla im dať boli investované do opatrení, ktoré by zabránili, aby pri najbližších výdatných dažďoch povodne sa neopakovali.  Avšak voľby, ktoré sú predo dvermi, zatemňujú politikom hlavy natoľko, že sa prikláňajú k nesystémovým a zlým riešeniam. 
 Preto problém povodní ostane naďalej nevyriešený a tak dažďové vody so živinami z polí budú aj v budúcnosti splachované a budú zalievať dolných a prinášať tragédiu tým, ktorí historický sa usídlili a vybudovali svoje domy, tam kde v minulosti veľká voda nebola.  Značne poškodená poľnohospodárska krajina nemá vitalitu zadržiavať dažďovú vodu v pôde a potrebuje obnovu, aby sa povodne neopakovali. Ako? Peniaze za odškodné vo výške 25 mil. EUR investované do prevencie by dokázali napríklad v povodí Torysy zadržať jednorázovo cca 8 mil. m3 a vytvoriť viac ako 2.500 pracovných príležitostí na jeden rok pre bezprizorne sa potulujúcich ľudí bez práce, ktorí môžu vytvárať protierózne a protipovodňové opatrenia, aby v budúcnosti bolo menej povodní a menej škôd i menej plaču nešťastných ľudských duší. 
 Je  mi ľúto všetkých, ktorí boli, sú a s istotou budú poškodení povodňami, ak neodstránime príčinu.  Takže teraz investovaných 25 mil. EURO do prevencie, prinesie o rok minimálne 5 násobný osoh a do 10 rokov tieto investované peniaze prinesú viac ako 10 násobný osoh pre ľudí, spoločnosť i prírodu. Prial by som si, že keď už nemá rozum Vláda SR v strategickom rozhodovaní riešiť problém protipovodňovej ochrany,  majme my občania. Skúsme všetci odškodnení na budúci týždeň porozmýšľať o tom, ako pri obnove svojho domčeka nesplachovať dažďovú vodu do najbližšej kanalizácie a na nechať  ju vo svojej záhrade. 
 Ak spravíme všetci odškodnení  vodozádržné prvky vo  svojej záhrade na zbieranie dažďovej vody zo svojej strechy a spevnených plôch, bude to významný príspevok pre zníženie povodňových rizík. Pre tejto realizácie sa môžete inšpirovať tu: http://kravcik.blog.sme.sk/c/228207/Dazdove-zahrady-pre-zdravu-klimu-miest-III.html. Týmto spoločne odštartujeme uplatňovanie princípu „horného nespláchne a dolného nezaleje" . Zároveň týmto inšpirujeme našich poľnohospodárov, našich lesníkov, našich vodohospodárov, urbanistov, územných plánovačov, architektov, starostov i politikov, aby pri využívaní pôdy v lese, v poľnohospodárskej krajine i v intravilánoch sme uplatňovali princíp: "Nechcem splachovať dažďovú vodu susedovi na hlavu" . 
 Zopár záberov z poškodenej poľnohospodárskej krajiny splachovanej a zalievanej. 
  
 Dažďová voda rigolikmi steká už na streche Európy (Pusté Pole) 
  
 Odtok dažďovej vody z poľnohospodárskej krajiny (Kamenica nad Torysou) 
  
 Oranie po spádnici splachuje dažďovú vodu i zeminu (Budimír) I. 
  
 Oranie po spádnici splachuje dažďovú vodu i zeminu (Budimír) II. 
  
 Zaliata diaľnica medzi Prešovom a Košicami 
  
 Vyliata Torysa vo Vajkovciach 
   
   
   

