
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kolísek
                                        &gt;
                Súkromné
                     
                 Zázračné byliny - III. 

        
            
                                    11.4.2010
            o
            18:03
                        |
            Karma článku:
                2.38
            |
            Prečítané 
            365-krát
                    
         
     
         
             

                 
                    Pokračování v popisu účinků byliny NONI - Morinda citrifolia. Poklepete-li 2x na moje foto otevře se můj soukromý blog a zde jsou všechny články které jsem zatím vyprodukoval.
                 

                     Nejdůležitější látky obsažené v NONI:   1. proxeronin  Organismus pomocí skopoletinu a serotoninu (hormony, které jsou rovněž obsaženy v NONI) transformuje proxeronin na xeronin (xeronin izoloval jako první Dr. Ralph Heinicke v r. 1981), který následně využívá v řadě důležitých vnitřních procesů. Xeronin aktivizuje enzymy, bez nichž by normální existence nebyla možná. Ovlivňuje rovněž činnost proteinů (bílkovin), které zapojuje do procesů látkové přeměny. Podle některých vědců mohou proteiny regenerovat poškozené buňky pouze díky xeroninu. Xeronin podporuje také stálou regeneraci poškozených buněk a zajišťuje jejich správné fungování, má analgetický účinek prostřednictvím vlivu na mozkové buňky přenášející nepříjemný pocit bolesti. Nedostatek xeroninu se může objevit u starších osob, v důsledku choroby nebo dlouhodobého stresu. Dr. Heinicke potvrdil, že šťáva z plodů NONI výborně doplňuje nedostatek tohoto hormonu.   2. skopoletin  Tato látka podporuje činnost šišinky (žlázy s vnitřní sekrecí), která produkuje nezbytné hormony, a dále: - chrání oběhový systém před přetížením - podporuje proces rozšiřování zúžených cév - upravuje krevní tlak - má podpůrný a zmírňující účinek při alergiích a astmatu - podporuje léčbu zánětů, otoků a revmatismu Lékařská literatura popisuje případy léčení skopoletinem u osob trpících artritidou, zánětem šlach a mazových váčků.   3. melatonin a serotonin (hormon štěstí)  Mají podobné vlastnosti, ale serotonin působí ve dne, kdežto melatonin je aktivní v noci. Tyto hormony jsou neurotransmitéry, které přenášejí podněty z nervového systému do všech tělních buněk a předávají také informace mezi mozkovými buňkami. Proto nedostatek melatoninu a serotoninu může způsobit neklid, dezorientaci, výkyvy nálad a nespavost. Melatonin a serotonin působí podpůrně při léčbě depresí, fobií (strach z výšek), břišních křečí v době menstruace a premenstruačního syndromu. Upravují psychický stav a spánek, regulují biorytmus spánku a bdění a zpomalují proces stárnutí buněk. Pro osoby, které využívají přehnaně mobilní telefon, je důležité vědět, že jsou ohroženy škodlivým působením elektromagnetického záření, způsobujícím postupnou ztrátu neurotransmiterů v mozku, což může být příčinou vážných nemocí! Podle amerických vědců melatonin a serotonin působí jako ochrana proti tomuto postupnému procesu. Serotonin ve spojení se skopoletinem a xeroninem upravuje činnost prostaty a pomáhá stabilizovat hladinu cukru v krvi.   4. antrachinony  Tyto látky povzbuzují trávení, zmírňují bolest a mají antibakteriální, antivirové a antimykotické vlastnosti. Z antrachinonů se japonským vědcům podařilo izolovat damnacanthal.   5. damnacanthal  Při výzkumu damnacanthalu zjistili japonští vědci, že tato látka zpomaluje dělení některých prekarcerogenních buněk (buněk ohrožených rakovinou) a lze předpokládat, že může podporovat rovněž obnovu jejich správného fungování.   6. vitamíny a minerály  Plod NONI obsahuje velké množství vitamínu C, beta-karotenu (provitamín A) a vitamín B1 a B2. Její složkou jsou rovněž důležité minerály, např: - železo - zlepšuje transport kyslíku do všech buněk  - měď - posiluje imunitní systém  - hořčík - pozitivně ovlivňuje srdeční činnost  - fosfor - důležitý prvek při stavbě kostí a měkkých tkání  Všechny složky plodů NONI se vzájemně spolupracují a díky tomu zesilují své individuální léčebné působení.   Další článek bude pokračování v popisu účinků NONI:  Zázračné byliny - VI.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Neobvyklá příležitost.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Povinné svícení automobilů a ekologie.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Na kolik si ceníte svoje zdraví ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Vytvoření vlastní podprahové audio nahrávky.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kolísek 
                                        
                                            Zázračné byliny – VII.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kolísek
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kolísek
            
         
        kolisek.blog.sme.sk (rss)
         
                                     
     
        Můj zájem je především zdraví a to jak fyzické tak i psychické. Sestavil jsem unikátní regenerační zařízení a i z vlastní zkušenosti tvrdím, že prokazatelně uzdravuje lidi.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    451
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




