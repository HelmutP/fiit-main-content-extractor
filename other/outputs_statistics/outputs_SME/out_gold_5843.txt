

   
 Joe Dassin – Champs Elysées 
  Známy a obľúbený frankofónny spevák, ktorého spevácka kariéra trvala necelých 15 rokov. Narodil sa v New Yorku (1938), no jeho rodičia majú ukrajinsko-maďarský pôvod.  Prečo potom to americké priezvisko? Jeho starý otec emigroval do Ameriky so slovami, že sa tam stačí zohnúť k zemi a môžete si  pozbierať plné hrste zlata. V čase emigrácie mal 14 rokov a samozrejme nevedel ani slovo po anglicky, preto keď sa ho pri registrácii pýtali na meno, zamrmlal, že je jeho rodným mestom je Odessa, a tak sa do jeho dokladov dostalo meno “Dassin.”   Dassinovi rodičia boli z Ameriky vyhnaní v čase maccarthismu (antikomunizmus) a odišli do Európy.  Joe vystriedal viaceré školy, v Michigane si urobil doktorát a vrátil sa do Francúzska k rodičom, kde pomáhal otcovi pri režírovaní filmov ako technik. Skúsil  tiež dabing, moderátorstvo, písanie článkov, no až potom ako spoznal tých správnych ľudí z nahrávacej spoločnosti CBS Records, ktorí si ho všimli, sa stáva prvým Francúzom, ktorý s nimi podpísal zmluvu. Rok 1969 sa Joea javí pradoxným v zmysle pozitívnych i negatívnych udalostí. Vychádza úspešný album “Champs Elysées,” dostane prý infarkt, prichádza úspech v Nemecku a ocenenie Grand Prix za album od Akadémie Charles Cros za menovaný album. Mnohé z jeho piesní sú adaptáciou známych anglosaských hitov a tie jeho sa dodnes preberajú. Dokonca vyšiel album “Salut Joe!” ktorý je vyjadrením pocty spevákovi, v ktorom každú z jeho najväčších hitov spieva niektorý zo známych francúzskych interprétov (Garou, Eddy Mitchell, Roch Voisine, Lesa Cadavres a iní…). Taktiež sa v roku v 2006 v Kanade hral muzikál s názvom “Joe Dassin la grande fete musicale" (Joe Dassin, veľká oslava hudby). Joe Dassin zomiera už vo veku 41 rokov na ďalší infarkt, počas dovolenky na Tahiti. Za krátku dobu svojej speváckej kariéry vyprodukoval množstvo albumov (16 – štúdiové, live, posmrtné) a naspieval mnohé svoje piesne do angličtiny, nemčiny, španielčiny, taliančiny I gréčtiny. Mnohé z melódií poznáme aj my, len v inom prevedení (Guantanamera, Cécilia, L´Amérique, Et si tu n´existais pas, Le café de Trois colombes, L´été idien /interpretovaná Michalom Dočolomanským – Ľúbim ťa/ etc.). 
    Moja naobľúbenejšia, ktorú volám aj piesňou dobrej nálady, je ako inak “Champs Elysées,” z ktorej srší radosť: 
   
 






 
   
 Khaled – Aïcha 
 Známy alžírsky spevák (*1960) spievajúci v arabčine a vo francúzštine, emigroval po fenomenálnom úspechu vo svojej rodnej krajine do Francúzska v roku 1985, aby aj tam presadil hudbu, ovplyvnenú štýlom zvaného “raï.” 
 V roku 1992 prichádza vydarený album pomenovaný “Khaled,” ktorý okamžite zaujme v krajinách Maghrebu i vo Francúzsku. Predovšetkým pieseň “Didi”  spopularizuje jeho prvý album a prináša prvý veľký úspech “raï.” Stáva sa N.1 v Izraeli a vôbec prvou piesňou v arabčine, ktorá obsadila prvú priečku. Spolupráca so známym Jeanom-Jacquesom Goldmanom v roku 1996, ktorý dopomohol nejednej hviezde k úspechu, vedie k vzniku albumu “Sahra,” predovšetkým však mega úspešnej skladby “Aicha,” neskôr prespievanej aj do viacerých jazykov.  V roku 2003 prevzatá dánskou skupinou Outlandish,  dosahuje prvé priečky v USA a v Nemecku. Naspieval taktiež duet s Mylène Farmer “La poupée qui fait non,” s Carlosom Santanom “Love to the people” a iné. Doteraz vydal 10 albumov, ten posledný vyšiel minulý rok a nesie titul “La liberté.” 
  A tu je…krásna skladba Aïcha… 
 






 
   
 France Gall – Ella, elle l´a 
 Bývalá francúzska popová speváčka (*1947), ktorá časť svojej tvorby vyprodukovala v spolupráci  so  svojím mužom, spevákom a skladateľom, Michelom Bergerom, už v súčasnosti aktívne nepôsobí na hudobnej scéne.   Prvý singel jej vyšiel v 16-ich, pod krídlami hudobného vydavateľstva Philips Records a volal sa “Ne sois pas bête”(Nebuď tak hlúpy).  Tento i ďalší singel v poradí napísaný známym Sergom Gainsbourgom, sa stali hitmi a vyniesli ju na prvé priečky francúzskej hitparády v roku 1964. O rok nato nahrala singel “Sacré Charlemagne” venovaný deťom, z ktorého sa predalo až dva milióny kopií. V roku 1965 bola vybraná, aby prezentovala Luxembursko v Eurovízii, kde odspieva pieseň “Poupée de cire, poupée de son" (rovnako od Gainsbourga) a napokon víťazí. Francúzsko jej však úspech vyčíta, pretože ho získala účasťou za inú krajinu. Skladba jej však zaistila známosť v Nemecku, v Taliansku a Japonsku, kde bola neskôr nahrávka vydaná.  Po niekoľkých drobných filmových príležitostiach sa objavuje v rockovej opere Starmania (1979), na vytvorení ktorej sa podieľal  jej vlastný manžel a Luc Plamondon. Show plná  nesmrteľných hitov, ktoré prežívajú dodnes. Posledný výraznejší úspech zaznamenal singel “Ella, elle l´a,” pochádzajúci z albumu Babacar (1987), ktorý sa umiestnil v niekoľkých krajinách na prvom mieste hitparád. V tejto piesni Michel Berger vyjadril poctu americkej jazzovej speváčke Elle Fitzgeraldovej. 
  Voilà…hneď sa mi chce tancovať :-) 
 






 
   
 Desireless – Voyage, voyage 
 Claudie Fritsch-Mentrop, narodená v Paríži (*1952) túži v 20-tich rokoch pracovať v oblasti módy, no preslávi sa ako speváčka.  Pod umeleckým menom Desireless vydáva v roku 1986 singel "Voyage, voyage," ktorý sa okamžite vystrelí na prvé miesta hitparád v celej Európe a vďaka nemu získava medzinárodný úspech s piesňou interpretovanou vo francúzštine, čo nebolo zďaleka bežnou záležitosťou. Vo Francúzsku získala zlatú platňu za pol milióna predaných kopií. Pieseň bola neskôr prevzatá rôznymi interpretmi a prespievaná do ruštiny, španielčiny a japončiny. Dodnes sa objavuje na kompiláciach typu Best of 80´s. 
 Tak poďme si trochu zacestovať… 
 






 
   
 Mylène Farmer - Désenchantée 
 Kontroverzná speváčka pochádzajúca z Kanady (*1961), nevydala ani jeden album, za ktorý by sa mohla hanbiť. Rusovláska sa v 80-tych rokoch stala vo Francúzsku najpredávanejšou speváčkou a uspela i ďaleko za frankofónnymi hranicami, v Rusku a vo východnej Európe. Predajnosť albumov prekročila 25 miliónov.  Mylene je považovaná za zvláštny zjav hudobnej scény, známa svojimi odvážnymi klipmi, ktoré sú často skôr krátkymi filmami a obdivuhodnými koncertnými vystúpeniami. 
 V rokoch 1986 a 1992, každý z vydaných singlov dosahuje obrovský úspech (Tristana, Sans contrefacon, Ainsi soit je…, Pourvu que´elles soient douces, Désenchantée…). Dostáva sa dokonca do Knihy Guinnessových rekordov, napríklad za predaj 1 300 000 exemplárov singlu “Désenchantée.” Rýchlo získava množstvo ocenení – najlepšia francúzska interprétka v roku 1988, najlepšie hudobné video /1990/, od roku 2000 a až do 2003 – najlepšia frankofónna ženská interprétka, album roka 2006, 2009 a množstvo iných ocenení. Láme rekordy v predajnosti svojich albumov,  DVD, stáva sa jedinou speváčkou, ktorej sa podaril husársky kúsok - skladby jedného albumu (Point de Suture) sa všetky ocitli na prvej priečke TOP 50. Ako jediná dvakrát vypredá Francúzsky  štadión a získa rekordný počet trofejí za jeden deň – až 8 na udeľovaní cien NRJ Music Award. Takto by som mohla pokračovať donekonečna. 
 Videoklipy slávnej Mylène Farmer dosahujú svetového renomé s sú považované za malé umelecké diela. Mnohé predstavujú tzv. krátke filmy a presahujú aj 17 minút (Pourvu qu´elle soient douces). Nejeden z nich sa na televíznej obrazovke objavil v cenzúrovanej verzii alebo skrátený. Mylène sa v nich často dokonca odhaľuje a zrejme sa nemá za čo hanbiť :-) 
 http://www.youtube.com/watch?v=7H4hWlpQ1E0 (Pourvu qu´elles soient douces) 
 Témy piesní sa týkajú predovšetkým sexu, smrti, náboženstva a lásky a badať v nich vplyv spisovateľov, akými sú napríklad Edgar Allan Poe, Oscar Wilde, či Charles Baudelaire. Hudba sa pohybuje žánrovo od popu, cez rytmy dance/elektro, rocku alebo new wave. 
 Mylène sa podieľala aj na úspechu začínajúcej korzickej speváčky Alizée, stala sa spolutvorcom albumu  (2000), z ktorého sa predalo 6 miliónov kusov. 
 Zvláštnosťou je, že speváčka po celý čas svojej kariéry odmieta založenie oficiálneho fanklubu, či internetového portálu na jej počesť. 
 A na záver pieseň, vďaka ktorej sa stala “najexportovanejšou” francúzskou umelkyňou.. . 
 Désenchantée  8:29, ale stojí za to, pozrieť si...:-) 
   
 






 
   
 Posledné menované tri skladby (Désenchantée, Voyage, voyage, Elle a, elle l´a) pomohli k sláve mladej belgickej speváčke Kate Ryan, ktorá im dala nový šat a prespievala do nových moderných verzií. Ťažko povedať, či lepších alebo horších…posúďte sami… 
 http://www.youtube.com/watch?v=x65k9dQScT8 
 http://www.youtube.com/watch?v=A2tj-XgD5SE 
 http://www.youtube.com/watch?v=Z0KkZWEjGro 

