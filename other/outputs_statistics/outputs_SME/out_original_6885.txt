
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Lettrich
                                        &gt;
                Nezaradené
                     
                 Nepodporujem registrované partnerstvá. Som homofób? 

        
            
                                    20.5.2010
            o
            10:55
                        (upravené
                20.5.2010
                o
                11:16)
                        |
            Karma článku:
                16.63
            |
            Prečítané 
            4612-krát
                    
         
     
         
             

                 
                    V súvislosti s Pochodom hrdosti, ktorý sa na PR zo strany médií naozaj nemôže sťažovať, zaznieva diskusia napríklad aj o registrovaných partnerstvách osôb rovnakého pohlavia. Ľudia, ktorí s nimi nesúhlasia sú nálepkovaní ako netolerantní, bigotní fanatici s homofóbiou. Chcel by som k tomu niečo povedať.
                 

                 Som proti možnosti uzatvárania registrovaných partnerstiev. Ak by som mal stručne odpovedať prečo, povedal by som, že preto, lebo registrované partnerstvo je hrou na niečo čím nie je a nikdy by nemalo byť. Nazdávam sa, že výstižne to vyjadruje aj zákon o rodine.   Ten definuje manželstvo ako zväzok muža a ženy. Spoločnosť tento jedinečný zväzok - všestranne chráni a napomáha jeho dobro. Hlavným účelom manželstva je založenie rodiny a riadna výchova detí. Rodina založená manželstvom je základnou bunkou spoločnosti. Rodičovstvo je spoločnosťou mimoriadne uznávaným poslaním ženy a muža. Spoločnosť poskytuje rodičovstvu nielen svoju ochranu, ale aj potrebnú starostlivosť, najmä hmotnou podporou rodičov a pomocou pri výkone rodičovských práv a povinností. Čo zákon o rodine hovorí? 1. Definuje manželstvo ako zväzok muža a ženy. 2. Za hlavný účel manželstva považuje založenie rodiny a riadnu výchovu detí 3. Považuje rodinu založenú manželstvom za základnú bunku spoločnosti.   Práve pre tieto dôvody spoločnosť manželstvo chráni a podporuje. Je evidentné, že vzťah osôb rovnakého pohlavia tieto dôvody naplniť nemôže. Nebolo by správne, aby takýto zväzok požíval priazeň, akú spoločnosť poskytuje manželstvu. Často som počul námietky, že tieto dôvody nie sú naplnené ani v bezdetných manželstvách. Spoločnosť však chráni manželstvo vo všeobecnosti. A vždy ostáva možnosť, že manželia nakoniec jeden z hlavných účelov manželstva naplnia (napríklad aj osvojením dieťaťa). Manželstvo vždy bude najvhodnejším priestorom pre výchovu dieťaťa, čo o homosexuálnom vzťahu jednoducho neplatí.   Je zaujímavé, že zákon o rodine prisudzuje privilegované postavenie manželstvu, a nie inej formy spolužitia muža a ženy, hoci aj bez manželstva možno priviesť potomstvo. Spoločnosť si však uvedomuje, že najvhodnejším priestorom skutočne ostáva manželstvo. A predstavte si, nikto nekričí o diskriminácii rodičov, ktorí manželmi nie sú/nechcú byť. Ak sa však niekto vyjadrí, že by výhody manželstva nemal mať vzťah osôb rovnakého pohlavia (ktorý nijako nemôže naplniť účel manželstva), tak je homofób a netolerantný fanatik.   Nepochopiteľne mi znie, že spoločnosť, ktorá neumožní registrované partnerstvá sa dopúšťa diskriminácie. Vždy som si myslel, že diskriminácia je konanie, keď rozdielne osoby majú v rovnakej situácii rozdielne práva. Sú muž a žena na jednej strane a homosexuálny pár na strane druhej v ROVNAKEJ situácii? Myslím, že nie. Pre zástancov registrovaných partnerstiev ide o rovnakú situáciu, pretože si vraj oba páry len chcú poriešiť právny status ich spolužitia. Pekná fráza. Ak však prijmeme, že kvôli nej nazveme situáciu oboch párov za rovnakú, neviem, čo budeme robiť, keď sa na našu spoločnosť obrátia dve ženy a jeden muž (alebo dvaja muži a jedna žena) - budú si chcieť len poriešiť právny status svojho spolužitia.   A nemyslím si, že v skutočnosti ide len o poriešenie právneho statusu v spoločnosti. Som presvedčený, že ak raz spoločnosť postaví vzťah osôb rovnakého manželstva, nebude možné zanedlho neumožniť im aj  možnosť adopcie. Tí, ktorí podporujú registrované partnerstvá, ale nesúhlasia s adopciou, vlastne jej umožneniu nahrávajú. Nechcel by som sa dožiť, že bude spoločensky nekorektné hovoriť rodičom otec a mama.   Nemyslím si, že tolerancia znamená prisúdiť status jedinečného zväzku, ktorý spoločnosť chráni a napomáha jeho dobru, pretože práve z neho sa stáva základná bunka spoločnosti aj zväzku, ktorý tento cieľ nikdy naplniť nemôže. Nemyslím si, že som preto homofób. Necítim nenávisť ani strach, odpor ani nič podobné. Každý človek je pre mňa v prvom rade človekom.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (638)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Cenzúrou za ochranu  - čoho vlastne? (príbeh denníka Sme)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Príliš falošná Solidarita
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Tak zakázal či nezakázal súd adopcie kresťanom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Prečo denník SME neuverejnil môj príspevok?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Lettrich 
                                        
                                            Oplatilo sa rozpučiť Orange a začať dýchať O2?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Lettrich
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Lettrich
            
         
        lettrich.blog.sme.sk (rss)
         
                                     
     
        Človek, kresťan a katolícky kňaz. Nie je mi ľahostajný svet okolo nás, ani hodnoty, ktoré vyznávame. 
  Od roku 2011 na blogu denníka SME nepublikujem. Budem rád, keď navštívite môj blog na tomto odkaze.



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3790
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Viera a náboženstvo
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Blog
                                     
                                                                             
                                            .týždeň
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Pred druhým kolom voľby budem stáť za Róbertom Ficom
                     
                                                         
                       Bloger Ondrášik zavádza o kardinálovi Turksonovi
                     
                                                         
                       Kto tu klame II? alebo Otvorená odpoveď pánovi Lučanovi
                     
                                                         
                       Bránime homosexuálne cítiacich
                     
                                                         
                       Keby tak Fico tušil
                     
                                                         
                       Reakcie LGBTI hnutia na ochranu manželstva v Ústave sú slabé
                     
                                                         
                       O mojom detstve.  (pre premiéra)
                     
                                                         
                       Putovanie po východnom Slovensku 3.: Belianske Tatry (videoblog)
                     
                                                         
                       Diktatúra sexuálnych menšín
                     
                                                         
                       Výhodná ročná percentuálna miera nákladov: 59140.89%
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




