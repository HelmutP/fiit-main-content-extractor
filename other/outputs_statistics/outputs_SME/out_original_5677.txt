
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milena Veresová
                                        &gt;
                Zo života
                     
                 Nemôžem sa hýbať, lebo (som) medveď 

        
            
                                    3.5.2010
            o
            8:00
                        (upravené
                3.5.2010
                o
                10:05)
                        |
            Karma článku:
                7.47
            |
            Prečítané 
            1034-krát
                    
         
     
         
             

                 
                    Vždy, keď sa rozhodnem, že budem konečne cvičiť, začne pršať. Aj minule. Ráno otvorím oči po dobrom sne a napadne mi, že je čas urobiť niečo so svojou kondičkou. Natiahnem údy, umyjem sa a medzi prípravou desiaty a venčením psa premýšľam, že taká rýchla prechádzka v prírode by urobila divy. Znovu by som bola pružná ako za mladi, moje telo by sa naplnilo príjemným pocitom a v noci by som po voňavej sprche a dobrom čítaní spala ako v bavlnke.
                 

                   Potom si všimnem, že slnko sa ešte nevyhrabalo z mrakov a stúpim do čerstvej mláky. Ešte celé doobedie premýšľam, dúfam, že sa počasie zlepší a motivujem sa úvahami o príjemnom úteku z domu. Neskôr na mňa doľahne ťažoba práce a poobede sa už teším, že prší. Nemusím ísť nikam...  Športovci a motivační experti tvrdia, že cvičiť sa dá aj v kuchyni. Napríklad brušné tance. No áno, ale mám malú kuchyňu, ktorá je skoro vždy plná ľudí. Tmolia sa tam večne hladné deti a obšmieta môj priateľ. Preto najprv narazím na syna, slalomovým oblúčikom sa mu vyhnem vpravo a ľavou rukou schmatnem zo stola CDčko. Potom zabočím vľavo, pustím hudbu a ľavým falšom sa dostanem do širšej časti kuchyne. Privriem lahodne oči, pupkom vrazím do priateľa... a prejde ma chuť na všetko.        Myslela som aj na beh. Teda iba hlavou. Moje pľúca a priedušky ihneď ohlásili štrajk, kolená kričali niečo o vhodnej obuvi, a ja som si predstavila ako červená a funiaca obieham zamilované páriky, prípadne padám do žihľavy a márne sa snažím doplaziť sa domov. Beh bol prehlasovaný. 
       Minulú jeseň sa raz vrátil od mojich rodičov nasupený synátor. Vraj sa ho dedo pýtal, či som tehotná. Jeho kamaráti z mokrej štvrte boli zvedaví. Pár dní na to som stretla susedu a opýtala sa ma: "Nič v zlom, ja Vám to prajem, veď ešte môžete mať bábätko. A neurazte sa prosím, ale to Váš manžel ušiel s tou pani zo susedného vchodu do Anglicka?"
   Vysvetlila som jej, že manžela nemám už desať rokov, nie som ani tehotná a bežala som domov na internet hľadať pomoc.       Našla som masážny posilňovač brušných svalov s obchodným menom AB Perfect. Volám ho priateľsky Cvičič a od lásky ho nederiem. Buď nemám čas, nervy, energiu či náladu, alebo v obývačke sedí priateľ a zvyšok bytu okupujú deti. Pri knižke za policou sa tak dobre skrýva a nikto mi neráta stránky ako zhyby. Pravda je, že pri tom veľmi neschudnem, no moja psyché mľaská od dobroty.       Kedysi som takto dovliekla domov steper. Chviľu som stepovala, ale produkované hydraulické zvuky rušili televízne spravodajstvo. Chválila som ho a suseda ma poprosila či jej ho požičiam. Tak som z neho zvesila prádlo a šiel na návštevu. Takto zmenil používateľa ešte niekoľkokrát. Naposledy pred pár týždňami, keď musela jeho posledná nájomníčka vypratať pivnicu. Spomenula som si, že kamarát býva len v susednom vchode a športuje rád. Promptne som mu ho ponúkla do opatery. Je ťažký, netrebný a k nim je to blízko. Napokon, na jeho mieste v mojom byte stojí teraz Cvičič.   
 Ale sľubujem, že vo štvrtok (alebo ten nasledujúci;-) si pôjdem konečne pretiahnuť svalstvo do Centra voľného času. Zaplatím si vždy pol roka vopred, aby som sa neulievala. V budove nevadí, ak vonku prší a ja si zacvičím v spoločnosti rovnako vyťažených žien.    Všetky si utrhli chvíľku pre seba a chcú ju využiť pre svoje zdravie. Rozumejú prečo nevládzem, funím a som červená. Sú na tom podobne. Chápu, že deviaty raz sa už nezdvihnem a činky sú mi priťažké. Keď vládzu, tak sa zasmejú - a keď nie, dychčíme vedno ďalej. Ony chodia zaberať pravidelne. Ale povedzte, môžem ja za to, že mám toľko dôležitých prekážok?       Pôjdem sa pozrieť na predpoveď počasia a keď sa budú meniť fronty, pokúsim sa znovu raz dobre zobudiť a ísť aspoň rýchlo chodiť.  Prípadne mi dajte vedieť a môžete sa pridať.   Športu zdar!        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milena Veresová 
                                        
                                            Kateřina Tučková: Vyhnání Gerty Schnirch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milena Veresová 
                                        
                                            Isaac Bashevis Singer: Satan v Goraji
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milena Veresová 
                                        
                                            Zákon ako toaletný papier, alebo ako dať demokracii na frak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milena Veresová 
                                        
                                            L. Kaaberb&amp;#248;lová, A. Friisová: Chlapec v kufri
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milena Veresová
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milena Veresová
            
         
        milenaveresova.blog.sme.sk (rss)
         
                                     
     
        "Fakt, ty nie si nijaká princezná. Si obyčajná mama!"
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    71
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1506
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Knihy
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jaromír Nohavica
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek
                                     
                                                                             
                                            moja obľúbená sestra - reštart :-)
                                     
                                                                             
                                            dobré recenzie
                                     
                                                                             
                                            moja obľúbená sestra
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Zatúlané psíky Šaľa
                                     
                                                                             
                                            ZOMOS
                                     
                                                                             
                                            Eurobooks
                                     
                                                                             
                                            Changenet
                                     
                                                                             
                                            HYENOVINY
                                     
                                                                             
                                            ja a filmy
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zápisky z veteriny – podvádzať či nepodvádzať?
                     
                                                         
                       Patrick Rothfuss: Strach múdreho muža
                     
                                                         
                       Aklimatizačná túra v národnom parku Ala Archa
                     
                                                         
                       Obsedantno- kompulzívny syndróm
                     
                                                         
                       Syndróm smoliara
                     
                                                         
                       Isaac Bashevis Singer: Satan v Goraji
                     
                                                         
                       Kto pácha domáce násilie? Monitoring Krimi novín TV Joj
                     
                                                         
                       Rady pre nádejných spisovateľov - 8 mýtov, ktoré treba vyvrátiť!
                     
                                                         
                       Radikalizácia.
                     
                                                         
                       Obyčajní komici
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




