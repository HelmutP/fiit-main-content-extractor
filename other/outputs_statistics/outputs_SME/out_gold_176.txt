

 
Ako v zasľúbenej
zemi
 
 
Tak a sme tu. V malebnom švajčiarskom mestečku, zo
všetkých strán obklopenom horami a zeleňou. Všetko je tu také maličké
a čistučké, že pôsobí ako z rozprávky alebo reklamy na šťastný život.
Takže v tomto raji mám stráviť leto a ešte si aj zarobiť? To beriem!
 
 
Hotel, kde máme prežiť nasledujúci mesiac, nie je ničím
výnimočný, navonok celkom obyčajný a zvnútra v podstate tiež…až na
pár detailov. „Toto je židovský hotel?“ neveriaco sa pýta budúca kolegyňa Andrea,
keď to vydedukuje z našej konverzácie. Zdá sa, že ona jediná to doteraz
nevedela, my ostatné sme na všetko pripravené. Aspoň si myslíme. Skupinka
ortodoxných na recepcii nás však aj tak trochu zaskočí. Na sebe majú dlhé
čierne kabáty, spod čiernych klobúkov im visia špirálovito zatočené bokombrady.
Starší majú bradu, mladší briadku, všetci majú bledé, tak trochu neduživé tváre
– ako by ich jedna mater mala. Vyzerajú trochu strašidelne, ale aj tak sa
teším. Vždy som chcela spoznať židovskú kultúru, videla som snáď všetky známejšie
filmy o druhej svetovej a židia mi boli vždy sympatickí.
 
 
 
 
 
židovská rodinka na prechádzke (fotené zdiaľky)
 
 
 
 
 
„Hello, what was the journey like?“ víta nás s úsmevom
majiteľka hotela Mrs. Stern. Je to šarmantná päťdesiatnička v elegantnom
kostýme s hustými gaštanovými vlasmi. Kým pijeme kávu pozdraví nás aj jej
manžel, ruku nám však nepodá. „Nemôže sa dotknúť ženy nežidovky,“ vysvetľuje
Petra, ktorá je tu už tretí rok. Hm, zvláštne.
 
 
Poberáme sa do izieb. Ja bývam s Veronikou z Humenného,
s ktorou som sa zoznámila v autobuse. Vyzerá byť v pohode baba. 
 
 
 
Uvítacie faux-pass
alebo naše zoznámenie s mikve
 
 
„Musím si dať sprchu,“ zničene si sadám na posteľ po tom, čo
som vytrepala kufor hore schodmi. Nás dve ubytovali v suteréne, takže sme
dosť odruky od ostatných dievčat. Na konci chodby sa nachádza WC a hneď
oproti našej izbe je sprcha. „To aké prasce tu boli?“ kričím na Veroniku, keď
vidím na podlahe pohádzané mokré uteráky. Je ich tam aspoň desať. Niečo sa mi
nezdá. Naľavo je sprcha a za ňou v rohu akási vaňa, alebo skôr menší
bazénik so schodíkmi, tak pre päť ľudí. Voda v ňom vyzerá prinajmenšom
nevábne. Sprcha je však ok a tak sa zamknem a osprchujem. Pre istotu
čo najrýchlejšie, aj keď…čo iné môže byť oproti personálnej izbe ako sprcha pre
personál? Veronika ide hneď po mne. Konečne sa cítime ako ľudia. 
 
 
Večer pri pive
sa nás dievčatá pýtajú, či bývame oproti mikve. „Kde?“ znervózniem. „Oproti nám
je akurát tak sprcha…s veľkou vaňou.“ Keď poviem, že sme sa tam osprchovali,
najskôr sa všetky do jednej zhrozia a potom vybuchnú do smiechu. „Keby to
zistili, už letíte. A zrejme by to museli celé zbúrať a postaviť
nové, lebo už ste to poškvrnili, hriešnice!“ vtipkuje Petra. Baby nám
vysvetľujú, že mikve je rituálny kúpeľ, kam sa chodia kúpať židovskí muži
a to tak, že si tam vlezú aj štyria-piati naraz. Voda sa pritom napúšťa
len ráno a celý deň sa kúpu v tej istej. Ženy sa tam kúpu vtedy, keď
im končí menštruácia, čiže obdobie, kedy sú „nečisté“. V mikve sa obradne
očistia. Našťastie, aspoň tie sa kúpu po jednej a vždy si vymenia vodu. Neviete
si predstaviť ako sa mi uľavilo, že nás tam nikto neprichytil.
 
 
             tak toto je mikve :D 
 
 
 
Celé je to o jedle
?
 
 
Druhý deň už poctivo pracujeme. Služobne staršie dievčatá
nám všetko ukazujú a vysvetľujú, až mám z toho hlavu ako balón. „Poď so
mnou do kuchynky,“ volá ma Soňa. „Zasvätím ťa do tajov košér stravy,“ ksichtí
sa.
 
 
Slovo košér v prenesenom význame sa v našom
slovníku udomácnilo. Pravý význam slova označuje rituálne bezchybný, čistý pokrm alebo jedlo podľa židovských
predpisov. Ešte doma som si naštudovala zoznam pokrmov, ktoré židia považujú za
nečisté, aby som nebola zaskočená. Z takých obvyklejších je nečisté
bravčové mäso, dary mora a králik. O chvíľu je mi však jasné, že celé
je to omnoho zložitejšie. 
 
 
„Toto je kuchynka,“ vovedie ma Soňa do miestnosti
s kávovarom, krájačmi na syr a chlieb a umývačkou obloženou
riadmi. „Pripravujeme tu raňajky, ktoré sú vždy mliečne. Čiže mlieko, maslo,
jogurty, syry plus vajcia. Žiadne mäso.“ Sústredene prikyvujem. „Mäso býva na
obed a na večeru a pripravuje sa dolu vo veľkej kuchyni. Takisto
dezerty.“ Ahá, takže dodržiavajú takúto „delenú“ stravu. Síce podľa mňa blbosť,
ale ľahko sa to pamätá. „Raňajky sa jedia z iných tanierov ako obed a večera,“
pokračuje Soňa. „Rozlíšiš ich podľa farebného pásika pokraji,“ ukazuje mi dva
taniere. „S červeným pásikom je na mlieko, čiže milchig a s modrým na
mäso - fleichig.“ Dokelu, prečo nie naopak? Rozlíšiť príbory však už nie je
také jednoduché. Sady sa líšia typom rukoväte, pričom v rámci „milšichu“ aj
„fleišichu“ existujú ešte dva typy. „Za žiadnych okolností sa nesmú pomiešať.
Čo pomiešať, ani dotknúť,“ zdôrazňuje Soňa. Nechce sa mi veriť tomu, že sa to
dá ustrážiť. 
 
 
„Neboj, zvykneš si,“ ukľudňuje ma. „Aj keď…niekedy v tom ani
oni sami nemajú jasno. Raz sme boli svedkom polhodinovej debaty, v ktorej
sa dve židovky snažili prísť na to, či majú dať synčekovi čisté špagety
s kečupom do mliečneho alebo mäsového taniera.“ To sú problémy! „Chudáci,
nechcela by som,“ hovorím súcitne. „Moc sa neteš, teraz budeš aj ty na košér
strave.“ A vôbec nekecala.
 
 
Poobede pomáham v kuchyni. Pracujú tu samí muži,
Francúzi a Portugalci. Prekrikujú sa jeden cez druhého, je veselo. Jediná
žena okrem mňa je tu Lizzie – mladá židovka z Izraela. Je veľmi milá, ale
všímam si, že na rozdiel od ostatných sa s prácou nepretrhne. Keď práve
nerozbíja vajcia, sedí so založenými rukami. Zdá sa však, že to nikomu
neprekáža. Od pekára Paula sa dozvedám, že košér kuchyňa spočíva aj v tom,
že vajcia vždy musí rozbíjať žena židovka. Takisto musí každé ráno zapáliť
oheň. Lizzie je tam teda viac-menej len na tieto dve činnosti. Ďalšia vec,
ktorú sa o košér stravovaní dozvedám je, že aj povolené zvieratá musia byť
absolútne zdravé a zabité jedným rezom. Aspoň toľko mi popri inštrukciách
ako mám šúpať jablká, lámanou angličtinou prezradí jeden z kuchárov Miguel.
Princíp košér stravy je teda nielen o zložení a kombinácii jedla, ale
aj o spôsobe prípravy a výroby. Preto si židia pečú vlastný chlieb,
nepoužívajú polotovary, dokonca aj mliečne výrobky si vyrábajú z čerstvého
mlieka. A ak si ich nevyrábajú, tak si kupujú všetko so štítkom košér – od
jogurtov, cez čokoládu až po zubnú pastu! 
 
 
Na večeru máme bolonské špagety. Bez syra. Namiesto syra si
dávame majonézu, tá sa tu môže ku všetkému. Rovnako ako Coca cola.
 
 
 
Jednoducho Mária
a chispy v posteli
 
 
Keď ráno otvorím oči, prvé čo zbadám je biela tvár
s čiernymi bokombradami a prekvapeným výrazom. Neviem kto je viac
v šoku či ja alebo dotyčný, ale našťastie behom sekundy niečo zamrmlal
a odišiel. Ach jaj, ďalší, čo si našu izbu pomýlil s mikve.
 
 
Čas plynie a my si postupne zvykáme, aj keď vždy nás
vie niečo zaskočiť. Na kopec vecí dodnes nemám jasnú odpoveď. Napríklad raz ma
takmer vyhnali z miestnosti, lebo išiel rabín a ja som ho nemohla
vidieť. Ale prečo? Lebo som žena alebo nežidovka? Čo je pravdy na tom, že židia
sexujú iba cez šábes? Prečo majú niekedy na hlavách jarmulky, inokedy klobúky,
ktoré sú niekedy normálne a inokedy kožušinové?
 
 
Po pár skúšobných dňoch na rôznych pracovných „pozíciách“
skončím ako chyžná. To mi vyhovuje, aj keď tringelty tu ktovieako nehrozia. Obliekam
si konzervatívnu rovnošatu s límčekom a zásterkou. V zrkadle sa
na mňa usmieva slúžka ako vystrihnutá z nejakej telenovely. Jednoducho
Mária! :D Jednoznačne sa musím odfotiť. 
 
 
Kladiem si ďalšie otázky a spoznávam fenomén menom „chipsy
rozsypané v posteli“. Vyskytuje sa pomerne hojne. Už viem čo odpoviem
mame, keď ma zase spýta či sú naozaj takí bordelári ako sa hovorí. Na otázku
prečo majú všetky židovky také krásne, husté a vždy upravené vlasy
nachádzam odpovede na nočných stolíkoch. Všetky majú parochne, teda pokiaľ sú
vydaté. Keď oddychujú, nosia akési turbany. 
 
 
Postupne spoznávame hotelových hostí. Väčšinou sú tu celé
rodinné klany, pričom jeden obýva aj šesť izieb. Klienti pochádzajú zo všetkých
kútov sveta, a napriek tomu sú jeden národ. Tí z Izraela sú
uzavretejší, Američania buď prehnane nároční alebo prehnane žoviálni. Sem-tam
nejakí Nemci, Švajčiari, Maďari, Taliani, Francúzi… Jeden starý pán má
slovenské korene a keď sa s nami dá do reči vidíme, že rodnú reč
ovláda dokonale. Sestry v strednom veku sa na nás vždy usmejú
a povedia „Spasíba.“ Až neskôr si uvedomíme, že nie ony sú Rusky, ale
myslia si to o nás.
 
 
 
 
 
moje zlatíčko 
 
 
  
Gút šábes!
 
 
Na svoj prvý šábes sa celkom tešíme, bude to minimálne
zaujímavé. Pre tých, ktorí by nevedeli – šábes alebo šabat sa začína
v piatok večer (po západe slnka) a trvá až do soboty večera.
Kresťanská nedeľa by v porovnaní so židovskou sobotou vyzerala ako Popoluška.
Oslavujú ju naozaj vo veľkom štýle, teda čo sa týka obradov a jedla. Je
fascinujúce, že napriek pokročilému vývoju spoločnosti sú stále verní svojim
tradíciám, hoci im to musí komplikovať život. Cez šábes nemôžu používať žiadne
elektrické ani iné spotrebiče. Nemôžu nakupovať a vôbec platiť. Nemôžu
chodiť autom. Miesto výťahom chodia po schodoch (hotel má osem poschodí
a polovica klientov je buď starých alebo s nadváhou, prípadne oboje. Toto
si trochu protirečí s ďalšou zásadou a to, že cez šábes nie je
dovolené vykonávať fyzickú námahu). Nemôžu zažínať ani zhasínať svetlo, čiže
napríklad na WC sa svieti nonstop. Nemôžu telefonovať, pozerať televíziu,
nedajbože pracovať. Asi by bolo jednoduchšie vypísať čo môžu :) 
 
 
Šábes sa začína spoločným obradom v synagóge, ktorá je
súčasťou hotela. Všetci sú sviatočne oblečení a tvária sa slávnostne. Všade
sa ozýva veselá vrava prerušená sem-tam srdečným „Gut schabes“. Hotel žije do
noci, takže personál má „padla“ až okolo jednej.
 
 
V sobotu začíname ráno, keďže my ako nežidia pracovať
môžeme (kde je spravodlivosť?), ba dokonca musíme. „Nezabudnite dať na izby
šábesový papier!“ kričí za nami Petra. „Šábesák“ nie je nič iné ako natrhaný hajzlpapier.
Židia si totiž počas šábesu nemôžu ani odtrhnúť papier! Pri všetkej úcte
k ich náboženstvu sa nemôžeme nesmiať. Keď som si predtým pripadala ako
Alenka v ríši divov, teraz sa cítim ako v Kocúrkove.
 
 
Počas zametania ku mne podíde mladá žena a požiada ma,
či jej neodveziem kočík výťahom na štvrté poschodie, keďže ona ho nesmie
použiť. O chvíľu dobehne Veronika. Práve vraj pichala jednej žene inzulín,
pretože kvôli šábesu si to nemohla spraviť sama. „Predstav si, že som jej
pomaly musela vymenovať môj rodokmeň, aby sa uistila, že nemám židovských
predkov,“ vysvetľuje. 
 
 
Počas obeda pomáhame dievčatám na sále. Oboznamujem sa
s novými jedlami, ale i so starými, ukrytými pod iným názvom. Hustá
polievka „šolent“ (chollent), „meretiš“ je najemno nastrúhaná cvikla
s chrenom a niečo ako vianočka „chala“ (challah). Hostina pozostáva
viacmenej zo studených mís a zákuskov, keďže variť sa cez šábes nesmie. Keď
všetko upraceme, máme až do večere pauzu. Po večeri je opäť obrad v synagóge.
Všimnem si, že muži sú vo vnútri a ženy sa modlia pred dverami. Keď niekto
vchádza dnu, jedným okom zazriem dav, ktorý niečo jednohlasne mrmle
a akoby zhypnotizovane kýve hornou polovicou tela. Neviem prečo si práve
vtedy predstavím metalový koncert. Muži majú na hlavách a pleciach
prehodené ozdobné obradné šále. Cítim sa čudne, radšej sa vytratím preč. Okolo
desiatej stretnem vo výťahu manželský pár a uvedomím si, že už je po
šábese. Jeho atmosféra však doznieva až kým nevyhoria sviečky a kahance na
zadnom dvore. Keď si pred spaním ideme zapáliť, vyzerá to tam ako cintorín na
Všech svätých.
 
 
 
 
 
židovské deti cez šábes
 
 
...........................
 
 
"No čó, vyprávaj, né? Jak bolo?" privíta ma kamarátka Lucy na
autobusovej stanici v Bratislave. Umlčím ju tristogramovou švajčiarskou
čokoládou a rozkecám sa až keď sedíme na káve. „No moja, na judaizmus by
som nikdy nemohla konvertovať,“ vyhlásim s čistým svedomím. „Hoci…niektorí
boli milí, našli sme medzi nimi dokonca aj dvoch normálnych (!) mužov,“
predvídavo hovorím to, čo ju najviac zaujíma. „Mali len takú decentnú jarmulku,
 ale inak rifle, tričko a tenisky adidas.
Dokonca voňali na dva metre a ani nesklopili hlavu, keď išla okolo nejaká
ženská.“ Sledujem ako Lucy zažiarili oči. „Ale jasné, že všetci boli pre nás
tabu, teda aj my pre nich,“ dodám pre istotu. Lucy chce vidieť nejaké fotky,
ale sklamem ju, že mám len zopár. Nedovolila som si len tak fotiť ľudí. 
 
 
Zamyslene ničím lyžičkou penu z kapučína.
„A predstav si, že aj malé decká tam bežne hovoria tromi jazykmi.“ „Ale sú
to cvoci, nie? Teda podľa toho, čo si mi hovorila do telefónu…“ uisťuje sa. V mysli
sa vrátim do hotela a zasmejem sa. „Cvoci? To sú, ale aj my sme cvoci pre
nich. Takže všetko je tak, ako má byť.“
 
 
--------------------------------------------------
 
 
Update: Pridávam do titulku 1 slovíčko, aby nedošlo k rôznym nedorozumeniam :) 
 

