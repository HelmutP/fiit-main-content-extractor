
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Horanský
                                        &gt;
                Politika
                     
                 Free Market Road Show 2010 - highlighty 

        
            
                                    25.5.2010
            o
            13:34
                        (upravené
                25.5.2010
                o
                13:45)
                        |
            Karma článku:
                3.48
            |
            Prečítané 
            1190-krát
                    
         
     
         
             

                 
                    Dňa 17. mája 2010 sa uskutočnil druhý ročník putovnej ekonomickej konferencie Free Market Road Show. Charakterizuje ju veľmi dobrá myšlienka – zlepšovať ekonomické vzdelanie širokej verejnosti a otvárať diskusiu o vážnych aktuálnych problémoch. Tento rok bola hlavná téma Konsolidácia verejných rozpočtov ako priorita.
                 

                 Mali sme možnosť si vypočuť domácich aj zahraničných rečníkov od USA až po Švajčiarsko. Silnú kritiku si vyslúžila väčšina vlád krajín EÚ, ako aj americký prezident Barack Obama – za nezodpovednú ekonomickú politiku nekonečného zadlžovania. Priemerné zadĺženie EÚ na úrovni 73,6 % HDP za rok 2009 je skutočne hrozivé číslo. Pri USA je to 86,1 % HDP za rok 2009, takže tiež žiadna sláva.     Alan R. Crippen porovnal Slovensko a USA a našiel tam niekoľko podobností – v minulosti ekonomické tigre, ktorým dnes vlády socialistov ničia hospodárstvo. Zameral sa na kritiku Baracka Obamu, ktorému vyčítal najmä zoštátňovanie – General Motors v Detroite, amerických bánk v snahe o ich záchranu, či kontroverznú zdravotnícku reformu. Erich Weede okrem iného kritizoval tzv. európsku pravicu, konkrétne CDU-CSU Angely Merkel, či Sarkozyho.  Fr. Marcel Guarnizo nás oboznámil s ekonomicky pravicovými názormi zosnulého pápeža Jána Pavla II., ktoré sa nachádzali v jednej z jeho encyklík. Upozornil na ostrý kontrast s Konferenciou biskupov Slovenska, ktorá razí skôr socialistický pohľad na hospodárstvo.  Daniel J. Mitchell prezentoval spojitosť medzi výdavkami na štát a rastom HDP. Túto spojitosť odôvodnil štúdiou, ktorú uskutočnili renomovaní odborníci. Tá hovorí, že pri výdavkoch 0 – 20 % HDP rastie z 0 aj číslo rastu HDP, ale od 20% začína pozvoľna klesať. Ako negatívny príklad uverejnil Francúzsko, trochu lepšie na tom je USA a v najvyšších bodoch grafu sa nachádzajú dnešné ekonomické tigre Hong Kong a Singapore. Veľmi zrozumiteľne vysvetlil aj význam rovnej dane a vyzdvihol, že ju Slovensko úspešne zaviedlo. Ukázal nám aj úskalia viacnásobného zdaňovania, ktoré demotivuje obyvateľov a má nepriaznivý efekt na ekonomiku. Pre mňa nová informácia bola, že v USA majú štátnu rovnú daň v 7 štátoch. Ako výnimočne pozitívny príklad boli uvedené Kajmanie ostrovy s 0% daňou z príjmu. Na zozname krajín so zavedenou rovnou daňou svietilo Estónsko, ktoré zaviedlo 0% daň z príjmu pre spoločnosti, v prípade, že zisk bude preinvestovaný v Estónsku.        Michael Jäger nadviazal na Daniela J. Mitchella a pokračoval v obhajobe nízkej a rovnej dane.  Samozrejme, toto je len zopár mojich spomienok na túto skvelú konferenciu. Zaznelo toho ďaleko viac, ale tieto myšlienky mi asi najviac utkveli v pamäti.  Pokial by ste si chceli vypočuť úplne všetko, čo zaznelo, záznamy z nej nájdete tu:   Free Market Road Show 2010 - zvukový záznam  Už teraz sa teším na Free Market Road Show 2011.   Odkazy na súvisiace štúdie:   The Impact of Government Spending on Economic Growth   Taxes and Economic Growth: Implications for German Tax Reform (PDF)   Cato Handbook for Policymakers (PDF) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Kosovo a Srbsko - good news
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Never slovenskému Maďarovi! Určite chce maďarské občianstvo!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Bielorusko - stále posledná diktatúra Európy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Protest proti obmedzovaniu slobody internetu v Maďarsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Robert Fico nepodpísal Memorandum o ochrane Žitného ostrova
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Horanský
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Horanský
            
         
        horansky.blog.sme.sk (rss)
         
                                     
     
        Bc. FMFI UK aplikovaná informatika,
študent FMFI UK - aplikovaná informatika - magisterské št.,
podpredseda Občiansko-demokratická mládež,
člen správnej rady OZ Občan pre demokraciu - projekt Mladý občan.

Verím v osobnú zodpovednosť a slobodu jednotlivca, voľný trh, zdravé podnikateľské prostredie, jednoducho pravicové hodnoty :) Nemám rád socializmus, fašizmus a anarchizmus - extrémy. 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1963
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            IT
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Běž domů, Ivane! A už se nevracej
                     
                                                         
                       Ficova blondínka u Kotlebu
                     
                                                         
                       Skvelá Petržalka, skvelé Ovsište
                     
                                                         
                       LEX Kolesík - SMER chce ropovod cez Bratislavu
                     
                                                         
                       Pán Janukovič,  zabudol som sa predstaviť
                     
                                                         
                       Pre Oracle nie sú korupčné obvinenia za biznis so štátom nič nové
                     
                                                         
                       Kaliňákove hlášky
                     
                                                         
                       Všestranný sprievodca po Lisabone a okolí
                     
                                                         
                       V akej krajine to žijeme?!
                     
                                                         
                       Ako sa rozísť s UPC v 40 jednoduchých krokoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




