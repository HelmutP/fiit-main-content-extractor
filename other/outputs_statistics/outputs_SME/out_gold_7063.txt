

   
 J a n k o      J e s e n s k ý 
 V ý b e r   z  d i e l a       
          
 P r e d      v o ľ b a m i 
 Jak  sa  kolú  !  Jak  sa  rýpu ! 
 Lámu  hlavy  !  Tlčú  kosti  ! 
 Bez  svedomia ,  taktu ,  vtipu 
 stúpajú  do  poctivosti ! 
   
 Letia  zdrapy . Letia chlpy . 
 Každá  strana zneuctená.                                                            
 Ako  voliť  ? Národ  hlúpy 
 poctivej  už  strany  nemá .                     
   
 21. november  1928. 
   
 D e m o k r a t i c k é  v o ľ b y 
 V  tých  voľbách   demokratických 
 je  predsa  čosi  novô, 
 a  to  je , že  hlas  národ má 
 a  vyvolený  slovo. 
   
 Pred   dažďom rastú  oblaky , 
 pred  voľbami  zas  sľuby, 
 po  daždi  rastú  rýdziky , 
 po  voľbách -  prázdne  huby. 
   
 Pri  voľbách  národ  vyberie 
 si  stranu  milovanú , 
 po  voľbách  strana  milovaná 
 ho  odloží  -   na stranu.  
   
 A pravda  pravdou  zostáva 
 pod  potmehúdskym  slncom : 
 voličia  prídu  ku  urnám 
 a vyvolený  -  k  hrncom.                           
   
 17. júla   1929.  
 Pomaly uplynulo  sto rokov od čias, keď Jesenský napísal tieto básne a stále sú aktuálne... Čím to asi bude? 

