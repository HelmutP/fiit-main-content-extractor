

 Časopis môžete zavrieť, prípadne si ho vôbec nekúpiť, nezapínať telku, proste ignorovať všetky diktatúry kultu super woman look. Lenže čo keď je  super vyzerajúca kočka vaša naj kamarátka. A akoby to nestačilo, vaša kamarátka má sestru  ešte v dokonalejšom prevedení. Vždy sme sa čudovali, že nešli za modelky. Postavičky aj tváričky nato majú. A to nehovorím o  ich super dízlovom tráviacom trakte, ktorý rozchodí aj troj meníčkovú večeru s dezertom na konci. Pri večernom posedení my ostatné len zakusujeme ryžový polystyrénový chlebík, kým ony zvládajú plnohodnotnú večeru chlapa. Všetko čo tieto dve slečinky spapajú sa neukladá do stehien, brucha alebo v syslíkových líčkach. Asi sa to niekde stráca v kozmickom priestore. Po našom - sú to proste kyselinárky. O žiadnej anorexii a bulímii nemôže byť ani reči. Tieto fešandy to majú jednoducho v genetickej výbave. Svet je naozaj nespravodlivý. Ale že vraj život kyselinárok má aj svoje negatíva ako svorne tvrdia sestry. 
 -  od rána riešia jedlo, ako prvú otázku života a smrti, aby sa čo najviac zasýtili, 
 -  každý deň MUSÍ pozostávať z päť až viac chodov, inak odpadávajú od hladu, 
 - stravné položky sa pekne zakusavajú do peňažného rozpočtu, 
 -  každá pohybová aktivita musí byť vyvážená dvojitou stravnou dávkou, už len samotná didžina ich vyhladovie ako vlčice, 
 -  ťažko si v obchodoch s handrami hľadajú oblečenie s ich menšou veľkosťou, 
 -  od ranného detstva boli pod tlakom vykrmovacej teórie pani doktorky, súdružiek učiteliek, v škôlke aj v škole. A že z toho boli pekne na nervy. 
 Ja takéto nič nepoznám. Až teraz som pochopila výrok Haliny Pawlovskej: „My krásne štíhle, dlhonohé, blondínky to máme v živote ľahšie." Idem si dať čokoládu nech sa mi príjemná sladkosť pomaly rozpúšťa v ústach. Nepoznám lepšiu terapiu proti všetkým nespravodlivostiam sveta. 

