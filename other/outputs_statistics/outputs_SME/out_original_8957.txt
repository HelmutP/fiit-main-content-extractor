
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Drotován
                                        &gt;
                Politika
                     
                 Neskutočne poddajný Smer alebo len paranoja CBR? 

        
            
                                    16.6.2010
            o
            12:00
                        |
            Karma článku:
                8.95
            |
            Prečítané 
            2339-krát
                    
         
     
         
             

                 
                    Včera bola podpísaná deklarácia SDKÚ-DS, SaS, KDH a Mostu-Híd vyjadrujúca spoločnú vôľu vytvoriť povolebnú koalíciu. Na internete sa však objavila výzva ako tomu zabrániť. Nepriamo je v nej zaangažovaný Smer. Jeho politici, členovia a sympatizanti sa zjavne nevedia uspokojiť s tým, že by mali byť len v opozícii a tak sú ochotní sľúbiť aj modré z neba, aby tomu tak nebolo.
                 

                 Dočítal som sa, aké medové motúziky vraj dáva Smer popod nos KDH. Ponúkajú im vraj post premiéra a polovicu ministerstiev. Keby to aj bola pravda a ak by aj KDH prijalo túto akciovú ponuku, tak len blázon by uveril, že ekonomické páky by nezostali ďalej na pleciach Smeru a jeho mecenášov. Politici Smeru by zostali nedotknuteľnými a jeho poslanci by všetko, čo by prijala vláda a len trochu by sa im nepáčilo, mohli hneď zmiasť zo stola svojou 62-kou poslancov.   Ďalej sú vraj ochotní prijať etické požiadavky KDH, vrátane zákazu potratov podľa poľského modelu. Toho sa hneď chytilo Centrum pre bioetickú reformu (CBR) a rozposlalo svojim sympatizantom (vrátane mňa) výzvu, aby hneď napísali predsedovi KDH Jánovi Figeľovi, aby išiel rokovať so Smerom. Ak vraj nepôjde tak sa stane zodpovedným za, citujem: “životy a duše desiatok tisícov“, čo sú veľmi silné slová.   Rozhodol som sa priložiť celý text výzvy, nech si každý urobí svoj názor. Ja som si ho už urobil a osobne si aj tak myslím, že v Smere je viac liberálov ako kresťansky zmýšľajúcich ľudí. Vychádzam pritom z ich hlasovania a vyjadrení k daným témam v minulosti. Je skôr pravdepodobné, že sa  dnes môže niekto snažiť KDH zdiskreditovať a pripraviť si pôdu na predčasné voľby, keďže s KDH by po ich odchode z vlády  so Smerom nechcel už nikto nič mať. Veď prečo aj, sľuby treba dodržiavať.       Milí priatelia,    KDH má možnosť zakázať potraty a prijať Vatikánsku zmluvu s výhradou vo svedomí, ako i presadiť ďalšie bioetické princípy, ale KDH nemá záujem ani len vyjednávať!    Včera KDH prijalo deklaráciu s liberálnymi stranami o zostavení vlády, v ktorej táto téma napriek ubezpečeniam predsedu Figeľa CBR nemá žiadnu prioritu.     Smer dal však KDH neoficiálne vedieť, že príjme etické požiadavky KDH, vrátane zákazu potratov podľa poľského modelu. KDH však dalo ľahkovážny prísľub (podobne ako ten pred rokom Radičovej) bez akýchkoľvek podmienok, že so Smerom ani len nebude vyjednávať.    1. Je evidentné, že KDH doteraz nedosiahlo žiadne garancie pre svoje bioetické požiadavky od svojich liberálnych spojencov. Liberáli sú zlom budúcnosti (na Západe už prítomnosti), kým komunisti sú zlom minulosti.    2. KDH, ktoré chce byť otvorené k ľuďom, má dnes príležitosť zastaviť genocídu rokovaním so Smerom, ktoré mu len zvýši cenu. KDH nemá čo stratiť. Jeho voliči nie sú primárne vyhranení voči Ficovi (na rozdiel od SDKÚ a SaS), ako sú zásadoví v hodnotových veciach.    3. Ak by KDH dostalo garancie od Smeru, má jedinečnú pozíciu, že Smer bude závisieť od KDH, a nie ako pred 4 rokmi, keď Smer nepotreboval KDH. Dnes sú okolnosti iné.    4. Zodpovednosť za životy a duše desiatok tisícov sú na pleciach Jána Figeľa. Povzbuďte ho, aby sa nebál zrušiť planú dohodu, do ktorej KDH aj tento rok vmanérvroval Daniel Lipšic.    5. Píšte, telefonujte, či SMSkujte Jánovi Figeľovi, aby okamžite predložil svojim liberálnym partnerom podmienku zakázať potraty a ak ju nepríjmu, aby šiel rokovať s Ficom.    Je príliš málo utešovať sa dohodou, že nebudú uzákonené homosexuálne partnerstvá, lebo tie nám tu každú chvíľu aj tak natlačí Európska únia skrz Lisabonskú zmluvu.       Jana Tutková  riaditeľka CBR           P.S.: Komu prospieva táto paranoja? Je Smer skutočne taký poddajný? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (55)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Ministerstvá a ich pobočky kupovali a predávali na poslednú chvíľu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Nezávislé osobnosti začali doplácať na Matoviča
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Televízny odpad namiesto serióznej politickej diskusie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            Okná od Eurojordánu, nikdy viac
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Drotován 
                                        
                                            TV LUX, malá televízia s veľkým srdcom, zahanbila ostatné televízie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Drotován
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Drotován
            
         
        jandrotovan.blog.sme.sk (rss)
         
                                     
     
        Som Bratislavčan s vlastným pohľadom na veci, pochádzajúci z mnohodetnej kresťansky založenej rodiny.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    21
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2042
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Média
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Média v čase globalizácie (Tadeusz Zasępa)
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ladislav Lencz
                                     
                                                                             
                                            Jozef Drahovský
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                             
                                            Viliam Búr
                                     
                                                                             
                                            Michal Drotován
                                     
                                                                             
                                            Peter Kunder
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Mediálne.sk
                                     
                                                                             
                                            imhd.sk
                                     
                                                                             
                                            Seriály.KINEMA.sk
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




