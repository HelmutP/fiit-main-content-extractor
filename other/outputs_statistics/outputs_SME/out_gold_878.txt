
 No potom prišli vojny. Tie čečenské a život sa zmenil.   Ako predslov musím niekoľko detailov. 

  My sme si tiež zopár krát v metre vystrelili. Pri exkavátoroch sedia v búdkach babky a akože dávajú pozor- No zväčša pospávajú a my sme približujúc sa dole na exkavátore spustili čo sa dalo mincu po konštrukcii rovno do plexy zopár centi pred driemajúcou babkou.   Reakcie si ani neviete predstaviť. 
  Raz sa mi stalo, stál som prilepený o nadpis "Neprislaniajtes" (neprilepujte sa k dverám a je napísaný na dverách metra) keď som postrehol hlúčik chalanov. Smrdeli. Urobili kruh a začali vyzliekať nejaké chlapca. Odtrhol som pohľad od svojho Sportexpressu a zbadal som, že zdola mi uprene do očí hľadí ufúľané chalanisko a pri bruchu mi drží nejakú vec.   Pre istotu som nejak ani neskúmal čo že to ma pri bruchu pichá. Na stanici pacany (chalani) s revom vybehli a v strede vagóna zostal stáť polonahí chalan, vystrašený, okradnutý. Vybehol za nimi.   Bolo to v rokoch deväťdesiatych, a ako som sa potom dočítal, boli to tlupy sirôt z hladových miest povolžia. 





  Dnes je to už nemožné. V metre je  dosť ťažko srandovať. Od prvej čečenskej vojny je tam plno policajtov. 

Milicionierov "vďaka" teroristov je už hádam toľko ako cestujúcich... 

  Aj mne sa stávalo, zopár krát keď som šiel napríklad so slovenskými tirákmi, keď nás zastavili a od Slovákov chceli pasy a ja som pri tom robil tlmočníka. Mimikry som mal asi také, že odo mňa nič nechceli.   No napriek tomu si pamätám na deň, keď som šiel metrom do kancelárie niekde na severe po fialovej vetve metra. Vediac, že len včera tu medzi týmito dvoma stanicami umreli ľudia.  Čečenská bomba.
  A túto študentskú historku mi povedal kamarátov syn, študent v Moskve a ktorý mal v skupine čistokrvného Čečenca .   S ním bolo chodiť po ulici nereálne. Každých 5 metrov minty zastavovali, dokumenty preverovali. A priateľovho syna (to si pamätám, otec pri mne sa to pýtal) sa aj často pýtali. Prečo chodí s „čiernožopym“ (žopa - sedací sval). Terorista „što li“ (predsa). No to sa témy netýka.   Mal tento Čečen jednú vlasnosť charakteru. Všetko „do lampočky“, taký „pofigista“, v zmysle : všetko mu bolo jedno. 
Ľahšie povedať, „kryšu rvalo no ne slabo“(trhalo streechu ale poriadne), v preklade, mal záskoky a poriadne.   AJ ja som mal takeho priateľa ale Stredneaziata. O ňom neskôr celý blog, zberám od spolužiakov spomienky.    „Čuť-čuť“ (stačilo trochu) a Čečen hneď to svoje : ZAREŽU (zarežem) (pedstavte si Kakazský akcent a „vsio jasno“(všetko je jasné)) a tak podobne.  

  Mal niečo pod dva metre. Len bradu nechať a rovno na stend na sídlisku na tabuľu „ v roziske milicii“ (hľadá polícia). 

  Raz tento Čečen prehral nejakú stávku. Alebo karty, alebo tarakanie behy, nie je to pre príbeh principiálne vážne. Preto musel urobiť niečo smiešne v metre.   Veď kto ste boli v metre občas ste mali možnosť vidieť pomaľovaného chalana, vyzlečenú babu pobehovať.   Tak toto tam ako hra letelo. Tak aj o tom bola stávka. On aj prehlásil : „Net bazara, vsio budet.“ (bez diskuie, všetko bude) 

Zmyzol, po 15 tich minútach sa vracia a s nie ľahkou taškou vyťahuje dav do metra. 
Spúšťajú sa. 

Na exkavátore pri ceste do podzemia im spolužiak hovorí, nech si sadnú keď vlak príde a majú sledovať čo sa stane a má to byť smiešne. On bude stáť v otvorených dverách.   Prišli na perón. On stojí v póze sochy. Nič sa nedeje. Spolužiaci nechápu, ale zjavne vypité bolo dosť, tak prečo nie! Srandujú. Že oni si sadnú, on sa zarehoce, nenastúpi a oni odídu. 

  No nie toto sa stalo.   Veď to poznáte: 

„Ostorožno dveri zakryvaiutsa, sleduiushtchaia stancia...i t.d. a tak pod.“ (hlas z reproduktora pred zatvorením dverí v metre: Opatrne, dvere sa zatváraju, nasledujúca stanica a tď.).    Tri sekundy do zatvorenia dverí po tmavší spolužiak mení svoju tvár na klasickú čečenskú bojovú variantu a s takým že prízvukom reve na plné hrdlo:    DA ZDRAVSTVUIET IČKERIA!!! ALACH AGBAR!!! A VHADZUJE TAŠKU DO VAGÓNA!!!  (Nech žije Čečensko! Alah  je veľký! )  Tá s hluchým zvukom padá na podlahu vo vagóne a dvere sa zatvárajú
3,5 sekundy bolo vraj ticho.   No potom to začalo. Všetci  s revom „lomanulis“ (slang - rozbehli sa) v druhý koniec vagóna, nejaká babka spadla študákom na kolené a kričala: „Spasite synki, spasite!!!“ (zachránťa, synčekovia, zachránte). 

  Chlapci úplne stvrdli od takejto nečakanej „šutky“ (ftipy). Sedia, nehýbu sa.   Vlak odchádza a ich po tmavší spolužiak sa rehoce na platforme stanice.   Mimochodom tam bola taktiež panika.   Nakoniec niekto stlačil páku extremálneho otvárania dverí a zavolali rušňovodičovi.  Vlak dokonca nestihol vyjsť zo stanice a ľudia utekajú po peróne.   Strašný „šucher“(slang - keď miílicia/polícia niečo robí). 
No kto poznáte potvrdíte. V moskovskom Metre sa to deje rýchlo.   Čečena skrútili kým bol vagón prázdny.
Dlho sa potom liečil, pretože sa mu minty poriadne odmenili.   A v taške bola tehla.   
