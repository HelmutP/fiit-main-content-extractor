
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Holko
                                        &gt;
                Nezaradené
                     
                 Akými chorobami nás môžu nakaziť komáre na Slovensku? 

        
            
                                    27.7.2009
            o
            9:33
                        |
            Karma článku:
                11.44
            |
            Prečítané 
            5193-krát
                    
         
     
         
             

                 
                    V dnešných dňoch rezonuje problém premnožených komárov, ktoré znepríjemňujú život obyvateľov postihnutých lokalít. Okrem toho však komáre predstavujú dôležitý článok v šírení niektorých nákaz ľudí aj zvierat. Táto ich funkcia prenášačov – vektorov infekcií bola tradične spájaná s tropickými oblasťami sveta, kde medzi najznámejšie choroby patrí malária. Komáre však prenášajú niekoľko desiatok ďalších chorôb. Zatiaľ čo väčšina z nich ostáva naďalej problémom trópov, klimatické zmeny spôsobili, že niekoľko závažných chorôb prenášajú komáre už aj na Slovensku. Bohužiaľ lekárska obec im u nás, na rozdiel od ČR, nevenuje takmer žiadnu pozornosť. Pritom paradoxne  vedecké názvy niektorých vírusov sú odvodené od  slovenských obcí, kde boli tieto vírusy zistené prvýkrát v Európe v 60-tych a 70-tych rokoch.
                 

                 
West Nile Virusnews.bbc.co.uk/.../diseases/img/westnile.jpg
   Geniálna príroda.   Dômyselnosť prírody sa prejavuje bohužiaľ aj pri takých javoch ako sú infekčné choroby. Ich šírenie sa často deje rafinovane pričom zasiahnu svoj cieľ s úctyhodnou presnosťou a navyše tak zamaskované, že hostiteľ má len malú šancu sa ubrániť. Jedným z takýchto spôsobov šírenia nákaz je ich prenos hmyzom cicajúcim krv. Tento hmyz pritom nebýva infekciou postihnutý, iba jej poskytne vhodné prostredie na posilnenie (zmnoženie pôvodcu) a prenesie ju na cieľového hostiteľa cicaním jeho krvi. Dôležité pritom je aby uvedený hmyz cical krv živočícha, ktorý je rezervoárom infekcie, ako aj krv konečných hostiteľov.   Vírusy a parazity.   Najpočetnejšou skupinou infekcií prenášaných komármi patria vírusové choroby spôsobené zástupcami z čeľade Arbovírusy. Názov je odvodený z anglického arthropod - borne viruses (vírusy zrodené v článkonožcoch), čo vystihuje práve spôsob ich šírenia pomocou článkonožcov kam zaraďujeme aj komáre (Culicidae). Je popísaných takmer 500 druhov arbovírusov, pričom okolo 100 z nich spôsobuje ochorenia ľudí. Jedná sa hlavne o zástupcov rodov Alfavírus, Flavivírus a Bunyavírus, ktoré sú prenášané práve komármi.   Druhou závažnou skupinou sú parazitárne choroby, medzi ktoré patria, okrem spomínanej malárie, najmä dirofilarióza, ktorej výskyt bol zaznamenaný už aj na Slovensku. http://tvnoviny.sk/spravy/domace/komare-na-slovensku-siria-dalsiu-nebezpecnu-chorobu.html    Prírodné ohniská infekcií.   Je potrebné povedať, že nie všetky druhy komárov sú schopné preniesť uvedené infekcie. Táto schopnosť je špecifická, to znamená, že určitý druh komára prenáša iba niektoré infekcie prípadne žiadne. Preto je výskyt tej ktorej choroby úzko viazaný na prítomnosť konkrétneho druhu komára na danom území. Ak sa na takéto územie zavlečie aj pôvodca infekcie, táto sa dokáže udržať v populácii komárov aj prenosom na potomstvo cez vajíčka (transovariálne). V takom prípade hovoríme o prírodnom ohnisku nákazy. Takéto ohniská sa v určitom období (aj niekoľko rokov) nemusia vôbec javiť ako aktívne, no v prípade priaznivých podmienok (výdatné dažde, záplavy) dochádza k silnému pomnoženiu komárov a tie potom spôsobujú aktivizáciu ohniska nákazy.   Valtická horúčka.   Na štúdiu pôvodcu tejto choroby, má významný podiel  český odborník RNDr. Zdeněk Hubálek, DrSc z Ústavu biológie stavovcov akadémie vied ČR,  ktorý svoju bádateľskú činnosť realizuje v oblasti Valtíc na južnej Morave, z čoho pochádza aj  názov choroby.  Na druhej strane je potrebné spomenúť, že pôvodcu prvýkrát izolovali Bárdoš a Danielová v roku 1958 v blízkosti obce Ťahyňa na východnom Slovensku (časť Pavloviec nad Uhom). Bol to prvý komármi prenášaný virus izolovaný v strednej Európe a bol nazvaný práve podľa miesta jeho záchytu - Tahyna virus. Rezervoárom vírusu môžu byť zajace, ježe, hlodavce prípadne netopiere. Choroba u dospelých väčšinou prebehne bez príznakov, no u detí sa často prejaví. Má podobné príznaky ako chrípka a vyskytuje sa hlavne v lete a začiatkom jesene. Charakteristický je náhly nástup horúčky, bolesti svalov, hlavy, kĺbov, zápal hrtanu, nevoľnosť, zápal priedušiek a pľúc, zápal mozgu a postih centrálneho nervového systému. Úmrtnosť doteraz nebola zaznamenaná (Hubálek a Halouzka, 1996).  Od roku 1963 bolo na Slovensku a v ČR zdokumentovaných asi 200 prípadov. V roku 1976 bol tento virus príčinou 15% prípadov letných chorôb detí v ČR  (Hubálek a kol., 1979).   Západonílska horúčka.   Jej pôvodca tzv. West Nile Virus  je vo svete veľmi rozšírený. V Európe bol dokázaný v južnom Rusku, na Ukrajine, Moldavsku, Bielorusku, Rumunsku, Francúzku, Španielsku, Portugalsku, Maďarsku, Slovensku a v ČR  (Hubálek a Halouzka 1996). Najnebezpečnějším prenášačom tohto vírusu je druh komára Culex pipiens biotyp molestus, ktorý žije synantropne (blízko človeka) a často býva príčinou prepuknutia epidémie v mestách (Hubálek 2000). Na chorobu sú veľmi citlivé kone, u ktorých sa infekcia prejaví horúčkou a zápalom mozgu, stratou koordinácie, chvením svalov a úmrtnosťou okolo 30%. (Hubálek a Kříž, 2003). U Človeka je charakteristický náhly nástup horúčky trvajúcej 3 - 5 dní. Ďalšie príznaky sú bolesti hlavy, hrdla, pohybového aparátu, vyrážky na trupe a končatinách, únava, nechutenstvo a bolesti brucha. U 15 % prípadov sa objavuje zápal mozgu pečene, pankreasu a srdcového svalu. Zotavenie býva rýchlejšie u detí. Úmrtnosť sa pohybuje medzi 5 - 10 % a týka sa prevažne starších pacientov. V rokoch 1996 - 2000 prebehli v Európe dve veľké epidémie: juhorumunská s 835 prípadmi a juhoruská s 826 prípadmi, z ktorých bolo 40 smrteľných (Hubálek a Kříž, 2003).V roku 1997 podľa štúdie vykonanej na Břeclavsku po povodniach bola akútna infekcia zistená u 2 dospelých a 2 detí. Jednalo sa o prvý dôkaz tohto ochorenia ľudí v strednej Európe (Hubálek a kol. 1999).   Biologický cyklus vírusu západonílskej horúčky.      Horúčka Sindbis.   Pôvodcu choroby prvýkrát izolovali v roku 1955 v egyptskej dedine Sindbis. V Európe bol po prvýkrát zistený na Slovensku blízko Malaciek (Hubálek a Halouzka 1996). Hlavnými hostiteľmi v prírode sú vtáky. Vírus u ľudí vyvoláva chorobu, prejavujúcu sa 3 - 4 dennou horúčkou, bolesťami hlavy, pohybového aparátu a miernou žltačkou. (Grešíková a Nosek,  1981). Niekedy sa tiež vyskytujú vyrážky na hrudníku a končatinách. Akútna fáza trvá do 10 dní, ale únava a bolesti šliach môžu pretrvávať aj niekoľko mesiacov pričom sa môže vyvinúť chronický zápal kĺbov (Hubálek a Halouzka, 1996). V roku 1981 boli zaznamenané stovky prípadov v Škandinávii.   Vírus Batai (Čalovo).   Patrí do rovnakej čeľade ako virus Ťahyňa a v Európe bol prvýkrát izolovaný v roku 1960 v oblasti Čalova na južnom Slovensku (rovnako je aj tento typ nazvaný podľa miesta záchytu). Vírus sa vyskytuje v poľnohospodárskych lokalitách, kde pravdepodobne koluje medzi komármi a domácimi prežúvavcami (Hubálek a Halouzka, 1996). U ľudí infekcia prebieha ako horúčkovité ochorenie podobné chrípke. Pri sledovaní protilátok v ČR v roku 2004 bol kontakt s týmto vírusom potvrdený u 7 zo 497 vyšetrených.   Identifikácia.   Napriek závažnosti spomínaných ochorení je ich výskyt relatívne málo zdokumentovaný. Príčinou je systém diagnostiky chrípke podobných chorôb. Na presné určenie ich pôvodcu je totiž potrebné odobratie vzoriek a ich laboratórne testovanie, čo sa vo všeobecnosti nerobí. Tieto ochorenia sa spravidla riešia tlmením príznakov (symptomatická terapia) a evidujú sa ako chrípke podobné respiračné ochorenia. Samozrejme v indikovaných prípadoch ako je v súčasnosti prebiehajúca prasacia chrípka sa diagnostika vykonáva, avšak je zameraná iba na potvrdenie či vylúčenie pôvodcu tejto chrípky.   Tento článok je informatívny s cieľom priblížiť problematiku  laickej verejnosti a pre tento účel bola upravená aj jeho odborná stránka.       Citovaná literatúra.   Grešíková M., Nosek J. (1981): Arbovírusy v Československu. SAV, Bratislava. 140                         Hubálek Z., Bárdoš V., Medek M., Kania V., Krychler L., Jelínek E. (1979): Ťahyňa virus- neutralizační protilátky pacientů na jiţní Moravě. Čs. Epidemiol. Mikrobiol. Imunol. 28: 87-96.                                     Hubálek Z., Halouzka J. (1996): Arthropod-borne viruses of vertebrates in Europe. Acta Sc. Nat. Brno. 30: 95                                                                                                                                                 Hubálek Z., Halouzka J., Juřicová Z., Příkazský Z. (1999): Surveillance virů přenosných komáry na Břeclavsku v povodňovém roce 1997. Epidem. Mikrobiol. Imunol. 48: 91- 96                                   Hubálek Z. (2000): European experience with the West Nile virus ecology and epidemiology: Could it be relevant for the New World. Viral. Immunol. 13: 415-426                                         Hubálek Z., Kříž B. (2003): Západonilská horečka. Klin. mikrobiol. inf. lék. 9: 59-68                      Hubálek Z., Zeman P., Halouzka J., Juřicová Z., Šťovíčková E., Bálková H., Šikulová S., Rudolf I. (2004): Protilátky k virům přenosným komáry u středočeské populace z oblasti zasaţené povodní v roce 2002. Epidem. Mikrobiol. Imunol. 53: 112-120 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Probiotické potraviny – inovácia aj pridaná hodnota.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Drahé potraviny - výsledok spoločnej poľnohospodárskej politiky EÚ.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Spoločná poľnohospodárska politika EÚ – fair play a la Brusel.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Sme krajinou psieho utrpenia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Holko 
                                        
                                            Mlieko z automatu - prevárať či neprevárať?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Holko
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Holko
            
         
        holko.blog.sme.sk (rss)
         
                                     
     
        Veterinár, vedec, odborný poradca v poľnohospodárstve. 
                                                      

Feedjit Live Blog Stats

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    26
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2997
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Moja spoločenská aktivita
                                     
                                                                             
                                            Moja práca
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




