
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Červeň
                                        &gt;
                Spoločnosť
                     
                 Ako sa nám Ficko zaťau... :-) 

        
            
                                    5.3.2010
            o
            11:15
                        (upravené
                5.3.2010
                o
                11:22)
                        |
            Karma článku:
                24.98
            |
            Prečítané 
            7736-krát
                    
         
     
         
             

                 
                    Pán súdruh premiér sa vyjadril, že sa už nebude vyjadrovať k emisiám, ktoré ešte donedávna obhajoval, lebo už zistil, že nie je čo obhajovať. Je zaujímavé sledovať ako niekto dokáže tie isté informácie použiť najskôr na horlivú obhajobu a potom na horlivé kritizovanie. Vieme všetci, že každý by na jeho mieste najradšej zaliezol niekde do kúta a hanbil sa ako pes, ktorý zlízal z narodeninovej torty všetok marcipán, a ktorého pristihli priamo pri čine. Lenže to nejde vyhovárať sa vtedy, keď počas celej kauzy boli u našich nacionalistov prítomní jeho ľudia. Nemôže sa tváriť ako neviniatko ani teraz, keď nacionalisti aj vďaka hlasom jeho strany pretlačili prvoaprílový zákon, ktorý poriadne naštval veľkú skupinu študentov a učiteľov. Naštvať študentov sa nevypláca, na to doplatili už jeho priami predchodcovia kamoši komouši v roku 1989...
                 

                 
Nič nepoviem, môžete mi...http://i.pravda.sk
  Ako by mu nestačilo rozoštvať študentov, skúša nakoľko sú mu verní aj jeho dôchodcovia, ktorým bez problémov siahol na ich obľúbené výpredaje v hypermarketoch. Keď to človek zosumarizuje a zamyslí sa nad jeho plagátovým heslom "Ide o ľudí a ide o Slovensko", tak mu napadne o akých ľudí mu vlastne ide a o aké Slovensko. Vieme, že mu ide o jeho ľudí. Veď o to ide každému. Možno maličký rozdiel je v tom, že málokto sa zastaví len pri tom. O aké Slovensko mu ide, je vidieť nie z jeho predvolebných sľubov, pretože tie má len pre oko diváka, ale z jeho skutkov. O Slovensko socialistické, klientelistické, nacionalistické, centralizované s veľmi silným vplyvom štátu, kde sa len dá. O Slovensko so zhoršujúcim sa podnikateľským prostredím, s čoraz horšou vymáhateľnosťou práva, s čoraz väčším porušovaním základných práv a slobôd človeka... Keby bol dedičným vládcom v nejakej monarchii, museli by sme držať hubu a krok, pretože on by bol naším pánom. V demokracii je však naším zamestnancom. Nemôže si svojmu zamestnávateľovi dovoliť povedať, že nevysvetlí, kde pomíňal peniaze, ktoré mu zveril. Nemôže si dovoliť prijímať také zákony, ktoré jeho a jeho kumpánov zvýhodňujú, a ktoré zamestnávateľa stoja čoraz väčšie prostriedky. Nemôže sa zaťať a tváriť sa ako drzý žiak pred tabuľou, ktorý učiteľke povie, že odpoveď vie, ale nepovie, a že mu aj tak nedá päťku, lebo jeho otec je sponzor školy. Zamestnávateľ má právo vedieť ako jeho zamestnanec nakladá s jeho majetkom. Má právo vyžadovať od zamestnanca, aby sa prestal venovať svojim fuškám, a aby začal pracovať na tom, kvôli čomu bol do zamestnania prijatý. V opačnom prípade ho musí prepustiť z práce skôr, kým nenarobí ešte väčšie škody.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (77)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Fico vymetá všetky kúty aj v osadách...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Budú Fica oblizovať aj zozadu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Napíš europoslancovi, nech neblbne...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Ukrajina potrebuje EÚ ale aj EÚ potrebuje Ukrajinu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Skončí postávanie pred kostolom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Červeň
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Červeň
            
         
        cerven.blog.sme.sk (rss)
         
                        VIP
                             
     
        Mám rád život s Bohom a 7777 ľudí. S obľubou lietam v dobrej letke, no nerád lietam v kŕdli. Som katolícky kňaz, pochádzam z dediny Klin na Orave. Farár vo farnosti Rakúsy. (pri Kežmarku)
...........................................


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1475
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3094
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            BIRMOVANCI
                        
                     
                                     
                        
                            ŠTIPENDISTI
                        
                     
                                     
                        
                            7777 MYŠLIENOK PRE NÁROČNÝCH
                        
                     
                                     
                        
                            Aké mám povolanie?
                        
                     
                                     
                        
                            Akú mám povahu?
                        
                     
                                     
                        
                            Cesta viery
                        
                     
                                     
                        
                            Duchovné témy
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Fotografie prírody
                        
                     
                                     
                        
                            Fotografie Rómov
                        
                     
                                     
                        
                            História mojej rodnej obce
                        
                     
                                     
                        
                            Kniha o Rómoch
                        
                     
                                     
                        
                            Lunikovo
                        
                     
                                     
                        
                            Mladí vo firme
                        
                     
                                     
                        
                            Náčrt histórie slov. Saleziáno
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Pastorácia Rómov
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Pokec s Bohom
                        
                     
                                     
                        
                            Rómovia
                        
                     
                                     
                        
                            Sociálna práca
                        
                     
                                     
                        
                            Sociálna náuka Katolíckej cirk
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Vedeli sa obetovať
                        
                     
                                     
                        
                            Vzťah je cesta cez púšť
                        
                     
                                     
                        
                            Zábava
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Štipendium pre chudobné zodpovedné dieťa
                                     
                                                                             
                                            Prečo si firmy neudržia mladých ľudí?
                                     
                                                                             
                                            Dejiny mojej dediny
                                     
                                                                             
                                            Vzťah je cesta cez púšť
                                     
                                                                             
                                            Akú mám povahu?
                                     
                                                                             
                                            7777 MYŠLIENOK PRE NÁROČNÝCH
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sociálny kódex Cirkvi - Raimondo Spiazzi
                                     
                                                                             
                                            Sociálna práca - Anna Tokárová a kolektív
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Free Download MP3 ruzenec
                                     
                                                                             
                                            MP3 ruženec
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Krištofóry - katolícky liberál
                                     
                                                                             
                                            Peter Herman - realistický idealista
                                     
                                                                             
                                            Michal Hudec sa nebojí písať otvorene
                                     
                                                                             
                                            Mária Kohutiarová - žena, matka, človek
                                     
                                                                             
                                            Branislav Sepeši - lekár o etike
                                     
                                                                             
                                            Katrína Janíková - neobyčajne obyčajná baba
                                     
                                                                             
                                            Martin Šabo - šikovný redemptorista
                                     
                                                                             
                                            Juraj Drobny - veci medzi nebom a zemou
                                     
                                                                             
                                            Peter Pecuš a jeho postrehy zo života
                                     
                                                                             
                                            Miroslav Lettrich - svet, spoločnosť, hodnoty
                                     
                                                                             
                                            Karol Hradský - témy dnešných dní
                                     
                                                                             
                                            Salezián Robo Flamík hľadá viac
                                     
                                                                             
                                            Salezián Maroš Peciar - fajn chlap
                                     
                                                                             
                                            Motorka a cesty: Martin Dinuš
                                     
                                                                             
                                            Pavel Škoda chodí s otvorenými očami
                                     
                                                                             
                                            Ďuro Čižmárik a jeho mozaika maličkostí
                                     
                                                                             
                                            Paľo Medár a scientológovia
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Modlitba breviára
                                     
                                                                             
                                            Nezabíjajte deti!
                                     
                                                                             
                                            Občiansky denník
                                     
                                                                             
                                            Dunčo hľadaj
                                     
                                                                             
                                            Správy z Cirkvi
                                     
                                                                             
                                            Prvý kresťanský portál
                                     
                                                                             
                                            Lentus
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voľby nemožno vyhrať kúpenými hlasmi najbiednejších Rómov!
                     
                                                         
                       Jednoduchý rozdiel medzi kandidátmi na prezidenta.
                     
                                                         
                       Doprajme Ficovi pokoru!
                     
                                                         
                       Fico vymetá všetky kúty aj v osadách...
                     
                                                         
                       tam kam Fico nemôže, TA3 mu pomôže
                     
                                                         
                       Vďakyvzdanie
                     
                                                         
                       Treba homofóbiu liečiť?
                     
                                                         
                       Procházka, Kňažko, Kiska... a jediný hlas
                     
                                                         
                       Napíš europoslancovi, nech neblbne...
                     
                                                         
                       Prezidentské voľby: Prečo budem voliť Procházku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




