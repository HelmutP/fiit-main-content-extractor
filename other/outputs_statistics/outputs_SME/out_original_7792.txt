
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Do volieb s požehnaním 

        
            
                                    1.6.2010
            o
            17:37
                        (upravené
                1.6.2010
                o
                19:35)
                        |
            Karma článku:
                3.98
            |
            Prečítané 
            604-krát
                    
         
     
         
             

                 
                    Patrím celkom jednoznačne k ľuďom, čo svoj názor neskrývajú ako pirátsky poklad na voľajakom tajnom a dobre zabezpečenom mieste, naopak celkom pokojne ho prezentujem a rád o ňom hovorím, lebo žiadnym iným spôsobom sa nedokážete lepšie predstaviť. Buďte si však istý, že úskalia takejto otvorenosti poznám veľmi dobre, skúsenosti a prax ma to naučili viac ako presvedčivo a naviac, moji obaja dedovia, ktorý takýto „zlozvyk“ do mňa naočkovali to vždy znova a znova podrobovali skúškam a testom. Boli to tí najúžasnejší učitelia, celkom mimoriadne osobnosti a hoci bol každý celkom iný vizážou a tak trošku aj dátumom narodenia a naozaj celkom iným životopisom, ich charaktery boli ako vybrúsené drahé kamene. Vždy som sa im chcel podobať, nie vždy sa mi to darilo, no nevzdával som sa. Aj v mojom životopise to vidieť a cítiť a keď na mňa zhora hľadia, celkom iste mi nič nevyčítajú.
                 

                     A načo takýto vysvetľujúci úvod!? Lebo raz darmo, v živote okolo nás sú veci a udalosti, okolo ktorých sa dá chodiť v slepote i hluchote, lebo je naozaj lepšie nevidieť a nepočuť, veď nenadarmo sa hovorí, opatrnosť matka múdrosti. Hovoriť na Slovensku o určitých veciach môže byť začiatkom problémov a v tejto súvislosti priznávam, že už viac ako mesiac píšem reportážny článoček o vzťahoch bývalého a súčasného trnavského arcibiskupa predovšetkým preto, že toho bývalého osobne poznám, niekoľkokrát som s jeho eminenciou osobne rokoval a jeho reakcie a chovanie som si nielen dobre zapamätal, ale veru aj dôkladne zaznamenal a analyzoval. Možno ho ponúknem vašej zvedavosti aj na blogu, lebo vysvetľuje mnohé z reálnych udalostí, ktoré našou veriacou komunitou riadne zatriasli, no tento článok má iné poslanie.   A vylučujem aj podozrenie, že sa chcem zamerať na najväčší problém svetovej súčasnosti v súvislosti so zločinmi cirkevných hodnostárov či obyčajných kňazov v tak citlivej sfére akou je pedofília, homosexualita, fyzické násilie, ako to s úmyslom veci dramatizovať preferuje nielen seriózna mediálna sféra. Vôbec nie, hoci aj v našej proveniencii sa nájdu výstrahy i mementá a ani naše životy nie sú oslobodené od negatívneho a neprajného posudzovania kvalít našich slovenských duchovných osobností. Naopak, chcem hovoriť akurát o tom, ako sa aj oni prezentujú, ako aj oni vyslovujú svoj názor a ako sa aj oni dobrovoľne posúvajú do centra pozornosti všetkých! Hovorím o tom, že pred týmito voľbami, tak ako už viackrát predtým sa cirkev rozhodla, že, prepáčte terminus technicus, svojim ovečkám odporučí účasť na voľbách.   Jasné, sú tam určité doplňujúce a sprievodné pokyny, ten hlavný som si prečítal s uspokojením, lebo naši biskupi predpokladajú, že takto vyslovené slová budú nekonkretizované a nie sú žiadnym diktátom! O to dôležitejší je tento postoj ak si uvedomíme, že cirkevná sféra súčasnosti je jednoducho nekompatibilná vo všetkých zložkách, že jej jednota je narušená a predstavte si, jeho eminencia spišský  biskup František Tondra mal potrebu publikovať stanovisko KBS v médiách voči vznesenému obvineniu, že cirkev u nás je poplatná tejto vláde, vychádza jej v ústrety až nepochopiteľne a tak trošku aj proti sebe a je v kríze! Pripomínam všetkým, toto stanovisko je napísané s nadhľadom, všetko konfliktné tak trošku zľahčuje cirkevnými istotami, ale kto chce vedieť pravdu rýchlo zistí, že vyhlásenie je aj tak ráznym varovaním pred eskaláciou avizovaných rozdielnych názorov.   Ak však hádate, že tento článok nebude ani o tomto načrtnutom spore Jána K. Balásza s KBS máte zasa pravdu, lebo to všetko je ešte príliš čerstvé a mále preukazné a tak sa vraciam znova k voľbám. Lebo už je to naozaj tak, že napriek môjmu životnému krédu, že za osobný názor sa netreba hanbiť, sa cirkvi veru podaril poriadny prešľap. A čo je horšie, avizovaný a v priamom rozpore s tým, čo svojim kňazom a ovečkám vo farnostiach v dobrej viere adresovali naši najvyšší preláti.   Akurát v spišskej diecéze, ktorá je spravovaná arcibiskupom Tondrom vznikla iniciatíva vyzvať veriacich, aby volili KDH. Podpis jej venoval aj spišský súdny vikár Ján Duda, ktorý už sa „preslávil“ kampaňou voči Ivete Radičovej a musím pripomenúť, že tejto líderke strany SDKÚ sa to vôbec nepáči ani teraz! A nielen jej, dal sa počuť aj predseda parlamentu Pavol Paška a ten by považoval za korektnejšie, keby sa sekularizmus posunul niekde celkom inde. A veru aj SNS, keď jej stanovisko vyznelo naozaj adresne – dobrí kresťania sú aj v iných politických stranách!   A tak sa v najbližšiu nedeľu kresťania v svojich kostoloch dožijú pastierskeho listu biskupov, kde bude všeobecné odporúčanie ísť voliť, ale viac ako isté je veru aj to, že iniciatíva mnohých kňazoch prekročí rubikon dobrých zvyklostí a to svoje – voľte KDH – veriacim vnúti ako niečo v mene Božom. Počul som, že KBS to nekomentuje, to nám biskupi odkázali cez svojho hovorcu Jozefa Kováčika a mne nedá, aby som svoju povahovú danosť nielen avizoval, ale veru aj doložil realitou.   Vážení cirkevní hodnostári, to celkom iste nie je Božie chcenie, aj vy ste len ľudia z mäsa a kostí, smrteľní, hriešni a omylní, nič vás neoprávňuje vysloviť takéto kacírstvo vŕšiace sa na spoluobčanoch len a len preto, že ste v úrade a máte moc! Ľudská dôstojnosť je boží predikát a vaša pozemská malosť je minimálne taká istá danosť, nič viac a nič menej! Kto sa povyšuje, býva ponížený, aj podľa tohto Božieho slova by ste mali žiť! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




