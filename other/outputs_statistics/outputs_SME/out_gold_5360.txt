

 No osud, darebák, 
 daroval mi lásky znak, 
 že hoci proti svojim zásadám 
 nepatrím k dobrým návnadám. 
   
 Meč mi len jemne kožu porezal, 
 no dušu spútal, do pekla vzal, 
 kde by sa moje chcladné vlčie telo 
 horúcim a zdravým stať smelo. 
   
 Milánku, na krku mám jazvu, 
 srdce a duša do záhuby ma zvú, 
 no kat nechce skrátiť moje trápenie, 
 odporúčil mi nosiť brnenie, 
   
 no ja stále chodím nahá, 
 hlavu pod gilotínu vkladám. 

