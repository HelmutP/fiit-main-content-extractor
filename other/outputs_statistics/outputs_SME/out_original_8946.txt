
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jano Topercer
                                        &gt;
                Ekoblógia
                     
                 Diaľničenie slovenskej prírody II. 

        
            
                                    16.6.2010
            o
            10:42
                        (upravené
                16.6.2010
                o
                11:04)
                        |
            Karma článku:
                9.21
            |
            Prečítané 
            1367-krát
                    
         
     
         
             

                 
                    Slovenská inšpekcia životného prostredia v Žiline objasnila môj podnet z 26. apríla 2010 a podnet Správy NP Veľká Fatra vo Vrútkach z 29. apríla 2010, upozorňujúci na zásahy do Prírodnej rezervácie Rojkovské rašelinisko (RK) pri prípravných prácach na diaľnici D1 Turany - Hubová v apríli 2010 (pozrite aj tu). Zápis o výsledku objasňovania podnetov z 9. júna 2010 je rozsiahly (11 strán), preto z neho odcitujem len záver.
                 

                 Slovenská inšpekcia životného prostredia - Inšpektorát životného prostredia Žilina, odbor inšpekcie ochrany prírody a krajiny na základe vykonaného objasňovania podnetu samostatného vedeckého pracovníka Botanickej záhrady UK Blatnica a podnetu Správy NP Veľká Fatra na vyšetrenie výkopových prác a výrubu krovitých porastov v ochrannom pásme i vo vlastnom území Prírodnej rezervácie Rojkovské rašelinisko zistila porušenie ustanovení zákona č. 543/2002 Z. z. o ochrane prírody a krajiny v znení neskorších predpisov (ďalej len "zákon"):   - spoločnosťou HARD FOREST, ktorá v mesiacoch február - marec 2010 vykonala výrub drevín na území PR Rojkovské rašelinisko a jej ochranného pásma v rozpore s ustanovením § 47 ods. 3 zákona a § 15 ods. 1 písm. e) zákona;   - spoločnosťou BETAMONT, ktorá v mesiaci apríl 2010 výkopovými prácami v ochrannom pásme PR Rojkovské rašelinisko zasiahla do biotopu európskeho významu a biotopov národného významu spôsobom, ktorým došlo k ich poškodeniu, bez súhlasu orgánu ochrany prírody podľa ustanovenia § 6 ods. 1 zákona, platného v čase vykonávania zásahov;   - spoločnosťou Doprastav, ktorá v mesiaci máj 2010 na parcele KN č. 4234/1 v k. ú. Stankovany, na ktorej platí tzv. predbežná ochrana ÚEV so štvrtým stupňom ochrany, oplotila pozemok, teda vykonala činnosť zakázanú podľa § 15 ods. 1 písm. g) zákona.   Podľa doplnenej informácie zo SIŽP v Žiline poslali citovaný zápis 10. júna 2010 aj dotknutým spoločnostiam, ktoré sa môžu k jeho obsahu vyjadriť. Nemôžu sa však voči nemu odvolať, pretože nejde o rozhodnutie vydané v správnom konaní. Inšpektorát životného prostredia SIŽP v Žiline na základe zistení pri objasňovaní našich podnetov začne voči dotknutým spoločnostiam správne konanie o porušení ustanovení zákona, výsledkom ktorého môže byť aj rozhodnutie o uložení sankcie za protiprávne konanie (§ 90 a nasl. zákona). V tejto fáze konania však postihy ešte nemožno konkretizovať, lebo budú predmetom konania o inom správnom delikte.   K týmto informáciám SIŽP v Žiline nemám čo dodať - len poďakovanie inšpektorom za vykonanú prácu a želanie, aby aj ich pričinením jedinečné Rojkovské rašelinisko (a riečny ekosystém Váhu a sutinové lipové javoriny na päte severných svahov Kopy a migračné trasy veľkých šeliem atď.) už neboli predmetom takých biotechnicky nedotiahnutých experimentov, ako je/bol PPP projekt tzv. povrchovo-tunelového variantu diaľnice D1 Turany - Hubová. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (36)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            D1 — jednotka degenerácie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            Nerušiť, iterujú!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            D1 Turany — Hubová kontra Natura 2000
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            Šúľky pálky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jano Topercer 
                                        
                                            Panská vôlenka II.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jano Topercer
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jano Topercer
            
         
        topercer.blog.sme.sk (rss)
         
                        VIP
                             
     
        ekológ (=zoológ v botanickej záhrade:)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    292
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    460
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ekoblógia
                        
                     
                                     
                        
                            Qvety kalamity
                        
                     
                                     
                        
                            Eva-Lucia &amp; Filová Žofia
                        
                     
                                     
                        
                            ZOO Politico
                        
                     
                                     
                        
                            Ľudmila Populovna
                        
                     
                                     
                        
                            Aforizmy
                        
                     
                                     
                        
                            Poetree
                        
                     
                                     
                        
                            Iraciol
                        
                     
                                     
                        
                            Mikrozprávky
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




