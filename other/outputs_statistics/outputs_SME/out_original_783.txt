
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Iliaš
                                        &gt;
                Izrael, ó môj Izrael
                     
                 Jana Shemesh: nech je Izrael v očiach iných hoci aj slabý 

        
            
                                    23.9.2008
            o
            14:43
                        |
            Karma článku:
                10.86
            |
            Prečítané 
            6932-krát
                    
         
     
         
             

                 
                    Jana Shemesh (za slobodna Mikušová)  je zahraničnou reportérkou denníka SME pre oblasť Blízkeho východu. Čitatelia SME ju poznajú zo nielen spravodajských článkov, ale aj z obľúbenej sobotnej rubriky "Izraelský zápisník". Ako vidí život, politiku a náboženstvo v židovskom štáte možno napovie aj tento rozhovor.
                 

                 
Jana Shemesh, izraelská dopisovate3ka pre denník SME. www.SME.sk
    1. Z článkov v SME vyplýva, že žijete s manželom a dieťaťom v Izraeli. Mohli by ste viac  povedat o sebe - ako ste sa dostali k žurnalistike a najmä - ako ste nakoniec zakotvili prístave manželskom v židovskej rodine v Tel Avive ?           K žurnalistike som dostala ako väčšina vyštudovaných žurnalistov - hľadala som školu, kde nebude matematika. Iný špeciálny či vznešený dôvod si ani velmi nepamätám. Úprimne, baviť ma to začalo až po čase. V prístave manželskom, ako to nazývate, som zakotvila po tom, čo som ako novinárka cestvovala do Izraela a stretla v Tel Avive svojho budúceho manžela. Keďže som si úprimne nevedela predstaviť ako by existoval na Slovensku, nemali sme veľmi na výber. V Tel Avive sa narodila aj naša teraz 13-mesačná dcéra Ruth. Pred časom sme sa presťahovali do Petach Tikva, čo je mesto neďaleko Tel Avivu.             2. Najskôr poblahoželám (aj keď až po trinástich mesiacoch) k prírastku do Vašej rodiny, pani Jana. Môžem sa opýtať, prečo ste Vašej ratolesti dali meno Rút ? Vari v tom bola inšpirácia biblickou knihou Rút ?            Ruth je Ruth pretože je to pekné meno a používa sa v Izraeli aj v Európe. Inšpirácia knihou Ruth tam trochu bola. Je to príbeh o súcite aj o tom, ako sa stať Židom z presvedčenia. A je tam aj krásny paradox, že sám kráľ Dávid je potomkom ženy nežidovského pôvodu.                 3. V príspevku do ženského časopisu  ste charakterizovali izraelské ženy. Vedeli by ste stručne ohodnotiť aj izraelských mužov ?            Ani nie. Ale keď sa pozerám na svojho muža, príde mi, že sú veľmi rozmaznaní a tiež radi rozmaznávajú.                               4. Rozhodli ste sa teda usadiť v Izraeli namiesto toho, aby ste s manželom prišli na Slovensko. S čím ste sa museli najviac boriť ? Ako sa Vám páči život v tejto krajine ?               Borím sa dennodenne. S izraelskou dobrosrdečnosťou, ktorá zachádza do drzosti, s tuhým objatím rodiny, ktoré je občas bolestivé a nepustí, aj s pocitmi smútku za domovom a občas aj otázkou, či mi to stálo za to. Na druhej strane Izrael je mimoriadne krásna a inšpiratívna krajina, a napriek všetkému ju nateraz považujem za najlepšie miesto na výchovu našej dcéry.                    5. Čo Vás najviac odrádzalo na Slovensku, keď ste dali prednosť Izraelu ?          Otázka nestojí, čo nás odrádzalo. Izrael je pre mňa pracovná výzva, cudzinec sa tu ľahšie dohovorí a aj počasie je lepšie. A veľkú úlohu pri rozhodovaní hralo aj to, že napríklad služby či zdravotníctvo sú v Izraeli na oveľa lepšej úrovni.                               .Politika                               6. Stiahnutia Izraela z južného Libanonu v roku 2000 a z Gazy v roku 2005 viedli k posilneniu hnutí Hizballáh v Libanone a Hamas v Gaze. Stálo to Izraelu za to ?                     Na to sa nedá jednoznačne odpovedať. Vtedajšia spoločenská aj politicka objednávka bola taká, že sa to zdalo ako najlepšie a nevyhnutné riešenie. Posudzovať to teraz je lacné. To, že Hamas a Hizballáh z toho profitujú, je jasné. Ale len Hamas a Hizballáh. Život Libanoncov a Palestínčanov sa v zmysle každodenneho prežívania sa určite nezlepšil.                            7. Aj keď sa terajšie posudzovanie zdá 'lacné', už pred stiahnutím (hovorme teraz konkrétne o Gaze) sa ozývali kritické hlasy, poukazujúce na bezpečnostné riziká. Komunity okolo Gazy to po stiahnuti pocítili v podobe zvýšeného ostreľovania po domácky vyrobených rakiet Kassam, plus objavili sa i sofistikovanejšie rakety Grad, ktoré teroristi dokázali prepašovať do Gazy.    Opýtam sa - akú predpokladáte 'spoločensku objednávku' vo veci stiahnutia sa zo Západného brehu ?           Ale ved to je v poriadku. Bezpečnostne riziká boli, nikomu sa však nepodarilo zastaviť Šaronovu vládu - ani v parlamente, ani v uliciah, aby Gazu opustila. To je demokracia, aj keď mnohým Šaronovým voličom sa ani nesnívalo, že by niečo také mohol urobiť. Spoločenska objednávka v prípade Západneho brehu je oveľa komplikovanejšia. Z náboženského, strategického, ale aj ekonomického hľadiska. Ja nevidím v súčasnosti na izraelskej politickej scéne lídra, ktorý by bol niečo také schopný vykonať. A nemyslím si, že Izraelčania po tom, aj s ohľadom na Gazu, prahnú. Nejaké riešenie ale bude musiet prísť, lebo zdá sa, že budúci palestínsky štát je nezvratná záležitosť.                         8. V článku v SME obšírne zdôvodňujete, prečo Izrael pristúpil na výmenu vraha za telá dvoch svojich vojakov. Starodávne zvyklosti, mediálny marketing, spoločenský pátos, mladá vdova atď. Mne osobne chýba analýza slabosti izraelskej spoločnosti a ňou zvolenej vlady, keď pristupuje s teroristami na takéto 'obchody'. Vidíte to aj Vy ako slabosť a ak áno, vedeli by ste ju vystihnúť ?           Ja by som o slabosti nehovorila, aj keď som hlboko presvedčená, že tá dohoda s Hizballáhom je zlá. Slabosť v čom? Že sa štát zachoval slušne k rodinám, čo poslali to najlepšie, čo majú, na jeho obranu? Problémom podľa mňa je, že to rozhodnutie urobila slabá vláda s problematickým premiérom, a to ho spochybňuje. Druhá zrada tej dohody spočíva v tom, že Hamas, ktorý drží Gilada Šalita, teraz vidí, že môže stupňovať cenu. Ale o slabosti by som nehovorila. Ak si Izrael myslí, že môže ustať svoje morálne víťazstvo, nech je v očiach iných hoci aj slabý. Je silný Hizballáh, že oslavuje sprostého vraha dieťaťa? A totálnu slabosť prejavili takzvaní prozápadní libanonskí politici, ktorí Kuntara vítali ako národného hrdinu. To je dôkaz, ze Bushov projekt demokratizácie Blízkeho východu je bohužiaľ len chimérou.                         9. Židovské osídlenia na Západnom brehu (Júdea a Samária): v tejto otázke na jednej strane rozdvojenej izraelskej spoločnosti stoja sionistickí náboženskí osadníci, na strane druhej ľavica a ľavicove združenia ako "Mier ihneď" usilujúce sa, aby Izrael úplne opustil sporné teritória v prospech budúceho palestínskeho štátu. Ako sa na tento problematický bod pozeráte ?           Na Západnom brehu nejde len o nejaké osady s pár karavanmi a náboženskými fanatikmi. Ide o mestečká, v ktorých  ľudia nežijú len z ideologických, ale často aj ekonomických dôvodov. Izrael podľa mňa nikdy neopustí celý Západný breh, bude však musieť urobiť niečo, aby budúci palestínsky štát bol ako tak celistvý. Ale stav mierových rozhovorov aj reprezentácii na oboch stranách je taký, že to asi tak skoro nebude.             10. Bezpečnostné (alebo ako hovoria ich odporcovia - separačné) ploty sú kontroverznou témou. Ľavicove a propalestínske organizácie, často so zahraničnou účasťou, organizujú protesty proti ich stavaniu, kedže rozdeľujú pôdu vlastníkov a de fakto vytyčuju izraelský a palestínsky priestor.    Myslíte si, že stavanie plotov prispeje k riešeniu izraelsko-palestinského konfliktu ?          Stavanie múrov a ich búranie je podľa mňa dosť sprofanovaná fráza. Faktom je, že počet teroristických útokov od postavenia bezpečnostnej bariéry preukázateľne klesol a to hovorá za všetko. A napokon múr sa da vždy zbúrať, stratený život obetí teroristického útoku nikto nevráti.                              11. Interpretácia izraelsko-arabského konflitku a celkovo histórie vzniku Izraela je kontroverzná téma. Napríklad arabista Emo Beška na blogu Slovak Press Watch tvrdí, že "jeden z hlavných dôvodov, ktorý vohnal arabské štáty do vojny" bol vnútorný konflikt medzi Židmi a palestínskymi Arabmi, kedy "státisí­ce Arabov boli vyhnaných zo svojich domovo ešte pred vypuknutim vojny (za nezavislosť v roku 1948, pozn.autora)".    Ako hodnotíte tieto rozdielne interpretácie ?          Nehodnotím. Ja nie som historička. Všetko závisí od zdrojov, na ktoré sa odvolávate. Arabi hovoria, že ich vyhnali Židia, Židia hovoria, ze odišli sami. Faktom, je že keby Arabi prijali plán na rozdelenie Palestíny, tak by žiadni utečenci neboli. Faktom je aj to, ze židovskí utečenci z arabských krajín - a o tých sa z rôznych dôvodov príliš nehovorí - dostali po príchode do Izraela všetky občianske práva. Palestínčania v Libanone či Jordánsku žijú ako občania druhej triedy v utečeneckých táboroch. A sú nástrojom vydierania pri vyjednávaní o mieri.                 12. Štatút Jeruzalema je osobitnou kapitolou v izraelsko-palestínskych vzťahoch. Izrael ho chce mať celý zjednotený (t.j. aj s východným Jeruzalemom, obývaným arabským obyvateľstvom) a pod svojou kontrolou, ako ju získal v roku 1967. Palestínska strana by zase chcela mat východný Jeruzalem ako svoje hlavné mesto. Svet zase uvažuje o akomsi medzinárodnom meste.    Aký výhľad mate pre Jeruzalem ? Stane sa podla Vás v budúcnosti rozdeleným ?            Nie som prorok. Ale myslím si, že v tejto generácii s Jeruzalemom nikto nepohne.                       .Náboženstvo         13. Hoci v počiatkoch sionizmu 19. a 20.storočia boli jeho idey prakticky pretavované do osídlení v zemi patriarchov hlavne sekulárnymi Židmi ("kibucnikmi"), v súčasnej dobe "prvé husle" sionizmu hrajú pravicoví naboženskí osadníci, ktorí sa, nazvime to zubami-nechtami, držia svojich obydlí na Západnom brehu. Pre nich je povinnosť osídlovať zem zasľúbenú (súčasné sporné územia), a v nej dodržiavať micvót  (prikázania Tóry) až do príchodu Mesiáša, ktorý vykúpi Izrael.    Aký je Váš názor na ich  vieru a na ich politicke zastrešenie v parlamente stranou NU-NRP (National Union - National Religious Party) ?             Do veci viery sa nemiešam, kazdý ma svoje presvedčenie. Čo sa mi už nepáči je zaťahovanie náboženstva- akéhokoľvek do politiky. Program náboženských strán v Izraeli sa bohužiaľ scvrkol na vyjednávanie sociálnych dávok a nekritické ľpenie na najortodoxnejšom výklade judaizmu.           14. Zaťahovanie náboženstva do politiky, ako píšete, je mnohokrát kritizovaniahodne. No zoberme to realisticky: i náboženstvom motivované masy ľudí, respektíve ich politickí reprezentanti, činia obzvlášť v Izraeli (ale aj kdekolvek inde) politiku, nech je už akákoľvek.     Vyvstáva preto pragmatická otázka, ako tento hrubý zlepenec náboženskej politiky v Izraeli čo najlepsie menežovat (zvýšiť dávky ultraortodoxným ? povoliť osadu náboženským sionistom ? povoliť moslimom dalšiu mešitu ? atď)    Ako by ste Vy postupovali, ak by ste boli napríklad  v pozícii správcu naboženskych záležitosti v Izraeli ?             To je príliš hypotetická otázka. Naviac nemám samovražedné sklony.                         Rozhovor bol urobený formou ímejlovej komunikácie v rozpätí  12.7.2008- 7.8.2008. Ilustračné obrázky  z internetu, pozmenšované.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (19)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Iliaš 
                                        
                                            Akademická stanovačka v Toulouse a iné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Iliaš 
                                        
                                            Nízkonákladové prežitie vedca v univerzitnom prostredí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Iliaš 
                                        
                                            Koľko by asi stála veriacich odluka cirkví od štátu ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Iliaš 
                                        
                                            Sekty, cirkvi a Sféry dôverné z 21.3.2012
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Iliaš 
                                        
                                            Svedectvo mesiánskeho evanjelistu Zeva Porata
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Iliaš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Lucia Švecová 
                                        
                                            Rovnosť alebo rovnakosť?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Iliaš
            
         
        ilias.blog.sme.sk (rss)
         
                                     
     
        Konzervatívec. Ž.(židofil) a s.(sionista).

 
Mesiánsky verš: 


 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    297
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1621
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Veda - aj tú treba, né ?
                        
                     
                                     
                        
                            Odluka cirkví od štátu
                        
                     
                                     
                        
                            Izrael, ó môj Izrael
                        
                     
                                     
                        
                            Športíky moje bojové
                        
                     
                                     
                        
                            ISATI
                        
                     
                                     
                        
                            Pataveda (pseudoveda)
                        
                     
                                     
                        
                            Súkromné dumania
                        
                     
                                     
                        
                            Peter Uher
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Kresťanská televízia Milosť TV
                                     
                                                                             
                                            TuneIN - všetky online rádia
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Môj foto-blog
                                     
                                                                             
                                            Blog - Karel Sýkora
                                     
                                                                             
                                            Hana Deutschmann
                                     
                                                                             
                                            Stan Goodenough
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Eretz.cz - spravodajstvo z Izraela
                                     
                                                                             
                                            IMRA
                                     
                                                                             
                                            Kresťanské spoločenstvo Milosť
                                     
                                                                             
                                            Výšiny Sionu, mesiánske podkasty
                                     
                                                                             
                                            Kresťania stojaci s Izraelom
                                     
                                                                             
                                            Eurábia - odvrátená strana polmesiaca
                                     
                                                                             
                                            hebrejská Biblia, židovské spisy
                                     
                                                                             
                                            Bblueletterbible - Biblia (pôvodný text, anglické preklady)
                                     
                                                                             
                                            Biblické štúdium Ichthys
                                     
                                                                             
                                            Iný môj blog na evanjelik.sk
                                     
                                                                             
                                            IsraelToday, biblické spravodajstvo
                                     
                                                                             
                                            ICEJ, kresťanskí sionisti
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Lesk a bieda letných brigád, alebo, ako sa naučiť nedôverovať
                     
                                                         
                       Šesť zabijackých aplikácií Západu
                     
                                                         
                       Nastal čas na nový model financovania cirkví a občianskej spoločnosti
                     
                                                         
                       Empatia. To je čo?
                     
                                                         
                       Feťáčkina matka
                     
                                                         
                       Demagógia a bludy na Chelemendikovej stránke
                     
                                                         
                       Niečo o podnikateľskom prostredí a konkurenčnej schopnosti
                     
                                                         
                       Eslovenčina je ťažká - Univerzita
                     
                                                         
                       Fotoreportáž ze socialisticko-budovatelského, rudého Prvního máje.
                     
                                                         
                       Kto je na vine? Bulvár?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




