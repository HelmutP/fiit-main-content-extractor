

  sme na motýle... 
  A ty si ma chcel učiť lietať, 
  S ľahkosťou a odvahou, 
  Snažil si sa ukázať mi iný svet... 
   
 Mal si odvahu vkladať všetko, 
  A pritom si nechcel brať nič. 
 Chcel si len, aby som ťa nekonečne milovala, 
 Aby som milovala sen a ilúziu v tebe.. 
   
 Stalo sa a ja som žila v krutej rozprávke, 
 Stalo sa a skončilo nie v polčase, ba ani nie v prestávke. 
 Odhalenia prišli celkom jasne a nečakane, 
 Nevedela som čo si myslieť, krídla si mi zlomil... 
   
 Boli polámané no iný ich poskladal, 
  Vkrádal sa ku mne a ja som zabúdala... 
 Stal sa mi priateľom, oporou možno i láskou 
 Hoci to ľahké s ním nebolo prestala som myslieť... 
   
 Bola len jedna spomienka, a to spomienka na lietanie, 
 Bála som sa vzlietnuť, bála som sa čo sa stane, 
  No on ... on spravil ten prvý krok 
  A on sám ukázal mi že niet sa čoho báť 
   
 Začala som mu veriť a k tebe som cítila už len nenávisť, 
  Odhalil si svoju pravú tvár... tvár lišiaka čo nestál za povšimnutie 
 Hoci som sa bránila a sama nevedela ako 
 Jedného dňa som vzlietla ako ten motýľ, ktorého si mi ukázal... 
  No už som nebola zbabelá s pokrčenými krídlami ... 
 Bola som hrdá na farby mojich krídel.. 
   
  Skončilo to z čoho som sa dlho nedokázala vyliečiť... 
  Už len lietam nepresviedčam sa o snoch... 
 Veď v tých snoch už nie si ty... 
 Tie sny už paria inému...ktorý mi lásku dal. 
   

