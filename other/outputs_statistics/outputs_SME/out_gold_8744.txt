

 Výsledky včerajších parlamentných volieb naznačujú, že „one man show" sa v slovenskej parlamentnej politike pomaly, ale isto končí. Jedna strana „jedného muža" je už mimo hry a tej druhej, SNS,  chýbali len stotinky percent na to, aby tam minimálne ďalšie štyri roky nebola. To je dobré znamenie, ktoré snáď premení „Slotensko" (citujem Sveťa Styka) na civilizovanú a demokratickú európsku krajinu.    Druhým pozitívnym signálom je, že v tejto dobe napätých slovensko-maďarských vzťahov, slovenský občan (Maďar i Slovák) jednoznačne ukázal strane, z ktorej vanie zatuchnutý duch Trianonu, že toto nie je cesta do civilizovanej Európy a dvere do parlamentu jej včas zatvoril. 
 Ďalším plusom z dnešného predpoludnia je fakt, že všetky relevantné politické strany, ktoré budú v budúcom parlamente, povedali SMER-u jasné NIE! Keďže SMER so SNS by vládu nezostavil, počty sú jasné.           Zostáva len dúfať, že svoje slovo dodržia a že v túžbe po moci nepodľahnú pokušeniu červeného diabla a jeho možno už dokonale prepratým špinavým peniazom. 
 Dobré ráno, Slovensko! 
   

