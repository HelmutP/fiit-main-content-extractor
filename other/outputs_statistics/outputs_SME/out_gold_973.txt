

 
Tak som bol v nedeľu pozrieť Paulu a aj Marcelu. Počasíčko – aspoň v Bratislave – ako lusk, tak prečo nie. Navyše manželka mala tiež cestu tým smerom. 
 
 
S Paulou sme si boli svojho času dosť blízki, nedá sa povedať, že by sme spolu chodili, to nie, skôr sme sa občas stretli a dávali sme Bratislave prvej polovice rokov 90-tych najavo, že sme tu a že sa intenzívne hlásime o svoje miesto pod slnkom. 
 
 
Obľúbeným podnikom mladých bol vtedy klub New Model Club. Teda, nedá sa povedať, že by bol mojím najobľúbenejším miestom na svete, ale oproti klaustrofobickému U klubu toto bolo dobré miesto...Kto si nepamätá new model club, bola to taká dlhá pivnica pod domomom na začiatku Obchodnej ulice. Trochu vlhká, trochu zafajčená a veľmi punková. Jedného dňa sa objavili akýsi dvaja Francúzi , ktorí sa rozhodlil pocestovať Európu vo svojom karavane. A neboli len oni dvaja, mali so sebou aj zasoby poctiveho francúzskeho pastisu, skvelých francúzskych gitaniek a vynikajúcej trávy odniekadiaľ z ázie. 
 
 
V tej dobe môj kamarát Michal balil Paulinu kamarátku, tak sme si 4 sedeli na pivku, pri prvom stole hneď pri vchode do priestorov New Model Clubu. Len čo francúzi vošli, prisadli si k nám, keďže inde bolo obsadené.  Mno...to nemali robiť. Pravdu povediac, tu začína moje okno, a tých pár obrázkov, čo sa vynára na povrch pamäti vyzerá ako : .....6 ľudí pri stole....ochutnávka pastis v karavane.......6 ľudí pri stole a pivo pre všetkých....ochutnávka pastis pri stole....veľký joint v karavane ...pastis v klube...požiadanie na odchod z klubu.Mno a potom sme sa rozhodli, že ukážeme tým jemným francúzom ozajstný život v centre nášho sveta, v Bratislave. Keďže nás už nikam veľmi nechceli pustiť, boli sme donútení pokračovať najprv v karavane a keď sa zásoba pastis, gitanes a konope skončila, pokračovali sme ďalej v petržalskom byte, ktorý už ani neviem komu patril...Síce sme skončili všetci šiesti v jednej rozkladacej posteli, ale kombinácia slovenského piva so všetkým hore spomenutým urobila svoje.Noc prebehla vo všetkej počestnosti. ..Akurát Michal mal na druhý deň ešte taký zvyškový alkohol v krvi, že mi v opitosti vyhodil tenisku ráno von oknom. Už som ju nenašiel. Tak som sa v to krásne februárové ráno vybral domov vo svojom dlhom koženom kabáte a jednej teniske.... na druhej nohe som mal dve ponožky. 
 
 
Takýto rokenrol zasa nebol nikdy blízky Marcele.... 
 
 
So svetom zábavy som začínal tak, že kamarát, čo mal pár videopožičovní a dovážal kazety z Nemecka, ma poprosil, či by som mu tie kazety nedaboval. Po nemecky som hovoril slušne, tak som sa do toho pustil. Denne 6 filmov. Asi dva roky. Samé béčkové a céčkové filmy...Ak som dostal film s Bruce Willisom, považoval som to filmársky sviatok. Dabing jedným hlasom sa robil tak, že existoval videoprehrávač, ktorý bol upravený tak, že zvuk som počul o tri sekundy pred obrazom. Prekladal som simultánne. Po dvoch rokoch pozerania týchto filmov som sa, pochopiteľne, prihlásil na scenáristiku a dramaturgiu na filmovú fakultu....Žiaľ, ani skúsenosť s videom  ma neodradila od showbiznisu .Rozhodol som sa otvoriť si modelingovú agentúru. Nazval som ju Divadlo Módy..DADAM..Niekde by som mal ešte mať vizitky. Na jeden z tréningov nášho zoskupenia, kde inak fungovali aj fantastický Jaro Bekr, či Mako a Ria, tak na jeden z tréningov priviedol niekto – ani si presne nepamätám kto – Marcelu. Tmavovlásku, tiché dievča s trochu vysokým čelom. Určite nie prvoplánová kráska, ale dievča s talentom a veľkou chuťou do práce.  Makač .              
 
 
Dodnes nechápem, ako to, že sme mali také šťastie. Niekto tam hore nás mal rád. V priebehu pár mesiacov od začiatku fungovania sme robili prehliadky na otvorení hotela Danube, na veľkých benefíciach v Redute. Mali sme to šťastie spolupracovať s Hankou, Renatou, Ferom, dnes všetko uznávanými módnymi tvorcami...Ich priezviská sú ich značkami...
 
 
No a robili sme aj na evente prvý slovenský silvester v hoteli Danube. Keď sme o polnoci mali začať, boli sme všetci, vrátane make up a hair dressers dosť spoločensky unavený. Únava sa prejavila aj tak, že sme robili ohňostroj v jednej z izieb, ktorú sme mali k dispozícii ako šatňu. Vonku sa oslavoval vznik Slovenska..My sme ho oslavovali vnútri. ….management hotela od nás potom chcel asi 50 000 korún za opravu izby ….No ale najväčšiu kariéru na Slovensku urobila práve ona, tmavovláska, ktorá prišla raz na náš tréning do klubu Kamel v Karlovej Vsi...
 
 
Hmm....tak som ich bol v nedeľu pozrieť. Veď počasie bolo fajn. Tak či tak sme šli upraviť hroby. Ony tam tiež odpočívajú. Neďaleko od seba. 
 

