
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Ličková
                                        &gt;
                Nezaradené
                     
                 O viere, duchovne, ezoterike a ostatných ťaľafatkách... 

        
            
                                    4.6.2010
            o
            13:19
                        |
            Karma článku:
                5.13
            |
            Prečítané 
            1505-krát
                    
         
     
         
             

                 
                    Dlho som uvažovala, či tento článok niekde pastnem. Povôdne som ho mala publikovať na jeden self-development blog, ale nejak sme sa s kočkou nedohodli (po prečítaní článku to bude asi jasné) :D  Článok beriem ako ľahko sebakritický reminder, keď opäť prepadnem ďalšiemu duchovnu :)
                 

                 Veľa sa v poslednej dobe zamýšľam sa nad pojmom „pozitívne myslenie“. Je to ako mor (ospravedľňujem sa za negatívny náboj). Z každej strany útočia nové teórie, self-helf knihy, moderné filozofické školy či naopak tie odvolávajúce sa na prastaré tradície, východné smery konkurujú tým západným, tie africkým, tie zas... atď. Je toho také množstvo, že „duchovno“ je jednoducho všade. Keď ste pred pár rokmi mohli stretnúť človeka, ktorý by sa čudoval novým poznatkom z tejto oblasti, dnes priemerne internetovo-gramotný človek je za úplneho blbca, ak ešte nevie čo je to reinkarnácia či reiki. Hovorím, o človeku modernej doby – informovanom, aktívnom (aj keď možno najviac práve vo virtuálnom priestore) a hlavne o človeku hľadajúcom... čokolvek, a veľmi rýchlo (aspoň tak je väčšinou).   Na internete nájdete všetko: od rodinných konštalácií, skrz tarot, numerológiu, exekutívneho koučingu či life-koučingu, kurzy stres manažmentu na základe jogy...Google Vám v priebehu pár sekúnd nájde milión odkazov k danému pojmu, a vy zistíte že trh s ezoterikou a pozitívnym myslením je jeden z najdynamickejšie rozvíjajúcich sa odvetví. Je to trh so silnou konkurenciou, a možem len dúfať že aj so zdravou, lebo ak premýšľam ako oddeliť v tomto biznise zrno od pliev, vedie to moju myseľ úplne iným smerom akým je pozitívne myslenie.   Nechcem to všetko odsúdiť. Chodievam pravidelne-nepravidelne na výklad kariet, hoci by som tomuto faktu pred pár rokmi neverila („kartami sa hrá Kent...“), mám absolvované dva stupne reiki zasvätenia, absolvovala som regresnú terapiu a na nič z toho by som ani dnes neodmietla, a už vonkoncom tieto zažitky neľutujem.   To, čo má trápi je akýsi nový „duchovný stredovek“, akceptujúc že do naších hláv nie je tlačená len jedna viera, jeden Boh...Nie! Teraz ich máte stovky! Vyberte si! A platťe!  Viete koľko debát som za poslednú dobu počula odsudzujúc tú či onu filozofiu, ťú či onú metódu, pitvajúc princípy meditácie..blabla? M R T E.   Celé toto nás má naučiť hľadať v samých sebe, v najhlbšom vnútri. Ale, dajte ľuďom dobrú myšlienku, a máte 200% garanciu, že to masívne dodrbú. Dobrou myšlienkou je náboženská, duchovná sloboda. A sloboda je v rovnítku so zodpovednosťou, a ako jej dôsledok je tu mohutne prekvitajúci trh s naivitou a ľahko získanými odpoveďami v on-line kurzoch (neverili by ste koľko duchovných vecí sa dá cez ne robiť).   Je to ako hlad. Potrebujeme zdravú duševnú potravu a miesto toho žerieme hamburgeri, lebo? Majú dobrý marketing. A tak kupujeme od pádu železnej opony, ako aj celý západny svet pred nami, všetky neuveriteľne nápomocné self-help príručky, ktoré aj tak nie sme schopní preniesť do reálneho života, a keď aj, tak to vydrží toľko čo nová diéta. Chodíme na všelijaké kurzy, kupujeme si všelijaké predmety, pár z nás sa zapisuje do spolkov... Jednoducho z viery/ duchovna/ transcedentna/ ezoteriky (toto slovo mi zvlásť lezie na nervy) sa stal jeden zo smerov konzumu, ktorý tak štedro podporujeme.   Stratili sme prirodzenosť hľadania. Nepočúvame sa, čo skutočne potrebujeme, a to že na nás vyskočí stránka s regresnou terapiou, keď hľadáme nové tesnenie do kúpeľne, berieme ako ranu osudu (móže byť, nehádam sa).   Som taká istá. Možno tu nekritizujem nikoho tak veľmi ako seba samú. Volám však spať po prirodzenosti! („Kto do teba kameňom, ty do neho zväzkom J.J.Rousseau!“)  Volám po vnímaní jemných vibrácií, po akceptovaní, že nám nemôže jednoducho byť fasa keď nám zomrie blízka osoba... ... že smútok je normálny cit, že sklamanie je ľudské, že strach (samozrejme v medziach) je motorom. Harmónia má dve strany, a vedel to už Akvinský, a vôbec veľa múdrých ľudí pred ním.  ... že pozitívne myslenie je keď si dám možnosť a priestor smútiť, a dokonca si kupím self-help knihu (ktorá ma úplne zblbne, a potom som ešte otravnejšia pre svoje okolie, kým niekomu nerupnú nervy, a nenazjape na mňa). ...že hľadám cestu von ak sa v smútku zaseknem, a nemusím si na to kúpiť self-help knihu, ale idem trebárs s kamoškou na kafé. ...že nezostanem stáť na mieste, ale posúvam sa ďalej – vo svojom vnútri a nie otrlo povrchne pravidelným chodením na jógu v značkových teplákoch.  Sme prazdní (pripúsťam, že toto tvrdenie už niekde odznelo). Čakáme, že epocha Vodnára/nový aztécky letopočet/ Nostradamova predpoveď o konci/ čokoľvek nám prinesie niečo nové, niečo prelomové. Všade sa píše, že sa časy menia, prichádza nové myslenie, že kríza je toho dôkazom. Nuž, tu sa zhodnem s veľa famóznymi duchovnými učiteľmi (s ktorými si môžete pokonzultovať od 20 dolárov na hodinu jedinečne cez Skype, a možno vychytáte aj nejakú zľavu), že kríza prináša nové a otvára nové obzory. Áno, kríza dokonale vykreslila duchovne vyprahlú západnu civilizáciu a otvorila dvere trhu s „duchovným všetkým“.   Choďte do prírody. Alebo si radšej prečítajte o jej fantastických účinkoch na www.blabla.com.°   Myslite pozitívne. Práve Vám síce umrela milovaná stará mama, ale predsa smrť je prirodzená, a tak sa usmievajte!  Jedzte klíčky. A zabudnite na to, že Vám robí dobre mäso. Však je nezdravé.   Všetko toto mám za sebou, neuveriteľne veľa „tohto“ pred sebou.V sobotu chcem s kamarátkou rozobrať karmický vplyv na moje vzťahy s mužmi. Ďakujem však svojmu psovi, že skoro každé ráno chodíme na dlhú predchádzku po Dúbravke a že ranné modré nebo, na ktoré pozerám v polospánku, hľadí moju dušu. Verím, že nič nie je väčším učiteľom než skúsenosť. Verím, že som tak blbá, že sa nepoučím, verím že som tak dôvtipná, že sa raz aj na mňa štastie spadne. Verím v seba, a nenávidim túto frázu. Verím v Boha, lebo som v 25 rokoch zistila, že moje kresťanské korene sú pre mňa krásne prirodzené (prirodzene krásne). Verím v pozitívno vo viac ako načančané úsmevy a večne dobrú náladu.   Toľko z mojej intergalaktickej múdrosti, dámy. Prajem Vám radostný deň    PS: Zbehnite na YouTube a dajte si nejaké „fajnučké motivačné“ video nejakej americkej (chudáci, a že prečo na nich furt ostatní kydajú) stár. Stačí zadať heslá „life change“ „change my life“ „life coach“...juhúúú, verte mi, bude to jazda!  PS 1a: Niečo na tých videách je, ale pointa tohto článku v biznise duchovna a jeho kritike. O ich dobrých vplyvoch nabudúce ;) PS 2: Ďakujem aj Monike Šindelárovej, ktorá mi povedala, že mám spochybňovať každého duchovného učiteľa, ktorého stretnem, pretože práve tak sa naučím najviac.    ° V prípade existencie stránky blabla.com sa jej majiteľom hlboko ospravedlňujem  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Ličková 
                                        
                                            Nezaujíma Vás politika...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Ličková 
                                        
                                            Rekapitulácia 2009 alebo Čo sa v HR za ten rok (ne)udialo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Ličková 
                                        
                                            Informovanosť mládeže o politike
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Ličková 
                                        
                                            The land the English named Australia...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Ličková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Ličková
            
         
        lickova.blog.sme.sk (rss)
         
                                     
     
        "Keď chceš rozosmiať  Boha, povedz mu o svojich plánoch," opakujem si stále.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1744
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




