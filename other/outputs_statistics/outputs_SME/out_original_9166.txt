
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Karol Jadaš
                                        &gt;
                Nezaradené
                     
                 ... krásnou krajinou. 

        
            
                                    18.6.2010
            o
            18:02
                        (upravené
                15.10.2014
                o
                6:27)
                        |
            Karma článku:
                4.29
            |
            Prečítané 
            324-krát
                    
         
     
         
             

                 
                    Keď sa prechádzam prírodou nemôžem si nevšimnúť ako daždivé a teplé počasie sa podpísalo na rozbujnenej zeleni. Nádhera. Sýtozelené ihličie a lístie stromov, vyrastená tráva... 
                 

                 
  Lúka. OcÚ Lednické     Vône s príjemnou vlhkosťou vytvárajú príjemný pokoj. Núti Nás to k precitnutiam, k pocitu splynutia s prírodou. Zamyslieť sa o vzťahu, vzťahu človeka a prírody. Po emancipácii Homo sapiens je načase sa posunúť do novej úrovne. Sebavedomie vybudované na vede spolu s pochopením, že netreba nad „prírodou zvíťaziť“ môžeme pokojne prejsť do fázy návratu, znovu spojenie na novej úrovni. Je na mieste položiť si otázku : môže človek existovať bez prírody? Pomaly Nám to dochádza, že človek/ľudstvo je v samej podstate vpletené do prírody. Ani nebolo by svojím druhom, bolo by iným, zovňajškom podobným, no iným. Zo samej podstate iným. Dozrel čas, že popri morálnom prebudení spojenom so spoločnosťou (ľudskou spoločnosťou) je správna chvíľa na morálne prebudenie i spoločnosti prírodno - človečenskej. Tento vzťah s novým prístupom ľudí pretaviť do politík novej vlády. Odstraňovanie škôd spôsobených záplavami premeniť na štart nového prístupu a postupu, novej environmentálnej paradigmy. Hľadanie koherencie medzi mať a byť. Vyvažovať ekonomický, urbanistický záujem ľudí a prirodzené podmienky potrebné pre prírodu. Práve nový pohľad cez novú prizmu Nám prinesie vidieť riešenie a postupy v inom uhle a svetle.Napríklad strohú reguláciu riek premeniť na vybudovanie rekreačných priestorov s mŕtvymi ramenami a jazierkami, ktoré poslúžia ako nárazníkové pásmo, priestor pre povodne. Zmeniť strach z povodní na rešpekt z obdobia dažďov. Systematicky sa venovať poľnohospodárstvu, úzko prepojeného s ochranou životného prostredia. Veď každý dobrý gazda/ poľnohospodár bol vždy ochrancom prírody. Ináč by neprežil. Dať pozor na nevhodnú orbu( správna orba je po vrstevnici) a monokultúrnosť plodín bez medzi či pásmami lesov/krovín. Priatelia poďme žiť s prírodou nie žiť v prírode. Žiť so Zemou nie žit iba na nej. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Jadaš 
                                        
                                            .keď niečo končí, poväčšine aj  niečo začína ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Jadaš 
                                        
                                            .inovať ( žeby ferbruár ?)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Jadaš 
                                        
                                            .vôňa ihličia_vôňa svedomia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Jadaš 
                                        
                                            .výzva k spoluobčanom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Jadaš 
                                        
                                            išiel som do lesa a...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Karol Jadaš
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Karol Jadaš
            
         
        karoljadas.blog.sme.sk (rss)
         
                                     
     
        ...o dianí...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    40
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    229
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       .vôňa ihličia_vôňa svedomia
                     
                                                         
                       .výzva k spoluobčanom
                     
                                                         
                       išiel som do lesa a...
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




