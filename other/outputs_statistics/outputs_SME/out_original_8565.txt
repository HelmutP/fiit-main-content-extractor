
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 To ešte nie je nekrológ 

        
            
                                    11.6.2010
            o
            5:17
                        |
            Karma článku:
                6.23
            |
            Prečítané 
            938-krát
                    
         
     
         
             

                 
                      Naozaj, tieto riadky o jednej z najznámejších osobností ponovembrových dejín Slovenska a vlastne ešte aj Československa neplánujem ako spomienkovú písomnosť, pre ktorú je predpis publikovať ju až post mortem , naopak, Vladimírovi Mečiarovi prajem ešte dlhé roky pekného života a dobré zdravie, no príliš naliehavo si uvedomujem, že svoje dni slávy už akurát v týchto voľbách pravdepodobne zavŕši v rozmere všednosti. Nie je to jednoduchý človek, to naozaj nie, vždy si celkom rád spomeniem na chvíle, keď sa naše životné dráhy pretínali a dal sa spoznávať ako jedinec mocne pôsobiacej charizmy a veru aj svalov, človek hrdý ba až pyšný, od istého času žijúci s pocitom nedotknuteľnosti a neomylnosti a teraz, keď celkom iste v hĺbke srdca i duše cíti, že sláva poľná tráva, prichádza veru aj nostalgia. Sám od seba sa nevzdá, to cítime a vlastne aj vidíme všetci, bojuje o svoje miesto na slniečku spoločenskej priazne tvrdohlavo, je predvídavý, alebo lepšie povedané, mal by byť! Dejiny sú nemilosrdné, nič neobchádzajú a ani nikoho nešetria, ani tých slávnych nie.
                 

                     Po prvý raz som ho videl v televíznom prenose z Námestia SNP v Bratislave, keď z tribúny v spoločnosti Kňažka a Budaya rečnil na verejnosti celkom iste v premiére a potom, po menovaní prvej slovenskej vlády naživo, doma v Považskej Bystrici. VPN vtedy bola progres, usilovala sa zmeniť zabehaný poriadok straníckej mašinérie a celkom iste som bol prvým jej reprezentantom na Slovensku, ktorému sa podarilo odvolať nielen samosprávu tohto okresného mesta, ale veru aj ONV. A nielen to, kooptovanie do funkcií bolo v móde a veru to bolo aj praktické, takže sme do príslušných funkcií hneď posadili ľudí, ktorí mali našu dôveru a veru aj schopnosti všetko riadiť. Prvé číslo Verejnosti to malo na titulnej stránke a takáto zmena bola naozaj mimoriadnou udalosťou.   Onedlho vymenovali prvú vládu s premiérom Čičom, možno si pamätáte a na druhý deň ráno, chvíľočku po ôsmej, keď som sedel na gremiálke odboru školstva pri ONV, ktoré som riadil už niekoľko dní, nás vyrušila sekretárka. Vzrušená, koktajúca a oznámila, že v kancelárii mám takého pána čo hovorí, že je minister vnútra a chce sa so mnou zhovárať! A naozaj, bol to on, Vladimír Mečiar v celej svojej veľkosti a s imidžom suverénneho pána nad všetkým.   Vraj chce vedieť, kto je ten inžinier, čo som ho ustanovil predsedom ONV, aký má kredit a povesť, aký bol komunista, kradol....? Tých pochybností a neprávd na mňa vychrlil naozaj plné vedro a jedným dychom. Ako ste si istotne všimli všetci, keď sa rozčúli, slovíčka zo seba vytláča ako prázdny kompresor, až som sa spravodlivo rozhneval. Som tiež radikál, tiež ma nie je málo a v živote som sa musel obracať celkom nadlimitne a napokon je mladší,  takže chvíľku počúval on mňa. Vtedy ešte nebol mesiášom, bol iba na začiatku svojej cesty za slávou a neomylnosťou, nuž ma počúval a akceptoval. Dal si vysvetliť moje kritériá výberu, uznal, že odbornosť a charakter i občiansky kredit sú dobré záruky a rozišli sme sa napokon v dobrom.   Neskôr, keď sa spoločnosť vyvíjala, VPN zmizla a jej zástavu si celkom falošne uzurpovalo HZDS, som jeho záujem pociťoval skôr prenesene než osobne. To viete, Bratislava ďaleko a školstvo je „jama levova“ v pravom slova zmysle, takže som iba na diaľku odolával jednej ponuke za druhou – prijmi ministerský post. Lákavé, ale s vedomím, že politika je všetko možné na svete, len nie čistý dvor, kde sa dá žiť bez hanby, výčitiek a pokušenia, som odolal. A prišla pomsta, minister Kučera ma odvolal, našťastie, to už som sa stačil prezentovať v učiteľskej obci naozaj preukazne a v priebehu mesiaca ma vo výberovom konaní do funkcie znova potvrdili.   Teda tak, naše vzťahy, dovtedy málo osobné sa ešte viac zanonymnili a môj kontakt s mocou sa realizoval výhradne cez ministerstvo. Ale to už je iná kapitola, lebo osobnosť vtedajšieho premiéra Mečiara si zaslúži prioritu v každom smere, aj v spomienkach.   Pamätáte sa, stal sa idolom más, predovšetkým kategória babiek demokratiek sa stala míľnikom jeho cesty ku sláve a k pýche, čo sa až nebies dotýkala a nikdy nevynechal možnosť stať sa tribúnom takýchto más najradšej v priamom zábere. Mítingy, stretnutia, spoločenské akcie, oficionality, prezentácie, tam všade Vladimír Mečiar rečnil, zabával a prezentoval sa ako politik, dobrodinca, pomstiteľ i prognostik, stvoril okolo seba priestor verných a oddaných, z ktorých už zostal vari iba Cabaj a hoci bol na všetko sám, nepoľavoval, stíhal! Už to však býva akurát tak, keď sa niečo má popsuť, tak sa popsuje zákonite a hoci sa jeho popularita ešte udržiavala vo výšinách volebnej prevahy, prišiel koniec. Nenašiel parťáka, ostatní politici nemali odvahu položiť na svoje plecia všetky tie hriechy a omyly, ktoré akurát jeho hnutie na politickú scénu vynieslo a bez podpory sa zodpovedne, no predovšetkým funkčne vládnuť nedá. Pamätáte sa, zaspieval nám Idem od vás, so slzami v očiach a na určitý čas sme o ňom počúvali menej.   Je to však bojovník, otriasol sa a napriek rokom sa ešte nevzdal príležitosti podieľať sa na riadení vecí verejných. A verte mi, vôbec nie nenápadne či nedôsledne, kdeže, tam kde sa dala prejaviť moc, tam ju prezentoval, kde výhoda, tam ju realizoval a kde zisk, tam sa na tom podieľal! Už nechce byť na čele verejného záujmu, je šedou eminenciou, ale „vo vlastnom košiari“ ešte stále zvrchovanou silou. Všimnite si, kde našiel odpor, kde zádrhel, alebo sa veci neposúvali k svojim cieľom neprerušene a hladko, cestičky čistil a zametal ako výkonný buldozer. Všetko zhrnul a odpratal a keď som spomenul pána Cabaja ako posledný relikt z čias slávy, je ich tu ešte niekoľko, ale to už je iba odlesk dávnej slávy. Pani Tóthová, pán Jureňa, no keď sa dobre poobzeráte, tak v ináč orientovaných zoskupeniach je naozaj dosť a dosť politikov, ktorými by si akurát dnes ešte vedel pomôcť! Žiaľ, boli konkurenciou, boli šikovní a museli odísť!   Neviem, iba si domýšľam, ako osobne prežíva svoje nezaujímavé pozície v rebríčkoch slávy – to naozaj  nie je obľúbený post, veď vždy bol jednotkou – no predpokladám, že silou vôle to celkom zvládol. Nedáva najavo svoju rezignáciu, naopak, usiluje sa národ presvedčiť že on – teda HZDS a Fico, teda SMER, vytvoria dvojkoalíciu a povedú Slovensko k lepším časom aj po týchto voľbách. Jasné, tak úplne už tomu vôbec neverí, ten dobrý húf oddaných pomocníkov a šikovných ľudí je dávno iba minulosťou, no zdanie mu pomáha udržiavať ilúzie. To velikášstvo, spomienku na neomylnosť a istoty večnej pravdy v ňom zrejme nepochová nič iné, iba zabudnutie. Nie vlastné, to nepríde nikdy, ale verejné, celospoločenské, národné a Vladimír Mečiar už to veru cíti. Aj v kostiach, ako sa hovorí, ale do posledného dychu statočne. Bol a je bojovník, mal vlastné pravidlá víťaziť – pamätáte, víťaz berie všetko – ale história Slovenska ako samostatného štátu by sa aj tak bez jeho účasti a osobného pôsobenia vlastne ani nezačala. Sám je presvedčený, že je „otcom vlasti“ a ak by to malo spríjemniť všetky tie roky budúce, venované odpočinku a dôchodcovskej aktivite, nech si to užíva. Rád som ho nemal a nemám, ale do našich dejinách sa podpísal pevným rukopisom. Nie je to nekrológ, to naozaj nie,  ale niečo na ten spôsob politického konca by to byť mohlo. Mám odvahu prísť s tým ako s veštbou a aj keď budem mýliacim sa prorokom, nech, nevadí – Vladimír Mečiar si aj takýto, v podstate pozitívny hodnotiaci text neodškriepiteľne zaslúži. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




