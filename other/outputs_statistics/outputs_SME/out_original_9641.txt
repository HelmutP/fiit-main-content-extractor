
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Kotian
                                        &gt;
                Nezaradené
                     
                 Kafkov Zámok podľa Weissa alebo Okom futbalového laika IV. 

        
            
                                    26.6.2010
            o
            11:07
                        (upravené
                26.6.2010
                o
                11:20)
                        |
            Karma článku:
                6.79
            |
            Prečítané 
            1571-krát
                    
         
     
         
             

                 
                    Po bitke je každý kaprál generálom - a niet nikoho, kto by čistil  záchody zubnou kefkou (zásada čo je mokré, je čisté, neobstojí pred  žiadnou manželkou). A tak si budem všímať iba pár zaujímavostí, ktoré  snáď neunikajú fajnšmekrom.
                 

                 Ukázalo sa, že keď ideme na doraz, tak majú problémy aj majstri sveta -  skvele ušitá taktika, Mucha iba chytal, konečne aj medzihra v strede  poľa, pohyb na ihrisku až do kŕčov, Štrba dohral so zašitou ranou na  kolene, Kucka sa nielen dôrazom vyrovnal Gattusovi a to sa podarí  málokomu, stopéri temer bez chýb atď. atď. Nikto neberie našim ich vnútorný pocit, že tak vraj hrali aj uplynulé  zápasy, ťažko mu však uveriť. V pondelok proti Holandsku (strašný zoznam -  Stekelenburg - Boulahrouz, Heitinga, Mathijsen, van Bronckhorst, van  Bommel, de Jong, Kuyt, Sneijder, van der Vaart, Robben, van Persie,  Huntelaar) nemusia vyhrať - ale ak takto opäť zahrajú, pre mnohých doma  budú majstrami sveta   Slovenskí futbalisti prešli po zápase s Talianskom okolo novinárov mlčky, lebo považovali ich kritiku za neúnosnú. Iste, športový publicista typu jednanuladvanula nie je vždy dôvodom na úžas, ale nebyť tej akoženeúnosnej kritiky (minule som písal o inakších francúzskych médiách), možno by regenerovali aj v poslednom zápase a nevyvolali by také nadšenie, aké sa im doma podarilo vyvolať (zrejme sa museli konečne zdravo naštvať, aby odpálili talianske hviezdy). A nechcem byť kverulant, ale ak už detinsky nevyužili jedinečnú šancu podeliť sa o oprávnenú radosť, kde je záruka, že proti Robbenovi a kol. podajú rovnaký a rovnako úspešný výkon a budú sa môcť tak tešiť ako vo štvrtok?  V Kafkovom Zámku je situácia, kedy K. má ojedinelú možnosť do zámku vstúpiť, keby v ten pravý čas požiadal dverníka o vstup - lenže si ju nevšimol. Prúser. A oranjes je iná káva - oveľa dynamickejší tím, než proti akému kedy títo futbalisti hrali.   V skupinovej fáze skončili mnohí - obaja poslední finalisti MS Taliani aj Francúzi, obe postjuhoslovanské mužstvá, Dáni, Gréci - ďalej však ide Anglicko, Argentína, Brazília, Nemecko, Holandsko, Španielsko, Portugalsko... Úžasná spoločnosť. Ako povedal v zápase s Talianskom ktorýsi český televízny komentátor, predchádzajúce výkony neboli v tom, že by naši nevedeli, ale bola to mentálna záležitosť, otázka sebavedomia.   Taliansky minister pre reformy Umberto Bossi pred rozhodujúcim štvrtkovým zápasom so Slovenskom na futbalových MS zaťal pekne hlboko: "Som presvedčený, že zápas proti Slovensku kúpia. Na budúci rok bude hrať v našich kluboch niekoľko Slovákov." Neskôr povedal, že len žartoval (korupcia je zrejme skvelý materiál na žartovanie, ako toť ukázal čerstvý poslanec, obyčajný Ľudko). To však nevylučuje, že v Taliansku nemôže hrať viac slovenských hráčov - v priamej konfrontácii bolo vidieť, že si jesto z čoho vyberať.    A teraz už len nezopakovať príklad hokejistov, kde sme po titule majstrov mali takisto veľké oči - a jediný, kto tento úspech dokázal skapitalizovať, bol prezident hokejového zväzu a jeho stavebná firma.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            7 dôvodov, prečo sme skončili druhí, alebo okom hokejového laika 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Aký veľký je prúser Richarda Sulíka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Súd s Tomom Nicholsonom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Kedy ak nie teraz?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kotian 
                                        
                                            Gorily verzus ovce
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Kotian
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Kotian
            
         
        robertkotian.blog.sme.sk (rss)
         
                        VIP
                             
     
        som novinár na voľnej nohe - odkedy som odišiel zo Sme.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2633
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




