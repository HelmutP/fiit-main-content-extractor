

   
 Myslím si, že vlk nie je typický pre Slovensko. Chodím po horách, lesoch a lúčinách, ale vlka som ešte nevidel okrem ZOO. Vo voľnej prírode som videl napríklad kamzíka, líšku, medveďa (ten by asi nemohol byť, lebo medveď bol maskotom v Rusku), srnky a malé svište. Slováci hovoria, že typické slovenské jedlo sú bryndzové halušky a tak typické zviera by mohol byť baran alebo ovca. Meno by bolo v tom prípade jasné Haluško a Haluška, mali by sme hneď dvoch maskotov. Najväčšia smola je, že súťaž sa robí len na meno maskota. Poznám niekoľko pre širokú verejnosť neznámich maliarov, ktorý by sa s hrdosťou zapojili do vymyslenia maskota, ale čo už radšej dáme zarobiť ,,uspešnému" grafikovi, ktorý dal vlkovi opilecký úsmev. 
   
 Podľa mňa by maskotom mohol byť aj sám Široký, veď pre Slovenský hokej ,,urobil veľa", aspoň on si to myslí. A Nemeček je maskot sám o sebe. 
   
 Pikoška na záver: Keď sme boli zlatý, úlohu maskota zastával Országh (fanúšikovia vedia o čom píšem), možno by si ju zaslúžil aj budúci rok.:) 
   

