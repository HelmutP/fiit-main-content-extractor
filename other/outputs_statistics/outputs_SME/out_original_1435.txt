
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Wiezik
                                        &gt;
                Nezaradené
                     
                 "Lesy SR" to z nás robíte s prepáčením debilov? 

        
            
                                    27.5.2009
            o
            15:00
                        |
            Karma článku:
                14.15
            |
            Prečítané 
            6982-krát
                    
         
     
         
             

                 
                    Počuli ste hovorkyňu lesov SR? "Lesy SR používajú na podkôrnikmi poškodené smrečiny v NAPANT­e rovnakú látku, akou poľnohospodári viackrát do roka striekajú zemiaky, hrach či obilniny, teda plodiny, ktoré denne konzumujeme". Toto robia v národnom parku? A toto nás má akože upokojiť?!
                 

                 Takže hneď na úvod, rozdiely medzi poľom v agrárnej krajine a lesom v národnom parku.   Pole slúži na produkciu plodín potravinového alebo surovinového charakteru pre potreby ľudskej spoločnosti. V zmysle odprírodnenia ide o narušené ekosystémy v ktorých prirodzené rastlinné a živočíšne spoločenstvá boli nahradené umelými kultúrami plodín. Cieľom obhospodarovateľa je dosiahnutie najvyšších možných výnosov plodiny požadovanej kvality. Pre dosiahnutie tohto cieľa je možná a nutná aplikácia prírodných a syntetických chemických látok charakteru hnojív a pesticídov, rovnako ako úpravy pôdnych vlastností a vodného režimu.      Toto je pole. Monocenóza kukurice. Biodiverzita rovná niekoľkým desiatkám druhov.   Dôsledkom takýchto postupov je vytvorenie druhovo chudobného spoločenstva cieľovej plodiny a niekoľkých sprievodných voči zásahom málo citlivých druhov. Narušenie systému spätných väzieb v dôsledku výpadku väčšiny regulačných prvkov má za následok, že takýto systém je náchylný na gradácie parazitov a konzumentov pestovanej plodiny. Voči premnoženiu takýchto druhov spôsobujúcich hospodárske škody sa konvenčne bojuje použitím chemických v oveľa menšej miere biologických prostriedkov. Z pohľadu ochrany prírody sú polia väčšinou zahrnuté do prvého - najnižšieho stupňa ochrany.   Les má funkciu produkčnú a celú škálu mimoprodukčných funkcií - ktoré sú exploatáciou produkcie lesa  priamo oslabované. Tu sa však nebavíme o hospodárskom lese, tu ide o les na území národného parku! Národný park, je rozsiahle prírodné územie, ktoré si národ ustanoví ako chránené, pretože uzná jeho význam ako jedinečného a ochrany hodného územia. Na takomto území sa v záujme jeho ochrany oslabuje hospodárske využívanie, a teda ochrana prírody je nad ostatné záujmy a činnosti nadradená. V takýchto lesoch neexistujú škodcovia, nakoľko v chránených lesoch nemôže vznikať hospodárska škoda.  Lesy v národných parkoch, pri dodržaní zásad ochrany prírody, predstavujú prirodzené (alebo k tomuto stavu zákonite smerujúce) ekosystémy s vysokou biodiverzitou, v rámci ktorých sú stromy síce určujúcim ale zďaleka nie jediným prvkom ekosystému. Lesy v NP majú byť obhospodarované s dôrazom na zachovanie kontinuity lesného prostredia a vysokej miery biodiverzity. Biodiverzita prirodzených druhov a spoločenstiev je imperatívom manažmentu zameraného na ochranu prírody. Spočíva v zachovaní pestrých a prirodzene sa dynamicky meniacich ekosystémov, vrátane všetkých štádií lesných spoločenstiev.      Toto je les. Les v NAPANTe pred aplikáciou pesticídov. Pestré spoločenstvo vrcholovej smrečiny. Biodiverzita neznáma, pravdepodobne niekoľko stoviek až tisícok druhov.    Nechcem podrobne riešiť problematiku manažmentu národných parkov. Zapamätajme si zatiaľ to, že biodiverzita je kľúčom a podstatou národného parku, správa národného parku by mala z tejto paradigmy vychádzať. Prečo teda advokáti postrekov v národných parkoch problematiku biodiverzity obchádzajú ako sa len dá?   Vráťme sa späť k vyjadreniu pani Fákovej. Ona sa vo vyjadrení obmedzila na možný vplyv pesticídov na ľudské zdravie. Inými slovami vraví, že používané pesticídy sa po pomerne krátkom období rozložia a pri dodržovaní stanoveného embarga na vstup do ošetrovaných území, nám nehrozí priama kontaminácia. Rovnako je to s plodinami z poľa. Nepoznám zúfalca, ktorý by sa išiel prechádzať pod žltými krídlami  práškovacieho lietadla. A naviac, všetky zodpovedné mamičky učia svoje deti, aby si jabĺčko, pred tým ako ho zjedia, najskôr umyli. Napriek tomu, dochádza ku zamoreniu potravného reťazca umelými aditívami a asi aj preto, tí čo o tom vedia a čo na to majú, dávajú prednosť produktom organického poľnohospodárstva.   Ako je to však s biodiverzitou našich polí? Vskutku mizerne! Najlepšie to vidieť na poľnej pôde. Aby som bol dôsledný musíme najskôr zadefinovať čo to pôda je. Veľmi stručne - oživený a životom premenený horninový substrát. Je to zmes minerálnej zložky so zložkou organickou - neživou rozkladajúcou sa a živou, zloženou z celej škály prokaryotických a eukaryotických organizmov, podieľajúcich sa na kolobehu látok v pôde, zabazpečujúcich biologickú aktivitu pôdy. Prítomnosť živej zložky je podmienkou pre to, aby sme mohli nejaký substrát nazvať pôdou. Čo teda život v poľnej pôde hovorí na používanie pesticídov?   Dovolím si v tejto súvislosti použiť príspevok profesora Ruseka, renomovaného pôdneho biológa, ktorý prezentoval na tohtoročnej konferencii v Českých Budějoviciach: Microarthropod fauna in arable soils of the Czech Republic. Profesor Rusek hodnotil vplyv konvenčných agrotechnických opatrení ako každoročná orba, aplikácia umelých hnojív a pesticídov na spoločenstvá chvostoskov (kľúčovej zložky pôdnej fauny) v černozemných pôdach Československa. V 60-tych rokoch 20. storočia zaznamenal hustotu chvostoskokov na 1m2 černozeme v hodnotách 40 600 - 139 600 kusov a 28 - 34 druhov. Len za tri roky (!) používania umelých hnojív a pesticídov klesli tieto hodnoty na úroveň 800 - 4200 jedincov štyroch až piatich druhov, čo profesor Rusek opodstatnene považuje za kompletný kolaps pôdneho spoločenstva. Rovnaký pokles bol zaznamenaný aj v bakteriálnej aktivite či v spoločenstvách dážďoviek.        Chvostoskoky, drobné organizmy zohrávajúce v pôde dôležitú funkciu rozkladačov organickej hmoty, ale pôsobia aj ako šíritelia spór mikoríznych húb, nevyhnutného predpokladu dobrej vitality rastlín.   A teraz si predstavte, že tie isté bezpečné pesticídy, ktoré dokázali rozvrátiť za tri roky spoločenstvá viazané na chemicky a fyzikálne pomerne stabilné černozeme, používame za výdatnej podpory PR pracovníkov štátnych lesov v prostredí kyslých plytkých pôd horských smrečín v našich národných parkoch. Nielen že tieto pôdy sú podstatne plytšie a kyslejšie, ale hlavne sú nepomerne bohatšie na množstvo druhov, z nich veľká časť patrí medzi úzko špecializované a lokálne, vzácne druhy o ktorých ekológii, veľkosti populácie, či citlivosti na "umelé chemické maškrty" máme len veľmi strohé resp. žiadne vedomosti.   Môže nám byť vrcholne ukradnuté, či lesníci používajú Fury 10 alebo Fury 100, či je pesticíd aplikovaný bodovo, alebo pomocou lietadiel, či je striekaný mimo turistických trás a objektov, či je zafarbený do červena alebo zelena. Realitou zostáva, že sa aplikuje na území národného parku, kde biodiverzita by mala byť v slušnej krajine chránená ako merítko kultúry národa. Neobstoja argumenty, že neexistujú alternatívne postupy, samozrejme že existujú, stačí sa pýtať susedov. Aplikácia pesticídov v horských ekosystémoch naviac v spojitosti s koristníckou a pramálo regulovanou ťažbou kalamitného dreva je doslova a do písmena ekologická katastrofa. Zaručujem vám, že minimálne 90% všetkých druhov živočíchov v mieste aplikácie postrekových látok bude týmito pesticídmi silno negatívne ovplyvnených a dôjde k lokálnemu vymretiu populácií citlivých druhov hmyzu a iných článkonožcov. Ak nie sme schopní rozoznať rozdiel medzi poľom a národným parkom, tak radšej zrušme všetky národné parky, aspoň si ušetríme hanbu pred svetom.   Odsudzujem takýto postup, ako spiatočnícke, nič neriešiace a škodlivé mrhanie financiami, ktoré likviduje národný park a na ktoré sme sa všetci poskladali z daní.   ... a pani hovorkyňa, na Vašom mieste by som dotyčnému súdruhovi, ktorý Vám káže verejne šíriť demagógie o neškodnosti postrekov povedal len: "Fákoff!" :) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (74)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Desať rokov postkalamitného vývoja
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Desať rokov úžasnej divočiny v Tichej a Kôprovej doline
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Vetrová kalamita - verzia 2014
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            Čo pytliak, to poľovník - kríza predátorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Wiezik 
                                        
                                            130 vlkov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Wiezik
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Wiezik
            
         
        wiezik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Pracujem na Fakulte ekológie a environmentalistiky, TU vo Zvolene. Presadzujem fungujúcu ochranu prírody na Slovensku. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    52
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5130
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            moja stránka
                                     
                                                                             
                                            Rozhovor v Pravde
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            F.W.M. Vera - Grazing Ecology and Forest History
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rádio Rock
                                     
                                                                             
                                            Miles Davis - Bitches Brew
                                     
                                                                             
                                            Jaga Jazzist
                                     
                                                                             
                                            Tom Waits
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Michal Wiezik
                                     
                                                                             
                                            wildlife.sk
                                     
                                                                             
                                            alternatívne lesníctvo z Čiech
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       10 rokov po ...
                     
                                                         
                       Nie len blondínky, ale aj brunetky
                     
                                                         
                       Vlčie hory
                     
                                                         
                       Čo pytliak, to poľovník - kríza predátorov
                     
                                                         
                       Názorná ukážka vplyvu človeka na vysušovanie krajiny
                     
                                                         
                       Zimné OH a vzorový alibizmus Ministerstva životného prostr. S.R.O.
                     
                                                         
                       Nehaňte vlka
                     
                                                         
                       Bude Slovensko ešte niekedy tým Švajčiarskom?
                     
                                                         
                       Popíjač koly sa nám všetkým smeje do tváre
                     
                                                         
                       Kto stojí za Cynickou obludou ?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




