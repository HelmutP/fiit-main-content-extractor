
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Hlina
                                        &gt;
                Nezaradené
                     
                 Maďari sú v pohode, v piatok utekajú z roboty. Ako my. 

        
            
                                    7.6.2010
            o
            11:15
                        |
            Karma článku:
                12.39
            |
            Prečítané 
            2435-krát
                    
         
     
         
             

                 
                      Asi by som mal vysvetliť tú tabuľku na pošte. Raz o tom  napíšem, zatiaľ len toľko:  Kto vie , pochopí. Kto nevie,  ale chce pochopiť , sa doučí. Kto nevie a ani nechce vedieť,  nech stále ďalej číta iba výplatnú pásku a horoskopy v NČ . Už som to raz písal ,šach sa hrá na viac ťahov dopredu.  
                 

                     Ale o inom som chcel. Hneď zahorúca. Lepšie je raz vidieť ako stokrát počuť.   Bol som 4. júna v Budapešti vrátiť koňa Orbanovi. Koňa mi zobrali 20 km pred Budapešťou, čo som inak predpokladal  , tak som išiel schválne  na inom aute pred autom s koňom.  Na svojom aute som prešiel a prechádzal som sa po Košútovom námestí   a môžem zodpovedne vyhlásiť : Maďari sú v pohode, v piatok utekajú z roboty ,ponáhľajú sa domov,  tak ako u nás.   O tretej už nikto nebol u Orbana na Úrade vlády. Nikto, kto by so mnou hovoril anglicky, na stálej službe mi  horko ťažko povedali: Je piatok - tri hodiny- nikto tu už nie je. Zatiaľ, čo môjho koňa vyšetrovalo viacero  policajných aut , ku posádke privolali tlmočníka,  ku koňovi veterinára, ja som sa prechádzal po Košútovom námestí , dal som si kávu v Parlament café , náhodou vošiel do uličky, kde formovala  šíky nejaká organizovaná skupina. Mali veľa všelijakých zástav ,tipujem, že  smútok za Trianonom bol dôvod,  prečo si dali tú robotu a prišli pred parlament. Uvedomil som si, ako málo stačí. Stačilo niečo zakričať, oni by sa na mňa vrhli. Vzhľadom k tomu, že pred parlamentom bolo viac policajtov ako civilov, tak by som to aj celkom ustál, kým by prišli policajti a rozdelili nás .   Ako málo stačí na problém , preto musíme byť zodpovední. Maďari sú v pohode, v piatok sa ponáhľajú domov tak ako my. V piatok  som síce videl zopár týpkov v kanadách ,zopár pánov s pivnými bruchami v  šušťákoch od Číňanov,  ktorí si vykračovali so šatkami po Košútovom námestí. Ale žiadna veľká sláva to nebola. Podobne ako u nás: frustrácia zmiešaná s hnevom na každého a všetko a upnutie sa k niečomu veľkému. Po maďarsky neviem ,ale naučil som sa tam ako správny obchodník dve slovíčka: Elado a Kiado  - čo znamená  predaj a prenájom.  Tie slová si hneď všimnete, lebo visia všade :  na výkladoch,  oknách,  budovách....  Keď ich je veľa, každý analytik hneď vie, v akom stave je ekonomika. Prajem Maďarom veľa zdaru a dobrých riešení . Maďari sú podľa mňa pracovitý a húževnatý národ,  takže ak budú mať správnu vládu , tak Elado a Kiado nebude také frekventované slovo a frustrácie na uliciach tiež bude menej. A poteší to aj nás susedov, lebo bude menej dusno.   Keby som naozaj,  chcel tak by  som toho koňa do Budapešti pred Úrad vlády dostal ,nebolo však mojim cieľom niečo niekomu dokázať ,vyhrať , a popritom porušiť zákony, predpisy cudzej krajiny. Ak ho Maďari nechceli,  rešpektujem to. Mimochodom, počul som,  že niekedy v auguste je v Budapešti akcia,  kde sa dá jazdiť na koni po meste. Ak by ma tam niekto pozval,  rád by som prišiel a dokonca môj kamarát Miro H.( ten čo viezol teraz koňa).  Je tak super jazdec na koni ,že by to, ak je to súťaž, aj vyhral. A keď nepozvú -  nevadí , stále platí to, čo je v nadpise.   Maďari sú v pohode.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (36)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Čo sa Penta v Číne nenaučila
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Zrušiť zdravotné odvody je nebotyčná hlúposť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Nočné rokovanie bude aj pred Paškovou vilou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Smer zlegalizoval nezákonné billboardy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            O slovenskej žurnalistike rozhodne Shooty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Hlina
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Hlina
            
         
        alojzhlina.blog.sme.sk (rss)
         
                                     
     
        Alojz Hlina - nezávislý poslanec NR SR, občan Slovenska, občan mesta Bratislava, oravský bača. www.alojzhlina.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    518
                
                
                    Celková karma
                    
                                                8.94
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak kvôli tomu vyhodili Čaploviča?
                     
                                                         
                       Politický nekorektné úvahy k Dňu Európy a Dňu víťazstva
                     
                                                         
                       Hanebná nominácia na ústavného sudcu
                     
                                                         
                       Stane sa  SMER chrapúňom  roka?
                     
                                                         
                       Košičania, budete svietiť ako baterky?
                     
                                                         
                       Simpsonovci a Hlinovo hrdinstvo
                     
                                                         
                       Ako udržiavajú P.Paška a R.Kaliňák krehkú rovnováhu?
                     
                                                         
                       Môžete ísť do kostola, ale omša nebude
                     
                                                         
                       Keď SMER zabezpečuje koledy
                     
                                                         
                       Študenti, ako chutila držková u premiéra?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




