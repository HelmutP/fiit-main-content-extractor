

 
Ministerstvo avizuje vycistenie krajiny..., 
 
 
neviem, ci tento moj clanok neprichadza pozde a zajtra uz bude Slovensko vycistene, ale napadlo mi po precitani clanku v Korzari, ze keby sa "prispelo" vyberacom kovovych "pokladov" , aby povyberali aj dalsie nerozlozitelne , nepovodne sucasti dna riek..., ze by sa urobil aj kus dobrej roboty...! Sice bolo by to  po kampani, ale 2x nevstupi.... 
 
 
 
 
 
Myslim si, ze v tychto horucavach by celkom ocenili dobre pracovne prostredie...:) 
 
 
"Vzácne kovy a železo lovia pod hladinou 
 
 
(mipo) 
 
 
Kvôli medi, plechu a železu sa michalovskí Rómovia denne brodia po pás v rieke Laborec. Proti prúdu urazia pešo približne štyri kilometre. Z vody vyťahujú staré sudy, práčky, drôty a kusy železa, ktoré odnášajú do zberných surovín. Za deň si privyrobia aj dve tisícky. 
 
 
MICHALOVCE. Michalovskí Rómovia sú vynaliezaví. Staré železo a vyhodený kovový odpad hľadajú už aj pod hladinou. Už druhý týždeň prečesávajú koryto Laborca skupinky hľadačov kovov. 
 
 
Tridsaťšesťročný Slavo z Michaloviec je nezamestnaný. Hľadaniu kovov v rieke sa venuje približne päť rokov. Keď je teplejšie počasie a klesne hladina vody, spolu s 27-ročným Tonom a ďalšími kamarátmi vytvoria rojnicu a postupne prehľadávajú dno meter po metri. "Vo vode sme päť až šesť hodín a prejdeme približne štyri kilometre," hovorí Slavo. 
 
 
Ten istý úsek prejdú niekedy viackrát. Po brehu ich sprevádza ďalší kamarát s vozíkom, do ktorého nájdené kovy naložia. "Za jeden plný vozík dostaneme v zberných surovinách okolo 2,5 tisíc korún. Záleží od toho, aký je deň. Kilo železa stojí sedem a plechu päť korún. Ak je viac medi a vzácnych kovov, je to samozrejme lepšie," dodáva." 
 

