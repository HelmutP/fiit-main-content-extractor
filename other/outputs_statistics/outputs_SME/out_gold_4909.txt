

 Až se bude psát rok 2006, až se všichni přestěhujem do obrovských měst, až dálnicí zeměkouli opředem, až budem pyšní na všechno, co dovedem, pak bude možná pozdě na to chtít se ptát co děti, mají si kde hrát? 
 Je rok 2010 a do obrovských miest sme sa zďaleka všetci nepresťahovali. Naopak, minimálne na Slovensku žije väčšina obyvateľstva stále na vidieku. Diaľnice obopínajú isté úseky zemegule, no celá nimi ani náhodou opradená nie je, veď ani Slovensku tú diaľnicu akosi nevieme dokončiť. A detské ihriská stále existujú, dokonca pri našom baráku nedávno postavili úplne novučičké ihrisko pre malých šarvancov. 
 






 
 Až suroviny budem vozit z Měsíce, až počasí bude řídit družice, až budem létat na Venuši na výlet, až za nás budou počítače přemýšlet... 
 Z Mesiaca zatiaľ vozíme len vzorky, družice počasie tiež len sledujú, neriadia ho. Výlety na Venušu? Zatiaľ len do Bardejovských kúpeľov. No a s myslením sa stále musíme trápiť sami. 
 Až budeme mít továrny i na stromy, až umělá tráva bude před domy, až zavládne v celém světě blahobyt, až budem umět skoro všechno vyrobit... 
 Milé a naivné, ale bola to populárna pesnička, učili sme sa ju hrať na gitarách a tešili sme sa, ale i trochu obávali budúcnosti. Zbytočne, veľa sa toho nezmenilo. 
 Aj Dalibor Janda prorokoval v piesni Všechno na Mars: 2005 lidi pohrdli Taxisem, 2006 Chuchli potáhli nápisem: "Koně už jsou nadbytečný, vstávejte z postelí, lidi v noci vystřelí koně na Mars!" 
 Ako je na tom Velká Chuchle? Najbližšie dostihy tam budú v piatok 25. apríla - The British Racing Day - Velká cena LBBW Bank CZ. 
  
 Všechno už máš na kolenou a pod vodou měj si príma byt, v tabletě ber dovolenou a místo kafe pij energit. 
 Áno, energetickým nápojom dnes mnohí dávajú prednosť pred kávou. Hoci ich hlavnou zložkou je stále kofeín, je to jediná časť proroctva, ktorá sa naplnila. Nezačal žiadny „vek robotov" a práca stále nejde „bez potu" - len sa pozrite na stavebných robotníkov, čo vám zatepľujú bytovku. Dalibor Janda spieva túto pieseň dodnes. 
 






 
 Ľudia radi fantazírujú o budúcnosti, zväčša s obavami o prírodu a človeka v nej. Nie sú to vždy len nevinne mienené mementá, obľube sa tešia najmä proroctvá o apokalypsách. Tá najbližšia nás má čakať už o dva roky, je o tom aj film. Vzhľadom na úspešnosť umeleckých predpovedí sa niet čoho obávať. Leda ak nízkej umeleckej kvality. 

