

 Cestou medzi Kysakom a Popradom som sa pokúšal fotiť krajinu z vlaku. Zopár fotiek sa vcelku vydarilo. 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 A toto sú už zábery z popradskej železničnej stanice. 
  
  
 Prvé, čo som si v Starom Smokovci všimol, že konečne zmizla tá megalomanská, desaťročia nedokončená socialistická stavba kultúrneho centra. Je zrovnaná so zemou. 
  
 Dlhé roky chátrajúci a nevyužívaný amfiteáter premenili na parkovisko, aj keď tam ešte stále strašia zbytky rozbitého pódia. 
  
 Kúpele zrekonštruovali liečebný dom Branisko a urobili do kúpeľného areálu vstupnú bránu, pribudli tam aj nové drevené sochárske výtvory. 
  
  
 Z kina urobili kongresové centrum. 
  
 Obzrel som si s nostalgiou svoj obľúbený Penzák (Odborný liečebný ústav respiračných chorôb), v ktorom som sa desaťkrát po 5-6 týždňov úspešne liečil z astmy a pritom som niekoľkokrát preliezol celé Tatry. Teraz je to Royal Palace a je to súkromné zdravotnícke zariadenie, kam sa obyčajný smrteľník len tak ľahko nedostane. Z kolegov pacientov, ktorí sa tam pravidelne v júli liečili, som nestretol nikoho. 
  
  
 Električkou som sa presunul na Štrbské Pleso, kde ma hneď po príchode zaujal konečne dokončený grandhotel Kempinský. Pekne upravili aj okolie hotela. 
  
  
  
  
 Ďalšou novinkou je člnkovanie po plese, čo tu už za mojej mladosti bolo, na južnom brehu jazera pribudlo aj pekné mólo. 
  
  
  
  
  
  
  
  
  
  
 Obišiel som jazero, chcel som sa ešte pozrieť na areál snov, no ohlásil sa znova kĺb, tak som to radšej otočil k vláčikom a zubačkou som sa spustil do Štrby. O 10 minút odchádzal rýchlik do Košíc s priamymi vozňami do Michaloviec, takže som si večer vyzdvihol u syna bicykel a ešte som sa stihol presunúť domov. Na moje veľké prekvapenie na bicykli som úplne ožil, priamo nezaťažený kĺb prestal bolieť. Pár dní zregenerujem a vydám sa opäť na cesty za poznávaním krás Slovenska. Pri bilancovaní som totiž zistil, že poznám len 20 % Slovenska, takže najbližších 10 rokov mám cez leto o zábavu postarané, len sa musím trochu krotiť. 

