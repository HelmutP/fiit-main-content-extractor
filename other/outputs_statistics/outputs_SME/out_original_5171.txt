
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dominik Marko
                                        &gt;
                Nezaradené
                     
                 Zonácia Tatier 

        
            
                                    24.4.2010
            o
            15:43
                        (upravené
                1.6.2010
                o
                15:23)
                        |
            Karma článku:
                6.18
            |
            Prečítané 
            1117-krát
                    
         
     
         
             

                 
                    V médiách sa teraz často prepiera téma a plán zonácie Tatier, ktoré nedávno predstavilo ministerstvo životného prostredia a čaká na odobrenie vládou. Týmto článkom by som sa zároveň chcel pridať takýmto spôsobom, k protestujúcim na námestí v Bratislave.
                 

                 Z prieskumu denníka SME zo začiatku roka 2006 vyplynulo, že najväčšiu moc na Slovensku má vláda a veľké finančné a podnikateľské skupiny. Vplyv parlamentu a politických strán je až za nimi. Toľko z prieskumu verejnej mienky.   Určite väčšina stihla zaregistrovať, tú krásne romantickú a idylickú reklamu, ktorá propaguje novú éru Tatier, plnú krásnych a špičkových hotelov, služieb a nových zjazdoviek. Je to reklama firmy Tatry mountain resorts, a.s., ktorú samozrejme zastrešuje aj finančná skupina J&amp;T. J&amp;T má široké pole pôsobnosti nielen na Slovensku, ale aj v zahraničí. Pôsobí v bankovom sektore, v nehnuteľnostiach a stavebníctve, v korporátnych investíciách, službách a privátnom bankovníctve. Majiteľom tejto finančnej skupiny je Patrik Tkáč a Ivan Jakabovič, ale viac ako polovičným vlastníkom J&amp;T je Jozef Tkáč, otec Patrika Tkáča. Čo a kto stál vlastne za vznikom tejto veľkej finančnej a investičnej skupiny? Začiatkom 90-tych rokov bol Jozef Tkáč najvyšším šéfom Investičnej a rozvojovej banky, a.s., ktorá bola dôkladne vytunelovaná a jej straty sa presunuli do Konsolidačnej banky, ktorá už tiež skončila svoju činnosť začiatkom roka 2002, pre svoje veľké straty. V rokoch 1993-94 stál za vznikom J&amp;T práve Jozef Tkáč, ktorý dosadil do riadiacich funkcií svojho syna a jeho blízkeho priateľa Jakaboviča. Z J&amp;T je dnes jedna z najväčších investorských a finančných korporácií v strednej Európe. Jej pôsobnosť zahrňuje Slovensko, Česko, Rusko, Švajčiarsko a Cyprus. J&amp;T stihlo vyrobiť Európskej zdravotnej poisťovni dlh vo výške 200 mil. korún, čo je v prepočte viac ako 6 a pol milióna eur. Fond národného majetku presunul do ich J&amp;T banky vyše 1,3 mld. korún, ako úložku. J&amp;T je jednou z tých rýchlokvasených spoločností, ktoré vznikli za viac ako podozrivých okolností a možno tu hovoriť o mafiánskych praktikách. Na tomto príklade znova vidieť, že zo štátneho sa pekne ryžovalo. Práve za vlády Vladimíra Mečiara v rokoch 1992-98.   Zonácia Tatier v prípade schválenia údajne umožní výstavbu hotelov a nových zjazdoviek, v chránených krajinných oblastiach Tatranského národného parku. Samozrejme, že to je znova len projekt, ktorý bude v prospech vysokej vrstvy a na úkor chránených oblastí Tatier a v neposlednom rade aj občanov. J&amp;T a ich blízki spolupracovníci budú mať len ďaľší dôvod opäť dobre ryžovať a naplniť si už aj tak plné vrecká z nakradnutého majetku. Vláda Róberta Fica pravdepodobne úzko spolupracuje s týmito finančnými skupinami a sama rozvíja svoj privátny biznis prostredníctvom takýchto finančníkov. Všetko je znova len o biznise plnom korupcie a zlodejstva a preto treba vysloviť rozhodné nie zonácii Tatier. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Zomrel autor najsmutnejšej symfónie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Primitívna antikampaň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Vyliečená mŕtvola
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Diametrálny pohľad na náboženstvo a cirkev cez prizmu Chatrče
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Bola Devínska Nová Ves obeťou masakru?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dominik Marko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dominik Marko
            
         
        dominikmarko.blog.sme.sk (rss)
         
                                     
     
        V podstate som človek z ľudu, teda jeden z mnohých.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1051
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Winston Groom - Forrest Gump
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Bob Dylan
                                     
                                                                             
                                            Leonard Cohen
                                     
                                                                             
                                            Tom Waits
                                     
                                                                             
                                            Bruce Springsteen
                                     
                                                                             
                                            Lou Reed
                                     
                                                                             
                                            Cat Stevens
                                     
                                                                             
                                            Nick Cave &amp; The Bad Seeds
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Internetové diskusie zaplavili proruskí trollovia, píše Guardian
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




