
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Igor Múdry
                                        &gt;
                Receptárium
                     
                 Prsíčkové rizoto – jedna báseň 

        
            
                                    1.4.2010
            o
            11:20
                        (upravené
                1.4.2010
                o
                11:12)
                        |
            Karma článku:
                10.71
            |
            Prečítané 
            2769-krát
                    
         
     
         
             

                 
                    V poslednej dobe sme spoločne pochodili niekoľko historických objektov. No to bolo schodov! Nová dierka na opasku je neklamným znakom, že je najvyšší čas doplniť trochu energie. Ideme preto kuchtiť niečo pod zub. Rizoto pôvodne nebolo vymyslené ako jedlo, ale ako chyták do diktátov. Koľko pätiek to prinieslo, nešťastných pokazených detstiev a písomných trestov: „Ostaneš dnes po škole a stokrát napíšeš vetu - Rizoto sa píše s mäkkým I." Fyzické tresty po rodičovskom združení radšej ani nespomínam. Potoky, Vážení adepti kulinárskeho umenia, potoky sĺz pretiekli našim obrodeným krajom, až hen do Dunaja. Zaslúži si rizoto takúto neblahú povesť? Tu nemá kadejaká pofidérna demokracia žiadny priestor! Preto odpovedám priamo aj za Vás. Nezaslúži. Možno Vás tak trošku prekvapí aj moje chvastúnstvo. Že jedna báseň. Pche! „Trošku skromnosti by mu veru neuškodilo." - pomyslí si nejeden návštevník hladný po texte, ale aj naozaj. Lenže prečo by som mal klamať, keď už to tak raz je. Ak už ste sa rozhodli, že si článok otvoríte, tak odporúčam čítať pateticky, hlbokým zastreným hlasom. Asi tak, ako reklamu na Tatry, čo teraz fičí v televízií.
                 

                 Pre Vás, moji čitatelia milí,   nachystal som v slabej chvíli,   primerane jednoduchú receptúru.   Varenie odstráni stres.   Kde zakopaný je ten pes?   A treba nám aj rúru?       Nech rozplynú sa ako hmla,   Vašej trudnej mysle chmáry.   Netreba nám sporákov!   Žiadne veľké čary.   Stačí párik horákov,   nech sa pokrm pekne varí.        Naostrím si veľký sekáč,   použijem brúsny kameň.   Cibulienka moja prepáč,   už je s tebou veru amen.   Tak bez rečí hop na pekáč,   veď už blčí veľký plameň.        Ryža, stopercentne dar je boží,   čínsky ľud sa od nej množí.   Načo sú nám ryže plné táčky,   stačia štyri nespisovné sáčky.   Cibuľu si prekrojíme,   do hrnca ju ponoríme.   Nesolím, no čo som kujon?   Radšej pridám jeden bujón.        Láska ide cez žalúdok,   vypomôže nám dnes Tesco.   Všetko chce svoj poriadok,   na rad príde teraz mäsko.   To je predsa jedla jadro,   pokrájam kuracie ňadro.        Šéfkuchár som morový,   šup tam sósik sójový.   Ide nám však hlavne o to,   nepaprať sa veľmi s tým.   Nech to nie je ako blato,   posypem to korením.   Zeleninka, času máš dosť,   neskáč teraz do hrnca.   Utlm svoje vášne, zlosť,   vyčkaj pekne do konca.        Mäsko s láskou premiešame,   odležať ho chvíľku dáme.        Cibuľka je na panvici,   upečená do sklovita.   To sú veci, to sú veci,   mäsko sa k nej samé pýta.        Sklovitosť je správny bod,   vareška nám príde vhod.   My to predsa dobre vieme,   rýchlo všetko opečieme.        Nájdeme si vhodný hrniec,   bo sa blíži akcie koniec.       Pokrm doňho povkladáme,   s veľkým citom premiešame.   Namáhavý bol to proces,   únavou nám ťažknú viečka,   nevyrobme preto exces,   pozor, nech to nie je brečka!        Postrúhame kúsok syra,   dobre padne pohár piva.   Tu sa končí naša púť,   všetkým želám dobrú chuť!             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (57)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Bobuľka a gaštanko
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Uhrovec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Jankov vŕšok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Kolo, kolo mlynské
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Igor Múdry 
                                        
                                            Vodná šlapačka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Igor Múdry
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Igor Múdry
            
         
        igormudry.blog.sme.sk (rss)
         
                        VIP
                             
     
          

 Ešte stále strojár. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    176
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1888
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko moje rodné
                        
                     
                                     
                        
                            Nemecko moje prechodné
                        
                     
                                     
                        
                            Pokusy o veselosti
                        
                     
                                     
                        
                            Domáce úlohy
                        
                     
                                     
                        
                            Receptárium
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Rolling Stones
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Je z čoho vyberať.
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje fotky
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




