

 Prechádzam sa lesom, 
 som tu aj tu niesom. 
 Vietor mám vo vlasoch 
 dotýkam sa neba. 
   
   
   
 Kráčam zlatým lístim 
 a snažím sa zistiť, 
 čo si práve myslí 
 malá sojka v korune. 
   
 Objímam stromy, 
 načúvam lesa reči. 
 Ticho poviem machu, 
 aby nepodľahol strachu 
 keď ho jemne pohladím. 
   
 Počujem ako šepká 
 strom stromu, 
 ako šumí tráva 
 Ako si nový život, 
 svoju cestu hľadá. 
   
   
   

