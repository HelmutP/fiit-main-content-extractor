

 Ja aj svadby považujem za príležitosť k veselosti a nie naopak.  S rozvodmi nemám žiadne skúsenosti, v našej rodinke sa nič také nevyskytuje. Keď čítam štatistiky rozvodovosti,  zakaždým sa čudujem, odkiaľ pochádzajú tie vzorky rozvodovosti, keď naša rodinka je nad populačným priemerom (čo sa týka počtu detí v jednom manželstve) a pritom nie sme typický reprezentant štatistickej vzorky, čo sa týka jej výsledkov:) 
 Moja mladšia dcéra mi to vysvetlila.  Že vraj dnes veľa párov žije spolu aj bez sobášneho listu a tí sa do štatistiky rozvodovosti nedostanú ako vzorka, ale že aj keby sa dostali, tak rozvodovosť sa určite nezníži, ale stúpne tak na 70%.  Príčiny totálnej ignorácie tohto pomerne obľúbeného javu (sobášiť sa potom sa rozvádzať)  hraničiaceho s dôvodným podozrením hrubého skresľovania štatistického procesu skúmania výsledkov percenta štatistickej rozvodovosti  našou rodinkou som odhalila zrovna na tej nedávnej  účasti na svadobnom obrade:)    Najskôr som nechápala som, že aj po skončení obradu  všetci pokračovali v tóne naladenom rovnako ako pred začatím obradu. Teda vážnom a dôstojnom. 
 Ale to nie u mňa nie je nič neobvyklé, že niečo nechápem:) 
 Potom sme pomaly sme kráčali všetci svadobčania dolu schodami z budovy, v ktorej sa nachádzala obradná sieň. Synovcovi, ktorý bude určite ďalší v poradí na rodinnú svadbu (:)) )  som sa posťažovala, že ženích je ešte stále viac vážnejší ako veselejší a synovec mi hovorí, že to si pritom už dal aj slivovičku pred obradom a vôbec to na ňom nie je vidno.  Kričím na svadobného otca, že sa všetci musíme vrátiť späť, lebo sobáš bude anulovaný z dôvodu, že ženích bol počas obradu  pod vplyvom alkoholu a teda obrad je neplatný:)  "No tak to aj ja si dám anulovať, lebo aj ja som bol počas sobáša pod vplyvom alkoholu." - hovorí svadobný otec, hurááá, humor v našej rodinke je večne živý, ani Lenin naň nemá:)) 
 "Ja som síce nebol bol vplyvom alkoholu počas sobáša, ale to mi dnes už nikto nedokáže po 33 rokoch, takže aj ja sa úplne dobrovoľne priznávam k požitiu alkoholu... " 
 Ani som sa nemusela obzrieť o schod vyššie, komu patrí ten hlas. 
 Patril môjmu mužovi.  Tak sa teším, že až! 
 Moja prítomnosť na tomto svete nebola márna.:) 
 Tradícia veselosti hlášok prezentovaných slovne a zmyslu pre osobitý humor v našej  rodinke  z vetvy po praslici bude pokračovať aj po meči!:) 
 I keď môj mi stále hovorí, že si nespomína, že by na tej svadbe vyslovil niečo, čo by bolo v rozpore s jeho naozajstnými názorom:))  

