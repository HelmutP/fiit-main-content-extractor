
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Odhady politológov k voľbám: najlepší bol Baránek, najhorší Horváth 

        
            
                                    16.11.2009
            o
            16:15
                        |
            Karma článku:
                11.98
            |
            Prečítané 
            13245-krát
                    
         
     
         
             

                 
                    Ako dobre tipovali víťazov a účasť v župných voľbách experti, ktorých nám médiá predkladajú ako znalcov politiky?
                 

                 
Na voličov sa v tejto krajine už vôbec nedá spoľahnúť.markiza.sk
   Čím lepší analytici v médiách, tým lepšia verejná diskusia. Preto sa cez SPW snažím kontrolovať výroky novinárov alebo politikov, ale aj expertov. Spätná väzba k ich výrokom je nakoniec dôležitá aj pre samotnú verejnosť. Kto bol teda v odhadoch výsledkov župných volieb najlepší a kto najhorší?    Účasť: Výška účasti sa v priemere skôr podcenila. Zhruba však analytici dobre hádali, výraznejšie mimo bol len P.Horváth. Nameraná účasť bola nakoniec 22,9%.     
   odhady účasti  
 analytik   do 15% Horváth (UCM)   do 18% Horský (UCM)   15-20% Slosiarik (Focus)   okolo 20% Baránek (Polis)   20-25% Mesežnikov (IVO), Haulík (MVK)      Najlepší odhad(13.11, HN, tiež 14.11. v Pravde):   Haulík pripúšťa, že by v sobotu mohlo prísť viac voličov ako naposledy - maximálne 23 až 25 percent. Aj politológ Grigorij Mesežnikov tvrdí, že voličská účasť by sa mohla zastaviť niekde medzi 20 a 25 percentami.   Najhorší: P.Horváth (5.10, HN):   Neočakávam účasť vyššiu ako 15 percent. Z hľadiska doterajších volieb by prekročenie hranice 20 percent bolo najväčším prekvapením.   P.Horvath (13.11, STV):   Číslo nad 20 percent by bolo skutočným prekvapením.     Župani: V Pravde všetkých 8 súbojov minulý týždeň odkomentoval Ján Baránek (Polis), v HN ešte v októbri G. Mesežnikov s P. Horváthom, len niektoré tipovali Miroslav Řádek (Trenčianska univerzita, komentoval súboje TN a ZA), Marcela Gbúrová (Prešovská univerzita, PO a KE) a Radoslava Brhlíková (UKF, NT a BB). M. Horský ani Miroslav Kusý (UK) k voľbám v TT, resp. BA, nechceli víťazov tipovať.  BA – prvý skončil Frešo, v druhom kole s Bajanom   Uhádli druhé kolo všetci, v druhom tipovali Mesežnikov a Baránek Freša, Horváth Bajana    TT – T.Mikuš vyhral už v prvom   Uhádli všetci traja (Baránek, Mesežnikov, Horváth)    TN – Sedláček prvý, v druhom kole s Fedorom   Uhádli všetci druhé kolo    NR – jasne vyhral Belica   Víťazstvo Belicu uhádli všetci, Mesežnikov ho nevylučoval už v prvom kole.     ZA – jasne vyhral Blanár   Opäť uhádli, no všetci čakali druhé kolo     BB – prvý Maňka, druhé kolo s J.Mikušom   Maňkove prvenstvo uhádli všetci, druhé miesto Mikuša neuhádol Horváth, ktorý tipoval Šimka z SNS (ten však skončil v regióne až 5.)    KE – jasne vyhral Trebuľa   Baránek správne tipoval Trebuľove víťazstvo v prvom kole. Horváth a Mesežnikov  až v druhom. Gbúrová tiež tipovala poradie Trebuľa-Süli, no bez rozlíšenia, v ktorom kole. Navyše Mesežnikov to videl ako veľmi tesný súboj so Sülim, ten pritom v prvom kole získal takmer trikrát menej hlasov ako Trebuľa.    PO – prvý Chudík, no druhé kolo s Hudackým   Druhé kolo uhádli Baránek s Mesežnikovom, Horváth víťaza videl v Hagyarim, ten sa ani len nedostal do druhého kola. Gbúrová tipovala súboj Chudík-Hagyari   Analytici teda v priemere podcenili dominanciu úradujúcich županov, lebo polovica krajov už nebude mať druhé kolo. Asi najhorší výrok k tomu mal P.Horváth: (10.11, STV):   Pravdepodobne vo všetkých krajoch bude druhé kolo volieb, kde potom to už bude súboj jedného proti jednému.   Hoci netipovali všetci všetko a tipy padali v rôznych časoch, dá sa zhruba povedať, že z mediálnych predvolebných vystúpení po prvom kole najlepšie vychádza Ján Baránek, tesne za ním Grigorij Mesežnikov.  Najhoršie zase Peter Horváth. Z médií si tak najlepšie vybrali v Pravde, najhoršie v STV. Inak podľa rebríčka mediálnej citovanosti politických analytikov bol minulý rok Baránek na 27.mieste, Mesežnikov na treťom a Horváth bezkonkurenčne na prvom.    Zabúdanie na nevoličov: V celom spravodajstve o župných voľbách mi chýbalo aspoň kúsok priestoru pre nevoličov, aspoň jeden osobitný článok či reportáž v každom médiu. Voliči samozrejme rozhodli. Ale tri štvrtiny sú nevoliči, a je dôležité vedieť ich dôvody pre neúčasť, lebo takto môžeme diskutovať o funkcii a prínose žúp pre krajinu. V televíziách som len počúval x ľudí, že aká je to občianska povinnosť voliť bla bla a len výnimočne hlas nejakého nevoliča. Tesne pred voľbami v piatok mala STV anketu, kde z 3 oslovených dvaja povedali, že samozrejme voliť idú. Reportér Peter Bubla zahlásil, že „predvolebná nálada zasiahla ľudí.“ Čo to má s realitou spoločné?  Dôvera k nášmu parlamentu: Pavel Sibyla v TRENDe cituje podpredsedu parlamentu:   „Parlament je inštitúcia, ktorá je kolbišťom názorov, názorových prúdov a jednotlivé výmeny môže verejnosť vnímať naozaj negatívne,“ myslí si Miroslav Číž zo Smeru a zdôrazňuje nárast dôveryhodnosti parlamentu u občanov za dva roky zo 17 na 38 percent.   Zaujímavé, že prieskumy Eurobarometra, ktorý túto otázku pravidelne kladie dvakrát do roka, nič také nezaznamenal. Od volieb 2006 sa dôveryhodnosť drží s minimálnymi odchýlkami zhruba na tých 38%. Pri 17% to bolo ešte v rokoch 2004-5.  Rýchlosť na úkor kvality: Novinári sa tradične predháňajú, kto ohlási víťazov volieb ako prvý. Nie vždy to dopadne dobre (nový prezident Gore...). Tentokrát na Sme.sk vyhlásili v nedeľu po polnoci za víťaza aj kandidáta, ktorý sa ním nestal.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (50)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




