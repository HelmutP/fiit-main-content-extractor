
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Poliačik
                                        &gt;
                Šamanský šlabikár
                     
                 Šamanský šlabikár – Runin Kamaranti ayahuasca a moje prvé ícaro 

        
            
                                    14.10.2008
            o
            8:00
                        |
            Karma článku:
                11.54
            |
            Prečítané 
            4960-krát
                    
         
     
         
             

                 
                    Včera som chcel na internete nájsť niečo o veľmi dôležitej súčasti mojej diéty, Runin Kamaranti ayahuasce. Predpokladal som, že aspoň v angličtine niečo nájdem, že už o tomto výnimočnom druhu ayahuascy niekto niečo počul a napísal. S veľkým prekvapením som zistil, že Google je úplne bezradný. Pri zadaní "kamaranti" vyhadzuje kopu vecí, ale nič o ayahuasce. "Camaranti" nič, "cama ranti", "camaranthi", "kama ranti".... o ayahuasce ani chýru, ani slychu. Čo teda robí Runin Kamaranti ayahuascu tak výnimočnou?
                 

                 
http://poliacik.blog.sme.sk verzia 1.0.0MP
    Jedného dňa Maestro prišiel a povedal - od budúceho týždňa budeš piť ayahuascu každý deň, 15 dní za sebou. Striaslo ma. Noc s ayahuascou vie byť mimoriadne vyčerpávajúcou záležitosťou a pri predstave, že by som to mal absolvovať 2 týždne vkuse, som ostal zaskočený. Spýtal som sa, či teda bude každý večer ceremónia. Maestro na mňa zadivene pozrel: "Chceš ma zabiť? Budeš piť sám, v tvojej izbe." Bol som blízko panike. Sám, bez Maestrovej ochrany a pomoci, bez jeho ícara, 15 nocí úplne napospas silám džungle, to už bolo dosť aj na mojho dobrodružného ducha. A vtedy som začul prvý krát tie tri slová, Runin Kamaranti ayahuasca - Runin (čítaj řunin, v Shipibo jazyku sa mnohé R číta Ř) znamená Anakonda - duch ayahuascy, ochranca šamana. Význam slova Kamaranti práve zisťujem, dodám časom.  Klasická liana ayahuascy má priemer tak do 50 cm, žnú sa z nej kusy nadzemných častí a z tých sa potom spolu s listami chakruny varí kakavko na ceremónie.       Runin Kamaranti ayahuasca je opradená legendou. Maestro rozprával o jej úctihodných rozmeroch - dvaja dospelí chlapi ju neoblapia. Plazí sa po zemi, potom sa ponára pod vodu a z nej sa po stromoch vyťahuje k nebesiam. Rastie stovky rokov. Nesmie sa z nej rezať, taká pochabosť končí väčšinou smrťou. Rovnako ako iné prejavy neúcty. Maestrov bratranec bol lovec, strieľal opice a kadejakú inú háveď džungeľnú pre obživu. Maestro ho varoval - tým smerom nikdy nechoď a tomu miestu sa zďaleka vyhni! Stráži ho Anakonda, veľmi mocná, a len veľkí majstri môžu vstúpiť.  Potom odišiel do inej osady. Milý bratranec sa vybral na lov a časom zašiel aj do tejto zakázanej oblasti. Necítil sa nijak zvláštne, tak pokračoval ďalej. Zrazu zbadal pohyb v korune stromu, zamieril a vystrelil. V tej chvíli sa zem zatriasla a zahrmelo. Do dvoch týždňov umrel vo vysokých horúčkach. Ani Maestrov dedko, veľký muraya, mu nedokázal pomôcť. Ak chce šaman z tejto ayahuascy pripraviť nápoj, musí sledovať jej koreň. Pomaly a opatrne ho odkrývať a po modlitbe a obetovaní tabaku odrezať kúsok z úplného konca. Runin Kamaranti sa nepije para mariar, teda kvôli navodeniu zmenených stavov vedomia. Pije sa len po troškách para dietar - adept ju pomaličky absorbuje do svojho tela a duch ayahuascy ho navštevuje v snoch a účí, čo potrebuje vedieť.       Druhú noc diéty som sa zobudil okolo tretej nad ránom a počul som Maestra spievať ícaro, silné a rytmické. A v mysli sa mi začali vynárať slová:       ayahuasca liečivá,  zázračnica premocná,  otvor brány žiarivé,  hraj fanfáry zvonivé...       Zobral som zošit a pero a začal písať. Strofu po strofe mi prichádzalo moje prvé ícaro, a je možné, že aj prvé ícaro pre ayahuascu v slovenčine vôbec (toto je otvorená provokácia, ak niekto viete o človeku, čo na ceremóniách spieval ícaro v slovenčine, natrite mi to, zničte ma, potupte :D, len mi prosím dajte na toho dobrodruha kontakt). Takže  - v premiére prvé slovenské ícaro, nech sa páči.      
 
            
            
            
            
            
	     Ráno mi Maestro povedal, že on nič podobné nespieval...     Ak by kohokoľvek z vás zaujímali konkrétne detaily, prípadne by ste sa radi vybrali na diétu s plantas maestras do Peru, napíšte mi mail alebo sa uvidíme v diskusii.  Nabudúce vysvetlím, čo je to toľko omieľané ícaro a čomu sa vlastne v Shipibo šamanizme hovorí diéta. A bonbónik - nahrávku Maestrovho spevu z ceremónie.      Hasta luego     Martín - Mono Blanco    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Martin Poliačik 
                                        
                                            Na povel nám mládež neschudne, pani Zmajkovičová
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Poliačik 
                                        
                                            Špina pod palcom Jany Laššákovej
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Poliačik 
                                        
                                            Ako ďaleko má pozerať štátnik?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Poliačik 
                                        
                                            Dve strany mince menom Smer
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Poliačik 
                                        
                                            Volím Kisku. Scenár bez neho ma totiž desí.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Poliačik
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Poliačik
            
         
        poliacik.blog.sme.sk (rss)
         
                                     
     
        Som zakladajúci člen SaS a predseda jej Výboru pre program. Momentálne za SaS deriem poslanecké lavice a kandidujem v bratislavských komunálnych voľbách.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7339
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            SaS
                        
                     
                                     
                        
                            Šamanský šlabikár
                        
                     
                                     
                        
                            So srdcom na dlani
                        
                     
                                     
                        
                            S rukami na ruckach vozicka
                        
                     
                                     
                        
                            Peruánsky denníček
                        
                     
                                     
                        
                            Múzou praštený
                        
                     
                                     
                        
                            Len tak
                        
                     
                                     
                        
                            S okom na hľadáčiku
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jeremy Narby - Cosmic Serpent - DNA and the origin of Knowledge
                                     
                                                                             
                                            Carlos Castaneda - Separate Reality
                                     
                                                                             
                                            James Redfield - Celestine Prophecy
                                     
                                                                             
                                            Daniel Pinchbeck - 2012: The Year of the Mayan Prophecy
                                     
                                                                             
                                            Kyriacos C. Markides - Pocta slunci
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Depeche Mode
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Predseda
                                     
                                                                             
                                            Bilcove funky
                                     
                                                                             
                                            šamani a rostlinky :-)
                                     
                                                                             
                                            Alica Spencer, Matkin je slabá káva proti tejto žabe
                                     
                                                                             
                                            Andrejka Drozdova
                                     
                                                                             
                                            Ivka Muková
                                     
                                                                             
                                            Child Denisa
                                     
                                                                             
                                            Lenka Krivá
                                     
                                                                             
                                            Lenka Koláriková
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            SaS
                                     
                                                                             
                                            Zuzka
                                     
                                                                             
                                            knihy - Malvern
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Hlavne sa na to nevykašlať
                     
                                                         
                       Z Dukly na Devín peši za 1 deň ?
                     
                                                         
                       Na každú blbosť zo scientológie vám ukážem inú blbosť v kresťanstve
                     
                                                         
                       Petícia za ukončenie ozbrojenej agresie na Ukrajine
                     
                                                         
                       Krvavá zem
                     
                                                         
                       Vy, ktorí ste v novembri 89 štrngali kľúčmi, aké to bolo?
                     
                                                         
                       Sčítavanie rýchlostí pri čelných zrážkach na diaľniciach?
                     
                                                         
                       Nazdravie z Mariatchi!
                     
                                                         
                       Bola raz malá, malá hviezdička...
                     
                                                         
                       Hovoriť o neprispôsobivých občanoch je jednoducho omylom
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




