
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katka Chovancová
                                        &gt;
                Politično
                     
                 Je ťažké byť politikom alebo Koľko stojí čestnosť a zásadovosť 

        
            
                                    26.6.2010
            o
            17:55
                        (upravené
                29.7.2011
                o
                13:21)
                        |
            Karma článku:
                7.46
            |
            Prečítané 
            1023-krát
                    
         
     
         
             

                 
                    Stojí veľa. A po voľbách najviac. Hnutie KDH by vedelo rozprávať, tá ich bola víťazom volieb ohodnotená na polovicu vládnych kresiel i post premiéra. Načo ale uplácať celú politickú stranu, keď potrebná väčšina, ktorou má disponovať ešte len vznikajúca vláda, sa môže rozsypať i inými spôsobmi. Chce to len trocha kreativity.
                 

                 Igor Matovič, Obyčajný človek, budúci poslanec a kandidát strany SaS dostal nečakane, mimovoľne, len tak pri káve zaujímavú ponuku- 20 miliónov eur za dosiahnutie predčasných volieb nepodporením novovznikajúcej pravicovej vlády. Odmietol. To ale máme šťastie, v našom zastupiteľskom orgáne budú sedieť skutočne zásadoví ľudia! Ak vás prepadol pocit radosti a pokoja z toho, že donebavolajúca skorumpovanosť členov parlamentu je už minulosťou, udalosti nasledujúce bezprostredne po tomto vyhlásení pána Matoviča museli zatriasť vašimi ideálmi. Ten vzápätí po tom, čo sa -celkom opodstatnene- o prípad začali zaujímať médiá len opatrne podotkol, že si nie je istý, ale možno išlo o žart. Tento návrh mu vraj dal kamarát a žalovať z pokusu o úplatok ho nemieni- veď to sa kamarátom nerobí. Usmiaty pán Matovič pred kamerami ešte vyhlásil, že pre istotu sa inkriminovaného priateľa opýta, ako vážne tú ponuku myslel. A následne sa slovenský občan dozvedá, že chodom udalostí zarmútený priateľ ponuku dal čisto „zo srandy".   Zas raz nič. Žiadna zmena. Človek si už priam musí hľadať podnety na to, aby mal pocit, že nám svitá nádej na budúcnosť, ktorá nebude z každej strany opradená korupciou. Ale aj tú nádej mu vezmú. Zo srandy.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (31)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Chovancová 
                                        
                                            Najzákladnejši múdrosti pre uchádzačov o zamestnanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Chovancová 
                                        
                                            Prečo som neprotestovala proti Gorile
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Chovancová 
                                        
                                            O punkeroch a dobročinnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Chovancová 
                                        
                                            Ani s tým Nota Bene to nie je len tak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katka Chovancová 
                                        
                                            O intimite, viere a náhodách
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katka Chovancová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katka Chovancová
            
         
        katkachovancova.blog.sme.sk (rss)
         
                                     
     
        Študujem, pracujem, realizujem sa v občianskom združení, ale hlavne- vnímam svet okolo seba.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1670
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politično
                        
                     
                                     
                        
                            O mojom a jemu podobných sveto
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




