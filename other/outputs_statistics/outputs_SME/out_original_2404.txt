
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Nechceme zavádzať školné, drogy sú svinstvo a program nepísal šaman 

        
            
                                    28.2.2010
            o
            21:23
                        |
            Karma článku:
                15.09
            |
            Prečítané 
            32657-krát
                    
         
     
         
             

                 
                    Nechceme ani odstrániť všetku reguláciu. Náš volebný program sme predstavili ako prvá strana. Namiesto prázdnych fráz sme napísali 120 konkrétnych nápadov, ktoré zlepšia život na Slovensku. I napriek našej snahe písať jasne a vecne, došlo k niekoľkým nedorozumeniam, ku ktorým by som rád poskytol záväzné stanovisko strany:
                 

                 Nechceme zaviesť školné   SaS považuje za správne, že Ústava v článku 42 garantuje vzdelanie na základných a stredných školách bezplatne. Tento stav chceme zachovať a preto nie je pravda, že navrhujeme školné na základných a stredných školách. Navrhujeme však zaviesť normatív ako jediný zdroj financovania štátom a jeho výška má pokryť náklady v plnom rozsahu. V dnešnom systéme je financovanie škôl rozdrobené a chaotické. Nami navrhovaný normatív poskytne škole potrebné prostriedky v jednom balíku a dá im možnosť slobodne rozhodnúť o ich použití. Okrem škôl, ktoré nebudú smieť  vyberať poplatky (takzvaná minimálna sieť, ktorej rozsah bude porovnateľný s dnešnou sieťou škôl), môžu vzniknúť ďalšie školy, ktoré budú mať možnosť vyberať poplatky, čo je možné aj dnes.   Nechceme rušiť reguláciu   Najmä tam, kde sa vyskytujú monopoly (alebo takmer monopoly), je nutná regulácia. Napríklad cena tepla musí byť regulovaná, inak by tepláreň ako monopolný dodávateľ tepla mohla účtovať akúkoľvek sumu. Preto existuje Úrad pre reguláciu sieťových odvetví. To, čo chceme my, sú jasné a logické pravidlá pri regulácii (napríklad dnes je zisk teplárne stanovený ako percento nákladov, tepláreň má teda záujem o čím vyššie náklady a to je zlé). Chceme lepšie pravidlá, nechceme však rušiť samotnú reguláciu, ako nás neprávom obvinil Rado Baťo. Nenavrhujeme zrušenie ani jedného regulačného úradu. Sme sa rozumnú reguláciu, sme ale proti zbytočným zásahom do podnikateľskej slobody.   Drogy sú svinstvo   Predpokladám, že každý súdny človek sa stotožní s týmto tvrdením. Či heroín, marihuana, alkohol alebo nikotín, drogy sú skratka svinstvo. Napáchali veľa zla a zničili už veľa osudov. Najlepšie by bolo, keby drogy neexistovali. Keď ale už raz existujú, nesmieme sa tváriť, že všetko je v poriadku len tým, že ich zakážeme. Práve preto navrhujeme dekriminalizáciu marihuany. Nechceme, aby niekto mal problémy  so zákonom kvôli tomu, že fajčil trávu. Nie je predsa správne strkať ľudí do basy kvôli tomu, že škodia sebe a nie iným. Preto sme za to, aby užívanie marihuany nebolo trestným činom. Naďalej sme však proti jej predaju.   Program nepísal šaman   Pani redaktorka z Markízy sa zrejme trochu nechala uniesť, no Martin Poliačik, ktorý písal program, nie je šaman. Žil 9 mesiacov v Peru a tam sa u šamana učil napríklad variť bylinky. To však nie je dôvod, prečo je Martin zodpovedný za program. Dôvod je ten, že Martin patrí k najlepším debatérom v Čechách a na Slovensku a presne takého sme potrebovali. Jeho úlohou totiž nebolo napísať celý program (na to máme jednotlivých tímlídrov), ale v spolupráci s nimi vytvoriť jednotný a čitateľný dokument. A to sa podarilo.   Určite existujú väčšie problémy a lepšie riešenia, ako našich 120 nápadov. To preto, lebo sa nehráme na vševedov a tiež preto, že nemáme v programe žiadne všeobecné bláboly. Navrhujeme len tie riešenia, o ktorých vieme, že budú prínosom a ak by sa zrealizovalo len našich 120 nápadov, stalo by sa Slovensko krajinou, v ktorej by bolo radosť žiť.   Na svete je teda program, ktorý nás stál veľa úsilia a na ktorý sme hrdí. To ale neznamená, že už nie je čo zlepšovať. Za akékoľvek pripomienky a ďalšie nápady ako zlepšiť život na Slovensku sme vďační a tie, ktoré budeme považovať za oprávnené a v súlade s našimi hodnotami, radi do programu zapracujeme. Prosím všetkých čitateľov, aby ich posielali priamo mne na richard@sulik.sk.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (673)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




