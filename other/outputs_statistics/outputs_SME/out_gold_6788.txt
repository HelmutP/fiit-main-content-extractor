

 Naraz pozerám ako obarená - zo stola spomedzi maminej šatky na krk a iných drobností na mňa zíza skratka slotostrany. 
 Svoju mamu poznám ako staré topánky a je mi jasné, že to nie je jej maslo. 
 A naozaj nie - keď odhrniem šatku, zisťujem, že na mňa máva krídlami orol a spod neho vyčnieva trochu hanblivo mamin cestovný lístok, zastrčený do puzdierka papierového obalu. 
 Nesiem to ukázať mame - je z toho prekvapená rovnako ako ja. Obal na lístok jej vraj pridala pokladníčka na stanici. 
 Neviem, či železnice pravidelne poskytujú svojim cestujúcim takýto luxus v podobe obalu na cestovný lístok z kvalitného kriedového papiera. Ale nech - len aby aj ostané služby tomu zodpovedali. 
 Viac ma škrie iné. 
 Železnice sú štátny podnik, a každoročne sa trúbi do sveta, akú majú sekeru a ako ich štát dotuje zo zmilovania z našich a teda aj mojich peňazí. 
 Je jasné, že práve tie isté železnice by sa mali čo najviac snažiť prihrabnúť nejaké to euro prostredníctvom legálnych možností - teda aj cez reklamu. (I keď ideálnejšie by to bolo cez pragmatickejšie riadenie, o čom radoví zamestnanci železníc vedia rozprávať celé state.) 
 Ale - keďže sú tieto železnice štátny podnik a dokrmujú ich z našich peňazí, príde mi totálne neetické, aby sa v štátnom podniku objavila verejne dostupná reklama na jednu politickú stranu, čo ako zazobanú. 
 Je mi jedno, či sú šumy o prepojení SNS a železníc pravdivé alebo nie a kam vlastne nitky tohto puzdra na cestovný lístok s trikolórou a chvastúnskym textom SNS vedú. 
 Ak totiž bude precedens takýto, tak potom môžem pomaly očakávať, že mi príde z Daňového riaditeľstva či štátnej nemocnice papier o hocičom s reklamou tej strany, ktorá si tam nájde cestičku... 
 Ja viem, jednoduchá robotička - nedá sa tomu uniknúť a marketingový záber je neuveriteľne veľký. 
 Ako daňový poplatník a tak trochu teda aj správca peňazí, ktoré sa točia v štátnych kolosoch si neželám, aby sa spájali s predvolebnou reklamou. 
 Čo je štátne, nech ostane apolitické a nestranné. 
 Aj keby motyka vystrelila a musela by som sa nebodaj dívať na žilinského Jana ako na šéfa parlamentu. 
 A basta! 

