

 "Majte na mysli slovo, ktoré som vám povedal: Ak mňa  prenasledovali, budú prenasledovať aj vás." - Ján 15:20. 
 Tieto inšpirované slová sú veľmi známe a počas asi 2,000- ročnej histórií boli dokázané nespočetnekrát. Lenže pozrime sa  na toto prehlásenie z iného úhlu pohľadu. Z úhlu pohľadu jedného z najväčších osobností modernej vedy minulého storočia. A to z úhlu pohľadu A. Einsteina a jeho presláveného citátu, ktorý znie asi takto: ´Veľké osobnosti vždy narážali na odpor priemerných ľudí´. Lebo priemerná osobnosť nemôže porozumieť človeku ktorý odmietol slepo  sa držať konvenčných zvyklostí a namiesto toho sa rozhodol vyjadriť svoj názor odvážne a úprimne. Nemecko-americký teoretický fyzik z 20. storočia, Albert Einstein, sám pocíťoval i vo svojom vedeckom živote pravdivosť slov svojho citátu. Lenže nikto z ľudstva nie je väčšou osobnosťou ako dokonalý muž, Boží Syn, Ježiš Kristus. Preto tento princíp v plnej miere aplikuje na jeho apoštolov i učeníkov, ktorých apoštol Pavol povzbudzuje takto: 
 "Lebo ´kto spoznal Jehovovu myseľ, aby ho poučoval´?  Ale my máme skutočne Kristovú myseľ." - 1. Korinťanom 2:16. 
 A tak je to až dodnes. Veľkým osobnostiam (pravým Kresťanom) odporovali a stále budú odporovať zúrivými útokmi priemerní ľudia až do konca! 

