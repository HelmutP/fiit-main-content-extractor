

 Presná lokalizácia Zádielskej tiesňavy je na následujúcej adrese: 
 http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=sk&amp;geocode=&amp;q=Z%C3%A1diel&amp;sll=37.0625,-95.677068&amp;sspn=51.754532,96.591797&amp;ie=UTF8&amp;ll=48.622073,20.84733&amp;spn=0.042609,0.094328&amp;t=h&amp;z=14 
 Wikipedia uvádza, že Zádielska tiesňava je monumentálny krasový kaňon s hĺbkou takmer 400 m, dĺžkou 3 km, najužšia šírka dna 10 m. Nachádza sa v Slovenskom krase pri obci Zádiel v Košickom kraji. Je vytvorený vodami pretekajúceho Blatného potoka a je chránený už od roku 1954. Pozornosť zaujme najmä 105 m vysoká ihlanovitá skala vymodelovaná erozívnou činnosťou vody, ktorá pre svoj tvar dostala meno Cukrová homoľa. Cukrová homoľa je najvyšší vežovitý skalný útvar na Slovensku.  Charakteristiku flóry a fauny podmieňuje hraničné položenie medzi Slovenským krasom a Slovenským rudohorím. Územie je známe aj zvratom vegetačných pásiem. Zádielska dolina je známa už od praveku, prechádzala tadiaľto dôležitá komunikácia spájajúca Rožňavskú a Košickú kotlinu, čoho svedkom sú aj skromné historické pamiatky, najmä zo staršej doby bronzovej. 
 Návšteva Zádielskej tiesňavy je ideálnou myšlienkou na absolvovanie nenáročného výletu. Prístup je priamo z konca obce Zádiel, kde je pre návštevníkov zriadené parkovisko. Priamo z neho ide celou tiesňavou asfaltová cesta, pričom táto je po celej dĺžke v miernom stúpavom sklone. Celá tiesňava sa dá prejsť zhruba za necelú hodinku a to pri pohodlnej chôdzi. Na konci kaňona je možnosť sa občerstviť v bufete a je možné si aj opiecť nejaké tie špekáčiky. 
 Ako som už spomínal, doposiaľ som tento výlet absolvoval len po spomínanej asfaltke, pričom keď uvážim, že som tam bol najmenej osemkrát, tak som tú cestu tam a späť prešiel 16 krát. Prečo to spomínam? Posledný výlet som absolvoval po trase, ktorá vedie vrchom tiesňavy. Existuje niekoľko možností ako sa na vrch tiesňavy dostať. Jedno riešenie je absolvovať spomínanú asfaltku, pričom blízko križovatky pred spomínaným bufetom je odbočka, vedúca cez drevený mostík ponad potok smerom do lesa a odtiaľ už nie je problém sa dostať na vrchol tiesňavy, prejsť jú celú vrchom a v závere zostúpiť po chodníku do obce Zádiel. Druhá možnosť je odparkovať svoje vozidlo, resp. vystúpiť z autobusu v obci Turňa nad Bodvou a odtiaľ si urobiť výstup na Turniansky hrad, pri ktorom je rázcestie, z ktorého jedna cesta vedie priamo na vrch tiesňavy, odkiaľ je možné sa rozhodnúť, či zostúpiť do obce Zádiel alebo prejsť vrchom k spomínanému bufetu a asfaltkou dôjsť do Zádielu. Pre tých, čo by si odparkovali vozidlo v Turni nad Bodvou to neodporúčam, nakoľko zo Zádielu je to k odparkovanému vozidlu dosť ďaleko. Tretia možnosť je vystúpiť na vrch tiesňavy priamo z obce Zádiel, prejsť ju vrchom a vrátiť sa asfaltkou. Túto možnosť som zvolil aj ja. 
 Čo je možné hore očakávať? No, predsa nádherný výhľad na bralá tiesňavy a na krajinu okolo. Mimo toho je hore nádherná príroda v podobe planín, lúk a lesov. 
 Pre tých, ktorým by bol takýto výlet málo, odporúčam navštíviť aj spomínaný Turniansky hrad, z ktorého je naozaj neskutočný výhľad a taktiež blízke Hájske vodopády, ktoré sú za neďalekou obcou Háj. V spomínanej obci je na miestnom cintoríne aj socha anjela, ktorá bola použitá pri natáčaní filmu Za nepriateľskou líniou ktorý sa natáčal v okolí. 
 Čo na záver. Už len pár fotiek: 
   
  
 Vstup do Zádielskej tiesňany 
   
  
 Pohľad na tiesňavu zo spodu 
   
  
 Asfaltka na dne tiesňavy 
   
  
 Zádielsky Blatný potok 
   
  
 Blatný potok 
   
  
 Ešte jeden potok 
   
  
 Takéto zvieratká boli všade celou cestou 
   
  
 Pohľad na Cukrovú homoľu 
   
  
 To je záver Zádielskej tiesňavy 
   
  
 Kvety (názov po mne nechcite) 
   
  
 Znovu tá asfaltka 
   
  
 A toto sú Hájske vodopády 
   
  
 Takéto potvorky bolo možné stretnúť hore na planine (vedeli aj riadne vystrašiť) 
   
  
 Pohľad na obec Háj od Turnianskeho hradu 
   
  
 A tu je pohľad z Turnianskeho hradu na opačnú stranu 
   
  
 Ruiny hradu 
   
  
 To hore ma zaujalo - vyzerá to ako bonsaj 
   
  
 Najzachovalejšia časť hradu 
   
  
 Lúky na vrchu tiesňavy 
   
  
 Takáto zeleň na nás čakala na vrchu tiesňavy 
   
  
 Kvet (aký?) 
   
  
 Zádielske planiny 
   
  
 No a to najlepšie k záveru - vrchol tiesňavy 
   
  
 Zeleň na bralách 
   
  
 Pohľad z vrchu dole 
   
  
 Moja maličkosť - autor 
   
   
   
   

