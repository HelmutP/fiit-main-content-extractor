

 Právny pohľad: 
Nový zákon 8/2009 Z.z. umožnil okrem paušálnej rýchlosti definovať aj lokálne upravenú rýchlosť a to nielen smerom k jej zníženiu ale aj k zvýšeniu.  


 Kto môže zmeniť rýchlosť: 
Najdôležitejší kto musí chcieť spraviť zmenu je správca komunikácie. 
Nemôže zmeniť dopravné značenie svojvoľne ale musí vypracovať projekt so 
všetkými náležitosťami, ktorý následne podlieha schvaľovaniu cestným správnym 
orgánom. Ak sa všetko schváli, potom správca komunikácie zabezpečí realizáciu v zmysle projektu. 

 Sú dnešné vozidlá stavané na rýchlosť 160 km/hod? 
Áno väčšina osobných vozidiel je stavaná aj na vyššie rýchlosti ako 160 km/hod. Nie je to len výkon motora, ale aj brzdného systému, stabilizačných systémov a aj pasívneho a aktívneho zabezpečenia posádky vozidla. Existujú aj staršie vozidlá, ktoré túto rýchlosť nedosiahnu, alebo dosiahnu ju na hranici svojich možností. Pritom si treba uvedomiť, že vyššia rýchlosť nie je povinnosť, ale len možnosť za vyhovujúcich podmienok, do ktorej spadá aj stav vozidla. 

 Je potrebné prispôsobiť sa k najslabším vozidlám? 
Ja si myslím, že v prípade diaľnic nie. Diaľnice sú určené na rýchle prekonanie veľkých vzdialeností a z toho pramení aj požiadavka na rýchlosť. Už teraz platí, že na diaľnice nesmú  isť vozidlá, ktoré nedosiahnu aspoň minimálnu rýchlosť 80 km/hod. Diaľnice sú konštrukčne uspôsobené na bezpečné predbiehanie preto rýchlosť nerobí problém ak aby v jednom pruhu išlo vozidlo rýchlosťou 80 km/hod a v druhom o 80 km/hod viac. Ak sa pozrieme do štatistiky, tak počet pokút udelený vozidlám idúcich rýchlejšie je podstatne vyšší ako počet pokút za nízku rýchlosť. 

 Je potrebné prispôsobiť sa najslabším vodičom? 
Väčšina vodičov zvláda rýchlu jazdu i keď osobne som za to, dať im školenie rýchlej jazdy. Lenže som aj za to, dať im základne školenie na zvládnutie obyčajného šmyku na snehu pri nízkych rýchlostiach, ktorú časť vodičov nezvláda. 

 Existuje krajina kde je možné jazdiť rýchlejšie ako 130 km/hod? 
Áno existuje. Najbližšie k nám je Nemecko, kde maximálna rýchlosť na diaľnici nie je obmedzená, respektíve je obmedzená len aktuálnou poveternostnou a dopravnou situáciou. V Nemecku mám najazdených niekoľko tisíc km a môžem povedať, že sa tam jazdí oveľa príjemnejšie. Vodič namiesto pozerania na tachometer, aby nedostal pokutu za rýchlosť, viac sleduje dopravnú situáciu a prispôsobuje svoju rýchlosť jej, čo priaznivo vplýva na bezpečnosť. 

 Ako sa pozerajú na zvýšenie rýchlosti v zahraničí? 
Je to rôzne. Záleží od konkrétneho človeka. Nedávno som zažil jednu priam komickú situáciu. Na medzinárodnej konferencii padla otázka na zástupcu EU ohľadom povoľovania vyššej rýchlosti Slovensku.  Reakcia zástupcu bola razantne negatívna. Povedal: zvýšenie rýchlosti neodporúčam, lebo tým spravíte zlý príklad pre ostatných. Kto ten neznámy zástupca EU je, som zistil až pri jeho ďalšej vete: odpovedám vám presne tak ako ste mi odpovedali keď som navrhol zrušiť nulovú hranicu pre toleranciu alkoholu vo vašej krajine.  

 Spomínam si, že keď nastúpil do funkcie, tak tlačou prebehla informácia o jeho cieľoch a nim bolo podľa médií zrušenie neobmedzenej rýchlosti na diaľniciach v Nemecku a zrušenie nulovej tolerancie alkoholu v nových krajinách východnej Európy.  Ľudsky povedané, závidel vodičom v Nemecku, že môžu jazdiť rýchlo a bez strachu z pokuty a hneval sa na východné krajiny, že si tu nemôže dať obvyklý pohár vína k obedu. 


 Čo na vyššiu rýchlosť hovoria záujmové združenia? 
Je známe, že Rakúsku testovali rýchlosti 160 km/hod. Testy 
z hľadiska bezpečnosti dopravy dopadli dobre, lenže proti sa ozvali práve 
záujmové združenia, ktoré namietali zvýšenú tvorbu CO2 a hluku. V niektorých štátoch EU sú známe diaľnice, kde z údajného dôvodu zníženia emisií znížili rýchlosť na 100 a 80! km/hod. Podľa mňa je to len 
podvod, lebo obmedzenie rýchlosti platí pre všetky vozidla a to aj hybridné 
vozidlá s elektrickým pohonom,  elektromobily a aj vozidla jazdiace na vodík. Čiže zníženie emisií je len takticky manéver pre obhájenie svojich záujmov. 


 Je vyššia rýchlosť nebezpečnejšia? 
Áno je. Vyššia rýchlosť predstavuje potenciálne vyššie 
riziko pri dopravnej nehode. Platí to všeobecne. Lenže z vývojom technológií sa 
posúva aj hranica akceptovateľnosti vyššej rýchlostiz a z nej plynúceho rizika. Napríklad zvýšenie rýchlosti vlakov nikomu nevadí, pritom je známe, že dopravná nehoda vlaku pri vyššej rýchlosti, predstavuje väčšie riziko ako dopraná nehoda osobného auta. Stačí si spomenúť na zrážku vlaku a mosta v susednej ČR. Navyše z príkladu Nemecka je zrejme, že striktne neobmedzená rýchlosť neznamená viac dopravných nehôd, práve naopak. 


 Aké argumenty proti sú na Slovensku? 
Zatiaľ jediný relevantný argument proti, som čítal o zanedbateľnej 
časovej úspore a predĺžený brzdnej dráhy od Ľubomíra Palčáka šéfa Výskumného 
ústavu dopravného v Žiline: "Na 20-30 kilometrovom úseku rozdiel v čase prejazdu je 2-3 min. Dochádza tu k nárastu brzdnej dráhy až o 50%. Myslím si, že správanie vodičov na Slovensku ešte nie je na takej úrovni, aby sa dalo predpokladať, že zvýšenie rýchlosti prinesie efekty." 

 To že brzdná dráha pri 160 km/hod voči 130 km/hod narastá je fakt, otázka je či až o 50% percent, najmä ak je vozidlo vybavené adekvátnym brzdným systémom. Keďže ide o prvé úseky s takouto rýchlosťou, priorita nie je v  šetrení času ale v odskúšaní vyššej rýchlosti nielen u vodičov, ale aj u správcu komunikácie a aj dopravnej polície. 

 Čo sa týka úrovne vodičov, tak za posledné roky stúpla. Navyše je ju možné zvýšiť. K tomu sa dajú použiť médiá ale aj preskúšavania pri spravení priestupku. 


 Čo bráni zavedeniu vyššej rýchlosti a vytypovaných úsekoch? 
Viem, že ministerstvo dopravy a aj vnútra je za. Vytypované úseky poznám a mám ich veľa krát odjazdené. Viem, že vodiči na týchto úsekoch často idú rýchlejšie ako povolených 130 km/hod a pritom idú bezpečne. 

 Podľa všetkého Národná diaľničná spoločnosť si dala vypracovať ďalšiu štúdiu, pre úseky D1 medzi Lúkami a Rakoľubami a medzi Vrtižerom a Bytčou, na základe, ktorej tvrdí, že existuje 60 až 70 pripomienok, 
ktoré je potrebné splniť aby mohlo dôjsť k zvýšeniu rýchlosti, pričom náklady 
predstavujú do milióna Euro. 

 Kompletný zoznam pripomienok sa ku mne zatiaľ nedostal, ale k niektorým, ktoré boli mediálne publikované si dovolím zaujať stanovisko. V každom prípade Národná diaľničná spoločnosť postupovala správne, že si dala 
takúto štúdiu vypracovať. 



 Výrub stromov. 
Môžem povedať, že z tejto pripomienky som zostal zaskočený. Totiž podľa noriem pre diaľnicu vyplýva, že do vozovky a v jej tesnej blízkosti nesmú byť pevné prekážky, teda ani stromy. Možno ide o niečo zabudnuté, alebo narastené tesne za zvodidlami ale v princípe to tam nesmie byť ani pri rýchlosti 130 km/hod. 

 Náklon v zákrutách a dĺžky odbočovacích pruhov. 
Tento problém na Slovensku skutočne existuje a bohužiaľ dotýka sa najmä novo postavených diaľnic. Mne z neznámeho dôvodu, došlo k zníženiu návrhovej rýchlosť na 80 km/hod. Z nej sa počíta dĺžka odbočovacích aj pripájacich pruhov a čo je ešte dôležitejšie aj náklon vozovky v zákrutách. Markantne je to vidieť napríklad aj na diaľnici Bratislava Trnava, kde vďaka nízkej návrhovej rýchlosti, sa nepostavili dostatočne dlhé odbočovacie pruhy na odpočívadlá a odbočku napríklad do Senca a muselo dôjsť k zníženiu rýchlosti pravého jazdného pruhu na 80 km/hod. 

 Tento problém je vážny a je potrebné ho riešiť nielen na spomínaných úsekoch ale hlavne pri výstavbe nových diaľnic a rýchlostných ciest, aj pri PPP. Lebo ak naďalej budeme stavať diaľnice pre rýchlosť 80 km/hod tak sú to vyhodené peniaze. Čiže ak je na niektorých zákrutách zlý náklon, ktorý by mohol za určitých podmienok spôsobiť vyletenie vozidla z vozovky tak je potrebné tam rýchlosť za týchto určitých podmienok obmedziť. 


 Premenné dopravné značenie. 
Jednou z podmienok je aj požiadavka na premenné dopravné značenie. Keď mám pravdu napísať, tak som očakával premenné dopravné značenie na 6 pruhu Bratislava Trnava. Nakoniec sa ukázalo, že to vodiči zvládajú aj bez neho i keď určitý komfort by iste prinieslo. 

 Keď si uvedomíme k čomu sa používa premenne dopravné značenie meniace maximálnu povolenú rýchlosť tak zistíme, že sa tam dá v tomto prípade zaobísť aj bez neho. 

 Najčastejšie sa maximálna rýchlosť mení od počasia, čím horšie počasie tým nižšia rýchlosť. Druhé použitie je v závislosti na hustote dopravy a mimoriadnych situácii. V niektorých veľkomestách využívajú zmenu 
maximálnej povolenej rýchlosti na rozkladanie nárazov v doprave s cieľom 
zvýšenia plynulosti. Väčšinou sa k tomu používajú automatické systémy, ktoré 
merajú hustotu dopravy veľa kilometrov pred príjazdom do mesta a na základe 
zváženia situácie na celej trase automaticky menia rýchlosť prostredníctvom 
premenného dopravného značenia, v budúcnosti zrejme aj prostredníctvom 
virtuálnych dopravných značiek. Zatiaľ takýto automatický systém na Slovensku 
neexistuje. 

 Z uvedeného si myslím, že na uvedených úsekoch sa dá zaobísť aj bez premenného dopravného značenia. Počasie aj hustotu dopravy  možné vyriešiť dodatkovou tabuľkou:  "za sucha a pri rozstupe väčšom ako XXX metrov". 
To XXX by mohlo byt v pásme 100 až 200 metrov na základe empirického odskúšania. 


 Záver: 
Je známe, že nepadlo posledné slovo, že 160 km/hod definitívne nebude. Naopak, 
hľadajú sa iné lokality, kde by bolo možné rýchlosť s minimálnymi nákladmi 
zvýšiť na 160 km/hod. Naproti tomu z EU centrály prichádzajú environmentálne 
návrhy na zníženie rýchlosti na diaľnici z obvyklej 130 na 120, 100 až 80 km/hod 
a pre všetky vozidlá, nielen tie, ktoré produkujú CO2 nad určitou hranicou.  

