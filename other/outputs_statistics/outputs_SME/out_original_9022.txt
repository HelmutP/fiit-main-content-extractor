
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Strapko
                                        &gt;
                Domáca politika
                     
                 Fico stále dúfa; SNS už nevadia ani farizeji 

        
            
                                    17.6.2010
            o
            8:25
                        (upravené
                1.3.2011
                o
                8:40)
                        |
            Karma článku:
                10.91
            |
            Prečítané 
            2189-krát
                    
         
     
         
             

                 
                    Umenie presadiť sa na politickej scéne vo veľkom záleží na správnej "bojovej" taktike. Je preto veľmi zaujímavé aké všelijaké hry hrajú jednotlivé politické strany. Pred voľbami na seba ceria zuby, štekajú, ale po nich by si najradšej oblizovali ksichtíky. Je takmer pravidlom, že vždy sa to týka najmä politických subjektov, ktorým hrozí strata privilégia vládnuť. Smer a SNS nám to potvrdzujú.
                 

                 Keď som sa asi týždeň pred voľbami stretol s jedným známym, ktorý má kontakty na vysokých politických postoch, prezradil mi jednu pikošku. Povedal mi, že ľuďom čo sú pri koryte naozaj zviera zadok a boja sa o svoje posty. Spočiatku som si myslel, že vraví len o členoch strany SNS, ale on povedal, že rovnako sú na tom aj SMERáci, pretože sa boja, že nebudú mať s kým zostaviť vládu.   Teraz je skoro týždeň po voľbách a ako sa ukázalo, mal 100pro pravdu. Obavy ľudí pri koryte sa do bodky naplnili.   Smer   Keď som v nedeľnej relácii TV Markíza sledoval ako Fico dostal hneď 4 krát košom v priebehu pár sekúnd, tak som na ňom videl mierny šok. Priznám sa, na pár sekúnd mi ho prišlo aj trošku ľúto, pretože opozičníci si svoje víťazstvo naozaj vychutnávali a Ficovi dávali čo treba (najmä keď nahodil také sklamané očká).   Na druhej strane som si ale uvedomil, že to, aký koaličný potenciál si dokážeš vytvoriť je len a len vecou a chybou Smeru. Politika vedená na antagonizme nemá dlhé trvanie a Ficovi sa teraz zrejme vypomstila.   Navyše, keď sa Fico otriasol, znova začal v zatmení ochrany svojej hrdosti chŕliť bludy a urážky...a vtedy má už akékoľvek smutné Ficove oči nedonútili empaticky nazerať na vec:-)   A Smer pokračuje, má ešte takmer týždeň na to, aby lámal sľubmi politické strany a prehovoril ich ísť na cestu s ním. Najlepšou obeťou pytačiek je KDH, strana, ktorá je zásadová. Je zásadová vo svojom programe (kvôli nenaplnenému vládnemu programu zrušila Dzurindovu vládu).  Fico sľubuje zrejme hory-doly, robí všetko, aby nebol odstavený od koryta. Nejde o ľudí, ale asi ide viac o kšefty. Prestrihnutie pupočnej šnúry k štátnej kase a k možnosti rozhodovať "transparentne" o verejných zákazkách by Smer asi stálo veľa sponzorov...   SNS   SNS zlyhala?... ja si nemyslím. SNS nemala v čom zlyhať, pretože nemá žiadny prínos myšlienok. SNS nezlyhala, pretože stále riešila tie svoje národné záujmy. SNS nezlyhala, to len "veľký" brat Smer prevzal SNS tému. Tých 6 či 7 percent Smeru oproti voľbám 2006 presne kopíruje stratu SNS vo voľbách 2010.   Nie je to tak dávno, čo SNS označila KDH ako farizejov, ako ľudí, ktorí nie sú národne orientovaní, ľudia, ktorí podporujú Maďarov....atď.   Dnes, kedy SNS nedostala ani len pozvánku na rokovanie so Smerom sa pomaličky podsúva a nechce byť na okraji politického diania.   Slotu zraze osvietilo, zmenil sa o 180 stupňov. KDH by bolo podľa neho dobré riešenie, pretože by tu bola pravá slovenská vláda. KDH doslova chváli za ich kresťanské postoje, vyjadruje im podporu v snahe o prijatie zmlúv s Vatikánom...       Smer aj SNS robí všetko preto, aby ostalo pri moci. A šikovne robí všetko preto, aby KDH strápnilo...   Politika je jedno veľké divadlo....       PS: Ak KDH ozaj trvá na svojich zásadách, tak sa zachová rozumne.             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Fico odchádza - podobne ako Mečiar aj Putin
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Komunálne voľby odhalili množstvo chýb - Môžu vás stáť budúci post!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Z vlastnej vôle, či bez - hlavne, že ideš preč!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Strapko 
                                        
                                            Should I stay or should I go?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Strapko
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Strapko
            
         
        strapko.blog.sme.sk (rss)
         
                                     
     
         Mám rád ľudí, aj keď často nedokážem pochopiť ich konanie. A napriek tomu, že som často moralista a skeptik, nikdy neprestávam hľadať pozitívne aspekty negatívnych javov. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    267
                
                
                    Celková karma
                    
                                                8.08
                    
                
                
                    Priemerná čítanosť
                    1523
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Domáca politika
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Z môjho života
                        
                     
                                     
                        
                            Zahraničná politika
                        
                     
                                     
                        
                            Škola
                        
                     
                                     
                        
                            Osobnosti, zaujímavosti
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Úradnícka vláda ako plán Ficovho návratu
                     
                                                         
                       Stránka katedry žurnalistiky radila, ako obísť Piano
                     
                                                         
                       Fico de facto pripustil Paškovu zodpovednosť, ale...
                     
                                                         
                       Anarchista odstúpil
                     
                                                         
                       Slovenský gympel, americká "high school"
                     
                                                         
                       Yankees go home! (desať minút s národniarom)
                     
                                                         
                       Medzi väzňami v Nairobi, alebo keď si myslíte, že vás už v živote máločo prekvapí.
                     
                                                         
                       Buď chlap
                     
                                                         
                       'Satanistický' festival? Meriame dvojakým metrom. A je to ešte horšie.
                     
                                                         
                       Slušne platený bočák
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




