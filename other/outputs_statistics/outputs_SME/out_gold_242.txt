

Najprv vám predstavím rýchly internet od T-Mobile, ktorý sa delí na 3 programy: 
 
Pripájať sa môžete cez F-OFDM, v podstate ho využijete len vo veľkých mestách a 3G, HSDPA ktoré je tiež malé pokrytie a EDGE či GPRS (v podstate veľmi malá rýchlosť internetu). 
 
Rýchly internet 2 

 
	 Aktivačný poplatok 355,80 SK 
	 
 
 
	 Mesačný poplatok 712,80 SK 
	 
 
 
	 Voľný objem dát 2 GB 
	 
 
 
Tento program vám silne neodporúčam. Ak ste jediný užívateľ a budete si len prezerať vaše e-maily, komunikovať cez Skype, ICQ, Messenger poprípade iný komunikátor (samozrejme ak nebudete telefonovať), sem tam si poklikáte na internete tak vám to stačí. 
 
Rýchly internet 10 
 
	 Aktivačný poplatok 355,80 SK 
	 
 
 
	 Mesačný poplatok 1188,80 SK 
	 
 
 
	 Voľný objem dát 10 GB 
	 
 
 
Tento program je veľmi veľký skok od prvého. Či už dátovo alebo cenovo. V podstate môžete surfovať koľko sa vám zachce, sem tam si pozrieť nejaké video na internete, môžete si dovoliť aj také 10 minútové rozhovory denne cez váš komunikátor a keď vám ostane niečo na konci vášho fakturačného obdobia tak aj stiahnuť nejaké mp3, alebo film poprípade program. 
 
Rýchly internet 20 
 
	 Aktivačný poplatok 355,80 SK 
	 
 
 
	 Mesačný poplatok 2259,80 SK 
	 
 
 
	 Voľný objem dát 20 GB 
	 
 
S týmto programom môžete robiť čokoľvek. Len treba dávať pozor na data. Osobne by som si nikdy tento program neobjednal. Zbytočne veľa peňazí a dát. 
 
Pripájať sa môžete cez 
 
	 F-OFDM kartu dosahuje rýchlosť 5,3 Mb/s (malé pokrytie Slovenska) 
	 
 
 
	 Globe Trotter Express 7.2 Mbit rýchlosť 3G, HSDPA až 3,6 Mb/s 
	 
 
K týmto dátovým kartám (s možnosťou pripojiť len k notebookom) máte možnosť si pripojiť prídavnú anténu pre zlepšenie signálu (čo vám silne odporúčam). 
 
	 USB Modem Huawei (odporúčam), niečo podobné ako GlobeTrotter Express 7.2 
	 
 
Tieto ceny platia samozrejme ak sa upíšete na 24 či 36 mesiacov. 
 
 
Teraz sa zoznámime s ponukou od Orange. 
 
Orange nám ponúka 3 balíky ktoré sa delia na 2 podskupiny. 
 
Najprv si vysvetlíme a poviem niečo o každom balíku a potom si poviem aj niečo k cenám. 
 
Balík večer a víkend 
rýchlosť do 1Mb/s, môžete sa pripájať len od 17:00 do 08:00 a cez víkendy nonstop . Ak sa pripojíte v iný čas ako tento, budete platiť za každé data zvlášť. Predplatené data sú 2 GB. 
 
Osobne tento balík odporúčam len ak máte stále rovnakú pracovnú dobu a domov sa dostanete až niekedy poobede a potrebujete dokončiť stále nejakú prácu doma, ktorú nasledovne odošlete niekam inam. Dát je málo, ale vy ich využijete taktiež málo keď sa budete iba v tom čase pripájať. 
 
Balík nonstop 
rýchlosť do 3,6 Mb/s, pripájate sa bez časového obmedzenia. Predplatené data sú 3,5 GB. Tento balík obsahuje aj 50 voľných sms, samozrejme iba do siete Orange. 
 
Tento balík využívam ja. Výhoda je pripojenie bez časového obmedzenia, ktorá je u mňa nutná a dát je tiež primerane dosť na klasické surfovanie, občasné prezretie nejakého videa, písania cez komunikátor a samozrejme ostanú aj nejaké data na stiahnutie maličkého programu. 
 
Balík biznis 
rýchlosť do 3,6 Mb/s, pripájate sa taktiež bez časového obmedzenia. Predplatené data sú 5,5 GB. Tento balík obsahuje taktiež voľné sms do sieti Orange, ale ich počet sa zdvojnásobil čiže dohromady 100 sms. Samozrejme čo by to bol Biznis balík bez roamingu. Môžete sa túlať svetom a budete stále on-line. Predplatených máte iba 10 MB. 
 
Tento balík je pomerne dosť vybavený dátami, rýchlosťou, sms a roamingom na internet. Odporúčam len keď potrebujete byť on-line v zahraničí.  
 
A teraz sa poďme pozrieť na ceny: 
 
Ceny sú rozdelené podľa toho na koľko sa upíšete pre 24 mesiacov to je: 
 
Večer a víkend: 
 
	 Aktivačný poplatok 1,20 SK (pre verných zákazníkov)  – 950,80 SK  
	 
 
 
	 Mesačný poplatok 653,30 SK 
	 
 
 
	 Voľný objem dát 2 GB 
	 
 
 
	 Voľné sms 0 
	 
 
Nonstop: 
 
	 Aktivačný poplatok 1,20 SK – 950 SK 
	 
 
 
	 Mesačný poplatok 950,80 SK 
	 
 
 
	 Voľný objem dát 3,5 GB 
	 
 
 
	 Voľné sms 50 
	 
 
 
Biznis: 
 
	 Aktivačný poplatok 1,20 SK – 950 SK 
	 
 
 
	 Mesačný poplatok 1783,80 SK 
	 
 
 
	 Voľný objem dát – 5,5 GB 
	 
 
 
	 Voľné sms 100 
	 
 
 
	 Roaming 10 MB 
	 
 
Ceny a data pre viazanosť na 36 mesiacov sú rovnaké ako pri 24 mesačnej viazanosti tak doplním iba mesačný poplatok: 
 
Večer a víkend – 593,80 SK 
Nonstop – 831,80 SK 
Biznis – 1545,80 SK 
 
Pripájate sa cez USB modem Huawei E220, dátovú kartu Option 3G/EDGE a dátovú kartu Huawei 3G Gold E630. 
 
Ak by som vám mohol odporučiť tak z T-Mobile to je Rýchly internet 10 a z Orange Balík Nonstop. 
Samozrejme  presnejšie informácie dostanete ak zavoláte operátorom, alebo sa preklikáte k nim na web. Ceny sa vám môžu automaticky zmeniť ak si zoberiete aj niektorý notebook, alebo PC ktoré vám ponúkajú obidvaja operátori. 
 
Pozor!!!! Pred tým ako budete uvažovať od koho si budete brať mobilný internet si najprv overte u operátora alebo na internete pokrytie vo vašej oblasti. 
A ešte jedno a   veľké pozor!!!!!!!!!!!! 
Prenesené data nie sú len tie ktoré si vy stiahnete z internetu. Každá a jedná stránka sa načítava do vášho PC. Už to sa berie ako stiahnuté data. Čiže už len keď surfujete, pozeráte video alebo počúvate internetové rádio  na internete tak sa vám odpočítavajú data. 

