
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Maximos Dragounis
                                        &gt;
                Nezaradené
                     
                 Kréta, krajina kultúry, poézie, slobody a pohostinnosti 

        
            
                                    23.3.2010
            o
            12:17
                        (upravené
                23.3.2010
                o
                22:55)
                        |
            Karma článku:
                3.35
            |
            Prečítané 
            1216-krát
                    
         
     
         
             

                 
                    Oten eskefthike o Theos tin ji na dimiurjisi, ipe i Kriti tha´ine epa! Ipe i Kriti tha´ine epa če leftheri tha zisi! To je jedna z tradičných krétskych ľudových mandinád, ktorá hovorí: Keď sa Boh rozhodol zem vytvoriť, povedal Kréta bude tu! Povedal Kréta bude tu a slobodne bude žit! Hlavné mesto ostrova je Iraklio (podľa hrdinu Iraklisa, slov. Herakles), iné mestá sú tu Rethymno, Chania či Lasithi. Na ostrove žije 600 000 obyvateľov, no v Aténach či v zahraničí nájdeme veľké množstvo ďalších Kréťanov. Najväčšia hora je tu Psiloritis (v staroveku známa ako Ida). Ak sa chcete dozvedieť viac o histórí a tardičnej ľudovej kultúre tohto nádherného ostrova prečítajte si nasledujúci článok, kde sa dozviete aj to čo je to mandinada a kto sú to vlastne Kréťania.
                 

                 História   Kréta je ostrov s veľmi bohatou históriou a kultúrou   Prví obyvatelia Kréty boli Minójci. Minojci na Krétu prišli pravdepodobne z neďalekej Malej Ázie (dnešné Turecko) a stalo sa tak niekedy po roku 3000 pred Kr. Minójci neboli však grécky kmeň. Rozprávali svojim jazykom, ktorý nám je neznámi. Vytvorili prvú veľkú európsku civilizáciu, mali vyspelú palácovú kultúru a obchodovali  s celým stredomorým.  Stopy po ich veľkej civilizácií najdeme dnes v paláci Knossos, kde môžeme vidieť, akí boli zruční umelci. Podľa mytológie v tomto paláci žil Minotauros, napol človek, napol býk, ktorého tu zabil Aténčan Thiseas za pomoci Kréťanky Ariadny. Ostaňme ešte pri mytológií, podľa nej najvyšší boh Zeus bol vychovaný práve na Kréte, kde sa schovával pred svojim otcom.   Minójska civilizácia sa zrútila niekedy v priebehu 15 stor. pred Kr. a dnes sa stále nevie prečo. Predpokladá sa, že Minojčanov zničila vlna tsunami, ktorá nastala po výbuchu sopky na ostrove Thira (dnes Santorini). Iná teória hovorí, že Minojčanov zničili Dóri. Je však isté, že v tomto storočí na Krétu prichádza grécky kmeň Achajcov a usadil sa tu. Achajci a Minojčania spolu pravdepodobne v dobrom vychádzali a Achajci boli prví Gréci na Kréte.   Neskôr však na Krétu prišiel iný grécky kmeň, Dóri. Dóri boli veľmi bojovní národ, poznali už železné zbrane. Na Krétu prišlo veľmi veľa Dórov, ktorí sa usadili na celom ostrove. Minojčanov si však nepodrobili tak, ako to spravili v Sparte s Achajcami, ale začali sa s nimi miešať. Minojci a Achajci sa s Dórmi pomiešali a Minojci teda prestali používať svoj jazyk, keďže Dóri mali početnú prevahu. Od tohto obdobia sa tak Kréta stáva ostrovom osídleným Grékmi, síce malé skupiny Minojčanov tu existovali až do klasickej doby.   Kréta nebola počas klasickej antiky významným gréckym štátom, no kultúra sa tu pestovala stále. Krétsky filozof Epimenides v 6 stor. pred Kr.  prišiel do Atén a povedal:  Oi Kretes en pseutoi (Kréťania sú klamári). Lenže potom sa Aténčan zamyslel. Ak sú Kréťania klamári, teda aj Epimenides klamal keď hovoril, že sú klamári a teda Kréťania stále hovoria pravdu. Lenže v takom prípade by neplatilo tvrdenie, že Kréťania sú klamári. A takto vznikol slávny filozofický Epimenidov paradox, ale vráťme sa k histórií. V roku 88 pred Kr. Mithridates, grécky kráľ z Pontu a nepriateľ Ríma dobyl Krétu, no nato sa na Kréte vylodili rímske légie a celú si ju podmanili. Od tejto doby tak Kréta patrí Rímu a jej hlavné mesto bola Gortyna. Postupne sa tu začína šíriť kresťanstvo, ktoré stále naberalo na sile až do 5 stor., kedy na Kréte zaniklo pohanské grécke náboženstvo. Od roku 395 patrí Kréta Východorímskej ríši (grécka Byzancia). V 6 stor. na Krétu prichádzali aj Slovania z Peloponézu, tí sa však na Kréte nikdy neusadili, ale prichádzali sem ako obchodníci predávať hlavne svoju keramiku. Aký však mali úspech to nevieme.   V 9 stor. však pre Kréťanov nastala ťažká doba. Ostrov si totiž podmanili rozpínajúci sa Arabi, ktorí úplne zničili mesto Gortyna a dokonca zabili tamojšieho biskupa Kyrilosa. Kréťania však s moslimskou vládou neusúhlasili a znovu sa ukázal ich bojovný dórsky duch. Proti Arabom rebelovali hlavne Sfakiančania, Kréťania z južnej Kréty, z kraja Sfakia ktorí sú priami a nepomiešaní potomkovia Dórov. V roku 960 Byzantský cisár Nikiforos Fokas za veľkej pomoci Sfakiančanov Arabov porazil a definitívne ich vyhnal z Kréty. Arabské obyvateľstvo Gréci vyhnali, no ostali tu aj moslimskí Kréťania, ktorých predkovia počas arabskej doby prestúpili na islam. Islamskí Gréci boli však znova pokresťančení.    V roku 1204 Byzanciu dobyli križiaci a tak si Krétu podmanili taliansky Benátčania. Benátska doba znamenala veľký rozkvet krétskej kultúry. V 15 stor. Grécko dobyli Turci, no na Kréte vládli Benátčania až do 18 stor. Vďaka tomu sa Kréta stala ostrovom slobody a mnoho gréckych vzdelancov sa sem pred Turkami uchýlilo. Kréta sa stala významným centrom gréckej kultúry. Počas tohto obdobia sa na Kréte usadili aj talianski kolonisti, ktorí si brali za ženy Kréťanky. Toto sa dialo hlavne vo vyšších vrstvách a Benátskej vláde začalo vadiť, že sa takýmto spôsobom vláda nad Krétou postupne dostáva do gréckych rúk a tak vydali zákaz sobášov medzi Talianmi a Grékmi. V tomto období tu nastal veľký rozvoj v literatúre. Kréťan Visentzos Kornaros napísal ľúbostný román Erotokritos. Dej tohto románu bol zasadený do starovekých Atén a patrí medzi najznámejšie európske literárne diela. Kornaros použil v tomto románe veršované ľúbostné motívy, inšpirované talianskou poéziou a tak vznikla aj mandináda, ľudové krétske porekadlá ku ktorým sa vrátim neskôr. V tejto dobe sa na Kréte narodil slávny európsky maliar Dominikos Theotokopulos, známy však hlavne pod španielskou prezývkou El Greco (Grék). Nie všetci Kréťania však súhlasili s benátskou vládou. Bojovní Sfakiančania spôsobili problémy aj teraz a Benátkam sa územie Sfakie nikdy nepodarilo účinne ovládať. Sfakiančania zničili aj rozostavanú benátsku pevnosť. Až neskôr sa ju Talianom podarilo dokončiť a jej názov je Frangokastro, teda katolícka pevnosť. Obdobie rozkvetu končí v roku 1718, kedy Krétu od Benátčanov dobyli Turci.   Turecká doba priniesla Kréťanom mnohé problémi. Ťažká moslimská nadvláda prinútila mnohých Kréťanov prestúpiť na islam a tak tu v 19 stor. tvorili moslimovia približne 30% celkového obyvateľstva. Krétski moslimovia si však uchovali svoj grécky jazyk a nikdy sa celkom nepoturčili. Postrachom pre Turkov sa na Kréte stali aj tentokrát Sfakiančania. Turkom sa Sfakiu nepodarilo dobyť a Sfakiančania neustále revoltovali a útočili na Turkov. V roku 1821 vypukla v Grécku vojna za nezávislosť od Turkov, ku ktorej sa okamžite pridali bojovní Kréťania. Veľkú krétsku revolúciu zničila až armáda privolaná z Egypta, ktorá sa potom vylodila aj na Peloponéze. V roku 1831 vznikol Grécky štát, no Kréta mu nepatrila. V nasledujúcej dobe vypukli obrovské povstania proti Turkom. Vtedy Kréťania zmasakrovali takmer polovicu moslimského obyvateľstva, mnohých vyhnali do Sýrie. Turci museli ustúpiť a Kréte uznali autonómiu a právo miestnej vlády. To však Kréťanom stále nevyhovovalo a znovu rebelovali. Turci nad ostrovom startili kontrolu. V roku 1913 sa gréckemu premiérovi Eleftheriovi Venizelosovi (rodák z Kréty) podarilo Krétu pripojiť ku Grécku. V roku 1923 sa konala Grécko-turecká výmena obyvateľov, počas ktorej Krétu opustili všetci moslimovia. Krétski moslimovia žijúci dnes v Turecku a Sýrií si dodnes uchovali grécky jazyk.     Kultúra   Rotisane tin leftheria, tin leftehria rotisane pias manas ine jenna, če ipe pos tin ejennise, če ipe pos tin ejennise to kritiko to ema! Spýtali sa slobody, slobody sa spýtali ktorá matka ju porodila a ona povedala a ona povedala že zrodila sa s krétskej krvy!    Toto je ďalšia mandináda, ktorá poukazuje na to, ako si Kréťania cenia slobodu. Sloboda (leftheria) je pre nich na prvom mieste, ostatne podobne je to aj s inými Grékmi. Kréťania si nesmierne ctia cudzincov. Ak cudzinec vstúpi do ich domu, je ich povinnosťou mu ponúknuť to najlepšie. Takto boli Gréci charakterizovaní už v staroveku a práve na Kréte sa tento zvyk dodržuje veľmi prísne. Pohostinnosť sa po grécky povie filoxenia čo je odvodenina slova filia-priateľstvo a xenos-cudzinec. Ak však Kréťanovi siahnete na to najcennejšie, na slobodu, bude za ňu bojovať aj na smrť.     Kréťania sú veľmi temperamentný národ, čo môžeme vidieť aj na ich zvykoch či hudbe. V minulsoti tu bola tradícia tzv. Vendetta, krvná pomsta, kedy medzi sebou bojovali miestne rodiny. Divoká je aj krétska hudba a krétske tance. Tradičný krétsky tanec je kruhový tanec Syrtos podľa ktorého bol vytvorený aj tanec Syrtaki pre film Grék Zorba, avšak Syrtaki nie je tradičný ľudový tanec. Známe mužské divoké tance sú Pentozalis a Siganos, ktoré sa tancujú veľmi rýchlo a obsahujú mnohé akrobatické prvky, podobné gréckemu pevninskému tancu Tsamiko. Tradičný je aj bojovný Povstalecký tanec (Andartikos choros). Pri tancoch sa často strieľa do vzduchu, lebo Kréťania sú známi aj tým že majú veľmi radi zbrane a strielanie. Obľubujú aj nože a tento zvyk bol charakteristický už pre Minójcov. Tradičná hudba je aj spomínaná mandináda, ktorej však budem venovať osobitnú kapitolu. Tradičným krétskym ľudovýmn nástrojom je lyra. Je to sláčikový nástroj, podobný husliam, na ktorý sa však hrá zvyslo a veľmi divoko. Lyra bola tradičným byzantským nástrojom a na Krétu sa dostala z Konštantinopolu. Popri lyre sa na Kréte hrá na brnkací nástroj udi a lauto. Krétska hudba je podobne, ako aj ostatná grécka hudba orientálna, no nájdeme tu silný taliansky vplyv, ktorý tu zanechali Benátčania. V Krétskej ľudovej hudbe sa spieva o romantike, miestnej prírode či o samotne Kréte. Aides amanmanama... to je tradičný krétsky pokrik na začiatku piesní. V mestských kluboch mesta Chania vznikol počas tureckej doby hudobný štýl tabachaniotika, je to orientálny štýl, ale nevyužíva sa tu tradičná lyra, ale brnkacie nástroje ako kanonaki, lauto, violi (husle) a udi.     Tradičný krétsky mužský kroj pozostával z nohavicovej suknice-vraka, alebo z nohavíc (pandelonia)  a košele. V prípade, že Kréťan smútil, nosil čiernu košelu (mauro pukamiso). Pojem čierna košeľa na Kréte znamená vyjadrenie smútku. Na hlave Kréťania často ešte aj dnes nosia priliehavú čiernu čiapku s nastrihanými koncami. Ženské kroje sú často modré alebo červené s trblietavými ozdobami.    Obyvatelia Kréty sa živili chovom oviec a kvôz a práve tu vznikol tradičný grécky syr feta, ktorý má ešte byzantský pôvod. Pestujú sa tu olivy a tiež vinič. Krétske víno patrí medzi najlepšie vína z Grécka. Vyrába sa tu aj tradičná viničová pálenka rakia, v krétskej gréčtine známa ako rači. Typickým produktom je veľmi kvalitný olivový olej, ktorý patrí medzi najkvalitnejšie olivové oleje na svete. Hovorí sa, že vďaka zdravej strave a vysokej konzumácií olivového olivového oleja Kréťania až do 20 stor. nevedeli, čo je to infarkt.   Na Kréte sa rozpráva krétskou gréčtinou a linguisti sa nevedia zhodnúť, či ide iba o dialekt štandardnej gréčtiny, alebo sa jedná o samostatný grécky jazyk, ako je to v prípade pontčiny, kappadóčtiny, griko, tsakončiny a cyperštiny. Krétska gréčtina je mäkšia, ako štandardná gréčtine a obsahuje aj mäkčene, na rozdiel od štandardnej gréčtiny. Je podobná s cyperštinou. V tomto jazyku sa nachádza aj veľa talianskych slov. Cyperčania a Kréťania sú si veľmi blízki, majú podobnú hudbu a mnohé podobné zvyky. Z cudzincov si Kréťania veľmi rozumejú so Sicílčanmi a Calabrijčanmi. Kréťania si už od staroveku veľmi ctia svoju najvyššiu horu Psiloritis.   Povedzme si aj niečo o pôvode dnešných Kréťanov. Tí su potomkovia zmiešaného obyvateľstva starovekých Minójcov, Dórov a Achájcov. S Kréťanmi splynuli aj talianski-benátski kolonisti. Bohatú kultúru zanechali v dnešných Kréťanoch vypelí Minójci, zatiaľ čo svoju bojovnú a divokú povahu zdedili po gréckych Dóroch. V južnej Kréte, v Sfakií žijú Sfakiančania, ktorí sú nemiešní potomkovia Dórov, keďže kraj Sfakia je hornatý a pred príchodom Dórov nebol Minojčanmi osídlený. Sfakiančania žili separovane až do 20 stor.   Z Kréty pochádzal známy hudobný skladateľ 20 stor. a autor piesne Zorba Mikis Theodorakis, spisovateľ Nikos Kazantzakis (napísal knihu Zorba), premiér Eleftherios Venizelos, maliar El Greco, spisovateľ Visentzos Kornaros, staroveký filozof Epimenides, bájny kráľ Minos, moderná speváčka Nana Muskuri a slávny súčastný skladateľ krétskej hudby Nikos Xiluris. Grék Zorbas skutočne existoval, no nemýlte si ho s Kréťanom, v skutočnosti tento slávny dobrodruh pochádzal z gréckej Makedonie. Svoj príbeh vyrozprával Kazantzakisovi, ktorý ho následne spísal.   Mandináda   Chérome pu me Kritikos če opu pao to leo, me mandinades cherome, me mandinades kleo. Táto mandináda hovorí: Radujem sa, že som Kréťan a kam idem to hovorím, s mandinádami sa radujem, s mandinádami plačem.    Mandinády sú citovým prejavom Kréťana. Kréťan ich spieva keď je šťastný, aj keď sa raduje. Mandináda je veršovaná pieseň, často sú to ľudové porekadlá alebo ironické komentovanie lásky, či rozchodu. Vznikla v benátskom období spojením talianskych romantických piesní s tradičnou krétskou orientálnou hudbou. Svoj pôvod má v románe Erotokritos, kde mala romantický podtón, no neskôr sa z mandinády stala vtipná krátka a často ironická zhudobnená poznámka. Spieva sa tak, že najprv sa vysloví samotná mandináda a následne sa zahrá na lyru. Mandinády vznikajú aj dnes a moderné mandinády sú veľmi vtipne ako napríklad:  PC me modem evala epano sto mitato, ja na pulo sto internet to gala ton provaton. PC s modemom umiestnil som si hore do domčeka, aby som si cez inernet kúpil ovčie mlieko.  Veľmi vtipné sú tradičné ironické mantinády, ktoré hovoria o láske, smrti ženách alebo rozchode:   *O chorismos če o thanatos echun tin idia axia, mono pu is ton chorismo den jinete kidia (Rozchod a smrť sú si podobné, až nato, že  pri rozchode sa nekoná pohreb).   *Rači če krasi den epina protu na se gnorizo, tora pino če ja tus dyo ja na se lismoniso (Rakiju a víno nepil som predtým, ako som ťa spoznal, teraz pijem za dvoch, aby som na teba zabudol).   *Tin ora pu chorisame, tin ora pu chorisame stin kardia mu ixa pono, če auti tin ekane jiorti če tin glendise mono (Keď sme sa rozišli, keď sme sa rozišli v srdci som mal bolesť a ona z toho spravila sviatok a iba sa z toho radovala).   *Polla ine ta´steria tu´ranu, ma to fengari ena, če sto myalo mu echo polles, ma stin kardia mu esena! (Veľa je hviezd na neby, mesiac však len jeden, aj v hlave mám mnoho-žien-v srdci iba teba!).   *Thelo ton tafo mu ftocho, apo kato če apo pano jiati akoma če nekros to pluti ego den kano (Chcem chudobný hrob oddola až hore, lebo ako mŕtvy nezbohatnem).   Nie všetky mantinády sú však ironické:   *Olu tu kosmu ta nisia pu´ne ston planiti, na ta enosis, den boris na kanis mia Kriti! (Všetky ostrovy sveta čo sú na tejto planéte ak aj ich spojíš nedostaneš ostrov podobný Kréte!).   *Aftos edo pu se fila, čera mu, aftos edo pu se filai mes´to stoma xeri kala pos i agapi su ime ego akoma! (Ten čo ťa bozkáva, drahá moja, ten čo ťa bozkáva na ústa vie veľmi dobre že tvoja láska som ešte ja!).   *I Krites, aide i Krites ine enas laos andardikos če s´olon ton kosmo ine gnosti, echun krasi, ladi če rači če jiafto zune mia zoi sosti! (Kréťania, aide Kréťania sú národ bojovný a v celom svete sú známi, majú víno, olej a rakiju a preti žijú jeden život správny!).      Toľko ku nádhernej Kréte a jej ľuďom, dúfam, že ju navštívite, aby ste spoznali túto nádhernú kultúru aj vy  apamätajte, Kréta, to nie je iba turistické letovisko plné hotelov, ale ostrov so skrytými krásami a vždy milými ľuďmi.                           

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok Gréka XI-Grécke voľby-bezvládie, pat a čoskoro ďalšie voľby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Rembetiko, hudobný štýl gréckeho podsvetia a gréckych gangstrov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného Gréka X: Prešiel úsporný program, rozpad parlamentu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného GrékaIX-Nová pôžička, rozpad koalície a grécke voľby
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maximos Dragounis 
                                        
                                            Článok jedného Gréka VIII.-Historická genéza gréckej krízy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Maximos Dragounis
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Maximos Dragounis
            
         
        dragounis.blog.sme.sk (rss)
         
                                     
     
        Volám sa Maximos, pochádzam z Grécka. 
Facebook:
http://www.facebook.com/pages/Maximos-Dragounis-Gr%C3%A9cko-bez-propagandy-%CE%97-%CE%95%CE%BB%CE%BB%CE%AC%CE%B4%CE%B1-%CF%87%CF%89%CF%81%CE%AF%CF%82-%CF%80%CF%81%CE%BF%CF%80%CE%B1%CE%B3%CE%AC%CE%BD%CE%B4%CE%B1/103882279704191?sk=wall
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1935
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




