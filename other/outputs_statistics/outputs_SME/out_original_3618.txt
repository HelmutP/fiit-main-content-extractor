
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Radovan Potočár
                                        &gt;
                Vlažne vážne
                     
                 Veľavážený pán minister! 

        
            
                                    30.3.2010
            o
            9:36
                        (upravené
                21.6.2010
                o
                20:22)
                        |
            Karma článku:
                8.35
            |
            Prečítané 
            1197-krát
                    
         
     
         
             

                 
                    My, národne najuvedomelejší občania Slovenska, prihovárame sa Vám týmto listom vo veci boja za ochranu nášho jazyka, národnej hrdosti a vlastenectva.
                 

                      Vážený pán minister,    pri dohliadaní na dodržiavanie jazykového zákona a iných noriem, ktoré sú pre zdravý chod našej spoločnosti absolútne nevyhnutné, treba byť veľmi dôsledný. Zákerný sprisahanecký útok môže prísť skadekoľvek. Zločiny, ktoré sa nedávno potvrdili v relácii Lampa Štefan Hríba, boli už poslednou kvapkou. Pre ochranu národa musíme robiť odteraz ešte viac.    Začať treba rozhodne od médií, lebo tie si servítku pred ústa vôbec nekladú. Za najhoršiu pokladáme situáciu práve v tých protištátnych, popretkávaných rôznymi hyenami a prostitútkami. Pre dosiahnutie ideálneho stavu preto treba začať jedine od nich. Navrhujeme, aby sme všetky súkromné média, ktoré aj tak vyvíjajú len samú protištátnu činnosť, začlenili pod ochranné krídla verejnoprávnych médií. Nie, pán minister, my ich nechceme zrušiť, chceme len usmerniť ich vývoj tak, aby boli prospešné pre chod našej spoločnosti.    Uvedené opatrenie sa musí pre dokonalý účinok týkať všetkých médií. Práca televízii by mohla byť pod odborným dohľadom Slovenskej televízie, ktorá by im svojou pronárodnou činnosťou mohla ísť príkladom. Rádia by si zas pod svoje krídla mohol vziať Slovenský rozhlas a tým by sme dosiahli, že tieto médiá by konečne prestali viesť nezmyselné a vykonštruované útoky na Vládu SR, ktorá by sa potom mohla plne venovať svojej práci.    Horšie to však bude s revitalizáciu pomerov v tlačených médiách. Tu navrhujeme všetku tlač zlúčiť do väčších celkov, ktoré by mohli byť priamo pod drobnohľadom ministerstva a predišlo by sa tým porušovaniu jazykového zákona a národných záujmov aj vo všetkých periodikách. Najproblematickejšie však bude kontrolovanie, nazvime to - znovuzrodenie, internetu, lebo na ňom sa to len tak hemží článkami bez hlavy a päty. S internetom treba skrátka raz a navždy urobiť poriadok, nebude si predsa hocijaký pisálek vypisovať, čo sa mu zachce. V prípade internetu síce budeme musieť vynaložiť trochu viac administratívneho úsilia, ale i tak veríme, že svoje ovocia to prinesie. Navrhujeme vytvoriť špeciálny orgán, napr. Národný úrad pre ochranu užívateľov internetu, ktorý by usmerňoval jeho chod a prečisťoval ho od škodlivých, protinárodných, protištátnych, doslova hyenistických útokov.    Sme však presvedčení, že v boji za národné uvedomenie, treba zájsť ešte ďalej. Národné záujmy, ochrana jazyka a vlastenectva by si podľa nás zaslúžili jeden ucelený komplexný zákon, ktorý svoje ochranné krídla rozprestieral nad všetkými zákonnými normami i nad Ústavou, ktorá bola aj tak postavená proti ľuďom a dnes nám bráni napríklad v rýchlejšom rozvoji národnej infraštruktúry svojimi nezmyselnými táraninami o právach na majetok a janeviemčo.    Takýto „národný superzákon" by mohol začleňovať celú agendu za uvedomelosť a lásku k národu. Mohol by napríklad pomôcť zjednotiť celé politické spektrum do jednej veľkej politickej strany, národnej aliancie. Tým by sme dosiahli, že všelijakí táraji by prestali brblať do činnosti vlády a konečne by sedeli tam, kde mali sedieť už dávno. Vláda by sa mohla začať naplno venovať len a len svojej pronárodnej práci a nestrácali by svoj drahocenný čas bezobsažným táraním s ľuďmi, ktorí riadeniu štátu aj tak nerozumejú. Sme presvedčení, že tento zákon by nás posunul o kusisko bližšie k tým svetlím zajtrajškom, kde nebudú modrí, zelení, červení, ale len jedna politická liga, ktorá kope za národ.    Okrem toho by však umožnil takýto zákon vstúpiť s ochranou národných záujmov ešte bližšie k občanovi - priamo do našich rodín. Existuje racionálny dôvod, aby sa hymna spievala len v školách či úradoch? Dajme zákonný základ na to, aby si ju mohol zaspievať každý! Vytýčme na to určitý čas, napríklad o siedmej hodine večer, kedy sa rodina stretne a spolu bude môcť osláviť nielen pronárodnú prácu našej Vlády, ale v nariadenom čase by občania oslávili i svoju úprimnú lásku k národu a vlasti. Uvedené opatrenie by však muselo byť pod prísnou kontroľou určených orgánov, nakoľko sa zaiste nájde mnoho živlov, ktoré si odmietnu ctiť svoju štátnosť.    Jeden spev hymny za deň však pre naše rodiny nemôže priniesť dostatok citu. Aby sme vlastenectvo priblížili ku každému človeku, treba už konečne urobiť poriadok i s menami našich občanov. Je neprípustné, aby slovenské mamičky nazývali svoje deti tak, ako sa im zachce. Mená ako Kevin, Jessica či Christopher výrazným spôsob podkopávajú základy našej národnej hrdosti. Preto prosíme Vás, vážený pán minister, aby ste sa zasadili za to, nech národný superzákon rieši i túto problematiku. Slovenským deťom slovenské mená! - to nech je našim mottom v boji za tento zákon. Nemali by sme však zabúdať ani na potrebu ujednotiť pomenovávanie našich občanov. Navrhujeme zaviesť povinné druhé meno rýdzo slovenského charakteru pre každého občana, ktoré by podčiarkovalo národnú uvedomelosť a silný cit vlastenectva. Pre tento cieľ doporučujeme prioritne využívať mená ako Robert, Marek, prípadne Pavol. Napríklad pán prezident by pri podpisovaní tohto veľkého národného zákona mohol i slávnostne prijať meno Ivan Robert Gašparovič.    Vážený pán minister, pevne veríme, že svojimi návrhmi sme aspoň trochu obohatili vašu proslovenskú fantáziu a posilnili Vaše úsilie v boji za národ. I keď je možné, že viaceré navrhnuté právne úpravy máte už detailne rozpracované, chceme, aby ste vedeli, že stojím pevne za Vami. Náš národ Vás potrebuje, nepoľavte!        S pozdravom    Národne uvedomelí občania     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Yankees go home! (desať minút s národniarom)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Bod nehybnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            O dvanástich kartičkách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Darjeeling - Goa - Bombaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Radovan Potočár 
                                        
                                            Vyzlečte sa, zvyknite si. Couchsurfing
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Radovan Potočár
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Radovan Potočár
            
         
        potocar.blog.sme.sk (rss)
         
                        VIP
                             
     
         Boli Sme. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    96
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3328
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vážne veci
                        
                     
                                     
                        
                            Vlažne vážne
                        
                     
                                     
                        
                            Trochu scesty
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Školy
                        
                     
                                     
                        
                            Esej?
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Yankees go home! (desať minút s národniarom)
                     
                                                         
                       Prechádzky čínskymi rozprávkami
                     
                                                         
                       Ako Fico priviedol Slovensko k bankrotu
                     
                                                         
                       Ľudia, odpojte sa!
                     
                                                         
                       Byt, kola, pivnica, vínko, Boh a Výboh ....
                     
                                                         
                       Prvýkrát
                     
                                                         
                       Cukornička
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Trnka vracia úder ....lebo iba " trtoší"  ?
                     
                                                         
                       David Luiz do PSG? Zbytočne vyhodené peniaze
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




