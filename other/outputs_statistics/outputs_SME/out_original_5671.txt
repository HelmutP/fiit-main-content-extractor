
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Brejčák
                                        &gt;
                ostatné
                     
                 Holandsko 

        
            
                                    2.5.2010
            o
            17:00
                        (upravené
                7.2.2011
                o
                22:16)
                        |
            Karma článku:
                4.98
            |
            Prečítané 
            644-krát
                    
         
     
         
             

                 
                    Úžasná krajina, ležiaca asi 1200 kilometrov od naších Tatier, ktorá úprimne miluje jej veličenstvo Kráľovnú Beatrix. Čo v sebe ešte ukrýva?
                 

                 Šestnásť miliónov obyvateľov, tlačiacich sa na ploche menšej ako my, zjavne nemalo problém so žiadnou národnosťou. Dokázali sme sa milo porozprávať s ľuďmi, ktorí si k nám prisadli na vedľajšiu lavičku v parku, ktorým sme sedením na brehu kanála zablokovali prístup k lodi, či s Poliakmi v autobuse alebo s náhodne stretnutými Slovákmi na ulici.   Tolerancia ľudí je ohromujúca. Ak by som sa prechádzal po ulici v uteráku, jedinými zazerajúcimi by boli turisti. Domáci sa o vás nestarajú, nech robíte čokoľvek, no keď treba pomôžu. Podľa slov miestnych väčšine tu nevadia ani imigranti, možno v oblastiach veľkých miest ako Amsterdam alebo Rotterdam.   Ubíjajúca vec na Holandsku je rovina. Žiadne hory, najväčšie nám prišli hrádze a nájazdy na diaľnicu. Najvyšší vrch, Vaalserberg, je vysoký iba 322 metrov. To je jeden z dôvodom, kvôli ktorým sa veľmi ťažku orientuje. Ďalším sú domčeky, ktoré všetky vyzerajú rovnako, aj keď je každý iný.   Pivo majú radi. Celosvetovo známa značka je Heineken. Oproti Slovensku je o čosi drahšie, za dve deci zaplatia najmenej dve eurá, pričom pivo, víno i cigarety si môžu kupovať od šestnástich. Nám, Slovákom, prišlo divné aj čapovanie, pretekanie peny neriešia, celý pohár dostanete mokrý.   Amsterdam   Osobitná kapitola Holandska. Počtom obyvateľov nie väčšie mesto ako Praha, no aj tak mi pripadá čistejšie, krajšie, zaujímavejšie. Možno je to počasím, ktoré nám vyšlo, možno usporiadanosťou, možno poriadkom, možno peknými vysokými, úzkymi a krivými domami, aké inde nenájdete, možno všetkým dohromady.   Veľmi podstatnou vecou, ktorá určuje charakter každého mesta na Zemi, sú ľudia. Ako som už písal, sú úžasne tolerantní, milí, nestarajú sa o vás. Človek sa necíti iný, pretože nikto nezazerá. Na Slovensku to robia takmer všetci.   Mesto však ukazuje veľkú časť jeho atmosféry až večer či v noci, no bohužiaľ, to nám nebolo umožnené.   Páči sa mi spôsob, akým sa prepravujú. Polovica ľudí jazdí na bicykloch, ktorých je tu viac ako ľudí na Slovensku, na jedného Holanďana pripadá 6 bicyklov.   Nie nadarmo sa Amsterdam inak nazýva aj Benátky západu. Kanále sú rôzne poprepletané pomedzi ulice, kde sa dá sú hausbóty. Škoda, že sme nenašli loďky na prenájom.   Na red light district to žije aj cez deň. Bolo zaujímavé vidieť zákazníkov v oblekoch vchádzať do výkladov a ako sa za nimi zaťahujú závesy. Rada pre vás, najlepšie ženy sú v bočných uličkách, nie na hlavných. Nebojte sa, ja som ich služby nevyužil.   Voda   Nie len v Amsterdame, ale takmer všade vo vnútrozemí nájdete vodné kanáliky. Holanďania žijú s vodou. Možno to vyznie divne, keďže stále vysušujú svoje územia. Postavia hrádze a čakajú aj desiatky rokov, kým môžu na novej zemi založiť sídla a začať obrábať pôdu.   Je to fascinujúce, povozili sme sa po diaľnici, na ktorej mieste bolo pred tridsiatimi rokmi more. Niet divu, že tretina územia sa nachádza pod hladinou mora.   30. apríl   Štátny sviatok, Deň kráľovnej. Všade vidíme holandské a oranžové vlajky. Každý by mal mať na sebe aspoň jednu časť odevu oranžovú, veď je to ich národná farba. Na uliciach sú tisíce ľudí, deti majú na dekách porozkladaný tovar. Väčšinou knihy, hračky, komixy. Pomedzi davy sa predierajú pochodujúce kapely, na námestiach sú koncerty.   Hromadne oslavujú narodenie predchádzajúcej kráľovny, pretože Beatrix sa narodila v januári a to nie sú dni vhodné na oslavy na uliciach. To som si nevymyslel, povedali nám to domáci.   Je na nich vidieť, že ich život kráľovskej rodiny zaujíma. Aj keď z nej mladí nie sú veľmi unesení, svojích vládcov majú radi. Počas tohto sviatku si kráľovná vyberie dve mestá v jednej či dvoch provinciách, kde pre túto udalosť pripravia špeciálny program.   Egypt, Rím, Praha   Cestujem rád. Osemnásťhodinová cesta autobusom nebola veľmi príjemná, no uvedomil som si jednu vec. Je jedno, kam idete. Či už je to Egypt, Rím alebo Praha, záleží na ľuďoch, s ktorými idete. Ak idete s kamarátmi alebo s inými ľuďmi, s ktorými si dobre rozumiete, dobre vám bude všade. Aj doma... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Prečo je zdanenie bohatých hlúposť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Fico v ženskom šate?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Rímsky Panteón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Veda vs diaľnice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Aká má byť STV?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Brejčák
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Brejčák
            
         
        brejcak.blog.sme.sk (rss)
         
                                     
     
        Ocením konštruktívnu kritiku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1017
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            názory
                        
                     
                                     
                        
                            fotky
                        
                     
                                     
                        
                            ostatné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            brejky.deviantart.com
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




