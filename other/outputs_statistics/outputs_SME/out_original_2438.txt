
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Fořt
                                        &gt;
                Nezaradené
                     
                 Thorshavn: Články mojej viery. 

        
            
                                    3.3.2010
            o
            17:54
                        (upravené
                3.3.2010
                o
                18:18)
                        |
            Karma článku:
                10.33
            |
            Prečítané 
            2986-krát
                    
         
     
         
             

                 
                    Dnes otváram svoj blogerský profil a pridávam sa k vám – blogerskej komunite. Niektorí z vás ma už poznáte z diskusii na sme.sk pod nickom Thorshavn. Môj prvý blog venujem vyznaniu svojej viery. Vybral som úryvok najdôležitejších článkov mojej osobnej ústavy, ktorá uzrela svetlo sveta pred viac ako 4 rokmi. Takže toto som ja:
                 

                 Preambula.    S úctou pred nepoznaným, nepochopeným a odlišným, s odvahou odkrývať svoje možnosti a rozvíjať svoje danosti,  s ambíciou napĺňať zmysel života, v duchu zásad liberalizmu a slobody, hľadajúc skutočné hodnoty a hodnotné ciele,  v dobrej viere a z vlastnej vôle uznášam sa na tejto osobnej ústave.    Hlava I.  Základné ustanovenia.    Článok 1   Najvyššou hodnotou, ktorú som povinný aktívne chrániť a na základe ktorej konám a z ktorej vychádzam pri rozhodovaní je sloboda; osobitne sloboda jednotlivca.    Článok 2   Ideálom mojej cesty životom je harmónia alebo napĺňanie harmónie. Obsahom harmónie je súbežné dosiahnutie stability, bezpečnosti, prosperity a zmyslu života.   ...   Hlava II. Sloboda.    Článok 6   Cestou k slobode je získanie kontroly nad svojim telom a vôľou. Človek, ktorý neovláda svoje telo alebo vôľu nie je slobodný.    Článok 7   Slobodný človek má v rukách všetky prostriedky, aby naplnil zmysel svojho života a je si tejto skutočnosti vedomý, verí jej a na základe toho aj koná.    Článok 8   Neoddeliteľnou zložkou slobody je zodpovednosť za svoje konanie a nekonanie. Zodpovednosť sa prejavuje najmä  schopnosťou plniť svoje sľuby a záväzky a v prípade ich neplnenia niesť následky.    Článok 9   Vonkajším prejavom slobody je rešpekt k názorom a konaniu iných, pokiaľ tieto nepredstavujú ohrozenie vlastnej slobody alebo slobody iného.    Článok 10   Sloboda sa prejavuje v rozhodnutiach. Ten, kto nerozhoduje alebo sa rozhoduje nedobrovoľne alebo sa nedobrovoľne podriaďuje rozhodnutiam iných, alebo pri rozhodovaní nemá možnosť výberu nekoná slobodne.    Článok 11   Nesiem svoj podiel zodpovednosti za celospoločenské presadzovanie slobody a odstraňovanie neslobody.    Článok 12   Nevyhnutnou súčasťou obsahu slobody je ochrana života, zdravia a nedotknuteľnosti vlastníctva.    Článok 13   Pracovnou činnosťou, ktorá neodporuje zásadám slobody je súkromné podnikanie, slobodné povolanie alebo verejná funkcia získaná voľbou. Zamestnanecký pomer možno prijať len dočasne a len vtedy, ak je to nevyhnutné z titulu získavania kvalifikácie, alebo z existenčných dôvodov prípadne iných vážnych dôvodov. Zamestnanecký pomer so štátom, a to aj dočasný, sa až na osobitné prípady vylučuje.    Hlava III. Harmónia. ...   Časť 3d  Zmysel života.    Článok 24   Kľúčom k žitiu zmyslu života je objavenie rozmernosti lásky a splynutie s ňou. ... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (157)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Preniesť sa ponad seba a zažiť kus večnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Aky je odkaz bilbordu Aliancie za rodinu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Couchsurfing, moja vlastná 6 ročná skúsenosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Drobné prekvapenia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Fořt 
                                        
                                            Kabaret: milujem cesty vlakom 2. triedy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Fořt
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Fořt
            
         
        fort.blog.sme.sk (rss)
         
                                     
     
        Mám rád slobodu, je to moja životná filozofia a môj životný kompas. Som pozorovateľ sveta. Fascinujú ma neviditeľné veci, ktoré hýbu dušou človeka. Zaujímam sa o happyológiu a dlhovekosť. Som couchsurfer. Obohacujú ma stretnutia. 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2977
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Karol Kaliský
                                     
                                                                             
                                            Samuel Genzor
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       6 tipov pri výbere osobného trénera
                     
                                                         
                       Gladiátor HD tréning
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




