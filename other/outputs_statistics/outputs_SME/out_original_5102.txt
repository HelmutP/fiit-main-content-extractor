
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Kalmár
                                        &gt;
                O pive
                     
                 Aké pivo pijete? 

        
            
                                    23.4.2010
            o
            10:05
                        (upravené
                23.4.2010
                o
                10:26)
                        |
            Karma článku:
                10.54
            |
            Prečítané 
            2669-krát
                    
         
     
         
             

                 
                    Isť s kamošmi na pivo, to sa aspoň raz za čas patrí, nie? Rovnako ako pri sledovaní futbalu, alebo v lete, v teple, na terase... Kto si nedá? Ale zamysleli ste  sa niekedy nad tým, že aké pivo vám chutí? Hovoríte, že sú vlastne všetky skoro rovnaké? Tak do veľkej miery máte pravdu.
                 

                 
Ilustračné fotoautor
      Prečo chutí väčšina pív u nás skoro rovnako?   V súčasnosti sme dospeli do štádia, keď názov značky piva nemusí mať nič spoločné s miestom, kde sa skutočne varí. Ak si kúpite u nás Zlatého bažanta, Martinera, Gemer, alebo dokonca Starobrno, tak s vysokou pravdepodobnosťou boli tieto pivá vyrobené na jednom a tom istom mieste a to v Hurbanove. Aby som však nebol len jednostranný to isté platí už aj o Topvare, Šariši alebo Kozlovi. Ďalšou kategóriou sú značky ako Stein alebo Tatran. Tieto značky ostali zachované aj napriek tomu, že v ich pôvodných závodoch bola výroba ukončená. Samozrejme ich súčasní výrobcovia tvrdia, že pokračujú v ich výrobe podľa pôvodnej receptúry.   Pivo, ležiak plzenského typu, ktoré sa u nás teší najväčšej obľube sa vyrába zo štyroch základných surovín: vody, jačmenného sladu, chmeľu a kvasiniek. Kvalita každej suroviny vplýva výrazne na výsledné vlastnosti piva. Okrem surovín aj samotný proces varenia, kvasenia a ležania je veľmi dôležitý. Už pri týchto základných surovinách existuje však veľa spôsobov ako pre výrobcu ušetriť, bohužiaľ zväčša to znamená aj pokúsiť sa to pivo oklamať. Oklamať hlavne o to, čo by v ňom malo byť, alebo o to čo, tam nemá čo hľadať.   Ako sa čaruje s pivom   Veľmi populárna je v súčasnosti tzv. HGB (high gravity brewing) metóda výroby piva. Pri nej sa uvarí povedzme že 16 stupňové pivo a to sa následne rozriedi podľa potreby na 10 alebo 12 stupňové pivo. Riediť pivo? Za také niečo v stredoveku v Čechách vraj popravovali sládkov utopením. Dnes je to ale bežný postup. Samozrejme že takto vyrobené pivo samo o sebe po zriedení nemá požadované vlastnosti. Ale aj to sa dá vyriešiť.   Na trhu je k dispozícii niekoľko prípravkov, ktoré dokážu skoro zázraky. V Čechách sa pred nedávnom rozvinula diskusia o používaní jedného z nich, ktorý je známy pod názvom Tetrahop. Ide vlastne o chemický extrakt pôvodom z chmeľu. Na rozdiel od klasického hlávkového chmeľu, chmeľových granúl, resp. klasických chmeľových extraktov sa Tetrahop pridáva až na samom konci výroby piva. Ide preto vlastne o aditívum, ale pivovari ho schovávajú veľmi radi pod pojem chmeľové produkty. Už v malých množstvách zlepšuje penivosť piva. Pena je bohatšia a na pohári ostávajú krúžky.  Taktiež vylepšuje chuť piva. Pivo je po jeho použití aj po prípadnom nariedení horkejšie, chmelovejšie. Čo na tom, že do piva sa takto dostávajú chemické látky, ktoré tam nemajú čo hľadať...   Okrem surovín, z ktorých je pivo vyrobené, je dôležitý aj technologický postup, aký sa použije. Spodne kvasené pivá, medzi ktoré patria ležiaky plzenského typu, je nutné počas hlavného kvasenia skladovať pri teplotách okolo 6°C a to 6 - 14 dní. Následne pivo by malo dozrievať (ležať) ešte aspoň ďalšie 3 týždne pri ešte nižších teplotách. Predstavte si, že jeden veľký pivovar sa rozhodol šetriť. Chladenie je veľmi drahé a preto po väčšinu obdobia dozrievania skladujú svoje pivo pri teplotách aj o 10 stupňov vyšších. Že to nespoznáte? A prečo Vás potom, už po vypití dvoch - troch pollitrakoch z ich produkcie, na druhý deň boli hlava?   Aké pivo máme potom piť?   Dobrou správou je, že na trhu sa dajú nájsť stále chutné pivá, ktoré sú vyrábané poctivo, len treba hľadať. Ide zväčša o malé, alebo stredne veľké pivovary, ktoré si ctia tradície a záleží im na kvalite. Problém je, že na Slovensku ich narozdiel od Čiech je veľmi málo. Pokúsil som sa preto zistiť, kde v Bratislave všade čapujú pivo z produkcie takýchto pivovarov, či už slovenských alebo českých. Za týmto účelom som využil služby stránky www.kamnapivo.sk. Počet takýchto krčiem, reštaurácii, pubov ma však nepríjemne zaskočil. Tvorí len sotva 9%.  Milí priatelia preto odporúčam hľadať a hľadať a následne samozrejme degustovať. Takže na zdravie!   Poznámka:   Pri tvorbe tohto článku som požil niekoľko nepasterizovaných a nefiltrovaných pív  pôvodom z Čiech. Snáď mi to bude odpustené.                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (80)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Je nízky záujem o regionálne voľby opodstatnený?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Malý prieskum ponuky piva na bratislavskej hrádzi, časť prvá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Ako sa dá hovoriť o výsledkoch volieb, keď o nich nemôžete hovoriť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Kalmár 
                                        
                                            Prečo dopravný podnik tají informácie o novej linke  do Rajky?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Kalmár
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Kalmár
            
         
        kalmar.blog.sme.sk (rss)
         
                                     
     
        Mne nie je jedno kto a ako v Bratislavskej VÚC minie ročne 122 miliónov z našich daní! A Vám??? Poďme to spolu zmeniť 9.11.2013. Po viac ako 10 ročnej skúsenosti ako poslanec v Miestnom zastupiteľstve MČ Bratislava Rusovce som sa rozhodol kandidovať za poslanca Bratislavskej VÚC. VÚC treba viac priblížiť k ľuďom a riešiť v nej naše reálne každodenné problémy. Práve ako poslanec miestneho zastupiteľstva mám s týmto skúsenosti. Petržalka, Jarovce, Rusovce, Čunova - Dovoľujem si Vás požiadať o Vašu podporu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1651
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Komunálne veci
                        
                     
                                     
                        
                            O pive
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       návraty pod poľanu
                     
                                                         
                       Je nízky záujem o regionálne voľby opodstatnený?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




