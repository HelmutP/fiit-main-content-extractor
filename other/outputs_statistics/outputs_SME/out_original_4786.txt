
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kuric
                                        &gt;
                Názory
                     
                 Kotrmelce, jaskyňa a vreckový nožíček 

        
            
                                    18.4.2010
            o
            16:05
                        |
            Karma článku:
                6.41
            |
            Prečítané 
            880-krát
                    
         
     
         
             

                 
                    Včera ráno som dostal inšpiráciu na napísanie článku o miléniovom stĺpe, ktorého torzo je na vrchu Zobor týčiacom sa nad Nitrou. Aby som bol presný a lokalizoval dané miesto presne, jedná sa o akýsi predvrchol vrchu Zobor, ktorý Nitrania, ale aj oficiálna turistická značka nazýva Pyramída. Po napísaní blogu som dostal chuť toto miesto znova naštíviť, využiť počasie a trocha vyskúšať počas zimy spiacu kondíciu. Nabláznil som celú rodinu, teda hlavne moje dve dcérky a s bojovým pokrikom tej mladšej, „Hulá ideme na Zobol", sme sa dali do malých príprav. Nič mimoriadne, len pár rožkov so syrom a so zeleninou, fľaša vody, pár jabĺčok, fotoaparát a vreckový nožík, čo sa niekedy volal rybička (však som nejaký chlap).
                 

                 Vychádzku sme začali pri tzv. Liečebnom ústave. Už tu sa mi môj nožík začal neplánovane vo vrecku otvárať, keď sa do prekrásnej prírody a prebúdzajúceho sa lesa vydávala aj skupinka možno 13 ročných tínedžerov. Ešte pred vstupom do lesa si zapálili cigaretky, ktoré vyzerali naozaj smiešne v tých ich ešte detských ručičkách a ešte komickejšie v ich pusách. Pohlavne heterogénna partička sa s krikom, rykotom a hulákaním vydala do lesa, plánujúc zakotviť niekde veľmi blízko, pretože nevládali niesť plecniaky pravdepodobne plné lacného vínka (a možno by som bol prekvapený). Ja a moja malá rodinná turistická skupinka sme radšej zastali, aby sme nemuseli počúvať tie strašné reči, nehodné nie len mladistvého poslucháča. Nožíček mi ako sa hovorí  vo vrecku poskakoval a ja som si musel zahryznúť do jazyka, lebo v sobotu som už naozaj nemal chuť nikoho vychovávať.   Radšej sme zotrvali dlhšie pri tzv. Svoradovom prameni, rozbalili prvé rohlíčky, lebo na čerstvom vzduchu rýchlejšie spaľuje. Keď sa opitia chtivá detská výprava stratila v lese pohli sme sa aj my. Putovali sme najskôr k Svoradovej jaskyni, no najmladšia turistka už nevládala (darmo kto by šľapal do kopca), tak som ju zobral na ruky a ona mi ševelila všakovaké veci do uška: o prekrásnom rozprávkovom lese, že tu kľudne môže žiť aj dinosaurus, že čo je hento zelené, že videl si toho prekrásneho motýľa, ako sa to tu ozýva a prečo sa to tu tak ozýva.   Keď sme mali k najbližšiemu cieľu pár metrov, staršia dcéra zbadala hore na miernom kopčeku nejakú skalnú dieru a bežala ju preskúmať. Samozrejme aj mladšia dostala zrazu silu do nôh a skoro zoskočila na zem z mojej náruče. Bežala hore briežkom a kričala: „aj ja, aj ja". Čo však čert nechcel, keď bola asi v polovici kopčeka, zaútočil na ňu nejaký strašný hmyz (asi mucha) a ona sa otočila a bežala dole briežkom. Zrazu stratila rovnováhu, a padla, a robila kotrmelce a my s manželkou v kŕči, neschopní reakcie sme sledovali jej artistické číslo. Bol som presvedčený, že naša exkurzia sa v tomto momente skončila, no našťastie sa nič nestalo, len jej plač prehlušil rev už  bujarých detských „trampov", vychádzajúci z lesa.   Keď plač ustal, a nám bolo vysvetlené, že za to všetko môže obrovský chrobák, ktorý si dovolil preletieť okolo jej hlavy, rozhodli sme sa, že ešte vylezieme k jaskyni a potom sa tam nad jaskyňou na chvíľu usadíme a oddýchneme si. Pravdupovediac potreboval som vyplaviť stresové hormóny, ktoré v takýchto situáciách, keď už vidíte hotovú katastrofu, zaplavia telo a svalstvo a vy len ľutujete, že ste si nezobrali aj ploskačku. Po obhliadke malej skalnej jaskyne a skúške odvahy, sme po pár kameňoch vyliezli na jej strechu.      Tu sa nám naskytol prekrásny výjav. Pri pohľade na to, čo sa otvorilo pred nami, a čo rozohrala príroda, som získal späť na chvíľu stratenú rovnováhu a duševný kľud.               Na vrchol Pyramídy sme už, ani nepokračovali. Vyhrievali sme na kameňoch a ja som rituálne svojim nožíčkom krájal jabĺčko a hral som sa na chlapa pri zvukoch vtáčieho spevu a vôni jarných kvetín.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (7)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Strach, ktorý sa vykŕmi deťmi, trúfne si aj na dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Svet včerajška na hrane zajtrajška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kuric
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kuric
            
         
        jozefkuric.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    323
                
                
                    Celková karma
                    
                                                5.98
                    
                
                
                    Priemerná čítanosť
                    4080
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Bulvár
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Politická realita
                        
                     
                                     
                        
                            Hudobná sekcia
                        
                     
                                     
                        
                            Zápisky a spomienky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Pokus o literatúru
                        
                     
                                     
                        
                            Cogito ergo...
                        
                     
                                     
                        
                            Tvorivosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            insitný predčítač
                                     
                                                                             
                                            Píšem aj sem
                                     
                                                                             
                                            a trocha aj sem
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




