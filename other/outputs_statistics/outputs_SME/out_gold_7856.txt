

 Vyrástla som v byte s ústredným kúrením, s ústrednou teplou vodou, a vôbec všetko bolo riešené akosi ústredne, a čo tak riešené nebolo, tak snáď okrem opravy práčky sa v ňom ani neriešilo. A tak to bol pre mňa tak trochu zázrak naživo, ktorý som sledovala s radostným žasnutím: kdesi sa čosi zmenilo (pribudol kotol na chodbe) a zrazu kdesi úplne inde (v kúpelni a v kuchyni) voda vytekajúca zo steny už nebola studená ale teplá. Trúbky v stene už dávno bez stopy zakryté pod omietkou tam predsa boli, aj keď ich už nebolo vidieť a vedela som, že sú tam, len preto, že som sledovala proces budovania. Uvedomila som si, že i samotné zapnutie svetla je vlastne podobný div - kdesi stlačíme gombík a kdesi inde sa rozsvieti žiarovka. Pre človeka z jaskyne by to zázrak isto bol. 
 Večer pri zaspávaní mi potom preblesklo hlavou, že i život a vôbec svet je vlastne taký dom, do ktorého sme prišli už keď mal omietnuté steny, a tak o mnohých trúbkach a kábloch nevieme, nevieme kade ich „Staviteľ" naťahal a nevieme, ktorá vedie odkiaľ kam. Bádame, zisťujeme metódou pokus-omyl, ktorý gombík, čo spôsobí, čo je príčinou a následkom čoho. 
 V porovnaní s domom je však svet oveľa zložitejší a život omnoho dynamickejší. Je to vskutku ťažká úloha, ale vzrušujúca - veľa sme už pochopili a ktovie, aké súvislosti ešte objavíme? Ja sa na ne teším (aspoň na tie, ktoré stihnem). A vy?:-) 
   

