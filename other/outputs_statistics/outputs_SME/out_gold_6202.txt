

 Vo štvrtok som dostala zopár veľkých dobrých správ a jednu malú slnečnicu. Budem sa jej každý deň pýtať, ako sa jej darí, či rastie, či to celé s tou pôdou a semienkami, a zalievaním v tejto dobe ešte funguje, a nebudem si o tom nič myslieť – žiadne znamenie a blabla, že ak rastie, rastie čosi iné kdesi vo mne či tak nejako, že ak uhynie, uhynie čosi iné kdesi vo mne, možno práve preto si o tom nechcem nič myslieť. Je toľko vecí, o ktorých stojí za to nič si nemyslieť, vždy stačí na to iba včas prísť. 
   Listovala som vo svojich starých poznámkach, skicách, v ktorých som vlastne nič nehľadala a rovnaké nič nenašla, iba svoje rukopisy spred pár rokov, myšlienky, ktoré sa mi vtedy zdali byť dosť rozumnými, dnes sa mi už iba zdajú. 
   
 Z. ma pozvala na kávu, preto bol z toho Dvakrát pasta šalát, prosím! a jedenkrát  rozhovor o tom, čo už tu raz bolo, o tom, že to všetko by nemalo stačiť na jeden ľudský život, že tých dvadsaťneviemkoľko rokov je tak príšerne veľa a ten jeden život tak príšerne málo. 
   Rozprávala som celý ten príbeh o tom, ako som sa jedného dňa naučila nežiť, až som začala nadobro chcieť nežiť. Rozprávala som o tom po ikstý raz nahlas, a po prvýkrát bez chuti zažiť to znova. To všetko ako jedinečný príbeh, akých zniesol tento svet už milióny. Ten svoj si práve žijeme, a kým sme tu, sme nedopovedaní. 
   
 Prežité a nedopovedané príbehy bývajú najdlhšie, niekedy bez konca, lebo koniec by mohol byť taký vážny, zásadný, vieš,  napísala som dnes mojej A., ktorú som nikdy nestretla, a tak mi poslala minulý týždeň malú slnečnicu z Prahy, ktorej sa budem každý deň pýtať, veď viete... Chcem ti poďakovať za milé prekvapenie, za list, aký som už dlho nedostala. Btw, moja Brieffreundin na strednej sa volala Daniela P., ale to je asi všetko, čo mi po nej v pamäti ostalo vrátane fotky z jej maturitného večierka, kde má biele šaty a tmavé kučeravé vlasy. Máš pravdu, bolo to také aktuálne, bolo to vhodné – mať kamaráta zo sveta, bolo to, aspoň mne osobne, úplne nanič. 
   
   

