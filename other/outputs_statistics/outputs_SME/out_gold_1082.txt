

 
   
 
  
  
  
 Platón je prvý idealistický filozof v gréckej filozofii. Ako 20-ročný sa stal Sokratovým žiakom. Keďže bol po otcovi potomkom kráľa, mohol si zabezpečiť filozofické aj múzické vzdelanie. 
  
  
  
  
  
 Popri tom sa pripravoval aj na politickú kariéru. Jeho prezývka Platón (vlastným menom sa volal Aristokles) hovorí o jeho skvelom nadaní. Pochádza z gréckeho slova „platys“, čo znamená široký, podľa širokého hrudníka a čela. 
 
 
  
 Platón vo svojej filozofii došiel k záveru, že Ústav je toľko druhov, koľko je druhov ľudí, pretože Ústava sa rodí z charakteru ľudí, ktorí tvoria štát. 
 Úplne náhodou som až dodatočne našiel filozofické veľmi zaujímavé pojedanie o Platónovi od jedného (už nežijúceho) revolucionára Nežnej revolúcie, Marcela Strýka na nasledujúcom http.: 
 http://www.youtube.com/watch?v=s__uCqzS5SY 

  
 
 
  

 Za najlepšiu formu štátu považuje Platón aristokraciu a monarchiu ( vládu niekoľkých najlepších alebo jedného najlepšieho, kráľa - filozofa ). 
 Vedúcou zásadou reálnych štátov podľa Platóna je: 
 
 
 1. odvaha (timokracia), 
 2. chamtivosť (oligarchia), 
 3. anarchia a svojvôľa (demokracia) 
 4.  strach a zločin (tyrania). 
 Platón vytvára aj obraz postupnej degenerácie Ústav nasledovnými fázami: 
 1. Timokracia - vláda odvážnych a ctižiadostivých, je to vraj najmenej zlé zriadenie. 
 Môže sa však skorumpovať na oligarchiu. 
 2. Oligarchia - ktorá sa zakladá na oceňovaní majetku, vládnu v nej bohatí a chudobní sú z nej vylúčení. K jej zániku viedla nenásytnosť túžby po bohatstve a zanedbávanie všetkého ostatného kvôli zisku. 
 Ak sa uskutoční úspešná vzbura proti bohatým, vznikne od oligarchie horšia forma a to demokracia. 
 3. Demokracia - keď zvíťazia chudobní a ostatným poskytnú rovnaký podiel na správe štátu a v úradoch. Heslom demokracie je sloboda. Je to najmenej stabilné zriadenie, ktoré charakterizuje nedostatok spoločenskej disciplíny, neschopnosť vládcov a nedostatok občianskej cnosti u obyvateľov. Ľudia tu nemusia robiť nič, na čo nemajú chuť, nemusia dodržiavať mier, nemusia počúvať, nemusia sa podieľať na správe štátu. 
 Sloboda v demokracii zákonite nastoľuje najhoršiu zo všetkých Ústav - tyraniu. 
 4.Tyrania - vláda násilia. Znamená zánik slobody a základných hodnôt života. Lebo sloboda vyhnaná do krajnosti sa zmení práve na otroctvo. Cesta proti tomuto vedie cez funkciu vodcu ľudu. Ten okúsi moc a tá ho občas prinúti vykonať hrozné činy. 
 Takýto človek potom musí nutne zomrieť rukou nepriateľov alebo sa stať tyranom, to znamená z človeka vlkom. 
  
 Platón je proti vrstve nemajetných občanov, proletárov, ktorí túžia po zvrate spoločenského poriadku, (proti revolucionárom) keď tvrdí: 
 Ich vznik je potrebné znemožniť vysťahovaním obyvateľstva a tam, kde dochádza k zakladaniu novej obce, tam je nutné najskôr vykonať očistu medzi občanmi ( smrťou alebo vyhnanstvom ). 
  
 Keby Platónove názory čítali  súčasné neziskové organizácie a občianske iniciatívy na ochranu všeobecných ľudských práv, tak by ich označili  za fašistické, či krajne pravicové a na Platóna by podali  trestné oznámenie. I keď oni vznikli už pred 2 300 rokmi. 
 Nakoniec si vypočujme  hymnu Francúzskych revolucionárov Marsejézu od Mireille Mathieu: 
 http://www.youtube.com/watch?v=GtoHs0Ua_Tg 
 a hymnu proletárov Internacionálu. 
 http://www.youtube.com/watch?v=suVB3YGIUk0 
  

