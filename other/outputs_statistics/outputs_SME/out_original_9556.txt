
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Koiš
                                        &gt;
                Hokej a futbal
                     
                 Yes, we can! 

        
            
                                    24.6.2010
            o
            21:25
                        |
            Karma článku:
                6.29
            |
            Prečítané 
            1368-krát
                    
         
     
         
             

                 
                    Pri sledovaní dnešného historicky najúspešnejšieho zápasu slovenskej reprezentácie mi v mysli vyskočili dva momenty z minulosti. Keď Róbert Vittek strelil svoj druhý gól v zápase a zvyšoval na 2:0, spomenul som si na rovnaký moment z roku 1994, kedy Roberto Baggio prestrieľal Talianov do finále, hoci dovtedy prechádzali turnajom len s odretými ušami. A druhý moment nastal po záverečnom hvizde, kedy som si spomenul na pamätné heslo dnešného amerického prezidenta Baracka Obamu - Yes, we can! Áno, dokážeme to. Prípadne, nič nie je nemožné.
                 

                 
Foto: TASR (futbal.sme.sk) 
   Všetko zlé je zabudnuté, ideme ďalej  Keď som predvčerom vydal futbalový blog, kde som analyzoval situáciu v našej skupine, dospel som k záveru, že naše prvé dva zápasy nič neznamenali. Aj keby sme hrali podľa papierových predpokladov, na postup zo skupiny by sme museli bezpodmienečne zdolať Talianov. V diskusii so mnou niektorí súhlasili, mnohí však nie. Tie komentáre sa nečítali ľahko. Zvlášť ak jeden tiežfanúšik zhodil naše doterajšie úspechy tým, že aj Poliaci si museli proti nám dať vlastný gól, aby nás nechali vyhrať a postúpiť na MS.  Že za tým postupom sa skrývalo víťazstvo v kvalifikačnej skupine, ktoré vôbec nebolo náhodné, to už mnohí fanúšikovia nevnímali. Rovnako mediálna hystéria okolo hroziaceho neúspechu bola prehnaná. Slovensku hrozilo posledné miesto v "ľahkej" skupine, a medzinárodná blamáž. Nikomu by nenapadlo, že tá znásobená blamáž sa nakoniec ujde Talianom, ktorí nielenže skončili v základnej skupine poslední, ale nedokázali zdolať ani jedného zo súperov. A to sa pred šampionátom talianske médiá domnievali, že táto skupina bude pre majstrov sveta prechádzkou ružovým sadom.  Poskočili sme o 13 priečok  Keď sa pozrieme na priebežnú tabuľku všetkých mužstiev na šampionáte (nájdete ju napríklad na stránke futbal.stv.sk v sekcii Štatistiky), Slovensko po tomto triumfe poskočilo z 27. na 14. miesto. Taliani v úlohe majstrov sveta sú zatiaľ na 25. mieste, Francúzi ako finalisti sú štvrtí od konca. Aj keby naši hráči dnes remizovali či prehrali, stále by sa im ušlo lepšie umiestnenie než dvom tímom, ktoré pred štyrmi rokmi bojovali o titul.  Zároveň sme sa opäť presvedčili, aká tenká dokáže byť hranica medzi úspechom a neúspechom. Prvé dva zápasy nám nevyšli (hoci ten s Novým Zélandom by bol úplne v inom svetle nebyť nešťastného záveru), lenže v poslednom súboji sme ukázali náš potenciál, a vďaka výhre v správnom čase mohla v našom tíme zavládnuť zaslúžená eufória.  Povestné športové šťastie  Ak si ju dokážeme preniesť aj do vyraďovacích súbojov, určite sme na šampionáte nepovedali posledné slovo. A je sympatické, že náš postup zo skupiny je po dnešnom výkone vnímaný zaslúžene. Morálne právo na postup si naši chlapci dnes doslova vydreli a vybojovali. Treba však zdôrazniť, že pri tomto úspechu sme mali aj OBROVSKÝ kus športového šťastia. Verme, že našim bude šťastie priať aj naďalej. Ak podajú v ďalších súbojoch podobný výkon ako dnes, môžu to dotiahnuť veľmi ďaleko.   PS: Keď už som spomínal Obamu, nemôžem opomenúť ani obľúbené heslo amerického viceprezidenta Joe Bidena, ktoré tiež vystihuje náš dnešný úspech - This is a BIG FUCKING DEAL!!! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Malé desatoro SaS: Koncesionárske poplatky (už) nezrušíme!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Referendum podľa Fica: občianska povinnosť len keď sa mi to hodí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Pán Kmotrík postaví štadión, prispejeme mu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Do tankoch a na živnostníkov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Koiš 
                                        
                                            Omeškal som sa so splátkou úveru. O mínus jeden deň (aktualizácia)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Koiš
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Koiš
            
         
        kois.blog.sme.sk (rss)
         
                                     
     
        Rád píšem o všetkom, čo ma zaujme. A rád dávam najavo aj pozitívne skúsenosti, ktoré nevnímam ako samozrejmé.


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    91
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2757
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Letecká doprava
                        
                     
                                     
                        
                            Cestovanie vlakom
                        
                     
                                     
                        
                            Spoločnosť a politika
                        
                     
                                     
                        
                            Médiá @ komunikácia
                        
                     
                                     
                        
                            Hokej a futbal
                        
                     
                                     
                        
                            Život v Bratislave
                        
                     
                                     
                        
                            Život v Prahe
                        
                     
                                     
                        
                            Ostatné články
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Gabin - La Maison
                                     
                                                                             
                                            Tom Waits - What's he building in there?
                                     
                                                                             
                                            Kings of Convenience - I'd rather dance with you
                                     
                                                                             
                                            One Night Only - Just for tonight
                                     
                                                                             
                                            Counting Crows - A Murder of One
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Ščobák
                                     
                                                                             
                                            Jozef Havrilla
                                     
                                                                             
                                            Jan Potůček
                                     
                                                                             
                                            Sergej Danilov
                                     
                                                                             
                                            Lukáš Polák
                                     
                                                                             
                                            Zdeno Jašek
                                     
                                                                             
                                            Jozef Červeň
                                     
                                                                             
                                            Richard Sulík
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Digitálně.tv
                                     
                                                                             
                                            Digizone.cz
                                     
                                                                             
                                            RadioTV.sk
                                     
                                                                             
                                            RadioTV.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Chrániť ľudí pred vrahom (a naopak)
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




