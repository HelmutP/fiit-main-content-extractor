
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Čo je nové na sme.sk
                                        &gt;
                Nezaradené
                     
                 Blog.sme.sk má 4 roky - čo najzábavnejšie SME s vami zažili? 

        
            
                                    19.10.2008
            o
            15:01
                        |
            Karma článku:
                12.75
            |
            Prečítané 
            26756-krát
                    
         
     
         
             

                 
                    Práve dnes máme na blog.sme.sk malé výročie – sú to presne štyri roky od publikovania oficiálne prvého článku na blogu SME. Pri tejto príležitosti sme pre vás pripravili niečo špeciálne. Nasledujúci výpis tých najneuveriteľnejších citátov blogerov neberte prosím v zlom. Nie je to výsmech a nie je to zlomyseľné. Buďte si istí, že admini blogu SME majú výrazne posunutý prah citlivosti smerom k totálnemu umŕtveniu. A z vás, blogerov a blogeriek, sa rozhodne nechcú smiať. Ale občas sa to už nedá ;-)  S čím všetkým sme sa postretli v administrácii blogov od r. 2004 až po dnešok... Malý výber toho, čo stojí za to ukázať. Za zozbieranie patrí vďaka všetkým súčasným aj bývalým adminom a nášmu archívu.   Na úvod vskutku výstižné motto – citát od nemenovaného admina:  Navrhujem uzákoniť trestný čin týrania blogmi!
                 

                  Prvý e-mail od blogera:  Subject: chcem vas poziadat o zaslanie hesla  Dobry den, chcem vas zaslat o poziadanie hesla ku schranke OPER zabudol som heslo... Prosim vas o heslo...podpis: XXX    Odpoveď admina:  Adresa na prihlásenie: XXX  Login (priezvisko): XXX  Prihlasovacie heslo: XXX  Po úspešnom prihlásení ho odporúčam v operátore blogu zmeniť (položka Prístup &gt; Zmena hesla).     Druhý e-mail od blogera:  HESLO SOM VEDEL!poplietol som si len vstupne login (priezvisko)! myslite ze som nejaky blbec?       Zo žiadosti o registráciu blogu:  som dievca.mam asi 165cm a 60kg.mam 3 psof.moji rodicia su rozvedeny.otec je skurv.syn a matka total prepnuta.ja som dusevne zdravie ziskala pred pol rokom(skvelu cvokarku ma...)mam aj frajera.jeho meno je XXX.odkedy si pamatam rada pisem.pisem vsetko:od vyplodov mojej chorej mysle, az po politicku situaciu kt ma natolko vytoci, ze to jednoducho musim zapisat.dokonca rada aj citam.viac menej som v celku zaujimava osoba.necudujte sa som cvok       Z odkazu blogera, ktorý bol upozornený na kopírovanie:  us sa to nebude opakovat us nebudem kopirovat,palagaitora robit.      Prvý odkaz od blogera ako odpoveď na upozornenie na porušovanie kódexu:  urcite si kodex neprecitam. sevas   Druhý odkaz od blogera ako reakcia na vyradenie z Výberu:  ci pana ! vyobcovanie ! ake priserne ! !     Tretí odkaz blogera (po 3 mesiacoch):  po mojej velmi drzej reakcii adminovi sa ziaden z mojich blogov nezobrazuje na titulke, aj ked dodrzuju vsetky predpisane pravidla. mohol by sa admin prestat hnevat a uvolnil by moje blogy do normalneho obehu ? dakujem       Z e-mailu od blogera (mimochodom od veštby prešlo už pol roka):  Mám pripravených zopár článkov, ktoré sú špecificky odlišné od toho, čo sa bežne uverejňuje. Súvisia so životne dôležitými otázkami a najmä s vážnou a rozsiahlou energetickou krízou, ktorá už za veľmi krátky čas postihne ľudstvo, o blížiacich sa extrémnych horúčavách a následných klimatických zmenách neočakávaného rozsahu a pod. Napríklad životnosť celosvetovej internetovej siete má pred sebou už len niekoľkomesačnú dobu funkčnosti, internet sa potom stane natrvalo už iba nenávratnou minulosťou...       Zo žiadosti o registráciu blogu (žiadateľka je študentka sociológie v XXX):  a pochadzam z Bratislavi... a mam chud povedat na vec aj svoj nazor...       Reakcia na neschválenie registrácie blogu:  idz te do riti! maory debilny       Otázka od blogerky:  dobrý deň, mám takú malú otázočku...neviete mi poradiť, čo som spravila zle, keď sa mi pod článkami stále ukazuje iba diskutujte( a žiadne reakcie?)žeby to bolo pre to, že nemám naozaj žiadne reakcie?       Otázka od blogerky:  Dobrý den, prosím chcela by som Vás upozornit na fakt, ze vo administrácii blogov sme.sk sa nachádza trojanský vírus, ktorý sa mi pri návsteve administrácie mojho blogu snazí prerazit do pocítaca. Moj spolahlivý vírusový program ho ale vzdy zneskodní. Neviem si to vysvetlit, kto má záujem manipulovat a ovadat mojím pocítacom. A co tí, co antivírusový program nemajú. Prosím, mohli by ste mi jasne odpovedat na tieto závazné otázky?       Odpoveď blogerky na upozornenie, aby nepísala VEĽKÝMI PÍSMENAMI (vypla si CapsLock):  BERIEM NA VEDOMIE A BUDEM SI NABUDUCE NA TO DAVAT POZOR. NEBOLO MOJIM UMYSLOM PUTAT POZORNOST, A UZ VOBEC NIE PORUSOVAT KODEX BLOGERA. DAKUJEM       Zo žiadosti o registráciu blogu:  AHOJ.VOLAM SA XXX.BYVAM V XXX.NENAVIDIM SA AJ VSETKO NA SVETE.ALE TO VAS NEZAUJIMA.NO SOM UPLNE NENORMALNE DIEVCA.MAM SUPER KAMOSKY KTORE SU MOJOU OPAROU.MAM AJ SUPER RODINU AJ KED NIEJE MOC VELKA.NO CO NEVADI.STRASNE RADA MAM HUDBU.PUNK JE NAJ.NO MYSLIM ZE STACI SAK UZ NEVIEM CO NAPISAT!       Zo žiadosti o registráciu blogu:  som XXX mam 13 rokov a mam rad hry, ciklistiku, akvaristyku, cestovanie, fotbal, volejbal, basketbal,plavanie (mam 2 rybky tak radenie o akvaristyke by nemal byt az taky problem) :-),mam rad fyziku a matematyku       Zo žiadosti o registráciu blogu:  Mam 18 rokov a z dovodu dlhych prazdnin prazdnin by som rad uverejnil svoje prace       Príspevok v internej diskusii:  Ponukam psie steniatka rasy Cavalier King Charles Spaniel bez PP. Male, chutne, vhodne aj do bytu. Pri trpezlivom vycviku dokazu vytukat aj login a heslo do blog-operatora.       Z odkazu blogera adminom:  Ja viem, ze som v minulosti robil neplechu ale som sa poucil, lebo som medzi tym zostarol o niekolko kratkych ale velmi vyznamnych mesiacov, ktore ma presvedcili, ze som praviciar z jemne komunistickov prichutov (absolutne zbaveny akychkolvek nacionalistickych a inych xenofobnych pachuti). Preto si myslim, ze si zasluzim dalsiu sancu na sirenie dobreho slova o tomto serveri a administratoroch tohto servera, lebo ma svojimi cinmi presvedcili ze su to bohovia.       Zo žiadosti o registráciu blogu:  nemam cas sa vam predstavovat - ale nechcem aani pisat clanky - chcem si pozriet administraciu systemu, lebo programujem vlastny system, a chcem vediet, co mi tam este chyba       Ukážková žiadosť o registráciu blogu:  Vazeni administratori, nebudem tu robit nic zle, chcem len pisat. Tak ma tu prosim nechajte.       Informácia O mne na osobnom blogu:  Podľa administrátora BLOG.sme.sk som nekvalitným autorom, ktorý nestojí za to, aby bol vo "výbere sme", alias vo vyvolených, pričom úspešne tam "nacpe" všetko nové. Funguje už aj na blogu typu SME, niečo také ako dvojitý meter? Alebo som niekomu padol do oka a som mu iba tŕňom v... zadnici?       Z perexu článku:  V úvode by som len chcel poprosiť adminov, aby mi tento „článok“ nezrušili, aj napriek tomu že porušuje kódex... Ide o vec ktorú potrebujem vyznať verejne. Ďakujem za pochopenie.       Zo žiadosti o registráciu blogu:  Prvá suvíslá veta o mne. Druhá suvíslá veta o mne. Tretia suvíslá veta o mne. Štvrtá suvíslá veta o mne. Piata suvíslá veta o mne. :) A teraz vážne: Mám XX rokov, študujem momentálne externe XXX na XXX v XXX. Ako vidieť vyššie mám zmysel pre humor.       Zo žiadosti o registráciu blogu:  No zančnem tim že chodim na gimnazium už druhy rok. Toto je fakt dobra stranka ale nechcem sa tu zaregistrovat len preto lebo si to mislim ale aj ze to potrebujem.       Správa z webu:  Pan XXX (meno admina),iba som sa tak trosicku hral s internetom, prisielsom asi na to co Vas trapi,ze 18 rocny chlapec ma viac karmy a rozumu ako vy. Myslim tym XXX (meno blogera) pekny den       Zo žiadosti o registráciu blogu:  Som tréner,ktorý vo vnútri smúti,že v športe máme na to,ale nemôžme. muž,ktorý by chcel zmeny v zákone o monogamii.otec milujúci tri deti a ich dve maminy     Z odkazu blogerky adminom:  moja otazka..preco moje identifikacne meno je take ake je vyrazne iritujuce napriek tomu ze som uviedla pseudonim...chcem zrusit cely blog...roztrhat spalit...neznesiem pohlad nato meno..ktore tam odrazu figuruje akoby malo zastupovat moju osobu...znicit destruovat vsetko tu!!!     Otázka blogerky adminom:  ehh ahoj ja len že by som chcela vedieť kedy sa konečne zmení to slovko "nový" nad mojimi niektorými aj staršími článkami :) dikyy       Odkazy notorického sťažovateľa na diskusie:  – prosím, chcem sa dostať do diskusie a bránite mi že?  – prečo sa mi nedá prihlásiť do diskusií? Ďalšia buzerácia??????       Odkaz blogera adminom:  nenavidim vas vyber clankov uverejnenych na titulke, ste obycajny bulvar.       Zdôvodnenie žiadosti o zrušenie blogu:  mno padnydovod je ten ze uzsomzmazal vsetky moje veci takbysomrad zmazat ajtentoposledny pokus o nie prisli dobru poeziu,dakujem       Odkaz blogera adminom:  Ja sa chcem opytat ci ma uz zostatnite, lebo sa daco take mlelo u tetky Zofky, ze po polroku ludi vraciate spat? Hm, tak ja iba toto som chcel a este ze chytro berte sanky, kym sa sneh neroztopi a sa sankovat chodte.       Odkaz blogera adminom (asi k zaraďovaniu článkov do rubrík):  nikto mne nemože a nebude diktovat kde budem davat svoje clanky, jasne???       Otázka blogera technickej podpore:  Ak môj blog nie je vedomo ignorovaný. potom by som rád vedel prečo sa nemôžem z neho odhlásiť. Keď kliknem na políčko dohlásiť sa zobrazí sa mi vždy karta prihlásiť sa.       Sťažnosť blogera na adminov:  Dobry den, vy nedodrzujete limit 24hodin pre zodpovedanie otazky ku blogu. Vy ma sustavne vylucujete z komunikacie, to je psychologicke sikanovanie. U vas je to pravidelne, pomaly je jun - a jaj mam spravu v marci a predtym len zminuleho roku. Vy mi davate najavo akobykeby som bol nulla. Ste psychosikanatori, cely kolektiv it obsluhy blogov ste spiknuty. Vam zelam emocie ktore by ste mali keby ste mali hovorit ku stene. Bezcitne, neempaticke kreatury! Odemocionalizovane nekomunikativne bytosti!  Dehumanizovany a mozno aj psychicky natuknuti. Nie ste cisty. Potrebujete komunikacny seminar! a, mozno aj purifikaciu. je to s vami vazne. je to s vami zle.       Otázka blogera adminom:  OTAZKA: DISPONUJETE SO STABILNYM DUSEVNYM ZDRAVIM? Citite sa fit?       Otázka blogerky:  zaujimalo by ma ako mam dosiahnut aby si aspon niekto klikol na moj clanok,ci uz mi prida karmu to uz je ine,alemoj clanok este nikto ani necital       Odkaz blogera adminom:  preco ste ma dali do "mimo nasho vyberu"? ak mi nedate dobry dovod, tak odtialto proste odidem a pojdem ku konkurencnemu blogu.       Zdôvodnenie žiadosti o zrušenie blogu:  lebo moj blog je fraska a nemam viac co povedat.       Odkaz blogera adminom:  Ten váš výber, to akože je riadna hlúposť, by ste mali nechaťrozhodovať ľudí, ale vidno, že všetko musíte robiť len vy, už keď, takto robte podľa karmu a rád by som vedel prečo niesom vo výbere????       Odkaz blogera adminom (opäť niečo o Výbere):  prosim ta ako casto sa meni zoznam "smrti" ?? ten "vyber" SME?? :))       Z e-mailu novoregistrovaného blogera:  Zaroven chcem poziadat, aby sa v nijakom pripade v mojom blogu nevyskytovala nijaka zmienka o karme. To tam nema co hladat! Na to, ake ste seriozne periodikum je to cosi naozaj hlupe. Znovu opakujem, nechcem tam mat nijake hluposti o karme a akesi trapne cisla. Som krestan, my mame lepsie veci ako takuto ubohost. Nechapte to v zlom, ale Vy ste to tam natrepali bez toho, aby som o tom ja rozhodol.       Odkaz blogera adminom:  Vážená redakcia, ak chcete aby môj blog bol zrušený, tak mi to povedzte priamo a nezmemšujte my písmená článkov ktoré sa Vám nepáčia. Tým škodíte sebe, ale hlavne Slovenskému národu.       Otázka blogerky:  Ak uz mam napisanych viac ako 10 clankov, mam zaplatit nejaky poplatok? Ovplyvnuje to nejako uverejnenie clanku na blog.sme.sk?       Zdôvodnenie žiadosti o pseudonym (vek blogera – 13 rokov):  Ján Smrek – na svojom blogu chcem prezentovať svoje hlboké myšlienky a pocity.Myslím si, že mojím zosobnením by sa výrazne znížila ich kvalita,teda aj ich prínos spoločnosti.       PS: Ak ste, milí čitatelia, nejaké hlášky nepochopili, nič si z toho nerobte. Občas nechápeme ani my ;-) Niektoré tieto perly zas vyžadujú znalosť systému blogov SME, takže im skôr porozumejú len samotní blogeri.  Tak či onak, na ďalší rok s vami, blogermi a blogerkami našimi milými (aj nemilými), sa tešia admini blogu SME.sk.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (394)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            SME hľadá redaktorov, online editorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Prečo pod niektorými článkami nie je možnosť diskutovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Sme.sk hľadá HTML/CSS kodéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Vitajte na úplne nových blogoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Nová titulná stránka SME.sk: Viac správ, viac čítania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Čo je nové na sme.sk
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Čo je nové na sme.sk
            
         
        novinky.blog.sme.sk (rss)
         
                                     
     
         Novinky, zlepšenia a vôbec všetko, s čím sa chceme podeliť. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    153
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6917
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Novinky na sme.sk
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




