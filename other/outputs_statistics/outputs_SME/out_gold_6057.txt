

   
  Ráno nás skutočne privítalo skvele – priezračne čistá, fotogenická obloha a slnkom prežiarený Tel Aviv. Nadšení, že meteorológovia sa zasa raz nemýlili, sme sa vydali aj s našimi dvomi ratolesťami a priateľmi na cestu. 
 Prvou zastávkou bol národný park Beit Šearim v dolnej Galilei, niekoľko desiatok kilometrov od pobrežia Stredozemného mora. Od hlavnej dopravnej tepny sme sa kľukatou cestičkou, lemovanou rozkvitnutými kríkmi s kvetmi všetkých možných farieb, dostali ku vchodu do parku. Podľa počtu áut pred nami a na parkovisku sme usúdili, že toto miesto je dosť navštevované. Veľa ľudí s rodinami sem prišlo stráviť celý deň, keďže – ako vo viacerých izraelských národných parkoch – aj tu boli veľmi dobre zariadení na piknikovanie a oddych. Naším cieľom bolo však neboli grilovacie zátišia, ale historické pamiatky. V tomto smere môže slovenského návštevníka názov „národný park“ či „prírodná rezervácia“ mierne zavádzať, avšak vo väčšine národných parkov v izraelskom ponímaní sa nájde aj menšia či väčšia pamiatka. 
 V Beit Šearim to boli zvyšky antického mesta na kopci, do úbočia ktorého bol vytesaný rozsiahly židovský cintorín. Samotné mesto bolo pritom ešte na prelome letopočtov len jedným z mnohých malých židovských osídlení v dolnej Galilei. Na význame získalo až v 2. storočí, keď sa po Bar-Kochbovej vzbure (132-135 po Kristovi) centrum židovského života presunulo z Judey do Galiley a Beit Šearim (po grécky Besara) sa stal sídlom Sanhedrinu – najvyššej židovskej rady a súdu. V Beit Šearim je aj hrob rabína Jehudu Ha-Nasiho, predsedu Sanhedrinu a zostavovateľa mišny (ústnej tradície), ktorého prítomnosť spôsobila, že sa na prelome 3. a  4. storočia po Kristovi miesto stalo hlavným pohrebiskom tak pre lokálnych, ako aj diasporálnych Židov. 
   
   
  
   
   
 Mesto dosiahlo najväčší rozkvet na prelome 3. a 4. storočia, avšak počas vzbury, ktorá tu vypukla roku 351 proti Gallovi - miestnemu rímskemu správcovi, Rimania mesto zničili. Život sa tu síce v nasledujúcich dvoch storočiach obnovil, no Beit Šearim už nikdy nedosiahol predošlú slávu. Časom bol úplne opustený a upadol do zabudnutia, až kým ho v prvej polovici minulého storočia náhodou neobjavil Alexander Zaid.  
 Archeologický výskum odhalil zvyšky mesta s rozlohou takmer 13 hektárov. Zachovali sa zvyšky starej synagógy, baziliky, náboženských škôl a obytných domov, ale aj mestských hradieb, vstupnej brány a výrobne olivového oleja – všetko z obdobia 2. až 4. storočia po Kristovi. Okrem toho sa objavilo viac ako 30 tzv. pohrebných jaskýň, ktoré patria medzi najzaujímavejšie a najkrajšie v celom Izraeli. V tomto „meste mŕtvych“ má vstup do veľkých jaskýň impozantnú fasádu, vytesanú v štýle „klasickej architektúry“. V strede sú kamenné dvere, ktoré vedú do podzemných pohrebných komôr. Tie majú rôzne veľkosti a nachádzajú sa v nich buď vtesané „pohrebné priečinky“, alebo masívne kamenné sarkofágy. Na stenách aj na sarkofágoch sa nachádzajú ozdoby a nápisy v hebrejčine, gréčtine a ďalších jazykoch. Najčastejšie uvádzajú informácie o pôvode, zamestnaní a rodinných vzťahoch nebožtíkov. 
  
 
 
   
   
  Jaskyňa sarkofágov je najväčšia. Názov dostala podľa „pojedačov mäsa“, ktorých sa tu našlo až 135. Sú na nich vytesané štylizované dekorácie zvierat, hlavne orlov, rýb a býkov. Miesto sa nazýva aj Jaskyňou rabínov, pretože nápisy na sarkofágoch obsahujú mená mnohých týchto náboženských predstaviteľov. 
   
   
   
  Okrem spomínanej jaskyne nás zaujala niekdajšia cisterna na vodu, ktorá bola prerobená na skláreň. V jej strede leží kus surového skla vážiaci takmer 9 ton. Sklári ho museli taviť niekoľko dní pri teplote vyše 1000 °C, avšak práca sa nevydarila. Miesto do stupy však toto ťažké sklo vošlo do análov.  
   
  Pre našich dvoch synov – Jožka a Tomáška - to tiež bolo zaujímavé dobrodružstvo. Zatiaľ nie v duchu Indiana Jonesa, skôr ako vítané miesto na skrývačku, resp. inšpirácia pre ich maliarske pokusy. Také niečo nevidia každý deň, aj keď Tomáško je pripravený vyraziť „na pamiatky“ aj o polnoci. 
 Naším ďalším sobotňajším cieľom bol Tel Hacor – ruiny dvetisícročného osídlenia, ktoré boli v roku 2005 zapísané do zoznamu Svetového kultúrneho dedičstva UNESCO, spolu s Megiddom a Berševou. Na rozdiel od našej predošlej zastávky sme tu boli jediní turisti. Možno aj preto, že automapa mierne zavádza a kladie lokalitu západne od cesty č. 90, hoci tel je na východ od nej – bližšie k Jordánu. Tel Hacor sa nachádza na úrodnej pôde na úpätí Galilejských hôr, skade vyteká potok Hacor, ktorý pri vtoku do údolia Hula vyerodoval strategickú pozíciu.  
   
 Pri hlavnom vstupe nás privítala milá pani, hovoriaca namiesto angličtiny pestrou zmesou hebrejčiny a ruštiny. Tak sme si zároveň oprášili školské vedomosti a spomienky na širokú ruskú dušu. Po obdržaní mapiek areálu sme sa najprv posilnili a uhasili smäd. Jednoduchý obed sme si skutočne vychutnali v tomto tichom prostredí na kopci, odkiaľ sa nám naskytol nádherný výhľad na okolitú krajinu. Apropo, mapy – berieme vždy najmenej tri kusy, keďže naši synovia sú už skúsení cestovatelia, na mapy sa už tešia, so záujmom ich študujú a dokonca sa podľa nich dokážu orientovať. Dúfame, že kým sa vrátime domov, stane sa Slovensko svetovým aj v takomto spôsobe propagácie svojich pamiatok.  
 Tel Hacor má dve hlavné časti – horné a dolné mesto – a jeho osídlenie spadá do dvoch hlavných období: kanaánskeho a izraelského. Výsledkom dlhého osídlenia je 21 rozpoznaných archeologických vrstiev. Prvé doklady o osídlení sú doložené zo skorej kanaánskej doby bronzovej (cca 2700 pred Kristom), kedy bolo obývané len horné mesto. Už v tomto období mal Hacor úzke vzťahy so Sýriou. Koncom strednej doby bronzovej bolo osídlené aj dolné mesto a spolu tu bývalo okolo 20 tisíc obyvateľov. Mesto tak bolo jedným z najdôležitejších centier regiónu a prechádzali cezeň obchodné karavány. 
   
   
 Hacor prekvital aj počas neskorej doby bronzovej (2. polovica 2. tisícročia pred Kristom), archeologické výkopy tu odhalili mestské hradby s bránami, chrámy a paláce, obytné budovy, zbrane a množstvo umeleckých predmetov. Nálezy dokazujú, že mesto udržiavalo kontakty nielen so Sýriou, Egyptom, Babylonom, ale taktiež s krajinami Stredomoria – Gréckom, Krétou a Cyprom. Hacor je napr. zmienený v dokumentoch z Mari (sýrsky Tel Haríri na západnom brehu Eufratu)  z 18. storočia pred Kristom, ako aj v zoznamoch o mestách dobytých faraónom Tutmosom III. Nachádza sa aj v dokumentoch z amarnského archívu v Egypte a na samotnom tele bol nájdený úlomok s titulom staroegyptského hodnostára z 13. storočia pred Kristom. Bohaté kontakty sú dôkazom významu mesta a zrejme aj dôvodom, prečo sa v Starom zákone Hacor (podľa slovenského prekladu Azor) nazýval aj „hlavným mestom týchto kráľovstiev“ (Kniha Jozue, 11:10) a hacorský vládca Jabin „kanaánskym kráľom“(Kniha sudcov 4:2). 
   
   
   
 Približne v tejto dobe (13. storočie pred Kristom) dolné mesto úplne zničil veľký požiar. Niektorí bádatelia ho spájajú s dobytím opevnenia izraelskými kmeňmi na čele s Jozuem. Chronológia vrstiev vypovedá, že po požiari zostal Hacor na takmer 150 rokov opustený, dolné mesto nebolo už nikdy viac osídlené a noví obyvatelia prišli až začiatkom 11. storočia pred Kristom. Hacor však už nenadobudol ani zďaleka takú pozíciu, akú zastával predtým. 
   
 V 10. stor. pred Kristom sa Hacor stal jedným z najdôležitejších miest v izraelskom kráľovstve. Podľa biblických kníh tu mal kráľ Šalamún nechať postaviť (kazematové) opevnenie, podobne ako v Megidde a Gezere (1 Kniha kráľov, 9:15), ktoré majú aj podobné vstupné brány. Opevnenie rozšíril v 9. storočí pred Kristom kráľ Achab, pričom osídlenie v hornom meste malo zdroj vody, stála tu citadela a sklady. Neskôr sa v Hacore striedali obdobia úpadku a opätovného rozkvetu a historické izraelské osídlenie tu mal definitívne zničiť asýrsky vládca Tiglat-pilesar III. v roku 732 pred Kristom. Poslednýkrát sa spomína v biblickej knihe Machabejcov (11:67), kde sa opisuje vojna Jonatána proti Demetriovi, ktorá bola na „Hacorskej pláni“, cca v roku 147 pred Kristom. 
   
   
  Toľko stručne o histórii Hacoru. S deťmi sme si prezreli aspoň časť niekdajšieho horného mesta, keďže areál je skutočne obrovský. Krátko po poludní už bolo aj pomerne teplo, zostup do pôvodnej vodnej cisterny bol teda príjemne občerstvujúci. Šachta je hlboká 45 metrov a zostupuje sa do nej po špirálovitom schodisku. Nič moc pre tých, čo majú slabú kondíciu, alebo trpia klaustrofóbiou. Zvládli sme aj s chalanmi, hoci ten menší mal trošku obavy zo strašidiel. Nakoniec sa dal nahovoriť a v bezpečí ockovho náručia sa dostal až na úplné dno. To bolo potom výskotu, ako sme hlbokóóóó! A ako vysokóóóóó musíme vyliezť, aby sme sa dostali von. 
   
   
   
  Plní dojmov sme vyrazili k poslednému sobotňajšiemu cieľu – Genazaretskému jazeru. Idúc kľukatými cestičkami v hlbokých malebných údoliach mohli sme do sýtosti obdivovať jarnú krásu galilejskej prírody, ktorá v tomto čase rozkvetu oplýva nespočetným množstvom farieb a vôní. Zanedlho sa pred nami ukázalo v celej svojej nádhere jazero, ktoré sa tiež nazýva Galilejským, Tiberiadským alebo Kinneretským. Podobne ako Mŕtve more, aj ono sa nachádza v hlbokej kotline pod úrovňou Stredozemného mora (cca 220m) a preto je tu teplota vždy vyššia ako v iných častiach krajiny. Okrem toho, že je na okolí jazera asi desiatka posvätných miest kresťanstva, pobrežie je vyhľadávaným turistickým rajom pre domácich i zahraničných rekreantov. 
   
   Keďže ubytovanie v hoteli sme mohli očakávať až po 16. hodine, rozhodli sme sa ešte pozrieť na miesto, kde podľa Nového zákona Ježiš kázal o blahoslavenstvách (Matúš, 5:1-8). Na hore sa nachádza Kostol blahoslavenstiev a novopostavený kláštor, ktorý poskytuje v krásnom prostredí aj ubytovanie pre pútnikov. Pôvodne sme mali aj my záujem stráviť tu jednu noc, no vzhľadom na plnú obsadenosť sme nakoniec boli nútení hľadať nocľah inde. 
   
   
 Je to vždy zvláštny pocit kráčať po svätých miestach a tak to je aj tu. Vo chvíľke ticha sa človek snaží ponoriť do dávnej minulosti a prežívať udalosti, akoby bol ich súčasťou. Sme radi, že takéto miesta môžeme ukázať aj našim deťom. Veď veľa ľudí o nich vie len z Biblie alebo z iných knižiek. 
   
   
   
 Hora blahoslavenstiev je rozhodne pastvou pre oči - nádherne upravené a na jar rozkvitnuté kvetinové záhrady pohladia nielen oči, ale aj nejednu dušu. V sezóne je tu vždy veľa pútnikov. Oddýchnuť si, porozjímať alebo sa občerstviť môžu v malom bufete, ktorý ponúka aj lokálne špeciality a víno z galilejských vinohradov. Z hory je nádherný pohľad na severnú časť Genazaretského jazera, Kafarnaum a Tabghu. 
   
   
 V Tabghe (názov má byť odvodený z gréckeho Hepta pegon, t.j. sedem prameňov) sú podľa tradície hneď dve sväté miesta. Na jednom sa podľa Nového zákona udialo nasýtenie 5000-hlavého zástupu po zázračnom rozhojnení piatich chlebov a dvoch rýb (Marek, 6:30-46) a nachádza sa tu benediktínsky Kostol rozmnoženia. Druhé miesto sa spája so zjavením Ježiša Krista po zmŕtvychvstaní (Ján, 21:1-24), so zázračným nachytaním rýb a potvrdením primátu svätého Petra. Na brehu jazera stojí františkánsky Kostol primátu sv. Petra, v ktorom je prírodný vápencový skalný útvar nazývaný „Mensa Christi“ (Kristov stôl), na ktorom mal Ježiš Kristus jesť s apoštolmi. 
   
   
   
 Navštívili sme len františkánsky kostol, keďže nemeckí benediktíni dodržiavajú otváraciu dobu s nemeckou presnosťou :-) Po celodennej turistike nám dobre padlo posedieť si na brehu jazera a pokochať sa pokojnou a  nerušenou scenériou. Deti neváhali a ako správni chlapci boli hneď povyzúvaní a po kolená namočení v príjemne osviežujúcej vode. Ani sa im nechcelo odísť, tak sa ponorili do stavania hradu z kamienkov a drobných mušličiek. Ale keďže aj františkáni čoskoro zatvárali areál, inú možnosť, ako ukončiť stavbu a odísť nemali. Zanedlho ale prišli na iné myšlienky a začali sa tešiť na hotel v meste Tiberias. 
   
  Simona a Jozef Hudecovci 
   

