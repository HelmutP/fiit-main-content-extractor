

   
 Všetci sme si všimli, že keď vojdeme do kníhkupectiev, tak pre knihy Paula Coelha obsadili už samostatný regál. Niekde medzi nimi sa nachádza aj tenká knižka Alchymista zobrazujúca nekonečnú africkú púšť a osamelého pútnika. Knižka sa čítala veľmi ľahko, takmer sama. Autor používa symbolický jazyk alchýmie a veru niekedy som si musela isté odseky a myšlienky prečítať aj niekoľko krát, aby som im porozumela. Ale to je celý Coelho. Takisto je symbolický aj príbeh Santiaga, pastiera z Andalúze, ktorý sa jedného dňa vydá na cestu za svojím ,,Osobným príbehom". Na tejto ceste je sprevádzaný znameniami, stretne lásku, aj trpkosť, ale nakoniec vysvitne, že všetko zlé je na niečo dobré. Pomocou tohto príbehu sa nám snaží Coelho ukázať hĺbku, krásu a tajomstvo života či šťastia ako takého. Možno si niekto povie, že je to len ďalšia z tých ,,čarovných" kníh, ktoré vyšli na trh v posledných rokoch a hovoria nám, že keď budeme postupovať podľa istých pravidiel, tak budeme šťastní až do smrti. No v Alchymistovi je to napísané tak, že ľudia si so sebou vezmú len tie myšlienky, ktoré chcú. Sú aj ľudia, ktorý si myslia, že kniha je situovaná do nereálneho prostredia dávnej minulosti, no treba si uvedomiť, že základný príbeh je podobný príbehu každého človeka. Nie len toho ,,púštneho" J 
 A ak vám nestačili moje slová chvály, tak snáď fakty, že kniha bola vydaná v roku 1988 a od tej doby sa stále drží na popredných miestach v predajnosti, bola preložená do 30 jazykov a predalo sa cez 5,5 milióna výtlačkov. 
   
   

