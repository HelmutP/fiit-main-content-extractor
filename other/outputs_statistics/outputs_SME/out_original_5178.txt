
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Čejka
                                        &gt;
                Živočíchy
                     
                 Májka nie je len pečeňová paštéta 

        
            
                                    24.4.2010
            o
            18:05
                        |
            Karma článku:
                7.06
            |
            Prečítané 
            2262-krát
                    
         
     
         
             

                 
                    Keď v najbližších dňoch vybehnete na lúku alebo do svetlejšieho lesa,  môžete tam stretnúť nápadného  chrobáka - májku.
                 

                 
Samička májky fialovej so zadočkom plným vajíčok.Foto autor
   Na Slovensku žije okolo 14 druhov májok rodu Meloe, s najväčšou  pravdpodobnosťou bude tá, ktorú stretnete, májka fialová (Meloe  viaolaceus). Odporúčam, aby ste ju radšej nebrali do ruky, lebo  dokáže vypustiť z kĺbov končatín trocha svojej hmyzej krvi (hemolymfy),  ktorá pripomína olejovú kvapku a obsahuje pomerne prudký jed - kantaridín. Ide o ten istý jed, ktorým disponujú  aj chrobáky menom pľuzgierniky, známe skôr ako "španielske mušky". Jed  pľuzgiernikov je síce v povedomí verejnosti známy skôr ako mužské  afrodiziakum (vyvoláva prudkú a bolestivú erekciu), ale nie je vôbec  problém sa ním predávkovať, ako dokázal experimentátor a priekopník  osobitých praktík milovania, markíz de Sade. Španielskymi muškami  nechtiac otrávil dve svoje asistentky - prostitútky. Jedna kvapka májčej  hemolymfy vás síce nezabije, ale môže spustiť nepríjmené alergické  reakcie. Usmrtiť človeka dokáže vraj 30 miligramov suchého jedu, čo je  pravdepodobne pomerne veľké množstvo týchto nevábne vyzerajúcich  chrobákov. Kto by ich v takom množstve jedol, že? Iná vec je použiť  májky na odtránenie bohatých príbuzných, aby už konečne mohlo prebehnúť  vytúžené dedičské konanie. Neodporúčam, už to totiž jeden netrpezlivec  vyskúšal a skončil v base. Ďalší sa chcel zbaviť nervnej svokry a  skončil takisto. Obidvaja najprv kŕmili sliepku pľuzgiernikmi a májkami,  pričom kantaridín sa pomaly kumuloval v jej svalovine. Potom  predstrčili chutne pripravenú sliepku svojej obeti a potom už len s  potešením sledovali ich nekoordinované pohyby [1].  Vývoj májky je  pozoruhodný a značne komplikovaný. Z vajíčka sa vyliahne prvé larválne  štádium , tzv. triungulín (trojprstá larva). Volá sa tak preto,  lebo má pazúriky podobné trojzubcu bájneho Poseidona. Larvička sa  vyšplhá na kvet a prichytí  sa na chĺpky divej, menej často "domácej"  včely,  hľadajúcej potravu. Na nej sa odvezie do úľa. Tam sa najprv živí  včelími vajíčkami, neskôr - zmenená na beznohého červíka - medom.  Vyvinie sa do druhého larválneho štádia, ktoré sa po dvoch zvliekaniach  premení na tzv. pseudonymfu. Z nej sa vyvinie na nasledujúcu jar ďalšie  larválne štádium, ktoré sa už môže zakukliť. Nakoniec sa z kukly  vyliahne dospelý chrobák, ktorý opustí včelie hniezdo [2].    Larva májky (triungulín)  Na  záver by som rád upozornil, že larvy májky sa dostávajú do úľov naozaj  zriedkakedy, čo väčšina normálnych včelárov vie a preto ich zbytočne  nevraždí. Tu trocha odobočím a musím napísať, že skutočne nechápem niektorých  pseuodovčelárov, prečo zapchávajú nory včelárikom. Takých včelárov by som na  niekoľko dní zavesil za semenníky do rozpálenej pieskovne. A to, prosím,  nie som žiadny ekoterorista.   Literatúra: [1] Kůrka, A  &amp; Pfleger, V. 1984: Jedovatí živočichové. Academia, Praha.  [2]  Hanzák, J., Moucha, J., Zahradník, J. 1973: Světem zvířat. V. (2.)  Bezobratlí (Hmyz), Albatros, Praha.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Čejka 
                                        
                                            Ferdo Mravec očami entomológa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Čejka 
                                        
                                            Ráno vstanem a ochladím sa čajom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Čejka 
                                        
                                            S taxonómiou nemusí byť vždy nuda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Čejka 
                                        
                                            Všivavé príbuzenstvo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Čejka 
                                        
                                            Sex a drogy octomiliek
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Čejka
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Čejka
            
         
        cejka.blog.sme.sk (rss)
         
                                     
     
         Prírodospytec. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    27
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4423
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Pivo
                        
                     
                                     
                        
                            Ochrana prírody
                        
                     
                                     
                        
                            Jedlo
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Živočíchy
                        
                     
                                     
                        
                            Rastliny
                        
                     
                                     
                        
                            Stupava a okolie
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            O ničení pamiatok
                                     
                                                                             
                                            O jedle a súvislostiach
                                     
                                                                             
                                            Slovenské kalvárie
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Hebefíilia nie je pedofília. Ale je normálna?
                     
                                                         
                       Som hranol, smerák a škodca Slovenska
                     
                                                         
                       Prečo povedať nie ateizmu? A - čo mu vlastne povedať?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




