

   
   
   
 Nie je to dávno čo bola vo Švédsku kráľovská svadba. Pri tejto príležitosti si ľudia do okien spontánne vykladali kvetináče s kvetmi kvitnúcimi na žlto-modro (pre vlajky neznajúcich I.: švédska vlajka je modrá so žltým krížom), nikto ich k tomu nenútil zákonom, proste to urobil pretože si vážia svoju vlasť a svoju budúcu kráľovnú. 
 Prišlo mi to rovnako úprimné ako keď som bol vo Švajčiarsku a na 1. augusta (národný sviatok – vznik Švajčiarskej konfederácie) som uvidel niečo čo som ešte nikdy nevidel. 
 Na stole boli nakrájané paradajky (nie rajčiny), v ktorých boli vyrezané kríže a tieto kríže boli vyplnené uvareným vaječným bielkom (pre vlajky neznajúcich II.: švajčiarska vlajka je červená s bielym krížom). Možno trochu infantilné, ale úprimné a milé. Okrem toho, že skoro na každom sedliackom dvore je tento kríž vytiahnutý na nejakej palici. 
 No a zo skúseností z Francúzska viem, že 14. júla (výročie VFR, pre históriu neznalých I.: Veľká francúzska revolúcia - 1789) sa okrem nadmerného „žrania a pitia“ aj do okien vyvesili trikolóry a sledovalo defilé armády 5. FR (resp. RF). 
 Ja som slovenskú vlajku či znak videl len v učebnici, v škole na stene, na značke auta, vo volebnej miestnosti a pod zadným sklom auta mojich rodičov, ktorú tam dal môj brat (inak nevolič a dovolím si povedať, že žiadny vlastenec). Potom mám latentný pocit ostychu, že ma označia ľudia za SNS-áka, keď si požičiam ich auto a idem s ním „do sveta“. 
 No len toľko chcem povedať (napísať), že pokiaľ sa nejedná o šport, tak Slováci nie sú národne cítiaci a nezmení na tom nič, ani zákon z dielne korheľov zo Žiliny, ani zákon vypratých komunistov (dnes sociálnych demokratov) o vlastenectve. 
 Už len preto, že existencia 2. Slovenskej republiky je krátka (nie je ani dospelá). Ľudia si nestihli vybudovať k nej vzťah. No a na 1. Slovenskú republiku (pre históriu neznalých II.: Tisov štát) spomína v dobrom asi len pán ex-arcibiskup Sokol. 
 Proste ma len fascinuje, ako sa všetci búchame do pŕs, že Slovenskóóó, iba pri športe. Rád pripomeniem, že z toho plný tanier nebude... 
 Ja som asi zlý Slovák, ale na rozdiel od tých, čo sa bijú do pŕs, viem zaspievať slovenskú hymnu (aj keď falošne, ale slová viem). A som hrdý vtedy, keď v zahraničnej tlači vidím napísané niečo pozitívne (prepytujem športu sa netýkajúce) o mojej rodnej krajine (pre históriu neznalých III.: do 1918 časť Rakúsko-Uhorska, do 1992 časť Československa). 
 V konečnom dôsledku sú mnohí občania Slovenska maďarskej národnosti väčší vlastenci ako tí, ktorí sa za nich vyhlasujú... 
   
   
   
   

