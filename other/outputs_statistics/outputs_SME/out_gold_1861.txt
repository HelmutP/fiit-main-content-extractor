

  
 Poznáme dva základné princípy: 
 1. šachovanie s papiermi 
 2. magorenie voličov 
 Najprv preberieme šachovanie s papiermi.  
 A/ Zneplatňovanie lístkov.  
 Po otvorení urny si súdruh zoberie kôpku lístkov a spočítava hlasy, pričom si perom robí na pomocnom papieri čiarky. Občas, keď súdruh zazrie zjavne nepriateľský volebný lístok a práve sa naňho nikto nepozerá, dorobí na lístok o jeden - dva krúžky viac, čím ho zneplatní. Súčasne sa môže venovať aj dokrúžkovaniu hlasov: 
 B/ Dokrúžkovanie hlasov.  
 Na hlasovací lístok, kde volič nevyužil všetky možnosti zakrúžkovania, súdruh dorobí zostávajúci počet krúžkov pre kandidátov, ktorí garantujú jeho životnú úroveň. 
 C/ Zlé spočítanie hlasov. Po otvorení urny súdruhovia zavelia: „rozdeľme si to do dvojíc, budeme sa navzájom kontrolovať, pôjdeme rýchlejšie domov". A po starej známosti vytvoria dvojicu, ktorá má charakter leninskej bunky. Spočítajú čo chcú, pričom stavajú na tom, že nikto nebude dosť asertívny, aby to po nich prepočítal. Stačí keď sa „pomýlia" o pár hlasov, komunálne voľby často končia s rozdielom desiatok hlasov. Keby ich niekto prichytil, povedia, že to bol len omyl. Úmysel im nikto nedokáže, riskujú len hanbu. 
 D/ Sfalšovanie zápisnice. Súdruh si pomýli rubriky a v záverečnej zápisnici napíše číselko do iného riadka. Áno, aj také už bolo. A keď si to nevšimne samotná komisia, nikto nemá dôvod rozliepať archivované krabice s lístkami - to môže nariadiť až vyšetrovateľ, ale kde niet žalobcu... 
   
 Aké sú protiopatrenia? Môžeme ich skombinovať do jedného účinného balíka. V spolupráci s rozumnými členmi komisie sa ešte počas dňa dohodnite na nasledovnom postupe: 
 
 
 Svojpomocne vyrobíte papier, na 	ktorom bude jasne napísané, koľko krúžkov treba dávať, a 	umiestnite ho za plentu. Papier nesmie navádzať na voľbu 	konkrétneho kandidáta. 
 
 
 Člen komisie, ktorý vydáva 	lístky a obálky, vždy automaticky povie: „jeden krúžok na 	župana, päť krúžkov na poslancov". Týmito dvomi opatreniami 	výrazne znížite počet neplatných lístkov. 
 
 
 Cez deň budete od samej nudy 	zrátavať počet voličov, a nezávisle od toho počet vydaných 	obálok. Na konci bude treba napísať tieto čísla do zápisnice, 	takže to nie je zbytočná práca. Vďaka tomuto opatreniu ešte 	pred otvorením urny budete vedieť, koľko by malo byť vnútri 	obálok. 
 
 
 Keď konečne zakolete urnu a na 	stôl vysypete kôpku obálok, v prvom kroku len pootvárate všetky 	obálky a vytriedite neplatné hlasy. Prebieha to tak, že komisia 	sedí okolo stola ako rytieri kráľa Artuša, každý má pred 	sebou nahrabanú kôpku obálok, otvára ich, vyberá lístky a 	OKAMŽITE hlási: „Neplatný lístok!" (Priveľa krúžkov, 	žiadny krúžok, prázdna obálka, priveľa lístkov v obálke...). 	Nikto nemá dôvod držať v ruke pero! Týmto ste zabránili 	dodatočnému zneplatňovaniu hlasov. 
 
 
 Keď tento krok ukončíte, jeden 	môže odniesť a pečatiť použité obálky, ostatní idú 	spočítavať. Za každú cenu zabráňte tomu, aby sa rozliezli po 	miestnosti jednotlivci alebo dvojice a spočítavali hlasy bez vášho 	dohľadu. Pekne lístok po lístku, jeden číta mená, lístok drží 	tak, aby ostatní videli, a ostatní si robia čiarky. Na jeden 	volebný okrsok pripadá okolo 1000  voličov, volebná účasť sa 	odhaduje medzi 10 - 15%, takže tých 100-150 lístkov zvládnete 	za pol hodinky. Keď budú súdruhovia presadzovať prácu v 	skupinkách, trvajte na tom, že musia mať perá inej farby alebo 	ceruzky, aby nemohlo dôjsť k dokrúžkovaniu hlasov. A že si 	potom chcete osobne všetky kôpky skontrolovať. Takže zistia, že 	žiadny čas neušetria. Ako prinútite skupinu zohratých skúsených 	súdruhov, aby fungovali podľa vašich návrhov? Buď po dobrom, 	alebo hlasovaním členov komisie, alebo pod hrozbou, že 	NEPODPÍŠETE ZÁPISNICU (ťažký kaliber, len o číslo menší, 	ako telefonát na políciu a ústrednú volebnú komisiu). 
 
 
 Po skončení všetkých procedúr 	a podpísaní zápisnice si odfotografujte zápisnicu, a nahrajte si 	na USB kľúč súbor s výsledkami, ak v komisii použijete softvér 	štatistického úradu. Máte na to právo. 
 Teraz preberieme magorenie voličov.  
 
 
 Nebudeme sa venovať trápnostiam, ako pero s logom strany, alebo ukecávanie voličov študujúcich nástenku. 
 
 A/ Zneužívanie oslabených. Ak 	máte vo volebnom obvode nemocnicu, alebo domov dôchodcov, rozhodne 	choďte osobne s prenosnou urnou. So sebou by ste mali mať len 	toľko lístkov, koľko bolo vopred nahlásených imobilných 	voličov. Dohliadnite, aby voliči hlasovali samostatne. Zákon 	umožňuje, aby človeku neschopnému označiť lístok pomohla ním 	poverená osoba, takže citlivo odlišujte takéto prípady od „veľkých 	sestier", ktoré si zoberú na povel skupinu starčekov a 	stareniek. 
 B/ Kupovanie hlasov. Existuje 	niekoľko spôsobov kupovania voličských hlasov. Vy ako člen 	okrskovej komisie môžete urobiť len to, že ak sa o kupovaní 	hlasov dozviete, zavoláte na číslo vášho koordinátora, alebo 	priamo na políciu. Chráňte anonymitu nahlasujúceho, ak si to 	praje. Niektoré spôsoby kupovania hlasov stoja na vopred vyplnenom 	hlasovacom lístku - ak by bolo podozrenie na „reťazovú voľbu" 	(tzv. vláčik, popísané v manuáli SaS), presvedčte komisiu, aby 	ste dali za plenty kôpku prázdnych hlasovacích lístkov - zákon 	to priamo nezakazuje. Kupovači hlasov svoje obete vlastne urážajú 	a ponižujú, preto im vytvorte podmienky pre slobodnú voľbu. 
 
   
 Na záver trochu psychológie: OSTATNÍ ČLENOVIA KOMISIE SÚ PRIATELIA, NIE POTRAVA. Drvivá väčšina členov komisií tam ide v dobrom úmysle, potenciálnych falšovateľov je len zlomok. Konajte preto dôrazne, ale slušne. Pokiaľ hneď od začiatku dáte zreteľne najavo, že ste síce príjemný človek, ale za svoje poslanie považujete prísnu kontrolu, budete mať neskôr menej problémov. Počas dlhých hodín nudy nadhoďte tému falšovania volieb, a aké sú protiopatrenia. Je to jedna z foriem prevencie, prípadní súdruhovia si uvedomia, že vás rožkom neopijú. 
   
 Príjemnú sobotu, 
 Juraj Smatana 
 (Text, ktorý som spracoval pre potreby dobrovoľných členov okrskových komisií v okrese Považská Bystrica... viac písať nejdem, lebo sa na mňa vzťahuje volebné moratórium:-) 
   

