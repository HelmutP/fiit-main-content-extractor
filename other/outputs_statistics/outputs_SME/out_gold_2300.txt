

      Ide o mafiánsky podnik s gýčovou atmosférou, ale so zázračnou kuchyňou. Hmm... taký skvelý boršč ste nejedli, a ten špenátový pie, nemám slov. Chodia tam 2 typy ľudí, tzv. mačky a psy. Každý vie ako sa tieto dve stvorenia „milujú“, však? (česť výnimkám). No ja by som mohla byť tá mačka, no a pes je ten, ktorý vstúpil do tohto lokálu. Chlapík mohol mať do štyridsať rokov, prišiel tam so svojou milenkou a synom (predpokladám). Frajerský krok, v ústach žuvačka, reťaz zdobiaca jeho „skvelý“ hrudník a výraz najsilnejšieho kohúta v okolí. Muž si sadol chrbtom ku mne, takže ma aj „obšťastnil“ obrazom americkej vlajky, ktorú mal na zadnej strane svojho koženého kabáta (hm.. bunda, niečo také). Prišla obsluha a „Dunčo“ si začal objednávať, natrčila som ucho, aby som si ho na 100% mohla zaradiť do kategórie „psy“. No, a môžem potvrdiť, že bol v prvej línii. Pri vychutnávaní tuniakového šalátu som sledovala priebeh konverzácie s jeho "svorkou". 
   
      Poviem Vám, nebolo to až tak zábavné, skôr smutné. Myslím, že tieto typy ľudí nie sú nebezpeční, sú len nevyrovnaní, ustráchaní a bez životnej filozofie.  
      Každopádne mu prajem, aby našiel správny smer  a ošúchanú  masku čím skôr zahodil. 
   
   

