
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kunder
                                        &gt;
                Občianska spoločnosť
                     
                 Sudcovia a porno 

        
            
                                    25.3.2010
            o
            16:35
                        |
            Karma článku:
                13.28
            |
            Prečítané 
            3169-krát
                    
         
     
         
             

                 
                    Harabinovi sa podaril pekný kúsok. Dostal sa na titulky všetkých novín v pozícii bojovníka proti nemorálnym sudcom, ktorí možno svoje perverzné chúťky ukájajú dokonca v pracovnom čase. Nuž...
                 

                 Najprv treba vypichnúť, že spomínané opatrenie sa týka len sudcov Najvyššieho súdu.   "Bežní" sudcovia totiž nemajú prístup k internetu (aspoň pokiaľ viem) vôbec. Napríklad na Krajskom súde v Bratislave sa na internet dostane predsedníčka súdu (nemýliť si s predsedom senátu), a ešte snáď niekto zodpovedný za správu súdov. Dokopy pár osôb. Podstatné je, že bežní sudcovia nie.   Tí môžu používať INTRANET (pre laikov zjednodušene: dostanú sa v podstate iba na stránky ministerstva spravodlivosti). Tak sa dostanú k rozsudkom (slovenských a niekoľkých európskych súdov), k rozhodnutiam rôznych inštitúcií, a pod. Nemôžu však navštíviť gmail, či sme.sk, atď. Správy si môžu prečítať len vo forme monitoringu (či rešerše) pripravenom ministerstvom spravodlivosti. Prečo?   Potrebuje sudca internet menej ako úradník?   Chceme mať kvalifikovanych sudcov - preto je dôležitá otázka - kde sa majú zorientovať v témach, ktorých pochopenia sa týka kvality ich rozhodovania? Kde má nájst informácie sudca, ktorý má súdiť napr. spory o autorských právach na webe, hackerov, alebo podvody typu phishing? Je vhodné, aby taký sudca vôbec tušil, čo je server, prenos dát, aby mal o veciach základnú predstavu - napr. aby bol schopný aspoň rámcovo pochopiť znalecký posudok?   Zmysluplné informácie sa o tom na webe dajú nájsť takmer okamžite - pričom v papierovej podobe na to treba dosť času a stojí to podstatne viac. Podobných tém, kde by bolo vhodné, aby si sudca dokázal urobiť aspoň základný prehľad, by sa pre každý typ senátu dalo nájsť viacero.   Základnou otázkou je ale niečo iné. Ako spoločnosť nemáme zásadný problém dôverovať bežnému úradníkovi, že možnosť prístupu na internet nezneužije. Veríme, že prínos jeho prístupu na internet bude väčší ako negatíva, ktoré to prinesie. Presnejšie - aspoň vláda tomu verí - pretože každý rok do toho investuje obrovské peniaze.   Sudcom spoločnosť dala dôveru, že dokážu zodpovedne posúdiť, kto z rodičov bude vhodnejší pre výchovu detí, koľko času má niekto stráviť vo väzení, prípadne kedy je v poriadku, keď sa dlžník ocitne deložovaný na ulici. Schizofréniou zaváňa fakt, že človeku, ktorému spoločnosť dôveruje natoľko, že mu dovolí rozhodovať o ďalšom živote dieťaťa, jeho nadriadený apriori neverí, že dokáže v pracovnej dobe pracovať.   Medzinárodná hanba?   Sudca má v ústave v mnohých oblastiach silnejšie postavenie ako člen vlády. Snaha blokovať nejakému ministrovi prístup na "chúlostivé" weby by sa asi stretla s veľkým pobavením a zrejme aj medzinárodnou hanbou. Ťažko predpokladať, že Harabinove opatrenie pridá medzinárodnú prestíž slovenskej justícii.   Harabin viackrát tvrdil, že médiá vytrhávajú z kontextu veci týkajúce sa jeho osoby, či inštitúcie, ktorú vedie. Ako je mi známe, negatívne správy o stave súdnictva neraz dal na roveň poškodzovaniu dobrého mena justície, a to aj vtedy, keď nebolo preukázané, že tieto informácie sú nepravdivé. Za jeho ministrovania sudcovia, ktorí na "špinu" v súdnictve poukazovali (napr. aj svedectvom "pod prísahou" na súde - ako to prikazoval zákon), boli dokonca trestaní.   Tentoraz by som uvítal ja, aby Harabin informáciu zasadil do širšieho kontextu a povedal (a doložil), koľkí sudcovia (pred jeho opatrením) prístup k pornu v pracovnom čase využívali, hoci neriešili prípad, ktorý by s tým priamo súvisel. Tiež by ma zaujala informácia, koľko pracovného času na pornostránkach strávili. Tiež by ma zaujímali technické parametre servera. Za tie peniaze, čo sa naliali na Najvyššom súde do IT by som totiž neočakával, že pri sťahovaní nejakého videa sa citeľne spomalí prístup ostatných počítačov k sieti. Rád by som pochopil aj to, prečo sa prístup na web neobmedzil len tým sudcom, ktorí (údajne) porno sťahovali a prečo im nenavrhol disciplinárne stíhanie. Ak ostatným sudcom obmedzili prístup "preventívne", chcel by som tiež vedieť, či napríklad Harabin má "preventívne" na služobnom telefóne zablokované volania na platené audiotextové čísla - predpokladám, že na ne prístup nepotrebuje, takže nie je dôvod, aby ho mal...   Viac by ma ale zaujímalo, čo robí v pracovnom čase on a aký výkon ako sudca podal. Teda koľko súdnych rozhodnutí v priebehu polroka urobil, koľko strán spisov musel preštudovať a prípadne koľko pojednávaní vytýčil. Snažil som sa na webe čosi nájsť, ale nebol som úspešný - a nerád by som bol nespravodlivý.   Predovšetkým by som rád počul vysvetlenie, ako sa stalo, že počas jeho ministerskej kariéry, v jeho mene odpovedala nejaká pracovníčka ministerstva na maily zaslané členom Súdnej rady. Ak by zaznelo rozumné vysvetlenie, tak by som sa možno začal čudovať, ako nejakým sudcom mohlo napadnúť, že sa im niekto môže chcieť hrabať v mailoch.   Ak máte pocit, že tento článok by nemal hneď zapadnúť prachom, môžete naň iných upozorniť na:  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kunder 
                                        
                                            Chcete znova platiť za online kataster? Možno budete.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kunder 
                                        
                                            Test: Vyhrali by ste tender nad hayekovcami?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kunder 
                                        
                                            Vraj mi niekto klientelisticky pridelil byt
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kunder 
                                        
                                            6 mýtov o hayekovskej zákazke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kunder 
                                        
                                            Diplomati a zastrašení sudcovia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kunder
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kunder
            
         
        kunder.blog.sme.sk (rss)
         
                        VIP
                             
     
        Muž v strednom veku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4195
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Občianska spoločnosť
                        
                     
                                     
                        
                            Pár vetami
                        
                     
                                     
                        
                            Rača - občiansky watchdog
                        
                     
                                     
                        
                            Tlačový zákon
                        
                     
                                     
                        
                            Financovanie v politike
                        
                     
                                     
                        
                            Tendre a pod.
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Register otvorených politikov
                                     
                                                                             
                                            Slnkorecords - príjemná muzika
                                     
                                                                             
                                            Nooby - web, ktorý aj pomáha
                                     
                                                                             
                                            Citáty slovenských politikov
                                     
                                                                             
                                            Red Meat - hlavne Earl
                                     
                                                                             
                                            Občianske noviny v Rači
                                     
                                                                             
                                            Aliancia Fair-play
                                     
                                                                             
                                            Hedviga Malinová
                                     
                                                                             
                                            Shooty
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




