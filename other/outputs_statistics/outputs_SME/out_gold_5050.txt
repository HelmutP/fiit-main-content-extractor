

 Pred rokom k nám dorazila obuv, ktorá "vytlačila" z pultov športové flip-flopky(žabky), či nie veľmi vkusné sandále. Tou obuvou boli práve, už spomínané, gladiátorky. Pohodlná športovo-elegantná obuv, ktorú si môžete obliecť skutočne k čomukoľvek a stále nestratíte na glamoure, sa stala skutočným hitom a našla si  fanušíkov v každej vekovej kategórii. Jediné, čo by som im vytkla je, že to boli topánky tak trošku "pritiahnute za vrkoč", bez akejkoľvek väčšej fantázie a zmyslu pre detail. To, sa však nedá povedať o kolekciách pre rok 2010. 
 Gladiátorky 2010 
 Chloé, Marc Jacobs, Christian Louboutin, Miu miu a mnoho ďalších dizajnérov to tento rok stavilo práve na tento druh obuvi. Od vysokých gladiátoriek, končiacich tesne nad kolenom až po klasiku, ktorú už poznáte z roku 2009. Samozrejme všetko v metalických, zemitých alebo krásne pastelových farbách, v ktorých nádherne vynikne vaša opálená pokožka. No, čo sa mne osobne páči úplne najviac, sú krásne detaily. Malé mašličky, cvoky, korále alebo moderné strapce, to všetko vám ponúka tohtoročný trend, trend 2010, božské gladiátorky. 
  
 Zdroj: www.google.com/pictures 
 www.polyvore.com/create 

