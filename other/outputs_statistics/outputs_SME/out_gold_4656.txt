

 Dostal som dnes v odkazoch otázku, prečo som ešte nebol na hrade Modrý Kameň? Za čerstva ten dojem naprávam, boli sme na hrade v Modrom Kameni, ale je pravda, že iba tak narýchlo, idúc naokolo cestou, domov z vlaňajšej dovolenky. Je to naozaj pekné, pokojné a veľmi príjemné miesto a mesto tiež. 
 Až ku hradu sa dá vyjsť autom, možno navštíviť múzeum v kaštieli a starý hrad, alebo sa vybrať do prírodnej rezervácie Modrokamenská lesostep, či prejsť sa kalváriou až ku krížu. Na kalváriu vás môže sprevádzať rozprávanie a fotodokumentácia z blogu Miroslava Lisinoviča. 
 Tu sú teda kúsky mojich spomienok na Modrý Kameň. 
  
 Ku hradu vedie strmá cesta. 
  
 Vchod do kaštieľa, dnes je tam múzeum bábkarských kultúr a hračiek. My sme nemali čas na prehliadku. 
  
 Vstupujeme na hrad cez bránu kaštieľa. 
  
 Pohľad na časť objektu kaštieľa z horného hradu. 
  
 Schody na horný hrad. 
  
 Pôvodný gotický hrad Modrý Kameň postavili po tatárskom vpáde v roku 1271 - 1277. Patril šľachtickej rodine Balašovcov.  V roku 1576 ho Turci dobyli a pri ústupe, v roku 1593 ho zničili.  V rokoch 1609 - 1612 ho obnovili, ale v roku 1683 bol opäť zničený. Na základoch dolného hradu bol v roku 1730 postavený rozsiahly barokový kaštieľ, ktorý stojí dodnes. 
  
 Hradné siluety ruín sú poznačená časom, 
  
 zeleným zdobením, 
  
 životom prispôsobeným daným podmienkam... 
  
 Modrý Kameň je utopený v zeleni stromov. 
  
 Mesto s najmenším počtom obyvateľov na Slovensku. 
  
 Stromy a lesy... 
  
 Konečný cieľ Modrokamenskej Kalvárie. 
  
 Hradná vybavenosť - schodište... 
  
 Májové hradné zátišie 
  
 a makový detail na záver. 
   
 Venované Anežke s rodinou.... 
   

