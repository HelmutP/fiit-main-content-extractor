

 
Milujem tresku v majonéze, preto som ju chcel skúsiť spraviť doma. Po zháňaní receptov som zistil, že podstata je vlastne rovnaká, ale každý recept sa od seba niečim líšil. Mojim cieľom bolo pripraviť aspoň takú chuť a konzistenciu akú mám rád, akú som si bežne kupovával. No nechcite vidieť ako vyzerala a chutila moja prvá, po domácky vyrobená treska. Ble... Okrem toho, že sa mi oči krížili z kyslosti octu, vôbec nevyzerala ako treska v majonéze, ale ako farebná zmes niečoho, čo smrdelo rybinou. Trvalo mi takmer rok, než som sa dopracoval k tomuto receptu. Prečo rok? Je pochopiteľné, že keď máte zjesť vyše kila tresky v majonéze, ktorá nechutí ako tá "pravá", tak jej máte na niekoľko týždňov vopred viac než dosť. 
 
 
Budeme potrebovať: 
 
Na uvarenie ryby: 
- 1 kg ryby (mrazené filety z tresky) 
- 250 ml 8% octu 
- 3 bobkové listy 
- 1 KL celého čierneho korenia 
- 1 KL soli 
 
 
Ostatné suroviny: 
- 100 ml 8% octu 
- 1 KL soli 
- 100 g jemne nakrájanej cibule (2 menšie alebo jedna väčšia) 
- 100 g jemne nastrúhanej mrkvy (2 menšie alebo jedna väčšia) 
- 1 KL čierneho mletého korenia 
- 1 PL plnotučnej horčice 
- 250 g strúhanej cukety  
- 3 stredne veľké jemne nastrúhané kyslé uhorky 
- veľa majonézy (domácej alebo kupovanej, také to 500ml balenie, ale je to na Vás koľko jej tam dáte ide vlastne o konečnú konzistenciu) 
 
 
Postup: 
Do vriacej vody pridáme 250 ml octu, bobkové listy, celé čierne korenie a rybu. Varíme kým ryba nie je hotová (cca 20-30 minút podľa typu použitej ryby).  
Trochou tejto horúcej vody zalejeme najemno nastrúhanú cuketu aby zmäkla (gratinujeme tak 5 minút - teda necháme ju v tej horúcej vode a potom hneď vyberieme a scedíme). Rybu odstavíme a necháme vo vode vychladiť. 
 
Po vychladnutí rybu vyberieme, rozstrapkáme ju na čo najmenšie kúsky, pridáme cibuľu, uhorku, mrkvu, cuketu, soľ, mleté čierne korenie, 100 ml octu, majonézu a horčicu.  
 
Dobre premiešame a dáme aspoň na 12 hodín odležať do chladničky.
 
 

PS: Pridávam ešte jeden tip na domácu majonézu z vareného vajíčka. Dlhšie Vám vydrží pocit, že tá treska je čerstvá.
 
 

Na majonézu z vareného vajíčka potrebujeme: 
- 1 natvrdo uvarený bielok 
- 0,7 dcl vlažného mlieka (tohoto môžete aj menej, podľa veľkosti vajíčka) 
- kávová lyžička kremžskej horčice 
- olej 
 
Postup: 
Bielok poriadne vymixujeme s vlažným mliekom. Najlepšie, keď si bielok
najprv rozotrieme aby výsledok nebol hrudkatý. Potom pridáme kremžskú horčicu a postupne za
stáleho miešania pridávame olej až dosiahneme požadovanú hustotu
majonézy. Ak chceme mať majonézu žltšiu, votrieme nakoniec žĺtok, ktorý nám ostal. 
 
 
A neostáva mi nič iné, len popriať dobrú chuť, priatelia.
 
 

