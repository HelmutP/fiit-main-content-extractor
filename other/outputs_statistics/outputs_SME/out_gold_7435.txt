

 ((preukaz)) 
 Postup: 
 1. Zistite si telefónne číslo a otváraciu dobu Vášho mestského, alebo obecného úradu. 
 Na tejto stránke stačí vybrať okres a potom obec: kontaktne informacie uradov 
   
 2. Dohodnite sa s nimi, že mi pošlete mailom, alebo faxom žiadosť. 
   
 3. Prepíšte túto žiadosť podľa svojich údajov:  ziadost_o_volicsky_preukaz.doc 
   
 4. Vytlačte si ju, podpíšte a pošlite faxom, alebo oskenujete a pošlite mailom. 
   
 5. Zistite si kontakt na OÚ, alebo MÚ kde chcete ísť voliť, zavolajte tam, a zistite si 
 kedy a kde budú voľby. 
   
 6. Občas sa pozrite do schránky :) 
   
 7. Chodte voliť !!! (vezmite si občiansky a ten A5 papier - preukaz) 
   

