

 An-124 Ruslan, v kóde NATO označovaný „Condor", je ťažké dopravné lietadlo majúce na konte radu svetových rekordov vo svojej kategórii. Uvádza sa, že je momentálne tretím najväčším dopravným lietadlom na svete, hneď po svojom „väčšom bratovi" An-225 Mria a Airbuse A380F. Bolo skonštruované v časoch studenej vojny pre účely vojenskej strategickej prepravy i civilnej prepravy nadrozmerných nákladov. Za jeho americký náprotivok je považovaný stroj Lockheed C-5 Galaxy, ktorý je však o niečo menší a unesie o 25% nákladu menej. 
  (Foto: An-124 Ruslan. Zdroj: http://news.kievukraine.info/)  
  (Foto: An-124 patriaci spoločnosti Volga-Dnepr - najväčšiemu prevádzkovateľovi týchto strojov. Zdroj: wikipedia) 
 Prvý let tohto výnimočného lietadla sa uskutočnil 26. decembra 1982 a do služby bolo po prvý krát zaradené v roku 1986. Civilná certifikácia mu bola udelená Medzištátnym leteckým výborom v roku 1992. Komerčná varianta, ktorá uniesla náklad o maximálnej hmotnosti 120 ton so šiestimi členmi posádky a životnosťou 24.000 letových hodín, dostala názov An-124-100. Produkcia prebiehala súčasne na dvoch miestach - v továrňach v ruskom Uljanovsku a v Kyjeve na Ukrajine. Ukončená bola v roku 1995 po vyrobení 57 kusov pre problémy vyplývajúce z krachu Sovietskeho zväzu. Z tohto vyrobeného množstva pripadlo ruskej armáde 26 kusov a zbytok bol odobraný civilnými dopravnými spoločnosťami. 
   
 Návrat po takmer dvadsiatich rokoch 
 Prvý náznak obnovenia sériovej produkcie strojov An-124 prebehol v roku 2003 na výstave MAKS po rokovaniach ruských a ukrajinských predstaviteľov. V ďalšom roku už bola sformulovaná pracovná skupina pozostávajúca zo zainteresovaných strán, poverená vytvorením podnikateľského plánu. Na obnove výroby obrích transportných lietadiel malo záujem, ako som už skôr naznačil, ministerstvo obrany RF a tiež civilní dopravcovia. Tu za všetkých spomeniem predovšetkým spoločnosť Volga-Dnepr, ktorá je v súčasnosti najväčším komerčným užívateľom strojov An-124-100 (vo svojom letovom parku ich má desať) a aktívne sa podieľa na celom procese obnovy výroby a modernizácie. Podľa expertov z tejto spoločnosti je znovuotvorenie výroby An-124 veľmi žiaduce, pretože svetový dopyt v oblasti prepravy ťažkých a nadrozmerných nákladov neustále rastie a to rýchlejšie ako ponuka týchto služieb. Ich slová potvrdzuje aj prezident RF Dimitrij Medvedev, ktorý sa na margo nového spustenia sériovej výroby vyjadril takto: „Existuje medzera na trhu veľkých nákladných prepravcov. Ak sa nám nepodarí chopiť sa tejto príležitosti, prídu iní." Ruskí dopravcovia sú lídrami v tomto segmente, ovládajú až 75-percentný podiel trhu. 
 Ako bolo oznámené v roku 2004, Rusko a Ukrajina sa mali spolupodieľať na výrobe, ktorá sa plánovala medzi rokmi 2006 - 2020 a ktorej výsledkom malo byť 80 nových strojov An-124-100M (modernizovaný). Tento zámer bol ale odložený, nie však zamietnutý. Po štyroch ďalších rokoch riešení problémov technickej, ekonomickej či politickej povahy sa vec opäť pohla dopredu v roku 2008. V máji bolo oznámené, že dohoda o reštarte výroby ďalej modernizovanej verzie An-124-100M-150 je už blízko a už sa len dolaďujú posledné detaily. V tomto roku bola nakoniec uzatvorená i predbežná dohoda. 
 Pri tejto príležitosti ešte spomeniem, že pre potreby modernizácie lietadiel na verziu An-124-100M-150 bola naviazaná spolupráca aj zo „Západnými" firmami. Výsledkom bolo zvýšenie maximálnej hmotnosti nákladu z 120 na 150 ton, zníženie počtu členov posádky zo šesť na štyri osoby, zvýšený dolet, modernizácia avioniky a brzdového sytému, urýchlenie a zjednodušenie systému nakladania a vykladania atď. Životnosť bola predĺžená na 50.000 letových hodín. 
 V súčasnosti už intenzívne prebiehajú prípravné práce na sériovú výrobu. Tá by sa mala podľa posledných predpokladov rozbehnúť v roku 2012, pričom cena by sa mala pohybovať okolo 200 miliónov USD za kus. Dodávky prvých strojov už očakávajú ruské ozbrojené sily, ktoré plánujú zaviesť do služby do roku 2020 približne dvadsať týchto dopravných lietadiel, ako aj spoločnosti z civilnej sféry. Ako príklad tu opäť uvediem spoločnosť Volg-Dnepr, ktorá v júli 2008 zadala objednávku na 40 lietadiel, ktoré by mali byť dodané postupne do roku 2027. Náklady na spustenie sériovej výroby sa odhadujú na pol miliardy USD. 
  (Foto: An-124 pri vykládke bojových vrtuľníkov Apache na letisku vo Victorville. Zdroj: wikipedia) 
   
 Ponuka pre Ameriku 
 Pre zákazníkov v USA určite nie je meno „Ruslan" neznáme. Výskum globálneho trhu prepravy nadrozmerných nákladov spred niekoľkých rokov odhalil, že na Severnú Ameriku pripadá celých 52% dopravy týchto nákladov. Z toho podiel USA činí približne 40%. Veľa z týchto prepráv sa uskutočňuje za pomoci ťažkých transportných lietadiel An-124 a preto zaujímavo vyznel návrh ruskej strany z konca roku 2009 zmieňujúci spoločnú spoluprácu pri výrobe, predaji a následnom servise týchto strojov. 
 Ruský vicepremiér, bývalý minister obrany Sergej Ivanov sa v máji 2010 vo Washingtone vyslovil pred novinármi takto: „Diskutovali sme o úplnom projekte, čo zahŕňa spoločnú výrobu lietadla, zriadenie spoločného podniku, zdieľanie práv, predaj ruským a americkým zákazníkom - civilným aj vojenským - a vytvorenie systému po-predajného servisu." 
 Lietadlá typu An-124 Ruslan plnia na území USA pomerne veľa úloh, spomeniem tu napr. uzatvorenie dohody medzi Boeingom a spoločnosťou Volga-Dnepr týkajúcej sa prepravy nadrozmerných nákladov pre závod Everett; transport častí kozmických nosičov Atlas V zo zariadení firmy United Launch Alliance v blízkosti Denveru na Cape Canaveral; prepravu satelitov z Palo Alto (Kalifornia) na kozmodróm v Kourou (Francúzska Guyana) atď. Zapojenia sa Spojených štátov amerických do tohto projektu by tak mohlo mať svoje opodstatnenie. 
 Americká strana sa k možnému spoločnému projektu ešte nevyjadrila, uvádza sa, že momentálne zvažuje klady a zápory tohto návrhu a jej stanovisko tak ešte nie je jasné. 
  (Foto: An-124 vo farbách spoločnosti Volga-Dnepr. Zdroj: www.air-and-space.com) 
   
 Program SALIS 
 Možnosť využívať letúny An-124-100 na prepravu ťažkých nákladov má v súčasnosti aj Slovensko. Táto skutočnosť vyplýva zo zmluvy uzatvorenej 17.1.2006 medzi agentúrou NAMSA (Agentúra NATO pre údržbu a zásobovanie - NATO Maintenance and Support Agency) zastupujúcou záujmy NATO a spoločnosťou Ruslan Salis, ktorá je spoločným podnikom dvoch civilných dopravcov vlastniacich typ An-124 Ruslan, konkrétne Volga-Dnepr (Rusko) a Antonov Airlines (Ukrajina). 
 Podľa tejto zmluvy, ktorá na strane NATO združuje šestnásť jeho členov (Belgicko, Maďarsko, Grécko, Dánsko, Kanada, Luxembursko, Holandsko, Nórsko, Veľká Británia, Poľsko, Portugalsko, Slovensko, Slovinsko, Francúzsko, Nemecko, Česko) plus dvoch nečlenov (Švédsko, Fínsko), majú tieto spomenuté štáty možnosť využívať dopravné služby lietadiel An-124 pomerne k svojmu príspevku. Spoločnosť Ruslan Salis je povinná trvale držať dva svoje stroje An-124-100 na letisku Leipzig-Halle v Nemecku pripravené k okamžitému použitiu a ďalšie štyri „antonovy" mať v rezerve v Rusku alebo na Ukrajine, ktoré v prípade potreby poskytne na požiadanie. 
 Zmluva bola najskôr podpísaná na tri roky s tým, že po ich uplynutí bude môcť byť naďalej predlžovaná na obdobie jedného roku. K prvému predĺženiu došlo v roku 2009 a tak bude platiť minimálne do 31.12.2010. Hlavnými iniciátormi tejto dohody boli členovia NATO zainteresovaní v programe na vývoj dopravného lietadla Airbus A400M, ktorí naberá meškanie. Prenájom strojov An-124 im takto umožní preklenúť obdobie do zaradenia A-400M do operačného použitia. 
  (Foto: An-124 na letisku v Poprade pripravený na nakládku vrtuľníkov Mi-17. Zdroj: www.vrtulniky.sk) 
  (Foto: An-124 na letisku Poprad. Zdroj: www.vrtulniky.sk) 
   
 Porovnanie najväčších dopravných lietadiel sveta 
   
   
   
 
 



Typ
An-225
A380F
An-124
B747 LCF
MD-11F
Il-96T


Umiestnenie
1.
2.
3.
4.
5.
6.


Dĺžka
84 m
72,73 m
68,96 m
71,68 m
58,6 m
63,9 m


Výška
18,1 m
24,5 m
20,78 m
21,54 m
17,6 m
17,5 m


Rozpätie krídel
88,4 m
79,75 m
73,3 m
64,4 m
51,7 m
60,11 m


Max. náklad
250 t
150 t
150 t
146 t
92 t
92 t


Max. vzletová hmotnosť
640 t
590 t
405 t
364 t
280 t
270 t


Max. rýchlosť
850 km/h
1020 km/h
865 km/h
??? km/h
945 km/h
900 km/h


Cestovná rýchlosť
800 km/h
945 km/h
830 km/h
878 km/h
876 km/h
860 km/h


Počet motorov
6
4
4
4
3
4




 1. Antonov An-225 2. Airbus A380F 3. Antonov An-124 4. Boeing 747 Large Cargo Freighter 5. McDonnell Douglas MD-11F 6. Ilyushin IL-96T 7. Antonov  An-22 8. Ilyushin IL-76 MF/TF 9. Airbus A300-600 Super Transporter 10. Antonov An-70 
  (Foto: Antonov An-225 Mria.) 
  (Foto: Airbus A380F. Zdroj: www.flightglobal.com) 
  (Foto: Boeing 747 Large Cargo Freighter. Zdroj: www.techfresh.net) 
  (Foto: McDonnell Douglas MD-11F. Zdroj: www.flickr.com) 
  (Foto: Iljušin Il-96T. Zdroj: www.flight-insight.com) 
   
 Zdroje článku: 
 http://www.defenseindustrydaily.com/natos-an124s-a-russian-solution-to-the-airlift-problem-and-more-02128/ 
 http://en.rian.ru/russia/20100518/159058236.html 
 http://www.aerospace-technology.com/projects/antonov/ 
 http://en.rian.ru/russia/20091228/157402215.html 
 http://www.global-military.com/russia-said-it-would-restart-the-an-124-large-transport-aircraft-production-line.html 
 http://www.globalsecurity.org/military/world/russia/an-124.htm 
 http://en.rian.ru/russia/20100526/159163128.html 
 http://www.defenseindustrydaily.com/more-an124s-on-the-way-antonov-signs-agreement-with-key-customers-02913/ 
 http://www.volga-dnepr.com/ 
 http://www.arabiansupplychain.com/article-2520-10-of-the-worlds-biggest-cargo-planes/ 
 wikipedia 

