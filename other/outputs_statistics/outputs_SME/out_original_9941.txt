
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Malovcová
                                        &gt;
                Nezaradené
                     
                 Statny sviatok 

        
            
                                    6.7.2010
            o
            10:23
                        (upravené
                6.7.2010
                o
                13:20)
                        |
            Karma článku:
                0.31
            |
            Prečítané 
            258-krát
                    
         
     
         
             

                 
                    Občas mám pocit, akoby čas letel takou rýchlosťou, že už ani sama neviem, aký je dnes deň, a tobôž už ani netuším, kto má vlastne koľko rokov. Neviem, či je to tým vekom, alebo dobou v ktorej žijeme, ale mám pocit, akoby som pracovala stále viac a viac a akoby mi na všetko podstatné ostávalo čoraz menej času.
                 

                 Včera ráno som vstala tak ako každý iný deň hneď ako mi zazvonil budík. Vstala som z postele, obliekla sa, pozrela si maily, a prekvapilo ma, že neprišiel ani jediný, bolo to zvláštne, pretože bežne po víkende mám hneď v pondelok zrána plnú schránku nových mailov a dnes ani jediný. Povedala som si nevadí, konečne budem mať kľudnejší deň.   Naraňajkovala som sa a nasadla som do auta. Mala som v pláne navštíviť  klientov z okolia Žiliny a Martina. Prvé čo ma zaujalo keď som nasadla do auta a prchádzala cez uličky mesta boli sviatočne obečení ľudia, a veľké množstvo ľudí pred kostolom v meste. Zdalo sa mi to čudné, tak som radšej vzala do ruky mobil a pozrela som sa do kalendára. Ukazovalo mi pondelok 5.júla 2010, bola som spokojná. Pravdupovediac som sa vyľakala, že je nedeľa, a ja som si pomýlila dni.   Ďalšie prekvapenie ma čakalo na diaľnici. Nikde žiadne kamióny, občas dáke to súkromné autíčko. Zapla som si rádio a počúvala som hudbu. Ako začali správy zazvonil mi mobil. Bola to moja nevesta. Napadlo ma, že asi bude chcieť večer prísť a bude chcieť, aby sme niečo spolu navarili. Chytila som telefón a ona sa ma spýtala, či ma nezobudila, vravím, že nie, že už som v aute na ceste za klientom. Spýtala sa ma, že prečo práve dnes, že čo neoddychujem. Vravím jej, veď je pondelok, a ona nato, že áno pondelok, ale je štátny sviatok, dnes nikto nepracuje.        V tom momente som stupila na brzdu, a vôbec som sa nepozrela do zrkadla, či niekto za mnou nejde. Mala som šťastie, že som bola jediná široko ďaleko. V rýchlosti viac ako 130 km/hod stupiť prudko na brzdu a zničoho-nič, no neviem keby je niekto za mnou, ako by to bolo dopadlo. Bola som už v Považskej Bystrici na novom diaľničnom moste, ktorý vedie ponad mesto, poviem Vám veľmi som sa tešila, kedy konečne po prvý krát týmto úsekom prejdem, ale bola som sklamaná, pretože som nič nevidela, len holé piliere a žiadny výhľad na mesto. Ale povedala som si, nevadí, dnes je voľno, a ja si môžem sama zato, že som nepozorná, a vôbec som si neuvedomila, že je voľno. Keď už som prešla toľko kilometrov, povedala som si, že ešte pár tých kilometrov prejdem, a doprajem si pôžitok zo zmrzliny akú majú len v  Bytči. Je naozaj výborná a nedopriať si ju v týchto horúcich dňoch by bolo hriechom.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Malovcová 
                                        
                                            Lietať sa skutočne oplatí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Malovcová 
                                        
                                            Vždy sa oplatí hľadať!Aj pri kúpe letenky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Malovcová 
                                        
                                            Skutočne sa blíži koniec sveta? Druhá časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Malovcová 
                                        
                                            Skutočne sa blíži koniec sveta?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Malovcová 
                                        
                                            Nový spôsob demokracie?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Malovcová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Malovcová
            
         
        malovcova.blog.sme.sk (rss)
         
                                     
     
        Obycajna zena ktora rada cita knihy rozneho zanru a momentalne vyplna volny cas pisanim blogu
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




