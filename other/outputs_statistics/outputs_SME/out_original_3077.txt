
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Kamil Krnáč
                                        &gt;
                Spoločnosť
                     
                 Oznámili by ste korupčný prípad na polícii? 

        
            
                                    23.3.2010
            o
            8:36
                        (upravené
                22.3.2010
                o
                22:49)
                        |
            Karma článku:
                9.51
            |
            Prečítané 
            1730-krát
                    
         
     
         
             

                 
                    Mnoho ľudí sa dožaduje prísnejších trestov za korupciu. Aj ten najprísnejší zákon na svete si ale rýchlo vyláme svoje zuby na neochote ľudí spolupodieľať sa na jeho dodržiavaní. Hovorí sa, že kde niet žalobcu, nie je ani sudcu. Ak by sme v tomto duchu nazerali na otázku krádeží, nikdy by nikto nebol za krádež potrestaný, pokiaľ by dolapenie a odsúdenie zlodeja bolo okradnutým obetiam úplne ľahostajné.
                 

                 Tu je aj ukrytý dôvod, prečo korupcia na Slovensku tak prekvitá. Vo vzťahu korumpujúci-korumpovaný totiž po technickej stránke zdanlivo nie je obeťou ani jeden z účastníkov. Poškodenou stranou tohto kvázi-obchodného vzťahu je v skutočnosti celá spoločnosť, u ktorej typický pocit rozhnevaného okradnutého človeka domáhajúceho sa spravodlivosti pochopiteľne absentuje.        Podľa prieskumov organizácie Transparency International Slovensko by iba 6% ľudí na Slovensku nahlásilo prípad korupcie polícii určite. Asi by ho nahlásilo ďalších 16% opýtaných, nerozhodne by sa k tejto možnosti postavilo 15% populácie a nahlásiť prípad korupcie by pravdepodobne alebo určite odmietlo spolu až 63% respondentov. Toto nepriaznivé zistenie vyznieva ešte závažnejšie, ak ho dáme do súvisu s výsledkami Eurobarometra, podľa ktorých korupciu na Slovensku vníma ako veľký problém až 83% opýtaných.   Jedným z možných dôvodov, prečo podľa prieskumov korupciu vidíme ako zlo, ale zároveň nie sme v boji proti nej ochotní spolupracovať s orgánmi činnými v trestnom konaní, môže byť strach. Obava, že vystavením kože na trhu nám namiesto spoločenského uznania vstúpia do osobného a pracovného života nepríjemnosti a komplikácie, staneme sa terčom zastrašovania, vydierania a posmechu. To všetko s rizikom, že páchateľ korupcie bude po dlhoročných súdnych prieťahoch napokon spod obžaloby oslobodený. Jedno z riešení tejto situácie by mohlo byť napríklad umožnenie anonymnej spolupráce nahlasovateľa korupčného prípadu s políciou.        Funkčný internetový systém na anonymné oznamovanie korupcie využíva napríklad nemecký krajinský kriminálny úrad v Dolnom Sasku. V dnešnej situácii, kedy sa ľudia z rôznych profesných oblastí potichu sťažujú na výrazný nárast korupcie v styku s poskytovateľmi verejných služieb, by možno zavedenie takéhoto anonymného komunikačného systému mohlo priniesť svoje ovocie. Minimálne v tom, že vďaka nemu by si už ani účastníci zabehaných korupčných schém odrazu nemohli byť ničím a nikým istí. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (63)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Zákonom chránené gorily
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Zmeňte požiadavky novembrového protestu, prosím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Najvyšší kontrolný úrad
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Mám pokračovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Vráťme moc ľuďom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Kamil Krnáč
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Kamil Krnáč
            
         
        kamilkrnac.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




