

 


 
 

 
Komplex CERN, Švajčiarsko, Francúzsko

 


 
 
Nás ľudí delí od ostatných zvierat vyvinutá schopnosť zamýšľať sa nad svojim pôvodom. Cítime prirodzené nutkanie poznať a chápať javy navôkol a to nás ženie dopredu. Ľudia chcú a vždy chceli poznať odpovede na tzv. "veľké" otázky našej existencie: Odkiaľ pochádzame? Z čoho sme? Aká je budúcnosť nás a nášho vesmíru?  
 
 

Ťažký deň generála Stubblebina
 
 

V jeden krásny letný deň v roku 1983. Sediac za stolom vo svojej kancelárii v Arlingtone vo Virgínii a dívajúc sa na dlhú stenu plnú vojnových vyznamenaní,  pocítil generál Albert Stubblebine III, muž veliaci 16 000 mužom a celej vojenskej rozviedke, silné nutkanie dačo vykonať.  Rozhodol sa, že si musí po niečo zájsť do druhej miestnosti. Vstal spoza stola a pomalými krokmi sa vybral smerom k dverám. Mohol samozrejme zavolať svojho asistenta ale od začiatku cítil, že toto je niečo čo musí urobiť sám. S hlbokým ťažkým nádychom ubezpečil sám seba, že teraz je ten správny čas:

 
 
"Z čoho prevažne pozostáva atóm? Z priestoru!" spýtal sa a sám si aj odpovedal. 

 
 

Pridal do kroku. "Z čoho pozostávam prevažne ja? Z atómov!"
 
 

Tempo chôdze generála Stubblebina sa naďalej zrýchľovalo. "Z čoho pozostáva stena oproti mne? Z atómov! Jediné čo musím urobiť je zlúčiť priestory! Stena je len ilúzia. Čo je môj osud? Je mojim osudom ostať v tejto miestnosti? Nie! Ha!...."
 
 
 
Potom generál vrazil čelom do steny. Rozbitý nos generála ležiaceho na zemi bol nič v porovaní s pocitom opätovného zlyhania prechodu cez murivo kancelárie... 
 
Kde urobil pri svojom dumaní nad zložením pevných látok generál Stubblebine chybu? 
 
 

NeDeliteľné atómy?
 
 

Genéza, pôvod všetkého hmotného aj nehmotného okolo a vrátane nás je systémom vzájomne pôsobiacich fyzikálnych mechanizmov (z ktorých pravdepodobne vznikol aj život). Spojovníkom medzi niečím tak zdanlivo nekonečne rozľahlým ako je vesmír a tak mikroskopickým ako sú jednotlivé častice je ich spoločný začiatok v jednom, najmenšom možnom meradle. Ako základ teórie prvotnej fázy expanzie vesmíru, tj. jeho vzinku, sa považuje sústredenie energie do jedného bezdimenzionálneho nekonečne hustého bodu- singularity. Energia v ňom skondenzovala rovnaké množstvo hmoty a antihmoty, ktoré sa pri kontakte vzájomne anihovali a uvoľnili tak čistú energiu. Táto teória vzinku vesmíru založená na spomínanom jave sa nazýva Veľký tresk. Práve tento moment je zjednotením vied kozmológie a časticovej fyziky. V hlavách mnohých ľudí to implikuje otázku pôvodu samotnej hmoty, ktorá tvorí väčšinu nami pozorovateľného a známeho vesmíru. Všetko vo vesmíre sa skladá z atómov. Tak znel základy fyziky z konca 19. storočia. Atóm pôvodne chápaný ako základná nedeliteľná (z gr. átomos=nedeliteľný) jednotka všetkej hmoty narušil kruhy vtedajšej vedy. Avšak javy ako rádioaktivita, gama žiarenie alebo roentgenové lúče nenechávali fyzikov v pokoji spávať. Netrvalo dlho a objavili sme elektrón (J.J. Thomson), ktorý obieha okolo atómového jadra (E. Rutheford). Einstein narušil aj posledné piliere newtonovskej fyziky, keď svojimi teóriami relativity, najprv špeciálnou (1905) a neskôr obecnou(1915) spôsobil odvrhnutie deterministického vnímania absolútneho času a priestoru. Hmota ohýba priestor, priestor naoplátku určuje pohyb hmoty. Svetlo je vlnenie rovnako ako aj častica. Energia a hmota sú zameniteľné. Realita nie je predurčená, ale pravdepodobnostná. Teórie modernej teoretickej, časticovej či kvantovej fyziky boli odvodené od základu tých Einsteinových. Pre zachovanie jednoduchosti teraz radšej skončím. Dovolím si však zhrnúť, že všetky látky sa skladajú z molekúl, tie z atómov, atómy z obiehajúcich elektrónov okolo jadra z protónov a neutrónov, tie tvoria tzv. hadróny pozostávajúce z kvarkov viazaných gluónmi... Nevravel tu niekto niečo o jednoduchosti? Aj keď sa o súčasných poznatkoch Einsteinovi, Planckovi či Newtonovi nemohlo ani snívať, stále dnešná veda čelí na mnohých frontoch silnej paľbe zásadných nejasnotí. Je to koniec časticového redukčného reťazca? Alebo sú aj kvarky a gluóny, najmenšie nám známe častice, tvorené ešte menšími časticami? 
 
Štandardný model a Higgsov bozón
 
 

Generál Stubblebine zabudol pri svojom dumaní o priechode cez murovanú stenu druhej kancelárie smerovať svoje myšlienky na sily, ktoré jednotlivé atómy pokope, tj. ich väzby. Tie dávajú hmote jej vlastnosti. Sily na tzv. makroúrovni, ako napríklad elektromagnetická, umožňujú časticiam vzájomnú interakciu prostredníctvom magnetického poľa a gravitačná sila umožňuje vziazanosť na seba vzhľadom Newtonov gravitačný zákon, atď. Cieľom teoretických fyzikov je nájdenie akéhosi univerzálneho modelu reality. Teórie, ktorá by bola aplikovateľným základom všetkých známych javov základnej (fundamentálnej) interakcie(z ang. GUT: Grand unification theory). Jeden takýto model tzv. "Štandardný" bol vyvinutý v šesťdesiatych a sedemdesiatych rokoch minulého storčia.  Opisuje vzájomnú interakciu častíc prostredníctvom troch zo štyroch známych síl. 16 častíc, z ktorých 12 sú častice tvoriace hmotu (fermióny=kvarky a leptóny) a 4 sú "nosiče" týchti síl (bozóny). Spomínanými silami sú elektromagnetická, silná a slabá nukleárna a gravitačná. Gravitačná sila je zďaleka najslabšia avšak na veľké vzdialenosti paradoxne najpodstatnejšia. Poprvé z dôvodu jej nekonečného poľa a po druhé preto, že všetká hmota má pozitívny náboj. Na rozdiel od elektromagnetickej sily tak nemôže byť rušená. Všetky planéty, hviezdy a galaxie sú pod jej vládou, pretože totálny elektrický náboj týchto masívnych hmotných štruktúr je nulový (polovica náboju je pozitívna a polovica negatívna ) Podstatné však je, že sami o sebe tieto spomínané elementárne častice nemajú hmotu (z ang. mass). Kde sa táto vo vesmíre vzala? 
 
Chýbajúcou časticou je tzv. Higgsov bozón. Túto časticu zvyknú niektoré média nazývať aj "božskou časticou" alebo "svätým grálom časticovej fyziky". Ide o časticu, ktorá vypĺňa prázdne medzery v Štandardnom modely, ktorú predpovedal pred 40 rokmi fyzik z Edinburgskej univerzity Peter Higgs. Higgsov bozón je hypotetickým nositeľom tzv. Higgsovho poľa, ktoré dáva elementárnym časticiam ich hmotnosť. Teoretický fyzik John Ellis pre National Geographic (2008) použil pre priblíženie predstavy Higgsovho poľa následujúci ilustračný príklad: 

 

 


 
 

 
 


 
 
"Predstavte si dav ľudí, predstavujúcich jednotlivé častice, bežiacich cez blato. Niektoré častice, napríklad kvarky, majú veľké topánky, na ktoré sa nabalí veľa blata, zatiaľ čo niektoré, napríklad elektrony, majú topánky malé a tak nabalia na seba málo ak vôbec nejaké blato. Iné častice, ako fotóny, nemajú topánky vôbec a cez blato len kĺžu. Na tie sa nenabalí blato žiadne. Higgsovo pole je práve to blato."
 
 

Higgsov bozón je pravdepodobne 100 až 200 krát hmotnejší ako protón. Vzinká pri extrémnych podmiekach uvoľnenia energie. Keďže je tak mohutný je veľmi nestabilný  pretrvá menej ako milióntinu miliarditiny miliardity sekundy pred tým, než sa rozpadne. Preto sme ho zatiaľ neboli schopní spozorovať. K Higgsovmu bozónu sa ešte vrátime. 
 
 Temná tmota a supersymetria
 
 
Na základe astronomických pozorovaní pohybu vzdialených galaxii vieme usúdiť, že na ne pôsobí väčšia gravitačná sila ako odpovedá viditeľnej hmote. Je možné z toho implikovať záver, že vo vesmíre musí existovať ešte nejaká iná, ďalšia supersilná hmota. Zažilo sa označenie "temná hmota". Ďaľšia teória, na ktorú sa mnohí teoretickí a časticoví fyzici sústredia je tzv. teória supersymetrie. Tá predpokladá, že každá elementárna častica mala v počiatkoch vesmíru svoj oveľa hmotnejší proťajšok (elektrón-&gt;selektrón, kvark-&gt;skvark a pod.). Takto mohutné častice sú veľmi nestabilné. Ale mohol by existovať jeden druh takejto častice, ktorý pretrval od začiatku vesmíru pred viac než 13.5 miliardami rokov. Niektorí teoretickí fyzici predpokladajú existenciu týchto častíc v iných paralených dimenziách. Podľa tejto teórie môžu častice temnej hmoty prenikať napríklad našim tkanivom alebo kosťami bez nášho povšimnutia. Nech to znie pre nás akokoľvek sci-fi stále sa pohybujeme v medziach reality. Lepšie povedané reality kvantovej mechaniky.  
 
 

Ďalší krok vpred- LHC?
 
 

Vráťme sa však na Zem roku 2008. Spomínate si ešte na tunel bol babkami vo francúzskej dedinke? Tunel v hĺbke 100 metrov (londýnsky Big Ben má pre ilustráciu 96.3 m) je súčasťou rozsiahleho komplexu Európskej organizácie pre jadrový výskum, známej pod skratkou CERN. Na rozlohe 58 kilometrov štvorcových (približný ekvivalent ostrova Manhattan) sa nachádzajú zariadenia určené na odhalenie najväčších fyzikálnych záhad súčasnosti, ktoré sme si už vyššie rozobrali. Pomocou najambisióznejšieho vedeckého projektu súčasnosti -Veľkého hadronového zrážkového urýchľovača- LHC (Large Hadron Collider) sa pokúsime priblížiť k odpovediam na spomínané otázky pôvodu hmoty elementárnych častíc, temnej hmoty či teórie supersymetrie. Fakt, že nejde o kratochvílu nejakej skupinky "šialených" vedcov dokazuje, že sa ich na 8 miliardovom projekte($) LHC podieľa približne 2 000. Keď ešte k tomu pripočítame ostatných odborníkov CERNU z temer 50 participujúcih krajín sveta dostaneme číslo približne 50 000.

 
 
 
Už v máji tohto roku sa spustí LHC. Vedci v CERNE, ale aj mimo neho budú sústrediť svoju pozornosť na bilióny protónov rútiacich sa oproti sebe v tuneli LHC takmer rýchlosťou svetla. Čím sa častice pohybujú rýchlejšie a čím sú mohutnejšie tým viac majú tendenciu raziť si cestu vpred. Preto je v LHC nainštalovaných 24 gigantických magnetov a nejakých približne 1224 menších diploidných, ktorých úloha je usmerňovať protóny na ich ceste po kruhovom tunely LHC ochladzovaných tekutým héliom pri teplote nižšej ako je vesmírne vaakum, tj. -271°c (1.6 K). Šesť samostatných detektorov v šiestich inštaláciach v komplexe CERN bude zbierať petabyty (milóny miliard) údajov z 800 miliónov kolízii protónov za sekundu. Každý z detektorov sa však sústredí na niečo iné. Zatiaľ čo detektor Alice bude simulovať podmienky pri vzinku vesmíru štúdiou kvark-gluónového plazmatu, detektory Atlas a CMS sa sústredia na elementárne sily- podstatu hmoty (higgsov bozón) či detektor označený ako LHCb zameraný na pochopenie otázky: prečo vo vesmíre prevláda hmota nad antihmotou. Spomínaný detektor CMS (Compact muon selenoid) je ťažší ako Eifelova veža alebo ak chcete 14.5 násobok hmotnosti Stonehenge. Jednotlivé strediská sú prepojené s ďalšími pracoviskami, inštitútmi a vysokými školami po celej planéte vlastným sieťovým systémom GRID(CERN sa dáva kredit aj vytvorenie www- pôvodný GRID). Cez ten budú prechádzať približne 15 petabytov informácii ročne z výsledkov pokusov v LHC. Ako sa zdá, pri hľadaní mechanizmov vzniku vesmíru na veľkosti použitej technológie skutočne záleži.

 
 
 
 
 

Masívny detektror Atlas, časť komplexu LHC ( Fotka od Maximiliena Brice-a, CERN)  

 
 


	
	
	
	
	


 
 
Simulácia LHC
 
 

 
 

Záver  

 
 

Čas, ktorý uplynul od vzniku nami známeho vesmíru sa odhaduje na približne 13.76 miliardy rokov. V tomto počiatočnom bode bola sústredená všetká energia vesmíru. V ziliótine ziliótiny frakcie času boli mohutnou explóziou vymrštené do priestoru elementárne častice vzájomne silami na seba pôsobiace a tvoriace tak hmotu. Z tej sme zložení aj my. Za necelých tristo rokov sme technologicky pokročili od galileovho teleskopu k hubblovmu, od newtonovho sadnutého jablka ku kvantovej mechanike a teraz sa chystáme u nás na Zemi vytvoriť podmienky rovnaké aké predpokladáme pri vzinku vesmíru. Myslím, že nie som sám, kto je uchvátený takýmto progresom ľudského poznania. Cieľom tohto článku malo byť priblíženie hlavných a zásadných otázok modernej časticovej fyziky, ktoré sú predmetom aj najväčšieho vedeckého projektu v súčasnoti vo švajčiarskom LHC-CERN. Aby sme sa mohli ďalej zamýšľať nad našim miestom vo vesmíre, musíme pochopiť jeho pôvod. To si ešte vyžiada veľa času a úsilia ale v každom prípade sme na dobrej ceste! 
 
 

Ale čo ak vedci v CERN-e nič neobjavia? To je takmer nepravdepodobné. Ale ako to už vo vede býva, a čo by nám aj generál Stubblebine vedel potvrdiť, náraz do steny býva tvrdý. Niekde sme pochybyli, niečo čo malo byť urobené, nebolo. Alebo nám jednoducho dačo podstatné ušlo. Dôležité je však hladať ďalej. 
 
 
 
Zdroje: 
 
BBC-Horizon-The Six Billion Dollar Experiment 
http://www.youtube.com/watch?v=WvEK5uZXpZU 
 
Energising the quest for 'big theory'  
http://news.bbc.co.uk/2/hi/science/nature/4524132.stm 
 
The Large Hadron Collider (LHC) 
http://www.scitech.ac.uk/SciProg/PP/Projects/LHC.aspx 
 
Grand unification theory 
http://en.wikipedia.org/wiki/Grand_unification_theory 
 
A brief history of the Grid 
http://gridcafe.web.cern.ch/gridcafe/Gridhistory/history.html 
 
Higgs boson 
http://en.wikipedia.org/wiki/Higgs_boson 
 
Fundamental interaction 
http://en.wikipedia.org/wiki/Fundamental_forces 
 
Standard Model 
http://en.wikipedia.org/wiki/Standard_Model 
 
At the Heart of All Matter 
http://ngm.nationalgeographic.com/2008/03/god-particle/achenbach-text 
 
 
 
 

 

