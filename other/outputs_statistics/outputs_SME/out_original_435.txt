
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Lukáč
                                        &gt;
                Slovenská divočina
                     
                 Strieborný jeleň 

        
            
                                    30.1.2008
            o
            7:07
                        |
            Karma článku:
                11.84
            |
            Prečítané 
            10818-krát
                    
         
     
         
             

                 
                    Poľovníci ohodnocujú trofeje zastrelených jeleňov medailami. Zlatými, striebornými i bronzovými. Kov je priradzovaný podľa bodov, ktoré parožie získalo počtárskym výpočtom z dĺžky parohov, viacerých obvodov parohov, počtu a dĺžky vetiev parožia či ich rozpätia  To všetko sa prenásobí rôznymi koeficientmi, číselne sa ešte zhodnotí vzhľad a vypočíta sa nejaké číslo. Ak je výsledok väčší ako 170, je za to medaila. Nad 210 bodov zlatá. Mojim najlepším úlovkom je strieborný jeleň.
                 

                 
Aby mali jelene poriadne parožie, napchávajú ich v ohradách hormónmi a iným dopingom. Tak ako tohto. Naše karpatské jelene sú proste iné, divoké. 
   Na Čergove je jedno výnimočné miesto. Nepoviem presne kde, pretože naň nechcem privolávať zbytočnú ľudskú pozornosť. Je to jeden svah, otočený na sever, plný šťavnatých javorov, ozrutných bukov a skrášlený jedľami. Nohy sa vám zabárajú do vlhkej pôdy plnej bukového lístia a machov. Striebristé mesiačiky mesačnice sa kývajú aj za slabulinkého vánku a nikdy inak, pretože na tomto čarovnom svahu nefúka silný vietor ani za najväčších presunov tlakov a frontov. 
 Neviem prečo, ale tento svah si v čase ruje obľúbili najväčšie čergovské jelene. Plošiny, ktoré tam zostali po uhliaroch, využívajú ako rozhľadne, z ktorých ich výborne počujú lane. Je to veľmi dôležité, pretože ako dokazujú najnovšie vedecké štúdie, poriadny, bujačí hlas, je lákadlom pre družky, i výborným afrodiziakom.  
 Tam som roky chodieval pozerať na impozantné súboje statných jeleňov, keď z parohov lietali triesky a krv striekala na metre. Aj som sa bál, aby som jednu neutŕžil, ale jelene si ma nikdy nevšimli. Dokonca ani tento bujak, ktorý s krvavými šrámami prechádzal niekoľko metrov predo mnou. Tak blízko, že prvé zábery som trasúcimi rukami ani nestihol nielenže zaostriť, ale ani poriadne zazúmovať a na fotografiách mám len štvorec jelenieho brucha - veľký ako dlaň. 
  
 Ale nie toto je môj najlepší úlovok. Ten sa mi podaril neskôr a veľmi nečakane a prekvapivo. 
 Nad miestom čarovného rujoviska je sedlo, kde sú tiež pekné plošiny po uhliaroch a keďže tam nie sú stromy, len horské lúky, je odtiaľ výborný výhľad na prichádzajúce a odchádzajúce jelene. Keď som sa tam raz so svojou manželkou pripravil na fotografovanie, dlho vyzeralo, že sa nič nebude diať. A dlho sa skutočne ani nič nedialo. 
 Zalezený do spacáku som pozoroval strašne dlhé tiene pokrútených bukov, ktoré na ostro zelenej tráve čergovského hrebeňa  kreslili čierne, pitoreskné obrazce, keď odrazu Dáša zasyčala. Dáša syčí len keď polejem obrus čiernym čajom alebo keď vidí niečo zaujímavé. 
 „Pozri sa, niečo sa dole hýbe, obchádza to ten veľký jalovec“.  
 Jalovec je v našej normálnej reči borievka obyčajná, Juniperus communis, základ slávnej spišskej borovičky. Pozrel som sa tým smerom a skutočne sa mi marilo, že niečo veľkého tam žerie prazdroj jedného z našich najslávnejších nápojov. 
 Chodilo to zľava - sprava okolo veľkej borievky, ale na fotografovanie už bolo málo svetla a mne sa už ani nechcelo vyliezať zo spacáku, tak som tento večer tohto jeleňa oželel. 
 Ráno, za svitania sme mali slnko oproti , ale aj tak sme obidvaja takmer naraz zazreli, že miesto, kde bol včera jeleň, opäť obchádza nejaký zver. 
 „Vyzerá to ako strieborný jeleň“, riekla moja polovička a ja som nasadil teleobjektív na ošúchaný fotoaparát a pripravil sa na životný úlovok. Rýchle sme sa obliekli a pomaly, medzi borievkami, nenápadne,  presne podľa manuálov amerických rangers, sme sa plazili k jeleňovi. 
 Päť metrov od borievky som sa prudko vztýčil, odistený fotoaparát v ruke, a cvakol som.  
 Bez rozmyslu, automaticky, previnul som film a pripravoval som sa na druhý záber. 
 Už nebol žiaden druhý záber.  
 Váľali sme sa po prehriatej tráve a rehotali sme sa o dušu. 
 O borievku bol zachytený nafukovací medvedík, z jednej strany celý strieborný, dôsledok veľkej propagačnej akcie pekárenskej firmy dole v Prešove,.  
 Chudáka medvedíka tu k nám zavial vietor, a v prekročení čergovského hrebeňa mu zabránila dlhá, dvojmetrová  šnúrka, ktorá vykĺzla nejakému decku z ruky a zamotala sa do borievky. 
 Nebol to strieborný jeleň. 
 Bol to lietajúci medveď. 
   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Spáľme kostoly?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Fialová farba Veľkej Fatry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Microblog
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Veľkonočný príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Lukáč 
                                        
                                            Fliačik na Sibíri
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Lukáč
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Mráziková 
                                        
                                            Ako tri Slovenky z UKF precestovali Ameriku
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Lukáč
            
         
        jurajlukac.blog.sme.sk (rss)
         
                        VIP
                             
     
        Elektronik, ktorý sa zamiloval do divočiny a ako východoslovenský chmurnik predpovedá počasie na každý víkend
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    124
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6651
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovenská divočina
                        
                     
                                     
                        
                            Môj priateľ Skúkam
                        
                     
                                     
                        
                            Drevorubači a poľovníci
                        
                     
                                     
                        
                            Stromy
                        
                     
                                     
                        
                            Na hranici zdravého rozumu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prečo budú tento rok povodne
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Flegr: Zamrzlá evoluce
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zavýjanie vlkov v lese
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vichodňarska dzifka
                                     
                                                                             
                                            Dievča so srdcom a rozumom
                                     
                                                                             
                                            Adam Wajrak - skutočný novinár
                                     
                                                                             
                                            trochu lásky
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            John Nash - môj matematický guru
                                     
                                                                             
                                            Anton Markoš - môj bakteriálny guru
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Spáľme kostoly?
                     
                                                         
                       Fialová farba Veľkej Fatry
                     
                                                         
                       Humbug zvaný Zachráňme slovenskú pôdu.
                     
                                                         
                       Mečiarovi pohrobkovia kántria lesy.
                     
                                                         
                       Slovenský Biomasaker motorovou pílou
                     
                                                         
                       Jak on může vědet, že strana bé půjde zrovna takhle?
                     
                                                         
                       Svetový objav poslanca Smeru
                     
                                                         
                       Diletanti na bratislavskej radnici
                     
                                                         
                       Študenti, do parlamentu nechoďte!
                     
                                                         
                       Prečo Rytmus môže to, čo nemohol Mikla ?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




