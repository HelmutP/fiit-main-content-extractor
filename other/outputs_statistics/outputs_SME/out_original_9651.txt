
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Makyta
                                        &gt;
                Recenzie
                     
                 Kvet púšte 

        
            
                                    26.6.2010
            o
            14:12
                        (upravené
                26.6.2010
                o
                14:19)
                        |
            Karma článku:
                6.73
            |
            Prečítané 
            2388-krát
                    
         
     
         
             

                 
                     Keď redaktorka žiadala slávnu top - modelku Waris Dirie ( Waris znamená Púštny kvet ), nech porozpráva o dni, ktorý jej zmenil život, mala na mysli krásnu rozprávku o chudobnej dievčine z Afriky, ktorá prežíva Popoluškin sen. Lenže jej medzný deň nebol ten, o ktorom rada počúva a číta západná kultúra, ale ten, keď bola ako trojročná obrezaná.
                 

                 
  
   Tento film je veľmi potrebný. Nielen kvôli tomu, že ohavná obriezka sa vykonáva neustále skoro na celom svete a denne ju podstupuje 6 tisíc dievčat... Ale aj kvôli tomu, aby si naša civilizovaná kultúra uvedomila ako sa žije v inom svete ako je náš. Etiópka Liya Kebede ( objavila sa v Obchodníkovi so smrťou s Nicolasom Cageom z roku 2005 ) je bezpochyby najkrajšia africká herečka, ale jej kúzlo nie je len v kráse, ale i citlivom stvárnení svojej postavy. Jej zoznamovanie sa s priateľkou v Londýne, novou prácou, mužom na diskotéke, ale i obyčajným vodovodom je ozajstné a zároveň milé. Film je natočený v dvoch dejových a časových rovinách, keď sa Waris v retrospektívach vracia k neľútostnému detstvu. Treba sa pozastaviť nad vynikajúcou hudbou Martina Todsharowa, v niektorých fázach mi pripomínala naliehavosť a precíznosť Mansellových skladieb z filmu Requiem za sen.     Kvet púšte vznikol podľa autobiografickej knihy Waris Dirie, ktorá vyrastala do svojich 13 rokov v Somálsku a po úteku do Londýna sa stala jednou z najznámejších svetových topmodeliek. Keď bola kniha vydaná v roku 1998, svet bol šokovaný životným príbehom, v ktorom popisuje neuveriteľnú cestu zo somálskej púšte na najznámejšie svetové módne prehliadky. Po zverejnení rozhovoru o obriezke skončila s kariérou a od roku 1997 sa stala veľvyslankyňou UNFPA ( United Nations Population Fund ) pre odstránenie problémov ženskej obriezky na svete.    Vizitka :   Púštny kvet ( Desert Flower )  Veľká Británia, Nemecko, Rakúsko 2009, dráma  130 minút, slovenské titulky, prístupný od 15 rokov  premiéra v kinách 17.6.2010, Continental    Moje hodnotenie: výborný 90% 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Makyta 
                                        
                                            „Vyhraj voľby a môžeš všetko“
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Makyta 
                                        
                                            Štát, obávaj sa hnevu slabých !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Makyta 
                                        
                                            Lekcia sociálneho správania
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Makyta 
                                        
                                            Hlava pána precedu to dokáže…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Makyta 
                                        
                                            Kto zabil Bartošovú ?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Makyta
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Makyta
            
         
        miroslavmakyta.blog.sme.sk (rss)
         
                                     
     
        Hľadám svoj priestor a našiel som ho.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    163
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1397
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Film a literatúra
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Tvorba
                        
                     
                                     
                        
                            Miesto pre život
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




