
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Drotován
                                        &gt;
                Spoločnosť
                     
                 Zožerie nás korupcia zaživa? 

        
            
                                    13.5.2010
            o
            19:30
                        |
            Karma článku:
                9.72
            |
            Prečítané 
            1481-krát
                    
         
     
         
             

                 
                    Jedným z najväčších problémov Slovenska je nízka vymožiteľnosť práva a korupcia. Ak si pozrieme mieru vnímania korupcie na svete, na mape je jasne vidieť koreláciu medzi vyspelosťou štátu a indexom CPI. Slovensko sa po zlepšení v posledných rokoch opäť prepadá. Človeku je až zle, keď počúva reči obchodníkov v IT sektore, strojárstve či stavebníctve, akým spôsobom sa niektoré firmy dostávajú k štátnym zákazkám. Často vyhráva najdrahšia a najnevýhodnejšia ponuka v tendri šitom na mieru nejakému kamarátovi s garážovou firmou. Prípadne sa jedná o veľkú firmu, v ktorej slovenský manažment je pre obchod ochotný urobiť prakticky čokoľvek.
                 

                 
  
   Existujú tri základné podmienky, ktoré výrazne znižujú mieru korupcie - občania si uvedomujú, že verejné prostriedky sú ich prostriedky, obstarávatelia sú kvalifikovaní a kontrola je založená na vysokej miere transparentnosti. V prvom rade v normálnej krajine neexistuje, že samotný minister hospodárstva propaguje "netradičné formy predaja". A ako sme sa nedávno dozvedeli, neprekvapí, že ryba smrdí od Širokého. Neprekvapí ani napojenie Plastiky na Pentu a ŠTBákov. Skôr je stále šokujúce, že podobné informácie v mori marazmu sú iba určitou kuriozitou na deň v médiach. Neohovoriac o tom, že je možné, že peniaze z azerbajdžanskej privatizácie pretiekli priamo cez Slovensko.   Akú úctu môže mať človek voči súdu, ktorý rozhodne, že vlastne Gabriel Karlín, ktorý fyzicky prebral pól milióna korún je nevinné pacholiatko? A nasleduje mlčanie jahniatok a Jahnátka.   Zákon o slobodnom prístupe k informáciám je v poslednej dobe výrazne okliešťovaný opäť rozhodnutiami súdov, kde často sedia tí istí ľudia ako pred novembrom 1989. Komunistická solidarita medzi ľuďmi so straníckou knižkou bola vždy veľmi veľká. (Harabín musí mať tiež hlavu v smútku, pretože jeho kamarát Baki Sadiki prišiel o celú úrodu, ktorú napadli nechutné huby). Transparentnosť je však možné zabezpečiť iba zverejňovaním všetkých relevantných dokumentov. Odvolávanie sa na obchodné tajomstvo v prípade štátnych zákaziek nemôže byť akceptovateľné. Ak niekto chce robiť obchod a sú do toho zatiahnuté aj peniaze daňových poplatníkov, musí ísť s kožou na trh. Ak nie, tak žiaden obchod nebude.   V USA, ak na základe zistenia občana dôjde k vráteniu neefektívne investovaných peňazí, daný občan má právo na 10% z vrátenej sumy, a samozrejme následne je veľký tlak na prepustenie z verejných služieb osoby (osôb), ktoré za pôvodné plytvanie nesú zodpovednosť. Tento systém by bolo podnetné zaviesť aj na Slovensku. Samozrejmosťou by tiež malo byť povinné elektronické verejné obstarávanie (aj v prípade nákupu toaletných papierov pre ministerstvá) a tiež vznik tzv. obstarávacích agentúr.   Môžeme sa sami rozhodnúť, či chceme mať slovo korupcia v slovníku cudzích slov ako Fíni alebo v dennej komunikácií ako bakšiš v Egypte. Kto navštívil obe krajiny vie, že rozdiel je vidieť na prvý pohľad. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Odpočet práce poslanca v Rači 2010-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Politika bez kšeftov a tajných dohôd
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Otvorený list primátorovi vo veci Železnej studienky a jej záchrany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Bude primátor Ftáčnik chlap alebo handrová bábika?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Ohlásenie vstupu do prvej línie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Drotován
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Drotován
            
         
        drotovan.blog.sme.sk (rss)
         
                                     
     
        Blogger na SME 2005-2014, nominovaný na Novinársku cenu 2012. Aktuálne som presunul blogovanie na http://projektn.sk/autor/drotovan/  
Viac o mne na: www.drotovan.webs.com 
 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    224
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4323
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Poznámka pod čiarou
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Bratislava - Rača
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Mesto Bratislava
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Môj blog na Račan.sk
                                     
                                                                             
                                            Môj blog- Hospodárske noviny
                                     
                                                                             
                                            Klasický ekonomický liberalizmus ako spojenec konzervativizmu
                                     
                                                                             
                                            Salón
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            KIosk
                                     
                                                                             
                                            Knihožrútov blog
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                             
                                            Ondrej Dostál
                                     
                                                                             
                                            Ivo Nesrovnal
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cyklodoprava
                                     
                                                                             
                                            Bratislava - Rača
                                     
                                                                             
                                            Project Syndicate
                                     
                                                                             
                                            Bjorn Lomborg
                                     
                                                                             
                                            Leblog
                                     
                                                                             
                                            Cyklokoalícia
                                     
                                                                             
                                            Anarchy.com
                                     
                                                                             
                                            EUPortal.cz
                                     
                                                                             
                                            Political humor
                                     
                                                                             
                                            Pravé spektrum
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




