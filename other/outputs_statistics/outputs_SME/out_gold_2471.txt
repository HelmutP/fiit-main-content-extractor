

 Potrebujeme: 
 Ako na každý správny balkánsky sladký dezert more cukru. V našom prípade je to cca 800g, ale aj taký celý jeden kilogram neublíži. Ďalej potrebujeme 2 balíky štrúdľového cesta (300 - 400g, nahradia originálne cesto na baklavu),125g masla, 300 g pomletých orechov (ja som mal orechy a nesolené pistácie 1:1, ale stačia aj samotné vlašské orechy), pol balíčka prášku do pečiva, 4 vajíčka, 200 g polohrubej múky, 3 balíčky vanilkového (vanilínového) cukru a 1 balíček škoricového cukru. 
  
 Práca jednoduchá ako facka... 
 Orechy pomelieme, 
  
 ...pridáme do nich asi 200 g cukru, 200 g múky, vajíčka, 1 balíček vanilkového a 1 škoricového cukru, prášok do pečiva a všetko vymiešame. Keď necháme orechovú zmes chvíľu postáť, cukor sa v nej roztopí a už nebude taká tuhá. Bude sa nám s ňou ľahšie pracovať. 
  
 Toto pekné a naozaj dobre vypracované štrúdľové cesto sme sa rozhodli takto odprezentovať. Dostať ho u nás v Devínskej v Terne. Pani Holzárová odvádza skutočne skvelú prácu. Cesto je navyše veľmi dobre narezané a výborne sa s ním pracuje. 
  
 Maslo vložíme do rajničky a na miernom planeni ho necháme rozpustiť. Odstavíme ho. 
 Najprv ním pomocou štetca či pierka potrieme klasický koláčový plech. Položíme prvú vrstvu cesta. Ak nám nesedí veľkosť, jednoducho prevyšujúcu časť odstrihneme a použijeme ju tam, kde chýba. Cesto potrieme maslom. Pokračujeme druhou vrstvou a tak ďalej až do minutia prvého balíčka štrúdľového cesta. 
 Na rad prichádza orechová plnka. Posledný plát prvého balíka ňou rovnomerne ponatierame. 
  
 Otvoríme druhé balenie cesta. A pokračujeme s potieraním maslom a ukladaním jednotlivých plátov. Zdá sa nám baklava pred pečením nízka? Nezabúdajme, že v plnke sú ako vajíčka, tak aj prášok do pečiva. Baklava mám teda pekne narastie. 
  
 Rúru predhrejeme na 170 stupňov a baklavu pečieme do zozlatnutia teda cca 15 až 20 minút. 
  
 Po upečení ju opatrne nakrájame dobre ostrým nožom na požadované porcie. Dbáme na to, aby sme jednotlivé porcie dobre poprekrajovali. Musia byť od seba úplne pooddeľované. Vrchná vrstva cesta sa nám bude pravdepodobne lámať. Originalne cesto na baklavu je predsa len trochu iné. Nič to zato, tvárime sa radostne a tešíme sa na neskutočne sladký, ale aj neskutočne skvelý dezert. 
  
 Zvyšok cukru nasypeme do vody (600 - 800 g cukru na 800 ml - 1l vody), pridáme dva balíčky vanilkového či vanilínového cukru a necháme zovrieť. 
  
 Varíme asi 5 minút. 
  
 Horúcim sirupom rovnomerne popolievame celú baklavu. Prikryjeme čistou utierkou, či iným vhodným spôsobom a necháme odstáť asi 12 hodín. Sirup sa do baklavy krásne vpije. 
  
 Jednotlivé porcie baklavy podávame poliate sirupom, ktorý sa nevpil. Pre osvieženie ich môžeme jemne pokvapkať čerstvou citrónovou šťavou. 
  
 Dobrú chuť. 
   
 Deti, bakláv je toľko rôznych druhov a tvarov, plnených len samotnými orechmi s cukrom, (bez vajec a múky), alebo aj rôznymi druhmi sušených plodov, stočených do samostatných 'balíčkov' či roliek, ktoré sa potom zatáčajú do väčších okrúhlych tvarov, že spomínať ďalšie a ďalšie druhy by bolo nadbytočné. Poznám množstvo ľudí, ktorí za  dobrou baklavou prejdú 'sveta kraj' a pritom je možné pripraviť ju skutočne veľmi jednoducho aj u nás doma. A poznám aj niekoľko jedincov, ktorí by ju do úst nevzali, lebo im pripadá príkro sladká. 
 Poradím vám. Po prvom súste trochu počkajte. A skúste to opäť ... a opäť. Kým si neprivyknete na tú voňavú sladkosť. A vaša závislosť je úspešne vypestovaná. Dielo skazy zavŕšené...:) 
   
 tenjeho 
   

