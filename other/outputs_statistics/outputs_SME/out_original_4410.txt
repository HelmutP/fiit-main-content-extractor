
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Hruška
                                        &gt;
                Trips
                     
                 Trip autom – part 4 – Grenoble, Ženeva, Zürich, Vaduz 

        
            
                                    14.4.2010
            o
            6:00
                        (upravené
                13.4.2010
                o
                14:00)
                        |
            Karma článku:
                6.00
            |
            Prečítané 
            1999-krát
                    
         
     
         
             

                 
                    Pred sebou sme mali „posledných" 1400 kilometrov cesty domov zo St. Tropez cez Švajčiarsko a Rakúsko. Do mojej promócie zostávali necelé dva dni a tak sme si museli švihnúť aby som ju stihol.
                 

                 Pobrežie francúzskej riviéry sme opustili poobede a po obyčajných hradských sme si to mierilo do Grenoblu. Táto cesta bola jednak kratšia ako diaľnica, lebo išla krížom cez kopce a bola aj krajšia lebo také výhľady aké sa nám naskytli z vrcholov, v Malých Karpatoch neuvidím.      Pri schádzaní z kopcov nám v ostrých zákrutách začali pískať brzdy. Jasný signál o tom, že končia brzdové platničky. V prvej horskej dedinke sme začali hľadať niekoho kto by nám pomohol, no nenašli sme nikoho ani kto by nám rozumel. Tak sme to riskli a išli ďalej. Večer sme prišli do Grenoblu, kde sme sa hneď spojili s mojim bývalým spoluhráčom Lucom (inak Francúz, čo prišiel na Slovensko odkukať ako sa u nás trénuje) a ten nám našiel niekoho, kto povedal, že to je v pohode nech ideme ráno bez obáv ďalej. Tak sme strávili kľudný večer v Grenobli, pri spomienkach na spoločné zápasy.   Ráno sme sadli do auta a pokračovali domov. Prvú zástavku sme mali až (už) v Ženeve, kde sme sa po flákali po centre, nakúpili a poobede pokračovali ďalej.            Ďalšia prehliadka mesta bola naplánovaná v Zürichu. Keďže už bolo neskoré popoludnie museli sme náš plán upraviť a skrátiť  - tak sme tu strávili len hodinu. Prešli sme centrum,  osviežili sa pri rieke, niečo pofotili a utekali do nasledujúcej destinácie. Lichtenštajnsky Vaduz nás lákal a tešili sme sa naňho. Keď sme tam prišli bol už večer a celé kniežatstvo sa ponáralo do tmy. Ja som už začínal byť v strese lebo ráno som mal mať spomínanú promóciu a tak som si tú polhodinu ani nevychutnal len nervózne pobehoval.      Vaduz   Konečne sme sadli do auta pokračovali domov. Museli sme prejsť krížom celé Rakúsko, začínalo pršať a brzdové platničky pískali stále viac a viac. Až doma som zistil, že z platničiek neostalo nič a že som začal drať aj kotúče. Takáto cesta bola až do Bratislavy, kde sme šťastne dorazili o piatej ráno. O ôsmej som už stál vyfešákovaný a preberal si diplom.    :-) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            10 fráz z prezidentskej diskusie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Sčítanie, druhý krát.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Sme anonymné ovce?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Taká malá trafika pre otca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Hruška 
                                        
                                            Atény
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Hruška
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Hruška
            
         
        michalhruska.blog.sme.sk (rss)
         
                                     
     
        Pracujem v IT, okrem toho hrám vodné pólo, cestujem, venujem sa rodine a sem tam ma niečo tak zasiahne, že o tom napíšem.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2440
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Trips
                        
                     
                                     
                        
                            Benelux
                        
                     
                                     
                        
                            Francúzsko
                        
                     
                                     
                        
                            Názory
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Francúzske piesne
                                     
                                                                             
                                            Schwangau
                                     
                                                                             
                                            Bezpečnosť na Internete
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Peter Cmorej
                                     
                                                                             
                                            Tibor Pospíšil
                                     
                                                                             
                                            História
                                     
                                                                             
                                            Excel
                                     
                                                                             
                                            Zdenko Somorovsky
                                     
                                                                             
                                            Kybi - cestovateľ na bicykli okolo sveta
                                     
                                                                             
                                            Peter Dzurinda
                                     
                                                                             
                                            Roman Herda
                                     
                                                                             
                                            Miroslav Babič
                                     
                                                                             
                                            Tomáš Kubuš
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       10 fráz z prezidentskej diskusie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




