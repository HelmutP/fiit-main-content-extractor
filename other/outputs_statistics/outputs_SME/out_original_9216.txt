
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Jánov
                                        &gt;
                Politika
                     
                 Odkaz novej koalícii: „Dohodnite sa na dekriminalizácii“ 

        
            
                                    20.6.2010
            o
            18:44
                        (upravené
                8.9.2010
                o
                11:15)
                        |
            Karma článku:
                7.66
            |
            Prečítané 
            794-krát
                    
         
     
         
             

                 
                    Po 20 rokoch sme sa konečne zbavili Mečiara a v parlamente sú ľudia, ktorí chcú do politiky priniesť slušnosť a spravodlivosť. Zahraničné média v tom vidia obrovský krok vpred a predpovedajú Slovensku lepšiu budúcnosť. Ako píše britský týždenník The Economist: „A political earthquake in central Europe brings new faces and high hopes." Obávam sa však citlivej témy dekriminalizácie marihuany. Verím, že v budúcej koalícii sú inteligentní ľudia, ktorí dokážu robiť kompromisy a zabránia návratu Roberta Fica.
                 

                    Predseda liberálnej strany Richard Sulík má vo svojom programe zahrnutú dekriminalizáciu marihuany, ktorá je v rozpore s predstavami KDH o budovaní slušného Slovenska.  O dekriminalizácii a legalizácii sa toho už popísalo dosť a sami predstavitelia SaS už n-krát vysvetlili, že nie je normálne, aby za neúmyselné zabitie boli tri roky väzenia a za prechovávanie akéhokoľvek malého množstva marihuany päť rokov. Myšlienku dekriminalizácie podporili Iveta Radičová, Ivan Mikloš a nebráni sa jej ani Béla Bugár.   „Naozaj vnímam ako problém, ak má mladý človek celý život výpis v registri trestov a zablokovanú ďalšiu kariéru. Myslím si, že spôsob dekriminalizácie je možné nájsť, iná téma je legalizácia,“ vyjadrila sa budúca predsedkyňa vlády Radičová.   „Je to citlivá otázka, ale legitímna. Dnešný stav, kedy de facto kriminalizujeme významnú časť mladej generácie, nepovažujem za dobrý a udržateľný,“ myslí si Ivan Mikloš.   A ja sa pýtam, či je normálne kriminalizovať niekoho kôli tomu, že si zapálil jeden ,,joint" keď by to urobil bez ohľadu na to, či dekriminalizácia bude alebo nebude? Tým ľuďom, ktorí sú na marihuane závislí treba predsa pomôcť, a nie ich viesť v záznamoch trestných registrov a ničiť im budúcnosť. Čo sa týka podpisu Vatikánskej zmluvy sú liberáli otvorení, a pevne verím, že otvorení diskusiám o dekriminalizácii marihuany budú aj poslanci strany KDH, pretože nebol to náhodou sám Ježiš, kto tvrdil: „Kde je vôľa, tam je cesta.“?   V súčasnosti je treba dať do poriadku ekonomiku po nezodpovednej politike Roberta Fica, čiže tieto otázky budú vnímané skôr ako sekundárne. V ekonomickej sfére sa však určite zhodnú a nájdu najvhodnejšie metódy riešenia týchto problémov. Mnoho mladých ľudí išlo podporiť Sulíka práve preto, že jeho názory sú z ich pohľadu tak eklektické a pokrokové. No a dôveru týchto ľudí v budúcnosti nemôže stratiť len SaS, ale aj celá vládna koalícia, pretože mladí budú mať k politikom opäť ten istý averzný prístup ako mali dovtedy, kým neprišiel Sulík.     Zdroje:   http://www.economist.com/node/16381292   http://aktualne.centrum.sk/domov/politika/clanek.phtml?id=1202141   http://hnonline.sk/slovensko/c1-44339870-bugar-vatikanska-zmluva-za-urcitych-okolnosti-ano   http://komentare.sme.sk/c/5430413/shooty.html             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Jánov 
                                        
                                            Prečo sa zúčastniť referenda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Jánov 
                                        
                                            Prvé fotografie a videá Slnka zo sondy Solar Dynamics Observatory
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Jánov 
                                        
                                            Ako to Sulík myslí s marihuanou
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Jánov
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Jánov
            
         
        janov.blog.sme.sk (rss)
         
                                     
     
        Som študentom astrofyziky a zaoberám sa politikou na Slovensku. Fascinuje ma svet, v ktorom žijeme, interakcia a symbióza jednotlivých zákonov, ktoré sú jeho neodmysliteľnou súčasťou a myslím si, že sa oplatí bojovať za to, aby existoval ďalej. Najdôležitejšie hodnoty pre mňa predstavujú pravda, pochopenie a otvorenosť.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1367
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Vesmír
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mzda
                     
                                                         
                       Fašizmus v batike
                     
                                                         
                       Prečo Kažimír tají kompletné výkazy z prezidentských volieb?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




