
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Erik Skácal
                                        &gt;
                Nezaradené
                     
                 Aby sme nevolili tých, čo nechcú premýšľať... 

        
            
                                    5.5.2010
            o
            19:24
                        (upravené
                5.5.2010
                o
                20:32)
                        |
            Karma článku:
                6.83
            |
            Prečítané 
            493-krát
                    
         
     
         
             

                 
                    Zvrátenosť najhrubšieho zrna. Politika, ktorú robí SNS už dávno nie je ani zďaleka príbuznou racionálnej politike, ale tento posledný krok bol poslednou kvapkou do už dosť tak preplnenej žumpy tejto strany.
                 

                 
Malá paródia na billboard strany SNS.Originálneho autora nepoznám, ale patrí mu všetká česť.
   Bol raz jeden dosť zvláštny ujo. Nepálilo mu to moc, mal sklony k alkoholizmu, ale napriek tomu sa snažil do všetkého, kde bola čo i len štipka zisku pchať svoje hnusné ruky a jeho pseudomúdrosti robili škody viac ako zbrane. Myslím, že každý z nás vie o kom teraz píšem.   Neprejavil by som ani štipku údivu keby teraz desaťtisíce ľudí vyšli na ulice a začali páliť vlajky, pretože sa hanbia za to, že sú Slováci. Však kto by sa nehanbil, keď máme vo vláde privatizéra, klamára a posprostejšieho zlodeja. Podľa mňa som bol doteraz dosť umiernený čo sa týka kritiky našej koalície a hlavne strany SNS, ale to čo som sa dnes dočítal na internete a následne videl v správach bolo svinstvo, prejav idiotizmu a populizmus smerovaný ku krčmovým a extrémne pravicovo zmýšľajúcim občanov, ktorí často namiesto pracovania vysedávajú v krčmách a volia ich krčmového idola, ktorý svojou rétorikou zaujme maximálne ich plus pár výnimiek. Príklady jeho desnej rétoriky, ešte horšieho vyjadrovania sa a artikulácie poznáme, či už z povestných nahrávok o tankoch, z tlačových besied, alebo naposledy z relácie O 5 minút dvanásť, kde tento pán prirovnával emisie, na ktorých sa nabalil nejeden politik z tejto pochybnej rany, k "vetrom" moderátora a tohto zvláštneho, jemne postihnutého pána.   Je najväčšou špinavosťou kradnúť kde to ide, robiť konflikty všade kde sú prítomní, poburovať ľudí a podporovať rasizmus, pričom sa hlásia k národniarom. Táto strana je tou najväčšou hanbou Slovenskej republiky. Keby videl zakladateľ SNS čo sa z jeho strany stalo, tak sa rovno zastrelí. Teraz sa len v hrobe obracia. Ako by sme nazvali kampaň tejto strany? Najskôr ju trošku opíšem. Reči tejto strany už všetci poznáme. Hovoria o zvýhodňovaní maďarskej menšiny na území SR, hovoria aj o rómskom probléme, ktorý rasistickými billboardmi určite nevyriešia a stále od nich počúvame akí sú bezúhonní a chcú len to dobre. Mne osobne je ľúto voličov tejto strany. Ale každý sme iný. Vycapiť všade plagát, na ktorom je Róm, ktorý ani nevedel prečo ho fotia, a ktorému ešte mu nejaký amatérsky grafik dorobí reťaz na tele, je nehanebnosť, svinstvo, podvod a neviem čo ešte. Na Slovensku je omnoho väčších problémov ale populizmus tejto strany sa opiera iba o Maďarov a rómskych spoluobčanov. Ja nepopieram že rómsky problém nie je, ale som presvedčený, že keby na Slovensku bolo aj 5 000 000 Rómov, tak nakradnú menej ako piati politici tejto strany.   Preto vyzývam všetkých, aby 12. júna nepodporovali mentálnych mrzákov, klamárov a zlodejov a volili tak, aby ich svedomie celé budúce štyri roky nehrýzlo, pretože čo robia niektorí vládni predstavitelia, tak to tu nebolo nikdy. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Skácal 
                                        
                                            Čo vlastne chcú?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Skácal 
                                        
                                            My, rasa vyvolená.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Skácal 
                                        
                                            Rómsky "problém" mojimi očami.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Skácal 
                                        
                                            Lebo používam rozum a nerád žeriem hovná.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Erik Skácal 
                                        
                                            Informatik ekonómom? Ťažko.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Erik Skácal
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Erik Skácal
            
         
        skacal.blog.sme.sk (rss)
         
                                     
     
        Viem, že nenájdem spôsob ako všetky problémy riešiť, ale aspoň na ne upozorním a nezatváram pred nimi oči.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    706
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




