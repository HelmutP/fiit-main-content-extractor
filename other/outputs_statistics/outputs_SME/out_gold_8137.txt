

 Keď ma stárka uvidela zaslzili sa jej oči, bola nesmierne šťastná. Objala ma oslovujúc. 
 - Syn môj. - 
 Tak ako ona i ja som bol nesmierne šťastný. Veď bol som s rodinou, ktorá mi nesmierne chýbala. Odrazu zavládlo medzi nami porozumenie, láska a pohoda. Zhovárali sme sa, popíjali kávu a všetko bolo opäť tak ako za starých čias. Niekedy sa rodinné, či iné spory vedia vyostriť na ostrie noža. V tej chvíli zabúdame na svojich blízkych, hnev nás viac ovláda ako láska. 
 Som človek, ktorý sa nedokáže hnevať, aj keď sa hnevá, tak len na pár mnút. Celý dôvod prečo sme sa tak dlho nevideli, čo starú mamu  /veľmi bolelo  i bolí / je, že ktosi nemenovaný v  rodine nevie odpúštať. Sú nezhody a sú rodinné zvady, a  však človek je preto človekom, lebo nosí v hrudi srdce,  dokáže tak nielen milovať, ale odpúštať. Žiaľ sú ľudia, ktorí to v živote nedokážu, čo je im na škodu. 
 Pri pohľade na na rodinu,keď som predstavil poprvýkrát starkej svoju priateľku, závládol vo mne akýsi pokoj, pohoda, šťastie a láska zároveň. Áno láska, je prameň živá voda, ktorá nás drží nad vodou. Drží nás pokope nielen ako ľudí, ale hlavne ako rodinu. Uvažujúc čím by sme boli  bez lásky prichádzam k záveru, že láska je  večný chlieb pre všetkých. 
 Je nedeľa, z okna neďalekej dediny vidieť kostol a vo mne bijú akosi zvony šťastia, že rodina je opäť súdržná. Prvý krok sa urobil, treba zotrvať a kráčať na tejto ceste.  Láska je mocná čarodejka ako vraví klasik, má ozajtnú  moc a nielen to, láska prináša teplo do sŕdc a učí žiť a milovať. 

