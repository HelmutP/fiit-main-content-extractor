

 Taká suma peňazí bola naposledy videť ešte v časoch keď medzi sebou bojovali Karpov a Kasparov (a tí to tak či tak museli odovzdať vláde). V 90. rokoch klesli dotácie do šachu tak, že majstrovstvá sveta mali dotáciu "len" 100 000 dolárov. Tento rok bola celková dotácia 3 milióny Eur a z toho 2 milióny priamo na rozdelenie medzi hráčov. 
 Zápas sa hral v Sofii medzi vyzývateľom Topalovom z Bulharska - bývalým majstrom sveta - a Viswanathan Anandom z Indie, ktorý je majster sveta už tri roky. 
 Zápas začal dramaticky už pred začiatkom prvej partie. Anand na ceste z Madridu do Sofie mal medzipristátie vo Frankfurte. Kvôli sopke ale zrušili ďalšie lety a Anand sa nemal ako dostať do Sofie. Organizátori, ktorí sú samozrejme na strane Topalova ho odmietli čakať a Anand si musel cestu do Sofie zabezpečiť inak. Vlaky boli beznádejne vypredané a tak si prenajal minivan, dvoch šoférov a vyrazil do Sofie. Po 40 hodinách dorazil a po chvíli odpočinku začala prvá partia, ktorú aj prehral. 
 Po dramatickom priebehu dokázal ale Anand vyhrať celý zápas až v rozhodujúcej poslednej partii. 
 Na rozdiel od SME a vďaka zvyšujúcemu sa záujmu o šach vo svete, sledovalo zápas na internete okolo 100 miliónov ľudí zo 165 štátov sveta. Správy priniesli všetky dôležité noviny - napríklad The New York Times alebo Wall Street Journal. 
   
 
   
 GO 
 Zaujímavá správa je aj zo sveta GO. V profesionálnom svete GO sa hrajú iba turnaje o rôzne tituly ale neexistuje titul majstra sveta. Pravdepodobne sa ale uskutoční prvý zápas, ktorý bude obdobou takého zápasu. Najlepší čínsky hráč Gu Li a kórejský hráč Lee Sedol budú hrať jubango (zápas na 10 partií) s dotáciou 500 000 dolárov. 

