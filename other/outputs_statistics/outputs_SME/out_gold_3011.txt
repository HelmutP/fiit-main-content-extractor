

 Traja išli spolu. Najmladšia s ňou. Muž sám. Každý zvieral vo vrecku sviečku, zápalky. 
 Kráčala rozochvená s dcérkou za ruku. Električka. Chôdza od zástavky smerom k Hviezdoslavovmu námestiu. Plná obáv o muža, deti. O seba nie. Čo už ona môže stratiť? Ale deti? Muž?... Des ju zadúšal, ale odháňala ho. Tuho objímala zvlhnutou rukou pršteky svojej dcérky. V tom sa k nim pridal. Cítila ho dlhšie v pätách. Ulízaný, pretučnený, sviatočne nahodený, v bielej košeli ako na hodokvas. 
 „Kam idete? Na Hviezdoslavovo...?“ pridal sa k nim. Vyzvedal, s prešibaným úsmevom. 
 „Do toho vás nič... ale, áno, ideme tam!“ odvrkla a zrýchlila krok. Dcéra za ňou sotva stačila. 
 „Čo tam idete?... Viete, že je to nebezpečné...?“ nenechal sa odradiť chlap. Snažil sa žoviálne nadväzovať ďalší rozhovor, kontakt. 
 „Nech...!“ vyhŕkla, a dali sa s dcérou skoro do behu, aby sa od úlisníka odpútali. Cítila ho však za nimi aj naďalej. 
 Prišli k Národnému divadlu. Kordóny eštebákov v civile i v uniformách, množstvá áut milície, taxíkov. V blízkosti sa ozýval brechot psov. A spústa ľudí po uliciach, v patričnej úctivej diaľke od stredu, kde pred divadlom stála zhrčená do seba hŕstka tých, čo chceli čeliť násiliu, obkolesená nerovnými partnermi. Došli s dcérou k prvej skupine esenbákov, eštebákov. 
 „Pustite nás, chceme ísť k divadlu...!“ rozhodne povedala. 
 „Tam nemôžete!“ vyhŕkol jeden. 
 „Prečo?... My tam chceme ísť...!“ 
 „Nesmiete!“ už skoro zrúkol. 
 Ulízanec práve dorazil k partii, s ktorou sa hneď zbratal a začal sa rozprávať: „Servus, Mišo,“ pozdravil známeho. Začal sa s ním baviť. Vôbec si ich už nevšímal: ju a dcérku. 
 „Poď, Hanka!“ potiahla dcéru. 
 Rýchlo sa od chlapov, čo sa hurónsky rehotali, poberali preč. Našli medzeru medzi nimi a prepchali sa dopredu za kordón alkoholom páchnucich strážcov pokoja a poriadku. Bolo päť minút pred šiestou. Zastali medzi svojimi. Vybrali sviečku, zápalky. 
 A vtom to začalo: zavýjanie sirén, hluk, húkanie, trúbenie áut, brechot psov, pohyb áut do ľudí, údery pelendrekov do občanov, ktorí prišli hájiť práva svoje i tých, čo zbabelo trčali doma a čakali, čo bude a ako sa to vyvinie, alebo čo nemali záujem. Prúdy špinavej vody z polievača dopadli na jej kabát. Oťažel. Bola jej zima. Padal mrholivý drobný dážď a vlhkosť zaliezala všade. Prenikla k roztrasenému telu a rozochvenej duši. 
 „Drž sviečku, Hanka...! Chráň plamienok dlaňou...! 
 „Otče náš, ktorý...,“ ozývalo sa z pier slabých na námestí, ohlušenom rachotom bezcitných mocných. 
 Horko-ťažko dokončili modlitbu, spev. Potom ozbrojené prilby s pelendrekmi ich vytláčali z námestia, mlátili, tisli, oblievali vodou z polievačov.  Smradľavý alkoholový dych žoldnierov ich zarážal. V skrumáži tiel, ktoré sa posunovali ťažko dozadu schmatla dcérku, aby sa v dave nestratila. Sviečky nedohorené ležali na zemi. Stúpalo sa po nich. Ozbrojenci. Aj tí, čo pred chvíľou, takou úžasnou, ktorú  nezabúda do smrti ani rozum ani srdce, držali ich rozžiarené blikajúce v hlbokej tme... 
   
 O niekoľko rokov potom. Televízna politická relácia KROKY. KROKY. Koľko ráz ich sledovala?  Všetky! Všetky, čo boli vysielané v STV. Nenechala si ich nikdy ujsť. Chcela vidieť na vlastné oči, počuť na vlastné uši, tých v koalícii i tých v opozícii. Utvárala si názor na politikov, na ich činy, na všetko okolo seba. Raz sa zarazila pri pohľade na obrazovku. Hlavná téma diskusie bola o vnútre, o verejnom poriadku a o bezpečnosti občanov v štáte. Predstavitelia polície. Zbystrila uši, zrak. 
 Z obrazovky sa na ňu díval „ulízanec“ spred pár rokov. O voľačo starší. Načúvala jeho reči, jeho akoby vypľúvané, šušlavé slová, vety. Akoby ich pľul, ako vtedy, keď sa jej pýtal, či idú na Hviezdoslavovo. Prečo má takú dobrú pamäť? Prečo si pamätá  tváre ľudí, ľudí, ktorých v živote stretáva? 
 Túto tvár videla v roku l988 na ulici, keď sa diali udalosti, ktoré začali hýbať dejinami, v roku 1996 na obrazovke, akože demokratickej STV. A dnes – kde je tá tvár?... 
   

