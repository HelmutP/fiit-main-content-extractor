

 Utorok zhruba 11:00. Návaly do hlavy, neschopnosť rozmýšľať, nevoľnosť, strata orientácie, panika. Cesta domov 39tkou, v lekárni mi nechcú odmerať tlak "lebo všetky tlakomery majú zabalené". Tak poliklinika, horný mierne zvýšený, magnézium. Doma citový záchvat, plač, tŕpnu mi ruky až v nich nedokážem udržať telefón a zavolať sanitku. Našťastie akurát prichádza brat. 
 Utorok 17:45. Nález psychiatričky z Antolskej: "v úvode vyštrenia pozorovať na pacientovi napätie, v priebehu vyšetrenia sa ukludnil, na kladené otázky odpovedá adekvátne, bez latencie, orientovaný kompletne správne, afektívna labilita, prchavá lakrimozita, nálada úzkostnejšia, reaktívne podmienená rozchodom z priateľkou, myslenie koherentné, v tempe primerané, obsahovo bez bludov, bez suicidálnych myšlienok a úvah, poruchy vnímania neprítomné, mnestické funkcie a intelekt orientačne v norme, osobnosť sa javí ako premorbídne abnormne štrukturovaná, bez psychotickej alterácie stavu". Injekcia do zadku, magnézium b6 a diazepam. Na doma Neurol tri krát denne, ďalšie magnézium, ale toto sa našťastie pije. 
 Práceneschopnosť: sa uvidí dokedy. Maličkosti (napr. nákup) mi teraz robia veľké problémy. Bez liekov je to pomerne zlé, s liekmi väčšinou spím. 
 Ponaučenie z celého: treba si ľudia na seba fakt dávať pozor. Časté stresy sa dokážu v podvedomí nahromadiť, a potom to môže raz zle dopadnúť. 

