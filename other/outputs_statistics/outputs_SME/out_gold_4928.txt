

   
  V doline, ktorú vytvára toto pohorie leží mesto Turzovka. Je bohaté nielen svojou históriou, ale i významnými osobnosťami, ktoré sa v nej narodili. Medzi ne patrí: Rudolf Jašík, slovenský spisovateľ; Ján Haranta, básnik, kňaz a predstaviteľ Katolíckej moderny; Ondrej Zimka, akademický maliar; Vladimír Gajdičiar, paraolympijský víťaz a mnoho ďalších.  
  Mesto Turzovka sa v súčasnosti dostáva do popredia,  najmä mariánskym pútnikom. Na rozhraní medzi Turzovkou a obcou Korňa na hore Živčákova sa nachádza mariánske pútnicke miesto.  
  Hájnik Matúš Lašút kráča horou ako každý iný deň. Bola nedeľa a zároveň sviatok Najsvätejšej Trojice, 1. jún 1958. Vie, že by mal byť radšej v kostole, ale práca nepustí. Na znak úcty sa rozhodne, že sa pomodlí. Zazrel záblesk. Vo vzdialenosti asi 12 metrov a vo výške 2 metrov uvidel Pannu Máriu, ktorá mu zjavila „sedem obrazov“.  
  Matúša Lašúta prehlásili za odporcu režimu, bol väznený a umiestnený v psychiatrickej liečebni. Ľudia však na miesto zjavenia prichádzali a modlili sa. Na začiatku postavili malý oltárik, neskôr kaplnku. Na mieste zjavenia vytryskli zo zeme tri pramene.  
  Voda z prameňov pomohla vyliečiť zdravotné problémy mnohým ľuďom. Veľakrát sa stáva, že na vodu zo Živčákovej chodia ľudia z ďaleka. Nie iba voda, ale aj celá hora dáva pútnikom telesnú, ale najmä duchovnú silu. Nie je nič nezvyčajné stretnúť na Živčákovej bosých pútnikov. Chodia pešo tri kilometre nielen v lete, ale i v zime počas mrazov. Nikde v okolí sa pri vyznávaní hriechov tak neotvárajú srdcia veriacich ako práve tu.  
  V súčasnosti sa na Živčákovej stavia Chrám Panny Márie, Matky Cirkvi, ktorý bude mať kapacitu 2 000 veriacich. Vedľa neho bude budova exercično - rekreačného zariadenia pre kňazov i veriacich.  Bude slúžiť pre duchovnú správu pútnického miesta, to znamená, že po dostavaní obidvoch objektov duchovnú správu preberie niektorá rehoľa. 
  Pozitívne sa o Turzovke vyjadril blahoslavený páter Pio, ktorý v liste slovenskému jezuitovi p. Mikušovi napísal: „Turzovka je pravé zjavenie. To budú raz Slovenské Lurdy!“ 
   

