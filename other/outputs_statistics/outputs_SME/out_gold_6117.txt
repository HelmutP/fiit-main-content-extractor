

 Na blog píšem od minulého roka. Prvý článok som so strachom zverejnila v auguste. Odvtedy už pretieklo mojou klávesnicou veľa písmenok a tých článočkov je už celý zoznam. Moja maminka sa o môj blog živo zaujíma. Má však problém, nie je majiteľkou počítača a tak si moje príspevky číta, keď je u nás, (žijeme v rôznych mestách) alebo jej občas zvykne môj bráško čosi z nich vytlačiť a odovzdá jej ich pekne do ruky. 
 Predvčerom maminka dostala oznámenie, že na pošte má balíček. Von lialo, ale zvedavosť zvíťazila. Premohla nechuť vytrčiť nos do takého nečasu  a vyzbrojená veľkým dáždnikom vyrazila na poštu. Prevzala si balíček nezanedbateľnej veľkosti a hmotnosti. Prekvapene hľadela na odosielateľa. Bol ním jej syn, avízo však žiadne od neho nemala, takže prekvapenie na svojej záhadnosti gradovalo. 
 Vravela mi, že domov letela ako raketa, zvedavosť dávala jej nohám silu a rýchlosť. Doma, nechajúc kvapkajúci dáždnik v chodbe, v topánkach, oblečená sa hádzala za nožnicami, aby mohla ukojiť svoju, každou sekundou, zvyšujúcu sa  zvedavosť. 
 Otvorila balík a vraj sa jej podlomili kolená. Pred ňou ležala veľká modrá kniha s písmenami, ktoré vytvárali moje meno. Začala listovať. Krásny lesklý kriedový papier, ilustrácie. Môj blog... Všetko, čo som doposiaľ na blogu zverejnila, všetko má v onej knihe. Vymyslel to bráško, vraj, keď nemá internet, bude mať knihu! 
 Maminka mi samozrejme hneď telefonovala, aby mi zvestovala, čo drží v ruke. Chvíľu mi trvalo, než som pochopila o čo ide. Ostala som stáť s mobilom na uchu,  ticho, v nemom úžase. Len som cítila, že ma nejako začali štípať oči a že mi čosi tečie dole lícom. Bola som dojatá. 
 Mám knihu! Prvú (a poslednú, cha) knihu! Krásnu, veľkú, má vraj tristo strán. Videla som ju zatiaľ len vďaka ememeske. Už tri dni sa vytešujem a užívam si ten pocit.  Pocit, že mám super brata, pocit, že mám knihu! Bez vydavateľa, bez starostí, bez ... jednoducho len tak! Jupí... 
 Viem, že to znie ako chválenie sa, no tí, čo ma už ako-tak z blogu poznajú, vedia, že nie kvôli chváleniu  som toto napísala. 
  Chcem aj touto cestou môjmu milému výmyselníkovi, môjmu skvelému bráškovi poďakovať. 
 Peťulo, mám Ťa veľmi rada a ďakujem Ti! 
 Cmuk 
   
 Foto: zdroj internet 

