
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lubo Kužela
                                        &gt;
                Ekonomika
                     
                 Peniaze, inflácia a zázračné tlačiarne na peniaze.. 

        
            
                                    6.5.2010
            o
            16:51
                        (upravené
                25.11.2014
                o
                12:10)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            1029-krát
                    
         
     
         
             

                 
                    Nedávno ma zaujala diskusia Rona Paula so šéfom FEDu Benom Bernankeom. Viac o jeho názoroch ponúkam v mojom preklade článku Sound Money. 
                 

                 Henry Ford kedysi povedal: "Je dobre, že ľudia nerozumejú nášmu bankovému a menovému systému, lebo ak by rozumeli, som presvedčený, že by do rána vypukla revolúcia."   Ste zmätení ako všetci hovoria o menovej politike, nekrytých peniazoch a inflácii? Nie ste sami. Bankári a politici pracovali ruka v ruke  mnoho desaťročí na zakrývaní svojich činností pred verejnosťou. Skrývajú sa za komplikovanou štruktúrou určenou k nafúknutiu peňažnej zásoby pri vytváraní falošného dojmu, že robia všetko najlepšie vo verejnom záujme.   Inflácia je veľmi jednoduchý koncept na pochopenie: Viac peňazí = nižšia hodnota. To sa môže zdať rozporuplné, ale je to veľmi jednoduché.   Pre ilustráciu si predstavme príbeh: V jedno krásne ráno sa môžete prebudiť a uvedomiť si, že vlastníte dvakrát toľko hotovosti, ako ste mali včera v noci. Zázrační peňažní škriatkovia vstúpili do vášho domova a bankového účtu a jednoducho zdvojnásobili celú hotovosť. Teraz ste dvakrát taký bohatý ako predtým.   Ale  čoskoro si uvedomíte, že to isté sa stalo všetkým v tejto krajine. Peňažné zásoby (celkové množstvo peňazí) sa zdvojnásobili! Je to len jednorázová udalosť a váš pravidelný príjem zostáva rovnaký. Proste jednorázové šťastie. Je pekné snívať, tak snívajte so mnou.   Čo bude ďalej? Ak ste ako väčšina ľudí, pravdepodobne ste začali utrácať. Môžete nakúpiť veci, ktoré ste vždy chceli kúpiť, ale nemohli ste si ich dovoliť. Môžete splatiť niektoré dlhy. Môžete nakupovať akcie. Inými slovami, môžete dať nové peniaze do obehu. Tak ako väčšina ostatných ľudí v krajine.     Dopyt po mnohých výrobkov sa zvyšuje, pretože oveľa viac ľudí si môže dovoliť  teraz nakupovať. Spotrebitelia kupujú toľko vecí, že sa vyskytujú nedostatky niektorých tovarov. Keďže sú tovary nedostatkové, obchody a podniky sa rozhodnú zvýšiť ich ceny. Akonáhle ceny pôjdu hore, bude menej ľudí, čo si dovolia kúpiť rovnaké výrobky, a situácia sa vráti do normálneho stavu.   Ako vedľajší účinok týchto vyšších cien, majitelia obchodov začnú zarábať vyššie zisky, než je obvyklé. Majú viac peňazí na ich bankových účtoch, čo im umožňuje zvýšiť svoje výdavky. Budú investovať do nových akcií alebo rozšírovať svoje podnikanie. Mohli by aj vyplácať dividendy svojim investorom a bonusy svojim zamestnancom, a umožniť týmto ľuďom kúpiť viac tovarov. Tento dodatočný dopyt kladie ešte väčší tlak na ďalšie zvyšovanie cien.   O niekoľko mesiacov neskôr sa zvýšia ceny takmer všetkého. Dodávatelia a výrobcovia sa stretávajú s príliš prudkým rastom dopytu zo strany svojich klientov, takže aj oni sa môžu rozhodnúť začať dodávať a vyrábať viac.   Teraz pochytíte  nákupnú horúčku a pozrite sa, čo sa stalo: Váš dôchodok (príjem) zostal rovnaký, ale po niekoľkých týždňoch si už nemôžete kúpiť rovnaké množstvo tovaru, pretože všetky ceny v ekonomike išli hore (inflácia).   Samozrejme, môžete žiadať vyšší plat od svojho zamestnávateľa. Ak ste samostatne zárobkovo činná osoba, musíte účtovať svojim zákazníkom viac peňazí len preto, aby ste si mohli zachovať svoj životný štandard a zaplatiť ľudí. Všetci ostatní sú v rovnakej situácii. Vyššie ceny sa šíria  po celej ekonomike, a je stále ťažšie a ťažšie sa uživiť (kúpyschopnosť klesá).   Vidíte, ako to šťastie, jednorázová udalosť, ktorá najprv vyzerala tak vzrušujúco, bola krajne škodlivá nielen pre vás, ale pre celú krajinu? Mohli ste  mať istý čas naozaj dobré obdobie, ale teraz ste na tom horšie než predtým. V našom príbehu je teraz v obehu dvakrát toľko dolárov, ale vaše príjmy zostávajú rovnaké a každý dolár, čo si zarobíte, stojí len asi polovicu toho, ako to bolo predtým. Môžete iba dúfať, že sa "finananční škriatkovia" vrátia.   V skutočnosti, niektorí ľudia, firmy a banky vytvorili  "peňažných škriatkov", ktorí im umožnia získať nové peniaze na ich bankové účty, kedykoľvek budú chcieť. Peniaze sú oficiálne pôžička (kredit), ale vedia, že ju nikdy nebudú musieť splatiť. Budú dlh proste nabalovať, čím vytvoria ešte väčší dlh. So všetkými tými ľahko "zarobenými" peniazmi na svojich účtoch, a po správach v televízii, že trhy idú iba hore, a že ceny nehnuteľností budú aj naďalej stúpať večne,  majú tendenciu byť trochu slabomyseľní a robiť zlé investičné rozhodnutia. Vedia, že ak sa niečo stane s ich investíciami, príde pomoc zo strany vlády, a tak neváhajú vziať na seba obrovské riziká.   Prestaňme snívať a pozrime sa na realitu. Čo keby som vám povedal, že títo "finanční škriatkovia" existujú, a že sa vracajú nie len raz za život, ale raz za pár týždňov? A že opakovane dávajú peniaze ich najbližším priateľom, ale nie vám? Že ceny idú hore, pretože celkový objem peňazí v obehu sa zvýši, ale vám neostáva veľa dôvodov na úsmev?   No, to je inflácia. Kto má prospech z inflácie? Iba tí, ktorí sú na vrchole pyramídy a prijímajú všetky tie nové peniaze priamo od zdroja. Ako ste si mohli myslieť, zdroj je Federálny rezervný systém, a jeho príjemcami sú vláda, ktorá si "požičia" každý rok veľa nových peňazí, bez úmyslu ich niekedy splácať späť. Ďalším príjemcom týchto peňazí sú skrachované banky, ktorým štát "finančne vypomohol" pre dobro "hospodárstvo". Alebo sa používajú na vojenské účely, na vybudovanie našej armády, takže môžeme mať stálu prítomnosť po celom svete a bojovať v nekončných a zbytočných vojnách. Dokonca sú tam aj obrovské množstvá malých príjemcov, ktorí dostali spotrebiteľské úvery a hypotéky, a ktorí ich nikdy nebudú schopní splatiť.   Čo sú teda nekryté peniaze? Je to presne to, o čom sme práve hovorili: peniaze, ktoré môžu byť  navýšené  stlačením jediného tlačidla po priklepnutí  mocnej osoby alebo organizácie. V dnešnej dobe je väčšina amerických dolárov len číslami na obrazovke počítača a je  veľmi jednoduché pre Federálny rezervný systém vytvoriť peniaze „zo vzduchu", kedykoľvek budú chcieť.   Keď  boli peniaze kryté zlatom a striebrom, ľudia nemohli iba sedieť v nejakom vymyslenom úrade a "stláčať tlačidlo" pre vytvorenie nových peňazí. Museli sa zapojiť do poctivého obchodovania s inou stranou, ktorá má zlato v držbe. Prípadne riskovať svoje životy a majetok s cieľom nájsť vhodné miesto pre vybudovanie zlatých dolov, zašpiniť sa a spotiť, a vykopať zlato. Nič také, čo by som si si predstavil ako našich "peňažných škriatkov" vo  Fede, ktorý prídu, kedykoľvek majú pocit potreby hrať sa na Boha s ekonomikou.   Ako môžete vidieť, inflácia a nekryté peňiaze sú veľmi zvodné a prospešné pre tých hore, a veľmi nebezpečné pre všetkých ostatných, a národ ako celok. To je presne to, čo Henry Ford hovoril. Vedel, že každá krajina, ktorá sa spolieha príliš na nekryté peňiaze je v troskách, skôr alebo neskôr.   Je len jedno  možné riešenie problému inflácie: Stop vytváraniu peňazí zo vzduchu. Ale my sme už v takom neporiadku, že jediný spôsob, ako mať reálny vplyv na množstvo peňazí v obehu je zvýšenie úrokových sadzieb tak, aby ľudia splácali svoje úvery a požičiavali si menej peňazí z bánk, čo znižuje množstvo peňazí v obehu. Je však možné, vyššie úrokové sadzby môžu priniesť krach ekonomiky. Takže "riešenia" Fedu na prekonanie inflácie sú... vytvoriť ešte viac peňazí.   Nekrytie peňazí je nebezpečná závislosť. Aj keby Fed našiel spôsob ako zastaviť infláciu, pokiaľ bude súčasný systém pretrvávať, bude tam vždy pokušenie znovu jednoducho stlačiť tlačidlo "tlačiť peniaze". To je dôvod, prečo potrebujeme späť zlatý štandard a zrušiť Federálny rezervný systém.   Ale to sa nestane "do rána", ako povedal Henry Ford, dokonca ani tento roku Ron Paul je presvedčený, že prvý krok smerom k menovej slobode, je umožniť otvorenú konkurenciu v oblasti mien. Federálny rezervný systém bude mať potom veľkú motiváciu zostať úprimní a udržať hodnotu dolára, pretože ak tak neurobí, jednoducho stratí všetkých svojich zákazníkov.   Originally published at RonPaul.com. (Autor písomne súhlasil s publikovaním článku.)       V tomto videu sa Paul Alan pýta šéfu FED-u, odkiaľ zobral FED peniaze na pomoc Grécku:              Ron Paul je zástancom zlatého štandardu a otvorenej hospodárskej súťaže v menách. Je známy svojimi diskusiami s Alanom Greenspanom a Benom Bernankeom o fungovaní Fedu.   Paul reprezentuje libertarianske krídlo Republikánskej strany. Pravidelne hlasuje proti programom všetkých štátnych výdajov, čím občas dráždi svojich spolustraníkov.   V ekonomickej oblasti podporuje zrušenie dane z príjmov, väčšiny vládnych úradov a Federálneho rezervného systému. Je zástancom opätovného zavedenia zlatého štandardu. Je odporcom ilegálnej imigrácie a podporuje zvýšenie ochrany hraníc, s čím mnoho libertariánov nesúhlasí.       Ćlánok je mojim prekladom článku Sound Money.   Poznámka: Autorom článku nie je Ron Paul   Záver je prevažne z Wikipedie.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Excel: Načo je tlačidlo "Scroll lock"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Excel: Fotka ako komentár bunky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Predpovede pohybu akciového trhu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Filtrovanie pošty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lubo Kužela 
                                        
                                            Pár technických vtipov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lubo Kužela
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lubo Kužela
            
         
        kuzela.blog.sme.sk (rss)
         
                                     
     
         Pracujem ako konzultant v IT. Zaujímam sa o technológie a ekonómiu. 
 
V prípade záujmu pomôzem s Excelom, prípadne naučím vybranú oblasť alebo spracujem zadanie. 

  
makrá, kontingenčné tabuľky, grafy, reporty, import údajov, databázy, výpočty, prehľadávanie a analýza dát.. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    61
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2778
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Informatika
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Veda
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Denník ítečkára :))
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Steve Wozniak - iWoz
                                     
                                                                             
                                            Dan Ariely - Jak drahé je zdarma
                                     
                                                                             
                                            Randall Stross - Planeta Google
                                     
                                                                             
                                            Leander Kahney - Jak myslí Steve Jobs
                                     
                                                                             
                                            Nick Mason - Pink Floyd
                                     
                                                                             
                                            Liker - Tak to dela Toyota
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            RADIO_FM
                                     
                                                                             
                                            Julee Cruise
                                     
                                                                             
                                            Belle and Sebastian
                                     
                                                                             
                                            I AM X
                                     
                                                                             
                                            RADUZA
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            LenTak
                                     
                                                                             
                                            Pinus@Chicago
                                     
                                                                             
                                            EcoNoir
                                     
                                                                             
                                            Rado Baťo
                                     
                                                                             
                                            Marek Psenak
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            HN Online - Stlpceky
                                     
                                                                             
                                            Radio_FM - Playlist
                                     
                                                                             
                                            eTREND - Odbornejsie blogy
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




