
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Vrátny
                                        &gt;
                Nezaradené
                     
                 Môj Prvý máj z pred polstoročia (4) 

        
            
                                    2.5.2010
            o
            11:04
                        (upravené
                27.5.2010
                o
                5:02)
                        |
            Karma článku:
                6.43
            |
            Prečítané 
            721-krát
                    
         
     
         
             

                 
                    Bolo to na Prvého mája, čo som sa dostal k prvému nožíku v živote.
                 

                 Moja dobrá mama dlho mi bránila, aby som mal nožík, stále mala obavy, že sa porežem a vlastne aj doma sme mali všetky nože tupé, len aby sme sa nedajbože neporezali. Ten nožík v tvare rybičky, ktorý som si jedného prvomájového dňa našiel na ihrisku, mama najprv skontrolovala, či je dostatočne tupý a keďže bol, aj mi zostal. Tak som sa dopracoval do riadneho stavu chlapčenského, lebo v tých dobách ešte stále platilo, že poriadny chalan musel mať vo vrecku nožík a špagát. „Časy sa zmenili. Dnes je to už mobil, prezervatív a  často aj injekčná striekačka," povedal mi lakonicky nedávno o dve generácie mladší známy, keď som mu o tom už ani neviem pri akej príležitosti rozprával. Čo sa dá robiť, verím mu. Nech mi slúži ku cti, že som sa dokázal, hoci len čiastočne, prispôsobiť. Mobil nosím vo vrecku stále aj ja.   U nás doma sa vo vzťahu k politickým sviatkom pestovala ideológia vlažnosti. Prvý máj ako sviatok práce sa však trávil sviatočne až do konca. Proste nerobili sa práce bežného dňa a nedeľná atmosféra mu zostala aj počas neskorého popoludnia a podvečer. Otec zorganizoval mariáš partiu so susedmi a ja som sledoval, ako mu stĺpiky hliníkových päť i desaťhaliernikov raz rastú, čo ma tešilo a napĺňalo sebavedomím, potom klesajú, čo ma skľučovalo. Vtedy som sa azda po prvýkrát začal dozvedať, že existuje čosi ako bezplatný zisk, či vykorisťovanie, keď sa niekoho stĺpik mincičiek začal dvíhať v očiach spoluhráčov okolo okrúhleho stola do závideniahodných výšok. Teraz to beriem ako súčasť dobromyseľného politického vtipkovania, ktoré sa u nás začalo rozmáhať medzi ľuďmi, ktorí si dôverovali, lebo im pomáhalo prekonávať frustrácie z nemožnosti vydiskutovať si otvorene vlastné názory, či pochybnosti. Partie to boli vyrovnané, ten stĺpik pred susedmi striedavo rástol i upadal a tak sa stále ktosi iný dostával na predstieraný ideologický pranier.   Keď sa susedia nepridali, tak sme mali rodinného žolíka alebo sa jednoducho vyprávalo. To bolo pred príchodom televízie a tuším sme sa viac rozprávali medzi sebou v rodine i so susedmi. Prvomájové oslavy, alebo prejavy, čo odzneli, nikdy nebývali témou, ja som sa na to nepýtal a tak som sa ani nikdy nedozvedel, čo si o tom všetkom mysleli dospeláci našej rodiny. Keď sa však teraz nad tým zamyslím, neviem sa ubrániť dojmu, že ma naši z ohľaduplnosti nechceli zaťažovať svojimi úvahami, aby zo mňa nevyrastala dezorientovaná, rozpoltená osobnosť. Možno si hovorili, že k tomu bude dosť príležitostí neskôr, keď dúfali, že už budem uvážlivejší a odolnejší, ba možno sa aj báli, aby som sa v nevhodnej spoločnosti nepreriekol a nedostal pred nastúpenou pionierskou skupinou pokarhanie a na vysvedčení trojku z chovania. Do polepšovne, pokiaľ sa dobre pamätám, sa za ideologicky vadné reči deti neposielali, ani zo základnej školy nevyliali, však bola povinná a kárne opatrenie z predchádzajúcej vety stačilo. Zato rodičia v práci si mohli užiť kádrovania za to, že nedokázali, či nechceli viesť svoje ratolesti k láske vlasti sovietov a k tej našej... Ale nechajme tieto pochmúrne úvahy, aby sme si nekazili sviatočnú náladu radostného Prvého mája.   Čas pokročil a to sa už Prvý máj pomaly schyľoval do ďalšieho bežného dňa. Mama nariadila povinnú modlitbu, ktorá sa večer končila poďakovaním za to, že som deň šťastlivo prežil a prosbičkou, aby anjeličku môj strážničku opatroval moju dušičku, aby bola čistá... Aj zuby som si umyl dočista, kefkou, lebo tú, žiaľ, žiadna modlitba, čo ako vrúcne mienená, nemohla a nesmela nahradiť, a šlo sa do postele. Na druhý deň bolo treba skoro vstať, ísť miništrovať na rannú omšu o šiestej, potom stihnúť ešte návrat domov, rýchle raňajky a hajde do školy učiť sa budovať lepšiu spoločnosť.    Súvisiace články:   http://vratny.blog.sme.sk/c/227048/Moj-Prvy-maj-z-pred-polstorocia-1.html   http://vratny.blog.sme.sk/c/227233/Moj-Prvy-maj-z-pred-polstorocia-2.html   http://vratny.blog.sme.sk/c/227382/Moj-Prvy-maj-z-pred-polstorocia-3.html  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Prechádzka po Lutherovom meste Wittenberg
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Slnečný babioletný deň v Drážďanoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Nagymaros-Visegrád v neskoré leto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Na tému Ukrajiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Spomienka na Veľkú  vojnu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Vrátny
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Vrátny
            
         
        vratny.blog.sme.sk (rss)
         
                        VIP
                             
     
        V živote som vystriedal viacero bydlísk i povolaní a som teda z každého rožku trošku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    211
                
                
                    Celková karma
                    
                                                5.73
                    
                
                
                    Priemerná čítanosť
                    1476
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Inde v Európe
                        
                     
                                     
                        
                            Ázia
                        
                     
                                     
                        
                            Nepriateľská osoba
                        
                     
                                     
                        
                            Britské ostrovy
                        
                     
                                     
                        
                            Osudy
                        
                     
                                     
                        
                            U nás na Slovensku
                        
                     
                                     
                        
                            U susedov
                        
                     
                                     
                        
                            USA, Kanada
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na tému Ukrajiny
                     
                                                         
                       Čarnogurský: tragédia Putinovho hnedého mužíčka
                     
                                                         
                       Migranti v mojej obývačke
                     
                                                         
                       Sedem dobrých rokov skončilo
                     
                                                         
                       Fragmenty týždňa
                     
                                                         
                       O alkoholikovi, ktorý namaľoval obraz za $ 140 miliónov (a jeho múzeu).
                     
                                                         
                       Slovensko sa opäť zviditelnilo
                     
                                                         
                       Nie, nemusíte jesť špekáčiky zo separátu, vy ich jesť chcete
                     
                                                         
                       Najkrajší primátor najkrajšieho mesta
                     
                                                         
                       Letný pozdrav Sociálnej poisťovni
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




