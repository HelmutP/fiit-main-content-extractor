

 V Žiline sa ide 19.4.2010 zase schvaľovať rozpočet na rok 2010. A podľa všetkého to teraz sa aj podarí. Napriek tomu, že je tam jasné mrhanie: 
 Na hokej mesto dá cca dvojnásobok oproti minulému roku - 1 122 000 EUR!!! Čo je 33,8 milióna korún! Minulý rok to bolo 597 000 EUR - skoro 18 miliónov korún!!! Hokejsti urobili hanbu, skoro vypadli, zachránili sa len v baráži proti Piešťanom a na poslednom zápase bolo 811 divákov. Super investícia obrovských miliónov korún, keď návštevy presahujúce aspoň 1000 ľudí neboli bežné. To máme už asi úplne geniálne opravené chodníky, cesty, lacnú a pohodlnú MHD, veľa zelene v meste a dostatok parkovacích plôch v centre, že sa dá takto mrhať peniazmi? 
 A samozrejme, keďže je predvolebný rok, napriek cca 20 percentnému prepadu v daniach treba zvýšiť platy v meste o cca 15 percent, nech úradníci a zamestnanci vedia, koho voliť. A treba aj trochu propagandy za naše dane - výdavky súvisiace s propagáciou, reklamou, inzerciou mesta (v tom hovorca mesta 100 000 €) 
 A aby sa to dalo nejako zaplatiť, mesto naplánuje predaj pozemkov za 2 milióny EUR - cca 60 miliónov korún. Rozpredávanie mesta vraj ukončil Slota a mesto už vraj nič nemalo. Ale predsa len sa našli pozemky za 60 miliónov korún, ktoré sa dajú pekne pred voľbami predať. 
 A samozrejme pozor - naplánované je aj: Pokuty, penále a iné sankcie 200 000 € - Príjmy z pokút uložených Mestskou políciou za porušenie predpisov. V minulom roku to bolo 99 582€, čiže sa máme na čo tešiť! 
   
 Pre našich poslancov a primátora Harmana pár ukážok, ako to v meste vyzerá, kde by sa tie peniaze teda oveľa viac hodili: 
 Zeleň sa likviduje parkovaním na chodníkoch, po ktorých sa samozrejme už pomaly nedá chodiť. Ale narobiť poriadok s Parkovacou spoločnosťou sa nijako nedarí, napriek tomu, že mesto v nej vlastní väčšinu. Samozrejme ľudia parkujú na chodníkoch zadarmo radšej, ako na parkovisku vedľa za kopu peňazí. A tak máme často prázdne parkoviská a plné chodníky. 
  
 Zdevastované mesto... 
   
  
   
  
 Super sa tade chodí, denne to tam tak na Hurbanovej ulici vyzerá a deti na gymnázium chodia už radšej po ceste. Mestskí policajti samozrejme majú iné problémy... 
   
  
 ... vo dvojici bežne robia živú rampu - strážia jeden z vjazdov do centra mesta. 
   
  
 Aspoň tá neschopnosť mesta rozvíja ľudskú tvorivosť :) 
   
  
 Takto sa musia obyvatelia Žiliny brániť, ak nechcú mať rozorané aj tie posledné kúsky zelene. 
   
  
 Radosť ísť po takom chodníku, vedľa krásnej zelene. 
   
  
 Takto na mnohých miestach pomohli zábrany proti parkovaniu - ten kvetináč vpravo. Autá zavadzajú o toľko viac chodcom. 
   
  
 Takto vyzeralo Hlinkovo námestie pred likvidáciou zelene na ňom, aby sa dal zásobovať nový obchodný dom... 
   
  
 ... a takto je to tam teraz. Stromy sa ale samozrejme nerúbali, nie sme barbari! Presadili sa pekne mimo mesta ku vodnému dielu! To zeleni v centre super pomohlo! 
   
  
 Takéto ulice sú často v strede mesta. 
   

