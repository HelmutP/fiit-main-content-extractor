
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Predstavujem Vám môj tím 

        
            
                                    20.1.2009
            o
            9:49
                        |
            Karma článku:
                11.80
            |
            Prečítané 
            34167-krát
                    
         
     
         
             

                 
                    Pred dvomi mesiacmi som oznámil vznik prípravného výboru, ktorý pracuje na založení politickej strany s názvom Sloboda a Solidarita, v skratke SAS. Našou najvyššou hodnotou je osobná a ekonomická sloboda jednotlivca pri dodržaní základnej miery solidarity. Našim cieľom je byť v roku 2010 v parlamente a v roku 2014 vo vláde.
                 

                     Na výzvu pridať sa k nám reagovalo vyše 400 ľudí, z ktorých prípravný výbor vybral 100 zakladajúcich členov. Z nich sme zostavili tím ľudí, s ktorými tiahneme do boja za správnu vec. O každom jednom z nich som presvedčený, že majú potrebnú odbornosť a dobrú povesť a že im ide o to, aby táto krajina bola o niečo lepšia.   Ako nižšie uvidíte, z 20 tematických oblastí je zatiaľ 7 neobsadených. Preto pokiaľ má niekto záujem sa k nám pridať, má patričné vzdelanie a nemá žiadnych kostlivcov v skrini, je vítaný.   Momentálne píšeme naše stanovy, tvoríme organizačnú štruktúru a do dvoch týždňov podá prípravný výbor žiadosť o registráciu strany Sloboda a Solidarita. Podpisov máme vyše 6 000 z potrebných 10 000, naše úsilie preto patrí tieto dni najmä im.   Ďakujem všetkým, ktorí nám pomáhajú s podpismi. Viem, že veľa ľudí má u seba rozpísané hárky a prosím všetkých, aby ich čím skôr poslali na moju adresu (Richard Sulík, Kýčerského 2, 811 05 Bratislava) alebo na adresu uvedenú na hárku (Sloboda a Solidarita, P. O. BOX 152, 820 14 Bratislava 214). Kto by chcel prispieť svojim podpisom a podpismi svojej rodiny a priateľov, nájde hárok tu.   Predtým ako prejdem k jednotlivým menám môjho tímu, by som ešte rád zaujal stanovisko k jednej klebete, podľa ktorej nás vraj financuje Juraj Široký. Nuž, od pána Širokého by som si nenechal zaplatiť ani len kávu a nie to stranu a definitívne vylučujem, že by sme priamo či nepriamo dostali od neho čo len korunu, teda po novom Euro. Mimochodom, všetci naši darcovia budú uvedení na našej internetovej stránke, ktorú spustíme ku dňu registrácii strany.   Spolu s nižšie uvedeným tímom začíname tvoriť program, ktorý predstavíme po registrácii strany. Oceňujeme množstvo diskusných príspevkov na rôzne témy na Facebooku, kde medzičasom máme takmer tritisíc sympatizantov. Facebook je skvelá komunikačná platforma a budem rád, keď sa tam zaregistrujete aj Vy.   Členovia tímu zodpovední za jednotlivé tematické oblasti:   Ekonomika   Člen vedenia strany zodpovedný za ekonomické oblasti je Jozef Mihál   •1.       Verejná správa a eGovernment - Róbert Huran, 26r.   Róbert vyštudoval právo a téma jeho rigoróznej práce bola „Informačné systémy vo verejnej správe".  Rok pracoval na Ministerstve vnútra, kde sa venoval správe Schengenských informačných systémov medzinárodnej policajnej spolupráce a teraz pracuje na rôznych IT projektoch. Nebol členom žiadnej strany.   •2.       Verejné financie a finančný trh - Jozef Kollár, 47r.   Jozef je takmer celý svoj pracovný život bankárom. Po štúdiu ekonómie pracoval na ekonomickom inštitúte SAV, po revolúcii v ŠBČS a 13 rokov v predstavenstve Ľudovej banky, z toho jej 6 rokov šéfoval. Je autorom zákona o NBS. Dnes pracuje ako nezávislý finančný poradca. Nebol členom žiadnej strany.    •3.       Výmena peňazí medzi občanom a štátom - Jozef Mihál, 44r.   Jozef je jeden z mála ľudí, o ktorých som ochotný priznať, že do odvodov vidia lepšie než ja. Vyštudoval matfyz a do roku 2006 vyvíjal 15 rokov vo firme Aurus mzdové a personálne programy, kde bol zároveň spolumajiteľom. Dva roky bol poradcom ministra Zajaca. Od roku 2004 prednáša na tému dane a odvody. Bol členom strany Nádej.   •4.       Podnikateľské slobody - Martin Chren, 28r.   Martin je zosobnený strážca podnikateľskej slobody. Vyštudoval ekonómiu a momentálne sa pripravuje na doktorát. Od roku 1999 pracuje v Hayekovej nadácii a od roku 2004 je jej riaditeľom. Na európskej tripartite zastupuje Slovensko a je spolautorom publikácie Audit podnikateľského prostredia. Nebol členom žiadnej strany.   •5.       Hospodárstvo vrátane dopravy a energetiky - zatiaľ neobsadené   Zatiaľ síce nemáme teamleadra, ale po odbornej, nepolitickej stránke spolupracujeme s Jozefom Drahovským, expertom na dopravu, ktorý píše najmä na sme.sk   •6.       EÚ fondy - zatiaľ neobsadené   •7.       Poľnohospodárstvo - Ivan Holko, 36r.   Ivan je odborník na pohľadanie. Vyštudoval veterinu a na SAV si spravil doktorát. Počas 10 ročnej praxe pracoval na veterinárnych ústavoch v SR a ČR, na českom ministerstve zemědelství a bol asistentom poslanca Národnej rady. Momentálne pracuje ako odborný  asistent na Univerzite Tomáše Bati v Zlíne. Nebol členom žiadnej strany.   Spoločnosť   Člen vedenia strany zodpovedný za spoločenské oblasti je Róbert Mistrík   •8.       Sociálna politika a rómska problematika - zatiaľ neobsadené   Ani tu síce zatiaľ síce nemáme teamleadra, no v rámci tohto tímu sa bude drogovej problematike venovať spolupracujúci nečlen Vladimír Schwandtner, ktorý píše najmä na sme.sk   •9.       Zdravotníctvo - Peter Kalist, 33r.   Peter je lekár a manažér. Vyštudoval medicínu, má atestáciu z kardiológie a Master of Public Health. V rokoch 2004 - 2007 bol vedúcim krízového riadenia košického kraja. Od roku 2007 je kardiológom polikliniky ProCare/Pro Sanus a od roku 2008 aj jej riaditeľom. Nebol členom žiadnej strany.   •10.   Osobné slobody - Stanislav Fořt, 31r.   Stano je liberál telom a dušou. V rokoch 1995 až 2000 vyštudoval ekonómiu, rok bol v USA,  rok v Dánsku a momentálne študuje posledný ročník práva. Pracoval na burze cenných papierov a od roku 2002 je bratislavský agentúrny riaditeľ pre finančnú skupinu Wüstenrot. Je členom Mladých Liberálov a nebol členom žiadnej strany.   •11.   Školstvo a vzdelanie - Ivan Juráš, 47r.   Ivan vyštudoval medicínu a do roku 1991 bol sekundárny lekár na I. detskej klinike LFUK, Bratislava. Od roku 1990 je spolumajiteľ a člen viacerých spoločností pracujúcich v oblasti zdravotníctva. Od roku 2006 je zriadovateľ súkromnej školy pracujúcej pedagogikou Márie Montessori. Nebol členom žiadnej strany.   •12.   Veda a výskum - Róbert Mistrík, 42r.   Róbert je ozajstný vedec, ktorý sa navyše svojimi vynálezmi celkom slušne živí. Vyštudoval analytickú chémiu a doktorandské štúdium ukončil v roku 1994 vo Viedni. Po dvoch rokoch v Amerike, kde pre ministerstvo obchodu vyvíjal matematické metódy, založil v roku 1998 firmu HighChem, ktorú vedie dodnes. Nebol členom žiadnej strany.   •13.   Životné prostredie - Jozef Bednár, 34 r.   Jozef je Tatranec a zanietený ochranca prírody. Na Univerzite Komenského vyštudoval environmentálne plánovanie a manažment, dva roky bol asistentom predsedu predstavenstva VW a.s. a v rokoch 2001 až 2006 vedeckým pracovníkom na Oxforde, kde si  spravil aj doktorát v obore environmentálna geografia . Nebol členom žiadnej strany.   •14.   Kultúra - zatiaľ neobsadené   •15.   Šport - zatiaľ neobsadené   •16.   Média - Vladimír Klimeš, 53r.   Vlado robí v médiách odjakživa. Vyštudoval síce stavebnú, no do revolúcie sa popri štúdiu venoval fotografii v denníkoch Šport, Smena a Práca. Po revolúcii bol do roku 1994 zástupcom šéfredaktora denníka Práca a neskôr riaditeľ vydavateľstva Concordia Trading. Od roku 1997 je spoločník a riaditeľ TREND Visual s.r.o. Nebol členom žiadnej strany.   Bezpečnosť   Člen vedenia strany zodpovedný za bezpečnostné oblasti zatiaľ nie je určený   •17.   Súdnictvo a spravodlivosť - Peter Pihorňa, 28r.   Peter vyštudoval právo a dva roky robil právnika v Dexia banke, ktorú medziiným zastupoval na konkurzoch. V súčasnosti pracuje v advokátskej kancelárii svojho otca. Nebol v žiadnej strane   •18.   Vnútorná bezpečnosť - zatiaľ neobsadené   •19.   Zahraničná politika a EU - Zuzana Višňovská, 26r.   V Amerike a v Anglicku vyštudovala politické vedy so špecializáciou v zahraničných veciach. Bola najlepším študentom ročníka a promovala v januári 2009 s vyznamenaním a ocenením za najlepšiu dizertačnú prácu. Nebola v žiadnej strane.   •20.   Vonkajšia bezpečnosť - zatiaľ neobsadené     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (642)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




