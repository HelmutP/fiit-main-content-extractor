
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boris Kačáni
                                        &gt;
                Politika
                     
                 Správa o stave republiky. Posledná 

        
            
                                    5.5.2010
            o
            10:00
                        (upravené
                5.5.2010
                o
                15:05)
                        |
            Karma článku:
                22.89
            |
            Prečítané 
            6065-krát
                    
         
     
         
             

                 
                    Vážení občania! Prinášam vám poslednú správu o stave republiky. Poslednú v tomto funkčnom období vlády socializmu, rozmachu a upevňovania právneho štátu a demokracie. Obdobia stabilizácie hospodárstva, vyrovnávania regionálnych rozdielov, zodpovednej fiškálnej politiky a úspešnému boju proti korupcii. Znovu by som rád pripomenul pánovi prezidentovi, že ak by mal záujem túto správu prevziať, autor s tým vyslovene súhlasí, bez nároku na finančné ohodnotenie. Ale ak by sa dalo, bodlo by pár hektárikov pod Tatrami, s reštituentmi to vybavíme, založím aj eseročku na Cypre, pán prezident.
                 

                 Vážení občania. V ostatných štyroch rokoch sme boli svedkami viacerých prelomových udalostí. Prvou a najdôležitejšou, bolo víťazstvo socializmu a jeho úspešná implementácia do verejného života. Obyvatelia našej prekrásnej krajiny si uvedomili, ako vážne sa Slovensko poškodilo neoliberálnou politikov pravicových krkavcov, hájacich výlučne záujmy finančných oligarchov a nadnárodných monopolov. Výsledkom bývalej vládnej politiky bolo, že nás farizejsky všetky štáty vychvaľovali, dokonca sme sa mohli pripojiť k EÚ a NATO. Dnes už aj malé dieťa vie, že za tým je jedno obrovské sprisahanie tajných služieb celého sveta, majúce za cieľ aj naďalej udržať pri moci politikov, zbedačujúcich slovenský národ.   Pozrime sa na jednotlivé oblasti verejného života, ako sa nám vyvíjali, ako sa nám menili, ako nám s neochvejnou istotou vylepšovali život a nádej na svetlú budúcnosť.   Vynikajúco sa so svojou úlohou, eliminovať kriminálne živly, popasoval pán minister vnútra, ten sympatický chlapčisko. V žiadnom prípade nemožno opomenúť hrdinské zásahy polície, riskujúc pri tom vlastné životy. Len si spomeňme na ultranebezpečnú novinárku, ktorá bola natoľko drzá, že na povolenej demonštrácii hanila súdruha kazašského prezidenta, ba priam by sa dalo povedať, že ho ohrozovala na živote, no naši hrdinovia v rovnošatách ju nebojácne umravnili drobným pelendrekovým gulášom. Určite nikdy nezabudneme na odvážny zásah príslušníkov našej polície, voči obrovskej presile, masám hysterických, po zuby ozbrojených demonštrantov, ktorých spacifikovali aj v spolupráci so spriatelenými tajnými agentmi zo súdružskej Číny. Ubezpečujem všetkých majiteľov iks pätiek a iných ekologických áut, že aj naďalej budú môcť parkovať kde chcú, najmä na miestach vyhradených pre invalidov a žiaden podivný policajt ich nebude otravovať. Ďalej ubezpečujem všetkých, že budeme dôsledne merať rýchlosť vozidiel najmä v úsekoch, kde nikdy nedošlo k nehode a najmä vodičov lacných a starších áut budeme exemplárne trestať aj za minimálne prekročenie rýchlosti. Ubezpečujem generálov polície a ministrov, že aj naďalej bude pre nich platiť neobmedzená rýchlosť bez ohľadu na to, či jazdia v obci, alebo na diaľnici a ešte im kúpime zo štátneho rozpočtu em päťky, aby nič nebránilo rozvoju ich pretekárskeho talentu. A samozrejme plošne odmeníme všetkých policajtov za vynikajúcu a nadštandardnú prácu tesne pred voľbami, lebo na to máme a oni si to na rozdiel od učiteľov a lekárov zaslúžia.   Kapitolou samou o sebe sú médiá. Je to potrebné spomenúť, lebo ako náš milovaný vodca, súdruh premiér poznamenal, sú to idioti, hyeny a duchovní bezdomovci. Pokiaľ v minulosti si mohli médiá písať čo sa im zachcelo, dnes im konečne bolo trošku klepnuté po prstoch. Náš drahý a zbožňovaný vodca im to ukázal. Nebudú veru oni len tak tárať, že niekto kradne, keď nie sú dôkazy! K očistnému procesu v súdnictve sa dostaneme neskôr, teraz stačí pripomenúť, že najlepšie odhaľujú farizejstvo a podlosť médií, vyhraté procesy pána premiéra a milióny, ktoré vysúdil. Tendencia pacifikácie médií je vzostupná, naši priatelia so spriatelenej finančnej skupiny majú jednu televíziu, najnovšie kúpili jedny noviny, štátna televízia je pevne v rukách kompetentného manažéra, ktorý svoju oddanosť vlasti a strane riadne preukazuje už niekoľko desaťročí. Pilnými a usilovnými žalobami postupne zruinujeme tých mediálnych škodcov, ktorí sú nespratní, neustále sa snažia konštruovať vymyslené kauzy, s jediným cieľom. Očierniť vládu a pána premiéra.   Proces reformy zdravotníctva úspešne napreduje. Veľmi múdro a štátnicky pán premiér zakázal zisky súkromným poisťovniam, veď to je nehorázne, aby niekto na zdravotníctve zarábal. Myslia sa tým poisťovne, ostatné články zdravotníctva zarábať môžu, veď z čoho by žili firmy a rodinní príslušníci pána predsedu parlamentu? A tie žaloby na štát, priatelia? Bagateľ! Tie sa budú tiahnuť roky, definitívne prehráme asi až vtedy, keď tu už naša socialistická vláda nebude, takže s najväčšou pravdepodobnosťou tie miliardy zaplatí nejaká iná, možno zase ultrapravicová vláda, ktorú potom obviníme, že prehrala proces a spravodlivý hnev našich voličov nám znovu prinesie víťazstvo vo voľbách! Antisociálne platby zavedené bývalými hyenami sa zrušili a napriek tomu naše zdravotníctvo prosperuje spolu s občanmi, čoho dôkazom je to, že aj keď priame platby pacientov stúpli oproti minulému obdobiu o 40%, nepozorujeme žiadne sociálne nepokoje, ani nevôľu verejnosti. Pacienti zrejme pochopili, že je férovejšie platiť viac, ako len tých 66 centov pri každej návšteve. Všetky zásadné zmeny v zdravotníctve viedli u mnohých zamestnancov v zdravotníctve k drastickému zvýšeniu platov, myslíme tým tých, ktorí si našli zamestnanie v zahraničí. Vážení občania, faktom je, že ste príliš často chorí. Toto treba zmeniť. Zefektívnili sa finančné toky tak, aby sa posilnili farmaceutické a distribučné firmy, najmä firmy pána predsedu parlamentu, zatrhli sme zisky tým šakalom zo zdravotných poisťovní, čím sa vytvorili vynikajúce podmienky pre likvidáciu malých nemocníc, ktorým už poisťovne nechcú platiť. Našim cieľom je postupne presadiť samoliečbu a drobný šamanizmus, lebo mnohí pacienti zbytočne zneužívajú systém pri liečení nezávažných ochorení. Preto odkaz našej sociálnej vlády je: nebuďte zbytočne chorí, neotravujte zdravotníkov, ak sa už predsa len cítite extrémne zle, navštívte miestnu bylinkárku! Sľubujeme, že v žiadnom prípade nebudú dostávať lekári na pohotovostných službách ani o cent viac ako mizernú almužnu, čo znamená, že sa pohotovosti budú rušiť jedna za druhou, lebo už dnes nie je nikto ochotný tú službu robiť a tým sa znovu priblížime k vyššie spomenutému, vytýčenému cieľu, vytvoriť sieť minimálnych bylinkárok a šamanov.   Aby sme len nechválili, musíme spomenúť aj drobné negatíva, ktoré sa nepodarilo odstrániť. Stále je množstvo neprispôsobivých občanov, ktorým sa nepozdáva, ako vláda rieši Rómsku otázku. Niektorí dokonca verejne protestujú, sťažujúc sa na to, že Rómovia im dennodenne vykrádajú záhrady a ešte ich aj fyzicky napádajú. Nuž, ak by im dali dobrovoľne, možno by ich Rómovia ani nebili. Takýmto občanom chýba cit pre spolupatričnosť, nie sú schopní tolerovať iných, ktorí to nemajú v živote ľahké, ktorí keď si zo sociálnych dávok zaplatia mobilné telefóny, a zvyšok minú na alkohol a cigarety, tak im nezostáva nič iné, len obslúžiť sa u susedov. Tu je nutné ešte popracovať, najlepšie okamžitými a možno aj preventívnymi zásahmi kukláčov voči nespokojným občanom. Tento nápad je zatiaľ len v príprave, ale ak znovu vyhráme voľby, pokúsime sa ho dať aj do legislatívnej podoby, aby bolo možné raz do mesiaca preventívne pelendrekmi spacifikovať občanov, ktorí zbytočne dráždia Rómov tým, že majú slušné domy, záhrady a prácu.   V oblasti pacifikácie neprispôsobivých občanov máme ešte aj iné rezervy. Zatiaľ sa našej múdrej a sociálnej vláde nedarí prijať také legislatívne prostredie, aby mohli všetkých tých šťúralov, gágajov a prďúsov efektívne poslať do prevýchovného zariadenia prvej, druhej, alebo tretej nápravnej skupiny. Dokumentovať to možno aj na opodstatnenej snahe pána predsedu najvyššieho súdu, ktorý svoju ambíciu dostať toho hajzla do basy (poslanca Lipšica) verejne deklaroval v NR SR. Ak by sme sa vedeli vysporiadať s luzou, ktorá drzo a verejne kritizuje našu vládu, s médiami, ktoré sú mafiánskymi spolkami, ako nás s touto skutočnosťou nedávno oboznámil sám pán premiér, hneď by sa nám všetkým ľahšie dýchalo. Možno nie úplne všetkým, ale mnohým. No, možno ani mnohým nie, ale niekoľkým určite! A predsa o to ide, však?   V rámci objektivity sa nedá nespomenúť aj na udalosti, ktoré reakčné médiá opisujú ako škandály. Jednou z nich je napríklad predaj „teplého vzduchu". Niet učebnicovejšieho príkladu, na ktorom možno zdokumentovať hyenizmus médií a opozície. Mali by sme byť naveky vďační našej sociálnej vláde, že našli vôbec niekoho, kto bol v tom čase ochotný zobrať si na plecia to obrovské riziko, kúpiť naše emisie a zaviazať sa, že ich splatí už za 60 dní! Určite to nebolo ľahké, lebo ak by sa pán minister pomýlil, hrozilo, že natrafí na podvodnícku eseročku, ktorá by nakoniec svoj záväzok vôbec neuhradila. To, že všetko prebehlo podľa plánu svedčí o nadmernej genialite pána ministra a hlavne nášho drahého premiéra. Demonštroval ju tým, že ak sa pán minister onehdy vyhrážal, že tento supervýhodný biznis zruší, pán premiér ho okamžite odvolal! Treba aj jednoznačne vyvrátiť mýty a cene. V deň, keď sa podpisovala zmluva s renomovanou americkou firmou (sympatické bolo, že v rámci šetrenia nákladov si firma dala vyrobiť web stránku na Slovensku a nemíňala peniaze na drahé a luxusné priestory), dokázateľne nepredávala emisie žiadna vláda na svete. Iné vlády predávali emisie buď o niečo skôr, alebo tesne neskôr, síce za dvojnásobok našej ceny, ale to je priatelia predsa trh. Ceny sa menia, ceny sa vyvíjajú a žiaľ v deň, kedy sme my predávali, bola to najlepšia možná cena. Nezabúdajte, že pán minister vybojoval v zmluve neuveriteľne výhodnú podmienku, že ak použijeme peniaze na „zelené projekty", môžeme dostať ešte neuveriteľných 15 miliónov eur. Trošku nás mrzí, že sme ich nedostali, zrejme náklady americkej eseročky na prevádzku sídla a náklady spojené s ďalším predajom emisií boli natoľko vysoké, že ich dostali do krachu a firmu bolo treba zrušiť, ale tak to chodí vážení občania.   Ako gigantický úspech možno hodnotiť budovanie infraštruktúry. Naša nezištne pracujúca vláda odovzdá viac kilometrov diaľnic, ako sľúbila. Teraz sa však pridržte stoličiek, nech nevypadnete. Vláda sa pričinila o to, aby na zelenej lúke nielen že začala, ale aj dokončila a odovzdala, neuveriteľných 40 kilometrov diaľnic a to už hraničí s ôsmym divom sveta! Navyše, pokračovalo sa vo výstavbe tých ciest, ktoré začala už minulá vláda, čo možno bez váhania hodnotiť ako kontinuitu smerovania Slovenska a tým berieme všetkým kontrarevolucionárom vietor z plachiet.   Žiaľ museli sme po vzore Grécka, ktoré s tým má bohaté skúsenosti, použiť aj drobnú fintu pri výstavbe diaľnic. Ale len kvôli tým úzkoprsým byrokratom z EÚ, ktorí strážia deficity štátnych rozpočtov. Preto sú projekty PPP veľmi vhodnou alternatívou. Musíme priznať, že kilometer diaľnice bude o dosť drahší, ako bežne, ale nejako to už zvládneme. Nakoniec, nie je to až také hrozné, napríklad výstavba urýchľovača CERN, ktorý je skoro tak isto dlhý, ako diaľnica pod Strečnom a celý je pod zemou, koštovala len o máličko menej, ako bude stáť naša diaľnica.   Po nástupe sociálnej vlády sťa by sa roztrhlo vrece s múdrymi a geniálnymi projektmi. Neprivatizovať Cargo bol jedeným z nich, stačila dotácia štyri miliardy korún a tá strategická firma zostala v rukách štátu. Alebo letisko. Umným a prezieravým zrušením privatizácie sme zamedzili nadmernému hluku a možnému chaosu zo zvýšenej frekvencie letov. Stavbou nového terminálu sme dali zarobiť spriateleným firmám a zabezpečíme tým, že v budúcnosti budú mať naši cestujúci k dispozícii krásny, nový a poloprázdny terminál.   Na tomto mieste je nutné pripomenúť prezieravé vyjadrenia pána premiéra, keď ešte v opozícii varoval pred šakalmi z EÚ. Je vrcholne nechutné a nevkusné, ak nám stále chodia kadejaké komisie, prešetrujúce údajné defraudácie peňazí daňových poplatníkov EÚ. Myslím, že si dovoľujú až príliš. Zrejme pod tlakom ultrapravicových lobistických združení, ktoré sú už od podstaty antagonisticky naladené voči pracujúcemu ľudu a sociálnej vláde, zbytočne otravujú a zneisťujú našich občanov a vládu v budovaní spravodlivej sociálnej spoločnosti. Niet lepšieho príkladu, ako sú sociálne podniky, kde nejakí šťúralovia z Bruselu vŕtajú do toho, ako sa používajú peniaze z eurofondov. Tie podniky zamestnali vyše tristo občanov! A im sa nevidí, že majitelia podnikov, ktorí na svoje plecia vzali tú obrovskú zodpovednosť, si dovolili za zlomok z tých štyroch miliárd korún nakúpiť luxusné terénne autá. Majú sa funkcionári strany Smer vari voziť vo Fábiách?   Podnikateľské prostredie sa prudko vylepšilo. Všetci tí pseudopodnikatelia, ktorí k nám neprišli podnikať, ale len vykorisťovať našich zúbožených obyvateľov, sa pod výhovorkou krízy zbalili a odišli inam. To je len pozitívne, pribudlo síce pár desiatok tisíc nezamestnaných, na druhej strane my nechceme podnikateľov, ktorí prídu k nám kvôli zisku! Týmto vyzývame všetkých podnikateľov z celého sveta, ak k nám chcete prísť, tak láskavo nie kvôli tomu, aby ste sa tu u nás domáhali nejakého práva, vykorisťovali pracujúcich a vytvárali zisk. Ak sa budete správať neprístojne, napríklad sa vám nebude páčiť, že na každého zamestnanca budete povinní zamestnávať jedného odborára, ktorý má mať k dispozícii auto, mobil a kanceláriu, tak s vami zatočíme a radšej sem ani nechoďte!   Kontrarevolučné sily však nespia, práve naopak. O čo viac sa našej sociálnej vláde darí, o to intenzívnejšie pokračujú v konšpiračnej a diverznej činnosti. Nedávno pán minister vnútra odhalil zaujatosť tej pofidérnej organizácie, prepojenej na najradikálnejšie zoskupenia svetovej finančnej oligarchie, ktorá pod názvom Transparency International , zverejnila ten štvavý pamflet, v ktorom sa snaží presvedčiť našich občanov, že sa radikálne zhoršilo korupčné prostredie v našej drahej krajine. Našťastie naši občania nie sú hlúpi, majú historickú skúsenosť s režimom, kedy existovalo množstvo podobných dezinformačných kampaní. Aj vtedy museli súdruhovia vysvetľovať a brániť sa, prípadne siahnuť u úplne pomýlených jednotlivcov k tvrdším opatreniam. Zvládali to elegantne, tie skúsenosti z minulého režimu pomáhajú dnešným súdruhom úspešne čeliť podobným podvratným činnostiam. A že sa našej sociálnej vláde darí, o tom niet najmenších pochýb! Treba sa len pozrieť okolo seba, na tie šťastné a veselé tváre občanov! Určite by sme ich našli aj niekoľko desiatok, ktorým sa pilnou prácou podarilo dosiahnuť nebývalého blahobytu. Tí, ktorí nie sú sponzormi našej štátostrany patrí tento odkaz. Vaša chyba, staňte sa všetci sponzormi strany Smer, bude vám dobre!   Financie sú konsolidované, deficit je síce na prvý pohľad vysoký, ale pozrime sa napríklad na Grécko, v tomto kontexte je celkom fajn. Elegantnému ministrovi nemožno nič vyčítať, aj keď dostal jednu žltú kartu, ale všetci sme len ľudia, berme pozitívne, že ten konverzný kurz prezradil len jednej firme. S tým Tiposom mu to celkom nevyšlo, tu zohrali svoju záškodnícku úlohu zase médiá a opozícia, v poslednej sekunde zmarili spravodlivý rozsudok súdu, aby tie stámilióny nedostala spriatelená firma.   A je tu čas, pochváliť našu pani ministerku sociálnych vecí. Koľko špinavých útokov sa na ňu zosypalo. Dokonca sa niekto pokúsil otráviť jej psa, čo bol dostatočný dôvod, aby zasadala vláda! Ona sa však nedala zastrašiť. Svojej úlohy sa zhostila vynikajúco, už od začiatku bolo cítiť jej sociálne cítenie a spolupatričnosť s tými najzraniteľnejšími. Mimo iných hviezdne fenomenálnych opatrení, nemožno nespomenúť asi najúžasnejší projekt z jej dielne, sociálne podniky. Dokázala nimi zamestnať vyše tristo nezamestnaných! A to len priamo, veď tie podniky musí niekto riadiť, takže k úplnej spokojnosti môžeme konštatovať, že sa podarilo zamestnať aj niekoľkých straníkov zo Smeru a prípadne ich rodinných príslušníkov. A je úplne jedno, či to zaplatí EÚ, alebo my všetci, nedajme sa znechutiť nejakými bruselskými byrokratmi, ktorí sú asi zmanipulovaní a nedokážu sa povzniesť nad nejaké korupčné drobnosti.   Na záver to najlepšie milí spoluobčania. Po nekonečných peripetiách a deviáciach súdnictva a spravodlivosti predchádzajúcich období, môžeme hrdo konštatovať, že tento problém je vyriešený. Vďaka prezieravosti našich politických špičiek, sa justície ujal schopný a energický muž, vysokých morálnych a odborných hodnôt. Musíme kategoricky odmietnuť štvavú kampaň opozičných supov. Kto iný by vedel lepšie bojovať s mafiánskymi a kriminálnymi živlami, ako ten, kto sa s nimi osobne a dobre pozná? Justícia sa pod vedením nášho najschopnejšieho sudcu konečne začala čistiť od kontrarevolucionárov a dosiahla prenikavé úspechy, ktoré nám závidí celý svet. Dokonca na disciplinárne konania sa chodia dívať zahraniční diplomati, aby sa poučili, ako správne a odborne riešiť nespratných sudcov. Niektoré rozhodnutia boli prelomové a dostanú sa do učebníc na celom svete. Naši ctihodní sudcovia odhalili mnoho komplotov, zistili v mnohých prípadoch, že vrahovia a mafiáni boli nespravodlivo odsúdení a pustili ich na slobodu, zistili, že dôkazy korupcie skorumpovaných politikov a verejných činiteľov vlastne neboli dôkazmi, že správa zo zasadnutia vlády nie je informáciou. Prísne, ale spravodlivo sme sa vysporiadali s médiami, akákoľvek kritika ctihodnosti v talári je okamžite pretavená do povinnosti platiť poškodeným sudcom milióny. Justícia sa konečne začala uberať správnym Smerom. Ubezpečujem vás, že rázne zakročí voči všetkým škodcom štátu, kritikom a šťúralom, nezodpovedným a rebelantským sudcom.   Priatelia! Smer smerovania so Smerom je plný nádeje v úžasný a krásny život na našom Slovensku. Staňte sa aj vy sponzorom a Smer smerom k vám nasmeruje finančné toky, o akých sa vám ani nesnívalo. Ak sa zostaví aj v ďalšom období podobná vládna koalícia, garantujem vám, že smerovanie Slovenska sa nezmení, budeme pokračovať v budovaní sociálneho štátu a spravodlivej justície, ako je tomu dnes. Rozhodne sme sa poučili z krízového vývoja na Slovensku z ostatných rokov a ubezpečujem vás, že si určite nabudúce dáme väčší pozor, tendre nebudú ani na nástenke, verejné obstarávanie zrušíme a podozrivo lacné ponuky budeme automaticky vyraďovať. Ak by sa inak nedalo, zvýšime vám dane, som si istý, že láska našich voličov k nášmu vodcovi je väčšia, ako túžba po peniazoch, určite to pochopíte, že naši sponzori musia predsa z niečoho žiť.   Záverom vám chcem popriať veľa zdravia, nech nezaťažujete naše zdravotníctvo, pevné nervy, aby ste nemuseli obťažovať naše súdnictvo s nezmyselnými žalobami. Ak ste nezamestnaní, začnite pestovať grúle, alebo inú poľnohospodársku plodinu a nebuďte lakomí, aj naši Rómovia sú hladní. Neprepadajte depresii, váš súdruh priemer na vás myslí, dokonca vám sľúbi čokoľvek, možno vám niekedy prihodí pár eurášov na futbalový štadión, aby ste mali dôstojný stánok pre spoločenské aktivity a požívanie alkoholických nápojov.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (42)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Mozog je plastický
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            List jeho excelencii od organizátora protestov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Tušenie budúcnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Najväčšia európska lúpež
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Reinkarnácia Štefánika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boris Kačáni
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boris Kačáni
            
         
        kacani.blog.sme.sk (rss)
         
                                     
     
        Potrím k tým, ktorí nikdy neparazitovali na štáte, ktorí nekradli a nekorumpovali. Teda k menšine.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    103
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5713
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            môj iný blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            vysúšanie muriva
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




