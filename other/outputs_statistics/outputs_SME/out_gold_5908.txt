

      Začnem od jednoduchšieho po zložitejšie. 
      Ak prdnem v malej miestnosti, kde som sám, tak sa nič zvláštne nedeje, maximálne som si znepríjemnil ovzdušie, ktoré budem musieť dýchať, ak sa náhodou nedá vyvetrať. Ak sa dá vyvetrať, tak vyvetrám a znova je všetko v poriadku. Ešte zostáva možnosť odísť z miestnosti. Ak sa dá. Ak nie, hádam čosi pretrpím. 
      Ak by niekto prdol v tej istej miestnosti, keď je tam návšteva a sú tam tie isté podmienky, ktoré som spomínal vo vyššom odseku, tak je to „citeľný" problém... 
      Obdobne by som mohol pokračovať vzostupne s inými príkladmi... ale pre krátkosť času si skúste domyslieť. 
   
      Všetko, čo robíme, čo + ako si myslíme, aké máme postoje, naše cítenie, ale aj vedomie, zážitky, skúsenosti → to všetko je poprepletané s ostatnými ľuďmi a aj so „svetom" ako takým... 
      Možno ste mali tú šancu vidieť jeden z modelov spoločnosti, ktorý veľmi  zjednodušene zobrazuje „Celok" ako pomyselnú pyramídu → ak by šlo o občiansku spoločnosť, tak za absolutistickej monarchie bol na čele/vrchu panovník a najnižšia základňa, čo boli najbežnejší obyčajní ľudia. 
      Pokiaľ ide o náboženskú spoločnosť, napríklad katolícku cirkev, tak na čele/vrchu je pápež a základňu tvoria najbežnejší obyčajní laici. 
      Samozrejme ide o maxi zjednodušenie a netreba ho brať príliš vážne, to nie je smerodajné. Prečo? V prípade katolíckej cirkvi sme totiž všetci súčasťou mystického tela Kristovho, alebo sa tiež zvykne spomínať pojmový výraz „nevesta Kristova"... Takže význam každej „bunky", t.j. každého 1 člena, je neoddiskutovateľný. A ak je niekto vo vyššej funkcii, tak PRÁVE preto, že viac sa mu dalo, tak sa od neho viac aj vyžaduje. Srdce má viac práce, väčší objem, zvádza nás povedať, že je dôležitejšie než... Než čo? Vari bedrový kĺb je menejhodnotný? Nemienim to rozvádzať do nemoty → ak sme čo len trochu súdni, tak všetky časti sú aj dôležité, aj nutné a aj originálne a nenahraditeľné... (Hrozí tu však omyl - ak srdce zlyhá, ostatné časti ho nenahradia → lenže v duchovnej oblasti a berúc do úvahy cirkev → nemožno brať príklad „tela" doslovne!) 
   
      Tento na prvý pohľad nezrozumiteľný článok píšem najmä preto, že niektorí nemenovaní bratia v Kristu majú podivuhodný pohľad na veci aktuálnej súčasnosti... 
      Citujem z jedného ohlasu: „Zdôrazňujete, že ste kresťan katolík, ale hneď potom Vám nerobí problém prevziať rétoriku anti - katolíkov a začať =kydať= na pápeža. Každý rozumne uvažujúci človek vie, že tie smutné prípady morálne padlých kňazov boli zneužité na štvavú kampaň proti Cirkvi, a zvlášť proti pápežovi. Je mi Vás ľúto, že tak ľahko podliehate klamu, a ešte sa k tomu aktívne pridávate." 
      Toto je tak absurdné, že ma to popchlo reagovať - aj keď viem, že kto nechce, tak neprijme pravdu, ani keby sa roztvorilo „nebo" a anjelský hlas by prehovoril. 
   
      Viac ako dve desaťročia ututlávania, zamlčiavania a krytia mnohých - preukázané tak v Írsku, tak v USA (a v ďalších iných krajinách), to sa už nedá zamiesť pod koberec, ani keby mal rozmery slnečnej sústavy. (Dôkazy sú, kto ich chce vidieť, ten ich vidí - ale - nariaďovaná slepota, hoc s dobrým úmyslom → je len ďalšia lož). 
      A stačilo by tak málo, napríklad: „Ja, pápež, ja za seba ľutujem, že som sa na tom nepriamo podieľal svojím mlčaním i svojou nečinnosťou." (A preto odstúpim hodinu pred konkláve vyše stovky kardinálov, ktorí majú právo voliť...) 
      Nikto tu nevraví o likvidácii, ani o trestoch, aké robievajú mohamedánski činovníci na námestiach... 
      Iný dopad má čin, slovo, myslenie jedného človeka, ktorý je kdesi na onom najnižšom stupni pomyselnej pyramídy a iný, ak to urobí prvý, čelný, najvyšší... 
   
 XXX 
   
      Dodatok, nepriamo súvisiaci s článkom: 
   
      Mt 7, 21: Nie každý, kto mi hovorí: „Pane, Pane," vojde do nebeského kráľovstva, ale iba ten, kto plní vôľu môjho Otca, ktorý je na nebesiach." 
      A aká je v podstate vôľa Boha Otca? Milujte sa navzájom tak, ako som Vás miloval ja... 
      Ak by sme toto uskutočňovali všetci do dôsledkov, neboli by ani krádeže, ani vydieranie, ani násilie, ani žiadne otroctvo, ani sexuálne delikty, ani nútenie do prostitúcie, ani spory o majetok, vraždenie, ani zneužívanie detí a ...zatĺkanie zneužívania, ani klamstvá, a tak ďalej a tak podobne... 
   
 ZDROJ citátu z Písma sv. → Mt 7, 1-29: 
 
 (1) Nesúďte, aby ste neboli súdení. 
 (2) Lebo ako budete súdiť vy, tak budú súdiť aj vás, a akou mierou budete merať vy, takou sa nameria aj vám. 
 (3) Prečo vidíš smietku v oku svojho brata, a vo vlastnom oku brvno nezbadáš? 
 (4) Alebo ako môžeš povedať svojmu bratovi: „Dovoľ, vyberiem ti smietku z oka" - a ty máš v oku brvno?! 
 (5) Pokrytec, vyhoď najprv brvno zo svojho oka! Potom budeš vidieť a budeš môcť vybrať smietku z oka svojho brata. 
 (6) Nedávajte, čo je sväté, psom; a nehádžte svoje perly pred svine, aby ich nohami nepošliapali, neobrátili sa proti vám a neroztrhali vás. 
 (7) Proste a dostanete! Hľadajte a nájdete! Klopte a otvoria vám! 
 (8) Lebo každý, kto prosí, dostane, a kto hľadá, nájde, a kto klope, tomu otvoria. 
 (9) Alebo je medzi vami človek, čo by podal synovi kameň, keď ho prosí o chlieb? 
 (10) Alebo keby pýtal rybu, čo by mu dal hada? 
 (11) Keď teda vy, hoci ste zlí, viete dávať dobré dary svojim deťom; o čo skôr dá váš Otec, ktorý je na nebesiach, dobré veci tým, čo ho prosia. 
 (12) Všetko, čo chcete, aby ľudia robili vám, robte aj vy im. Lebo to je Zákon i Proroci. 
 (13) Vchádzajte tesnou bránou, lebo široká brána a priestranná cesta vedie do zatratenia a mnoho je tých, čo cez ňu vchádzajú. 
 (14) Aká tesná je brána a úzka cesta, čo vedie do života, a málo je tých, čo ju nachádzajú! 
 (15) Chráňte sa falošných prorokov: prichádzajú k vám v ovčom rúchu, ale vnútri sú draví vlci. 
 (16) Poznáte ich po ovocí. Veď či oberajú z tŕnia hrozná alebo z bodliakov figy? 
 (17) Tak každý dobrý strom rodí dobré ovocie, kým zlý strom rodí zlé ovocie. 
 (18) Dobrý strom nemôže rodiť zlé ovocie a zlý strom nemôže rodiť dobré ovocie. 
 (19) Každý strom, ktorý neprináša dobré ovocie, vytnú a hodia do ohňa. 
 (20) Teda po ich ovocí ich poznáte. 
 
 •(21)          Nie každý, kto mi hovorí: „Pane, Pane," vojde do nebeského kráľovstva, ale iba ten, kto plní vôľu môjho Otca, ktorý je na nebesiach. 
 
 (22) Mnohí mi v onen deň povedia: „Pane, Pane, či sme neprorokovali v tvojom mene? Nevyháňali sme v Tvojom mene zlých duchov a neurobili sme v Tvojom mene veľa zázrakov?" 
 (23) Vtedy im vyhlásim: Nikdy som vás nepoznal; odíďte odo mňa vy, čo páchate neprávosť! 
 (24) A tak každý, kto počúva tieto moje slová a uskutočňuje ich, podobá sa múdremu mužovi, ktorý si postavil dom na skale. 
 (25) Spustil sa dážď, privalili sa vody, strhla sa víchrica a oborili sa na ten dom, ale dom sa nezrútil, lebo mal základy na skale. 
 (26) A každý, kto tieto moje slová počúva, ale ich neuskutočňuje, podobá sa hlúpemu mužovi, ktorý si postavil dom na piesku. 
 (27) Spustil sa dážď, privalili sa vody, strhla sa víchrica, oborili sa na ten dom a dom sa zrútil; zostalo z neho veľké rumovisko. 
 (28) Keď Ježiš skončil tieto reči, zástupy žasli nad jeho učením, 
 (29) lebo ich učil ako ten, čo má moc, a nie ako ich zákonníci. 
 
   
 XXX 
   
      Lk 11, 23: Kto nie je so mnou, je proti mne, a kto nezhromažďuje so mnou, rozhadzuje. 
      Veru, veru → A kto je teda s Bohom? Ten, ktorý hovorí, myslí, koná v láske, skrze lásku a pre lásku → odmieta klamstvo, podvod, zavádzanie → láska má tú zázračnú vlastnosť: čím viac jej dáš, tým viac jej máš... 
   
 ZDROJ citátu z Písma sv. → Lk 11, 14-28: 
 (14) Raz vyháňal zlého ducha, ktorý bol nemý. Keď zlý duch vyšiel, nemý prehovoril. Zástupy žasli; 
 (15) no niektorí z nich hovorili: „Mocou Belzebula, kniežaťa zlých duchov, vyháňa zlých duchov." 
 (16) Iní žiadali od neho znamenie z neba, aby ho pokúšali. 
 (17) Ale on poznal ich myšlienky a povedal im: "Každé kráľovstvo vnútorne rozdelené spustne a dom na dom sa zrúti. 
 (18) Ak je aj satan vnútorne rozdelený, akože obstojí jeho kráľovstvo? Vy hovoríte, že ja mocou Belzebula vyháňam zlých duchov. 
 (19) No ak ja vyháňam zlých duchov mocou Belzebula, čou mocou ich vyháňajú vaši synovia? Preto oni budú vašimi sudcami. 
 (20) Ale ak ja Božím prstom vyháňam zlých duchov, potom k vám prišlo Božie kráľovstvo. 
 (21) Keď silný ozbrojený človek stráži svoj dvor, jeho majetok je v bezpečí. 
 (22) Ale keď ho prepadne silnejší ako on, premôže ho, vezme mu zbrane na ktoré sa spoliehal, a korisť rozdelí. 
 (23) Kto nie je so mnou, je proti mne, a kto nezhromažďuje so mnou, rozhadzuje. 
 (24) Keď nečistý duch vyjde z človeka, blúdi po vyschnutých miestach a hľadá odpočinok. Ale keď nenájde, povie si: „Vrátim sa do svojho domu, odkiaľ som vyšiel." 
 (25) Keď ta príde, nájde ho vymetený a vyzdobený. 
 (26) Tu odíde, vezme sedem iných duchov, horších, ako je sám, vojdú dnu a usídlia sa tam. A stav takého človeka je nakoniec horší, ako bol predtým." 
 (27) Ako to hovoril, akási žena zo zástupu pozdvihla svoj hlas a povedala mu: „Blahoslavený život, ktorý ťa nosil, a prsia, ktoré si požíval." 
 (28) Ale on povedal: „Skôr sú blahoslavení tí, čo počúvajú Božie slovo a zachovávajú ho." 
   
      Všimnite si 24 až 26 → ak niekto pokáním vyštval nečistého ducha, ale nenaplnil dom svojej duše láskou (t.j. Bohom), nedal ju na trón svojej duše, vráti sa nečistý duch s ďalšími ešte horšími... Treba ešte viac dodávať? 
   
   
   

