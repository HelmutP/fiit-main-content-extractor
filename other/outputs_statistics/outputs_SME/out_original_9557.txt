
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ladislav Větvička
                                        &gt;
                Slovenšti bratři a sestry
                     
                 Slovaci zežrali Makarony! Pomstili zme se za rok 1934 

        
            
                                    24.6.2010
            o
            0:00
                        (upravené
                14.12.2010
                o
                9:27)
                        |
            Karma článku:
                8.62
            |
            Prečítané 
            2301-krát
                    
         
     
         
             

                 
                    Tuž a je to doma. Kurňa, synci, to byste nevěřili, co mě to stalo sil. Že je slovenska reprezentačka silna, to sem věděl. Ale celu dobu sem to nesměl prozradit! Fčil, jak zme zežrali Makarony, to připada každemu jako normalni, ale to byste nevěřili, jaka to byla makačka. A malokdo temu věřil!
                 

                 ...byl majovy večer, hrdliččin zval ku lasce hlas, a ja sem seděl ve sve oblibene knajpě Na Mexiku a litoval sem, že se ti naši češti mamlasi neprobojovali do Afriky.   Vtom kdosik rozrazil vrata, vběhnul do knajpy v doprovodu synku vybobtnanych na haluškach, sednul ku mně a tiše povida: "Ladik, ja su Vladimir. Tuž to zme taci menovci, ne?" Otočil sem se, prohlidnul sem ho, a povidam: " Ja, ich weiss, tebe znam, synku. Co chceš?"   Tuž, co vam mam povidat. On za mnu přišel, bo pry umim propagandu mezi černyma haviřama, a oni pry musi byt tež uspěšni na jihu te černe země. Chvilu sem přemyšlal. Tuž, cosik vam možu pomoct. Na to, abyste byli celosvětovi mistři, na to nemate. Ale možna byzme mohli vratit Makaronum jejich podraz z čtyřiatřicateho, co temu řikaš, Vladimir?   A Vladimir řekl: "Ja. Zrobim cokoliv, Ladik. Prostě to naplanuj."   A jak řeknul, tak se aji stalo. Fčil to už možu prozradit. Řikam mu - "Posluchaj, ať se stane cokoliv, synci musi dodržet taktyku, chapeš to?" "Jasne. Chapu. Ale co když třeba budeme ve vedeni, co pak?" Ty mi nerozumiš, Vladimir. Prostě  ať se děje cokoliv, prvni zapas musite prohrat, nebo nejhuř remizovat. Rozumiš?"   "Tuž jasne, rozumim. Ale pak ty Paragrafy porazime a rozmetame je jak kamziky v podunaji, nie?" Chytil sem se za zbytky mych vlasu a šeptam mu do ucha: " Naopak, ty mamlas slovensky milovany! Ten zapas musiš prohrat! Enem tak přesvědčime ty Makarony, že vlastně o nic neide a že vas převalcuju jak etyjopsky asfalt!"   "A to si zo mňa robíš kozy, Laco", povedá mi ten Vajs. "Predsa sa nenecham poraziť od akychsik mamelukou z Paragrafu enem preto, aby si zo mňa robili cypa!"   "To neni všecko, Vlado. Aby ti makaronšti hňupi ztratili všecky strachy z jakychsik středoevropskych mstitelu, musiš se navic pohadat z vlastnima novinařama." "A to si zo mňa robiš prdel, nie?" "Nie. Musíš jim řict něco hodně sprosteho, třeba, že to su čuráci blbí." "No počkej, to ve slovenčině nic neznamená. Čo keby som povedal, že to sú kokoti vyjebaní?" " No, nevím. To zase v češtině nic neznamená, to je normalni osloveni.  Ale když mysliš, že to pomože, tuž to prubni. Ale pamatuj, Vlado. Prvni zapas musite remizovat. Druhy musite prohrat, aji kdybys měl ty tvoje synky postřilat, no a ve třetim přiletite jak anděl pomsty. Za Česko, aji za Slovensko. Ať se ten trener Musoliny obrati v hrobě, bo nam zebral vitězstvi."   Co ja sem to vlastně chtěl řict? Ja, už to mam:   Mam z vas radost, slovenšti synci :-)       Hystorije:   1. zapas s Novym Jičinem   2. zapas s Paragrafama   3. zapas s Makaronama (1934?) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Větvička 
                                        
                                            Stanky na prešporske Hlavne stanici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Větvička 
                                        
                                            Česi budu pozityvně dyskriminovani a povinně dostanu cyganske minystry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Větvička 
                                        
                                            Jak sem se snažil pomoct slovenske slanince
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Větvička 
                                        
                                            EU: Česi budu papat domaci uzene, Slovaci možu žrat chemicke blato
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Větvička 
                                        
                                            Cosik o propagandě: Krvežiznive hordy vtrhly na Krym
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ladislav Větvička
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ladislav Větvička
            
         
        vetvicka.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ostravsky cyp. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    95
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3802
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Knižka MAMULOVY DĚTI
                        
                     
                                     
                        
                            Slovenšti bratři a sestry
                        
                     
                                     
                        
                            Moravšti/Slezšti/Češti bratia
                        
                     
                                     
                        
                            Ostravske hlupoty a cypoviny
                        
                     
                                     
                        
                            Štreky ven z Ostravy
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Havlik
                                     
                                                                             
                                            Vašiček
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Uspěch
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Stanky na prešporske Hlavne stanici
                     
                                                         
                       Česi budu pozityvně dyskriminovani a povinně dostanu cyganske minystry
                     
                                                         
                       EU: Česi budu papat domaci uzene, Slovaci možu žrat chemicke blato
                     
                                                         
                       Môže prezident nemať názor na Nežnú revolúciu?
                     
                                                         
                       Česi. Podivní masochisti ve středu Európy.
                     
                                                         
                       Tajemstvi Vasila, hybrydniho polityka rusinskeho puvodu
                     
                                                         
                       Elita slovenská, kam si sa podela?
                     
                                                         
                       Vysoke školy produkuju zbruselizovane absolventy
                     
                                                         
                       The Best Větva Foto 2013
                     
                                                         
                       Cely svět, včetně Burkina Fasa, zavidi Slovakum hokejove dresy
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




