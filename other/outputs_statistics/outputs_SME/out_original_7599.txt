
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Sýkora
                                        &gt;
                Svet ľudí sa nás týka
                     
                 Príbehy Medzi nami – poznáte z NotaBene 

        
            
                                    30.5.2010
            o
            14:24
                        |
            Karma článku:
                5.19
            |
            Prečítané 
            728-krát
                    
         
     
         
             

                 
                    Asi tak by znel slogan reklamy v teleshoppingu... Lenže nejde o „načančanú“ reklamu, ale len o bežný život. Život, ktorý často prináša niektorým až príliš veľa zlého. Práve pri čítaní NotaBene sa to často zdá až neuveriteľne nespravodlivé.
                 

                 
  
   Občianske združenie Medzi nami skutočne môžete poznať z časopisu NotaBene. Totiž, toto malé združenie prináša každý mesiac konkrétny príbeh ľudí, ktorí sa nie vlastnou vinou ocitli v ťažkej situácii. Aktivity sa zameriavajú jednak na materiálnu pomoc - často rodinám s postihnutým členom – ako aj sociálnu výpomoc, napríklad komunikáciu s úradmi. Pomoc je však vždy výlučná a adresná.    Aj preto je pri každom príbehu uvedená možnosť pomoci, v prípade finančných darov aj heslo prostredníctvom ktorého, je možné poukázať prostriedky pre konkrétny príbeh. Samozrejme, spätne nájdete pri každom príbehu, ako sa - v rámci možností - jednotlivým rodinám pomohlo. (Ako ste/sme pomohli v roku 2008.)    Slniečko, Pôjde domov?, Navždy, Pripravený?, Musí žiť, Downove slniečko, Šťastie s bolesťou, Nevadí, Trojica, Murárove viazaničky, Luxus bez vody a Pre Mariánku sú práve názvy príbehov publikovaných v roku 2008. Pridanou hodnotou sú dojímavé texty novinárky Lucie Laczkó a kvalitné fotky reportéra Alana Hyžu.    Chceme zdôrazniť, že píšeme o ľuďoch, ktorí nesnívajú o veľkých peniazoch, pre nich má - v našej starej mene - desaťtisíc korún, hodnotu povestného „milióna“, a ktorí sa, častokrát aj vďaka takejto sume, doslova dokážu „vyhrabať z dna.“ (Viac info o občianskom združení Medzi nami.)    ...    Nedávno Medzi nami spustilo svoju internetovú stránku www.ozmedzinami.sk, takže odteraz nájdete tieto príbehy aj na internete (vrátane Facebooku). Samozrejme, keď vás niektorý príbeh zaujme a máte tú možnosť, je možné aj prispieť – či už finančne alebo materiálne, ale čo je nezáväzné a bezplatné (to je účinné posolstvo reklamy!) je prečítanie si zopár príbehov a reflexia toho vlastného. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Nosme si do supermarketov vlastné „sáčky“
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Daruj darček
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Neľudské zlo v koncentračnom tábore Sachsenhausen
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Malé múzeum Stasi, tajnej služby z čias NDR
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Matej Sýkora 
                                        
                                            Odtíňať ruky fajčiarskym prasatám
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Sýkora
            
         
        matejsykora.blog.sme.sk (rss)
         
                                     
     
        Človek. Aspoň sa snažím.
 Občas trochu stručný a chladný,
 inokedy zas obšírnejší a citlivý.

 Facebook 
 LinkedIn 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    151
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1173
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Občianske združenie BERKAT
                        
                     
                                     
                        
                            Potrebujeme (r)evolúciu?
                        
                     
                                     
                        
                            Život (je krásny)
                        
                     
                                     
                        
                            Svet ľudí sa nás týka
                        
                     
                                     
                        
                            Na Slovensku je to tak...
                        
                     
                                     
                        
                            Adjumani, Uganda
                        
                     
                                     
                        
                            Írsko, Dublin
                        
                     
                                     
                        
                            Londýn, Veľká Británia
                        
                     
                                     
                        
                            Randers, Dánsko
                        
                     
                                     
                        
                            New York, USA
                        
                     
                                     
                        
                            Triedenie odpadu
                        
                     
                                     
                        
                            Business
                        
                     
                                     
                        
                            Užitočné
                        
                     
                                     
                        
                            Festival Jeden svet
                        
                     
                                     
                        
                            Festival Pohoda
                        
                     
                                     
                        
                            SOS Povodne 2010
                        
                     
                                     
                        
                            STOP ZLU
                        
                     
                                     
                        
                            Energia naša každodenná
                        
                     
                                     
                        
                            Každý iný - všetci rovní
                        
                     
                                     
                        
                            Aktualizované z archívu
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo by som nešiel do povstania ?
                     
                                                         
                       TOP 10 výrokov politikov v roku 2013
                     
                                                         
                       Dobrý projekt jedna krádež nezastaví
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




