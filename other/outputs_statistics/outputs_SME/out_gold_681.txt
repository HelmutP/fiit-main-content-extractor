

 

Predslov:
 
 
Andrejko má po sérií "nešťastný zhôd okolností" zakázaný školský klub. V praxi to znamená, že každý deň, okrem utorka, mašíruje domov hneď po obede. Takže štyri dni v týždni je doma už o pol jednej. Okrem utorka. V utorok má po obede náboženstvo a tak končí o 13-tej. Domov zvyčajne príde 13:15. + / -.
 
 
 
 
 

Zápletka:
 
 

27.5.2008    12:22    (utorok) 
V zámke štrngajú kľúče, dvere otvára  Dubi.  Spokojný sám so sebou, ako rýchlo zvládol nekonečných 500 metrov od školy k domu. 
L:    Čo tu robíš? 
D:    Netešíš sa? Veď som prišiel rovno domov! 
L:    Veď máš dnes náboženstvo! 
D:    Božinku, ja som zabudol.  
L:    Aďulo, veď budeš mať neospravedlnené hodiny. Zajtra sa opýtať pani učiteľky, čo teraz. 
D:    Dobre.
 
 
 

28.5.2008    12:31    (streda) 

V zámke štrngajú kľúče, dvere otvára  Dubi.  Spokojný sám so sebou ... 
D:    Som domááááááááááááá. Načas! 
L:    Super. Čo povedala pani učiteľka na včerajšok? Mám niečo písať? 
D:    Nijééééééééé
 
 
 
 
 

3.6.2008    12:30    (utorok-zas) 
V zámke štrngajú kľúče, dvere otvára  Dubi.  Spokojný sám so sebou ... 
L:    Zase si zabudol na náboženstvo? 
D:    Hehe, nie, dnes neprišla sestrička Avila! 
L:    Určite? 
D:    Jasné! Mi neveríš?
 
 
 
 
 

Vyvrcholenie: 
 
 

5.6.2008    7:38    (štvrtok) 
Posledná kontrola Aďkovej  tašky pri príležitosti vkladania desiaty. Mama spokojná sama so sebou, že sa jej zo syna darí kresať poslušného prváka, nachádza malinký zdrap papiera. Napriek nízkemu tlaku je na pokraji infarktu.  
L:    Aďoooooooooooooooooooo! Zastrelím ťa! Čo má znamenať toto???? Povedz, že to nie je to, čo si myslím! 
D:   A čo si myslíš? To je ospravedlnenka.  
L:   To som si myslela! Prečo máš ospravedlnenku v taške? 
D:    Lebo pani učiteľka povedala, že dodnes ju musím doniesť. Inak bude zle. To kvôli nábožku ...
 
 
D: To som si napísal SÁM!! 
 
 
Naozaj sa mi zle dýcha, som červená v tvári. Viem to, lebo na chodbe máme obrovské zrkadlo ...
 
 
L:   Ale ospravedlnenky si nemôžeš písať sám! Prečo si mi to nepovedal? 
D:    A prečo ich nemožem písať? 
L:    To musí písať rodič! 
D:    To pani učiteľka nepovedala. Povedala, že ju mám doniesť. Veď ja viem písať. Chcel som pomôcť ...
 
 
 
 
 

Záver:
 
 
Dubi je vystrčený za dvere. Celou cestou po schodoch zanovito opakuje "ja predsa viem písať". Ja cestou do kuchyne "to snáď nie je možné! prečo?" Som príšerne naštvaná. Kým prídem do kuchyne a dialóg prerozprávam Jožkovi, smejem sa. Situácia je taká absurdná ... že stať sa môže iba u nás. Vlastne má pravdu - "pani učiteľka povedala ... "  
 
 
Ale hnevám sa na Teba Dubi, mal si povedať, že máš zle.
 
 
  
 

