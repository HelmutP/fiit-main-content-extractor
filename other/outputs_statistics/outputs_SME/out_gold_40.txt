

 
Začnime tým, že Islam označuje náboženstvo. Moslim je vyznavačom náboženstva - islamu. Tak ako slovo kresťan označuje človeka, ktorého viera je kresťanská. Moslimov je na svete okolo 1,3 miliardy. Najviac moslimov žije v severnej Afrike a v rôznych častiach Ázie. 
 
 
Z tejto veľkej skupiny 1,3 miliardy ľudí je iba menšia časť arabská. Počet Arabov je okolo 300 miliónov. Zvyšok sú iné národy. Slovo Arab vyjadruje teda národnosť. Tak ako slova „Slovák", „Nemec", „Američan". 
 
 
Najväčším moslimským štátom je prekvapivo nearabská (ale moslimská) Indonézia s okolo 250 miliónmi obyvateľov. Ďalšími nearabskými moslimskými krajinami sú: Pakistan, Irán, Afganistan, Turecko, stredoázijské krajiny ako Kazachstan, atd. A zväčša moslimským národom no bez vlastného štátu sú Kurdi. Moslimom môže byt aj Slovák ak prestúpi na islamskú vieru. A už sú aj takí Slováci. 
 
 
Takže zopakujme si to. Turek nie je Arab. Iránec nie je Arab. Pakistanec, nie je Arab. Afganec, Kazach, Indonézan, Kurd nie sú Arabi. Sú moslimovia, ale nie Arabi. Každý z nich je iný národ, ale ich viera - islam - je rovnaká. Tak ako Slováci, Maďari, Rakúšania, Taliani sú každý iný národ, ale majú rovnakú vieru - kresťanstvo. 
 
 
A teraz Arabi. Arabi sa medzi sebou ďalej delia, hlavne podľa štátov, v ktorých žijú. Arabských štátov je okolo dvadsať, nachádzajú sa v severnej Afrike a na blízkom východe, počnúc Marokom na západe Afriky a končiac Irakom na hranici s Iránom. 300 miliónov Arabov je rozdelených do približne 20 arabských štátov, z ktorých najväčším je Egypt s 80 miliónmi obyvateľov. 
 
 
No a tu je zaujímavá vec. Arab sa na jednej strane cíti byť súčasťou arabského národa, a zároveň sa cíti byť súčasťou národa svojej krajiny. Napríklad Egypťan sa hlasí k arabskému národu a zároveň k egyptskému národu. Iračan (od slova Irak, nie Irán) sa hlási k arabskému národu a zároveň k Irackému národu. Palestínčan, Tunisan, Sýrčan, Jemenčan detto. Je to niečo čo celkom neviem vysvetliť - akoby každý Arab mal dve národnosti, popri arabskej ďalšiu, ktorá jeho národnosť charakterizuje bližšie, pretože každá z tých oblastí má svoje špecifické kultúrne „ukazovatele". A tak môžete toho istého Araba v tom istom rozhovore počuť hovoriť „My Arabi" a o par minúť „my Libanončania" a „oni Maročania". 
 
 
Je to podobné ako so Slovanmi, ibaže by slovanské povedomie muselo byť omnoho silnejšie a živšie. Pekne sa to dá ukázať na jazyku. Tradičná písaná (a spisovná) arabčina je jedna, spoločná pre všetkých arabov a používaná vo všetkých arabských krajinách. Používa sa v písaných textoch, v novinách, v správach, pri ofocialnych príležitostiach. Bežne sa ňou však ľudia nedorozumievajú. Nato slúži takzvaná kuchynská arabčina a tá sa v každej krajine líši. Tak sa môže stať, že Arabi z navzájom vzdialených krajín sa svojimi hovorovými dialektmi nedorozumejú. Ale keďže obidvaja sa v škole učili spisovnú arabčinu a počujú ju každý deň v rádiu a televízii, môžu prepnúť na ňu. Predstavte si, že by spoločným spisovným jazykom všetkých slovanských národov bola staroslovienčina. Keby ste šli vlakom s Rusom nemali by ste problém sa s nim dohovoriť hoci neviete po rusky, jednoducho prepnete na staroslovienčinu, pretože je to aj váš aj jeho spisovný jazyk. 
 
 
Ako je to s Arabmi a Islamom. Väčšina arabského národa je moslimského vyznania okolo 90 percent. Ďalšími náboženstvami v arabskom svete sú najme kresťanstvo a potom judaizmus. Časť Arabov bola kresťanská ešte pred vznikom Islamu a časť bola ovplyvnená križiackymi výpravami. Časť mojej rodiny v Palestíne napríklad o sebe vedia, že sú potomkami križiakov z Korziky. V niektorých oblastiach si araby vyvinuli a zachovali svoje vlastne kresťanské cirkvi. Napríklad Koptská cirkev v Egypte, Maronitská cirkev v Libanone, a Asýrska cirkev v Sýrii. V Levante sú významnou cirkvou tiež Druzovia. Vznikli medzi rokmi 1000 a 1300 N.L. odčlenením od Islamu a vytvorili si svoje vlastne jedinečné a trochu misteriozne náboženstvo. 
 
 
No... neviem či som týmto článkom veci vyjasnil alebo viac skomplikoval. Ak sa v tom stále nevyznáte, stačí ak si zapamätáte že: 
 
 
- moslimova sú vyznavači náboženstva a je ich 1,3 miliardy. 
 
 
- z toho je iba 300 miliónov Arabov, teda príslušníkov arabského národa. 
 
 
- obrovská väčšina Arabov sú moslimova, ale existujú aj kresťanské menšinové komunity ako Kopti, Maroniti, Asýrania, ale aj tradiční katolíci, pravoslávni a protestanti. 
 
 
- ďalšími významnými moslimskými národmi sú Turci, Iránci, Kurdi, Pakistanci, Afganci, Indonézania - a títo nie sú Arabi. 
 
 
PS. Ospravdlnujem sa všetkým za trestuhodné paušalizovanie. Viem, že nie všetci Slováci, Taliani, Nemci, Maďari sú kresťania. Že nie všetci Indonézania, Pakistanci, atd. sú Moslimovia, aj to že mnohé menovane krajiny (ako Irán, Afganistan,...) nie sú tvorene jedným národom, ale zmesou viacerých národov. Viem, že hoci sú Kopti integrálnou súčasťou arabskej spoločnosti, niektorí o sebe hovoria, že nie sú arabmi ale potomkami faraónov.  Prosím berte v úvahu, že niektoré tu prezentovane fakty sú v skutočnosti o niečo zložitejšie a vyžadovali by si vysvetliť detailnejšie, ale jednoducho, pre väčšiu jednoduchosť som sa rozhodol to zjednodušiť. 
 
 
 
 

