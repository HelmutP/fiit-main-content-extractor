
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Mikloško
                                        &gt;
                Nezaradené
                     
                 Voľby a Calvados z roku 1946 

        
            
                                    30.6.2010
            o
            11:52
                        |
            Karma článku:
                9.12
            |
            Prečítané 
            1930-krát
                    
         
     
         
             

                 
                    Dnes bude reč o fľaške Calvadosu, ročník 1946, ktorej sa až po 20 rokoch dostalo uznania. Príčinou bol historický pád Fica, Mečiara a Csakyho, aj sedem stotín tenkej gumy v gatiach SNS. Zasa raz suverénnych Goliašov porazil Dávid s prakom. Ďakujem všetkým za 6230 preferenčných hlasov. K tomu, aby moja spisovateľská kariéra skončila chýbalo 240 krúžkov. Po kampani, ktorú som si sám organizoval, to nie je zlý výsledok. Ešte sa k nemu, aj k 8,5% KDH, vrátim.
                 

                 
  
  Po nežnej revolúcii ma často pozývali na rôzne večere. Debaty, v ktorých sa riešili dôležité veci som mal rád, ceny jedál ma hnevali. Kaviár Malosol v pražskom hotely Palace stál 2000 Kčs, želví polévka pětistovku. Mával som na jazyku Polívkove slová: „Želvy nežerem, chci guláš a pivo!“ Raz, niekedy v r. 1990-91, som dostal pozvanie na večeru do nevľúdneho hotela Palace od slovenských krajanov - podnikateľov z USA, ktorí prišli do ČSFR s A. R. Barronom, šéfom druhej najväčšej káblovej televízie na svete Time Warner Enterprice. Barron ponúkol zadarmo zaviesť káblovú televíziu po celom štáte, platilo by sa až jej používanie. Naše zákony, za ktoré som vtedy zodpovedal, takúto transakciu neumožňovali, aj keď už v marci 1991 moja komisia udelila prvých osem licencií na rozhlasové vysielania. Po dlhom čakaní nastúpil roj čašníkov. Hlavný povedal pst a spartakiáda začala - z tanierov odkryli strieborné dekle. Američania zhíkli od vzrušenia, ja od zhrozenia, lebo za veľa peňazí na tanieroch nebolo nič. Rozprúdila sa vážna debata v angličtine, pri víne padali vtipy. Barron povedal „sorry“, odišiel a vrátil sa so sedemdeckou zapieskovaného Calvadosa, ktorý pred pár dňami kúpil v Paríži za 2500 dolárov. Povedal slávnostný prípitok, pre jeho významnosť ho tlmočil Slovák Viliam Janovský, riaditeľ World Media Agency, Beverly Hills, Ca., USA. Calvados sa otvoril, v runde sme prepili dva vládne platy. Nálada stúpla, čašník dolial a Barron ho vyzval, aby si tiež vypil. Čašník  nerozumel anglicky, vzal fľašku a zmizol. V rozpakoch sme stíchli, majiteľ zbledol, keď si predstavil, že jeho „apple brandy“ pijú kuchárky. Opýtal som sa čašníka, kde je fľaša, česky sa Barronovi poďakoval za dar. Všetci padali do mdlôb až sa fľaša zasa objavila. Pri rozlúčke Barron navrhol, aby som zvyšnú tretinu flaše uschoval, dorazíme to pri nejakej budúcej výnimočnej príležitosti. Verejne som na etikete urobil čiaru a Calvados som doma schoval. Občas som ho kontroloval, čiarka povyrástla, obsah získaval na sile! Roky bežali, Barron sa nehlásil, poklad som strážil. 17. júna 2010 mal  spisovateľský PI-klub verejnú prezentáciu v Bratislave. Jeho členovia sú katolíci,  protestanti, ateisti a Židia, prívrženci pravice, stredu, ľavice a liberáli, Slováci a mix národností Slovenska. Snažíme sa na starobu zabudnúť na deliace čiary, nikoho neškatuľkujeme, robíme literárne akcie, vzájomne sa podporujeme. Ako signorovi „il Presidente“ klubu mi napadlo, že lepšia príležitosť na oslavu – päť dní po voľbách, už nebude. Bývalí papaláši zúfalo zostavovali nezostaviteľnú vládu, blýskalo sa na lepšie časy. Počas vystúpenia som z tašky vytiahol zvyšok 64 ročného Calvadosu a predsedníckemu stolu som nalial do plastikových pohárov KDH k prípitku za víťazstvo zdravého rozumu. Radosť z 20 ročnej slobody sa spojila s radosťou, že demagógia a populizmus končia. 25.júna 2010 som bol pozvaný majstrom Petrom Michalicom na jeho festival „Hudba pod diamantovou strechou“ do Kremnice, kde české Quarteto di Gioia zahralo aj nádherné Dvořákove Sláčikové kvarteto G-dur op.106. S majstrom Michalicom sme spomínali na staré časy, na jeho úspešné talianske vystúpenie v Ravelle. Po koncerte ma oslovil Vilo Janovský, bývalý riaditeľ a režisér World Media Agency, ktorý so svojou manželkou Alžbetou Štrkulovou, prvou Miss ČSSR z r. 1967, žije v Kremnici. Spomenuli sme si na stretnutia z porevolučných časov s Berlusconim, Kirchom, Maxwellom, Barronom a inými mediálnymi korifejmi, ktorí sa vtedy snažili privatizovať spoločný televízny kanál pre ČSFR alebo pre východnú Európu. Dozvedel som sa aj novú informáciu: prípitok Barrona Janovský vtedy vedome zle preložil, Barron nepovedal, že oferuje triumf z Paríža nežnej revolúcii. Nikto si nevšimol, že pri otváraní fľašky Barron zbledol a vyskočil, ostatných zaujímal iba tajomný zafúľaný Calvados z Normandie, povojnový ročník 1946...

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (56)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Mladí, príďte do Prahy, na stretnutie Taizé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Ďalší škandál vo verejnom obstarávaní
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Spomienka na tých, čo odišli do večných lovísk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Interpelácia na premiéra Fica
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            11. Kalvárske olympijské hry - fotoreportáž
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Mikloško
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Mikloško
            
         
        jozefmiklosko.blog.sme.sk (rss)
         
                        VIP
                             
     
         Mám dve na tretiu krát tri na druhú plus dva rokov (bŕŕŕ), 49-ty rok ženatý, štyri deti, jedenásť vnukov. Do r.1989 som bol vedec - matematik, potom politik, poslanec, prorektor, publicista, veľvyslanec v Taliansku (2000-2005), spisovateľ. Od r.2005 som dôchodca, vydavateľ najmä vlastných kníh (7), hudobno-dramatického CD-čka Ona, Ono a On, predseda Združenia kresťanských seniorov Slovenska. Pravidelne športujem, počúvam klasiku a rád sa smejem. Zabudol som, že od 11.3.2012 som sa stal poslancom NR SR, takže som zasa politik. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    216
                
                
                    Celková karma
                    
                                                7.77
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ďalší škandál vo verejnom obstarávaní
                     
                                                         
                       Tento november si už Paška zapamätá
                     
                                                         
                       Spomienka na tých, čo odišli do večných lovísk
                     
                                                         
                       Interpelácia na premiéra Fica
                     
                                                         
                       Obchod s ľudskými orgánmi - zbraň najťažšieho kalibru v informačnej vojne o Ukrajinu
                     
                                                         
                       Písať o tom, čo je tabu
                     
                                                         
                       Rozvod a cirkev
                     
                                                         
                       11. Kalvárske olympijské hry - fotoreportáž
                     
                                                         
                       Holokaust kresťanov v Iraku a Sýrii sa nás netýka?
                     
                                                         
                       Som katolíčka. Nejdem príkladom.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




