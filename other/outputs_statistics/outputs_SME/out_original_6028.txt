
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marcel Burkert
                                        &gt;
                Ekonomika
                     
                 Za gréckou krízou je egoistický volič 

        
            
                                    7.5.2010
            o
            9:13
                        (upravené
                11.2.2012
                o
                13:52)
                        |
            Karma článku:
                9.75
            |
            Prečítané 
            1470-krát
                    
         
     
         
             

                 
                    Bolo by možno zaujímavé poznať, koľkí z tých, ktorí sa v týchto dňoch v Grécku búria proti nevyhnutnému uťahovaniu opaskov, v minulosti volili tie politické strany alebo programy, ktoré ich priviedli na pokraj štátneho bankrotu. Koľkí z nich sa ulakomili, resp. volili „sociálne istoty", štrnáste dôchodky, atď. bez ohľadu na to, či na to štát má, resp. čo ich to bude v budúcnosti stáť. Za krízu preto nesú zodpovednosť možno v prvom rade práve tí, ktorí si nevideli ďalej od nosa, keď dali svoj hlas, a tým aj moc populistickým politikom a tiež tí, ktorí sa volieb nezúčastnili, čím sa nepriamo pričinili o vznik nezodpovednej vlády. Grékom ju nikto nevnútil, zvolili si ju sami.   
                 

                 Niekedy mi pripadá, akoby značná časť voličov stále žila v tom, že peniaze štátu padajú do kasy len tak; poprípade z neba... Veď ako inak by si politické strany stále dovolili lákať voličov na „bezplatné zdravotníctvo" a pod. a to dokonca v dobe, keď už by každému mohlo byť na Gréckom príklade jasné za akú cenu mali Gréci nadštandardné, možnostiam štátu neúmerné sociálne istoty a výhody. Pripadá mi to neuveriteľné, ako môže niekto veriť, že voliči ešte dnes uveria, že štát je schopný zabezpečiť ľudu bezplatné zdravotníctvo. To akože ho budú financovať politici z vlastného vrecka, alebo lekári budú pracovať bez nároku na mzdu? Nehovoriac o tom, že čím väčšia solidarita v zdravotníctve, tým je toto bližšie ku kolapsu, keďže bezbrehá solidarita sa dá krásne zneužívať.                   Ani by som sa veľmi nečudoval, ak by niekto navrhol populizmus nejakým spôsobom sankcionovať. Veď ten sa v prípadoch sľubov bez reálnych podkladov dosť podobá podvodu. V oboch prípadoch sa uvádza niekto do omylu z ktorého druhá strana nejakým spôsobom ťaží, alebo chce ťažiť. Populizmus navyše môže mať, alebo v niektorých prípadoch aj má, katastrofálne následky.   Bol by som za to, ak by štáty EÚ mali zakázané žiť na dlh, ktorý prekročí nejakú stanovenú mieru, resp. vládne strany, ak by chceli kandidovať v nasledujúcich voľbách, mohli by tak urobiť len pod podmienkou, že nasledujúca vláda by po nich nezdedila väčší dlh ako ten, ktorý tu bol pri predošlých voľbách. Ak by vláda počas vládnutia prekročila povolené zadlženie štátu, musela by predčasne odstúpiť. Ak by mala v programe napr. 13. dôchodky, musela by konkrétne definovať z čoho sa budú uhrádzať; či to bude na úkor štátnych zamestnancov, a ak áno tak ktorých, či to bude na úkor zvýšených daní, štátneho dlhu, alebo čoho. Ja viem, je to skôr utópia. Ale vtedy by bez problémov víťazil zdravý rozum, racionálne (aj keď nie vždy populárne) opatrenia nad populizmom, resp. využívaním, či zneužívaním krátkozrakosti a egoizmu mnohých voličov.   Po nás potopa. Tak mi väčšinou vo všeobecnosti pripadá často krát politika, ktorá sa zvykne označovať ako sociálna. Môže sa to javiť aj ako účel, lebo na kritizovaní nepopulárnych opatrení prípadne novej vlády, nútenej naprávať následky prehnane sociálneho cítenia tej predošlej, sa potom budú dať ľahko získavať politické body.   Zaujímalo by ma, ako zvykla reagovať pred krízou sociálne orientovaná grécka vláda na tamojšiu opozičnú, alebo mediálnu kritiku týkajúcu sa žitia na veľkej nohe, keď možnosti štátu tomu zďaleka nezodpovedali, teda žitia na úkor ďalšej generácie, svojich detí, ktoré zadlžili po uši. Či kritiku zľahčovali, resp. čím proti argumentovali. Či by sme sa z toho nemohli poučiť aj my. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Východ najzápadnejšieho Orientu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Komu hlavne slúži mestská polícia. Napr. v bratislavskej Dúbravke
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            B. Dúbravka a krásy D. Kobyly a okolia XV - XVI. Plus retro
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Aspoň na bilbordoch som "nezávislým"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Kto chce občanom slúžiť, musí si ich najprv kúpiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marcel Burkert
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marcel Burkert
            
         
        burkert.blog.sme.sk (rss)
         
                        VIP
                             
     
         Spoluzakladateľ OZ Stop alibizmu, učiteľ, príležitostný turistický sprievodca po horách a krajinách a od 15.12.2014 poslanec miestneho zastupiteľstva BA-Dúbravka za stredopravú koalíciu. Prvé články na blogu SME zverejnil v júni 2007: "Mýty a zvrátenosti vo vzdelávaní" a "Matka prírody - Boh materialistov". Od r. 2010 prispieva aj na blog spomínaného OZ.  
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    276
                
                
                    Celková karma
                    
                                                7.07
                    
                
                
                    Priemerná čítanosť
                    2342
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Výroky na zamyslenie 11/2014
                        
                     
                                     
                        
                            Rómska otázka
                        
                     
                                     
                        
                            Duchovné otázky
                        
                     
                                     
                        
                            Polícia s.r.o.
                        
                     
                                     
                        
                            Marenie výkonu cestnýh kontrol
                        
                     
                                     
                        
                            Školstvo a výchova
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Muž a žena
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Politika, spoločnosť, mediácia
                        
                     
                                     
                        
                            Zahraničná politika
                        
                     
                                     
                        
                            Ochrana a bytosti prírody
                        
                     
                                     
                        
                            Environmentálna kriminalita
                        
                     
                                     
                        
                            Cesty z bludného kruhu
                        
                     
                                     
                        
                            Dúbravka, Bratislavsko
                        
                     
                                     
                        
                            Krásy Devínskej Kobyly
                        
                     
                                     
                        
                            Potulky Slovenskom a ČR
                        
                     
                                     
                        
                            Potulky po Európe a okolí
                        
                     
                                     
                        
                            Potulky po USA
                        
                     
                                     
                        
                            Potulky maratónske
                        
                     
                                     
                        
                            Šport netradične
                        
                     
                                     
                        
                            Zvrátenosť vrcholového športu
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ako eliminovať vlády s.r.o.
                                     
                                                                             
                                            Ako prichádza polícia o produktívnych policajtov
                                     
                                                                             
                                            Manželské sexuálne povinnosti??? Duchovný pohľad
                                     
                                                                             
                                            Hodnotová slepota - Rusko vs. USA
                                     
                                                                             
                                            Manuál mladého konšpirátora
                                     
                                                                             
                                            Rodinné prídavky len na 2 deti=1. krok k riešeniu rómskej otázky
                                     
                                                                             
                                            21 zvrátenosti v Policajnom zbore
                                     
                                                                             
                                            Sonda do duše politika
                                     
                                                                             
                                            Museli sme zabiť Božieho Syna, aby sme mohli byť spasení...
                                     
                                                                             
                                            Čím neškodnejšia polícia, tým väčšia korupcia
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vo svetle Pravdy
                                     
                                                                             
                                            Zaviate doby sa prebúdzajú
                                     
                                                                             
                                            Kráľ Lear
                                     
                                                                             
                                            Sokrates (od D. Millmana)
                                     
                                                                             
                                            Rešpektovať a byť rešpektovaný
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Rudolf Pado
                                     
                                                                             
                                            Martin Škopec Antal
                                     
                                                                             
                                            Ján Macek
                                     
                                                                             
                                            Radovan Bránik
                                     
                                                                             
                                            Miroslav Kocúr
                                     
                                                                             
                                            Marek Strapko
                                     
                                                                             
                                            Ján Galan
                                     
                                                                             
                                            Ján Žatko
                                     
                                                                             
                                            Peter Konček
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Slavomír Repka
                                     
                                                                             
                                            Jozef Kamenský
                                     
                                                                             
                                            Michal Legelli
                                     
                                                                             
                                            Karol Kaliský
                                     
                                                                             
                                            Michal Wiezik
                                     
                                                                             
                                            Boris Kačáni
                                     
                                                                             
                                            Vlkáč Juraj Lukáč
                                     
                                                                             
                                            Smädný pútnik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Svet Grálu
                                     
                                                                             
                                            O cigánoch. Jozef Varga
                                     
                                                                             
                                            Môj iný blog
                                     
                                                                             
                                            Dúbravčan
                                     
                                                                             
                                            Šachový klub Dúbravan
                                     
                                                                             
                                            Na skládky nie sme krátki
                                     
                                                                             
                                            Lesoochranárske združenie VLK
                                     
                                                                             
                                            JUDr. Ing. Ivan Šimko
                                     
                                                                             
                                            Roman Kučera - cestovateľ
                                     
                                                                             
                                            Toulky
                                     
                                                                             
                                            Bežecké dojáky
                                     
                                                                             
                                            Vitariánska reštaurácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Hrozba vojnového konfliktu nie je zahranično-politickou otázkou! Váš minister zahraničia
                     
                                                         
                       Zákaz bilbordovej kampane - prvý krok k zrovnoprávneniu šancí kandidátov
                     
                                                         
                       Nechci zmeniť celý svet. Zmeň len svoje okolie
                     
                                                         
                       Dear John, odchádzam z blogu SME
                     
                                                         
                       111 krokov, ako vrátiť Bratislavu Bratislavčanom
                     
                                                         
                       Páni Milanovia, mňa nezastavíte
                     
                                                         
                       Tak oblečme si dresy, ale ...
                     
                                                         
                       Sonda do duše politikov a voličov
                     
                                                         
                       Ficov minister posadil svojho šéfa do bytu Goríl a on neprotestuje!
                     
                                                         
                       Je potrebné deštruovať ľudskú blbosť a nie volať po rekonštrukcii štátu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




