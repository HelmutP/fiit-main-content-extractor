
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana O'Deen
                                        &gt;
                Na rozmedzí
                     
                 O obriezke a brvne v oku 

        
            
                                    30.9.2009
            o
            9:25
                        |
            Karma článku:
                14.22
            |
            Prečítané 
            5884-krát
                    
         
     
         
             

                 
                    Sú veci, ktoré viem tolerovať. Stále je však dosť vecí, ktoré ma iritujú. Jednou z nich je kritizovanie neporiadku u iných, keď sami máme doma binec ako v tanku. Hneď na začiatok by som chcela upozorniť, že článok obsahuje dosť explicitné opisy niektorých procedúr a sexuálnych techník, ktoré som považovala za relevantné na podporenie môjho názoru k danej téme. Obrázkový materiál nie je použitý. Tiež upozorňujem, že je to jeden z dlhších článkov. A tak teda po včasnom upozornení vstúpte na svoju vlastnú zodpovednosť (ach jaj, asi už strašne dlho bývam v tom USA, keď som mala nutkanie toto upozornenie vôbec napísať :-D).
                 

                 USA je jeden z vedúcich štátov, ktoré vedú boj proti ženskej obriezke v Afrike. Ženská obriezka, ktorá sa nazýva aj FGM/C (femal genital mutilation/cutting - ženské genitálne mrzačenie/rezanie). Samozrejme, že som plne za, aby sa robila osveta ohľadom ženskej obriezky v Afrických krajinách, ale pripadá mi to iba viac ako divné, že sa USA stavia dosť laxne ku svojej vlastnej osvete, čo sa týka obriezky mužskej, ktorú považujem za také isté mrzačenie genitálii ako ženské.   Kým som neprišla do USA, nemala som ani tušenie, že sa v USA vykonáva rutinná novorodenecká obriezka chlapcov. Dovtedy som si myslela, že mužská obriezka je iba rituálnou tradíciou židovskej a islámskej kultúry. O to väčší bol môj šok z tohto zistenia. Samozrejme prekvapená situáciou, ktorú som neočakávala, som v dosť intímnom momente vyvalila na môjho manžela oči s otázkou, či je židovského vierovyznania (s írskym menom ako O'Deen sa mi to zdalo dosť nepravdepodobné a preto tá reakcia). Uznám, nie práve najvhodnejšia predohra. Ten zase valil buľvy na mňa, že jasne, že nie, a že prečo sa ho zrazu pýtam také veci. Keď som sa ho spýtala, že prečo je teda obrezaný, tak nechápal súvislosť otázok. A keď mi povedal, že v USA je obriezka bežná u väčšiny mužov (v jeho ročníku bola miera obriezky viac ako 90%) tak som nechcela veriť vlastným ušiam. Bola som z toho tak otrasená, že nas prvý intímny moment skončil dosť neintímne dlhým rozhovorom o obriezkach a otázkach k nej.   Začala som sa vtedy o tento problém viac zaujímať. Je prekvapujúce, ako málo informácií je ohľadom obriezky poskytované nastávajúcim rodičom. Strčia vám do ruky formulár, kde je ako jedna zo súčastí otázka, že ak sa vám narodí chlapec, či budete chcieť aj obriezku. Žiadne brožúrky v čakárniach, žiadne informácie, pokiaľ sa adresne nespýtate. Dokonca som počula aj také veci, že v časoch, keď sa narodil môj manžel, bola obriezka tak rutinná, že sa rodičov často ani nespýtali. Je tiež zaujímavé, aký silný odpor som zažila od americkej ženskej populácie k neobrezanému penisu a že s takým chlapom by sa hneď rozišli (po zistení faktu). Keď som sa zvedavo začala pýtať prečo, nemali racionálne vysvetlenie, iba že fúúúúj, hnus a špina.   Viem, že je dosť ľudí, čo tvrdí, že ženská obriezka je neporovnateľná s mužskou, pretože táto nespôsobuje mužovi žiadne sexuálne potiaže. Preto najprv rozoberme pár faktov obriezky. Celosvetovo je viac obrezaných mužov ako žien (mimochodom, iba 10% ženských obriezok je infibulácia - najťažší typ ženskej mutilácie, v ktorom sa odstráni klitoris, vnútorné pysky pošvy a vonkajšie pysky pošvy sa zašijú spolu a nechá sa iba malá dierka). V samotom USA sa obreže viac chlapcov, ako celosvetovo dievčat všetkými troma typmi obriezky. V prvom rade, predkožka nie je iba nadbytočná časť kože penisu, ale je vysko inervovaná. Poskytuje ochranu žaluďa pred vonkajším dráždením a tiež podporuje tvorbu maziva na dodatočnú ochranu. Na rozdiel od toho je obrezaný mužský žaluď stále vystavený vonkajšiemu treniu o spodné prádlo a mazivo sa prestáva vylučovať a koža vysychá. Novorodenec má predkožku pevne prirastenú k žaluďu. Táto sa musí pri obriezke násilne odtrhnúť tupou sponou, kde sa následne predkožka priškripne a odreže skalpelom. Až do nedávna sa tento výkon síce robil v sterilnom prostredí nemocnice, ale bez akejkoľvek anestézy, lebo sa verilo, že nervová sústava novorodenca nie je dostatočne vyvinutá na pocit bolesti. Priame problémy s obriezkou sú nasledovné: úmrtie, poškodenie žaluďa, alebo aj penisu, pritesná obriezka a následná neschopnosť erekcie, či bolestivá erekcia, infekcia rany. Sú to síce iba percentuálne malé čísla, ale sú úplne zbytočné. Ale najväčší, najrozšírenejší a najpodceňovanejší problém je znížená citlivosť penisu.   Podotýkam, že nasledujúce vety sú moje priame pozorovania a názory z nich vyvodené. Mark nebol môj prvý partner a preto som mala možnosť porovnať niekoľko skúseností. Veľkosť vzorky je síce štatisticky nedostatočná, ale napriek tomu si trúfam urobiť niektoré logické uzávery.   Nakoľko je muž s obriezkou schopný erekcie a ejakulácie, ľudia majú pocit, že je jeho sexuálny život neporušený (mimochodom, vraj aj ženy s infibuláciou dokážu mať vaginálny orgazmus). Myslím si však, ze tieto prehliadané rozdiely v citlivosti sa dosť odrážajú v kolorite vtipov, sexuálnych praktík a tradícii. Napríklad veľa vtipov, čo tu v USA koluje je o neochote žien vykonávať orálny sex. Typický predstaviteľ takého vtipu je napríklad tento: Kamaráti sa pýtajú ženícha, prečo sa tak veľmi usmieva. A on im hovorí: „Páni, práve som dostal najlepšiu „fajku" svojho života a tá žena sa stane za pár minút mojou manželkou." Kamarátky sa pýtajú nevesty, prečo sa tak veľmi usmieva. A ona odpovedá: „Lebo som práve dala poslednú „fajku" svojho života.". Na Slovensku takéto vtipy veľmi nevidíme. Tento vtip je peknou ukážkou stratenej citlivosti. Kým vykonanie orálneho sexu je u neobrezaného chlapa otázkou zábavných 5 minút, tento istý akt sa mení u obrezaného muža na niekedy aj 20 minútovú tvrdú drinu. Uznajte sami, že americké ženy majú skutočne dôvod sa tomu vyhýbať. Absencia citlivej hojne inervovanej predkožky a necitlivý žaluď so zhrubnutou (a zjazvenou) kožou je rozhodne hlavnou príčinou tohto drastického rozdielu.   Samotný vaginálny sex môže byť pre ženu viac nepohodlnejší s obrezaným penisom, ak má žena zníženú prirodzenú lubrikáciu pošvy a nastáva nepríjemný pocit väčšieho trenia. Je síce pravda, že obrezaní muži majú lepšiu výdrž pri samotnej súloži, ale aj to má svoju opačnú mincu. S pribúdajúcim vekom a konštatným otupovaním žaluďa bez ochrany predkožky majú starší muži problém s ejakuláciou a dokončením súlože. Tento problém vraj často vyústi do problému s impotenciou a problémovou erekciou (väčšina erektilnej dysfunksie je v psychike, ako fyziologická). Nepoznám štatistiky erektilnej dysfunkcie na Slovensku a ani v USA, ale podľa toho ako agresívne sa tu propagujú lieky ako Viagra a Cialis sa mi zdá, že je to bežný problém staršej generácie v USA.   Niekedy sa ako dôkaz neškodnosti rutinnej obriezky uvádza, že americkí muži sa viac angažujú v pestrejších sexuálnych praktikách. Jedna z prvých, čo mi príde na um je análny sex. Americkí muži sú posadnutí análnym sexom. Nepoznám jediného Markovho kamaráta (viď článok o otvorených diskusiách o sexe), ktorý by nevyslovil zasnenú túžbu o súhlase s análnym sexom od jeho partnerky. Znova, na Slovensku som sa s týmto javom skoro vôbec nestretla. Predpokladám, že je to spôsobené väčšou tesnosťou análneho otvoru a teda väčšej dráždivosti penisu.   Rada by som sa dotkla historických príčin rutinnej obriezky v USA. Kresťanstvo ako také obriezku nielen že neuznáva, ale priamo zakazuje. A tak prečo sa v silne kresťanskom USA stala obriezka bežnou záležitosťou? Nie, nie je to dôkazom židovského ovládnutia USA (ako som si istá, že to pár ľudí napíše). Je to jednoducho pre tú istú príčinu ako aj ženské obriezky, snaha o sexuálnu zdržanlivosť. Koncom 19-ho a začiatkom 20-ho storočia začal obriezku doporučovať pán Kellogg (áno, ten istý, čo začal aj produkciu corn flakes). Pán Kellogg bol posadnutý zdravým životným štýlom, ale žiaľ bol aj silným advokátom sexuálnej abstinencie, dokonca aj v manželstve. Zvlášť bol zanietený bojovník proti masturbácii, ktorej pripisoval mnohé zdravotné potiaže. Od zhoršeného zraku, cez epilepsiu, šialenstvo až po impotenciu. V jeho dosť vtedy uznávanej knihe teda propaguje obriezku ako jeden z úspešných spôsobou liečby masturbácie. Časom sa toho chytila lekárska komunita a neskôr sa obriezke pripisovala zdravotná prevencia voči močovým infekciám, rakovine penisu a prevencia proti sexuálne prenosným chorobám. Tieto fakty boli v 1999 vyhlásené Americkou akadémiou pediatrov za nedostatočne dokázateľné na odporučenie rutinnej novorodeneckej obriezky.   Jeden aspekt, ktorý nechcem opomenúť je obriezka ako prevencia proti HIV. Je dokázaná pozitívna korelácia medzi obriezkou a nižším percentom nákazy vírusom HIV. Je to práve kvôli odstráneniu predkožky a jej jemných epitelových buniek, ktoré sú náchylnejšie na prienik vírusu a zhrubnutej koži na žaludi penisu, ktorá je odolnejšia na mechanické zranenia trením. Ale obavám sa, že aj to je taká dvojsečná zbraň. Pretože mnoho mužov sa môže nechať uspať falošnou myšlienkou, že obriezka ich pred HIV ochráni úplne (neochráni, iba sa zníži) a nebudú pouzívať žiadne iné preventívne opatrenia. Plus to spojme s celkovou neochotou obrezaných mužov používať kondómy (stačí iba pomyslieť ako aj niektorí neobrezaní muži s normálnou citlivosťou fňukajú, že je to ako lízať med cez sklo) a myslím si, že obriezka môže problém aj prehĺbiť.   K dnešnému dňu je rutinná novorodenecká obriezka považovaná za lekársky výkon bez dokázateľného prínosu. Napriek tomu sa naďalej pomerne bežne vykonáva. Momentálne je miera novorodenckých obriezok 56% všetkých US novorodencov. 56% viac ako je potrebné. Je pravda, že toto číslo klesá, ale smutná je aj príčina tohto klesania. Príčina poklesu je čisto ekonomická stránka veci a žiaľbohu nie dôsledok osvety. Odkedy bola obriezka prehlásená za výkon bez zdravotného prínosu, ju poisťovne stiahli zo zoznamu preplácaných výkonov a bola zaradená pod kolónku elektívnych výkonov, čo znamená, že ju rodičia musia hradiť zo svojho vlastného vrecka. Je smutné, že 56% ľudí má potrebu mrzačiť telá vlastných synov. Príčiny a zdôvodnenia sú podobné, ako príčiny a zdôvodnenia ženskej obriezky. Je to tradícia, robí to každý. V škole by sa mu smiali kamaráti. Syn bude vyzerať ináč ako otec. Chlapec bude mať problém nájsť si partnerku, lebo neobrezaného žiadna nechce. Neobrezaný penis je penis imigrantov a nižších vrstiev. Neobrezaný penis je špinavý a odporný. Mnoho rodičov sa nad pokračovaním tejto tradície vôbec ani nezamyslí, proste to berú ako normálnu súčasť kultúry a či zdravotnej prevencie.   Žiaľbohu sú obe obriezky porovnateľné v jednom. Obe sú nepotrebným a barbarským mrzačením genitálii druhého človeka bez jeho súhlasu. Sú hrubým narušením ľudských práv a preto by osveta mala rovnako dôsledne brojiť nie len proti ženským obriezkam mimo USA, ale aj proti rutinným novorodeneckým a aj proti tradičným rituálnym mužským obriezkam v USA a vo svete. Na svete je 100 miliónov (5% svetovej populácie) genitálne zmrzačených žien a 650 miliónov (23% svetovej populácie) genitálne zmrzačených mužov bez ich súhlasu. Mnohí z nich budú s touto beštiálnosťou pokračovať na svojich deťoch. Mnohí z nich sú najsilnejší zástancovia genitálnych mutilácií. Tvrdia, že ich sexuálny život je normálny, ale ťažko môžu porovnať normálnosť svojho pocitu, keď v živote nič iné nepoznali. Týchto ľudí a tiež obhajcov obriezok musíme presvedčiť, že obriezka je brutalita proti vlastným deťom. Ten kruh sa musí raz prelomiť. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (291)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana O'Deen 
                                        
                                            Ako si to tie prasce americké dovoľujú!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana O'Deen 
                                        
                                            Aj kreacionisti majú právo na sexuálne fantázie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana O'Deen 
                                        
                                            Zahoďte konečne tie ženské časopisy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana O'Deen 
                                        
                                            Vyplakávanie nad Vianocami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana O'Deen 
                                        
                                            Sú dnešné manželstvá odsúdené na zánik?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana O'Deen
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana O'Deen
            
         
        odeen.blog.sme.sk (rss)
         
                                     
     
        Dieťa šťasteny a večný optimista.
Možno zasnený romantik?


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    113
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2749
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života nevážne
                        
                     
                                     
                        
                            Foto-články
                        
                     
                                     
                        
                            Na rozmedzí
                        
                     
                                     
                        
                            Moja rodina
                        
                     
                                     
                        
                            Moje úlety
                        
                     
                                     
                        
                            Pohľad do duše US tínedžera
                        
                     
                                     
                        
                            ...iba slová
                        
                     
                                     
                        
                            Kobyla - filozof
                        
                     
                                     
                        
                            Tam za vodou v rákosí
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dick Francis
                                     
                                                                             
                                            Isaac Asimov - Všetko od neho
                                     
                                                                             
                                            Jack Schaefer - Monte Walsh - moja obľúbená
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Roland Cagáň
                                     
                                                                             
                                            Jiří Ščobák
                                     
                                                                             
                                            Anča Jamerson
                                     
                                                                             
                                            Blanka Ulaherová
                                     
                                                                             
                                            Veronika Bahnová
                                     
                                                                             
                                            Viera Mikulská
                                     
                                                                             
                                            Adriana Markovičová
                                     
                                                                             
                                            Zuzana Dubcová *zuz*
                                     
                                                                             
                                            Damien Thorn
                                     
                                                                             
                                            Helena Jakubčíková
                                     
                                                                             
                                            Miroslava Krivošíková
                                     
                                                                             
                                            Ivka Vrablanská *IV*
                                     
                                                                             
                                            Martin Basila
                                     
                                                                             
                                            Marian Baran
                                     
                                                                             
                                            Z nostalgie
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Prevody do metrickeho systemu
                                     
                                                                             
                                            Konske stranky
                                     
                                                                             
                                            Startribune - MN news
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




