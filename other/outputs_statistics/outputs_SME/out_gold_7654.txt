

    
 to zviera ma zviera 
 hlboko v hrudi 
 zo sna ma budí 
 za dňa ma desí 
 súkromné besy 
 potom sa ukryje 
 ja kričím “kde si?” 
   
 aká som nešťastná 
 chorá &amp; bledá, 
 to zviera ma zviera 
 a spať mi nedá 
   
 ten odporný pocit 
 v tele sa rozhostí 
 trávi mi krv 
 a preniká do kostí 
 a ja už dosť mám tej jeho podlosti 
   
 on vždy ma prepadne 
 tam kde ho nečakám 
 všetky slnká zhasnú 
 a ja tu stojím sám 
 znova zas riešim prastarý hlavolam 
   
 ruky čo nevidím 
 zvierajú mi pľúca 
 ja som tá nevesta jeho budúca 
 skončíme spolu v svadobnej posteli 
 dúfam, že smrť nás konečne rozdelí 
   

