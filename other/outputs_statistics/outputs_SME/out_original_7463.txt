
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            František Földeš
                                        &gt;
                politika
                     
                 Chcel som voliť KDH. Čo teraz? 

        
            
                                    28.5.2010
            o
            12:12
                        |
            Karma článku:
                9.83
            |
            Prečítané 
            1984-krát
                    
         
     
         
             

                 
                    Tento článoček sa chystám napísať už týždeň. Ak by som ho písal pred týždňom, tak by vyzeral inak ako dnes. Ak by som ho písal pred tromi dňami, tiež by vyzeral inak ... Pretože situácia, okolo pre mňa dôležitých otázok sa zatiaľ vyvíjala ...
                 

                 Nemávam vopred vybrané, koho chcem voliť. Pri každých voľbách som sa rozhodoval vždy až v posledných dňoch, či týždňoch pred voľbami, možno s výnimkou 1998, kedy som mal jasno v tom, že sa chcem prestať hanbiť za to, z akej krajiny som - preto som vtedy volil SDK (tuším sa vtedy tak volali - proste Dzurindovu stranu).   Jasno mám vždy v tom, že chcem voliť pravicovú stranu. Volenie pravicovej strany totiž chápem tak, že chcem, aby som ja bol zodpovedný za svoje šťastie. Čiže, ak sa budem snažiť zarobiť, tak nech mám viac, a ak budem lenivý, tak logicky budem mať menej. A štát má pre mňa len vytvárať rámec, aby to takto mohlo fungovať. Samozrejme sú tam aj ďalšie dôležité otázky, ale tými v tejto chvíli čitateľa zaťažovať nechcem.   Rok 2002, mal som pocit že reformy v mojom záujme (ak sa budem snažiť, tak budem mať viac ...) boli naštartované dobre, preto som ostal u SDKU.   Rok 2006, veľká dilema, začal som mať pocit že káuz okolo SDKU pribúda a čím ďalej ým viac mi bol sympatický Béla, tak som zavolil za SMK (z omylu ma Csáky, Duray a spol vyviedli veľmi skoro po voľbách). Vtedy som nad KDH ešte vôbec neuvažoval. Odchod pána Čarnogurského zo strany (alebo len z jej vedenia) bol ešte pre mňa príliš čerstvý (nemal som ho rád) a pán Hrušovský bol nemastný, neslaný a nie príliš sympatický.   Keď som niekedy uvažoval o KDH, vždy mi boli sympatické hodnoty na ktorých je strana postavená. Rodina, spravodlivosť, kultúrnosť ... Čo mi však vždy vadilo (a kvôli tomu som nad nimi doteraz neuvažoval) bola netolerancia. Netolerancia k inakosti. Či už sa jednalo o iné vierovyznanie, či kultúrne prvky pochádzajúce z iných kultúr (narýchlo si spomeniem na snahu o zavedenie nejakého cvičenia, tuším Jogy, do školstva a hystériu, ktorú KDH okolo toho rozpútalo), netolerancia k neštandardným, nekonformným ľuďom (napríklad homosexuálom, ktorých pán Čarnogurský chcel liečiť a úspešne vyliečiť).   Keď sa nedávo stal šéfom strany pán Figeľ, zrazu som začal dúfať, že celé hnutie dostane novú iskru. Aj tak bolo, napriek tomu, že občas vyzerá ako uspávač hadov (sorry Janči, nič v zlom), jeho vystúpenia majú iskru, má to hlavu a pätu, kultúra prejavu je vysoká (na rozdiel napr. od pána premiéra, pre ktorého sú všetci idoti - a hlavne my, že sme si ho zvolili). Proste, ak by KDH získalo najviac hlasov a malo by možnosť zostavovať vládu, na tohto premiéra by som bol hrdý.   Až do minulého týžda to teda bolo na vážkach. Voliť, či nevoliť KDH? Principiálnou otázkou pre mňa bolo, či by KDH bolo ochotné ísť do koalície s pánom Ficom a jeho Smerom SD. Vtedy som chcel tento blog napísať prvýkrát a poslať mu tak odkaz, že pokiaľ nevylúči Smer SD z koaličných úvah, tak si budem musieť "prácne :)" hľadať inú stranu. Pretože v rozhovore na súkromnej VŠ spoluprácu so Smerom nevylúčil, z vyšších pohnútok (prijať spoluzodpovednosť za SMERovanie štátu). Pre mňa to boli žvásty, nie som ochotný voliť stranu, ktorá je ochotá vládnuť s touto skupinou indivíduí, ktoré len hrabú a hrabú a hrabú ...   Tuším tento týťdeň v pondelok sa to zlomilo a pán Figeľ vyhlásil, že so Smerom nie! Super, hovorím si. Dobrá správa, je vymaľované, do volieb mám prázdniny, nemusím rozmýšľať.   Medzitým vypukla hySLOTéria okolo maďarského zákona o udeľovaní občianstva zahraničným Maďarom. Bolo jasné, že vládny trojlístok sa toho chytí ako topiaci sa slamky. Pán Fico totiž stále nenašiel dobrú výhovorku na to, že predával poslanecké a ministerské kreslá, takže štekanie ponad južnú hranicu mu prišlo ako záchranné koleso, ktoré má prebiť tému financovania kampane Smeru z roku 2002.   Premiér zvolal radu obrany štátu (o čom sa tam asi bavili?) a riešili obrovskú úlohu, ktorá pred nimi stála. Čo s tými Maďarmi, ktorí si dajú aj druhé občianstvo?   Ak je pravda, že stačí vedieť po maďarsky a občianstvo človek môže dostať, tak som v skupine "ohrozených". Moja maďarčina zďaleka nie je excelentná, ale tých pár stoviek slov, čo poznám ma hádam zaraďuje do radu ľudí potenciálne ohrozujúcich územnú celistvosť Slotenského štátu. V živote som seriózne neuvažoval nad tým, žeby som zmenil občianstvo (aj keď v rokoch 94-98) som sa fakt za Slovensko hanbil. Pri tejto hySLOTtérii, však nad tým len tak žartom začínam uvažovať. Len aby som nasrdil Slotu a Fica. Stále to však bola len sranda, teším sa, keď vidím pána premiéra červeného od jedu, v tomto som škodoradostý.   Šok pre mňa nastal predvčerom, keď sa na špeciálnom zasadnutí národnej rady hlasovalo o kontrazákone. A tí, pre ktorých som už bol pevne rozhodnutý, zahlasovali spolu s tou bandou naničhodných ničomníkov, ktorí v jednej chvíli povedia nie trinástym a štrnástym dôchodkom pre Grékov a v nasledujúcej minúte vyvesia bilbord o trinástom dôchodku pre našich dôchodcov. KDH u mňa zlyhalo! Hlasovať so skupinou vymetencov, ktorí tam sedia s krvou podliatymi očami, za zákon vymyslený a napísaný behom jednej noci (áno preháňam) spôsobuje diskvalifikáciu u mňa. Ja mám pocit, že mali hlasovať proti už len z toho dôvodu, že od júla tu bude (pevne dúfam) nová vláda, ktorá bude mať možnosť niečo vymyslieť s chladnou hlavou.   Práve tu sa mala ukázať veľkosť a sila ducha hnutia. V tejto chvíli nechcem riešiť, či je zákon dokonalý alebo nie. Vadí mi, že vznikol za pár dní z hystérie a že KDH tú hystériu podporilo. Možno chcelo udržať hlasy voličov, ktorí zvažujú medzi KDH a SNS a sú zo severného slovenska a živého maďara ešte nevideli, ale aj tak. Obrovský prešľap v mojich očiach. Čakám na sakrametsky dobré vysvetlenie. Je ten zákon taký super, že sa zaň má hlasovať so Smerom a s SNS? Alebo som sa len mýlil, keď som si myslel, že netolerancia v KDH je na ústupe? Prosím KDH, aby mi to vysvetlilo, lebo teraz tomu nerozumiem a v tejto chvíli sú u mňa out. Možno som jediný, komu toto vadí a na mojom hlase im nezáleží, ale som sklamaný a muselo to von.   P.S. v komunálnych voľbách určite KDH môj hlas nemôže dostať. Ak v BA budú ešte 4 roky vládnuť KDH, tak sa zastavia aj posledné steblo trávy, posledný strom bude výrúbaný a developeri sa budú mať dobre. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (62)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        František Földeš 
                                        
                                            Z Bratislavy do Spišskej Novej Vsi na bicykli za tri dni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        František Földeš 
                                        
                                            Fico, Janukovič, Putin, ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        František Földeš 
                                        
                                            Malá rada Hruškymu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        František Földeš 
                                        
                                            Spoločná československá futbalová liga
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        František Földeš 
                                        
                                            Milí odborári a hlavne pán Machyna
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: František Földeš
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                František Földeš
            
         
        foldes.blog.sme.sk (rss)
         
                                     
     
        Bežec, futbalista ale predovšetkým svetoobčan.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    45
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2509
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            rodina
                        
                     
                                     
                        
                            šport
                        
                     
                                     
                        
                            politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            šport a marketing
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            marketing
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Petržalský beh pre radosť a zdravie
                                     
                                                                             
                                            Coremark
                                     
                                                                             
                                            Maratón
                                     
                                                                             
                                            Fiľakovský cross
                                     
                                                                             
                                            Engeraufans
                                     
                                                                             
                                            www.beh.szm.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Z Bratislavy do Spišskej Novej Vsi na bicykli za tri dni
                     
                                                         
                       Špinavému feťákovi sa ošetriť nedám
                     
                                                         
                       Môj boj s Univerzitnou nemocnicou Bratislava
                     
                                                         
                       Chcete dať svoje deti k skautom? Prečítajte si najprv toto.
                     
                                                         
                       V tejto krajine majú policajti kolty prekliate nízko
                     
                                                         
                       Moja skúsenosť s očkovaním
                     
                                                         
                       4.4. Svetový deň športu
                     
                                                         
                       Úžernícky scientológ na hrad!
                     
                                                         
                       Pozdrav z Dobrého anjela
                     
                                                         
                       Nechcem sa viac hanbiť za prezidenta – Kto je Andrej Kiska?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




