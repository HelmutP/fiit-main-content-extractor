
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boris Kačáni
                                        &gt;
                Politika
                     
                 Nezabudnime 

        
            
                                    25.6.2010
            o
            10:00
                        (upravené
                24.6.2010
                o
                17:06)
                        |
            Karma článku:
                23.01
            |
            Prečítané 
            6740-krát
                    
         
     
         
             

                 
                    Už to máme pár dní za sebou a môžeme s úľavou skonštatovať, misia splnená. Je tu konečne znovu nádej, že úpadok a rozkrádanie Slovenska sa zastaví, prípadne znovu otočí. Tento článok sa však bude venovať ešte raz a dúfam, že naposledy, zvrhlosti boľševicko-zlodejskej chunty, ktorá štyri roky mučila a vyciciavala Slovensko. Aby sme nezabudli...
                 

                 Teraz písať článok pre ficovoličov je asi úplne zbytočné, napriek tomu si ešte jeden neodpustím. Zvrhlosť, zákernosť a podlosť korporácie Smer na čele ktorej stojí u notára zazmluvnená figúrka v podobe psychopatického boľševika bola málokedy taká kryštálovo čistá, ako v ostatných dňoch. Ten cirkus, ktorí predvádzajú konkuruje najhoršej nočnej more.  
 S premáhaním som pozeral niekoľko debát, v ktorých som mal tú možnosť vidieť a počuť tých nímandov s gumenými maskami na ksichtoch, ako drístajú naďalej, sťa by bola ešte volebná kampaň. A pri tom už naozaj musí byť každému aspoň trošku normálnemu človeku jasné, že tlupa kmínov združená okolo tunelárskych finančných oligarchov je obyčajná banda zákerných chrapúňov. Niekoľko príkladov.   Vypliešťajú oči, vzdychajú, zvíjajú sa ako posolení červíci, vraj aké to bude šialené, vraj sa pravica nedohodne, lebo majú hlboké názorové nezhody. Fakt súdruh docent? A tvoja boľševická chunta, čo tu devastovala 4 roky Slovensko, tak ty si bol s jachtárovou stranou a mečiarovou stranou v súzvuku? Ty, čo si normálne verejne vyhlásil, že jachtár je zlodej? A Mečiara si posielal ešte pred rokom 2006 do horúcich pekiel ako najhoršieho vagabunda? Fakt, vážne nechutné.   Takže podľa smeráckych kmínov sa pravica nemá šancu dohodnúť. A ak áno, bude to katastrofa, oklamú voličov, rozpadnú sa a podobné drísty. Milí ficovoliči, vaša superstar socialistického neba by si dala rada Fedorom gule odtrhnúť (za ktoré ho drží), ak by ktorákoľvek z tých pravicových strán bola ochotná ísť s ním obcovať v koalícii. Ak by bol v parlamente aj sám Satan rohatý, do pol boka odratý a mal by záujem o súdruhov z boľševickej chunty, tak ho váš idol berie všetkými desiatimi! To by mu už nevadil ani „najväčší prúser v histórii demokratického Slovenska", financovanie SDKÚ. Ani marihuana, ani Vatikánska zmluva, skrátka ktokoľvek by mohol s ním vládnuť. Tak tomu sa hovorí principiálnosť a morálka, vážení!!!   Ale vy vlastne máte neskutočné šťastie. Viac šťastia ako rozumu. Pravica to tu zase dá ako tak do poriadku a na tom budete participovať aj vy, tí ktorí ste zostali tak blbí, že ste chceli nechať vládnuť boľševickú chuntu. Nedokážem to zmeniť, ale tak veľmi by som vám doprial toho psychopata, ak by sa dalo, aby ste si ho zobrali niekde do prdele a všetci spoločne by ste si užili to lúpežné komando. Je vás medzi nami tak strašne veľa, až ma to desí. Celých 35% obyvateľstva chcelo, aby naša krajina skrachovala. Neuveriteľné.   Ďalší príklad. Zmluvy sa podpisujú zbesilým tempom (väčšina je už aj tak podpísaná, paraziti sa zabezpečili už pred voľbami, žiadne strachy), poslednými kšeftami sa to len tak hemží, ale na podpis odblokovania záchranného balíka štátov eurozóny, na to mandát samozrejme nie je.   Alebo pomoc obetiam povodniam. Je mi do plaču, keď si spomínam, ako sa súdruh pred voľbami v gumákoch s ustarostenou maskou na ksichte producíroval pred kamerami, „zachraňoval životy" a vykrikoval „všetkých odškodníme!". Tak to teraz vidíte, ako odškodnil! Dokonca obce dostali len 10% z podielových daní a sú pred krachom! To je boľševikova ozajstná tvár.   Alebo tak dobre a úspešne skrývaný deficit, ktorý sa podarilo spolu s tou nagélovanou figúrkou zastupujúcou finančné skupiny tajiť až do volieb. Až teraz zisťujeme, aká katastrofa to je. A ten pankhart normálne vyštekne, že odovzdáva štát v super kondícii. Nie je toto už vážna diagnóza? Keďže sám k psychiatrovi nepôjde, diagnózu musia určiť jeho voliči. Všetky príznaky sú viditeľné. Jedinou terapiou je odchod do večných lovíšť aj s celou tou bandou grázlov a tupcov. Ale to je len môj sen, zrejme tejto rakoviny sa tak skoro nezbavíme.   Jedno je isté. Nie je najmenšia šanca, že by sa boľševik a jeho klony zmenili. Dni po voľbách ukázali, že patologická túžba po moci a rabovaní prerástla do neliečiteľnej formy.   Mne osobne by to nevadilo, veď tárajov, klamárov, zlodejov, tunelárov máme aj v bežnom živote okolo seba požehnane. Ale nikoho nenapadne, že by takúto luzu zvolil, aby riadila štát. Preto mne osobne vadí to, čo vadí, je to hrôza, že až také veľké množstvo voličov je tak katastrofálne mimo!   Až sa mi ježia chlpy na zadku, keď si len tak „light" pomyslím na všetky tie podvody a tunely, ktoré za štyri roky postíhali napáchať. Všetky tie nástenky, emisie, sociálne podniky, cencúle, tá hanba keď si tu pomaly mohli kontrolóri z Bruselu prenajať byty na trvalý pobyt, deficity, dlhy, skorumpovanú justíciu, štátny aparát, no skrátka katastrofa. To je záverečný účet za štvorročný cech boľševicko-nacionalisticko-zlodejskej chunty. Ale za ten účet ste zodpovední vy, ktorí ste im dali svoj hlas. Škoda, že nie je možnosť, aby ste ho zaplatili z vlastného.   Ja za seba môžem prehlásiť, že misia je splnená. Viac či menej sa naplnilo moje motto, ktoré mám pod fotografiou. Preto asi odchádzam do blogerského dôchodku. Veľmi by som bol nerád, keby ma nová vláda donútila k návratu. Ak budete robiť skopičiny priatelia, natriem vám to veľmi rýchlo.   Jedno nám ale musí byť jasné. Nejaká vážna čistota verejného života nehrozí. Bude to samozrejme tisíc krát lepšie ako to bolo, ale nerobme si ilúzie. Na každej politickej strane je nacucnuté hejno parazitov, ktorí už dnes majú trvalú erekciu z prichádzajúceho biznisu. Tak to bude žiaľ aj teraz. Podstatné je, aby toho nebolo veľa, aby to nebolo extrémne drzé a aby si mohol občas „čuchnúť" aj normálny človek bez politických konexií.   Tak či onak, nová vláda musí byť mimoriadne ostražitá. Už onedlho začne boľševikova chorá myseľ spriadať kulehy a intrigy. Otrase sa, chvíľu asi počká a udrie všetkými možnými svinskými spôsobmi.   Záverom. Na čas sme sa zbavili tej najhoršej luzy, akú dokázala vyplaviť politická žumpa. Prekazili sme pokračovanie fungovania štátnej mafie, ožobračovania Slovenska, vyhnali sme banditov z verejného sektora, odstavili sme vlastizradcov od výkonnej moci. Podlosť, zákernosť a drzosť minulej vlády nemala obdobu. Škody napáchané finančníkmi a ich nastrčenými figúrkami skrývajúcimi sa za infantilné socialistické slogany, ktoré celkom otvorene ovládli médiá a justíciu sú obrovské. Humus a špina o ktorej ešte nevieme sa objavia dodatočne. Akokoľvek, vyhliadky sú konečne predsa len optimistickejšie.   No a blížia sa moje posledné vety. Ešte raz chcem úprimne poďakovať mojim virtuálnym priateľom za podporu, za povzbudivé slová a pochvaly. Teda tým, ktorí ma držali v nádeji, že nie sme úplné stádo blbov. Tým, ktorých podpora pre mňa znamenala nekonečne veľa, najmä vo chvíľach, kedy som už bol frustrovaný a chcel som rezignovať. Dúfam a verím, že aspoň niektorých moje články pobavili a spríjemnili im deň. Priatelia, ďakujem. Len pre drobnú ilustráciu, dal som si tú námahu a spočítal som si čítanosť. Samého ma to ohúrilo. Za obdobie niečo vyše roka, presne od 1.3.2009 do 11.6.2010 som uverejnil 36 článkov, na ktoré bolo kliknuté 260.232 krát.   Takže v tejto chvíli sa s vami všetkými lúčim, držme si navzájom palce, nech sa nebudem musieť vrátiť k písaniu, nech sa už konečne podarí aj na Slovensku implementovať trochu normálnosti a slušnosti do verejného života, nech sa nám začne lepšie dariť. A samozrejme to klasické zdravíčko a šťastíčko.   P.S. Na mojom inom blogu som napísal ešte článok z mojich vyše trojročných skúseností z písania tu na sme.sk. Je to také zhodnotenie, trošku nejaké veci z kuchyne a moje postrehy, či názory na blog. Koho by to zaujímalo, tu je link: http://boriskacani.blog.cz/1006/moje-skusenosti-z-blogovania-na-sme  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (58)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Mozog je plastický
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            List jeho excelencii od organizátora protestov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Tušenie budúcnosti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Najväčšia európska lúpež
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Kačáni 
                                        
                                            Reinkarnácia Štefánika?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boris Kačáni
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boris Kačáni
            
         
        kacani.blog.sme.sk (rss)
         
                                     
     
        Potrím k tým, ktorí nikdy neparazitovali na štáte, ktorí nekradli a nekorumpovali. Teda k menšine.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    103
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5713
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            môj iný blog
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            vysúšanie muriva
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




