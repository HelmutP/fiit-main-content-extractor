
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Július Farkaš
                                        &gt;
                Nezaradené
                     
                 Ragga Jungle 

        
            
                                    11.7.2008
            o
            22:52
                        |
            Karma článku:
                4.27
            |
            Prečítané 
            1388-krát
                    
         
     
         
             

                 
                    Ragga jungle je jedným zo štýlov Jungle - Drum'n'bass-u, ovplyvnený ragga, reggae a dancehall prvkami.  Nasledujúcich pár riadkov vnesie iskierky svetla do tejto nekomerčnej časti elektronickej scény.
                 

                  Jungle je štýl elektronickej hudby. Vyvinul sa z britskej hardcorovej scény na začiatku 90tych rokov minulého storočia. Raveová scéna sa v tom čase začala inovovať - zvýšil sa počet bpm (beatov za minútu) zo 120 na 145,ubúdal vplyv techna, disca a housu, ale pribúdali ragga a dancehallové prvky. Nastala zásadná zmena štruktúry rytmu - rovný beat sa rozbil, polámal.                Hoci prvý krát pojem "jungle" použil ešte v dvadsiatych rokoch 20. storočia Duke Ellington, jazzový velikán, na označenie typického zvuku svojej kapely, dnes pod junglom rozumieme:  - drum'n'bass (súčasný terminus technikus zahrnujúci všetky hudobné žánre junglu a drumandbassu)  - oldschool jungle (štýl charakterizujúci začiatky d´n´b, produkovaný dodnes)  - ragga jungle (podštýl oldschool junglu,charakteristický ragga vokálmi)                Aký je rozdiel medzi ragga a reggae? Raggamuffin (skrátene Ragga) sa spája so vzostupom elektronickej tanečnej hudby. Tá sa začala prudko šíriť v 90´s a ragga nebolo žiadnou výnimkou - svojim spôsobom prispelo k revolúcii v samotnom reggae. Jedným z najhlavnejších dôvodov náhlej popularity ragga bola všeobecne menej náročná a lacnejšia produkcia než reggae v podaní tradičných hudobných nástrojov. Ragga ovplyvnilo raný jungle do tej miery, že kdesi v ich priesečníku vznikol štýl dnes známy ako ragga jungle.                 Aj napriek tomu, že všeobecne neexistuje jednoznačne definovateľný rozdiel medzi drum´n´bassom a junglom, ja osobne v jungli vidím menej pevnú štruktúru taktu (obrátený beat sa vzďaľuje od prvého beatu), viac oldschool samplov ako aj väčšie zastúpenie perkusií. Podľa toho by som "oficiálne známy" ragga jungle ešte ďalej rozdelil na dva podštýly:  - ragga jungle (staršiu tvorbu reprezentuje label Congo Natty, tú novšiu nájdeme na labeli Chopstick Dubplate. Staršia a nová tvorba sa výrazne líši samplami, prechodmi ako aj kvalitou zvuku: toto je niečo, čo napríklad taký producent Tester dodnes nepochopil :-), aj keď jeho hudobným nápadom niet čo uprieť).  - ragga drum´n´bass (Producenti ako Shy FX, Kenny Ken, Benny Page, raggovo ladená tvorba Simona Bassline Smitha alebo skladby ako Pendulum - Tarantula a Subfocus - Soundguy).                Poznámka: Stofy su v štýle Ragga Drum'n'Bass a refrény v štýle Ragga Jungle      Podľa mojich skúseností práve tá časť ragga junglu, ktorú nazývam ragga drum'n'bass, je vo väčšej miere prijateľná pre mainstreamových clubberov. Tá ozajstná ragga junglová hudba je rytmicky natoľko nesúdržná (a práve preto pre mňa príťažlivejšia), že pre veľa d´n´b fanúšikov je tanečne ťažko zvládnuteľná (a z toho zrejme vyplýva jej menšia popularita ).                 Ragga Jungle je produkovaný mnohými labelmi a producentmi.  Ťažnou silou tohto štýlu je v tejto dobe hlavne Severná Amerika (Kanada a Spojené Štáty). Britská scéna stagnuje a už veľmi dlho neprišla so žiadnou novou sériou nabitých trakov.  Benny Page a nádejný nováčik Psychofreud na to evidentne nestačia, kedže sa orientujú zväčša ragga Drum’n’Bass. Nedá mi nedodať,  že obe scény (severoamerická a britská) fungujú skoro nezávisle a približne do roku 2000 bolo aj pomerne ťažké zohnať vačsinu zámorských releasov v Európe. Z oboch spomeniem aspoň zopár mien, ktoré u mňa zarezonovali a podľa mňa púšťajú na trh hodnotné tracky:  - Labely: Celestial Conspiracy, Chopstick Duplate, Clash, Congo Natty, Dubwize, Digital SoundBoy, Dirty Dubs, Humble, Jungle Royale, JungleX, Knowledge &amp; Wisdom, Mad Dem Sound, Mashit, Mix'n'Blen, Nitrous Oxide a dcersky label Big Cat , Rockers Dubs, Royal Crown, RudeBoy Plastic, Souljah, Tricky Tunes, Tuff Gang, Zion Gate Records.  - Producenti: 16Armedjack, Benny Page (Soundclash), Bong-Ra, Capital J, Debaser, DJ K, General Malice, Heretic, Jahba, Jackie Murda (Chopsticks), Kenny Ken, KGB Kid, Krinjah, Paulie Wallnuts, Rebel MC (Conquering Lion, Congo Natty, X Project),  Psychofreud, Roughcut, Serial Killaz, Simon Bassline Smith, Shy FX, Souljah, Skoobz, Soundmurderer &amp; SK1, Sumone, Visionary, Tester, Terry T, The Upfull Rockers, Top Cat, Tribe Of Issachar, Twinhooker.                 Prirodzene, tak ako každý iný hudobný štýl, aj ragga jungle sa neustále vyvíja, triešti a následne spája s ďalšími štýlmi, aby v procese vývoja mohlo dochádzať k vzniku úplne nových druhov (hudby :-) ). Na scéne sa začiatkom 21. storočia objavuje RaggaCore, špecifický divokosťou a obsahom množstva industriálnych a experimentálnych prvkov. Je ešte o zhruba 20% rýchlejší než Jungle a môže obsahovať viacero motívov striedajúcich sa neobvyklým spôsobom (d´n´b a jungle vyžaduje pravidelné striedanie 16tkových pasáží a dejovú plynulosť motívu). Prvá raggacoreová platňa, ktorú som počul, vo mne vyvolala dojem Squarepusherových trackov s nádychom ragga. Na rozdiel od známejšieho breakcoru kladie väčší dôraz na tanečnosť (čo je v podstate tiež relatívne). Ak má niekto záujem siahnuť po takejto produkcii, odporúčam label DAMAGE (Peace Off) - ponúka pestrý výber tejto muziky. Do pozornosti dávam tvorbu Endusera, Cardopushera a Aarona Spectre.                 Úplne najsamprvá lastovička ragga junglu na Slovensku ostala pre mňa neznáma. Medzi pvými "raggajunglistmi" určite boli Bojdy (NoBong!) a MTK23. Títo dvaja Bratislavčania založili vlastný ragga junglový soundsystem Natty Beats. Neskôr sa k nim do tohto zoskupenia ešte pridal Pedro, člen starého českého soundsystému Cirkus Aliens, ktorého určite poznajú starí harcovníci z UW caffe. Pošuškáva sa, že časom MTK23 presedlal na trance a Bojdy sa ešte kde-tu objaví na nejakej akcii, Pedrovo meno sa dnes skloňuje so širokou škálou hudobných štýlov s prvkami reggae a ragga. Do kompletného obrázka prešporských pionierov chýba už len Effiks. Ani ten nebol čistý ragga junglista, orientoval sa aj na iné odnože elektronickej hudby. Okrem Bratislavy bolo možné začuť ragga jungle aj na akciách v okolí Nových Zámkov, hrával ho v rámci d´n´b X2um, hoci už presedlal viac na afrodajtovky a liquid. Dá sa povedať, že všetci títo priekopníci buď od ragga junglu upustili, alebo na akciách hrávajú už len príležitostne, respektíve sa ich mená vytratili z povedomia scény aj napriek tomu, že sa stále snažia na nej pôsobiť.                 V ragga junglovej začatej "takmertradícii" však ďalej pokračuje nováčik Fezry z Nových Zámkov, hoci naň nie je "vysadený" :-) a zahrá aj oldschool a ostatný dramáč. Ďalšími nováčikmi na domácej ragga junglovej scéne sú enEmy a Drumazz, menej známé tvár z Levíc, avšak predpokladáme, že sa to v dohľadnej dobe zmení :-). Z okolia Čadce je slečna Mass-Ada, ktorá občasne hráva na lokálnych akciách. "Osobitným prípadom" je Selector crew (ktorý vznikol v roku 2005), teda Bugan, Wiruz a Stanley, a hráva výlučne ragga jungle (a takýmito článkami si robí reklamu :-P). Podotýkam, že mimo mojich spolubojovníkov z crew by som nechcel nikoho hodnotit, kedže informácie o ich vystúpeniach sú nejednotné, i keď napríklad ja vkladám nádej do už spomínaného enEmy-ho. Vopred sa ospravedľnujem každému DJ, ktorý nestihol dostatočne rozvíriť prach a dať tak o sebe vedieť.                Na Slovensku nemáme reálne rezidentné ragga junglové nocí. Ani  periodicita špeciálnych edícií zameraných na tento štýl nie je potešeniahodná. V minulosti bola evidentná snaha v bratislavskom subclube s párty Wicked Jungle, ktorá sa však nedožila svojho tretieho pokračovania. Aspoň čiastočnou náplasťou je event Neckbreak v Trstíne, na ktorom počuť síce viac štýlov, ale vždy dostatočné zastúpenie ragga junglu. Na druhú stranu nedostatok akcií dáva tomuto štýlu nezvyčajnosť a šmrnc. Osobne sa desím predstavy, že by Ragga Jungle dospel do mainstreamového štádia, ktoré teraz prežívaj jeho mladší brat - Drum'n'Bass.     Toto bol môj pohľad na ragga jungle a scénu okolo neho. Ďakujem manželke za výpomoc, wikipédii, google a nespočetnému počtu webov, ktoré som použil ako zdroje.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Július Farkaš
            
         
        juliusfarkas.blog.sme.sk (rss)
         
                                     
     
        Ragga Junglový Selektor
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1388
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




