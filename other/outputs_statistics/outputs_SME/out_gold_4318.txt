

 Zišli sa tu duše spriaznené, aby potešili srdcia životom skúšané. 
 Vytvorili sa priateľstvá milé, mnohé prekročili aj hranice a iné prerástli z virtuálnych na ozajstné. 
 Každý deň sa tešíme, na tie pekné slová a obrázky vymaľované. 
 Hoci, každá duša, iné zamestnanie má, denne sa rýchlo na blog ponáhľa. 
 Po prečítaní zaujímavých básni a článkov, nabíjame naše srdcia krásnym nábojom - spolupatričnosťou a kamarátstvom. 
 Chcem všetkým milým poďakovať, že moja maličkosť smie životnú silu, z týchto vašich darov, čerpať. 

