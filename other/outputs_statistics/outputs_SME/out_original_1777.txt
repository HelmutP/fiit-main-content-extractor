
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lýdia Koňárová
                                        &gt;
                Španielsko
                     
                 Tajná láska poetky Wallady, poslednej omejadskej princeznej 

        
            
                                    12.4.2011
            o
            8:12
                        (upravené
                12.4.2011
                o
                11:09)
                        |
            Karma článku:
                3.80
            |
            Prečítané 
            482-krát
                    
         
     
         
             

                 
                    Významná moslimská poetka Wallada bint al-Mustakfi, arabsky ولادة بنت المستكفي sa narodí v roku 994 ako dcéra Mohameda III., posledného kalifa z Córdoby a kresťanskej otrokyne Amin´am. Obdivovatelia krásy sú vo vytržení z jej provokatívnych, priesvitných, zlatom  vyšívaných šiat s ľúbostnými básňami, ako z háremu v Bagdade. Na  verejnosť chodí s odhalenou tvárou, bez hidžábu.    Strieda milencov. Ponúkne vzdelanie ženám bez rozdielu pôvodu, aj otrokyniam.
                 

                        Posledná omejadská princezná a významná moslimská poetka Wallada bint al-Mustakfi, arabsky ولادة بنت المستكفي sa narodí v roku 994 ako prekrásna dcéra Mohameda III., posledného kalifa z Córdoby a kresťanskej otrokyne Amin´am.                    Detstvo a mladosť prežíva v politických intrigách a občianskej vojne, pochovávajúcej Córdobsky kalifát. Otec si ukradne trón v roku 1024 vraždou kalifa Abd ar-Rahmána V. Politickí protivníci mu to spočítajú likvidáciou syna Mansúra al-Muzzu a po dvoch rokoch si v Uclés podajú aj jeho.                   Nová emirka Wallada začne písať prvé stránky nezávislého kráľovstva taifas. Ponúkne palác vzdelávaniu dievčat a žien bez rozdielu pôvodu, z rodín šľachticov, chudobných, ba aj otrokyniam. Hlavnými vyučovacími predmetmi sú Poézia a Umenie lásky.                    V roku 1027 otvorí Majaliss al Adab, prvý literárny salón v meste. Stretávajú sa v ňom intelektuáli, filozofi a umelci. Vyhlasuje súťaže poézie a ako jediná žena sa ich zúčastňuje.                   Na svoju dobu city vyjadruje otvorene a odvážne:            Cuando caiga la tarde, espera mi visita, pues veo que la noche es  quien mejor encubre los secretos;  siento un amor por ti,  que si los astros lo sintiesen  no brillaría el sol,  ni la luna saldría y las estrellas  no emprenderían su viaje nocturno.           Keď sa zvečerieva, čakám návštevu,   veď viem, že noc je    tá najlepšia skrýša tajomstiev    cítim lásku k tebe   ba aj hviezdy to cítia    slnko nesvieti   ani mesiac nevyšiel a hviezdy    sa nevydali na svoje nočné putovanie.                    Častým návštevníkom je jej zbožňovateľ, slávny filozof, literát, psychológ, historik, právnik a teológ Abū Mohamed Alī ibn Ahmad ibn Saīd ibn Ḥazm, arabsky أبو محمد علي بن احمد بن سعيد بن حزم n(994 - 1065).                Hlásateľ myšlienok perzskej školy Záhirí a autor štyristo diel o logike, etike, dejinách, práve a teológii nie je len teoretický suchár. Zaľúbené verše Sobre el collar de la paloma, Na náhrdelníku holubice prezrádzajú, že neuteká ani pred láskou.                   Obdivovatelia krásy, inteligencie, vzdelania a hrdosti ležia Wallade pri nohách. Sú vo vytržení z jej plavých vlasov, modrých očí, svetlej pleti a provokatívnych, priesvitných, zlatom vyšívaných šiat s ľúbostnými básňami, ako z háremu v Bagdade. Na verejnosť chodí s odhalenou tvárou, bez hidžábu.        Strieda milencov. Fundamentalistickí mulahovia na čele so slávnym Averoem ju idú roztrhať v zuboch a na každom kroku kritizujú jej zvrátené správanie.                    Vášnivo sa zaľúbi do Abu al-Waleed Ahmad ibn Zajdúna al-Makhzumiho, أبو الوليد أحمد بن زيدون المخزومي (Córdoba 1003 – Sevilla 1071) básnika, obľúbeného v Córdobe a Seville, muža s veľkým politickým vplyvom, šľachtica zo starobylej dynastie Bani Makhzum, jednej z najstarších v Al-Andalus. Dedko Mohamed Al-Quaïssi vykonával funkciu kadiho (sudcu) a šéfa civilnej gardy mesta, tato bol slávny teológ.                    On jej lásku opätuje, no je stúpencom konkurenčnej politickej dynastie Banu Jawhar, a tak sa schádzajú potajomky.                    Z dodnes zachovaných deviatich básní je osem venovaných jemu. Päť ho štipľavo satiricky kritizuje, okrem iného aj pre mužských milencov. Tri nostalgicky smútia za príliš rýchlo skončenou láskou a túžia byť opäť s ním.                   Žiarlivo zúria, plačú hlbokým sklamaním a vyčítajú mu neveru. Podľa jedných s čiernou otrokyňou, ktorú kúpila Wallada a darovala jej vzdelanie, podľa druhých s otrokom, čiernym ako eben. Tretia verzia tvrdí, že nevera s čiernym otrokom a otrokyňou bol v islamskej literatúre tých čias častý námet a neodolala mu ani Wallada, a teda neopisovala svoje pocity:            Sabes que soy la luna de los cielos   mas, para mi desgracia, has preferido a un oscuro planeta.           Vieš, že som mesiac na oblohe   ale, na moje zahanbenie, máš radšej tmavšiu planétu.                   Ibn Zajdún odpovedá, že slobodu a nezávislosť si cení zo všetkého najviac. Už ju nikdy neuvidí.        Wallada sa vrhne do náručia jeho najväčšieho politického protivníka, vezíra ibn Abdúsa. Je do nej šialene zaľúbený a na ibn Zajdúna nevysloviteľne žiarli. Zhabe mu majetok a vyhlási na neho zatykač.        Zaberie to. Wallada sa k nemu presťahuje do paláca, no nikdy sa za neho nevydá. On stojí pri jej boku a ochraňuje ju pred politickými protivníkmi až do svojej smrti.                   Ibn Zajdún utečie do Sevilly a hľadá ochranu u Abbada II. a jeho syna al-Mu'tamida. V exile píše verše  o nenávratne stratenej láske a mladosti a nostalgicky spomína na rodnú Córdobu:            „Boh zoslal dážď nad opustené príbytky tých, ktorých milujeme. Utkal nad nimi pruhované, mnohofarebné rúcho z kvetov a nad nimi žiari jeden v tvare hviezdy.    Koľko dievčat ako obrázok priťahuje svojimi rúchami, ako tie kvety, keď život bol čerstvý a čas k našim službám ...    Aké šťastné boli tie dni, ktoré skončili; dni potešenia, keď sme žili s tými, ktorých vlasy voľne splývali na chrbát a biele ramená ...“                   Wallada zomiera takmer storočná 26. júna 1091.            Jedna z jej najtalentovanejších žiačok, Mujo bint al –Tajjani, dcéra chudobného predavača fíg sa po jej smrti svojsky poďakuje za vzdelanie a šancu žiť na dvore. Začne písať o svojej učiteľke kruto satirické a zlomyseľne výsmešné pamflety.               Zakomplexovane si uvedomuje, že kvalitu originálu nikdy nedosiahne ?   Žiarlila na jej krásu a úspechy u mužov ?   Ako jej Wallada ublížila ?                    Matilde Cabello sa pokúsi po tisíc rokoch o jej rehabilitáciu životopisom Wallada, La última luna (2005).                Foto: Córdoba  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Zo Smrtnej hory fúka na Trygve Gulbranssena
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Pizza Clandestina s Vitom Corleone
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Ako Pavol Jantausch vzdelaním národné sebavedomie podporoval
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            De Syv Sostre, Sedem sestier
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lýdia Koňárová 
                                        
                                            Diego Velázquez - Las Hilanderas
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lýdia Koňárová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lýdia Koňárová
            
         
        konarova.blog.sme.sk (rss)
         
                                     
     
        ochranca prírody a pamiatok
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    54
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3598
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Chansons
                        
                     
                                     
                        
                            Bretónsko
                        
                     
                                     
                        
                            Capri
                        
                     
                                     
                        
                            Korzika
                        
                     
                                     
                        
                            Normandia
                        
                     
                                     
                        
                            Nórsko
                        
                     
                                     
                        
                            Paris
                        
                     
                                     
                        
                            Prado
                        
                     
                                     
                        
                            Pro veritate cum caritate
                        
                     
                                     
                        
                            Sardínia
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Škótsko
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Thassos
                        
                     
                                     
                        
                            Wien
                        
                     
                                     
                        
                            Zámky na Loire
                        
                     
                                     
                        
                            P&amp;#234;le-m&amp;#234;le
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Granada, Boabdil a kráľovná Aisha
                                     
                                                                             
                                            Meč z Toleda, tajný sen každého kráľa
                                     
                                                                             
                                            Diego Velázquez - Los Borrachos
                                     
                                                                             
                                            Vodný výťah pre lode vo Falkirku
                                     
                                                                             
                                            Siracusa, jedno z najslávnejších miest starovekého Stredomoria
                                     
                                                                             
                                            Córdobsky emirát a kalifova mešita
                                     
                                                                             
                                            Zámok Cheverny a Mona Lisa v oranžérii
                                     
                                                                             
                                            Zámok Chambord a dvojité schodisko Leonarda da Vinciho
                                     
                                                                             
                                            Zámok Ussé a Kráska spiaca v lese
                                     
                                                                             
                                            Zámok Blois a prekliaty básnik François Villon I.
                                     
                                                                             
                                            Zámok Blois, kabinet s jedmi a kredenc II.
                                     
                                                                             
                                            Aliki a pristávacia dráha pre mimozemšťanov
                                     
                                                                             
                                            Florencia, Catherine de Medicis a zámok Louvre
                                     
                                                                             
                                            Bayeux, Odon a tapiséria kráľovnej Matildy
                                     
                                                                             
                                            Perníkové chalúpky v Kerascoete
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




