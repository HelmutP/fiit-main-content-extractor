
 Nemám v úmysle opakovať a vyvracať alebo podporovať všetko, čo na túto tému včera a dnes zaznelo. Mám však pár komentárov. 
 
Mnoho ľudí si lacno koplo do ministerstva školstva. Vraj keď nie sú učebnice, peniaze na školy a pod., tak (parafrázujem) prečo vyhadzuje peniaze na nejaký šport. Neinformovaným pripomínam, že ministerstvo školstva má v kompetencii okrem iného i štátnu starostlivosť o šport. Napriek tomu, že sa to v názve nespomína. Preto je pochopiteľné, že pokiaľ niekto z výkonnej moci má riešiť peniaze na športovú akciu, je to v prvom rade ministerstvo školstva. Okrem toho, (toto predpokladám) rozpočtové peniaze na šport a na školstvo samotné sú oddelené a keď si raz v vláda povie, že dá na šport, tak to nevezme priamo práve školstvu. Samozrejme, niekde to vezme... 
 
Druhá vec je žiadosť golfového klubu o štátnu podporu pri organizovaní medzinárodnej akcie. Či sa nám to páči alebo nie, v podstate každý môže požiadať. Priamo by som teda určite nekritizoval organizátorov za to, že požiadali. Dokonca i na argumentácii pána Sotáka, že (parafrázujem) prečo by oni na Horehroní nemohli na nejakú akciu dostať peniaze, či oni sú len na platenie daní (koniec parafrázy), a priori nevidím nič zlé.  
 
Fajn. Týmito dvomi bodmi však nechcem povedať, že s takouto podporou tohto turnaja súhlasím. Lebo... 
 
... otázka je, prečo by vláda mala dať pol milióna eur práve na golfový turnaj. Hlavným argumentom, ktorý pre podporu zaznel, je tento (citujem z článku na SME): 
 

Vzhľadom na to, že možný televízny dosah je viac ako 800 miliónov domácností v celom svete, turnaj LET zabezpečí Slovenskej republike, okrem výnimočného športového zážitku, aj mimoriadne rozsiahlu, efektívnu a účinnú medzinárodnú propagáciu," uviedlo ministerstvo. Majstrovstvá môžu podľa neho prispieť k rozvoju cestovného ruchu a v budúcnosti aj k zníženiu nezamestnanosti v regióne. 

 
Nuž, najmä tých 800 miliónov domácností je vrcholne podozrivých. Jednak pochybujem, že si z tých možných niekoľko miliárd divákov, ku ktorým sa správa o turnaji môže dostať, nejaká podstatná časť vôbec všimne, že Tále, Slovakia, aké kúl. Druhak, pokiaľ sú tieto televízne prenosy a šoty nejako výrazne lukratívne, ako je možné, že sa významné firmy a reklamné agentúry nebijú o reklamný priestor. Milión eur, to pri takom obrovskom dosahu nie je žiaden peniaz. Ibaže by... 
 
Ďalej predpokladané použitie: 
 

Celkový rozpočet golfových majstrovstiev predstavuje 1,14 milióna eur, z tohto necelá tretina je určená na výhry pre hráčky. Víťazka by si mala odniesť 52 500 eur. Väčšia časť štátnej podpory, 330-tisíc eur, by sa mala využiť na usporiadanie turnaja, štvrť milióna eur na aktivity, ktoré majú Slovensko vo svete zviditeľniť. 

 
Najmä ten štvrť milión po uplynulých skúsenostiach s reklamou štátu smrdí vyhodenosťou. Slovensko - krajina môjho srdca? To srdce akosi krváca. 
 
A nakoniec otázka štátnej podpory konkrétneho podujatia alebo golfu ako takého. O štátne peniaze sa uchádzajú mnohí. Pokiaľ má štát v úmysle podporovať šport, mal by si predovšetkým mal určiť priority. Chce podporovať športy všetky rovnakopodľa nejakých kritérií? Alebo len niektoré? A ktoré - úspešné? Nádejné? Športy s oprávnenými olympijskými ambíciami? Šport vrcholový alebo masový? Športy chudobných? Alebo športy bohatých? 
 
Naschvál som si "športy bohatých" nechal ako posledné. Či chceme alebo nie, na Slovensku je golf - a ešte dlho bude - predovšetkým športom bohatých. To nemyslím pejoratívne, konštatujem to ako fakt. A je ho treba financovať? Prečo nie. Ale prečo rovno pol miliónom eur na jedno podujatie?  
 
Aby sme dostali celú záležitosť do nejakého meradla, na stánke ministerstva školstva som našiel prehľad dotácií športu v roku 2009 (xls). Financovanie podujatí je na treťom liste 02606-Podujatia. Spolu boli schválené dotácie na 117 podujatí v celkovej sume málo cez 1 milión eur. Z nich najväčšia bola na 2. kolo svetového pohára v kanoistike na divokej vode. Hádajte koľko! Ďalej nasledujú Atletický most 2009 míting EAA Permit, GP CSIO Svetový pohár družstiev a SP jednotlivcov (jazdectvo), ME juniorov a do 23 rokov v kanoistike na divokej vode, mnohé podujatia rôznych Európskych a Svetových pohárov. 
 
Tak čo, koľko ste hádali? Správna odpoveď na hádanku z predchádzajúceho odstavca je 50 000. 
 
A teraz by sa na jedno podujatie malo vysoliť desaťkrát toľko. Tu je fakt niečo divné. 
 
 
 
 
 
 
P.S. Priznávam sa, že som v tom Exceli o jednu stranu ďalej... 
 
 
