

   
 A tešili sme sa aj my, lebo sýkorky sme tu ešte nemali. Navyše sýkorčie mláďatá majú zjavne slabšie zadočné svaly a narozdiel od lastovičích kolegov na okno „nedostrelia". Lenže čo čert nechcel prišiel 12. jún, 22.41 večer. Všetci sme sedeli natlačení pred televíznou obrazovkou čakajúc, kedy už Markíza zverejní očakávané exit polls. A zrazu blik - TV, internet vypadli. Po krátkom vyhliadnutí z balkóna sme spoznali následky cca 2-minútového (tornáda?), toho istého ktoré predtým na juhu Slovenska posunulo voľby o 30 minút. 
 Tak sme sa výsledkov nedočkali, ale omnoho horšie skončili sýkorky. Nasledujúce ráno sme zistili, že (večne nadržaného) samčeka niekam odfúklo a samička zjavne štyri hladné krky sama nestíhala. To už mláďatá nevydržali a nejaká sprostá evolúcia ich naučila v dobe neznesiteľného hladu vyskákať z hniezda (všetky do jednej). Tri ktoré nezabudli čvirikať sme našli a pozbierali, tomu štvrtému sa asi neotvoril padák (bývame na 4. poschodí). 
   
 Tak hladné tvory som ešte v živote nevidel, dokonca ani kamoša ortodoxného katolíka, ktorý si ustanovenia Zákona č. 1/325 Z.z. „Biblia", článok XVII, § 21 ods. 2 písm. d) s názvom „Pôst" vykladá až príliš doslovne. Inými slovami, boli veľmi veľmi veľmi (30x) hladné. Tak sme ich rýchlo nakŕmili všetkým živým (lietajúcim, kráčajúcim aj plazúcim), čo byt dal a následne napakované do dostatočne vysokej krabice s výstelkou (sú to dobrí skokani) sme ich vyložili „starej" na balkón. Aby na ne nezabudla. 
 To sme ešte netušili, že jedno mláďa spapá približne 1x svoju hmotnosť každé 2 hodiny. Keď už svojimi skokmi začali vyvracať všetky Newtonove bláboly o gravitácii, muselo sa ísť do Tesca po múčne červy, rovno 100  gramov! Veď sme sa dočítali, že jedna sýkorka pri kŕmení mladých denne oberie zo stromov 500 húseníc a inej hávede. Naozaj tieto mladé boli ako trubice - jedným koncom dnu a druhým von. 
 Test jedlosti dopadol výborne, „stará" ponúknutými červami neohrdla a narvala ich asi 5 do každého mláďaťa. Naopak muchu nechala celý čas ležať úhorom, našťastie sa ale mladým nič nestalo z tých, ktorými sme ich predtým prikŕmili. Zdalo sa, že mladé sú zachránené. Na noc sme ich dali radšej dnu. Skoro ráno sme ich zase vyložili. To sme ale ešte netušili, že ranných 15 stupňov C bude pre mladé Mission Impossible. Jedno neprežilo, druhé len tak tak. Iba jeden nezmar, ten najvyvinutejší pokračoval v skokoch do výšky akoby nič. Ako dočerta mohli prežiť tieto „mrazy" keď ešte boli v hniezde? Zrejme ich matka cez noc zahrievala. Túto noc ale neprišla, žeby sa jej nepáčila tá krabica? 
 Nezostalo nám nič iné, ako každú noc ich brať dnu a ráno ich vyložiť striedavo vždy na max. pár minút. Aby stará na ne nezabudla (áno rodičovský pud po čase bez kontaktu s mladými vyprchá). Po akom čase? Srnky netušia. Nechceli sme riskovať. 
   
 Kto videl aspoň jednu časť zo série Nezvratný osud (Final Destination), tomu asi budú nasledujúce riadky pripadať povedomé. Teta Smrtka sa zjavne rozhodla dokončiť to, čo sa jej nepodarilo 12. ani 13. júna. Prišli sibírske mrazy, v noci 13 stupňov, o deň 12 stupňov, potom 11 a nakoniec 9! K tomu sa pridal víchor a dážď. Cez deň 15 stupňov, neskôr 12, vietor a dážď, nakoniec krúpy. Toto počasie nemalo konca kraja a prekonávalo všetky júnové historické rekordy. Ďalšie mláďa padlo. Nevydržalo ani denné teploty. Večer  zomrelo zrejme na zápal pľuc alebo nejakú inú chorobu. Hrozný pohľad vidieť ho umierať. Ďalší deň som v práci asi moc HDP nevytvoril... Tak už ostal iba náš nezmar. 
 S vekom mu rástol apetít a stará teraz už nestíhala ani tohto jedináčika. Ešte že má nás a naše červy. Mizli v ňom ako v čiernej diere, jeho hlad nemal konca kraja a stále skákalo. Tréning robí majstra a my sme museli už tretíkrát vymeniť krabicu. Táto mala 1,5 metra do výšky. Zatiaľ pikovalo kolmo dole, na let ešte nebolo pripravené. Navyše noc by neprežilo ani v dvojitom skafandri, vonku sodoma gomora. 
 V sobotu bambusovým tempom pribral a významne mu podrástli letky a chvost. Bolo nám jasné, že čoskoro nebude stačiť ani posledná, ultimátna krabica. Pri lete už pekne zaberalo. Poobede si už každú chvíľu vyskočilo na roh krabice úplne suverénne. Tak sme ho nakoniec nechali, nech sa deje vôľa božia (resp. vôľa Zubatej). Preletelo asi 50 metrov na ten najvzdialenejší strom. Na prvýkrát mu ešte všetko nevyšlo, asi po 10 minútach bol na zemi. 
 Tak sme znova raz Bohovi (či Smrtke) vstúpili do rybníka a zobrali sme ho dnu. Už sme ho nejako ustrážili aj bez priviazania :) a poslednýkrát u nás nocoval. Ďalší deň sa na pár minút ukázalo Slnko a práve vtedy vyletel (teraz už takmer doslovne „vyletel"). V noci zase 9 stupňov, vietor a dážď a rovnaká predpoveď ešte 2 ďalšie noci. Tak a tu sa končí náš príbeh a aj jeho (nezmarov) príbeh. 
   
 Ale ono nie! Končil by sa, ale to sme ešte netušili, že tento nezmar v zárodku asi nejako zmutoval, lebo ráno už zase čivkal tým naliehavým hladným tónom na celé sídlisko. V tom vetre a chlade. A čivkal aj ďalšie a ďalšie ráno. Konečne prišla jar (leto malo prísť až o týždeň). Zubatá definitívne prehrala. Dnes už si poletuje po celom Martine. Naposledy sme jeho nezameniteľný tón nachvíľu začuli pred dvoma dňami, než zase odleteli s matkou inde. Či už sa naučilo kŕmiť samo nevieme, ale o tohto Chucka Norrisa nemáme najmenšie obavy. The (Happy) End. 
 Na záver zvyšok fotiek aka čítanka pre dyslektikov: 
   
  
  
   
  
  
  
   
  
   
  
   
  
   
  
  
   
  
   

