
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Denisa Hájková
                                        &gt;
                také dni
                     
                 la primavera 

        
            
                                    25.3.2010
            o
            12:50
                        (upravené
                25.3.2010
                o
                14:12)
                        |
            Karma článku:
                2.83
            |
            Prečítané 
            838-krát
                    
         
     
         
             

                 
                    Nechcem pripustiť, aby sa jar stala klišé. Je to momentovka. Kontinuálne plynutie záberov a flešbekov.
                 

                 
¿Que hora son mi corazón ?paul cezzane
                           Od dieťaťa.       Bliky smutných pocitov, ktoré som nevedela pomenovať. Bolo mi z toho len smutno. Možno to boli hrdličky, možno iné zvuky. Niečo sa menilo, vo vzduchu iný pohyb. A mne ako malému škôlkarovi to nerobilo dobre. Bola som osamelý pútnik. Pozorovala som prichádzajúce aj existujúce s maximálnou intenzitou. Tvrdnutie živice na dlaniach. Že to nejde dolu mydlom, ani pieskom. Že mám potom čierne ruky. Že deti sú rýchle a nič z toho čomu nerozumiem, ich netrápi. Keď sa topilo pole a ostávali len biele mapy po snehu, nemala som sa kam schovať. Blížila sa Veľká noc a vajcia v škrupinách. Rada som im manikúrovými nožničkami prepichovala konce, vyfukovala visiace bielka a natierala lakom na nechty. Starým, hnedým, čo sa hrčkavel a lepil na malý štetec. To bolo to najpríjemnejšie, čo si z jari pamätám. Acetón.       Tie fľakaté polia mi nikdy neboli sympatické. S malými prsíčkami pod fialovou vetrovkou z Poľska sme sa po nich brodili, s Katkou. Myslím, že to bol optimistický tvor. Popri nej som tie polia vnímala len periférne. Vtedy som mala najväčší zážitok z branného cvičenia a prázdneho bytu. Mohla som sa vtedy rozprávať sama so sebou, alebo so zrkadlom. A na brannom sledovať koordinátora voľného času. Osamelý bežec vo vlastnoručne uštrikovanom svetri - ja. Tvorila sa mi vtedy tuším identita. Mala som trinásť. A chýbala mi zima a sneh. V tú zimu som začala lyžovať o niečo profesionálnejšie a dokonca som chodila na kopec  vlekom. Pluhovala som vcelku schopne. A ešte mesiac mi chýbal k hranici mnou vytýčenej dokonalosti. Žiaľ sneh sa roztopil, a začala smrdieť zem. Vtedy som videla prvého Oscara, prihlásila som sa na angličtinu a pindala som viac ako inokedy.       V tomto období som začala fajčiť. Prekryla som dymom pach zeme. A čvirikanie som zavrela pred dverami krčmy. Ak ho ešte bolo počuť cez okno, zišla som do „undergroundu". Boli koncerty, a akýsi návrat psychedélie. To ma odrezalo od akéhokoľvek romantického ponímania jari. Vzbúrila som sa proti pôstu a nostalgia údeného s vajcom a chrenom ma hnala von. Vytiahla som hnedé menčestráky a koženkovú bundu a sedávala na zemi, aj keď stoličiek bolo všade dosť. Ráno som sa roztrasená vracala k spiacim rodičom a tie vtáky mi išli roztrhať bubienky. Hrdličky a sucho v ústach.       Hneď o rok som sa prebudila z katarzie, práve vďaka vôni zeme a spevu hrdličiek. V knižnici som v slúchadlách počúvala muzikál,  HAIR. A háčkovala tašky a tielka. A nadychovala som spolu s chesterfieldkami aj kusy slobody. Veľa som vtedy prečítala a vypočula. Začala som žiť impresívne. S expresionistami. V modrom, žltom, pravdivejšie.       Vysokoškolská jar, bol spánok na trávniku v medickej. Slastné vyčerpanie. Rozpad individuality. Žila som pre Neho. Jediná vôňa ktorú som registrovala, bola tá Jeho.       Flešbeky predošlých jarí. A tehotenstvo. Ťažké nohy a dávenie s ranným zore. Zem znovu smrdela a vtáčky zneli smutne. Čakala som na chvíľu, kedy to skončí a obviňovala orgovány a repku olejku. Skončila som na desiatom poschodí Ružinovskej nemocnice. A to čo bolo predtým bolo stokrát lepšie. Jarná cibuľka na raňajky bola moja jediná konfrontácia s prírodou.               2010. bez klišé.                     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Denisa Hájková 
                                        
                                            Aké zložité je padnúť na dno a dôvod odraziť sa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Denisa Hájková 
                                        
                                            Obchodík ktorý voňal
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Denisa Hájková 
                                        
                                            Vedieť viesť ( a nečakané momenty )
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Denisa Hájková 
                                        
                                            byť veľmi blízko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Denisa Hájková 
                                        
                                            žiť teraz
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Denisa Hájková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Denisa Hájková
            
         
        denisahajkova.blog.sme.sk (rss)
         
                                     
     
        vo farbách. v obrazoch. v hudbe. poeticky. krehko.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    33
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    651
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            také dni
                        
                     
                                     
                        
                            Lucia
                        
                     
                                     
                        
                            	illogique
                        
                     
                                     
                        
                            pohľadnice odinakiaľ
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Časy, ktoré nenechám odísť
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




