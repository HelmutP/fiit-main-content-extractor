

 
* * * 
Karola Kuzmányho, ktorý pôsobil v Martine, tu pochovali 16. 8. 1866 ako prvého veľkého národovca. Pohreb sa stal doslova národnou manifestáciou, veď Martin a jeho okolie boli v tých časoch centrom slovenského národného života. Na mnohých náhrobkoch môžeme čítať nápis Вечная память. 
 
 
 
 
Od tých čias sa martinský cintorín stal nielen martinským, ale celonárodným. Sú tu pochovaní mnohí významní predstavitelia slovenského národného života. V článku nie je možné ukázať hroby všetkých, ktorí by si to zaslúžili. Niekto môže namietať, že v tom výbere nemá byť ten alebo onen a zas iný tam chýba. Je to vec názoru a kapacity článku.
 
 
 
 
* 

 
 
 
 
 
Lipová aleja pri vchode do cintorína 
 
 
Centrálny pomník s citátom z Hviezdoslava 
 
 
Hrobka rodiny Kuzmányovcov (Karol, 1806-1866, jeho manželka Karolína  
a dcéry Ľudmila Augusta a Flóra) 
 
 
Janko KRÁĽ, 1822-1876, básnik 
 
 
Ján KALINČIAK, 1822-1871, spisovateľ 
 
 
Samo CZAMBEL, 1856-1909, jazykovedec 
 
 
Štefan Marko DAXNER, 1822-1892, publicista, ideológ národného hnutia,  
autor Memoranda národa slovenského 
 
 
Ján FRANCISCI-RIMAVSKÝ, 1822-1905, politik, spisovateľ, redaktor 
 
 
Mikuláš Štefan FERIENČÍK, 1825-1881, novinár a spisovateľ 
 
 
Ján KADAVÝ, 1810-1883, osvetový pracovník, vydavateľ a hudobný skladateľ 
 
 
Pavol MUDROŇ, 1835-1914, predstaviteľ slovenského národného hnutia 
 
 
Andrej HALAŠA, 1852-1913, národný a kultúrny pracovník 
 
 

Svetozár HURBAN-VAJANSKÝ, 1847-1916, redaktor, spisovateľ, literárny kritik,  
ideológ a organizátor národného hnutia 
 
 
Martin KUKUČÍN, 1860-1928, spisovateľ 
 
 
Viliam PAULÍNY-TÓTH, 1826-1877, spisovateľ, novinár, politik, podpredseda MS 
 
 
Ambro PIETOR, 1843-1906, novinár, národný pracovník 
 
 
Elena MRÓTHY-ŠOLTÉSOVÁ, 1855-1939, spisovateľka 
a priekopníčka národného ženského hnutia 
 
 
Jozef ŠKULTÉTY, 1853-1948, literárny vedec, redaktor, publicista, jazykovedec,  
správca MS 
 
 
Andrej KMEŤ, 1841-1908, ľudovýchovný pracovník, zberateľ  
a organizátor vedeckého života 
 
 
Ján MELIČKO, 1846-1926, učiteľ a hudobný skladateľ 
 
 
Izabela TEXTORISOVÁ, 1866-1949, významná botanička 
 
 
Matúš DULA, 1846-1926, politik, predsedal zasadnutiu pri prijatí Martinskej deklarácie 
 
 
Jozef KOHÚT, 1838-1915, organizátor hasičského hnutia 
 
 
Miloslav SCHMIDT, 1881-1934, organizátor hasičského hnutia 
 
 
Jaroslav VLČEK, 1860-1930, literárny historik, univerzitný profesor, správca MS 

 
 
Literatúra: Zdenko Ďuriška: Národný cintorín v Martine, Pomníky a osobnosti, MS, 2007 
 
 
 
Nasleduje pokračovanie
 
 
 
 

