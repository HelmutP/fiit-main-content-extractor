
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Maroš Ovčarik
                                        &gt;
                Nezaradené
                     
                 Ako si požičať peniaze zadarmo? 

        
            
                                    17.5.2010
            o
            10:00
                        |
            Karma článku:
                11.48
            |
            Prečítané 
            5078-krát
                    
         
     
         
             

                 
                    Je možné si v dnešnej dobe požičať od banky peniaze a  nezaplatiť za to  ani cent ? Áno a nejde pritom o žiaden podvod. Riešením je jednoducho využívať nástroje, ktorá nám banky ponúkajú, pričom sa však paradoxne spoliehajú, že ich dostatočne nevyužijeme.
                 

                    Reč je o kreditných kartách.        Aký je rozdiel medzi kreditnou a debetnou kartou?       Debetnú kartu má v peňaženke asi každý z nás, kto má v banke bežný účet. Čerpať s debetnou kartou  môžeme len prostriedky, ktoré máme na tomto účte k dispozícii. To paradoxne znamená, že hoci sa karta volá debetná, jej hlavnou úlohou je umožniť nám míňať naše vlastné peniaze.   Pri kreditnej karte nám banka schváli úver (z anglického CREDIT=ÚVER) a čerpanie prebieha prostredníctvom výberov hotovosti z bankomatov alebo platbami kartou za nákupy. Pri kreditnej karte nemusíme mať v banke ani otvorený bežný účet.       Ako to, že nezaplatím banke ani cent?        Najväčšou výhodou kreditných kariet je možnosť využívať bezúročné obdobie. Je to opakujúce sa obdobie (zvyčajne okolo 45 dní), počas ktorého nám požičané peniaze banka neúročí. V praxi to znamená, že keď zaplatíme v priebehu mesiaca za nákup kreditnou kartou a na konci bezúročného obdoba požičané peniaze banke vrátime, banke nezaplatíme ani cent. Preto, ak chceme ušetriť, mali by sme si  každý mesiac  vo  výplate vyčleniť čiastku na splatenie požičanej sumy. Peniaze si môžeme následne každý mesiac opätovne požičať.       Pozor, lebo niektoré banky limitujú bezúročné obdobie len na platby kartou, nie na výbery hotovosti z bankomatu!       Je jasné, že banky to nerobia kvôli tomu, aby nám dali niečo zadarmo. Samozrejme, chcú na nás zarobiť. Spoliehajú sa, že ku koncu bezúročného obdobia neuhradíme požičanú sumu a potom si už veselo inkasujú úroky. Preto pozor, lebo úroky sa bežne pohybujú na úrovni okolo 18% p.a.  Takže pokiaľ by sme platili kartou mesačne napríklad 400 EUR a každý mesiac by sme banke uhradili len minimálnu povinnú splátku (t.j. zvyčajne 5% z dlžnej sumy), tak len na úrokoch zaplatíme banke za rok až 372 EUR (pri úroku 18%), čo je v starej mene viac ako 11210 Sk. Dodržaním pravidiel však banke nezaplatíme ani cent!       Pri kreditných kartách si banky zvyčajne účtujú poplatok za vydanie karty, resp. mesačný alebo ročný poplatok za kreditnú kartu. Na trhu sú však karty aj bez takýchto poplatkov.   Pri platbách za tovar alebo služby kreditnou kartou si banky poplatky neúčtujú. Za výbery hotovosti z bankomatov si účtujú zvyčajne až 2% z vyberanej sumy.   Bližšie informácie a porovnanie parametrov si môžete pozrieť na portáli www.FinancnaHitparada.sk.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (59)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Ovčarik 
                                        
                                            Pozor, termínovaný vklad nemusí vždy zarobiť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Ovčarik 
                                        
                                            Skutočná pomoc alebo len marketing?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Ovčarik 
                                        
                                            Nebojte sa zmeny, môže ísť o výraznú úsporu vo Vašej peňaženke!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Ovčarik 
                                        
                                            Porovnávajte, lebo rozdiely sú skutočne obrovské !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Ovčarik 
                                        
                                            Nedarujme bankám ani  cent navyše!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Maroš Ovčarik
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Maroš Ovčarik
            
         
        ovcarik.blog.sme.sk (rss)
         
                                     
     
        Spolutvorca portálu www.FinancnaHitparada.sk

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    7
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2028
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




