

 Vinianske jazero sa nachádza v katastri obce Vinné, 11 km od Michaloviec, 3 km severne od Zemplínskej šíravy. Leží v malebnom prírodnom prostredí, obkolesené hustými lesmi, napájané prameňmi Vihorlatských vrchov. Plocha prírodného jazera je pomerne malá a vyznačuje sa teplou vodou. 
  
  
  
  
 Jazero vzniklo vytvorením umelej hrádze a privedením vody z Vinianskeho potoka do sedla na úpätí vrchov Marečkovej (401m) a Šutovej (319m). Celková plocha jazera je 8 ha, priemerná hĺbka 3 m. Voda z horských potokov v lete láka na kúpanie a člnkovanie, v jeseni na rybolov, v zime na korčuľovanie, beh na lyžiach, turistiku a relaxačne pobyty.   
  
  
 Počas môjho detstva a mladosti stál uprostred jazera altánok na drevených koloch, pri ktorom sa radi pristavovali člnkujúci a plavci. Fotku altánku som bohužiaľ vo svojom archíve nenašiel. 
  
  
  
  
  
  
 Prírodná scenéria vytvára ideálne podmienky pre turistiku a cykloturistiku v priebehu celého roka. Okolité lesy poskytujú možnosť zbierania húb. 
  
  
  
 Rekreačné strediská umožňujú ubytovanie v chatovej osade a autokempingu, dá sa ubytovať aj v hoteli. Vybavenosť je na veľmi dobrej úrovni a nájdete tu občerstvovacie stánky a reštaurácie, požičovňu člnov a vodných bicyklov, tenisový kurt, požičovňu športových potrieb, ihrisko plážového volejbalu alebo detský areál. 
  
  
  
 Pred pár dňami som sa zo spravodajstva dozvedel, že do Vinianskeho jazera sa tohto roku sťahuje z okolitých lesov mimoriadne veľa žiab. Invázia žiab už skončila, jej dôkazom sú priemerne dve rozpučené a kolesami áut zlisované žaby na meter štvorcový po ceste okolo celého jazera.  Takýmto masakrom vás nechcem v tieto sviatočné dni obťažovať, radšej som vyfotil pár úspešne migrujúcich žiab. 
  
 Cestou domov som odfotil obec Vinné, na prvej fotke v pozadí s Vinianskym hradom, ktorý som prvýkrát navštívil tiež ešte v rannej mladosti s kamarátmi na bicykli. 
  
  
 Moja cesta domov viedla pozdĺž severných brehov Zemplínskej šíravy smerom na východ. Počas výstavby umelej vodnej nádrže (1961-1965) s pôvodným názvom „Podvihorlatská vodná nádrž" zatopil dolné konce obcí Kaluža, Klokočov a Kusín a s nimi aj starú cestu. Novú, rovnejšiu, už asfaltovú cestu, postavili severnejšie, popri brehoch „zemplínskeho mora", s rozlohou 33,5 kilometra štvorcového. Maximálnu návštevnosť dosiahla Zemplínska šírava na sklonku komunistického režimu, po otvorení hraníc zákonite poklesla. Organizáciu cestovného ruchu prevzali od okresného centra jednotlivé obce, no napriek ich snahe a vybudovaniu bazénov v strediskách Kaluža a Klokočov návštevnosť stagnuje. 
  
  
 Klokočov uprostred severných brehov Z. Šíravy je najvýznamnejším gréckokatolíckym pútnickým miestom na Zemplíne. Veriaci každoročne prichádzajú do chrámu Zosnutia Presvätej Bohorodičky k obrazu Panny Márie, ktorý slzil v roku 1670, keď ho zneuctili heretici. Pôvodná ikona bola nakreslená neznámym maliarom na dreve. Dnešná ikona Bohorodičky v Klokočove je druhou kópiou, korunovaná bola v roku 1948. 
  
  
  
 Poslednú zastávku výletu som si urobil pri obľúbenej lúčke s bleduľami jarnými. 
  
   

