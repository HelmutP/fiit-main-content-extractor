

 Kavu doma pripravujem vylucne v mojom piestovom kavovare. Je to v podstate nasa zalievana kava, ktorej vyluhovane pomlete zrnka sa piestikom stlacia na dno. 
  
 Kava je pre mna. Pomlety vyluhovany spodok je pre moju hortenziu 
  (susedia nevedia, ze kavou obcas dopujem aj azalky za domom). 
   
 Pred par rokmi som kupila malicku hortenziu, zasadila som ju a velmi som sa o nu nestarala. Bola taka nijaka, staroruzova, rastla pomaly, presadzala som ju z miesta na miesto, no proste nic moc. Potom som kdesi citala o tom, ze farba kvetov zavisi od kyslosti pody. Ta sa da zmenit a s nou sa moze zmenit aj farba hortenzii. 
 Tento rok sa mi moja "stara dievka" (tak volala hortenzie moja mama a dodnes neviem, preco) prvy raz za ranne krmenie kavou odvdacila. 
  
 Takto sa to cele zacina na jednej vetvicke... 
   
  
 ... a takto na vedlajsej.... 
   
  
 Ziva ci umela? Hodvabne lupienky s perlickami uprostred vyrastli v mojej zahrade... 
   
  
 Do ktorej umeleckej skoly chodi "pani priroda"? 
   
   
 Mama hortenzia so svojimi viacfarebnymi detmi. 
   
 V kaviarni recyklujeme vsetko, aj pouzitu kavu z espressa. Mame jej tolko, ze nou zasobujeme zahradkarov z celeho okolia a najma v lete jej nikdy nie je dost. Pri vyluhovani sa vacsina kyslosti z kavy odstrani, pouzity "odpad" ma pH 6.3 a pomer uhlika k dusiku je 20:1. 
 Pouzita vyluhovana kava je uzasna! Nielen ze je znovupouzitim prispievate k ochrane zivotneho prostredia, ale ak mate zahradu, dokaze v nej zazraky! 
 Pouzitie v zahrade je totiz  mnohonasobne: 
 - Obohacuje podu dusikom. 
 - Azalky, ruze, hortenzie a ine rastliny, ktore maju rady kyslu podu, budu rast ako "z kavy". 
 - Paradajky su obzvlast povdacne za salku kofeinoveho zlata. (Za obsah kofeinu v paradajkach vsak nerucim!). 
 - Vyluhovana kava je pre dazdovky to, co pre je nas zmrzlina. Vsetky dazdovky z okolia budu prekyprovat podu vo vasich hriadkach. 
 - Slimaky a mravce, naopak, ju neznasaju a takto jednoducho ich poslete k susedom alebo niekam inam. Staci, ak posypete problematicke uzemie vyluhovanymi kavovymi zvyskami. 
 - Vsetky macky zo susedstva, ktore pouzivaju vasu zahradku namiesto zachoda, pojdu k tym susedom, ktori neholduju kave. 
 - Ak si vyrabate vlastny kompost, pridanie pouzitej kavy urychluje cely proces. Cim viac, tym lepsie. 
   
 Ak pojdete okolo niektorej z kaviarni, skuste sa v nej zastavit a spytajte sa, co robia s vyluhovanymi kavovymi zvyskami. Mozno budu ochotni ich pre vas z casu na cas odlozit. Prospech bude vzajomny. Menej vyhadzovania (tazkych) smeti pre nich a krasna zahradka pre vas. Skuste, neobanujete! 
   
 Cim by bol svet bez kavy? 
   
   

