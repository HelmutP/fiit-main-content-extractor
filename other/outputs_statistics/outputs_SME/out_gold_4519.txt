

 Táto spomínaná osoba sa volala Margarette Steiff, a pre úplnosť informácie musím dodať, že sa nejednalo o plyšákov ako takých, ale skôr o vypchávané látkové zvieratká. Ja ich proste volám plyšáky, aj keď plyš by ste na nich nenašli ani s mikroskopom. Z nejakého neurčitého dôvodu som si vždy myslela, že plyš je špecifický druh tkaniny, ktorý vyrábajú chrobáci alebo motýle ako hodváb. Sklamane však musím dodať, že plyš je proste akákoľvek vlasová tkanina s vlasom dlhším ako tri milimetre. 
 V dnešnej dobe by ste asi len s ťažkosťou našli dieťa (teda aspoň v Európe, Japonsku, Južnej Kórei, Severnej Amerike a pár ďalších štátoch) ktoré vyrastalo bez jediného plyšáka. Ja sama som svojho najväčšieho dostala na dvadsiateprvé narodeniny. Keď sa ale kritickým okom pozrieme na všetky odvetia nášho života do ktorých zabŕda technika, by som sa skôr zamyslela nad tým či nám za pár rokov (alebo desaťročí) deti budú veriť že nie všetky plyšáky musia spievať, prijímať SMSky a mať Bluetooth. 
   
 "Hm-hm-hm macht Kinder froh und Erwachsene ebenso!" 

