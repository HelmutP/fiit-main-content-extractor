

 Starý vzorec bol takýto:  
 Vzorec na výpočet karmy článku: (čítanosť/400) + (počet hlasov/čítanosť x 100). Ak si napríklad článok prečíta 500 ľudí a 30 klikne na karmu, karma = 1,25 + 6 = 7,25. 
 ČO SA ZMENILO: Bol nastavený strop pre zvyšovanie karmy na základe čítanosti.  
 Odteraz ak má článok čítanosť do 4000, platí vzorec uvedený vyššie. Ak je čítanosť viac ako 4000, ďalšie body za čítanosť sa nepripočítavajú. Príklady: Ak si článok prečíta 4000 ľudí a 300 klikne na karmu, karma = 10 + 7,5 = 17,5. Ak si článok prečíta 5000 ľudí a 500 klikne na karmu, karma = 10 + 10 = 20. 
 Prečo zmena?  
 Vzorec karmy bol nezmenený od štartu blogov pred štyrmi rokmi. Čítanosť blogov sa však odvtedy výrazne zvyšovala a tak boli v rebríčku karmy už príliš zvýhodnené články, kde menej ľudí hlasovalo na karmu, ale mali obrovskú čítanosť. Vďaka novému vzorcu sa rebríčky karmy a čítanosti prestanú tak výrazne podobať a karma bude viac vyjadrovať názor čitateľov na kvalitu textu a menej to, ako je text čítaný. 

