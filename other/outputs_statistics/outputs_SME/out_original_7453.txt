
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 Zbabelec Fico a pozemok OLO 

        
            
                                    28.5.2010
            o
            8:55
                        (upravené
                28.5.2010
                o
                9:03)
                        |
            Karma článku:
                19.54
            |
            Prečítané 
            48895-krát
                    
         
     
         
             

                 
                    Robert objavil Ameriku. S veľkým tramtará vytiahol na mňa "kauzu" pozemok OLO, ktorá sa udiala v roku 2005, bola medializovaná v septembri 2006 a ku ktorej som vtedy zverejnil stanovisko a všetky príslušné podklady.
                 

                 O čo v krátkosti ide?   OLO malo nepotrebný pozemok, o ktorom predstavenstvo rozhodlo (nie na môj podnet!), že bude odpredaný. Pôvodne sa tu malo stavať recyklačné centrum, ale mestská časť Vajnory nedala k tomu súhlas a tak bolo recyklačné centrum postavené vedľa spaľovne vo Vlčom hrdle.   Bolo uverejnených sedem inzerátov (SME, Pravda, HN a Večerník) a celkovo sa zúčastnilo 18 firiem, medzi nimi aj FaxCopy a.s., v ktorej som mal vtedy 48 percentný podiel.   FaxCopy odovzdala najvyššiu ponuku a keby sa súťaže nezúčastnila, OLO a.s. by inkasovalo o 2 milióny korún menej. Toť vsjo. Koho zaujímajú detaily, nájde všetko na mojej stránke.   Jedna poznámka k Ficovému bludu, že "pozemok bol predaný pod cenu". To nebol ani náhodou, veď súťaž bola verejná (nie ako nástenkový tender, ale naozaj verejná). Zúčastnilo sa dosť firiem a ktokoľvek mohol ponúknuť akúkoľvek cenu a FaxCopy ponúkla najviac! Práveže dosiahnutá cena vyše 2600 Sk v roku 2005 bola reálna a trhová.   Koho zaujíma, čo som vlastne v OLO a.s. robil, môže sa pozrieť tu.   Čo to chcelo vlastne byť?   Fico zvolá mimoriadnu tlačovku kvôli päť rokov starému predaju pozemku a oduševnene rozpráva o niečom, čo bolo medializované v roku 2006 a čo sa odvtedy kompletne nachádza na mojej webstránke. Navyše, som pripravený zodpovedať akékoľvek otázky týkajúce sa predaja pozemku.   Mne z toho vyplýva, že smeráci musia byť hodne nervózni, keď kvôli mimoparlamentnej strane zvolajú mimoriadnu tlačovku. Zrejme im tečie do topánok. To aj chápem, lebo mať na krku nevysvetlených 284 miliónov by aj mňa robilo hodne nervóznym. Za to totiž hrozí niekoľko rokov basy. Preto je aj logické, že všetky strany, ktorým aspoň trochu záleží na ich povesti, sa od SMERu dištancujú. Ako prvá SaS, potom SDKÚ, po nej KDH a nakoniec MOST.   Celá vec je o to pikantnejšia, že bol to práve Fico, ktorý útočil na Dzurindu s oveľa menej závažnými obvineniami. Ako vieme, Dzurinda následne odstúpil z kandidátky. A keďže sa to celé stalo ešte pred odovzdaním kandidátnych listín (15. marec), je teraz líderkou Iveta Radičová. Keby sa Fico bol zdržal do 15. marca, mala by SDKÚ vážny problém, no takto jej práve naopak pomohol.   Je o mne známe, že Dzurindovi poviem, čo si myslím a že si myslím, že v politike je už pridlho. Ale Dzurinda môže právom nosiť titul najúspešnejší premiér Slovenska, zatiaľ čo Fico si zaslúži akurát tak titul najväčšieho babráka akého kedy Slovensko zažilo. Niet sa ani čo čudovať u človeka, ktorý nikdy nič poriadne v živote nerobil a vždy žil iba z cudzích peňazí. Preto Fico, ktorý čelí oveľa závažnejším podozreniam akým čelil Dzurinda, by mal z kandidátky  odstúpiť tiež. To sa samozrejme nestane, lebo by prišiel o imunitu a naozaj by mohol skončiť v base.   Potom, pán Fico, buďte aspoň natoľko chlap, že sa postavíte súperovi zoči voči. Keď sú ostrieľaní politici na Vás priveľa, skúste to aspoň so začiatočníkom ako som ja.   Preto Vás vyzývam, aby ste išli so mnou do televíznej diskusie. Vyberte si formát, miesto a čas. Poďme si na rovinu vydiskutovať, čo je transparentne kúpený pozemok od OLO a.s. oproti emisiám, najdrahším diaľniciam na svete, skorumpovaným Eurofondom a rekordnému zadlžovaniu.   Inak, obávam sa, budete pred celým národom za obyčajného zbabelca. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (539)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




