

 V súčasnosti máme na Slovensku už len jeden nebankový subjekt, ktorý verím, že v sobotu 12.06.2010 skrachuje. „Klienti“ tohto nebankového subjektu  menom SMER – SD sú podľa najnovších informácií veľmi spokojní. Prezrádza to dnešná nahrávka, údajne s hlasom  šéfa tohto nebankového subjektu. Je smutné, že výnosy im garantuje štát  v podobe netransparentných verejných obstarávaní, predražených tendrov a nezmyselne predražených PPP projektov.  
 Ste ochotní takéto niečo podporovať ďalej?  
 Ja osobne určite nie! 
 Klienti niekdajších nebankových subjektov, spýtajte sa dnes pána premiéra, či na Vás nezabudol a či je stále v jeho záujme odškodniť Vás.  Zabudol a už si ani nespomenie, ale na svojich „klientov“  určite nezabudne a bude to na úkor nás všetkých. Zastavme  takéto nezmyselné rabovanie nášho štátu  na úkor pár spokojných mecenášov SMERu, ktorí sa smejú do očí celému národu. Nedovoľme viac zneužívať verejné financie na súkromné záujmy ľudí blizkych tejto nebankovej inštitúcií. 
 Zastavme rozmach tak netransparentného a nedôveryhodného politicko-ekonomického subjektu na našom trhu, aby sme nemuseli len smutne pozerať, ako rabujú ďalšie štyri roky.  
 Zastavme to spoločne kontrolovaným krachom, už v sobotu 12.06.2010, v duchu bodky pána Lasicu za Ficovou vládou.  
   

