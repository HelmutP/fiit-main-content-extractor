
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Tutková
                                        &gt;
                Súkromné
                     
                 Do nového roka: Viac bezpečnej lásky! 

        
            
                                    1.1.2009
            o
            10:46
                        |
            Karma článku:
                11.19
            |
            Prečítané 
            3227-krát
                    
         
     
         
             

                 
                    Z niekoľko filmov, ktoré som v televízií zachytila počas týchto Vianoc, ma oslovil životný príbeh Ernesta Hemingwaya s názvom Láska a vojna. Viedol ma k úvaham o  nebezpečenstve voľného sexu pre lásku. Ponúkam ich tým, ktorí ho videli i nevideli v čase polnočnej omše, čo som tentokrát neabsolvovala pre chorobu.
                 

                  Ernie, mladý reportér hľadajúci na vlastnú päsť reportáže z vojnových zákopov, je zranený a hrozí mu amputácia nohy. Za týchto okolností spoznáva Agnes von Kurowsky, mladú ošetrovateľku Červeného kríža. Ich zaľúbenosť vo mne vyvoláva trochu rozpaky, lebo Ernie, aby ju zaujal, správa sa takmer arogantne, no napriek tomu jeho pozornosť Agnes lichotí. Priznám sa, že začiatok filmu mi ušiel a tiež som si zámerne nechala újsť milostnú scénu. Tá je však kľúčová v celom filme a viedla ma k hlbším úvahám.       Odohráva sa v bordeli, kde si Ernie rezervuje izbu pri návšteve Agnes, ktorú bola preložená na iný front. Nič iné nablízku nie je a tak jej ponúka len to. Najprv v kútiku duše dúfam, že to Agnes nepríjme, potom sa snažím jej zaslepenosť ospravedlniť faktom, že je vojna a vtedy ťažko dúfať v sobáš a všetko podľa poriadku... Hneď túto myšlienku oľutujem, keď sa Agnes objaví pred bordeľom, ktorý dýcha špecifickou atmosférou. Každému je jasné, ako je to nedôstojné, navyše aj Ernie sa za to Agnes ospravedlňuje. No Agnes, ktovie z akých pohnútok, oceňuje aj to málo...       Ernie sa vracia domov, kde žije na účet vlastného otca s ambíciou písať. Je mladší než Agnes, ktorá si uvedomuje jeho nezrelosť. Medzitým však ona padne do oka lekára, ktorý ju pozýva do svojho paláca v Benátkach, kam napokon po skončení vojny Agnes odchádza. V rozpoltenosti srdca urobí rozhodnutie pre zrelejšieho muža, ktorý jej ponúka spoluprácu na projekte novej nemocnice, manželstvo a úctu. Agnes je poctená a hoci chce byť racionálna, nevie sa ubrániť spomienkam, emóciám, putu, ktoré ju spája po jednej noci s Erniem. Erniemu už oznámila, že ich vzťahu je koniec a ona uvažuje nad manželstvom s iným. Ernieho ego to ťažko znáša, zúri.       Po čase sa Agnes vracia do Ameriky, nasledujúc svoje srdce. Navštívi Ernieho, ktorý svoje sklamanie nevie prehĺtnuť ani po tom, čo mu Agnes povie o svojej láske a uzná Benátky ako chybu. Toto je koniec filmu, ktorý dopĺňa text o osudoch hlavných hrdinov. Ešte dlho ostáva Agnes slobodná, kým Ernie sa do smrti štyrikrát ožení.       Čakala by som odpustenie a happyend, no preberám sa do tvrdej reality zákonov života. Prajem si, aby len milostná scéna nebola (nemyslím len zobrazená vo filme) a tuším, že si na konci to obaja „hrdinovia“ filmu rovnako želajú. Možno to však nie je len jedna udalosť v živote, ktorá veci môže pokaziť. Zvyčajne je to akumulácia menších rozhodnutí, činov, myšlienok, túžob, ktoré vyústia do toho, čo nazývame životným omylom. V každom okamihu, aj v tom, keď ide už o veľa – napr. pri sexe, ktorý zväzuje tak telo ako i dušu – má človek možnosť slobodne sa rozhodnúť, hoci jeho vôľa bude oslabená predchádzajúcimi voľbami. Ak zneužije svoju slobodnú vôľu, nerobí ho to viac slobodným, ale zotročeným.       A to je príbeh Agnes, ktorá po noci v bordeli, stráca slobodu. Jej srdce je spútané sexuálnym spojením s Erniem, hoci sa tomu snaží racionálne brániť. Jasná biochémia. Ľudské telá sú nadizajnované tak, že manželským spojením sa dvaja stávajú „závislí“ od svojich tiel, jeden od druhého. Tvár sexuálneho partnera sa vtláča do podvedomia človeka tak hlboko, že dlhý čas sa tieto rany hoja, ak dôjde k rozpadu vzťahu. Akokoľvek sa to bude prekrúcať na „bezpečný“ sex, ak sa použije kondóm, je to veľmi nebezpečné pre lásku. Vlastne v Amerike sa musel slogan zmeniť na „bezpečnejší sex“, lebo jeho zlyhanie štúdie uvádzajú medzi 14 a 30%. Nikdy tiež neochráni pred sexuálne prenosnými chorobami, ktoré sa prenášajú stykom kože a niektoré z nich sú neliečiteľné a smrteľné. Voľný sex je však hazardom pre duševné zdravie, lebo „láme srdcia“.       Predstavujem si, ako by sa vzťah Agnes a Ernieho vyvíjal, ak by vtedy odmietla. Určite by to prehĺtol a ak nie, tak lepšie, aby sa rozišli predtým a bez tejto jazvy. Je šokujúce, že dnes čokoľvek, pokiaľ si to ľudia odsúhlasia, je považované za dobré. Ale konsenzuálny sex tiež ubližuje, ak nerešpektuje dizajn ľudského tela. „Body language“ sexuálneho spojenia muža a ženy je „dávam sa ti navždy, úplne a exkluzívne“. Možno ponuka na to by prišla od Ernieho aj po odmietnutí noci v bordeli. Práve jej odmietnutím by o trochu dospel, a nie, ako si často mladí namýšľajú, že sexom (lebo to robia dospelí...). Agnes mala šancu ho naučiť čakať na lepšie, čo mohlo prísť. Ich lásku skazila žiadostivosť, ktorú mala Agnes možnosť u Ernieho zastaviť a premeniť na lásku. Jeho búrlivé ego by maličkými krokmi naučila zomierať a podstupovať obety z lásky k nej. To je podstatné pre lásku - ochota zomierať svojmu egu.       Ernie svoje ego už nevedel ovládnuť, keď prišlo životné zaváhanie zo strany Agnes. Cítil sa tak veľmi odmietnutý a zároveň podvedený, lebo sexom si vyjadrili čosi viac, než na čo boli pripravení, než čo tým skutočne mysleli. Ernie mal inú predstavu o význame sexu ako Agnes, ktorá ho nepovažovala za partnera navždy. Voľný sex vždy lásku pokazí. Nenahovárajme si, že len za istých okolností, keď sú ľudia nezrelí. Voľný sex je prejavom nezrelosti. Aj keď slobodne si ho človek zvolí, nerobí ho to slobodným, ale naopak. Najprv je zaslepene spútaný s druhou osobou, ale časom znecitlivie na toto biochemické superlepidlo - hormón oxytocín. Ako keď lepiaca páska, po opakovanom prelepovaní, už viac nelepí. Je to evidentné z nasledujúcich Ernieho vzťahov so ženami...       Sex mimo manželstva (alebo aj manželstva nie podľa nadizajnovaného „body language“), hoci so súhlasom, zraňuje oboch ľudí. Dáva chvíľkovú slasť intímnosti, dokáže však tak zlomiť srdce, že človek ostáva v zajatí zmätku niekedy aj do konca života. Ľudské srdce túži po láske a prijatí. Rozum však zastretý neskrotenými vášňami ukazuje človeku dobro práve v jeho opaku. A vôľa oslabená žiadostivosťou neodolá lákaniu... Aj v manželstve je treba kontrolovať pocity a pudy. Láska, ak má byť zodpovedná a slobodná, musí sledovať rozum. Ak rozum hľadá pravdu a je otvorený prijímať fakty, dokáže si podriadiť aj vášne. Srdce ho bude ľahko nasledovať. Veľkosť lásky sa meria trvácnosťou a obetou, nie intenzitou vášní.       Takú bezpečnú lásku prajem všetkým do nového roka 2009. Tá má však veľmi ďaleko od „bezpečného“ sexu.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (262)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Cirkev učila „darwinizmus“ dávno pred Darwinom!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Sme provokatívni – prinášame pohľad, ktorý je nepohodlný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Keď sa darovanie orgánov stáva vraždou...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Randenie nielen pre fundamentalistov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Tutková 
                                        
                                            Video: Aj Hitler nakupoval u Darwina
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Tutková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Tutková
            
         
        tutkova.blog.sme.sk (rss)
         
                                     
     
        žena slobodnej mysle, ktorá v srdci nosí túžbu pomôcť najchudobnejším z chudobných - nechceným embryám
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6106
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.cbreurope.sk
                                     
                                                                             
                                            www.abortionfacts.com
                                     
                                                                             
                                            www.familychoices.info
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




