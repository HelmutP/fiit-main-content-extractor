

 Tu sú niektoré z nich.. 
  
  
  
 Keď dostala moja stará mama artritídu, nemohla sa zohnúť a namaľovať si  nechty na nohách. A tak jej ich potom stále maľoval dedko, aj keď neskôr  tiež ochorel na artritídu. To je láska. (Rebeka, 8 rokov) 
 
  Keď váš niekto ľúbi, vyslovuje vaše meno iným spôsobom. Viete, že vaše  meno je v jeho ústach v bezpečí.(Biely, 4 roky) 
 
  Láska je, keď sa dievča navonia voňavkou a chlapec si dá na seba vodu po holení a potom idú spolu von a navzájom sa voňajú. (Karl, 5)  Láska je, keď sa ideš s niekým najesť a dáš mu väčšinu svojich hranoliek bez toho, aby si chcela tie jeho. (Chrissy, 6)  Láska je to, čo ta rozosmeje, keď si unavený.(Terri,4)  Láska je, keď moja mama urobí kávu pre ocina a trochu z nej odpije, než  mu ju dá, aby si bola istá, že chuti dobre. (Danny, 7) 
  
  Láska je to, CO je s vami v miestnosti počas Vianoc, keď prestanete otvárať darčeky a počúvate. (Robby, 5)   Keď sa chcete naučiť ľúbiť lepšie, začnite od niekoho, koho neznášate.  (Nikka, 6)  Sú dva druhy lásky : naša a božia. Ale Boh ľúbi obidvoma  spôsobmi.(Jenny, 4) 
   Láska je, keď povieš chalanovi, že máš rada jeho tričko, a potom ho nosí každý deň.(Noelle, 7) 
 
  Keď sa majú dvaja radi, zostanú spolu, aj keď sú z nich mala starenka a  malý deduško. Napriek tomu, že sa poznajú tak dobre. (Clare, 5)  Moja mamička ľúbi ocina, preto mu dáva najlepší kúsok z kurčaťa.(Elaine,5) 
 
  Láska je, keď mamina uvidí ocina spoteného smradľavého, a aj tak ho považuje za krajšieho ako Róberta Redforda. (Chris, 8)  Láska je, keď ti tvoje šteniatko olíže tvar, aj keď si ho nechala samé cely deň. (Mary Ann, 4)  Keď niekoho ľúbiš, tvoje mihalnice behajú hore a dole a z teba vychádzajú malé hviezdičky.(Karen, 7)  Láska je, keď mamina vidí Tatianu na záchode a nemysli si, že je to hnusne. (Mark, 6)  Ozaj by ste nemali hovoriť niekomu Ľúbim Ta, pokiaľ to nemyslite väzné, ale ak to tak myslite, radšej to hovorte často. Lúdia zabúdajú. (Jessica,8) 
 
 P.S.  V závere emailu bolo uvedené, že Láska môže byť aj keď... myslíš na svojich priateľov a zapratávaš im mailovu schránku takýmito milými mailikami ako sa to stalo aj mne.:-) 
  
 A čo je láska podľa Vás? 
 
   
 

