

 Pesnička od Elánu vznikla niekedy v osemdesiatych rokoch a podľa toho aj vyzerá - text je o tom, že je keď je vonku teplo a človek je smädný, má požiť nejakú tekutinu, ideálne vychladenú: 
 „Mám chuť na niečo chladené, od rána mám chuť. Smäd to je trápenie. Na niečo mám chuť, čo hneď ho zaženie. Že nemáte to nevravte mi, radšej nie." 
 Na prvý pohľad skladba vyzerá len ako ľahký letný hit, no napriek tomu je pod povrchom badať istý politicko-spoločenský apel a zúfalstvo, vyplývajúce z vtedajšej doby - text „Že nemáte to nevravte mi, radšej nie" jasne ilustruje nedokonalosť centrálne riadeného socialistického hospodárstva, a to nedostatok tovarov, a to aj tých najbežnejších, ako je napríklad malinovka alebo minerálka. 
 „Mám vineu rád. Po nej si dám, pre každý prípad ešte mušt. Nakoniec deň zalejem už len dákou tou minerálkou." 
 Uvedená časť textu predstavuje veľmi výnimočný jav v populárnej hudbe danej doby, a to spomenutie konkrétnej značky produktu. Zatiaľ čo v dnešnej dobe je product placement v hudbe pomerne bežný jav (viď dodatok), na danú dobu ide o inováciu a takpovediac o textovú nadprácu, keďže iný druh sladeného nealkoholického nápoja než vinea sa vyskytoval iba sporadicky (alebo išlo o no name produkt, ktorý sme v danej dobe volali podľa farieb, napríklad „prosím si tú červenú malinovku"). 
 Ale poďme ďalej: „Po nej si dám, pre každý prípad ešte mušt." Osobne si neviem predstaviť, aký prípad by musel nastať, aby som po vinee dostal ešte chuť na mušt, preto ide asi o veľmi výnimočnú situáciu, ktorú ale ďalší text nešpecifikuje. Navyše v dnešnej dobe je už hroznový mušt veľmi ťažko k zohnaniu, takže táto súvislosť sa zrejme stratila v priebehu histórie. 
 „Smäd ma na žiaden pád neprejde hneď. Môj pohľad stále blúdi tam, ku fľaškám, veď je v nich nápoj pravý na horúčavy dní, letných dní nádherných, keď sa viem sotva hnúť a šepnúť." 
 Ďalší text už spomína iba neustály autorov smäd a pocit tepla, ktoré môžu indikovať buď cukrovku alebo problémy so štítnou žľazou. Skôr by som sa priklonil k prvej možnosti, poukazujúc na poslednú strofu „keď sa viem sotva hnúť a šepnúť", ktorá poukazuje na nízku hladinu glykémie, alebo ešte horšie, na tzv. diabetickú kómu. Smutné je, že autor si svoje ochorenie zrejme vôbec neuvedomuje, keďže konzumuje vineu, ktorá je pre diabetikov absolútne nevhodná. Aj keď možno ide zámer autora, vytvoriť až existencionalistický paradox, spočívajúci v posolstve, že to, čo máme radi nás veľmi často zabíja. 
 Takže ako vidíte, aj v ľahkom letnom hite sa toho môže ukrývať viac, ako je na prvý pohľad vidno. 
   
 Dodatok: Nedávno som počul znovu tento, už takmer zabudnutý kus slovenskej hudobnej histórie, a prekvapilo ma, že ako vokál nepočujem uškrečaný pseudometalový záškrt Joža Ráža, ale akýsi bezpohlavný šansónový chrapot. Až neskôr som sa dozvedel, že tento rok spravila inak úplne zbytočnú coververziu tejto skladby speváčka (Mar) Tina (Csillághová). Bude to asi náhoda, porovnateľná s prítomnosťou policajnej hliadky ukrytej pri stopke zakrytej košatým kríkom, že sa tak stalo práve v čase, keď vineu začala vyrábať Kofola, a.s. a odvtedy je jej chuť zameniteľná s akýmkoľvek farebným tatrgelom v plastovej fľaši, na ktorej je obrázok hrozna, a preto hrozí, že sa bude predávať oveľa horšie ako kedykoľvek predtým. 
 






 

