
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Lisánsky
                                        &gt;
                Komentáre a úvahy
                     
                 České facky ľavici 

        
            
                                    30.5.2010
            o
            11:47
                        (upravené
                30.5.2010
                o
                12:20)
                        |
            Karma článku:
                10.27
            |
            Prečítané 
            531-krát
                    
         
     
         
             

                 
                    Včerajšie výsledky volieb do Poslaneckej snemovne Českej republiky boli pre zodpovedne zmýšľajúcich ľudí veľmi príjemným ,,prekvapením". Prekvapením v úvodzovkách preto, lebo Paroubkova konfrontačná kampaň, okrem agresie neponúkajúca žiadne rozumné riešenia bola asi najpravdepodobnejšou príčinou porážky českých socialistov vo voľbách, len nikto túto porážku nečakal tak skoro. Znamená teda včerajšok v Česku zmenu viac než na nasledujúce štyri roky ? Uvidíme.
                 

                 Začnime prvou časťou českého úspechu : Schwarzenbergovou a Kalouskovou stranou TOP09. Bývalý minister zahraničia a financií, obaja patriaci k tomu lepšiemu, čo sa v českej politike nachádza vytvorili stranu, ktorá dosiahla niečo cez 16 percent, vybila zuby komunistom a rozhodujúcou mierou sa podieľala na porážke socialistického tábora. Schwarzenbergovi sa tak splnil ,,tajný sen" poraziť KSČM. Pre Českú republiku je výborné, že konzervatívna strana, sľubujúca škrty a upozorňujúca na závratný štátny dlh najvýraznejšie zo strán politického spektra ČR vôbec, porazila nezdravé a nenásytné uvažovanie ľavice. Či ale českí občania škrty prijmú aj po ich zavedení, je však otázne. Štartovacia čiara je však už narysovaná.   Druhá časť : výmena lídra prospela najsilnejšej strane stredopravej oblasti, a to ODS. Po síce schopnom, ale nekontrolovateľnom Topolánkovi zavítal na čelo strany pravdepodobný budúci premiér Petr Nečas. Tento Moravák síce podľa komentátorov preferencie nepriniesol, ale zastavil ich prepad spôsobený vo veľkej miere práve Mirkom Topolánkom. Nečas, označovaný za konzervatívneho politika, bude tiež veľkým prísľubom rozpočtového a právneho ozdravenia Českej republiky, dobrého vývoja zahraničnej politiky a aktivity v EÚ. Nerád by som sa v tejto prognóze mýlil. Proti Nečasovi budú pôsobiť menej solventní voliči ČSSD, ktorých je v Česku najviac ( i keď to na úspech Paroubka nestačilo ) a taktiež ďaľší budúci koaličný partner Věci Veřejné, ktorý môže niektoré sporné rozpočtové opatrenia zabrzdiť, ale pomôcť naopak pri transparentnosti. Preto sa dá očakávať od Nečasa i mierna benevolencia k nižším zárobkovým skupinám občanov.   Na záver : ľavica. ČSSD síce voľby vyhrala, ale stredopraví voliči ukázali výbornú angažovanosť a iniciatívu, ktorá nakoniec bola sociálnej demokracii osudná. Paroubkov odchod je len logickým vyústením situácie, do ktorej vlastnú stranu priviedol on sám : urážlivá kampaň voči oponentom a médiám a arogantné vystupovanie na verejnosti zohrali obrovskú rolu. Bohuslav Sobotka, ktorý prijal funciu momentálneho lídra ale môže stratené hlasy pomaly začať získavať a osloviť nerozhodnutých v budúcich voľbách. Vyjadrenie komunistov, že prijmú akúkoľvek ponuku na zostavovanie koalície je už zaraditeľné do kategórie čierny humor.   Vývoj politickej situácie v Česku ponúka teda možnosť zostaviť disciplinovanú a racionálnu vládu stredopravých strán. Keďže na Slovensku sa o 2 týždne odohrá lotéria najvyššieho rangu, ostáva len dúfať, že sa Robertovi Ficovi, so všetkou cťou, stane niečo podobné ako Paroubkovi. Má k tomu v tejto chvíli bezvýhradne nakročené.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Monopol na diplomaciu končí. Začína sa bezpečnejší svet ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Siedma nemoc : čo napíšeme, to platí !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Byť konzervatívcom je úžasné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Od Atén k Bruselu, Slováci spievajú !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Lisánsky 
                                        
                                            Ľudia včerajška verzus tí ostatní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Lisánsky
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Lisánsky
            
         
        lisansky.blog.sme.sk (rss)
         
                                     
     
        Študent , ktorý píše o tom , čo ho baví a zaujíma ( a verí , že to baví a zaujíma aj iných ) .
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    820
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Komentáre a úvahy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




