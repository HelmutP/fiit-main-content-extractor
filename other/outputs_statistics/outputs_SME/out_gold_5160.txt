
 V apríli počujem niekoho hovoriť vezmem ťa do mesta, ktorým preteká Váh, budeme sedieť na jeho brehu, pozerať sa na vzdialenú druhú stranu, na stromy s čerstvými zelenými listami, na psov, ktorí plávajú do stredu rieky a potom na nich niekto zavolá, aby sa už vrátili. 
Na osobách nezáleží, dôležitý je súzvuk. 
Popri rieke pôjde starý pán tlačiaci bicykel, bude celkom blízko a nečakane povie dobrého zdravia vám prajem, práve vo chvíli, keď si všimnem, že zo zeme v neprirodzenom uhle trčí zopár veľkých čiernych pierok. Vtáčí hrob, úsmevy neznámych ľudí, pohľad na rieku a na jasnú modrú oblohu, myšlienky o slobode.  

((((foto))))

Cestou späť dvaja chlapci pod veľkými taškami sklonení nad albumom, okolo nich na zemi niekoľko malých bielych papierikov, ktoré tu ostali ako nepotrebná súčasť nálepiek. 
Spomeniem si na svoje albumy, zelený WWF a ružový Barbie. Bolo vzrušujúce otvárať nový balíček s nálepkami, netrpezlivo ich vybrať a preletieť očami, či je medzi nimi aj chýbajúce číslo 78. A tie, čo potom boli navyše, dvojmo, sme si cez prestávky vymieňali. 
V roku 1994 bolo dôležité niečo zbierať, učiť sa hrať na klavíri či akordeóne a škrobiť záclony.  

Dnes príliš často prechádzam schematickými priestormi, životmi. Možno som ich súčasťou a ukrývam sa vo vysokých vežiach. Potom odchádzam, vraciam sa, chodím hore a dole po schodoch. A všetko čo mám, je výhľad, strach z výšok a prievan.  

Stále vyššie a vyššie. Lietať, nerobiť kompromisy alebo objaviť iný svet, možno pravdu, lásku, niečo bájne, súzvuk.   
