

   
 Zo spoločného prehlásenia STOPu a NaStraš! 
   
 „ My národ slovenský, po tisícročia trpíme nadvládou maďarských turulov. Ukazuje sa, že spupný nepriateľ nakazil i vedúcich predstaviteľov oslobodeneckých myšlienok.  
   
 A preto: 
   
  Ľutujeme  výpadok Jána Slotu, spôsobený Momentálnou Vlasteneckou Indispozíciou (MVI) keď pre iredentistické zahraničné médiá (!) určil autorom našej posvätnej hymny Janka Kráľa a vypadol mu text. Ale, zobuďme sa bratia, nie je to len ďalší dôkaz hĺbky nákazy? Snáď nekvalitná maďarská pálenka alebo iný fígeľ zatemnil pamäť tohto predného politika. 
   
 Ľutujeme, že premiér Fico prosí nášho prezidenta aby korigoval jeho predošlý koaličný legislatívny návrh. Pán prezident, aspoň vy, tak ako mnohokrát v minulosti konajte národne! Vlastenecký zákon Jána Slotu schváliť musíte, do septembra to nemusíme vydržať. (pozn. NaStraš!: Ani gen. Paulus v štyridsiatom treťom pri Stalingrade nevydržal!) 
   
 A inak, opäť sa ukazuje, nepriateľ môže byť všade, väčšinou ale nehovorí spisovne po slovensky. 
   
  A preto: Ak nejaký Maďar na služobnej ceste urazí náš jazyk, nejaký Sólyom (pozn. NaStraš!: Jastrab -  čiže janičiar je to!), persona non grata z Budapešti, konajme okamžite. Nič nemôže ohroziť našu veľkolepú a inak za normálnych okolností neohroziteľnú štátnosť viac ako toto. Zabráňme iným Maďarom komoliť náš jazyk a po úspešnej blokáde mostu v Komárne, testujme Maďarov nielen na školách ale i na všetkých hraničných prechodoch!  
   
 Žiadame Štátnu školskú inšpekciu aby konečne prestala byť apolitickou inštitúciou a začala slúžiť tomu jedinému kto si túto obeť zaslúži. Nikomu len národu, a určite nie Maďarom! Navrhujeme posilniť túto dobovo dôležitú inštitúciu a zriadiť špeciálne Pohotovostné Oddiely Štátnej školskej inšpekcie  (POŠŠI), ktoré by hĺbkovo skontrolovali každú maďarskú školu pri akomkoľvek protislovenskom výroku od Nového Sadu po Dunajskú Stredu.  Jednotlivé POŠŠI by sa vždy regrutovali z oblastných združení STOPu a NaStraš! (pozn. NaStraš!: Na Belehrad!) a v prípade potreby by jazykoví inšpektori odborne pôsobili i na priľahlých hraničných prechodoch.  
   
 Veríme rovnako ako premiér Fico, že vlastenectvo nie je pre srandu. V minulých dňoch nám bolo rovnako ako jemu smutno ale naozaj veľmi smutno z reakcii médií na Vlastenecký zákon (pozn. NaStraš!: pán Fico, ale v septembri už naozaj? Je nám fakt smutno teraz, pozastavený zákon a ešte aj tí policajti čo chránili sionistov cez víkend v Bratislave...) Nič nie je v horšie, ako školská samospráva ponechaná bez politického dozoru, nič nedodá vlastencom z celého Slovenska viac odvahy ako preventívna prehliadka v sídle nepriateľa. Hovoriť v tomto zmysle o ideologických kritériách vnášaných politikmi do pedagogických inštitúcii môžu len zakomplexovaní pokrytci, čiže maďaróni. Slováci, rozmýšľate vôbec komu slúžia tí psychológovia, riaditelia škôl a rodičia ktorým už len toto môže vadiť?! 
   
   
 Rovnako ako minister Mikolaj Veríme že slovensko-maďarské vzťahy to len posilní. I študenti, v budúcnosti snáď slovenskí vlastenci, raz docenia význam týchto kontrol. Ak nedocenia STOP a NaStraš! sú pripravení dohliadnuť na dôstojné privítanie inšpektorov. 
   
 Veríme že premiér Fico sa zachová korektne a tentoraz si to s týmto ďalším nutným opatrením na ochranu vlasti nerozmyslí!  
   
 Ľutujeme nekontrolovanú, násilnú, brutálnu a snáď len obrovským nedorozumením spustenú čistku na iných ministerstvách SNS a poslednému ministrovi tejto strany verní zostávame... 
   
 ...pokiaľ Slovák na Slovensku pánom nebude!" 
   
   
 PS: Odkaz záujemcom o členstvo, či už z vlasteneckých alebo pragmatických dôvodov: Vzhľadom na príliš reštriktívnu legislatívu ako dedičstvo nezmyselne liberálnych 90. rokov spolky STOP a NaStraš! zostávajú rovnako ako napríklad Sitnianski rytieri v poloutajení a preto nový nábor členov prebieha vyslovene len na iniciatívu spolkov.  Priaznivcov ale ubezpečujeme, že nespíme, ba naopak! - naši spolupracovníci majú vplyv na dôležitých miestach naprieč politickým spektrom, i také paralyzovanie opozície pri Vlasteneckom zákone nech je ich večnou zásluhou. Plná a slávnostná aktivácia spolku za hudobným a vlajkovým doprovodom ako sa patrí cca každých 70 rokov. 
   
   

