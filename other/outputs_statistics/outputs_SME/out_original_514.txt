
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Urda
                                        &gt;
                Nezaradené
                     
                 Voľby 1992 - Úvod 

        
            
                                    3.6.2008
            o
            10:02
                        |
            Karma článku:
                11.29
            |
            Prečítané 
            5462-krát
                    
         
     
         
             

                 
                    Voľby v r. 1992 boli poslednými v spoločnej federatívnej republike. Po nich nastalo jej delenie. Čo im predchádzalo a ako dopadli: Kto boli v tých dobách hlavnými aktérmi? Hoci som si na začiatku blogovania povedal, že nebudem písať o politike, veď o tej je toho na blogu aj tak dosť, prvý raz porušujem toto moje predsavzatie. Porušujem ho preto, aby som práve tým neskoršie narodeným ukázal na pomery, ktoré vládli v tom období a tým skôr narodeným oživil tú dobu.
                 

                  V r. 1992 pri voľbách som pracoval aj ako anketár pre IVVM (Institut pro výzkum veřejného mínění) Praha. Niektoré materiály tej doby sú zaujímavé - programy strán a hnutí, ich poprední predstavitelia...     Teda pre oživenie pamäti pre tých skôr narodených a pre lepšiu orientáciu neskoršie narodených. Človek sa nemôže celkom odosobniť, môže sa o to len snažiť. Nevenujem tu pozornosť všetkým oblastiam, ktoré volebné programy obsahovali. Zameral som sa iba na oblasť štátoprávneho usporiadania, aby ste mohli porovnať volebné programy a povolebné kroky strán a hnutí.     *  *  *     A teraz fakty:       O priazeň slovenských voličov sa uchádzali v tabuľke uvedené politické subjekty. Niektoré sa uchádzali o priazeň voličov v celej federácii, iné len na Slovensku    Pred voľbami prebiehali rôzne prieskumy verejnej mienky, uvádzam výsledky dvoch z nich:    Názory občanov na ich vplyv na politické dianie      Názory obyvateľov ČR a SR v apríli 1991 a apríli 1992 na prípadné rozdelenie ČSFR    *  *  *     Strany a hnutia na masových zhromaždeniach sa predbiehali v predvolebných sľuboch a priazeň voličov sa snažili získať aj predvolebnými materiálmi. Mám ich mnoho, niektoré veľké plagáty sa mi nedajú zoskenovať, zopár menších tu uvádzam:       Plagátik i veľký plagát celoštátnej koalície ODS - DS na čele s Klausom, ktorá kandidovala v ČR i SR        Volebný program Demokratickej strany                      Volebný program Občianskej demokratickej strany              Práce Václava Klausa, ktoré na predvolebných mítingoch rozdávali a autor ich podpisoval      Dovtedy málo známy Kaník, budúci minister (podľa niektorých blogerov aj politik a štátnik)      Spevácky zbor Hnutia za demokratické Slovensko, ktoré vzniklo štiepením Verejnosti proti násiliu najprv ako platforma a v máji 1991 sa z nej vyčlenilo ako samostatný subjekt. Starší poznajú tváre mnohých známych spevákov      Vladimír Mečiar, predseda HZDS - fotografia s vlastnoručným podpisom      Roman Kováč, bývalý predseda Konfederácie odborových zväzov, budúci kandidát HZDS na prezidenta SR v prvom kole volieb, tiež s vlastnoručným podpisom       Občianska demokratická únia - Langoš, Čalfa, Demeš, Woleková, Porubjak, Mikloš, Hoffmann, Vavro          Volebný program Občianskej demokratickej únie      Plagátik Slovenského kresťansko demokratického hnutia s vlastnoručným podpisom predsedu Jána Klepáča          Alexander Dubček, niekdajší predseda Federálneho národného zhromaždenia          Ďalší predstavitelia Sociálnodemokratickej strany na Slovensku      Volebný program Kresťanskodemokratického hnutia s vlastnoručnými podpismi mnohých jeho predstaviteľov          Letáčik Slovenskej národnej strany s vlastnoručnými podpismi Mórica, Brňáka a Böhma, ktorý tam dopísal to známe "ZA BOHA A NÁROD"      Peter Weiss, predseda Strany demokratickej ľavice                  Volebný program Strany demokratickej ľavice        To bolo zopár predvolebných materiálov. Na zhromaždeniach v preplnených športových halách, natrieskaných veľkých zasadačkách, i pod holým nebom sa to len tak hemžilo ústrednými i regionálnymi funkcionármi strán a hnutí, vo veľkom sa rozdávali igelitky, plné propagačných materiálov - plagátov, letáčikov, samolepiek, fotografií, odznakov...     *  *  *     Najväčšiu predvolebnú kampaň mali koalícia ODS-DS, HZDS, SDĽ, ODÚ a KDH.  Ako zberateľ autogramov som si tiež prišiel na svoje, získal som ich množstvo.     *  *  *     A ako dopadli samotné voľby? Tu sú ich výsledky - rozdelenie kresiel:    *  Do SNR sa dostali: 7 - HZDS, 8 - SDĽ, 20 - MKDH, 25 -  KDH a 37 - SNS.                 Suverénnym víťazom na Slovensku sa stalo HZDS na čele s Vladimírom Mečiarom, úplne prepadli ODÚ a koalícia ODS-DS, ktoré sa nedostali do zastupiteľských zborov.    *  *  *    Tým prakticky začal rozpad federácie, ku ktorému nemalou mierou dopomohol aj prezident Václav Havel svojím necitlivým prístupom ku Slovákom a ktorý nedokázal zjednocovať, zato rozdeľovať áno.     *     Kandidáti jednotlivých strán a volebný program, týkajúci sa usporiadania štátu sú uvedené v súvisiacom článku. Je zaujímavé vidieť tie vtedy také známe tváre a čítať známe mená, z ktorých niektoré už zapadli prachom, iné sú na výslní stále, niektoré zas kdesi zazimované na dobre platených miestach správnych a dozorných rád... Niektorí stačili prejsť odvtedy niekoľkými stranami, dokonca aj predsedníckymi funkciami v nich, a aj ich dovedením k zániku...       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (58)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Z Detvy do Hriňovej...
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Znova z Handlovej do Prievidze
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Králická tiesňava a vodopád
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Zákopčianskymi kopanicami
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ján Urda 
                                        
                                            Hmly a spomienky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Urda
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Urda
            
         
        urda.blog.sme.sk (rss)
         
                        VIP
                             
     
         jednoducho dôchodca, jednou rukou na blogu, druhou nohou v hrobe so životnou zásadou podľa Diderota: 
 "Lepšie je opotrebovať sa, ako zhrdzavieť" 

                                    * * * 
Krásy Slovenska som propagoval i tu: 
http://fotki.yandex.ru/users/jan-urda/ stále živá stránka - http://fotki.yandex.ru/users/jujuju40/ 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    517
                
                
                    Celková karma
                    
                                                7.55
                    
                
                
                    Priemerná čítanosť
                    2324
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Detstvo - spomienky
                        
                     
                                     
                        
                            Škola - spomienky
                        
                     
                                     
                        
                            Práca - spomienky
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Z Turca
                        
                     
                                     
                        
                            Staroba - spomienky
                        
                     
                                     
                        
                            Filatelia - svet poznania
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Z Detvy do Hriňovej...
                     
                                                         
                       Nezamestnanosť - najzávažnejší problém na Slovensku
                     
                                                         
                       Môj 4% ný kamarát.
                     
                                                         
                       Straka
                     
                                                         
                       Zadarmo do Žiliny
                     
                                                         
                       Oneskorene k 17. Novembru ....
                     
                                                         
                       Komunálne voľby prerušené na 38 minút.
                     
                                                         
                       Inzerát
                     
                                                         
                       Koľko je reálne hodné euro?
                     
                                                         
                       Gruzínsko - cesta do kraja bez ciest
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




