

   
 Typický príklad: Martin Šimečka. Má stĺpček v týždenníku Respekt, čo je, mimochodom, veľmi dobrý časopis (Šimečka ho dokonca v minulosti viedol). Zoberme si len jeho predvolebný komentár. V prvom rade musel, samozrejme, spomenúť nahnevaný prejav Roberta Fica po zverejnení údajnej tajnej nahrávky, kde má Fico hovoriť o sponzorovaní strany. Fico bol k novinárom dosť agresívny. V Šimečkovom podaní vystúpenie vyznieva ešte tvrdšie. Kým Fico hovoril o tom, ako sa novinári na ňom „odbavujú", Šimečka použil „onanují". To predsa len vyznieva o dosť horšie (mohol použiť „ukájejí"). 
 Zvyšok článku je plný ničnehovoriacich fráz, Fico „byl zjevně zcela neplánovaně zasažen", podľa Šimečku „poslední dny kampaně se zvrhly v jednu obrovskou vlnu skandálů". A ďalej „na Slovensku se stává tradicí, že předvolební kampaň je plná skandálů". Floskula na floskulu. Ohromne informatívny článok pre českých čitateľov! 
 Ešte lepší reprezentatívny slovenský intelektuál je Fedor Gál. Ten minulý týždeň v rozhovore pre ČT24 veľmi emotívne rozoberal napríklad odozvu slovenskej vlády na maďarský zákon o občianstve pre zahraničných etnických Maďarov. Slovenská vláda novelizovala zákon o štátnom občianstve. Gál sa v českej televízii rozkričal, že on si nepraje znova stratiť občianstvo. Druhýkrát ho stratil, keď sa rozpadlo Československo a prvýkrát, keď bola jeho rodina odvlečená do koncentračného tábora... 
 Hm. Tak po prvé, zákon rieši nadobúdanie dvojitého občianstva, nie ľudí, ktorí už viac občianstiev majú. Po druhé, slovenský zákon je teraz identický s českým (zrejme bol odtiaľ priamo opísaný). 
 Najzábavnajšie na tom je, že takíto Šimečkovia a Gálovia si evidentne myslia, že oni sú na vyššej morálnej či intelektuálnej úrovni ako ich bubáci: Mečiar, Fico, atď. 
 Čerta starého... 
   

