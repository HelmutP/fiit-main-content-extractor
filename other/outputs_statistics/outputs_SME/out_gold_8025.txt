

 Keď to vyvrcholí vzdychni moje meno, 
 zubami, nechtami tú chvíľu chráň! 
   
 Ruku do vlasov vnor, pritlač o seba tvár! 
 Povoľ až vo chvíli keď smiem a mám! 
   
 Keď to vyvrcholí dovoľ nohám sa chvieť, 
 do slaných dotykov chuť rosy vpleť! 
   
 Zataj predo mnou dych, spomaľ nech zrýchliť vieš! 
 Mojimi pocitmi sfarbi si pleť! 

