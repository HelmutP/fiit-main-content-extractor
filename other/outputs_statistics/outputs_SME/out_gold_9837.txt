

 Vedenie KDH sa však rozhodlo inak. KDH bude z hľadiska politického vplyvu najslabším článkom štvorkoalície, v ktorej majú prevahu liberálne sily. KDH urobilo pri rokovaniach o vytvorení novej vlády najviac ústupkov. Dostalo sa nezávideniahodnej situácie... 
 Vedenie strany to zdôvodňuje potrebou zmeny. Ide vraj o hodnotovú orientáciu. Pochybujem - lebo účasť v (akejkoľvek) dvojkoalícii dáva politickej strane omnoho väčšie možnosti presadiť svoj program ako účasť v nevyrovnanom a nevyváženom účelovom zlepenci viacerých strán. 
 Nie je to prehra pravdy, ale môže to byť prehra tohto hnutia. Po nasledujúcich voľbách budú možno predstavitelia a priaznivci strany veľmi smutní. Nie, neželám Kresťansko-demokratickému hnutiu zánik, želám mu múdrejších a statočnejších vodcov. 
 (Poznámka: glosovaniu spoločenského diania v širších súvislostiach sa venujem na stránke www.priestornet.com) 

