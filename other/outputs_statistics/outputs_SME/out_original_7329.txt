
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Valerián Švec
                                        &gt;
                Úvahy
                     
                 Meditácia – začíname praktizovať 2.fáza 

        
            
                                    26.5.2010
            o
            12:34
                        (upravené
                26.5.2010
                o
                12:45)
                        |
            Karma článku:
                4.60
            |
            Prečítané 
            842-krát
                    
         
     
         
             

                 
                    2. Fáza prichádza, keď je telo pokojné a neruší nás nepohodou. Máme pred sebou novú úlohu. Upokojiť myseľ. Objavuje sa nám predstava, ktorá nás desí alebo znervózňuje? Spomeňme si na cvičenie z praxe v úvode.
                 

                   a. Je tá myšlienka skutočnosťou alebo si len my sami premietame na plátno mysle vlastnú konštrukciu? Prečo sa chceme desiť? Prečo sa chceme znervózňovať? Čo nám táto vtieravá konštrukcia našepkáva onás? O našich túžbach, ambíciách, hnevoch ktoré sa snažíme zatajiť sami pred sebou?   b. Je to spomienka? Ako sa dá zmeniť minulosť? Buďme rovnako zhovievaví k iným ako k sebe. Odpúšťajme rovnako sebe ako iným. Snažme sa pochopiť vlastné konanie rovnako ako konanie ostatných. Prečo sa to NAOZAJ stalo? Čo bolo OZAJSTNOU príčinou? ODPUSTIME! POCHOPME! POUČME SA! Rozhodnime sa napraviť, čo sa dá. Premietnime si tú nápravu. Do každého detailu, aj keď je nerealizovateľná, lebo účastníci tej spomienky sú mŕtvi alebo vzdialení. Premietnime si ten moment nápravy, či odpustenia. Smrť je len ilúzia a vzdialenosť neexistuje. Stále sme všetci v spojení a my týmto spôsobom SKUTOČNE napravíme a SKUTOČNE odpustíme.    c. Je to predpoklad? Kto je pánom toho, čo sa stane, ak nie my sami? Prečo sa takto sebatrýznivo zabávame na vlastnom utrpení? Zlo nejestvuje a my sme schopní čokoľvek vidieť a tvoriť vláske, s láskou a šťastím. Pozrime sa na to teda takto. Do každého detailu. Ako chceme zrealizovať niečo, čo si nevieme predstaviť? Ako majú byť ľudia okolo nás dobrí, keď my sami si ich premietame opačne? Ako nám majú byť okolnosti priaznivé, keď my sami si ich kazíme svojimi konštrukciami? My sme tí architekti, čo budujú vlastnú budúcnosť!     Desivé - rušivé predstavy nás budú stále pokúšať, ale my predsa sedíme pred plátnom a sledujeme len to, čo si sami vyberieme. Vyberajme si teda pozorne! Asi najúžasnejším obťažovateľom je Ego. S Egom sa musíme stretnúť a upokojiť ho, že ho nechceme zničiť. Musíme ho len informovať, že podstata je architekt. Ego je nástroj prejavu v tomto svete. Nie je to sama podstata. Keď sa dostávame k vhľadom, samozrejme je napádané a konfrontované s tým, že nejestvuje. Bráni sa teda. Kričí! Dokazuje opodstatnenosť svojej existencie.   Keď sa napáda ego nepraktizujúceho, tak sa rozčuľuje, háda, všetko a všetci sú proti nemu. Všetci od neho niečo chcú!!! Stále od neho niekto niečo požaduje, pýta.   U praktizujúceho sa Ego vie prejavovať tajnejšie, ako vojenská rozviedka. Podhadzuje tisíce iných dôvodov na negatívne emócie, len nie vlastný strach. V takomto prípade, keď zavládne zmätok v mysli a cítime, že neprichádza uvoľnenie ani po meditácii, buďme ostražitejší. Vnímajme okolie pozornejšie a našu spätnú väzbu naň. Je to ako diagnostika, keď nám lekár hmatá brucho. Pýta sa:   „Bolí?"   Hmatá potiaľ, až sa ozve:   „Aú"!   Tak a máme to!   Tu platí, že diagnostikovať síce môže lekár, ale liečiť si to musíme sami. Lekárom je každá bytosť, čo nám spôsobí rušivú emóciu. Viac sami seba vnímajme, čo nám ten ktorý človek alebo prejav spôsobuje a nedajme sa mýliť, že to robia zle druhí svojimi energiami. Skúsme sa pitvať len a len v sebe. Áno, aj iní vedia skomplikovať situáciu. Negatívne energie sa však potrebujú dačoho zachytiť a to, o čo sa zachytávajú v našej mysli je to, čo treba riešiť. Ak nás niekto posudzuje a nám to spôsobuje emóciu, je to naše EGO. Nehovorím, že posudzovanie nesmieme odmietnuť. Hovorím o objavení sa rušivej emócie. Akejkoľvek. Dobrej aj zlej. Neodmietajme Ego. V prípade, ak sa jedná o príjemnú emóciu, pochváľme si ho.   „Dobrá práca, kamarát, to sme zvládli."   Ak sa jedná o negatívnu emóciu ako hnev, pýtajme sa ho:   „Čím sme si to privolali? Za čo sa hanbíme? Čo skrývame? Po čom túžime? Čo nás priťahuje, že cítime taký strach?"   Hnev, ak to totiž ešte nevieme, je len ziapajúci strach.   Aplikovaním nových vedomostí v praktickom živote a pozornosťou k okoliu odhalíme postupne všetky dôvody našich problémov. Rušivé predstavy ustúpia samé, aspoň budeme mať taký pocit, pretože budeme odstraňovať mylný pohľad na okolie a tým aj dôvody našich problémov. Uvedomujeme si konkrétnu pravdu alebo poznanie. Riešenia nám prichádzajú ako pošepkané. Naučme sa ich poslúchať. Naučme sa konať podľa nich. Zrazu si všímame, že nám do cesty prichádzajú presne takí ľudia, akých sme potrebovali. Posúvajú nás ďalej. Už sa tomu nečudujeme, lebo si uvedomujeme, že sa to všetko deje zámerne. My sme tí, čo to organizujú. Strácame potrebu kontaktovať ľudí alebo navštevovať miesta, ktoré nám nemajú čo odovzdať. Zbavujeme sa zlozvykov akosi samozrejme. Ide o naozajstné rozhodnutie, ktoré nie je mučením sa nejakou disciplínou. Ide o poznanie, že sú veci, ktoré k životu jednoducho nepotrebujeme. Prestanú nám chutiť.   PREBERÁME ZODPOVEDNOSŤ ZA VLASTNÝ ŽIVOT! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Švec 
                                        
                                            Príručka otrokára - Starostlivosť o mláďatá - Základné zásady vzdelávacieho systému
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Švec 
                                        
                                            Príručka otrokára - Starostlivosť o mláďatá –dohľad nad pôrodom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Švec 
                                        
                                            Príručka otrokára
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Švec 
                                        
                                            Utópia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Švec 
                                        
                                            Som otrok
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Valerián Švec
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Valerián Švec
            
         
        valeriansvec.blog.sme.sk (rss)
         
                                     
     
         Som jednoduché stvorenie, čo na tento svet prišlo na chvíľu porobiť, čo treba a zasa sa vráti späť ... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    960
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Nevyžiadaná pomoc sa nevracia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Príručka otrokára - Starostlivosť o mláďatá - Základné zásady vzdelávacieho systému
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Verný sám sebe
                     
                                                         
                       Príručka otrokára - Starostlivosť o mláďatá –dohľad nad pôrodom
                     
                                                         
                       Príručka otrokára
                     
                                                         
                       Utópia
                     
                                                         
                       Som otrok
                     
                                                         
                       Dôkaz
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




