

 Potom som akosi dospela.  
 Zrazu som pochopita vela veci. Zacala som si vsimat veci a vnimat fakty ktore sitce vzdy okolo mna boli, ale ja som ich akosi nevnimala a nechapala. Zacala som si vsimat ludi. Ako sa menia. To bolo pre mna najhorsie. Ludia okolo mna sa zacali menit neskutocnou rychlostou. Z deti sa zacali stavat postupne dospelejsi ludia a ja som nevedela pochopit preco sa vsetko tak meni. Takto: vedela som pochopit, ale nechcela si to pripustit. Drviva vacsina ludi okolo mna sitce dospeli fyzicky, ale mam pocit, ze za vrstvou „dospelackeho“ make up-u a „slecinkovskym“ oblecenim sa skyrva psychicky nevyvinuty a krehky clovek. (Priklad make-up a slecinkovske oblecenie je sitce o zenach, tak isto ide aj muzske pohlavie..) Ludia okolo mna zacali nosit masky a „tvarit sa“. Nevedela som to prekusnut, ale ze je to sucastou zivota som pochopila, az ked som dospela. 
   
 Potom prislo obdobie kedy mi akosi prestala „ist karta“. 
 Vztahy s niektorymi blizkymi sa zacali narusat. Z detskych „najlepsich kamaratstiev“ toho vela neostalo. Ukazal sa pravy charakter ludi a ja som nechapala. Preco zrazu tolka falos a pretvarka?! Neskor som pochopila, ze vtedy som este len nebola zvyknuta.. 
 Ani veci v ktorych som predtym tak vynikala nesli ako po masle. Rozhodnutia ktore som robila asi neboli najspravnejsie, ale vtedy mi to nikto nepovedal. A tak som sa viezla.. 
 Prislo sklamanie za sklamanim. Preplakane vecery spocitat neviem, bolo ich strasne vela. Konecne som pochopila co to znamena trapit sa v plnom rozsahu. Boli, a su to facky, ktore som potrebovala a este stale potrebujem k tomu aby som rastla ako osobnost. Aby som si viac vazila veci, ktore su podstatne a dolezite. Aby som sa naucila robit spravne rozhodnutie a spravne sa rozhodovat, kedy ich robit srdcom a kedy rozumom. Aby som sa naucila zit.  
   
 Ako pomerne mlade a este stale realite nie uplne odolne dievca obcas podlahnem samolutovaciemu syndromu. Predvcerom to na mna prislo. Lahla som si ako posledna z rodiny do postele, prikrila sa az po usi perinou, pustila si pomale pesnicku a premyslala som. 
 Zrazu to na mna vsetko prislo. Prechadzala som vsetkymi „odvetviami“ svojho zivota a vsade som videla len to, co MA tak sklamalo a kde som SA tak strasne sklamala.  
 V zivote je potrebne pozerat sa viac na to dobre. Vzdy hladat to dobre. Inak nemame sancu.. Niekedy vsak uz niet sil hladat viac to dobre. Tak to bolo predvcerom. Videla som len vsetko to zle, videla som ako sa mi tolko veci ruca pred ocami, videla som ako sa mi nedari.. 
 Prislo to na mna a ja som sa tomu nebranila. Rozplakala som sa jak male dieta.. 
 Potrebovala som to zo seba dostat von. Slzam som nebranila. Nechala som ich volne stekat po licach a citila som ako sa mi na nich tvoria dva rozvetvene potociky. Bola to samolutost. Nehanbim sa za to.  
 Potrebovala som to niekomu povedat. Potrebovala som sa zdoverit a nemala som komu. Ludia ktorym by to mozno aj slo boli strasne daleko a noc bola uz pokrocila. Zacala som premyslat komu na svete by som to vobec mohla povedat, kto by ma za moju ubohu samolutost neodsudil. Nevedela som.. 
 Chcela som zobudit maminku, ale nechcela som jej pridavat dalsie starosti k tym, ktorych uz beztak ma dost. V zufalstve a s stipkou nadeje som sa cez telefon prihlasila na facebook.com a nenapadne sa snazila nacrtnut svoju situaciu. Vela ludi ten status like-lo. Aj tak nevedeli o co ide, ale myslienka sa im pacila. 
 Jeden jediny clovek sa ozval. A hoc som sa zdoverila len okrajovo, zisila som, akeho (*dalsieho) velkeho cloveka mam pri sebe. DAKUJEM!  
   
 (* slovo „dalsi“ neznamena sedemnasty, osemnasty.. ale treti, mozno stvrty.) 
  
  

