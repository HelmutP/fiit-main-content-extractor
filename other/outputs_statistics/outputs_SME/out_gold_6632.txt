

   
 Azda najväčším meradlom športového úspechu je záujem fanúšikov. Majstrovstvá sveta v ľadovom hokeji sú neodmysliteľnou tradíciou spätou práve s týmto športom. Hokejový sviatok, na ktorom sa rok čo rok stretáva šestnástka najlepších hokejových krajín bojujúcich o titul najlepšieho reprezentačného mužstva na svete. 
   
 Do povedomia slovenských fanúšikov sa MS v hokeji dostali predovšetkým vďaka našim najväčším úspechom, zakončených medailovými umiestneniami v rokoch 2000, 2002 a 2003. 
   
 Najlepšia generácia našich hráčov dosiahla až na hokejový trón a navždy sa zapísala nielen do histórie, ale aj do sŕdc a povedomia každého Slováka.  
   
 Po týchto mimoriadnych úspechoch sme ale čakali dlho. Slovensko znovu žilo hokejom, no dôvodov na radosť nebolo ani zďaleka veľa. Začiatkom roka sa však na vancouverskej zimnej olympiáde stretla snáď najsilnejšia zostava, akú mohlo Slovensko na vrcholové podujatie poskladať a málo chýbalo k tomu, aby sme znovu prežívali tie neopísateľné pocity radosťi, čo pri predošlých úspechoch.  
   
   
   
    
   
 Pred týmito majstrovstvami sveta konajúcimi sa v Nemecku bolo celkom jasné, že náš tím bude musieť prejsť generačnou výmenou. Úspešného trénera Jána Filca nahradil na našej lavičke kanadský odborník Glen Hanlon, no a jemu sa podarilo počas krátkej prípravy poskladať partiu mladých hráčov a tých, ktorí nikdy v reprezentácii nedostali dostatok priestoru. 
    
   
 Tomuto výberu nikto neveril. Nominácia sa mnohým „akože-fanúšikom“ zdala prislabá. Premládnutý výber sa však nenechal zahanbiť a všetkých prekvapil už v prvom stretnutí, proti hviezdami nabitému výberu Ruska. Hoci sme nakoniec prehrali 1:3, je to prehra za ktorú sa ani jeden člen slovenského mužstva nemusí hanbiť.  
 V ďalších zápasoch naši ukázali nezlomnú vôľu a otočili stretnutie s Bieloruskom, či pomerne hladko zdolali Kazachstan po výsledku 5:1. 
   
 V prvom zápase osemfinálovej skupiny však naši narazili na veľké hokejové prekvapenie z Dánska. Dáni v predchádzajúcich zápasoch šokovali Fínov aj USA, no a hoci boli na papieri proti nám jednoznačným outsiderom, zápas sa vyvinul celkom inak. 
 Po prvej tretine na svetelnej tabuli svietil hrozivý stav 0:6. Šok, chladná sprcha, tvrdá facka pre nás, ktorej by nikto predtým neveril. Odrazu bolo sebavedomie našich hráčov, ale dovolím si tvrdiť že hlavne fanúšikov, zrazené poriadne k zemi.  
   
   
 Priznám sa, že patrím k tým, ktorí tomuto výberu moc neverili. Však nemôžeme vypadnúť, je jedno či prehráme všetko, hlavné je, že priestor dostali mladí, ktorí sa potrebujú rozohrať. Napokon sa však ukázalo, že aj tento zdanlivo neznámy tím dokáže zahrať vyrovnané zápasy na najvyššej úrovni. A keďže sme my národ slovenský natoľko zvláštny, hneď potom ako naši začali predvádzať dobré výkony, sebavedomie bolo vystrelené do nadpozemských výšin a každý začal mať hubu plnú rečí. A akonáhle naši dostali na hubu od toho „Legolandu čo nevie čo je to hokej“ tak sa začalo na nich odvšadiaľ kydať.  
   
 „Doprdele, to snáď neni možné.“ 
   
 Citujem slová nášho kapitána Richarda Lintnera, ktoré vyslovil po skončení prvej, hrôzostrašnej tretiny súboja s Dánskom do televíznych kamier. Tieto slová sa potom akousi záhadnou cenzúrou na internet už nedostali, namiesto toho internetové diskusie zaplavili ešte horšie bludy presne od tých ľudí, ktorí len deň predtým vynášali celú repre s Glenom Hanlonom na čele k nebesiam. Vedia tí ľudia čo vôbec chcú? Potom to doprajem radšej aj Dánom, ktorí sa narozdiel od našich hokejom vedeli baviť aj keď prehrávali. Doprdele s nimi, alebo s nami? Toto fakt nie je možné. 
   
   
 Jedna prehra, v ktorej sme podali oduševnený výkon. Jedno skutočne pekné víťazstvo, potom jeden zápas s dobrým koncom, avšak poriadne odfláknutý. A teraz prehra, akú sme ešte nezažili. 
   
 Taká je bilancia našich na tohtoročných majstrovstvách. Stále nás však čakajú dve stretnutia, najprv silní Fíni a potom domáci Nemci, hnaní vpred svojimi vernými fanúšikmi.  
 V podstate je jedno, či tieto dva zápasy prehráme. Záleží na tom, akým spôsobom ich prehráme. Alebo naši znovu prekvapia, znovu budú pre každého najlepší a potom dostaneme na hubu? Doprajem tomuto tímu len to najlepšie. Nech idú pekne krôčik po krôčku za svojím snom, o ktorom sa im snáď ani len nesnívalo. 
 Nepochybujem o tom, že na to majú. No ak sa im predsa nepodarí splniť to, nič sa nedeje. Stále sa len vo veľkom hokejovom svete obzerajú, stále len hľadajú svoju cestičku. A niekedy stačí skutočne málo, len kúsok srdiečka a nezlomnej vôle. 
   
 Trikrát sme to nečakali, trikrát sa to podarilo. Tak prečo nie teraz? 
   
   
   
 Áno, znovu sa budem opakovať. Stačí iba málo – nájsť správnu dávku optimizmu, ale nestrácať pritom triezvy úsudok. Len je veľká škoda, že my Slováci to zatiaľ nevieme.  
   
   

