
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kocúr
                                        &gt;
                Spoločnosť
                     
                 Keď arcibiskup hovorí aj keď mlčí... 

        
            
                                    12.5.2010
            o
            15:45
                        (upravené
                12.5.2010
                o
                19:03)
                        |
            Karma článku:
                14.46
            |
            Prečítané 
            3690-krát
                    
         
     
         
             

                 
                    "Keď vidím, že pán arcibiskup si bol pokecať s takým človekom, ako je Slota, tak je mi zle. Hovorím to na plné ústa. To mám akože teraz chápať? To akože rozvody zrazu nie sú zlé, keď si pán arcibiskup pokecal s človekom, ktorý je už dvakrát rozvedený, ktorý je vulgárny, ktorý nadáva ľuďom a rozosieva nenávisť? O emisiách a kradnutí už ani hovoriť nebudem. A pán arcibiskup ešte ani nechceli nič povedať."
                 

                 
Ako vystúpiť do neba, keď sa tak držíme zeme...?archiv autora
   Hneď ako som si prečítal môj dych zastavujúcu reportáž o stretnutí SNS a KBS prebehol som očami diskusiu pod ňou na webe. Okrem expresívnych vyjadrení bolo medzi diskutujúcimi veľa rozčarovaných a zdesených ľudí. Niektorí diskutéri sa vyznačovali až naivnou dobromyseľnosťou. Mediálne hodnotím to, ako s kapitálom tohto stretnutia Ján Slota naložil, ako jeho absolútny úspech a geniálne PR Slovenskej národnej strany a jej tzv. kresťanskej orientácie. Pozrime sa však aspoň krátko na to, čo ja považujem za kľúčové stupne v pomerne reprezentatívnom spektre pohľadov na toto stretnutie.    Podľa jedného z diskutérov k správe o stretnutí zástupcov KBS a SNS je úlohou cirkvi byť morálnou autoritou bez ohľadu na momentálnu nepohodu či prípadné nepríjemnosti. Píše: „Cirkevní predstavitelia by sa mali hlasnejšie vyjadrovať k istým spoločenským problémom, aj za cenu útokov. Takisto by mali verejne prezentovať svoje názory a požiadavky, minimálne pred veriacimi. Tak môže ostať cirkev morálnou autoritou so spoločenskou váhou. Skôr či neskôr bude vystavená rôznym vplyvom, ktoré sú nezlučiteľné s učením cirkvi, vývoj v Európe je jasný."    Dobromyseľný diskutér, snažiaci sa o široký horizont a toleranciu voči tomu ako sa Ján Slota krátko po stretnutí so zástupcami KBS odprezentoval píše: „Pokiaľ nemáme vyjadrenie z druhej strany .... Je jasné že Slota to prifarbuje tak aby sa mu to hodilo. Keby ho Zvolenský pol hodinu drbal za to ako žije, tak aj tak by po odchode Slota novinárom vykladal ako si veľmi rozumeli a čo všetko majú spoločné..."    Kritickejší diskutéri sa dostávajú do roviny apelu na cirkevných predstaviteľov: „Pán biskup a vám nevadí: že SNS zneužíva na plagáte v predvolebnom boji náboženský symbol ruženec???? Nevadilo by vám keby Jano Slota sa nechával fotiť s hostiou na jazyku?" Prípadne sa pozerajú na stretnutie v kontexte vnútrocirkevného diania a posledných škandálov na slovenskej či svetovej cirkevnej scéne a lakonicky komentujú: „Stretnutie KBS a SNS to je skutočne ten najlepší najlepší spôsob, akým si cirkev očistí meno po všetkých škandáloch, ktoré sa na ňu valia. Rovný rovného si hľadá." Vrana k vrane..., takto nazval svoj príspevok budúci volič strany Most-Híd a ďalej pokračuje:  „ Treba oprášiť to staré a osvedčené. Za boha a za národ. Potom už môžu kľudne nasledovať aj heslá typu, Maďari za Dunaj, Čecha do mecha ....., Žida do .... Spolu s bojom za mier proti zvrátenému imperializmu, všetkým triednym nepriateľom...."    Posledný z príspevkov, ktorý som si dovolil vybrať do reprezentatívnej mozaiky textu o tom, čo si diskutéri myslia o tomto stretnutí zástupcov KBS s Jánom Slotom veľmi kultivovane sumarizuje: „Toto nie je možné. Som kresťanom, chodím do kostola a som čestným človekom. A keď vidím, že pán arcibiskup si bol pokecať s takým človekom, ako je Slota, tak je mi zle. Hovorím to na plné ústa. To mám akože teraz chápať? To akože rozvody zrazu nie sú zlé, keď si pán arcibiskup pokecal s človekom, ktorý je už dvakrát rozvedený, ktorý je vulgárny, ktorý nadáva ľuďom a rozosieva nenávisť? O emisiách a kradnutí už ani hovoriť nebudem. A pán arcibiskup ešte ani nechceli nič povedať. Dúfam, že ma riadne pokarhajú na budúce, keď pôjdem na spoveď a poviem, že som hrešil. Keď oni verejne jednajú s takými vulgárnymi ľuďmi, ako je táto banda, tak to je v poriadku".           Podľa agentúrnej správy SNS súhlasí s katolíckou cirkvou v tom, že odsudzuje manželstvá homosexuálov, výchovu detí v takýchto zväzkoch, jednoznačne podporuje prijatie ústavného zákona, ktorý by chránil rodinu ako niečo, čo je prioritné. Obidve strany sa podľa Jána Slotu zhodli na potrebe rovnocenného financovania štátnych, súkromných a cirkevných škôl a aj v názoroch na otázky zdravotníctva a sociálnych služieb.    Protimaďarské, protisvätoštefanské či protirómske apely Jána Slotu v poslednom období ostali neokomentované. Vrátane „čudného" bilbordu. Znovu je potrebné pripomenúť, že podľa klasickej zásady katolíckej učebnicovej morálky platí, že kto mlčí pri pohľade na mravné zlo, ten s mravným zlom súhlasí. Qui tacet, consentire videtur...     V tejto súvislosti je zaujímavé vnímať účasť arcibiskupa Zvolenského na zhromaždení k výročiu sviečkovej manifestácie. Ján Slota verejne niekoľkokrát Františka Mikloška - jej hlavného iniciátora verejne ponížil a urážal práve kvôli jeho kresťanskej angažovanosti v období husákovského komunizmu v Česko-Slovensku. ...že či si naozaj myslí, že komunizmus padol vďaka nemu. Ján Slota v čase normalizácie raboval v susednom Rakúsku a v slovenských obchodíkoch s mäsom a údeninami. Po nežnej revolúcii koristí už len na Slovensku, no ktovie. Primitivizmus slotovských prospechárov dostal týmto stretnutím ďalšie požehnanie.    Ostáva už len veriť, že občania Slovenska, ktorí sú veriaci a postoje katolíckej hierarchie sú pre nich dôležité či mienkotvorné, budú mať viac zdravého úsudku a občianskej cti ako občan Zvolenský. A to som si myslel, že hlbšie sa už po „rómskom" bilborde v predvolebnej kampani zostúpiť ani nedá. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (585)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Františkova cirkev s ľudskou tvárou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Nežiť aj žiť v demokracii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pocit bezpečia vystriedali kultúrne vojny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Vydať sa na zaprášené chodníky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pápež František, rodová rovnosť a idea machizmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kocúr
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kocúr
            
         
        kocur.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.aomega.sk


  
 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Today
                        
                     
                                     
                        
                            WEEK
                        
                     
                                     
                        
                            Theology Today
                        
                     
                                     
                        
                            MK Weekly
                        
                     
                                     
                        
                            Scriptures
                        
                     
                                     
                        
                            Listáreň
                        
                     
                                     
                        
                            Nechceme sa prizerať
                        
                     
                                     
                        
                            Oral History
                        
                     
                                     
                        
                            Zo školských lavíc
                        
                     
                                     
                        
                            Epigramiáda
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Last_FM
                                     
                                                                             
                                            BBC_All Things ...
                                     
                                                                             
                                            Radio_FM
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.aomega.sk
                                     
                                                                             
                                            The Tablet
                                     
                                                                             
                                            www.bbc.co.uk
                                     
                                                                             
                                            www.bilgym.sk
                                     
                                                                             
                                            www.vatican.va
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




