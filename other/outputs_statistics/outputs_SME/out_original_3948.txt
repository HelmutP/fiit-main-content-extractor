
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Jankes
                                        &gt;
                Fototúlačky
                     
                 Sochy 

        
            
                                    4.4.2010
            o
            21:52
                        |
            Karma článku:
                7.99
            |
            Prečítané 
            1526-krát
                    
         
     
         
             

                 
                    Kedysi som chcel byť sochárom. Nemal som k tomu ďaleko. Otec bol celý život kamenárom. Keď som mal asi 15 rokov, občas som si požičal jeho náradie a na dvore som sekal do pieskovca. Bez prípravy, bez zmenšeného modelu. Zistil som, čo to je za drina a tiež, že to niekedy môže stáť človeka život. Veľa otcových spolupracovníkov, ktorí robili v pieskovcových lomoch, zomrelo na silikózu.
                 

                 Keď som bol zamestnaný pred viac ako dvadsiatimi rokmi v keramičke, podarilo sa mi cez prestávky v robote vymodelovať niekoľko menších sošiek z keramickej hliny. Ešte ich mám vysušené na polici v knižnici. Teraz počas sviatkov prišli domov na návštevu deti.  Syn z Brna, dcéra z Belgicka. So synom sme na chvíľu skočili na Vlkolínec. Krásne - takmer ideálne počasie na fotenie. No nenabitá baterka vo fotoaparáte mi dovolila vyfotiť iba niekoľko obrázkov.         Pred dedinou, zapísanou do kultúrneho dedičstva UNESCO, stojí niekoľko drevených sôch. Keď sme tam boli naposledy, boli čerstvo vyrezané. Teraz po niekoľkých rokoch chytili peknú patinu.      Tú istú chybu s nenabitou baterkou už viackrát nezopakujem. Dnes prišla dcéra na niekoľko dní z Belgicka. V Bratislave sme mali pred jej príletom viac ako dve hodiny času, tak sme si zašli do Čunova do Danubiany.                        Zaujímavé miesto, inšpiratívne sochy sochárov zo Slovenska i zo zahraničia. Pripomínalo mi to trochu Stedelijk muzem v Amsterdame. Vľkorysé priestory vnútri, venované aktuálnym výstavam Miroslava Cipára a Antoni Clavé. Premietacia miestnosť s filmom, kníhkupectvo s knihami o umení. Pekné. Až sa mi nechce veriť, že som na Slovensku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Neverím, že sa tento rok preveziem po diaľnici medzi Dubnou Skalou a Turanmi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Štyri hodiny v Tatrách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Čaká sa tu dlho?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Bociany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Jankes 
                                        
                                            Možno posledný sneh
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Jankes
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Jankes
            
         
        jankes.blog.sme.sk (rss)
         
                        VIP
                             
     
        Raz na to možno prídem.


  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    315
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1632
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovník cudzích slov
                        
                     
                                     
                        
                            Rozhovory
                        
                     
                                     
                        
                            Moje knihy
                        
                     
                                     
                        
                            postrehy
                        
                     
                                     
                        
                            Autozážitky
                        
                     
                                     
                        
                            cudzie perie
                        
                     
                                     
                        
                            Fototúlačky
                        
                     
                                     
                        
                            Kuchárska kniha pre úplných za
                        
                     
                                     
                        
                            mamonár
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            80 strán
                        
                     
                                     
                        
                            S kreslenými vtipmi
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ivan Štrpka: Rukojemník
                                     
                                                                             
                                            Pavel Hirax Baričák: 666 anjelov
                                     
                                                                             
                                            Ivan Jančár - Lee Karpiscak: Koloman Sokol 100 nepublikovaných d
                                     
                                                                             
                                            Inspire - magazine with a difference
                                     
                                                                             
                                            Zberateľ alebo zmluva s diablom
                                     
                                                                             
                                            Milan Kundera: Směšné lásky
                                     
                                                                             
                                            Egon Bondy: Mníšek, Vylitý nočník
                                     
                                                                             
                                            Rudolf Sloboda: Krv
                                     
                                                                             
                                            Rasťo Piško: Bohémska kolonáda
                                     
                                                                             
                                            Barry Miles: Charles Bukowski
                                     
                                                                             
                                            Dušan Mitana: Koniec hry - prvé vydanie
                                     
                                                                             
                                            Egon Bondy: Filosofické eseje sv.4
                                     
                                                                             
                                            Ivan Kadlečík: Škoda knižke nerozpredanej ležať
                                     
                                                                             
                                            Rudolf Sloboda: Rozum
                                     
                                                                             
                                            Rudolf Sloboda: Z denníkov
                                     
                                                                             
                                            Pierre Restany: Hundertwasser
                                     
                                                                             
                                            Liv Ulmann: Choices
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            London grammar
                                     
                                                                             
                                            Katarzia
                                     
                                                                             
                                            Korben Dallas
                                     
                                                                             
                                            Aminata Kamissoko
                                     
                                                                             
                                            Herbie Hancock Feat Corinne Bailey Rae - River
                                     
                                                                             
                                            Iva Bittova + Vladimír Václavek - Zvon
                                     
                                                                             
                                            Adele - Someone Like You (on 'Later Live with Jools Holland') -
                                     
                                                                             
                                            Grzegorz Karnas feat. Tomasz Szukalski
                                     
                                                                             
                                            Scott Dunbar - Sober
                                     
                                                                             
                                            Offret - Sacrificatio (1986) Andrei Tarkovskij / Part 1 / Eng. S
                                     
                                                                             
                                            Bach - Julia Hamari - Matthäus Passion - Erbarme dich
                                     
                                                                             
                                            Vladimír Václavek - Sen
                                     
                                                                             
                                            Lightnin' Hopkins - Goin' Back Home
                                     
                                                                             
                                            AG flek - Carpe diem
                                     
                                                                             
                                            Fred McDowell: Keep Your Lamp
                                     
                                                                             
                                            Seban Rozsa Buntaj - Come Together
                                     
                                                                             
                                            Scott Dunbar, ArtsWells Festival, Part 3
                                     
                                                                             
                                            Lenka Dusilová - Za vodou
                                     
                                                                             
                                            Stand By Me | Playing For Change | Song Around the World
                                     
                                                                             
                                            KRZAK - Blues dla nieobecnych
                                     
                                                                             
                                            Scott Dunbar One Man Band
                                     
                                                                             
                                            Bittová, Godár - Lullabies
                                     
                                                                             
                                            Vlasta Třšešňák - Atlant
                                     
                                                                             
                                            Sting - Shape Of My Heart
                                     
                                                                             
                                            Knopfler &amp; Clapton - Same old blues
                                     
                                                                             
                                            Tore Down - Chris Shepherd Band
                                     
                                                                             
                                            Iva Bittova - Uspavanka
                                     
                                                                             
                                            Chuck Beattie Band &amp; Kenny Dore
                                     
                                                                             
                                            Blind Boys of Alabama - Satisfied Mind
                                     
                                                                             
                                            'God Moves On The Water' BLIND WILLIE JOHNSON
                                     
                                                                             
                                            Eric Clapton Crosroads
                                     
                                                                             
                                            Suni Paz Performs Tierra Querida- Bandera Mia/Smithsonian Folkwa
                                     
                                                                             
                                            Guy Forsyth Band - Taxi
                                     
                                                                             
                                            Drum and Bass (Impressive street performer)
                                     
                                                                             
                                            Rihanna feat. Justin Timberlake rehab
                                     
                                                                             
                                            Justin Timberlake Vs Eminem
                                     
                                                                             
                                            Rupa &amp; the April Fishes "Une americaine a Paris"
                                     
                                                                             
                                            Audition (3)
                                     
                                                                             
                                            Fleret: Zafukané
                                     
                                                                             
                                            ČECHOMOR - Karel Holas - Rehradice
                                     
                                                                             
                                            Michal Prokop - Snad nám naše děti prominou..
                                     
                                                                             
                                            Michal Prokop - Kolej Yesterday
                                     
                                                                             
                                            Vladimír Mišík Variace na renesanční téma ( Večernice)
                                     
                                                                             
                                            Vladimir Mišík - Stříhali dohola malého chlapečka
                                     
                                                                             
                                            Knocking on Heaven's Door by Bob Dylan
                                     
                                                                             
                                            Prežijem
                                     
                                                                             
                                            Rachmaninov mal veľké ruky
                                     
                                                                             
                                            Kapela ze Wsi Warszawa - Live!
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Dášena
                                     
                                                                             
                                            zina veličková
                                     
                                                                             
                                            Anna Strachan
                                     
                                                                             
                                            Videoblog: Tanec je všetko
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Ivan Kadlečík
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Malá Fatra zo Strečna.
                     
                                                         
                       Stehno veľryby
                     
                                                         
                       Robert a jeho úžasný život v bubline
                     
                                                         
                       Konečne letím, alebo z Viedne do Quita (Ekvádor - 1. časť)
                     
                                                         
                       Suť
                     
                                                         
                       V úzkosti zo smrti
                     
                                                         
                       Päť užitočných otázok o Modrom z neba
                     
                                                         
                       Delová guľa Ruda Slobodu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




