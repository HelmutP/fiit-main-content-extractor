
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Inocent Szaniszló
                                        &gt;
                Nezaradené
                     
                 Nežijeme nad pomery? 

        
            
                                    18.5.2010
            o
            16:24
                        (upravené
                18.5.2010
                o
                17:35)
                        |
            Karma článku:
                12.09
            |
            Prečítané 
            2528-krát
                    
         
     
         
             

                 
                    Čo mňa najviac pri cestách domov zaráža je na jednej strane luxus, ktorý nám istá vrstva slovenskej spoločnosti ukazuje pred očami a na druhej strane možnosti, ktoré naša ekonomika ponúka. Dnes ukázali rakušania v denníku Kurier zadĺženosť krajín Európy, kde Slovensko dokonca malo dlhy o jednu desatinú čiarku vyššie ako Česko. Existuje jeden nemecký výraz "Nachhaltigkeit", ktorý nie je jednoduché preložiť do slovenčiny. Ide o istú schopnosť ľudí zriekať sa veci, ktoré nie sú nevyhnutné a takto pomôcť ako životnému prostrediu, tak aj vlastnej vízií a cieľom života človeka na tejto planéte.
                 

                 
  
  Nedávno sme sedeli s viacerými zahraničnými slovákmi (áno väčšina z nich má dvojité občianstvo...) a rozmýšľali nad fenoménom, ktorý nás často zarazí pri príchode do vlastnej domovienky. Zvlášť tí naši spoluobčania, ktorí jednoznačne viac zarábajú a majú oveľa kvalitnejší systém sociálnej starostlivosti ako nám ponúka naša vlasť, akosi nevedeli pochopiť, prečo u nás jazdí toľko vyslovene drahých a luxusných áut a prečo našincom záleži natoľko mať všetko tzv. značkové, čo len je možné si zaopatriť (pozrime sa len na ŠPZ-ky áut, ktoré chodia nakupovať do Parndorfu- 80% je z východnej Európy). Nuž chodiac po mnohých západných metropolách môžem povedať, že človeku udrie do očí ak práve v domácom malom meste vídí na vlastné oči prvýkrát isté luxusné značky automobilov zaparkované často vo väčšom počte dokonca na jednej ulici, alebo snaha mať oblečené len veci so správnym logom a naháňať sa za vecami, ktoré mnohí ľudia západu vôbec nepovažujú za potrebné. Samozrejme, že to vôbec nie je fenomém len Slovenska a slovákov. Znalci konzumnej spoločnosti hovoria, že treba len vydržať, kým sa nasýtia isté vysnívané potreby. Na druhej strane ma prekvapujú mnohí "západniari", ktorí sa zriekajú dokonca aj nákupu auta, aby sami takto prispeli k lepšiemu a čistejšiemu životu v ich spoločnosti. Tu im zase ja rád pripomínam, že je dobré nevlastniť auto v krajine, kde jednotlivé mestá a dediny sú prepojené verejnou dopravou v takom takte, ktorý ani v našich veľkých mestách neexistuje. Kto by sa však nechcel mať dobre a nevlastniť najlepšie veci, aspoň podľa jeho vlastnej predstavy? Keď som pracoval v Ženeve, často ma prišli čakať autom jednoduchí emigranti v pre mňa pekných autách (napr. typu Golfa). Aké však bolo moje prekvapenie, keď niektorí ani tak neboli spokojní a snívali: "ako by to bolo fajn voziť sa v amerických búrakoch, to by sme sedeli ako vo foteli"! Takže ono vlastne to bude všade nejako komplikované... A predsa budeme musieť pomaly myslieť na brzdu konzumizmu. Teda na život podľa skutočných pomerov aj našej ekonomiky. O tom je aj táto ekonomická kríza. Len to môžeme vydať, čo sme naozaj zarobili. A sociálna spoločnosť bude možná až vtedy, ak ľudia budú chcieť a môcť viac myslieť na spoločný rast spoločnosti ako na vlastné nároky

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (44)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Zbytočná Európska Únia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Základné právo na náboženskú slobodu a nezávislosť od štátu (IV.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Príčina náboženskej slobody v modernej dobe III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Príčina náboženskej slobody v modernej dobe II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Pápež a kondómy?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Inocent Szaniszló
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Inocent Szaniszló
            
         
        szaniszlo.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolský učiteľ, večný hľadač pravdy, dialógu a vzájomnej výmeny ideí. Spolu s mladšími a staršími kolegami a študentmi rozvíjame obzory nášho poznania./
L´enseignant academique, le chercheur eternel de la vérité, du dialogue et du changement des idées. Avec ses collegues et étudiants nous élargisons l´horisont de notre conaisance.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    849
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politická filozofia
                        
                     
                                     
                        
                            Verejný život
                        
                     
                                     
                        
                            Hľadanie koreňov
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Klaus Berger: Die Wahrheit ist Partei
                                     
                                                                             
                                            Notker Wolf-Matthias Drobinski: Regeln zum Leben
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Laura Passini
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Primitívne nedeľné politické debaty na RTVS a Markíze
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




