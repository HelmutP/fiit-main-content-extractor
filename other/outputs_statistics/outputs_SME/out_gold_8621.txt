

 Pred minuloročnými prezidentskými voľbami v diskusii Zlatice Puškárovej s dvoma hlavnými kandidátmi Markíza nevhodne nastrihala vyjadrenia Orbána a Radičovej tak, že diváci si mohli vytvoriť falošný dojem o tesnom súzvuku týchto dvoch politikov. Ani včerajšia markízácka televízna debata sa nevyhla priamemu poškodeniu kandidáta, tento raz premiéra Fica. Na začiatku časti o ekonomike došlo k tejto výmene: 
 Puškárová: Pán Fico, je pravda, že Slovensko zaznamenáva najvyšší ekonomický rast v únii. Rovnako je aj pravda, že nám najrýchlejšie rastie dlh... Fico: Predovšetkým opäť musím opraviť jeden z údajov, ktorý ste uviedli... Puškárová: A čo sa Vám nezdalo v top klipe? Fico: ..Nie je pravda, že rast dlhu u nás bol najrýchlejší v EÚ... Puškárová: Aby sme boli úplne korektní, my máme aj v tabuľke tento dlh aj rast dlhu...Dynamika rastu je podstatná a tam sme najrýchlejší.. Fico: Neklamte pani Puškárová.  Puškárová: Nie, neklamem, však to tu mám aj podporené grafikou. Fico: Poviem presné čísla. Verejný dlh v rámci EÚ stúpol o 20 percentuálnych bodov zo 60 na 80 percent, teda  20 percent bol priemerný rast dlhu. Na Slovensku to ani zďaleka tak nebolo, sme nižšie ako tých 20 percent, podstatne nižšie... Nezavádzajte, prosím..  Puškárová: Nie, nezavádzam, v žiadnom prípade, to by som si nedovolila, dôkladne som sa pripravovala, máte to tu aj v tabuľke... Fico: Ale ste to urobili, bohužiaľ. 
 Čísla Eurostatu vrátane predpovedí Európskej komisie (tabuľka 42, str. 202) pre tento rok potvrdzujú tvrdenia predsedu vlády, nie moderátorky. Priemerné zadlženie v EÚ stúpne zo 62% HDP v roku 2008 (59% v 2007) na očakávaných 80% na konci tohto roka. Nárast pre Slovensko je z 28% na 41% HDP, teda o 13 percent HDP oproti 18im pre EÚ. Áno, je to veľký nárast vzhľadom na to, že Slovensko nemuselo sanovať banky ako viaceré západoeurópske štáty či úplne neskolabovala ekonomika ako v pobaltských krajinách, a stále je to najviac z V4 krajín. Ale nie je to ani náhodou najrýchlejšie zadlžovanie v EÚ (sme až 15. najrýchlejší), a navyše je ten rast dlhu nižší ako je EÚ priemer. Podobne by to bolo, ak by sa porovnávali len roky 2009 a 10. 
  
 Je síce pravda, že veľké PPP dlhy či rôzne pochybné pôžičky štátnym podnikom sa nám nerátajú do zadlženia podľa eurostatovej definície, a že skutočný rast dlhov je teda u nás vyšší (viď dnešné SME). Ale samozrejme, to platí aj pre iné krajiny, a takéto porovnanie všetkých skrytých dlhov nemáme k dispozícii. No aj pri započítaní PPP dlhov len pre Slovensko (to by spravilo celkový nárast zhruba o 23 percent HDP za obdobie 2008-10) by sme stále boli ďaleko od titulu najzadlžujúcej sa krajiny – Írska (+33).   Mimochodom, to platí aj ak nárast nerátame percentami v HDP, ale v absolútnych číslach. Navyše Puškárová Ficovi krivdila aj vtedy, keď v obrane okrem zlých čísel používala aj nelogický argument. Odvolávanie sa na zobrazený graf vývoja slovenského dlhu predsa nijako nemohlo rozriešiť Ficovu námietku, že sme v porovnaní s inými krajinami EÚ na tom dobre - veď na to treba práve ukázať vývoj aj v iných štátoch. 
 Update 12.6: Zlatica Puškárová namieta (pozri celú reakciu): 
 Kým ja som hovorila o „ dynamike rastu dlhu“, premiér R. Fico hovoril o absolútnom náraste dlhu v porovnaní s priemerom EÚ.  Nárast priemerného dlhu v EÚ zo 62% ( r. 2008) na 80% HDP ( prognóza EK na rok 2010) znamená, že dlh za pozorované obdobie narástol zhruba o takmer tretinu. Nárast nášho dlhu z 27,6% ( r.2008) na vyše 41% HDP ( prognóza EK na rok 2010) znamená, že náš dlh za pozorované obdobie narástol takmer o polovicu. Z toho teda jasne vyplýva, že dynamika rastu nášho dlhu je oveľa vyššia ako dynamika rastu dlhu v únii.  Považujem preto za podstatné povedať, že som nielenže nenafúkla údaje, ale na druhej strane ani nespochybňovala údaje premiéra R. Fica.  
 Nesúhlasím. V prvom rade, aj keby sme zobrali takto meraný rast zadlženia, moderátorka v diskusii ani raz nepovedala, že sme nad priemerom EÚ, ale opakovane len to, že sme v celej únii "najrýchlejší." Lenže to zďaleka nie sme, ako ukazuje tabuľka - aj v tomto indikátore dynamiky rastu dlhu by sme boli až 10. najrýchlejší s obrovským odstupom za prvými: 
 
 



krajina
pomer dlhu k HDP v 2010 vs v 2008
dynamika v %


Litva
39/16
144


Lotyššsko
48/20
140


Rumunsko
30/13
131


Estónsko
10/5
100


Slovinsko
42/23
83


Írsko
77/44
75


Španielsko
65/40
63


UK
79/52
52


Fínsko
51/34
50


Slovensko
41/28
46



Zdroj: Eurostat, Eur.komisia (pri prvých troch krajinách som pôvodne chybne uviedol index 244, 240 a 231)  
 A po druhé, pre rast zadlženia to nie je veľmi zaujímavý ani "podstatný" indikátor, určite menej výpovedný ako ten Ficov. Pre ktorú krajinu je rast zadlžovania horším problémom, pre Estónsko (nárast z 5 na 10% HDP), alebo pre Slovensko, ktoré má nárast z 28 na 41%? Podobne keď opozícia kritizovala vládu, že nám nadpriemerne rýchlo rastie nezamestnanosť počas krízy, tak sa neodvolávala na 50-percentný nárast 12%/8%, ale logicky zdôrazňovala nárast o 4 perc. body. Lebo opäť, krajina s nárastom nezamestnanosti z 1 na 2% by bola podľa Markízy veľmi ohrozená, no z pohľadu vzrastu problému nezamestnanosti by sotva nejaký ekonóm nad ňou v porovnaní so Slovenskom zalamoval rukami. 
  PS Prekvapilo ma, že v tejto volebnej sezóne médiá verejnosti neponúkali overovanie výrokov politikov v predvolebných diskusných reláciách, či už vlastnou prácou alebo s pomocou stránky demagog.sk. Keď už sú tie debaty natoľko sledované a komentované (hlavne Fico vs Radičová – kto vraj vyhral a kvôli čomu), prečo pre čitateľov nespraviť odpočet korektnosti uvádzaných údajov a tvrdení? Nie je zaujímavé vedieť, že Fico relatívne viac klamal či zavádzal ako Radičová, alebo že z vybraných politikov patrili k najväčším klamárom Mečiar a Slota? Veď takéto odpočty pomáhajú vo voličoch posilňovať úlohu rozumu a poctivosti, a zmenšovať rolu emócií a imidžu pri hodnotení politikov. 
  Zdroj: Tvrdenia politikov - Demagog.sk (sorry za ich pravopisnú chybu) 
   
 Radičovej preferenčné hlasy: Denník Pravda v pondelok napísal: 
 V minulých parlamentných voľbách získala Iveta Radičová od voličov najviac preferenčných hlasov zo všetkých kandidátov vôbec. 
 To platí len v rámci SDKÚ, zo „všetkých kandidátov vôbec“ bola až tretia. Predbehli ju dvaja kandidáti Smeru, Fico a Kaliňák. 

