

 Po tom, čo sme štyri dni po nevydarenej sobote úspešne dovŕšili senzačný úspech slovenského futbalu vydretým víťazstvom v Poľsku,  sme zaznamenali väčšiu pozornosť médií ešte niekoľko týždňov a následne akoby sa celá tá eufória okolo futbalu zrazu stratila. 
 



 
 O mesiac, tesne pred prípravnými zápasmi Slovenska s USA a Chile vyjadril reprezentačný tréner Vladimír Weiss st. miernu frustráciu z fungovania Slovenského futbalového zväzu. Zdôraznil nevyhnutnosť vyhľadávania nových ľudí, ktorým záleží na budúcnosti tohto športu a potrebu využitia súčasnej atmosféry z dosiahnutého úspechu. V marci tohto roku hlavný tréner slovenskej reprezentácie, inak muž, ktorý sa zaslúžil o jej najväčší úspech, opäť prehovoril o pomeroch v slovenskom futbale. Vyslovil myšlienku, že ak sa v tunajších futbalových vodách veci nepohnú k lepšiemu a každý sa bude hrať len na svojom piesočku nepredĺži po MS 2010 zmluvu so SFZ. Taktiež prejavil sklamanie nad správaním verejnosti, zo strany ktorej mu chýba väčšia podpora postupu na futbalové MS do Južnej Afriky. 
 Položte si sami otázku: Uvedomujeme si vôbec, že o pár týždňov sa predstaví naša krajina na jednom z najsledovanejších podujatí na svete? Do otvorenia veľkého futbalového festivalu ostávajú presne dva mesiace a od októbrového postupu Slovenska na majstrovstvá sveta skutočne v našej krajine chýba väčšia mediálna propagácia, pripomínajúca a zdôrazňujúca význam jedinečného úspechu, ktorý sme dosiahli. Treba si uvedomiť, že ten sa nemusí tak skoro opakovať. A zvlášť v našich futbalových podmienkach. Futbal pre náš národ ani zďaleka nie je náboženstvom ako v iných krajinách, ale krok po kroku ho môžeme posunúť správnym smerom. A kedy inokedy vyzdvihnúť produkt futbal v očiach verejnosti na vyššiu úroveň a využiť atmosféru na zvýšenie záujmu detí o túto hru ako v dobe, keď sa slovenskí hráči stávajú pre nás hrdinami? 
 



 

