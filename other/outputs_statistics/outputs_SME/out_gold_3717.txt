

   
 Na neviem koľkej rue 
 Prekladala som kamene žltými lodičkami 
 Vytiahnutá na breh 
 V šatách slušného strihu 
 So žitankou gavaríla pa rúsky 
 Pri malej káve 
 V kaviarni na rohu tej istej rue 
 Malátny dym 
 A pomoc španielskym slovom 
 Keď som sa smiala 
 Zakláňala som hlavu 
 Vlasy šteklili operadlo tonetky 
 A ja som medzi Nimi bola taká sama 
 Bolo ma plno vytlačila som z priestoru čaro 
 A ostal len tlkot niekde pod slušivým odevom 
   
 Spala som za Seinou 
 V ulici kde Cézanne a Tí čo trhali atmosféru 
 Farebnosť za nechtami  na plátenom habite v duševnom diári 
 V tú noc som vlastne nespala 
 Lepila som čo Tí potrhali 
 A tancovala tango sama so sebou 
 V tej priveľkej kúpeľni plnej zrkadiel 
 Zakláňala som hlavu 
 Vlasy šteklili holý chrbát 
 A ja som v tom porcelánovom svete klopkala sama 
 Bolo ma plno v zrkadlách v oknách 
 A ostal len tlkot 
 Niekde medzi 
   
 Sedela som na tom nárožnom štukovanom balkóne 
 Na rohu ulice kde ráno čo ráno 
 Rozťahujú rybári a zeleninári svoje šapitó 
 Tabak praskal a červenal sa pri premene 
 A dym sa belel a omotával okolo záclony 
 V ruke kniha otvorená už tri dni na tej istej strane 
 Kolená som trela do zodretia o pieskovec 
 Zakláňala som hlavu 
 Túlila ju k omietke a krúžila ňou akoby to bola niečia hruď 
 Prach sa sypal dolu chrbtom 
 A moje vlasy premočené sebaľútosťou 
 Nepekne viseli 
 Ako nitky ochabnutej marionetky 
   
   
   
   
   
   
   
   
   
   
   

