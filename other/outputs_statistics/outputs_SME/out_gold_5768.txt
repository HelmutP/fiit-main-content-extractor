

 „Už si ani nevarí, a keď sa jej pýtajú či jedla, iba prikývne, hoci vôbec nejedla," pokračujem. 
 „A sama býva?" 
 „Sama. Aj ju dcéra vzala k sebe, no na druhý deň jej ušla domov a povedala, že odtiaľ ju už nikto nedostane. Tak za ňou chodia, jedlo jej nosia..." 
 „Fíí, to nie je dobre, chorá je , chorá," ukončuje rozhovor o Bujdášovej teta Kristína. 
 Babka Bujdášová bola ešte nedávno plná energie, plná života. Vyvárala, vypekala, sadila a okopávala v záhrade, s okoloidúcimi mala o čom slovka prehodiť. Vždy každého, kto do jej domu zavítal, mala čím pohostiť. A veru bývalo u nej rušno. Radi ju navštevovali nielen jej dospelé deti a vnúčatá, ale aj ich priatelia. S babkou sa bolo vždy o čom pozhovárať, na všetko mala svoj názor. A keď ju nemal kto zaviesť autom do kostola na omšu, vyše osemdesiatročná babka neraz odbicyklovala tri kilometre tam i späť. 
 Avšak ochorela a je teraz ako vymenená. Už nehostí deti, vnúčatá, pravnúčatá ani priateľov. Prestala vyvárať, vypekať i do záhrady či do kostola chodiť. Len občas zájde za rovesníčkami ku babke Konderlíkovej. Posedia, pozhovárajú sa. 
 Minule ako každý deň prišla k nám teta Kristína a medzi rečou spomenula: „A Bujdášová nie je taká chorá, ešte behá. Včera som bola u Konderlíčky. Bola tam aj Jakabová a Bujdášová, rozprávali sa a víno pili. A nič jej nie je! Hádam len pije!" 
 Nenašla som slová. Nevyvracala som jej to. Veď len nedávno som jej opisovala zákernú chorobu babky Bujdášovej. Nepomohlo. Ona už „vie  svoje". Stačilo jej raz ju vidieť u Konderlíčky a raz-dva našla dôvod i na to, že silno veriaca babka prestala chodiť do kostola. 
   

