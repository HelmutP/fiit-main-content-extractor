

 
  Ale späť k texaskám. Sú všade okolo nás. Nosia ich starší, stredná generácia, teenageri, dokonca aj bábätká. Prečo? Sú pohodlné, cenovo dostupné, hodia sa ku všetkému, dostať ich všade a predsa nosia ich všetci. Pamätám si aj ja na svoje obdobie texaskového šialenstva. Vždy som musela mať nový strih, novú farbu, proste to čo bolo in. Momentálne sa nachádzam v období keď ich nosím minimálne, v podstate fakt len keď musím. Čudujete sa tomu? Prestali ma baviť. Áno, je tu omnoho viac iných kusov oblečenia, ktoré vyzerajú lepšie a celkovo si myslím, že ľudia vo všeobecnosti v nich vyzerajú viac trendy a fabulous. 
 Ženy, čo tak šaty, áno šaty! Pri dnešnom výbere ich môžete mať krátke, dlhé, elastický materiál, satén, hodváb, pohodlné, party, proste od výmyslu sveta. Teraz si možno poviete, ale šaty v bežný deň a načo? Po prvé, budete viac ženskejšia, Vaše telo sa v nich bude vlniť ako v rytme samby a po druhé, keď nemáte dokonalú postavu v šatoch je ju omnoho lepšie zakryť, ako v takých texaskách. Nemusia to byť však len šaty, môžu to byť sukne, krátke nohavice, v prípade zimy s punčoškami, alebo iný typ nohavíc z iného materiálu. Nebojte sa toho! Keď pôjdete najbližšie nakupovať a budete sa načahovať po texaskách, zastavte sa a povedzte si: „život je zmena, však aj tak mám doma už asi 8 v podstate identických párov, čo tak radšej nové šaty". 
 Čo sa týka mužov je to to isté. Poznáte ten štýl polo-golf nohavíc v kaki, hnedej, bielej, smotanovej alebo čiernej farbe. Môžu byť vpredu s pukmi, ale nemusia. Môžem Vám zaručiť, že budete viac elegantní, nezapadnete do bežného texaskového davu a určite sa v teplejších dňoch nebudete toľko potiť. Bojíte sa, že budete vyzerať príliš snobsky, come on... Keď si k tomu nedáte vyslovene u nás akože snobská značka Lacoste polo tričko a pásikavý svetrík cez plece. No worries, snob z Vás určite nebude!   
 Ak Vám tieto argumenty nestačia a stále si myslíte, že texasky sú proste to najlepšie čo móda priniesla, tak ma to mrzí. Každopádne, aj texasky sú módny trend a nie je ľahké ich zladiť, mať ten správny strih a vyzerať v nich sexy-štýlovo. Takže nemyslite si, že keď si dáte "obyčajné" texasky, že z Vás nebude chodiaca módna katastrofa. Len si spomeňte tak 5 rokov späť, ako sa nosili dole rozšírené, potom to boli cigaretové, potom dole čo sa vyhŕňali a dnes sú to úzke, dlhé tak najviac po členky, alebo  tzv. "steal your boyfriend jeans". Taktiež to je časť oblečenia, ktorú treba vedieť zladiť a pri dnešnom výbere to nie je jednoduchá záležitosť. 
 Texasky boli, budú a každý bude mať v skrini aspoň jeden pár. Pokojne ich noste ďalej, len nezabúdajte, že sú tu aj iné možnosti v ktorých by ste mohli vyzerať viac šik a viac trendy! 
 Možno ma milujete, možno ma nenávidíte, hlavne čítajte ďalej! 
 xoxo, o týždeň som tu opäť! 
     
   
 
   
      
   
   

