

 Technické prostriedky nám umožňujú rýchlejšie komunikovať a získavať informácie. Takmer bez obmedzení môžeme zverejňovať svoje názory, diskutovať, vytvárať spoločenstvá, a to najmä prostredníctvom internetu. Takzvané sociálne siete sú stále populárnejšie. Napriek tomu, alebo práve preto, sa naše vzájomné vzťahy zhoršujú a čoraz viac ľudí pociťuje osamelosť. 
 Nijaká (a obzvlášť nie anonymná) komunikácia na diaľku nemôže nahradiť osobný kontakt. Sociálne siete na internete v skutočnosti spoločnosť atomizujú a rozdeľujú, vytvárajú sektársky uzavreté (záujmové) skupiny, ktorým chýba spätná väzba s ostatným svetom. V tejto súvislosti možno pripomenúť, že aj teroristické skupiny s úspechom verbujú ľudí cez internet. 
 Naproti tomu v prirodzenom ľudskom spoločenstve (rodina, obec, národ) sa stretávajú a žijú spolu (učia sa to) ľudia s rôznymi záujmami a vlastnosťami. Tu nie je možné niekoho len tak vyradiť, ale ani zaradiť do zoznamu a skrývať svoju pravú totožnosť. Hodnotné väzby sa budujú dlhodobo. V prirodzenom ľudskom spoločenstve sú vzťahy nutne vzájomné a do značnej miery kontrolované, možno iskrivé, problémové, vždy však opravdivé a nútiace ľudí rešpektovať jeden druhého. Jeden skutočný priateľ znamená viac než tisíc virtuálnych. 
 Neodsudzujem internetovú komunikáciu ako takú (napokon, sám sa jej aktívne zúčastňujem), chcem však poukázať na jej úskalia a obmedzenia... Cez internet si ruku nepodáme! 
   

