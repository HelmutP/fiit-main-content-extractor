
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Hugo Pauliny
                                        &gt;
                Nezaradené
                     
                 Kazenie Mládeže? 

        
            
                                    24.3.2010
            o
            0:16
                        |
            Karma článku:
                5.38
            |
            Prečítané 
            422-krát
                    
         
     
         
             

                 
                    Je pravda, že Hip-hopová hudba kazí mládež? Naozaj je to až také zlé, ako starý hovoria? Má vôbec táto hudba nejaký dopad na správanie mladých?  
                 

                   
   Týmto sa budem zaoberať v dnešnom článku, lebo toho, že sa, už, na regulérny mainstreamový žáner hudby stále vznášajú kritiky mi už je zle. Ja mám tento žáner rád už od malička. Ako každé správne dieťa 90tych rokov som pozeral Fresh Prince-a s Bel Air, v hlavnej úlohe s Willom Smithom, na Supermaxe, čo bolo asi mojou prvou skúsenosťou s týmto žánrom hudby. Neskôr som miloval film Muži v čiernom, pričom si ma tak isto získala titulná skladba od Smitha a neskôr samozrejme Eminemove celoplošné rádiové hity. Neskôr sa moje obzory začali rozrastať o spolupracovníkov mojich obľúbených interpretov až som sa ocitol dnes, keď si môžem povedať, na žáner Hip-hop som trochu odborník. Pričom ma zaráža práve to, koľko negatívnej kritiky je na tento žáner hudby vznesený.  Aby sme začali pekne od začiatku, tak by som chcel začať práve zrodom tohto žánru v južnom Bronxe v New Yorku, kde Dj Kool Herc chodil po uliciach so svojim pick-upom naplneným reproduktormi a hrával takto, alebo aj na akciách zoslučkované podklady koncom 70tych rokov. Neskôr sa tento žáner vyprofiloval na radikálnu výpoveď afro-americkej komunity, od ľudí pre ľudí, o stave society, o ich problémoch alebo o vládnom prístupe podanú nevyberaným slovníkom ľudí bez 9ročky, pričom treba podotknúť, že konečné texty boli veľmi hlbokou kritikou toho čo sa tam dialo koncom 80tych rokov. Počas rokov 90tych sa ale tento žáner zvrhol na manifestáciu bohatstva a slávy tých, ktorý zarobili práve na verejnej žalobe na Ameriku. Hlavnou témou textov hlavne na západnom pobreží sa stali peniaze, ženy, alkohol, drogy, gangy, sex a ďalšie potešenia súvisiace so slávou a nadobudnutým bohatstvom. Na východe sa texty stále upriamovali na problémy komunity, ale tieto témy boli preberané už 20 rokov a sťažovanie sa na políciu a na to ako štát zle zaobchádza s čiernymi už tak povediac nikto nebol zvedavý.    A práve v tejto ére sa začala hlavná vlna kritiky práve preto, lebo sa tento žáner z uzavretej komunity rozšíril medzi biele deti z predmestí, do Európy a do miest kde to s autenticitou nemalo nič spoločné. Ľudia boli jednoducho zvedaví na niečo nové a pre nezainteresovaného jedinca zaujímavé.   Deti so slabšou osobnosťou sa začali správať ako ich pseudo-idoly z rapových nahrávok, čo vôbec neprospelo dobrému menu žánru, ale paradoxne jeho popularite.   Ľudia robiaci hip hop vychádzajú z ich vlastných skúseností s predaja narkotík a uličného života na hrane, pričom sa ale niet čo diviť. Podľa štúdie BBC viac ako 60% čiernych chlapcov vyrastá bez otca, pričom si hľadá vzor pri najbližšom mocnom mužovi, ktorým je v prípade chlapcov s ulice Comptonu či Watts buď drogový díler alebo pasák. Preto by sa nemali biele deti s bohatších predmestí stotožňovať s činnosťami opisovanými v týchto nahrávkach a mali tento žáner brať ako zábavu a niečo nové, nie ako návod na prežitie.   Treba ale k tejto kontroverzií podotknúť, že pokiaľ sa medzi sebou strieľali čierne deti, bolo to okrem pár černošských aktivistov každému jedno, ale akonáhle sa na scéne tohto žánru objavil biely chlapec predávajúci dvojnásobok ako ktokoľvek pred tým, rozprávajúci o problémoch bielych detí a o ich radikálnom riešení, začala sa krížová výprava na tento žáner.   Marshal Mathers III. To všetko začal a posedel si značný čas svojej kariéry za lavicou pred súdom, čelil tonám protestov a cenzúre jeho tvorby až do miery kedy to podľa mňa začalo byť v rozpore so slobodou prejavu.  Po tom sa už zviezli aj jeho spolupracovníci, ale aj ľudia čo s ním nemali nič spoločné a cez absolútne zákazy istých slov až po odstránenie tvorby interpretov s MTV toto nezmyselné razenie pokračuje.   Trochu mi to pripomína podobné razenie proti metalovej hudbe v 80tych rokoch, čo vyvrcholilo v roku 1990 súdnym sporom so skupinou Judas Priest vo veci podprahovej samovražednej správy a navádzania na ublíženie si. Tento absurdný proces bol samozrejme ukončený v prospech metalovej kapely  a všetky tvrdenia boli vyvrátené.    Nuž, moja generácia má tento hudobný žáner rada, a ja osobne nevidím dôvod na kritiku. . .   Vulgarizmy? Počúvaj ako sa vonku ľudia rozprávajú, ešte viac keď cítia zlosť nad vecou, o ktorej hovoria.   Agresia? Čudujete sa?   Keď človeka nikto nepočúva a svoj názor nemôže dať normálne najavo prichádza čas na prejavy iné, hoci aj umelecké a hip-hop je jedným s týchto prejavov, treba ho tak brať a akceptovať, ako prostriedok.       Holla!! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Hugo Pauliny 
                                        
                                            Halo?!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Hugo Pauliny 
                                        
                                            Čo Z Nás Bude vol. 3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Hugo Pauliny 
                                        
                                            Bratislavčania.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Hugo Pauliny 
                                        
                                            Je To Tu Nanič.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Hugo Pauliny 
                                        
                                            Frustrácia.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Hugo Pauliny
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Hugo Pauliny
            
         
        pauliny.blog.sme.sk (rss)
         
                                     
     
        Righteous Brother
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    362
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Epitaf na hrobe starej pravice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




