
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Babič
                                        &gt;
                Fotky
                     
                 Čiernobiele cintoríny... 

        
            
                                    25.4.2010
            o
            12:30
                        (upravené
                26.4.2010
                o
                11:52)
                        |
            Karma článku:
                8.19
            |
            Prečítané 
            1672-krát
                    
         
     
         
             

                 
                    ...jeden Petržalský, doteraz pre mňa neznámy a starý Židovský pod hradom.
                 

                 Do včera som netušil, že v Petržalke máme viac ako jeden cintorín. Ten klasický v starej Petržalke poznám už roky, ale len prednedávnom som sa dozvedel o druhom, ktorý sa nachádza v pohraničnej oblasti, pri starej ceste do Kittsee. Ako som sa dočítal, ešte pred piatimi rokmi to bola zabudnutá oblasť a cintorín doslova splýval s prírodou okolo. Preto som sa tam aj vybral a dúfal som, že nájdem zabudnutý opustený cintorín. To čo som našiel ma však príjemne prekvapilo. Ako som ho zbadal, prvé čo mi napadlo boli americké filmy a tie vojenské pohreby na rozľahlých pekne upravených cintorínoch. Tu som to mal bez niekoľkých výstrelov a v menšom vydaní.                               Po preskúmaní tohto miesta, potulkách v okolí dediny Kittsee a Berg som sa vybral pohľadať starý Židovský cintorín, ktorý sa mal nachádzať pod hradom. Trošku som poblúdil, kým som našiel správnu odbočku na Žižkovu ulicu po všetkých úpravách, čo sa v tejto lokalite za posledné roky udiali, ale onedlho som už otváral bránu na toto miesto večného odpočinku. Aj keď sem dolieha ruch električiek, či stavbárov z náprotivného River Parku, toto miesto má svoje čaro.                              Oprava: Ako som sa v diskusii dozvedel, na Žižkovej ulici som navštívil  Mikulášsky cintorín a nie Židovský. Ten sa nachádza na rovnakej ulici o  niekoľko desiatok metrov ďalej.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (57)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Ftáčnikove fantázie vs. realita
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Ak chcete aby autá nejazdili po chodníkoch musíte si priplatiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Rím...večné mesto na jar
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Helsingor...na kraji Oresundu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Babič 
                                        
                                            Niečo je zhnité v slovenskej pošte (porovnanie s rakúskou)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Babič
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Babič
            
         
        miroslavbabic.blog.sme.sk (rss)
         
                                     
     
        dusou cestovatel..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    173
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2918
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Myšlienky z nevyspatej hlavy
                        
                     
                                     
                        
                            Tour de Europe 2009
                        
                     
                                     
                        
                            Potulky Európou
                        
                     
                                     
                        
                            Potulky svetom
                        
                     
                                     
                        
                            Paródia na život
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Ostatné
                        
                     
                                     
                        
                            Sprostosti
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje fotky
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




