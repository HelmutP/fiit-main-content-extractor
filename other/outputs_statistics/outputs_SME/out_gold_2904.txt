

 Keď chcú mať len jednu povinnú štátnu zdravotnú poisťovňu, nech to urobia ako to navrhujem ja. Nech je jedna štátna zdravotná poisťovňa. Do nej sa bude odvádzať nejaká minimálna suma a táto štátna ZP bude garantovať nejakú minimálnu zdravotnú starostlivosť. Ak bude chcieť niekto niečo lepšie, tak si bude platiť komerčnú zdravotnú poisťovňu. Taktiež navrhujem zmeniť systém zdravotného poistenia v tom, že zmluvy sa budú uzatvárať medzi zdravotnými poisťovňami a ich klientami. A ZP bude preplácať zdravotnú starostlivosť u každého lekára na Slovensku a aj v zahraničí v rámci uzatvorenej zmluvy. 
 Táto štátna zdravotná poisťovňa by dostávala všetky peniaze z povinného zdravotného poistenia a štát by ju mohol dotovať koľko by chcel. Čo všetko patrí do minimálnej zdravotnej starostlivosti by mohla určovať nejaká komisia, aby to bolo aspoň trochu nezávislé. 
 Myslím, že tak by bolo spokojných viacej ľudí ako doteraz. A vláda by sa mohla hrajkať na svojom piesočku bez obáv, že by niečo pokazila. 

