

 Slovensko musí znižovať deficit verejných financií. Kto dnes vie, v akom stave sú verejné financie? Vláda tají údaje, čo by určite nerobila, keby čísla boli pozitívne... Po voľbách nás čaká asi veľké prekvapenie. Transparentnosť vo verejných financiách znamená zverejňovanie stavu a vývoja financií. Vrátane situácie v jednotlivých častiach rozpočtu ako je napríklad nakladanie s rezervou predsedu vlády. Keby bola na internete zverejnená rezerva predsedu vlády, ale aj iné podobné kopy peňazí, je podstatne menej pravdepodobné, že by predseda vlády z našich peňazí financoval fitnesku... 
 Transparentnosť je veľmi dôležitá pri zmluvách. Každá zmluva, kde jednou stranou zmluvného vzťahu je verejná správa, musí byť verejná. V normálnej spoločnosti musí byť úplne jasné kto, za čo a koľko dostáva od štátu za rôzne služby a pochopiteľne, ako sa k tomu dostal. 
 Transparentnosť nie je potrebná len tam, kde sa priamo narába s financiami (rozpočet, dotácie, obstarávania...) ale aj v súdnictve, školstve alebo pri zverejňovaní záznamov z rokovania vlády. Všade. Všetky oblasti, ktoré je potrebné upratať, majú spoločného menovateľa - potrebu výrazného zvýšenia transparentnosti. 
   
 Pokiaľ transparentnosť nebude povýšená na princíp, zmeny budú nanič. 
   

