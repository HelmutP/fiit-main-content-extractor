

   
  Prichádza jar, pomaly, ale isto ......... 
   
 No u mňa je stále zima, chlad, srieň mi padá do duše....... nenávratne ....... 
   
 Stromy začnú pomaly nahadzovať listy, trávička sa zazelenie, už vidieť spod snehu prvé snežienky, ich krehké hlávky sa nakláňajú nad zmrznutou zeminou záhradiek. Milé. 
   
 Už ani mrazov nebolo, ale dnes ráno meteostanica zase ukazuje mínus štyri  L 
   
 Mínus jeden život. 
   
 Mínus jedno priateľstvo. 
   
 Mínus jedna oddaná dušička . 
   
 Mínus jeden zo zmyslov života. 
   
 Prečo strata tak veľmi bolí ? 
   
 Prečo sa musí umierať ? 
   
 Nemá sa oplakávať, kto ešte žije, hoci má vymeraný čas na tejto Zemi. Ale dá sa to  ??? 
   
 Teória je pomerne nekomplikovaná, jednoducho sa predkladá, no prax - to je už „iná káva". 
   
 Cítim si vlastné slzy -  sú slané a teplé, také mám aj líca, aké iné by mali byť, keď tadiaľ vedú 
 potôčiky sĺz ? 
   
 Život je síce krutý, ale aj krásny . Poďme spomínať na to krásne .........Vstúpte do komnaty spomienok  - nech sa páči . 
   
 Tri malinké klbôčka - dve trojfarebné, jedno hrdzavobiele. Mäkké kožúšky. Velikánske najprv modré, neskôr zelené okále. MAČIČKY. Naše tri poklady spomedzi mnohých pred nimi.  
 Šantenie po veľkej záhrade, pomedzi hriadky zemiakov, prelet nad kvietkami, po mäkkej trávičke.... Nádherný obraz, keď unavené prídu k mame a pijú mliečko. Neskôr všetky tri obstanú misku a počuť už len slastné mľaskanie J Teplé mliečko robí zázraky...... aspoň, kým boli malí, neskôr robilo „iné" zázraky ! Postupne zistili, že ich teritórium siaha aj do domu -  chodba, kuchyňa, pravdaže, aj izby treba preskúmať, a to najmä periny . Šup do nich -  aké mäkké..... Neskôr najobľúbenejšie miesto. V zime zase na koberčeku pod gamatkami.... No lenivé neboli, Božechráň -  každý z nich chytil denne minimálne dve myšky, ktoré doniesol pekne na dvor ukázať / o tých sme teda logicky vedeli / . Úplný a presný počet je nám záhadou. Maznanie, tuľkanie, papanie - ich celodenná činnosť kombinovaná so spinkaním. Pohoda a balzam na dušu -  MOJU, našu. Pradenie, vrnenie, alebo ako to kto nazýva - ozaj príjemnejší zvuk nepoznám. My sme to mali trojnásobne. Ako aj tri misky, tri nádobky na ..... veď viete na čo J Neskôr sa to zdvojnásobilo , odvlani ubudla ďalšia micka ......... nie je už ani ten dvor, ani tie záhony zemiakov, ani kvetinové hriadky.........ostali len tieto spomienky a posledný kocúrik..... ten najkrajší, najmilší, hrdzavobiely okánik. Pozerá na mňa tými svojimi nádhernými očkami a ja mu neviem pomôcť .  
 Alebo viem ? Je na veterine, v teplúčku, každý deň dobré jedlo....... no koniec sa blíži v nenávratne .  
   
 Denne sa chodím tešiť ale aj trápiť . 
   
 Láskavá náruč veterinárky Mišky ........ a moja.  Len neviem, či ma to teší, alebo skôr trápi...... 
 Sama v sebe sa nevyznám.......som zmätená .......postupne odchádza 20 rokov môjho života, ktorý bol aj vďaka Ryškovi zmysluplný. 
   
 Ostanú spomienky, fotografie, zážitky, pocity -  a to je dosť, dosť na to, aby som si uvedomila, že všetko raz začína a raz končí. Toť neyvrátiteľný kolobeh života ..... 
   
 O pár dní bude koniec. 
   
 Alebo nový začiatok  ? Možno  - nové tri klbká ......... J 
   
 Ryšinko, si ešte medzi nami, môj drahý kocúrik, strašne ťa mám rada, a ďakujem , že som s Tebou mohla prežiť naozaj nádherných 20 (!!!!!!!!!) rokov. Už si zaslúžiš pokoj, a aby ťa už nič neboľkalo. Aký bol Tvoj kocúrikovský život, pokojný, nikdy si ani len nezavrčal, ani teraz, keď Ti pani doktorka ošetruje očko a robí všakovaké divy s Tebou, vrátane injekcií, taký dúfam, bude pre mňa aj život bez Teba ...... Ryšičko môj .................kocúriček............. L 
   
 Je mi hrozne. 
   
 Idem za Ryškom. Pohladkať, pritúliť, popočúvať pradenie ...... poďakovať ......  poplakať si . 
   
 apríl 2010 
   
 Ryško je stále tu, medzi nami, ešte niet miesta pre slzy. A možno ani nijaké nebudú, veď mal a stále má krásny život, je obklopený ľuďmi,ktorí milujú tieto milé šelmičky..... 
   
 20 rokov .................veľa alebo málo ? 
 Veľa pre krásne chvíle, veľa kvôli nášmu priateľstvu, veľa.......veľa pre všetko. 
 Ale málo pre to, aby som vyjadrila, ako mne a celej našej rodine naplnil tento milý hrdzánik život. Naučil nás mnohému, daroval nám veľa mačičkovskej lásky, veľa tepla do sŕdc, veľa pritúlení, veľa všetkého, čo len mačička môže človeku dať. 
 Dáva nám to najcennejšie -  seba. 
 Ryško, veľmi Ťa ľúbim !!!!!!!! 
 Nikdy nezabudnem ...... 
   
   
   

