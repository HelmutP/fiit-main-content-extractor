
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Ráchela
                                        &gt;
                Fejtóny
                     
                 Čo s našimi Maďarmi 

        
            
                                    6.6.2010
            o
            9:33
                        |
            Karma článku:
                7.19
            |
            Prečítané 
            832-krát
                    
         
     
         
             

                 
                    Napriek slovakizácii, reslovakizácii, výmene obyvateľstva a posilňovaniu slovenského živlu sú tu. Majú dobrý koreň, naši Maďari. Sú naši. Tisíc rokov sme žili spolu a títo naši ostali s nami trčať za Dunajom, Ipľom, či nad Bodrogom, vďaka Čechom, najskôr v Československu a teraz na Slovensku. Nemali to s nami vždy ľahké. Ale ani my s nimi.
                 

                 Česi zachránili Slovákov, vyhnali odtiaľto boľševika v osemnástom, aby ho potom v štyridsiatom siedmom dosadili, ale po 74 rokoch to s nami definitívne vzdali. Musíme sa vysporiadať sami s boľševikom i medzi sebou navzájom. S Čechmi sme sa rozišli tvrdiac, že sme odlišní. S Maďarmi sme si podobní, tak by sme sa azda mohli dohodnúť. Tým, že tak dlho s nami žili (a my s nimi), sú takí istí, ako my. Keď stretnete na Slovensku človeka, kým vám to sám nepovie, neviete, či je Maďar, alebo Slovák. Je mnoho Slovákov vyzerajúcich presne ako archetyp Maďara, a je mnoho Maďarov, vyzerajúcich presne ako archetyp Slovana. Sú oni po maďarsky hovoriaci Slováci, alebo sme my po slovensky hovoriaci Maďari? Ťažko povedať. Aj povahovo sme si s našimi Maďarmi podobní. Nejednotní a hašteriví. Pluralita nám, chvalabohu, nechýba. A to je dobre. Je v tom záruka, že nás nezjednotia zase na báze nacionalizmu. Múdri hovoria, že keď chceš niekoho spoznať, treba mu dať do rúk moc. My sme to urobili pred štyrmi rokmi s našimi Maďarmi. Odovzdali sme i do rúk Nitriansku župu. Správali sa podľa očakávania - rovnako ako Slováci. Brali všetko. Každá politická strana, aj tá najdemokratickejšia, baží po moci. A najradšej by sa o ňu s nikým nedelila. Chce všetko, chce sto percent. A je pre ňu najväčším nešťastím, keď všetko získa. Teraz so mnou kategoricky nesúhlasia členovia Komunistickej strany, ktorí sa vôbec netaja tým, že chcú sto percent ako mali kedysi a tvrdia, že to by bolo pre nás ostatných ideálne, keby boli opäť vedúcou silou spoločnosti. My však zažívame na vlastnej koži, ako to dopadne, keď strana má všetko. Ale vráťme sa k našim Maďarom. Nedá sa nič robiť, sú tu. Nepodarilo sa nám ich slovakizovať (až na niekoľkých v SNS), reslovakizovať (niekoľko pomaďarčených Slovákov je v SMK či v Moste - Híd), ani vysťahovať (aj keď zopár - 83 tisíc - sme ich šupli za Dunaj). Presvedčili sme sa, že nie je dobre, keď majú v rukách župu len oni. Treba sa o ňu podeliť a dávať na seba navzájom pozor, dívať sa pozorne jeden druhému na prsty. Mali by sme sa s nimi dohodnúť. Česi to s nami vzdali. Nechali nás trčať tu, v Zalitavsku, s našimi Maďarmi. Bisťu, ištenbizoň, s nimi by sme sa mali dohodnúť! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Malé porovnanie dvoch veľkých krajín
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Kampaň pred prezidentskými voľbami ukazuje slabiny pravice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Ukrajina - nešťastná krajina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Nad výsledkami župných volieb v jednom kraji a v jednom okrese
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Za tú našu slovenčinu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Ráchela
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Ráchela
            
         
        rachela.blog.sme.sk (rss)
         
                                     
     
        Skeptik s kritickým myslením.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    60
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1197
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kampaň pred prezidentskými voľbami ukazuje slabiny pravice
                     
                                                         
                       Július píše Oľge
                     
                                                         
                       Daňové licencie alebo škola podnikania hrou
                     
                                                         
                       Ficov veleodvážny sľub
                     
                                                         
                       Výhodná ročná percentuálna miera nákladov: 59140.89%
                     
                                                         
                       Vďaka, herr Kotleba, za búranie mýtov o tejto krajine
                     
                                                         
                       Hnedý kôň volieb a úbožiak Fico
                     
                                                         
                       Kto za to môže?
                     
                                                         
                       Extrémizmus, moc a politická paradigma spoločnosti
                     
                                                         
                       Volíme menšie zlo, len aby sme v hanbe neostali. Už zasa
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




