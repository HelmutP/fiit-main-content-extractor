

   
 Všetko to začne v piatkový večer tak tri týždne pred štartovým výstrelom. Spontánny nápad ako každý iný, teda rozumej šialený. Zdvihnete telefón a zavelíte kamarátovi, ktorý zase raz týždeň netrénoval, lebo si zase raz natiahol sval: „Ideme na maratón!". Čo to nabehané máte, no predsa len je rozdiel odbehnúť 10, 15, 18 alebo 42 kilometrov. Každopádne, jedno zo základných pravidiel znie, treba si veriť. 
   
 Pražská trať je naozaj krásna, 80% trate bežíte po historickom centre, a i keď vám beh často strpčujú nesúmerné dlaždice, z ktorých človeka ide trafiť šľak, užívate si to... Aspoň do určitého kilometra... Štart organizátori stanovia na 9.00 hod a nie je dva krát jednoduché predrať sa miliónmi povzbudzujúcich na to správne miesto. Vlastne, ani sa vám to nepodarí. V konečnom dôsledku je to však jedno, pretože na rozdiel od napr. takého Národného behu Devín - Bratislava, časomiera meria dva časy: čas od výstrelu a druhý, podstatnejší čas štart - cieľ. 
   
 Teda, štartujeme... prvé metre, pohoda. Prvý krát pretnete Vltavu, prvý krát sa občerstvíte, všetko sa zdá v pohode. Aspoň zatiaľ.... Potom však príde prvá prekážka - štvrtý kilometer. Konkrétnejšie, ostnatý drôt trčiaci z pouličného osvetlenia. (Paráda Šemšej, zase si to ty, kto sa vykýblil na zem pred bežiacim davom!) V duchu nadávate, nahlas nadávate... skrátka, nadávate ako pohan. Pád vyzerá hrôzostrašne, no napokon spôsobí iba oškreté koleno a drobnú ranu na dlani. Zapôsobí na vás akoby opačným efektom, nohy sa rozbehnú ešte rýchlejšie. Móóóž byť! 
   
 Okrem tradičného maratónu sa bežia i rôzne ďalšie súťaže, ako napr. majstrovstvá Európy policajtov (vyhral Rus, šak Ká Gé Bé, rozumíš!), závody tímov, či štafeta na 4 x 10 km (posledný zo štvorice 12, 195 km). Problém štafety spočíva v tom, že niektorí borci precenia svoje sily a rozbehnú sa ako Vinetuova Hátatitla. Potom im musia policajti lemujúci trať podávať prvú pomoc - celkom zaujímavý pohľad. Každopádne, nechajme amatérov amatérmi a poďme ďalej. Na 8 kilometri miniete práve takéhoto „bežeckého inžiniera" a uvedomíte si, že sa vám beží v nesmiernej pohode. Fyzickej aj psychickej. Táto pohoda lesná trvá približne do 18 kilometra, na ktorom sa tešíte, ako sa budete všetkým chváliť svojím skvelým časom ďaleko pod 4 hodiny. No čo netušíte: onedlho utrpí ako prvá vaša psychika... Pretože, z dvoch túb s podpornou látkou pripnutých za vašim potítkom vám vypadne pri občerstvovaní práve tá určená na prekonanie potenciálnej krízy, ktorú ste plánovali do seba natlačiť na 30. kilometri. Mávnete však rukou, aká kríza, ak to takto pôjde ďalej, žiadna nepríde. Tak dobre sa vám beží... Pár kilometrov neskôr je ale všetko ináč. 
   
 Kríza klope na dvere už pri dosiahnutí polovice trate, no naplno sa usadí najmä na nohách po takom 25 kilometri. Zle je, zle je... (Tí ktorí tvrdia, že maratóny bežíte vlastne dva: jeden do 30 kilometrov a ten druhý po 30-tke, vedia, o čom hovoria. Hranicu si samozrejme posuniete ešte nižšie, ak ste odpadli napríklad pri takom 25 kilometri...) Keď to už proste ďalej nejde, tak to proste ďalej nejde. A vy sa musíte vzdať prvého z dvoch hlavných cieľov - zabehnúť maratón bez prestávky. V skutočnosti váš výkon po 25 kilometri (až do takého 38-teho) neustále striedajú chvíle behu s momentmi blahodarnej chôdze. Ešte po tamtú tabuľu, a bežíte ďalej...ešte tento kopček, ešte pár krokov... Pri takejto mini pauze spozorujete, že vás predbiehajú ľudia, do ktorých by ste to ani za svet nepovedali. Začínajúc vodičom s balónikom pokresleným časom 4 hodiny na 30 kilometri (váš tajný sen sa práve rozplynul, teraz ide iba o to, nenechať sa predbehnúť ďalším balónikom, no to príde tiež), cez chlapíka so psom na 34 kilometri, až po ženu v stredných rokoch a viac niekde o chvíľu neskôr. Našťastie asi na takom 31 kilometri natrafíte na chlapíka s obandážovaným kolenom, ktorý vás ďalšie dva kiláky celkom fasa potiahne. Až pokým sám neodpadne a nenahodí čoraz viac obľúbenejší pešibus... 
   
 Iné dôležité konštatovanie: žena s veľkým zadkom stále dokáže bežať rýchlejšie ako vy. Jednu takú prenasledujete asi tri kilometre, no potom sa hlási občerstvenie a ona vám skrátka utečie. Prdel sa stratí v dave, takpovediac. A vy ju už nikdy neuvidíte... Ha, ešte jeden totálny fakt: po takom 30 kilometri si predstavujete, ako sa práve kúpete v bazéne, oblievate vodou, či pijete dokonale načapovanú dvanástku. Mysľou vám lietajú všetky asociácie, ktoré súvisia s ľadovým občerstvením. Samozrejme, pravidelne dopĺňate sacharidy a tekutiny na občerstvovacích staniciach, no dvesto metrov neskôr máte také sucho v papuli, že sa to nedá ani popísať. Ostanú vám teda aspoň predstavy a konštatovanie, že veď už to nebude trvať dlho. Leda hovno!!! Pretože teraz už ste v takej fáze, že každým krokom sa vám cieľ akoby vzďaľoval, presný opak skutočnosti. Bežíte už celú večnosť a hľa: ubehol iba jeden kilometer. A takto sa to vlečie vlečie a neutečie... Neustále míňate tých istých ľudí, ktorí už v danej fáze bežia podobne ako vy, teda intenzívne striedajú beh, krok, beh, krok.... Až na dobitého týpka na 38 kilometri v bezvedomí. Ten už iba leží a nevníma. 
   
 Niekde pri 36 kilometri na vás kričí pani s pneumatikou väčšou ako koleso kamiónu, že, citujem: „hop hop hop hop". Milé od nej, ale aj tak na ňu v duchu nadávate a slepo jej závidíte. Alebo ako poznamenal sucho jeden od vedľa: „Teď už ani to hop nepomůže". Na 37 kilometri hrá ešte stále ten istý falošný chlapík na gitaru, pričom všetko doprevádza „najúžasnejším" spevom. Započúvate sa a hľa, majsterpís hudby, Perfect day od Lou Reeda. V danej situácii si spomeniete iba na jediné: ako hlavného protagonistu filmu Trainsporting pri tejto piesni odnášala sanitka zdrogovaného ako dogu do nemocnice. Rýýýýchlo preč odtiaľto!!!! Jec aba šnelst müúúúgliš vek.... 
   
 Niekde medzi 36 a 38 kilometrom dostanete zrazu nesmiernu chuť socializovať seba sa s okolitým svetom. Naľavo prepletá nôžkami malý plešatý Talian, prónto prónto, angličtina nič moc... šak Talian. Napravo sa trápi Rakúšan v strednom veku. Až vás zamrazí, keď počujete koľký maratón beží - 28! No a či chcete alebo nie (môžem vás ale ubezpečiť, že chcete), blíži sa cieľ. Na 39 kilometri dokážete ešte predbehnúť obézneho mladíka (česť jeho výkonu a pamiatke, naozaj obdivuhodné) a zrazu sa pred vami vynorí cieľová rovinka. Hecnete sa, naposledy chcete dať všetko. Tento elán vás však prejde po 20 metroch pološprintu, ohlási sa totiž prvý kŕč. No v duchu sa aj tak iba zaradujete, že prvý kŕč až v cieľovke, jééés, a ďalej sa budete kľudne už aj plaziť, proste to dáte aj keby neviem čo bolo... Napokon, po 4 hodinách a 17 minútach po štartovom výstrele (skutočný čas lepší asi o 5 minút, keďže chvíľu trvá, než ste sa dostali k štartovej čiare), 54 pesničkách v ipode (vypli ste ho ohučaný na 35 kilometri), neskutočnom výkone a ešte neskutočnejšom zážitku, pretnete cieľ. 
   
 No a keďže ste nenormálne smädný a dehydrovaný a keďže je všetko zadara (šáááák Slováč sa nezapre), ládujete do seba čo to dá. Voda, jontový nápoj, banány, pomaranče, jablko... Ak je to váš šťastný deň, možno sa nepogrciate....  Nepogrciate sa možno ani vtedy, keď asi pomedzi stovkou ľudí v úzkej uličke, v ktorej čakáte na gravírovanie medaily, prefrčí oldtajmer s Indiánmi (tými z Ázie) na palube. Vraj sťažujte sa Bímovi, starostovi Prahy, že to dnes nezakázal. Veľmi radi sa mu posťažujete, ak na vás z tých svojich 160 centimetrov pán starosta dovidí. Hold, biznis je biznis, že áno... Ale zážitok vám to určite nepokazí. 
   
 Ste hrdinovia, dokázali ste to! Čo tam po tom, že si tri dni nesadnete bez nenormálnej bolesti všade od pol pása nadol. Že budete chodiť ako chromá kačica vyradená na odstrel. Ani tú bolesť zošúchanej kože pod pažou a medzi nohami nevnímate, takí ste šťastní (to príde večer v sprche!). Môžete sa totiž pochváliť historkami, ktoré bude vaše okolie počúvať nasledujúce tri týždne. No ale teda bravó, zvládli ste svoj prvý maratón v živote. Zrejme na dlhšiu dobu i váš posledný... 
   
   
 A ešte nejako takto to vypadá v tabuľke: 
   




 Split 


 Time 


 min/Km 


 Delta 


 min/Km 


 RealTime 




 10 Km 


 0:59:02 


 5,54 


 0:59:02 


 5,54 


 0:53:57 




 20 Km 


 1:51:24 


 5,34 


 0:52:22 


 5,14 


 1:46:19 




 Half marathon 


 1:57:41 


 5,34 


 0:06:17 


 5,43 


 1:52:35 




 30 Km 


 2:52:27 


 5,44 


 0:54:46 


 6,09 


 2:47:21 




 40 Km 


 4:03:54 


 6,05 


 1:11:27 


 7,08 


 3:58:48 




 Finish 


 4:17:47 


 6,06 


 0:13:53 


 6,19 


 4:12:42 




   
 PS: môj heroický výkon venujem iba jednej jedinej osobe.... 
   
   

