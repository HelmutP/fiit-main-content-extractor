
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Natália Blahová
                                        &gt;
                nezaradené
                     
                 Život pred životom 

        
            
                                    10.9.2007
            o
            6:30
                        |
            Karma článku:
                13.97
            |
            Prečítané 
            4948-krát
                    
         
     
         
             

                 
                    Možno o tom, kedy začína život.
                 

                  Dr. Fedor bol pôrodník a zároveň psychiater. Rozpracoval zaujímavý projekt. Keď ho u nás predniesol vedeckej obci, bol vysmiaty.   Švédsko sa stalo jeho druhou domovinou. Zmenil si meno na Freyberg a nebolo to jediné, čo zmenil.     Základná myšlienka jeho projektu bola v tom, zistiť, ako intrauterinný vývoj človeka vplýva na jeho ďalší život. A prišiel k nečakaným záverom.     Jedným so zdrojov jeho informácií boli deti, ktoré sa narodili z nechcených tehotenstiev. Ako sa k nim dostal? Veľmi ľahko. Zo zoznamov. Boli to tie deti, ktorých matky v tej dobe predstúpili pred interupčnú komisiu, kde vysvetľovali zúčastneným cudzím ľuďom, prečo dieťa nemôžu a nechcú mať. A nepresvedčili ich. Tie nechcené deti sa narodili.     Psychiater sledoval ich rast a vývoj celých 25 rokov. Zistil, že ich životy sú si v mnohom podobné. Seba samých takmer všetci popisovali ako nešťastných ľudí, štruktúra ich osobnosti bola depresívna, rozvádzali sa, strácali záujem o vlastné deti a ich osudy boli neraz tragické.     Ako psychiater sa často potýkal s prípadmi , keď príčiny fóbií, narcistických psychóz a rôznych hraničných stavov u pacientov nedokázal zanalyzovať. Strávil  s pacientom dlhé hodiny pátraním po príčinách jeho stavu, venovali sa problému zo všetkých strán, ale jeho základy nikde nenašieli a katarzia neprichádzala. Keď nevedel, kedy a kde vznikol v jeho mysli daný problém, s ochorením sa bojovalo veľmi ťažko.     Na pomoc prišla retrográdna hypnóza. Pomocou nej vracal svojich pacientov do minulosti. Zistil, že sa dá vrátiť veľmi veľmi ďaleko v čase. Do života pred životom.     Dospelí ľudia mu rozprávali ako sa cítili a čo počuli, keď boli ešte súčasťou matky. Konečne našiel odpovede na nezodpovedané otázky, konečne sa nejeden bludný kruh, ktorý spôsoboval pacientovi utrpenie, uzavrel.      V hypnóze boli schopní získať vzácne informácie, ucelené pocitové aj faktické celky. Priemerný človek sa za pomoci hypnózy dostal približne do svojho 8 mesiaca od splodenia. Rozprával o tom, čo o ňom matka hovorila, aké zážitky prežívala, aké stresové situácie ju postretli, kto sa v jej okolí vyskytoval a aké slová a pocity si z neho pamätal.     V rámci štúdia týchto faktov prišiel k nečakanému záveru. Čím bo človek na vyššej inteligenčnej úrovni, tým sa dostal ďalej v čase. Princíp tejto priamej úmery bol úplne spoľahlivý. Zistil, že tí najinteligentnejší z jeho skupiny boli schopní sa rozpamätať na 4. až 5. mesiac svojho vnútromaternicového života.     Rozpracoval metodiku, aby mohol tvrdenia svojich pacientov a klientov overovať. A boli to tisíce prípadov. Desiatky študentov a spolupracovníkov sa vydali do terénu a hľadali dôkazy pre tvrdenia, ktoré hypnotizovaní podávali. Pátrali po kalendároch, denníkoch, dokumentačných materiáloch, korešpondencii. Pýtali sa na detaily príbuzných, starých rodičov, súrodencov. Po zhromaždení tohto množstva údajov, boli všetky výpovede pacientov verifikované.     Zistili, že ak matka zažila hrôzy, bola týraná, žila v strachu a úzkosti, dieťa trpelo po narodení úzkostnými atakmi.   Zistili, že manické stavy, ktorých nemohli nájsť príčinu, mali tiež korene v prenatálnom vývine.   Najnižšie a najstaršie štruktúry mozgu takto postihnutých skúmali aj pomocou elektromagnetickej rezonancie a zistili, že sú užšie a atrofované.     Istý klient, ktorý mal veľký problém s nenávisťou voči starším tlstejším ženám, v hypnóze rozprával o svojom živote v 5. mesiacoch vnútromaternicového vývoja. Rozprával súvisle a myšlienky a pocity formuloval jasne.   Jeho otec bol na vojne a prichádzal občas na víkend za matkou, s ktorou nebol zosobášený. Často hovoril o potrate a rozchode, čím spôsoboval jeho matke veľkú bolesť.   Oveľa horšie na tom však bol jej vzťah k svojej matke, u ktorej vtedy bývala. Často sa spolu hádali, panovala tam zlosť a neznášanlivosť. Matka sa na svoju dcéru hnevala, že sa takto spustila, denne jej to vytýkala a nazývala nenarodené dieťa iba pankhartom. Matka po rokoch priznala, že jej syn si po celý život nevedel nájsť cestu k svojej starej mame, napriek tomu, že situácia sa po jeho narodení výrazne zmenila. Stará mama sa s tým vyrovnala, svojho vnuka mala veľmi rada a vždy sa snažila byť ústretová.  V jeho mysli však stále predstavovala hrozbu pre jeho matku a pre seba. Preto typ týchto žien, pripomínajúcich starú mamu v ňom vzbudzoval nenávisť.     Keďže bola pacientova trauma pomenovaná a jej základy sa našli, našli sa aj prostriedky, ako proti nej bojovať a zmierovať sa a jeho liečba úspešne napredovala.           Spomienky ukryté v najhlbších častiach nášho mozgu sú do nás vryté nezmazateľne ako do kameňa. Vynárajú sa nečakane a ovplyvňujú náš život viac, ako by sme tušili a chceli priznať.       Zdroj: MuDr. Darina Štúrová, psychiatrička a psychoanalytička, seminár pre pracovníkov a spolupracovníkov OZ Návrat       A ešte malý dovetok. Často si na prednáškach a seminároch píšem na okraje osobné poznámky k téme, aby som si neskôr spomenula, čo mi z nich vyplýva pre prax alebo niekedy iba toľko, kde sa moja myseľ pri týchktorých slovách zatúlala. Pri tejto časti som si našla poznámku napísanú veľkými písmenami: ŽIVOT JE DôLEŽITEJŠÍ, AKO SI MYSLÍM!             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (53)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Alipaškovia a ich majetky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Čo bude po SME
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Závažné podozrenie zo sexuálneho zneužívania dieťaťa odfláknuté
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Žasnem a neprestávam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Trnava – mesto za euro alebo za zlámaný groš
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Natália Blahová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Natália Blahová
            
         
        nataliablahova.blog.sme.sk (rss)
         
                        VIP
                             
     
          
 
 
Srdečne ďakujem za Vašu priazeň. Nájdete ma aj tu: nataliablahova.sk
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    363
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5102
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodina
                        
                     
                                     
                        
                            úvahy
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            pocitovky
                        
                     
                                     
                        
                            knižnica
                        
                     
                                     
                        
                            Komentáre
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            u nás
                        
                     
                                     
                        
                            foto
                        
                     
                                     
                        
                            nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vymierame a vlastná hlúposť nás dorazí
                                     
                                                                             
                                            nataliablahova.sk
                                     
                                                                             
                                            Rada Vás privítam medzi priateľmi
                                     
                                                                             
                                            301
                                     
                                                                             
                                            Pre mamičky, detičky a iné -ičky
                                     
                                                                             
                                            SaS plní sľuby a drží slovo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            nataliablahova.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            nataliablahova.sk
                                     
                                                                             
                                            Rada Vás privítam medzi priateľmi
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




