
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Vrátny
                                        &gt;
                Nepriateľská osoba
                     
                 Nepriateľská osoba 18 Keď analýza znamená domovú prehliadku 

        
            
                                    19.6.2010
            o
            18:11
                        (upravené
                28.4.2011
                o
                10:28)
                        |
            Karma článku:
                7.81
            |
            Prečítané 
            1304-krát
                    
         
     
         
             

                 
                    „Analýza" v odbornej terminológii eštebákov znamenala domovú prehliadku, samozrejme bez vedomia dotknutej osoby. Aj v mojich veciach, či doma, či v práci, sa silové zložky socialistického štátu prehrabávali a svoje zistenia vždy uzavreli konštatovaním, že sa našli nejaké zaujímavé veci, ale nikdy neuvádzali čo. Beriem to tak, že nenašli nič, čo by predstavovalo veci spravodajského záujmu a len potrebovali nejaké zdôvodnenie pre vlastné ospravedlnenie. Však čo len môžu nájsť v byte starého mládenca, ktorý dokonca ani porno neprechováva!
                 

                 V spise sa mi v tejto súvislosti zachovali aj nasledovné agentúrno-operatívne opatrenia:   - Vzhľadom k doteraz získaným poznatkom k navrátilcovi VRÁTNEMU ... navrhujem vykonať u objekta v byte STÚ „ANALÝZA" so zameraním sa na:      
zisťovanie rôznych písomností, hlavne vo vzťahu na KZ (kapitalistické zahraničie), za účelom zisťovania ich obsahu, či nemajú spravodajský charakter,
   zisťovanie adries a stykov na KZ, do soc. štátov, ale aj v ČSSR, s cieľom ich preverenia, či sa nejedná o osoby z nášho hľadiska zaujímavé /prepážky, krycie adresy apod./ Adresy taktiež získať za účelom porovnania s adresami, ktoré sme doposiaľ zistili,    rôzne materiály spravodajského charakteru, ako sú šifry, kódy, tajnopis, foto a filmovacie prístroje,   podľa možností prehodnotiť fotografie a diapozitívy, ktoré objekt vlastní, nakoľko značná časť z nich bola zhotovená po absolvovaní turistických ciest,   
iné veci a predmety súvisiace s podvratnou činnosťou /špeciálne upravený rádioprijímač, väčší obnos západnej meny, TK apod./ (TK - tuzexové koruny, zvláštne poukážky, na ktoré bolo možné vymeniť cudzie meny na nákup vlepšie zásobovaných špecializovaných obchodoch zvaných Tuzex, odvodené od tuzemského exportu)
     Termín: 30.8.1981     - V prípade, že po vykonaní STÚ „ANALÝZA" budú získané poznatky, ktoré by nasvedčovali nepriateľskej činnosti objekta, bude jeho rozpracúvanie aktívne pokračovať naďalej za účelom dokumentácie tejto činnosti s cieľom realizácie objekta, prípadne vykonania ďalších príslušných opatrení.   - V opačnom prípade, t.j. ak nebudú získané spravodajsky hodnotné poznatky prevedenou „ANALÝZOU", navrhujem vykonať prepočutie objekta pod vhodnou legendou, s cieľom objasnenia zistených stykov na KZ i ostatných zaujímavých okolností. Po tomto navrhujem zväzok ukončiť a uložiť do archívu. Prepočutie vykonať so súhlasom odb. II. S-ZNB Praha.    Termín: podľa výsledkov „ANALÝZY"    ... Plánované agentúrno-operatívne opatrenia v akcii „FILOZOF" sa budú v priebehu rozpracovania doplňovať a rozvíjať podľa vzniklej operatívnej situácie, pričom hlavné úsilie bude zamerané na odhaľovanie a dokumentovanie takých okolností, ktoré by potvrdzovali podozrenie, že objekt „FILOZOF" je v spojení s cudzou spravodajskou službou a vyvíja voči ČSSR nepriateľskú činnosť.    Operatívny pracovník, nadporučík z Krajskej správy Zboru národnej bezpečnosti, Správy ŠtB Košice, je autorom nasledovného záznamu:   „Dňa 5. novembra 1981 pri realizácii STÚ „ANALÝZA" v akcii „FILOZOF" reg. č. 12199 v súvislosti s odvolaním a zabezpečením objekta, jeho príbuzných, ako aj osôb bývajúcich v susedstve objekta, vznikli výdavky vo výške 217,30 Kčs /slovom dvestosedemnásť korún a tridsať halierov čsl./ a to za účelom zakúpenia pohostenia pre osoby, ktoré sa na zabezpečovaní akcie podieľali. Jednalo sa o spoľahlivé osoby z Fakultnej nemocnice Košice, ZŤS a MsNV Košice.   Vzhľadom k tomu, že som uvedenú sumu zaplatil z vlastných finančných prostriedkov, žiadam o jej úhradu.   K záznamu prikladám potvrdenku o zakúpení tovaru."   Chápem to tak, že eštebáci predtým, ako vykonali prehliadku bytu, v ktorom som býval s matkou a so sestrou, využili vybraných pracovníkov Fakultnej nemocnice, Závodov ťažkého strojárstva, môjho zamestnávateľa a Mestského národného výboru, aby nejakým spôsobom odvábili ľudí, ktorí by sa mohli počas prehliadky bytu vyskytovať v blízkosti, samozrejme vrátane mňa. Za tieto služby socialistickej vlasti boli potom pohostení. Nech si majú. Želám im, aby im nikdy nezabehlo! Viem si vysvetliť to tak, že do akcie zapojili spoľahlivých pracovníkov môjho zamestnávateľa i mestského národného výrobu, pod ktorý som spadal, lebo predstavovali predĺženú ruku štátu v starostlivosti o, či do občana. Prečo ale účasť vybraných pracovníkov Fakultnej nemocnice? Ich úlohu si neviem vysvetliť. Napadlo ma však, či ich do akcie nezapojili pre prípad, že by sa celá akcia zvrhla na divokú akčnú scénu hodnú filmov o Jamesovi Bondovi. Aby mal kto ošetriť zákerných a zbabelých nepriateľov, ak by sa predsa len postavili do cesty nebojácnym ochrancom socializmu. U eštebákov by som sa ničomu nedivil.   Analýza, teda domová prehliadka, sa urobila aj byte mojej sestry, ako to vyplýva z plánu prísne tajného Plánu agentúrno-operatívneho rozpracúvania objekta „FILOZOF":   Vykonať v byte sestry Evy objekta STÚ ANALÝZA, kde sa objekt dosť často zdržuje, s cieľom zistenia alebo vylúčenia podozrenia predpokladanej trestnej činnosti, nakoľko vykonanou ANALÝZOU v byte a na pracovisku objekta nebol nájdený fotoaparát, ktorý objekt používa pri turistických cestách a tiež nebola nájdená najnovšia korešpondencia z NSR.   O vykonaní domovej prehliadky v byte sestry som nenašiel v spise nijaký záznam. Prehliadku si sotva mohli odpustiť, len sa asi hanbili za to, že zase nič kontrarozviednej povahy nenašli a nechceli o tom zanechať nijaký záznam. Tá korešpondencia z NSR (Západné Nemecko - Nemecká spolková republika) bola pracovná, lebo v podniku som patril medzi málo jazykovo dostatočne zdatných ľudí, čo vedeli zdvihnúť telefón, poslať list alebo telex pri vybavovaní dodávok zo zahraničia. Písomné materiály som samozrejme odkladal do príslušného spisu na pracovisku, nie doma, ani u mojej sestry. Možno môj vtedajší zamestnávateľ ich ešte stále má vo svojich archívoch, tam si ich oprávnení záujemcovia môžu nájsť. Že sa mi prehrabávali, pardon, vykonávali analýzu v pracovnom stole i spisoch na pracovisku mi môj spis potvrdzuje niekoľkokrát.   V súlade s plánom AOR (agentúrno-operatívneho rozpracovania) v danom prípade bol vykonaný STÚ ANALÝZA na pracovisku objekta, kde boli potvrdené styky na kapitalistické zahraničie. Objekt zo svojho zamestnania mal možnosti nadväzovania stykov s kapitalistickým zahraničím, nakoľko pracoval na úseku dovozu. Iné poznatky alebo veci spravodajského charakteru ANALÝZOU zistené neboli. Objekt v júli 1982 rozviazal pracovný pomer v ZŤS Košice a doposiaľ sa ešte nezamestnal.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Prechádzka po Lutherovom meste Wittenberg
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Slnečný babioletný deň v Drážďanoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Nagymaros-Visegrád v neskoré leto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Na tému Ukrajiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Vrátny 
                                        
                                            Spomienka na Veľkú  vojnu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Vrátny
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Vrátny
            
         
        vratny.blog.sme.sk (rss)
         
                        VIP
                             
     
        V živote som vystriedal viacero bydlísk i povolaní a som teda z každého rožku trošku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    211
                
                
                    Celková karma
                    
                                                5.73
                    
                
                
                    Priemerná čítanosť
                    1476
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Inde v Európe
                        
                     
                                     
                        
                            Ázia
                        
                     
                                     
                        
                            Nepriateľská osoba
                        
                     
                                     
                        
                            Britské ostrovy
                        
                     
                                     
                        
                            Osudy
                        
                     
                                     
                        
                            U nás na Slovensku
                        
                     
                                     
                        
                            U susedov
                        
                     
                                     
                        
                            USA, Kanada
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na tému Ukrajiny
                     
                                                         
                       Čarnogurský: tragédia Putinovho hnedého mužíčka
                     
                                                         
                       Migranti v mojej obývačke
                     
                                                         
                       Sedem dobrých rokov skončilo
                     
                                                         
                       Fragmenty týždňa
                     
                                                         
                       O alkoholikovi, ktorý namaľoval obraz za $ 140 miliónov (a jeho múzeu).
                     
                                                         
                       Slovensko sa opäť zviditelnilo
                     
                                                         
                       Nie, nemusíte jesť špekáčiky zo separátu, vy ich jesť chcete
                     
                                                         
                       Najkrajší primátor najkrajšieho mesta
                     
                                                         
                       Letný pozdrav Sociálnej poisťovni
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




