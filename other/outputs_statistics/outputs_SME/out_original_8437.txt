
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dominik Marko
                                        &gt;
                Nezaradené
                     
                 Skoro zabudnutý 

        
            
                                    9.6.2010
            o
            16:04
                        (upravené
                24.7.2011
                o
                19:47)
                        |
            Karma článku:
                2.73
            |
            Prečítané 
            499-krát
                    
         
     
         
             

                 
                    Chcel by som na tomto mieste predstaviť a ponúknuť čosi z histórie jedného malého komorného kostolíka v Podhradí, o ktorom mnoho rodených Bratislavčanov nemá ani len potuchy. Je to krásny a takmer zabudnutý skvost bratislavského Zuckermandlu.
                 

                 
Priečelie kostolíkaDominik Marko
   Tento barokový kostolík Najsvätejšej Trojice pochádza z 18. storočia a nachádza sa v Podhradí (Zuckermandel), v jadre mesta Bratislava. Na jeho pôvodnom mieste stála kedysi drevená kaplnka, miesto ktorej v rokoch 1734-1738 postavili tento kostolík zachovaný dodnes. Jeho súčasťou je pôvodné bočné krídlo so sakristiou, nad ktorou bola v 19. storočí postavená jednoposchodová farská budova.   Kostol prešiel prestavbou v roku 1957. V jeho interiéri sa nachádzajú sochy, kľačiace plastiky ochrancov pred morovou nákazou sv. Rocha a sv. Šebastiána, ktoré sú z okruhu žiakov sochára Georga Rafaela Donnera a pripomínajú mor z roku 1713, ktorý sa údajne zastavil na mieste dnešného kostolíka. Súčasťou výbavy kostola je i niekoľko vzácnych obrazov, vzácne zdobené liturgické rúcha a rôzne náboženské predmety. Exteriér kostola je zdobený sochami sv. Floriána a sv. Jána Nepomuckého.   Na priečelí fary, na mieste okna na prízemí, umiestnili v roku 1908 pamätnú bronzovú tabuľu pátra Karola Scherza de Vasoja od bratislavského sochára Alojza Rigeleho. Alojz Rigele je aj autorom tabernákula v interiéri kostolíka.   Karol Scherz de Vasoja, narodený 17. januára 1807 v Bratislave, zomrel 19. septembra 1888 tiež v Bratislave, bol od roku 1855 farárom v Zuckermandli. Bol známy svojou dobročinnosťou a ľudomilnosťou. Zachránil niekoľko desiatok ľudí pri požiaroch a povodniach. Zahynul tragicky na dnešnej Špitálskej ulici, kde ho zrazil povoz. K jeho 20. výročiu tragického úmrtia venovali vďační obyvatelia Zuckermandlu pamätnú tabuľu, ktorú odhalil vtedajší zástupca mešťanostu Kumlík.      Pohľad na Zuckermandel z roku 1913, kde je naľavo vidieť farskú budovu kostolíka.      Na dobovej pohľadnici z roku 1915, ktorá zobrazuje súčasnú Žižkovu ulicu, je dobre   vidieť kostolík v jej pozadí.      Pamätná tabuľa na pátra Scherza z pohľadnice z roku 1908, osadená k   jeho 20. výročiu úmrtia. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Zomrel autor najsmutnejšej symfónie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Primitívna antikampaň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Vyliečená mŕtvola
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Diametrálny pohľad na náboženstvo a cirkev cez prizmu Chatrče
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominik Marko 
                                        
                                            Bola Devínska Nová Ves obeťou masakru?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dominik Marko
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dominik Marko
            
         
        dominikmarko.blog.sme.sk (rss)
         
                                     
     
        V podstate som človek z ľudu, teda jeden z mnohých.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    17
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1051
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Winston Groom - Forrest Gump
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Bob Dylan
                                     
                                                                             
                                            Leonard Cohen
                                     
                                                                             
                                            Tom Waits
                                     
                                                                             
                                            Bruce Springsteen
                                     
                                                                             
                                            Lou Reed
                                     
                                                                             
                                            Cat Stevens
                                     
                                                                             
                                            Nick Cave &amp; The Bad Seeds
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Internetové diskusie zaplavili proruskí trollovia, píše Guardian
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




