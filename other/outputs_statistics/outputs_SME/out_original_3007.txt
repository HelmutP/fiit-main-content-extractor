
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Kravčík
                                        &gt;
                www.theglobalcoolingproject.co
                     
                 Nesplachujme Boží dar 

        
            
                                    22.3.2010
            o
            7:57
                        (upravené
                22.3.2010
                o
                10:25)
                        |
            Karma článku:
                9.80
            |
            Prečítané 
            2170-krát
                    
         
     
         
             

                 
                    Dnes (22.marec) si pripomíname Svetový deň vody. Múdri ľudia si ho zapísali do kalendára, aby mobilizovali pospolitý národ planéty Zem, chrániť vodu pre seba, potraviny i prírodu. Bude sa diať množstvo aktivít, ktoré majú napomôcť zvýšeniu zodpovednosti za stav vody na všetkých kontinentoch. Aj na Slovensku je množstvo aktivít na podporu ochrany vôd. Aká je realita tejto starostlivosti v podaní vodohospodárov? Ponúkam zopár poznámok na zamyslenie.
                 

                 Najmasovejšou aktivitou občanov, ktorá prebieha na Slovensku s príležitosti Svetového dňa vody  je čistenie vodných tokov povodia rieky Žitava (www.zitava.sk).  Samozrejme aj zodpovední za stav vody budú prezentovať starostlivosť o vodu na Slovensku XV. celoslovenskou konferenciou ku Svetovému dňu vody pod názvom „Čistá voda pre zdravý svet" (www.enviro.gov.sk) v Holliday Inn v Žiline, aby ukázali, ako zodpovedne sa starajú o toto nenahraditeľné bohatstvo. Zvláštnosťou tohto stretnutia, že to spravili v tých istých priestoroch, v ten istý deň, kde sa malo konať už rok pripravované Višegrád  Komunál Fórum na tému VODA (www.euroedu.sk), čo len potvrdzuje, že vodohospodári nikoho nepotrebujú. Stačí im „lenonový" syndróm v starostlivosti o vodu.   Dážď je tvorcom všetkých vôd, ktoré sa nachádzajú v pôde, v podzemí, v prameňoch, potokoch, riekach i tie ktoré tečú do našej domácnosti z najbližšieho vodného zdroja. Výdatnosť dažďa, jej časové a priestorové rozloženie je zároveň závislé, či sa voda z krajiny vyparí. Ak krajina vysýcha, vysýcha aj dážď. Aj preto zvykneme hovoriť, že dážď je boží dar. Máme však veľké rezervy v starostlivosti o Boží dar. Lebo zo všetkých našich príbytkoch, verejných budov i hotelov splachujeme dažďovú vodu do najbližšej kanalizácie bez úžitku, kde sa zrieďuje s našimi splaškami a odteká na najbližšiu čistiareň odpadových vôd. Po jej vyčistení a v čase povodní nevyčistení vyteká voda do najbližšieho vodného toku a potom do mora.   Paradoxom je, že z našich príbytkov spláchneme bez úžitku viac ako 100 mil. m3 Božieho daru. Minimálne toľko pitnej vody privedieme do našich príbytkov z niekoľko kilometrov vzdialených vodných zdrojov na vypláchnutie našich toaliet. Systém navrhovaný, praktizovaný a spravovaný vodohospodármi, splachovaním dažďovej vody do najbližšieho potoka nie je len veľmi nákladný, ale má na svedomí aj zmenu vodného režimu krajiny, vysušovanie urbannej krajiny, častejšie a extrémnejšie povodne, vysýchanie krajiny, nedostatok vody, zhoršovanie jej kvality i klimatickú zmenu.   Za tieto služby, ktoré nám zvyšuje riziko sucha a extrémy v počasí, v nevedomosti platíme, lebo za odkanalizovanie každého kubíka dažďovej vody sa platí. Tak naši starostliví vodohospodári nám ponúkajú služby, aby sme všetok Boží dar spláchli do ich kanalizačných rúr a zaplatili 1 EURI za každý kubík. Odhadujem, že vodárenské spoločnosti inkasujú viac ako 100 mil. EURI ročne za spláchnutie  Božieho daru do ich kanalizácií.   Tak isto nám ponúkajú vodu z vodovodu, za čo inkasujú viac ako 2 EURI za každý kubík. Mimochodom je to viac ako v Bruseli. Vodohospodári nám taktiež ponúkajú unikátne veľkokapacitné vodné zdroje v podobe vodných nádrží na zásobovanie pitnou vodou, aby sme mohli platiť ešte viac.  Momentálne presviedčajú sedliakov na východnom Slovensku, aby neprekážali pri výstavbe ďalšieho veľkokapacitného vodného zdroja na Tichom Potoku „len" za 332 mil. EURI z verejných zdrojov. Takto si oni predstavujú čistú vodu pre zdravý svet.   To, že je nekultúrne a nechutné likvidovať vidiecke komunity, kvôli budovaniu nových vodných zdrojov za nekresťanské peniaze, aby sa táto voda použila na splachovanie toaliet, to im nevadí. Vodohospodári to považujú to za profesionálnu česť. To, že je to zločin na ľudskosti, za ktorý by sa mali vodohospodári hanbiť im neprekáža. Farizejská ľahostajnosť preberať vznešené motto svetového dňa vody „čistá voda pre zdravý svet" na klamanie verejnosti, že zvyšovať cenu vody je nevyhnutné, že likvidovať vidiecke komunity pre výstavbu nových vodných zdrojov je v ľudské, že splachovanie Božieho daru spolu so splaškami do potokov je kultúre hovorí o hlbokej kríze profesionality vodohospodárskej komunity.   Prial by som si, aby nová generácia mladých vodohospodárov našla dostatok občianskej odvahy, aby pomohla odštartovať novú kultúru pre vodu, aby sa nespreneverila mottu Svetového dňa vody „čistá voda pre zdravý svet" a zmobilizovala sa pre nové inovatívne riešenia získavania vodných zdrojov zvýšenou saturáciou dažďovej vody do krajiny, ktoré nelikvidujú vidiecke komunity, lebo sa to v 21. storočí nepatrí. Prial by som si, aby nová generácia vodohospodárov sa mobilizovala pre nové inovatívne technologické riešenia napríklad využívaním dažďovej vody v mestách pre sanitáciu, či už hotelov, verejných budov i domácnosti, pretože je nekultúrne kanalizovať dažďovú vodu bez úžitku z mesta a zároveň likvidovať vidiecke komunity, aby ustúpili novým veľkokapacitným vodárenským nádržiam pre mestá v ktorých sa ročne splachne milióny kubíkov dažďovej vody.   Je niekoľko inovatívnych riešení využívania dažďovej vody, ktorá by mohla nová generácia vodohospodárov na Slovensku rozvinúť. Jednou zo skromných inovácií je využívanie dažďovej vody na sanitáciu toaliet. Napríklad len pre mesto Prešov a Košice by zavedením technológií využívania dažďovej vody pre sanitáciu toaliet by sa znížila spotreba vodného zdroja o viac ako 400 l/s (60% kapacity navrhovaného vodného zdroja Tichý Potok). Realizáciou inovatívnych riešení využívania dažďovej vody v domácnostiach na celom východe Slovenska by sa ušetrilo viac ako 1000 l/s vodného zdroja. Toto riešenie by bolo lacnejšie, ako navrhovaný rozpočet plánovanej priehrady Tichý Potok a vytvorilo by minimálne 5.000 pracovných príležitostí. Ale to je už o inej kultúre, ktorej vodohospodári na Slovensku zatiaľ nerozumejú     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Poloz na hrad!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Zavšivavená spoločnosť trollami s podporou SME?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Bratislava šiestym najbohatším regiónom EÚ. Západné Slovensko 239-tým…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Nemajú Boha pri sebe!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Kravčík
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Kravčík
            
         
        kravcik.blog.sme.sk (rss)
         
                                     
     
         Presadzujem a podporujem agendu „VODA PRE OZDRAVENIE KLÍMY“. Jej cieľom je posilnenie environmentálnej bezpečnosti prostredníctvom zodpovedného prístupu v ochrane prírodného a teda i kultúrneho dedičstva. Napĺňanie agendy, založenej na prijatí novej, vyššej kultúry vo vzťahu k vode, môže na Slovensku vytvoriť viac ako 100 tisíc a v Európe vyše 5 miliónov pracovných príležitostí. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    629
                
                
                    Celková karma
                    
                                                7.10
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Povodne
                        
                     
                                     
                        
                            Hladujúci potrebujú vodu
                        
                     
                                     
                        
                            Klimatická zmena
                        
                     
                                     
                        
                            VODA zrkadlo kultúry
                        
                     
                                     
                        
                            http://s07.flagcounter.com/mor
                        
                     
                                     
                        
                            Nová vodná paradigma
                        
                     
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            http://moje.hnonline.sk/blog/4
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Kandidáti za poslancov do EP
                                     
                                                                             
                                            Ladislav Vozárik
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            http://www.clim-past.net/2/187/2006/cp-2-187-2006.pdf
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.aktualne.centrum.cz/blogy/jana-hradilkova.php
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hospodarskyklub.sk
                                     
                                                                             
                                            ashoka.org
                                     
                                                                             
                                            bluegold-worldwaterwars.com
                                     
                                                                             
                                            holisticmanagement.org
                                     
                                                                             
                                            theglobalcoolingproject.com
                                     
                                                                             
                                            ludiaavoda.sk
                                     
                                                                             
                                            watergy.de
                                     
                                                                             
                                            waterparadigm.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nemajú Boha pri sebe!
                     
                                                         
                       Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                     
                                                         
                       Ako sa z jednej ochranárskej ikony stala obyčajná nula
                     
                                                         
                       10 rokov po víchrici v Tatrách stále v zákopoch
                     
                                                         
                       Primátor všetkých Košičanov?
                     
                                                         
                       Dnes zasadá Vláda v Ubli
                     
                                                         
                       Aspoň pokus o integráciu Rómov? Za primátora Rašiho? Zabudnite!
                     
                                                         
                       Róbert Fico je bezpečnostným rizikom pre Slovensko
                     
                                                         
                       Kto je zodpovedný za pád starenky do kanalizačnej šachty v Michalovciach
                     
                                                         
                       Zdravé Košice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




