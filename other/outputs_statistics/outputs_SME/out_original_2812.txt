
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Petka
                                        &gt;
                Nezaradené
                     
                 Prezident: Viem, ale nepoviem 

        
            
                                    18.3.2010
            o
            0:07
                        |
            Karma článku:
                12.84
            |
            Prečítané 
            1330-krát
                    
         
     
         
             

                 
                    Viem, ale nepoviem. To povedal niekedy Gašparovičov súputník, a jeho vtedajší vzor: Vladimír Mečiar. Po tom, čo sa ich cesty rozišli, sa mocenské pomery a politické vplyvy diametrálne zmenili. Ale napriek tomu dokázal túto vetu Gašparovič úspešne zopakovať. A súčasne tým veľa povedať.
                 

                 
Gašparovič, FicoSITA, T. Benedikovič, http://hnonline.sk/ekonomika/c1-39019080-f
           Dnes vieme, kto podporoval prezidenta vo voľbách, a naopak, koho on podporuje teraz. „Nestrannosť prezidenta,“ myslenie národne, cítenie sociálne. To sú kecy, na ktoré sme si už museli zvyknúť.        Prezident považuje za normálne nepriame novelizácie zákonov, hoci sú protiústavné. Sám deklaroval, že ich podpisovať nebude. Napriek tomu podpisuje. Pri tom má byť ochrancom Ústavy. Ďakujem pekne za takého ochrancu.       Prezident vie, že či podpíše alebo nepodpíše zákon o vlastenectve. V súčasnej podobe je tento zákon úplným a zbytočným paškvilom. Vie, ale nepovie.        (Nechcem spomínať Sulíkov list prezidentovi ohľadom vyjadrenia sa k vyhláseniu referenda, resp. obrátenia sa Ústavný súd, ktorý v podstate zostal bez odpovede. Tiež vieme, ako by to dopadlo.)       Vo viacerých prípadoch by sa dalo s trochou fantázie hovoriť o porušovaní Ústavy (čo sa týka nepriamych noviel, alebo navodzovaniu „stavu právnej neistoty“).        Najhoršie na tom všetkom je, že v podstate všetko je v súlade so zákonom. Zákon a morálka sú ale dve zásadne odlišné veci. A ako sa dokážu od seba odlišovať nám (nielen) prezident ukazuje snáď stále. Asi bude niečo pravdy na tom, že právnici ešte tomuto štátu nič dobré nepriniesli. Navyše, ešte keď ide o bývalých komunistov.        Prezident by mal byť morálnym vzorom a osobou, ktorú si občania vážia. Ja sa obávam, že už veľmi dávno sme nemali na čele štátu takúto osobu. Žiaľ. Ja osobne si prezidenta, ktorého bolo vidieť na verejnosti od volieb len pri novoročnom prejave, a pár ďalších udalostiach, nevážim ako symbol tohto štátu. Bývalý komunista, človek, ktorý podlieha politickým vplyvom, ktorý rozhodne nie je nadstrannícky. Ani nemám dôvod, prečo by som si ho vlastne vážiť mal.         Rovnako, ako si nevážim vlastenecký zákon, ktorý skončí len ako nástroj politickej moci, lacného populizmu vládnych vrstiev, a ukážka blbosti „národniarov,“ prezlečených socialistov a zlodejov. Či dôjde k jeho podpisu, alebo k jeho vráteniu parlamentu, je v podstate jedno.    Pán prezident, my už vieme. Povedať už netreba, pochopili sme. Celé je to ďalšia skvelá ukážka morálky tých, ktorí nám tu vládnu. Žiaľ.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (8)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Petka 
                                        
                                            Otvorený list MV SR
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Petka 
                                        
                                            Ako v Pravde počítať nevedeli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Petka 
                                        
                                            Takto to je so slovenským športom!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Petka 
                                        
                                            Slota after party... Hmm
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Petka 
                                        
                                            Klamár Fico, "Nezávislá" Pravda
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Petka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Petka
            
         
        petka.blog.sme.sk (rss)
         
                                     
     
        Pravda víťazí








        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    87
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2000
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zvolen
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jozef Červeň
                                     
                                                                             
                                            Vladimír Schwandtner
                                     
                                                                             
                                            Marek Gajdoš
                                     
                                                                             
                                            Sergej Danilov
                                     
                                                                             
                                            Dávid Králik
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




