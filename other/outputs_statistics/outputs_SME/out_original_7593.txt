
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Drotován
                                        &gt;
                Poznámka pod čiarou
                     
                 Česi majú rozum, bude ho mať aj Slovensko? 

        
            
                                    30.5.2010
            o
            12:57
                        |
            Karma článku:
                13.26
            |
            Prečítané 
            2397-krát
                    
         
     
         
             

                 
                    Včerajšie voľby v Českej republike jasne ukázali, že naši susedia odmietajú socialistov/komunistov, a ich spôsob gréckeho riadenia štátu. Česi a Moraváci mali rozum, keď povedali jednoznačné NIE Paroubkovi, komunistom, sľubovaniu nesplniteľného, strašeniu "modrou horúčkou" a rozkrádaniu verejného majetku. Budú mať o dva týždne rozum aj obyvatelia Slovenskej republiky?
                 

                 
  
   Veľký kamarát Paroubka, padlého anjela českej sociálnej demokracie (toho času čerstvá politická mŕtvola), Robert Fico sa snaží opäť drať k moci podobným spôsobom ako ČSSD. Brnkaním na strunu najnižších vášni voličov (maďarska karta vs. české strašenie slovenskými reformami), neustálym klamaním a zatĺkaním (napr. Róbert Kaliňák vs. Bohuslav Sobotka), agresívnou nervozitou (Fico vs. Paroubek), sľubovaním trinásteho dôchodku na dlh (obe strany), sľubovaním lacných liekov (obe strany), sľubovaním nesplniteľného (obe strany), fiktívnym tvrdením o útoční všetkých na sociálnu demokraciu (obe strany) atď.   Kampaň strany Smer-SD je založená na klamaní, strašení a zatĺkaní.   Voliči v Českej republike jasne ukázali, že majú dostatok zdravého rozumu. Budú mať dostatok rozumu o dva týždne aj občania Slovenskej republiky? Alebo budú musieť ľudia rozmýšľať nad emigráciou napríklad aj k našim západným susedom? Rozhodnutie je na každom z nás. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (71)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Odpočet práce poslanca v Rači 2010-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Politika bez kšeftov a tajných dohôd
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Otvorený list primátorovi vo veci Železnej studienky a jej záchrany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Bude primátor Ftáčnik chlap alebo handrová bábika?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Drotován 
                                        
                                            Ohlásenie vstupu do prvej línie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Drotován
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Drotován
            
         
        drotovan.blog.sme.sk (rss)
         
                                     
     
        Blogger na SME 2005-2014, nominovaný na Novinársku cenu 2012. Aktuálne som presunul blogovanie na http://projektn.sk/autor/drotovan/  
Viac o mne na: www.drotovan.webs.com 
 
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    224
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4323
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Poznámka pod čiarou
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Bratislava - Rača
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Mesto Bratislava
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Môj blog na Račan.sk
                                     
                                                                             
                                            Môj blog- Hospodárske noviny
                                     
                                                                             
                                            Klasický ekonomický liberalizmus ako spojenec konzervativizmu
                                     
                                                                             
                                            Salón
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            KIosk
                                     
                                                                             
                                            Knihožrútov blog
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                             
                                            Ondrej Dostál
                                     
                                                                             
                                            Ivo Nesrovnal
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Cyklodoprava
                                     
                                                                             
                                            Bratislava - Rača
                                     
                                                                             
                                            Project Syndicate
                                     
                                                                             
                                            Bjorn Lomborg
                                     
                                                                             
                                            Leblog
                                     
                                                                             
                                            Cyklokoalícia
                                     
                                                                             
                                            Anarchy.com
                                     
                                                                             
                                            EUPortal.cz
                                     
                                                                             
                                            Political humor
                                     
                                                                             
                                            Pravé spektrum
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




