
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Gajdoš
                                        &gt;
                Z práce
                     
                 Môj život 

        
            
                                    4.2.2010
            o
            20:24
                        |
            Karma článku:
                23.86
            |
            Prečítané 
            13855-krát
                    
         
     
         
             

                 
                    Keď som mojej mame (zdravotnej sestričke) oznámil v poslednom ročníku gymnázia, že pôjdem na medicínu, jej reakciou bolo, že som sa zbláznil. Ja sám som nedokázal vysvetliť (ani sebe samému), čo ma k tomu viedlo. Dnes už to viem. Neviem to presne pomenovať, ale stačí, že to v danú chvíľu cítim.
                 

                 - Keď ma môj pacient?, nie on už nie je mojím pacientom. Je mojím kamarátom. Ja som jeho veľkým Marekom. Nie doktorom... Ani nevie ako sa z neho teším...a čo mi tým objatím dáva.   Rozkáže: "Papuče!" Chytí ma za ruku a brázdime chodbou. Ja ho držím za palec a on ma ťahá. Rozpráva, čím ďalej tým viac...a veľa sa pýta... Vezmeme aj Katku (kolegyňa) a napchávame sa lentilkami a smejeme sa do prasknutia nad... nad všetkým...       - Keď v službe oznamujem matke, že to bohužiaľ nevyzerá dobre. Ona ma prosí, aby sme ho pokrstili. Je pred vianocami. Ona nasadne do auta a 500 km preletí do noci k synovi. My bereme striekačku an malé čielko v inkubátore kvapkáme vodu v mene...   A po necelých dvoch mesiacoch odchádzajú domov tých istých x kilometrov.  Len s úplne iným nákladom emócií... Mamička plače, už zase, ale tentokrát od šťastia. A malý sa na nás usmieva, ako keby si z nás vtedy len srandu urobil. Všetky sestričky sa predbiehajú, ktorá ho ešte naposledy postíska a zavrú sa za nimi dvere...       - Keď sa dotýkam, ako dnes, hranice života. Snažím sa katétrom s priemerom menším ako milimeter trafiť do cievy. S veľkým rešpektom a jemnosťou im pomáhame na tomto svete.       Keď dávame šancu...životu...       Vtedy viem, prečo... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Gajdoš 
                                        
                                            17. november - deň najmenších bojovníkov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Gajdoš 
                                        
                                            Za oknom, alebo o dúhe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Gajdoš 
                                        
                                            Sviatočné klokankovanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Gajdoš 
                                        
                                            Aj po mnohých rokoch...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Gajdoš 
                                        
                                            Človečina...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Gajdoš
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Gajdoš
            
         
        marekgajdos.blog.sme.sk (rss)
         
                        VIP
                             
     
        Lekár na novorodeneckom oddelení.






        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    88
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6065
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            S úsmevom
                        
                     
                                     
                        
                            Čo Vy na to?
                        
                     
                                     
                        
                            Fotečky
                        
                     
                                     
                        
                            Rozprávky-nerozprávky
                        
                     
                                     
                        
                            Tatry
                        
                     
                                     
                        
                            Praha
                        
                     
                                     
                        
                            Poézia?
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Z práce
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            novorodenecke odd
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




