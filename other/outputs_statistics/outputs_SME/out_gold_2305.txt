
 Obzrela som sa a videla som niečo krásne. 
Z auta na mňa kývali dvaja mladí usmiati manželia – moji známi.
Ona ešte unavená a ubolená z pôrodu ale v očiach mala radosť a šťastie a On hrdý usmiaty galantne pomáhal vystupovať manželke z auta.
Hrdo si niesli v maličkej autosedačke svoj nádherný vzácny poklad, ktorý zababušený nežne spinkal.
Bola som veľmi potešená, že som ako prvá mohla vidieť ich maličký batôžtek šťastia.
Na chvíľku sme sa stretli a povedali si zopár slov a okolo nás pobehovali ešte ich dve malé krásne princezné, ktoré sa tešili zo svojho maličkého bračeka.

Bol na nich naozaj krásny pohľad a veľmi som sa z nich tešila, deň bol hneď krajší a veselší.
 
