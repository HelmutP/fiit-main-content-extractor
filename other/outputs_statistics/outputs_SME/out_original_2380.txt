
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Robert Mihály
                                        &gt;
                Nezaradené
                     
                 Pochod homosexuálov v Bratislave alebo Slováci v pupku homofóbie. 

        
            
                                    26.2.2010
            o
            0:25
                        (upravené
                28.2.2010
                o
                14:51)
                        |
            Karma článku:
                16.80
            |
            Prečítané 
            4497-krát
                    
         
     
         
             

                 
                    Keď pred takmer mesiacom vyšla informácia o plánovanom májovom pochode (pride) homosexuálov v Bratislave, okamžite sa voči tomu zdvihla vlna diskusie, no najmä vlna protestov, urážok či dokonca vyhrážok. Iba na sociálnej sieti facebook vznikli prinajmenšom 4 skupiny, vyzývajúce k zabráneniu tomuto pochodu s počtom podporovateľov okolo 6000 ľuďí a to ani nespomínam diskusie s homofóbnym obsahom na mnohých internetových verziách printových médií.
                 

                 
  
   „Láska je silnejšia než čokolvek"   Kým napríklad ľuďia vo Švédsku ľudské práva neheterosexuálov  chápu a podporujú, na Slovensku to očakávať z viacerých dôvodov nemôžme. Tak isto ako tu nemôžme očakávať, že by bratislavský pride podporili najvyšší štátni činitelia. S heslom „Láska je silnejšia ako čokoľvek!“ podporili protest v Štokholme vo Švédsku pred tromi rokmi i zástupcovia luteránskej cirkvi, slovenskí cirkevní predstavitelia by naopak neheterosexuálov najradšej ukameňovali v "mene Božom". Osočovanie homosexuálov  katolíckou cirkvou na Slovensku, ale nie je ničím výnimočným, tak ako je k tejto otázke jednoznačný i postoj Vatikánu. Je to veľmi nebezpečné, najmä ak zoberieme do úvahy fakt, aký veľký vplyv na verejnú mienku veriacich má táto inštitúcia katolicizmu na Slovensku a Vatikán vo svete. Ak katolícka cirkev diktuje či vštepuje priamo či nepriamo veriaciam ich názor, väčšina ľudí na Slovensku je potom jasne názorovo nesvojprávna. Okrem toho sa k tejto téme okrem jednotlivcov začína vyjadrovať i extrémna pravica najmä z radov okolo "pohrobkov slovakoštátu", pre ktorú sú typické názory o homosexualite ako jave, ktorý nemá nárok na existenciu v spoločnosti. Práve tohto sa obávam najviac. Mám obrovské obavy aby práve toto hnutie nevyužilo svoje demagogické a propagindistické metódy na zburcovanie fyzickej sily proti plánovanej akcii. Podľa môjho názoru, i z viacerých diskusných príspevkov na internete je to možné. A ak by sa to napokon i stalo realitou, bol by to nepredstaviteľný vrchol stupidity, ktorý nemožno tolerovať!   Na začiatok musím povedať, že nie som homosexuál,   No pochod homosexuálov v Bratislave jednoznačne podporujem a to najmä z jedného dôvodu: Kto rozhoduje ktorá láska je správna, a ktorá láska má byť zakázaná či utajovaná? Prečo by sa mal človek, ľudská bytosť, schovávať a tajiť svoje presvedčenie, sexuálnu orientaciu či názory iba kvôli ľuďom, ktorí nevedia prekusnúť to, že na tejto planéte nie je všetko jednotvárne, to že všetci sú odlišní a všetci by si mali byť rovní (zdôrazňujem MALI byť rovní). Prečo by mal mať nehoterosexuál iné možnosti a práva ako heterosexuál?  A najmä prečo sa musí slovenská verejnosť opäť prejaviť ako homofóbna a nenávistná? Aký dôvod majú ľuďia, ktorí žiadajú zákaz pochodu? Homosexuáli na pochode v Bratislave predsa nechcú vyzývať k porušovaniu práv iných a ani ich neporušujú. Naopak dožadujú sa vlastných, ktoré sú im upierané! Na Slovensku je zvykom o veciach mlčať a nediskutovať, potom sa nesmieme čudovať, že ľuďia nerozumejú problémom do hĺbky, ale iba povrchovo. Práve preto je nutné aby postupne všetci vyšli z ticha. Dosť bolo ticha.   Pomýlené myslenie.   Homosexuáli nemôžu mať deti lebo by z nich vychovali homosexuálov. A heterosexuálni rodičia tiež vychovali zo svojho dieťaťa homosexuála? Nemali by sme zakázať rodičovstvo pre istotu vôbec? (:) Homosexuáli sú zvrátení lebo praktizujú análny sex. 65% sexuálne aktívnych heterosexuálnych mužov vyžaduje od partneriek análny sex. Majú i oni teda "zvrátené homosexuálne sklony"? Ak má dieťa dvoch otcov (dve matky) tak trpí. Nie je najdôležitejšie aby rodičia svoje dieťa milovali? Je prijatelnejší fakt, že 1/4 detí sa rodí slobodným matkám a fakt, že narastá počet rozvodov? Homosexuálita - no dobre, ale nech si s tým zostanú ticho doma. Keby vám niekto upieral práva zostali by ste ticho doma alebo by ste chceli prehovoriť?   Obyvatelia vyspelých krajín homofóbov odsudzujú, na Slovensku sú to práve homofóbici, ktorí rozhodujú. Práve toto potom svedčí o našej krajine a o ľuďoch, a práve to je potom smutné! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (130)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Gratulácia i kritika Lajčáka ohľadom obchodu so zbraňami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Slovensko zrejme porušilo zbrojné embargo voči Číne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Protest počas medzinárodnej bezpečnostnej konferencie v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Partnerom GLOBSEC 2013 je zbrojárska firma známa korupciou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Hlinkov prostredník pred Ústavom pamäti národa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Robert Mihály
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Robert Mihály
            
         
        robertmihaly.blog.sme.sk (rss)
         
                                     
     
        Som človek, ktorý sa snaží hľadieť svojimi očami. Som človek, ktorý navôkol vidí iba ďalších ľudí, nikoho viac a nikoho menej.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    82
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2006
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Indiáni v Izraeli
                     
                                                         
                       Mier nie je extrém
                     
                                                         
                       Gratulácia i kritika Lajčáka ohľadom obchodu so zbraňami
                     
                                                         
                       Slovensko zrejme porušilo zbrojné embargo voči Číne
                     
                                                         
                       Protest počas medzinárodnej bezpečnostnej konferencie v Bratislave
                     
                                                         
                       Partnerom GLOBSEC 2013 je zbrojárska firma známa korupciou
                     
                                                         
                       Hlinkov prostredník pred Ústavom pamäti národa
                     
                                                         
                       Kontraples pred operou!
                     
                                                         
                       Za rozdávanie jedla zdarma sú fyzicky napádaní!
                     
                                                         
                       Vycvičme si SS-ákov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




