
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Brejčák
                                        &gt;
                ostatné
                     
                 Ako je to s Čínskym múrom 

        
            
                                    22.3.2010
            o
            17:40
                        (upravené
                7.2.2011
                o
                22:15)
                        |
            Karma článku:
                5.69
            |
            Prečítané 
            527-krát
                    
         
     
         
             

                 
                    Istá legenda, ktorú sme už iste všetci počuli, hovorí o tom, že Čínsky múr je viditeľný z vesmíru. Je to naozaj pravda? To záleží od toho, ako sa pozeráme.
                 

                 S touto legendou prišiel istý Richard Halliburton v roku 1938. Tvrdil, že Čínsky múr je jediným ľudským výtvorom viditeľným z Mesiaca voľným okom. Nevieme ako na to prišiel, keďže ľudia sa na mesiac dostali až v roku 1969.   V dnešnej dobe je už z vesmíru viditeľné prakticky všetko, čo je na zemskom povrchu. Či už je to moja bytovka alebo niekoho dom, s pomocou digitálnej techniky nie je problém nájsť čokoľvek. Takže ak je pekné počasie, s teleskopmi je jednoduché tento múr zazrieť.   Tvrdenie pána Halliburtona je samozrejme zlé. Čínsky múr nie je, bez použitia moderných prístrojov, viditeľný. Veď ako by len mohol? Áno, je dlhý 6700 km, ale taká transsibírska magistrála má takmer 9300 km a nikto o jej viditeľnosti z vesmíru nič nehovorí. Je jasné, že v tomto prípadne na dĺžke nezáleží – inak by sme z vesmíru videli tisícky kilometrov rôznych diaľnic či železníc.  Venovali sme sa dĺžke, ale čo šírka? Čínsky múr je široký v rozpätí od 5 do 9 metrov. Navyše, keď si porovnáte jeho farbu (stavaný je z kameňov a hlinených tehál) s okolitým porastom, ako sú stromy a kry, nie je možné, aby ho niekto zazrel z diaľky 384 400 km, čo je priemerná vzdialenosť Mesiaca od Zeme. Je vidieť, že si na tomto mýte pán Halliburton nedal veľmi záležať.   Ak by ste neverili týmto faktom, môžete sa opýtať aj Yanga Liweia, čínskeho kozmonauta, ktorý tvrdí, že múr pri pozorovaní nezbadal. A to bol pri Zemi o niekoľko tisíc kilometrov bližšie, ako je Mesiac.  Ktorý výtvor človeka je teda viditeľný z vesmíru voľným okom? Na túto otázku vám už neviem odpovedať, keďže som vo vesmíre nikdy nebol. S najväčšou pravdepodobnosťou to bude nejaké jazero, ktoré vzniklo po stavbe niektorej z obrovských priehrad, možno nejaké prieplavy. Ak sa raz do vesmíru dostanem, napíšem. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Prečo je zdanenie bohatých hlúposť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Fico v ženskom šate?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Rímsky Panteón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Veda vs diaľnice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Brejčák 
                                        
                                            Aká má byť STV?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Brejčák
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Brejčák
            
         
        brejcak.blog.sme.sk (rss)
         
                                     
     
        Ocením konštruktívnu kritiku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1017
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            názory
                        
                     
                                     
                        
                            fotky
                        
                     
                                     
                        
                            ostatné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            brejky.deviantart.com
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




