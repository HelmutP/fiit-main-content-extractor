
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Bunčák
                                        &gt;
                Politika
                     
                 Cirkev v predvolebnom programe SaS 

        
            
                                    24.2.2010
            o
            10:41
                        (upravené
                24.2.2010
                o
                10:48)
                        |
            Karma článku:
                13.94
            |
            Prečítané 
            3462-krát
                    
         
     
         
             

                 
                    Každý volič sa rozhoduje na základe svojich vlastných priorít a kritérií, ktorému politickému subjektu odovzdá vo voľbách svoj hlas. Pre mňa osobne je vo voľbách jedným z hlavných kritérií program politických strán pre oblasť vzťahu cirkvi a štátu.
                 

                 Cirkev sa spomína hneď v úvode predvolebného programu SaS: "Ak nebudete dávať dobrý pozor, ani si nevšimnete kde všade vám je uberaná sloboda. Nevšimnete si, že ste nútený podporovať zo svojich peňazí náboženské inštitúcie, ktorých nielen že nie ste členom, ale ktoré majú program opačný vášmu svetonázoru." Nemôžem sa ubrániť dojmu, že SaS si v tomto prípade vybrala zo všetkých možných terčov ten najvďačnejší. Veď okrem cirkví sa jej ako fackovací panák ponúkajú aj športové organizácie, kultúrne inštitúcie a iné, ktorých drvivá občanov nie je členom a napriek tomu ich "nútene" podporuje. Pacifisti svojimi daňami podporujú taktiež armádu i políciu, občania bez televíznych prijímačov "nezávislo" provládnu STV atď. Zákon č. 218/1949 Zb. o hospodárskom zabezpečení cirkví a náboženských spoločností je však tŕňom v oku mnohých celkom oprávnene. Cirkvi svojho času ostro protestovali proti prijatiu tohto zákona. Odmietali ho katolíci aj evanjelici. Je neuveriteľné, že zostal v platnosti dodnes. Zobral cirkvám ich finančnú sebestačnosť a nezávislosť. To preto, aby ich komunistický štát mohol lepšie kontrolovať a mal o ich činnosti podrobný prehľad. Dnes platnosť tohto zákona mnohí chápu tak, že štát je pre cirkvi dojná krava a podobne. Osobne s týmto názorom nesúhlasím. Bol by som však rád, keby zákon o hospodárskom zabezpečení cirkví a náboženských spoločností nikdy prijatý nebol. Program SaS v bode 94 navrhuje finančnú odluku cirkví od štátu. Tento návrh síce podporujem, ale tvrdenie, že sa tak odstráni nespravodlivý prístup k rôznym skupinám platiteľov daní, nie je pravdivé. Konkrétny model financovania cirkví navrhovaný SaS zavedením odvodového bonusu z dobrovoľných príspevkov vo výške 1 % z príjmov fyzických osôb (1% základu dane je podľa SaS vo väčšine prípadov približne desaťktrát viac ako asignácia 2% z dane z príjmu), nech posúdia ekonómovia.  V 74. bode svojho programu SaS navrhuje "vyňať vyučovanie náboženstva zo zoznamu povinne voliteľných predmetov a v rámci odluky cirkví od štátu dať školám možnosť slobodného výberu svetonázorového zamerania ... pričom nebude zvýhodňovaná žiadna svetonázorová doktrína a nebude znevýhodňovaný žiadny zriaďovateľ školy." Nie som právnik, ale nemôžem sa ubrániť dojmu, že v tomto bode svojho programu ide SaS celkom nepochopiteľne nad rámec platnej Ústavy Slovenskej republiky. Ako môže laický štát, ideologicky a nábožensky neutrálny, ktorý sa neviaže na nijakú ideológiu ani náboženstvo, dať štátnym školám možnosť slobodného výberu svetonázorového zamerania? Alebo majú štátne školy nahradiť školy cirkevné? Ak sa všetky školy v jednom meste alebo obci rozhodnú pre rovnaké svetonázorové zameranie, čo si počnú rodičia detí iného svetonázoru? Chvályhodný je však návrh SaS o zrovnoprávnení škôl rovnakého typu bez ohľadu na to, kto je ich zriaďovateľom.  V 92. bode programu SaS sa hovorí: "Pre páry rovnakého pohlavia zaviesť nový právny inštitút životného partnerstva ... Cirkevnú formu uzatvárania životného partnerstva navrhujeme ako prípustnú, závislú od dobrovoľného rozhodnutia registrovanej cirkvi takúto službu poskytovať." Programový prienik SaS v tejto otázke so stranou SMER je veľavravný. Korešponduje s inými konkrétnymi mediálnymi vyjadreniami a rozhodnutiami predstaviteľov SaS. Nepoznám žiadnu registrovanú cirkev či náboženskú spoločnosť v našej krajine, ktorá by takúto iniciatívu bola ochotná podporiť.  V 94. bode svojho programu SaS navrhuje: "Duchovných v ozbrojených silách zamestnávať len pre zahraničné misie." Nerozumiem, prečo by mal byť duchovný zamestnancom Armády SR len vtedy, keď je súčasťou zahraničnej misie. Neviem si predstaviť, ako by sa niečo také dalo realizovať v praxi. Návrh mi síce pripomína dánsky model spred niekoľkých rokov (snáď stále platí), ale keďže SaS túto svoju predstavu bližšie nešpecifikuje, zrejme sama v tejto veci ešte nemá celkom jasno.  Legalizácia marihuany priamo s cirkvou ako takou síce nesúvisí, ale spolu s vyššie uvedenými výhradami ma nabáda v parlamentných voľbách SaS radšej nepodporiť a odovzdať svoj hlas inej strane z pravého politického spektra. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (296)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Bunčák 
                                        
                                            O (od)počúvaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Bunčák 
                                        
                                            K pôsobeniu cirkví v rómskych osadách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Bunčák 
                                        
                                            Karikatúra alebo lož?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Bunčák 
                                        
                                            O hyenách medzi nami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Bunčák 
                                        
                                            Nenávistný nácko a dúhový omyl
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Bunčák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Bunčák
            
         
        janbuncak.blog.sme.sk (rss)
         
                                     
     
        Evanjelický farár - v súčasnosti žije a pôsobí v Trenčíne.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2273
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cirkev
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Ján Bunčák
                                     
                                                                             
                                            ECAV Trenčín
                                     
                                                                             
                                            ECAV na Slovensku
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




