
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miloš Sipták
                                        &gt;
                Mix
                     
                 Hrdinský zápisník 

        
            
                                    8.10.2008
            o
            23:33
                        |
            Karma článku:
                11.10
            |
            Prečítané 
            5308-krát
                    
         
     
         
             

                 
                    Miloval som ju a ona sa vrátila. Trošku iná, ale predsa je to tá moja - obľúbená kniha.
                 

                  Mama bola práve v pôrodnici s mladším bratom a ja som na Vianoce roku 1980 bol u svojej starej mamy. Naozaj nezabudnuteľná spomienka, trošku som bol smutný ale okrem bohatej nádielky snehu mi Vianoce spríjemnil aj darček "od Ježiška" malá knižka od Kláry Jarunkovej – Hrdinský zápisník. Prečítal som ju pravdepodobne dosť rýchlo ale knižku som si natoľko obľúbil, že som sa k nej vracal naozaj často. Roky plynuli, ja som vyrástol a knižku zdedil môj brat. Pri sťahovaní sa nám knižka záhadne stratila, je pravdou, že mi nejako extra nechýbala ale len do doby, keď náš syn dosiahol  školský vek a sám rád číta knihy. Vlastne až teraz som pocítil, že sa moja obľúbená knižka stratila. Po opätovnom, márnom, fyzickom hľadaní  som skúsil "pán GOOGLA" výsledok bol zarážajúci. Knižka vyšla myslím naposledy v roku 1980 a od Kláry Jarunkovej sa dá momentálne  kúpiť len "Tulák". Skúsil som to najskôr u priateľov a známych, či sa im doma náhodu táto knižka nepovaľuje. Keď som neuspel, podal som si inzerát. Po dlhom čakaní som sa dočkal troch odpovedí, žiaľ knižku mi neposlal nikto. Už som ani nedúfal, že sa s ňou  niekedy stretnem, keď mi v nedeľu na rodinnej prechádzke zazvoní  telefón, či ešte potrebujem tú knižku. S pani alebo slečnou som sa dohovoril na sume a doručení, aj keď som nedúfal, že to zrealizuje. Za dva dni mi poštárka dáva do rúk zásielku a ja neverím vlastním očiam a ešte sa pýtam, či je to na dobierku, ale poštárka vraví, že nie. Po rozbalení leží na stole vysnívaná kniha a list s prosbou o úhradu dohodnutej sumy. Platbu som samozrejme realizoval ihneď a od neuveriteľnej radosti poslal aj SMS s poďakovaním. Každopádne ešte raz ďakujem aj prostredníctvom môjho článku. Knihu som prečítal za necelú hodinu a teraz bude rad na mojom synovi. Dúfam, že sa mu bude páčiť. Aj keď už teraz viem, že mu budem musieť vysvetliť pojmy : Súdružka učiteľka, pionier, iskra, ČSSR, Kčs, Mesiac československo-sovietského priateľstva, pioniersky tábor a podobné slová,  čo pri knihe napísanej v roku 1960 nie je predsa nič výnimočné. Ešte musím dodať, že knižku ilustroval Miroslav Cipár. Ak náhodou tento článok číta niekto, kto by mohol  realizovať ďalšie vydanie knihy, prosím skúste si ju prečítať celú, stojí to zato, tie socialistické pojmy by sa už nejako vysvetlili na konci knižky.     Klára Jarunková napísala knižku v roku 1960, jej prvé vydanie bolo o rok neskôr. V tom istom roku jej za knižku udelili cenu výboru Slovenského literárneho fondu. Klára Jarunková sa niekoľkokrát stala laureátkou viacerých medzinárodných ocenení. Jej knihy vyšli v 39 jazykoch sveta, vo vyše 150 vydaniach. Zomrela po ťažkej chorobe v pondelok 11. júla 2005.     Aby tým, ktorý sa s knižkou nestretli nebolo ľúto, dovolím si malý úryvok.       A šli sme zase na povalu do kabíny, lebo Mišo povedal, že teraz už presne vie , čo a ako. Aj vedel a povedal:  - Naplánoval som inakšiu cestu, na Mesiaci sa zastavíme len na jeden-dva dni a hneď pôjdeme, ďalej na Mars. Ja o Marse všetko viem a tú knihu mám so sebou, lebo musím aj teba pripraviť, aby si potom nemal hanbu. Potom sme pomocou drôtu namontovali v kabíne loveckú baterku a študovali sme celý deň. Večer Mišo spísal všetky body, lebo ich musíme poslať do geofyzikálneho roku, aby podľa toho stavali kozmický koráb. Tie body sú:     1.  Na Marse je jar, leto , jeseň, zima ako tu. My by sme chceli prísť v zime, keď sú na póloch snehové čiapky, lebo sa radi lyžujeme. Zobrať 2 lyže a 1 sane.  2.   Po zime príde jar, aj vtedy tam chceme byť, lebo príroda sa prebúdza a vtáčiky spievajú a na jar sa sejú rastlinky a my tam založíme mičurinské políčko, aby videli. Zobrať 1 rýľ, 1 motyku, 1 hrable, 50 balíčkov pozemských semien (aj kvety) a 1 vrece umelého hnojiva.  3.  Na Marse sú veľké pieskové sahary, v ktorých budeme bádať a chodiť sem i tam. Zobrať 1 ťavu (dvojmiestnu).  4.  Sú tam vodnaté kanály. Veda to nevie isto ale tak to vyzerá. Zobrať  1 čln, 2 veslá, 2 plavky.  5.  Na Marse sú bytosti, a keď sú tam bytosti tak sú tam aj pionieri. Zobrať dary pre jeden pioniersky oddiel, dievčence bábiky v krojoch a chlapci pozemské zvieratá z dreva.     Keď sme boli hotoví, vystúpil sme z truhlice a okienkom sme hľadeli ne nebo, aby sme videli Mars, lebo bol večer. Ja som povedal:  - A možno sa takto dívajú na nás Marťania a možno sa dívajú na tú Sovietsku raketu a videli, skade vyletela a čudujú sa.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miloš Sipták 
                                        
                                            Nech sa páči, mal som na srdci......
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miloš Sipták 
                                        
                                            Štátny smútok?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miloš Sipták 
                                        
                                            Vajcami na Fica!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miloš Sipták 
                                        
                                            Potrebuje únia svoj parlament?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miloš Sipták 
                                        
                                            TEST - List politickým stranám
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miloš Sipták
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miloš Sipták
            
         
        siptak.blog.sme.sk (rss)
         
                                     
     
        Stále aktívny muzikant pracujúci v "nemuzikantskej" sfére s pozitívnym pohľadom na svet a s negatívnymi názormi na ľudí, ktorí nám to kazia.
 
 
 




        
      
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    49
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1922
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Mix
                        
                     
                                     
                        
                            Music
                        
                     
                                     
                        
                            Story
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Inzercia
                        
                     
                                     
                        
                            Technic
                        
                     
                                     
                        
                            Economic
                        
                     
                                     
                        
                            Sedem zbytočných
                        
                     
                                     
                        
                            Moje Československo
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Když bolševici zrušili vánoce
                                     
                                                                             
                                            Bledomodrý svet Júliusa Satinského
                                     
                                                                             
                                            Odklínanie
                                     
                                                                             
                                            A Christmas Carol
                                     
                                                                             
                                            Cubase SX
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Mraz Jason-WE SING.WE DANCE.WE STEAL THIN
                                     
                                                                             
                                            James Blunt - All The Lost Souls
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Nataša Holinová
                                     
                                                                             
                                            Ondrej Dostál
                                     
                                                                             
                                            Gabriel Šípoš
                                     
                                                                             
                                            Marek Gajdoš
                                     
                                                                             
                                            Miro Veselý
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            MacBlog
                                     
                                                                             
                                            Nooby
                                     
                                                                             
                                            eTrend.sk
                                     
                                                                             
                                            Online kontrola
                                     
                                                                             
                                            midimusic.cz
                                     
                                                                             
                                            Midi.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Primátor Bratislavy sa najradšej pýši cudzím perím
                     
                                                         
                       Kde je to trestné oznámenie na Kisku?
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       O čom sa písalo pred 100 rokmi
                     
                                                         
                       Prečo budem voliť A. Kisku
                     
                                                         
                       Sociálne byty socialistom!
                     
                                                         
                       Delší řeč proti socializmu
                     
                                                         
                       Nech sa páči, mal som na srdci......
                     
                                                         
                       Volebný denník: Keď ideológia porazí zdravý rozum
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




