
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivona Mertusová
                                        &gt;
                lukostreľba
                     
                 Lukostreľba II. - Luky a príslušenstvo 

        
            
                                    15.1.2009
            o
            13:02
                        |
            Karma článku:
                10.58
            |
            Prečítané 
            6279-krát
                    
         
     
         
             

                 
                    Základnou výbavou lukostrelca je luk. Nikdy si ho ale nevyberajte sami, nemuseli by ste si vybrať dobre. Dôležité je poradiť sa o výbere s odborníkom, najlepšie s nejakým skúseným trénerom v najbližšom klube. A je jedno, či chcete strieľať doma v záhrade alebo sa chystáte stať členom klubu, či kupujete luk pre dieťa alebo pre seba.
                 

                 
 web
   Na začiatku sa treba rozhodnúť, či sa chcete venovať športovej alebo len záujmovej streľbe. Od toho sa potom odvíja samotný výber. Pri športovej streľbe sa požívajú olympijské luky alebo kladkové luky (compound) so zameriavačom, pri záujmovej a terénnej lukostreľbe väčšinou tradičné luky bez zameriavača. K tradičným lukom patrí napr. anglický longbow a staromaďarský luk .     Niekoľko rád pre výber luku a príslušenstva:     1. Luky sú svojou konštrukciou určené buď pre praváka (šíp sa kladie na základku z ľavej strany, lukostrelec drží luk v ľavej ruke, tetivu naťahuje pravou rukou) alebo pre ľaváka (lukostrelec drží luk pravou rukou, tetivu naťahuje ľavou rukou). Pravák mieri pravým okom, ľavák ľavým.     2. Nekupujte si zbytočne silný luk, pretože sa na ňom nedá naučiť technika streľby a zbytočne sa vysilíte.     3. Všetky luky mimo tradičných je vhodné doplniť zameriavačom (sight), základkou pre šípy (arrow rest), opierkou (button), klapačkou (clicker) a stabilizátormi (stabilizers).     4. Ďalšími nutnými doplnkami pre lukostrelca sú chránič predlaktia (armguard), chránič hrude (chestguard), chránič prstov (tab), ktoré držia tetivu, tulec na šípy (quiver) a stojan na luk.     5. Luk a príslušenstvo najlepšie ochráni plastový kufrík alebo taška .     6. Výber šípov takisto patrí do rúk odborníka. Každý luk podľa napínacej sily a dĺžky šípov potrebuje iný parameter (priemer, hrúbka steny, hmotnosť, kvalita materiálu) šípu. Parameter je napísaný na šípe (číselná hodnota) a funguje rovnako ako kaliber nábojov pre iné zbrane. Všetky ďalšie dokupované šípy musia mať rovnaký parameter a dĺžku. Šípom sa budeme venovať v samostatnom článku.     7. Vždy strieľame do vhodných terčovníc a dbáme na zvýšenú bezpečnosť, najmä ak máme terčovnicu umiestnenú v záhrade. Je vhodné použiť aj záchytnú sieť za terčovnicou pre prípad, že niektorý šíp minie terčovnicu.     8. Aj detská hračka s napínacou silou 1 kg môže byť smrtiacou zbraňou. Nikdy nenechávame malé deti strieľať bez dozoru.     9. Nekupujte luky, akokoľvek lacné, bez dostatočne zaisteného servisu, prísunu spotrebného materiálu a riadneho návodu na používanie.     10. Vždy je vhodné absolvovať základný kurz v lukostreleckom klube alebo s trénerom. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Rýchly obed - rajčinová polievka a syrovo - cibuľový sendvič
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Lukostreľba V. - užitočné stránky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Patchworková výstava v Záhorskej Bystrici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Hľadáte originálny darček?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivona Mertusová 
                                        
                                            Patchwork v Dunajskej Strede
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivona Mertusová
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivona Mertusová
            
         
        ivonamertusova.blog.sme.sk (rss)
         
                                     
     
        Milovníčka škótskych teriérov a nevyliečiteľná gurmánka. Okrem lukostreľby a biliardu sa venuje aj cudzím jazykom a patchworku.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    53
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1901
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            škótsky teriér
                        
                     
                                     
                        
                            gastro
                        
                     
                                     
                        
                            na cestách
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            snooker
                        
                     
                                     
                        
                            lukostreľba
                        
                     
                                     
                        
                            patchwork
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Norbiho tajemství
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marína Mery Dobošová
                                     
                                                                             
                                            Ľubomír Mercery Pecho
                                     
                                                                             
                                            Tomáš Bella
                                     
                                                                             
                                            Martin Basila
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Gorila
                                     
                                                                             
                                            Update Slovakia
                                     
                                                                             
                                            Prevody kuchynské
                                     
                                                                             
                                            Prevody do metrického systému
                                     
                                                                             
                                            Slovenský lukostrelecký zväz
                                     
                                                                             
                                            1. Slovenský školský lukostrelecký klub
                                     
                                                                             
                                            PRIHLÁSENIe do blogu
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




