

   
 
 
Bohvie, kto ho vymyslel, s radosťou ho uvádza český demagóg Pavel Kohout a na Slovensku ho podľa mojich informácií používa Martin Chren. 
 
 
Ekonóm, pokiaľ sa správa ako ekonóm, nie je ani pravicový ani ľavicový. Inflácia nie je pravicová ani ľavicová. Hospodársky rast nie je ideologickou veličinou, ani stochastický dynamický mikrosimulačný model s behaviourálnymi funkciami nie je ideologickou veličinou. Ekonóm proste skúma nejaké vzťahy. 
 
 
Samozrejme, jeho osobné preferencie môžu vplývať na dve veci. Jednak na to, aké témy si pre svoje skúmanie vyberie. A zároveň jeho osobné postoje môžu mať vplyv na to, aké recepty potom na základe svojich skúmaní uvádza. A pretože svet je rozmanitý a nedá sa sumarizovať deterministicky, veľa otázok verejnej politiky jednoducho nemá jedine správne riešenie. A rôzne ponúkané recepty sa niekomu už potom môžu zdať ľavicové alebo pravicové. 
 
 
Ekonómia je obrovská veda a pohybujú sa v nej ľudia s rôznymi chuťami, estetickým vkusom, sexuálnymi preferenciami, telesnou váhou a politickým zameraním. 
 
 
Niektorí presadzovali riešenia, ktoré boli tvrdo proti nespútanému trhu. Napríklad nositeľ Nobelovej ceny Gunnar Myrdal alebo tvorca britského sociálneho štátu William Beveridge. V Beveridgeovom prípade je však na rozdiel od Myrdala označovanie za socialistu mimoriadne scestné, keďže podobne ako Keynes bol v zásade veľký liberál, ale z čisto analytických dôvodov presadzoval určité mechanizmy vyvažujúce a dopĺňajúce trh, nevidel možnosť, ako bez nich ekonomický rozvoj udržať. 
 
 
Naopak, viacero mimoriadne významných ekonómov sa osobne angažovalo v radikálnej ľavici. Gramsciho priateľ, základateľ neoricardovskej školy Piero Sraffa. Alebo trockista Ernest Mandel. Pred časom som sa pri dlhej večeri v Madride pokúšal žartom presvedčiť jedného známeho britského ekonóma a politicky skôr pravičiara, že by sa mal stať trockistom. Trockizmus mu veľa nehovoril, ale akonáhle som spomenul Mandela, hneď ma prerušil „ó, Mandel, poznám, dlhé vlny kapitalistického rozvoja, samozrejme, že som si to preštudoval." 
 
 
Iní ekonómovia bývajú označovaní za ľavicových sčasti preto, že používajú marxistické koncepty. Dobrý príklad z histórie je fenomenálny a dlho pre svet neobjavený Michal Kalecki. Dnes napríklad Michel Aglietta, zakladateľ hlavného prúdu francúzskej regulačnej školy, inak môj osobný vzor. Alebo John Roemer. 
 
 
Veľa ekonómov by si od niekoho asi vyslúžilo nálepku ľavičiar preto, že upozorňujú na nekonzistentnosti neoklasickej ekonómie, alebo ukážu, že liberálne riešenie môže priniesť pre aktérov zhoršenie materiálnej situácie. Typický príklad je Jagdish Bhagwati, ktorý teoreticky rigorózne ukázal, ako slobodný obchod môže za určitých podmienok narušiť hospodársky rozvoj (immiserizing growth). Bhagwati je pritom inak vo všeobecnosti tvrdým bojovníkom za rušenie ciel. Potom je tu špičkový špecialista na ekonomiku práce, jeden z horúcich kandidátov na Nobelovu cenu, Richard Freeman. Ten často píše články a pamflety kritizujúce naivne liberálne pohľady na ekonomiku trhu práce. Dani Rodrik je zase podobne kritický voči „slobodnému obchodu" , ktorý v mnohých prípadoch nie je skutočne slobodný. Ukázal tiež, že zaujímavý fakt, že otvorenejšie ekonomiky mávajú väčší rozsah sociálnej politiky, má teoretické rácio.   
 
 
No a špičkový príklad ekonóma, ktorý by mohol byť veselo onálepkovaný ako ľavičiar (a zrejme ľuďmi, ktorý na viac ako nálepkovanie nemajú, aj často býva), a to  kvôli zameraniu svojej práce a navrhovaným riešeniam, je nositeľ Nobelovej ceny Amartya Sen. Zaoberá sa najmä problémami rozvojového sveta. 
 
 
Inak, na nobelovskej stránke má mimoriadne zaujímavú autobiografiu. Osobne sa mi páči spomienka na jeho strednú školu, ktorá mala mnohé vysoko progresívne črty. Cenila sa inteligencia, ktorá sa podľa mnohých do určitej miery nezlučuje s „dobrým" prospechom. Raz Senovi učiteľ povedal o inom žiakovi: „ten chalan vyzerá veľmi chytrý, napriek tomu, že má dobré známky." 
 
 
A takto by sme mohli pokračovať. Manažérsky guru Peter Drucker, obrovská celebrita v podnikateľských kruhoch, začal byť zrazu napádaný ako ľavičiar, keď povedal, že obrovské platy top manažmentov nemajú nijaký ekonomický zmysel a navrhol ich všeobecnou dohodou obmedziť. 
 
 
Mimochodom, keď už som spomenul ten citát od Sena, ktorý mi rozohreje srdce vždy, keď ho vidím, spomeniem aj jeden z obľúbených od Druckera: "There is nothing so useless as doing efficiently that which should not be done at all." (Koľkokrát v pracovnom živote som si toto povedal ešte predtým, ako som to čítal u manažérskeho guru! ) 
 
 
Atakďalej. Mnohí ľavicoví politici majú za sebou slušnú dráhu akademických ekonómov, ako napríklad Gordon Brown*, alebo slušné vzdelanie a praktickú aplikáciu, ako napríklad ekvádorský prezident Rafael Correa. 
 
 
Takže tak. Hovoriť o nezmyselnosti existencie „ľavicových ekonómov" je vrchol stupidity po prvé preto, že ak hovoríme prísne o roli vedca, ideologické nálepky nemajú zmysel, po druhé preto, že ak hovoríme, naopak, o politických preferenciách ekonómov, nájde sa kopa takých, ktorí sa buď vyslovene ľavičiarsky angažujú, alebo takých, ktorých výskum irituje hlupákov, pretože nepotvrdzuje jednoduché poučky o tom, že zrušenie ciel je vždy žiaduce, trh práce je trh ako každý iný a podobne. 
 
 
Zase, na druhej strane, u niektorých konzervatívnych amerických autorov sa objavilo znevažovanie Keynesa ako ekonóma cez fakt, že bol homosexuál alebo bisexuál a teória relativity bola svojho času odsudzovaná preto, že ju vyvinul Žid Einstein. Chvalabohu, že takto ďaleko na Slovensku zatiaľ nie sme. 
 
 
 
 
 
 
 
 
*(23. 8. 2008) Tu som spravil chybu. Ako ma upozornil diskutujúci, Brown nebol ekonómom ale historikom.  
 

