
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Grecula
                                        &gt;
                Nové Sandokanove dobrodružstvá
                     
                 Rok života, smrti a všetkého medzitým 

        
            
                                    24.9.2008
            o
            15:22
                        |
            Karma článku:
                17.65
            |
            Prečítané 
            5036-krát
                    
         
     
         
             

                 
                    Prečo človek nikdy nečaká to, čo ho čaká? Ešte je do konca roka dosť ďaleko, ale mám pocit, že je treba spísať to, čo sa nám stalo, lebo sa nám to neskôr bude zdať také neskutočné, až tomu nebudeme veriť, a nebudeme o tom ani chcieť rozprávať.
                 

                Kate, moja manželka, a ja, spolu s naším synom, žijeme na Borneu už dva roky. Po počiatočnom vrušení sa nám život postupne začal zdať prázdny. Bolo nám v našej džungli pri pláži čoraz menej ako v raji, a cítili sme sa ako Adam a Eva, ktorí radšej budú riskovať vyhostenie než zakúšať rutinu. Bolo treba niečo urobiť. Chcelo to nejakú novú skúsenosť. Nájsť strom poznania dobra a zla a zahryznúť sa do jablka.   Asi to vyznie cynicky, ale myslím, že keď manželia po rokoch nevedia ako si naplniť život, majú deti. Tak sme si povedali, že aj my ešte jedno potrebujeme. A našli sme si tisíc dôvodov, pre ktoré sa to zdalo ako dobrý nápad.     Za normálnych okolností by to vôbec nemal byť zlý nápad. Ľudia sú hlavne na to, aby sa mali radi a popritom sa množili ako sa patrí. No občas by stačilo, keby sa ľudia mali radi. Najmä keď im pred pár rokmi, po prvom pokuse s plodením lekár povedal: " Možno by ste to už nemali skúšať." Ale ľudia si po rokoch naozaj nepamätajú ani bolesť, ani strach, ani utrpenie. Je to neuveriteľné, ale je to tak. Ani my sme si nepamätali. A naraz sme navyše pred sebou mali to vzrušujúce pokušenie urobiť niečo zakázané.     Neviem, či tu naozaj bol nejaký had, ktorý nám s diabolským úškrnom ponúkol to zakázané ovocie. Keď ale nad tým teraz rozmýšľam, automaticky sa mi do tejto role obsadí miestny gynekológ. Milý typ. Vypočuje si všetky pochyby. Pokýve hlavou. A potom dohodí svojej privátnej nemocnici ďalšieho solventného pacienta so slovami: " Ja tu problém nevidím. Mali sme ženy aj s cukrovkou, aj s vysokým tlakom. A viete čo, vyskúšajme túto novú hormonálnu liečbu, všetko pôjde ako po masle". A veru išlo. Najprv na vrchol, a potom bleskurýchlo dolu kopcom.     Je všetko naozaj zapísané vo hviezdach? Ako je možné, že niekto po troch rokoch z ničoho nič povie  po telefóne novoročné želanie, ktoré sa už dokonca aj začalo plniť? Pri vymieňaní vinšovačiek cez webcam sa môj normálne veľmi opatrný otec zrazu preriekol, že nech sa nám všetko čo si želáme splní, a nech nám nový rok aj nové bábätko prinesie. Sám zostal ako obarený, a hneď sa začal ospravedlňovať, lebo vedel, že v našom prípade žiadne také plány nie sú, a že vlastne želať nám tehotenstvo ani nie je želaním niečoho dobrého.     Len som ho ubezpečil, že nič sa nestalo, a že ďakujem, že to by bolo naozaj perfektné. Mysliac pritom na to, čo mi Kate práve povedala. Že jej to mešká.     Rokmi som zistil, že každý kúsok fatalizmu, ktorý sa na teba v živote prilepí, sa raz zíde. Chvalabohu, že som ho mal do zásoby. Naraz som ho začal potrebovať skoro každý deň. Prvýkrát, keď sa mi Kate stratila kdesi pri nákupoch v uliciach Miri. Bez telefónu a bez stopy. Predpokladal som, že kdesi odpadla. Zúfalo som brúsil ulicami, hĺadajúc ležiace telo na nejakom zaprášenom chodníku. Nakoniec mi zavolala z domu, vraj kde som. Sanitka ju medzičasom v bezvedomí vzala do nemocnice, ale už sa cítila dobre, tak si dala priepustku a odišla taxíkom.     Potom jej v treťom mesiaci tehotenstva začal stúpať tlak. Renomovaný gynekológ prehlásil, že je to nadváhou, že sa má viac pohybovať. Moja fatalistická povaha to zožrala aj s chlpami a odmietla sa znepokojovať.     Fatalistu tiež neporazí, keď doletí na dovolenku do komunistického Vietnamu a žena mu povie, že si zabudla inzulín v doma chladničke. Naopak, uteší manželku že všetko bude okej, zoženie nejakú miestnu podozrivú žbrndu, ktorá nemá skoro žiaden účinok, a vôbec si nepripustí, že vysoký cukor môže z jeho nenarodeného potomka vyrobiť v maternici strašidlo.     Fatalista tiež neberie vážne, keď mu manželka miesto romantiky zašepká do ucha: "Prepáč miláčik, myslím, že strácam naše dieťa." Fatalistický ja som sa tiež nerozplakal od bezmocnosti, keď mi po piatich mesiacoch podobných príhod došiel email, že Katin otec umiera doma v Anglicku na rakovinu.     Leteli sme cez pol zemegule. Kate v šietom mesiaci, s vysokým tlakom, a skoro nekontrolovateľnou cukrovkou. Zaujímavé, že človek riskuje život pre smrť.     Svokor bol v nemocnici. Čakal na výsledky testov, alebo na samotné testy. Nič nebolo potvrdené, ale už vedel, že s najväčšou pravdepodobnosťou to bude nádor na mozgu. Občas bol ešte starým Malcolmom, ako sme ho všetci poznali, ale často sa duchom vytrácal, akoby to tu na zemi preňho už naozaj nebolo dôležité. Chodili sme za ním denne, celá naša rodina, včítane svokry, a občas aj švagra. Posadali sme si okolo jeho postele, a začali sa rozprávať, akoby sme si práve sadli doma k večeri. V dnešnej dobe si nie je možné priznať, že človek zomiera, ani keď zomiera. Veci sa plánujú napriek tomu, čo sa očividne deje, len aby sme neplakali. Ešte stale záleží na maličkostiach, ešte stale sú občas hádky, ešte stale nemáme mnohé na sebe radi.     Ja som tam väčšinou sedel a nevedel, čo so sebou. Občas som sa nútil do depresívnych, existenciálnych pocitov, chtiac precítiť tragiku toho, čo sa deje. Podarilo sa to iba keď som začal rozmýšľať, ako sa asi cíti Kate, a čo to pre ňu znamená. Mal som Malcolma, rád, ale skoro som ho nepoznal. Uvedomil som si, že sme smutní iba keď si predstavíme, že niečo čo máme radi, už okolo nás nebude. Úsmev, žarty, výlety, spoločné pozeranie telky. Myslím, že skôr smútime za tým, čo stratíme my, ako za tým o čo príde náš blízky. Keď toho príliš nestratíme, vnímame iba to choré telo, ktoré po čase už nebude choré. Alebo je azda ľahšie zažiť smrť blízkeho, keď sa blíži nový život? Možno som sa jednoducho na smrť nemohol sústrediť.     Racionálne sa na dôležité veci v živote skoro nikdy nedá ísť. Keď naozaj o niečo ide, väčšinou vieme, čo treba urobiť, a čo nie. Niekedy je nám jasné, že nebezpečné veci je treba urobiť. Nikto sa nikdy naozaj nepozastavil nad tým, či sa je alebo nie je treba ísť s Malcolmom rozlúčiť. Rovnako nikto, o pár týždňov neskôr, keď už sme boli zas doma na Borneu, nepotreboval ísť naspäť Malcolmovi na pohreb. Bolo to ťažké ľuďom vysvetliť, ale nebolo o tom pochýb.       Väčšinou vedome nechcem, aby ma ovládali pocity. Snažím sa ignorovať predtuchy. Je veľmi ľahké skončiť ako totálny paranoik. Ale v prípade Katinho tehotenstva som to prehnal. Keď ti pod nohy prilieta jedno poleno za druhým, možno je treba ustúpiť a povedať: OK. Rozumiem. Je treba veci brať vážne.     Ja som tak neurobil, a bol som v ten týždeň, keď ku všetkému došlo, služobne v Kuala Lumpure. Aj keď som sa pár dní predtým zdráhal kamkoľvek ísť, veď moja žena môže skončiť každú chvíľu v nemocnici. Nakoniec ma nikto nenútil, a predsa som šiel.     Bolo to zvláštne, tých pár dní v hoteli som mal čas o všeličom po večeroch premýšľať, a nech to znie čudne ako chce, na všetko sa pripraviť. Ešte aj keď mi Kate zavolala, že jej je zle, a asi pôjde do nemocnice, bol som neuveriteľne kľudný. Vôbec ma nenapadlo, že by som bol na prahu jednej z najväčších drám svojho života. Ešte stále ignorancia? Alebo ma už prúd osudu strhol so sebou a ja, miesto zúrivého kopania nohami a mávania rukami, som sa nechal unášať.     Kate bola v pondelok na večeru s kamarátkami. V utorok jej bolo zle. Myslela si, že od pokazeného jedla. V stredu zašla k lekárovi. Vo švtrtok sa odviezla do nemocnice a odmietla odtiaľ odísť. V piatok ráno som bol pri nej, práve keď dostala prvý záchvat eklampsie. O dve hodiny neskôr ju rozheganou sanitkou previezli po rozbitých cestách na druhý koiec mesta. Z privátnej do štátnej nemocnice. Toto vyzeralo príliš seriózne, aby sa to súkromníkom oplatilo riskovať. Hoci aj za veľké prachy.     Cestou Kate odtiekla voda a dostala ďalší záchvat. Na oddelenie kam ju previezli, sa zbehol kopec lekárov a sestričiek. Vytlačili ma von na chodbu. Toto nebolo pre amatérov. A potom rýchly prevoz na operačný sál. Nedalo sa čakať. Rozumiem, že dieťa teraz nie je na prvom mieste? Je treba zachrániť matku, taký je predpis.     Nikto sa ma nepýtal na súhlas, ale súhlasil som. A potom som dlho čakal za zatvorenými dverami. O pár hodín som Kate uvidel. Vyzerala lepšie.     A potom zasa horšie.     Aká je správna reakcia na vetu: "Myslím, že umieram ..." ? Moja bola - NEDOVOLIŤ ! Neskúšal som sa modliť, ani mi nezačali tiecť slzy. Len som opakoval: " Nechoď, my všetci ťa veľmi potrebujeme. Ľúbime Ťa, zostaň s nami". Neviem, či to bolo nástojčivosťou v mojom hlase, alebo strašným stískaním rúk, ale Kate neumrela. Dosť často ma teraz, po čase, napadne, či by niečo bolo inak keby som sa rozreval alebo len stál ako soľný stĺp. A vôbec, ako človek vie, že umiera? Dá sa ľuďom na nemocničnej posteli veriť? Zas na druhej strane, záleží na tom?     Viem, že Kate a moja malá Isabel boli špeciálny, a ťažký prípad. Viem to podľa toho, že mi dovolili kedykoľvek ich navštíviť. To v Malajzii nie je samozrejmé. Ako vo vsetkých tradičných autoritatívnych krajinách, doktor je v nemocnici boh, pacienti jeho osobné vlastníctvo, a navštevníci akurát hlučný, nespratný, a infekciou nasiaknutý problém. Do pôrodného oddelenia ockovia nesmú ani strčiť nos. A predsa, ja som mohol dnu, na izbu intenzívnej starostlivosti, kedykoľvek. Ochranka pri vchode, o ktorej som vtedy ešte nevedel na čo je, mi len priateľsky kývala na pozdrav. Dlho som si myslel, že je to tým, že som jediný beloch ktorý tam chodí, a preto automaticky celebrita, na ktorú sa zákony nevzťahujú. O pár dní mi jedna pani v uniforme vysvetlila, že ten môj voľný priechod nariadil sám primár.     Zdá sa, že si Kate tými vyhláseniami nevymýšľala.     Mimochodom, ochranka je hlavne na to, aby zastavila pacientov, ktorí sa snažia z nemocnice ubziknúť bez platenia.     Keď som prvýkrát uvidel Isabel, bola miniatúrna, a bledá. Lekár ma tam dlhú dobu nechcel pustiť. Myslím, že čakal, že každú chvíľu zomrie. Predtým, než mi ju konečne dovolil vidieť, mi opakoval, že je veľmi malá a slabá. Že je to kritické. Že nech toho veľa nečakám. Nevedel som, či myslí od Isabely, od neho, alebo od života ... Nečakať že raz pôjde na univerzitu, že sa vydá, že ma bude navštevovať s vnúčikmi? Alebo len že prežije nasledujúci týždeň? Že začne sama dýchať?     Je to zvláštne. Možno som emocionálne deprivovaný typ. Isabel bola malá, bledá, ale vôbec mi neprišlo, že slabá. Akosi som sa o ňu vôbec nebál. Pritom som sa vôbec necítil istý, že keď prídem zas o pár hodín, ešte bude žiť. Ale ani pevnú vieru som nemal. Možno som akurát nemal energiu na to, aby som sa bál.     Keď jej o pár dní bolo treba dať transfúziu krvi, vysvetlili nám, že my ako Európania darovať nemôžeme, lebo podľa malajských vyhlášok máme všetci chorobu šialených kráv. Chcel som na nich kričať, že aj naša dcéra je európanka, a že keď sme šialené kravy my, možno je aj ona. Že sa na tom tak či tak veľa nezmení. Ale vyhlášky sú vyhlášky. Isabel dostala dávku malajskej krvi. Potom ešte raz o pár týždňov, a ešte raz neskôr.     Teraz už rozumiem, prečo teraz, keď je už doma, rada vylihuje vonku v tridsaťstupňových horúčavách. Už len čakám, kedy si vypýta pikantné omáčky, ryžu, a možno ešte vysušenú lebku na šnúrke.     Oddelenie popôrodnej intenzívnej starostlivosti v Miri je neuveriteľné. V malom meste v pralese, a pritom napratané novou technikou, vyumývané,  plné profesionálne vyzerajúcich ľudí. A pomedzi to malé bábätká, pripútané k prístrojom, infúziám, monitorom. Na stenách grafy úspešnosti liečby. Prepočítal som si, že v priemere tu umrie jeden a kúsok bábätka za mesiac. Na to, že ich je tam stále minimálne pätnásť, ale občas aj tridsať, je to skvelý výsledok.     Pokiaľ to bábätko nie je vaše.     Videl som tam jedno bábo umrieť. Nečudo, chodil som tam za Isabel dva mesiace. Štatistika. Ale keď sa to stane, nikto na grafy nepozerá. Počuť akurát plač matky. A všetci ostatní stoja okolo a pozerajú sa. Nikto sa neodváži nič povedať, alebo matku objať. Lekár tiež zdvihne hlavu od písania správy, ale po chvíli pokračuje. Ten plač nie je až taký strašný, ako keď človek vidí v televízii plakať iracké matky, držiac na rukách mŕtvych malých chlapčekov. Asi sa na dieťa, ktoré je zavreté v plexisklovej krabici a nedá sa ho ani dotknúť, až tak veľmi nezvykne. Srdce asi pukne o trošku menej. Je to pre mňa úplne neuveriteľné, ale na smrť sa v nemocnici musí zvyknúť.     Nakoniec prišli aj Kate aj Isabel domov. Je z nás opäť "normálna" rodina. Isabel plače, Kate kŕmi, ja hojdám na rukách. V noci rodinná párty, vrieska sa na celé hrdlo, ráno kruhy pod očami. Isabel sa zdá ľuďom malá, ale nám veľká, Kate sa zdá každému normálna. Ľudia prídu, pozrú do kočíka, usmejú sa, a potom nás poľutujú, lebo počuli o našich ťažkostiach. "Ale hlavne, že je všetko v poriadku". Ľudia milujú happyendy. Ľudia majú strašný strach, keď sa niečo neskončí happyendom. Čo potom? Čo povedať? Pomôcť? Ako sa dá ďalej žiť?     Ja tiež milujem happyendy. Vôbec netuším , ako odpovedať na tieto otázky. Celé to KEBY je jeden paralelný vesmír. Skoro sme doňho skočili. Správali sme sa nezodpovedne? Mali sme všetci robiť niečo iné, a inak? Bolo by potom všetko nakoniec lepšie, alebo horšie?  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Grecula 
                                        
                                            Chudoba v hlave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Grecula 
                                        
                                            Ako byť v bohatej krajine bohatý?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Grecula 
                                        
                                            Pätnásťročné manželstvo ktoré nebolo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Grecula 
                                        
                                            Čarodejné pobrežie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Grecula 
                                        
                                            Prvý Apríl
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Grecula
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Grecula
            
         
        grecula.blog.sme.sk (rss)
         
                                     
     
         Pozorujem svet. Snažím sa pochopiť, o čo ľuďom ide. A o čo ide mne. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3857
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo zemegule
                        
                     
                                     
                        
                            Z rodnej hrudy
                        
                     
                                     
                        
                            Rozvahy a odvahy
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




