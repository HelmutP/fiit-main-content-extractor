
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Boris Burger
                                        &gt;
                Nakrátko
                     
                 Policajti nezlyhali 

        
            
                                    24.5.2010
            o
            11:07
                        (upravené
                2.6.2010
                o
                7:55)
                        |
            Karma článku:
                13.19
            |
            Prečítané 
            5106-krát
                    
         
     
         
             

                 
                    Viacerí z nás (účastníkov stretnutia Bratislavský Pride) sa zhodli v názore, že polícia nezlyhala.
                 

                 Článok pána Nicholsona považujem za prehnaný. Nechápem, že ak niekto zoberie na podobný riskantný podnik deti, že nepočíta s nebezpečenstvom. Je to riziko. V samom epicentre akcie sme ani netušili (vďaka policajtom), čo sa deje na periférii. Zadržanie extrémistov blízko americkej ambasády prebehlo nanajvýš profesionálne.      Policajti rozostavení pred sochou Hviezdoslava - po celej šírke námestia. Odporučili mi, aby som šiel okolo (od nemeckého veľvyslanectva).   Pripadá mi, že sme obaja, pán Nicholson a ja, boli na dvoch úplne odlišných podujatiach. Jeho opis je natoľko expresívny, že sa nestačím čudovať...   Naša priateľka z blogu podstúpila toto riziko (zobrala so sebou dcérku), ale, chvalabohu, pokiaľ sme boli spolu, nikto ju neohrozil - a obe dievčatá rozdávali okolo seba iba radosť.   Policajti na začiatku podujatia nepúšťali ľudí, ktorí prichádzali od Carltonu. Ja som prišiel od hotela River Park.      Dymovnica alebo slzný plyn predchádzala zraneniu chlapa (posledný záber).   Akcia Pride bola z pohľadu viacerých dobre chránená. Nie je možné zabrániť prieniku extrémistov na námestie, ktoré nie je hermeticky uzavreté. Samozrejme, že došlo k výtržnostiam aj k osobným útokom. To nespochybňujem, sám som bol svedkom troch. Ale vďaka polícii boli škody "prijateľné" (obával som sa horšieho).      Tento mladý muž sa stal, bohužiaľ, reálnou obeťou...   Keď čítam niektoré titulky (v novinách či na blogu), zdá sa mi, že sme národ flagelantov. Ak sa podobne vyjadrujú zahraniční komentátorí, asimilovaní Slováci alebo ľudia z ambasád, napadá mi, že oni o našej záľube v sebabičovaní dávno vedia :)   Hviezdoslavovo námestie, plné turistov a oddychujúcich Bratislavčanov, nie je futbalový štadión, na ktorom môžete oddeliť sektory fanúšikov.       Pár fotografíí:   http://burger.blog.sme.sk/clanok.asp?cl=229535&amp;bk=72220 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (91)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Svet Stefana Zweiga (tip na dve knihy)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Istanbul nie je iba Taksim (a ten nie je Gezi Park)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Hüzün
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Čo mi vlastne je (vie ľudový liečiteľ)?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Boris Burger 
                                        
                                            Ako som mal strach (Kuala Lumpur a Singapur 1)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Boris Burger
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Boris Burger
            
         
        burger.blog.sme.sk (rss)
         
                                     
     
          
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    212
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3407
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje kecy
                        
                     
                                     
                        
                            Nakrátko
                        
                     
                                     
                        
                            Pokus o poéziu
                        
                     
                                     
                        
                            Rozprávanie z ciest
                        
                     
                                     
                        
                            Listy Štefana Bohunického
                        
                     
                                     
                        
                            Veselé povzdychy
                        
                     
                                     
                        
                            Nadlho
                        
                     
                                     
                        
                            Príbeh bývalého miništranta
                        
                     
                                     
                        
                            poviedka na nedeľu
                        
                     
                                     
                        
                            Fotografie-súkromné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Reč S. Krčméryho na súde
                                     
                                                                             
                                            Spomienka na Čiernu labuť
                                     
                                                                             
                                            Nikdy by som nebol veril
                                     
                                                                             
                                            Ako som dusil blogerky a piekol
                                     
                                                                             
                                            Môj New York III.
                                     
                                                                             
                                            Mamino lono
                                     
                                                                             
                                            Niečo o Coca Cole, hoteli Palace a o...
                                     
                                                                             
                                            Hitlera by trafil šľak
                                     
                                                                             
                                            Rozhovory Karola Sudora sú to najstrhujúcejšie na sme.sk
                                     
                                                                             
                                            RDB: Pôjdeš tam, neviem kam
                                     
                                                                             
                                            Alenka Technovská: Ľadové abstrakcie
                                     
                                                                             
                                            Moja videovizitka
                                     
                                                                             
                                            Barborka Bzdušková: Možno (ale dostala ma naozaj)
                                     
                                                                             
                                            Viktor DAHOME Holman: Kto som?
                                     
                                                                             
                                            Chen Lidka Liang: Možno mi nežiť v živote, nemožno mi ťa neľúbiť
                                     
                                                                             
                                            Miriam Novanská: Premeny ženy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Orhan Pamuk: Sníh
                                     
                                                                             
                                            Haruki Murakami: Sputnik, má láska
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Johnny Cash - God's Gonna Cut You Down
                                     
                                                                             
                                            Kill Bill - Strawberry Fields Forever
                                     
                                                                             
                                            The Beatles
                                     
                                                                             
                                            Happiness Is A Warm Gun z Across The Universe
                                     
                                                                             
                                            Underworld
                                     
                                                                             
                                            Portishead
                                     
                                                                             
                                            Bat For Lashes
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Beáta Grünmannová
                                     
                                                                             
                                            Jana Shemesh
                                     
                                                                             
                                            Helenka Fábryová
                                     
                                                                             
                                            Kika Kováčiková
                                     
                                                                             
                                            Adriana Didi Markovičová
                                     
                                                                             
                                            Miriam Reid
                                     
                                                                             
                                            Magduška Kotulová
                                     
                                                                             
                                            Katka Bagoči
                                     
                                                                             
                                            Samo Marec
                                     
                                                                             
                                            Mika (Mirkaa!) Polohová
                                     
                                                                             
                                            Janko K. Myšľanov+
                                     
                                                                             
                                            Kamila
                                     
                                                                             
                                            Juro a Apollo
                                     
                                                                             
                                            Katka Maliková
                                     
                                                                             
                                            Natalka Blahová
                                     
                                                                             
                                            Jarka Mikušová
                                     
                                                                             
                                            Miro Šedivý
                                     
                                                                             
                                            Andy Sekanová
                                     
                                                                             
                                            Bett Mateová
                                     
                                                                             
                                            Chen Lidka Liang &amp;#26753;&amp;#26216;
                                     
                                                                             
                                            Nataška Holinová
                                     
                                                                             
                                            Gabina Weissová
                                     
                                                                             
                                            Roman Daniel Baránek
                                     
                                                                             
                                            Danka Sudorová
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Hlas od sporáka
                                     
                                                                             
                                            O všetkom aj o ničom
                                     
                                                                             
                                            Sieťovka
                                     
                                                                             
                                            MHD
                                     
                                                                             
                                            Weather report
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                     
                                                         
                       Ktorý November by radšej oslavovali komunisti?
                     
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Nesto a jeho štyri manželky
                     
                                                         
                       Toto má byť akože umenie?
                     
                                                         
                       Chceli sme sa vrátiť domov, ale…
                     
                                                         
                       V knižnici
                     
                                                         
                       Ako zarobiť na onkologickom pacientovi
                     
                                                         
                       Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                     
                                                         
                       Bratislavská žumpa blafuje
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




