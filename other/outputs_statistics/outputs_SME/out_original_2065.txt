
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Lužinský
                                        &gt;
                Viera
                     
                 Životnosť planéty Zem 

        
            
                                    22.1.2010
            o
            23:57
                        |
            Karma článku:
                25.57
            |
            Prečítané 
            2554-krát
                    
         
     
         
             

                 
                    Čo sa týka našej planéty Zem, Biblia nás uisťuje, podľa inšpirovaných slov presláveného starovekého izraelského kráľa Šalamúna:
                 

                 "Pokolenie odchádza a druhé prichádza, ale Zem stojí naveky."  - Kazateľ 1:4 (Evanjelický preklad).   Sú tieto a iné podobné inšpirované Božie slová príliš pekné na to, aby to bola pravda? Prečo by sme mali veriť, že naša prekrásna planéta Zem nikdy nezanikne, keď niektorí vedci tvrdia opak? Skúsme sa len na chvíľu zamyslieť nad každodennými príkladmi v obchodoch, kde všetky výrobky, ktoré sa tam predávajú, majú vyznačené dátumy spotreby. A kto určil tieto dátumy? Možno ich odhadli vedúci obchodov na základe svojich dlhoročných skúseností? Samozrejme, že nie! Ale dátumy spotreby určili výrobcovia produktov! A všeobecne dôverujeme, že takéto dátumy sú určené správne, pretože výrobcovia  vedia lepšie, a poznajú dôkladnejšie svoje produkty, ako ktokoľvek iný. O čo viac by sme mali dôverovať Tvorcovi našej úžasne prekrásnej  planéty Zem, lebo tiež Jeho Slovo, Biblia, jasne hovorí, že ´dôkladne..upevnil Zem´ aby existovala navždy! Aj jej životnosť nie je obmedzená žiadnym ´dátumom spotreby´,  ako to uisťuje Žalm 119:90. Preto aj písateľ knihy Žalmov mohol  dodať tieto inšpirované a veľmi povzbudivé slová:   "Založil si Zem na jej pilieroch, neskláti sa na večné veky."  - Žalm 104:5 (Evanjelický preklad).   Ani neprichádza vôbec do úvahy, aby nezodpovedné ľudstvo zdevastovalo Zem natoľko, že by ju už nebolo možné obnoviť! Zakrátko jej Tvorca zakročí (Zjavenie 11:18). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (122)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Vlády ako divé zvery
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Prenasledovanie za pravdu je príjemné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Pravá božska láska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Kto pozná Božie meno?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Lužinský 
                                        
                                            Smrťou sa končí všetko
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Lužinský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Lužinský
            
         
        luzinsky.blog.sme.sk (rss)
         
                                     
     
         Autor sa bude snažiť budovať, povzbudzovať a utešovať ľudí (vrátane samého seba) svojou prózou i poéziou.    
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1303
                
                
                    Celková karma
                    
                                                1.18
                    
                
                
                    Priemerná čítanosť
                    481
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Teológia
                        
                     
                                     
                        
                            Historia
                        
                     
                                     
                        
                            Veda
                        
                     
                                     
                        
                            Viera
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




