
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Maroš Cuník
                                        &gt;
                Politika
                     
                 Aj oni si zrejme sľúbili vydržať. Vydržia? 

        
            
                                    19.12.2009
            o
            17:00
                        (upravené
                15.7.2010
                o
                22:38)
                        |
            Karma článku:
                5.69
            |
            Prečítané 
            1825-krát
                    
         
     
         
             

                 
                    Keď som včera navštívil môjho kamaráta Andreja ako to už u neho doma býva, debata sa strhla na politickú tému. Pán Bartek, Andrejov oco, sa ma opýtal na môj názor na billboardy komunistickej strany slovenska, z ktorých viaceré sa nachádzajú aj vo Svite. Jeden z nich už voľajaký aktívny antikomunista stihol znehodnotiť, za čo má odo mňa zároveň pochvalu a obdiv (keďže sa jedná o KSS) i pokarhanie (keďže si dovolil zničiť súkromný majetok). Pán Bartek zároveň podotkol jednu veľkú pravdu a to je, že keby tam ktosi vedľa cesty vylepil symbol SS alebo hákový kríž, stálo by ho to pravdepodobne právo žiť na slobode...   Zároveň naznačil že osobne nevidí markantný rozdiel medzi jedným a druhým režimom, čo sa ich totality týka. S tým nemôžem než súhlasiť.
                 

                 
  
   Ako som mu odpovedal?   V prvom rade som sa vyjadril, že osobne mi nevadí, že vedľa cesty sa nachádza billboard KSS s ich znakmi kosákom a kladivom v červenej hviezde a to aj na priek tomu, že s komunistami nezdieľam zrejme ani jediný názor. Na rozdiel od nich si ctím slobodu a to aj slobodu prejavu. Keďže oni mňa svojim billboardom nijako neobmedzujú, nevidím dôvod prečo by mi mal vadiť.   Iné už je, čo si myslím o tom čo na ňom je. Je to urážkou slušného človeka, že takýto ľudia, ktorý kradli ostatným slobodu 40 rokov si ešte dovolia im takto napľuť do tváre, svojim provokačným billboardom. Ja ho skôr chápem takto:  "Sme tu aj po 20 rokoch..." (a medzi riadkami sa dá prečítať) "tak vám treba, že ste sa s nami nevyrovnali poriadne, my vám váš život ešte strpčíme!"   Je však treba povedať (teda v druhom nemenej dôležitom rade), že aj na priek tomu, že mi tento billboard nevadí, pretože je to slobodné vyjadrenie vadí mi iná vec. A to bola aj moja odpoveď pánovi Bartekovi.  Totiž mne vadí to, že aj na priek tomu, že v tejto republike sa deklaruje sloboda prejavu, niektoré veci vyriecť nemôžeme, ani ich nemôžeme ukázať. To je práve tá druhá vec o ktorej sme sa bavili, teda hákový kríž alebo znak SS.  Tak teda aký je rozdiel medzi jedným a druhým TOTALITNÝM režimom, že jeden z nich zakazovaný byť môže a ten druhý nie. Záleží azda na počte perzekvovaných alebo zabitých? Alebo na dĺžke trvania (teda najkratšie sú zakázané)? Alebo na nejakej inej činnosti konanej proti slobodnej vôli jednotlivca?  Je nutné povedať, že žiadne štatistiky nám priamo nepovedia kto je vinnejší z ohromnejších zločinov a kto je v práve. Faktom je, že tak ako nacisti za svojich 6 rokov tak komunisti za 41 rokov vykonali toľko bezprávia na tomto územi, že trest si zaslúžia obidve frakcie skupiny neslobodných režimov.  No ide o to, že nie zákonom sa nám podarí obmedziť ich vplyv, nie nariadeniami, ktoré zakážu voľajaký symbol. Otázkou je, ako človek škodí druhému, že si dá na lakeť červenú pásku s hákovým krížom? Alebo že pozdvihne pravicu, ukáže ako vysoko vie docikať a zakričí známe heslo tretej ríše?  Samozrejme že nijako. A nijako neškodí ani človek, ktorý si dá na lakeť symbol kolovrat.  Tento štát, tým že zakáže jeden z najhorších totalitných režimov vôbec nijako nepomôže tomu, aby ho ľudia nenasledovali. Pozrime sa spoločne na USA, kde nacistické strany nie sú zakázané, rovnako tak nie sú zakázané ani komunistické a otázkou je, akú majú podporu? Myslím si že zakázané ovocie chutí lepšie.  A teraz si vezmime Mariána Kotlebu... človeka, ktorý sa populistickými rečami snaží v ľuďoch vzbudiť podporu, rovnako iní fašisti ako Slovenské hnutie obrody, ktoré svojimi pokryteckými ťahmi snaží v ľuďoch prebudiť pocit , že práve táto cesta je tá správna. Naposledy to bolo zbieraním hračiek pre detské domovy. Keď som sa opýtal, či to pôjde aj rómom, odpovedal mi, že sa nemám báť, vraj vybrali dobre... Potrebujeme takýchto kvázi spasiteľov, ktorým nejde o nič iné než o poularitu a zavedenie nového Slovenského štátu? (možnože Všeslovanského)   Týmto sa však nechcem vyjadriť, že mi ich pofiderná činnosť prekáža. Nech si robia, čo chcú, nech aj majú podporu más, len nech svoje rozhodnutia nevzťahujú na mna bez môjho súhlasu.   Ako je možno toto zmeniť? Určite nie s našou vládou, ktorá síce je podporovaná veľkou časťou obyvateľstva ale zužuje priepasť medzi extrémom (v jej prípade i nacizmom i komunizmom) a bežnou praxou. Je to totiž vláda, robí uzavreté rozhodnutia, také ktoré s páčia tej časti obyvateľstva, ktorá ju podporuje ale tá zvyšná potrebuje hľadať alternatívu.   Bežní ľudia hľadajú takých, ktorý ponúkajú "riešenia" nech už sú akokoľvek nezmyselné alebo drastické, ľuďom sa páčia. Počet populistických krokov rastie a normálni ľudia,   S takýmito alternatívami nie je možne existovať.   Ako teda zamedziť komunistom byť tu aj po dvadsiatich rokoch bez toho aby sme ich zákonom poslali do ilegality?   V demokracii je to veľmi ťažké. Tam totiž pracuje blúdny kruh. Na to aby mali dlhodobo dobrú vládu treba, aby dlhodobo múdrych ľudí. No politici múdrych ľudí nepotrebujú, preto školstvo nezlepšia.   Je teda nutné aby nikto nemal politickú moc nad vecami, ktore zasahujú do osobnej slobody jednotlivca a ich prípadná zmena by teda znamenala tvorbu extrémneho krídla, s absolútne opačným názorom na danú tému.   Čo nám teda z toho vyplýva? Ako zachovať komunistov, nacistov, umiernených socialistov a pravicových liberálov i konzervatívcov a zároveň nikomu nezobrať možnosť slobodne sa rozhodovať o svojích veciach?   Odpoveď je jednoduchá. Stačí aby sme si vytvorili vládu formátu USA z osemdesiatych rokov osemnásteho storočia, vláu Islandu po viac ako 1000 rokov, vládu, ktorá nezasahuje do súkromných vecí. Potom už búdú môcť komunisti existovať slobodne, a keď sa ku nim niekto pridá, nech si aj spoločne a nenútene vytvoria komúnu, do ktorej nikoho nebdú nútiť vstúpiť. A nacisti nech si vytvoria spolok v ktorom budú hromadne hajlovať. A štát bude len dozerať, že či sloboda ich pestí naozaj končí na špičke môjho nosa.   No toto je zatiaľ hudba budúcnosti (alebo minulosti?). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Cuník 
                                        
                                            List pánu ministrovi zahraničných vecí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Cuník 
                                        
                                            Slovenská reprezentácia v lete pôjde do JAR
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Cuník 
                                        
                                            Svit Masaker a ako pokračuje undergroundová scéna v PP okrese
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Maroš Cuník
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Maroš Cuník
            
         
        maroscunik.blog.sme.sk (rss)
         
                                     
     
        Black a Deathmetalový antistate antigovernment kapitalisticky študent
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1224
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Alternatíva voči Ficovi existuje
                     
                                                         
                       Biele kone sú na Slovensku legálne
                     
                                                         
                       Varíme s medveďom - Rýchlovka - Čokoládová pena
                     
                                                         
                       Lalibela – etiópsky Jeruzalem
                     
                                                         
                       Základné pravidlá predpovedania budúcnosti vo volebnej kampani
                     
                                                         
                       Trnavská hlavná ulica (1)
                     
                                                         
                       Kollár: Neinformovaný volič je ovca. Dokedy?
                     
                                                         
                       Chod psychicky chorej mysle je dôležité udržať na uzde
                     
                                                         
                       Kollár: Fico potichu okráda Slovákov o stámilióny eur
                     
                                                         
                       Malilinká bodka
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




