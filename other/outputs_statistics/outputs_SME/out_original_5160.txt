
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            sona hruzikova
                                        &gt;
                dušou
                     
                 Vyššie 

        
            
                                    24.4.2010
            o
            12:47
                        |
            Karma článku:
                7.23
            |
            Prečítané 
            1386-krát
                    
         
     
         
             

                 
                    Odchádzam a vraciam sa. Cestujem v čase. Len prítomnosť mi (občas) nie je dosť dobrá.
                 

                V apríli počujem niekoho hovoriť vezmem ťa do mesta, ktorým preteká Váh, budeme sedieť na jeho brehu, pozerať sa na vzdialenú druhú stranu, na stromy s čerstvými zelenými listami, na psov, ktorí plávajú do stredu rieky a potom na nich niekto zavolá, aby sa už vrátili.  Na osobách nezáleží, dôležitý je súzvuk.  Popri rieke pôjde starý pán tlačiaci bicykel, bude celkom blízko a nečakane povie dobrého zdravia vám prajem, práve vo chvíli, keď si všimnem, že zo zeme v neprirodzenom uhle trčí zopár veľkých čiernych pierok. Vtáčí hrob, úsmevy neznámych ľudí, pohľad na rieku a na jasnú modrú oblohu, myšlienky o slobode.      Cestou späť dvaja chlapci pod veľkými taškami sklonení nad albumom, okolo nich na zemi niekoľko malých bielych papierikov, ktoré tu ostali ako nepotrebná súčasť nálepiek.  Spomeniem si na svoje albumy, zelený WWF a ružový Barbie. Bolo vzrušujúce otvárať nový balíček s nálepkami, netrpezlivo ich vybrať a preletieť očami, či je medzi nimi aj chýbajúce číslo 78. A tie, čo potom boli navyše, dvojmo, sme si cez prestávky vymieňali.  V roku 1994 bolo dôležité niečo zbierať, učiť sa hrať na klavíri či akordeóne a škrobiť záclony.   Dnes príliš často prechádzam schematickými priestormi, životmi. Možno som ich súčasťou a ukrývam sa vo vysokých vežiach. Potom odchádzam, vraciam sa, chodím hore a dole po schodoch. A všetko čo mám, je výhľad, strach z výšok a prievan.   Stále vyššie a vyššie. Lietať, nerobiť kompromisy alebo objaviť iný svet, možno pravdu, lásku, niečo bájne, súzvuk.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Ortuť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Mariánska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Imágo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Posledné prázdniny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Zmeny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: sona hruzikova
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                sona hruzikova
            
         
        hruzikova.blog.sme.sk (rss)
         
                        VIP
                             
     
        "Musí sa, pomyslela si a rozvážne ponorila štetec do farby, udržať na úrovni prostého zážitku, jednoducho cítiť, že toto je stolička, toto stôl, ale zároveň cítiť aj to, že je to zázrak, je to extáza." Virginia Woolfová
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    137
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1683
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            dušou
                        
                     
                                     
                        
                            úlomky
                        
                     
                                     
                        
                            spolu
                        
                     
                                     
                        
                            hrach o stenu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Oštiepok
                                     
                                                                             
                                            Behind the fence
                                     
                                                                             
                                            Respice Finem
                                     
                                                                             
                                            re-wilding
                                     
                                                                             
                                            Sami people
                                     
                                                                             
                                            Kenozero Dreams
                                     
                                                                             
                                            Rudo
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Michal Ajvaz - Lucemburská záhrada
                                     
                                                                             
                                            Olga Tokarczuk - Dom vo dne, dom v noci
                                     
                                                                             
                                            Sándor Márai - Čutora. Pes s charakterem
                                     
                                                                             
                                            Graham Swift - Krajina vôd
                                     
                                                                             
                                            Ladislav Ballek - Pomocník
                                     
                                                                             
                                            Dragan Velikić - Ruské okno
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Outliers
                                     
                                                                             
                                            Ryat
                                     
                                                                             
                                            Nils Frahm
                                     
                                                                             
                                            Julia Holter
                                     
                                                                             
                                            iamamiwhoami
                                     
                                                                             
                                            Ólafur Arnalds
                                     
                                                                             
                                            Julianna Barwick
                                     
                                                                             
                                            Jono McCleery
                                     
                                                                             
                                            Fever Ray
                                     
                                                                             
                                            Sophie Hutchings
                                     
                                                                             
                                            Soap &amp; Skin
                                     
                                                                             
                                            Hidden Orchestra
                                     
                                                                             
                                            Kraków Loves Adana
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jonathan
                                     
                                                                             
                                            Saša
                                     
                                                                             
                                            Simona
                                     
                                                                             
                                            Baša
                                     
                                                                             
                                            Samo
                                     
                                                                             
                                            Tony
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Andrej
                                     
                                                                             
                                            Tomáš
                                     
                                                                             
                                            Daniela
                                     
                                                                             
                                            Džejn
                                     
                                                                             
                                            Anna
                                     
                                                                             
                                            Alenka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            iGNANT
                                     
                                                                             
                                            Lemon
                                     
                                                                             
                                            Humno
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




