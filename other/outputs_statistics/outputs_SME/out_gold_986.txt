

 
Základné parametre: 
displej: aktívny, 240 x 320 b., 262 144 farieb 
fotoaparát: 3,2 Mpix, 
pamäť: 16 MB + 256 MB karta (v balení) 
výdrž: približne 3 dní
 
 
 
K810i a K770i. 
Už samotné čísla prezrádzajú, ktorý z modelov je dokonalejší, čo však nemusí stále hrať hlavnú rolu. K770íčko ma v porovnaní s modelom K810i menšie rozmery a podľa môjho subjektívneho názoru aj lepšie prevedenie a hlavne aktívny kryt objektívu sa mi zdá u K770i oveľa kvalitnejší (konštrukcia je kovová na rozdiel od umelej pri K810i) 
 
Veľké plus pre K810i je xenónový blesk, ktorý pri modely K770i nahradili obyčajnou a známou LED diódou, čo však nepovažujem za zásadné mínus. 3,2 megapixelový fotoaparát robí dokonalé fotky a s množstvom nastavení je dokonalý. 
 
K770i ma menší displej ako K810i, no rovnaké rozlíšenie. Má to svoje plus aj mínus. Veľké rozlíšenie na menšom displeji robí obraz kvalitnejším a taktiež sa šetrí na spotrebe energie.   
 
Oba telefóny majú okrem spoločnej „k-áčkovej“ rady omnoho viac. Ja sa skôr prikláňam na stranu K770i, hlavne kvôli už spomínaným veciam ako je vyššia výdrž batérie, jasnejší display a cena. Spomenúť by som mohol aj 5 smerové tlačítko, ktoré u K770ička nahradilo používaný joystick v predošlých modeloch. 
 
Začíname. Vzhľad. Prevedenie. 
Prijemný nenápadný vzhľad s istou dávkou elegancie. Aj napriek tomu, že kryt je vyrobený z plastu (okrem kovového krytu fotoaparátu), konštrukcia je kvalitná a pevná. Na prvý pohľad sa zdá, že kovová je aj numerická klávesnica a akýsi pásik, ktorý je po obvode hrany telefónu, čo len pridáva na elegancii. 
 
Podľa môjho názoru je nešťastne umiestený fast port, ktorý nájdeme na bočnej hrane. 
 
Ovládanie. Klávesnica. 5 smerové tlačítko. 
Jednoduché položenie kláves na numerickej klávesnici je obrovskou výhodou. Zdvih a tvrdosť je skutočne napasovaná „tak akurát“. Na rozdiel od jeho predchodcov konštruktéri stavili na istotu a neustále kaziaci sa joistick nahradili kvalitným 5 smerovým tlačítkom. 
 
Klávesnica ma jedinú chybu, ktorá vlastne nesúvisí s prevedením ale softwarom. Problémom môže byť tlačítko pre rýchly prístup na internet umiestene pri tlačítku „späť“ a „vybrať“, ktoré sa mi neraz podarilo nechtiac zatlačiť. Žiaľ jeho funkcia sa nedá upravovať bez zásahu do systému. Riešením je zakázať prístup na internet aby ste sa vyhli nechcenému surfovaniu. Chýba tlačítko „play“.
 
 

 
 
 
 
 
 

 
 
 
Display. Zobrazenie. Menu. 
Jeho obrovskou výhodou okrem veľkého rozlíšenia je aj senzor na snímanie svetelných podmienok, ktorý automaticky upraví jas displeja podľa potreby čím sa pozorovateľne zvyšuje výdrž batérie. 
 
V menu nájdeme 12 známych položiek. Pohyb v ňom sa mi zdá trochu pomalý, hlavne ak mám nastavenú nejakú prepracovanú tému. Ale môže to byť len môj pocit. 
 
Fotoaparát. Video. 
Snímanie obrázkov patrí k špičke medzi telefónmi. Množstvo nastavení od rôznych scén až po vyváženie bielej farby zvyšuje kvalitu fotografie. V tme neočakávajte svetoborné zábery ale postačia na zaznamenanie potrebných veci. Stále však treba pamätať, že je to telefón a nie fotoaparát.
 
 
     
Stále čakám, kedy sa video pohne kvalitou vpred. Stále „kockaté“ v malom rozlíšení (176 x 144 b.). Škoda ale asi si ešte musíme počkať. 
 
Zaujímavé sú modré ikony, ktoré sa rozžiaria po spustení fotoaparátu pri potrebných tlačidlách.
 
 

 
 
Zábava. Multimédiá. Hry. 
Na úpravu fotografií a iných obrázkov môžete použiť PhotoDj. Video môžete jednoducho postrihať v aplikácii VideoDj, ktorá umožňuje viacero zaujímavých funkcií. Funkcia TrackID vám zase pomôže zistiť názov a interpreta piesne, ktorú nepoznáte. Realizovať svoju hudobnú kreativitu môžete pomocou aplikácie MusicDJ. 
 
 
Zaujímavým je taktiež diaľkové ovládanie pc (len vybrané aplikácie) využívajúce prenos dát cez bluetooth. 
 
Nahrávanie zvuku, videoprehrávač a prehrávač hudby netreba bližšie predstavovať. Žiaľ kvôli veľkému rozlíšeniu nie sú všetky java aplikácie spustiteľné. Čo sa však v blízkej budúcnosti určite napraví. 
 
Správy. 
V správach sa už dlhý čas nič nezmenilo. Klasické SMS,EMS a MMS sú všetkým známe. Sony Ericssony ponúkajú najprepracovanejší slovník T9. Pred odoslaním správy sa, pri výbere kontaktu, zobrazí niekoľko naposledy použitých mien alebo čísiel. To je veľkou výhodou. 
 
E-mailový klient patrí k najlepším, zvláda aj automatickú kontrolu. 
 
Rovnako automaticky dokáže K770i kontrolovať aj kanály RSS, ktoré vám poskytnú neustály prehľad o aktualizáciách vašich obľúbených webových stránok. 
 
Pripojiteľnosť. 
Globálny prenos dát je možný cez GPRS a UMTS. Lokálny len cez BT. Infračervené chýba, v dnešnej dobe však nie je veľmi využívané. 
 
Plusy: Cena, rozmery a dizajn, kvalita fotografovania 
Mínusy: „len“ 256MB karta a slabé slúchadlá v balení, Fast port na bočnej stene 
 
Záver. 
Sony Ericsson K770i je veľmi vydarený následník dva roky starého úspešného modelu K750i, ktorý bol vylepšený vo všetkých smeroch. Jediným nedostatkom môže byť len LED blesk a záznam videí. K770i zase ponúka menšie rozmery a všeobecne krajší dizajn v porovnaní s K810i. Stačí sa len rozhodnúť. 

