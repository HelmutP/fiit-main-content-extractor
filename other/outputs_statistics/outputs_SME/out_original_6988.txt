
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jakub Šimek
                                        &gt;
                Nezaradené
                     
                 Problémom nie sú homosexuáli, ale štátna moc 

        
            
                                    21.5.2010
            o
            19:26
                        |
            Karma článku:
                7.79
            |
            Prečítané 
            1358-krát
                    
         
     
         
             

                 
                    Palko a Weisenbacher si môžu podať ruky. Ich články pekne dokazujú, že západný liberalizmus a konzervativizmus sú len dve strany jednej mince a rovnako podkopávajú slobodu jednotlivca. Obaja argumentujú rovnako a snažia sa svoj súbor hodnôt politicky nanútiť spoločnosti. Zaujímavý je aj fakt, že v dnešnom liberálnom diskurze sa nedájú presadzovať nové opatrenia, alebo proti nim bojovať, bez toho, aby sme z človeka neurobili obeť. Palko si vypožičiava ľavicové argumenty typu „Ľudia kvôli svojmu presvedčeniu v Británii prichádzajú o prácu, toto chceme?" a Weisenbacher zasa útočí na emócie podobne ako kresťanské mamičky, keď smúti nad tým, že „homosexuálne páriky sa nemôžu na verejnosti držať za ruky bez toho, aby na nich niekto zazeral". Pointa môjho článku je, že keď niekto robí z človeka páchateľa alebo obeť, vždy mu tým berie časť jeho slobody.
                 

                 Podporujem prvý Dúhový pochod, lebo je to pre mňa Udalosť v zmysle pojmu Alaina  Badioua. Teda predstavuje niečo dôležité iba pre zainteresovaných. To sú paradoxne hlavne konzervatívni kresťania a homosexuáli. Majorita je voči pochodu indiferentná a zajtrajšou akciou sa pre nich nič nemení. Pre mňa je kľúčové slovo  „prvý". Druhý pochod by som asi podporiť nešiel, ale ten prvý má existencialistický náboj v zmysle Kierkegaardovho „kroku viery".  Teda bláznivé vykročenie do neznáma. Palko a ostatní konzervatívni kresťania berú fakt, že ide o prvý „Dúhový pochod" hystericky, niečo ako keby do ich nepoškvrnenej krajiny mal zajtra vniknúť satan a všetko bude stratené. No mali by si skôr spomenúť, že aj oni boli niekedy prenasledovanou menšinou, a preto ak už pochod nepodporujú, tak by mali aspoň taktne mlčať.   Raz dávno mi môj spolubývajúci, ktorý je maďarskej národnosti, povedal, že chce, aby aj jeho deti vyrastali s pocitom menšiny. Totiž kto zažije pocit menšiny a občasné poníženie s tým spojené, získava schopnosť empatie, spolupatričnosti a lepšie rozumie ľuďom.  Kto tento pocit nezažil, prišiel o veľa.   Nalejme si čistého vína. Ani Palkovi kresťania a ani Weisenbacherovi homosexuáli nie sú tie najviac diskriminované skupiny. Zvoliť si jednu z množstva identít, ktoré človek má a podľa toho ho posudzovať a robiť z neho obeť, je hlúposť. Mladý Róm, ktorý vyrastal v detskom domove a je povedzme aj gay to určite nemá v živote ľahké, rovnako ako napríklad chudobní kresťania v krajinách s moslimskou majoritou. Ale vyslovovať paušálne súdy na základe jednej identity je podľa mňa nepostačujúce.   Na minuloročnej Pohode sa mi páčil výrok Fedora Gála, ktorý povedal, že sloboda je stav mysle a nie politiky. Sú ľudia, ktorí dokážu byť slobodní za každého režimu. Každá menšina si musí uvedomiť, že jej životný štýl prináša vyššie náklady spoločenskej akceptácie. Podobne, ako na mieru vyrobený produkt bude vždy drahší ako ten sériovo vyrábaný. Náklady akceptácie menšín sú vyššie, ale aj vďaka tomu je aj hodnota takéhoto životného štýlu vyššia. Práve stratou statusu menšiny sa môže vytratiť aj polovica spolupatričnosti a vzrušenia.   To je ten paradox. Ideológie často dosiahnu opak svojich cieľov. Komunisti dokázali ožobráčiť proletariát tak, ako by sa to nepodarilo nijakému kapitalistovi a konzervatívna politika už roky dosahuje presný opak než jej deklarovaná snaha o zachovanie hodnôt a napríklad boj proti drogám. Napríklad stačí zakázať Absint a hneď bude príťažlivejší. Môj francúzsky kamarát si ho vždy keď je v Španielsku kúpi, ale keby Absint bol vo Francúzsku legálny, tak oňho pravdepodobne ani nezavadí. Rovnako ako dekriminalizácia drog vedie k nižšej spotrebe (napr. porovnanie Holandska a Francúzska), lebo sa vytratí pôžitok zo zakázaného, tak aj „dekriminalizácia homosexuality" bude mať pravdepodobne rovnaký efekt. Teda, keby Palko uvažoval racionálne, tak by mal byť zajtra na čele Dúhového pochodu.   Na tom vtipnom „pochode proti pochodu" bola aj moja mama, ktorá na kameru hovorila, že „je z desiatich detí a má päť detí, a preto veľmi podporuje rodinu."  To isté, ale môže povedať každá druhá Rómka. Verím, že väčšina homosexuálnych párov by dokázala vytvoriť lepšiu rodinu, ako bola tá, v ktorej som vyrastal. Mame som ale veľmi vďačný a vážim si ju. Aj ona mi pomohla pochopiť to, za čím určite smútia aj viacerí bývalí disidenti: „Byť diskriminovaný znamená byť slobodný".       Páči sa mi motto na blogu Roberta Mihalyho: „Som človek, ktorý navôkol vidí iba ďalších ľudí, nikoho viac a nikoho menej". Ja by som dodal, že „len ak vidíme iba človeka, dokážeme vidieť viac ako len jeho".         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jakub Šimek 
                                        
                                            Lenivý kapitalizmus
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jakub Šimek 
                                        
                                            Križiak na nežnej križovatke
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jakub Šimek 
                                        
                                            Tvrdiť, že sme zlí je absurdné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jakub Šimek 
                                        
                                            Nerobme z komára Procházku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jakub Šimek 
                                        
                                            Boj o dušu a text
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jakub Šimek
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jakub Šimek
            
         
        jakubsimek.blog.sme.sk (rss)
         
                        VIP
                             
     
         Študoval som medzinárodné vzťahy na FMV EUBA, v súčastnosti pracujem ako projektový koordinátor v treťom sektore.  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    98
                
                
                    Celková karma
                    
                                                3.16
                    
                
                
                    Priemerná čítanosť
                    1671
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       RTVS pred voľbami spojila šmejdov s logom Quatra
                     
                                                         
                       Vznikne „prezidentská“ strana?
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Rodová rovnosť ako kultúra života
                     
                                                         
                       Päť vecí, ktoré neboli v pastierskom liste
                     
                                                         
                       Ako som dostala lekciu za slovenských biskupov...
                     
                                                         
                       Berme si späť svoju krajinu, urobme revolúciu, natrime to tým hore!
                     
                                                         
                       Do úmoru pracovali a keď si chceli odpočinúť, napádali ich levy...
                     
                                                         
                       Juvenilná justícia: demagógia (nielen) v Markíze
                     
                                                         
                       Mravec z Gorily: Dali sme oligarchom absolútnu moc
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




