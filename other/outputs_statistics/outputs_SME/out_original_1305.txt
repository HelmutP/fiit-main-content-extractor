
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Sýkora
                                        &gt;
                Vojenské
                     
                 Prehľad spomienok na základnú vojenskú službu z internetu 

        
            
                                    29.4.2009
            o
            7:25
                        (upravené
                13.4.2011
                o
                9:01)
                        |
            Karma článku:
                10.40
            |
            Prečítané 
            11585-krát
                    
         
     
         
             

                 
                    Nie je to ešte tak dávno, keď takmer každý mladý muž musel narukovať na základnú vojenskú službu a prinášal si z nej neuveriteľné spomienky život v zelenom, taký odlišný od civilného života. Väčšina bola zlých (tie sa zabúdajú prvé), niektoré však boli i veselé (vydržia dlhšie). Ale pretože všetky spomienky postupne blednú, až sa nakoniec úplne vytratia, rozhodol som sa okrem vlastných viac než dvadsiatich doposiaľ zverejnených  vojenských spomienok zverejniť zoznam aj tých ďalších cca. dvesto najlepších, ktoré som našiel na internete, aby som nimi odradil tých, čo by uvažovali o návrate povinnej základnej vojenskej služby.
                 

                 Samochvála smrdí alebo moje vlastné vojenské príbehy http://jansykora.blog.sme.sk/r/7213/Vojenske.html  
 Ako sme na vojne zachránili život súdruhovi Kaddáfímu http://www.ab-obchod.sk/download/kapitola1.doc http://www.ab-obchod.sk/download/kapitola2.doc   Ako sme na vojne opravovali bránu http://www.websky.cz/20030514-185612-spot.html   Ako sme sa na vojne chránili pred neutrónovou bombou http://cs-club.blogspot.com/2007/01/neutrn.html   Veselé príbehy z vojenskej katedry v Brne a z vojny http://www.inmagazin.cz/kategorie/valka-je-vul.aspx http://www.inmagazin.cz/kategorie/zelene-pribehy.aspx   Spomienky na vojenskú katedru v Praha a záverečné sústredenie http://strojnifakulta.com/roman.htm   Príbehy z vojenskej katedry v Bratislave http://siska.blog.sme.sk/c/13778/Vojenska-katedra-15.html  http://siska.blog.sme.sk/c/13780/Vojenska-katedra-25.html http://siska.blog.sme.sk/c/13783/Vojenska-katedra-35.html http://siska.blog.sme.sk/c/15013/Vojenska-katedra-45.html http://siska.blog.sme.sk/c/15014/Vojenska-katedra-55.html http://knor.blog.sme.sk/c/183940/Mesiac-v-kasarnach.html http://knor.blog.sme.sk/c/183566/Vojenska-katedra.html http://knor.blog.sme.sk/c/184008/Modra-knizka.html   Ako som bránil vlasť http://brada.blog.sme.sk/c/4189/Bol-som-na-vojne-len-na-semeno-Ako-som-branil-vlast-I.html http://brada.blog.sme.sk/c/4190/Vojaci-to-maju-na-salame-Ako-som-branil-vlast-II.html http://brada.blog.sme.sk/c/4193/Coze-je-to-za-vojaka-Ako-som-branil-vlast-III.html http://brada.blog.sme.sk/c/4194/Asi-som-bol-na-vojne-naozaj-len-na-semeno-Ako-som-branil-vlast-IV.html   Príbehy z vojenskej katedry v Prahe a zo služby v Čáslavi http://danielrehak.blog.idnes.cz/r/11135/Vojna.html   Zážitky z vojenskej katedry v Prahe http://klimes.mysteria.cz/clanky/ostatni/armada.htm  http://hlinka.blog.idnes.cz/c/168681/Vojenska-katedra.html   Vojna nie je kojná http://bakic.blog.cz/rubrika/vojna-neni-kojna http://my.opera.com/reportik/blog/index.dml/tag/army http://flaska.blog.idnes.cz/c/138843/Vojna-nebyla-kojna-fakt-ne.html   Neuveriteľný príbeh zo strážnej služby http://www.militaria.cz/cz/clanky/benes-noviny/mimoradna-udalost.html   19 príbehov z románu „Bol som Puchna XIII" o pohraničníkoch  http://www.tn.psg.sk/prezenta/knp/poviedky.htm   Jaminau - veľmi veľa veselých príbehov, odporúčam čítať od najstarších http://jaminau.bloguje.cz/   Román „Vyzývatel" - rok v elitnom bojovom útvare u tankistov. Taká je skutočná pravda o armáde. http://www.pismak.cz/index.php?data=readcoll&amp;id=349 1. diel http://www.pismak.cz/index.php?data=readcoll&amp;id=498 2. diel  Zážitky Rossa Hedvička - veľká huba, ale píše prekrásne http://hedvicek.blog.sme.sk/c/58588/Jak-slobodnik-Balog-nedostal-opustak.html http://hedvicek.blog.sme.sk/c/58384/Vlna-nasili-v-hotelu-Posta.html http://hedvicek.blog.sme.sk/c/58368/Jak-jsem-dostal-opustak.html http://www.humintel.com/clanek31.htm  http://www.humintel.com/clanek39.htm http://www.humintel.com/clanek75.htm http://www.humintel.com/clanek92.htm   Zážitky Viktora Holmana http://holman.blog.sme.sk/c/17963/Ako-som-zabince-pil-piesok-sypal-a-za-10-rublov-babu-skoro-mal.html http://holman.blog.sme.sk/c/23834/Preco-Viktor-namiesto-chlastania-travu-natieral-a-ine.html http://holman.blog.sme.sk/c/23974/MCSSP-aneb-co-cigan-to-muzikant.html   Veselé spomienky Vladimíra Kulíčka zo služby za Čepičky http://www.pozitivni-noviny.cz/cz/clanek-2007050020 http://www.pozitivni-noviny.cz/cz/clanek-2007050033 http://www.pozitivni-noviny.cz/cz/clanek-2007050047   Ako sme slúžili za Čepičku http://mozaika.sme.sk/c/3391935/Prvych-24-hodin-v-Cepickovej-armade.html http://mozaika.sme.sk/c/3407550/Vojenske-historky-z-vojenciny-z-50-tych-rokov.html   Najhorší vojak Varšavskej zmluvy - aspoň autor to o sebe tvrdí http://www.23.sk/index.php?option=com_content&amp;view=article&amp;id=68:najhori-vojak-varavskej-zmluvy-as-1&amp;catid=35:blog&amp;Itemid=79 http://www.23.sk/index.php?option=com_content&amp;view=article&amp;id=77:najhori-vojak-varavskej-zmluvy-as-2&amp;catid=35:blog&amp;Itemid=79 http://www.23.sk/index.php?option=com_content&amp;view=article&amp;id=79:najhori-vojak-varavskej-zmluvy-as-3&amp;catid=35:blog&amp;Itemid=79 http://www.23.sk/index.php?option=com_content&amp;task=view&amp;id=80&amp;Itemid=42   Reportáž psaná na zeleno („Jakou barvu má bílý kúň" a iné veselé príbehy) http://www.netusil.net/ke_stazeni////Report%C3%A1%C5%BE%20psan%C3%A1%20nazeleno.DOC http://74.125.77.132/search?q=cache:uczSqLy8Y_UJ:www.netusil.net/ke_stazeni/Report%C3%A1%C5%BE%2520psan%C3%A1%2520nazeleno.DOC+b%C3%ADl%C3%A1+barva+na+nekrology&amp;cd=2&amp;hl=sk&amp;ct=clnk&amp;gl=sk   Spomienky Vladimíra Kroupu http://vladimirkroupa.blog.idnes.cz/clanok.asp?cl=62587&amp;bk=47628 http://vladimirkroupa.blog.idnes.cz/clanok.asp?cl=36314&amp;bk=45016 http://vladimirkroupa.blog.idnes.cz/c/63810/Jak-jsme-porazili-imperialisty-prstem-na-mape.html http://vladimirkroupa.blog.idnes.cz/c/138956/Vojna-obrousila-lidi-na-kost.html http://vladimirkroupa.blog.idnes.cz/c/142964/Nasili-na-vojne.html#t2 http://vladimirkroupa.blog.idnes.cz/c/142670/Ahoj-vsichni.html http://vladimirkroupa.blog.idnes.cz/clanok.asp?cl=142670&amp;bk=78928   Historky z vojny http://chucpe.bloguje.cz/607595-historky-z-vojny-i.php http://chucpe.bloguje.cz/621657-historky-z-vojny-ii.php http://necasent.blogspot.com/2008/11/historky-z-vojny.html    Sláma: Ako sme na vojne v hovnách plávali http://slama.blog.sme.sk/c/103541/Ked-cistota-tak-cistota-alebo-jak-sme-v-hoach-plavali.html#t2   Percyho vojenské zážitky http://percy0.blog.sk/?s=4519   Falošný farár na vojne, nesprávny generál na kontrole psovodov, hra na trúbku, omylom na cvičení s ruskými tankami http://popismenku.blog.sk/detail.html?a=298c196b249af3624e2c68ea138e9b18 http://popismenku.blog.sk/detail-general---na-vojne-ii.--html?a=04ae2b929c63ad5567eb10f52d72cacc http://popismenku.blog.sk/detail-trubkar---na-vojne-iii.--html?a=a68cce5ae72688df418b753424efe910 http://www.popismenku.blog.sk/detail-videl-som-pink-floyd-s-gustavom-husakom---a-strazili-nas-ruske-tanky--.html?a=e165b4ea71cf4fd0974798e1c56f3a7f   Cvičný bojový poplach a nesprávna adresa, vychádzka s opitým podplukovníkom http://wewko.bloguje.cz/501332-cvicny-bojovy-poplach-co-se-pri-tom-muze-stat.php http://wewko.bloguje.cz/473644-vychazka-s-podplukovnikem.php   Spomienka z vojenskej basy http://fekar.blog.respekt.ihned.cz/c1-46027990-vratil-jsem-se-z-kriminalu   Spomienky z tankového práporu http://storch.blog.idnes.cz/r/5713/Zazitky-z-vojny.html http://nitka.blog.idnes.cz/r/16074/Vojna.html Spomienky z vojny http://www.teleplus.sk/modules.php?name=News&amp;file=article&amp;sid=2599 seizmické cvičenie http://www.teleplus.sk/modules.php?name=News&amp;file=article&amp;sid=2792 lekárske vyšetrenie    Spomienky bývalého ženistu z Domažlíc http://schultz.blog.sme.sk/c/191463/Ako-som-vykupal-velitela-a-vysluzil-si-riadny-kopanec.html http://schultz.blog.sme.sk/c/191381/Ako-som-uz-nebol-ku.html http://schultz.blog.sme.sk/c/192766/Civil-moj-milovany-uz-kracam-len-za-tebou.html#t2 http://schultz.blog.sme.sk/c/193316/Ked-sa-velenia-ujme-odbornik-dopadne-to.html   Výroky z MLP a PŠM http://vteriny.szm.sk/   Skutočné príbehy z vojenského prostredia http://armada.apbest.cz/story/index.html   Príhody z vojny (položky Co jsme prožili, jak jsme se bavili,...) http://www.csla.cz/vojenskasluzba/index.htm http://forum.csla.cz/blogs/vojensk_sluba/archive/2010/04/13/z-itky-z-vojny.aspx   Ako sa na vojne nasadzovali plynové masky http://kriz.blog.idnes.cz/c/91703/Kdo-se-nepobleje-nesoudruzi-s-ostatnyma-vojaky.html   Rok mimo http://www.mujweb.cz/www/pisar/pokus-rm.htm http://www.volny.cz/dedekm/vojna.html   Dva roky prázdnin http://oktabec.blog.idnes.cz/r/26333/Dva-roky-prazdnin.html   Zážitky z OT 64 http://www.ot-64.estranky.cz/clanky/pribehy/   Pravdivé príbehy z vojenskej služby http://www.tmavytien.szm.com/ http://necasent.blogspot.com/2008/11/historky-z-vojny.html http://www.birdz.sk/webka/Drkremes/blog/kategoria/02-pribehy-skutocne/ http://pohranicnik.bloguje.cz/775943-vzpominka-na-sluzbu-u-ps.php http://74.125.77.132/search?q=cache:LsuwLlObljUJ:pohranicnik.bloguje.cz/839273-vzpominka-na-sluzbu-u-ps.php+%22Vzpominka+na+sluzbu+u+PS%22&amp;cd=1&amp;hl=sk&amp;ct=clnk&amp;gl=sk http://interkom.vecnost.cz/2006/20060861.htm          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Sýkora 
                                        
                                            Ako som na vojne (ne)zložil vodičák na náklaďák.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Sýkora 
                                        
                                            Ako som v rodnom mestečku zaviedol digitálnu televíziu.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Sýkora 
                                        
                                            Ako sme na jadrovej elektrárni prerazili blokádu zelených.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Sýkora 
                                        
                                            Ako som arogantného taxikára odnaučil parkovať pred mojou garážou.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Sýkora 
                                        
                                            Ako sme na vojne učili pochodovať služobných psov.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Sýkora
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Sýkora
            
         
        jansykora.blog.sme.sk (rss)
         
                        VIP
                             
     
        Bežný občan so zmyslom pre humor.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    73
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    7221
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vojenské
                        
                     
                                     
                        
                            Policajné
                        
                     
                                     
                        
                            Ruské
                        
                     
                                     
                        
                            Zo života
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blogy veselé Tropkovské
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




