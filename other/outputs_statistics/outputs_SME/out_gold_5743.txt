

 Na antikampani sa zúčastnilo vyše dvadsať mladých ľudí, ktorí držali v rukách veľké písmená a spolu vytvorili súvislý názov Červenú Ficovi. Združenie mladých sa snažilo upozorniť spoločnosť na to, čo Róbert Fico počas štyroch rokov urobil niečo pre Slovensko a či má význam aby ďalej reprezentoval našu krajinu. 
 Parlamentné voľby sa už blížia. Konajú sa 12. júna 2010 a preto chce Nová Generácia vyzvať občanov aby namiesto hlasu dali vo voľbách Ficovi symbolickú červenú kartu v duchu futbalovej terminológie. Viac informácií na www. červenuficovi.sk. 

