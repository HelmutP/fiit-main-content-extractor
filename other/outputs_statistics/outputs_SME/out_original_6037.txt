
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Čo je nové na sme.sk
                                        &gt;
                Novinky na sme.sk
                     
                 Najvtipnejšie výroky komentátorov odteraz na Sme.sk 

        
            
                                    7.5.2010
            o
            10:06
                        |
            Karma článku:
                10.61
            |
            Prečítané 
            5110-krát
                    
         
     
         
             

                 
                    "Nemecký šprinter Cocot bezkonkurenčne vedie, a kde sú naši, ďaleko za ním, bodaj by sme aj my mali takých Cocotov". Túto a ďalšie najvtipnejšie hlášky slovenských športových komentátorov odteraz nájdete na Sme.sk.
                 

                 Šport je zábava nielen, keď ho robíte, ale aj vtedy, keď ho sledujete v televízii. S našimi slovenskými komentátormi hneď dvakrát. Ak sa tiež zabávate na tom, čo sa im občas podarí počas živých prenosov, potom by ste sa mali ísť pozrieť na stránku Niňajov svet, ktorú sme dnes spustili pod Sme.sk.   
 Na stránke nájdete to najvtipnejšie, čo zaznelo počas športových prenosov, môžete hlasovať za najvtipnejšie perly a pridávať vaše vlastné postrehy zo sledovania televízie.   Skúste si to. V diskusii nám môžete povedať, či vás to aspoň trochu zabavilo. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            SME hľadá redaktorov, online editorov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Prečo pod niektorými článkami nie je možnosť diskutovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Sme.sk hľadá HTML/CSS kodéra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Vitajte na úplne nových blogoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Čo je nové na sme.sk 
                                        
                                            Nová titulná stránka SME.sk: Viac správ, viac čítania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Čo je nové na sme.sk
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Čo je nové na sme.sk
            
         
        novinky.blog.sme.sk (rss)
         
                                     
     
         Novinky, zlepšenia a vôbec všetko, s čím sa chceme podeliť. 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    153
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    6917
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Novinky na sme.sk
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




