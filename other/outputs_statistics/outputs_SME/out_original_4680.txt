
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Emka Vrábliková
                                        &gt;
                Súkromné
                     
                 Keď srdce štrajkuje, aj dážď a blato na vrchoch musí ustúpiť 

        
            
                                    16.4.2010
            o
            15:08
                        (upravené
                16.4.2010
                o
                15:20)
                        |
            Karma článku:
                9.30
            |
            Prečítané 
            2016-krát
                    
         
     
         
             

                 
                    Služba ako každá iná. Z výjazdu do výjazdu, od jedného pacienta k druhému. Tu bolí, pichá, zle sa dýcha, slabo je. Meriame vitálne funkcie, podávame liečbu, vezieme do nemocnice, preberáme ďalší hovor. Počkať... ale veď dnes nemám v posádke lekára!
                 

                     Ešte nie je ani sedem hodín ráno, no na stanici je už rušno. Výmena posádky. Záchranári z nočnej dopíjajú kávu, pofajčievajú poslednú cigaretu, zberajú sa na odchod k svojim rodinám. Dobrú nočnú mali – po polnoci iba jeden výjazd, aj to nad ránom, vyspali sa. Veď aj my budeme mať isto kľud, aha, ako vonku prší. Tak nech len dobre slúžime, ozve sa z úst staničnej a dvere sa zabuchnú.       Prvé štyri výjazdy ubehnú ako voda. Epileptický záchvat, hypertenzia, opitý dedko spadnutý na schodoch, úraz hrudníka. Až na ten posledný prípad sa scenár opakuje: odoberiem anamnézu, zmeriam tlak krvi, glykémiu, napojím pacienta na monitor, natočím EKG, neurologicky vyšetrím, zaistím žilu, podám lieky, vypíšem papiere, odovzdám do rúk službukonajúcemu lekárovi. Ako študentka posledného ročníka sa teším, že mi všetko ide od ruky. Dôverujú mi, nechajú ma robiť samu, chvália ma. Vraj už aj bez dozoru môžem chodiť do výjazdov. A srdce mi pýchou až tak podskočí.       Na stanici sa mi kopia papiere, žalúdok zúrivo kňučí od hladu, no telefón už opäť vyzváňa. Babka diabetička, lieči sa na tlak, no akýsi nízky jej ho namerali: 96/60 a srdce bije ako o život. Ďaleko od Žiliny, na samom konci dediny. V lese, ale nájdeme to poľahky, veď nás budú čakať, stačí len rovno za nosom na sanitke uháňať. Ešte, že dnes jazdíme na záložnom vozidle – vpredu je miesta pre troch, akurát sa zmestím, nemusím vzadu sama sedieť ako po iné dni. A o to plnohodnotnejším členom posádky sa opäť cítim.       Na majákoch sa ponáhľame pomedzi autá, veď čo ak babka, takmer 80-ročná, už odchádza na onen svet? Na konci dediny čaká pani pod dáždnikom, kýva na nás, len nech už rýchlo ideme. Ale veď to vilka rovno pri ceste, ešte dobre, že sa nemusíme do tých lesov terigať. Podám liekový kufor vodičovi, sama beriem defibrilátor, záchranárke zhovievavo prenechám len papiere. Veď má už, chúďa, svoje odslúžené, sama sa na vysoký tlak lieči.       Pani nás naviguje, že len tuto za zákrutou, do kopca treba trochu podísť a už sme tam. Do kopca?! Veď tu len samé blato naokolo a dážď steká až za golier. V duchu už rátam každú možnosť transportu – sanitkou nevyjdeme ani náhodou, na plachtu – beniačovku, ako ju voláme – nás je málo a aj šmyknúť by sme sa mohli. To nebodaj hasičov ideme volať?       Po hodnej chvíli vojdeme k pacientke do izby, tá pod veľkou duchnou leží naobliekaná, z pece sála teplo, ako keby ruské mrazy udreli. Okamžite otváram okno, našťastie babka dobre vyzerá. Zvedavo na nás hľadí, pri vedomí je, všetko nám sama porozpráva. Už dlhšie máva výkyvy s krvným tlakom, na kardiológii jej toho veľa nepovedali, liečbu nezmenili. Tlak má v norme, na EKG nový nález nevidíme, len to srdiečko je akési rozbehnuté, na sekundu spomalí a už opäť uteká ako o preteky – 140 úderov za minútu. Skúsime podať lieky intravenózne, či sa tá tachyarytmia neupraví, no bez výsledne. Nič babka, ideme my do špitálu s vami, vyšetriť vás treba. Ale ako s vami dole tým kopcom?       Babka usmiata, že ona len po vlastných, žiadnu sedačku nechce, veď ešte pred pol rokom sama všetky zemiaky vykopala. A tak len šatku jej mám dať na hlavu, deku parádnu, čo má nachystanú, jej stačí cez plecia prehodiť. A pomaličky, myšacími krokmi dolu sama zíde, ešte aj o hubách porozpráva. V sanitke sa jej ležať nechce, že ona radšej posedí, veď nie je z nej žiaden invalid. Napojím ju opäť na monitor, stav nezmenený, srdiečko si bije po svojom. Aspoň, že to horšie s ňou nie je.       Babka čiperka by celú cestu len rozprávala, aký ťažký život mala, alkoholika si na krk zavesila a troje deti sama vychovala, uprostred noci koľkokrát do cudzej chalupy utekala. Biť ju chcel, naničhodník jeden, poza bučky s frajerkami sa vláčil, no po päťdesiatke ho rakovina skosila. Priznám sa, že dedinu celkom dobre poznám, aha, tam mamin rodičovský dom chátra, bo jej brat tiež všetko prepije. Tu rozjasnia sa babkine oči: „A či si ty Milkina dcéra? Šak to skoro ako moja vlastná, spolu sme batohy vláčili, aj tvojho deda Vráblika poznám, všetkých! Náááále, jaká si ty len pekná neúrekom po mamke...“       Snáď babka, ešte dlho požijete. Ja si dnes síce topánky od blata očistím, aj nohavice vyperiem, no na vás veru tak skoro nezabudnem...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Emka Vrábliková
            
         
        emkavrablikova.blog.sme.sk (rss)
         
                                     
     
        Dcéra svojich rodičov, sestra svojich bratov, snáď budúca záchranárka, súčasná pseudoredaktorka... s túžbou žiť pre seba a tých druhých.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2016
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




