

 Pamätám si na asi prvý billboard v Bratislave avizujúci film Votrelec 1. (Vtedy proste Votrelec). Bol zavesený na plote Medickej záhrady a dominovalo mu písmeno V modrej farby.  
 Bol to silný film. Napínavý, poučný. Najsam sa mi páčila tá scéna, keď sa Votrelec vyliahol.  
  
 Zrazu sa... 
  
 ...objavil... 
  
 ...Votrelec. Rozhliadol sa po okolí a zmizol v útrobách lode. Po stresujúcom napätí sa nakoniec ozvala časť symfónie Howarda Hansona a človek mohol ísť z kina domov s kožou naježenou na husací spôsob. 
 Aj dvojka bola výborná, zvlášť po prílet na planétu. Tak si predstavujem akčný, dynamický film.  
 Ostatné diely už nevynikli až tak, ale stále sa tešim na nový diel.  
 Piaty diel votrelca sľubuje veľa krvi už v upútavkach. Reklama na film zjavne evokuje prvý diel. A všetci vieme, čo treba robiť, keď sa niekde niečo pohne. Také organizmy je nutné zabíjať "na potkání". Čímkoľvek, čo máte poruke. Samopalom, plameňometom, granátmi.  
 Podľa reklamy na billboardoch to teda vyzerá na slušný masaker: 
  

