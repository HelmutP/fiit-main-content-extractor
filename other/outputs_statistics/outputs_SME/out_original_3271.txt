
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Bogric
                                        &gt;
                História
                     
                 10. rok vojny- 4. časť- Dohoda o mieri na päťdesiat rokov. 

        
            
                                    25.3.2010
            o
            8:00
                        (upravené
                25.3.2010
                o
                12:34)
                        |
            Karma článku:
                4.31
            |
            Prečítané 
            720-krát
                    
         
     
         
             

                 
                    Neveľmi som chcel písať o tejto dohode, ale mám za to, že jej znenie slúžilo v minulosti a možnože aj v terajších dobách slúži ako vzor pre dohody. A z druhého aspektu má táto pasáž veľký význam, lebo je to "pojítko" medzi nasledujúcimi rokmi vojny, ktoré Tukydides opísal vo svojej knihe- Historiai, preloženej Petrom Kuklicom, ako Dejiny peloponézskej vojny. Všimnite si prepracovanosť tejto zmluvy, ale ak chýba vôľa ju rešpektovať, tak tá zmluva môže byť hoci aj zo zlata... Vzhľadom k tomu, že tu chýbali takí ľudia ako Brasidas a Kleon, tak takmer nič nestalo ceste k trvalejšiemu mieru. Takmer nič...len Kleona začal nahrádzať Alkibiades. Mnohé mená osôb a názvy miest, ktoré tu budú spomenuté, vám budú povedomé a určite sa vám pri ich prečitaní vybavia ich činy, alebo osudy.
                 

                     " Aténčania a Lacedemončania, ako aj ich spojenci uzavreli dohodu obsahujúcu nasledujúce body a jednotlivé mestá ju potvrdili prísahou.   Čo sa týka spoločných svätýň: každý podľa obyčají predkov môže do nich vstúpiť a vykonávať obety, radiť sa vo veštiarňach a vysielať slávnostné posolstvá po pevnine i po mori bez akéhokoľvek strachu.   Posvätný obvod a Apolonov chrám v Delfách a takisto delfský ľud nech je podľa obyčají predkov nezávislý a samosprávny. Obyvateľstvo a jeho pozemky nech sú poplatné len svojej vláde a nech podliehajú svojim vlastným súdom podľa zákonov predkov.   Tento mier nech trvá 50 rokov, lebo ho uzavreli Aténčania a ich spojenci na jednej strane a Lacedemončania a ich spojenci na druhej strane bez ľstí a škody na zemi i na mori.   Zakazuje sa bojovať Lacedemončanom a ich spojencom proti Aténčanom a Aténčanom a ich spojencom proti Lacedemončanom a ich spojencom. Prípadné spory medzi nimi sa majú rozhodovať na súdoch a prísažne podľa dohôd.   Lacedemončania a ich spojenci nech vydajú Aténčanom Amfipolis. Obyvatelia miest, ktoré vydajú Lacedemončania Aténčanom, nech môžu slobodne odísť, kam chcú, s celým svojím majetkom. Mestá, ktoré platia daň uloženú za čias Aristeida, nech sú samosprávne a nech nie je dovolené Aténčanom a ich spojencom použiť zbraň proti nim, ak určenú zbraň zaplatili. Takýmito mestami sú: Argilos, Staigeiros, Akantos, Skolos, Olyntos a Spartalos, ktoré nepatria do nijakého spojenectva, ani so Sparťanmi, ani s Aténčanmi. Keď však dobrovoľne požiadajú o vstup do spolku s Aténčanmi, nech sú prijatí.   Mekyberňania, Sanajčajnia a Singajčania nech bývajú vo svojich mestách ako Olynťania a Akanťania.   Lacedemončania a ich spojenci nech vydajú Aténčanom Panakton.   Aténčania nech vydajú Lacedemončanom Koryfasion, Kyteru, Metonu, Pteleon, a Atalantu a mužov pochádzajúcich z Lacedemonu, ktorí sú v zajatí vo väzeniach v Aténach a na iných územiach, ktoré patria Aténčanom. Nech umožnia slobodný odchod tým, čo sú obliehaní v Skione a ostatným spojencom Lacedemončanov, ktorí sú v Skione a ktorých tam poslal Brasidas a tým spojencom Lacedemončanom, čo sú väznení v Aténach a v iných oblastiach patriacich Aténčanom.   Rovnako aj nech Lacedemončania prepustia na slobodu tých Aténčanov a ich spojencov, ktorých držia vo väzení.   Aténčania majú právo rozhodnúť podľa vlastného uváženia o Skiončanoch, Torončanoch a Sermylčanoch a o obyvateľoch iných miest ktorých majú vo  svojej moci." - možnože je to zlý preklad, ale ak je dobrý preklad a znenie tej zmluvy je autentické, tak stojí za povšimnutie, to čo som vysvietil. Tu sa prejavuje demokracia v praxi- ktorých majú vo svojej moci- a pritom to boli ich bývali spojenci, ktorí sa proti ním pre určité veci vzbúrili tým, že vystúpili zo spojenectva. A je ešte zarážajúce, že Aténčania na tie mestá mali nárok, aj napriek tomu, že ich nedobyli, lebo tie mestá ešte boli obsadené posádkou Peloponézanov. Tiež je zvláštne, že Aténčania akosi ignorovali vôľu obyvateľov dotknutých miest, keď im nedali na výber. Popritom, aj počínanie Lacedemončanov je veľmi čudné, lebo nechránili si svoje záujmy a nepotvrdili záruky, ktoré týmto mestám dal Brasidas. -   " Aténčania nech prisahajú Lacedemončanom a ich spojencom po jednotlivých mestách. V každom meste nech obidve strany zložia najvyššiu prísahu v prítomnosti sedemnástich občanov z každého mesta.   Prísaha nech znie následovne:-" Budem dodržiavať túto zmluvu a zachovávať mier spravodlivo a bez ľstí." Túto prísahu nech zložia aj Lacedemončania a ich spojenci pred Aténčanmi a ich spojencami a nech ju obnovujú každý rok. Nech všetký strany postavia pamätný stĺp v Olympii, v Delfách, na Istme, v Aténach na Akropole, v Sparte a v Amklách.   Ak niektorá strana niečo zabudla, nech je to čokoľvek, nech sa to nepokladá za porušenie prísahy, ale nech otvorene a čestne posúdi každý prípad, aby sa so súhlasom obidvoch strán, Aténčanov a Lacedemončanov, urobili zmeny v dohode."   - Táto dohoda bola uzavretá v Sparte za efora Pleistola v 24. deň mesiaca artemisia a v Aténach za archonta Alkaia 26. dňa mesiaca elefeboliona. podpísali ju všetci významní muži Atén a Sparty ( za Spartu okrem eforov aj obaja králi ), no ako som už naznačil, mala v sebe zárodky nespravodlivosti. Napríkad najviac ukrívdení sa asi museli cítiť obyvatelia miest, ktorym Brasidas a teda Lacedemon sľúbili ochranu a ktorá tým pádom prestala platiť. Taký Klearidas sa vzburil a odmietol vydať Amfipolis, v ktorom bol Brasidov hrob a až v Sparte mu osobne a dôrazne pripomenuli, že musí počúvať. Na zákade toho mal vydať to mesto, hoci obyvatelia Amfipolidy boli proti. K tomu sa začali nabaľovať aj požiadavky jednotlivých spojencov Sparty, hlavne Bioťanov pod vedením Téb, ktoré boli proti akejkoľvek zmlúve  s Aténčanmi. Títo boli nahnevaní na to, že mali vydať Panakton a nedostali za to alikvotnú náhradu, takisto mali k tomu výhrady Korinťania a dokonca Megarčania. Koryfasion ako ho nazývali Lacedemončania -bol Nestorov Pylos - taktiež bol problémovou oblanou a Aténčania sa ho nechceli vzdať. Vzhľadom k tomu, že v Aténach sa v tomto mieri najviac angažoval Nikias a on bol 1. archontom tesmotetom, tak táto dohoda uzavretá 12. apríľa 421 p.n.l., bola nazvaná podľa neho - Nikiov mier. No táto zmluva bola pre Lacedemončanov slabá a tak uzavreli s Aténami bilaterálnu dohodu, ktorá sa dá nazvať aj zmluvou o vzájomnej pomoci voči všetkým, koho ktorá strana určí za nepriateľa. Najviac sa obávali Argu, ktorý bol nezasiahnutý vojnou a mal tendenciu stáť sa spojencom Aténčanov. Argos v minulosti Lacedemončania porazili a donútili podpísať mier na 30 rokov a k tomu stratil kynurský kraj na ktorý si teraz Argejčania robili nárok.   Dohodu o mieri za Atény podpísali:- Alkiaos - archont eponymos; Lampon- kňaz a veštec a intrigán; Istmionikos- jeho funkcia je neznáma, ale pravdepodobne bol jedným z archonov tesmotetov, Nikias- vojvodca a štátnik, predstaviteľ umiernenej strany a zastánca mieru; Laches- aténsky stratég; Eutydemos-aténsky stratég; Prokles-tiež jeden zo stratégov; Pytodoros- archont a zároveň stratég, úspešne si počínal neskôr v Lakónii; Hagnon- Nikiov syn (no nie tu hore spomenutého Nikia), otec Teramena, bol spolustratégom Perikla, obliehal Poteidai a Amfipolis, kde boli jeho oddiely zničené; Myrtilos a Iolkios- nepoznám ich funkcie, no tiež predpokladám, že boli ďalší z archontov tesmotetov; Trasykles- vojvodca; Teagenes- vojvodca; Aristokrates- syn Skeliov, stratég a oligarcha; Timokrates- predpokladá sa, že bol otcom Aristotela, a spolu s - Leonom- ktorý obetoval svoje 3 dcéry na odvrátenie moru v 2. roku vojny; - Lamachosom- synom Xenofanovým, neskôr stratégom na Sicílii a s - Demostenom- podľa mňa- najschopnejším aténskym vojvodcom vďaka ktorému Kleon získal také úspechy aké získal - patrili k strane umiernených. Alkibiades- neskorší olympijsky víťaz na 91. hrách, stratég a štátnik, žiak Sokrata a priateľ Perikla, zastánca tvrdej línie proti Lacedemončanom, sa na tejto zmluve, ako jej signatár nenachádza.   Za Lacedemon ju podpísali:- Pleistoanax, syn Pausaniov a Agis, syn Archidamov- králi; Pleistolas- efor eponymos; Damagetos, - Chionis, - Metagenes, - Akantos - nepoznám ich funkciu, pravdepodobne efori, Ischagoras- vojvodca, priviedol posily Brasidovi na Chalkidiku; Filocharidas- bol spolusignatárom prvej zmluvy na 1 rok; Zeuxidas, - Daitos, - Antippos, - Alkinidas, - Empedias, - Menas, - Lafilos - Lacedemončania o ktorých tiež nemám poznatky; Tellis- otec Brasida.   Až po podpísaní všetkých dohôd Aténčania prepustili svojich zajatcov, hlavne z ostrova Sfaktéria ( pričom Lacedemončania a ich spojenci to urobili ešte po podpísaní mierovej zmluvy a k tomu vydali mestá, okrem Amfipolisu, ktoré bolo neurčité a o ktorom jednali). Týmto sa skončilo prvých 10 rokov konfliktu, ktorý jedni nazývali dórska, alebo druhí podľa kráľa Sparty Archidama- archidamská vojna.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            23. rok vojny- 1. časť- Alkibiadove vojenské výpravy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Xenofón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            22. rok vojny- Úspechy a porážky aténskeho vojska.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            21. rok vojny- kapitola č. 2- Politika Farnabaza, osud Syrakúzanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Kapitola č. 1 – Pokračovanie Tukydida
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Bogric
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Bogric
            
         
        bogric.blog.sme.sk (rss)
         
                                     
     
        Kto nepozná minulosť, nepochopí súčastnosť a nebude vedieť, čo ho môže čakať v budúcnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    138
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    722
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vyhoďme do vzduchu parlament!
                     
                                                         
                       Elektronické mýto a oligarchická demokracia za vlády Róberta Fica
                     
                                                         
                       Starosta je vinný a čo tí druhí
                     
                                                         
                       Nehoráznosť a zvrátenosť
                     
                                                         
                       Prečo končím s blogovaním na SME
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Naši zákonodarcovia
                     
                                                         
                       Karel Kryl – nekompromisný demokrat aj po revolúcii
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Závisť aj po dvoch rokoch
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




