
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Valerián Valášek
                                        &gt;
                Šach
                     
                 Francúzska obrana 

        
            
                                    30.6.2010
            o
            7:41
                        |
            Karma článku:
                4.87
            |
            Prečítané 
            1641-krát
                    
         
     
         
             

                 
                    Pod písmenko "F" som zaradil fajnú francúzsku obranu. Dostala názov podľa korešpondenčnej partie Londýn-Paríž, hranej v rokoch 1834-1836, v ktorej francúzski šachisti použili toto otvorenie a zvíťazili. Do polovice 20. storočia to bola najpopulárnejšia polootvorená hra a často sa hráva aj v súčasnosti. Ide o korektné otvorenie, ale pre čierneho nie je veľmi ľahké, lebo vyžaduje dobrú znalosť šachovej teórie, presný sled ťahov a dávku trpezlivosti. Charakteristicky vzniká už v prvom ťahu úvodníkom 1. e4 e6.
                 

                 
  
   Ak čierny na bieleho ťah kráľovským pešiakom 1. e2-e4 nechce hrať žiadnu otvorenú hru, môže použiť napr. už preberanú Caro-Kannovu obranu a zahrať 1... c6, alebo môže hrať aj 1... e6, čím hra prejde do hry, nazývanej FRANCÚZSKA OBRANA.   Po logickom ťahu bieleho 2. d4 odpovie čierny 2... d5 a núti bieleho sa vyjadriť ohľadom navzájom sa napádajúcich pešiakov e4 a d5. Vyskytujú sa aj menej obvyklé varianty druhého ťahu bieleho, napr. 2. f4 alebo 2. Sc4, ale sú menejhodnotné, aj keď postup správnej hry za čierne proti nim tiež nemusí byť hneď zrejmý hlavne začínajúcim šachistom. Po 2. d3 d5 3. Jd2 c5 vznikajú tzv. moderné varianty. Po 2. Jf3 c5 sa otvorenie zas môže preklopiť do vôd sicílskej hry (príklady takýchto partií, ktoré sa mi osobne vyskytli v praxi pri hre po nete, si môžete otvoriť v dynamických prezentáciách kliknutím na farebne vyznačené linky v texte týchto viet).   V treťom ťahu môže biely previesť jednoduchý výmenný manéver 3. exd5 a v tejto výmennej variante sa potom pozične obe strany vyvíjajú regulérne často až do rošády zrkadlovo (jazdci na f3, resp. f6, strelci zas na polia d3, resp. d6 a potom malé rošády).   V prípade, že biely nechce čierneho pešiaka na d5 meniť, môže natiahnuť svojho e-pešiaka dopredu ťahom 3. e4-e5 a vzniká najčastejšie hrávaná, tzv. zatvorená varianta francúzskej obrany, v ktorej čierny neskôr atakuje zaklinený reťazec bielych pešiakov ťahmi c7-c5 a f7-f6. Príklad dobre vedenej hry za čierne v zatvorenej variante uvádzam v ďalšej peknej prezentácii z mojej turnajovej praxe (opäť vo farebne zvýraznenom linku), z ktorej sú zrejmé ďalšie motívy hry čierneho, vyvíjajúcej sa hlavne po dámskom krídle.   Inou možnosťou bieleho je pochrániť atakovaného pešiaka e4 v treťom ťahu dámskym jazdcom, a to buď ťahom 3. Jb1-c3 alebo ťahom 3. Jb1-d2. Tieto varianty sa nazývajú Steinitzov (ešte charakterizovaný 4. Jf6 e5), resp. Tarraschov a patria do repertoára skúsených klubových a turnajových hráčov, lebo vyžadujú veľmi dobrú teoretickú prípravu za obe strany. Partie skúsených šachistov po týchto úvodníkoch bývajú kvalitné a vyžadujú dobrý prehľad na šachovnici. Perlou v dnešnom mojom článku je v množstve ponúkaných prezentácií táto zriedkavá, tzv. MacCutcheonova varianta francúzskej obrany, ktorou v roku 1962 porazil majster sveta Tigran Petrosjan vyzývateľa v kandidátskom turnaji - samotného mladého šachového génia Bobbyho Fischera! Podľa môjho úsudku ide o najzložitejšiu variantu mne známeho otvorenia s dnes už dobre prebádanou teóriou vo svojich rôznych pokračovaniach, ktoré vedú všetky ku veľmi krkolomnej a neprehľadnej hre. Príklad takej zapeklitej pozície po 15. ťahu čierneho v jednej z podvariánt tejto hry dokumentuje nasledovný diagram:      Bežné pokračovania francúzskej hry sú pre skúsenejších šachistov za čierne všeobecne dobre hrateľné a poskytujú celkom reálne vyhliadky na remízu, ale aj zisk bodu, hlavne pre bohatú škálu prekvapivých zvratov po zaváhaní bieleho. Predpokladom zvládnutia francúzskych vôd je dostatočná trpezlivosť, lebo len správnou výstavbou udrží čierny krok s priestorovou prevahou bieleho, aby v neskorších fázach hry podnikol v správnu chvíľu protiofenzívu. Otvorenie sa hodí do vážnych majstrovských partií a skúšať ho môžu aj kluboví hráči na rôznej úrovni s cieľom nájsť nedostatky v teoretickej príprave svojich súperov. V bleskovkách často jedna zo strán spraví pomerne skoro fatálnu chybu a takto hrané partie slúžia potom len na pobavenie.   Pozn.1: V každej z ponúkaných dynamických prezentácií je nastavená východzia pozícia v inom ťahu (niekedy až v zaujímavom závere partie), ale  partiu si možno posunúť na začiatok ovládačmi pod šachovnicou, pripomínajúcimi ovládanie videa.    Pozn.2: Bohatosť prezentácií v texte dnešného článku vyplýva z obľuby francúzskej obrany u autora a z bohatých vlastných skúseností a množstva materiálu, ktorým autor disponoval.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Valerián Valášek 
                                        
                                            Altenburg
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Valerián Valášek 
                                        
                                            Gera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Valášek 
                                        
                                            Leipzig (Lipsko)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Valášek 
                                        
                                            Malý krušnohorský skanzen
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Valerián Valášek 
                                        
                                            Annaberg-Buchholz
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Valerián Valášek
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Valerián Valášek
            
         
        valasek.blog.sme.sk (rss)
         
                        VIP
                             
     
         Lekár-rádiológ, 41 rokov. Býva na Orave, momentálne pracuje v Nemecku. Záľuby: šport (bodybuilding, šach, tenis, windsurfing), počítač a internet. 
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    174
                
                
                    Celková karma
                    
                                                4.79
                    
                
                
                    Priemerná čítanosť
                    3333
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Medicína
                        
                     
                                     
                        
                            Peniaze
                        
                     
                                     
                        
                            Stravovanie
                        
                     
                                     
                        
                            Šach
                        
                     
                                     
                        
                            Počítač a soft
                        
                     
                                     
                        
                            Tovar
                        
                     
                                     
                        
                            Nezmysly
                        
                     
                                     
                        
                            Toto nemá chybu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            HIT Radio RTL
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Juraj Lörinc
                                     
                                                                             
                                            Katarína Džunková
                                     
                                                                             
                                            Tomáš Paulech
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            vlastná šachová stránka
                                     
                                                                             
                                            IQ test národa
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




