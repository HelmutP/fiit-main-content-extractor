



 
 
  
 
 
 
 
 
 
 




 
 
  



 Spravodajstvo 
 
 Spr?vy 
 Dom?ce 
 Zahrani?n? 
 Ekonomika 
 Videospravodajstvo 
 Reality 
 Kult?ra 
 Koment?re 
 Shooty 
 
 ?port 
 
 ?port 
 V?sledky 
 
 Slu?by 
 
 Blog 
 Vybrali SME 
 Predpove? po?asia 
 TV Program 
 Teraz 
 English news - Spectator 
 Magyar h?rek - UjSz? 
 


 Tematick? weby 
 
 Porad?a 
 AUTO 
 TECH 
 TV SME 
 ?ena 
 Prim?r (Zdravie) 
 R?movia 
 Vysok? ?koly 
 Cestovanie 
 Dom?cnos? 
 
 Pr?lohy a sekcie denn?ka SME 
 
 Rozhovory 
 TV OKO 
 V?kend 
 



 
Spr?vy
 

 
  



  Vybra? svoj regi?n  
 Z?pad 
 
 Bratislava 
 Levice 
 Nov? Z?mky 
 Nitra 
 Pezinok 
 Senec 
 Topo??any 
 Tren??n 
 Trnava 
 Z?horie 
 
 V?chod 
 
 Korz?r 
 Humenn? 
 Ko?ice 
 Michalovce 
 Poprad 
 Pre?ov 
 Star? ?ubov?a 
 Spi? 
 


 Stred 
 
 Bansk? Bystrica 
 Kysuce 
 Liptov 
 Novohrad 
 Orava 
 Pova?sk? Bystrica 
 Prievidza 
 Turiec 
 Zvolen 
 ?iar 
 ?ilina 
 



 
Regi?ny
 

 
  



 Praktick? slu?by 
 
 Druh? pilier 
 TV Program 
 Artmama 
 Deti 
 Dvaja.sme.sk 
 Fotky 
 Gul? 
 Horoskop 
 Inzer?ty 
 Jazykov? porad?a 
 Nani?mama 
 Napizzu.sk (don?ka pizze) 
 Pr?ca 
 Re?taur?cie 
 Torty od mamy 
 Tla?ov? spr?vy 
 ?ahaj 
 Zlat? fond (aj e-knihy) 
 


 Odborn? recenzie elektroniky 
 
 mobily 
 telev?zory 
 tablety 
 notebooky 
 fotoapar?ty 
 hern? konzoly 
 
 Tret? sektor 
 
 Cena ?t?tu 
 ?akujeme 
 Obce 
 Otvoren? s?dy 
 ?koly 
 Tender 
 Vysok? ?koly 
 



 
Slu?by
 

 
  



 
 Last minute dovolenka 
 Don?ka jedla 
 Reality 
 Z?avy 
 Autobaz?r 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 SME knihy a DVD 
 SME predplatn? 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 



 
Nakupujte
 

 
  
    

  
 
 
 
Kl?vesov? zkratky na tomto webu - roz???en? 
Presko?i? hlavi?ku port?lu
 
 
  
   
     
Sme.sk | TV OKO | Zora Jaurov?: St?le rob?me hrub? ?iary 
     
       







       
     
   
  
  
   
    
     
      
       


 
 
diskutujte VYTLA?I? UPOZORNITE NA CHYBU Po?lite:
E-MAILOM na facebook
VYBRALI.SME | ?al?ie
 
 
 delicious.com 
 Google Bookmarks 
 MySpace 
 Twitter 
 
 
 
 
rozhovor 
Zora Jaurov?: St?le rob?me hrub? ?iary 
 
 
 
 
Nakr?tko sa vzdialila z divadeln?ho sveta, aby sa ocitla vo svete, ktor? je divadlom s?m osebe. Pred p?r rokmi uverila v mo?nos? zmeny v slovenskej kult?re. Na ministerstve sa stala ?lenkou mladej ?asti t?mu Rudolfa Chmela, ktor? bol mlad?m v 60. rokoch. Po bezv?slednej snahe o z?sadn? zvrat v uva?ovan? politikov o kult?re, ktor? nepri?iel ani po na?om vstupe do Eur?pskej ?nie, sa rozhodla st? pri za?le?ovan? Slovenska do jedin?ho grantov?ho programu ?nie pre umeleck? a kult?rne projekty - Kult?ra 2000. Pr??inu kr?zy v slovenskej kult?re vid? v osudovej ned?vere finan?n?kov vo?i ?u?om, ?o chc? uchov?va? a vytv?ra? p?vodn? umeleck? hodnoty na Slovensku. "?t?t si predov?etk?m mus? ur?i?, ?o chce... Keby zajtra minister financi? povedal, ?e na kult?ru ned? ani korunu, aj to je kult?rna politika," tvrd? slovensk? koordin?torka programu E? Kult?ra 2000 - Zora Jaurov?. 
 
 
O tom, ?e na Slovensku trh zv?azil nad kult?rou, st?le diskutuj? ?udia, ktor? vzniku a preh?beniu tohto probl?mu nezabr?nili. Nie je na ?ase, aby ho za?ali rie?i? in?, mlad???  
Je to mo?n?. Ka?dop?dne jedn?m zo z?kladn?ch probl?mov slovenskej kult?ry je, ?e na?e kult?rne in?tit?cie funguj? ?astokr?t s filozofiou 19. storo?ia a nefunguj?cim mana?mentom 70. a 80. rokov. A ke?e je na?a kult?ra aj pomerne konzervat?vna, mnoh? ?udia, ktor? dodnes ur?uj? jej povahu, s? zo star?ej gener?cie. A tak?to podivuhodn? ?tvar sa m? naraz vyrovn?va? s realitou 21. storo?ia. M? sa za?leni? do akejsi zjednotenej Eur?py, ktor? takmer nepozn?. Navy?e v?etky diskusie, ktor? tu na tieto t?my prebiehaj?, s? ve?mi lok?lne - akokeby neexistoval svet okolo n?s a ?ili sme vo v?kuu. Pri?om sta?? ?s? do Ma?arska, ?iech, alebo Po?ska, sta?? na?rie? aspo? do tohto mal?ho kontextu, ke? u? nehovor?me o tom ?ir?om. 
 
 
 
 

 
Ako n?rodn? koordin?torka za Slovensko v grantovom programe E? Kult?ra 2000 pravidelne chod?te do Bruselu. Ak?ch ?ud? na tomto f?re stret?vate? 
Je to ve?mi zauj?mav? reprezentat?vna vzorka, nako?ko tento program m? n?rodn?ch koordin?torov z ka?dej krajiny ?nie a navy?e aj z pridru?en?ch kraj?n EHS, ?i Bulharska a Rumunska. Ide v??inou o mlad?ch, pomerne v?konn?ch ?ud?, ktor? maj? kontakty aj s v?konn?m kult?rnym sektorom, aj s ofici?lnymi ?trukt?rami. A daj? sa na nich od??ta? rozdiely medzi krajinami a ich kult?rnou politikou. 
 
 
Napr?klad? 
Vidno napr?klad, koho v tej ktorej krajine kult?rny sektor vlastne zamestn?va, a to sa t?ka aj ofici?lnych z?stupcov, diplomatov, ?radn?kov a politikov. Briti s? v??inou pragmatick?, s?stre?uj? sa na ve?mi konkr?tne ?lohy. Franc?zi v?dy prich?dzaj? s nejakou v?e?udskou filozofiou a hlavne maj? pre kult?ru neuverite?n? mno?stvo pe?az?. Nemaj? probl?m ?h?dza?" ...mili?nmi. (Smiech.) A potom s? tam nov? ?lensk? krajiny, medzi ktor?mi s? u? teraz o?ividn? obrovsk? rozdiely. Je tu Ma?arsko, ktor? si kult?ru ur?ilo ako svoju prioritu a vytvorilo pre jej rozvoj syst?m, ktor? Eur?pska komisia uv?dza ako pr?klad pre in?ch - dokonca aj pre svoje star? ?lensk? krajiny. No a potom s? tu, samozrejme, aj krajiny ako sme my, ktor? sa uv?dzaj? v tom opa?nom pr?klade.
 
 
Je v?bec niekto podobn? Slovensku v tom, ako svoju kult?ru zanedb?va? 
Naozaj m?m moment?lne pocit, ?e nie. Dnes je u? aj v Rumunsku, na ktor? si Slov?ci st?le tr?faj? pozera? akosi zvrchu, ove?a lep?ie zreformovan? kult?rny sektor. Rumuni v ?om presadzuj? ove?a progres?vnej?ie idey ako my na Slovensku. 
 
 
Nem?te komplexy z toho, v akom svetle sa jav? na?a kult?rnos? na medzin?rodnej ?rovni? 
Na jednej strane to tak samozrejme je, na druhej strane sa d? Slovensko pou?i? ako vypukl? a odstra?uj?ci pr?klad toho, kam to a? m??e dospie?. N? pr?beh pou??vam, ke? chcem pouk?za? na to, ?o sa m??e sta?, ak Eur?pska ?nia bude od niektor?ch oblast? d?va? ruky pre?. Kult?ra nav?zuje na ekonomiku, v ktorej s? pr?sne regulat?vy. Hospod?rske krit?ria ?nie s? probl?mom pre mnoh? vl?dy, ale kult?ra je postaven? na princ?pe subsidiarity. V eur?pskej politike neexistuje ?iadny mechanizmus, ak?m sa daj? vl?dy zaviaza?, ?e musia da? na kult?ru napr?klad 2 percent? HDP. Program Kult?ra 2000 je v?ak zalo?en? na spolufinancovan?. A hoci ?nia pon?ka na na?e pomery dos? ve?k? peniaze, po?aduje, aby rovnak? sumu do projektov vlo?ili subjekty, ktor? o grant ?iadaj?. Minim?lny rozpo?et projektu, ktor? m??e by? v tomto programe, je 100 tis?c EUR, ?o s? 4 mili?ny kor?n. Zohna? na Slovensku na kult?ru polovicu z nich - 2 mili?ny, je takmer nemo?n?. To znamen?, ?e v r?mci Eur?py nie je zaru?en? rovnocenn? pr?stup k tomuto programu. Na?a ??as? priamo z?vis? od na?ej dom?cej kult?rnej politiky a tu je ?nia ve?mi rada, ?e existuje princ?p subsidiarity a d?va od toho ruky pre?. 
 
 
Ste teda za regulat?vne pokyny od ?nie? 
Nie, ale ke? sa povie ?a", mus? sa poveda? aj ?b", alebo potom nerobme rad?ej ni?. Rozhodne v?ak nie som ve?k? pr?vr?enec prehnan?ch kompetenci? eur?pskych in?tit?ci? v r?mci ?nie. Navy?e, po tom, ako sa neprijala eur?pska ?stava, je ??m ?alej jasnej?ie, ?e je ve?mi ?a?k? postavi? Eur?pu na ?isto ekonomickom princ?pe. Ot?zka je, ?i sa d? n?js? nejak? in? idea, ktor? ju sp?ja. Ale ak tak? idea existuje, tak kult?ra v nej zohr?va k???ov? ?lohu.
 
 
Pre?o nie ste pr?vr?enky?ou ?pr?li? eur?pskeho" riadenia Eur?py? 
Vznik? tu zauj?mav? situ?cia, ke? nov? ?lensk? krajiny maj? na riadenie trochu in? n?zor oproti viacer?m star?m ?lenom. A niektor? star? ?lensk? krajiny si a? teraz za??naj? uvedomova?, ?o roz??renie vlastne znamenalo a d?sledkom s? ned?vne referend?. Pred p?r t??d?ami som bola na konferencii v Turecku, kde sa rozpr?valo o jeho mo?nom vstupe do Eur?pskej ?nie. Ist? pani z Franc?zska, b?val? podpredsedn??ka Eur?pskeho parlamentu, tam doslova povedala, ?e Eur?pa si teraz mus? d?va? ve?k? pozor na to, koho pr?jme a koho nie. Vraj pri ned?vnom vstupe desiatich kraj?n si nedala dobr? pozor a teraz sa m??e len ?udova?, koho v?etk?ho tu m? a nevie sa dohodn?? napr?klad na postoji vo?i vojne v Iraku. Rozpor tu teda je a aj bude. Na druhej strane sa mi zd? absurdn?, ako sa na Slovensku propaguje my?lienka, ?e s na?imi ekonomick?mi reformami si tu vybudujeme lep?? a in? syst?m, ako kdeko?vek v Eur?pe. 
 
 
Ako by sa pod?a v?s dalo u n?s obr?ti? hodnotov? poradie, aby sa kult?ra dostala medzi na?e priority, podobne ako to maj? Ma?ari? 
Hodnotov? ot?zky maj? svoju metafyzick? rovinu, ktor? ur?ite hr? svoju ?lohu, ale nakoniec v?dy treba najprv urobi? nie?o pragmatick?. To, ako sa dnes propaguje, ?e ekonomika je podstatn? a kult?ra pr?de a? potom, do ve?kej miery vypl?va z toho, ?e na Slovensku na politickej ?rovni existuje fat?lna ned?vera medzi ekonomick?m a kult?rnym sektorom. Pr??ina je v tom, ?e riadenie kult?ry nebolo od roku 1989 riadne zreformovan?. Pokusy o reformy, ktor? sa tu odohrali, situ?ciu len zhor?ili. 
 
 
Tak?e postoj finan?n?kov ku kult?re je logick?? 
Z?asti viem pochopi? ministra financi?, ke? hovor?, ?e nebude do kult?rnych in?tit?ci? nalieva? peniaze, preto?e sa v nich hlboko prepad?vaj? a neprin?aj? v?sledky. Ale to nie je syst?mov? pr?stup. Syst?mov? by toti? bolo v prvom rade uskuto?ni? reformy a vybudova? funguj?cu ?trukt?ru a a? n?sledne hovori? o tom, ko?ko pe?az? sme ochotn? do tak?hoto syst?mu vlo?i?. A na efekt?vne reformy tu predsa existuje ve?a pr?kladov. Ne?ijeme vo v?kuu, m?me sa kde in?pirova?. 
 
 
Ako si predstavujete prv? z?sadn? kroky? 
?t?t si predov?etk?m mus? ur?i?, ?o chce v kult?re dosiahnu? a teda ak? je jeho z?kazka vo?i kult?rnym in?tit?ci?m. To nie je ?iadny ?avicov? princ?p, ale norm?lna pravicov? ekonomika. Dnes tu v?ak nem?me zadefinovan? absol?tne ni?. Keby zajtra minister financi? povedal, ?e na kult?ru ned? ani korunu, aj to je kult?rna politika. Bolo by to mo?no dokonca lep?ie, ako to, ?o je tu dnes. 
 
 
Ako to mysl?te? 
Keby v programovom vyhl?sen? vl?dy st?lo, ?e t?to vl?da nefinancuje kult?ru na Slovensku, ned? jej ?iadne peniaze a kult?rny sektor sa s t?m mus? vysporiada? s?m, bola by to jasne zadefinovan? situ?cia. Potom by sme t?m napr?klad mohli argumentova? na r?znych eur?pskych platform?ch a treb?rs po?iada? o asisten?n? programy. Je to samozrejme absurdn? ?vaha a d?fam, ?e sa ?ou niekto nein?piruje, ale chcem t?m uk?za? na d?le?itos? jasne formulovanej politiky.
 
 
M??e v?bec kult?ra fungova? bez ?t?tnej podpory? 
V Eur?pe ur?ite nie. ?t?tna podpora v?ak mus? by? presne nacielen?. Vl?da by kone?ne mala presne pomenova?, ?o je pre ?u v kult?re d?le?it? a ?o chce dosiahnu?. Potom sa in?tit?cie m??u na demokratickom princ?pe uch?dza? o naplnenie jej predst?v. A to je len jedna z mo?nost?, s? aj in?, ale podstatn? je zadefinova? verejn? z?ujem.
 
 
Nebola by pre ?t?t u?ito?nej?ia odluka cirkvi u? aj pre tak? detail, ?e dnes zna?n? ?as? beztak n?zkeho rozpo?tu ministerstva kult?ry ide na platy k?azov? 
Osobne som za my?lienku odluky cirkvi od ?t?tu. Mysl?m si, ?e by to ozdravilo cirkev aj politiku. Navy?e by t?m zanikla za vlasy pritiahnut? proticirkevn? liber?lna agenda a vz?ah ?ud? k cirkevn?m in?tit?ci?m by sa vy?istil. 
 
 
V? man?el Du?an Jaura bol svojho ?asu kazate?om. K?e v?m v s?krom?? 
Pok??a sa, ale v??inou mu to ?plne nevyjde. (Smiech.) Po tom, ako vy?tudoval teol?giu a nesk?r religionistiku, dva roky k?zal. Dnes sa venuje r?znym vzdel?vac?m projektom v tre?om sektore. M?vame vyhroten? diskusie na niektor? t?my, ?o je asi norm?lne. On je mierne konzervat?vnej?? ako som ja, ale to je sk?r o type osobnosti.
 
 
Obaja ste ?ast?mi hos?ami rel?cie Pod lampou. Slovenskej telev?zii sa zvykne vy??ta?, ?e je zodpovedn? za ?boh? stav kult?ry na Slovensku. Do akej miery to plat?? 
Je to za vlasy pritiahnut? v??itka. Telev?zia je len mal? ?as? fenom?nu kult?ra a sk?r odr?a nejak? stav, ako ho sp?sobuje. Na druhej strane, rozpornos?, ktor? v ot?zke verejnopr?vnosti st?le existuje, je vyvolan? t?m, ?e rovnako nikto z v?konnej, ani z?konodarnej moci nezadefinoval, ?o sa od telev?zie ?ak?. A tak zrazu ka?d?, komu sa STV nep??i, m??e poveda?, ?e nenap??a svoju funkciu. 
 
 
V?m sa STV p??i? 
Ke? nast?pil nov? mana?ment STV, ve?mi som mu fandila, ako v??ina ?ud?, ?o trochu videli do toho, ?o sa v telev?zii dialo. Pri moment?lnom v?voji v?ak m?m jednu z?sadn? obavu - principi?lne never?m v?chodisku, ke? chce telev?zia pritiahnu? div?ka ist?mi primetimeov?mi rel?ciami, ktor? sa dot?kaj? dolnej hranice vkusu a kvality, a potom ho pl?nuje vychov?va? kvalitn?mi programami na Dvojke. Never?m, ?e to tak m??e fungova?. Div?k, ktor?ho telev?zia oslov? rel?ciou Dri??akoviny u? predsa nebude pozera? nejak? intelektu?lny program. 
 
 
Dnes sa v?ak debata okolo STV zvrtla na porovn?vanie jej komer?n?ch programov s Mojsejovcami. M? to zmysel? 
Mysl?m si, ?e ak STV pou??va peniaze da?ov?ch poplatn?kov, tak by aspo? v nejakom limite mala dr?a? ?rove? programu. Peniaze od koncesion?rov m? predsa pr?ve na to, aby ju zv?hod?ovali v trhovom prostred? a aby nemusela ?plne podliez? latku vkusu v mene nejak?ho komer?n?ho princ?pu, a aby si udr?iavala ak? - tak? kvalitu. Sledovanos? je ur?ite ve?mi d?le?it? - nem? zmysel vyr?ba? rel?cie, ktor? sleduj? traja ?udia - ale ke? STV t? pomyseln? latku podlieza, tak potom komer?n? telev?zie musia ?s? e?te ni??ie. P?tam sa, ?i pr?ve toto nebude probl?m Mojsejovcov. Ve? v?dy sa d? ?s? e?te ?alej, e?te ni??ie. M??eme napokon vysiela? aj ?iv? sex, a ?udia to bud? pozera?. Filozofia, ?e d?vame div?kovi to, ?o si ?iada, celkom nefunguje, lebo div?k bude pozera? v?eli?o. Tam, kde div?k nalad? len STV, bude pozera? telenovely, ke? ich vysiela, ale pokojne bude pozera? aj nie?o in?.
 
 
Vedenie STV svoje rozhodovanie od?vod?uje t?m, ?e sa mu podarilo ?t?tnu telev?ziu oddl?i?. Je to argument? 
To, ?e Rybn??ek STV oddl?il, je naozaj skvel?, ale trag?dia je, ?e t?, ?o ju pred n?m zadl?ili, za to nikdy neniesli ?iadnu zodpovednos?. Ak sa dnes ?lovek pok?si o kon?trukt?vnu kritiku, hne? naraz? na jasn? fakt, ?e teraz je STV na tom lep?ie oproti ned?vnej minulosti. Plat? v?ak, ?e aj napriek tomuto ?spechu kritika mus? by? mo?n?, ve? nikto nie je dokonal? a mali by nast?pi? norm?lne kontroln? mechanizmy. V STV s? progres?vne trendy, ale mnohokr?t za cenu, ktor? je diskutabiln?. T?m, ?e sa neurobili r?zne postihy niekoho, kto to predt?m cel? zbabral, je dnes znemo?nen? ak?ko?vek zmyslupln? diskusia. 
 
 
Nie je to v s?lade so zn?mou slovenskou trad?ciou v?etko preml?a?? 
?no, nikdy sa tu ni? neuzavrelo. St?le rob?me hrub? ?iary a tv?rime sa, ?e za??name odznova. Ale to nie je to ist?. Minister kult?ry Rudolf Chmel napr?klad odi?iel z ministerstva ako hrdina, hoci za tri roky toho ve?a neurobil. Intelektu?li z jeho gener?cie v?ak v momente, ak maj? pomenova? Chmelovu zodpovednos?, zm?knu a ni? nepovedia. 
 
 
Tak?e p?vodn? v??itka nasmerovan? na STV patr? sk?r ministerstvu kult?ry? 
Ministerstvo na dne?nom stave kult?ry ur?ite ve?k? podiel. V politickom zmysle slova je pr?ve minister zodpovedn? za to, ?i rob? reformy, alebo nie. Rudolf Chmel r?d hovoril, ?e nebola politick? v??a nie?o zmeni?, lebo t?to vl?da nemala kult?ru ako prioritu a ni? sa v nej nedalo presadi?. Zrejme to aj bola pravda, ale my to nem??eme s istotou vedie?. Od vytvorenia s??asnej vl?dy ministerstvo kult?ry ani raz nepredlo?ilo z?sadn? dokument, pr?vnu ?pravu, n?vrh z?kona, ?oko?vek, ?o by vl?da nesk?r neprijala a ??m by v tomto zmysle mohlo ministerstvo argumentova?. V kult?re ch?baj? dokumenty, ak?mi sa dnes m??u preuk?za? ministri zdravotn?ctva, alebo spravodlivosti, ktor? maj? pr?vo poveda?, ?e navrhli nie?o, ?o vl?da zamietla. 
 
 
Vy ste ministerstvo kult?ry opustili e?te sk?r ako Rudolf Chmel. Je dnes toto ministerstvo pot?paj?ca sa lo?? Potrebujeme ho v?bec? 
S??asn? nie, ale principi?lne ur?ite ?no. Zru?i? ministerstvo je do ve?kej miery populistick? zjednodu?enie. ?o t?m vyrie?ime? V?ade v Eur?pe, kde ministerstv? kult?ry neboli, za posledn?ch 10-15 rokov vznikli. Rie?enie vid?m v radik?lnej reforme celej in?tit?cie. V prvom rade by sa mala oddeli? politicko-administrat?vna a exekut?vna funkcia, teda financovanie kult?ry. Som z?stanca princ?pu arts council, ke? ministerstvo kult?ry pln? politick?, administrat?vnu a spr?vnu funkciu a pre financovanie je ur?en? ?plne in? in?tit?cia, ktor? je vzdialen? od politick?ch ?trukt?r a pribli?uje sa kult?rnemu sektoru. Krajiny, ktor? uplatnili pr?ve tak?to model, maj? pomerne kvitn?cu kult?ru.
 
 
Ak? model maj? Ma?ari, ktor?ch v ?nii d?vaj? za pr?klad? 
V Ma?arsku je bohato ?trukturovan? syst?m podpory kult?ry. Naproti tomu u n?s je ministerstvo kult?ry jej jedin?m zdrojom financovania. Neexistuje nijak? in?. Nem?me ani funk?n? z?kon pre sponzoring, ktor? by zv?hod?oval komer?n? subjekty, ak maj? z?ujem kult?ru financova?. Na?a vl?da sa jednoducho rozhodla, ?e vyl??i ak?ko?vek finan?n? mechanizmy, ktor? by umo?nili kult?ru podporova? nepriamo. V mnoh?ch ?t?toch, a aj v Ma?arsku existuje takzvan? kult?rna da?, ktor? prin?a stabiln? a siln? tok do kult?ry z priameho rozpo?tu. Funguje na princ?pe, ?e na ist? produkty, ktor? v??inou s kult?rou s?visia - napr?klad na c?de?ka, knihy, l?stky, DVD prehr?va?e a mnoh? in?, je uvalen? neve?k? da? od 1 do 5 percent. Tieto peniaze id? do kult?rneho fondu, ktor? ich rozde?uje sp? do kult?rneho sektoru syst?mom grantov, alebo priamych dot?ci?. A v ko?k?ch krajin?ch sa kult?ra financuje zo ?t?tnej lot?rie! Na?a lot?ria Tipos, ktorej v?etky pr?jmy sa musia prerozde?ova? v r?mci ?t?tu, naposledy zverejnila na internete spr?vu o tomto prerozdelen? za rok 2001. 
 
 
Ministerstvo m? teda ak?si monopol? 
To, ?e ministerstvo je jedin?m miestom, kde sa m??u ?udia v kult?re uch?dza? o podporu, je jednou z pr??in trag?die, ktor? dnes panuje v kult?rnom sektore. V??ina ?ud? sa v tejto situ?cii nau?ila akosi chodi? a ministerstvo kult?ry teda logicky nebud? kritizova?. K?m tu jednoducho niekto nenavrhne systematick? reformu, k?m sa tu neobjav? minister, ktor?mu sa podar? obnovi? d?veru medzi ekonomick?m a kult?rnym sektormi a ktor? bude chodi? na vl?du zastupova? z?ujmy kult?ry a by? partnerom ekonomick?m ministrom, ni? sa neudeje. My sme sa v?ak ned?vno v rel?cii Pod lampou dozvedeli, ?e to nie minister kult?ry alebo poslanci maj? robi? reformy, ale samotn? umelci. Nie je mi v?ak jasn?, ako by sa to malo realizova?.
 
 
?o sa t?ka praktickej kult?ry, u? dvakr?t ste boli ?lenkou poroty v celoslovenskej anonymnej s??a?i o najlep?iu poviedku. Vedia Slov?ci p?sa? po slovensky? 
Mysl?m, ?e ?no, napriek tomu, ?e dobre pozn?me n?zor, ?e my vlastne u? ni? nevieme a nem?me sa ??m prezentova?. Minul? rok som z ?rovne poviedok bola trochu ne??astn?, nemohli sme udeli? ani prv? cenu. Mo?no sa to stalo preto, ?e sa vtedy prihl?silo ve?a star??ch autorov. Tento rok som sa v?ak ve?mi te?ila, preto?e ?trukt?ra s??a?n?ch pr?c sa radik?lne zmenila. Prihl?silo sa ve?a mlad?ch ?ud? s nov?m pr?stupom, t?mami a ?plne in?m poh?adom na svet. 
 
 
Tak?e predsa len pom??e genera?n? v?mena? 
T? rozhodne mus? nasta?, ale spolieha? sa a ?aka? na ?u je trochu alibistick?. Jednoducho treba nie?o robi?.
 
 
 

	
		
			
Zora Jaurov? (1973) vy?tudovala divadeln? dramaturgiu na ?aBF V?MU v Bratislave. Absolvovala nieko?ko st?? v zahrani?? (Ve?k? Brit?nia, N?rsko). P?sob? ako divadeln? dramaturgi?ka, kriti?ka a prekladate?ka. Dva roky pracovala na Ministerstve kult?ry SR. Moment?lne je riadite?kou n?rodnej kancel?rie eur?pskeho grantov?ho programu Kult?ra 2000. Je ?lenkou poroty s??a?? Dr?ma a Poviedka (roky 2004 a 2005), publikuje vo viacerych periodik?ch. So svojim man?elom teol?gom Du?anom Jaurom ?uje v Bratislave.
		
	



 


 
 

 
  
 
 
 ?tvrtok 23. 6. 2005 | Text: TINA ?ORN? / Foto: MIRKA CIBULKOV? &amp;copy 2005 Petit Press. Autorsk? pr?va s? vyhraden? a vykon?va ich vydavate?. Spravodajsk? licencia vyhraden?. 
 
 
 
  
 
 
 
 

 
 
 
 

				 
        
         
 
 
  
 
 
          
           
      
     
       
 
 4 hodiny 
 24h 
 3dni 
 7dn? 
 PLATEN? 
 
       
 
	 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 17?839 
 
Taliban zabil v ?kole v Pakistane 132 det? 7?604 
 
Lek?r vzal ako ?platok 500 eur a f?a?u, u? ho obvinili 6?934 
 
Premi?r Fico ?iline neodpust? dlh za k?rejsk? dedinu 6?324 
 
Cynick? obluda: Medzi?asom na opustenom ostrove 5?854 
 
Cez obec Trhovi?te ?iel 109 km/h, pokutu odmietol zaplati? 5?104 
 
Obama podp?e nov? sankcie vo?i Rusku, Kerry hovoril opak 4?924 
 
?esko a Slovensko si vymenili ?lohy 4?225 
 
Protestuj?ci v Ma?arsku str?caj? dych, Fidesz ich m?tie nov?mi n?vrhmi 3?643 
 
?i ostanem v Slovane? Sp?tajte sa Maro?a, vrav? Vondrka 3?477 
	 
 
 
 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 79?008 
 
Taliban zabil v ?kole v Pakistane 132 det? 69?755 
 
Cynick? obluda: Medzi?asom na opustenom ostrove 48?403 
 
?esko a Slovensko si vymenili ?lohy 48?054 
 
Radovan Bondra m??e ?s? na majstrovstv? sveta. Je najmlad?? a najvy??? 40?426 
 
Cez obec Trhovi?te ?iel 109 km/h, pokutu odmietol zaplati? 37?964 
 
Prednostu z detskej nemocnice na Kram?roch prichytili pri bran? ?platku 31?566 
 
Erdogan odk?zal ?nii: Nechajte si svoje n?zory 29?359 
 
Lek?r vzal ako ?platok 500 eur a f?a?u, u? ho obvinili 27?997 
 
?noscu zo Sydney pol?cia zastrelila, zomreli aj dvaja rukojemn?ci 27?756 
 
 
 
 
 
?noscu zo Sydney pol?cia zastrelila, zomreli aj dvaja rukojemn?ci 189?836 
 
Za?al sa boj o ?eleznicu. ?lt? vlaky RegioJetu boli pln?, ale me?kali 113?501 
 
Putinov ropn? ?ok 99?560 
 
Prednostu z detskej nemocnice na Kram?roch prichytili pri bran? ?platku 79?320 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 79?008 
 
Taliban zabil v ?kole v Pakistane 132 det? 69?755 
 
Rube? st?le pad?, ropa m??e ?s? na 40 dol?rov 51?335 
 
Radovan Bondra m??e ?s? na majstrovstv? sveta. Je najmlad?? a najvy??? 50?974 
 
?esko a Slovensko si vymenili ?lohy 48?495 
 
Cynick? obluda: Medzi?asom na opustenom ostrove 48?403 
 
 
 
 
 
?noscu zo Sydney pol?cia zastrelila, zomreli aj dvaja rukojemn?ci 189?836 
 
?eleznice uk?zali svoje najlep?ie vozne. Porovnajte ich s RegioJetom 159?016 
 
Za?al sa boj o ?eleznicu. ?lt? vlaky RegioJetu boli pln?, ale me?kali 113?501 
 
Putinov ropn? ?ok 99?560 
 
U Kisku sa hl?si exek?tor, Ga?parovi? nechal ?al?? dlh 95?476 
 
Ke? sa Ham??k objav?, zatv?raj? obchody 95?046 
 
Prednostu z detskej nemocnice na Kram?roch prichytili pri bran? ?platku 79?320 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 79?008 
 
Pre drah? obedy v nemocniciach padli traja riaditelia aj ??f ?radu 77?072 
 
Taliban zabil v ?kole v Pakistane 132 det? 69?755 
 
 
 
 
 
?esko a Slovensko si vymenili ?lohy
 
 
Radovan Bondra m??e ?s? na majstrovstv? sveta. Je najmlad?? a najvy???
 
 
Prv? penzie z II. piliera nepote?ia
 
 
Pre?o sme tak? bohat? na ?kand?ly
 
 
??na buduje nov? ve?k? m?r, chce zabrzdi? p???
 
 
Z?pisn?k: Bradat? Conchita men? Rak?sko
 
 
Glv?? mal doda? zmluvu na pu?ky. Uk?zal dva listy
 
 
Sydney pom??e aj nemeck?m radik?lom
 
 
Premi?r Fico ?iline neodpust? dlh za k?rejsk? dedinu
 
 
Ga?parovi?a ako ?estn?ho ob?ana ?as? Bardejov?anov nechce
 
 
 
     
           
          
 
 
	
	 
	 
		Vybran?
		Najnov?ie
		Naj??tanej?ie
		 
	 
	
 
	 
 Zima je tu ? hor?ci n?poj spr?jemn? chladn? ve?ery 
 Za?ite kr?su beskydskej pr?rody na vlastnej ko?i 
 Vyberte peniaze z pan??ch!Vieme, kde zarobia aj 200-n?sobne viac 
 Ako zvl?dnu? predviano?n? stres bez vir?zy 
 Tepl? obed je d?le?itou s??as?ou d?a. Nevynechajte ho 
 Ani prechladnutie nezastav? doj?enie 
 Objavili sme viano?n? dar?ek, ktor? pote?? ?plne ka?d?ho 
 ?SOB Pois?ov?a je op? Pois?ov?ou roka na Slovensku 
 Nepr?jemn? vir?za: Komplik?cia pre cel? rodinu 
 Ako zvl?dnu? prechladnutie v tehotenstve? 

	 

 
 
	 
 Vernis? v?stavy ART BRUT v Brne do 15.janu?ra 
 Ako zvl?dnu? predviano?n? stres bez vir?zy 
 Tepl? obed je d?le?itou s??as?ou d?a. Nevynechajte ho 
 ?SOB venovala 10 000 eur na skvalitnenie onkologick?ch oper?ci? 
 V?etky inform?cie z v?ho telef?nu na z?p?st? 
 Rozpr?vkov? dar?ek na ka?d? de? od Baletu SND 
 ?tudenti Tren?ianskej univerzity vyhrali s??a? KIA 
 Jeden tablet, ve?a mo?nost? 
 Nov? podstr?nka o ADHD u dospel?ch 
 Ani prechladnutie nezastav? doj?enie 
	 

 
 
 
 
Kv?li pohodlnosti zbyto?ne prich?dzame aj o desiatky eur mesa?ne 10?220 
 
Top hodinky 2014 7?150 
 
Objavili sme viano?n? dar?ek, ktor? pote?? ?plne ka?d?ho 6?587 
 
Vyberte peniaze z pan??ch!Vieme, kde zarobia aj 200-n?sobne viac 6?208 
 
Sauna a prechladnutie - ve?k? nepriatelia 4?295 
 
Bojujete s v?rusom? Toto funguje! 3?253 
 
Success Academy chce pom?c? ?u?om vyhn?? sa pracovn?mu ?radu 2?920 
 
Ten spr?vny parf?m ako dar?ek 2?537 
 
Pr?roda pom??e prekona? prechladnutie 1?560 
 
Robotick? vys?va? ETA Bolero 1?559 
 

 
 

 
  
 


  
  
 


  
         
        
        


       
      
     
    
    
    

     
     
       
 Titulka 
 Spravodajstvo 
 REGI?NY 
 Zahrani?ie 
 Koment?re 
 Ekonomika 
 Kult?ra 
 ?port 
 TV.SME.SK 
 Denn?k 
 Predplatn? 
       
     
 
 
  
 
 
 
  
 
 
 Porad?a 
 Korz?r 
 Auto 
 Tech a veda 
 Zdravie 
 Horoskopy 
 Z?avy 
 Don?ka pizze 
 Denn? menu 
 Recenzie 
 Reality 
 V?no 
 Parf?my 
 Hodinky 
 Hosting 
 Tvorba webov 
 DVD 
 
 
 
 
     

   
   
  

 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	   
 
 



 
	
	
	 
Kontakty Predplatn? Etick? k?dex Pomoc Mapa str?nky
 
	 
		
		
		
		
		
		
		
		
		
		  
	 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia Osobn? ?daje N?v?tevnos? webu Predajnos? tla?e Petit Academy SME v ?kole © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  





 

 

 









 
 
 
 
 






