
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Kolman
                                        &gt;
                Politické
                     
                 Volebné výsledky v ČR 

        
            
                                    30.5.2010
            o
            1:03
                        (upravené
                30.5.2010
                o
                2:19)
                        |
            Karma článku:
                6.81
            |
            Prečítané 
            1544-krát
                    
         
     
         
             

                 
                    V ČR sa dnes o 14:00 uzavreli volebné miestnosti a začali sa sčítavať hlasy. Pri prvých odhadoch to vyzeralo priaznivo najmä pre nové strany ako TOP 09 a Veci verejné. V konečnom výsledku sa to napokon potvrdilo. Víťazom volieb sa síce stala ČSSD, vládu však pravdepodobne nezostaví a tak vládu na najbližšie štyri roky bude zostavovať ODS.
                 

                 Na začiatku chcem pogratulovať českému národu k výsledkom volieb, ktoré, podľa mňa, naznačujú pozitívny smer a zabránili "Gréckej ceste." Taktiež gratulujem občanom ČR k uvedomelosti mladých, prvovoličov, ktorí prišli voliť.   Predpokladám, že väčšina si už výsledky volieb pozrela, ale pre prípad uvediem zopár faktov:   Voľby vyhrala strana ČSSD so ziskom 22,08%,   Druhí skončili občianski demokrati z ODS, 20,22%,   Pomerne prekvapujúci výsledok zaznamenala strana kniežaťa Karla Schwarzenberga TOP 09 so ziskom 16,7%.   na 4. mieste sú komunisti z KSČM s 11,27%.   A poslednou stranou v parlamente bude so ziskom 10,88% strana Veci verejné s predsedom Radkom Johnom.   Do parlamentu sa prekvapujúco nedostala KDÚ-ČSL so 4,39%, ďalšie strany mimo parlamentu sú Zemanovci so 4,33% a Suverenita s 3,67% a so ziskom 2,44% skončili Zelení.       Ako som už naznačil v úvode, hoci ČSSD voľby vyhrala, zostavovať vládu bude pravdepodobne ODS, ktorá spravila rozumný krok vpred odchodom Mirka Topolánka, ktorý vyrovnával a občas aj prekonal nekultúrnosť prejavov jeho konkurenta Jiřího Paroubka.   Petr Nečas, pravdepodobný budúci premiér, ako bývalý minister financií, bola taktiež veľmi dobra voľba, je to sympatický človek, má aj skúsenosti a hlavne, je to človek, ktorý sa vyzná v peniazoch. Premiér ekonóm je podľa mňa veľká výhoda každej krajiny.       Druhou najsilnejšou stranou, ktorá bude pravdepodobne členom nastávajúcej koalície bude TOP 09. Charizma, skvelé vystupovanie, dôveryhodný prejav a rozumné, hoci nepopulárne predsavzatia z volebného programu Karla Schwarzenberga evidentne zapôsobili predovšetkým na mladú generáciu, ktorá ukázala svoju silu. Tú však nadobudne len v prípade, že sa do jej čela postaví niekto, kto ju spojí. Ako sa dokázalo, ten vodca nemusí ani byť mladý. Česi môžu byť hrdí, že majú v politike človeka ako je Karel Schwarzenberg, taktiež pre uznanie a rešpekt v zahraničných i domácich kruhoch. Myslím, že aj Slovensko potrebuje svoje knieža v politike. Predpokladám, že sa opäť stane ministrom zahraničných vecí.       Tretím a posledným členom budúcej koalície bude strana Veci verejné, ktorej predsedom je Radek John, publicista, spisovateľ a scenárista. Táto strana taktiež vo svojom volebnom "desatore" uvádza znižovanie štátneho dlhu a zníženie nezamestnanosti, takže skvelo zapadne do náčrtu prvých dvoch strán. Veľmi zaujímavý prístup, s ktorým rozhodne súhlasím, má táto strana aj k sociálnym dávkam, ktoré chce riadiť heslom "Potřebným ano, příživníkům ne!" Významným bodom je aj priama voľba prezidenta, starostov a županov, či ústavný zákon o referende. Toto je pre mňa veľký nedostatok v ČR, ktorý by mohol byť konečne zmazaný. Napriek tomu mám pocit, že Radek John a jeho spolupracovníci sú nevyspytateľnejší, ako sa zdá.       Volebné neúspechy v niektorých stranách spôsobili padanie hláv, pri ktorom by sa v hrobe obracal aj Robespierre. Okrem vodcu sociálnych demokratov Jiřího Paroubka odišli aj predseda KDÚ-ČSL Cyril Svoboda, Miloš Zeman (Zemanovci) a Ondřej Liška (Zelení).       V každom prípade to vyzerá, že Česi si zvolili novú cestu a tieto voľby mali pre ČR základné 3+.   + Smer: "Čím ďalej od Grécka, tým lepšie." (premiér ekonóm, program TOP 09)   + Vystupovanie: "Dosť bolo vulgarizmov a primitivizmu." (Odchod J. Paroubka a M. Topolánka)   + Mladosť: Prebudila sa mladá genrácia, prišli nové strany, oslabila sa silná dvojka ČSSD a ODS. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (41)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                        
                                    
                                        Matej Kolman 
                                        
                                            Stolová hra z niekoľkých papierov?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                        
                                    
                                        Matej Kolman 
                                        
                                            Smer vyhral, pravdepodobne však skončil
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                        
                                    
                                        Matej Kolman 
                                        
                                            Rozdelenie ministerských kresiel v ČR
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Kolman
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                                            
                    



         
            
                Matej Kolman
            
         
        kolman.blog.sme.sk (rss)
         
                                     
     
        študent, "politológ" :D
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    780
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Politické
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




