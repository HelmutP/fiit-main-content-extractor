

 Do Hainburgu sa z Bratislavy dá dostať autobusom, bicyklom aj autom. Je to kúsok cesty a žiadne prekážky v podobe colníc už na ceste nepostretnete. Smerom od Bratislavy je na konci Hainburgu také mini obchodné centríčko, kde nájdete dve veľkopredajne potravín, nejaké lacné šaty a na druhej strane cesty záhradníctvo, ktoré sa oplatí vidieť hlavne kvôli dizajnu.  
  
 Príjemne upravené trávniky a okolie vás doslova donútia, aby ste zasadli na kávičku do kaviarne na terase. 
  
 Obsluha je tak príjemná, že vždy rozmýšľam, či nemám nejakých súrodencov o ktorých ja neviem a oni o mne áno. Srdečné, priateľské privítanie je tu štandardom. Posledne sme dokonca narazili na čašníčku, ktorá s nami hrkútala po našom. V tejto kaviarni cítite, že sú radi, že ste prišli. 
 Po kvalitnej káve sa človeku zažiada ísť sa pozrieť na ľalie, či čo sú to za kvety.  
  
 Keď sú takto tri pohromade, človek im nemôže ani vynadať. A tak... 
  
 ...spláchne a poďho do prírody. Tesne pri mestečku je taký nejaký kopec - videli ste ho v strede prvej fotky.  
 Kopec sa dá zdolať. 
 
 Autom.  
 Pre masáž srdca aj peši - asi 5 kilometrové stúpanie do strmého kopca.  
 Pre rodinu s deťmi výlet na také 2 a pol hodiny. (Iná cesta.) 
 Samovrahovia, športovci, šialenci idú do kopca aj bicyklom, alebo behom. 
 
 Na kopci je príjemné ticho. Môžete sa do nemoty dívať do ďalekých krajov. Viedeň veľmi nedovidíte, ale letisko áno.  
  
 Teraz na jeseň sú prekrásne farby. Fľaky vľavo v pozadí - to je Bratislava. 
   
 Ak máte ďalekohľad, môžete si pozrieť, ako vyzerá. Ak ďalekohľad nemáte, môžete hodiť jedno ojro do ďalekohľadu, ktorý tam niekto dal, ako pasívnu sporkasu. 
  
 Prípadne sa pozrite na Devín. 
 Spomínam si, ako sme pred vyše dvadsiatimi rokmi sedeli na Devíne na tej lúčke čo bol amfiteáter. Pozerali sme sa nenápadne ďalekohľadom na kopce pred sebou. Rakúsko. Vzadu bol náznak Álp. Sloboda. Ale ďalekohľad sme mali dobre ukrytý. Ani fotiť tým smerom sa nesmelo. Chlapom so samopalmi, ktorí sa pri Dunaji pred Devínom potĺkali sa to mohlo znepáčiť. Bola to divná doba. Nevedel som si predstaviť, že raz sa bude dať bez pýtania ísť na kopec o pár kilometrov ďalej, akože len tak... 
 Ľahký vetrík však pripomína končiacu jeseň.  
  
 Ešte pohľad na krajinu pri Hainburgu.  
  
 Na druhú stranu.  
  
 Kuk na štvornohé potvory, ktoré od detí za ohradou drankajú sladkosti. 
  
 A už len krátka cesta späť do bežných povinností a života.  
 Ak budete mať cestu okolo, možno ešte bude pekné počasie. Odpočiniete si.  


