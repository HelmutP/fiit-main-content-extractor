

   
   
   
 Táto spomienka, ktorá ma sprevádza od detstva, stále bolí. Navyše, je celé roky obalená rúškom záhady, teda aspoň pre mňa. Písal sa rok 1971... 
 "Mami, mami, mamina zomrela", vykríkla som nad ránom a  vzápätí som vyskočila z postele a rozbehla som  sa do rodičovskej spálne s očami plnými sĺz, spotená  a vystrašená... 
 Starú mamu, maminkinu mamu, som volala mamina. Bola mi  druhou mamou a trávila som s ňou veľa času a veľmi rada. Milovala som ju. Osud mi však nedoprial užiť si jej blízkosti toľko, koľko by som si bola priala. Zahral si  s nami, s ňou, a moja mamina ochorela. Diagnóza bola vážna. Mne, ako dieťaťu  pravdu nepovedali.  Vedela som teda, že je vážne chorá, no netušila som, že jej čas medzi nami odmeriava posledné týždne, dni... 
 "Neplač, upokoj sa", hladkala ma maminka a snažila sa zastaviť môj plač a opakovanie  tej strašnej vety. Opakovala som ju ako mantru. 
 "Dobre vieš, že je v nemocnici a že ju liečia. Bol to len zlý sen, všetko je v poriadku, nemusíš sa báť", odprevádzala ma pomaly nazad do mojej postele, do detskej izby. Chvíľu pri mne posedela a šla si aj ona ľahnúť. 
 Dlho mi trvalo, než som sa upokojila a dlho trvalo, než som znovu odovzdala telo spánku. Prišlo ráno a náš zvyčajný kolotoč každému pridelil jeho miesto. Rodičia do práce, bráško do škôlky a ja do školy. Po škole som chodila do družiny. 
 Pani učiteľka  v družine mi opatrne oznámila, že naši museli za starou mamou do nemocnice do Martina, lebo sa jej pohoršilo. Že prídu po mňa neskôr. Nepovedala mi pravdu, že sa jej tak pohoršilo, že v noci zomrela... (Z nemocnice však našim volali dosť neskoro, až okolo obeda, čomu doteraz nerozumiem.) 
 Keď som si to neskôr dala všetko dohromady, tak som si s úžasom uvedomila, že čas úmrtia mojej maminy sa zhodoval s časom, kedy som sa na krutý sen zobudila. 
 Doteraz mám z toho zvláštny pocit a nakoľko si myslím, že niečo medzi zemou a nebom existuje, predpokladám, že mamina sa prišla so mnou  asi rozlúčiť... 
   
 Musím podotknúť, že sme žili v Ružomberku a mamina ležala v martinskej nemocnici. Vďaka tejto smutnej udalosti, ktorá veľmi zasiahla moju detskú dušu, som Martin znenávidela. Dávala  som mu za vinu, že mi mamina navždy odišla práve v nemocnici na jeho území. Martin som vyhodila z mojej mapy Slovenska a aspoň tak som sa mu mstila. 
 Čo však "čert" nechcel, po  dlhých rokoch po tejto smutnej udalosti,  som prácu našla práve v Martine. Nešla som sem rada, predsavzala som si, že nejaký čas to vydržím, ale priebežne si budem hľadať prácu inde. Ale vlak môjho života tu vošiel do depa a ďalej sa mu akosi nechcelo. Poslal mi do cesty životného partnera (Liptáka), doprial nám dcérku a tak tu žijem(e)   už celú večnosť. Musím však priznať,  že stále sa viac "doma" cítim na Liptove ako tu. Musím  tiež podotknúť,  že  tunajšiu nemocnicu obchádzam širokým oblúkom a pomoc radšej hľadám v inom meste. Slovami sa to ťažko vysvetľuje, ale na tú nemocnicu, sa to ukrivdené dieťa, ktoré  vo mne prebýva, stále hnevá a asi stále aj hnevať bude. 
   
 *** 
 M.H.Clark: 
 Dostane sa človek niekedy z toho, keď stratí niekoho, koho ľubi? 
 Nie, ale naučí sa byť vďačný osudu, že ho vôbec mal, i keď nie na dosť dlho... 
 *** 
 Mamina, chýbaš mi, ale ďakujem osudu aj za ten čas, ktorý nám daroval. 
   
   

