
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Bruno Sloboda
                                        &gt;
                Nezaradené
                     
                 Pasce pripravované štátom 

        
            
                                    31.3.2010
            o
            22:39
                        (upravené
                31.3.2010
                o
                23:21)
                        |
            Karma článku:
                6.40
            |
            Prečítané 
            906-krát
                    
         
     
         
             

                 
                    V tomto príspevku chcem rozobrať myšlienku o fungovaní štátu, ktorá mi napadla po tom, ako som sa stal ďalšou obeťou firmy Web-content. Rovnako pridám ešte príklady, ktoré pracovne nazvem „Pasca nachystaná bankou“ a „Pasca nachystaná nebankovými subjektmi“. Teraz si asi možno niektorí z vás položia otázku, čo má štát spoločné s týmito prípadmi. Iní zase naopak, možno začnú tušiť, kam mierim. Je to len samozrejmé, sme len ľudia a ako takí sme rôzni. No určite by som len ťažko hľadal niekoho, kto by už teraz pomerne jasne tušil, ako to moje „filozofovanie“ naozaj skončí. Poďme teda na to.  
                 

                 1. Prípad : Pasca nachystaná firmou Web-content.   Tí, čo sa stali tiež obeťou tejto firmy vedia o čo ide (spolu je nás už dnes aj asi okolo 3000, možno i viac), ale v krátkosti to vysvetlím. Predstavte si že ráno vstanete, otvoríte si internet a svoju mailovú schránku (napr. na Googli) a tam nájdete faktúru na sumu 60 EUR od nejakej firmy, v našom prípade Web-content (pre tých, ktorí sa chcú dozvedieť niečo viac o tejto kauze, kliknite na http://www.cms-antonak.sk/kauza-60-eurovych-faktur/ ). Z obsahu sa dozviete, že vám fakturuje to, že ste navštívili jej webovú stránku a využili jej služby. Snažíte si spomenúť, kedy ste na takej stránke boli, aké služby ste využili, no márne, nič vám nenapadá. Potom začnete rozmýšľať, ako by ste teda zistili, o čo sa vlastne jedná. Zavoláte na číslo, ktoré nájdete na faktúre (musí vám to aj niekto zdvihnúť), opýtate sa manželky, priateľov, oslovíte známeho, ktorý je právnikom, zavoláte na Slovenskú obchodnú inšpekciu, podáte trestné oznámenie na neznámeho páchateľa. Na konci toho enormného úsilia je zistenie, že firma na vás nachystala jednoducho pascu, že s tým môžete len málo niečo urobiť, ale môže vás tešiť, že nie ste jediný. Podstatou tejto kauzy je to, že ak niekto navštívi ich stránku (ktorá mimochodom obsahuje len „free“ programy, ktoré sú inde bezplatné a básne, ktoré sú tiež na iných stránkach, ale voľne dostupné), je mu ponúknuté vyplniť bežný prihlasovací formulár bez upozornenia, že je niečo spoplatnené. Výsledkom je už spomínaná faktúra, ktorú vám firma pošle po asi šiestich mesiacoch s tým, že sa platí dopredu na celý rok. A aby toho nebolo dosť, existuje aj iná skupina obetí, medzi ktorú patrím aj ja, ktorí na tej stránke vôbec neboli, ale faktúru na 60 EUR sme dostali tiež. Ešte dodám, že na tej faktúre, ktorú som dostal, nesedí ani moja poštová adresa, ani IP adresa, ktorú mi poslali na vysvetlenie, že to je adresa, z ktorej som sa na ich stránku prihlásil.                   2. Prípad : Pasca nachystaná bankou   Teraz naozaj len stručne. Ak chcete od banky nejaký úver, musíte prísť niekoľko krát do banky, vydokladovať rôzne veci, povypĺňať rozličné formuláre a na konci, ak spĺňate jej podmienky, podpísať zmluvu s bankou. Možno som na niečo zabudol, ale „iste to nie je dôležité“.  Súčasťou tejto zmluvy sú aj tzv. všeobecné podmienky. Teraz vám nepoviem kde presne to je, ale niekde je najmenšími písmenami napísané niečo ako, že banka si účtuje aj poplatky a banka môže tieto poplatky sama meniť a vy že s tým dopredu súhlasíte. Samozrejme potom neskôr, keď k tomu príde, vy sa len čudujete, koľko EUR navyše musíte tej banke platiť a ani neviete poriadne za čo. Zoberte si len takú upomienku, že ste nezaplatili včas (pri dnešnom zhone sa to môže prihodiť každému). Prvá vás stojí cca 15 EUR a druhá (nedajbože ju dostať) cca 25 EUR. A vaše zdôvodnenie: „Veď kto by už dnes čítal nejaké všeobecné podmienky na 4-5 husto popísaných stranách. Iný (aj banka) povie: „Tvoja chyba, mal si si ich prečítať“. Čiastočne majú všetci pravdu. No ja chcem poukázať na niečo iné. Čo to je, vysvetlím o chvíľu.       3. Prípad : Pasca nachystaná nebankovými subjektmi    A posledný prípad je všeobecne známy a tak ho nebudem vôbec popisovať. Len ho zhrniem tým, že poviem: „Dôverčiví občania „sadli“ na lep podvodníkom“.        Rozuzlenie.   Položme si teraz teda dve otázky, „Čo, alebo kto, je vinný ?“ a „Prečo sa to všetko stalo?“ Viem, určité odpovede som už naznačil. Mnohé ďalšie (i keď odlišné) by určite našiel každý z vás. Ja sa na to však pozerám „prísne“ z pohľadu jednotlivca/občana a sústredím sa na vzťah jednotlivca a štátu. A čo vidím? Paternalizmus. Zasahovanie štátu, kde sa len dá. Veľmi ťažko by sme hľadali oblasť, kde sa dnes ešte nenachádza štát ?    Americký významný politológ Francis Fukuyama vo svojej knihe Budování státu podle Fukuyamy napísal (Praha, 2004, s.13): „Štáty majú najrôznejšie funkcie, niektoré prospešné, iné škodlivé. Rovnaké donucovacie prostriedky, ktoré umožňujú chrániť vlastnícke práva (pozn. autora: aj iné práva a slobody občanov) a chrániť spoločnosť, im umožňujú zabrať súkromný majetok a zneužívať práva svojich občanov“.             Prvé, čo by som mohol z tohto citátu využiť, ak by som chcel ohovoriť štát, sú jeho škodlivé funkcie. Ale to by bolo príliš jednoduché a asi i obrovským sklamaním pre pozorného čitateľa. Ja sa zameriam na tie „dobré“ funkcie, teda také, ktoré štát neskrýva, ako napr. obrana, bezpečnosť, právny systém a pod. a potom na tie sociálne (prerozdeľovacie), ktorými sa štát priam hrdí, ako napr. sociálne dávky, podpory, zákonník práce chrániaci pracujúcich, na to, ako nedá (premiér) ani v kríze na nikoho dopustiť a pod.    Teraz sa zastavme. Vidíte (cítite) aj vy, tak ako ja, koľko dobra srší z týchto funkcií štátu ? Ak áno, potom sme na koreni veci. Veď načo by som mal o niečo usilovať, keď tu je niekto, kto ma neustále presviedča, že sa o mňa vždy postará! Načo by som bol ostražitý a pozorný vo všetkom, čo robím, keď je tu niekto, kto mi tvrdí, že zabezpečí právo a spravodlivosť všade okolo mňa! Načo by som sa zaujímal o to, kde čo kúpim, a za akú cenu, keď je tu vláda, ktorá ma presviedča, že sa postará, aby veľké medzinárodné spoločnosti nezvyšovali ceny. Účinok takejto politiky štátu nedá na seba dlho čakať a ja začínam vláde najskôr nesmelo a potom úplne dôverovať. Ba čo viac, začínam sa na ňu spoliehať.     No a teraz sa opýtam, no mohli sa aj tie všetky obete nechytiť do pascí, ktoré im boli nastavené, ak žijú v takomto prostredí? Asi nie, je totiž veľmi ľahké a príjemné (je to i ľudské) mať o seba postarané bez vlastnej námahy. Z dlhodobého hľadiska je to však veľmi nebezpečné a preto majme sa na pozore...                 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bruno Sloboda 
                                        
                                            Prosím, informujte každého na Slovensku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bruno Sloboda 
                                        
                                            Aby sa Ficova pretvárka nezmenila na klamstvo!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bruno Sloboda 
                                        
                                            Ako sa môže stať, že sa náš Ficoštát dá na grécku cestu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bruno Sloboda 
                                        
                                            Aby sa nezabudlo na hlas podobný Ficovi!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bruno Sloboda 
                                        
                                            Aby sa veci nezamietli pod koberec!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Bruno Sloboda
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Bruno Sloboda
            
         
        brunosloboda.blog.sme.sk (rss)
         
                                     
     
        Záujmy: politológia


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1374
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




