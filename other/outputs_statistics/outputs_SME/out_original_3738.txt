
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alena Škutchanová
                                        &gt;
                Medické
                     
                 Po nemocnici rovno za nosom 

        
            
                                    1.4.2010
            o
            13:25
                        (upravené
                1.4.2010
                o
                21:15)
                        |
            Karma článku:
                9.50
            |
            Prečítané 
            1573-krát
                    
         
     
         
             

                 
                    Čuch je ako zmysel často podceňovaný. Pritom má obrovský vplyv... (Varovanie: zľahka smrduté)
                 

                 Pojednanie o rúškach   Rúška, tie klasické, majú jeden význam. Chránia pacienta pred lekárom. Aby naňho lekár nenakýchal alebo nenadýchal bacily a inú háveď. Nayvše vyzerá to veľmi coolovo, odborne a zamestnane, keď sa po oddelení preháňate s rúškom spusteným na krku .   Rúško neochráni lekára pred pacientom. Teda aspoň nie po treťom nádychu. Takže, čo sa týka pachov, je to skôr imidžovka, dobrý pocit. Od tohoto zistenia som sa na ňu ako osobnú na ochranu pred smradom vykašlala.   Miesto toho som to začala riešiť takou malou hrou. Spočíva v tom, že hrám sama so sebou hru, že na koľko nádychov sa dá ktorá vec urobiť. Napríklad taká sadrová dlaha predkolenia sa dá odstrihať na päť nádychov. Nadýchnuť (ústami, nie sú v nich čuchové bunky) a ponoriť. Ach áno, pán mal tú sadru na nohe počas dvoch najhorúcejších júlových týždňov...       Tetuša - autíčko   Na psychiatrii vám môže byť dobre. Ibaže raz vás z nej pustia. No a ak sa tam chcete dostať naspäť, dá sa to napríklad demonštratívnym pokusom o sebevraždu.   Staršia pani vyplia isté nie smrteľné množstvo technického benzínu. Keď ju doviezli na príjem, mala ústa na široko lemované absorbčným uhlím a strašne ju sušilo. Kým jej púšťali infúziu na zavodnenie a rozhodovali, kam ju poslať... pani síce dali plienku, ale benzín tak popohnal pohyb čriev a produkt bol tak tekutý, že pretiekol. Tetuša a vôbec celá izba príjmu začala páchnuť ako veľmi pokazené autíčko...   Páchnuci človek je veľmi, veľmi zlá vec. Mozog kričí prastarými inštinktami, že uteč. Darmo ostatné, vycvičené a ráciom ovládané zmysly sa snažia ho prekričať, že je to predsa (chorý) človek a... Páchne lebo je chorý, alebo pretože ani sám sa ani nikto druhý ho dostatočne neobriadil,  alebo oboje súčasne a... bolo by pekné sa usmievať, prehodiť pár slov... ale , zvyčajne všetko, čo má schopné nohy, prchá do lepšie vetraných priestorov. Aj zdravotníci sú len ľudia a v tomto prípade nič, čo nie je nevyhnutné, nerobia. Horšie je, ak nie je kam utiecť, alebo takéhoto človeka posadia/zaparkujú/uložia vedľa vás v čakárni alebo na izbe.   To, z čoho je človeku pri chorom či mŕtvom zle, je totiž pach. Keď vidíte rozcupovanú mŕtvolu v televízii (CSI, napríklad, nie je to vyštvorčekované a dosť realisticky urobené), najskôr vás to nijak extra neznepokojí a môžete ďalej pokračovať vo večeri (špagety s mäsovo-paradjkovou omáčkou). Nie je to o jej realistickosti či nerealistickosti na pohľad, je to preto, že tomu chýba čuchový vjem. Týka sa to aj krvi, má taký zvláštny sladkastý železitý pach, a ak som v jeho prítomnosti pridlho, zvyknem byť nepokojná (aj keď nemusím, lebo o dotyčného je postarané). Ono - sú veci, z ktorých je zle aj otrlým a ostrieľaným zdravotníkom...       Teória a prax pachovej bomby   Mojou obľúbenou vôňou je Old Spice Whitewater.   V nejakom dokumente hovorili o nevražedných zbraniach (non-letal weapons) na rozohnanie davu. Jedným z nich je pachová bomba. Má dve zložky. Jedna je veľmi príjemná a jedna veľmi nepríjemná (ako jeden z nanepríjemnejších pachov pre človeka boli vyhodnotené ľudské exkrementy).   Napriek tomu, že upratovačky robili, čo mohli, záchod na chirurgii nesmierne páchol. A tak sa sestra rozhodla, že to porieši. Z poličky sesterne vzala moju milovanú voňavku, ktorú na oddelení nechal nejaký pacient. Celý záchod ňou intenzívne vystiekala. V tu ranu som pochopila, o čom pachová bomba je. Pachová bomba vám zmätie mozog.   Informácie z našich zmyslov sú pred tým, než sa dostanú do kôry, kde si ich signály uvedomujeme, prepájané v nižších častiach mozgu do rôznych častí mozgovej kôry nového typu, dobre vôľovo ovládateľnej (nazvime si to racionálnej). Preto napríklad dokážeme presne pomenovať, čo vidíme. Čuch je iný. Čuch ide rovno do starých častí kôry bez prepojenia. Preto sa čuchové dojmy len veľmi ťažko pomenovávajú. Ten istý, starý typ kôry tvorí aj systém zodpovedný za emócie a pamäť. A preto má čuch na ne taký obrovský vplyv - a navyše sa to dá len veľmi ťažko potlačiť!   Oná pachová bomba spôsobila zmätok a preťaženie v mojom mozgu. Netušila som, či zostať alebo utekať čo najďalej... Chaos v pocitoch, myslení, všetkom... Iba ten prastarý inštinkt "Uteč!"   Druhá zaujímavá kombinácia boli tie isté WC (tetoraz však bol zápach toaliet len mierny a vytváral také ľahké pachové pohubie) a vanilka. To už som bola presvčená, že môj mozog použil nejaký úhybný manéver a v snahe neregistrovať čosi ozaj príšerné tento vjem nahradil vanilkovou fantasmagóriou. Ako sa neskôr ukázalo, bola to číra realita - vanilkovo-jogurtový sprchový gél jedného pacienta. A ozaj mi ten deň zlepšil náladu.       Čuch je v nemocničných podmienkach taktiež užitočný. Po čase sa zo zdravotníka stane akýsi smradový someliér. Po mesiaci som sa naučila po pachu poznať dekubity, tažkú pankreatitídu a iné (teda diagnostika od dverí a niekedy, keď pach zahýba aj za roh...) Nielen tieto, či bežne rozšírené testovanie opitosti, ale aj veľa chorôb, napríklad cukrovka či zlyhanie pečene sa totiž prejavuje špecifickým zápachom. Jeho rozpoznanie môže uľahčiť a zrýchliť diagnostiku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Cisársky rez
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Anestézia na poslednej ceste
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Dotykoví
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Neprebudení
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Škutchanová 
                                        
                                            Vymysli si niečo pekné na snívanie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alena Škutchanová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alena Škutchanová
            
         
        skutchanova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Lekárka - anestéziologička skautkapoviedkárkaa ešte všeličo možné
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    39
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2294
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Anestéziologické
                        
                     
                                     
                        
                            Intenzívne
                        
                     
                                     
                        
                            Medické
                        
                     
                                     
                        
                            Skautské
                        
                     
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Bydžovský - Akutní stavy v kontextu
                                     
                                                                             
                                            Forrest Carter - Škola Malého stromu
                                     
                                                                             
                                            Reif Larsen - Mapa mojich snov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Miro Tropko
                                     
                                                                             
                                            Lenka Rubensteinová
                                     
                                                                             
                                            Marek Gajdoš
                                     
                                                                             
                                            Ľubica Kočanová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Moje obrázky
                                     
                                                                             
                                            Moje osobné stránky
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ľudia, ktorí chodia po štyroch
                     
                                                         
                       Pochod za právo na rozhodnutie
                     
                                                         
                       Úsvit a kultúrne zákulisia vedy o sexe. Prípad masturbácie.
                     
                                                         
                       Polovačka na fotky
                     
                                                         
                       Dobrý lekár vždy zachraňuje...
                     
                                                         
                       Čo radí autobusár, kým radí
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       V takýchto chvíľkach, má človek chuť, stále začínať odznova
                     
                                                         
                       Uväznená mitochondria a zrod komplexného života
                     
                                                         
                       Vrkoč
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




