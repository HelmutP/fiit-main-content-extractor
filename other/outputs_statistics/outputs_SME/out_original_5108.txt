
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Bogric
                                        &gt;
                História
                     
                 12. rok vojny- 4. časť- Nezhody Lacedemončanov s Elejčanmi. 

        
            
                                    23.4.2010
            o
            11:35
                        |
            Karma článku:
                3.62
            |
            Prečítané 
            679-krát
                    
         
     
         
             

                 
                      V predchádzajúcej časti som, vďaka Tukydidovi a jeho knihe o dórskej vojne, ktorú vydavateľstvo I. Zacharopula nazvalo Historiai, ktorá u nás vyšla v r. 1985 v edícii Tatran v preklade od P. Kuklicu, ktorú nazval Dejiny peloponézskej vojny, naznačil začiatky nezhôd medzi Elejčanmi a Lacedemončanmi. Tieto nezhody sa začali udalosťami v Lepreu, mali neskôr veľkú odozvu a sa stali aj precedensom v dejinách Hellady. Tukydides, okrem toho v tejto časti, opisuje niekoľko bojov a problémy lacedemonskej kolónie Herakleie.  
                 

                     " Lacedemončanom nedovolili Elejčania vstúpiť do posvätného obvodu, preto nemohli ani obetovať, ani súťažiť, lebo nezaplatili pokutu, ktorú im Elejčania uložili podľa olympijského zákona za to, že začali voči ním výpravu, napadli pevnosť Fyrkos a poslali v čase olympijského prímeria do Leprea svojich hoplitov. Boli odsúdení na pokutu dvetisíc min, dve miny za každého hoplita, ako stanovil zákon. Lacedemončania vyslali posolstvo, aby prednieslo námietky, že boli odsúdení nespravodlivo, lebo vraj v čase, keď vysielali vojsko, v Lacedemone nebolo ešte nebolo vyhlásené prímerie. Elejčania povedali, že v tom čase prímerie už platilo (oni ho najskôr uzavreli sami doma) a v čase, keď ho chceli uzavrieť nerátali s vpádom; Lacedemončania sa teda prehrešili. Lacedemončania im potom vysvetľovali, že keby Elejčania už vtedy boli presvedčení o ich previnení, nemali sa v Lacedemone ani ukázať a nie to ešte rokovať o prímerí. Po uzavretí prímeria oni (Lacedemončania) už proti ním nikdy neútočili. Elejčania opakovali stále to isté, že sa nedajú presvedčiť, že neutrpeli krivdu, ale ak im vrátia Lepron, nielenže ustúpia od peňažnej pokuty, ale sú ochotní zaplatiť za nich aj to, čo prináleží božstvu.   Keď Lacedemončania odmietli tento návrh, Elejčania prišli s novou požiadavkou, nech sa teda Lacedemončania nevracajú, ak nechcú, ale ak si predsa len želajú, aby mali prístup do chrámu boha Dia Olympského, nech pri oltári prisahajú, že pokutu zaplatia neskôr. Lacedemončania nesúhlasili ani s týmto návrhom a tak im nebol dovolený prístup do svätyne a prinášali preto obety doma. Ostatní Gréci sa na slávnostiach zúčastňovali, iba Lepreončania nie. Elejčania sa obávali, že sa obetovania môžu domáhať násilím, preto postavili okolo chrámu stráž z mladíkov. Na pomoc im prišlo ešte 1000 Argejčanov a taký istý počet Mantinejčanov a niekoľko jazdcov z Atén, ktorí čakali na slávnosť v Harpine. Všetkých účastníkov slávností sa zmocnili obrovský strach, že Lacedemončania na nich zaútočia, keď ich občana, Licha, syna Arkesilaovho, na závodisku napadli a zbili udržovatelia poriadku pri hrách. Jeho dvojzáprah totiž zvíťazil ale za víťaza bol vyhlásený Boióťan, lebo Lichas, ako Lacedemončan, nemal právo ( na základe predchádzajúceho verdiktu) pretekať. Vtedy Lichas, aby dokázal, že kone patria jemu vošiel na dostihovú dráhu a ovenčil kočiša. Preto ho usporiadatelia bitkou vyhnali z dráhy. Potom sa všetci začali obávať, čo sa bude diať. Lacedmončania sa však chovali pokojne a tak slávnosť pokračovala. Po olympijských hrách Argejčania a ich spojenci prišli do Korint, kde požiadali Korinťanov, aby sa pridali k ich spojenectvu. Boli tam aj poslovia Lacedemončanov. Hoci rokovania trvali dlho, účastníci sa na ničom nedohodli a keď nastalo zemetrasenie, rozišli sa do svojich domovov. Tak sa skončilo leto.   V nasledujúcu zimu sa Heraklejčania stretli v boji s niekoľkými tesálskymi kmeňmi. Tieto kmene žili v susedstve Herakleie a boli s ňou v nepriateľstve, lebo založenie a opevnenie toho mesta ohrozovalo ich záujmy. Hneď po založení tohto mesta robili všetko, čo bolo v ich silách, aby ho zničili. V tej vojne porazili Heraklejčanov a zabili lacedemonského vojvodcu Xenara a mnohých Heraklejčanov. "   Ako si môžete spomenúť, Herakleiu založili pri Termopylách v 6. roku vojny na žiadosť obyvateľov mesta Dóris, k odvráteniu útokov Oitaiov, ale aj z iných dôvodov, traja Lacedemončania Leon, Alkidas a Damagon spolu s ostatnými kolonistami, ktorí mali záujem o nové bývanie. Takže už od začiatku mali problémov dosť, lebo aj Aténčanov sa založenie tohto mesta nepáčilo a podnikali všetko možné, aby mesto zničili. Priamo ho nenapadli, lebo bolo dobre umiestnené a malo za obrancov Lacedemončanov, ale nepoznali skutočnosť, že sami obyvatelia neboli jednotní, lebo vodcovia si robili, čo chceli a vládli tyranským spôsobom. Preto pomerne často obyvatelia mesta posielali do Lacedemonu žiadosti, aby im dávali rozumných vládcov. Ale opak bol pravdou. Lacedemončania posielali za správcov Herakleie tých ľudí, ktorých sa chceli zbaviť a tak tu zahynul  Xenares, jeden zo spartských eforov, ktorý bol za pokračovanie vojny s Aténčanmi.   Takto sa skončila zima a aj dvanásty rok vojny. Hoci by sme tie udalosti, ktoré tu opíšem, mohli smelo zaradiť do 13. roku, ale pravdepodobne z dôvodu nadviazania na udalosti v Herakleie a aj preto, že tie udalosti, ktoré sa tu udiali, boli hneď na začiatku leta, tak pokračuje Tukydides v ďalšom opise pre 12. rok vojny po tej časti, kde zahynul Xenares. Ani smrť bývalého efora nepoučila Lacedemončanov v tom, že do správy kolónii by mali posielať schopných ľudí a tak ďalšie záležitosti za nich vyriešili Boióťania.   " Hneď na začiatku leta obsadili Boióťania Herakleiu, lebo bola po vojne oslabená a poslali preč Agesippida, Lacedemončana, ako zlého úradníka. Zmocnili sa mesta, lebo sa báli, že ho ovládnu Aténčania, keď Lacedemončania boli zamestnaní spormi na Peloponéze. Lacedemončanov toto počínanie Boióťanov veľmi nahnevalo.   V to isté leto aténsky stratég Alkibiades, syn Klionov, sa dal do spolku s Argejčanmi a ich spojencami a prešiel s neveľkým oddielom aténskych hoplitov a lukostrelcov na Peloponéz. Dostal pomoc aj od niektorých peloponézskych spojencov; Alkibiades robil všetko, aby upevnil vytvorené spojenectvo. Okrem iných nahovoril obyvateľov mesta Patrai, aby predĺžili mestské hradby až k moru a rozmýšľal, že sám postaví podobnú pevnosť pri Riu v Achái. Korinťania a Sikyončania boli proti, ak by im opevnenie mali spôsobovať škody."   V trinástom roku vojny bude výprava Lacedemončanov proti Leuktrám a Argu proti Epidauru. Ani jeden a ani druhý nedosiahol svoj cieľ. Došlo k určitým nezhodám medzi Aténčanmi a Lacedemončanmi, kde Lacedemončania tajne presunuli 300 vojakov do Epidauru, ako obrannú posádku a Argejčania to vyčítali Aténčanom, že dovolili prejsť ich nepriateľovi popri Aténach po mori.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            23. rok vojny- 1. časť- Alkibiadove vojenské výpravy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Xenofón
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            22. rok vojny- Úspechy a porážky aténskeho vojska.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            21. rok vojny- kapitola č. 2- Politika Farnabaza, osud Syrakúzanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Bogric 
                                        
                                            Kapitola č. 1 – Pokračovanie Tukydida
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Bogric
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Bogric
            
         
        bogric.blog.sme.sk (rss)
         
                                     
     
        Kto nepozná minulosť, nepochopí súčastnosť a nebude vedieť, čo ho môže čakať v budúcnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    138
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    722
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Politika
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vyhoďme do vzduchu parlament!
                     
                                                         
                       Elektronické mýto a oligarchická demokracia za vlády Róberta Fica
                     
                                                         
                       Starosta je vinný a čo tí druhí
                     
                                                         
                       Nehoráznosť a zvrátenosť
                     
                                                         
                       Prečo končím s blogovaním na SME
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                                                         
                       Naši zákonodarcovia
                     
                                                         
                       Karel Kryl – nekompromisný demokrat aj po revolúcii
                     
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                                                         
                       Závisť aj po dvoch rokoch
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




