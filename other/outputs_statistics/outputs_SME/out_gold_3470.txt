

   
 Ak si prečítate nejaké štúdie o postavení žien napríklad v EÚ, vo viacerých sa okrem priemerne nižších platov a iných problémov, autor/-ka dotkne aj problému domáceho násilia. Ženy sú často na svojich partneroch závislé (a neklamme si, nielen finančne). Ale ako sa finančná závislosť premieta do domáceho násilia mužov voči ženám? Nie je to len fakt, že žena sa zo strachu pred finančnými problémami obáva týrajúceho partnera v balíku opustiť. Ženy z toho istého dôvodu váhajú opustiť i muža, ktorý dokonca ani dostatočne rodinu nezabezpečuje. Scénár, keď muž stratí prácu, to sa dotkne jeho ega, on si problémy začne „riešiť" na partnerke, prípadne sa z toho istého dôvodu do celej situácie zamieša alkohol (ďalšia bezodná jama, kam miznú rodinné financie), nie je výnimkou. 
 Chcete viniť moslimov z týrania žien? Pozrite sa najprv na seba.  Ak vás, muži zo „západu", moslimovia aj v tomto predbiehajú (čo nie je celkom isté), prečo sa im, preboha, chcete vyrovnať?? 
 Ďalšia, podľa mňa výhovorka, týraných žien - deti. Nuž, pochybujem, že vidieť svoju matku zbitú, alebo psychicky utýranú a plačúcu, je pre ne tým správnym zázemím. 
 No a nezabudnime na kráľovnú nerovnoprávnosti medzi ženou a mužom - kresťanské náboženstvo. (Ako rodená katolíčka som do kostola chodila až do svojich sedemnástich a tak s ním mám veľmi osobnú skúsenosť, ale už moje detské polemické debaty s farárom na hodinách náboženstva ma zjavne predurčovali stať sa časom agnostikom. To len ak by ma chcel niekto obviniť, že neviem, o čom hovorím.) Nuž, v dobrom aj v zlom... Podľa mňa dosť veľká blbosť, ale čo už. Ešte raz zdôrazním - ak už chcete byť týrané vy samé, prosím, ale deti si vybrať nemôžu. Môžete za ne rozhodnúť len a len vy. 
 Riešenia sú viaceré a pekne pracujú spolu. Tu a teraz spomeniem dve. Rodinné zázemie, harmonická rodina. Ak nie je možná, radšej už ten rozvod. Inak si vaša dcéra bude myslieť, že veci, ktoré vo vzťahu nie sú v poriadku, sú normálne. A syn takisto. 
 Vzdelanie. Stále nedocenené. Bez vzdelania si ženy v tomto svete, kde muži stále profesne tak či tak dominujú, svoj rešpekt a nezávislosť nevybudujú. Lebo, či sa nám to páči, a či nie, aj partnerstvo má väčšiu šancu na úspech v prípade, ak sú si partneri aj vzdelanostne, aj zárobkovo aspoň približne rovní. 
 A vzdelanie dá žene podporu, ak sa hoci proti svojej viere rozhodne partnera opustiť. Lebo veriť v Boha neznamená slepo veriť cirkvi, a už vôbec nie zakomplexovanému chlapovi. Tak nebuďme hlupane. 
 (Toto je môj nedeľný príspevok pre tých, čo pravidelne nepočúvajú kázne, aj pre tých, ktorí kázne počúvajú, ale neberú ich ako kamennú pravdu, ale skôr ako dôvod na diskusiu.) 
   
   

