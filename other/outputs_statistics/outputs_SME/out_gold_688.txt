

 
Som mladá, celý život mám pred sebou, a skúseností málo. Mala by som vás počúvať, mala by som vás rešpektovať na toľko že každé vaše slovo mi bude sväté. A ja sa chovám ako malé hlúpe decko, ktoré nevie rešpektovať autoritu. Takto to vidíte vy. Ja nesúhlasím.
 
 
Kde ste prišli k svojim skúsenostiam? Prežili ste si ich. Tak ma nechajte tak, ja si chcem rozbiť hubu na vzťahu o ktorom ste mi hovorili, že je nezmyselný. Chcem vidieť všetko to zlé čo ste videli aj vy, aby sa zo mňa mohol stať skutočný človek s hodnotami. Nemá zmysel, aby ste ma zavreli do klietky, vysvetlili mi čo mám rada a čo budem v živote robiť. 
 
 
Prosím vás, nechajte ma žiť po svojom. Neubližujem vám hudbou ktorú počúvam, tak mi je nezakazujte počúvať. Neubližujem vám tým ako sa obliekam, tak mi prosím dovoľte aby som sa obliekala ako chcem. Viem čo ma baví a čo chcem dosiahnuť, tak prosím, nesnažte sa mi linkovať život...
 
 
Odpustite mi to, ale nebudem bábka vo vašich rukách. Nebudem ten kto splní vaše tajné sny. Nebudem to ja, kto bude robiť to čo ste vy nedosiahli. Mám svoju cestu a robím presne to čo ste robili vy, keď ste boli mladí. Lebo ak si túto cestu nevyšlapem sama, s bolesťou, sklamaniami a pádmi nemôžem byť sťastná. Pretože to čo by som žila by nebol život. 
 

