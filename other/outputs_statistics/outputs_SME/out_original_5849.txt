
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Pekár
                                        &gt;
                Nezaradené
                     
                 Grécko treba sprivatizovať... 

        
            
                                    4.5.2010
            o
            18:11
                        (upravené
                4.5.2010
                o
                18:53)
                        |
            Karma článku:
                11.25
            |
            Prečítané 
            1013-krát
                    
         
     
         
             

                 
                    Je podľa vás logické, aby sa všetky krajiny Európskej únie skladali na pomoc jednému členovi, ktorý falšoval doklady a klamal aby skryl svoju zadlženosť? Aby sa iné krajiny zadlžovali pre pohodlnosť Grékov? Podľa mňa a miliónov ďalších ľudí, ktorí majú zdravý rozum je odpoveď jasná: NIE.
                 

                 
  
     
 Ako si však poradiť s touto prasacou krajinou (PIGS) bez toho, aby sme im museli požičiavať horibilné čiastky, ktoré by viedli len k ďalšej zadlženosti a bezvýchodiskovému stavu? Netreba sa dlho zamýšľať a hneď sa sama ponúka jednoduchá odpoveď: Grécko treba sprivatizovať! Samozrejme, že nie doslova. Pre zjednodušenie predstavy si zoberieme obyčajnú rodinu, žijúcu povedzme na Slovensku.       Otec zarába a vôbec nie zle. Žena robí v domácnosti, majú dve deti a žije s nimi ešte aj babka. Táto rodina si žije veľmi dobre. Vždy majú všetko najnovšie, vozia sa v drahých autách a bývajú v honosnom dome. Okolité rodiny nežijú zďaleka na takej vysokej nohe, napriek tomu, že zarábajú rovnako alebo dokonca viac. Jedného dňa sa príde na to, čo bolo dôvodom. Rodina si brala jeden úver za druhým, v bankách predkladala falošné výkazy o príjmoch a žila vysoko nad svoje pomery. Zrazu sa na to príde. Čo sa bude diať? Prídu teraz všetci susedia a povedia: „Pozrite, vy si tak skvele žijete, míňate oveľa viac ako my a absolútne neviete hospodáriť. My sa vám poskladáme a dáme vám pôžičku. Aby ste mohli aj naďalej bývať v tom krásnom dome a jazdiť na piatich autách. Vrátite nám keď budete mať.“? Nie, to sa nestane. Miesto toho príde exekútor, zoberie im dom, autá, všetky drahé veci, celá rodina si poriadne utiahne opasky a začne žiť tak, ako zarába. Otec bude ešte rád že nešiel sedieť.       Presne podľa tohto príkladu by sa mala uvaliť exekúcia na Grécko. Nehovorím že musia vyrovnať celý dlh. Je veľa krajín ktoré sú zadlžené a medzi nimi nájdeme samozrejme aj Slovensko. Mali by však predať štátny majetok v takej výške, aby nepotrebovali finančnú výpomoc. Mala by sa zorganizovať verejná dražba pod dohľadom Európskej únie a privatizovalo by sa všetko, až kým by suma z dražieb nedosiahla čiastku, ktorú potrebujú. Platy v štátnej správe a dôchodky by sa znížili aj o 50 %, teda na reálnu úroveň, trináste a štrnáste platy by sa zrušili všetkým, nie len tým, čo majú plat nad 3000 Eur. Daňová reforma by mohla prebehnúť tak ako je plánovaná aj teraz. Gréci by skutočne pocítili, čo znamenajú ich sociálne istoty tak, ako nikdy predtým.       Po týchto zmenách by žiadnu finančnú výpomoc nepotrebovali, bankrot by im nehrozil a nabudúce by si trikrát rozmysleli, koho budú voliť a s čím budú spokojní.       To by bolo ideálne riešenie. Gréci by sa mohli búriť. Veď sa aj búria. A to im chceme darovať 110 miliárd. Môžu si za to však sami. Oni si zvolili túto cestu - vo voľbách. Malo im byť vopred jasné, že nie je možné žiť, ako žili oni, tak isto ako malo byť každému jasné, že BMG Invest bol podvod. V mojom obľúbenom programe na konci vždy odznie veta: Pamätajte si, keď niečo znie príliš dobre na to, aby to bola pravda, tak to pravda pravdepodobne nie je.   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (57)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Pekár 
                                        
                                            S korupciou nikto nebojuje, každému vyhovuje...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Pekár 
                                        
                                            Milujem MHD!!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Pekár 
                                        
                                            Ako som mal zlomené oko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Pekár 
                                        
                                            Nákup v BILLE
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Pekár
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Pekár
            
         
        pekar.blog.sme.sk (rss)
         
                                     
     
        Som študent vysokej školy s triezvym pohľadom na svet. Moje vízie sa niekedy môžu zdať nereálne. Vtedy sa treba pozrieť ešte raz.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2106
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Čoho sa vlastne bojime?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




