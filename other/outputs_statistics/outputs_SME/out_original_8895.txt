
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Kubuš
                                        &gt;
                Malta
                     
                 Malta. Dva dni na ostrove 

        
            
                                    16.6.2010
            o
            8:31
                        |
            Karma článku:
                9.15
            |
            Prečítané 
            2222-krát
                    
         
     
         
             

                 
                    Malý ostrovný štát vhodený do vĺn Stredozemného mora geograficky blúdi medzi Európou a Afrikou. História do neho vpísala množstvo príbehov a dnes sú spomienky na časy dávno minulé roztrúsené po celej krajine. Krásne pláže s azúrovým morom, megalitické chrámy zahalené rúškom tajomstva, mestá dýchajúce starobylou stredomorskou atmosférou, orientálne arabské vplyvy a čulý nočný život robí z Malty príťažlivú destináciu...
                 

                 Sú dva dni málo či veľa? Našťastie je Malta veľmi malá krajina, kde sa aj za dva dlhé dni dá zažiť veľké množstvo zážitkov. Keď ich človek spätne prežíva tak sa mu v mysli dva dni roztiahnú na celý týždeň, pretože jednoducho neverí, že to všetko prežil za 48 hodín. Bývali sme v mestečku Sliema, ktoré akoby splývalo s hlavným mestom Vallettou. Za ten krátky čas sme sa prechádzali tichými uličkami Mdiny kde sa raz za čas prešla kamennou dlažbou konská drožka, v susednom Rabate sme našli jaskyňu s podobizňou sv.Petra v rovnomennom kostole, odviezli sme sa polo rozpadnutým žlto-oranžovým autobusom do malého mesta Naxxar s aristokratickým palácom a odtiaľ sme maltskou krajinou šliapali peši až ku katedrále v meste Mosta. Objavovali skryté zákutia Valletty a napokon odkrývame históriu pri tajomných megalitických chrámoch Haggar Qim a Mnajdra. Napadlo nás, prečo sa nepozrieť aj na malý ostrovček Gozo? Už aj sedíme na trajekte kde fúka poriadny vietor a oboplávame okolo ešte menšieho Comina. Mestečko Mgarr na ostrove Gozo je rozťahané po priľahlých kopcoch obkolesujúcich záliv. Prístav hrá viacerými farbami. Svoj domov tu má množstvo typických maltských rybárskych lodiek Luzzu. Sú pomaľované výraznými odtieňmi modrej, žltej, červenej alebo zelenej a neodmysliteľne patria k histórii ostrova. Nad mestom stráži prístav neogotický kostol Panny Márie z Lúrd. Dodáva celému Mgarru akúsi vznešenejšiu atmosféru. Hlavné mesto ostrova Victoria (prezývané aj Rabat) leží vo vnútrozemí. Starý maltský autobus sa neúnavne prebíja dopravou a miestami máme pocit, že lietame. Šoféri z Goza sú preslávený pre svoj divoký štýl jazdy. „Koniec sveta“ ako sa zvykne prezývať Dwejre priťahuje svojim pozoruhodným Azúrovým oknom. Ostrov sa zvažuje prudko do mora a mohutné vlny bičujú ostré skalnaté pobrežie. Z pevniny vybieha do vĺn len úzky kamenný prst, ktorý sa namáča do slanej vody a vytvára okno. Pohľad cez neho je krajší ako cez stovky iných okien. More je rozbúrené a každý dotyk vlny so skalou ju premení na milióny malých častíc poletujúcich vzduchom. Skvelý pocit stáť na skalách tak blízko nebezpečného živlu, cítiť vôňu mora na tvári, odviezť sa trajektom pri západe slnka, kde sa jeho zlaté lúče ponárajú do vĺn, večer si sadnúť na skaly s ľuďmi čo mám rád, počúvať more, zahryzovať si pizzu, popíjať kinnie a len tak si byť na Malte...        Príbehy tichého mesta. Mdina   Stará zvonica v uličkách Mdiny   Katedrála sv.Petra. Mdina   Ospalé a tiché uličky Mdiny   Konská drožka v uliciach Mdiny   Kostol sv.Petra v meste Rabat   Upršané dopoludnie v Rabate   Socha sv.Petra v jaskyni pod kostolom. Rabat   Rabat   Pôsobivý kostol v mestečku Naxxar   Malý improvizovaný trh v Naxxare   Maltskou krajinou kráčame do mesta Mosta   Konečne ju vidíme! Známa Katedrála v meste Mosta   Mosta   Mosta   Plavíme sa okolo ostrovčeka Comino   Prístav Mgarr na ostrove Gozo   Dwejra. Azúrové okno   Čakanie na vlnu   Rozbúrené kamenné pobrežie na "konci sveta"   Malý tichý záliv s jaskyňou ktorou sa dá dostať k Azúrovému oknu   Hlavné mesto ostrova Victoria   Úzke uličky Victorie   Jeden z mnohých kostolov. Victoria   Typická loď "Luzzu" odpočívajúca v prístave   Katedrála sv.Jána v hlavnom meste Valletta   Interiér Katedrály sv.Jána   Katedrála sv.Jána   Valletta   Nákup priamo z balkóna   Vyspádované ulice Valletty   Pevnosť St.Elmo na konci Valletty   Ulicami Valletty   Megalitický chrám Haggar Qim   Megalitický chrám Haggar Qim   Megalitický chrám Haggar Qim   Megalitický chrám Haggar Qim   Megalitický chrám Mnajdra   Megalitický chrám Mnajdra   Megalitický chrám Mnajdra   Malá zátoka neďaleko Modrej jaskyne   Modrá jaskyňa  foto: Tomáš Kubuš, Valletta, Mdina, Rabat, Naxxar, Mosta, Mgarr, Victoria, Dwejra, Valletta, Haggar Qim, Mnajdra, 22.-23.4.2009   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Libanon 2014 (foto)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Buzkaši na brehu kirgizského Issyk Kul
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Sýrska Resafa. Prekvapenie v púšti
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Tatev. Najkrajší kláštor Arménska
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tomáš Kubuš 
                                        
                                            Severný Cyprus 2013 (foto)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Kubuš
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Kubuš
            
         
        kubus.blog.sme.sk (rss)
         
                        VIP
                             
     
         Milujem cestovanie, cudzie krajiny, jedlá, čaj, Turecko, Blízky či Stredný východ, Indiu, fotografovanie, písanie..no jednoducho CESTOVANIE!!! mám sen precestovať celý svet, zdvihnúť sa a ísť, zastaviť sa na miestach po ktorých túžim a pokračovať až na jeho koniec, potom sa vrátiť a ísť opäť, ale inou cestou...   
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    292
                
                
                    Celková karma
                    
                                                8.28
                    
                
                
                    Priemerná čítanosť
                    3047
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Grécko s batohom 2007
                        
                     
                                     
                        
                            Káhira - Istanbul 2008
                        
                     
                                     
                        
                            Čarovná Perzia 2008
                        
                     
                                     
                        
                            Central Asia, Iran 2009
                        
                     
                                     
                        
                            Južný Kaukaz 2010
                        
                     
                                     
                        
                            Turecko, Irak 2010
                        
                     
                                     
                        
                            Turecko
                        
                     
                                     
                        
                            Severná Európa
                        
                     
                                     
                        
                            Maroko
                        
                     
                                     
                        
                            Monako
                        
                     
                                     
                        
                            Taliansko
                        
                     
                                     
                        
                            Blízky Východ - Stredný Východ
                        
                     
                                     
                        
                            Sicília
                        
                     
                                     
                        
                            Francúzsko
                        
                     
                                     
                        
                            Anglicko
                        
                     
                                     
                        
                            Benelux
                        
                     
                                     
                        
                            Blízkovýchodné dobrodružstvo
                        
                     
                                     
                        
                            Španielsko
                        
                     
                                     
                        
                            Grécko
                        
                     
                                     
                        
                            Ukrajina
                        
                     
                                     
                        
                            Rusko
                        
                     
                                     
                        
                            Irak
                        
                     
                                     
                        
                            Nemecko - Rakúsko
                        
                     
                                     
                        
                            Malta
                        
                     
                                     
                        
                            Tunisko
                        
                     
                                     
                        
                            Čechy a Morava
                        
                     
                                     
                        
                            Cyprus
                        
                     
                                     
                        
                            India, Nepál 2013
                        
                     
                                     
                        
                            Juhovýchodná Ázia
                        
                     
                                     
                        
                            Arábia
                        
                     
                                     
                        
                            Balkán
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Za zvoncami karaván
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Vilček
                                     
                                                                             
                                            Renáta Kuljovská
                                     
                                                                             
                                            Braňo Skokan
                                     
                                                                             
                                            Frenky Hříbal
                                     
                                                                             
                                            Zdenko Somorovský
                                     
                                                                             
                                            Peter Dzurinda
                                     
                                                                             
                                            Marek Pšenák
                                     
                                                                             
                                            Peter Gregor
                                     
                                                                             
                                            Andrej Hajdušek
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dobrodruh
                                     
                                                                             
                                            CestovanieSvetom
                                     
                                                                             
                                            Blízky Východ
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo navštíviť Thajsko?
                     
                                                         
                       Beslan desať rokov po tragédii
                     
                                                         
                       Skopje - mesto, kde sa snúbi nové so starým
                     
                                                         
                       Krížom čarovnou, ale chudobnou Indiou
                     
                                                         
                       Tartu a Čudské jazero
                     
                                                         
                       Škandinávsky road trip: Bergen
                     
                                                         
                       Cestoval som v Afganistane. Blázon alebo fajnšmeker?
                     
                                                         
                       Lahemaa - v krajine zátok, lesov a močiarov
                     
                                                         
                       V horskom pohraničí Čečenska a Dagestanu
                     
                                                         
                       Cez Turkmenistan a Uzbekistan do centra Hodvábnej cesty
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




