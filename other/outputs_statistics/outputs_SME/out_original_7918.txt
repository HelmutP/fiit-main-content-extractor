
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Juraj Dučák
                                        &gt;
                Nezaradené
                     
                 Jeden predvolebný 

        
            
                                    3.6.2010
            o
            10:12
                        (upravené
                18.5.2013
                o
                11:41)
                        |
            Karma článku:
                9.79
            |
            Prečítané 
            776-krát
                    
         
     
         
             

                 
                    Tiež patrím medzi vzývačov apatickej pasívnej časti našej spoločnosti, aby išla voliť. Jednoducho si neviem predstaviť, že by tam mal zasadnúť na ďalšie 4 roky ten odpad, čo je tam teraz.
                 

                 
  
   Tohtoročné voľby považujem za minimálne také dôležité, aké boli tie v roku 1998. Boli sme o 12 rokov menej skúsený so slobodou a predsa sme dokázali poraziť absolútne zlo. Je možné, že vtedy šlo naozaj o viac a potrebovali sme stihnúť posledný vlak. Nakoniec sme na ten vlak našťastie nasadli a vydýchli sme si. Naštartovali sa nepopulárne, ale nevyhnutné reformy, ktoré sa ukázali, ako správna cesta. ( Časom sme trošku spohodlneli a začali byť menej ostražitý, začalo sa konečne blískať na dobré časy a pred krízou tu už bol doslova boom. )   Veľmi sa obávam, že náš už riadne zabrzdený vlak, by mohol zaradiť aj spiatočku ( ale zároveň som pokojný v duchu hesla pokrok nezastavíš ). A tak naše politické a predvolebané dianie sledujem intenzívnejšie ako zvyčajne a rád by som sa s vami podelil a pár faktov:   1. Hrozí reálna šanca, že súčasná vládna koalícia ostane vládnuť ďalšie volebné obdobie. ( V tom prípade som ale pripravený výjsť do ulíc a aj do parlamentu osobne ich odtiaľ vykopať von. ) Fico sa môže spojiť jedine so štvrtou cenovou skupinou SNS a HZDS. Tie ale musia získať 5%. ČÍM VIAC ĽUDÍ PôJDE VOLIŤ, TÝM VIAC HLASOV BUDE POTREBNÝCH NA ICH ZÍSKANIE, PRETO NEPODCEŇUJTE SVOJ HLAS.   2. Ficové sociálne sľuby znejú lákavo, ale som presvedčený, že ich bude schopný naplniť len gréckou cestou, teda zadlžovaním.   3. Ak sa vám nechce voliť, mali by ste si uvedomiť, že zahraniční investori čakajú na výsledok volieb a na ich základe sa rozhodnú, či sa im oplatí u nás investovať.   4. Súčasné podnikateľské prostredie má mnoho byrokratických a iných prekážok. Podnikanie je sloboda a čím silnejší podnikatelia, tým slabší vplyv a kontrola zo strany štátu. Ja mám skôr dojem, že Fico bude radšej podporovať almužnami poslušné, bezbranné, na jeho drobných závislé ovečky, ako by sa mal deliť s niekym o sféru vplyvu. ( Pozerajú sa nateba z výšky a smejú sa, keď nechceš žrať ich zvyšky ... )   5. Nepodceňoval by som dôchodcov tým, že sa dajú obalamútiť 13-tym dôchodkom, keď ináč o nich celý rok nikto ani nezakopne a ani neponúkne nádej na zlepšenie do budúcnosti. Verím, že nie sú hlúpi a nedajú sa lacno kúpiť.   6. Veľmi ma potešili voľby v Čechách. Tam bolo krásne vidno ten skok, keď sa už ľudia nedajú oklamať pázdnymi sľubmi, ktoré im ponúkajú arogantné indivíduá, ale roznýšľajú dopredu, nie krátkozrako. Nemáme síce také kvalitné diskusné relácie ako Češi, aj kampaň u nás je podobná nuda, ale stále si myslím, že sme ako národ podrástli minimálne ako oni.   7. Veľkú nádej vkladám hlavne do mladých ľudí, verím v ich - našu silu.   8. Včera som si čítal článok, ako si ľudia kupujú domy a pozemky v pohraničí Rakúska. Je to lacnejšie, Rakúšania nedovolia hrabivým developerom napĺňať ich nenažraté sny. Stavebné povolenie tam získate za 2 hodny bez tortúry, nevpustia si tam arogantných hlupákov, ktorých my stále trpíme. Tiež by som sa jedného dňa rád dožil podobného prístupu, bez bezodnej korupcie a arogancie.   9. Ľudia si začínajú stále viac veriť, že svojim konaním, môžu veci zmeniť. Vidím to na sebe, vidím to aj na ostatných.   10. Pre vládu ako SMER - SNS - HZDS tu už nie je miesto, děkujeme, odejděte!   Pripájam vtip:   Na rannej prechádzke sa Fico náhle potkol a spadol.  Ukázalo sa, že to bola porážka. A pretože podľa hesla "Zdravie nie je  biznis" nemala blízka nemocnica dostatok personálu, tak zomrel.     Onedlho stál pred nebeskou bránou a Svätý Peter ho privítal:     - "Vítaj na Nebesiach, ale skôr, než sa tu usadíš, tak sa zdá, že tu máme  malý problém. Málokedy tu máme sociálnych demokratov a tak si nie sme tak  úplne istí, čo s nimi."     - "To nie je žiadny problém, jednoducho ma pustite ďalej, som dobrý kresťan,  som veriaci."     - "Rád by som ťa pustil ďalej, ale mám pokyny priamo od Boha.  Hovorí, že pre zavedenie novej politiky SOCIÁLNEHO ŠTÁTU musíš stráviť jeden  deň v Pekle a jeden deň v Nebi. A potom si sám vyberieš, kde chceš stráviť  Večnosť.     - "Ale ja už som rozhodutý. Chcem do Neba!"  Svätý Peter odviedol Fica k výťahu a zišli dolu do pekla.  Brána Pekla sa otvorila a Fico se ocitol na krásnej zelenej lúke.  Pripadal si tam ako v Raji. Zelená tráva, slnko svieti, ale nie je príliš  teplo. Vanie mierny vlahý vetrík. Všade sú malé krčmy s pivom, párkami a  údenými kolenami, tienené šumiacimi stromami.  Všade postávajú slávni sociálni demokrati, sám Willy Brandt ho víta. Pod  gaštanom diskutuje Lenin s Rozou Luxemburgovou. Všetci sa voľne prechádzajú,  hrajú kolky a futbal. Na obed si dávajú bifteky, grilované prasiatka,  šampanské a plzeň. Sám diabol priniesol Ficovi pohár oroseného piva a rezeň.     - "Daj si jedno, Robko!" "Nie, sľúbil som, že už nebudem piť a budem  štíhly", odmieta Fico.     - "Toto je peklo, synak. Tu môžeš jesť a piť čo chceš a neuškodí ti to, skôr  prospeje."  Robko si teda dá a zistí, že sa mu diabol páči, že je to fajn chlapík, ktorý  vie rozprávať korenené vtipy a vyvádzať o statným kanadské žartíky.  Jednoducho presne to, čo majú socialisti radi.  Všetci si skvelo užívajú a skôr než si to Fico uvedomí, deň sa skončí.  Všetci sa s ním búrlivo lúčia a mávajú mu, keď nastupuje do výťahu. Keď se  dvere výťahu znovu otvoria, je zase pri bráne Raja a svätý Peter naňho čaká.     - "Tak a teraz je čas na návštevu Raja", a otvára mu bránu.  A ďalších 24 hodín je Robko donútený tráviť čas s partiou čestných,  vychovaných ľudí, ktorí majú radi spoločnosť, ale hovoria o iných veciach  než o jedle, o peniazoch, a chovajú sa k sebe slušne a úctivo. Žiadne hrubé  vtipy alebo kanadské žartíky.  Nie sú tam ani žiadne krčmičky a aj keď je jedlo dobré, biftek a údené  kolienko to nie sú. A všetci sú chudí. Niet tam nikoho, koho by poznal. A  naviac sa k nemu nikto nechová ako k dôležitej osobe.     - "No toto, na niečo takéto ma Mečiar so Slotom nepripravili.  Ani Kaliňák s Maďaričom sa o tomto nezmienili," hovorí si Fico.  Deň sa skončil a svätý Peter sa vrátil.     - "Tak, strávil si deň v Pekle a deň v Nebi. Teraz si vyber, kde stráviš  Večnosť."  Ale Robko dlho nepremýšľa a rozhodne sa hneď:     - "Nikdy som si nemyslel, že toto poviem - vieš Nebo bolo krásne, a tak...  ale ja si naozaj myslím, že patrím k priateľom do Pekla."  Tak ho Peter odvedie k výťahu a on schádza stále dolu až do Pekla.  Dvere výťahu sa otvárajú a ocitne sa uprostred pustatiny pokrytej odpadkami  a toxickým odpadom. Vyzerá ako púšť, ale je to ešte horšie. Asi ako bane v  okolí Prievidze. Je vydesený, keď vidí svojich priateľov, oblečených v  zdrapoch, a ko prikovaní k reťaziam zbierajú do plastových vriec odpadky pri  ceste. Stenajú a vzdychajú od bolesti, tváre a ruky majú čierne od špiny.  Diabol pristúpi k Ficovi, objíme ho okolo ramien. Ten se otrasie a hovorí:     - "Tomuto nerozumiem, bol som tu včera, všade slniečko, trávička zelená,  pivo tieklo prúdom. Stále sme sa flákali a bolo to skvelé.  A teraz je tu pustatina, všade iba neporiadok a všetci vyzerajú biedne."  Diabol sa na neho pozrie, prefíkane sa usmeje a povie:     - "No, zlatý môj, včera to bola volebná kampaň, dnes si si nás už zvolil!" 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Dučák 
                                        
                                            Ako ma vykopli z môjho prvého zamestnania.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Dučák 
                                        
                                            Prečo sa komunizmus už mladej generácie „netýka“.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Dučák 
                                        
                                            Prchký tréner futbalovej reprezentácie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Dučák 
                                        
                                            Ako premôcť molocha.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Juraj Dučák 
                                        
                                            Aj na Slovensku sa oplatí bojovať.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Juraj Dučák
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Juraj Dučák
            
         
        ducak.blog.sme.sk (rss)
         
                                     
     
        Na križovatke života.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    20
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1063
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




