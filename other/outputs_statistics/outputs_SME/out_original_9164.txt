
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Edita Vyšná
                                        &gt;
                šport
                     
                 Svetový plážový volejbal ze Štvanice 

        
            
                                    18.6.2010
            o
            17:19
                        (upravené
                16.7.2012
                o
                10:36)
                        |
            Karma článku:
                2.00
            |
            Prečítané 
            809-krát
                    
         
     
         
             

                 
                    Kto má radšej volejbal ako futbal, ako napríklad ja, ten by určite skôr ako majstrovstvá sveta z JAR na veľkoplošnej obrazovke v súčasnosti uvítal zážitok z turnaja Patria Direct Open v plážovom volejbale. Turnaj, ktorý sa koná s veľkou podporou sponzorov, avšak zatiaľ so slabším záujmom divákov v Prahe na ostrove Štvanice v ČEZ KOLOSEU a na 3 priľahlých tenisových kurtoch (hráva sa tu DAVIS CUP). Na túto udalosť boli premenené na plážové ihriská s brazílskym pieskom, ktorý sa nezahrieva . Prítomní sú aj olympijskí víťazi, a nechýba hádam žiadne meno svetového plážového volejbalu. Najlepšie tímy sa proti najlepším ukážu ešte aj dnes i v sobotu. V nedeľu  ulahodia športovo gramotnej verejnosti finálové zápasy.  Hádam príde viac ľudí. Keď si predstavím nejaký tenisový turnaj veľkej štvorky, je mi smutno.To, že volejbal (ani šestkový ani plážový) naozaj nemá takú pozíciu vo svete ako tenis, je hneď jasné..Pozícia Slovenska? Biedna.
                 

                      Turnaj je dotovaný 190 000 USD. Vstup je zdarma. Na každý zápas, na všetkých 6 dní (ostávajú tri aj s dnešným). Tento turnaj zo série SWATCH FIVB WORLD TOUR sa koná tento rok šiesty krát, ešte sa uskutoční 12 podujatí v iných svetových mestách. Pre kvalifikačnú časť turnaja bolo prítomných  64 tímov. 4 tímy z nich  sa aj do tejto kvalifikačnej časti museli prebojovať prostredníctvom zápasu s tímom zo svojej krajiny, aby boli splnené národné kvóty. Domov išiel jeden tím z Brazílie, Holandska, Ruska a Talianska. Keby som sa tam „dotrepala“ z Brazílie, asi by ma to naštvalo. Z Brazílie sa cez túto časť prepracoval tím zložený z jedného strieborného a druhého bronzového medailistu LOH v Pekingu (ich konečné miesto po dnešku: 13.). Nasledoval boj 64 tímov cez kvalifikáciu. Jedno prehraté kolo v kvalifikácii znamenalo koniec v turnaji. Keby som bola z Číny, asi aj to by ma naštvalo=) Číňania si však počínajú dobre a miliardová populácia opäť prispieva k tomu, že sú tu veľké talenty-Piati nasadení Wu- Xu, ktorí spomínanú Brazílsku dvojicu včera porazili úžasným výkonom. Brazílčania ničili svoj výkon sťažovaním sa na (ne)existujúce prehrešky súpera u rozhodcu. Na tento zápas 5. nasadených Číňanov a olympionikov z Brazílie sa pozeralo asi toľko šudí, ako na slovenský vidiecky futbal v obci z 300 obyvateľmi.    Do hlavnej časti turnaja sa z kvalifikácie prebojovalo 8 tímov. Spolu bolo v hlavnej časti miesto pre 32 tímov. 20 najlepších tímov vo svetovom rankingu, 2 držitelia divokých kariet, 2 karty domáceho usporiadateľa (veď sa mi zdalo, že na hlavnom ihrisku v koloseu hrajú samé české tímy=)) a 8 tímov, ktoré sa kvalifikovali. Okrem brazílskej dvojice Ricardo a Marcio Araujo ktorí boli 3, resp. 2. na olympiáde v Pekingu, sa do hlavnej časti turnaja museli cez kvalifikáciu pretĺcť aj ďalší Brazílčania, ktorých ale už asi nebavila konkurencia v ich domovine a tak sa rozhodli reprezentovať Gruzínsko-Renato Gomes a Jorge Terceiro- z ich histórie možno spomenúť 4 miesto na OH v Pekingu ( už svoje účinkovanie na turnaji Patria Open skončili, a to na 17.mieste).   V hlavnej časti turnaja sú „potrebné“ na vypadnutie dve prehry. Počet aktívnych tímov na turnaji sa zmenšuje. Dnes si zahrajú o postup ešte Američania Rogers-Dalhausser [ 1- najvyššie nasadení, olympijskí víťazi z Pekingu] proti dvojici Rusov Kolodinsky-Koshkarev [27], Španieli Herrera-Gavira [4. nasadení] proti tímu z Holandska, Nummerdor-Schuil [7]. V tejto skupine hrá tiež brazílska dvojica Benjamin-Bruno [11] proti poľskému tímu v zostave Fijalek-Prudel [8] a Lotyši Samoilovs-Sorokins [23] proti ďalším zástupcom brazílskej plážovovolejbalovej veľmoci Cunha-Thiago [13]. Víťazi si zahrajú o postup do bojov najprv o vyššie ako 9.miesto, po prípadnom ďalšom víťazstve o vyššie ako 6. miesto. Proti sebe si tiež zahrajú dve dvojice tímov, ktoré doteraz neprehrali na turnaji ani jeden zápas. Prvou je nemecká dvojica Brink-Reckermann [3]  a opäť tím z Brazílie Alison-Emanuel [2]. Títo Brazílčania, skúsený Emanuel, bronzový z OH v Pekingu a mladučký, 24-ročný, 202 cm vysoký Alison porazili naozaj pekným technickým volejbalom zatiaľ všetkých. Druhou dvojicou tímov zatiaľ neporazených je americký tím Gibb-Rosenthal USA [17] a Číňania Wu-Xu [5], ktorí si spolu zahrajú. Najhoršie môžu tieto tímy skončiť piate. Ktože skončil piaty, sa dozvieme v sobotu, tak ako víťazov semifinálových stretnutí. Zápasy o tretie a prvé miesto sa budú na ostrove Štvanice konať v nedeľu.   Predstavím tiež slovenských zástupcov: Slovensko malo zastúpenie v podobe dvoch kvalifikantov. Ani jeden tím sa cez prvý zápas nedostal. Prvými zástupcami boli Horváth- Kollár. Prehrali 2:0  (20:22 a 22:24) po 41 minútach s japonským párom Asahi Shiratori. Druhým tímom bol Kriško- Majzel. Vyše dovch metrov vysoký príjemný mladík Tomáš Kriško hrá blok za VKP Bratislava. Jeho spoluhráč na tomto turnaji bol jeho bývalý spoluhráč zo Žiliny zo stredoškolských čias, Robo Majzel. Tiež veľmi sympatický chlapec, 185 cm, hrá v súčasnosti prvú ligu v Žiline. Plážový volejbal spolu vôbec netrénujú. Prečo sa na tento turnaj prihlásili, medzi dobre zohraté dvojice zbierajúce body zo sériových podujatí, vlastne neviem, aj keď som sa to určite pýtala. Hanbu však naozaj nespravili. Na prvý zápas dostal 63. tím zo šesťdesiatich štyroch  tvrdý oriešok, už spomínaných Brazílčanov reprezentujúcich Gruzínsko. Opäť vyzdvihnem 4. miesto tejto dvojice na olympijských hrách v Pekingu. Slovenskí chlapci, extraligista a prvoligista, skončili účinkovanie na turnaji po tomto zápase so skóre 12:21, 11:21. Prehupnúť sa cez desiatku bol ich prvotný cieľ... Tomáš po  prehratom zápase trvajúcom 33 minút povedal, že „Boli hrateľní.“ =)    Bol to pekný zápas. „Objektívne“ treba poznamenať, že autorom najefektnejšieho smeču tohto zápasu bol Tomáš Kriško, slovenský 21-ročný blokár=)...Nie, naozaj, raz neskutočne položil. A celkovo to, čo obaja predviedli, bolo solídne a nikto by asi nepovedal, že tu boli omylom, že to boli len dvaja milí zozbieranci, ktorí boli náhradníkmi...Nečakala som, že budem môcť na tomto turnaji niekomu naozaj fandiť. Tlieskam hocikomu, keď vytvorí na ihrisku niečo pekné, však na to je fanúšik. Avšak kričať tak, ako som kričala (dosť výrazné, keďže tam vlastne nik nefandil nikomu), sa dalo iba kvôli tomu, že som tam nečakane stretla týchto dvoch slovenských chlapcov.  Ale to, že Slovensko na naozaj svetovom turnaji reprezentuje tím mladých chalanov, ktorí spolu netrénujú, je, myslím si, dosť smutné. Slovenská volejbalová federácia aj každý, koho možno počítať za kompetentného, by si mohol vstúpiť do svedomia. Zážitok mi to však nepokazilo..Volejbal je šport a šport je podľa mňa to najkrajšie, čo človek vytvoril.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            Teach First
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            Dobrá kampaň nevládnej organizácie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            ani tak..a hlavne nie po francúzsky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            bude lepšie-a..už je!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Edita Vyšná 
                                        
                                            Nevedecky a nadšene detsky o faune Austrálie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Edita Vyšná
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Edita Vyšná
            
         
        editavysna.blog.sme.sk (rss)
         
                                     
     
         rada píšem.. o veciach, ktoré ma zaujali, oslovili, nahnevali. neznamená to, že to bude niekto ďalší považovať za hodné opisu;) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    137
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    919
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            enviro
                        
                     
                                     
                        
                            rozjímanie
                        
                     
                                     
                        
                            USA
                        
                     
                                     
                        
                            šport
                        
                     
                                     
                        
                            Brusel
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prezident...náš sused
                     
                                                         
                       Ľúbime ťa, prepáč ...
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




