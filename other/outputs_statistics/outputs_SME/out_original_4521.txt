
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Horanský
                                        &gt;
                IT
                     
                 Môj počítač, môj hrad 

        
            
                                    14.4.2010
            o
            0:23
                        (upravené
                27.4.2010
                o
                0:13)
                        |
            Karma článku:
                5.29
            |
            Prečítané 
            1390-krát
                    
         
     
         
             

                 
                    Vo volebnom programe SDKÚ-DS v časti informatizácia sa nachádza bod Môj počítač, môj hrad. Zaregistroval som na internete rôzne otázky o tom, čo tým autori mysleli a kritiku zbytočnosti tohto bodu. V tomto článku by som čitateľom rád objasnil, čo týmto bodom bolo naozaj myslené a za akým účelom bol včlenený do volebného programu. Mal som totiž tú česť spolupracovať na tvorbe časti informatizácia.
                 

                 Čo hovorí bod:   2.4.78. Môj počítač, môj hrad   Zachováme ochranu súkromia osobných počítačov v zmysle hesla „Môj  počítač, môj hrad“. Nepodporíme žiadne opatrenia nad rámec európskych  štandardov.   Čo je cieľom tohto bodu:   Zachovať súkromie pre používateľov domácich počítačov – nepovoliť žiadnu formu preventívneho sledovania občanov. Nechať povolené sledovanie počítača len na základe súdneho príkazu kvôli kriminálnym deliktom – napr. detská pornografia, terorizmus, či organizovaný zločin. Nechceme model štátu z knihy Georga Orwella – 1984.   SDKÚ-DS týmto bodom programu chce jasne deklarovať, že nesúhlasí s akýmikoľvek snahami o paušálnu kriminalizáciu občanov SR bez dôvodného podozrenia. Jedna zo základných pilierov stredopravých strán je individuálna sloboda a tento návrh treba chápať ako jej ochranu.   Čo bola motivácia na jeho napísanie:   Sme svedkami plazivého príchodu éry cenzúry internetu a preventívneho sledovania jeho používateľov štátnymi inštitúciami v mnohých západných demokraciách (napr. USA, Veľká Británia, Francúzsko). V týchto krajinách k tomu dochádza pod zámienkou boja proti pirátstvu a vidím tam veľkú podobnosť s čínskou verziou internetu. Rozdiel je len v tom, že v jednom prípade o správnosti a legálnosti informácií rozhoduje nahrávací priemysel a v druhom Komunistická strana. Neslávne známy francúzsky protipirátsky "odpájací  zákon" sa pokúsili schváliť aj na úrovni EÚ, ale po veľkej vlne ľudového odporu neprešiel hlasovaním Európskeho parlamentu. Naopak, prešiel dokument Fundamental Rights, ktorý každému občanovi priznáva právo na prístup k internetu. Vo Francúzsku "odpájací zákon" skončil na Ústavnom súde. Myslíte, že to, že priateľka francúzskeho prezidenta Nicolasa Sarkozyho Carla Bruni je speváčka, je náhoda?   Na Slovensku sme svedkami tendencii SMERujúcich k obmedzovaniu slobody internetu hlavne od vládnej strany SMER-SD - regulácia internetovej televízie (ktorá ide ďaleko nad rámec požadovanej smernice EÚ), zákaz anonymizérov, snaha o zaznamenávanie internetových diskusií a pod.      Linky:     Volebný program SDKÚ-DS  Facebook skupina Môj komp, môj hrad  Facebook page Informatizácia Slovenska 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Kosovo a Srbsko - good news
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Never slovenskému Maďarovi! Určite chce maďarské občianstvo!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Bielorusko - stále posledná diktatúra Európy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Protest proti obmedzovaniu slobody internetu v Maďarsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Horanský 
                                        
                                            Robert Fico nepodpísal Memorandum o ochrane Žitného ostrova
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Horanský
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Horanský
            
         
        horansky.blog.sme.sk (rss)
         
                                     
     
        Bc. FMFI UK aplikovaná informatika,
študent FMFI UK - aplikovaná informatika - magisterské št.,
podpredseda Občiansko-demokratická mládež,
člen správnej rady OZ Občan pre demokraciu - projekt Mladý občan.

Verím v osobnú zodpovednosť a slobodu jednotlivca, voľný trh, zdravé podnikateľské prostredie, jednoducho pravicové hodnoty :) Nemám rád socializmus, fašizmus a anarchizmus - extrémy. 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    12
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1963
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            IT
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Běž domů, Ivane! A už se nevracej
                     
                                                         
                       Ficova blondínka u Kotlebu
                     
                                                         
                       Skvelá Petržalka, skvelé Ovsište
                     
                                                         
                       LEX Kolesík - SMER chce ropovod cez Bratislavu
                     
                                                         
                       Pán Janukovič,  zabudol som sa predstaviť
                     
                                                         
                       Pre Oracle nie sú korupčné obvinenia za biznis so štátom nič nové
                     
                                                         
                       Kaliňákove hlášky
                     
                                                         
                       Všestranný sprievodca po Lisabone a okolí
                     
                                                         
                       V akej krajine to žijeme?!
                     
                                                         
                       Ako sa rozísť s UPC v 40 jednoduchých krokoch
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




