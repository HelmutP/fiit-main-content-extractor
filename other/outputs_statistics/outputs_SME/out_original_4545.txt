
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Kvasnička
                                        &gt;
                Nezaradené
                     
                 Šikanovanie vodičov na cestách. 

        
            
                                    16.4.2010
            o
            17:36
                        (upravené
                16.4.2010
                o
                19:03)
                        |
            Karma článku:
                6.03
            |
            Prečítané 
            419-krát
                    
         
     
         
             

                 
                    Napriek tomu, že na Slovensku sa neustále menia a novelizujú pravidlá cestnej premávky, základný prístup štátnej moci k jej účastníkom ostáva v mnohých prípadoch nezmenený - prioritou nie je zvýšenie bezpečnosti a plynulosti premávky, ale potreba niekoho nachytať a pokutovať. Fakt, že výška vybratých pokút je stále plánovaná pri zostavovaní verejného rozpočtu, túto skutočnosť len podčiarkuje.
                 

                 
  
   Vodiči sú každodenne vystavovaní absurdným situáciám, v ktorých sú šikanovaní za konanie, ktoré nenarúša plynulosť a bezpečnosť premávky, príp. kedy postihy nie sú adekvátne k nebezpečnosti ich konania. Nový zákon o cestnej premávke priniesol zmenu v tom, že za obec sa považuje akékoľvek územie od značky začiatok obce po značku koniec obce. V starom zákone bola podmienka, že súčasne musí ísť aj o zastavané územie.   Súčasný sadzobník pokút je stále viac odporúčaním  materiálom, než reálnym výpočtom sadzieb pokút za jednotlivé priestupky. Dáva príslušníkom polície voľnosť v interpretácii okolností priestupku a možného postihu zaň. Vodiči sú takto často ponechaní skôr výkyvom nálady daného policajta, než objektívnemu posúdeniu situácie.       SaS vo svojom programe navrhuje zmeniť sadzobník pokút tak, že výška   pokuty za každý   priestupok bude kategorizovaná nielen podľa potenciálnej nebezpečnosti,   ale aj podľa konkrétnej nebezpečnosti konania vodiča v nasledovných   kategóriách, od najzáväznejších po najmenej závažné:   1. Porušil predpis, pričom nikoho neobmedzil, neohrozil, atď.   2. Porušil predpis a niekoho ohrozil.   3. Porušil predpis a spôsobil dopravnú nehodu.   Odstrániť šikanovanie vodičov zo slovenských ciest tým, že každá pokuta   či trest bude vo vyrovnanom pomere k nebezpečnosti konania účastníka   cestnej premávky.   Pokuty :   1. Zadefinovať zákonom zabezpečenie proti zneužitiu vozidla tak, aby   nebolo možné pokutovať vodičov za ponechanie otvoreného okienka na   vetranie, alebo otvoreného kabrioletu.   2. Vyňať plánovanie pokút z verejných rozpočtov. Vybrané pokuty budú   sústreďované v samostatnom fonde, z ktorého budú nancované   výstavby dopravných ihrísk, vzdelávacie programy a kampane na   zvyšovanie bezpečnosti.   3. Zaviesť výhradne elektronické, resp. bezhotovostné vyberanie pokút.       Touto cestou by sa zjednodušil systém represívnej časti vyberania pokút v rámci cestnej premávky.         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Koniec ekonomickej slobody Európskych národov !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            7 základných znakov komunizmu !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Rómsky problém ? - riešenie Sociálne Odkázané Spoločenstvá !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Problém dekriminalizácia marihuany ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kvasnička 
                                        
                                            Slovenský šport je čas na zmenu.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Kvasnička
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Kvasnička
            
         
        jankvasnicka.blog.sme.sk (rss)
         
                                     
     
        Ján Kvasnička
zakladajúci člen strany Sloboda a Solidarita
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    579
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            slobodná európa -nikdy to tak nebude
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            palmeiro.wbl.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




