
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marián Šumšala
                                        &gt;
                Vrakuňa
                     
                 Poslanci zastupiteľstva odsúhlasili zbúranie Bratislavského hradu 

        
            
                                    18.2.2010
            o
            11:33
                        (upravené
                18.2.2010
                o
                22:10)
                        |
            Karma článku:
                11.95
            |
            Prečítané 
            3490-krát
                    
         
     
         
             

                 
                    Poslanci bratislavského Developerského zastupiteľstva včera schválili zbúranie Bratislavského hradu. Primátor Zbojník Ďurko večer pred zastupiteľstvom rozoslal poslancom dokument, v ktorom navrhol zbúranie hradu.
                 

                 Je to podľa neho, a poslanci si jeho názor osvojili, najlepšie riešenie situácie, kedy sú už pozemky pod hradom predané a developerská firma Hradbury development neprijala žiadnu rozumnú ponuku na ich spätné predanie. Argument, ktorý poslancov presvedčil bol tiež, že hrad má veľa stavebných chýb a dnes sa už hrady stávajú ináč a lepšie. Takisto riešenie pomôže pri riešení hospodárskej krízy, keďže mesto bude platiť 20 rokov spoločnosti Hradbury Development 14 miliónov korún ročne za to, že občania budú môcť nový hrad navštevovať. Peniaze zaplatené súkromnej firme bude môcť táto firma utratiť a tak rozhýbe našu ekonomiku, doteraz sme hrad navštevovali zadarmo, čo len prehlbovalo ekonomickú krízu. Poslanci jednej kresťansko-demoličnej strany vyhlásili, že sú radi, že budeme mať nový hrad, aj keď priznali, že v čase, keď odsúhlasili predaj pozemkov pod hradom nečakali, že to takto dobre dopadne. Spolu s kresťansko-demoličnými poslancami za návrh hlasovali aj ďalší poslanci, ktorí sú ľudia ako my. Na námietku jedného poslanca, že hrad má veľkú kultúrnu a historickú hodnotu, naňho poslanci upreli prázdne pohľady. Janičiarski aktivisti, ktorí sa nevedeli zmieriť s rozhodnutím, vyhlásili: „Ej, kdeže je tá stará baba a kde je ten hrach, ktorý tomuto Zbojníkovi a jeho zlodejskej bande podrazí nohy a privedie ho na šibenicu."   O tom ako sa v Bratislave búra naozaj pozrite napr.: PKO - Právo Kultúrnych Občanov. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marián Šumšala 
                                        
                                            Ftáčnik: Nájomné byty aj cez mŕtvoly
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Šumšala 
                                        
                                            Druhý dôchodkový pilier je ekonomický nezmysel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Šumšala 
                                        
                                            Vrakunčania sa búria proti svojvôli samosprávy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Šumšala 
                                        
                                            Nevyvážajte mŕtvoly do nášho lesoparku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marián Šumšala 
                                        
                                            Philip Morris a popieranie zmeny podnebia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marián Šumšala
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marián Šumšala
            
         
        sumsala.blog.sme.sk (rss)
         
                                     
     
         Konštruktér automatických strojov, ekológ amatér, prekladateľ, vydavateľ, manžel, dvojnásobný tatko. 

 Kandidujem za poslanca miestneho zastupiteľstva vo Vrakuni. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    25
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1113
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vrakuňa
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




