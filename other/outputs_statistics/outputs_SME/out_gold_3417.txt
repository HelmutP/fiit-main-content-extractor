

     
   
  
   
 Počet ľudí hrajúcich social games sa v roku 2009 pohyboval okolo čísla 250 miliónov hráčov, v roku 2008 to bolo len 50 miliónov. Medzi najpopulárnejších tvorcov hier patria spoločnosti Playfish, Zynga, Playdome a MindJolt. Vzorec na úspešnosť hier je jednoduchý: 
   
   
   
 ÚSPECH = JEDNODUCHOSŤ + KLONOVANIE + VIRALITA + FREEMIUM BUSINESS MODEL 
   
   
   
 Jednoduchosť - aj na strane tvorby hry, aj na strane používateľov. Intuitívne ovládanie, nenáročnosť na hraciu platformu. Len tak pre zaujímavosť, vytvorenie novej hry trvá približne 3 až 7 mesiacov, náklady na vytvorenie $100 000 až $300 000.  
   
   
 Klonovanie – ak ste patrili medzi skalných fanúšikov Atari a Nintenda, systém tvorby nových social games je vám celkom blízky. Spoločnosti neustále vydávajú evidentne klonované hry. Ľudia radi hrajú niečo, čo už poznajú z minulosti a ak to má aj vylepšenia (novo klonované hry obsahujú to, čo ich predchodcom chýbalo), tým lepšie. Ako jeden z mnohých príkladov Zynga vydala Café World, ktorý sa nápadne podobá na Restarant City od Playfish. 
   
   
 Virálny marketing – šírenie samočinnou reprodukciou, za pomoci sociálnych sietí, blogov, updejtov, návrhov, wall postov a podobne. Hry propagujú jedna druhú, keď spustíte jednu, hneď na vás zaútočí reklama na ďalšie štyri.  
   
   
 Freemium model – ponúka používanie hry zadarmo, zatiaľ čo si máte možnosť kúpiť si za reálne peniaze vylepšenia – v tomto prípade virtuálne peniaze, ktoré potom použijete na vylepšenie svojej pozície v hre. Zinga má v súčasnosti viac ako 230 miliónov aktívnych hráčov (väčšina hráčov hrá viac ako jednu hru, preto to číslo). Koľko môže zarobiť použitím freemium modela sa dozviete tu - Krutá pravda o Farmville   
   
   
 Priamy vzťah s používateľmi – na rozdiel od klasických hier, výhoda tvorcov social games je priamy prístup k užívateľom ich produktov. Nápady, podnety a návrhy na vylepšenia sa tak dostanú bez použitia sprostredkovateľa priamo k tvorcom. 
   
 Na tento rok sa odhaduje tržba z týchto hier na približne 1 bilión dolárov. Čo k tomu dodať? Ako príklad  existencie "nového marketingu" by sa ťažko hľadala lepšia ukážka. A tak to aj treba brať. Ja osobne som rád, že mi ešte idú Lemmingsovia a na internete som videl k stiahnutiu nové leveli, takže...-) 
   
 Pre zaujímavosť, toto je 5 najpopúlárnejších social games na Facebooku. (čísla predstavujú registrovaných hráčov) 
   
   
   
 1. FarmVille 82,157,932 
 2. Café World 30,142,515   
 3. Texas HoldEm Poker 28,602,015 
 4. Mafia Wars 25,264,431 
 5. Happy Aquarium 24,427,534  
   
   

