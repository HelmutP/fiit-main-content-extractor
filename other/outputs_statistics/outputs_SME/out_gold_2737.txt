

 Pri pohľade z diaľky nebolo na ňom nič výnimočné. Fakt, vyzeral úplne obyčajne. Avšak keď som si ho v rukách trochu pootočila, objavila som na ňom vyrytú tvár istého významného politického predstaviteľa dvadsiateho storočia. 
 „Načo tu má starý otec takúto haraburdu?" pýtala som sa seba. Pohár skutočne nebol použitý aspoň tridsať rokov. Vrstva prachu v ňom presahovala jeden a pol centimetra. 
 „A vlastne, pil z neho už vôbec niekto? Tak načo tu je? Ba dokonca otočený naopak, aby nikto nevidel muža, ktorý ublížil toľkým ľuďom na celom svete. Je tu len do počtu? Aby na poličke nezostalo prázdne miesto?" 
 Dlho som potom nad tým premýšľala. Nerozumela som, prečo sa už dávno neocitol v kontajneri na sklo. Proste som tomu nerozumela. Stál nehybne na poličke s odvrátenou tvárou osoby, o ktorej aj sám starý otec si myslí svoje... 
 Niečo podobné sa stalo u babky a dedka na Morave, keď som k nim ešte to leto prišla na prázdniny. Tentokrát som hľadala nejaký hrnček, do ktorého som si chcela naliať babkin úžasný jahodový koktail. A vtedy som si všimla opäť nenápadný sklený pohár zastrčený vzadu. Hoci bol trochu iný, ako ten u starého otca, pôsobil znova všedne. Vlastne nič pozoruhodné na ňom nebolo. Bol to iba obyčajný pohár. Ale naozaj bol obyčajný? 
 Ako som sa načahovala za ním, zneistela som. Môj vnútorný hlas mi hovoril, aby som si ho nebrala. Bol taký silný, že som ho nemohla neposlúchnuť, a tak som si radšej zobrala šálku s ružovým sloníkom. Ostatné dni som sa spomínanej poličke zdiaľky vyhýbala, lebo som cítila, že čosi s ňou nie je v poriadku. Táto záhada bola o niekoľko dní rozlúštená. 
 Keď prišla nedeľa, babka vyslala dedka do záhrady pred domom, aby nazbieral tie najsladšie jahody na koktail. Bolo mi ho ľúto, lebo poriadne pražilo slnko a dedko to dosť zle znáša. No babku začala „honit mlsná", ako to sama hovorieva, a tak dedko voľky-nevoľky poslúchol. Moju pomoc neprijal, lebo na svoju záhradu je veľmi hrdý. Nedovolí, aby cudzia noha vkročila do nej. Vraj by som mu mohla pošliapať petržlen. J 
 O chvíľku bol mliečny nápoj na stole. 
 „Aničko, dej si," núkala mi babka, aby som si z neho naliala. 
 „Áno, babi, za chvíľku," odvetila som jej, síce som vôbec netušila, čo mi povedala. Mala som rozčítaný veľmi zaujímavý príbeh a nechcela som sa dať rušiť. Babka však videla, že sa akosi nemám k činu, a tak sama začala obsluhovať. 
 Keď som dočítala kapitolu, zavrela som knižku. Môj pohľad preletel po stole predo mnou a zastavil sa na pohári. 
 „Babka, čo má toto znamenať?!" zdesene som vykríkla. 
 „Jéžiši! Ty mě ale lekáš, holko!" odpovedala mi dosť podráždene. 
 Moje zdesenie však bolo opodstatnené. Z pohára sa na mňa vyškierala akási namachlená bruneta. Jej nahé telo s umelým silikónovým poprsím mi bilo do očí. Bol to ten pohár z poličky, no teraz správne otočený. 
 „Babka, ja sa toho ani nedotknem!" rázne som jej odvetila. 
 „Proč ne?" znela pre zmenu babkina odpoveď, zatiaľ čo tón jej hlasu zmäkol. Už nebol taký tvrdý ako predtým. 
 „Pretože na pohári je nahá žena!" bránila som sa. 
 „No a co má být?" nedala sa. 
 „Babi, neviem čím to je, že nerozumieš, čo sa ti práve snažím naznačiť, ale ak chceš, tak ti to teda vysvetlím." A začala som. 
 „Keď vidím nahú ženu, po prvé - veľmi ma to zraňuje. Zdá sa mi, že sa mi tá žena vysmieva. Mám pocit, že som príšerne škaredá. Viem, že nikdy nebudem vyzerať tak „dokonale" ako tá modelka. Som zo seba hrozne nešťastná a tým pádom nedokážem prijať samú seba. Preto musím proti tomu bojovať. Keby som ju nevidela, nemusela by som sa tým zaoberať. 
 Po druhé - vytvára to vo mne predstavu, že len vtedy, keď budem ako ona, sa budem páčiť druhým. Snažím sa robiť, čo sa len dá, ale nedokážem sa približiť k tomuto „ideálu". A potom sa trápim a opäť sa neprijímam. Zase musím bojovať o to, aby som si seba dokázala vážiť. 
 Po tretie - veľmi ma bolí, keď vidím, ako sa ženy predávajú sťa by boli tuctový tovar na trhu. Neponúkajú svoje nádherné vnútro, ich poklad, ale povrch - ich telo, ktoré za niekoľko rokov stratí všetku svoju krásu. A je to znova spôsobené tým, že sa nedokážu prijať. Nedôverujú svojmu vnútru. Nevedia, aké je vzácne. A tak najpodstatnejšia časť ľudského bytia zostáva úplne, ale úplne bez povšimnutia. 
 Po štvrté - trápi ma, keď sú ženy ľahostajné k mužom. Nepripúšťajú si, že ubližujú nielen sebe, ale aj každému mužovi, ktorý ich vidí. Vôbec si neuvedomujú, že muži sú z „horľavého materiálu" a že povinnosťou žien je chrániť ich zrak, aby nezbĺkli! 
 A po piate - sexualita je úžasný dar od Boha, ale s jednou veľmi dôležitou podmienkou - keď sa žije v manželstve medzi mužom a ženou. Ak sa uplatňuje mimo manželstva, sexualita nie je chránená a úplne stráca na kráse. Premieňa sa len na telesnosť, pudovosť. Ľudské srdce zostáva naďalej nespokojné, nenaplnené a prázdne. Vytratil sa zámer, prečo Boh stvoril sex. 
 A čo je tým zámerom, cieľom? Boh stvoril muža a ženu, aby si navzájom prejavovali lásku, aby sa milovali. Táto láska má byť odrazom (síce veľmi nepatrným), ako nás On miluje svojou nekonečnou láskou. A sex chránený manželstvom má byť zase zábleskom tej intimity, ktorú budeme zažívať s Ním, keď Mu budeme raz hľadieť do tváre a ktorá bude trvať naveky!" 
 Babka ticho vstala, vyliala koktail do umývadla a vyhodila pohár do nádoby na sklo. Nikdy viac som potom už v jej dome niečo podobné nenašla. 
   
 annieMERCY©2010 
   
   

