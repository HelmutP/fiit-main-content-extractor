
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Ščepko
                                        &gt;
                Trenčín
                     
                 Ako do Trenčína priemysel dostali (Historické fotografie) 

        
            
                                    23.3.2010
            o
            9:38
                        (upravené
                21.8.2010
                o
                18:05)
                        |
            Karma článku:
                11.60
            |
            Prečítané 
            7739-krát
                    
         
     
         
             

                 
                    Oblasť vo východnej časti mesta Trenčín (Pod Sokolicami) významne ovplyvnili vojenské kasárne a priemyselný areál, ktorý pretrval dodnes. Aj napriek tomu, že Trenčín mal na konci 19. storočia veľký administratívny význam (sídlo župy) a vojenský význam, priemyselne toto mesto významne zaostávalo. A to nie len v rámci Rakúsko – Uhorska, ale aj za mnohými slovenskými mestami (napr. Žilinou či Trnavou). Trenčín mal od konca 19. storočia vybudovanú železnicu, ktorá vytváral predpoklad na ďalší rozvoj. V meste existovalo len niekoľko väčších remeselníckych dielní. Dôsledkom takto "rozvinutého" priemyslu bol nedostatok financii, nezamestnanosť a aj celková hrozba o neistú perspektívu tohto mesta.
                 

                 Už v roku 1890 poslanec za Trenčiansku župu, Valér Smialovský (!) kritizoval tento stav v parlamentnom prejave. Presadzoval, aby ľudia prácu dostali doma a nemuseli za ňou odchádzať do iných regiónov. Kritizoval tiež názor o rozdelení monarchie na priemyselne Rakúsko a agrárne Uhorsko. Niečo naznačuje aj správa o vývoji mesta z roku 1901 (spomínaná vo Vlastivednej monografii Trenčína). V nej starosta konštatuje, že „priemysel v uplynulom roku, tak ako aj v predchádzajúcom, pokrýva iba domáce potreby“. Teda žiadny export. V roku 1891 sa rokovalo s podnikateľmi, ktorí nakoniec dali prednosť Žiline, v roku 1893 o stavbe cukrovaru, ktorý nakoniec v roku 1901 postavili v Trenčianskej Teplej. Aj napriek vybudovanej železnici Trenčínu chýbal priemysel.      Pre predstavu prikladám aj mapu Trenčianskej župy počas monarchie. Aj podľa nej je vidieť, že administratívne mal Trenčín, ako sídlo župy, veľký význam. V dolnom rohu je časť mapky zobrazujúcej vtedajšie rozdelenie regiónov v severnom Uhorsku (dnešné Slovensko). Župa sa tiahla od Nitrianskej (Nyitra) až po Oravskú župu (Árva). Zdroj: lazarus.elte.hu, commons.wikimedia.org      Východná časť, kde mesto končilo vojenskými kasárňami. Pohľadnica je pravdepodobne z obdobia okolo roku 1904. Dnes je areál kasární rozsiahlejší. V prvom roku vojny (1914) boli kasárne doplnené o nemocnicu s nízkou zástavbou. Tá je vidieť na ďalšom zábere danej lokality. Miesto, kde je dnes priemyselný areál, bolo v tej dobe lúkou a poľom. Zdroj: z knihy Trenčín na starých pohľadniciach    Istý čas tu skúšal podnikať aj viedenský vynálezca Norbert Ceipek. Ten plánoval vyrábať svoj Titanit, teda „bezpečnostnú“ výbušninu, ktorá mala ambíciu konkurovať zabehnutému a osvedčenému dynamitu. Stihol postaviť časť výrobne, ktorá by mala stáť dodnes. Či objekt naozaj dodnes stojí sa mi zistiť nepodarilo.      Prikladám ešte dobovú reklamu na trhavinu Titanit z uhorského týždeňníka. Uverejnená bola v novinách, ktoré vychádzali pod názvom "Jó Szerencsét". Vyšli pred 100 rokmi, konkrétne 21. augusta 1910. Zaoberali sa baníctvom, hutníctvom a technikou v týchto odvetviach. Zdroj: epa.hu    Pre zaujímavosť uvádzam preklad dobovej reklamy:    "Maďarská bezpečnostná trhavina (podľa metódy Ceipek). Výborný maďarský produkt. Úplne necitlivý voči nárazu, tlaku, mrazu a teplu. Možnosť prepravovať a skladovať všade. Jednoduché a bezpečné ovládanie. Vzhľadom na výbušnú silu konkuruje všetkým dostupným trhavinám. Mierna cena. Pokusy boli vykonané na náklady výrobcu. Ďalšie informácie poskytuje centrálna kancelária a predajný úrad, Budapešť."     Za zmienku určite stojí, že Titanit bol použitý aj pri rozbíjaní starého 	opevnenia pri mestskej veži v Trenčíne. Podnik sa však aj napriek ozdravovaniu nepodarilo zachrániť. Neúspešných projektov bolo v Trenčíne viac. Prelomovým rokom bol pre Trenčín až rok 1906.    Až vtedy sa úspešne presadili bratia Tiberghienovci zo severofrancúzskeho mesta Tourcoingu (dodnes sa v tomto meste vyskytujú obyvatelia s menom Tiberghien). Ich zámerom bolo postaviť v Uhorsku továreň na spracovanie vlny. V roku 1905 dohodli v Budapešti finančnú pomoc a rôzne úľavy, či daňové výhody. Stavba mala začať 6 mesiacov od podpísania zmluvy a dokončená mala byť do júna 1907. Zástupcovia budúcej továrne od začiatku roka postupne prechádzali mestá v hornom Uhorsku. Od Košíc až po Nové Mesto nad Váhom. Nakoniec sa rozhodli pre Trenčín, kde svoju významnú úlohu zohral nový župan Smialovský (ten, čo v roku 1890 kritizoval absenciu priemyslu). Vo svojom volebnom programe mal industrializáciu a tiež mal dobré styky s vládou. O podpore stavby sa 22. mája 1906 pre jeho presvedčivé vystúpenie ani nerokovalo. Stavať sa začalo na konci roka.      Zdroj: z knihy Vlastivedná monografia, foto: M. Strieženec   V marci 1907 sa začalo s inštaláciou strojov a začali sa zaúčať prvé tkáčky. Z Francúzska do Trenčina prichádzali prevažne staršie a opotrebované stroje. V roku 1914 mala továreň 13134 vretien, 294 krosien. V toku 1913 v nej pracovalo 620 zamestnancov. Polovicu produkcie predávala Uhorskej monarchii a druhú polovicu rakúskej časti. Sklady mala v Brne, Budapešti, Temešváry (Západné Rumunsko) a v Záhrebe. Fotografie z interiéru továrne sa mi získať nepodarilo. Snáď nejaké existujú. Našiel som však staré fotografie z továrne, ktorá spracovávala vlnu vo francúzskom Tourcoingu. Keďže aj prvé stroje pochádzali z Francúzska, mohlo to aj v Trenčíne vyzerať podobne ako na nasledujúcej fotografii.      Továreň na vlnu, Francúzsko. Obdobie na prelome 19. a 20teho storočia. Viac fotografii a starých pohľadníc by sme našli v knihe venujúcej sa spracovaniu textilu v Severovýchodnom Francúzsku. Zdroj: notreloft.com       Celkový pohľad na časť kasární a rozrastajúci sa priemyselný areál. Medzi pôvodnými budovami kasární a priemyselným areálom je viedieť nízke objekty vtedajšej vojenskej nemocnice. Zdroj: archív Trenčianskeho múzea      Detailnejší pohľad na priemyslovú časť. Vydavateľ: Prague JKO   Výrobky z „tiberghienky“ (bývalá Merina) nemali špičkovú kvalitu, no to sa pri cieľovej skupine ani nevyžadovalo. Výrobnú škálu tvorili hlavne jednoduché, farebné látky pre vidiecke obyvateľstvo a strednú vrstvu. Francúzi boli v začiatkoch zastúpení od vedúcich úradníkov až po majstrov. Aj ženy, ktoré zaúčali prišli z Francúzska. Niektorí sa v Trenčine usadili natrvalo. Zaujímavý je aj postreh z roku 1908, kde sa firma pokúšala nalákať „inteligentné dievčatá z lepších rodín“. Keďže mestské dievčatá považovali takúto prácu za dehonestujúcu, neuspeli.      Pre porovnanie. Prvý záber je z medzivojnového obdobia, okolo roku 1925. Dnes v spodnej časti fotografie stojí OC Max. Z pôvodných domov popri ulici zostali len dva. Vydavateľ: neznámy      Druhý záber z roku 2008. Časť z pôvodných budov naozaj ešte stojí. Aj vila pri kruhovom objazde stojí dodnes.      Prikladám pre zaujímavosť dobovú reklamu z kuchárskej knihy, vydanej Mestským ženským spolkom v roku 1944. Z reklám v knihe patrí k tým najväčším.   Za zmienku určite stojí aj areál parnej píly, ktorú je možné vidieť za továrňou Tiberchien. Píla spracovávala vyťažené drevo zo severozápadných svahov Inovca od roku 1912. Nielen píla, ale ja pozemky (lesy) patrili budapeštianskej spoločnosti Hazai Erdöipar Részvény T. Gözfürésze. Jej zámerom bolo drevo rýchlo vyťažiť a maximalizovať tak svoj zisk. Pre rýchlu a čo najefektívnejšiu dopravu bola z tohto areálu vybudovaná úzkorozchodná železnica. Mala rovnaký rozchod (760 mm) ako napr. Čiernohronská železnica v Čiernom Balogu a viedla cez Trenčín, Tr. Turnú, Tr. Stankovce, Soblahov, Mníchovú Lehotu a Selec. Dodnes je zachované trasovanie mosta pri Soblahove a napríklad úsek nad Mníchovou Lehotou sa využíva ako lesná cesta. Počas 1. sv. vojny tu pracovali ruskí aj rumunskí vojnoví zajatci. Drevo spoločnosť vyvážala do Maďarska, Rakúska, Švajčiarska a Nemecka. Po vzniku 1. ČSR preložila spoločnosť svoje sídlo do Trenčína a zmenila názov. V roku 1928 bola časť úseku železničky prerušená, neskôr bola zastavená aj samotná píla a drevo sa od 1930 predávalo priamo v horách. Rok 1931 znamenal koniec železničky. V miestach, kde kedysi stála píla je dnes areál spoločnosti Trens. V roku 2012 uplynie sté výročie od vzniku úzkorozchodnej železničky. K tejto príležitosti sa pripravuje aj publikácia zaoberajúca sa železničkou. Myslím, že to bude zaujímavé čítanie.       V popredí je možné vidieť továreň Tiberghien a za ňou je vidieť spomínanú pílu, ku ktorej viedla úzkorozchodná železnička. Vydavateľ: Prague JKO      Iný záber na danú lokalitu o niekoľko rokov neskôr. Vydavateľ: foto Tatra      Detailnejší pohľad. V popredí a aj na pravej strane fotografie je viedieť koľajnice a vozne úzkorozchodnej trate. Fungovala takmer 20 rokov. Dnes už by sme po nej v Trenčíne asi veľa nenašli. Zdroj: ReVUe Trenčín   Osudy ľudí, ich úspechy, neúspechy, pohnútky doby. Niečo sa dá vyčítať zo starých pohľadníc, fotografii, z archívov, písomností, korešpodencie, no mnohé príbehy si väčšina starých ľudí berie so sebou do hrobu. Tým sa končí aj možnosť uchovať niektoré skúsenosti, postrehy a aspoň čiastočne zaznamenať pohnútky doby. V Trenčíne už asi nežíjú ľudia, ktorí by si pamätali dožívajúcu monarchiu. Ešte však žijú ľudia, ktorí si túto časť našej (áno, aj našej) histórie pamätajú z rozprávania svojich rodičov. Aké by však mohlo byť rozprávanie starých Trenčanov, ktorí zažili Rakúsko - Uhorsko a aj neisté pomery v novovnikajúcej 1. Česko-Slovenskej republike? Nestálo by za to, pokúsiť sa "zachrániť" tieto spomienky aj pre ďalšie generácie? Možno by stačilo osloviť domovy dôchodcov, uverejniť výzvu v miestnych novinách, v regionálnej televízií. Som presvedčený, že by nemuselo ísť o finančne náročnú záležitosť. Možno by sme boli sami prekvapení, aké zaujímavosti by nám inak zmizli v nenávratne. Čas totiž neúprosne beží...   Ešte raz by som chcel upozorniť, že články venujúce sa starému Trenčínu nemajú za cieľ nahrádzať serióznu prácu pracovníkov múzea ani historikov. K tomu slúžia vydané vlastivedné monografie a ďalšie cenné knihy, ktoré sa touto problematikou zaoberajú. Objavil sa toziž názor, že: „ludia, ktori sa tvaria ako najvacsi Trencania - a aj Vy - v skutocnosti nevedia o Trencine takmer nic, ale recnit o tom budu ako najvacsi znalci.“ Uznávam, že o svojom meste viem ešte stále velmi málo, no rozhodne sa nechcem tváriť ako „najväčší znalec.“ To prenechám luďom, ktorí v tejto oblasti už niečo dokázali. Mojim cieľom je tak trochu vzbudiť aj u mladšej generácii záujem o svoje mesto a o jeho naozaj zaujímavú históriu.    Použitá literatúra:    Ján Hanušin, Trenčín na starých pohľadniciach, Dajama, 2. vydanie, 2005  Milan Šišmiš, Trenčín – Vlastivedná monografia 2, Alfa-press, 1996   Časopis ReVUe Trenčín, rok 2008       Videozáznam z 1. verejnej diskusie o budúcnosti trenčianskeho nábrežia si môžete pozrieť na tejto stránke.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Ulica 1. mája - 1. časť (Historické fotografie)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Pozvánka na výstavu Mestských zásahov Trenčín
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Pozvánka na verejnú diskusiu o otvorenej samospráve v Trenčíne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Ďakujem! Naozaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Ščepko 
                                        
                                            Trenčianskeho primátora prezývajú "Braňo Obálka". Prečo?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Ščepko
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Ščepko
            
         
        scepko.blog.sme.sk (rss)
         
                                     
     
        V roku 2010 ako spoluorganizátor zrealizoval 1. verejnú diskusiu na tému trenčianske nábrežie. Súčasťou ktorej bol bývalý primátor, architekti, ekológ s moderátorom Š. Hríbom. Cieľom diskusie bolo dostať tému nábrežia pred verejnosť a poukázať na pochybné zámery vtedajšieho vedenia mesta. Je organizátorom projektu Mestských zásahov Trenčín 2013.
 
http://www.mestskezasahy-tn.sk



     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    40
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Trenčín
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




