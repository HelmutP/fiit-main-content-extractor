
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Adrián Lachata
                                        &gt;
                Nezaradené
                     
                 Prečo mi, bohužiaľ, ostala iba kapurková 

        
            
                                    7.6.2010
            o
            18:51
                        (upravené
                8.6.2010
                o
                12:35)
                        |
            Karma článku:
                5.27
            |
            Prečítané 
            294-krát
                    
         
     
         
             

                 
                    Môžem odísť do Austrálie, získať aj austrálske občianstvo, no vždy budem Slovák. Vždy sa mi bude páčiť slovenský jazyk, slovenská  krajina,  slovenský duch.  Môžem sa snažiť o akúkoľvek nestrannosť, no ak budem môcť pomôcť iba jedným ľudom v rovnakých problémoch, srdce mi bude šepkať, pomôž Slovákom.
                 

                 Na Slovensku sa mi nepáči veľmi veľa vecí. No predsa si myslím, že máme to šťastie, mať sa dobre. Máme čo jesť, vždy pitnú vodu, obdobie teplého slnka i obdobie čistého snehu. Môžeme byť na Slovensku slobodní, no môžeme aj odísť, ak sa nám nepáči. Naučíme sa cudzí jazyk sme takmer nerozoznateľní od občanov najvyspelejších štátov. Dokonca mame aj tú výhodu, že slovenčina nemá taký silný akcent, a pri troche snahy získame prízvuk na nerozoznanie od rodeného domáceho. Teda, zmieril som sa s tým, že hoci sa mi nad veľa vecami zastavuje rozum, je fajn, žiť na Slovensku.  Idú voľby. Ako správny občan, by som mal ísť voliť. Neobávam sa prepadnutia svojho hlasu, ale nechcem iba stále vyberať menšie zlo. Ide mi o to, aby bol môj hlas čestný predo mnou samým. Koalíciu môžem vylúčiť a voliť nejakú veľkú stranu už nemám chuť. I keď veľmi na mňa zapôsobili pani Lucia Žitňanská a pán Eugen Jurzyca kandidátke SDKU-DS.  Mám pocit (pocit laika), že sú to čestní ľudia, ktorí sa riadia ľudskými hodnotami a chcú poskytnúť svoje vedomosti a skúsenosti ľudom. Ak by som mohol voliť iba ich dvoch, asi by som tak spravil.  Ako zaujímavá alternatíva sa javí strana SaS. Pravdupovediac, celkom som aj potešil, že možno mám koho voliť. No netešil som sa dlho. Predseda strany mal na svojom sme blogu uvedený iba jediný cieľ. Dostať sa do parlamentu. Ach, nepríde mi to ako nejaký nový cieľ. Ďalej som skúsil zistiť, čo vlastne to SaS znamená. Sloboda a Solidarita. Ou. Pre mňa osobne pojem Sloboda znamená veľmi veľa. Sloboda, za ktorú naši predkovia dali život. Nie pre nich. Ale pre nás. A za ktorú vsadím kedykoľvek život aj svoj. Dať si slobodu do nazvú mi príde ako zneuctenie jej samotnej. Myšlienky, princípu. Už to nie je tá sloboda, za ktorú zomierali naši predkovia s vierou lepšej budúcnosti. Už je to sloboda toho, kto si ju dal do názvu.  Pojmom solidarita som si nebol úplne istý, a tak som načrel so slovníka. Solidarita znamená dobrovoľnú spoločenskú súdržnosť, ochotu ku vzájomnej pomoci a podpore v rámci nejakej skupiny. Človek je solidárny s nejakou skupinou, pokiaľ podporuje jej úspechy i neúspechy pociťuje ako vlastné. Nemali sme tu nejakú (iba jednu) skupinu nie tak dávno?  Osobne by som asi aj ocenil, keby mala strana SaS na starosti ekonomiku. Z toho, čo viem, ide prevažne o úspešných podnikateľov, ktorí sú v ekonomike doma. Aj myšlienka súkromného podnikania sa mi veľmi páči, lebo rapidne znižuje korupciu. Jednoducho každý podnikateľ si chráni svoje záujmy a robí pre seba to najlepšie, čo môže v rámci zákona. A tak je konkurencia, a keď má človek slobodu výberu, musí podnikateľ stále skvalitňovať svoje služby, ak chce prežiť (princíp veľmi podobný evolúcií). Štát do toho nezasahuje, lebo nejde o štátne peniaze, ale peniaze konkrétneho podnikateľa. Vláda poskytovala cenné veci za pár korún, ale to len preto, že nedávali peniaze so svojho vrecka, ale so spoločného vrecka všetkých. Za komunizmu sa koľko kradlo, a to len preto, že všetko bolo všetkých... Ale čo sa týka celkového vládnutia, nie je na to strana SaS vyzretá a nedá sa všetko vidieť iba ako business... Ale to nie iba SaS, pravdepodobne žiadna strana.   Osobne mi veľmi chýba diferencia politických strán. Každá strana chce všetko riadiť a tak mrhá čas aj veci, ktoré by bolo naozaj lepšie prenechať iným, namiesto toho aby neustále pracovala na tom, čo jej ide najlepšie. Ako som spomínal, som politicky laik. No asi ťažko sa nájde lepší kandidát na ministra spravodlivosti ako je pani Lucia Žiťňanská. Ekonomika je zvláštna, lebo SDKU-DS ma pravdepodobne tiež veľa schopných ekonómov. Čo je veľa ľudí na jednu oblasť a ostatné oblasti sú potom zanedbávané, lebo odborníci na ďalšie oblasti nedostanú do parlamentu. Vezmite si napríklad šport, školstvo alebo iné veci, nezačínajúce na š.    Ako som povedal, chcem dať niekomu svoj hlas s čistým svedomím. Najhodnotejšie princípy demokratickej spoločnosti, ake poznám, sú sloboda a demokracia. Preto som si veľmi všímal medializované prejavy slobody a demokracie z rádov občanov a ich podporu u politikov. Tieto príklady si pamätám najviac.  Príklad č.1 (rok 2006):  Primátorka Zlatých Moraviec sa rozhodla držať hladovku pred Úradom Vlády SR pre dobudovanie cesty, ktorá by zachránila ľudské životy. Podpora zo strany politikov, žiadna.  Príklad č.2 (rok 2009): Študent hladoval pred parlamentom za zrušenie imunity. Okoloidúci zahraniční turisti za pristavovali a pýtali sa na dôvod. Väčšina iba nechápavo kývali hlavou, ako môže mať politik iné práva ako občan. Politici sa tvárili, že ho nevidia. Možno si myslia, že najviac sa priblížia ľudom, ak budú imúnni.(Všetci svine sú si rovné, no niektoré sú si rovnejšie. George Orwell, Zvieracia farma).  Príklad č.3 (rok 2010): Protest proti vlasteneckému zákonu pred prezidentským palácom stoviek študentov. Dostalo sa tomu protestu podpory. Bohužiaľ dosť pochybnej, lebo sa jednalo iba o slovenských politikov s maďarskou orientáciou. Slovenskí politici so slovenskou orientáciou sa neukázali.  Príklad č.4 (rok 2010): Dúhový Pride za ľudské práva ľudí s inou sexuálnou orientáciou ako heterosexuálnou. Za akceptáciu človeka v spoločnosti. Akciu za demokratické práva prišli podporiť osobnosti svetového kultúrneho , spoločenského života i politického života. No bez podpory jediného slovenského politika.  Všetky uvedené príklady sa odohrali v centre Bratislavy, hlavného mesta Slovenskej Republiky.  Viete, myslel som si, že sloboda a demokracia je presne to, prečo v demokratickej krajine existuje politika. Že politici sú tu na to, aby slúžili  ľudu a obhajovali slobodu a demokraciu i za cenu vlastného  života.  No mám pocit, že politikom nejde o skutočné blaho ale ide im, v lepšom prípade,  o presadenie svojej pravdy, ktorú neomylne považujú za najlepšiu, bez toho, aby o svojej pravde aspoň na chvíľu zapochybovali. A ak sa ľudia rozhodnú povedať NIE , tak slepo veria v tú ich pravdu, že nepočúvajú. Keď sa začnú blížiť voľby, začnú míňať horibilné sumy ale nie na to, aby ľudí počúvali, ale na to, aby ich presvedčili o ich neomylnej pravde a nahovárajú im, že jediné, čo môžu spraviť, je isť voliť ich pravdu.   Politika mi už príde o ľudoch, no bez ľudí. Preto svojím hlasom poviem, že chcem voliť, ale s čistým svedomím naozaj nemám koho. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Otvorený podrav prezidentovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Odkaz pre mladých študentov a školákov (Khan Academy)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Za pád vlády môže strana SMER, a to jednoznačne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Známkovanie telesnej výchovy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Notebook pre študenta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Adrián Lachata
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Adrián Lachata
            
         
        lachata.blog.sme.sk (rss)
         
                                     
     
        Uvažujúci individualista, symbiotický schizofrenik.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1183
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Programovanie
                        
                     
                                     
                        
                            Zážitky
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Výpisky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Antropoložka na Marsu
                                     
                                                                             
                                            Karla Čapka
                                     
                                                                             
                                            Dekameron
                                     
                                                                             
                                            Candide
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janette Maziniová
                                     
                                                                             
                                            Dávid Králik
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mal iba pätnásť
                     
                                                         
                       Prečo ešte Harabin nie je v červenej knihe?
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Matka spí s mužom svojej dcéry, to je ponuka JOJ pre siedmakov
                     
                                                         
                       O stratenom a premárnenom čase
                     
                                                         
                       O pravdu asi všeobecne nie je záujem
                     
                                                         
                       Dôležitý nie je žiak, ale byrokracia!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




