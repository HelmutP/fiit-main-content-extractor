

  
 Snežienky. 
  
 Polia bielych hlávok končiace v nedohľadne. 
  
 Rok čo rok vítajú jar aj napriek stádam lovcov-zberačov, čo vynášajú z lesa plné hrste umierajúcich kvetov. 
  
 Keď sme boli malí, vraveli sme, že si ich tí zvláštni ľudia trhajú na šalát. (prosím, snežienkový šalát radšej doma neskúšajte)  
  
 Veterník žltuškovitý. 
  
 Všade plno klíkov - človek môže len hádať, čo z nich raz vyrastie. Žeby chochlačka? 
  
 Súkvetia chochlačiek mätú pozorovateľa rafinovanými odtieňmi od bielej, cez rôzne fázy ružovej až do fialova... 
  
  
 Ako dieťa som odmietala uveriť, že také rozdielne kvety môžu patriť k jednému druhu. 
  
 A možno ani nepatria, ale kto rozozná chochlačku dutú od chochlačky plnej, keď rozdiel v názvoch nehovorí o farbách, ale o dutej či plnej podzemnej hľuze? 
  
 Fialka skromne kloní hlávku k zemi... Asi naschvál, nerada stojí modelom. 
  
 Pečeňovník - odjakživa môj obľúbenec. Hoci sa v minulosti používal na liečenie chorôb pečene, tiež ho nemožno odporúčať do šalátu. 
  
  
 Pľúcnik - ďalšia liečivka pomenovaná podľa orgánu. Navyše - žilkovanie na okvetných lístkoch pripomína "komôrkatý" vzor pľúcneho tkaniva... 
  
  
 Ohnivec - neprehliadnuteľná huba v jarnom lese. 
  
 Blyskáč - vtedy ešte ojedinelý zjav. 
  
 Krivec - mne vždy pripadal celkom rovný... 
  
 Drienka - drevina kvitnúca malými slniečkami. Posledná zastávka na prechádzke čerstvo zrodenou jarou. Čas vydať sa späť do "teraz", kľučkovať pomedzi kvapky. 

