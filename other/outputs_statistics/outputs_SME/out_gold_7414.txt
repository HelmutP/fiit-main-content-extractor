

 Viktor Orbán utrpel vo voľbách Pyrhovo víťazstvo. S ústavnou väčšinou v parlamente sa bude ťažko vyhovárať, že nemá možnosť (a povinnosť) vytiahnuť Maďarsko z ťažkej hospodárskej situácie. Pripomeňme si, že Maďarsko čerpá pôžičku od Medzinárodného menového fondu (MMF), pretože podobne ako Grécko stratilo schopnosť si požičať na finančných trhoch. Maďarsko je teda reálne v nútenej správe, ktorú vykonáva MMF. Maďarsko, podobne ako Grécko, prisľúbilo šetriť a znižovať deficit. A do tretice, v Maďarsku, podobne ako v Grécku dúfajú, že tie sľuby nebudú musieť splniť. 
 Akokoľvek potrebné, uťahovanie opaskov obyvateľov Maďarska Orbánovi voličov uberie. Preto sa rozhodol si zadovážiť voličov aj mimo územia Maďarska. 
 Róber Fico koná veľmi podobne. Slovenskí Maďari ho budú sotva voliť. Preto je pre neho lepšie odovzdať týchto občanov Orbánovi. Nehľadí na riziko, že tým aktívne prispeje k vytvoreniu situácie, keď na časti slovenského územia nebudú v menšine len občania slovenskej národnosti, ale aj občania Slovenskej republiky. 
 Róber Fico ako bonus získa aj možnosť obrať o občianstvo tých Slovákov, ktorí sa dlhodobo zdržujú v zahraničí a je pre nich praktické mať tamojšie občianstvo. Títo „svetobežníci" tiež zjavne nepatria medzi voličov našich národných socialistov. A bezplatnú stratu slovenského občianstva niektorí dokonca môžu privítať. 
 Fico s Orbánom sa pokúšajú o niečo podobné, ako sa úspešne podarilo Mečiarovi s Klausom. Rozdeliť si voličov (a nebuďme naivní, že téma územia by zostala dlhodobo mimo tento obchod) tak, aby každý dostal tých sebe verných. Čo si budeme nahovárať. Smer s.r.o. podobne, ako Fidesz s.r.o. zatiaľ nutne potrebujú k existencii tzv. „demokratickú" voľbu občanov. A prečo by mali prispôsobovať svoju politiku týmto občanom, keď môžu zapracovať aj na tom, kto bude tým občanom. 
   
   

