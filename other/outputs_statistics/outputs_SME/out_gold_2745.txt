

 Minulý rok zomrelo na našich cestách o 40% ľudí menej ako v roku 2008: 347 namiesto 558 osôb. Zaujímalo ma teda, či aj v televíziách bolo obetí zobrazovaných menej. Teda či novinári kopírujú realitu, ale príťažlivosť dopravných tragédií je pre spravodajstvo taká silná, že skutočný vývoj v nehodovosti veľmi nesúvisí s tým, koľko havárií a smrteľných obetí divák spravodajstva na obrazovke dostane. Keďže podobný prehľad som robil už pre rok 2005, keď bolo mŕtvych na cestách takmer rovnako ako v roku 2008, pripojil som tie čísla do porovnania. Ako reprezentantov súkromného a verejnoprávneho média som vybral Markízu a STV. Tu sú výsledky: 
 
 



úmrtia v dopravných nehodách
2005
2008
2009


Markíza
36
65
61


STV
9
11
27


Realita pre SR
560
558
347








 Ako vidno, hoci v rokoch 2005 a 2008 bolo obetí na cestách zhruba rovnako, Markíza takmer zdvojnásobila počet obetí dopravných nehôd, o ktorých informovala vo svojich správach. Tu zrejme nárast môže súvisieť s tým, že Televízne noviny zdvojnásobili svoju dĺžku. Roky 2008 a 2009 sú však formátom spravodajstva Markízy celkom porovnateľné, a ako ukazujú čísla, napriek prudkému reálnemu poklesu úmrtí televízia znížila počet „televíznych“ obetí len minimálne, z 65 na 61. Verný divák Markízy teda nejaký dojem o veľkom poklese nehodovosti mohol získať akurát z pár reportáží, ktoré o zmene v štatistikách aj informovali. 
 STV dokonca počet obetí v správach výrazne stúpol, čo je hlavne dôsledok minuloročnej tragédie v Polomke (12 mŕtvych). Aj bez Polomky by však STV ukázala viac obetí nehôd minulý rok ako v roku 2008, hoci nastal v ich počte spomínaný takmer 40 percentný pokles. Pri STV je však ťažko robiť záver o príklone k bulvarizácii vzhľadom na relatívne malý počet reportáží o obetiach nehôd (každoročne menej ako 10). 
 V každom prípade získané čísla skôr podporujú ako vyvracajú tézu, že smrteľné dopravné nehody sú pre televízie „dobré“ – majú drámu, dynamické obrázky, emócie. Na rozdiel od diskusií o deficite verejných financií, napríklad. A tak teda televízie budú motivované mať ich v spravodajstve aj keď by boli v skutočnosti pre život občanov menej a menej dôležité.  Prečo je zaujímavé robiť takéto porovnania? Médiá totiž realitu spoluvytvárajú, a aj na základe ich informácií sa ľudia rozhodujú a konajú. Čím je relatívna váha udalostí v médiách bližšia skutočnosti, tým lepšie rozhodovanie verejnosti. Britskí akademici pred niekoľkými rokmi porovnali, v akom pomere sa vybrané britské médiá venujú rôznym rizikám pre zdravie a život jednotlivcov. Porovnali počet ročných úmrtí na dané riziko, a počet článkov tomu venovaných. Zistili veľký nepomer. Kým na jeden článok či reportáž o rizikách fajčenia pripadá takmer 8.600 úmrtí ročne, na jeden článok o chorobe šialených kráv pripadlo 0,3 úmrtia. Čiže médiá ďaleko viac priestoru venovali téme, ktorá má len malý dopad na životy verejnosti, ako riziku, ktoré bežne ohrozuje kvantá ľudí.   
 Prieskum bez SaS: Rastislav Turek ma upozornil, že keď TV Joj minulý utorok informovala o prieskume politických preferencií od agentúry MVK, v grafike ale aj sprievodnom slove televízia celkom vynechala stranu SaS. Hoci tá bola podľa prieskumu štvrtá najsilnejšia strana s 9,2%. Mala ju v správe aj TASR, ktorú Joj cituje. Okrem toho SMK nemala 5,2% ale 6%. A nehovoriac o tom, že grafický rozdiel medzi Smerom a SDKÚ je len zhruba dvojnásobný, hoci by mal byť podľa čísel takmer trojnásobný. Smer teda na grafe nevyzerá tak dominantný, ako podľa prieskumu naozaj je. 
  
 Dosť veľký prešľap. 
   
 Ticho v STV: Kým väčšina hlavných médií sa chytila objavu TRENDu o ďalšej veľkej podozrivej reklamnej zákazke, tentokrát na Ministerstve životného prostredia, pre verejnoprávnu televíziu nie je mrhanie verejných financií vo veľkom zaujímavou témou.  Podobne takmer všetky médiá sa zmienili o kritike Slovenska v Správe o ľudských právach, ktorú vydáva Ministerstvo zahraničných vecí USA. Spomínajú sa tam aj politické tlaky v STV.  Dotknutá televízia sa s takýmito informáciami so svojimi divákmi pre istotu nepodelila. Vtipné je, že keďže STV opakuje neskoro večer aj správy Českej televízie, diváci STV o správe aj počuť mohli, hoci nie o jej obsahu týkajúcom sa Slovenska. 
 PS Upozornenie - tento blog podporuje ambasáda USA na Slovensku. 
   
 Sankcie v rozhlase: Šéfka spravodajstva v Slovenskom rozhlase Anna Sámelová mi zaslala rozhodnutie o budúcnosti redaktorky Anny Síposovej, ktorá sa najprv dopustila plagiátu a následne ospravedlňovala svoju chybu falošným dôkazom. Síposová môže v rozhlase naďalej pracovať, ale dostala finančné sankcie a dištanc pre pôsobenie v určitých reláciách. Komplet vysvetlenie v update pôvodného článku na blogu. 

