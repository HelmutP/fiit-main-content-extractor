

 Treba si uvedomiť, že striedavé radenie je nástroj podporujúci plynulosť 
cestnej premávky a práve preto bol vymyslený. 

 Predpisy na Slovensku sa venujú striedavému radeniu len okrajovo v troch bodoch. Konkrétne: zákone 8/2009 Z.z., 
§ 10 Jazda v jazdných pruhoch, odstavec 6 a odstavec 10. Vykonávacia vyhláška 9/2009 Z.z. dopravná značka C30. 


 Skúsim problematiku striedavého radenia na dvojprúdovej ceste vysvetliť na príkladoch: 

  
Obr. 1. Na ceste je prekážka, napríklad vo forme jamy po prasknutom potrubí. 
Popisované situácie sú zľava doprava. 

 Ak ide vozidlo po ceste samé, nie je reálny a ani právny problém. Preradí sa 
z pravého pruhu a pokračuje v jazde v ľavom pruhu. 

 Ak ide o súbežnú jazdu jazdu viacerých vozidiel, ktoré sa už striedavo radia, 
tak vodič prichádzajúceho vozidla pokračuje v striedavom radení. Nie je reálny a 
ani právny problém. 

 Ak k prekážke prídu dva prúdy vozidiel, kde na čele sú vozidlá  vedľa 
seba, tak je potrebné riadiť sa zákonom aby bolo zrejme kto ma prednosť. V tomto 
prípade použijeme 8/2009 Z.z. §10 ods. (10) Ak je pri súbežnej jazde v niektorom 
jazdnom pruhu prekážka cestnej premávky, vodič vozidla idúceho vo voľnom jazdnom 
pruhu je povinný umožniť vodičovi prvého vozidla nachádzajúceho sa v jazdnom 
pruhu, v ktorom je prekážka, jej obídenie, ak ten dáva znamenie o zmene smeru 
jazdy. Vodič obchádzajúci prekážku pritom nesmie ohroziť vodiča jazdiaceho vo 
voľnom jazdnom pruhu. Z uvedeného je zrejmé, že vozidlá pôjdu v poradí 
4,1,5,2,6,3. Aj táto situácia je po reálnej aj právnej stránke jednoznačná. 

   
 
 
Obr. 2. Na ceste je prekážka, napríklad vo forme jamy po prasknutom potrubí, ale prišla havarijná služba, ktorá zabezpečila miesto zvislým dopravným značením. Otázka je čo sa zmenilo voči predchádzajúcemu stavu. 

 Ak ide po ceste jediné vozidlo nie je reálny a ani právny problém. Preradí sa 
z pravého pruhu a pokračuje v jazde v ľavom pruhu. 

 Ak ide o súbežnú jazdu jazdu viacerých vozidiel, ktoré sa už striedavo radia, 
tak vodič prichádzajúceho vozidla pokračuje v striedavom radení.  

 Ak k prekážke prídu dva prúdy vozidiel, kde na čele sú vozidlá  vedľa 
seba, tak je potrebné rozhodnúť kto má prednosť. V tomto 
prípade sa bijú dve ustanovenie zákona 8/2009 Z.z. § 10 Jazda v jazdných pruhoch 
(6) Vodič pri prechádzaní z jedného jazdného pruhu do druhého jazdného pruhu je 
povinný dať prednosť v jazde vodičovi jazdiacemu v jazdnom pruhu, do ktorého 
prechádza; pritom je povinný dávať znamenie o zmene smeru jazdy. Tam, kde sa dva 
jazdné pruhy zbiehajú do jedného jazdného pruhu tak, že nie je zrejmé, ktorý z 
nich je priebežný, vodič jazdiaci v ľavom jazdnom pruhu je povinný dať prednosť 
v jazde vodičovi v pravom jazdnom pruhu; to neplatí, ak ide o striedavé radenie 
upravené príslušnou dopravnou značkou. 
(10)  Ak je pri súbežnej jazde v niektorom 
jazdnom pruhu prekážka cestnej premávky, vodič vozidla idúceho vo voľnom jazdnom 
pruhu je povinný umožniť vodičovi prvého vozidla nachádzajúceho sa v jazdnom 
pruhu, v ktorom je prekážka, jej obídenie, ak ten dáva znamenie o zmene smeru 
jazdy. Vodič obchádzajúci prekážku pritom nesmie ohroziť vodiča jazdiaceho vo 
voľnom jazdnom pruhu. 

 Vo všeobecnosti sa uznáva to, že dopravné značenie má prednosť pred všeobecným 
ustanovenýmzákona, preto prvé vozidlo by malo ísť vozidlo číslo 1 a  nie
vozidlo číslo 4. Je to presne opačne ako v predchádzajúcom prípade, keď tam nebola dopravná značka o znížení počtu jazdných pruhov. 


   
 
 
Obr. 3. Keď sa projektujú komunikácie, tak sa dávajú dva jazdné pruhy hoci 
ďalej pokračuje len jeden. Prečo sa to robí a nedá sa hneď len jeden pruh? Je to 
preto aby sa vozidla naakumovali v časti cesty s dvomi jazdnými pruhmi a 
neblokovali križovatky, ktoré by blokovali aby by stáli len v jednom jazdnom 
pruhu za sebou. 

 Ak ide samotné vozidlo tak nie je problém preradí sa do ľavého jazdného 
pruhu. 
 Ak ide o súbežnú jazdu jazdu viacerých vozidiel, ktoré sa už striedavo radia, 
tak vodič prichádzajúceho vozidla pokračuje v striedavom radení. Lenže je tu 
právny problém takéto radenie nie je v zmysle nášho zákona povinné 
(pozor v zahraničí často povinne je). 

 Ak k miestu zníženia počtu jazdných pruhov prídu dva prúdy vozidiel, kde na čele sú vozidlá vedľa seba, tak prednosť má vozidlo v priebežnom pruhu. Otázne je, či za ním je povinnosť vzniku striedavého radenia, alebo vozidla v pravom pruhu majú zastať a čakať kým sa ľavý jazdný pruh ľubovoľne dlhý, nevyprázdni. Podľa všetkého právna povinnosť striedavého radenia v tomto prípade podľa aktuálnych predpisov na Slovensku neexistuje, i keď v rámci ohľaduplnosti a plynulosti je bežnýmjavom.  


   
 Záver: 
Existuje aj možnosť použitia dopravnej značky C30 - Striedavé radenie, ktorá 
môže prikázať striedavé radenie aj za iných okolnosti. Napríklad striedavé 
radenie z hlavnej aj vedľajšej cesty.  

 Je si potrebné uvedomiť, že v prípade  zrážky vozidiel sa prednosť 
pri striedavom radení veľmi ťažko dokazuje a policajti často nemajú inú možnosť 
ako to dať 50:50. 

 Striedavé radenie sa deje zvyčajne pri nízkych rýchlostiach. Zatiaľ jediný 
raz som sa stretol zo striedavým radeným pri vysokej rýchlosti
(100 km/hod na diaľnici), 
ale tam bolo pred miestom radenia definovaný odstup vozidiel 50 metrov, čiže 
bolo dosť miesta aby pokračoval jeden jazdný pruh o pôvodnej rýchlosti. Teraz 
takýchto postup chystajú v susednej Českej republike. 

 V zahraničí je zvyk pri striedavom radení taký, že vozidla idú vo všetkých 
jazdných pruhoch a radia sa až na konci. Je to z toho dôvodu, že ak by 
sa radili pribežne, tak rýchlosť priebežného pruhu sa výrazne spomalí až dôjde k 
jeho zastaveniu. U nás si na to vodiči ešte nezvykli a preraďujú sa 
čo najskôr do priebežného jazdného pruhu a potom nadávajú na ostatných vodičov, 
lebo si myslia, že sa predbiehajú. 

 Treba si uvedomiť, že vo veľa štátoch je striedavé radenie pri dopravných 
zápchach dennodenná samozrejmosť, bez ohľadu na doslovnosť textu zákona, lebo inak by doprava úplne skolabovala. 

 U nás by sa zišlo v zákone 8/2009 Z.z. dodefinovať niektoré špecifické 
situácie pri striedavom radení, aby táto problematika bola jednoznačná. 

