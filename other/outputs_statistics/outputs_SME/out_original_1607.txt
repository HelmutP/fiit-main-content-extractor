
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Sveťo Styk
                                        &gt;
                Politika
                     
                 Deň nezávislosti (Bieloruska) 

        
            
                                    8.9.2005
            o
            20:28
                        |
            Karma článku:
                7.43
            |
            Prečítané 
            5286-krát
                    
         
     
         
             

                 
                    Pravdepodobne málokto z nás počastuje srdečnou návštevou susedov, o ktorých sa verejne vie, že žijú neusporiadaný život - matka je alkoholička, otec despotický tyran, ktorý trestá pesťou aj trochu odvážnejší pohľad detí, z ktorých jedno je vydržiavané v obéznej sýtosti, a druhé je vyháňané na ulicu, lačné a otrhané.    K takýmto susedom sa asi nepohrniete pri výriočí ich svadby s fľašou drahého vína a kyticou kvetov. Zvlášť keď patríte k združeniu podnájomníkov, ktorí unisono vyzývajú dospelú časť rodiny k rozumu a inak s ňou neudržiavajú žiadny kontakt. 
                 

                 
  
  Nášmu nádejnému víťazovi budúcoročných volieb a mnohoročnému čakateľovi na kreslo premiéra Róbertovi Ficovi sa to ale podarilo - spolu so zástupcami KSS (ak jediní predstavitelia parlamentu) navštívil bieloruské veľvyslanectvo pri príležitosti osláv Dňa nezávislosti.    Kôli objektivite treba povedať, že sa nejedná o prvý prejav ústretovosti pána Fica voči režimu Alexandra Lukašenka - už v roku 2003 bola medializovaná jeho návšteva Bieloruska spolu so skupinou podnikateľov, čo bolo miestnou propagandou samozrejme využité a predkladané ako dôkaz, že tento režim je akceptovaný aj na parlamentnej úrovni - do akej miery je to pravda, tuší asi len samotný pán Fico. Faktom ale zostáva, že samotná Európska únia, a samozrejme aj krajiny v nej združené, nevynechajú jedinú príležitosť ku kritike nedemokratického a autokratívneho režimu posledného európskeho polodiktátora, ktorý sa na území "svojho" štátu neštíti použiť žiadne prostriedky, ktorými si upevní svoju pozíciu - či už ide o gumenné prispôsobovanie si ústavy, alebo o faktickú likvidáciu akejkoľvek opozície, prenasledovanie novinárov, alebo kopec iných metód upevnenia si moci, využívaných celé veky v ktoromkoľvek autoritatívnom štáte.    Že sa na oslavách zúčastnili komunisti, ma vôbec neudivuje - opak by bol zarážajúci - určite im pri predstave krajiny, v ktorej sa netoleruje iný názor, ako ten jediný správny, behá vzrušením mráz po chrbte. Ale v prípade predsedu najpopulárnejšej strany je to už trochu iná káva. Jedine, ak by sa tam šiel pán predseda spýtať na stav bieloruskej spoločnosti, na príčiny prenasledovania mnohých ľudí, nestotožňujúcich sa s oficiálnou štátnou líniou, na príčiny násilného prerušovania opozičných demonštrácií a podobne. Obávam sa ale, že nič podobné tam nezaznelo. Žiaľ.     A v tých obavách ma utvrdzuje aj úryvok z oficiálnych straníckych novín Smeru Slovenský rozhľad, kde sa redaktor zhováral s bieloruským veľvyslancom na Slovensku Valerijom Voroneckým, a ako prvú otázku zo seba vysúkal: "Keďže súčasná slovenská propaganda je veľmi skúpa na akékoľvek informácie o Bielorusku, okrem sústavnej kritiky bieloruského režimu a tamojších politických pomerov, chceme vás najskôr požiadať o bližšiu charakteristiku súčasného Bieloruska, čiže chceme vidieť to, čo sa dnes skrýva za tou našou propagandou, vytvorenou umelou ideologickou oponou." (SME, 08/09/2005)    Mňa by zaujímalo, čo na celú záležitosť hovorí poslankyňa europarlamentu a tieňová ministerkyňa zahraničných vecí, Monika Beňová....

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (97)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Sveťo Styk 
                                        
                                            Zachovajme pokoru, prosím!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Sveťo Styk 
                                        
                                            Sklamanie!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Sveťo Styk 
                                        
                                            Na rovinu: to už nám vážne všetkým šibe?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Sveťo Styk 
                                        
                                            Kýbel špiny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Sveťo Styk 
                                        
                                            Mám dobrý pocit.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Sveťo Styk
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Sveťo Styk
            
         
        styk.blog.sme.sk (rss)
         
                        VIP
                             
     
        Človek, ktorý sa snaží hľadať rovnováhu a mať oči otvorené...





        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    409
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2905
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Fotky
                        
                     
                                     
                        
                            Knihy
                        
                     
                                     
                        
                            Koníčky
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Od spriaznených duší
                        
                     
                                     
                        
                            Pudlíkoviny
                        
                     
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Slotensko
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sapkowski - Svetlo večné
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            King`s X - XV
                                     
                                                                             
                                            Steve Vai - all
                                     
                                                                             
                                            The Flower Kings - Instant Delivery
                                     
                                                                             
                                            Neal Morse - Sola Scriptura
                                     
                                                                             
                                            Avantasia
                                     
                                                                             
                                            Dream Theater - all
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ivan Spacek
                                     
                                                                             
                                            Ross Hedvicek
                                     
                                                                             
                                            Ross Hedvíček - SME
                                     
                                                                             
                                            Majka Lazarová
                                     
                                                                             
                                            Pavel Škoda
                                     
                                                                             
                                            Marianna  Varjanová
                                     
                                                                             
                                            Ďuri Balucha
                                     
                                                                             
                                            Peťka Jankovičová
                                     
                                                                             
                                            Luplik
                                     
                                                                             
                                            Peťo Lehotský
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            O našich cicuškách
                                     
                                                                             
                                            Rock jam radio
                                     
                                                                             
                                            Moje fotky
                                     
                                                                             
                                            SME
                                     
                                                                             
                                            Zive
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Niečo je zhnité v slovenskej pošte (porovnanie s rakúskou)
                     
                                                         
                       Prihlásenie vozidla na dopravnom inšpektoráte? Katastrofa
                     
                                                         
                       Kontroverzný plot pred americkou ambasádou ostáva aj neostáva
                     
                                                         
                       Zachovajme pokoru, prosím!
                     
                                                         
                       Prosím, nevoľte Fica
                     
                                                         
                       Ak Fico chce refendum, tak mu ho dajme
                     
                                                         
                       Sklamanie!
                     
                                                         
                       Andrej Kiska - prvý krok pre lepšie Slovensko?
                     
                                                         
                       Na rovinu: to už nám vážne všetkým šibe?
                     
                                                         
                       Riziká pred druhým kolom prezidentských volieb
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




