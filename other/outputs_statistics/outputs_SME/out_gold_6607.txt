

 Na Slovensko všetko prichádza akosi oneskorene, akoby táto krajina žila v mystérioznom vákuu izolovanosti a mentálne v minulom storočí. Áno, nakoniec sem všetko raz dorazí, akosi predražené, tak trochu vyslabnuté, bez intenzity a tesne pred dobou expirácie. Koniec koncov 
Slovensko je posledná krajina v Európskej únii kde sa ešte Gay Pride nekonal.
  
Nebudem sa rozpisovať o histórii gay pochodov, ktoré v zapadných krajinách sú  skôr len symbolom karnevalovej tradície  s alegorickými vozmi ako symbolom za demonštráciu práv homosexuálov.
  
Vo svete sa vedie diskusia o tom či uzákoniť homosexuálne manželstvo
a na Slovensku ešte nie je ani zákon o registrovanom partnerstve. To čo je vo svete normálne, je v tejto krajine považované buď za nenormálne, alebo dokonca nemorálne. Niekedy ani ja neviem rozoznať Adama od Evy a vice versa, ale netrápi ma to, pretože za pár tisíc rokov všetci budeme hermafroditi. Nie som ani matka, ktorá zakrýva pohoršene oči svojmu dieťaťu keď vidí dvoch mužov držať sa za ruky a ani samonasieraci dôchodca keeerýýý povííí, že za všeckúúú tú sodóóómu gomóóóru môže žid Rockeffeler, Ipod a americké filmy!
Nie som veľa vecí, dejov a javov možno aj preto, že som trisexuál a feministka so silným sklonom k vulgarizmom.  Ja viem, že trisexualita ešte tiež na Slovensko nedorazila, žiaľbohu, a niekedy je to aj chvalabohu, že žiaľbohu:-) 


  
Nech už je prvý slovenský "gejprájd" o čomkoľvek, podľa mňa by mal byť hlavne o tom, aby poukázal na to, že v tejto krajine existuje menšina, ktorej sú upierané občianske práva, vrátane práva na registrované partnerstvo dvoch ľudí rovnakého  pohlavia. Niekto môže argumentovať, že určité vzťahy sú riešiteľné
aj bez registrovaného partnerstva občianskym zákonníkom. Súhlas, na lásku
človek nepotrebuje papier. V praxi však dvaja heterosexuálni partneri napr. nemajú problém požiadať si o hypotéku, prečo potom upierať túto možnosť
dvom homosexuálnym partnerom? Pretože na Slovensku neexistuje zákon o registrovanom partnerstve a hovoriť o adopcii homosexuálmi na Slovensku je asi skôr v sfére scifi aj napriek tomu, že vo svete je to bežná prax. Iný kraj, 
iný mrav.  Mimochodom...pre odporcov adopcie homosexuálmi vrelo doporučujem 
navštíviť akýkoľvek detský domov. Z pragmatického hľadiska dieťaťu, ktoré vyrastá
bez rodičov bude vždy lepšie v rodine dvoch matiek, alebo dvoch otcov než vyrastať v prostredí, ktoré mu nedokáže poskytnúť emočnú istotu. Vo výchove dieťaťa medzi heterosexuálnymi, alebo homosexuálnymi pármi nie je kvalitatívny rozdiel. Je to len iné...a o tom to celé je. O inakosti a schopnosti nielen ju vnímať, ale aj tolerovať. Źi a nechaj žiť? Tento srdcervúco humanitný slogan nepomohol? Tak čítajte pani doktorka ďalej...:-) 


  

Barometer vyspelosti každého národa podľa mňa reprezentuje čistota verejných záchodov a miera tolerancie k menšinám. Ak sa k akejkoľvek menšine správa ako ku kategórii druhoradých občanov tak tu nastáva problém. Nielen legislatívny, ale problém morálny. Je prirodzené, že človek  sa mentálne bráni niečomu čo nepozná. Prelomiť strach z neznámeho a nepoznaného. Prijímať a akceptovať to čo je človeku cudzie si vyžaduje dávku odvahy a tolerancie, ktorú človek získa buď výchovou, alebo vlastnou nadobudnutou skúsenosťou. 
  

Slováci sa tolerancii nemali kde učiť ak nemali to šťastie, že ich formovalo liberálne prostredie. Bigotné Slovensko, ktorého kostoly v nedeľu praskajú vo švíkoch nie je práve tým najlepším prostredím. V nedeľu sa pomodlíme, pár hodín budeme milovať bližného svojho okrem rómov, maďarov, čechov a homosexuálov a potom už nejako prečkáme do ďaľšej nedele, alebo hneď po omši sa s krucifixom poprechádzame po Bratislave a budeme šibrinkovať transparentom: Homosexuálne partnerstvá = Morálny rozklad spoločnosti.
Dalo by sa to napísať aj jednoduchšie ako to raz napísal Werich: 
Fanatismus je určitý nedostatek statečnosti. On je to strach přijímat svět v jeho složitosti. 

  
A bude ako nebolo, alebo suma sumárum tohoto textu: 
  
Je 21.storočie. Nie je podstatné koľko ľudí je dnes proti pochodu  gejov, alebo proti registrovaným partnerstvám na Slovensku, alebo proti adopcii homosexuálnym
párom. Nie je podstatné koľko slizkého fanatizmu, agresie a nenávisti budú ľudia ešte schopní zo seba vypľuvnúť. Nie je podstatné koľko primitívných politikov sa bude staviať k otázkam menšin s ironickým úškrnom na tvári. Progres človek môže spomaliť, ale nezastaví ho. Jedného slnečného dňa sa bude musieť aj Slovensko,  raz a navždy zbaviť svojich mindrákov homofóbneho burana, ktorý ešte v 21.storočí sedí v kadibudke a so strachom v očiach pozoruje cez vyrezáné srdiečko na dverách okolitý svet..., ktorý sa napriek katastrofickým prognozám stále točí.
  




PS: Pani doktorka, vy ma chápete, ale pre nechápavých to ešte raz zopakujem: Je 21.storočie. Have a gay day!:-)
  
 




