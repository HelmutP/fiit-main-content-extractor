

   
 To že sa voľby blížia je veľmi dobre vidieť na tom ako sa politické strany snažia prilákať voličov. Každá strana na to používa rozdielne prostriedky . Čo je však najviac do oči bijúce sú práve praktiky volebnej kampane Slovenskej Národnej Strany.  
  Nasadiť do predvolebnej kampane bilbordy zamerané Rómom je pred voľbami naozaj silná káva a pripadá mi to aj ako taký extrémistický ťah SNS. Nemyslím si to len ja ale väčšina ľudí, ktorých takéto konanie a kampaň SNS poburuje. Avšak je jedna pravda že od SNS a pána Slotu sa v poslednej dobe nič dobrého nedá čakať.  
  Čo môže byť hlavným motívom?? Žeby veľká nenávisť a výzva do boja ? Prieskumy verejnej mienky v poslednej dobe ukázali, že popularita SNS klesá a pohybuje sa okolo 5%. Žeby sa báli že nedosiahnú potrebnú hranicu na získanie kresiel v parlamente?? SNS už nevie ako získať voličov tak skúšajú všetko možné ako prilákať pozornosť ľudí aby ich volili. Žeby toto bol ten najsprávnejší spôsob??  
  SNS už naozaj narobila veľmi veľa zla a bolo by na čase aby sa do parlamentu ďalšie volebné obdobie nedostala. Ak sa verní voliči SNS doteraz nepresvedčili že treba voliť niekoho iného, dúfam že toto im otvorí oči.  

