
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Minárik ml.
                                        &gt;
                Politické
                     
                 Ad Miroslav Kocúr: Menšina, o ktorú nikto nestojí 

        
            
                                    19.6.2010
            o
            16:32
                        (upravené
                30.11.2010
                o
                9:05)
                        |
            Karma článku:
                9.17
            |
            Prečítané 
            2432-krát
                    
         
     
         
             

                 
                    Vo štvrtok uverejnil Miroslav Kocúr v SME komentár, ktorý stojí za pozornosť. Chvíľu som myslel, že bude písať o kresťanoch alebo o kresťanských demokratoch a ich situácii po voľbách. Môj odhad bol nesprávny - článok je o homosexuáloch. Miroslav Kocúr, (bývalý kňaz a - táto formulácia nie je technicky správna, upravené 27.6.) kritik katolíckej cirkvi, ktorému v liberálnom SME tak radi dávajú priestor, neprekvapil. Keby napísal rovnaký článok o kresťanoch, bol by bližšie k pravde.
                 

                 Na začiatok musím dať Kocúrovi za pravdu: "Politici citlivé civilizačné témy pred voľbami ignorovali. Donekonečna sa im to dariť nebude." Rovnako má pravdu, že sa tu presadzuje kultúra valcovania a tyrania väčšiny. No stačí sa pozrieť okolo seba a zistíme, že valcovanou menšinou v Európe nie sú homosexuáli a podobné skupiny, ako nám podsúva Kocúr, ale kresťania.   Neveríte? Krátky pohľad na históriu európskej politiky ukazuje, že je tomu tak. Kresťania postupne prehrávajú v európskych krajinách jeden súboj s liberálmi za druhým, či už ide o homosexuálne partnerstvá, alebo oveľa závažnejšie témy, ako sú potraty a eutanázia. Podobne na úrovni Európskej únie - stačí si len spomenúť, akým spôsobom "utláčaná menšina" odstránila z komisie (podľa Kocúra väčšinového) katolíka Rocca Buttiglioneho. Alebo že talianski kresťania musia zvesiť zo stien kríže.   Ako je na tom kresťanská "väčšina" na Slovensku? Keď Ján Figeľ spomenul zmluvu o výhrade vo svedomí, začali mnohí KDH pripomínať, že získala len 8 percent. Ak bol niekto v ostatných dňoch valcovaný, tak to bol práve kresťan Figeľ, a práve za to, že presadzoval záujmy slovenských katolíkov. Pán Kocúr, máte svoju agendu a je to vaša vec, skúste však zároveň neignorovať fakty.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (106)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Ako veľmi Fico skrivil rovnú daň?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Od Husáka do Fica: Koniec revolúcií na Slovensku?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Prečo nemôžem podporiť požiadavky slovenských učiteľov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Zaostávajúca Cirkev ako nádej pre dnešnú dobu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Minárik ml. 
                                        
                                            Pussy Riot a hranice slobody slova
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Minárik ml.
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Minárik ml.
            
         
        pavolminarik.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolský učiteľ v Prahe.  

  


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    168
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2657
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politické
                        
                     
                                     
                        
                            Ekonomické
                        
                     
                                     
                        
                            Iné
                        
                     
                                     
                        
                            O Amerike
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            G. Guareschi: Don Camillo
                                     
                                                                             
                                            G. Weigel: The Final Revolution
                                     
                                                                             
                                            P. Seewald: Světlo světa. Papež, církev a znamení doby
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vladimír Palko
                                     
                                                                             
                                            Zuzka Baranová
                                     
                                                                             
                                            Radovan Kazda
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Impulz
                                     
                                                                             
                                            Konzervatívny inštitút
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




