

 Počas fallas sa nespi, iba zabáva a pije. V každej ulici sú diskotéky, koncerty, stánky s pitivom, kolotoče a hlavne petardy, tie sa strieľajú nonstop. 
 Takže aj keby ste spať chceli,( čo sa nechce), hudba a buchot, ktorý otriasa celým okresom, vás vyženú do ulíc určite. 
 Deti majú v škole prázdniny, dospelý si berú dovolenky - nikto si fallas nenechá ujsť.  Dokonca aj ty najstarší, sú v uliciach a s hrdosťou obzerajú a fotografujú figuríny z každého uhla. 
 Je to predsa ich tradícia! 
 V skutočnosti sa tento sviatok koná na počesť svätého Jozefa , patróna tesárov, známy, ako deň otcov. Jeho počiatky sa vzťahujú do VIII storočia, kde sa Valencijsky tesári, koncom každej zimy pripravovali na deň svojho patróna, tak, že pálili odpad a staré haraburdy zo svojich dielni. Pálili hlavne drevene štruktúry na ktorých viseli lampase ktoré im osvetľovali dielne počas zimy , keď že ich po príchode jari a predlžovaním sa dní už nepotrebovali. 
 Niečo ako jarné upratovanie, kde pálenie symbolizuje odstránenie problémov a zla. 
 Lenže Valencijčania sú veľmi vynaliezavý a tesárske haraburdy čoskoro začali nadobúdať ľudskú podobu. Stačilo pár starých kúskov oblečenia, a úmyselne vyberaných doplnkov, aby figúra nadobudla satiricky a groteskný obsah a výsmešne takto upozornila na konfliktných susedov, alebo polemických starostov. 
 V súčasnosti,( ako už určite predpokladáte), sú figuríny zamerané hlavne na politickú kritiku a aktuálne dianie v satirickej scenérii. 
 Aj materiály sa počas histórie menili a vyvíjali, od tradičného dreva, cez praktickejší vosk, prechádzali, cez  sadro-karton čo povoľovalo vytvárať čoraz precíznejšie figuríny. 
 Dnes sa postavičky známe ako "ninots" vyrábajú z mäkkého korku,  pretože tento materiál umožňuje čoraz väčšie  a ľahšie formy. 
 Sú to priam umelecké diela, ktoré  prichádza obdivovať takmer milión turistov ročne .Iba v meste Valencia je vystevenych 385 figurín a ďalších 250 je rozmiestnených po celom okrese. A aj keď sa oficiálne  stavajú do ulíc  až 15-teho Marca, v poslednej dobe sa s ohľadom na rozmery sa začínajú stavať už niekoľko týždňov vopred (niektore uz 27 februára) a za pomoci žeriavu. 
 Neskutočné nákladná práca, ktorá už po štyroch dňoch zhorí v plameňoch.Uz 19-teho Marca je ohnivá noc kde nočné ohňostroje a pirotechnické kreácie  ohlasujú koniec slávnosti. Tento najúžasnejší ohňostroj rozsvieti oblohu Valencie a trva okolo 25 minút pričom sa spáli asi 2500=3500 kg  strelného prachu  najrôznejších farieb zvuku  a efektov. 
 Strelný prach a ohňostroje sú pre Valenciáncov veľmi obľúbené a nenechajú si ich ujsť pri žiadnej slávnosti, sú ale menej pochopiteľné pre turistov a návštevníkov. 
 Aby sme pochopili je veľmi dôležité byť v blízkosti miesta kde explodujú, pretože  to nieje iba otázka pohľadu, ale je nutné to cítiť a počuť. Ak sa necháme uniesť hlukom a buchotom podobný koncertu dosiahneme pocit, ktorý nás opantá a unesie, keď že za 5 až 7 minút dosiahne viac ako 120 decibelov. 
 Fallas rozhodne najlepšie definuje kultúru  a históriu Valencie aj jej obyvateľov  oheň, hudba a život v uliciach... 
 !Visca Valencia! 

