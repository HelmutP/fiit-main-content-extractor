

 Našla som jeho mail, a riskla to. 
 "Prosím Vás, neviete nič o jednej pani doktorke?... Nie ste náhodou jej rodina?" 
 Kým som písala, mala som ju znovu pred očami. 
 Úžasná žena. Pediatrička, ktorá liečila predovšetkým príjemným úsmevom, povzbudením, radosťou zo života. Prišla mi ako tá zázračná babička z rozprávky "Byl raz jednou jeden král". Láskavá a pokojná, múdra a skromná. 
 Naše deti milovala, a mne sa na nej rátalo, ako aj vlastnú profesiu berie s nadhľadom bez pocitu "ja spasím svet". Nevedela? Povedala mi, zistila... alebo poslala tam, kde vedeli. 
 Vyjsť z jej ordinácie znamenalo byť nakazený radosťou a pokojom. 
 A potom sme sa odsťahovali, a po čase prešli k inej lekárke, dochádzať sa bez auta s malými deťmi nedalo. Akurát sme vedeli ešte od nej, že končí s praxou pre vážne zdravotné problémy. 
 Telefón, ktorý nám dala na seba domov, po čase hlásil halali. 
 Nezostávalo nič iné, iba na ňu stále myslieť.  V dobrom. Čo by povedala naša pani doktorka... 
 Kým som dopremýšľala, prišla mi odpoveď. Krátka. 
 "Ste na správnej adrese, ale žiaľ, stretnutie už nie je možné. Mama už od marca 2007 nie je medzi nami. 
 ... 
 A tak sa hanbím. 
 Za to, že som si nedokázala nájsť čas, aby som ju vypátrala a potešila ju tým, že máme ešte ďalšie deti... veľmi nám fandila. 
 Hanbím sa, že existuje všeličo, čo má prednosť, veď len chvíľku, teraz nie, nechce sa mi, inokedy, či tá hlúpa myšlienka - či to nebude blbé. 
 Hanbím sa aj za to, že si nedokážem nájsť čas na tých, ktorí sú mi blízki. 
 Na nášho Jakuba, ktorý ma má chápať, že teraz pracujem... nech para tlačí robotu! 
 Na kmotru, ktorá v ťažkej situácii potrebuje pomoc, a mne sa zdalo byť lepšie rýchlo doma, ako prerušiť cestu a vytešiť sa spolu. 
 Na mnohých, na ktorých myslím - ako nám spolu bolo fajn, ale život nás trošku od seba oddelil ďalej. 
 Musím sa naučiť mať čas. 
 A prepáčte, pani doktorka, že som si to uvedomila až teraz... 

