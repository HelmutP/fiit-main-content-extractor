

 Raz by som chcela muža, akým je on, 
 normálne, dopriať svojim deťom takého tata, 
 obyčajného chlapa, 
 aby dal občas zaucho, ak je za čo, 
 aby zvýšil hlas, keď je treba, 
 aby si kľakol a uprel oči dohora, 
 takého chlapa,.. 
 Zobral by decká do lesa 
 a nekupoval by im všetko, 
 ale naučil by ich vyrobiť píšťalku z vŕby 
 alebo by vyrezal dcérke posteľ s baldachýnom pre barbinu, 
 taký chlap, viete, normálny, čo by vedel na čo má ruky, 
 ale vedel by používať aj srdce 
 a používal by ho často.. 
 aj keby to občas nebolo vidno. 
 Taký chlap, čo by v noci skontroloval či sú decká doma 
 a cez búrku by pozavieral všetky okná. 
 A naučil by ich, že si všetko treba zaslúžiť.. 
 Taký normálny chlap ako je môj tatko. 
 Dnes sme išli spolu na bicykloch a dolu kopcom ma držal za tričko, 
 bál sa, že mi nefungujú brzdy. 
 Takého chcem, čo by dal život za svoje. 
 Nie za svoje auto, za svoje peniaze, ale za svoje deti! 
 Dík, tati. 
   

