

   
  
  
  
  
 Spolok ctiteľov Slovenskej republiky, jej štátnych symbolov a jej prezidenta. 
  
 



 
  
 Teraz, krátko pred voľbami, vynoril sa v parlamente SR „Vlastenecký zákon,“ ktorý mal nanútiť občanom SR  lásku k svojej vlasti, aj pod hrozbou trestu  za neuposlúchnutie tohto zákona. 
 Je len samozrejme, že tento zákon nevyriešil by to, čo malo byť  jeho poslaním, lebo násilím sa láska k vlasti nedá vynútiť. 
 Túto skutočnosť pochopil aj pán prezident Slovenskej Republiky, rodu verný vlastenec, Ivan Gašparovič a preto on tento zákon nepodpísal. 
 Tento akt pána prezidenta otvoril cestu k úprimnému uctievaniu štátnych symbolov aj bez zákona a to zo slobodnej vôle vlastenecky orientovaných občanov Slovenskej republiky. 
 V snahe dať priestor vlastenecky orientovaných občanov Slovenskej republiky, bez rozdielu politickej príslušnosti, vierovyznania a rasy, vyjadriť sa slobodne a demokraticky k štátnym symbolom Slovenskej republiky, ako aj prezentovať ich lásku k vlasti, zriaďujem túto stránku na adrese jaray.blog.sme sk pod názvom: 
 Spolok slobodných ctiteľov Slovenskej republiky, jej štátnych symbolov ako aj jej prezidenta. 
  
 Preambula: 
 Vlastenecky orientovaní občania SR, združení v rôznych politických stranách, nikdy nie sú schopní zjednotiť sa na akceptovaní ctihodných, ešte žijúcich osobnostiach Slovenskej republiky, čím sa Slovenská republika ochudobňuje ako na úrovni spoločenskej, tak aj na úrovni vlasteneckej. 
 Skutočná, ctihodná osobnosť, sa tak na Slovensku nemá možnosť zrodiť, lebo ak nepatrí k žiadnej politickej orientácii, tak o nej nikto nevie, nikoho nezaujíma a keď sa ona zaradí do niektorej politickej strany, tak tým pádom, razom stratí svoju ctihodnosť. 
 Pokiaľ si my, slobodní občania SR, sami "nevychováme" ctihodnú osobnosť,  tak sa žiadna osobnosť,  sa na Slovensku nikdy sama nezrodí!  
 Mojím kandidátom na post ctihodnej osobnosti SR, je prezident SR,  
 Ivan Gašparovič  
 a to preto, že odmietnutím  zákona na povinné milovanie vlasti, prejavil vysokú štátnickú múdrosť, ako aj úprimnú lásku k Slovenskej republike a jej občanom, ale aj k Ústave SR a jej demokratickým princípm. 
 (Ale aj preto, že jeho vysoká štátna funkcia, v súčasnosti ako jediná, nie je závislá na žiadnej politickej strane.) 
 To ale neznamená že s mojím kandidátom musí každý súhlasiť, ba čo viac, každý iný kandidát na ctihodnú osobnosť Slovenskej republiky, bude na tejto stránke vrelo privítaný. 
 Ľud Slovenský, neparazituj iba na pozitívnych dejinách tvojích predkov, tvor si sám a slobodne, pozitívne dejiny Slovenskej republiky, tu a teraz!  
 



 
 
 
 Štátne symboly Slovenskej republiky 
  
 Štátny znak 
 Symbolom Slovenska sa stal prvý raz v revolučných rokoch 1848 - 1849. Trojvršie symbolizuje tri karpatské pohoria Tatry, Fatru a Mátru (v súčasnosti leží v Maďarsku). Dvojkrížom sa Slovensko hlási ku kresťanstvu a zároveň k tradícii sv. Cyrila a Metoda, ktorí sem v období Veľkej Moravy (9. storočie) priniesli kresťanstvo. 
 
 


 
                                                             
  
   
 Štátna vlajka 
 Oficiálne bola zavedená 1. januára 1993, ale pochádza z roku 1848.  Biely, modrý a červený vodorovný pruh nachádzame aj na ruskej alebo slovinskej zástave. Sú známe ako panslovanské farby. Štátna vlajka sa vztyčuje na vlajkovom stožiari. Štátna zástava je odvodená od štátnej vlajky. Je vždy pevne spojená so žrďou alebo priečnym rahnom. 
  
   
  
 Štátna pečať 
 Štátna pečať sa používa na originál listiny ústavy a ústavných zákonov Slovenskej republiky, medzinárodných zmlúv, poverovacích listín diplomatických zástupcov a v ďalších prípadoch, v ktorých je jej použitie obvyklé. Priemer pečate je 45 mm. Pečatidlo uschováva prezident SR. 
 
 


 
                                                                     
  
   
   
 Slovenská hymna 
 Štátnu hymnu tvoria prvé dve slohy piesne Janka Matúšku "Nad Tatrou sa blýska" z roku 1844. Text napísal pri protestnom odchode štúrovcov z Bratislavy. 
  
  
 
 


 
                                                                
  
   



 
 Nad Tatrou sa blýska, hromy divo bijú, Zastavme ich, bratia, veď sa ony stratia, Slováci ožijú.  To Slovensko naše posiaľ tvrdo spalo, ale blesky hromu vzbudzujú ho k tomu, aby sa prebralo. 
  

  
  
 
 
   

