
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alojz Hlina
                                        &gt;
                Nezaradené
                     
                 Slovák na koni v Budapešti 

        
            
                                    26.3.2010
            o
            10:28
                        (upravené
                26.3.2010
                o
                12:31)
                        |
            Karma článku:
                13.21
            |
            Prečítané 
            3088-krát
                    
         
     
         
             

                 
                    4.júna 2010 bude 90-te výročie podpisu Trianonskej zmluvy. V ten deň budú v Budapešti viaceré akcie, ktoré budú na túto historickú okolnosť upozorňovať. A zdá sa, že nielen v Budapešti. Je vysoký predpoklad, že sa vyskytnú aj nejaké provokácie, ktoré budú u nás doma vnímané  - diplomaticky povedané - citlivo.
                 

                     Kto na ne najviac čaká? Samozrejme Slotovci.   Dovolím si tvrdiť, že Slotovci nemusia robiť žiadnu kampaň, míňať peniaze. Pokojne môžu pokračovať v nastúpenom trende „efektívneho spravovania štátnych peňazí" a čakať na 4. Jún. Po 4.júne to naplno rozbalia a povedia, aká je pravda. Povedia nám, ktorí tomu nerozumieme, aká je pravda.   Chlapi od nás - z Oravy chodili do Budapešti v dávnych časoch buď stavať parlament, alebo sa učiť za farára. Boli časy, keď aj môj, ako hovorím, sused - P.O. Hviezdoslav rozmýšľal, a nakoniec napísal básničku Dedinôčka je to. Som rád, že  ja môžem ísť do Budapešti ako hrdý občan svojej krajiny - Slovenska. Krajiny, ktorú mám rád.   Prídem tam na koni, ktorého budem chcieť odovzdať maďarským predstaviteľom ako symbolický dar. Našu vzájomnú históriu zaťažujú viaceré historické legendy, ktoré pôsobia citlivo. Slováci nikdy nepredali svoju krajinu za bieleho koňa. A preto, ak by mal byť kôň problémom, tak ho prídem do Budapešti symbolicky nie vrátiť, ale darovať.   Neviem, aké reakcie to vyvolá, ale viem, že keď 4.jún necháme v rukách radikálov na jednej strane aj na druhej strane Dunaja, tak pokoj nebude.   J.F. Kennedyho sa pýtali, či vie, koľko vojakov do Vietnamu šlo, a koľko z nich sa vrátilo. Odpovedal, že nevie, ale že určite vie, že sa vrátilo menej vojakov, ako tam šlo. Ja rovnako neviem presne, aké sú problémy na slovenskom juhu, a už vôbec neviem, aké sú problémy v Maďarsku. Ale viem, že pokiaľ históriu našich národov zveríme do rúk radikálov, tak tie problémy budú určite väčšie, a nie menšie, ako sľubujú.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (85)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Čo sa Penta v Číne nenaučila
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Zrušiť zdravotné odvody je nebotyčná hlúposť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Nočné rokovanie bude aj pred Paškovou vilou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            Smer zlegalizoval nezákonné billboardy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alojz Hlina 
                                        
                                            O slovenskej žurnalistike rozhodne Shooty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alojz Hlina
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alojz Hlina
            
         
        alojzhlina.blog.sme.sk (rss)
         
                                     
     
        Alojz Hlina - nezávislý poslanec NR SR, občan Slovenska, občan mesta Bratislava, oravský bača. www.alojzhlina.sk
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    518
                
                
                    Celková karma
                    
                                                8.94
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak kvôli tomu vyhodili Čaploviča?
                     
                                                         
                       Politický nekorektné úvahy k Dňu Európy a Dňu víťazstva
                     
                                                         
                       Hanebná nominácia na ústavného sudcu
                     
                                                         
                       Stane sa  SMER chrapúňom  roka?
                     
                                                         
                       Košičania, budete svietiť ako baterky?
                     
                                                         
                       Simpsonovci a Hlinovo hrdinstvo
                     
                                                         
                       Ako udržiavajú P.Paška a R.Kaliňák krehkú rovnováhu?
                     
                                                         
                       Môžete ísť do kostola, ale omša nebude
                     
                                                         
                       Keď SMER zabezpečuje koledy
                     
                                                         
                       Študenti, ako chutila držková u premiéra?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




