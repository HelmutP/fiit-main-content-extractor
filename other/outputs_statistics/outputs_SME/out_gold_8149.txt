

   
 AMFO je najstaršia a najväčšia celoštátna súťaž tvorby amatérskych fotografov na Slovensku. Jej vyhlasovateľom je Národné osvetové centrum (NOC) so sídlom v Piešťanoch, príspevková organizácia Ministerstva kultúry SR. Odborným garantom súťaže je kabinet amatérskej umeleckej tvorby NOC. Celoštátnemu kolu AMFO predchádzajú súťažné kolá na regionálnych úrovniach na celom území SR. Krajské kolo súťaže amatérskej fotografie AMFO 2010 v Košickom samosprávnom kraji (KSK) zorganizovalo Krajské osvetové stredisko v Košiciach, kultúrne zariadenie KSK. Vernisáž výstavy spojená s vyhlásením výsledkov súťaže a s odovzdávaním cien sa uskutočnila 04. júna 2010 v priestoroch Slovenského technického múzea v Košiciach. 
   
 Do súťaže sa zapojilo 93 autorov z Košíc (50), Košíc a okolia (7), Trebišova (1), Michaloviec (4), Spišskej Novej Vsi (21) a z Rožňavy (10). Do súťaže bolo zaslaných spolu 405 súťažných fotografií a 12 multimediálnych prezentácií. Súťažiaci boli rozdelení do troch vekových skupín (do 16 rokov, od 16 do 21 rokov, nad 21 rokov), v rámci ktorých súťažili v troch kategóriách (čiernobiela fotografia, farebná fotografia a multimediálna prezentácia). Samostatnú tematickú kategóriu vyhlásilo NOC podľa pokynov MK SR a MPSVaR SR v rámci Európskeho roka boja proti chudobe a sociálnemu vylúčeniu 2010. Odborná porota v zložení Ing. Miroslav Pfliegel, CSc., Miro Černý a Mgr. art Ján Polák ocenila spolu 15 autorov a na výstavu navrhla vystaviť 141 prác od 76 autorov. 
  
 Štefan Lipták - Pohľad naspäť 
 Vystavené práce sa niesli v duchu rôznych tém. Od zachytenia hmyzu, cez krajinky, street fotografie, portréty a umelecké akty až po abstraktné zobrazenia. Aj na tejto košickej výstave podobne ako na minuloročnej som objavila práce, ktoré sa mi veľmi páčili. Boli to umelecké akty s názvom Kúpeľ I a Pohľad naspäť fotografa Štefana Liptáka, člena Združenia košického klubu fotografov K-91. Upozornil ma na ne jeden návštevník, ktorý ma k nim doslova pritiahol za ruku. Asi u neho zafungoval povestný šiesty zmysel, keďže podobná tvorbu mám naozaj rada. 
 Zaujala ma tiež víťazná fotografia Karola Stollmana, tiež člena K-91, Hráme iba Jazz v kategórii čiernobiela fotografia v skupine nad 21 rokov, ktorá verne zachytila osobitú pouličnú atmosféru Košíc. Hlavnú cenu v skupine autorov nad 21 rokov získal Milan Filičko z Košíc s fotografiami Levoča a Ružomberok. Jednu z víťazných fotografií si môžete pozrieť tu. 
  
 Príjemne ma prekvapila tvorba len 14-ročnej Soni Smolárikovej, ktorá s fotografiami Posledný jednorožec... či? a Mi alma získala cenu za čiernobielu fotografiu v skupine do 16 rokov. 
  
 Markéta Tatjaková - Granada 
 Hlavnú cenu v skupine autorov do 21 rokov získala Markéta Tatjaková s fotografiami Granada a Svetlo a tieň. Obe dievčatá sú členkami fotografického krúžku pri Centre voľného času - Regionálnom centre mládeže v Košiciach. 
  
 Markéta a Soňa na vernisáži 
 Najviac ma zaujala kolekcia vystavených fotografií Moniky Rusnákovej zo Spišskej Novej Vsi, za ktoré získala cenu v samostatnej tematickej kategórii Európskeho roka boja proti chudobe a sociálnemu vylúčeniu 2010 a fotografie Vladimíra Mulika z Košíc Bez názvu I, II (Vladimír Mulik taktiež získal cenu v uvedenej samostatnej kategórii, s fotografiou ...a mám toho dosť...). 
  
 Fotografie Moniky Rusnákovej 
  
 Vladimír Mulik - Bez názvu I, II 
 Vystavené práce si môžete pozrieť od 07. júna do 30. júna v priestoroch Slovenského technického múzea na Hlavnej ulici 88 v Košiciach, v pracovných dňoch od 08:00-17:00 hod., cez víkend od 12:00-17:00 hod. Všetky práce postupujú do celoštátneho kola súťaže AMFO 2010, ktorej vernisáž a vyhlásenie výsledkov sa uskutoční 5. novembra 2010 v Múzeu SNP v Banskej Bystrici. 
   
   
   
   

