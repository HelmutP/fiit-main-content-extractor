
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Beata Bírová
                                        &gt;
                Z medicínskej pôdy
                     
                 Dva roky v živote medika 

        
            
                                    28.6.2005
            o
            13:20
                        |
            Karma článku:
                12.63
            |
            Prečítané 
            4539-krát
                    
         
     
         
             

                 
                    Včera sa konečne pre mňa začali prádzniny po dvoch rokoch driny. A tak ako som večer zaľahla do postele, začala som sumarizivať udalosti uplynulých mesiacov...
                 

                Nemožem povedať, že by som mala kľudný spánok, lebo som stále nemohla uveriť, že poslednú skúšku, s ktorou som sa trápila dva roky som konečne urobila. Stále mi v hlave vírilo učivo, rozmýšľala som čo ešte neviem , mozog nie a nie vypnúť a stále ma vyrušoval tým , že mi pripomínal celé tie litánie, ktoré som sa drvila zo 7 kníh. Mala som pocit, že to nie je noc po skúške, ale noc pred skúškou. Možem povedať, že včera som tak trochu prekonala seba samú. Toto štúdium nie je prechádzka ružovou záhradou, ale skor obrovským pralesom, v ktorom som sa stratila. Neviem, či tento článok niekoho zaujme, ale možno keď dostanem zo seba to , čo som dusila dlhé mesiace, možno mi bude lepšie a ja konečne pocítim radosť z toho, že som sa dostala ďalej. Pretože teraz som celou tou školou len znechutená . Nemožem sa radovať. Včera ako som vyšla od examinátora som už bola polomŕtva a ľudia na chodbe si mysleli, že ma vyhodil, lebo som sa tvárila ako kopa nešťastia. Ono sa to všetko začalo tým ako som nastúpila na túto  fakultu. Nejaké tie skúšky vyšli, iné vyšli na druhý, tretí pokus a človek mal pocit, že skúškové trvá celých tých desať mesiacov výuky. A tak keď začalo letné skúškové obdobie, tak už ani moc síl nezbývalo. Kým ostatní buď to brigádovali, alebo ležali celé dni niekde u vody , ja som trávila celé prázdniny nad knihami. Moj denný režim vyzeral asi tak , že skoro ráno budíček a spať sa išlo pozde, žiadna telka, žiadna voda. Ale človek si povie, že veď to má určite zmysel, urobím skúšku a potom mi ešte ostanú dva týždne do začiatku nového šk. roka. No len nie, vždy to vyjde a ak už idete na tretí pokus v septembri, tak vám to je v podstate už jedno, lebo ak to beriete poctivo, viete že je to len o šťastí a nálade examinátora. Tak som sa tam teda v septembri vybrala , no a čo čert nechcel dostala som sa k osobe, ktorá už mala zakázané skúšať kvoli prístupu hlavne k ženskej časti populácie. Takže to dopadlo zle a pre mňa sa načas zrútil svet. S nikym som nechcela hovoriť o tom , čo sa tam stalo, izolovala som sa od rodiny a známych a v meste som sa bála, že stretnem niekoho, kto sa ma bude pýtať na školu a ja budem musieť povedať pravdu. Nejako to prebolelo, aj keď moje sebavedomie bolo hlboko dole. Ale povolili mi po dlhých peripetiach opakovať a ja som začala pekne od začiaku. Opeť prišli pocity, ktoré boli spojené so stretávaním spolužiakov, ktorý boli už vo vyššom ročníku. Hanbila som sa ako pes a občas sa tvárila, že ich nevidím. A noví spolužiaci ? Tí sa od repetentov stránili a doteraz sa s večšinou ani nezdravím. Ale nejako moc im to nezazlievam, pretože aj ja som sa minulý rok na repetentov pozerala tak, že buď sú buď blbí, alebo flákali. Teraz to už všetko samozrejme vidím inak. To nie je ako na základnej škole, že sa opakuje kvoli tomu, že niekto školu fláka. Ale stále mi je nepríjemné hovoriť ľuďom, že opakujem. A niektorí doma o tom ani nevedia a myslia si , ktovie aká šikovná ja tá Beatka.... Tak som si to teda pretrpela, učila sa to čo celý rok predtým, občas nejaká tá brigáda a tak. Som si zaumienila, že si teda aspoň najdem priateľa, keď už nič iné. Ale ako to býva, keď si niečo človek zaumieni, tak to večšinou nevyjde. Prišiel deň D, tešila som sa , že to už konečne budem mať za sebou a začnú prázdniny. Ale sa stalo to, čo nik neočakával, teda hlavne ja nie, že examinátor bol zrejme už znudený a tak sa do mňa zavŕtal a chcel odo mńa veci, ktoré akosi nikde neboli. Takže po dvoch rokoch som bola opeť na začiatku. Nechcela som už viac pokračovať. To čo som si musela vytrpieť nikomu neprajem. Viem, že sa stávajú aj horšie veci, škola nie je všetko, ale predsa to človeka položí na lopatky a už nemá chuť sa postaviť a bojovať ďalej. Načo ? Veď to nemá zmysel a aj tá štipka sebavedomia čo mal , sa vyparila.  Bolo to dosť nepríjemné, pretože to dá zabrať aj psychike. Dala som si pauzu. Týždeň som len spala a naberala sily a prihlásila sa na ďalší termín. Ale robila som to viac menej z povinnosti a nie kvoli tomu , že som to  chcela. A ten odpor s akým som každý deň otvárala knihy a čítala si to stále znova a znova. Už som viac nemohla a mala som pocit, že ak to tentokrát nedopadne dobre, tak skončím niekde u psychiatra. Večer mi ešte volali naši, čo ma dosť rozľútostilo, ale povedali mi ,že stoja pri mne nech sa stane čo sa stane. A to ma ukľudnilo. Ráno som sice musela prekonávať obrovskú únavu a nevoľnosť celou cestou, ale som si hovorila, že som sa neučila predsa toľko kvoli tomu, aby som tam niekde odpadla. Ale tá únava bola príšerná. Na príprave som musela mať zatvorené oči a podopierať si hlavu. Tentokrát som sa dostala k niekomu kto má aj ľudský prístup. Po troch hodinách som odtiaľ vyšla ako po ťažkom porode. Smutná a vyčerpaná. Ale to čo mi povedal examinátor pred všetkými mi asi bude ešte dlho znieť v ušiach : " Vy toho víte strašne moc. Opradvdu víc jak Ste tady řekla a víc jak si myslíte. Len působíte moc vystřašeně, tak kousek života do toho, sakra vždyť Ste nejaká ženská !"....po tom ako som si myslela, že som fakt úplne tupá boli tieto slová ako balzam na moju boľavú dušičku. A tak sa nikdy nevzdávajte v tom čo chcete robiť. Síce Vás to možno odradí ako mňa, ale na druhej strane budete už tak obrúsení, že Vás už čosi len tak  nerozhádže.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (22)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beata Bírová 
                                        
                                            Išli ovce na vandrovku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beata Bírová 
                                        
                                            Írsko mojim pohľadom - Dingle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beata Bírová 
                                        
                                            Pár životných právd
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beata Bírová 
                                        
                                            Írsko mojim pohľadom ( časť druhá )
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beata Bírová 
                                        
                                            Írsko mojim pohľadom (časť prvá)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Beata Bírová
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Beata Bírová
            
         
        birova.blog.sme.sk (rss)
         
                                     
     
        Každý má svoj vlastný príbeh.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    46
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1796
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Ja a hudba
                        
                     
                                     
                        
                            Z medicínskej pôdy
                        
                     
                                     
                        
                            Srdcové záležitosti
                        
                     
                                     
                        
                            Zamyslime sa ..
                        
                     
                                     
                        
                            Ireland
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            škotska fauna
                                     
                                                                             
                                            amor es puta
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




