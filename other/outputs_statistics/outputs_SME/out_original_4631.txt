
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Adrián Lachata
                                        &gt;
                Próza
                     
                 A možno iba ja by som plakal, keby som to takého školského systému 

        
            
                                    15.4.2010
            o
            16:11
                        (upravené
                16.4.2010
                o
                10:06)
                        |
            Karma článku:
                6.16
            |
            Prečítané 
            852-krát
                    
         
     
         
             

                 
                    Včera som sa spýtal priateľa z gymnázia, že ako sa má a čo robí. Klasika. Po jeho odpovedi mi spadla sánka. „Snažím sa z netu učiť goniometrické funkcie.“ Chvíľu mi trvalo, kým som bol schopný sánku znova zdvihnúť. Potom som sa dozvedel, že nemajú učebnice a z učiteľových poznámok v zošite sa veľmi nedá vyjsť.
                 

                 Že minister spravodlivosti klame, nevie sa vyjadrovať a na verejnosti tancuje Kalinku, som po dňoch nejak zabudol. Slotu si ani nevšímam, i keď ma udivuje, že si beztrestne môže porušovať nie len zákon, ale aj demokratickú ústavu a princípy ľudskosti. Nerozumiem, ako môže také niečo akceptovať národ, ktorý si privlastňuje prívlastok demokratický.   Opozičný poslanci, nie sú na tom o nič lepšie. Myslia si, že sa chovajú čestne a slušne. No okrem toho, že sú poslanci, už aj občania. Sadne si slušný a čestný občan do miestnosti napríklad  s vyššie vymenovanými indivíduami?. A keďže sú aj poslanci, sú najbližšie k tomu, aby proti tomu mohli niečo spraviť. Aby povedali nie, a nie iba si zakrývali oči. A nie je nový problém!  Tí ľudia sú v politike roky, no nie a nie odísť. Správny politik by okrem iného mal vedieť, kedy je čas odísť... A ak napríklad taký Slota neporušuje ústavu SR, tak sa vzdávam svojho rozumu.   Ináč ma aj dostalo, že verejný protest proti spievaniu slovenskej hymny na školách prišli podporiť iba poslanci maďarskej strany. Hm, "najmudrejšie strany (napr. SDKU-DS, SaS) " asi nezaujíma, že ľudia protestujú...   Ale nie o tom som chcel písať, teraz, keď nám najviac práce, že som takmer 3 mesiace nebol ani doma. Po odpovedi spomínaného priateľa som sa začal pýtať ostatných študentov gymnázií, a naozaj, nemajú učebnice. Jedna kamarátka mi povedala, že im aspoň učiteľka píše poznámky vo Worde. Čo je obdivuhodný prínos učiteľa  ale určite nie povinnosť. Potom som sa dozvedel niečo o reforme a mal som pocit akoby Slovensko chcelo vychovávať ľudí, ktorí budú robiť to málo (veľmi málo), čo sa naučia a najlepšie, ak nebudú pri tom vôbec uvažovať.  Aktuálna situácia je taká, že na gymnáziách je o tretinu menej učiva a pribudli predmety ako kultúra a umenie (čo je super, ale sú podceňované a nič sa na nich nerobí). Na matematike sa vyhodili rovnice/nerovnice s parametrom, ale limity a integrály ostali. Pričom rovnice/nerovnice s parametrom sú elementárna vec (poniektorí sa to učili už na základnej škole) a ich pochopenie je celkom pekné myšlienkové i matematické cvičenie (ale v matematike na školách už asi dlho chýba pochopenie. Ľudia sa naspamäť naučia algoritmus, ale nemajú šajnu prečo to je tak. A keď som to pár ľudom ukázal prečo, tak trojkári odrazu boli najlepších matematikári v triede. Nedávno sa mi jeden študent zdôveril, že ak nad to sadne a snaží sa to pochopiť, tak nielen, že to ide, ale aj ho to začína baviť...) Rovnice/nerovnice s parametrom sa dajú úplne pochopiť bez problémov. Ale v stredoškolskej matematike ostali ostali limity a integrály, čo úplne pochopiť na strednej škole je takmer nemožné. Trúfam si odhanúť, že definíciu limity postupnosti nenapíše viac ako 5% gymnazistov (to ešte nerátam pochopenie tej definície, na čo sa ma pýtajú väčšinou  budúci matfyzáci).   Keď som sa čistého jednotkára kvôli jeho veľkým medzerám opýtal maturanta na bilingválnom gymnáziu, či pre každé reálne čísla A,B platí, že A x B je to isté ako B x A, tak aj keď nevedel nájsť protipríklad, myslel si, že komutatívnosť násobenia (tak sa odborne volá ten zákon v matematike) neplatí. Ten čistý jednotkár je ináč naozaj inteligentný človek. Komutatívnosť násobenia je úplne základná vec. Je jeho chyba, že to nevie?   Teda čo vlastne chceme aby študenti vedeli? Potom o čom je jednotka z nejakého predmetu?  A tak som sa začal trochu zamýšľať. Z gymnázia som preč štyri roky a asi som bol predposledný ročník, keď to bola ešte výberová škola. Do prvého ročníka nás nastúpilo 90 z toho 70 s priemerom 1,00. Keď už bol blbý učiteľ, vždy sme mali učebnice, z ktorých sme sa mohli učiť a vedieť.   Nova reforma vyhodila 30% učiva. Ale nemyslím si, že ho bolo nejak veľa. Takmer jediný problém bol literatúra, kde bolo nutné memorovať kvantá učiva, ktoré človek zabudol hneď po maturite. Ako pamätám si, o čom bol starovek, stredovek, humanizmus, realizmus, klasicizmu atď. aj viem niečo o Štúrovoch a Učenom Slovenskom Tovarišti. Ale to akurát tie základne kostry, ale 95% čo som musel na literatúru vedieť, som zabudol. Áno, pochopím nadčasové diela klasikov, hneď po ruke to mám Candide, Dekameron, Karla Čapka, ale bolo aj toľko úplne dobových kníh, že ak človek nežil v tej dobe, tak vôbec nechápe aké problémy sa tam rozoberajú... A keď bolo toľko memorovania na literatúre, priestor na nejaké chápanie, kreativitu takmer ani nebol. Najdôležitejšie bolo memorovať (do maturity). Ale ináč, asi keby sa literatúra (čo je sama o sebe krásna) učila rozumne, tak by som nepovedal, že gymnázium bolo ťažké.   Vyzerá to tak, že gymnázium už viac nie je výberová škola, pre budúcich intelektuálov. Myslím, že si je treba  uvedomiť, že síce áno, sme všetci rovnakí, čo sa týka práva na život, na šťastie, lekársku starostlivosť. Ale sme aj iní. A teda robiť taký kompromis, že rovnaké vzdelanie pre všetkých asi nebude najlepšie riešenie, lebo trpia všetci. Trpia učitelia, lebo sú veľmi rôzni študenti. Trpia študenti intelektuáli, ktorí túžia po vedomostiach a poznaní, ale v škole robia niečo nezmyselné alebo sa nudia preto, aby to aj zvládli študenti, ktorí nie sú intelektuáli. A tí ktorí nie sú, tak sa tiež trpia, lebo je to pre nich náročné a lepšie by im bolo niekde na nejakej odbornej škole. Nakoniec to končí tak, že všetci robia nezmyselné veci a nikto nie je skutočne užitočný a ani spokojný. Pri čom by mohli aj všetci...  Ale keby aspoň boli učebnice, aby sa mohli tí, ktorí chcú vedieť, mohli učiť... Ale učiť sa na internete goniometrické rovnice, ani neviem ako napísať, aké to je. Asi iba na plač. Veď si ničíme vlastnú budúcnosť... :(((((   P.S.: Najlepšie na tom je, že toto média nezaujíma. A možno ani nikoho. A možno iba ja by som plakal, keby som to takého školského systému musel dať svoje deti... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Otvorený podrav prezidentovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Odkaz pre mladých študentov a školákov (Khan Academy)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Za pád vlády môže strana SMER, a to jednoznačne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Známkovanie telesnej výchovy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Adrián Lachata 
                                        
                                            Notebook pre študenta
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Adrián Lachata
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Adrián Lachata
            
         
        lachata.blog.sme.sk (rss)
         
                                     
     
        Uvažujúci individualista, symbiotický schizofrenik.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1183
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Programovanie
                        
                     
                                     
                        
                            Zážitky
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Výpisky
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Antropoložka na Marsu
                                     
                                                                             
                                            Karla Čapka
                                     
                                                                             
                                            Dekameron
                                     
                                                                             
                                            Candide
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janette Maziniová
                                     
                                                                             
                                            Dávid Králik
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mal iba pätnásť
                     
                                                         
                       Prečo ešte Harabin nie je v červenej knihe?
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Matka spí s mužom svojej dcéry, to je ponuka JOJ pre siedmakov
                     
                                                         
                       O stratenom a premárnenom čase
                     
                                                         
                       O pravdu asi všeobecne nie je záujem
                     
                                                         
                       Dôležitý nie je žiak, ale byrokracia!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




