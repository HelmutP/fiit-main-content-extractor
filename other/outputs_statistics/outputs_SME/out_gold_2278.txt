

 Súčasná spoločnosť rieši jeden problém, ktorý je vo svojej podstate paradoxom.  Na jednej strane tu máme súhrn vecí, ktoré boli ešte v nedávnej minulosti úplne nepredstaviteľné. Primárne sa chcem zamerať na možnosti komunikácie. Človek v tejto oblasti takmer nepozná hranice. Skype, icq, msm, sociálne siete ako myspace a monentálne asi najväčší boom = Facebook. Človek, aj menej rečnícky zdatný a hanblivý, má možnosti spoznať nových ľudí. Taktiež ľahko zoženie kontakt na "starých známych". Môže byť s nimi, aj keď s nimi nie je. Vie o nich všetko, aj keď sa nepýta.  Človek sa prostredníctvom sociálnych sietí stáva súčasťou veľkej komunity, ktorá pozostáva z ľudi, ktorí sa považujú za priateľov. Častokrát ale ide o veľký klam...pretože nie je priateľ ako priateľ. 
 Prečo ale píšem tento článok. Niekto by povedal, že mám snahu kritizovať niečo, čo je vo svojej podstate naozaj dobrá vec.  Moja kritika ale smeruje inde a chcel by som na príkladoch poukázať na skutočnosť, že ľudia naozaj strácajú záujem medzi sebou komunikovať naživo. A to je druhá stránka problému -  spontánny rozhovor , ktorý boli v minulosti bežný, sa dnes stáva nevídaným a dokonca aj zvláštnym až otravným. Citlivo tento problém vnímam najmä na Slovensku. 
 Príklady: 
   
 
 Stretnutie na ulici, námestí, parku, zastávke... 
 
 Už si ani neviem spomenúť, kedy ma naposledy niekto oslovil na ulici. Kedy niekto navodil nezáväznú (samozrejme slušnú) konverzáciu. (nepočítam členov rôznych siekt) Čo je však horšie, ľudia k sebe ťažko spontánne hľadajú cestu aj v prípade, že sú účastníkmi nejakého podujatia. Kto sa začne rozprávať na športových podujatiach, kultúrnych podujatiach? Pritom napr. o Superstar sú ľudia schopní diskutovať na internete celé dni...  Nedávno som bol na koncerte kapely Europe v Trnave. Všimol som si, že účastníkmi koncertu sú aj ľudia staršej generácie. Po skončenií koncertu som vo chvíľke pocitu šťastia z vynikajúceho kultúrneho zážitku oslovil starší manželský pár. Ich reakcia bola rozpačitá. Ťažko mi odpovedali na jednoduchú otázku: chcel som vedieť ako sa im páčil koncert....  ... pre ľudí je taká obyčajná vec, ako spontánne začatá komunikácia v reálnom živote niečím nečakaným, niečím čo je tu naozaj zvláštne. ľudia si odvykli komunikovať, pokiaľ nemusia. 
   
 
 
 Pozdravenie... 
 
 Od malička do nás rodičia doma  a učitelia v škole vštepujú, aby sme zdravili. Najmä starších ľudí, je to prejavom úcty.  Koľkokrát sa vám stane, že kráčate chodníkom, na ktorom sa zoči-voči míňate s človekom, ktorý taktiež kráča sám. Pozdravíte sa? Dovolím si tvrdiť, že keby robím štatistiku, tak z 20 prípadov by pozdrav zaznel maximálne 2krát. Namiesto toho sme svedkami do zeme zahľadených a zachmúrených tvárí a očí, ktoré robia všetko preto aby vás nebodaj nezbadali. 
 
   
 
 
 Virtuálne vzťahy versus vzťahy reálne... 
 
 Už vyššie spomenuté sociálne siete majú výhody, ale aj nevýhody. Témou blogu nie je ich porovnávať. Chcel by som len poukázať na to, že množstvo novo nadobudnutých vzťahov sú vzťahy nereálne. Vzťahy podobné virtuálnej realite, v ktorej je možné všetko.  Cez internet som spoznal množstvo ľudí. No najviac zo všetkých si cením tých, s ktorými som dokázal vzťah virtuálny preniesť do reality. To v praxi znamená, že s dotyčným človekom som sa aspoň raz stretol (hej niekedy je to ťažké, pretože virtuálny priateľ nemusí byť zo Slovenska). V princípe nejde však ani tak o tú potrebu fyzického stretnutia, ale o znak toho, že ten vzťah už nie je len na anonymnej báze.  Na internete sa dá uskutočniť všetko: môžte si zahrať poker, billiard.... ale v skutočnom vzťahu sa hrá billiard s tágom v ruke a nie myškou, v skutočnom vzťahu vám človek úprimne radí pri pohľade do očí a nie len písmenkami na obrazovke. Koľko je takých ľudí, čo skôr ako si urobia rannú hygienu a raňajky zapnú Pc a Facebook. Majú tam stovky priateľov a pritom nikoho s kým by sa šli prejsť do prírody, alebo sadnúť na kávičku do mesta... 
   
 
 
 Slovensko a zahraničie... 
 
 Nikdy by ma možno nenapadlo písať podobný blog, keby som neprežil určitú dobu v zahraničí. Najviac na mňa zapôsobil pobyt v Spojenom kráľovstve, konkrétne v Severnom Írsku.  Je to krajina na vyspelejšej úrovni, čo sa týka platov, ekonomiky, služieb. Taktiež aj rozvoj technológií je tam výraznejší. Prístup k internetu má skoro každý, od detí po dôchodcov. Asi každý je súčasťou nejakej internetovej sociálnej siete.  Napriek tomu sa ľudia k sebe správajú úplne inak ako u nás. Na ulici vás len tak oslovia, či už ste na zastávke, alebo sedíte niekde na lavičke v meste alebo obchodnom centre. Dokonca aj v zhone pri nákupoch sa vás spýtajú na váš názor, napr, čo vravíte na tieto nové stoličky atď. Nezáleží na tom, či vás poznajú či nie, či máte roztrhané rifle, alebo sako.... či čítate noviny alebo pijete pivo na lavičke. Ľudia chcú komunikovať, pretože to považujú za prirodzené. 
 Dokonca aj v tej, toľkými ľuďmi nenávidenej Amerike, si okolo domov nestavajú ploty. 
 
   
 Na Slovensku si ľudia obháňajú svoje domy 1,5 metrovými múrmi. čo je však horšie, väčšina z nás si podobný múr stavia vo vnútri samých seba. Obliekajú si brnenie, aby boli odolní voči snahám o komunikáciu zo strany neznámych. Nechcú sa usmiať a rozprávať. A ani nepozdravia a keď pozdravíte VY, tak pozerajú s údivom, že prečo ich zdravíte. 
   
 NA tom Slovensku sa niečo pokazilo. Bol to socializmus, ktorí ľudí chcel spájať do jednej triedy a pritom ich navzájom odcudzil? Alebo je to v našej povahe? Kde je tá príčina?  Dnes, vo svete neobmedzených komunikačných možností, kedy môžme každý deň spoznať nových ľudí, strácame tú základnú schopnosť, ktorá nás oddeľuje od zvieracej ríše.... 
 ... strácame dar reči. 
   
 Je na každom z nás akou cestou pôjde, či mu bude prednejšie pohodlie domova, spred obrazovky Pc, alebo skutočná a príjemná konverzácia s úsmevom na tvári, ktorá urobí svet veselším. 
   
 Internetové sociálne siete a komunikačné prostriedky nám majú pomáhať, ale nemali by sa stať náhradou za živú komunikáciu. Hrozí, že tá sa pre ďalšie generácie stane niečím úplne neznámym, čo by malo za následok úplné odcudzenie sa ľudí.... 
   
 ....tak ako spieva David Gilmour z Pink Floyd.... "All we need to do is make sure we keep talking!" 
   
 Tak sa nebojme a rozprávajme sa! 
 
   
   
   

