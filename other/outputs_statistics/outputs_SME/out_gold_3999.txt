

   
 Sú isté veci, ktoré nechcem pošpiniť, lebo si myslím, že čakajú na väčšie chvíle, na väčšie myšlienky. Či také ale prídu, to je otázne.  Otázne je, či je niečo väčšie ako svetlá môjho mesta, keď sa vraciam z inokade. Či je niečo väčšie ako snežienky, väčšie ako kachličky na našej škole, bazén do ktorého chodíme plávať, škatuľa od topánok plná srdiečok, lístkov, pozorností. 
 Pred týždňom som začala používať hrnček z čínskeho porcelánu s vtákmi. Chcela som počkať na časy, keď príde elegancia, keď príde uhladenosť, keď príde vznešenosť. Od istého času však viem, že tie tri veci nechodia všade spolu. Že vznešené je všetko, čo si tým nazvem. 
 Ten hrnček som dostala od známych, keď ich paniu prišiel brat pred piatimi rokmi vyšibať. Vyložila som ho na skriňu a vyberala z krabičky, len keď som ho chcela niekomu ukázať. Tieto dni z neho pijem čaj a vidí ho každý. A asi to je pre popularitu toho hrnčeka u nás doma lepšie. 
 Nie je to záležitosť, ktorú riešim ojedinele. Mám prázdne zápisníky, do ktorých nechcem nič napísať, učebnice, do ktorých nechcem nič zaznačiť. A pritom, aká je to radosť nájsť poznámky v knihách z knižnice. Napríklad také odrátavanie strán dokonca v Suchej ratolesti od Vajanského. 
 Čítam si zoológiu a bavím sa na rybe, čo pláva hore bruchom. Lebkáči z nemenovanej cvikárne sa vyhrážajú ďalšími zápočtami. Mesiac menom apríl neveští na blízke obdobie pokojnú budúcnosť. Na učebnici ruštiny je narastajúca vrstva prachu. 
 Na stole máme papier na ktorý si kreslíme prasiatka a tvory neznámeho druhu. Je to skrátka žabičkus. Prikreslila som jednému okuliare na oči (nie uši) a napísala som pod neho Romankus žabičkus. Vyobrazený tvor, mi potom odkreslil Helenkusa žabičkusa a napísal nad neho veľkými písmenami ANATÓMIA. A k tvorovi šípky: nožičky, chvostík, oči, uši (nie oči), pravá packa, ľavá packa, nožičky, jazýček. A ten papier je skvelá vec. Sú na ňom mnohé zákony. Napríklad zákon o tom, že je čuníkovský zákon. Ešte je tam prasačí zadok so srdiečkom a čuník ako ho poznáme zo ZOO. Na tom papieri je okrem našich hovorov k sebe samým zoznam vecí, čo treba porobiť. 
 Treba sa s tým prestať naťahovať. Treba začať používať odkladaný hrnček, napísať pár viet do oranžovo hnedého zošita, možno pár poznámok. Skočiť do vody a odplávať dva kilometre. Lebo väčšie veci prídu, len ak ich zavoláme. 
   

