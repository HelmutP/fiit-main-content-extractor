

 Milujem žlté kvety a púpavy na zelenej tráve vytvárajú ten najkrajší koberec. "Však sme krásne." - smejú sa a veselo klebetia. Ktovie o čom? 
  
 O kus ďalej sa pri chodníku predvádzali krásavice hluchavkovité. Šuchorili si modré lupienky na prehliadku krásy. 
  
 Ich väčšie príbuzné sa zasnene kývali vo vánku a spievali o dokonalej kráse svojich jemne fialkových hlávok a nádhernom tvare zelených sukienok. 
   
 A ktože to konkuruje púpavám žltou farbou? "Sme síce malé, ale je nás veľa na konci jednej stonky. Spolu vytváramé kaskády a veselé žlto zelené gule." - smejú sa družne zvonivými hláskami. 
  
 Zanechala som cestičku, vybehla na kopec a ľahla si do trávy. Šteklila a šuchotala. Hmyz bzučal: "Čo je to za čudný tvor? Vyskúšame?" Ignorovala som ich a zahľadela sa do blankytnej oblohy. Zrazu som zachytila také šu-šu-šu. Tri strapaté hlávky spolu si čosi hovorili. 
  
 O kom sa to vedú reči? Á, tam je. Vysoký a pyšný so sukienkou z jemných kvietkov sa kolíše na dlhej stopke kvet skorocelu. "Som najkrajší. Proste dokonalý." - oznamuje celému svetu. 
  
 Nuž je naozaj krásny. Možno trochu namyslený, ale ktovie. V spoločnom tanci vzpínajú k oblohe svoje štíhle stonky podporené kopijovitými pevnými listami. Jaj, niektorým sukienky ešte šijú, ale im to nevadí. 
  
 Nechala som ich im tanečným hrám a zaskočila som na skúšku tanečnej choreografie drobných fialiek. Fialková diagonála musí byť dokonalá. 
  
 Oproti na briežku snehobiele sukničky krásne ladia so slniečkovou žltou a zúbkovanými listami. "Nielen naše sladké plody, ale aj naše listy sú zdravé a dobré." - prezrádzajú mi jahodové krásky. 
  
 Áno, čaj z nich je vynikajúci, ale drobné voňavé červené jahôdky sú naj, naj... zalizujem sa v duchu. "No toto! Táto nádhera sa tu kde zobrala? Žeby návšteva z ovocného sadu?" - prekvapene pozerám na zľahka frivolný ružový obláčik. 
  
 Stromy, ktorých zelené púčiky sa ešte len nesmelo rozvíjajú, sa s obdivom tlačia k útlemu stromu a obdivujú jeho krásu. 
  
 Ale jeden zo susedov sa nedal zvábiť obdivom. "Pozrite, moja kreácia v zelených tónoch je predsa mimoriadna a obdivuhodná, nie?" 
  
 Opustila som prekárajúce sa stromy a vybrala som sa na návštevu k drobným krásaviciam udomácnených v riadkoch vinohradu. Najskôr som stretla dve kamarátky. Neprezradili mi svoje mená. 
  
 To pastierske kapsičky, ktoré tvorili hustý koberec veselých hrkálok, sa neostýchali pripomenúť mi detské spomienky. 
  
 A čo za malý motýľ sa hneď vedľa objavil? "Nie, ja nie som motýľ, len sa na neho hrám." - zachichotal sa drobný kvietok. 
  
 Aha a tu poletujú ďalšie tri. 
  
 Nechala som za sebou ich veselé hry a zbiehajúc z kopaca ešte zdravím moje obľúbené púpavy. "Stopujete slniečka?" "Hi-hi-hi. My nie. Ale naše deti sa zanedlho rozpŕchnu do sveta." 
  
 Púpavy sa zdravia a posielajú ma preč. Vraj im zacláňam ... chcú sa kúpať v lúčoch slnka. 
  
 Opodiaľ sa starý vinič halil do kvietkov úplne iných. "Ale no, nevidíš, že to nie sú kvety?" - zahudral naoko dotknutý. Keď púpavy dávno zabudnú na svoju slniečkovú krásu a ich deti odletia do sveta na krídlach vetra, starý vinič omladne v bohatom zelenom šate a bude sa pyšniť slnkom zakliatym v jeho sladkých a opojných plodoch. Ale to je už o inej kráse. 
  
 Do videnia. 
   

