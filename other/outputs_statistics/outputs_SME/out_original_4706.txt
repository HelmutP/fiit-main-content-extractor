
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miro Kačmár
                                        &gt;
                Recenzie filmov
                     
                 Recenzia filmu: Bratia 

        
            
                                    16.4.2010
            o
            19:10
                        (upravené
                17.4.2010
                o
                9:45)
                        |
            Karma článku:
                3.40
            |
            Prečítané 
            1101-krát
                    
         
     
         
             

                 
                    Medzi Tommym (Jake Gyllenhaal) a Samom (Tobey Maguire) existovalo vždy prirodezne silné bratské puto. Dokázali si ho udržať napriek odlišným osobnostiam i spôsobu, akým zvládali prísnu výchovu vojenského otca i svoj život. Kým nezodpovedný  Tommy skončil vo väzení za ozbrojenú lúpež a stala sa z neho čierna ovca rodina, Sam sa naopak stal otcovou (Sam Shepard) pýchou. Oženil sa so svojou školskou láskou Grace (Natalie Portman), po vzore otca vstúpil do armády a príkladne  vychováva svoje dve dcéry.
                 

                 
Bratia Sam (Tobey Maguire) a Tommy (Jake Gyllenhaal)movies.nytimes.com
   V čase, keď sa Tommy po odpykaní trestu vracia domov, Sam odchádza na ďalšiu vojensku misiu do Afganistanu. Krátko po jeho odchode však prichádza správa, že Sam zahynul pri páde helikoptéry.  Napriek utrpeniu, práve Samova smrť poskytne Tommymu možnosť dospieť a ukotviť svoj život na pevnom bode. Tým bodom sa stáva Samova smútiaca rodina, s ktorou sa Tommy bližšie zoznamuje, a postupne si nimi buduje blízky vzťah - ako s jeho dcérami, tak aj so Samovou ženou Grace.   Sam však nezomiera, dostáva sa ale do afganského zajatia. Podstupuje výsluchy a nesutále fyzické, no predovšetkým psychické mučenie. Po nejakom čase je zachránený, a vracia sa domov. Prichádza však ako veľmi odlišný, silne traumatizovaný človek  z narušenou osobnosťou.    Radosť z návratu manžela a otca však rýchlo vystrieda pocit odcudzenia a strachu. Sama zožiera vlastné svedomie, a svoj rastúci hnev obracia proti Grace a Tommymu, ktorých podozrieva z toho, že ho podvádzajú.   Film Bratia vznikol ako remake úspešného rovnomenného dánskeho filmu z roku 2004, ktorý zozbieral viacero ocenení na filmových festivaloch a cenách.  Scenár americkej verzie napísal David Benioff (25. hodina, 2002) a natočil skúsený Jim Sheridan (V mene otca, 1993; Boxer, 1997). Nevidel som pôvodný film, ale myslím, že Bratia nie je ani zďaleka iba bezduchá kópia, natočená s vidinou lacného úspechu. Stačí si pozrieť tržby ďalších nedávnych filmov, ktoré tak, či onak reflektujú vojnu proti teroru/Irak/Afganistan, a každému producentovi vyjde podobná investícia ako vysoko riziková.   Bratia sú dobrou drámou, ktorá síce nachádza svoje východiská hneď v niekoľkých klišé (dvaja bratia - jeden príkladný, druhý „nevydarený", trauma z vojny, partnerský trojuholník), ale našťastie v nich nekončí.  A to je oveľa štastnejší prípad než, keď je originálny príbeh v závere utopený v záplave klišé.   Celkovému dojmu výrazne napomáhajú silné herecké výkony. Tobey Maguire podáva výborný výkon v najexponovanejšej role filmu, za ktorý bol nominovaný na Oscara, ale vyrovnané a nadpriemerné herecké výkony odviedli aj ostatní zúčastnení - Natalie Portman, Sam Shepard a Jake Gyllenhaal.   Hodnotenie: ●●●○○ 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Kúpili sme ZOO
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Princ z Perzie:Piesky času
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Robin Hood (2010)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Svitanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miro Kačmár 
                                        
                                            Recenzia filmu: Na hrane temnoty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miro Kačmár
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miro Kačmár
            
         
        kacmar.blog.sme.sk (rss)
         
                                     
     
        31-ročný pozerač filmov, čitateľ kníh, počúvač hudby. Tiež syn, kamarát, kolega, zamestananec, a všeličo iné
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    62
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1804
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Recenzie filmov
                        
                     
                                     
                        
                            Filmové všeličo
                        
                     
                                     
                        
                            Off Topic
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Anthony Hopkins odpovedá na otázky študentov
                                     
                                                                             
                                            Zaujimavé interview s Ridleym Scottom a Russellom Croweom
                                     
                                                                             
                                            Roger Ebert - The golden age of movie critics
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jonathan Franzen - Freedom
                                     
                                                                             
                                            Philip Roth - Lidská skvrna
                                     
                                                                             
                                            Pavel Vilikovský - Vlastný životopis zla
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            IMT Smile - Rodina
                                     
                                                                             
                                            Bruce Springsteen - Wrecking Ball
                                     
                                                                             
                                            Bob Seger &amp; Silver Bullet band - Greatest Hits Vol.1
                                     
                                                                             
                                            Ben Lee - Awake Is The New Sleep
                                     
                                                                             
                                            Stephen Kellogg &amp; the Sixers - Glassjaw Boxer
                                     
                                                                             
                                            Simple Minds - Graffiti Soul
                                     
                                                                             
                                            U2 - No Line On A Horizon
                                     
                                                                             
                                            Bryan Ferry - Dylanesque
                                     
                                                                             
                                            Bruce Springsteen - Working On A Dream
                                     
                                                                             
                                            Kings of Leon - Only by the Night
                                     
                                                                             
                                            Jason Mraz - We Sing We Dance We Steal Things
                                     
                                                                             
                                            Last.fm
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Roger Ebert
                                     
                                                                             
                                            Vlado Schwandtner
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Unie filmových distributorů
                                     
                                                                             
                                            Česko-Slovenská filmová databáze
                                     
                                                                             
                                            medialne.sk
                                     
                                                                             
                                            Box Office USA
                                     
                                                                             
                                            Internet Movie Database
                                     
                                                                             
                                            Roger Ebert
                                     
                                                                             
                                            www.movieweb.com
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ohýbaj ma, mamko...
                     
                                                         
                       objatie prostitútky
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




