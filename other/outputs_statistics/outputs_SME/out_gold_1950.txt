
 10. &gt; The XX - The XX   






  Vzostup a prvotný úspech debutantov The XX prišiel na konci leta, v čase rozpadu dovolenkových lások, rozchodov po opojných prvých milovaniach a začiatku sklamania, že celoročná rutina sa vracia do rozjarených mladých životov. Štyria dvadsaťroční Londýnčania, z ktorých počas koncertovania ostala trojica, to zdokumentovali svojou zmesou post-punku, R&amp;B, gitarovým popom a minimalistickou elektronikou. ‘The XX’ je priamočiarým vyjadrením lásky, telesnej príťažlivosti, neistoty a strachu v tom najobnaženejšej a najpríjemnejšej možnej forme.    9. &gt; Ben Frost - By The Throat   






 V rámci tejto top desiatky je Ben Frost obrovskou alternatívou, na experimentálnej scéne je ‘By The Throat’ zvláštne konvenčným a harmonickým dielom. Na novinke sa miesto cieleného pokúšania limít ušných bubienkov najrôznejšími frekvenciami a hlukom Frost rozvíja niečo oveľa jednoduchšie a pritom tak ťažko uchopiteľné - strach vo svojich mnohých podobách. V gotickej vážnosti sláčikov a ich plačlivej neistote. Vo vytí vlkov a nervozite spôsobenej divou, neskrotenou prírodou. V tvrdom a depresívnom metale. Všetky tieto vplyvy kombinuje v magickom celku intenzívneho audio-hororu vyvolávajúceho zimomriavky a vysoký tep, ktorého strhujúcosť sa ukrýva v dávkovaní pochybností, nepredvídateľnosti, premenlivej povahe strachu útočiacej na poslucháča z mnohých strán.     8. &gt; Marissa Nadler - Little Hells   






 Marissa Nadler bola vždy jedna z tých staromilských pesničkárok s gitarou v ruke a svojim smutným srdcom v hrdle chŕliaca jemné a jednoduché piesne o samote, potrebe čistej lásky a smrti. ‘Little Hells’ je našťastie dokonalejšia forma sebavyjadrenia. Balady o opustených a nechcených sa miešajú s autobiografickými skladbami, kde Nadler hľadá samu seba, snaží sa definovať svoje vnútro a pomenovať pocity. Na svojom štvrtom albume je Marissa odvážnejšia v ohľade použitých nástrojov, tak aj v pestrosti nálad, ktoré obmieňa, zdokonaľuje a prehlbuje. Zhudobnené príbehy v jej podaní sú najkrajšou tohtoročnou poéziou.     7. &gt; Fuck Buttons - Tarot Sport    






 Potom, ako úvodná ‘Surf Solar’ odhalí celú svoju vrstevnatú výšku, ‘Tarot Sport’ sa premieňa na smršť. Cyklón naukladaných slučiek, chladných elektronických melódií, presne odmeraných súzvukov; záplava hluku a podivuhodne usporiadanej symetrickej zvukovej anarchie, ktorá strhne a nepustí. Nie tým bolestným a deprimujúcim spôsobom Bena Frosta, ale osviežujúco a akosi oslobodzujúco. Fuck Buttons odpaľujú svoje melódie z tém predchádzajúcich, nechávajú ich vyvrcholiť v hlukovej katarzii a opäť pretvoria na niečo podobne hymnické a energické. ‘Tarot Sport’ je príťažlivý a oproti ich minulosti aj prístupný koncept harmonického chaosu.    6. &gt; Animal Collective - Merriweather Post Pavilion   






 Jedna z najvýraznejších indie kapiel práve končiacej dekády siahla tento rok na vrchol svojich tvorivých síl. Animal Collective dokázali uchovať svoju lásku k experimentovaniu, psychedelickým aj infantilným náladám s tým, že nahrali svoj najprístupnejší a najzábavnejší album. Viac dospelosti a viac rodičovskej zodpovednosti, no stále úžasná hravosť a radosť zo života rozmrazili už január obrovskou dávkou energie, ktorá nevyprchala ani trocha za takmer dvanásť mesiacov. Čo je predsa prívetivejšie ako vyhlásenie “I don't mind material things/.../ I just want, Four walls and adobe slats, For my girls”?    5. &gt; Hope Sandoval - Through the Devil Softly   






 S rozpadom Cocteau Twins, Slowdive a nakoniec aj Mazzy Star sa kvalitný dream pop vytratil zo scény na viac než dekádu. Komatózny shoegaze a zasnený pop však v poslednej dobe ožíva v podaní mnohých interpretov, z ktorých resuscitáciu v tomto roku najlepšie zvládla Hope Sandoval, speváčka Mazzy Star. Prináša to, čo by sme aj očakávali - narkotickú náladu, temné fantázie, abstraktný vnútorný svet uprostred snov obalený mnohovýznamovými poetickými textami. Zvonkohry, uhrančivé violončelo, či disonantné gitarové steny skvele dotvárajú ponuré hľadanie pokoja v krajine hmly, neistoty a snov, ktorej Sandoval vládne svojim šeptom.    4. &gt; Nils Frahm - The Bells   






 Je potešujúce, že stále vzniká kvalitná ‘súčasná klasika,’ snažiaca sa preniknúť pomedzi hradby elitárskych klasikov k “bežnému” publiku. Ku sprostredkovateľom takejto prístupnej, no stále vkusnej, originálnej a zaujímavej hudby patrí aj nemecký klavirista Nils Frahm. Pre odchovanca Rachmaninovho a Čajkovského hudobného odkazu je štúdiový debut ‘The Bells’ akousi etudou na nacvičenie najrôznejších nálad, od idylickej romantiky (‘Over there, it’s raining’), cez napäté hľadanie dokonalej harmónie (‘My Things’) až k neistote a neurčitosti zarývajúcej sa do kostí v strhujúcej ‘It Was Really Gray.’ ‘The Bells’ je zároveň exhibíciou jeho technických zdatností, jeho citu pre harmóniu a v zručnosti pri navodzovaní konkrétnych pocitov poslucháča. ‘The Bells’ je sľubom, že od Frahma môžeme čakať ešte veľmi zaujímavé kúsky.     3. &gt; Memory Tapes - Seek Magic   






 Leto a jeseň tohto roka sa jednoznačne niesli v znamení glo-fi a chill wave. Spomedzi všetkých týchto “samoukov” najviac kvalitatívne vytŕča Dayve Hawk stojaci za spriaznenými projektmi Memory Cassette a Weird Tapes. Všetky evokujú nostalgiu - za minulosťou, detstvom, slobodou a svetom fantázie - a všetky sú podivne tanečné a chytľavé. ‘Seek Magic’ je chvíľu tanečným parketom plným farebných indie kidz ako z nezávislého štokholmského klubu, inokedy je to zasnené spomínanie na časy odídené, potom zas príde radosť zo života a mladosti. Poslucháč sa musí až spokojnosťou usmievať, keď sa vrstvami priam infantilných loopov prediera Hawkov falzet spievajúci “It was a beautiful dream.” ‘Seek Magic‘ takým snom totiž skutočne je.     2. &gt; Hildur Guðnadóttir - Without Sinking   






 Na nezávislej scéne, ktorej dominuje čoraz viac príklon k jednoduchosti, uchopiteľnosti skĺbenej s ohromnou prekombinovanosťou a eklektizmom, je album typu ‘Without Sinking’ žiadaným vytriezvením. Klasicky trénovaná violončelistka totiž neprichádza zo scény vážnej hudby, ale z experimentálneho indie sveta. Prvotným konceptom albumu ‘Without Sinking’ je hľadanie duše violončela, pocitu dýchania a vzdušnosti. Výsledkom je stiesnená túžba po oslobodení, zachytaní každej ťaživej molekuly vzduchu a následnom ponore do hĺbok vedomia. Minimalizmus tohto diela sa ukrýva v čistote melódií, jasného vývoja a účelnej vrstvovitosti. Vnútorný nepokoj, s akým sa sláčik dotýka strún, je počuteľný v každej sekunde, rovnako ako technická rozvážnosť, s akou buduje všetky tematické celky. ‘Without Sinking’ presahuje žánrové škatuľky a každé jeho počúvanie odhaľuje nové zákutia hľadania strateného dychu.    1. &gt; Fever Ray - Fever Ray   






 Prvý samostatný album švédskej umelkyne Karin Drejier Andersson zásoboval poslucháčov hitmi počas celého roka. Temný, paranoidne chladný androgýnny nárek na absenciu citov ‘If I Had a Heart’ bol na prelome rokov len prvou lastovičkou. "If I had a heart I could love you, If I had a voice I would sing." Opis popôrodných depresií; izolácia od okolitého sveta; hymny na nordický svet sú len jednými z nespočetných dimenzií tohto pútavého projektu. Okrem hudby je totiž podstatnou aj vizuálna stránka spoliehajúca sa tajuplnosť, pohanské rituály a celkovú mystickosť. Charakteristický čierny obal nie je len zobrazením relatívnej temnoty vo vnútri Dreijer, jej momentálnu záľubu v opustených domoch, dyme a tvrdom metale, ale aj eleganciu, s ktorou podáva svoju tvorbu. Fever Ray sa nespolieha na intenzitu vyjadrovania myšlienok prostredníctvom tempa a surovosti (tak ako projekt The Knife s jej bratom Olofom). Temnota, ale aj popová prístupnosť sú v jej hudbe skryté oveľa sofistikovanejšie. Pomalým rozvíjaním tém zvyšuje zvedavosť, meditatívnou monotónnosťou v pozadí zintenzívňuje napätie, deštrukciou vokálov a nástrojov poslucháčov nepokoj. Tie potom zarazí pri ľahších a svetlejších skladbách pripomínajúcich prúd vedomia, aby sa opäť mohla uhrančivo vrátiť k pôvodným metódam, ktorými si podmanila poslucháča.    Úspech a tohtoročné prvenstvo Fever Ray potvrdila konceptuálnosť, s akou rozvíjala ladenie jednotlivých singlov a pritom stačilo tak “málo.” Vytvoriť súvislú líniu, prepojenie, akýsi príbeh uprostred svojich temných fantázií. Tma podporuje fantáziu poslucháča a dokresľuje aj to, čo nevidí, či nepočuje, rovnako, ako veci nevyslovené a nedopovedané. A v tom bola Dreijer vždy majster. Album Fever Ray je triumf ťažko definovateľnej Švédky dokazujúc, že na chytľavosť nie je potrebný rýchly rytmus a primitívne melódie a že nájdenie tej správnej miery mysticizmu, čiernoty, ale aj akejsi ľahkej irónie je možné s uchovaním elegancie a dobrého vkusu.     Bol teda rok 2009 rokom temnoty len preto, že Fever Ray, SunnO))) a Mount Eerie dosahujú najvyššie priečky? Je možné vytvoriť dobrý pop len prostredníctvom elektroniky tak ako Animal Collective, Memory Tapes, či Fuck Buttons, alebo to ide aj 'klasickou' akustickou cestou? Prepadáva sa nezávislá scéna sama do seba, keď Thom Yorke a Grizzly Bear prispievajú svojou hudbou infantilnému Súmraku? Sú nostalgické urob-si-sám žánre ako glo-fi, či surf pop naozaj také dobré, alebo je to len hra elitárskych redaktorov na alternatívcov? A vzniklo počas roka 2009 aspoň niekoľko pozoruhodných singlov, z ktorých vzniknú evergreeny? Všetky odpovede sú správne, pokým má poslucháč na ne názor a vie si vybrať, čo sa mu páči a čo už je “trocha moc.” Aj tak nakoniec siahne po tom, čo ho upútalo a nie po tom, čo sa vydáva za dobrý vkus. Akokoľvek, 2009 bol rokom kvality (viď rebríček), prekvapení (The XX, jj, The Big Pink, žáner glo-fi a mnohí debutanti) a príjemných návratov (Hope Sandoval, Kings Of Convenience). 
