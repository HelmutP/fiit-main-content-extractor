
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Roy
                                        &gt;
                Všetkým
                     
                 Naraz vypílili 500(!) stromov. Nezákonne! 

        
            
                                    18.3.2010
            o
            20:57
                        (upravené
                29.3.2010
                o
                10:13)
                        |
            Karma článku:
                5.49
            |
            Prečítané 
            1533-krát
                    
         
     
         
             

                 
                    Častokrát mi v tejto krajine dochádzajú slová. Nemôžem hovoriť. Mlčím, ochromená správou, kam sa až dá zájsť. Kam ešte môžeme zájsť? Kde sú hranice úcty voči našim blížnym, zamestnancom, pacientom, občanom? Kde sú hranice úcty k prírode, k historickým pamiatkam? Sú vôbec nejaké? Máme v sebe ešte (zdravé) hranice, alebo sme už tak chorí, že "všetko" je možné? Všetko! Vyrúbali 500 stromov - naraz a bez povolenia! Ja opäť nemám slov!
                 

                 Mlčím, keď tá správa cezo mňa prechádza - zhrozená v údive z toho, čo všetko sa (ešte) dá. Mlčím - vo svojom vnútornom tichu začínam písať   Bez úcty, bez rešpektu k tomu, o čo sa vôbec nezaslúžili, čomu nedali život, čo ich samých mohlo (malo) pretrvať, čo mohlo (malo) zostať aj po nich - nielen pre nich - zničili. Znovu - aké jednoduché!    Napadlo už niekedy týmto ľuďom, že to, čím sú, ich narodenie sa do ľudskej podoby - je len čistá "náhoda"? Svoju ľudskú podobu si ničím nezaslúžili! Ničím si nezaslúžili to, že žijú ako ľudia - nie stromy, nie zvieratá.   Ničím!   ____________________________   Viktor Emanuel Frankl vo svojej knihe "A napriek tomu povedať životu áno" - hovorí z pohľadu psychológa o (vlastnej) skúsenosti väzňa (väzňov) v koncentračnom tábore. Hovorí o tom, či a ako sa dá vôbec žiť a prežiť v koncentračnom tábore.   Hovorí - píše, že jedným z najzákladnejších predpokladov ako žiť a zvýšiť svoje šance na prežitie je vlastný, vnútorný svet človeka. Jeho vlastný, spirituálny, duchovný svet, do ktorého sa z nepriazne reálneho života môže uchýliť. Môže sa v ňom skryť, ukryť, nájsť v ňom útočisko. Môže v ňom zažívať pocity bezpečia, radosti, nádeje. Môže v ňom spočívať, prebývať, môže sa doň pravidelne vracať - je len jeho, nik mu ho nemôže vziať. Dáva človeku možnosť oddýchnuť si - v ňom, načerpať vnútornú silu. Dáva mu možnosť pomyslieť na to, že jeho krutá realita nemusí byť všetkým. Nemusí mať konečné slovo!   Jeho vnútorný, spirituálny, duchovný život, je oblasť, kam môže "kedykoľvek" vojsť - dokonca aj pri kopaní kanálov, v zime, v nedostatočnom oblečení, na smrť vyhladovelý, vyčerpaný, s omrznutými nohami, v nedostatočnej obuvi.   Jeho vnútorný, duchovný svet je len jeho. Môže sa v ňom napr. vracať do minulosti, ktorú mu už nik nevezme, aj keby ..... . Môže v ňom prebývať a prežívať -  meditovať - šťastné chvíle minulosti, môže sa v ňom (duchovne) stretávať so svojimi blízkymi, milovanými a "rozmlouvat" s nimi, predstavovať si ich, spomínať na chvíle prežité s nimi, môže meditovať - aké by to mohlo byť ... potom.   A môže v ňom aj spočinúť - v Bohu ....   Tento svet patrí len jemu. Nik mu ho nemôže (a nedokáže) vziať. Môže sa ho vzdať len on sám. Len on sa môže vzdať nádeje, svojej duchovnej slobody - utvárať svoje postoje k životnej nepriazni. Len on sám môže rezignovať, prestať čerpať zo svojho vnútorného, duchovného sveta.   Strata nádeje (vyvierajúcej aj z vnútorného, duchovného života) v kombinácii s nulovou imunitou fyzického tela je v podmienkach koncentračného tábora, v podstate stopercentne, kombináciou smrteľnou.   Boli takí - nemnohí - ktorí si svoj vnútorný, duchovný život priniesli do koncentráku so sebou, ale museli sa učiť čerpať z neho úplne odznova. A boli aj takí - nemnohí - ktorí ho objavili až tam. Pri hranici života a smrti.   Ako lekár, pracoval V. Frankl v koncentráku aj v nemocničnej časti. Spomína príbeh mladej ženy - pacientky, ktorá vedela, že v najbližších dňoch sa jej život skončí. Píše, že napriek tomu zostávala šťastná.   Takto na ňu v knihe spomína:    ""Som svojmu osudu vďačná za to, že ma tak tvrdo zasiahol," povedala mi doslova, "pretože vo svojom predchádzajúcom malomeštiackom živote som bola príliš zhýčkaná a svoje spirituálne ambície som nebrala dosť vážne."   Vo svojich posledných dňoch bola nesmierne oduševnelá.   "Ten strom tu - je mi vo chvíľach osamenia jediným priateľom." ukázala von, cez okno baraku. Vonku práve kvitol gaštan, a keď som sa zohol k prični chorej, videl som cez okienko zelenú vetev s dvoma rozkvitnutými svietnikmi. "Ja sa s tým stromom dosť často zhováram", povedala.   Divím sa a neviem, ako si jej slová vysvetliť. Je snáď v delíriu a má halucinácie? Zvedavo sa jej preto pýtam, či jej strom aj odpovedá. Áno. A čo hovorí? Na to mi žena odpovedá:   "Povedal mi: Som tu, ja som tu. Ja som život, večný život.""                                  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (15)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            O Zaujatí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Situácia je fakt lepšia: Už sa len Klame, Kradne a Kupujú hlasy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Ako sme volili v porodnici alebo O "prave nevolit"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Prochazka &amp; Knazko, alebo Nevolim zbabelcov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Nemozem uz citat spravy zo Slovenska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Roy
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Roy
            
         
        zuzanaroy.blog.sme.sk (rss)
         
                                     
     
         V posledných rokoch ma najviac oslovili myšlienky a knihy Anselma Grúna - nemeckého benediktína, a slovenských feministiek z ASPEKTu. Píšu o tom, kde nám to drhne (v živote) a ako z toho von. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    152
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1606
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Foto: Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Verše bez veršov
                        
                     
                                     
                        
                            Letters to Animus
                        
                     
                                     
                        
                            Môj pes Dasty
                        
                     
                                     
                        
                            Budúci prezident - aký si?
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Alternatívne školy - Bratislavský kuriér
                                     
                                                                             
                                            Môj Prvý pokus - Démonizácia Waldorf. pedagogiky v slov.médiách
                                     
                                                                             
                                            Odkaz Sorena Kierkegaarda
                                     
                                                                             
                                            Tomáš Halík: O výchove a vzdelávaní
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Joni Mitchell - absolútna nádhera: Both Sides Now
                                     
                                                                             
                                            Anselm Grün: Zauber des Alltäglichen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dr.Christiane Northrup - Womens Wisdom
                                     
                                                                             
                                            Sloboda Zvierat
                                     
                                                                             
                                            Anselm Grün
                                     
                                                                             
                                            Tomáš Halík
                                     
                                                                             
                                            Dominik Bugár - Fotograf, kolega - rodič
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       "Suma bola zaplatená pánovi dekanovi, ktorú pán dekan schoval do zásuvky"
                     
                                                         
                       David Deida - "Intimní splynutí - Cesta za hranice rovnováhy"..
                     
                                                         
                       All inclusive a piesok na zadku. Je toto dovolenka?
                     
                                                         
                       Moja Pohoda
                     
                                                         
                       Lož na kolesách: Pád Lancea Armstronga
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Gentlemani na Wimbledone
                     
                                                         
                       Druhá šanca pre človeka
                     
                                                         
                       Facka Ficovi
                     
                                                         
                       Mantra národa.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




