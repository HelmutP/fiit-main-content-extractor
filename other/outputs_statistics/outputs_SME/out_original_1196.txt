
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Doprava
                     
                 Zákon 8/2009 o cestnej premávke 

        
            
                                    4.2.2009
            o
            0:01
                        |
            Karma článku:
                10.26
            |
            Prečítané 
            49068-krát
                    
         
     
         
             

                 
                    Text zákona je písaný komplikovane a nie najprehľadnejšie. Na jeho  pochopenie je potrebne najprv pochopiť tvorcu zákona. Aby sa bežný  človek tohto zákona nenaľakal a ľahšie ho pochopil, pripravil som analýzu zákona s odkazmi.
                 

                 
  
   Najprv si treba uvedomiť plný názov zákona, ktorý je:  Zákon o cestnej premávke a o zmene a doplnení niektorých zákonov. Zákon sa  venuje cestnej premávke len v článku I. Ostatné články II, III, IV,  V, VI, sa venujú novelizácie iných zákonov a článok VII definuje iba termín účinnosti zákona.     Zákon 8/2009 Z. z. sa delí na nasledujúce články:      Čl. I Paragrafové znemie tohto zákona   Čl. II Zákon č. 135/1961 Zb. o pozemných komunikáciách (cestný zákon)    Čl. III Zákon Slovenskej národnej rady č. 372/1990 Zb.o priestupkoch    Čl. IV Zákon Slovenskej národnej rady č. 564/1991 Zb. o obecnej polícii    Čl. V Zákon Národnej rady Slovenskej republiky č. 145/1995 Z. z. o správnych poplatkoch   Čl. VI Zákon č. 725/2004 Z. z. o podmienkach prevádzky vozidiel v premávke na pozemných komunikáciách   Čl. VII Nadobudnutie účinnosti tohto zákona           Článok I, o cestnej premávke, sa delí na nasledujúce časti, spolu je ich osem:      
PRVÁ ČASŤ - ZÁKLADNÉ USTANOVENIA     DRUHÁ ČASŤ - PRAVIDLÁ CESTNEJ PREMÁVKY   TRETIA ČASŤ - DOPRAVNÉ NEHODY A EVIDENCIA DOPRAVNÝCH NEHÔD   ŠTVRTÁ ČASŤ - OPRÁVNENIA POLICAJTA PRI DOHĽADE NAD BEZPEČNOSŤOU A PLYNULOSŤOU CESTNEJ PREMÁVKY   PIATA ČASŤ - VEDENIE VOZIDIEL   ŠIESTA ČASŤ - EVIDENCIA VOZIDIEL, EVIDOVANIE VOZIDIEL A EVIDENČNÉ ČÍSLA   SIEDMA ČASŤ - ZODPOVEDNOSŤ ZA PORUŠENIE POVINNOSTÍ   ÔSMA ČASŤ - SPOLOČNÉ, PRECHODNÉ A ZÁVEREČNÉ USTANOVENIA           Niektoré časti sa delia na hlavy a niektoré hlavy na oddiely. Text zákona je zapísaný v paragrafoch, spolu je ich 146:    PRVÁ ČASŤ ZÁKLADNÉ USTANOVENIA   § 1 Predmet úpravy  § 2 Vymedzenie základných pojmov     DRUHÁ ČASŤ PRAVIDLÁ CESTNEJ PREMÁVKY  PRVÁ HLAVA - ZÁKLADNÉ POVINNOSTI  § 3 Všeobecné povinnosti účastníka cestnej premávky  § 4 Povinnosti vodiča   § 5 Povinnosti niektorých vodičov a spolujazdcov   § 6 Povinnosti prevádzkovateľa vozidla   § 7 Povinnosti inštruktora autoškoly   § 8 Používanie bezpečnostných pásov a iných zadržiavacích zariadení     DRUHÁ HLAVA - JAZDA VOZIDLAMI  § 9 Spôsob jazdy   § 10 Jazda v jazdných pruhoch   § 11 Jazda v mimoriadnych prípadoch  § 12 --//--  § 13 --//--  § 14 Obchádzanie   § 15 Predchádzanie   § 16 Rýchlosť jazdy   § 17 Vzdialenosť medzi vozidlami   § 18 Vyhýbanie   § 19 Odbočovanie   § 20 Jazda cez križovatku   § 21 Vchádzanie na cestu   § 22 Otáčanie a cúvanie   § 23 Zastavenie a státie   § 24 --//--  § 25 --//--  § 26 Zastavenie vozidla v tuneli   § 27 Železničné priecestie  § 28 --//--  § 29 --//--  § 30 Znamenie o zmene smeru jazdy   § 31 Výstražné znamenie   § 32 Osvetlenie vozidla  § 33 --//--  § 34 Vlečenie motorového vozidla   § 35 Osobitosti premávky na diaľnici a rýchlostnej ceste  § 36 --//--  § 37 --//--  § 38 Osobitosti premávky v zimnom období   § 39 Obmedzenie jazdy niektorých druhov vozidiel   § 40 Vozidlá so zvláštnymi výstražnými zvukovými znameniami alebo svetlami  § 41 --//--  § 42 --//--  § 43 Prekážka cestnej premávky   § 44 Osobitné označenie vozidla a parkovací preukaz    TRETIA HLAVA - PODMIENKY PREPRAVY OSÔB A NÁKLADU   § 45 Preprava osôb  § 46 --//--  § 47 Preprava osôb vozidlom pravidelnej verejnej dopravy osôb  § 48 Preprava osôb v ložnom priestore nákladného automobilu a v ložnom priestore nákladného prívesu traktora  § 49 --//--  § 50 --//--  § 51 Preprava nákladu   ŠTVRTÁ HLAVA - OSOBITNÉ USTANOVENIA O NIEKTORÝCH ÚČASTNÍKOCH CESTNEJ PREMÁVKY  § 52 Osobitné ustanovenia o chodcoch  § 53 --//--  § 54 --//--  § 55 Osobitné ustanovenia o cyklistoch  § 56 Jazda so záprahovým vozidlom a ručným vozíkom  § 57 Jazda na zvieratách, vedenie a hnanie zvierat  § 58 Osoba vykonávajúca prácu na ceste  § 59 Osobitné ustanovenia o cestnej premávke v obytnej zóne, pešej zóne a školskej zóne    PIATA HLAVA - ÚPRAVA A RIADENIE CESTNEJ PREMÁVKY  § 60 Dopravné značky a dopravné zariadenia  § 61 --//--  § 62 Riadenie cestnej premávky  § 63 Oprávnenie na zastavovanie vozidiel     TRETIA ČASŤ - DOPRAVNÉ NEHODY A EVIDENCIA DOPRAVNÝCH NEHÔD  PRVÁ HLAVA - DOPRAVNÉ NEHODY  § 64 Dopravná nehoda  § 65 Povinnosti vodiča pri dopravnej nehode  § 66 Povinnosti účastníka dopravnej nehody a škodovej udalosti    DRUHÁ HLAVA - EVIDENCIA DOPRAVNÝCH NEHÔD  § 67 Evidencia dopravných nehôd  § 68 Poskytovanie údajov z evidencie dopravných nehôd     ŠTVRTÁ ČASŤ - OPRÁVNENIA POLICAJTA PRI DOHĽADE NAD BEZPEČNOSŤOU A PLYNULOSŤOU CESTNEJ PREMÁVKY  § 69 Všeobecné oprávnenia  § 70 Zadržanie vodičského preukazu  § 71 Osobitné ustanovenie o zadržaní vodičského preukazu  § 72 Zadržanie osvedčenia o evidencii, evidenčného dokladu vydaného v cudzine, technického osvedčenia vozidla a tabuľky s evidenčným číslom     PIATA ČASŤ - VEDENIE VOZIDIEL  PRVÁ HLAVA - VEDENIE MOTOROVÝCH VOZIDIEL  Prvý oddiel - Vodičské oprávnenie  § 73 Všeobecné ustanovenia  § 74 --//--  § 75 Rozsah a členenie skupín a podskupín motorových vozidiel  § 76 Rozsah a členenie skupín a podskupín vodičského oprávnenia  § 77 Podmienky na udelenie vodičského oprávnenia  § 78 Vek na udelenie vodičského oprávnenia a vedenie motorových vozidiel    Druhý oddiel - Odborná spôsobilosť  § 79 Skúška z odbornej spôsobilosti  § 80 --//--  § 81 Skúšobný komisár  § 82 --//--  § 83 --//--  § 84 --//--  § 85 --//--    Tretí oddiel - Zdravotná a psychická spôsobilosť  § 86 Zdravotná spôsobilosť  § 87 Lekárska prehliadka  § 88 Psychická spôsobilosť  § 89 --//--  § 90 Povinnosti poskytovateľov zdravotnej starostlivosti    Štvrtý oddiel - Preskúšanie odbornej spôsobilosti, preskúmanie zdravotnej spôsobilosti a psychickej spôsobilosti  § 91 Preskúšanie odbornej spôsobilosti, preskúmanie zdravotnej spôsobilosti a psychickej spôsobilosti  § 92 Obmedzenie a odobratie vodičského oprávnenia  § 93 Vzdanie sa vodičského oprávnenia    Piaty oddiel - Vodičské preukazy  § 94 Vodičské preukazy  § 95 Vydanie vodičského preukazu  § 96 Výmena vodičského preukazu  § 97 Obnovenie vodičského preukazu  § 98 Povinnosti držiteľa vodičského preukazu  § 99 Nájdený vodičský preukaz  § 100 Neplatnosť vodičského preukazu  § 101 Medzinárodný vodičský preukaz    Šiesty oddiel - Vodičský preukaz vydaný v cudzine  § 102 Uznávanie vodičského preukazu vydaného v cudzine na vedenie motorových vozidiel  § 103 --//--  § 104 Výmena vodičského preukazu vydaného v štáte Európskeho hospodárskeho priestoru alebo v štáte dohovoru  § 105 Obnovenie vodičského preukazu vydaného v štáte Európskeho hospodárskeho priestoru alebo v štáte dohovoru  § 106 Neplatnosť vodičského preukazu vydaného v štáte Európskeho hospodárskeho priestoru alebo v štáte dohovoru    Siedmy oddiel - Evidencia vodičov  § 107 Evidencia vodičov  § 108 Poskytovanie informácií pre evidenciu vodičov  § 109 Poskytovanie informácií z evidencie vodičov    DRUHÁ HLAVA - OPRÁVNENIE NA VEDENIE ELEKTRIČKY A TROLEJBUSU  § 110 Oprávnením na vedenie električky alebo trolejbusu     ŠIESTA ČASŤ - EVIDENCIA VOZIDIEL, EVIDOVANIE VOZIDIEL A EVIDENČNÉ ČÍSLA  PRVÁ HLAVA - EVIDENCIA VOZIDIEL  § 111 Evidencia vozidiel   § 112 Poskytovanie informácií pre evidenciu vozidiel  § 113 Poskytovanie informácií z evidencie vozidiel   DRUHÁ HLAVA - EVIDOVANIE VOZIDIEL  § 114 Prihlasovanie vozidiel do evidencie  § 115 --//--  § 116 Zmeny v evidencii vozidiel   § 117 Zápis držiteľa vozidla  § 118 --//--  § 119 Odhlásenie vozidla do cudziny  § 120 Vyraďovanie vozidiel z evidencie  § 121 --//--  § 122 Zastupovanie   TRETIA HLAVA - EVIDENČNÉ ČÍSLA  § 123 Evidenčné číslo  § 124 --//--  § 125 --//--  § 126 Osobitné evidenčné číslo  § 127 Zvláštne evidenčné číslo  § 128 --//--  § 129 --//--  § 130 --//--  § 131 --//--  § 132 Spoločné ustanovenia o osobitnom evidenčnom čísle a o zvláštnom evidenčnom čísle  § 133 Poznávacia značka    ŠTVRTÁ HLAVA - SPOLOČNÉ USTANOVENIA  § 134 Výroba tlačív dokladov a tabuliek s evidenčným číslom a manipulácia s nimi  § 135 Osobitné povinnosti  § 136 Osobitné ustanovenia o niektorých vozidlách     SIEDMA ČASŤ - ZODPOVEDNOSŤ ZA PORUŠENIE POVINNOSTÍ  § 137 Porušenie povinností  § 138 Správne delikty  § 139 Ukladanie pokút     ÔSMA ČASŤ - SPOLOČNÉ, PRECHODNÉ A ZÁVEREČNÉ USTANOVENIA  § 140 Výnimky  § 141 Vzťah k správnemu poriadku  § 142 Vzťah k medzinárodným zmluvám  § 143 Prechodné ustanovenia  § 144 --//--  § 145 --//--  § 146 Zrušovacie ustanovenie         Text zákona sa zobrazuje v novom okne po kliknutím na príslušný paragraf alebo sem.   Ak chcete vo svojich článkoch, diskusiách odkazovať na konkrétny paragraf zákona, môžete to spraviť linkom, kde za znakom mreža uvediete požadované číslo paragrafu.   Príklad:  http://blog.sme.sk/blog/115/181318/2009-8-zakon-o-cestnej-premavke.html#146   Prajem veľa trpezlivosti pri štúdiu a nezabudnite ani na vykonávaciu vyhlášku 9/2009 Z. z. podla ktorej sa tento zákon vykonáva. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (28)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




