
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Richard Sulík
                                        &gt;
                Nezaradené
                     
                 K hodnote života ešte raz, polopatisticky 

        
            
                                    2.11.2008
            o
            22:39
                        |
            Karma článku:
                13.09
            |
            Prečítané 
            6055-krát
                    
         
     
         
             

                 
                    Veru vyše 700 diskusných príspevkov a taktiež 4 blogy (tu, tu, tu a tu) ako reakcie som nečakal a tiež ma príjemne prekvapila pomerne vecná rovina diskusie, aj blogerov. Bolo tam síce zopár prekrútení a tvrdení, ktoré som nikdy netvrdil, ale zažil som aj horšie a musím povedať, že som čítal množstvo kvalitných argumentov, z obidvoch táborov. Rád by som teda pokračoval v diskusii, odpustím si akékoľvek srandičky a už teraz sa teším na príspevky.
                 

                    Hodnota života     Tvrdím, že je nezmysel povedať, že každý život má presne rovnakú hodnotu. Áno, každý život má vysokú hodnotu, hodnota života je vysoko subjektívna vec a pre hodnotu života neexistuje jednotka, čiže hodnota života nie je merateľná. S týmto všetkým súhlasím a zároveň tvrdím, že životy nemajú presne rovnakú hodnotu (subjektívnu a nemerateľnú a vysokú).     Predstavme si, že Marek Nikolov je na topiacej sa lodi, jednou rukou visí na lane a druhú má voľnú. Do druhej ruky má možnosť zobrať svoje dieťa alebo moje dieťa. Koho zoberie? On to nevie, lebo tvrdí, že pre neho má každý život najvyššiu hodnotu. Ktorý život teda zachráni, keď každý ma presne rovnakú hodnotu? Hodí si mincou? Slečna Tutková by možno pristúpila k tomu tak, že predsa vždy je nejaká nádej, bude sa snažiť zachrániť obidve deti a utopia sa všetci traja. Ale čo spraví Marek Nikolov? Možno sa zacyklí.     Viem, čo by som spravil ja. Zachránil by som moje dieťa. Bez najmenšieho zaváhania. A prečo? No preto, lebo život môjho dieťaťa má pre mňa vyššiu hodnotu ako život ktoréhokoľvek iného dieťaťa. Samozrejme, že by som sa dostal do obrovskej dilemy, keby som si musel vybrať jedno spomedzi dvoch mojich detí a to práve preto, že ich život má pre mňa presne rovnakú hodnotu. Zaujímavé je, že keď niekto povie, že (keby sa musel skutočne rozhodnúť) by zachránil svoje dieťa a nie cudzie, že to je pochopiteľné, ale veľa ľudí sa začne zavzdušňovať, keď sa im povie nahlas dôvod pre takéto konanie.     Odbočím teraz k slovenskému top ekonómovi Eugenovi Jurzycovi, ktorý napísal článok o alibistoch, populistoch a ekonómoch. Úvodom trochu zdĺhavý, ale podstata je táto: Vo vyššie opísanej situácii by populista povedal: "Nie je pravda, že zachrániť sa dá iba jedno dieťa. Som ochotný sa rozhodnúť, ale najprv musíme vedieť zachrániť obidve deti" Bude to tvrdiť, až kým sa ho neopýtate, ako to druhé dieťa chce zachrániť, vtedy hrozí, že sa utopí niekto úplne iný (čo je opäť o rozličnej hodnote života). Alibista by povedal: "Pre mňa má každý život najvyššiu hodnotu, ak máte svedomie poslať na smrť nevinného človeka, urobte to. Ja s tým nechcem mať nič spoločné." Ak by súťaž o záchranu dieťaťa nepokračovala, alibista by nerozhodnutím rozhodol o smrti oboch detí (slovo súťaž nie je úplne vhodné v tomto kontexte, ale to je tým, že sú to vety kopírované z Jurzycovho textu).     A čo spraví ekonóm? Ekonóm je ten frajer, ktorý ľuďom povie, že existujú rôzne hodnoty (v tomto prípade života, všeobecne statkov, i keď uznávam, že slovo statok je silne späté s peniazmi) a existuje dopyt a ponuka, čo takisto nie je len o peniazoch.     "Nemôžeme mať všetko - napríklad, len času by sme väčšinou chceli viac, než ho máme k dispozícii. Podstatou ekonómie je pomáhať ľuďom vybrať si z možností, ako čo najlepšie uspokojiť ich neobmedzené požiadavky na obmedzené zdroje. Ekonóm je človek, ktorý túto pomoc ľuďom poskytuje bez ohľadu na to, či ekonómiu študoval, alebo nie. Je to každý človek. Jeho nezmieriteľnými protivníkmi sú už od nepamäti populista a alibista. Tiež sme nimi všetci. Ibaže niekto menej, niekto viac.", citát Jurzyca.     Ekonóm (a znovu opakujem, že to zďaleka nie je len o peniazoch) má ľudí radšej ako alibista a populista, ale ľudia nemajú radi jeho, lebo im na rovinu povie aké sú mechanizmy rozhodovania. Samozrejme, že im je milší Fico, ktorý všetkým všetko sľúbi a zamlčí, že to najprv musí niekomu vziať.     Ale späť k interrupciám. Použijem slová dopyt a ponuka a hodnota a prosím všetkých diskutérov, nech si odpustia reakcie, že som to zredukoval len na peniaze. Teraz to už vôbec nie je o peniazoch, je to o slobode. Takže, je splodené dieťa. Čo k tomu minimálne trebalo? Ju a jeho, povedzme posteľ a plodné dni. Tým chcem povedať a písal som to aj  minule, že splodenie dieťaťa nie je udalosť raz za rok. Je to biologický proces, všedný (70 tisíc ročne) a bodka. Naši spoluobčania sú výborným príkladom že nie je problém porodiť 15 a viac detí za život, ale problém je ich slušne vychovať. V prvom momente, život neživot, je sloboda matky ďaleko hodnotnejšia ako zopár buniek. OK, boh vložil dušu to tohto života, ale tú vkladá predsa do každého života, to tiež neobmedzuje ponuku "novosplodených" životov. Tým je aj zodpovedaná otázka jednému z diskutérov, že čo by som povedal, keby sa moja mama rozhodla ísť na interrupciu keď bola tehotná so mnou. No nič by som nepovedal, čo by som aj hovoril. Moja duša by sa narodila niekde inde.     To, že Hitler argumentoval s rôznou hodnotou života a napáchal toľko zla, predsa neznamená, že každý, kto si dovolí spochybniť rovnakú hodnotu života, by chcel posielať ľudí do plynu. Takisto v žiadnom prípade nechcem povedať, že len preto, že život vraha má nižšiu hodnotu ako život vysokoškolského profesora, musíme vraha zabiť (i keď v najstaršej demokracii sveta vrah dostane trest smrti). Čo chcem povedať je, že mužská spermia v ženskom vajíčku má nižšiu hodnotu ako sloboda matky. Na odôvodnenie som uvádzal extrémne situácie ako znásilnenie a postihnuté dieťa. Spravil som to preto, lebo keď nájdeme spoľahlivé riešenie v extrémnych situáciách, tak sme ho našli pre všetky situácie.     Takže sloboda matky. Celý článok bol o slobode a hodnotu života som použil ako pomôcku pochopiť, že aj sloboda je hodnota. Vysoká. Slobodné rozhodnutie je skvelá vec a musíme sa oveľa viac brániť, keď nám niekto chce na ňu siahnuť. Kresťania tvrdia, že život začína počatím a má ihneď vyššiu hodnotu ako sloboda matky? OK, je to ich slobodné rozhodnutie a ich svetonázor a každý kto chce byť kresťanom, sa bude musieť toho držať (alebo nech nie je kresťanom). A my ostatní? My nezasahujme do ich slobody a nenechajme si zasahovať nimi do slobody našej.     Na záver ešte jeden blog od jedného mladého lekára. Krásne napísaný, odporúčam prečítať. Držím palce Ivonke a jej rodičom. A odkaz pre Mareka Gajdoša: v prípade, že rodičia chcú (že sa rozhodnú už dávno pred pôrodom), určite to zmysel má.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (528)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Stručný prehľad rozkrádačiek a plytvania Ficovej vlády
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            V sobotu je referendum o politike SMERu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Napraviť škody v daniach bude náročné
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Ktorý pako nariadil kontrolovať mäkčene?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Richard Sulík 
                                        
                                            Harašenie s minimálnou mzdou alebo Prečo vám v obchode neodnesú nákup k autu?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Richard Sulík
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Richard Sulík
            
         
        richardsulik.blog.sme.sk (rss)
         
                        VIP
                             
     
         Od mája 2014 som Europoslancom a od marca 2009 predsedom strany SaS. Niečo vyše roka som bol predsedom parlamentu a cca tri roky obyčajným poslancom NRSR. Zažili sme raketový vzostup, pád vlády, pád preferencií, vnútorný konflikt, intrigy, špinu, spravili sme začiatočnícke chyby a nie jednu, ani desať. Ale nespreneverili sme sa našim hodnotám, nenechali sa vydierať, nemáme problém s financovaním a nekradli sme. Navyše, dnes sme omnoho skúsenejší. 

 V marci 2016 sa od voličov dozvieme, či to všetko stačí, či podľa nich patríme do parlamentu. Dovtedy makáme a spravíme všetko preto, by sme sa tam po tretí krát dostali. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                13.58
                    
                
                
                    Priemerná čítanosť
                    22456
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Registrované partnerstvo SMER - KDH
                     
                                                         
                       Pravda a presvedčenie v EÚ
                     
                                                         
                       Pán Vůjtek, teraz už mlčte
                     
                                                         
                       Bez peňazí z Bruselu by sme neprežili
                     
                                                         
                       Dva zúfalé týždne Roberta Fica
                     
                                                         
                       Kotleba je hlavne prehrou SMERu
                     
                                                         
                       Keď ide o smeráckych kmotrov, životy idú bokom
                     
                                                         
                       Bolševik (Fico) sa aj na prehratom spore nabalí
                     
                                                         
                       Slovensko a jeho pochybné Smerovanie
                     
                                                         
                       Ako páni Lipšic a Kollár na opačnú vieru konvertovali
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




