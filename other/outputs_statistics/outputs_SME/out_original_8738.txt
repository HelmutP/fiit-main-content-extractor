
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Matuška
                                        &gt;
                Nezaradené
                     
                 Loď do neznáma 

        
            
                                    13.6.2010
            o
            13:52
                        (upravené
                22.6.2010
                o
                12:21)
                        |
            Karma článku:
                6.58
            |
            Prečítané 
            701-krát
                    
         
     
         
             

                 
                    Tak a je to. Máme po voľbách.  Vieme, kto vyhral, vieme, kto sa do parlamentu nedostal a tušíme, kto nám bude ďalej vládnuť. To, čo nevieme je, ako nám tie nadchádzajúce 4 roky bude. Lepšie? Horšie? To je ťažko povedať, keďže tieto pojmy sú relatívne a hodnotenie je iné od človeka k človeku. Pred voľbami som často počul i čítal, ako všelijako sa  dá ľuďom pomôcť. Čo všetko sa dá urobiť aby sa mali lepšie, nižšie dane, akýsi príspevok štátu k minimálnej mzde (bližšie som sa tomu nevenoval, tak to ani neviem odborne pomenovať), a už klasicky, ako budú zákony platiť pre všetkých i tých „neprispôsobivých“.
                 

                 Akosi som nepostrehol to čarovné slovíčko - kríza. To sa spomínalo v poslednom čase len na spôsob: „Táto vláda za to nemôže, to kvôli kríze!“ No kríza tu stále je. I keď opadáva, no jej dôsledky s ňou neodídu. Tie musia riešiť ľudia vo vláde.  A ako riešiť? Nie som odborník na ekonomiku, no logicky mi vychádza, že vláda bude musieť šetriť. Budeme musieť platiť za to, čo sme nespôsobili, ale tak to už v globálnom svete chodí. Ja to chápem, že moja mama hneď k minimálnej mzde nedostane príplatok od štátu. Aj to, že sa možno spomalí výstavba diaľnic (politicky dosť citlivá téma).   Ale kto okrem mňa ešte vidí to, čo sa tak veľmi pred voľbami nespomínalo? Ty? Ten ktorý toto čítaš? Možno. Ale vie to aj Ondrej Bača z Hornej Dolnej?   A tu vidím veľkú vodu na mlyn pravdepodobnej opozícií. Ak bude v opozícií strana Smer, to ako keby vyhrala. Veď kto vie lepšie kritizovať ako Fico?  Pre mňa je bývalí pán premiér populistom s veľmi dobrým vystupovaním. A to môže byť pri voľbách 2014 veľmi dobre využité a sme tam kde sme boli.  Preto čím skôr, tým lepšie je si uvedomiť, že táto vláda nám zatiaľ možno viac zoberie ako dá, no v našom vlastnom záujme. Rozvoj môže prísť až potom, keď bude zastavený úpadok. Preto voľby do parlamentu 2010 sú síce dobrým začiatkom ku krajším zajtrajškom, no či chceme tie zajtrajšky pekné sa musíme rozhodnúť v roku 2014.   Tak snáď to, čo sa začne, sa bude môcť dokončiť. Je to len na ľuďoch a ich zdravom sedliackom rozume. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Prežitie v chotári. Pavol Satko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Znechutenie z blogov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Tridsať metrov pod morom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Dekriminalizácia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Matuška 
                                        
                                            Utópia, či?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Matuška
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Matuška
            
         
        matejmatuska.blog.sme.sk (rss)
         
                                     
     
        Som človek ktorého zaujíma všetko a nič.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    709
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Slovensko sa opäť blyslo neúčasťou na veľtrhu IHM
                     
                                                         
                       Slovensko už nemá žiadne remeslá?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




