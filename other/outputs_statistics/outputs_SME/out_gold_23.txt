
 Nechcem nikoho presviedčať o tejto forme dovolenky - že je to najlepšie a podobne.   
Sám som zažil  aj zájazdy organizované cestovkami (mal som len dobré skúsenosti), takže viem porovnať a mne to vyhovuje viac takto.  
Ak sa  na základe tohto článku rozhodne aspoň jeden z Vás pre tento spôsob cestovania, budem veľmi rád. Ak nie, aspoň sa s Vami podelím so svojimi zážitkami. 
 
Thajčania z turistov žijú a nepripravený človek sa nechá ľahko nachytať. Sú veľmi šikovní a vyna- 
liezaví a dosť sa vnucujú (samozrejme nie všetci, len predavači a spol. na ulici). Robia to absolútne suverénne, naozaj máte pocit, že tam čakajú práve na Vás. Už z diaľky na Vás kričia a mávajú, akoby tam čakal Váš najlepší kamarát. 
Riadil som sa heslom "s nikým sa nebav". Všade som sa snažil kráčať priamo a tváriť sa, že to tam  
poznám, na ich ponuky absolútne nereagujte, na nič neodpovedajte, len sa usmievajte a sem tam stačí  
povedať "thank you". Možno to vyzerá naduto a povýšenecky, ale len to Vás zachráni pred neskoršími  
problémami.  
Takže všetkých taxikárov, vodičov tuk-tukov, predavačov oblekov na mieru, nadháňačov pred  
reštauráciami a mnohých iných smelo ignorujte.  
 
S  Katkou sme sa brali v máji a na svadobnú cestu sme chceli ísť do Thajska. Rozhodli sme sa  navštíviť terajšie hlavné mesto Bangkok (Khrung-tep), bývalé hlavné mesto Ayutthaya a konečne relaxovať na ostrove Koh Samui v Thajskom zálive. 
Boli sme tam 12 dní čistého času + 2 dni cesta. Spolu 14 dní, prvých 5 dní v Bangkoku, zvyšných 7 na ostrove Koh Samui. 
Víza sme si vybavili na  thajskej ambasáde ktorá je v Bratislave vo vysokej budove Incheby na najvyššom poschodí. Potrebujete len fotku pasového formátu a 1000,- korún, ináč nič.  
Po víza si prídete v najbližší pracovný deň (PO-ST-PI), ak chcete víza na počkanie, musíte si priplatiť.  
 
Je dobré niečo si pred cestou o Thajsku naštudovať dopredu. Keďže sa v našom prípade jednalo o svadobnú cestu, nechcel som žiadne prekvapenia  a dosť poctivo som si naštudoval informácie o miestach kam sme chceli ísť.   
 
Tieto linky mi k tomu výraznou mierou pomohli a vrelo ich odporúčam: 
www.routehunter.com - super link na lowcostové aerolínky celého sveta  
www.virtualtourist.com - destinácie celého sveta s tipmi, trikmi, odporúčaniami - výborná pomôcka 
www.directrooms.com - cez tento link sme si rezervovali ubytovanie, absolútne bez problémov 
 
 
Jednoznačne najdrahšou položkou celého zájazdu je cesta. Niekoľko dní som teda hľadal na internete lacné letenky. Podarilo sa mi to cez Studentagency, kde mali akciové spiatočné letenky za 14500,- Sk  
s poplatkami so spoločnosťou Malév (Praha - Budapešť - Bangkok), alebo Aerosvit (Budapešť - Kyjev -Bangkok). My sme leteli z Prahy. Pripravte sa na takmer 14 hodín čistého času v lietadle, prestupy boli načasované dobre, takže sme zbytočne na letisku nestrávili veľa času. Po vystúpení na letisku si zaobstarajte z niektorého stánku mapu mesta s vyznačenými linkami metra, vlakov, prístavmi na rieke. Máp je dostatok a sú zdarma, odporúčam viac druhov.   
Z letiska sme sa do hotela dopravili autobusovou linkou spoločnosti Airport Express  
ktorá sa nachádza na najspodnejšom podlaží letiska hneď po východe vonku naľavo. Náš Bangkok Centre Hotel ležal na zastávke linky A4 (autobus 150 bahtov/os): 1 baht = 0,75 Sk .   
Venujte veľkú pozornosť výberu ubytovania. Obzvlášť v tak obrovskom meste ako Bangkok je, veľmi oceníte ak budete mať z hotela možnosť použiť niektorý z nasledujúcich spôsobov dopravy po tomto meste.  
 
Sky Train (BTS) - v BKK sú dve linky, ktoré  vedú v novšej časti mesta. Je to rýchly, klimatizovaný a čistý spôsob dopravy. Lístky sa kupujú na každej zastávke, musíte mať dosť drobných (ak nemáte, sú tam zmenárne). Na dotykovom displeji si zvolíte stanicu kam chcete ísť, vhodíte požadovanú sumu a vezmete si lístok. Ten potom vsuniete do turniketu, ktorý Vás pustí na perón - pozor, lístok si však musíte zobrať. Pri východe z cieľovej stanice musíte ten istý lístok opäť vsunúť do turniketu aby Vás pustil von. Vymakané a bez čiernych pasažierov. 
 
Metro (MRT) - mesto má len jednu linku, z dvoch zastávok sa však dá prestúpiť na Sky Train. Je to najnovší dopravný systém v meste. Metro je veľmi komfortné, čisté a samozrejme rýchle. Opäť si musíte kúpiť lístok na zastávke, na dotykovom displeji si zvolíte angličtinu, zadáte cieľovú stanicu a zaplatíte. V metre sa dá platiť aj papierovými bankovkami. Vezmete si výdavok a žetón, ktorý funguje presne tak ako lístky v Sky Train. Pri nástupe priložiť - vpustí Vás, pri výstupe vhodiť - môžete ísť. Môžete si kúpiť aj celodenný lístok, alebo rôzne iné zvýhodnené lístky. To platí aj pre Sky Train. Pozor však - celodenný lístok z metra neplatí na Sky Train a naopak. 
 
TAXI - v BKK jazdí obrovské množstvo taxíkov a pri dodržaní nasledovného sú veľmi lacné. Odporúčam aby ste si nechali na recepcii hotela preložiť do thajčiny názvy miest ktoré chcete vidieť a tieto kartičky len ukazovať taxikárom, porozumejú skôr ako mape. Vždy si vyberajte len taxík s označením TAXI METER a skôr ako nastúpite trvajte na tom aby bol taxameter zapnutý. Ak sa bude šofér ošívať, kľudne sa otočte a skúste iný taxík. Nepristupujte na žiadne dohody a ponuky. Jazdite len so zapnutými taxametrami. Za 30km jazdu na letisko zaplatíte cca 220 bahtov + mýtne poplatky na diaľnici, čo je však zanedbateľná položka. Tie bahty naskakujú  
naozaj pomaly :-). 
 
TUK-TUK - dobrodružný spôsob dopravy, hlavne v centre. Tu však musíte byť pripravení na všetko. Platia len ústne dohody, pred jazdou treba vždy na mape ukázať kam chcete ísť a vopred sa dohodnúť na sume. Beťári skúšajú rôzne finty. Napríklad, že Vás odvezú za smiešnu sumu kam chcete, ale cestou Vám ukáže ("just look"), niekoľko krajčírskych salónov, obchodov s diamantmi a podobne. Budú Vám hovoriť, že za to potom dostanú poukážky na benzín a podobne. My sme vedeli do čoho ideme a tak sme sa za 40 bahtov vozili dve hodiny po meste, samozrejme sme nič nekúpili, ale dobre sme sa bavili a hlavne obchodíky boli klimatizované, tak sme sa trochu ochladili. Často skúšajú na turistov aj také  
absurdity, že Vás budú presviedčať, že napríklad chrám do ktorého chcete ísť je zatvorený,  
alebo že oni majú lepší chrám, ktorý je otvorený len práve tento deň v roku a podobné nezmysly.  
Ničomu neverte a pre istou to skúste s niektorým iným, je si z čoho vyberať. 
 
Lodná doprava - cez BKK tečie Chayo Phraya River. Tomuto mestu sa kedysi hovorilo Benátky Ázie. Je popretkávané kanálmi - po tých sa môžete nechať zviesť prenajatou tzv. Long tail boat. Sú to úzke a dlhé lode s motormi z náklaďákov a dlhými lodnými  skrutkami s efektnou vlnou na konci. Odporúčam, je to zážitok. Uvidíte ako žijú ľudia pri kanáloch, všetko to, čo by ste za normálnych okolností asi nevideli. Hodina stojí 1000 bahtov/loď. Ak vystúpite v niektorom z osobných prístavov (Pier) na brehu rieky počítajte s poplatkom 20 bahtov za súkromnú loď.  
Avšak po hlavnej rieke premávajú verejné lode, niečo ako riečne autobusy. V každom z mnohých prístavov si môžete pozrieť mapku rieky s trasami a hlavne zastávkami kde ktorá loď stojí. My sme najčastejšie jazili Oranžovou linkou. Jazda za 17 bahtov, platí sa priamo na lodi sprievodkyni. Je jedno či pôjdete jednu, alebo 15 zastávok, cena je rovnaká. Kľudná a pohodlná  plavba celým mestom trvá takmer dve hodiny, za pár korún tak máte peknú vyhliadkovú plavbu. 
 
Železnica - Hua-Lamphong station - hlavná stanica v BKK. Od nášho hotela bola krížom cez križovatku. 
Za málo peňazí do okolia BKK.  
 
Ja teda odporúčam už vyššie spomínaný hotel. 21 eur/noc za klimatizovanú dvojposteľovú izbu s TV a minibarom, v cene raňajky formou švédskych stolov. Vyplatil som ho mesiac pred dovolenkou  
cez internet, neboli žiadne problémy, len som na recepcii ukázal pasy a voucher ktorý mi došiel  
domov mailom. Je takmer v centre, na hranici s Chinatownom a 10 minút pešo od prístavu na rieke. Leží na konečnej metra a blízko železničnej stanice. Hneď za rohom sa nachádza Wat Tramit - chrám s Budhom z 5,5 tony zlata. Po rieke sa potom loďou pohodlne dostanete k ostatným chrámom, takže žiadny problém. Metrom k Sky Train - tým sa akčný rádius riadne zväčšuje.  
 
A čo v BKK vidieť? To už nechám na každom z Vás. My sme sa ako som už spomínal previezli loďkou po kanáloch a loďou po hlavnej rieke. Na tuk-tuku sme obehli niektoré chrámy (odporúčam Gold Mount s pekným výhľadom na mesto). Každý večer sme boli na prechádzku v Chinatowne, kde to naozaj žije. Na ulici môžete vidieť skutočný život skutočných ľudí. Neváhajte ochutnať skvelú kuchyňu  v niektorej z mnohých reštaurácií. Jedlo od pouličných predavačov na ktoré nezabudnete. Vychutnajte si ten zhon a totálny chaos od stola jednoduchej reštaurácie s chladeným Singha beer v ruke. 
Ak máte radi nákupy, odporúčam nietorý z trhov o ktorých sa píše v bedekroch. Zaujímavý je obrovský MBK - obchodný dom na konečnej Sky Train - National Stadium Station, taká ázijská verzia Auparku3 
 
V okolí BKK je niekoľko zaujímavých miest vhdodných na jednodenný výlet. My sme navštívili bývalé  
hlavné mesto Ayutthaya. V súčasnosti sú to už len viac-menej zachovalé ruiny mnohých chrámov  
roztrúsené na veľkej ploche. Je to však pekné a po ich zhliadnutí si človek uvedomí aké to muselo byť 
kedysi krásne a obrovské mesto.  
Do Ayutthaye sme išli 3. triedou vlakom (20 bahtov) z hlavnej stanice pri hoteli. Cesta trvala 1,5h a tak si vezmite veľa vody.  
Sedí sa v otvorenom vagóne s niekoľkými ventilátormi na strope. Cez otvorené okná Vás ofukuje 
vietor a môžete sledovať obyčajných ľudí ktorí sedia a hlavne stoja naokolo.  Tu sa už netraba ničoho báť a dajte sa s prísediacimi do reči. Sú veľmi milí a hrdí na svoju zem a majú radosť  
ak sa Vám u nich páči.  
Po vystúpení z vlaku ste opäť v realite dobiedzajúcich tuk-tukárov a túlavých  
psov. Hodina tuk-tukom stojí 200 bahtov, my sme sa s jedným dohodli na 3 hodiny s tým, že nám ukáže najkrajšie chrámy a odvezie späť na stanicu. Mal nachystané zalaminované obrázky chrámov, hneď nám ukázal oficiálnu licenciu a tak sme išli. Do jeho notesa sme mu dopísali pár slov v slovenčine. Má  notes plný správ v jazykoch z celého sveta a ak mu poviete odkiaľ ste, ukáže Vám venovanie  Vášho krajana. 
Čo sľúbil aj splnil. Videli sme asi 7 chrámov, asi v polovici sa platí vstupné cca 30 bahtov na osobu, ale stojí to za to. Len majte veľa vody a prikrývku hlavy! Jednodňový výlet nás stál spolu 900 bahtov, úplne bez problémov. Boli sme sa pýtať v jednej cestovke, chceli 1200 na osobu... 
 
Na piaty deň sme ráno leteli hodinu  lokálnym lowcostom z BKK (bookované asi mesiac dopredu) Air Asia do mesta Suratthani,   na letisku si zapatíte transfer (350 bahtov) a idete 1,5h autobusom        do prístavu Don Sak na trajekt a odtiaľ   na Samui tiež 1,5h. Už v autobuse z letiska si dohodnite odvoz z prístavu na Samui do Vášho rezortu (my sme platili 400 bahtov) a tak budete mať pokoj. 
Odporúčam obed v prístave Don Sak, v jednoduchej samoobslužnej reštaurácii Vás výjde porcia riadneho karí na 25 bahtov. Štípe dvakrát. 
 
 
Na Koh Samui sú dve najznámejšie pláže: Chaweng a Lamai. Ostrov je hornatý, známy množstvom kokosových paliem a niekoľkými vodopádmi, dva z nich sú naozaj pekné. Trochu severne od ostrova je morský Marine park - chránená oblasť plná neobývaných ostrovčekov s peknými miestami na potápanie a šnorchlovanie, odporúčam ako jednodenný výlet. Je to tretí najväčší ostrov Thajska. Bohužiaľ je už aj na tomto ostrove letisko a tak časom to bude asi ako na ďalšom Phukete. Všetky komplexy a hotely ktoré tam boli doteraz, sú stavané len do výšky paliem.  
Ale už pri našom odchode z ostrova tam vyrastal ohavný Shopping park zo skla, betónu a ocele, ak sa 
tam niekedy vrátim asi to už nespoznám.    
Pláž Chaweng je drahšia, je tam biely piesok a veľa ľudí. Aj mimo sezóny som bol prekvapený koľko ich tam bolo. Ak je odliv, voda je ďalej od pláže a tak je to podľa mňa vhodné skôr na dovolenku s deťmi. Práve kus od tohto strediska nájdete letisko, Tesco, Makro a podobné civilizačné znamienka.  
 
Keďže sme chceli kľud a pohodu, rozhodli sme sa pre pláž Lamai. Cez net som poctivo hľadal recenzie a rôzne možnosti až som sa nakoniec rozhodol pre rezort Samui Orchid Suites. Nemohol som vybrať  
lepšie: 19 eur/noc (spolu, nie na osobu) za chatku s raňajkami formou švédskych stolov, s klimatizáciou, TV, chladničkou, WC s kúpeľňou, parádna široká posteľ pre dvoch, terasa s kreslami a výhľadom na  
more a palmy. Čisté chatky sú v krásnej a poctivo udržiavanej tropickej záhrade medzi kokosovými palmami. Okolo Vás lietajú  krásne motýle a  pobehujú rôzne  jašteričky. K dispozícii bezplatne parádny bazén s vodopádom a vírivkou, lehátka a slnečníky. 
K moru je to kúsok dole svahom pomedzi  dve jednoduché reštaurácie na brehu. Pláž je z jemného žltého a čistého piesku. Vstup do vody bez problémov a voda je hneď dosť hlboká na kúpanie. A keď je odliv, nemáte pocit, že ste pri polovyschnutom ramene rieky. 
Celý rezort sa nachádza na kraji hlavnej časti Lamai, takže nie ste v hluku, ale máte kľud a ticho. Nepočuť tam žiadne diskotéky ani ruch dopravy. Pritom pešo ste v centre za tri minúty, stačí zbehnúť na hlavnú cestu. 
 
Okrem kúpania a opaľovania sa toho dá robiť a vidieť na ostrove dosť. Požičali sme si Yamahu, nový malý skúter (500 bahtov/3dni) a prejazdili sme celý ostrov. Vidieť môžete pekné vodopády, v kopcoch je niekoľko  označených miest s výhľadom na celý ostrov a okolie, je tam taktiež Big Buddha - naozaj veľký, motýlia farma - pekné ak neprší, zoo, zvieracia šou, mumifikovaný mních ktorý umrel pred rokmi posediačky a doteraz tak sedí... Cez potápačské centrá sa dá ísť na rôzne výlety tohoto charakteru  
do okolia. Bolo to ako v malom raji, všade blízko, ale pritom kľud a pohoda. 
 
Stravovali sme sa zásadne v reštauráciách. Ceny ázijskej kuchyne sú pre nás veľmi priaznivé. Na ulici  
vyjde porcia kuraťa na 25 bahtov, v reštaurácii na pláži morské plody s ryžou 90 bahtov. Vždy sme sa riadne a chutne najedli a aj s nápojmi sme nikdy dokopy neplatili viac ako 350 bahtov. Počas dvoch  
týždňov sme toho ochutnali dosť a nemali sme žiadne tráviace ani iné problémy.  
 
Celú dovolenku som vybavoval prostredníctvom internetu, v pohodlí z domova. Aj keď je Thajsko vzdialená a exotická krajina, všetko klaplo na 100%. Cestovať tam na vlastnú päsť nieje žiadny problém, je to bezpečné a pri troche domácej  prípravy pred cestou aj bez problémov. 
Ako som už na začiatku spomenul, cesta tam/späť je najvyššia položka celého tripu, ktorá tvorí asi 50% 
nákladov. Dvoch nás celá dovolenka aj s vreckovým stála menej, akoby tam išiel jeden cez cestovku. 
 
Tu je niekoľko fotiek: 
 
Ayutthaya 
 
 
 
Náš hotel v BKK 
 
 
 
Ja a tuk-tuk 
 
 
 
Naša kamarátka na terase chatky 
 
 
 
Koktejl z čerstvého ovocia za 20 bahtov 
 
 
 
Veľký ležiaci Budha v Ayuatthaya 
 
 
 
Long tail boat 
 
 
 
Fastfood po thajsky 
 
 
 
Väčší brat nášho mosta SNP - Chao Phraya River v BKK 
 
 
 
Motílik v Butterfly garden na Samui 
 
 
 
Lamai 
 
 
 
 
Lenivý podvečer 
 
 
 
Riečny autobus 
 
 
 
Kto je šéf? 
 
 
 
Plávajúci trh 
 
 
 
Park and eat 
 
 
 
Big Buddha 
 
 
 
Vodopád na Samui 
 
 
 
A ďalší 
 
 
 
Vtáci 
 
 
 
Výhľad na BKK z Gold Mount wat 
 
 
 
Aj rožky nakoniec vystrčil 
 
 
 
5,5 tony zlata 
 
 
 
Konečne 
 
 
 
Tomu sa hovorí pohoda na pláži 
 
 
 
 
Ďalší chrám v Ayutthaya 
 
 
 
 
Ešte jeden 
 
 
 
 
Žltá je v Thajsku kráľovskou farbou 
 
 
 
Menšia križovatka 
 
 
 
 
 
 
Šťastnú cestu! 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
