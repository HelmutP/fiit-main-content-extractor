
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Miroslav Kocúr
                                        &gt;
                Theology Today
                     
                 Ktorá strana je kresťanská? 

        
            
                                    3.6.2010
            o
            13:30
                        (upravené
                3.6.2010
                o
                23:37)
                        |
            Karma článku:
                7.47
            |
            Prečítané 
            1859-krát
                    
         
     
         
             

                 
                    Ak sa pozrieme na označovanie slovenských politických strán adjektívom „kresťanský, -á" určite si musíme uvedomiť, že „nekresťanský" volič musí mať hneď niekoľko otázok. Okrem názvu sa totiž v čase predvolebnej kampane množia aj výroky a verejné postoje štylizujúce sa do niečoho, čo verejnosť môže považovať za „kresťanské".
                 

                 
... smery a cesty... je ich viac.archiv
       Javí sa to totiž tak, že deklarovaná a predstieraná príbuznosť kresťanskej tradícii môže pomôcť pri získavaní spoločenskej popularity, prípadne takáto „kresťanskosť" nechá slovenského priemerného voliča pokojným. Deklarovaný kresťan budí na Slovensku viac dôvery ako deklarovaný nekresťan. Najlepšie to vidno na paušálnom až alergickom hone na tzv. bojovných ateistov či moslimov akonáhle svoje nastavenie ako také deklarujú. V minulosti na Slovensku na svoju „nekresťanskosť" už doplatili Židia. Počas druhej svetovej vojny im dokonca nepomohla ani preukázateľne úprimná konverzia na kresťanstvo ani administratívne motivované krsty po spustení deportácií. Skrátka úprimní nekresťania to majú na Slovensku ťažšie ako kresťanskí komunisti modliaci sa raz k Stalinovi, raz k Brežnevovi a potom k Sokolovi či Korcovi. Niet nad priamočiaru jednoduchosť.       Byť kresťanom    Ako to však môže byť a dá sa to vôbec dešifrovať? Byť kresťanom je vo vedomí ľudí spájané s niečím vznešeným a akosi samo o sebe dobrým. Od kresťanov sa čaká, že vo svojom vystupovaní budú slušní, nevulgárni, korektní. Že nebudú kradnúť, že budú mať usporiadané rodinné vzťahy, že nebudú klamať, podvádzať, a nenávidieť. Skrátka na kresťana sú kladené aj zo strany jeho ideových odporcov tie najvyššie nároky. Nie je možné pre nedostatok priestoru všetky vymenovať. Na dôvažok sa očakáva, že ak kresťanovi niekto ublíži, ten mu to šmahom ruky odpustí. Nie je pritom potrebné ani požiadať o odpustenie, škodu odčiniť a svoje správanie zmeniť.  To je také skrátené predstavenie pasívneho vnímania kresťanskosti v prostredí slovenskej spoločnosti. Čo proti takémuto - podľa môjho názoru skreslenému vnímaniu „kresťanskosti" spravili za posledných slobodných dvadsať rokov kresťania sami? Podľa mňa mohli spraviť oveľa viac.       Kresťan nemusí byť slušný človek   Dnes je preto faktom, že byť kresťanom je skôr sociologická kategória a nie je spojená s morálnym imperatívom vyšších štandardov v porovnaní so spoločenstvami či jedincami, ktorí takúto ochrannú známku „patentovanú" nemajú. Kresťania boli spolupracovníkmi ŠtB, boli členmi komunistickej strany, spolupracovali s Husákom i Mečiarom, podporujú Jána Slotu aj Mariána Kotlebu. Kresťanom je Anton Srholec aj Ján Sokol. Ako to pred časom vo svojej knihe Mere Christianity popísal britský autor C. S. Lewis, byť kresťanom neznamená samo o sebe nič. Podobne ako byť džentlmenom znamenalo len, že nositeľom tohto titulu je človek istej spoločenskej kategórie. Aj zo slova džentlmen sa časom stalo niečo úplne iné. Za džentlemena dnes považujú ženy muža, ktorý sa slušne správa, slušne oblieka, má distingvované spôsoby. Označenie džentlmen má dnes skôr etický ako sociologický význam.   Na rozdiel od možnosti stretnúť džentlmena s modrou krvou, možnosť stretnúť kresťanov, ktorí zvyšujú cirkvám čísla v štatistikách, je dnes oveľa jednoduchšie. Preto nedorozumenie pretrváva. Kresťanom je aj podľa vnútrocirkevných predpisov každý, kto sa za kresťana považuje. Jeho morálnu výšku a kvalitu je síce možné spochybniť, nemožno však až na hraničné situácie povedať o niekom bez použitia sankcie či násilia, kto sa za kresťana pokladá, že kresťanom nie je.   Strany kresťanské podľa názvu   Na Slovensku kandidujú v roku 2010 dve strany, ktoré majú v názve aj prívlastok kresťanská, kresťanské. Niektorí politici využili počas uplynulých štyroch rokov príležitosť ukázať alebo deklarovať svoju príslušnosť ku kresťanských cirkvám či svoju ústretovosť cirkevným predstaviteľom tým, že sa s nimi stretávali pred kamerami, nechávali sa s nimi fotiť, boli voči nim ústretoví. Tým odpovedali na objednávku verejnosti spoločensky želateľným správaním. Moc kresťanského zaklínadla sa javí stále ako príliš nevýpočítateľná a nechcú si túto bohyňu slovenskej politickej šťasteny pohnevať.   Keď je meno príťažou   Faktom však je, že spájať s prívlastkom kresťanský len to, čo je dobré, mravné a vznešené môže byť  krátkozraké rozhodnutie. Pri pohľade na to ako sa o tento prívlastok uchádzajú ľudia rozsievajúci nenávisť voči menšinám, podnapití hulváti, ľudia s pochybným pôvodom obrovského majetku, ľudia figurujúci ako režiséri korupčných káuz či ľudia, ktorí verejne klamú a urážajú svojich názorových či politických oponentov majú veriaci ľudia povinnosť zostúpiť vo svojom uvažovaní trochu hlbšie. Aj keď je etiketou človeka a politickej strany deklarovaný  kresťanský background, dôležité je to, čo hovorí a ako koná v dlhodobom horizonte. Zároveň strany, ktoré v názve prídavné meno „kresťanský" majú, môžu skutočne mať v niektorých veciach týkajúcich sa spoločensko-politického fungovania navrch. Je to však skôr napriek tomu, že sú „kresťanské" ako vďaka tomuto prívlastku. To, prečo je to tak, je však už iná kapitola.   Od názvu k podstate   Spoločenská diskusia v kresťanskom prostredí sa preto musí presunúť z úzkeho konfesionálne zadefinovaného priestoru do oblasti univerzálnejších mravných hodnôt. Je potrebné vnímať hodnotu etickej integrity ako kategórie, ktorá je prínosom pre spoločnosť. Príslušnosť a lojalita k tej ktorej cirkvi nie je zárukou mravnosti ani etickej integrity. Naopak, niektorí ľudia sa vyznačujú osobnostnými kvalitami napriek tomu, že z úcty k tomu, čo oni považujú za duchovný a morálny étos kresťanského presvedčenia, by sa sami za kresťanov nikdy neoznačili. Papierovo však kresťanmi môžu byť. Je preto zrejmé, že podnapitý hulvát, ktorý pokrikuje na zhromaždenie slovami bratia a sestry má k duchovným princípom kresťanského životného štýlu neporovnateľne ďalej ako konfesionálne nezaradený intelektuál, ktorý nás osloví slovami milí priatelia. To, že takéto tvrdenia majú oporu aj v biblických textoch, je nad slnko jasnejšie každému veriacemu. Niektorí kresťania tieto riadky budú spochybňovať a vnímať ako proticirkevné. To asi aj sú. No len v tom prípade, že za cirkev niekto považuje úzko, priam etnocentricky, zadefinované spoločenstvo, ktoré nič cudzie nechce a svoje si nedá. Práve o tom je biblický príbeh ľudu, ktorý sa zrodil keď vyšiel z Egypta a začal mať problémy vtedy, keď odmietol byť soľou zeme a svetlom sveta.       Každý chce byť kresťan   Praktickým záverom by možno bolo povedať, ktorá slovenská politická strana je naozaj kresťanská ako to spravila skupina katolíckych kňazov zo Spiša. No bolo by to proti duchu a litere toho, čo som práve napísal. No ako som naznačil, na Slovensku je strán, ktoré sa hlásia ku kresťanstvu svojim názvom či ústami svojich vodcov viac. Pán Mečiar mal veľmi dobré vzťahy s pánom arcibiskupom Sokolom a neraz vyzdvihoval aj pána kardinála Korca. To mu nebránilo fyzicky napadnúť novinára, či poslať ich pri inej príležitosti do r... a postaviť si Elektru. S pánom kardinálom Korcom sa ochotne stretol krátko po voľbách premiér Fico a stál niekoľkokrát tesne vedľa neho na Devíne či na vedeckom sympóziu o Novembri. To mu však tiež nebránilo vysúdiť asociálne či „nekresťansky" vysoké sumy od médií, ktoré písali o tom, čo a ako hovoril a medzitým ich ešte aj označiť za idiotov.  Predseda KBS si bol podebatovať dokonca so všetkými nádejnými parlamentnými stranami. Jánovi Slotovi, ktorý sa svojim alkoholizmom, vulgarizmami, nenávistnými prejavmi a životom nad pomery vôbec netají, ani symbolicky prstom nepohrozil, takže aj on je vlastne prijateľný.                 Politika nie je náboženská viera, má však merateľné výstupy   Na Slovensku - podobne ako inde vo svete -  neexistuje ideálna strana. Politika je o miere kompromisu a netreba z nej robiť automat na ideály či predmet náboženskej viery. Sú tu však určite niektoré strany, ktoré sa hodnotám slobody, spravodlivosti a sociálnej solidarity snažili pri správe vecí verejných ostať verné viac, iným sa to darilo výrazne menej. Sú tu strany, ktoré prispeli výrazne k tomu, že sme členmi Európskej únie. To nás prezieravo uchránilo pred ešte väčšími korupčnými aférami ako bol obchod s emisiami.   Sú tu však aj také politické prúdy a hnutia, ktorých členovia neodškriepiteľným spôsobom spolupracovali s ponovembrovým či prednovembrovým zlom a od členstva v Európskej únii nás ich pôsobenie vzďaľovalo.  Svoju beztrestnosť a beztrestnosť spolupáchateľov si dokonca zabezpečili amorálnymi amnestiami. Robert Fico v roku 2006 uprednostnil spoluprácu s týmito ľuďmi pred spoluprácou s niekým iným. Následne vládol, tak ako vládol spolu s nimi - s dopredu amnestovanými zločincami a ich vtedajšími koaličnými komplicami.   Kresťania môžu voliť samozrejme koho chcú. Ľudia, ktorí budú voliť podľa svedomia reflektujúceho skutočnosť podľa objektívnej normy mravnosti a chcú spraviť pri voľbách informované rozhodnutie by z úvah o tom, koho voliť, mali určite vylúčiť HZDS, Smer a SNS hneď na začiatku svojho uvažovania.                  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (30)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Františkova cirkev s ľudskou tvárou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Nežiť aj žiť v demokracii
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pocit bezpečia vystriedali kultúrne vojny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Vydať sa na zaprášené chodníky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Miroslav Kocúr 
                                        
                                            Pápež František, rodová rovnosť a idea machizmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Miroslav Kocúr
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Miroslav Kocúr
            
         
        kocur.blog.sme.sk (rss)
         
                        VIP
                             
     
        www.aomega.sk


  
 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    181
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Today
                        
                     
                                     
                        
                            WEEK
                        
                     
                                     
                        
                            Theology Today
                        
                     
                                     
                        
                            MK Weekly
                        
                     
                                     
                        
                            Scriptures
                        
                     
                                     
                        
                            Listáreň
                        
                     
                                     
                        
                            Nechceme sa prizerať
                        
                     
                                     
                        
                            Oral History
                        
                     
                                     
                        
                            Zo školských lavíc
                        
                     
                                     
                        
                            Epigramiáda
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Last_FM
                                     
                                                                             
                                            BBC_All Things ...
                                     
                                                                             
                                            Radio_FM
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.aomega.sk
                                     
                                                                             
                                            The Tablet
                                     
                                                                             
                                            www.bbc.co.uk
                                     
                                                                             
                                            www.bilgym.sk
                                     
                                                                             
                                            www.vatican.va
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




