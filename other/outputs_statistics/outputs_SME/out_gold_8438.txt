

 Chceme sa ďalej takto baviť spolu s vami až do posledných chvíľ. Počas volieb síce budeme šíriť veľa tvrdých faktov, ale kto vie, možno na Facebooku sa ani my neubránime emóciám. 
 Ak teda túžite po volebných správach made in SME, toto od nás môžete čakať: 
 V sobotu až do uzavretia volebných miestností 
 
 zážitky a udalosti vo volebných miestnostiach slovom aj obrazom 
 môžete prezradiť koho ste volili v našich živých komentároch 
 
 Keď sa dovolí 
 
 rýchle odhady volebných výsledkov z prieskumov medzi voličmi 
 do živého komentovania sa zapoja komentátori SME (Leško, Shooty, Baťo, Fila, Morvay) 
 zhruba po polnoci prvé reálne výsledky 
 
 Keď budú výsledky 
 
 bleskové interaktívne štatistiky - strany, obce, poslanci 
 reakcie politikov 
 koaličné odhady 
 
 Všetko čo len trochu podstatné uvidíte na www.sme.sk, kompletne všetko na www.volby.sme.sk. 
 Všetko, čo nemôže byť v novinách, budeme zdieľať s našimi fanúšikmi na Facebooku a Twitteri. 
 
 pripojte sa k hlavnej stránke SME
 
 
nájdite si na Facebooku svoje mesto a dostávajte lokálne informácie 
 sledujte náš twitter @denniksme
 
 
 Držte nám palce, nech sa nezoderieme od roboty a nech nám vydržia stroje (pre každý prípad máme prichystanú aj záložnú titulku www.sme.sk). 
 Ale hlavne, nech vydrží optimizmus. 
   
   

