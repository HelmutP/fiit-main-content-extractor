

 
 
Toto bola tá príjemnejšia stránka vzťahu psa a človeka. Keď som pred približne rokom videl v televízii reportáž ako muž bil oceľovou tyčou svojho vlčiaka, pričom druhý muž to všetko snímal na kameru, zmocnil sa ma obrovský pocit rozčarovania (okrem iného) ako môže človek byť niečoho podobného schopný. Aby však toho nebolo málo, príbeh bol o to silnejší, že pes po celý čas (samozrejme, pokiaľ vládal stáť na nohách) pozeral na svojho majiteľa, akoby sa mu snažil povedať, že ty si môj pán, Tebe verím, zrejme vieš, čo robíš. Až kým padol do bezvedomie, kde nechutné divadlo pokračovalo.  
 
Častokrát sú však slová najslabším nástrojom ako na vážnosť nejakej témy upozporniť, preto vám prinášam zábery tých najneľudskejších prípadov týrania psov s krátkym popisom situácie. Vopred však upozorňujem, že fotografie sú naturalistické, so všetkým, čo k týraniu psov patrí, preto ak ste slabšej povahy, radšej v pozeraní príspevku nepokračujte. Iba, ak by ste aj napriek tomu chceli vidieť, čoho je človek - tvor najľudskejší – schopný.  
 
Všetky fotografie sú uverejnené s autorským súhlasom príslušných organizácií, ktoré fotografie vlastnia. 
 
Na obrázku je 10 – ročná fenka amerického kokeršpaniela. Fenka bola nájdená dehydratovaná, zanedbaná, v zlom výživovom stave. Niekoľko mesiacov nebola venčená, na sebe mala nabalené výkaly, uši hnisavé, nechty prerastené a stočené dokola. Fenka zomrela aj napriek obrovskej snahe veterinárov o jej záchranu. Mala rozsiahle infekcie uší, dutín, poškodený dvanástnik, najmä vďaka strave posledných dní, keď ju jej majiteľ prestal živiť úplne a nezostávalo jej nič iné ako jesť vlastné chply a výkaly. 
 
 
 
 
 
 
 
 
 
 
 
Na kosť vychudnutý, s hlbokými hnisajúcimi ranami na krku. Tak vyzeral tri a pol roka starý pittbulteriér, ktorý svoj boj o život prehral, pravdepodobne na celkovú otravu organizmu. Príbeh tohto psíka vzbudil obrovský záujem v médiach a odsudzujúce diskusie na internete, po ktorej nasledovali petičné akcie na zamedzenie podobných prípadov. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Myslím, že nasledujúce obrázky nepotrebujú komentár. Každý z týchto psíkov má svoj príbeh, ktorý mu udelil človek svojou krutosťou. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Táto doga mala tak silný zápal medziprstia a opuchnuté nohy, že nebola schopná chodiť. 
 
 
 
 
 
 
 
 
Fotografií s podobnými osudmi týraných psov je k dispozícii neskutočné množstvo. Niektoré sa končia smrťou v rukách veterinárov, iné v preplnených útulkoch. Je na každom z nás ako sa k tejto problematike postaví. Považujem však za potrebné, aby sa o tom neustále hovorilo. Aj tak sa dá pomôcť. 
 
 
 

