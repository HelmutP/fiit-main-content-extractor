

 Tu je zopár zaujímavých slovíčok, ktoré vydavateľstvo Oxford university press zaradilo do najnovšej, už ôsmej edície Oxford Advanced Learners Dictionary (časť slovníka je k dispozícii aj online): 
 staycation - stay (ostať) + vacation (dovolenka) - dovolenka na ktorej ostávate doma 
 yummy mummy - mamička na zjedenie - také tie 20niečo mamičky s hlbokým výstrihom a kočíkom 
 BOGOF - Buy One, Get One Free - dva za cenu jedného (akcie v supermarketoch) 
 wag - Wifes And Girlfriends of famous men - manželky a priateľky známych mužov (napr. manželky                 futbalistov sediace na štadióne vo VIP lóži) 
 chillax - chill out (ukľudni sa) + relax (relaxuj) - vykašli sa na to, nechaj tak, nebuď nervózny 
 big box - (veľká krabica) - veľká jednoposchodová budova v tvare krabice - hypermarkety 
 Všetko sú to slangové slová, ktoré sa stali natoľko obľúbené a používané, že sa ich v Oxforde rozhodli zaradiť do slovníka. Všetky tieto výrazy však majú pôvod v angličtine a teda sa nejedná o slová prevzaté z iných jazykov. Preto som si nechal to najlepšie nakoniec. 
 Čo naši najlepší národovci kritizujú najviac, je preberanie takých cudzojazyčných výrazov, ktoré už majú svoj slovenský ekvivalent, ergo nie je dôvod používať cudzie výrazy, najmä anglické. A ako je na tom angličtina? Za všetky len jeden príklad: 
 nada (nič) - pôvodne španielsky výraz 
 Tak takéto slovíčko sa rozhodol Oxford dať do svojho slovníka - používa sa najmä v USA, kde veľkú časť obyvateľstva tvorí hispánska komunita, angličtina má mnoho vlastných výrazov, ktoré vyjadrujú to isté (nothing, zero, zilch,...) a predsa si povedali, že obohatiť anglickú slovnú zásobu by nebolo od veci. 
 Má slúžiť jazyk ľuďom, alebo ľudia jazyku? 
 Človek sa učí celý život a preto argumentácia, že starší ľudia odchovaní na ruštine nerozumejú dnešným anglickým názvom obchodov a produktov je nonsens. Učiť sa, učiť sa, učiť sa - ako povedal súdruh Lenin. Btw. ja som bol odchovaný už na angličtine a nemčine a momentálne sa učím po rusky, pretože to potrebujem pri svojej práci. Len treba chcieť. 
 Alebo si radšej povieme, že nám žiadnych cudzincov netreba - "svoje si nedáme a cudzie nechceme"? 
   
   

