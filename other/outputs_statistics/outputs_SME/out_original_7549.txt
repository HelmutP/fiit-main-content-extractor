
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Kolenič
                                        &gt;
                Nezaradené
                     
                 Ako som sa stal dospelým kresťanom 

        
            
                                    29.5.2010
            o
            18:17
                        (upravené
                2.6.2010
                o
                6:50)
                        |
            Karma článku:
                10.56
            |
            Prečítané 
            1874-krát
                    
         
     
         
             

                 
                    Pred dvoma rokmi by mi vôbec nenapadlo, že ja budem práve ten, koho by ste mohli hľadať v kostole. Ale, ako sa hovorí, Božie cesty sú nevyspytateľné.
                 

                 
Kostol sv. Vincenta - ten náš RužinovskýShebi Team @ geocaching.sk
   Odmalička som bol podľa svojich rovesníkov staromódny. Nikdy som nedostal Tamagotchi (na ktoré som sa veľmi nechytal) alebo svietiace tenisky (ktoré boli veľkým hitom v mojich škôlkarskych rokoch), a čiernobiely Gameboy som si z nostalgie a dávnej túžby kúpil asi v dvanástich, keď už technológie boli o hodný kus dopredu. A napriek tomu "hendikepu" som úspešne prežil 16 rokov svojho života a kupodivu som bol sčítaný, svižne mysliaci a celkom veselý človiečik, ďaleko od niekoho, koho by som ja označil pojmom "staromódny".   "Staromódne" (používajúc slovo tak, ako moji rovesníci) som bol vychovaný, čo sa týka morálky,. Bol som slušný, hoci občas som nebol práve ten najsvätejší. Nechodil som do kostola, no pokrstený som bol (tak, ako zvyšok mojej najbližšej rodiny). Zásady morálky mi vštepovala mama do duše od útleho detstva, preto zo mňa nevyrástol žiaden vagabund, ale muzikant. Napriek tomu som bol vo viere vlažný a chvíľami som mal riadne krízy. To viete, hormóny robia svoje, a človek sa občas nechá stiahnuť prúdom doby... niekedy by sme mali byť silnejší.   V šestnástich som sa dostal k sólovému spevu na cirkevnej ZUŠ, kde som vlastne urobil prvé, neisté kroky smerom k plnšiemu prežívaniu viery. A slovo dalo slovo, pani učiteľka ma zapísala na koncert na slávnosť svätej Cecílie, patrónky ZUŠ, v kostole u nás v Ružinove. V tom čase, bolo to začiatkom novembra, som od babičky, ktorá dávno prestala chodiť do kostola, zrejme ešte kvôli červeným tlakom, dostal krížik po prababičke. Tá ešte pár rokov dozadu dala babičke krížik, ktorý napokon skončil v zásuvke. Prababička sa, mimochodom, na prelome rokov 2009 a 2010 dožila krásnych deväťdesiatín. Ten krížik bol symbolom akéhosi volania, ktoré sa vo mne ozývalo, túžby ísť do kostola... chýbala už len odvaha. Koncert v kostole mi preto prišiel náramne vhod. Po koncerte, ktorý nebol zlý, sa mi prihovoril jeden mládenec, Fero, ktorý ma zavolal spievať do zboru, ktorý v kostole mali a ktorý účinkoval na svätej omši, ktorá sa po koncerte konala.   Tu nastal ten zlom. Vnútri som jasal: "Taká úžasná príležitosť!" a náramne som sa tešil na nadchádzajúcu nedeľu. Moje začiatky na svätej omši boli zložité, nevedel som kedy vstať, čo odpovedať, vôbec som netušil, ako to tam beží, no časom som zvládol aj to. Začal som byť náramne aktívny v miestnej farnosti, dnes spievam v dvoch tamojších zboroch, hrám v ochotníckom divadle a zapájam sa aj ako animátor v Združení mariánskej mládeže (spoločenstvo podobné saleziánom).   Viera sa mi zdala celkom logická, vravím si "veď toľké tisíce rokov veria ľudia v Boha" a niečo vnútri mi šepká, že to je isto pravda. Aj prešpekulované zákonitosti prírody ma v tom všetkom začali utvrdzovať. Nie som prírodovedne založený, ale biológia je moja srdcovka, pretože ma neustále presviedča, že to všetko funguje úžasne a že nad tým Boh drží ochrannú ruku. Ale moje skutočné stretnutie s Bohom sa odohralo celkom inde, nie na hodine biológie.   Bolo to v kostole na Daliborovom námestí v Petržalke. Krátko po tom, čo som začal chodiť do kostola, ma zavolala moja pani učiteľka spievať na adorácie v ich kostole. V tom čase som bol vnútorne, použijem moderné slovo, rozbitý... skrátka, v duši som mal neporiadok. Prežíval som sklamanie z nenaplnenej lásky (to som prvý krát zažil, čo robí chémia), mal som nevysporiadané vzťahy s niektorými členmi rodiny a celkovo som bol na tom biedne. Adorácia mi ponúkla možnosť sa vnútorne stíšiť a vnímať Božiu lásku. Ešte som tomu celkom nerozumel, ale veril som, že Ježiš je prítomný v oltárnej sviatosti. A on tam bol. Keď som vychádzal z kostola, cítil som sa neskutočne príjemne, všetok bôl na duši bol na chvíľu preč a na mojej tvári sa spontánne objavil úsmev.   To ma presvedčilo úplne. Dotkol sa ma. Práve tam. Preto som začal pracovať na tom, aby som k Nemu mohol prísť bližšie. Pol roka na to som vďaka jednej úžasnej rehoľnej sestre, ktorá mi doplnila medzery v náboženskom vzdelaní (keďže ja som bol "etikár"), mohol pristúpiť k svätému prijímaniu. Spoznal som čaro spovede a radosť z Božieho milosrdenstva. Učím sa rozumieť svojmu svedomiu, ktoré to však občas preháňa a naženie mi niekedy riadny nepokoj do duše, ale vďaka svojim blízkym z kostola (a spod kostola, kde máme naše "dúpä") sa mi darí si svedomie cvičiť.   Uvedomil som si, že by som rád išiel aj na birmovku. Ešte som nebol celkom uzrozumený s tým, čo to obnáša, no stretnutia adeptov na birmovku a katechézy pod kostolom (hovoril som už, aké je to skvelé miesto?) mi všetko objasnili. Začal som sa náramne tešiť. Prišli aj skúšky, kde som našťastie obstál, a vtedy moja radosť z nadchádzajúcej birmovky začala ešte viac rásť.   Moje snahy o plnosť kresťanského života sa naplnili práve dnes. Dnes som bol pobirmovaný otcom arcibiskupom Zvolenským. Poviem vám, je to zážitok na celý život. Za birmovnú mamu mi išla kamarátka spod kostola, ktorá mi mnohokrát pomohla, keď som sa trápil so svojim svedomím, úžasné dievča. A na znak toho, že sa začína môj život dospelého kresťana, som prijal meno Pavol. Svätý Pavol z Tarzu, ktorý ma k mojej voľbe inšpiroval, má zaujímavý osud, podobný tomu môjmu (môj príbeh bol našťastie miernejší).   Svoju dospelosť pred štátom budem oslavovať až o necelé tri týždne, no za tie necelé dva roky môj život nabral nový smer a v mnohom som už dozrel. Cítim sa šťastný. Viem, kam patrím. Viem, komu patrím. Patrím Bohu. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (117)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kolenič 
                                        
                                            Čas realizmu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Kolenič 
                                        
                                            Žiadne drogy, radšej kefku! (alebo Ako mi vŕtali zub)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Kolenič
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Kolenič
            
         
        kolenic.blog.sme.sk (rss)
         
                                     
     
         Časy streleného gymnazistu sú preč. Teda, hlavne to gymnázium... strelený som zostal. Momentálne pracujúci študent, s dvoma veľkými láskami (lingvistikou a hudbou), trochou dezilúzie a snahou o aký-taký vlastenecký optimizmus. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    3
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1040
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




