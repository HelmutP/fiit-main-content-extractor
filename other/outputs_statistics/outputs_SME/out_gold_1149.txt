

 
Ak cítite, že sa s partnerom navzájom milujete a Váš vzťah má pred sebou perspektívnu budúcnosť, mali by ste vyskúšať spoločný život, počas ktorého partnera spoznáte omnoho lepšie a ujasníte si, čo by bolo najlepšie podniknúť. Vzájomné spolužitie má svoje výhody ale aj nevýhody, ktoré zažíva mnoho z nás a treba sa na ne vopred pripraviť. Aby všetko prebehlo v najlepšom poriadku a Vám nezostali len oči pre plač, nezúfajte a prečítajte si, čo všetko Vás čaká a neminie. 
 
 
Ak sa rozhodnete pre veľký skok do neznáma, vedzte či nie, ale tým pádom sa skončí Vaša sloboda a Vy sa budete musieť chctiac či nechtiac prispôsobiť návykom Vášho partnera. Ak budete mať k dispozícii zdieľať spoločnú izbu s partnerom a Vy budete radi ponocovať pri zapnutej televízii, zatiaľ čo on dáva prednosť skôr odpočinku bez akéhokoľvek zvuku a Vy nemáte možnosť presunúť sa niekam inam, budete sa musieť prispôsobiť a a vyhovieť jeho požiadavkám. Ak sa tak nestane a Vy budete presadzovať svoje, skôr či neskôr Váš vzťah stroskotá a Vám zostanú oči pre plač. 
 
 
Život v spoločnej domácnosti by mal byť okrem iného založený aj na kompromisoch, bez ktorých to skrátka nejde. Váš partner bol zvyknutý pred Vaším príchodom zvyknutý na určitý denný režim, ktorý mu vyhovuje a rád by sa ho pridržiaval. Inak tomu nie je ani teraz, keď ste sa k nemu nasťahovali Vy. Je samozrejmé, že aj Vy máte svoje návyky, ktoré dodržiavate, ale ak sa nemienite prispôsobiť ani v jeho príbytku, mali by ste zvážiť, či je spoločný život pod jednou strechou tým pravým pre Vás. V tom prípade by ste mali zostať ešte nejaký čas bývať sama a až po čase zvážiť, či to spoznajú a zistia, či sú pre seba ako stvorení, alebo si nemajú už čo povedať. 
 
 
Pri spoločnom bývaní môžu nastať omnoho častejšie hádky a pocit nevraživosti, keď bude mať každý z Vás iný názor. Častým prípadom je najmä riešenie finančnej situácie spoločného bývania s partnerom. 
 
 
V prípade ak sa k nemu sťahujete plná dojmov zamilovanosti a spokojnosti, nič také ako financie zrejme neriešite a pripadáte si byť taká bezstarostná. Postupom času však môže po Vás partner vyžadovať zloženie sa na nájomné. Navrhne Vám ultimátum. Buď mu pomôžete, alebo si môžete zbaliť kufre a ísť kade ľahšie. Vtedy je už len na Vás ako sa rozhodnete a zachováte. 
 
 
Ak budete zdielať spoločný byt či dom so svojim partnerom, celkom určite by ste si mali rozdeliť úlohy, ako je to, kto sa postará o celú domácnosť, uprace, navarí, operie a nakúpi. Ak sa dokážete rozumne dohodnúť a obom Vám bude tento systém vyhovovať, ste na najlepšej ceste užívať si harmonický a šťastný vzťah! :))
 

