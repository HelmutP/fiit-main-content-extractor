
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Banáš
                                        &gt;
                Liptovský Mikuláš
                     
                 Mikulášania na sme.blog.sk 

        
            
                                    27.5.2010
            o
            14:24
                        |
            Karma článku:
                2.36
            |
            Prečítané 
            727-krát
                    
         
     
         
             

                 
                    Jedna z prvých vecí, ktoré ma okamžite po registrácii na tejto skvelej stránke zaujímali bola, koľko tu nájdem ďalších blogerov zo svojho rodného mesta. Dlho som preto neotáľal a okamžite začal pátrať. A k čomu som sa dopátral?
                 

                     Na stránke sme.blog.sk je funkcia, ktorá pomocou google.map dokáže celkom jednoducho lokalizovať blogerov, podľa toho aké mesto udali ako svoje rodné, prípadne to, v ktorom práve bývajú.        Funkcia je to síce pekná a šikovná, o to ale náročnejšia na minimálne požiadavky – na slabšom PC alebo s pomalším internetovým pripojením ste takmer bez akejkoľvek šance na to ju využiť. Ako príklad spomeniem.    Hľadal som rodné mesto jednej mojej známej, rovnako aktívnej prispievateľky na tomto blogu, no a používal som pritom počítač svojej priateľky. Keďže ide o trochu starší „kompjúter“, navyše s pomalým (ale v reklamách samozrejme chválením) T-Comáckym internetom, mohol som „googlo – mapovať“ do čistého šialenstva, aj tak som sa nedočkal ničoho iného, ako brutálne škrípavého vrčania procesoru, trápacieho sa mimoriadne zložitou operáciou a následného tvrdého resetu celého počítača. Ani na ďalšie pokusy som nebol úspešný a tak som sa na to proste vykašľal.        Nasledujúci pokus bol už našťastie úspešný, išlo však o môj notebook – ktorý je len o čosi výkonnejší a s wifi- pripojením to tiež nie je práve ideálne. Veľa mojich známych vlastní počítače, ktoré celkovo majú problémy s novým softvérom, ale o tom potom, to by vystačilo aj na celý nasledujúci článok. Len som chcel trochu viac rozobrať podstatu problému, aj podstatu tohto článku.       Pre tých ktorí majú podobné problémy je venovaný aj tento „zoznam.“    Aj napriek tomu, že Liptovský Mikuláš je pomerne malé mesto, nájde sa tu veľa ľudí ktorí radi píšu – jedni sa naplno snažia presadiť svoj literárny talent, niekto skúša presadzovať svoje novinárske vlohy a niekto sa potrebuje len vykecať zo života. Zoznamujem vás (a nielen Mikulášanov) s blogermi z nášho krásneho malého mestečka:           Lucia Majková              Stránka: majkova.blog.sme.sk       O všetkom, čo mi preletí hlavou, čo zažijem, vypočujem si alebo uvidím a bude to stáť za to:)       O mne:       Far away in the sunshine are my highest aspirations- I may not reach them, but I can look up and see their beauty, believe in them... (Louisa May Alcott 1832-1888 )               Juraj Kostka      Stránka: jurajkostka.blog.sme.sk       o živote, o pocitoch, o štastí, o láske...       O mne:       Žijem aby som prežil a prežívam aby som mohol žiť....       Barbora Valková      Stránka: barboravalkova.blog.sme.sk       Make love, not war       O mne:       "Nenávidět neumím a jsem tomu rád. Když pro nic jiného, tak už proto, že nenávist kalí zrak a tím znemožňuje hledat pravdu." Václav Havel       Čítajte viac: http://barboravalkova.blog.sme.sk/#ixzz0oV028AfY       karma: 0.00       Bibiana Valigurová      Stránka: valigurova.blog.sme.sk       Budem písať o všeličom, ale najmä o tom, čo mi prinesú moje mozgové bunky a tých pár neurónov, čo sa tam ponevierajú.       O mne:       Plávam životom ako parník. Len dúfam, že nie som Titanic...       karma: 0.00       Veronika Baroková      Stránka: barokova.blog.sme.sk       O čom píšem? Myslím, že o mne.. O tom, čo ma upúta, povzbudí, potopí.. O veciach, ktoré do môjho života prinášajú optimizmus, ale občas aj negativizmus.. Proste o svete, ktorý ma obklopuje..       O mne:       Kto som? Ťažko povedať.. Komplikovaná osoba.. Inak povedané fenomenálne úžasná a vôbec nie žltá ;) Na svoj vek som príliš infantilná a hyperaktívna.. Tak, ako dokážem milovať, dokážem aj nenávidieť, proste som extrémista.. Jediné, na čo som za svoj život prišla je, že srdce robí veci z rozumu, ktoré rozum nikdy nepochopí..       karma: 0.00       Peter Druska      Stránka: druska.blog.sme.sk       Otázka života, vesmíru a vôbec = 42       O mne:       Viem, že nič neviem. Okrem toho vedenia.       karma: 0.00       Kamil Vanko      Stránka: kamilvanko.blog.sme.sk       Som šťastný, že ťa mám. Hoci viem, že nie v objatí sa skrývaš, ale v myšlienke a túžbe si uzavretá navždy.(Lier)....       O mne:       ....Ad astra per aspera****       karma:  0.00       Ján Púček      Stránka: pucek.blog.sme.sk       Čím čítať ? Očami ? (Ivan Laučík)       O mne:       V ľudoprázdnom interiéri preľudnená samota (Eugen Gindl)       karma: 0.00       Marek Hliničan      Stránka: hlinican.blog.sme.sk       Budú tu hlavne nejaké fotky odo mňa, ale raz za čas sa objaví aj nejaký ten text prípadne úvaha.       O mne:       ...usmievaj sa na toho, kto ti najviac ublížil, aby nevidel, ako veľmi ťa to bolí...       karma: 0.00       Juraj Mojžíš      Stránka: mojzis.blog.sme.sk       karma: 0.00       Ľubomír Šrol      Stránka: srol.blog.sme.sk       život,láska, trápenie, radosti, starosti       O mne:       veselý, často usmiaty (okrem rána keď treba vstávať) snivam, no stojím pevne na zemi       karma: 0.00       Martin Kuľhavý      Stránka: martinkulhavy.blog.sme.sk       Píšem o veciach, ktoré sa ma nejakým spôsobom dotýkajú, alebo o tých, ktorých sa chcem dotknúť ja.       O mne:       Tiger s motýľom na nose       karma:  8.52       Alenka Sabúchová      Stránka: sabuchova.blog.sme.sk       take care of my secrets       O mne:       niekedy sa pýtam, na čo je (toto) všetko dobré.       karma: 0.00 (VIP )               Anna Vlčková      Stránka: vlckova.blog.sme.sk       o bežnostiach       O mne:       Každý deň organizujem oslavy. Aj s balónmi a konfetami. 23 rokov, sever SR.       karma:  7.71 ( VIP )       Dominika Vlčková      Stránka: dominikavlckova.blog.sme.sk       o tom, co mam rada a co ma bavi...a momentalne o tom, ako sa studuje s americanmi       O mne:       nedavno mala 20 a od septembra studuje na LCI v Gamingu v Rakusku       karma: 0.00       No a nakoniec samozrejme ja :)   Milan Banáš      Stránka: milanbanas.blog.sme.sk       Bolesť nespôsobuje rozbitá tvár. Ale keď vám niekto ukradne sny... Témam sa medze nekladú, rovnako tak mojej fantázii. Tak s každého rožku trošku.       Ak chceš vedieť aký som - spoznaj ma. Ak chceš vedieť ako píšem - čítaj ma :)    Čo viac dodať? Som začínajúci autor z Liptovského Mikuláša.       Karma:  3.50       kontakt: Ak dačo tak písať na Facebook – Milan Banáš :)           Som rád že sa nás na najkvalitnejšom slovenskom blogovom webu zišla aspoň takáto „hŕstka“ Mikulášanov. Verím, že som svojím náhľadom na „píšucich Liptákov“ niekomu pomohol, no a tým druhým naopak ukázal niekoľko blogov, ktoré určite stoja za povšimnutie.          

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Ako za starých časov 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Dva póly filmového roka 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 2
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Banáš
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Banáš
            
         
        milanbanas.blog.sme.sk (rss)
         
                                     
     
        Ak chceš vedieť aký som - spoznaj ma. Ak chceš vedieť ako píšem - čítaj ma :) 
Čo viac dodať? Som začínajúci autor z Liptovského Mikuláša.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    129
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1458
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Liptovský Mikuláš
                        
                     
                                     
                        
                            Úvaha
                        
                     
                                     
                        
                            The Game
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Vyjednávač
                        
                     
                                     
                        
                            Záhady
                        
                     
                                     
                        
                            Legendy
                        
                     
                                     
                        
                            Poviedka
                        
                     
                                     
                        
                            Spektrum
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Luceno J. - Háv klamu
                                     
                                                                             
                                            Barry D. - Veľké trampoty
                                     
                                                                             
                                            Keel J. - Mothman Prophecies
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lady GaGa
                                     
                                                                             
                                            Depeche Mode
                                     
                                                                             
                                            Hudba z prelomu 80 a 90 rokov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Sčobák
                                     
                                                                             
                                            Peter Bombara
                                     
                                                                             
                                            Pavol Baláž
                                     
                                                                             
                                            Juraj Lisický
                                     
                                                                             
                                            Dominika Handzušová
                                     
                                                                             
                                            Roman Slušný
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Supermusic
                                     
                                                                             
                                            Gorilla.cz
                                     
                                                                             
                                            Hokejportál
                                     
                                                                             
                                            Facebook
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




