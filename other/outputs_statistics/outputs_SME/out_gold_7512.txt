

    
 Každý chce vyletieť 
 zo zovretých dlaní. 
 odraziť od zeme, 
 byť nespútaný... 
 Ľahko však prídete 
 o krídla v lete, 
 padáte ku zemi, 
 prečo? neviete... 
 Po lete prichádza 
 jeseň a zima... 
 V mraze viet kričíte: 
 „Miloval si ma?!“ 
   
 No o čom by bol život bez lietania? 
   
 Roztváraš krídla, 
 riskuješ pád... 
 Neostáva iné, 
 len veriť, že má rád... 
 že slová nie sú 
 len hŕstkou fráz, 
 po horúcom lete 
 nepríde mráz... 
 Veriť, no predsa 
 otvoriť oči..., 
 nech ťa ten pád 
 nikdy nezaskočí... 
   

