

   
 Podkopanie základov relativity 
   
 Anizotropia Vesmíru (L´anisotropie de l´espace) je názov dôležitého, viac ako 750 stranového zväzku, ktorý Maurice Allais publikoval vo francúzštine prostredníctvom Editions Clement Juglar (1997). V podstate prvý zväzok obrazoboreckého diela o fyzike, referujúcom o experimentálnych výskumoch, ktoré podľa autora vyústia v podkopanie základov teórie relativity. 
   
 Výskum začal v roku 1953, keď M. Allais uskutočnil dlhú sériu experimentov, ktoré súviseli so slávnymi Michelson-Morleyho experimentmi, skúmajúc vplyv pohybov Zeme na pozemské fenomény. Medzi najvýznamnejšie črty tejto práce sa ráta to, že jej autorom je ekonóm. A to nie hocijaký: M. Allais dostal Nobelovu cenu za ekonómiu v r. 1988.  V skutočnosti Allaisova orientácia na ekonómiu bola v prvom rade odozva na potrebu,ktorú predstavovala druhá svetová vojna. V úvodných stranách jeho knihy nám rozpráva, že pôvodne sa chcel oddať fyzike úplne. Odhaduje, že od r. 1950 strávil prinajmenšom štvrtinu svojho času v teoretických a experimentálnych výskumoch v tejto oblasti. 
 Táto práca sa ohraničuje na analýzu experimentálnych dát, jediný skutočný zdroj našich vedomostí a obzvlášť na analýzu nových experimentálnych dát otvárajúcich nové perspektívy v štyroch súvisiacich oblastiach výskumu: správanie kyvadla; optické odchýlky pozorovacích nástrojov a kolimátorov; až doteraz nezaznamenaným pravidelnostiam v experimentoch Esclangona a podobným veciam v D.C Millerových pozorovaniach s interferometrom . 
  
 Allais nástojí s rovnakým dôrazom aj na fakte, že v protiklade s inými výskumami tohto druhu, tie jeho sú založené na veľmi početných nepretržitých pozorovaniach - dňom i nocou - uskutočnených v dlhých časových obdobiach. „Nové dáta vyvodené z týchto experimentov sa javia nekompatibilné rovnako s teóriami pre-relativistického obdobia, ako aj so špeciálnou či všeobecnou Teóriou relativity." Táto práca je rozhodne nezvyčajná aj pre úžasný počet citácií, ktoré poukazujú na autorove hlboké vedomosti z histórie vedy. Allais nám hovorí, že už skoro od začiatku bol presvedčený, že gravitačné a magnetické pôsobenia sa dejú postupne, navrhujúc existenciu prenosového média, éteru. Avšak na rozdiel od predstavy, ktorú mali o étery pre-relativistickí fyzici , Allais považuje za dôležité priznať, že toto médium nemôže byť považované za absolútne vzťažný systém, ale je podrobený pohybu vo vzťahu k tzv. fixným hviezdam. 
 V prvých výskumoch, ktoré začal v r. 1950, bolo jeho zámerom experimentálne zriadiť vzťah medzi gravitáciou a magnetizmom, pozorujúc efekt magnetického poľa na kyvadlo vyrobené zo sklenenej gule. Tieto experimenty mu však neposkytli jednoznačné výsledky. Na druhej strane, ako sa však často stáva- najzaujímavejšia vec, ktorej existencia nebola predvídaná, a v dôsledku nebola skúmaná, prišla ako niečo dodatočné, iba zaznamenané. Konkrétne to, že pohyby kyvadla, ktoré najprv študoval s cieľom porovnať ich pohyb s iným kyvadlom pri absencii magnetického poľa, "nemohli byť zredukované na Foucaultov efekt, ale predstavovali veľmi dôležité anomálie, ktoré sa menili v priebehu času". A tak toto neočakávané pozorovanie vyústilo do výskumov, ktoré ustanovili tému jeho knihy. 
 Skôr ako sa lepšie pozrieme na Allaisove experimenty, je dobré si pripomenúť históriu, v ktorej už jeden Francúz pomocou kyvadla zmenil smerovanie vedy. 
 História Foucaultovho kyvadlového experimentu   
 Aristrachos zo Samos (3. stor. pr.Kr) vysvetlil zdanlivý pohyb Slnka a planét navrhujúc, že Zem sa točí okolo svojej osi a tiež putuje okolo Slnka. Hipparchus a Ptolemaj (2. stor. pr.Kr) odmietli tento pohľad  z dvoch dôvodov. Po prvé, rotáciu Zeme nie je cítiť. Po druhé, nie je vidieť ročné zmeny v relatívnej pozícii hviezd (bez výkonného teleskopu). Geocentrizmus dominoval v európskej vede až do 17. storočia. O guľatosti Zeme samozrejme nepochybovali počnúc Aristotela  ani stredovekí vzdelanci ani novovekí objavitelia. Americký paleontológ a známy popularizátor vedy Stephen Jay Gould vo svojej eseji Neskorý zrod plochej Zeme píše: „Za najstaršie zdroje, z ktorých sa šíril mýtus o stredovekej predstave o plochej Zemi sú: Dejiny konfliktu medzi náboženstvom a vedou (1874) a Dejiny vojny medzi vedou a teológiou kresťanstva od Andrewa Dicsona Whitea." 
 Hyparchovo a Ptolemajovo pozorovanie, že človek nemôže cítiť rotáciu je správne. Avšak miera rotácie požadovaná pre heliocentrický model (0,007 obrátky za minútu) je tak pomalá, že ani sa neočakáva, aby ju niekto cítil. Ako je teda možné zmerať tak pomalú rotáciu ? V r. 1851, Jean-Bernard - Leon Foucault zavesil jedno 67 metrov dlhé a 28 kilogramov vážiace kyvadlo z dómu v Pantheóne v Paríži. Dráha jeho kmitania, vzhľadom k Zemi, sa točila v smere hodinových ručičiek rýchlosťou 11° za minútu a tak urobila kruh za 32,7 h. Tento pohyb je najjednoduchšie vysvetliteľný rotáciou Zeme. 
  
 Je potrebné upraviť bežné nedorozumenie o Foucaultovom kyvadle. Niekedy sa uvádza (snáď poeticky), že kyvadlo sa hojdá v dráhe viazanej na vzdialené hviezdy, pričom Zem rotuje pod ním. Toto je najbližšie k pravde na póloch, a to neberieme do úvahy obeh Zeme okolo Slnka, ale len dennú rotáciu. A tiež to platí pre kyvadlo kmitajúce v smere východ západ na rovníku. Avšak pri všetkých ostatných zemepisných šírkach to pravda nie je... 
   
 A ešte jeden argument, zvažujúc pohyb kyvadla po jednom otočení Zeme. Vzhľadom k Zemi je perióda precesie kyvadla 23,9 h / sin stupňa zemepisnej šírky. Pre väčšinu miest je to značne dlhšie ako jeden deň. Takže po tom, čo sa Zem otočila raz, kyvadlo sa ešte nevrátilo do pôvodnej dráhy vzhľadom k Zemi. Takže čo je teda cesta pohybu kyvadla? Nezabúdajte, že bod uchytenia kyvadla je akcelerovaný okolo Zemskej osi. Teda sily pôsobiace na kyvadlo sú trochu komplikovanejšie a ich popis si vyžaduje nejakú tú matematiku. (Skutočne, dokonca aj pri dráhe pohybu v krátkej časovej škále je to len približne, pretože už pri jednom kmite záchytný drôt preletí cez veľmi mierne zakrivený povrch.) 
  
 Kozmologické otázky 
 Čo znamená, že objekt nerotuje? Čo je referenčným bodom, kde odstredivá sila a Coriolisove sily miznú? Bod, kde fungujú Newtonové zákony? Z pozorovaní zistíme, že Newtonov, či inerčný bod je ten, v ktorom vzdialené galaxie nerotujú. Ale ak odoberieme z vesmíru všetko okrem Zeme, ako budeme vedieť, či sa Zem točí, alebo nie ?! Alebo položíme otázku formálne, je to iba zhoda náhod, že bod v ktorom vzdialené galaxie nerotujú je inerčným bodom? Ernst Mach sa domnieval, že nie a špekuloval, že vzdialené hviezdy musia nejako ovplyvňovať inerčný bod (Machov princíp), ale nikto doteraz neprišiel s úspešnou a elegantnou teóriou. 
 Allaisove experimenty 
   
  
 Allaisove experimenty boli prevádzané od r. 1953 do r. 1960   v laboratóriu, v priestoroch Inštitútu Výskumu Železa a Ocele  v Saint-Germain a od r. 1958 aj v podzemnom kameňolome ( pod 57m skaly )  v Bougival. 
 Použité kyvadlo bolo autorom charakterizované ako "parakónické" , pretože jeho závesný systém pozostával z guľôčkového ložiska s priemerom 6,5 mm (amplitúda uvoľnenia bola 0,11 rad). Odlišné extrémne polohy tohto kyvadla vytvárajú obraz plochy približne v tvare kužeľa. 
 Kyvadlo bolo uvoľnené každých 20 minút prepálením lanka, ktoré ho udržiavalo v oddychovej pozícii. Jeho pohyby boli sledované 14 minút z hľadiska kalibrovaného na azimut  ( azimut -smerový uhol počítaný od základného smeru zemepisného alebo magnetického poludníka) dráhy kmitania s presnosťou v rade desatín stupňa. Kyvadlo bolo potom opäť zastavené a po 6 minútach opäť uvoľnené v dráhe posledne pozorovaného azimutu a toto sa robilo deň a noc, počas pozorovacích zasadaní v priebehu jedného mesiaca. 
 POZOROVANÉ RÔZNE PERIÓDY 
 Výsledky tejto prvej série experimentov ukazujú existenciu niekoľkých periód, ktorých analýza bola prevedená štatistickými metódami. Hlavné periódy a to 24 hodín, 24h 50min prislúchajú k vlnám K1 a M1 v teórii prílivu a odlivu. Tieto sú takto klasifikované autorom ako luni-solárne. Čo je vskutku pozoruhodné, je ich amplitúda. Vo všeobecnosti sa vie, že gravitačné pole Slnka a Mesiaca musia hrať rolu v pohybe kyvadla na povrchu Zeme, ale podľa Newtonovej teórie, neskôr upravenej a potom nahradenej teóriou relativity by boli tieto efekty veľmi jemné, ale nie o intenzite objavenej Allaisom. To je jedna z príčin, prečo sú tieto experimenty tak zaujímavé. 
 Strany 118-136 nás prevádzajú výpočtami zahŕňajúce takéto fenomény z pohľadu klasickej fyziky. Potom nasleduje extrémne kráka diskusia o možných príčinách omylu. Jediná navrhnutá  príčina a následne aj odmietnutá, zahrňuje možné chyby  v podpere na ktorej zotrváva zavesenie guľôčkového ložiska. Je to rozoberané možno trochu zosumarizovane, ale v poslednej kapitole sa to rozoberá znova. Akokoľvek, podobnosť výsledkov získaných v Saint-Germain a v Bougival v lome "Blanc Mineral" 60 m pod zemským povrchom, ukazujú, že musíme vylúčiť, zástup iných možných vinníkov. 
 Ak bola stále nejaká pochybnosť o skutočnosti javov pozorovaných Allaisom, pozoruhodné fakty sa objavili počas dvoch zatmení Slnka, napriek tomu to vyzerá, že autor minimalizuje ich dôležitosť, a priniesli prvok hodnoty pripísanej k výsledkom jeho výskumu ako celku. V priebehu prvého zmieneného zatmenia ( úplné zatmenie Slnka z 30.júna 1954) napríklad, dráha kmitania kyvadla sa otočila prudko o 15 grádov, až potom sa vrátila do svojho pôvodného azimutu. Obdobný efekt bol zaznamenaný počas zatmenia 2.októbra 1959. 
  
   
  
  
   
   
 Inerčno(zotrvačno)-gravitačná ekvivalencia 
  
 Rémi Saumont, riaditeľ Hlavného zdravotníckeho inštitútu Francúzska pracujúci na matematických a fyzikálnych otázkach, publikoval recenziu na Allaisovu knihu, tá sa najskôr objavila vo francúzskom Fusion a neskôr americkom 21st CENTURY. Domnieva sa, že vo výklade gravitačných anomálií v priebehu zatmení Slnka spočíva problém v interpretácii: „Je to otázka inerčnej anizotropie, alebo gravitačnej anizotropie? Podľa všeobecnej teórie relativity sa treba pozerať na inerciu aj gravitáciu rovnakým spôsobom - to jest ten slávny princíp nazývaný ekvivalencia, medzi gravitáciou a inerciou. Zabúdame, že dokonca aj z pohľadu relativity je ekvivalencia striktne lokálna. V astronomickej mierke musí byť mechanizmus zodpovedajúci týmto dvom fenoménom odlíšený, pretože, pravdupovediac, nemajú rovnakú dimenzionálnu hodnotu. Z toho dôvodu sa zdá byť pozorovaná anizotropia skôr gravitačná, ako inerčná, pretože vyzerá to tak, že gyroskop nie je ovplyvnený." To však môže byť objasnené konštrukčnými vlastnosťami gyroskopu - jeho rotor nedokáže reagovať na bočný pohyb, zatiaľ čo kyvadlo áno. 
   
 V druhej fáze jeho experimentov sa Allais snaží zistiť prísnym spôsobom smer prejavujúcej sa anizotropie.  Získané výsledky potvrdzovali tie predošlé. Merania uskutočnené v roku 1959 počas zatmenia Slnka umožnili potvrdiť, že jedným z efektov zatmení je otáčanie sa dráhy kmitania kyvadla smerom k Mesiacu a Slnku. (p.136) 
 Vo všeobecnosti by v žiadnom momente nemal existovať uprednostňovaný smer, ku ktorému by sa dráha oscilácie kyvadla mala tendenciu premiestniť a tento smer by sa nemal meniť v priebehu času, ako funkcia astronomických podmienok v určitom momente. 
 DÔSLEDKY JEHO PRÁCE 
 Takto by jeho výsledky, celkovo vzaté, naznačovali na anizotropiu fyzikálneho vesmíru, kde smer je premenlivý v čase, ale ktorého hlavný smer bol orientovaný z východu na západ. Allais nás prevádzal detailným popisom týchto odlišných pozorovaní a kalkulácií, išlo o popis vo forme laboratórnej správy, ktorej štúdium pre človeka skutočne namáhavé a podrobné, pretože si to vyžaduje odborné technické znalosti nad úroveň priemerného čitateľa, alebo dokonca vedca. 
 A čo sa týka tých podrobností pri každom výklade jeho výsledkov, musíme ich snáď pripísať túžbe po rigoróznosti, tak trochu vystupňovanej opozíciou, ktorú našiel vo vedeckej komunite.  K tomuto nám Allais hovorí dobre okorenenú anekdotu. Nasledujúcu pasáž cituje z odmietavého listu, ktorý mu poslal Jean Leray: "Publikovanie vášho textu, nech by sa objavil kdekoľvek, vrhne pochybnosti na metódy, ktoré používate nielen vo fyzikálnych, ale aj ekonomických vedách; v tomto zmysle by bolo jeho publikovanie užitočné." Allais uštipačne dodáva: „Som zvedavý aká bola jeho reakcia, keď mi bola udelená Nobelova cena za ekonómiu." 
  
 Treba pripustiť, že písomnosti, ktoré Leray odmietol publikovať, obsahovali veľmi zvláštny subjekt, ktorého technický prístup sa javí byť dokonale pochopený a zvládnutý iba špecialistami v topografickej geografii, alebo zopár astronómami, či optikmi: "Deviácie optických pohľadov a zameriavacej techniky." 
 Allais nám hovorí, že to bola experimentálna práca Ernsta Esclangona a rovnako aj Millera, ktorá ho priviedla k záveru, že vesmír je opticky asymetrický. Tá prvá publikovaná v roku 1928 obsahuje 40 tisíc meraní urobených v sérii 150 pozorovaní, počas dňa aj noci. Experiment v observatóriu v Štrasburgu obsahoval: horizontálne natiahnutý drôt a za ním - vo vzdialenosti 1,5m - zrkadlo s jeho odrazeným obrazom. To všetko bolo zamerané a zoradené v línii a cez teleskop sa pozorovali pohyby zrkadleného obrazu drôtu. Táto séria experimentov bola najskôr prevedená v severovýchodnom a potom aj v severozápadnom smere. V oboch smeroch bola z údajov objavená pravidelná zmena, ktorá závisela na priamom čase dňa a ktorá zodpovedala sínusoidovej  fluktuácii s periódou hviezdneho dňa. Na základe týchto pozorovaní by bol záver taký, že vesmír je optický anizotropický. 
  
 Eperimenty Dytona Millera 
  
  
   
 Millerove interferometrické experimenty vychádzali z rovnakej filozofie. Boli uskutočnené medzi rokmi 1925-1926 na Mount Wilson Observatory v Kalifornii. Avšak na rozdiel od predošlých experimentov (napr. MM) prevedených na Mt. Wilson - a na tomto bode zvlášť nalieha- tu boli pozorovania prevedené v úplne nepretržitom slede ku všetkým azimutom a každú chvíľu dňa i noci v priebehu dlhého časového obdobia. Preto považuje tieto experimenty za viac rozhodujúce, ako tie s krátkym trvaním, ktoré boli použité na potvrdenie izotropického šírenia svetla. Allais sa potom venuje škrupulóznej štúdii výsledkov, ktoré mu umožnili rozpoznať periódy nezaznamenané Millerom. Vrátane týchto analýz Allais zaznamenáva úžasný súlad, ktorý existuje medzi pozorovaniami jeho kyvadla, optické pozorovania s pohľadmi z teleskopu, Esclangonove pozorovania a Millerove interferometrické pozorovania - súlad, ktorého hlavnou črtou je existencia veľmi silného vzájomného vzťahu s pozíciou Zeme na jej obežnej dráhe. 
  
  
 Vesmír by sme tak museli považovať za vystavaný na anizotropii spôsobenej hviezdami a slnečnou sústavou, s výsledkom, že rýchlosť svetla by nebola vo všetkých smeroch rovnaká, v protiklade s tým čomu sa dnes verí. Zaznamenané hodnoty boli rádovo 10 na -5, čo je zhruba 8 km/s . Môžeme pripomenúť, že je to v rámci odchýlky v meraní rýchlosti svetla predpovedanou napr. A. Kastlerom. Ale namiesto toho, aby sme sa snažili dokázať s absolútnou istotou(bez ďalšieho experimentovania),že Millerove výsledky sú skutočné, by sme mohli zvažovať absolútne nulové výsledky požadované Einsteinovou teóriou- to je určite omnoho náročnejšie preukázať s istotou. 
 "Nepresnosť"  v súvislosti s Millerovými experimentami vôbec nezmenšuje ich dôležitosť, práve naopak. Experimentálne zistenie veľmi malých odchýlok od očakávaných výsledkov je skutočným srdcom vedy a základom jej pokroku. Keplerove určenie veľmi nepatrných odchýlok obežnej dráhy Zeme od dokonalého kruhu sú ukážkovým príkladom. Štatistická analýza dát Tychona Brahea v kombinácii so zvažovanými účinkami príšerne studených zimných nocí, ktoré vládli na ostrove Hven v Dánsku, na jeho kovové nástroje môžu poskytnúť vhodné zázemie, aby sme ignorovali maličké uhlové odchýlky, na ktorých stojí celá Keplerova astronómia. V skratke, experimentálne základy pre Keplerovu astronómiu neboli platné (v čase keď ju rozvinul) podľa štandardov, ktoré by mnohé vedecké autority chceli aplikovať dnes! 
 Záver 
 Súhrnne: Optické pozorovania Esclangona predstavujú periódu  hviezdneho dňa , štvrťročnú periódu ktorej fáza je blízka jarnej  rovnodennosti. Azimuty a rýchlosti interferometrie Millera predstavujú  periódu hviezdneho dňa rovnako ako u Esclangona, tiež sú  charakterizované štvrťročnými a ročnými periódami, ktorých fázy sú  blízke jarnej rovnodennosti. 
 Pozorovanie kyvadla s izotropickou aj anizotropickou podperou majú 24  hodinové periódy s priamou viazanosťou na relatívne pohyby Slnka  a Mesiaca vo vzťahu k dennej rotácii Zeme. Rovnako predstavujú mesačné  periódy Mesiaca vzhľadom ku hviezdam. Priemerný mesačný azimut  parakónického kyvadla s anizotropickou podperou prejavuje rovnako  štvrťročné periódy vzhľadom na pozíciu Zeme na orbite. Tiež predstavujú  periódu v rade 5,9 rokov vo vzťahu k planetárnym pohybom.(p.504) 
 V IRSID ( Inštitút pre Výskum Železa a Ocele) bolo pozorované, že pre  mesačnú vlnu 24h 50min je amplitúda sínusoidy (vytvorená spojením  uhlových odchýlok)  v rade 1", to jest 1,57 na -1O radiána. V každom  prípade, experimenty uskutočnené v IGN (Národný Geografický Inštitút) v   roku 1959 definitívne preukázali, že ani deformácia zeme, ani relatívny  pohyb pilierov nemohol objasniť uvedené efekty. Zmienené optické  anomálie sa nedajú vysvetliť inak ako vplyvom prenosového média medzi  prístrojmi, čiže anizotropiou vesmíru. Optické odchýlky tak ako pri  kyvadle majú 24-hodinové a mesačné periódy a tiež štvrťročné periódy  blízke jarnej rovnodennosti.(p.500)  Vzdialenosť teleskopu od pozorovaného  cieľa bola 8,30 metra a piliere na ktorých boli nástroje upevnené boli  azimutálne orientované , totiž pozorovania sa robili súčasne v smere  Sever-Juh a Juh-Sever. 
 Ak by sme súhlasili s autorom, tieto závery by viedli k návratu koncepcie prenosového média nazývaného éter Fresnela, Faradaya alebo Maxwella, ale s týmto rozdielom, že médium samotné treba považovať za schopné byť miestom relatívneho pohybu. Toto je zjavne tvrdenie, ktoré sa stavia úplne proti vládnucemu konceptu bežnej fyziky. Predsa, ak by sa aj Maurice Allais mýlil, čo si zasluhuje diskusiu, pretože si to vyžaduje experimentálne fakty. Allaisova kniha je nesmierne zaujímavá, už len pre zdravie autorovej vzdelanosti v odbore. 
 Allaisove kyvadlové experimenty mu zabezpečili v roku1959 Galabert Prize of the French Astronautical Society  (Galabertovu Cenu  Francúzskej Astronautickej Spoločnosti ) a ten istý rok sa stal  aj laureátom the United States Gravity Research Foundation ( Nadácie pre Výskum Gravitácie v Spojených Štátoch). 
   
   
  
   
   
   
 Zoznam použitej literatúry pre všetky tri časti je v tabuľke, ktorá je zároveň aj zoznamom gravitačných anomálii (prípadne teoretických prác) pozorovaných v priebehu zatmení Slnka a Mesiaca, príp. konjukcí a opozícií planét (geo aj heliocentrických) 
 Na tento článok boli použité osobitne tieto zdroje: 
 http://www.animations.physics.unsw.edu.au/jw/foucault_pendulum.html  
 http://en.wikipedia.org/wiki/Foucault_pendulum 
    
 http://home.t01.itscom.net/allais/blackprior/latham/latham-rep21.pdf 
   
 http://allais.maurice.free.fr/English/media11-1.htm 
   
   
 http://docs.google.com/viewer?a=v&amp;q=cache:PtVdR_Bv2zEJ:www.conspiracyoflight.com/Ernest%2520Esclangon%2520-%2520On%2520the%2520mechanical%2520and%2520optical%2520dissymmetry%2520of%2520space%2520in%2520connection%2520with%2520the%2520absolute%2520movement%2520of%2520the%2520Earth.pdf+ASTRONOMIE+PHYSIQUE.+-+Sur+la+dissym%C3%A9trie+m%C3%A9chanique+et+optique+de+l%27espace&amp;hl=sk&amp;gl=sk&amp;pid=bl&amp;srcid=ADGEESh9L20nb-gbt79jBEA-Z-SyvoClpoFS1O0DaLd5DiAAtXOBuETREJ-2_qyACsfh-6sT57LDB6s5SXHF8vVk3k_qKES3Ct0UTU-kmjuS1Kx1fTQFktz3sMB2re0zV9IcXMpDFk8r&amp;sig=AHIEtbT79pP9JM9JUFz3HGKIcp-yxTxxiA 
   
 Stephen Jay Gould - Dinosauři v kupce sena, str.83 (80-200-1338-5) 
   
 E-experiment / T- teoretická analýza 




 Dátum a 
 Typ zatmenia 


 E/  T  


 Pozorovanie   anomálnych gravitačných účinkov zatmenia 




 1954 (30.  jún) a 
 1959 (22.  okt. ) 
   
 Obe boli  čiastočné 


 E.  


 potvrdené 
 M.F.C. Allais, "Mouvement du pendule paraconique et éclipse   totale de Soleil du   30 juin 1954", C.R. French Academy of Science 245 (1957) 2001; 
 "Should the Laws of Gravitation be reconsidered?" Aero/Space   Engineering, Sept.  and Oct.   1959;  "L'anisotropie de l'espace" (Clément Juglar,   Paris, 1997).;   French   Academy of Sciences: C.R.A.S. (1959) 245, 1875; 245, 2001; 244, 2469; 245,   2467;245;2170.  See also   Autobiography of Maurice Allais -   Copyright ©1999   The Nobel Foundation.  Review: G.T. Gillies, "a   gravitational anisotropy at the   level of 5 micro-G", American J. of Physics (58, 530, 1990);  M.   Allais,   Aero/Space Engineering, Sept. 1959, p. 46-52; Aero/Space Engineering, Oct.   1959, p. 51-55; Aero/Space Engineering, Nov. 1959, p. 55; C.R.A.S. (Fr.),   247,1958, p. 1428; ibid, p. 2284; C.R.A.S. (Fr.), 248,1959, p. 764; ibid, p.   359 
 http://www.allais.info/allaisdox.htm,     http://allais.maurice.free.fr/English/Science.htm 
 http://www.flyingkettle.com/allais/eclipses.htm 




 1954 (30.  jún) 


 E 


 nepotvrdené-prístroje   v nepracovali správne 
 R. Tomaschek, Tidal gravity measurements in the Shetlands, Nature 175   (1955)   937 
 http://home.t01.itscom.net/allais/blackprior/tomaschek/tomaschek-1955.pdf 




 1961 (15.  feb.) 


 E 


 potvrdené 
 G.T. Jeverdan, et al, 1961,  Science et Foi 2, 24(1991)/An.Univ.Iasi   7,457 (1961)   www.allais.info/priorartdocs/jeverdan.htm, 
 http://www.science-frontiers.com/sf074/sf074a05.htm 




 1961 (15.  feb.) 


 E 


 nepotvrdené  
 http://www.springerlink.com/content/weq57ybugxb20j2w/ 
 nejasné-ale zrejme potvrdené 
 http://www.ias.ac.in/jaa/dec2006/JAA431.pdf 
   
 nejasné-zmiešané výsledky 
 http://www.springerlink.com/content/n86m4253h0871v37/fulltext.pdf?page=1 




 1970 (7.  marec) 


 E 


 potvrdené 
 E.J. Saxl and M. Allen, "1970 Solar Eclipse as 'Seen' by a Torsion   Pendulum",   Phys. Rev. D 3 (1971) 823-825. http://link.aps.org/abstract/PRD/v3/p823 
 http://prd.aps.org/abstract/PRD/v3/i4/p823_1 
 http://home.t01.itscom.net/allais/blackprior/saxlallen/eclipse/saphysrev.pdf 




 1974 (20.  jún) 
 1980 


 E 
 T 


 potvrdené   (samotný gyroskop nebol ovplyvnený, ale Talyvelov elektronický   snímač zaznamenal nečakaný bočný pohyb)   http://www.allais.info/priorartdocs/latham.htm 
   
 R. Latham, An Interim Report on a repeat of the Allais Experiment - the   measurement of the rate of increase of the minor axis of a Foucault pendulum   -   using automatic apparatus, I.C. Report G28, Imperial College, London, January     1980,   http://www.allais.info/priorartdocs/latham.htm 




 1981 (15.  feb.) 
 Resp. 1980(16.  feb) ak sa  ráta rok  0.po Kr. 


 E 


 potvrdené 
 G.T. Jeverdan, G.I. Rusu, and V. Antonesco, 
 Jassy University, Rumania "Experiments using the Foucault Pendulum   during  
 the Solar Eclipse of 15 February, 1981" (February 16, 1980 -   including 0 A.D) 
 http://xoomer.virgilio.it/iovane/mails.htm#s29 




 1985 


 T 


 W. Rabbel and J. Zschau, Static deformations and gravity changes at the   Earth's   surface due to atmospheric loading, J. Geophys. 56 (1985) 81. 
 http://adsabs.harvard.edu/abs/1985JGZG...56...81R 




 1987(23.  sept.) a  1988  (18. mar.)    a  1990 (22.  júl.) 


 E 


 potvrdené 
 Zhou, S. W.; Huang, B. J., "Abnormalities of the time comparisons of   atomic   clocks during the solar eclipses", Nuovo Cimento C, vol. 15 C, no. 2,   Mar.-Apr.   1992, p. 133-137.] "Time comparisons of two atomic clocks were made   during the  solar eclipses of September 23, 1987, March 18, 1988, and July 22, 1990   http://home.t01.itscom.net/allais/blackprior/zhou/zhou-1.pdf 




 1990 (22.  júl ) 


 E 


 nepotvrdené 
 Ullakko, K..,  Liu, Y.,  Xie, Z., The 1990 solar eclipse as seen   by a torsion   pendulum,   Proceedings of the 25th Annual Conference of the   Finnish Physical   Society 1 p (SEE N92-10362 01-70), 1991] 
 http://adsabs.harvard.edu/cgi-bin/nph-  bib_query?bibcode=1991fnps.conf.....U&amp;db_key=AST&amp;high=37692492d311278 
   [T. Kuusela, Effect of the solar eclipse on the period of a torsion pendulum,   Phys.   Rev. D 43 (1991) 204,   http://link.aps.org/abstract/PRD/v43/p2041; 
 potvrdené,   ale nepriaznivá interpretácia  
 Luo Jun, L. Jianguo, Z. Xuerong, V. Liakhovets, M. Lomonosov and A. Ragyn,     Observation of 1990 solar eclipse by a torsion pendulum, Phys. Rev. D 44   (1991)   2611,   http://prola.aps.org/abstract/PRD/v44/i8/p2611_1 




 1991 (11.  júl) 
 úplné 


 E.  


 nejasné -potvrdená detekcia, ale nepriaznivá   interpretácia 
 B. Ducarme, H.-P. Sun, N. d'Oreye, M. van Ruymbeke and J. Mena Jara,   Interpretation of the tidal residuals during the 11 July 1991 total solar   eclipse, J. of  Geodesy 73 (1999) 53.  http://www.springerlink.com/openurl.asp?genre=article&amp;id=doi:10.1007/s0019000  50218 ]    T. Kuusela, New measurements with a torsion pendulum during the solar   eclipse,   Gen. Relativ. And Gravitation 24 (1992) 543.   http://www.springerlink.com/content/g58x6k1463032277/ 
     SPECIAL DEVICE FOR DETECTION OF GRAVITY 
 EFFECTS DURING ECLIPSES 
 L. Savrov (SAI,MSU) 
 http://home.t01.itscom.net/allais/blackprior/savrov/savnote.pdf 
 http://www.allais.info/priorartdocs/savrov.htm 




 1994 (3.   nov.) 
 úplné 


 E. 


 nejasné- anomálie boli zrejme v rámci chýb merania a prístroja 
 L.A. Savrov, Experiment with paraconic pendulums during the November 3,   1994  solar eclipse in Brazil, Measurement Techniques 40 (1997) 511.  http://www.springeronline.com/sgw/cda/frontpage/0,11855,5-136-70-35743976-  0,00.html 




 1995 (24.  okt.) 


 E 


 potvrdené 
 D.C. Mishra and M.B.S. Rao, Temporal variations in gravity field during   solar   eclipse on 24 October, Current Science 72 (1997) 783.  http://science.nasa.gov/newhome/headlines/Eclipse_Mishra.html   http://www.ias.ac.in/j_archive/currsci/72/vol72contents.html 
 see also: http://www.agu.org/pubs/crossref/1998/98GL01781.shtml 




 1995 (24.  okt.) 


 E 


 potvrdené 
 Zhou, S. W "Abnormal Physical Phenomena Observed when the Sun,   Moon,  and Earth are Aligned" 21st CENTURY, Fall 1999, p 55-61 
 http://home.t01.itscom.net/allais/blackprior/zhou/zhou-3.pdf 




 1997 (9.  mar.) 
 úplné 


 E. a 
 T.  


 potvrdené 
 X.-S. Yang and Q.S. Wang Gravity anomaly during the Mohe total solar   eclipse   and new constraint on gravitational shielding parameter, Astrophys. Space   Sci. 282  (2002) 245. http://home.t01.itscom.net/allais/blackprior/wang/yangwang.pdf 
 Q.-S. Wang, X.-S.Yang, C.-Z. Wu, H.-G. Guo, H.-C. Liu and C.-C. Hua 
 Precise measurement of gravity variations during a total solar eclipse,   Phys. Rev.   D 62 (2000) 041101  http://prd.aps.org/abstract/PRD/v62/i4/e041101 
 http://home.t01.itscom.net/allais/blackprior/wang/wangetal.pdf    nepriaznivý postoj: C.S. Unnikrishnan, A.K. Mohapatra and G.T.   Gillies,   Anomalous gravity data during the 1997 total solar eclipse do not support the     hypothesis of gravitational shielding, Phys. Rev. D 63 (2001) 062002  http://link.aps.org/abstract/PRD/v63/e062002 ] 




 1999 (11.  aug.) 
 úplné 


 E. 


 Neukončené !!! http://science.nasa.gov/newhome/headlines/ast06aug99_1.htm   http://science.nasa.gov/science-news/science-at-nasa/1999/ast12oct99_1/ 
 http://spacescience.spaceref.com/newhome/headlines/ast06aug99_1.htm 
 http://www.allais.info/priorartdocs/noever.htm,     http://home.t01.itscom.net/allais/blackprior/noever/noeverpaper.doc 
 http://eclipse.gsfc.nasa.gov/eclipse.html 
   
 http://www.welt.de/print-welt/article579881/Mysterioese_Pendelversuche.html 
 http://news.bbc.co.uk/2/hi/sci/tech/specials/total_eclipse/415601.stm 
 http://www.sciencemag.org/cgi/content/summary/285/5424/39b 
   
 http://www.cybercitycafe.com/explore/gravity.html 




 1999 (11.  aug.) 
 úplné 


 E. 


 potvrdené,   ale nepriaznivá interpretácia (obe) 
 http://home.t01.itscom.net/allais/blackprior/vienna/wuchterl.htm 
 http://home.t01.itscom.net/allais/blackprior/vienna/wienpendecl.htm 
 http://science.nasa.gov/science-news/science-at-nasa/1999/ast12oct99_1/ 
 http://home.t01.itscom.net/allais/blackprior/vienna/wienpendecl.htm 
 http://www.youtube.com/watch?v=9FuDKfOmLU0 (4:04) 




 1999 
 (11. aug.) 
 úplné 


 E 


 potvrdené 
 Antonio Iovane: 
 http://xoomer.virgilio.it/iovane/xtrieste.htm 
 http://xoomer.virgilio.it/antiovan/ 
 http://allais.maurice.free.fr/English/media18-5.htm (AUGUST´99 ECLIPSE   STUDIES SHOW THE „ALLAIS EFFECT" IS REAL !  
 By Henry Aujard , 21st CENTURY Summer 2001) 
 http://xoomer.virgilio.it/iovane/xtrieste.htm 




 1999 
 (11. aug.) 
 úplné 


 E 


 potvrdené   
 Ieronim MIHĂILĂ , Nicolae MARCOV , Varujan PAMBUCCIAN , Maricel  AGOP (THE PUBLISHING HOUSE PROCEEDINGS OF THE ROMANIAN  ACADEMY, Series A, 
 OF THE ROMANIAN ACADEMY Volume 4 , Number 1 /2003, pp.  ) 
 http://home.t01.itscom.net/allais/blackprior/mihaila/1999/mihaila1999.pdf 




 1999 
 (11. aug.) 
 úplné 


 E 


 potvrdené 
 Annual of the National Museum of Bucovina for 2000-2001-2002 
 http://home.t01.itscom.net/allais/whiteprior/olenici/olenici2001.pdf 




 1999 
 (11. aug.) 
 úplné 


 E 


 nejednoznačné- nepotvredené vysoké hodnoty   z predošlých pozorovaní 
 Thomas Udem, Jörg Reichert, Ronald Holzwarth, and Theodor Hänsch 
 Max-Planck-Institut für Quantenoptik (MPQ) , Laser Spectroscopy Division  
 On the Behaviour of Atomic Clocks during the 1999 Solar Eclipse over   Central Europe http://www.mpq.mpg.de/~haensch/eclipse/full.html 




 1995 (24.  okt.) 
 1999 
 (11. aug.) 
 úplné 


 E 
 T 


 Rôzne experimenty v súvislosti s atmosférou, ionosférou,   magnetickým poľom  Zeme a pod.:   http://www.sciencedirect.com/science?_ob=ArticleURL&amp;_udi=B6V3S-  3YCDKN1-  1N&amp;_user=10&amp;_coverDate=12%2F31%2F1999&amp;_rdoc=1&amp;_fmt=high&amp;_orig=sea  rch&amp;_sort=d&amp;_docanchor=&amp;view=c&amp;_acct=C000050221&amp;_version=1&amp;_urlVersi  on=0&amp;_userid=10&amp;md5=b7883d94e2fd13e18a9c2c19b0a288b2 
 http://www.springerlink.com/content/jv8310013136h051/ 
 http://www.sciencedirect.com/science?_ob=ArticleURL&amp;_udi=B6VHB-  48JK78M-  3&amp;_user=10&amp;_coverDate=04%2F30%2F2003&amp;_rdoc=1&amp;_fmt=high&amp;_orig=searc  h&amp;_sort=d&amp;_docanchor=&amp;view=c&amp;_acct=C000050221&amp;_version=1&amp;_urlVersio  n=0&amp;_userid=10&amp;md5=051c31960567810e6900e5bf0c1ba00d 
 http://www.sciencedirect.com/science?_ob=ArticleURL&amp;_udi=B6VHB-  4CCP0H8-  2&amp;_user=10&amp;_coverDate=07%2F31%2F2004&amp;_rdoc=1&amp;_fmt=high&amp;_orig=searc    h&amp;_sort=d&amp;_docanchor=&amp;view=c&amp;_acct=C000050221&amp;_version=1&amp;_urlVersio    n=0&amp;_userid=10&amp;md5=64199495cbd72a593d943741aa9e47ea 
 http://www.springerlink.com/content/5767l152562h17p1/ 
 http://www.terrapub.co.jp/journals/EPS/pdf/2007/5901/59010059.pdf 
 http://ieeexplore.ieee.org/Xplore/login.jsp?reload=true&amp;url=http%3A%2F%2Fieee  xplore.ieee.org%2Fiel5%2F7086%2F19096%2F00882753.pdf%3Farnumber%3D  882753&amp;authDecision=-201 




 1999 
 (11. aug.) 
 úplné 



 celkovo nepotvrdené, pri jednom gravimetri v   Bondy station potvrdené     Search for the Gravitational Absorption Effect Using Spring and Super-  conducting Gravimeters during the Total Solar Eclipse of August 11, 1999   Michel van Ruymbeke 1) ,Liu Shaoming 2) ,Lalu Mansinha 3) &amp; Bruno   Meurers4)http://www.observatoire.be/ICET/bim/bim138/vanruymbeke2.htm   http://www.ta3.sk/caosp/Eedition/FullTexts/vol28no3/pp251-255.pdf   http://sciencelinks.jp/j-east/article/200114/000020011401A0565442.php  




 2001 (21  . jún) 
 úplné 
   


 E.  a 
   
   
   
   
 T. 


 potvrdené. 
 K. Tang, Q. Wang, H. Zhang, C. Hua, F. Peng and K. Hu 
 Gravity effects of solar eclipse and inducted gravitational field,   Am. Geophys.   Union, Fall Meeting 2003, Abstract #G32A-0735  (Eos Trans. AGU, 84(46),   Fall   Meet. Suppl., Abstract G32A-0735, 2003) http://www.spacedaily.com/news/china-  01zi.html 
 J. B. Almeida: A theory of mass and gravity in 4-dimensional optics,   http://arxiv.org/abs/physics/0109027 ,   http://arxiv.org/PS_cache/physics/pdf/0109/0109027v2.pdf 




 2002 (4.   dec.)   úplné 


 E. 


 potvrdené. 
 K. Tang, Q. Wang, H. Zhang, C. Hua, F. Peng and K. Hu Gravity effects   of solar  eclipse and inducted gravitational field, Am. Geophys. Union, Fall   Meeting   2003, Abstract #G32A-0735  (Eos Trans. AGU, 84(46), Fall Meet. Suppl.,   Abstract G32A-0735, 2003) http://www.agu.org/cgi-  bin/SFgate/SFgate?language=English&amp;verbose=0&amp;listenv=table&amp;application=fm0  3&amp;convert=&amp;converthl=&amp;refinequery=&amp;formintern=&amp;formextern=&amp;transquery=k  eyun%20tang&amp;_lines=&amp;multiple=0&amp;descriptor=%2fdata%2fepubs%2fwais%2fin  dexes%2ffm03%2ffm03|544|4238|Gravity%20Effects%20of%20Solar%20Eclipse  %20and%20Inducted%20Gravitational%20Field|HTML|localhost:0|%2fdata%2fep  ubs%2fwais%2findexes%2ffm03%2ffm03|9169562%209173800%20%2fdata2%2  fepubs%2fwais%2fdata%2ffm03%2ffm03.txt 




 2003 


 T. 


 nepriaznivý postoj. 
 X.-S. Yang; 
 http://prd.aps.org/abstract/PRD/v67/i2/e022002 




 2003 
 (31. máj) 


 E 


 potvrdené 
 Ieronim MIHĂILĂ, Nicolae MARCOV, Varujan PAMBUCCIAN, Ovidiu   RACOVEANU 
 (THE PUBLISHING HOUSE PROCEEDINGS OF THE ROMANIAN   ACADEMY, Series A, 
 OF THE ROMANIAN ACADEMY Volume 5, Number 3/2004, pp. 000-000) 
 http://home.t01.itscom.net/allais/blackprior/mihaila/2003/mihaila2003.pdf 




 2003 
 (31. máj) 


 E 


 potvrdené 
 Dimitrie Olenici, Ştefan Bogdan Olenici 
 Studies upon the effects of planetary alignments performed using 
 a romanian style-paraconical pendulum  at Suceava Planetarium 
 from August 2002 to AUGUST  2003, ANUARUL COMPLEXULUI MUZEAL  BUCOVINA XXIX-XXX  vol.II   ,  2002-2003, Suceava ,2004 
 http://home.t01.itscom.net/allais/whiteprior/olenici/art2004.doc 




 2004 (14.  okt.) 
 Slnka 
 (28. okt.)  Mesiaca 


 E 


 potvrdené 
 Dimitrie Olenici, Suceava Planetarium, Romania 
 http://home.t01.itscom.net/allais/whiteprior/olenici/kuchrep-1.doc 
   
 potvrdené  
 http://home.t01.itscom.net/allais/whiteprior/olenici/kuchrep-1.doc 




 2004 


 T 


 Chris Duif   A review of conventional explanations of   anomalous 
 observations during solar eclipses http://arxiv.org/ftp/gr-  qc/papers/0408/0408023.pdf 
 http://www.allesoversterrenkunde.nl/content.shtml?http://www.allesoversterr  enkunde.nl/cgi-bin/scripts/db.cgi?ID=263&amp;view_records=1&amp;ww=on 




 2005 (8.   apr.) 
   
   
   
   
   
 (3.okt) 


 E 


 nejasné, nepotvrdené v súvistlosti so zatmením, ale iné zvláštnosti   v správaní   kyvadiel boli zaznamenané  Goodey's team - http://www.allais.info/panarep/panawork.htm 
   
   
   
   
 výsledky sú nedostupné ! http://www.arauto.uminho.pt/pessoas/bda/ 
 experiment sa iba spomína, str.32: http://www2.fisica.uminho.pt/RelDF_2005.pdf 




 2005(8.   apr.) 
   
 Slnka 
   
 (24. apr.) 
 Mesiaca 


 E 


 potvrdené   
 Prof. Dimitrie Olenici 
 (Preliminary report about pendulum experiments made  in Romania   during   solar eclipse from 8 April 2005 and lunar eclipse from 24 April 2005)   home.t01.itscom.net/allais/whiteprior/olenici/suceavarep-a.doc 




 2005 
 (3. okt.) 


 E 


 potvrdené 
 Ieronim MIHĂILĂ, Nicolae MARCOV, Varujan PAMBUCCIAN 
 (THE PUBLISHING HOUSE PROCEEDINGS OF THE ROMANIAN   ACADEMY, Series A, 
 OF THE ROMANIAN ACADEMY Volume 7, Number 2/2006, pp. 000-000) 
 http://home.t01.itscom.net/allais/blackprior/mihaila/2005/mihaila2005.pdf 




 2006 
 (29. mar.) 
 úplné 


 E a T 


 potvrdené,   ale relatívne malé hodnoty:  -Kuusela T, Jäykkä J, Kiukas J, Multamäki T, Ropo M and Vilja I. „Gravitation     experiments during the total solar eclipse", Phys. Rev. D 74, 122004 1-8   (2006)   GRAVITATION ANOMALIES DURING SOLAR ECLIPSE   http://users.utu.fi/kuusela/gravity/Report.pdf 
 http://users.utu.fi/kuusela/gravity/overview.html 




 2006  
 (29. marec) 


 E 


 potvrdené obdobné anomálie 
 1National Observatory of Athens, V. Pavlou and I. Metaxa, P. Penteli,   15236,   Athens, Greece  2Laboratory of Atmospheric Physics, Physics Department,   Aristotle University of Thessaloniki, Thessaloniki, Greece 3Institute of   Oceanography, Hellenic Center for Marine Research, Athens, Greece    4Laboratory  of Agronomy, Faculty of Plant Production, Agricultural University of Athens,   Athens Greece  5University of Crete, Chemistry Department, Environmental   and   Chemical Processes Laboratory, Crete, Greece 
 6Department of Meteorology and Climatology, Aristotle University of   Thessaloniki, Thessaloniki, Greece 
 http://www.atmos-chem-phys.net/8/5205/2008/acp-8-5205-2008.pdf 
   
 Ionospheric response to the partial solar eclipse of March 29, 2006,   according to   the observations at Nizhni Novgorod and Murmansk 
 http://www.springerlink.com/content/b62337461qpv6268/ 




 2006 (22.  sept.) 


 E a T 


 potvrdené 
 V. A. Popescu 1, D. Olenici 2 
 (A confirmation of the Allais and Jeverdan-Rusu-Antonescu effects   during   the solar eclipse from 22 September 2006 , and the quantization of behaviour   of pendulum - Proceedings of the 7th European meeting of the Society for   Scientific 
 Exploration (2007)) 




 2006 (22.  sept.) 


 E 


 nejasné (potvrdená detekcia, ale neskôr   nepriaznivá interpretácia dát):  
 (Institut für Gravitationsforschung Newsletter 29.Sep. 2006) 
 http://www.gravitation.org/E_Pressetext_Allais.pdf   http://www.gravitation.org/institute_of_gravity_research/Experiments/experiments  .html (The Paraconical Pendulum (Allais-Effect) reconsidered 
 T.. Heck, E. Zentgraf, T. Senkel, T. Junker, L. Lemons 
 Göde Wissenschaftsstiftung - IGF, Am Heerbach 5, 63857 Waldaschaff) 
 video:   http://video.google.com/videoplay?docid=6502448681363777247&amp;ei=Ka76SZH_  KIn4-gGkrp2gCg&amp;q=wachsende+erde&amp;hl=en#,   http://www.youtube.com/watch?v=9FuDKfOmLU0 (4:48) 




 2007  


 T 


 Allais Effect and TGD 
 M. Pitkänen1, August 1, 2007 




 2008 


 T 


 Concerning the Allais effect and Majorana by Miles Mathis 
 http://www.wbabin.net/mathis/mathis37.pdf 
 http://www.milesmathis.com/allais.html  




 2008 (1.   aug.) 


 E 


 potvrdené 
 CORRELATED ANOMALOUS EFFECTS OBSERVED DURING A SOLAR   ECLIPSE 
 T.J. Goodey A.F. Pugach D. Olenici 
 http://www.allais.info/docs/pugarticle.pdf 




 2009(22.   júl.) 


 E 


 potvrdené ?! 
 http://www.newscientist.com/article/mg20327183.800-eclipse-sparks-hunt-for-  gravity-oddity.html , http://www.newscientist.com/article/dn17481-july-eclipse-is-  best-chance-to-look-for-gravity-anomaly.html   http://www.spacedaily.com/news/china-01zi.html   http://english.cas.cn/Ne/CASE/200907/t20090720_44683.shtml 
 zemetrasenia:http://www.rqm.ch/earthquake_warnings_with_magnitu1.htm,     http://www.torquepowered.com/community/blog/view/15946/3 
   
 nepotvrdené Thomas Goodey, http://www.youtube.com/watch?v=Gh67c5k1QJE 




 2010 (10.  jan.) 


 E 






 neukončené: 
 Allais Effect January 2010 Annular Eclipse 




 Eclipse Chasing with a Pendulum  -Thomas Goodey reports from the Maldives 




 http://www.eclipse-chasers.com/ase2010Allais.html 





 E,T 


 Zaujimavosti: 
 Experimental   evidence that the gravitational constant varies 
 with   orientation, http://arxiv.org/ftp/physics/papers/0202/0202058.pdf 
 http://dspace.anu.edu.au/bitstream/1885/41361/1/Remarks_to_solve.pdf 
 anomálie v dráhe prvých satelitov na orbite: 
 http://www.enterprisemission.com/Von_Braun.htm 
   




   
 Tabuľka je zostavená  podľa vzoru:  http://xavieramador2.50webs.com/anomalies.htm#12 
   
   
   
   
   
   
   
   

