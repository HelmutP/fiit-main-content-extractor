
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magdaléna Stýblová
                                        &gt;
                Nezaradené
                     
                 Prvý dojem? 

        
            
                                    3.5.2010
            o
            19:01
                        (upravené
                3.5.2010
                o
                20:00)
                        |
            Karma článku:
                3.56
            |
            Prečítané 
            658-krát
                    
         
     
         
             

                 
                    Prajeme Vám hlboký umelecký zážitok...
                 

                 Čo je vlastne ten umelecký zážitok? Okrem známeho faktu, že je to výsostne subjektívny pocit, musí mať nejaké spoločné črty?. Čo ho spôsobuje? Kto? Tieto otázky si môžme klásť pri každom jednom koncerte, či inom kultúrnom podujatí.   V minulom blogu som písala o koncerte festivalu Ars Organi, ktorý prebieha práve v Nitre, o organistovi z USA Davidovi Di Fiore.  Túto nedeľu sa nám predstavil iný interpret, v iných priestoroch a na inom organe. Koncert sa konal v Nitre v Evanjelickom kostole Sv. Ducha. Táto moderná sakrálna stavba má svoje plusy i mínusy. Pre publikum bolo zaujímavé vidieť organ "naživo", ked'že nie je umiestený na tzv. chóre, ale zarovno s publikom. Tento fakt v praxi až vyrážal dych, ked' sme s napätím sledovali hru nôh, rúk, registrovanie ...Neprítomná "prirodzená" akustika priestorov, však zohrávala skôr negatívnu zložku. Prečo potom tento kostol? Pretože ovplýva novým a kvalitným nástrojom a bola by škoda nevyužiť ho. Zároveň je to pre interpretov výzva, na tomto organe počuť každú nečistotu, každú chybu, každé zaváhanie. Organista tak môže decentne využiť svoj potenciál.   Predstavil sa nám mladý organista z Mad'arska, István Mátyás. Tomuto mladému umelcovi by človek na prvý pohľad tipol 18 rokov a žiadne skúsenosti. No v skutočnosti sa tento 30 ročný muž toho nahral viac než dosť, či už to bola koncertná činnosť, vydávanie CD alebo súťaže. Koncert začal s dielom J.S. Bacha, Prelúdium a fúga e mol (BWV 548). Veľmi čistá prezentácia tohto barokového velikána musela zaujať pozornosť každého. István preznetoval aj mad'arského autora, D. Antalffy-Zsirossa nasledujúcou skladbou s názvom Minnesang. Takto sme sa dostali o  160rokov do roku 1916. Skladba veľmi príjemná, s veľmi príjemnou interpretáciou. No v programe d'alej zaznel "oslávenec" Robert Schumann s Fúgou B-A-C-H, op.60, č.2 a Tri štúdie v kánonickej forme, Op.56. Vypočuli sme si aj F.M. Bartoldyho či Liszta. Osobne ma zaujal výber skladby od spomínaného skladateľa F. Liszta, Ave Maria od Arcadelta. Nádherná skladba. Organista si dal záležať, vypracované detaily, nádherná farba registrov, veľmi jemná, čistá a priezračná hra. Výborná voľba. Koncert zakončil so skladbou D. Antalffy-Zsirossa, Festa bucolica.   Zaskočil ma fakt, že publikum bolo chladné. István nám síce neponúkol skladby založené len na efekte, čo strhne každého, dal prednosť skôr dielam s veľkou umeleckou hodnotou. Tu sa mi naskytá otázka umeleckého zážitku.  Minulotýždňový koncert bol skvelý, a strhol publikum. Tento koncert bol tiež skvelý, len možno iný a publikum ostalo chladné. Dramaturgia koncertu donútila poslucháča rozmýšlať, veľmi sa snažila zasiahnuť duše v najlhlbších miestach. Hľadala cestu až do transcendentálna. Prečo ostalo publikum chladné a odmenilo Istvána len potleskom? Obávam sa, že sa nechce zamýšlať a rozmýšlať, chce len prijať. Alebo je to inak? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Naivných. Koľko nás v skutočnosti je?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Kultúrne Slovensko časť 2. alebo ako sa mi rozum zastavil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            "Kultúrne Slovensko" časť 1.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Dovolenka inak
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magdaléna Stýblová 
                                        
                                            Zdravotníctvo šokuje
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magdaléna Stýblová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magdaléna Stýblová
            
         
        styblova.blog.sme.sk (rss)
         
                                     
     
        ponáhľajme sa milovať ľudí, pretože rýchlo odchádzajú...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    810
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Na malomeštiakovej svadbe v SND
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




