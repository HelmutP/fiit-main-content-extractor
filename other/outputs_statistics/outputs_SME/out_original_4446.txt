
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matej Adámy
                                        &gt;
                Nezaradené
                     
                 Prebudenie treťotriednych poetov? Najlepšie post-mortem 

        
            
                                    12.4.2010
            o
            21:22
                        |
            Karma článku:
                6.22
            |
            Prečítané 
            1430-krát
                    
         
     
         
             

                 
                    Slovenskí novinári, ako aj tí zo zvyšku sveta, musia prestať používať slovné spojenie "druhá Katyň". Mohol by postačiť jeden miniatúrny dôvod: nič také neexistuje.
                 

                 Z ponúknutého zoznamu reakcií na haváriu lietadla s prezidentom Poľska a 95 spolucestujúcimi som si opakovane prečítal len jednu. Tú od Václava Havla. Povedal: "Kaczynského smrť ovplyvní dejiny Poľska. Nie preto, čo sa skutočne stalo, ale pre dohady, ktoré sa začnú šíriť." Zrejme si medzičasom stihol prečítať noviny, teda ich e-vydanie.   V jednom prúde dohadov je to tak (paradoxne, na rozdiel od tohto shitu): tragédie robia z novinárov poetov tretej kategórie, veľmi presvedčivo skladajúcich metafory a nachádzajúcich inotaje, akým vedúci vydania jednoducho nemôžu odolať. Druhá Katyň? Jasné! "Symbol utrpenia národa"? Sem s tým! Takto napríklad vyzerá „spravodajstvo“ z titulnej strany denníka SME. Nebojte sa, smútku je dosť na ďalších päť strán.   Poľsko teraz potrebuje jednu jedinú vec: pokojnú výmenu elít. Žiaľ nejako prekoná samo. Časom. Robí to človek jeden a rovnako to robí národ ako taký. V sobotu bude pohreb bývalého prezidenta, v nedeľu sa o tom bude hovoriť pri obede a v pondelok začne čas prirodzeného zotavovania. A minimálne dvaja ľudia sa budú čudovať, že nikto nie je s nikým spojený o nič viac (tu a tu).   Vtedy našťastie nebude dôležité, čo sa píše dnes. Prečo našťastie? Lebo si viem predstaviť (keď už) lepšiu "morálnu podporu" krajine, ako písať, že je prekliata, pričom len tak nepatrne naznačiť, že vďaka behu dejín ju niečo podobné postihne opäť (Zavedenie eura?). Aj priemerne inteligentný človek vie, že dve udalosti na jednom mieste v priebehu sedemdesiatich rokov majú spoločné len jedno: nič. A pýta sa len tak na okraj podotknúť, že ruské lietadlá nie sú za ten čas o nič kvalitnejšie.   Prečo to nevedia ľudia, ktorí podľa popisného slovíčka tvoria mienku? Možno to aj vedia, ale to by sa zrejme míňali s ich nadriadenými, ktorí mienku predávajú. V akýchkoľvek časoch. Áno, zvykli sme si na ohromneveľapalcové titulky, využívajúce silu slova. Aj na snímky na dotvorenie atmosféry. Dokonca aj na to, že text nemôže byť celkom sterilný, aj keď by mal.   Na druhej strane, žiadna reportáž o smrti desiatok ľudí Hviezdoslavov Kubín nevyhrala a  premýšľanie nad zmyslom tragédie malo zomrieť s antickými dramatikmi. Preto som napríklad začal sledovať dianie na Haiti až na tretí deň po vyčíňaní Matky prírody, veď je jasné, že naše malé denníčky na tvorenie mienky nie sú v žiadnom smere výnimkou.   Pointa však zostáva: nechajme mýty a emocionálne prežívanie na pospolitý ľud. Občas to dokonca potrebuje. Vo mne zostal pocit, že keď už aj titulná strana "seriózneho periodika" nestojí ani za jeden cent, nemám chuť zisťovať, ako je na tom jeho zvyšok. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (26)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Adámy 
                                        
                                            Mystika tragického klauna
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Adámy 
                                        
                                            Káva, reči, káva
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Adámy 
                                        
                                            Odchody spojov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Adámy 
                                        
                                            Hneď za potrebou prežiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matej Adámy 
                                        
                                            Sloboda a maskulinita
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matej Adámy
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matej Adámy
            
         
        adamy.blog.sme.sk (rss)
         
                                     
     
        Better to write for yourself and have no public, than to write for the public and have no self. Cyril Connolly
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    134
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1239
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Glosárium
                        
                     
                                     
                        
                            Čriepky ostrovnej poézie
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Ostatné
                        
                     
                                     
                        
                            Iné
                        
                     
                                     
                        
                            Stand-up
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




