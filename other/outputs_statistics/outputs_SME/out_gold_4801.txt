

   
 Pozor! Tento článok vám nepomôže, ak STE idiot. Varovné príznaky sú: 
   
   
 
 nikto vás neberie vážne, ani váš pes, 
 vyhrali ste "hlod kola" v relácií Aj múdry      schybí, 
 často počujete vetu "pripomínaš mi toho blbšieho z      filmu Blbý a blbší" 
 chodíte na rovnakú základnú školu ako váš syn 
 
   
   
 Ak teda STE idiot, netrápte sa, nie ste jediný na svete, je vás veľa, nemusíte sa báť vyhynutia. Ak ale idiot nie ste, no niekedy taký dojem vzbudíte (ruku na srdce, komu sa to ešte nestalo?), treba si v prvom rade uvedomiť úvodný výrok v širšich súvislostiach. Myšlienka nemusí byť vyslovene blbá, no nemusí sa ani práve niekomu páčiť a skončíte oveľa horšie ako keby ste držali jazyk za zubami... Obľúbené témy sú politika, náboženstvo, šport, muži alebo nebodaj téma ženy... 
   
 Modelové výroky:  
   
 "&lt;XY politická strana&gt; sú samí klamári a lumpi" (možno áno, ale každá strana má svojich fanúšikov, tak načo rozbúriť hádku, ktorú nemôžem vyhrať?) 
 "Vy muži ste všetci kkkkkkkk / myslíte len na xxxxxxx / a podobne..." (generalizovať je primitívne, ak máte aj v 90% prípadov pravdu, zbytočne urazíte 10% svetlých výnimiek) 
 "Wow to je samica, nebol by som proti, keby to jej tričko bolo priesvitné..." (aj keď si to možno pomyslí skoro každý chlap pri pohľade na sexi kočku vo vyzývavom oblečení, nie som si istý koľko z nich to povie v prítomnosti iných žien - bobrík odvahy ak nebodaj v prítomnosti svojej frajerky. A podobne... 
   
 Je jedno, či to poviete v skupine ľudí, ktorých názor na danú tému nepoznáte (alebo aj viete že by na vás pozerali ako na debila, ale aj tak si to neodpustíte), alebo si podobný kúsok dáte do statusu na facebooku, twitteri, či blogu - vyjdete z toho ako idiot. Každý z nás má na niečo vyhradený názor, každý z nás má svoj politický názor, náboženské vyznanie, názor na ľudí alebo vás občas napadne nejaká prasačina... Jedno je však isté - vykrikovaním týchto názorov do sveta akuráttak rozdelíte ľudí, ktorých vás poznajú, na dve skupiny - jedny s vami súhlasia a povedia si v duchu "hej má pravdu" ale to je všetko, pravdepodobne vás verejne v tomto názore nepodporia, lebo sa nechcú strápniť tak ako vy. Druhá skupina s Vami nesúhlasí, úplne ste ich nasrali alebo pobúrili a myslia si o vás, že ste idiot! Oplatilo sa vám to??? Jasné, že nie :). 
   
 Ako teda nevyzerať ako idiot? Zhrnul by som to do týchto rád: 
   
 Nehovorte nahlas názor, ktorý by mohol byť akokoľvek v rozpore so "všeobecným názorom" alebo by mohol byť natoľko vyhranený, že by niekomu mohlo pri jeho vstrebaní zabehnúť. Tiež sa neodporúča povedať, čo vám práve napadlo, hlavne niečo fakt nezmyselné, nevhodné, neetické, šovinistické alebo detinské - akonáhle to vyjde z vašich úst (alebo to niekde napíšete), už si o vás ľudia "myslia svoje"... 
   
 V tomto prípade platí, že menej je niekedy viac. Povedzte si to pri pive, kde je odľahčená atmosféra, kde na 100% poznáte názory parťákov na danú tému, pridajte do toho dávku humoru, hlavne sa uistite, že nehrozí, aby sa niekto urazil, zháčil, pobúril alebo sa na vás jednoducho nasral. 
   
 Ak sa vyjadreniu názoru nevyhnete, snažte sa byť diplomatický. 
   
 (príklad: niekto vo vašej firme predstavil projekt reklamnej kampane, ktorá má byť vyslovene kreatívna, inovatívna a moderná, no podľa vás je to čistý odpad. Reakcie: 
   
 a) "Toto je vtip alebo ste naozaj taký babrák? Môja 100ročná babka by to spravila modernejšie!" (- poviem narovinu, čo si myslím a ešte aj dotyčného urazím.) 
   
 b) "Vo mne to nevzbudilo pocit niečoho fresh" (- som čo najstručnejší, čiže nepoviem pred slovesom veľké dôrazné "ABSOLÚTNE" aby som zvýraznil aké je to hrozné, pričom poviem asertívne svoj názor, na ktorý sa ma pýtali.) 
   
 Je toľko spôsobov, ako vyjadriť to isté. Ktorý si vyberiete? Myslím, že to chápete, pravda? Ale pozor, tu riziko nekončí! Dávajte si pozor, aby ste hneď po porade nepovedali kolegovi niečo ako "to bol riadny odpad", lebo ten si o vás môže pomyslieť niečo ako "to je pokrytec" - pokiaľ neviete ako to berie hociktorá strana, nehovorte svoj názor, keď sa vás nepýtajú. Výnimkou je asi len keď vám niekto povie "povedz mi úplne narovinu čo si o tom myslíš" - vtedy by som bol otvorený, no tiež by som nechcel nikoho uraziť. Jednou zo všeobecných zásad komunikácie je pýtať sa ľudí na ICH názor namiesto prezentovania toho SVOJHO. Ľudia radšej rozprávajú o sebe ako počúvajú druhých a hlavne - pokiaľ sa vás nikto nič nepýta, skúste svoje ego a podrezaný jazyk trošku pribrzdiť - vyplatí sa to. 
   
 Kedysi som si myslel, že vlastnosť "čo na srdci, to na jazyku" je pomenovanie pre úprimného človeka, ktorý nie je pokrytec,je otvorený a pomenuje veci vždy pravým menom a celkom hrdo som si hovoril, že taký som. S odstupom času to beriem skôr ako pomenovanie pre niekoho, kto nevie držať hubu a musí si vždy povedať svoje aj keď je to netaktné a zbytočne si buduje negatívny imidž pretože keby si nepovie svoje tak asi nezaspí - je to možno pohoďák, ale netuší čo je to diplomacia. Už to neznie tak fajn, hmm:)? "Čo na srdci, to na jazyku" berte prosím ako dobrú vlastnosť, keď ak niekto fajčí na zastávke a vám to prekáža tak mu to poviete a požiadate ho, aby prestal, namiesto toho, aby ste naňho celý čas škaredo zazerali, nič nepovedali a večer sa víťazoslávne pridali do skupiny na Facebooku "Neznášam, keď je niekto taký drzí a fajčí na zastávke MHD". Buďte úprimný, keď sa vás niekto spýta a aj keď už chcete povedať, čo máte na srdci, urobte to maximálne diplomaticky - to je to spomínané "čo na srdci, to na jazyku" a nie, keď začnete pred spolužiakmi vyhlasovať, že škola, na ktorej študujete, stojí za hovno a celý systém je zlý, hoci vaše seminárne práce visia vystavené na katedre v kolonke "kolegovia, zasmejte sa". 
   
 Na záver treba ale povedať, že nie každému až tak moc záleží na tom, aby nevyzeral ako idiot. Každý má vlastný štýl, buduje si vlastný imidž, je zodpovedný sám za seba. Možno sa niekto vyžíva v tom, že ho ľudia berú ako rebela alebo ako "priameho". Sú aj flegmatici, ktorí to majú proste "na háku" - je im to jedno, keď chcú, povedia si svoje, neriešia iných a sú so sebou spokojní. No pre tých, ktorým záleží na tom, aká je ich povesť a ako sú braní okolím dúfam tento článok pripomenie to, čo už možno vedeli, ale občas na to zabúdajú. Konieckoncov to, ako nás budú brať ostatní záleží len na nás a vôbec sa nemusíme pretvarovať, niekedy stačí len "trošku" ubrať... ;) 
   
   
 A aby som skončil odľahčene, prikladám pár vtipov na danú tému. 
   
 Na svadbe hovorí jeden z hostí: "Fúúúha, to je ale škaredá nevesta!". 
 Z poza chrbta sa mu ozve: "No dovoľte, to je moja dcéra!!!" 
 Hosť sa zahanbene otočí a zakokce: "Ehm, a-le niee, je prekrásna, ste šťastný otec..." 
 Odpoveď: "No dovoľte, ja som matka!" 
   
 .... 
   
 V Tescu. Príde zákazník za predavačom, a pýta sa ho: 
 "Prosím Vás, chcel by som polovicu tejto kapusty." 
 Predavač na to: "Ehm, je mi ľúto, ale kapustu predávame iba po hlávkach..." 
 Zákazník: "Ale ja som zákazník a vyžadujem iba polku tejto kapusty, urobte s tým niečo!" 
 Predavač nevie, čo s tým a tak sa ide spýtať nadriadeného. Príde za ním a hovorí mu: 
 "Šéfe, neviem, čo mám robiť, jeden debil si chce kúpiť polku kapusty..." 
 no v tom si uvedomí, že zákazník je hneď za ním a počuje ho a tak sa otočí a pokračuje: 
 "...ale tento milý pán by si zobral druhú polku a tak navrhujem kapustu rozdeliť a dať im obom polovicu." 
 Šéf si všimol o čo ide, teda súhlasil, zákazník odišiel spokojný a po chvíli šéf hovorí predavačovi: 
 "Synak, páčilo sa mi, ako ste pohotovo zareagovali! Kde ste chodil do školy? 
 Predavač: "Ále, študoval som v Ostrave, no bolo to tam otrasné, lebo sú tam len samé kurvy a baníci." 
 Šéf: "Moja žena je z Ostravy..." 
 Predavač: "Naozaj? A v ktorej bani fárala...?" 
   
   

