



 

 
 Polyfunkčný objekt, na ktorom pôvodne stál podnik Alibaba, vyrástol začiatkom jesene. Zatiaľ v ňom sídli len reštaurácia s terasou. Mali by k nej pribudnúť pobočka banky, dopravného podniku, kozmetický a kadernícky salón. Študentom tak konečne skončia výpravy s ťažkými vakmi do karlovky, aby zaplatili ubytovací šek. 
 



 Mlynská dolina je pre mnohých synonymom študentského života. Preslávené diskotéky navštevuje v miestnych kluboch aj veľa Bratislavčanov. 
 Výstavba internátneho komplexu, ktorý tvoria výškové bloky, átriové domky a Mladosť, sa  začala v polovici sedemdesiatych rokov. Približne vtedy vznikali aj ostatné budovy, v ktorých sídlia najmä súkromné firmy. 
 Od roku 1995 sa v areáli koná hudobný festival Bažant na Mlynoch, a to pravidelne koncom mája. Tohtoročná bažantovská bilancia spomína 15000 návštevníkov a 29000 vypitých pohárov piva. Vstupnou bránou do sveta študentov  je Slávičie údolie. Pomenovali ho po Jakubovi Nachtigalovi, ktorý tu vlastnil pozemky. Po prvej svetovej vojne Nachtigalovo údolie poslovenčili na Slávičie. 
 Mlynskej doline dominuje STV a cintorín. Nachádza sa tu aj meteorologické observatórium, ktoré mapuje priestorové úhrny zrážok pre celé Slovensko. 	 
 FOTO SME - PAVOL MAJER 


 

 
 Klub Elam v internátnom komplexe Mladosť riadia výhradne študenti a to bez nároku na honorár. Elam sa preslávil diskotékami Oldies, tanečnou akciou Doubledecker a diskotékou Mexická noc, ktorá sa koná raz za mesiac. 
 





 

 
 Pôvodne bolo na mieste internátov smetisko. Niektorí študenti sa striktne držia tradície a ich izby pripomínajú skládky jedla a oblečenia. K menej šťastným sa môžu nasťahovať aj hlodavce. 
 





 

 
 Najnovším prírastkom medzi podnikmi je čajovňa Aura. Získava si čoraz viac priaznivcov nielen vďaka bohatej ponuke čajov, ale aj čokoládových nápojov. V interiéri zariadenom v pestrofarebnom orientálnom štýle hráva príjemná relaxačná hudba. 
 





 

 
 Vstupnou bránou do Slávičieho údolia je Slovenská televízia. Obyvatelia internátov zhodne tvrdia, že príjem verejnoprávnych kanálov je zo všetkých staníc najhorší. 
 





 

 
 Študenti po štátniciach, predtým, než definitívne opustia internáty, zvyknú na pamiatku zvečniť svoje mená spolu s čerstvo nadobudnutým titulom na chodníku. 
 




