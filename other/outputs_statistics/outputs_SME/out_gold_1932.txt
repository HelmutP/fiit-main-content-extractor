

 Jeden môj priateľ, vyštudovaný psychológ, mi raz povedal, že vzťah medzi ženou a mužom v tejto dobe je obchod. Svet sa stal vďaka internetu jednou veľkou zoznamovacou kanceláriou. Každá bytosť má svoje vlastnosti dotiahnuté na určitú úroveň a s touto ponukou ide aj na trh. Niektorá žena môže byť fyzicky menej atraktívna, no inteligentná. Iný muž krásny, no tupý ako klinec. V skratke to zhrnul takto: „Dievčička, ak máš ukončenú trojročku, z rodnej dedinky si sa nikdy nepohla, vieš piecť iba buchty a prdélka sa ti ledva zmestí na dve stoličky, zbytočne ty snívaš o šumnom šuhajovi s titulom, vejárikom zlatých kreditiek, mužovi, ktorý ťa bude dokonca z úprimnej lásky každý večer uspávať nežnými básňami, ktoré mu natečú do pier priamo z vesmíru. A ty, vyčesaný metrosexuál bez školy, zbytočne ceríš svoj úsmev na prekrásny púčik práve zvierajúci čerstvý červený diplom. Jej mamina je právnička a otec šéf firmy, to predsa vieš. Nevidíš, že si vystúpil z rozpadávajúcej sa Lady? Áno, láska hory prenáša, ale iba dovtedy, kým colník Čas nevytiahne palacinkár." Uznal som, že v jeho slovách bol kus pravdy. 
 Ubehli roky a mne šlo roztrhnúť dušu. Dve bytosti a obe som miloval. Čítate dobre: MILOVAL. V hlave som roloval zoznam oblastí a porovnával. Na paškál prišla láska, neha, sex, vzájomná fyzická a duševná príťažlivosť, level rozprávania sa, schopnosť dať mi deti, starať sa o ne, o domácnosť, vzťah k materiálnemu, k duchovnu a k tisícke iných hodnôt. Vnímal som cit, srdce, počúval dušu, kalkuloval rozumom, chodil zmätený po svete, radil sa s blízkou dušou z čeľade vŕbovitej. Svet chcel vidieť slzy, ale keď som mu z hrude vytiahol krvácajúce srdce, onemel. Patová situácia. Na každé rozhodnutie musí človek dorásť. Chce to čas. Až keď sa vnútro plnohodnotne nenaplní rozhodnutím, zmierením či prijatím, až potom nastane vnútorné prímeria. Dovtedy budú všetky rady okolia bezzubé. Dovedú vás síce ku križovatke, ale vykročiť budete musieť sami. Takisto ako kráčať k ďalšiemu životnému rázcestiu. 
 Po rokoch sa situácia otočila, náreky vnútra som počúval ja. Na pni ležal dvanásťročný vzťah. Jeho tenký krk žiadal o existenciu. Kamarát dostal od života čertove eso, bol zúfalý: 
 - Petru poznám. Roky neoklamem. Má svoje muchy, takisto ako ja. Tak ako sa stará o domácnosť, tak sa nestará o seba. Byt máme ako cumlík, no aj tak by sa najradšej iba vytŕčala po podnikoch a bavila sa. Oblečená ako chalan, v džínach a tričku... Je s ňou sranda. Ale sex so Sisou je nadpozemský. 
 - Sex nie je všetko, - oponoval som. 
 - Ale vo vzťahu je dôležitý. Je to jedna tretina, čo musí klapať. Ak nie, všetko sa časom rozpadne. 
 - Súhlas, - pritakal som. 
 - Pri Sise mi nonstop stojí. To som ešte nezažil, čistá striekačka. Petra nevie sex precítiť, kontroluje sa, nie je pri ňom... - žaloval sa mi kamarát. 
 - Časom to opadne... ak sa po desiatich rokoch so Sisou pomilujete trikrát do týždňa, bude to slušná štatistika. 
 - Viem, ale stále to s ňou bude dobré. Proste to s ňou funguje. Počúvaj ma, ja som raz chodil s jednou babou a pri tej sa mi nepostavil. Nikdy. Normálne zo mňa príroda vedľa nej vyrobila impotenta. Vtedy som pochopil, čo je to stratiť mužské sebavedomie. 
 - Proste ste sa nestretli, tak to chodí. Ale so Sisou to nemôžeš postaviť iba na spálni. 
 - Sisa ma ťahá do prírody, oblieka sa elegantne a takisto sa s ňou viem rozprávať. Vedľa mňa kráča žena, rozumieš? 
 - Koho ľúbiš? Čo ti šepká tvoje srdce? Na ktorú ukazuje tvoja duša? - vyslovil som najzákladnejšie otázky. 
 - Paľo, ja sa ti bojím odpovedať. 
 - Miluješ obidve, však? - jemne som mu pomohol. 
 - Vyrozprával som to len pár ľuďom, ale ani jeden ma nepochopil. Áno, milujem obidve, - odvetil ticho. 
 - Rozumiem ti, - odpovedal som mu. 
   
   
   
 Buďte šťastní, Pavel "Hirax" Baričák 
  Najbližšie aktivity Hiraxa:  Streda 9. 12. 2009 o 17:02, Poprad, v Groteske, Krst básnickej zbierky Život nie je pes, je to suka od Janka Martona. Ako hosť Hirax a jeho čítačka k novému románu Sekundu pred zbláznením a jeho prvej básnickej zbierke More srdca. A do tretice sa ukáže aj poetka Ľubica Lju Mesárošová z Veľkých Kapušian.  Štvrtok 10. 12. 2009 o 15:33, Ružomberok, Mestská knižnica. Čítačka Hiraxa k novému románu Sekundu pred zbláznením a jeho prvej básnickej zbierke More srdca. Plus premietanie z potuliek Indiou, Thajskom, Etiópiou a Panamou. Hostia-poeti: Ján Marton a Ľubica Lju Mesárošová.  Štvrtok 10. 12. 2009 o 19:02, Liptovský Mikuláš, Cafe &amp; Cocktail. Čítačka Hiraxa k novému románu Sekundu pred zbláznením a jeho prvej básnickej zbierke More srdca. Ako hosť Jano Marton a predstavenie jeho debutovej básnickej zbierky Život nie je pes, je to suka, ako aj poetka Ľubica Lju Mesárošová z Veľkých Kapušian. Od 22:00 do 24:00 rozhovor do L-rádia.  Piatok 19. 02. 2010 o 17:03 hod. Martin, Kocka Koncert Horárovi III. Vystúpia kapely LUNATIC GODS, PROTEST, EDITOR, ODPAD a RODDATES. Benefičný koncert organizovaný ako spomienka Jarkovi "Horárovi" Vavrovi, spevákovi skupín Protest a Lunatic Gods. Výťažok poputuje na konto martinského občianského združenia „RAKOVINA". 
   
   
   
   

