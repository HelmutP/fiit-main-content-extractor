

 Čo sa deje, mojka?" „Tá vodička, čo si mi dala, je studená! Ja chcem teplú! Maminka, čo si to za človeka?!" 
 Dobré dve minúty som sa nemohla prestať smiať. Podobne ako manžel pri detskom vzdychu o dva dni skôr v znení: „To je život, tatinko..." Hej, to je život, Matúško, keď musíš upratať všetky tie autíčka, čo si rozťahal. 
 „To som nikdy v živote nevidel, taký neporiadok." Počkaj, všetko neunesiem, nie som predsa chobotnica!" „Tak to teda v žiadnom prípade." „Maminka, si dnes pekná. Budú ťa obdivovať." „No to snáď nemyslíš vážne?!?" „Poslúchal si, Matúško? No to bude mať maminka radosť." Dospelé reči z maličkých detských úst, krivé zrkadlo niektorým našim výrokom. Väčšine z nich nerozumejú. Ale hovoríme ich a malí sa nám chcú podobať. Tak ich opakujú. Bože, zlatíčka, kiež by vám to vydržalo. Ale - aj v napodobňovaní vecí, ktoré sú, na rozdiel od niekedy hlúpych slov, chvályhodné... 

