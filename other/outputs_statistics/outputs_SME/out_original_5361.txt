
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kolář
                                        &gt;
                Nezaradené
                     
                 Dotkol som sa mesta 

        
            
                                    27.4.2010
            o
            13:07
                        (upravené
                27.4.2010
                o
                13:43)
                        |
            Karma článku:
                3.81
            |
            Prečítané 
            770-krát
                    
         
     
         
             

                 
                    Na podnet mojej známej som sa cez víkend vybral s kamarátkou na bratislavskú akciu, na ktorej sa počas niekoľkých dní dalo všeličo absolvovať. Či už návšteva Magistrátu hlavného mesta, divadla P.O. Hviezdoslava, prípadne rôzne hudobné a folklórne akcie, priehladky mesta, otvorenie promenády pred novovybudovaným centrom Eurovea a kopec ďalšieho. Bolo si len treba vybrať podľa chuti. Všetko sa to hádam ani nedalo absolvovať.
                 

                 
  
   Nápor ľudí sme očakávali, nenávidím to, ale povedali sme si, že to riskneme. Počasie vyšlo, tak čo doma? Všetko by bolo v poriadku až na to, že infraštruktúra mesta absolútne nie je stavaná na takéto aktivity. Druhá vec, ktorá ma zarazila napriek tomu, že sa dala očakávať, boli predvolebné aktivity rôznych politických strán. Neviem, či si niečím pomôžu ak zneužijú protestnú akciu na námestí SNP a budú rozdávať reflexné prúžky pre cyklistov... To čo sa udialo členovi protestnej akcie s jeho drevenicou, k tomu je škoda sa vyjadrovať. Ak to bolo úmyselné, je možné, že sa dotkol niektorej osoby vyvesený transparent na námestí. K politike sa strašne nerád vyjadrujem, ale mám pocit, že slovo demokracia, sa na Slovensku o chvíľu stane archaizmom.       V nedeľu sme si z pestrého programu vybrali „slávnostné otvorenie zrekonštruovaného Kačína“. Bolo príjemne teplo, tak sme oprášili bicykle a vyrazili sme na cestu. Už pri prechode cez Železnú studničku nás čakalo ďalšie sklamanie. Miesto užívania si prírody sme s bicyklami uhýbali autobusom a pretierali si oči od rozvíreného prachu.  Keby chodili takto autobusy v ranných špičkách, ľudia by možno nahradili osobnú dopravu MHD a bolo by na cestách menej áut. Všetky autobusy boli preplnené ľuďmi, ktorí vyzerali byť typu, ktorý do prírody po vlastných nikdy nepôjde. Samozrejme česť výnimkám.  Je to na zamyslenie, keď sa mladí a zdraví ľudia vyvezú až priamo na miesto autobusom a staršia pani tlačí svoju imobilnú (pravdepodobne) dcéru na invalidnom vozíku sama do kopca.   Zrekonštruovaný Kačín nám ničím nepripomínal miesto, kam sme v detstve radi chodievali so svojími rodičmi. Pokojné miesto v lese sa zmenilo niečo, pripomínajúce park v strede mesta, plus ten rozvírený prach z autobusov, zničenú trávu, popraskané balóniky z predvolebných kampaní a rozdupané plastové poháre. O pódiu s hudbou ani nehovorím. Bol som naučený, že v lese treba byť ticho, aby sa neplašila zver a potom vidím pódium s vypeckovanou hudbou... Nie som žiadny ochranár, ale pýtam sa... Je toto normálne?    Nechcem hádzať špinu len na politikov, organizátorov a predajcov s občerstvením, ktorí tam mali svoje stánky, mali by sme sa zamyslieť v prvom rade my sami... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kolář 
                                        
                                            Skúšky z lások
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kolář 
                                        
                                            Aký je rozdiel medzi starým sedanom a novým Ferrari?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kolář 
                                        
                                            Croatian Tour
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kolář
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kolář
            
         
        peterkolar.blog.sme.sk (rss)
         
                                     
     
        som človek snívajúci, plánujúci ale zároveň žijúci pevne oboma nohami na zemi :) ...teda dúfam...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    961
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




