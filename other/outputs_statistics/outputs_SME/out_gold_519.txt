

 
Určite poznáte situáciu, keď nemáte pri sebe mobilný telefón a keď sa ku nemu vrátite, nájdete si X zmeškaných hovorov. Slušný človek zavolá volajúcim naspäť. Čo v prípade, že si nájdete zmeškaný hovor od neznámeho čísla? Hoci máte Orange, prevládne vo Vás slušnosť a zavoláte na 0904XXXZZZ. Najprv telefón 3x zvoní a potom to dvíha neznámy hlas, ktorý vraví, že pokiaľ nie je k zastihnutiu na tomto čísle, tak je na čísle 0900XXXYYY. Zložíte a vidíte na telefóne prevolané necelé 2 minúty. Finta číslo 1.
 
 
Pokúsite sa spojiť s dotyčným volaným a tak zavoláte na číslo 0900XXXYYY, telefón je zaneprázdnený, tak čakáte až kým si uvedomíte, že voláte na predvoľbu 0900, čo určite nie je celkom bezplatná predvoľba. Zložíte, veď telefón len vyzváňal, tak snáď ste nič neplatili. Omyl, z Vašeho paušálu ste práve prevolali 2 minúty na platenú linku za viac ako 50 Sk na minútu. Finta číslo 2.
 
 
Rozhodnete sa dotyčného vypátrať, tak zavoláte na informácie a zistíte, že majiteľ telefónneho čísla 0900XXXYYY sa nechal utajiť, napriek tomu, že má spoplatňované číslo a napriek tomu, že ste mu práve nedobrovoľne venovali niečo cez 100 korún.
 
 
Telekomunisti ako prevádzkovateľ danej služby Vám poradia, že sa máte obrátiť na políciu, meno majiteľa (názov firmy) Vám neprezradia. Keď sa obrátite na Telekomunikačný úrad, dostanete odpoveď so žiadosťou o ďalšiu spoluprácu. A tak zrazu sa stávate spolupracovníkom pri vyšetrovaní podvodných telefonátov.
 
 

Finta č. 1: telefónne číslo 0904XXXZZZ je pripojené na počítač, ktorý po Vašom zazvonení zodvihne a začne Vám prehrávať vyzváňací tón a následne aj nejaký odkaz. Vaša strata pár korún za hovor do siete T-Mobile. Zisk majiteľa telefónneho čísla pri službe BONUS pár korún? Zisk spoločnosti T-Mobile pravdepodobne pár korún z prepojovacích poplatkov? 
 
 

Finta č. 2:  telefónne číslo 0900XXXZZZ je pripojené na počítač, ktorý po Vašom zazvonení zodvihne a začne Vám prehrávať vyzváňací tón. Vaša strata pár desiatok až stoviek korún za hovor na spoplatnené číslo. Zisk majiteľa telefónneho čísla je z poplatku, ktorý ste zaplatili za volanie na toto číslo. Zisk spoločnosti telekomunistov je rovnako z poplatku, ktorý ste zaplatili za volanie na toto číslo.
 
 

Záver?
 
 
Spoločnosť T-Mobile poskytuje službu BONUS, vďaka ktorej je možné hovormi z cudzej siete dobíjať kredit. Nápad určite dobrý, no v praxi zneužívaný na neoprávnené obohatenie. Spoločnosť T-Mobile nemá dôvod takúto službu zrušiť, pretože jej prináša určitú časť prichádzajúcich hovorov a teda aj zisku z prepojovacích poplatkov.
 
 
Spoločnosť telekomunistov poskytujúca spoplatnené číslo by mohla pri prvej zmienke o podvode konať a minimálne neumožniť skryť vlastníka spoplatneného čísla na informáciách. Nemá na tom však záujem, ide jej predsa o zisk z Vašich hovorov na spoplatňované číslo.
 
 
Spoločnosť podvodníka je na tom najlepšie. Nikto jej prakticky nič nedokáže a ak sa náhodou nájde človek, ktorý sa o to pokúsi, spoločnosť to zahrá spôsobom vyskytla sa technická porucha zariadenia, zaplatíme Vám Vašich 120 korún. O ostatných okradnutých sa nehovorí.
 
 

Názor na uvedené spoločnosti ako aj celý proces zarábania na prezváňajúcich nech si urobí každý sám. Môj osobný názor je, že existujú call centrá, v ktorých sa nič iné nerobí, len takto umelo generuje zisk 3 spoločností na úkor bežných ľudí. Napriek tomu sa považujem za slušného človeka a na zmeškané hovory spätne zavolám. 
 

