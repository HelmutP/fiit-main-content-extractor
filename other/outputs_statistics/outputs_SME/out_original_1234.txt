
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zdenko Dzurjanin
                                        &gt;
                Ruská federácia v symboloch
                     
                 Ruská federácia v symboloch: Štátny znak Ruskej federácie 

        
            
                                    23.2.2009
            o
            6:59
                        |
            Karma článku:
                10.30
            |
            Prečítané 
            7313-krát
                    
         
     
         
             

                 
                    Kde sa vzal, tu sa vzal... alebo odkiaľ prišiel štátny znak Ruskej federácie.
                 

                 
  
   Prvotný výskyt štátneho znaku Ruska na jeho území sa datuje do roku 1452. So zmesou zvláštnych okolností - ľúbostnej drámy a politického rozmachu Osmanskej ríše - sa do týchto končín dostal až z ďalekého Ríma... Ale vráťme sa v dejinách ešte trocha naspäť.   Vtedy to pre celý kresťanský svet boli časy častých nájazdov moslimských armád, ktoré hlava-nehlava plienili územia východnej Európy. Osmanská ríša najprv zničila Byzantskú a obsadila jej hlavné mesto (1453), ktoré sa od týchto čias volalo Istanbul. Neskôr padlo aj Grécko a po jeho zdrvujúcej porážke začal Mehmed II. ohrozovať Rím a Vatikán.   Pápež Pavol II. mal vtedy jedinú možnosť zachrániť sa. Bol ňou Foma Paleologa, brat bytantského cisára, ktorý spoločne s rodinou utiekol zo svojej ríše k pápežovi. Foma mal mladú dcéru, princeznú Sofiu, ktorú mal pápež vo veľkej obľube. Rozmýšľal teda, za koho ju vydať tak, aby zjedodušil postavenie Ríma; s kým vstúpiť do zväzu v tak ťažkých časoch?     
      Drevený Kremeľ    Pápežov pohľad utkvel na kresťanskej Moskve a veľkom kniežati Ivanovi III.[1], ktorý len nedávno ovdovel. Podľa neho bol aj jediným panovníkom, ktorý bol schopný rozumom a silou prekabátiť tureckého sultána. Práve za jeho vlády sa Rus (Moskovské kniežatstvo) oslobodila spod hrozby útoku Tatárov, a tiež bola vydaná prvá zbierka zákonov (nie osobitné zákony (tzv. Ruská pravda), ktorých autorom je Jaroslav I. Múdry). Je považovaný za zakladateľa dnešnej polície v Rusku, ale najväčším úspechom pre neho samotného bolo to, že dokázal upevniť postavenie Moskvy ako hlavného mesta mladého štátu.  V tom čase mal Ivan III. len 20 rokov, hoci už bol raz ženatý a mal jedného syna.   Pápež aj napriek tomu dúfal, že u Ivana III. uspeje a že si on princeznú Sofiu vezme za ženu (hoci nebola katolíčka) a spolu odídu do Konštantínopolu, kde moskovské knieža vyprovokuje u Osmanov odvetu za obsadenie Druhého Ríma.   Do Moskvy za Ivanom teda prišla delegácia, ktorá mu priniesla obraz jeho potenciálnej budúcej ženy. Bola to naozaj krásna deva. Keď sa dozvedel o čo pápežovi Pavlovi ide, zamyslel sa a povedal  
      Kremeľ z bieleho kameňa    : „Svadba so Sofiou by mi priniesla nárok na to veľké územie, z ktorého na Rus prišlo svetlo kresťanskej viery, hoci je okupované vojskami Osmanskej ríše. Z týchto dvoch dôvodov vašu ponuku prijímam." a nechal poslať hojne darov pápežovi aj princeznej Sofii.  Samotný svadobný obrad a odovzdanie obrúčiek sa konalo v máji bez prítomnosti Ivana III. v Ríme. Heď po ňom sa Sofia vydala na cestu za svojím mužom do Moskvy, kam dorazila po šiestich mesiacoch cesty 12. novembra 1472. V ten istý deň večer boli mladomanželia v paláci Ivanovej matky Márie Jaroslavny oddaní.     
          Dvojhlavý orol na pečati Ivana III. z roku 1497    Sofia toho pre Rusko urobila veľa, a práve jej môžu Rusi ďakovať za to, že dnes majú vo svojom štátnom znaku dvojhlavého orla - na hrudi s erbom Moskvy, - ktorý vystupuje do popredia spoza červeného štítu. Tak ako už spomenuté kresťanstvo, aj vyobrazenie dvojhlavého orla[2], ktorý mal na hlave koruny[3] - symbol nezávislosti Byzantskej ríše - Rusko prijalo z rúk cudzinca.  Erb v takejto forme (bez erbu Moskvy) vzbudzoval u ruských panovníkov úžas a tajomnú silu a aj z toho dôvodu ho prijali za svoj vlastný. Sprvoti mali k nemu takú veľkú úctu, že sa ho nikto nedotýkal, okrem kniežaťa Ivana IV. Hrozného. Od čias jeho vlády (1533 - 1584) sa na hrudi dvojhlavého orla zobrazoval aj moskovský erb - udatný junák na koni, ktorý kopijou zabíja draka. Ten junák je dodnes v mysliach ruského národa spojený s menom Georgija Pobedonosca (Jurij Dolgorukij), ktorý v roku 1147 založil na brehu rieky Moskva rovnomenné mesto (medzi ruským národom sa traduje, že to bolo 4. apríla).   Takto domyslený erb pridával štátnemu znaku Ruska hrozivejší vzhľad, nakoľko sa k dvom hlavám orla pridali ešte tri: vojak, kôň, had a čerešničkou na torte bola kopija. Toto zobrazenie erbu však okám Romanovcov nelahodilo a postupne ho v priebehu štyroch storočí modifikovali. Zo začiatku boli krídla orla vystreté, tak akoby sa orol chystal na vzlet a z jeho zobákov trčal jazyk hada. Jeho nohy zdobili mocné pozúry, v ktorých držal guľu (symbol štátu) a žezlo (symbol moci). K dvom korunám na hlavách orla pribudla tretia, čo dohromady symbolizovalo Svätú Trojicu: Otca, Syna a Ducha Svätého.     
        Štátny znak v časoch Petra I. a Mikuláša II.      V časoch Petra I. Veľkého (1682 - 1721) na hruď dvojhlavého orla pribudla zlatá reťaz apoštola Ondreja - najvyššie ocenenie Ruského impéria - a trojicu korún na hlavách orla spája belasý moarový pás. Panovník touto reorganizáciou štátneho symbolu docielil spojenie verného vojaka a víťazonostného plukovníka. Okrem toho prikázal prefarbiť orla načierno a vyzdvihnúť ho do vzduchu, čo malo symbolizovať ideu ochrany rodného hniezda, z ktorého vták vylietava.  Symbolika takto vymysleného erbu a symbolika samotného orla Petra I. Veľkého znamenala ohlásenie nového kurzu Ruského impéria - kurzu rozmachu jeho územia.   Začiatkom 19. storočia cár Alexander I., po tom čo je ho krajina podľa neho dosiahla želanú veľkosť nariadil, aby bol orol zase premaľovaný zlatou farbou a insígnie panovníka nechal odstrániť, pričom ich nahradil bleskami, fakľou a varínovým vencom.   Symbolika erbu bola pochopiteľná. Panovník ňou chcel svojím poddaným dopriať slobodný život v slobodnej krajine, kde má každý prístup ku vzdelaniu (symbol fakle) a nepriateľom Ruského impéria želal len to najhoršie (hromy a blesky), ak by sa ho niekedy pokúsili napadnúť.   Samozrejme, územná expanzia Ruského impéria pokračovala aj za vlády nasledovníkov Alexandra I., kedy impérium získalo územia na Kaukaze (Dagestan, Azerbajdžan). Začatá vojna s Osmanskou ríšou priniesla pozitívny výsledok pre krajiny na Balkánskom polstrove. Po pripojení Besarábie k Ruskému impériu, získali nezávislosť (určitú formu autonómie) aj Grécko, Srbsko či Moldavsko. Po úspešných momentoch na juhu Európy sa ťažisko vojen premiestnilo na územie Litvy. Po poslednom rozdelení Poľska (24. október 1795) prišlo oslobodenie Bulharska (3. marec 1878) a triumfálny zisk Fínska.   Všetky tieto výdobytky sa za vlády Mikuláša II. znova odzrkadlili na štátnom znaku. Pribudlo naňho 6 erbov získaných území Kazanskej, Astrachaňskej, Sibírskej, Poľskej, Fínskej a Gruzínskej gubernie.     
          Štátny znak Zväzu sovietskych socialistických republík    Dejiny sa ale s Ruským impériom kruto zahrali. Štátny útvar sa po októbrovej revolúcii v roku 1917 rozpadol a starý štátny znak celkom zrušili a nahradili ho zemeguľou, ktorá, pravda, bola natočená na hornú časť, presne tak, aby bolo vidno východ slnka nových víťazstiev. Nad zemeguľou navyše visel symbol spojenia robotníkov a roľníkov - kosák a kladivo - a vrch erbu ukončovala päťcípá hviezda, ktorá mala symbolizovať nadradenosť Sovietskeho zväzu (hoci mystickú) nad všetkými štátmi sveta.  Sen o veľkej a ničím nezrušiteľnej krajine (tak ako sa humorne spievalo na začiatku sovietskej hymny Sajuz nezrušimyj), ktorý začal uskutočňovať Vladimír Iľjič Lenin a neskôr generalissimus Josif Vissarionovič Stalin a jeho odchovanci, sa koncom roka 1991 rozplynul. Mnohí z tohto neúspechu udržať prvú sovietsku krajinu sveta pri živote vinili Michaila Sergejeviča Gorbačova a jeho perestrojku, ktorou rozbehol záchranu toho, čo sa ešte zachrániť dalo. Pravdou však je to, že ľud Sovietskeho zväzu skôr či neskôr pád Kolosu na hlinených nohách očakával. Veď ako sa vraví, kto chce kam, pomôžme mu tam. A tak večer 31. decembra 1991, keď išiel sovietsky národ postupne spať, na druhý deň sa zase postupne budili ľudia na Kamčatke, Sibíri, Urale a Moskve, ale už do demokracie a demokratického štátu.  
          Pokus o štátny znak Ruskej federácie z roku 1991    Hurá, Sovietsky zväz padol, máme tu demokraciu, o ktorej sme už dávno snívali!  Demokracia tu síce možno aj bola, ale nebolo štátneho znaku. Ten sa rodil postupne na troskách predchodcu dnešnej Ruskej federácie a z dávnych tradícii ruského národa. Kedysi veľmi veľmi dávno - si ktosi v roku 1991 pamätal, - bol na pečati cára Ivana III. Veľkého dvojhlavý orol. V prvých dňoch po vzniku Ruskej federácie ho teda vzkriesil zmŕtvych. Kritici ho ale vysmiali, pretože bez pazúrov, insignií a troch korún vyzeral ako zmoknuté kura.   Všetky dovtedajšie atribúty mu prinavrátil národný umelec Ruskej federácie (1967), architekt a tvorca pamätníka obetiam GULAGu v Petrohrade - Jevgenij Iľjič Uchnaľov (* 1931), - ktorý je zároveň považovaný za autora súčasného štátneho znaku Ruskej federácie.   Teraz, keď už zmoknuté kura malo všetko na čo bolo zvyknuté predtým, však nastala komická situácia. Erb totiž nechtiac pripomínal obdobie Ruského impéria. Rusom, aj napriek tomu, že žili a dodnes žijú v demokratickom štáte, to ale vôbec nevadí, ba naopak, vymysleli si vlastnú symboliku.   A ako vlastne súčasný štátny znak Ruskej federácie - ktorý Rusi považujú za spojenie prísahy vojaka a modlitby - vyzerá? Predstavte si na pozadí heraldického štítu, ktorý je v dolnej časti v strede zaostrený (je to tzv. francúzsky štít) vyobrazeného zlatého dvojhlavého orla. Ten orol má na každej jeho hlave dve koruny, ktoré ukončuje jedna veľká, umiestnená v strede, nad dvoma menšími. Tieto tri koruny, tak ako v erbe Ruska za čias Petra I. Veľkého, spája belasý moarový pás. V pravej nohe drží v pazúroch žezlo a v ľávej zase guľu (spomínané symboly moci a štátu). Na hrudi orla je ešte jeden menší štít s erbom Moskvy, na ktorom strieborný jazdec odetý v modrom plášti a sediaci na striebornom koni, striebornou kopijou zabíja čierneho draka.     
      Súčasný štátny znak Ruskej federácie  potvrdený prezidentom Vladimírom Putinom  v roku 2000      Na tento obraz sa možno pozerať rôznym spôsobom. Oficiálny výklad štátneho znaku Ruskej federácie však znie nasledovne: „Ruská federácia sa i ako predtým nachádza pod ochranou svätej Trojice, verí v Boha, v cára (moc) a vlasť. Zo všetkých síl sa snaží udržať si územie vlastného štátu. Je poslušná zákonu a spravodlivosti sveta, o čom svedčí rádový pás - znak hierarchie. Ruská federácia nikomu nehrozí, jej zámery sú čisté ako striebro, jej sily sú poslušné modrej farbe poslušnosti, jej kopija je nasmerovaná dole a symbolizuje boj proti spoločnému zlu človečestva. To zlo, je iba zlo hriechov a spoločnej chudoby, nie ľudí ani nie štátov."      
      Kremeľ z červených tehál    [1] Ivan III. Veľký bol najjasnejšia postava ruských dejín v období pred Petrom I. Veľkým. S jeho menom sa spája stavebný rozmach moskovskej pevnosti na Borovickom kopci.  Z Moskvy, čiže symbolu Kremľa, ktorý ako drevený slúžil od roku 1156 Jurajovi Dolgorukému a predchodcom Ivana III. urobil pevnosť takú, akú ju poznáme dnes: mestečko palácov a chrámov umiestnené za stenou s červenými tehlami s bielym pásom vo vrchnej časti. Pri stavbe nového Kremľa sa nechal inšpirovať ideami florentského štýlu, ktoré do Moskvy priniesla jeho žena Sofia. Tá sa zaslúžila aj o vznik prvého sadu v Moskve v blízkosti Kremľa (Alexandrovský sad) a tiež prvého jazera pre zlaté rybičky. [2] Dve hlavy, z ktorých jedna sa pozerá doľava a druhá doprava symbolizovali moc nad západnou a východnou častou Byzantskej ríše. [3] Dve koruny symbolizovali duálnu moc. [4] Dzurjanin Z.: Z vývinu dejín Moskovského Kremľa vo fotografiách. 2008.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdenko Dzurjanin 
                                        
                                            Ako vyzeral vstupný test z literatúry na GAMČI v roku 1982
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdenko Dzurjanin 
                                        
                                            Ako vyzeral vstupný test zo slovenského jazyka na GAMČI v roku 1982
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdenko Dzurjanin 
                                        
                                            Migrácia po rozpade Starého Veľkého Bulharska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdenko Dzurjanin 
                                        
                                            Nápis pod Sochou sv. Trojice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zdenko Dzurjanin 
                                        
                                            Nech žije Ukrajina!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zdenko Dzurjanin
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zdenko Dzurjanin
            
         
        dzurjanin.blog.sme.sk (rss)
         
                                     
     
        Študent odboru ruské a východoeurópske štúdiá Filozofickej fakulty Univerzity Komenského.




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    266
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1827
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Osobnosti Bulharska
                        
                     
                                     
                        
                            Bulharské sviatky
                        
                     
                                     
                        
                            Balkánske vojny
                        
                     
                                     
                        
                            Z histórie Bulharska
                        
                     
                                     
                        
                            Pamätaj na svoju históriu
                        
                     
                                     
                        
                            Zamatová revolúcia v Bulharsku
                        
                     
                                     
                        
                            Spomienky z bulharskej školy
                        
                     
                                     
                        
                            Moje revolučné básne (bulharsk
                        
                     
                                     
                        
                            Z histórie Slovenska
                        
                     
                                     
                        
                            Z histórie Slovenska - dodatky
                        
                     
                                     
                        
                            Nezľaknite sa!!!
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Prvá svetová vojna
                        
                     
                                     
                        
                            Zujímavosti z jazyka
                        
                     
                                     
                        
                            Literatúra
                        
                     
                                     
                        
                            Brepty zo školských lavíc
                        
                     
                                     
                        
                            Na chvíle voľna
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Produkty vlastnej výroby
                        
                     
                                     
                        
                            Prechádzky po Bratislave
                        
                     
                                     
                        
                            Z výletov po našom Slovensku
                        
                     
                                     
                        
                            Z výletov po svete
                        
                     
                                     
                        
                            Uhorské hrady
                        
                     
                                     
                        
                            Z dejín, ktoré rozprávajú
                        
                     
                                     
                        
                            Ruská federácia v symboloch
                        
                     
                                     
                        
                            Z bulharských kroník
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Ján Lacika: Bratislava na starých pohľadniciach
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Toše Proieski: Live 2007
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ladislav Šebák
                                     
                                                                             
                                            Ján Urda
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Všetko o Bielorusku (anglicky, rusky, bielorusky)
                                     
                                                                             
                                            Cestovný poriadok Ukrajinských železníc (rusky)
                                     
                                                                             
                                            Topografická mapa Ukrajiny
                                     
                                                                             
                                            Historická kurzová kalkulačka
                                     
                                                                             
                                            Hymny štátov sveta (*.mid)
                                     
                                                                             
                                            Svetové rádiostanice online
                                     
                                                                             
                                            Chcete vedieť dĺžku hraníc niektorých štátov sveta? (bulharsky)
                                     
                                                                             
                                            Dejiny štátov východnej Európy
                                     
                                                                             
                                            Teória liturgickej hudby
                                     
                                                                             
                                            Taký zaujímavý blog
                                     
                                                                             
                                            Moje fotografické výtvory
                                     
                                                                             
                                            Zbierka zákonov ČSR od roku 1918 po súčasnosť
                                     
                                                                             
                                            Galéria socialistického dizajnu
                                     
                                                                             
                                            Z blogu Tomáša Bellu
                                     
                                                                             
                                            Informácie o MHD
                                     
                                                                             
                                            Ako si vytvoriť vlastný blog?
                                     
                                                                             
                                            Stare DOSovské hry
                                     
                                                                             
                                            Učebnica bulharského jazyka pre pokročilých
                                     
                                                                             
                                            Ruská internetová encyklopédia
                                     
                                                                             
                                            Všetko o astronómii (bulharsky)
                                     
                                                                             
                                            Bulharské ikony
                                     
                                                                             
                                            Biblia v bulharskom jazyku
                                     
                                                                             
                                            Mp3s for free (SK, CZ, US)
                                     
                                                                             
                                            Vážna hudba vo formáte MP3 (RUS)
                                     
                                                                             
                                            Vážna hudba vo formáte MP3 (UA)
                                     
                                                                             
                                            Vážna hudba vo formáte MP3 (ITA)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




