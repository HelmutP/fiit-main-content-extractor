

   
 Pred šiestimi rokmi sme postavili Facebook na niekoľkých jednoduchých nápadoch. Ľudia chcú zdieľať informácie a zostať v spojení so svojimi priateľmi. Ak dáme ľuďom kontrolu nad tým, čo zdieľajú, budú chcieť zdieľať viac. Ak ľudia zdieľať viac, bude svet stále viac otvorený a prepojený. A svet, ktorý je viac otvorený a prepojený, je lepší svet. Toto sú dnes stále naše hlavné princípy. 
 Facebook rastie rýchlo. Stal sa spoločenstvom viac ako 400 miliónov ľudí v priebehu niekoľkých rokov. Je to výzva, uspokojiť toľko ľudí v krátkom čase. Tak sme sa hýbali rýchlo, aby sme ponúkli pre túto komunitu nové spôsoby, ako sa pripojiť k sociálnej sieti a navzájom komunikovať. Niekedy sme sa pohybovať príliš rýchlo - počúvame však nedávne obavy a reagujeme. 
   
  
 Mark Zuckerberg 
 Problémom je, ako siete, ako je naša, uľahčia zdieľanie, ponúknu kontrolu a výber (voľnosť), a umožnia túto skúsenosť jednoducho a  pre každého. To sú otázky, o ktorých premýšľame celú dobu. Kedykoľvek  urobíme zmenu, snažíme sa uplatniť skúsenosti, ktoré sme sa naučili počas našej cesty. Najdôležitejsia správa, ktorú sme počuli v poslednej dobe je, že ľudia chcú jednoduchšiu kontrolu nad svojimi informáciami. Jednoducho povedané, mnohí z vás si myslia, že naše ovládanie bolo príliš zložité. Naším zámerom bolo, aby sme vám dali veľa možností kontroly, ale to nemusí byť to, čo mnohí z vás chceli. Iba sme minuli cieľ. 
 Počuli sme spätnú väzbu: Musí byť jednoduchší spôsob, ako ovládať vaše osobné dáta. V nadchádzajúcich týždňoch budeme pridávať funkcie na nastavenie súkromia, ktoré sú oveľa jednoduchšie na používanie. Tiež ponúkneme jednoduchý spôsob, ako vypnúť všetky neželané služby. Snažíme sa, aby boli tieto zmeny k dispozícii čo najskôr. Dúfame, že budete spokojní s výsledkom našej práce, a ako vždy, budeme čakať na váš názor. 
 Tiež sme počuli, že niektorí ľudia nechápu, ako sú ich osobné údaje použité, a majú strach, že sú zdieľané spôsobmi, aké nechcú. Chcel by som to teraz upresniť. Mnoho ľudí si môže vybrať, ktoré zo svojich informácií  zviditeľnia pre každého, a ľudia ich môžu voľne nájsť  na Facebooku.  Takže už ponúkame funkcie na obmedzenie viditeľnosti týchto informácií a chceme, aby boli ešte silnejšie.  
 Tu sú zásady, podľa ktorých sa Facebook riadi:   
   - Máte kontrolu nad tým, ako sú zdieľané vaše informácie.     - Nezdieľame  vaše osobné údaje s ľuďmi alebo službami, s ktorými nechcete.     - Nechceme dať inzerentom prístup k vašim osobným údajom.     - Nechceme a nikdy nebude predávať akékoľvek informácie o vás komukoľvek.     - Budeme vždy držať Facebook ako bezplatnú službu pre každého.      
 Facebook sa vyvinul z jednoduchých internátnych projektov  na  globálnu sociálnu sieť spájajúcu milióny ľudí. Budeme budovať, budeme počúvať a budeme mať viesť dialóg s každým, komu záleží na tom,  aby sa s Facebook-om podelili o svoje nápady. A budeme sa súsrediť  na dosiahnutie nášho poslania: dávať ľuďom schopnosť, aby mohli zdieľať a urobiť svet otvorenejší a viac prepojený. 
   
 By Mark Zuckerberg, Monday, May 24, 2010 (stĺpček vo Woshington Post) 
   
 Autor článku je zakladateľ a šéf spoločnosti Facebook. 
      

