
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Veronika Vlčková
                                        &gt;
                Priv(i)ate
                     
                 To, čo mám 

        
            
                                    15.1.2010
            o
            2:54
                        |
            Karma článku:
                8.20
            |
            Prečítané 
            1884-krát
                    
         
     
         
             

                 
                    Dlho som sa neukázala, tu, kde sa mi tak dobre básni o mojich cestách vlakom... Je toho dosť, o čom by som mohla napísať. Veľmi konkrétne. Napríklad o padajúcich stanoch, veľkých padajúcich stanoch v lete. Alebo by som sa mohla zmieniť o mesiaci preč, v cudzine, kde si človek uvedomí, aspoň trochu, na čom mu skutočne záleží. A ako veľmi dokážu maličkosti meniť pohľad na svet. Mohla by som písať o tom, ako starnem, ako sa zo mňa stáva zodpovedný a cieľavedomý človek. Ale to ja vôbec nechcem, ani nemôžem, aspoň nie teraz, po tej ceste vlakom krajinou, ktorú aj najväčší mestský cynik-intelektuál musí nazvať rozprávkovou.
                 

                 Zaspala som svoje dávky každodennej poetiky tu na blogu. Vymenila som ich za sociálne siete a vôbec sa za to nemám rada. Zaspala som svoju tradičnú novoročnú básničku. Mohla vzniknúť, podmienky na Silvestra boli vhodné, ale ja som sa radšej nechala uniesť pocitmi bez nutnosti verbalizácie. A obrazmi, s nutnosťou zachytenia. Moja krajina bola často tmavá, bez života.      Našťastie, ešte stále mám schopnosť vidieť farby.  A ten zvláštny opar, ktorý sa dá zachytiť vo vzduchu. Presvedčila som sa o tom aj pri poslednej ceste vlakom, ktorá mi konečne pripomenula, že na niečo už dlho zabúdam, a že je čas to napraviť. Inak celkom vážne hrozí, že vyrastiem zo svojej rozprávky. A pritom, ja sa vôbec necítim staršia, ani zodpovednejšia. Len teraz častejšie uvažujem nad tým, čo mám.      Prednedávnom ma takto prekvapil človek, ktorý ma vždy prijímal bezvýhradne a bez komentárov. Povedal mi, že by som bola v živote oveľa spokojnejšia, keby som stále neuvažovala nad vecami, ktoré nemám, a vážila si tie, ktoré mám. Aká jednoduchá pravda. A predsa, pýtam sa, koľko ľudí si ju uvedomuje? Koľko ľudí je ochotných si ju pripustiť?      Začala som preto na sebe pracovať. Teda, aspoň sa o to snažím. Je to kvôli tým jednoducho pravdivým slovám, ale nielen kvôli nim. V mojom živote asi muselo nastať niečo smutné, niečo zásadné, aby mi to v hlave "preplo". Aby som sa začala strachovať o blízkych. Aby som si začala vážiť ľudí, ktorí mi  poskytujú podporu, priateľstvo, lásku. Myslím tým skutočne vážiť. Odpúšťať drobné mraky na nebi vlastnej dobrej nálady. Veď ktovie, koľko takýchto mrakov som sama spôsobila. Mám teraz potrebu ďakovať, prosiť o odpustenie a šíriť ďalej to slnko, ktoré aj po nepodarenom predsilvestrovskom dni vyšlo a všetko (mi) osvetlilo. Ďakujem za to, čo mám. Vôbec to nie je málo. Rada by som toto uvedomenie rozšírila ďalej, presne tak naivne, ako kandidátky v súťaži miss túžia po rozšírení svetového mieru.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Veronika Vlčková 
                                        
                                            Óda na Miša Kaščáka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Vlčková 
                                        
                                            Moje miesto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Vlčková 
                                        
                                            Ako zmeniť svet
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Vlčková 
                                        
                                            Šťastie onkologického pacienta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Veronika Vlčková 
                                        
                                            Lot of zajíce, lot of slepice, you know....
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Veronika Vlčková
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Veronika Vlčková
            
         
        veronikavlckova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Psychoštudentka. Pozorovateľka. Degustuje samostatný život.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    120
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1785
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Priv(i)ate
                        
                     
                                     
                        
                            Vietor v hlave
                        
                     
                                     
                        
                            Kam ma odvialo
                        
                     
                                     
                        
                            Píšem svetlom
                        
                     
                                     
                        
                            A iné (vý)plody
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Madeleine Peyroux
                                     
                                                                             
                                            Beirut
                                     
                                                                             
                                            Raul Midón
                                     
                                                                             
                                            Dan Bárta
                                     
                                                                             
                                            Buena Vista Social Club
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Strachanovci
                                     
                                                                             
                                            The road is home
                                     
                                                                             
                                            Pája cestuje
                                     
                                                                             
                                            Míša v Rusku
                                     
                                                                             
                                            Zina
                                     
                                                                             
                                            Anna
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hviezdičkujem filmy
                                     
                                                                             
                                            pán fotograf
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




