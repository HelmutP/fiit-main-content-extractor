
 Vzduch vonia ako jeseň. Tá jeseň, keď som svoje sny vložila do tvojich rúk. Niečo sa zmenilo. Veď už kvitnú prvosienky. Šťastie je tak krehké. Prekĺzne ti pomedzi prsty a ty márne vystieraš dlane.    Vidím analógiu a tá ma núti premýšlať. 
