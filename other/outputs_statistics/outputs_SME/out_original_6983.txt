
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Janka Štefková
                                        &gt;
                Nezaradené
                     
                 Čím užšia, tým lepšia! 

        
            
                                    21.5.2010
            o
            18:42
                        (upravené
                21.5.2010
                o
                19:36)
                        |
            Karma článku:
                4.95
            |
            Prečítané 
            1211-krát
                    
         
     
         
             

                 
                    Výlet sme si dohodli na štvrtok. Po raňajkách nás Denisa berie autom na vraj  najkrajšiu a najčistejšiu pláž. No, som zvedavá. Dúfam, že nepresedím pol dňa v aute pre kus farebného piesku. "Pláž sa volá Elafonissi a je tam ružový piesok. Je to síce ďaleko, cesta dosť blbá, ale stojí to za to."
                 

                     Denisa má, žiaľbohu pravdu. Cesta je príšerná. Hoci ideme niekoľko desiatok kilometrov po krajšej ceste ako sú naše diaľnice, po čase sa každá krása končí. Prechádzame serpentínami po otrasnej úzkej ceste. Keď ide oproti autobus, Denisa musí zájsť s autom na kraj. Pod nami sa rozprestiera roklina. Otrasná, veľká, katastrofálne strmá a hlboká. Aj napriek strachu, ktorý ma zviera ako železná ruka musím uznať, že krajina je tu  fakt fascinujúca. Míňame krásne domčeky. Skoro na každom sú solárne sklá. Všade sú kvety, kvitnú citronovníky. Proste romantika ako má byť.   „Tam na tých stromoch rastnú olivy," ukazuje Denisa. „Z olív sa tu vyrábajú oleje a kadečo iné. Olivy sa pridávajú do mnohých vecí. Nájdete ich v mydle aj v opaľovacom mlieku." Ty kokso, aká múdra. Čo nám chce robiť celú cestu prednášku? Od zédeešky predsa viem, na čo je olivovník. No, ale dobre, presne toto potrebujem počúvať, aspoň nemyslím na to, že sa mi z tej hnusnej cesty chce vracať.   „Zastaneme tu pri babičke, ktorá predáva domáci med," oznamuje nám a o chvíľu už brzdí pred malým domčekom, pred ktorým na lavičke pri stole sedí staršia žena a jeden muž. Zrejme manžel. Dosť starý a dosť ohnutý. Ochutnávame to ich sladidlo od včiel. Musím uznať, je naozaj  vynikajúce. Sarah a aj ja si kupujeme čistý med, teda taký ničím ochutený. Viktória berie pre Igora akúsi vraj super pálenku. Je chutná, lebo ju ako správna gurmánka na dovolenke ochutnávam. Potrebujem alkohol, normálne ľutujem, že som si nevzala tľapku. Pri takej debilnej ceste by som si zaslúžila. Aspoň dve deci, ak nie celý polliter. Po chvíli sa opäť sa vyberáme na cestu. Vchádzame do tunela. Najskôr však musíme počkať, kým oproti prejde autobus s turistami. Tunel je neuveriteľne úzky. Pozorovať manévrovanie šoféra je príšerné. Musí byť šikovný, aby sa do úzkeho priestoru vopchal bez toho, aby poškodil autobus. Ďakujem Bohu, že v ňom nesedím. „Naozaj to vyzerá nebezpečne, ale nie je to tak," ukľudňuje nás Denisa, keď ju zasypeme otázkami, či je takéto prechádzanie normálne. Prikývne a mňa oblieva pot. Zas ľutujem, že som si pred cestou niečo riadne nehrkla. Znášala by som to lepšie. Tá ochutnávka u sladkej babičky s medom bola pre mňa málo. Teraz mi je všelijako len nie dobre. Samozrejme, slušáčky, Sarah a Vikina to znášajú dobre. Ony alkohol nepotrebujú vôbec. Keby sme všetci boli takí ako ony, výrobcovia alkoholu by na Slovensku skrachovali. A to nechcem. Potrebujeme  predsa podporovať výrobu slovenských výrobkov, no nie? A alkohol nevynímajúc.   „To sa vôbec nebojíte, že sa zrútime do tých priepastí?- pýtam sa s nervami.   „Prečo by sme na to mali myslieť? Ukľudni sa a cestuj s úsmevom," povie Vikina a nadchýňa sa nad priepasťou. Ona fakt nie je normálna.  Bože, keby sa radšej nadchýňala nad peknými Grékmi. Ale nie, ona radšej ostáva verná tomu svojmu ženáčovi. Žiarlivcovi a idiotovi v jednom.„Pozri na tú hĺbku, to je nádhera, fascinujúce," vykrikuje v čase, keď mne je už takmer na omdletie.   „Ty nie si úplne normálna! V jednom kuse musíme stáť na krajnici a púšťať protiidúce autá. Čo je na tomto preboha, fascinujúce?" rozčuľujem sa a ľutujem, že som sa na toto dala nahovoriť. Moje milované lehátko pred hotelovým bazénom, kde si?   Situáciu sa snažím ako - tak zvládať, ale keď mám pocit, že autobus sa nám obtiera o karosériu, začínam sa modliť. Najskôr v duchu a a potom už aj nahlas. „Ó, aká pobožná," rehoce sa Sarah „Len ju nechaj, o chvíľu sa bude modliť v kostole," povedala Denisa a skutočne o niekoľko minút sme už vchádzali do akéhosi chrámu. Ja tieto pamiatky veľmi nemusím, skôr som sledovala more po ľavej strane, ale dobre, uznávam, aj kostol  či čo to bolo, bol úchvatný. Aj sprievodca, ale len dovtedy, kým som nezačala fotiť. Však na dovolenke sa fotí, no nie?  Dačo hovoril po grécky, ale keďže som mu nerozumela a  „delegátka" Denisa bola kdesi vzadu, len som sa prihlúplo usmievala a ďalej cvakala spúšťou. Sprievodca začal potom hroziť prstom, čo z duše neznášam. „Tu nemôžeš fotiť," vysvetlila mi Denisa, ktorá sa zrazu zjavila predo mnou.   „Hej, a preč? Veď som dala skoro desať eur vstupné."    „To je síce pravda, ale aj tak nemôžeš," povedala a keďže ma už ako - tak pozná, pre istotu mi radšej hneď aj zhabala foťák. Super kamoška. Normálne si ju pridám na facebooku za priateľku. Ešte že som k tomu  foteniu ako bonus nedostala nejakú pokutu, lebo by ma porazilo.   Z chrámu je na spomínanú pláž len pár metrov. To, čo zrazu vidíme nám vyráža dych. Čistá, nedotknutá príroda a jemný žlto - ružový piesok nás fascinuje. Prechádzame vodou, ktorá nám siaha len po kolená. Baby - tie moje sú pomalé, my s Denisou sme sa do  plaviek obliekli už doma a teraz sme zo seba len zhodili šaty a poďho do vody. Kým sa Viki a Sarah prezlečú, som vo vode ponorená dobrých pätnásť minút. Neskôr zabehneme do stánku po nejaké jedlo a pitie, ale inak sme stále vo vode. Cvakáme foťákmi ako o dušu, smejeme sa ako šialené. Fakt je tu všetko  tak úchvatné, že som naozaj presvedčená, že by som tu dokázala žiť.   Jedine, čo nám, alebo konkrétne iba mne  trochu kazí radosť, je pomyslenie, že o dva dni tu už nebudeme. „Neprídeme tu nikdy," skonštatujem smutne.   „Možno pôjdeme niekde inde," povie Viktória.   Silne o tom pochybujem. Kúpila som si byt, a to dosť drahý. Práve ho rekonštruujem a do toho ešte táto dovolenka. Ani s Vikinou to nevyzerá najlepšie. Igor jej podľa mňa, hneď ako začnú spolu bývať urobí dieťa. Aj napriek tomu, že je otrasný žiarlivec, Vika sa ho ale aj napriek tomu  nechce vzdať. Takto si ju poistí a bude ju mať doma. Takže dovolenka je v nedohľadne, ale nenechám si kaziť krásnu náladu myšlienkami na to, čo nás čaká po návrate doma. Bohviečo to nebude, to viem iste.   A ani so Sarah so dobre nevyzerá. Chce si v Španielsku, kde býva otvoriť ďalší krajčírsky salón a zamestnať niekoľko žien. Na pláži ostávame takmer do večera. Potom sa znova  najeme v bufete( nejem nejako veľa?) a ideme naspäť. Denisa nám ukazuje, čo všetko vidíme, robíme si prestávky na fotenie. Do hotela prichádzame o deviatej. Zničené.  Gréci sa tvária mimoriadne pohostinne, lebo aj napriek pokročilej hodine na nás čaká riadne veľká večera.   Dojeme a ja prehováram baby, aby sme si šli ešte pofotiť večerné more. Už sa riadne zotmelo, ale aj tak.   „Ide sa do baru, tak ako včera," - zavelila som a vybrala jeden z preplnených barov.   „Je tu veľa ľudí",- frfle Viki.   -No a čo? Mám to tu dať vyprázdniť? Nevymýšľaj a sadaj."    Prichádza čašník a keď zistí, že sme Slovensky, pijeme grátis. No teda, byť slovenskou ženou v Grécku je asi celkom výhodné. Dávam si pivo a baby to hnusné  pelendrekové ouzo. Potom otočíme ešte dve kolá a ja sa začínam dôslednejšie obhliadať po okolí. Teda po chlapoch, čo sedia pri stoloch. Ženy sa snažím prehliadať.   „Pozri sa na toho pri stole,"- ukazujem na celkom zaujímavý kúsok pri vedľajšom stole.   -Máš doma frajera moja, nezabúdaj."    -No a čo? Je ženatý a tento je naozaj chutnučký.   „Možno je tiež ženatý a k tomu mu na mozog a aj niekde inde  pôsobia horúce slnečné lúče,"- uzemňuje moje chuťky po dobrodružstve Sarah.   "-Ale baby, neštvite ma, to si tu neužijeme ani trochu zábavy?"- pýtam sa vysielam ku môjmu fešákovi žiarový úsmev. Asi aj celkom zvodný, lebo nám posiela po drinku. Ale tým to končí. Postaví sa odchádza.   „No vidíš, nejak ti to nevyšlo, ale nebuď smutná, možno niekoho zbalíš zajtra v lietadle". Ja tu Sarah zabijem.   Idem na vécko a keď prídem, mám pocit, že som v krúžku esemeskujúcich. Baby,  presne ako aj všetky dni a večery predtým, aj teraz vypisujú esemesky. Musia tým svojim podávať hlásenia. Mňa by asi porazilo. Môjmu som napísala pár správ, že je pekne a teplo a to je všetko. Ale baby píšu takmer v kuse. Vikina, keď drahému neodpíše do desať minút, hneď je urazený a vypisuje jej také správy, že vzápätí hádže mobil do piesku. Nuž, čaká ju krásne spolužitie. Môj nie je žiarlivý, ale možno preto, lebo ma tak nemiluje. Alebo áno? Neviem. Možno by som niekedy radšej chcela byť sama. Najviac ma štve, že vlastne sama neviem, čo chcem.       Pokračovanie čoskoro             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Štefková 
                                        
                                            Brigádoval  som v pekárni  – Do strúhanky zomleli aj prepravku !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Štefková 
                                        
                                            Zdravotnícke či plynové komory?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Štefková 
                                        
                                            Zdravotnícke či plynové komory?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Štefková 
                                        
                                            Zdravotnícke či plynové komory?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Janka Štefková 
                                        
                                            Dobrá dovolenka v Tatrách: Existuje niečo také?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Janka Štefková
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Janka Štefková
            
         
        stefkova.blog.sme.sk (rss)
         
                                     
     
        Mám rada ľudí, spoločnosť,rada cestujem spoznávam cudzie krajiny, milujem literatúru a zvlášť Maxima. E. Matkina
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1268
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Brigádoval  som v pekárni  – Do strúhanky zomleli aj prepravku !!!
                     
                                                         
                       Zdravotnícke či plynové komory?
                     
                                                         
                       Zdravotnícke či plynové komory?
                     
                                                         
                       Zdravotnícke či plynové komory?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




