

 Počty vynechaných hlasovaní v NR SR:  
 1.) Ján Slota (SNS):                  3572 
 2.) Ľuboš Micheľ (nez.):            3327 
 3.) Vladimír Mečiar (HZDS):       2399 
 4.) Vladimír Palko (KDH):           1836 
 5.) Pál Csáky (SMK):                  1669 
 6.) Sergej Chelemendik (SNS):  1638 
 Zdroj: Hospodárske noviny, 4.1.2010,  
   
 Mimoriadne zaujímavým exemplárom v oblasti dochádzky do práce je najmä pán poslanec Ján Slota, ktorý v rebríčku parlamentných absentérov dlhodobo okupuje prvé miesto. Vo volebnom období 1998 - 2002 chýbal dokonca pri vyše 90 percentách hlasovaní. V tomto volebnom období sa podstatne zlepšil, i keď jeho mužná orientácia na výkon mu určite nedá, aby svoje prvenstvo v absenciách len tak ľahko prepustil niekomu inému. K 22.5.2008 chýbal len na 73 percentách hlasovaní v NR a to, musíte uznať, je skutočný posun vpred. 
 Mimochodom, viete o tom, že (ako uviedla svojho času TASR) vo volebnom období 1998 - 2002 patrila medzi najväčších absentérov na hlasovaniach v NR SR aj Anička Belousovová? 
 V týchto súvislostiach je potom zaujímavé sledovať a uvažovať o metódach, aké chce SNS aplikovať pri riešení problémov rómskej komunity na Slovensku. 
 Môžeme dedukovať, že pán Slota v jeho úvahách o tom, ako riešiť rómsku otázku a prínos rómskej populácie pre štátnu kasu, zrejme úplne vylúčil najbežnejšiu učebnú metódu: metódu vlastného príkladu, metódu odpozorovania a napodobovania.  
 Zrejme chce tento problém riešiť formou encyklopedických vedomostí v internátnych školách pre rómske deti.                                                                                                                      
    

