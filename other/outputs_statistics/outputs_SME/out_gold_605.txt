

 Desiatky - tých najväčších a najslávnejších. Priam svetových. Tragédie aj komédie. 
Vybrala som ich zopár aj pre vás. 

  

Ako sa vám páči (William Shakespeare) 

  

Rómeo a Júlia (William Shakespeare) 

  

Staviteľ Sollness (Henrik Ibsen) 

  

Nora (Henrik Ibsen) 

  

Nosorožec (Eugene Ionesco) 

  

Zbojníci (Friedrich Schiller) 

  

Lakomec (Jean-Baptiste Poquelin Moliere) 

  

Ideálny manžel (Oscar Wilde) 

 

 Cyrano z Bergeracu (Emil Edmond Rostand) 


  

Sklený zverinec (Tennessee Williams) 

  

Špinavé ruky (Jean-Paul Sartre) 

  

Tri sestry (Anton Pavlovič Čechov) 

  

Obzri sa v hneve (John Osborne) 

  

Kamaráti (August Strindberg) 


  

Návšteva starej dámy (Friedrich Dürrenmatt) 

  

Modrý vták (Maurice Maeterlinck) 

  

1+1=3 (Ray Cooney) 

 Svetové, ale kedysi aj naše. 

  

Matka (Karel Čapek) 

  

Ze života hmyzu (Josef a Karel Čapkovci) 

  

Bílá nemoc (Karel Čapek) 

  

Osel a stín (Jiří Voskovec a Jan Werich) 

 A nakoniec zopár slovenských. 

  

Sonatína pre páva (Osvald Záhradník) 






  

Pštrosí večierok (Ivan Bukovčan) 



  

Mravci a svrčkovia (Ivan Stodola) 

  

Bačova žena (Ivan Stodola) 



  

Kým kohút nezaspieva (Ivan Bukovčan) 

  
Matka (Július Barč - Ivan) 

  

Nemé tváre (Stanislav Štepka) 

  

Kúpeľná sezóna (Stanislav Štepka) 

 Všetky predstavenia boli odohraté prvého mája 2008. V hlavných úlohách účinkovali obyvatelia zoologickej záhrady v Schönbrunne. 






