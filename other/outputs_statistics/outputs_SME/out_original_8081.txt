
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viera Valentová
                                        &gt;
                Pomimo
                     
                 CouchSurfing v Banskej Bystrici ! 

        
            
                                    5.6.2010
            o
            0:58
                        (upravené
                5.6.2010
                o
                1:21)
                        |
            Karma článku:
                12.40
            |
            Prečítané 
            3554-krát
                    
         
     
         
             

                 
                    CouchSurfing je úžasná komunita ľudí rôzneho veku, národnosti, farby pleti, náboženstva, ktorých spájajú spoločné záujmy ako napríklad ochota ubytovať celkom neznámych ľudí vo svojich príbytkoch, poukazovať im mesto, pohostiť ich a v neposlednom rade cestovať úplne rovnakým, možno trochu bláznivým spôsobom.
                 

                 Banská Bystrica je 6. najväčším mestom Slovenska. Je síce pravda, že západní návštevníci chcú  prioritne navštíviť najmä naše hlavné mesto, ale na moje veľké prekvapenie ich to celkom často ťahá aj k nám na stredné Slovensko. Nasledujúci článok približuje couchsurferov, ktorí prešli cez môj gauč.   Laura a Maarten z Holandska, Utrecht.      Moji prví gaučoví hostia. Prišli na dve noci koncom októbra minulého roka. Kúpili si superlacné ryanarovské letenky do Bratislavy, kde strávili prvú noc. Ráno sa postavili na Zlaté piesky a zdvihli prst. Stopári. To ich asi aj nasmerovalo ku mne. Ja tiež nezatajujem, že stopovanie patrí k  môjmu životnému štýlu. Kontaktovali ma približne týždeň pred príchodom. Zaujímali sa o hudbu akéhokoľvek štýlu a tak sme ich s priateľom vzali na koncert začínajúcich kapiel. Úprimne podotknem, že im sa to páčilo viac ako nám. Nasledujúci deň bol pondelok, čo znamenalo pre mňa školské povinnosti. Zobrala som ich na prehliadku mojej fakulty a potom mali až do poobedia voľný program. Večer sme im s priateľom ukázali jednu štýlovú bystrickú čajovňu.   Dnes spolu príležitostne prehodíme pár slov cez sociálne siete. Boli mojimi prvými gaučovými hosťami. Týmto spôsobom už prechodili polovicu Európy a samozrejme, že sa im to veľmi páči.       Martí zo Španielska ( Katalánska ), Barcelona.      Martí ma kontaktoval tiež približne týždeň pred plánovaným príchodom. Prespal u nás počas dvoch nocí koncom novembra minulého roka. Vo februári 2009 sa vydal na cestu východným smerom. Začal v Taliansku a postupne prešiel všetky balkánske krajiny, tie medzinárodné uznané a aj tie ostatné. Potom sa cez Ukrajinu presunul do našej strednej Európy. Cestoval prevažne verejnou dopravou, aj keď miestami skúšal aj stop. Počas dní strávených u nás bolo síce extrémne zlé sychravé a daždivé počasie, ale aj tak som mu dala prehliadku mesta a  šli sme sa pozrieť na moju fakultu. Keď začalo príšerne liať, skryli sme sa do útrob študovne vedeckej knižnice, čo je jedno z mojich najobľúbenejších miest. Žiadnej z pracovníčok nevadilo, že ani ja ani on sme pri sebe nemali preukazy. Jednoducho sme si tri hodinky listovali v knižkách. Večer sme boli pozvaní na halušky k mojej krstnej mame. Takto Martí ochutnal aj naše národné jedlo a celkom mu chutilo. Pôvodne plánoval skončiť svoju cestu po polroku putovania, keď však zistil, že si v rámci finančných možností môže dovoliť pokračovať tak  nakoniec strávil na cestách 14 mesiacov a dnes je už naspať doma v Barcelone. Takisto neustále udržujeme kontakt cez internet.       Florien a Stephanie z Belgicka, Roeselare.      Tieto zlaté babenky boli prvými couchsurfermi, ktorých ubytovala moja kamoška Majka. Chceli jednoducho ísť na nejaký eurotrip a letenka na Slovensko vychádzala najlacnejšie. Takto sa ocitli v Banskej Bystrici v polovici februára. Pôvodne mali prísť tri a chceli si prenajať auto priamo na letisku v Bratislave a týmto spôsobom sa prepraviť do Bystrice. Nastali však mierne komplikácie, na letisku im nechceli zobrať platobnú kartu atď. Najprv tu zamýšľali ostať len jednu noc, ale keď im Majka vysvetlila, že v pondelok ( deň ich príchodu ) sa nekonajú nijaké študentské zábavy a bolo by to vskutku nerozumné opustiť Bystricu bez možnosti spoznať pravú študentskú veselicu, tak pristali a odišli až v stredu. Z neznámych príčin dostali nenormálnu chuť na lyžovanie, tak si šli vyskúšať druhýkrát v živote aké je to byť na svahu. Našťastie sa to zaobišlo bez zlomenín :-)       Benjamin a Dennis z Nemecka, Heidelberg.      Títo dvaja nemeckí chalani ma kontaktovali len jeden deň pred svojím príchodom. Keďže milujem spontánne akcie, samozrejme som ich prijala. Prišli vlakom z Nemecka a pred Banskou Bystricou už strávili jednu noc v hosteli v Bratislave.  Boli vegetariáni, čo som však dopredu nevedela, lebo to nemali v profiloch a tak som to mäso určené na večeru zjedla sama. Oni samozrejme na túto situáciu boli pripravení a spoločne si v našej kuchyni ukuchtili chutné zemiakovo jablkové neidentifikovateľné čudo. Taktiež sa ich pobyt nezaobišiel bez povinnej prehliadky mesta a našej školy. Na študentskú párty sa tentoraz nešlo. Už z toho vyrástli. Nasledujúci deň sa vybrali na výlet do Štiavnice, kde sa im náramne páčilo, aj keď nikto v informačnom stredisku nehovoril žiadnym cudzím jazykom zato však po ceste stretli kopec nemecky hovoriacich ľudí, ktorí im radi všetko vysvetlili. V tom období som bola veľmi vyťažená školou a asi to bolo na mne aj patrične vidieť. Za iných podmienok nedovoľujem nikomu cudziemu u nás doma umývať riady, ale im som to  jednoducho nebola schopná zakázať.       Julia z Brazílie, teraz žijúca v Británii, Londýn.      Táto energická žena ma kontaktovala približne dva mesiace pred plánovaným príchodom dlhočiznou správou. Keďže nepoužíva na cestách mobil a potrebuje mať všetko vopred naplánované a premyslené, už pred jej príchodom sme si vymenili kopec informácií, skrátka keď prišla, už to nebola cudzia osoba. Vo všeobecnosti cestuje sama a s couchsurfingom má iba tie najlepšie skúsenosti. Bola nesmierne fascinovaná hradmi a zámkami, preto prvý deň smerovala do Slovenskej Ľupče a druhý deň šla do Bojníc. Veľmi rada a dlho vedela rozprávať o všetkom možnom aj nemožnom. Niekedy som na ňu naozaj nestačila :-). Dnes spolu sem tam komunikujeme cez maily, keďže ona sociálne siete nepreferuje. Už viem u koho mám dvere otvorené v Londýne.       Maxime z Francúzska, Chalon-sur-Saone.      Maxime ma kontaktoval práve vtedy, keď sme sa vracali s mojím priateľom z veľkonočného nórskeho výletu ( samozrejme sa aj tento náš výlet niesol v znamení couchsurfingu ). Prišiel o pár dní po našom príchode na Slovensko. Celý čas čo som súčasťou tejto úžasnej komunity som snívala o tom, aby prišiel aj niekto z francúzsky hovoriacej krajiny, aby som nemusela neustále len spíkovať. Tak konečne prišiel Maxime. Podobný prípad ako Martí, ktorého som hostila pred necelým polrokom. Po našetrení dostatočných finančných prostriedkov sa vydal na cestu okolo sveta. Začal najprv obchádzať couchsurferov, ktorých on ubytoval u seba vo Francúzsku a potom sa vydal kade kde. Keďže mal so sebou aj notebook s množstvom fotiek, nechala som sa unášať jeho cestovateľskými zážitkami a aspoň na chvíľku som zabudla na moje začínajúce skúškové obdobie. Práve v tom čase mal u nás v škole prednášku francúzsky veľvyslanec na Slovensku, kde som ho zavolala. S Maximom sme sa zúčastnili pravej študentskej intrákovej narodeninovej oslavy. Síce si tam pripadal mierne starší ( čo aj bohužiaľ bola pravda ) zábave to nebránilo. Ten večer sa takisto konal koncert mojej obľúbenej kapely Para v jednom veľkom bystrickom klube, kde sme takisto nechýbali. Dnes pozorne sledujem všetky jeho cestovateľské kroky na jeho blogu. Naposledy písal z Rigy.   Couchsurfing je squellý, smelo vyskúšajte ! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Valentová 
                                        
                                            Ako sa dostať na festival Pohoda zadarmo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Valentová 
                                        
                                            Prečo a ako ísť na Európsku dobrovoľnícku službu? (napr. do Nepálu)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Valentová 
                                        
                                            Zimný outdoorový kurz v Lotyšsku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Valentová 
                                        
                                            Čriepky z Istanbulu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Valentová 
                                        
                                            Ako sa CouchSurfuje a užíva v Mexico City !
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viera Valentová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viera Valentová
            
         
        valentova.blog.sme.sk (rss)
         
                                     
     
        Celkom obyčajné dievča, ktoré podľa zaužívaných štandardov vysoko pravdepodobne pochádza z inej planéty. Úspešne infiltrovaná do spoločnosti bežných ľudí, dokonca v minulosti zaškatuľkovaná ako vysokoškolský študent !

Možete ma nájsť aj tu: http://tripsurfing.sk/wjerocka


pre viac nekonvenčných a afektovaných príbehov, ktoré by sa nehodili na seriózny blog mi stačí napísať :-)

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    42
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1755
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Pomimo
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            GusGus
                                     
                                                                             
                                            Jamie Woon
                                     
                                                                             
                                            The Black Keys
                                     
                                                                             
                                            Battles
                                     
                                                                             
                                            Damabiah
                                     
                                                                             
                                            Bonobo
                                     
                                                                             
                                            Parov Stelar
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Brat píše
                                     
                                                                             
                                            Emily a náš izraelský výlet
                                     
                                                                             
                                            Alexandra Šupolová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Môj profil na FaceBooku
                                     
                                                                             
                                            Môj profil na CouchSurfingu
                                     
                                                                             
                                            Dobrovoľníctvo-farmy -WWOOF
                                     
                                                                             
                                            Dobrovoľníctvo-farmy a hostely
                                     
                                                                             
                                            Wikipedia pre stopárov
                                     
                                                                             
                                            CouchSurfing !
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo a ako ísť na Európsku dobrovoľnícku službu? (napr. do Nepálu)
                     
                                                         
                       Zimný outdoorový kurz v Lotyšsku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




