

 
Dadaizmus, umelecko-kultúrne hnutie z počiatku dvadsiateho storočia, je, aj napriek tomu, že pôsobilo krátko a mnohými bolo označené za nezmyselné, zdegenerované a zbytočné, jedným z najdôležitejších avantgardných smerov. Jeho myšlienková línia sa totiž vinie celým dvadsiatym storočím, cez surrealizmus a nový realizmus, po konceptuálne umenie a neodadaizmus, a ako súčasť postmoderny ovplyvňuje naše myslenie podnes.
 
 
Hoci sa dadaizmus primárne spája s výtvarným umením (Duchampove ready-mades) a literatúrou (Tzarova náhodná montáž slov), tvorí aj neoddeliteľnú súčasť filmového umenia.  
 
 
Dadaizmus, ako hnutie, vzniklo počas Prvej svetovej vojny, a to najmä, ako reakcia na jej hrôzy a nezmyselnosť. Najzákladnejšou myšlienkou hnutia sa stalo absolútne popretie vtedajších hodnôt, či už spoločenských, morálnych alebo kultúrnych, ktoré sa tvárou v tvár hrôzam vojny, zdali nezmyselné, ba dokonca, za ňu zodpovedné. Z toho dôvodu sú pre hnutie charakteristické pojmy ako anti-umenie alebo anti-estetika. Dadaisti sa totiž rozhodli postaviť absolútne voči všetkému, dokonca proti samotnej podstate umenia, ktorou bolo vytváranie krásna a ľudí znechutiť, vyburcovať v nich odpor.
 
 
Všetky tieto charakteristiky sa premietli do filmu Marcela Duchampa Anemic Cinema z roku 1926. Sedemminútový nemý čierno-biely záznam môžeme pokojne označiť za anti-film. Jeho podstatou je striedanie záberov na rotujúce disky s Duchampovými špirálami a nezmyselných fráz, nalepených na rotujúcej kruhovej ploche. Okrem toho, že ide o zábery natočené kamerou, má tento záznam ešte jednu vlastnosť filmu, a tou je strih. Tým však podobnosť z klasickým filmom končí a my sa dostávame na územie anti-filmu, ktorý nemá postavy, ani dej, ktorý nezobrazuje realitu, ani žiadne konkrétne predstavy, ktorého cieľom nie je vytvorenie estetickej alebo morálnej hodnoty. Naopak jeho cieľom je znechutiť, odradiť, vzbudiť odpor a pohoršenie, alebo len jednoducho zosmiešniť a pobaviť.  
 
 
A v tom je film nadmieru úspešný. Striedajúce sa hypnotické obrazy nielenže neobsahujú žiadnu výpovednú hodnotu, ale ich pozorné sledovanie môže viesť až k fyzicky nepríjemným zážitkom ako je nevoľnosť, pocit sťaženého dýchania a, u jedincov s predispozíciou, dokonca k epileptickému záchvatu. 
 
 
No napriek tomu všetkému, napriek zdanlivej nezmyselnosti a obsahovej prázdnote, je dielo nositeľom myšlienky. Anemic Cinema je, vo svojom popieraní, symbolom straty viery v ľudský svet, v morálku, ktorá dospela k vojne, humanizmu, zašliapanému do blata delostreleckých zákopov, symbolom prázdnoty, ktorým pôsobí akékoľvek umelecké snaženie, konfrontované z realitou biedy a nezmyselného utrpenia miliónov ľudí. Je však zároveň autorovým dištancom od týchto hodnôt, jeho výsmechom a silnou kritikou spoločnosti.
 
 
Chtiac-nechtiac tak dielo nesie výpoveď a Duchamp sa od dadaizmu dostáva ku konceptuálnemu umeniu, ktorého zárodok v sebe dadaizmus celý čas nosí, do umeleckého poľa, kde stačí iba jediný skutok, gesto alebo dokonca aj samotné nekonanie, na to, aby sme vyjadrili myšlienku a tú premenili na umelecké dielo. Vzniká tak dadaistický paradox, pretože snaha o popretie významovosti, vytvára významovosť novú a tým pádom popiera samu seba. Duchamp tým však len potvrdzuje základnú ideu dadaistov, že pravý dada nie je dada.  
 
 
V šesťdesiatych rokoch sa pokračovateľmi dadaistickej línie stali členovia medzinárodného umeleckého zoskupenia Fluxus. Z pôvodnej ideovej základne si, ako hlavný nástroj svojej výpovede, zobrali práve popieranie, a tiež charakteristický dadaistický zmysel pre humor. Avšak na rozdiel od pôvodného hnutia, ktoré vzišlo ako reakcia na bolesť a zúfalstvo vojny, sužujúcej spoločnosť, revolta neodadaistov paradoxne smerovala opačným smerom.
 
 
Rýchlo rozvíjajúca sa  americká povojnová ekonomika spôsobila obrovský konzumný boom. Ameriku ovládla spotreba, ľudia upadali do kolobehu nakupovania a z neho prameniacej povrchnej spokojnosti. Stali sa apatickými voči ideálom, za ktoré bojovali, stratili záujem o verejný život, zľahostajneli.  
 
 
A reakciou na takúto spoločenskú situáciu je film Nam June Paika, Zen For Film. Niekoľko minútové nemé čierno-biele dielo je formálne podobné Duchampovmu, no v snahe o popretie filmovosti ide ešte ďalej, keď sa úplne vzdáva strihu a pohybu. Film je iba dlhým statickým záberom na biely obdĺžnik, položený na čiernom pozadí.
 
 
V tomto kontexte je Zen For Film akýmsi absolútnym výtvorom dadaizmu, ktoré v porovnaní s produktmi hollywoodskych tovární, vyrábajúcich sny, ktoré si za pár centov mohol kúpiť každý, vyznieva absurdne. Stáva sa z neho do očí bijúci výsmech celého amerického pozlátka, rozkladajúci mýtus amerického sna, a rovnako ako u Duchampa, je nástrojom autorovej vzbury a verejného odmietnutia stavu spoločnosti. 
 
 
Napriek tomu, že navonok pôsobí dielo ako vtipná dadaistická hračka, skrýva v sebe koncepčnosť, ktorá ho odlišuje od Anemic Cinema. Tá tu totiž nie je skrytá a náhodná ako u Duchampa, ale je presne cielená a dielo samotné predchádza, nie je iba jeho sprievodným javom.
 
 
Autor nám práve v názve dáva kľúč k ďalším vrstvám svojho diela. Zen For Film nás odkazuje k náboženskému smeru Zen, odnoži budhizmu, ktoré do Ameriky priniesli z východu práve umelci, a ktoré bolo v tej dobe populárne. Podstatou Zenu je snaha o odhodenie všetkých naučených a vyčítaných názorov a postojov a dopátraní sa k pravde cez priamu skúsenosť. Zen je teda náboženskou analógiou myšlienok dadaizmu. Paik, ktorý pochádza z Južnej Kórei, no ktorý žil a pracoval v Spojených štátoch, pomocou svojho filmu preklenuje priestor medzi východnou a západnou kultúrou, hľadá styčné plochy, snaží sa dopátrať univerzálnej pravdy, tomu, čo je spoločné všetkým ľuďom, a tým dáva nový rozmer humanizmu. 
 
 
V ďalšom pláne sa na Zen For Film môžeme dívať ako na vizuálny koán. Koán je zdanlivo nevyriešiteľná úloha, javiaca sa ako paradox alebo lingvisticky nezmyselná otázka, ktorá sa kladie žiakom Zenu, no ktorej pochopenie vedie k odhaleniu podstaty a videniu sveta v jeho pravej podobe. Úlohou koánu je rozbiť klasické naučené myslenie žiaka a obrátiť ho k pravde. Zen For Film tak funguje aj ako náboženská pomôcka.
 
 
V neposlednom rade môžeme film vnímať ako odkaz na suprematizmus, vtipnú reinterpretáciu  Malevichovho Čierneho štvorca. 
 
V porovnaní s Anemic Cinema je Zen For Film, napriek zachovaniu dadaistickej formy a jeho myšlienkového základu, konceptuálnym dielom, zasahujúci svojou vrstvovitosťou až do postmoderny. 
 
V šesťdesiatych rokoch však  proti establišmentu bojoval ešte iná a azda aj známejšia skupina umelcov ako Fluxus, Beat generation. Jeden z jej najznámejších členov William S. Burroughs sa na svojich potulkách Európou zoznámil s Brionom Gisinom a ten ho zasvätil do techniky Cut ups, ktorú vymyslel, a ktorá funguje podobne ako Tzarovo ťahanie slov z klobúka. Burroughs mal už v tom čase skúsenosť z nelineárnym rozprávaním v podobe Nahého obeda, a tak ho táto technika zaujala a sám s ňou začal experimentovať. V tom období stretáva aj anglického filmára Antonyho Balcha, s ktorým začína tvoriť experimentálne filmy.
 
 
V roku 1967 mal v Londýne premiéru ich The Cut-ups. Reakcie na film sú ako vystrihnuté z manifestu o dadaizme.  Niektorý diváci tvrdili, že sa im počas filmu urobilo nevoľno, iný žiadali o navrátenie vstupného a ďalší film hodnotili ako niečo nechutné. 
 
 
Základ filmu tvorí materiál zozbieraný počas niekoľkých rokov, ktorý mal slúžiť ako dokument o Burroughsovom živote. No autori film na základe techniky cut-ups rozstrihali a náhodne zoradili. Výsledok je tak zaujímavý experiment, ktorý sa od Anemic Cinema a Zen For Film vizuálne značne odlišuje. Zatiaľ čo tieto filmy postupovali technikou potlačenia vlastností charakteristických pre film, The Cut-ups využívajú filmovú reč v plnej miere. Rozstrihané zábery sú časťou dokumentárneho filmu, rozprávajúceho klasickou filmovou rečou. Dadaizmus sa tu preto prejavuje inak, a to, podobne ako v literatúre, búraním syntaxe a deštrukciou filmovej reči. Zvuk a strih sa stávajú najdôležitejšími nástrojmi ich výpovede. Vo zvuku sa striedajú nastrihané repliky slov a ich mechanické opakovanie niekoľkonásobne zvyšuje hypnotický charakter filmu. Spolu s so strihom vytvárajú akúsi abstraktnú dramaturgiu, jedinečný rytmus, ktorého tempo smerom ku koncu narastá a v úplnom závere sa prepadá späť. Strihovou skladbou, v ktorej sa vysokou frekvenciou náhodne kombinujú lineárne reálne zábery, dosahujú prelínanie obsahov. Jednotlivé línie pôvodného filmu je totiž možné do istej miery vypozorovať, dokonca je tam rozpoznať niekoľko scén z Nahého obeda, ktoré si Burroughs zinscenoval, a tie akoby jedna z pod druhej presakovali do vedomia.
 
 
Napriek použitiu rozdielnej technológie, The Cut-ups sa v ideovej rovine stretávajú z Anemic Cinema a Zen For Film, pretože jeho filmové nástroje sa stávajú prostriedkom na rozbitie zavedeného spôsobu myslenia a revoltou voči naučenému spôsobu vnímania. Zároveň je film pokračovaním Burroughsovej snahy o modifikáciu ľudského vedomia skrz umenie, jeho večným hľadaním novej komunikácie, ktorá by umožnila pochopiť podstatu sveta. Burroughs sa tak spolu s Balchom stávajú, napriek tomu, že to nikdy nemali v pláne, učiteľmi Zenu, zadávajúci svojim žiakom koán. 
 
 
The Cut-ups samozrejme fungujú, rovnako ako Zen For Film, okrem dadaistickej roviny aj ako konceptuálne dielo a odkazy na Burroughsou  Nahý obed a realitu pôvodného dokumentu doňho vnášajú prvky postmoderny.
 
 
Dadaizmus sa teda, napriek krátkosti svojho trvania, zaradil medzi najvplyvnejšie a najsilnejšie prúdy v umení dvadsiateho storočia a vďaka svojej transformácii do iných smerov nás ovplyvňuje dodnes. Hoci sa v počiatkoch javil v umení ako slepá ulica, vedúca iba k deštrukcii prázdnote a nezmyselnosti, opak bol pravdou. Dadaizmus svojou deštrukciou otvoril nové cesty mysleniu, zničil zaužívané nahliadanie na kultúru estetiku a morálku a posunul ľudské chápanie vpred. 
 
 
 
 
 
Filmy 
Anemic Cinema - http://www.ubu.com/film/duchamp_anemic.html 
Zen For Film - http://www.ubu.com/film/fluxfilm.html 
The Cut-ups - http://www.ubu.com/film/burroughs_cut.html 
 
 
 

