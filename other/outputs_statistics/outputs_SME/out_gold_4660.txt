

 Táto Tánina je skôr literatúrou faktu. Opisuje v nej chronologicky svoje zážitky z drogového obdobia a jej „kariéru“ prostitútky. Obidve knihy mali krst v Zrkadlovom háji v Petržálke, kde som roky chodil na Protidrogové fóra.   Knihu sme krstili bublifukom. To sa mi zdalo byť najprimeranejšie vzhľadom na podtitul knihy „Sprchovanie jednej zašpinene duše“. Teda mydlovou vodou sme poslali do života toto dielko. Bublifuky sme rozdali aj medzi obecenstvo a v momente krstu sa v sále vznášalo množstvo farebných bubliniek, ktoré vo farebnom osvetlení vyzerali veľmi krásne. K tomu všetkému hrala dobrá muzika a celé to tam vyzeralo podľa mňa úžasne.   Prišlo tam plno našich detí, niektorí prišli aj s rodinami, niektorí len sami a tiež prišli rodičia dievčaťa, ktoré sa vrátilo naspäť k drogám.   Kto by sa chcel pozrieť na fotky z tejto akcie kliknutím na obrázok sa prepne na galériu zloženú z fotiek našich detí, ktoré tam boli.      Kto by si chcel knihu kúpiť, tak tu je jeden tip: 

   




