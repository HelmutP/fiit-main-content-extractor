
 Pred sebou vidíš púšť, tak hroznú púšť. Čo tu mohlo zanechať tak spalujúcu spúšť!?! Pot na tele nechávastopy slané, potkneš sa a tvoja kosť sa láme.  Slnko, to slnko, ktoré ti kožu na tele praská, to slnko ťa vysúša a prehĺby sa každá vráska, s tvojej tváre je len mŕtva maska,  opúšťa ťa štastie, život, nádej i láska.  Zem vyprahnutá, slza vyronená a hneď stratená. 
