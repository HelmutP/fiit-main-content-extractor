
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Nezaradené kadečo
                     
                 Dve tváre centra mesta 

        
            
                                    16.6.2010
            o
            17:55
                        (upravené
                16.6.2010
                o
                18:34)
                        |
            Karma článku:
                6.40
            |
            Prečítané 
            1166-krát
                    
         
     
         
             

                 
                    Tieto riadky píšem len preto, lebo som zvedavá na vaše skúsenosti. Chcela by som vedieť, či takto to funguje aj v tom vašom meste. Ak si nájdete čas na vyjadrenie sa v diskusii, budem vám vďačná.
                 

                 Kráčam mestom, teda, aby som to upresnila - kráčam centrom mesta Martin, po jeho pešej zóne. Kto pozná mesto, vie, že nejde o nejakú veľkú plochu, je to také, "komorné", za pár minút prejditeľné územie.   Kontrolujem čas, moje hodinky ukazujú 11 hodín 10 minút. Sprevádzajú ma tancujúce kvapky dažďa a môj dáždnik (čakajúci na výsluhový dôchodok) sa hompáľa vo vetre. Chvíľami podľahne  jeho náporu a vyvráti sa. Obraciam ho do protismeru a tak sa poslušne vracia sa do použiteľného stavu. Dnes to ešte zvládol, no myslím, že ma dnes sprevádzal posledný raz, cha.   Kráčam si teda mestom, mojim cieľom je návšteva knižnice. Kráčam, obzerám výklady, ľudí, keď zrazu počujem za sebou akosi veľa detských hlasov. Zvedavosť mi  otočí hlavu a ja vidím za sebou hlúčik  detí v doprovode troch dospelákov. Predpokladám že išlo o výletníkov, ktorí si za cieľ zvolili práve naše mesto. Decká sú odeté do pršiplášťov, len niektoré majú v ruke veselé dáždniky.    "Pani učiteľka, prečo tu majú toľko smetiakov", pýta sa chlapča, ktoré kráča akurát na mojej úrovni. Pani učiteľka sa s odpoveďou netrápi. "Neviem", bola jej stručná a jasná odpoveď. Chlapec čosi začal hovoriť spolužiakovi, no to som už nepočula, hoci priznávam, že ma to zaujímalo.    Skutočnosť, ktorá upútala výletníka, neraz už upútala aj mňa. Ináč tomu nebolo ani dnes. Chlapec nemal na mysli smetiaky ako také, ale "bordel" okolo nich. Do pravého poludnia chýba 50 minút. Mesto je plné ľudí, motajúcich sa jeden cez druhého... Každý má pred očami obrázok - centrum mesta je na každých pár metroch "vyzdobené"  veľkými, čiernymi, plnými, odpadovými vrecami,  zauzlenými igelitovými taškami, ktoré sú poukladané okolo smetných nádob rovno stredom pešej zóny, prípadne sú priložené k pouličným lampám. Pri jednej lampe moknú igelitky s logom Hudy sportu...   Už neraz ma takýto pohľad naštval, pretože to vyzerá otrasne. Neviem, či takáto situácia sa opakuje denne, či len pri odvoze smetí pár krát do týždňa, v meste nie som každý deň, tak naozaj neviem, ktorá domnienka je správna. Je ale pravdou, že tento obrázok sa mi naskytne často. Keby takto vyzerala pešia zóna trebárs len ráno, tak to viem pochopiť. Obchody, možno i firmy tam sídliace, sa nejako odpadu zbaviť musia. Že ho vynášajú práve k smetným nádobám pešej zóny v  centre, no to už celkom pochopiť neviem. Naozaj ma mrzí a zaráža, že takýto pohľad sa naskytne návštevníkom mesta  ešte aj na obed.  Nie je to pekná ozdoba a neviem, či si to mesto ako také vôbec uvedomuje, či to prekáža aj iným, alebo som jediná, ktorá vníma citlivo čosi,  o čom si možno mesto myslí, že si to  jednoducho netreba všímať. Neviem...   Ide letná sezóna. Práve teraz do mesta zavítajú návštevníci, Slováci či cudzinci. Nemyslím si, že tieto, okolo smetiakov uložené odpadové vrecia, prispejú k dobrému dojmu z  nášho mesta.  Myslím, že to malé územie pešej zóny, to malé martinské centrum, by nemalo pútať pozornosť smetiami, ale radšej niečím iným.   Oceňujem na jednej strane snahu skrášliť centrum napr. zaujímavo riešenými stojanmi s muškátmi, ktoré lahodia vari každému oku. Na druhej strane spomínaný odpad...       Ďakujem za prečítanie a za čas, ktorý ste mi venovali.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (31)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




