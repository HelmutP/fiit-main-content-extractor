
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Filip Németh
                                        &gt;
                camino de santiago
                     
                 Cesta do Compostely I. - Prológ, alebo čo a ako a prečo púť? 

        
            
                                    29.9.2007
            o
            19:14
                        |
            Karma článku:
                11.55
            |
            Prečítané 
            12076-krát
                    
         
     
         
             

                 
                    Nepamätám si kedy a nepamätám si ani ako som sa o nej dozvedel, no jedno mi už vtedy bolo jasné - musím tam ísť, toto raz musím zažiť! A ja keď sa rozhodnem, tak sa rozhodnem! Teraz po jej absolvovaní, po naplnení sna, mám skôr pocit, že nie ja som si vybral púť, to ona si vybrala mňa. Akoby aj tých dlhých 60 dní (väčšina ľudí si na to vymedzí tak 30 - 40) nebola naša voľba. Jednoducho to tak malo byť. A to, že o nej napíšem sériu článkov, to som tiež pôvodne nechcel, ale cítim, že to tak má byť. A tak je tu prvý, ktorý sa asi bude venovať tomu, čo to vlastne tá púť je, a načo to je vlastne dobré.
                 

                 
  
  Celé sa to začalo niekedy v 9. storočí po Kristovi, keď sa v Španielsku jednému z biskupov prisnil sen, že niekde v týchto krajoch by sa mal nachádzať hrob jedného z Kristových najbližších, hrob svätého apoštola Jakuba staršieho, syna Zachariášovho, brata apoštola Jána, toho, čo napísal (či skôr spísal) jeden z Ježišových životopisov i chýrnu Apokalypsu. Neodradilo ho ani to, že svätý Jakub bol sťatý vo Svätej zemi v roku 44 po Kristovi. Veď aj tak kolovali legendy o tom, že apoštolovo telo v kamennej(!) lodi prepravili jeho učeníci do Hispánie, kde ho pochovali. I našiel hrob akýsi pastier (jeho meno je už dávno zabudnuté), na mieste, ktoré mu ukázala žiarivá hviezda. Na tom mieste vyrástla najprv kaplnka, neskôr katedrála, ktorú potom hádam každý, kto len mohol dostavoval a prestavoval až z nej ostalo to, čo z nej ostalo. (Áno, pravdupovediac, po ceste sme videli aj krajšie.) Mesto, čo tam vyrástlo pomenovali po apoštolovi - Santiago a po hviezde - Compostela (názory na pôvod tohto názvu sa líšia, pravdepodobne to však znamená čosi ako miesto hviezdy). A putovanie sa mohlo začať. Najskôr prichádzali ľudia spontánne, no keďže niektorí zistili, že títo pútnici sú celkom dobrý biznis, začali aj s masívnou reklamnou kampaňou, s výstavbou ciest a mostov (obnovou tých rímskych), vznikli dokonca aj akési predpotopné bedekre (po slovensky turistickí sprievodcovia) a keďže na púť sa postupne vydávali ľudia prakticky z celej Európy, prospievalo to značne obchodu a tak mali Španieli za čo stavať ďalšie a ďalšie kostoly a vyrábať ďalšie a ďalšie relikviáre a tak ďalej... Po stredovekom boome záujem o Camino (ako sa tiež púť do Compostely nazýva) postupne upadal a v druhej polovici 20-teho storočia to vyzeralo skoro na zánik tradície, no obrat nastal v osemdesiatych rokoch, keď púť do Compostely absolvoval vtedajší pápež Ján Pavol II. a neskôr i dnes už veľmi populárny spisovateľ Paolo Coelho a v súčasnosti zažíva Camino akýsi novodobý boom. Ročne ho absolvuje viac ako stotisíc pútnikov. Asi toľko k histórii. Čo sa mojej vlastnej púte týka, absolvoval som ju s mojou priateľkou Barborkou, takže odteraz budem túto konkrétnu púť nazývať "naša púť", keďže naozaj je do slova a do písmena "naša". Jej prehistória sa začala písať v zime 2005/06, keď sme sa zoznámili a krátko na to sme zistili, že máme spoločný sen, podniknúť Camino. Barborka chcela pôvodne ísť už v to leto, na čo som si ja netrúfal, no to bol naozaj len sen, takže nakoniec to dopadlo tak, že sme sa rozhodli - najbližšie leto ideme a basta. A tak sme šli. Týmto vás pozývam nahliadnuť na našu púť a poprosím vás, nechajte ma viesť vás ňou. Ľahko by sa totiž mohlo stať, že by ste aj napriek žltým šípkam, ktoré označujú cestu, zablúdili. Predtým ako sa však vydáme na púť, bolo by nanajvýš užitočné, ba priam praktické povedať si, čo to tá púť vlastne je. Púť je v prvom rade cesta. Proces, ktorý trvá istú časovú dobu. Je to však cesta, ktorá má svoj cieľ. A to nie len fyzický (miesto, kam dorazíme, v našom prípade Santiago de Compostela, mesto na severozápade Španielska), ale aj cieľ duchovný, spirituálny. Nie nevyhnutne náboženský, ale väčšinou to tak býva. Mimochodom je celkom zaujímavé, aké množstvo neveriacich ľudí (agnostikov i skeptikov) sme na našej ceste stretli. Práve cieľ je to, čo púť odlišuje od obyčajného chodenia. Keďže naše putovanie sa šťastne zavŕšilo už pred niekoľkými týždňami, a ja som sa za ten čas stihol porozprávať s množstvom ľudí, skúsim teraz v krátkosti zhrnúť najčastejšie padajúce otázky, ktoré spočiatku každého zaujímali. Najskôr bol každý zhrozený už len z predstavy prejsť pešo okolo 800 kilometrov. Nuž áno, nie je to málo, ale dá sa to. My sme si cestu naplánovali zhruba na dva mesiace, z čoho sme niekoľko dní strávili prepravou do Španielska, resp. Francúzska a späť, pár dní sme si oddýchli pri oceáne vo Finisterre(v preklade koniec sveta), ku ktorému sme aj pešo prišli (asi 80 kilometrov od Santiaga), pár dní sme venovali prehliadke Compostely, plus rezerva, keby sa niečo stalo. Takže celkovo to vychádzalo asi 20 až 25 kilometrov chôdza denne, čo pri rýchlosti 4 až 5 kilometrov za hodinu je asi 5 hodín chôdze denne. Čo zas nie je až také strašné. Pravdupovediac chodenie ako také je vlastne absolútna pohoda, svinstvo sú batohy, ktoré máte stále so sebou a celú cestu ich vláčite. Každý, kto píše o Camine, varuje najmä pred príliš ťažkým batohom, napriek tomu má takmer každý svoj batoh naprataný množstvom zbytočností. Prejsť tridsať kilometrov každý deň s fľaškou vody v ruke je v podstate hračka, prejsť tridsať kilometrov každý deň s dvadsaťkilovým ruksakom na chrbte je už zaberačka. (pozn. tento lacný rým bol nechcený, ale je to presné, tak to nebudem meniť) Čo sa týka ubytovania, po ceste je vytvorený systém ubytovní tzv. albergues alebo refugios, ktoré poskytujú nocľah, sprchu a často aj kuchynku za pár eur, prípadne za dobrovoľný príspevok. Niekde je v cene aj spoločná večera, či raňajky, aj práčka  na mince a internetový automat. V miestach najčastejšieho začiatku väčšinou dostanete zoznam ubytovní. Mimochodom, aby to niekto nezneužíval tak ubytovanie sa poskytuje iba na jeden deň a iba pútnikom, čo majú pútnicky preukaz (kredenciál). Cesta sa čo najviac drží tej stredovekej a označujú ju žlté šípky, znaky mušle (symbol pútnikov), prípadne v Pyrenejách turistické značky a rôzne iné znaky - napr. kôpky kamenia navŕšené pútnikmi. No a poslednou z "najdôležitejších" otázok je, koľko to všetko stojí. Nie veľa. My sme sa zmestili do 450 eur na osobu (za dva mesiace!), aj keď treba povedať, že sme celkom šetrili. Tzn. nestravovali sme sa v reštauráciách a vyhľadávali sme skôr lacnejšie albergues. Pravdaže plus letenky a topánky. Mimochodom topánky to zvládli bez problémov, úplne stačí jeden pár kvalitnejších trekingových topánok. A v lete sandále využiteľné aj ako prezuvky. Základné informácie máme za sebou, o všetkom ostatnom porozprávam v nasledujúcich článkoch, ktoré budú (asi pomerne podrobne) popisovať jednotlivé etapy našej cesty. Buen Camino!

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Németh 
                                        
                                            Priestor pre názorový luxus
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Németh 
                                        
                                            Koľaje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Németh 
                                        
                                            Slnko aj zapadá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Németh 
                                        
                                            Slečna M.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Filip Németh 
                                        
                                            Všade ako doma
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Filip Németh
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Filip Németh
            
         
        filipnemeth.blog.sme.sk (rss)
         
                                     
     
        decko, ktorému sa akosi nechce dospievať
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    60
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2321
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            pevnina detstva
                        
                     
                                     
                        
                            hudba
                        
                     
                                     
                        
                            kultúra
                        
                     
                                     
                        
                            súkromné
                        
                     
                                     
                        
                            nezaradené
                        
                     
                                     
                        
                            vráble
                        
                     
                                     
                        
                            camino de santiago
                        
                     
                                     
                        
                            nebásne (piesňové texty)
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            László Márton - Bratstvo
                                     
                                                                             
                                            Thomas Mann - Buddenbrookovci
                                     
                                                                             
                                            James Monaco - Jak číst film
                                     
                                                                             
                                            Boris Vian - Pena dní
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Kanapa - pesničky, čo sú môjmu srdcu najbližšie
                                     
                                                                             
                                            Sufjan Stevens
                                     
                                                                             
                                            Leonard Cohen
                                     
                                                                             
                                            Zero 7
                                     
                                                                             
                                            Kings of Convenience
                                     
                                                                             
                                            Jack Johnson
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            michal hudec a jeho fotky
                                     
                                                                             
                                            názory navzdory
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            rádio
                                     
                                                                             
                                            anglická wikipédia
                                     
                                                                             
                                            jarisov videokutik
                                     
                                                                             
                                            rodny moj kraj
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




