
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dagmar Rusnáková
                                        &gt;
                Nezaradené
                     
                 Vždy, keď idú maturanti mestom 

        
            
                                    14.5.2010
            o
            14:36
                        (upravené
                14.5.2010
                o
                15:01)
                        |
            Karma článku:
                8.93
            |
            Prečítané 
            1635-krát
                    
         
     
         
             

                 
                    Vždy, keď idú maturanti mestom... schytávam splín a posúvam sa o niekoľko desaťročí dozadu. S úsmevom si spomínam na spolužiaka, ktorý pred košickým, vtedy Priorom, zložil klobúk a pýtal niečo na podporenie maturantov. Skoro za to vyletel zo školy...
                 

                         Takže, vždy, keď idú maturanti mestom, schytávam taký ten blahosklonný úsmev na perách i na srdci. Myslím si: No, veď uvidíte, čo Vás ešte čaká..." a naozaj mám vtedy pocit, že by som už sladkých - násť naozaj nechcela mať. Nechcela by som zažívať zas sklamania, rozchody. Či?   Zo sentimentálneho rozjímania ma však vytrhne ani nie tak piskot píšťaliek, ale pokrik: „Podporte nás!" , zvyčajne  spojený s nastrčeným „klobúkom". Vtedy vo mne ožíva neveriaci a pochybujúci tomáš. Ja, ktorá som sa zapovedala, že žobrajúcim deťom, spoluobčanom tmavšej i svetlejšej pleti peniaze nedám, lebo  nie som až taká naivná, aby som verila v ich racionálne /?/ použitie, ja, radšej riskujúca, že rožok, oplátka, alebo niečo iné konkrétne vhodné len na jedenie mi bude vhodené do hlavy a miesto ďakujem, odznie niečo iskrivejšie... JA mám vytiahnuť svoju peňaženku a prispieť našim nádejným inžinierom, lekárom, technikom, kaderníčkam a čo ja viem ešte akým nádejným odborníkom, na ich, veď si to povedzme  na rovinu, alkoholické stretnutie?   No, neviem. Zatiaľ som tento boj vyhrala v prospech slovka: NIE. Nikdy som nenašla odvahu povedať na rovinu: Prepáčte, ale veď by som Vám vlastne ublížila, ak by som Vás podporila. Nikdy som nemala chuť spustiť mravokárnu reč, lebo to mi naozaj neprislúcha. Nejaká tá výhovorka sa vždy našla, aby som odrazila domŕzajúcich nádejných maturantov. A tiež sa priznám, že keď vidím, ako idú maturanti mestom, občas ma nohy akosi samé od seba odnesú na náprotivnú stranu cesty ako je tá, po ktorej idú tí prevahoví.   Za starým politickým režimom som nikdy neplakala, ale k niektorým mravným imperatívom, na ktoré sme ako - násť - roční nadávali, nerozumeli im a nesúhlasili s nimi, by som sa aspoň občas, aspoň zlomkovo, vrátila.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (96)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            Sprievodca cestovateľa MHD na Slovensku - 1. kapitola
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            S mobilom na večné časy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            Prečo mám rada šarištinu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            Slovenskí gladiátori 21. storočia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dagmar Rusnáková 
                                        
                                            O Šindliari, ľuďoch a divadle
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dagmar Rusnáková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dagmar Rusnáková
            
         
        dagmarrusnakova.blog.sme.sk (rss)
         
                                     
     
        Som len taká malá poľnohospodárka v teréne.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    964
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Jarek Nohavica: Darmoděj
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




