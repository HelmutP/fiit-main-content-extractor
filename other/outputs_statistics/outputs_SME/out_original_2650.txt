
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Trnovcová
                                        &gt;
                Verejné
                     
                 Veda vs. náboženstvo 

        
            
                                    13.3.2010
            o
            16:39
                        (upravené
                13.3.2010
                o
                17:09)
                        |
            Karma článku:
                6.19
            |
            Prečítané 
            1215-krát
                    
         
     
         
             

                 
                    Prečo sa náboženstvo ešte stále drží domnienky, že veda je postrach ľudstva. Ja by som skôr povedala, že je to opačne. Nič nebrzdí vedu tak nemožne veľkoryso ako náboženstvo.
                 

                   
 Prečo cirkev odjakživa prenasleduje vedcov?   A tým sa dostávame k otázke, aký je vzťah vedy a náboženstva.Pre toho, kto je pesvedčený, že všetko dianie je zreťazením príčin, pre toho je úplne smiešna a neprijateľná predstava bytostí, ktorá zasahuje do behu života a do rytmu a chodu sveta i vesmíru. Boh, ktorý odmeňuje a trestá, je pre neho nemysliteľný už preto, že človek koná podľa vonkajšej a vnútornej zákonitej nevyhnutnosti. Z hľadiska božieho nenesie teda ani žiadnu zodpovednosť. Etické správanie sa človeka nepotrebuje nijakú oporu v náboženstve. Bolo by to s ľudstvom veru veľmi smutné, keby sa malo krotiť iba strachom z trestu a nádejou na posmrtnú odmenu. Je teda pochopiteľné, že cirkvi odjakživa potierali vedu a prenasledovali jej prívržencov. Ako povedal William James, psychológ- osobné náboženstvo predstavujú subjektívne zážitky, ktoré môžu byť nezávislé od panujúcich názorov šírených inštitucionálnym náboženstvom. Sigmund Freud- autoritatívne náboženstvo je nefunkčné, pretože odcudzuje človeka od seba samého a od toho, čo je ľudské.   Stvorenie sveta?  Keby informácie o pôvode sveta pochádzali z božích zjavení, museli by byť všetky totožné. Preto existujú iba dve vysvetlenia: Buď ide o predstavy vyvolané halucináciou, ktorej živosť prispela k viere v tieto predstavy a ich uvereniu, alebo ide o zámerné dezinformácie sledujúce isté ciele. Toto primälo niektorých vedcov k výskumu novej skupiny drog- halucinogénov. Najvýraznejší z nich je huba tzv. ,,svätá huba“. V minulosti však ešte nevedeli aké sú účinky tejto huby a ani to, že vyvoláva halucinogénne predstavy a intenzívne duševné zážitky. Obsahuje látku psilocybin, čo je droga patriaca do skupiny psychotropných látok a jej použitie je obmedzené iba na lekárske a vedecké účely. V minulosti samozrejme nič ako medzinárodné právo nebolo a teda nemal kto zakázať používanie tejto drogy. Tieto predstavy súvisia s niektorými oblasťami v mozgu, takže keď niekto užil drogu neuvedomujúc si jej účinky, je pravdepodobné že sa prerušili niektoré oblasti mozgu( napr. pocit nekonečného priestoru- zväzok neurónov v hornom spánkovom laloku je oblasť orientácie). Podobne vznikajú ,,vízie“. Keď sa do spánkovej oblasti mozgu vypustia malé elektrické výboje, vyvolávajú mystické zážitky. Autor usudzuje, že podobné vzruchy vznikajú aj pri stavoch úzkosti, osobných kritických životných okamihoch, nedostatku kyslíka, zníženej hladine cukru v krvi, alebo aj pri vyčerpanosti. Čiže tento jav prerušenia určitých spojení v mozgu sa nazýva DISOCIÁCIA. Takto sa vysvetľujú aj hypnotické stavy. Teda na záver môžem dodať, že ľudské mystické zážitky(rozprávanie sa s bohom, zjavenie sa nejakých nadprirodzených bytostí a pod.) sú produktom fyziológie mozgu

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (166)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            La Maladie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            L´amour
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            La Solitude c´est la mort
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            Anketa roka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Trnovcová 
                                        
                                            Citáty5
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Trnovcová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Trnovcová
            
         
        trnovcova.blog.sme.sk (rss)
         
                                     
     
        Študentka, čo chce vyjadriť svoj názor.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    19
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    467
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Verejné
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




