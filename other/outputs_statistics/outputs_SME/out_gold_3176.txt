

 Copacabana...stačí sa nadýchnuť teplého morského vzduchu, vône kokosu, ktorá sa plaví vánkom a ucítite tú neuveriteľnú pulzujúcu energiu tohto miesta. Započúvajte sa do zvukov samby, veselého smiechu a šumu mora. A nakoniec otvorte oči, kochajte sa slnečnými lúčmi, ktoré sa odrážajú od modrastej hladiny mora. Dotknite sa zlatistého zohriateho piesku a ponorte sa do hmatateľnej pohodovej atmosféry, ktorá tu panuje. 
  
  
 Cristo Redentor (Kristus Spasiteľ)...keď ho uvidíte prvýkrát, pocítite rešpekt, úžas, silu, ktorú dodáva Riu táto obrovská socha. Je majestátny. Týči sa nad mestom a akoby ho objímal svoji rukami. Rio de Janeiro máte ako na dlani. 
  
  
 Pão De Açúcar...opačný vrch k vrchu Kristovmu. Pri sviežom čaji Matté si vychutnáte výhľad na vody Altantického oceánu. Veriaci tvrdia, že čistá duša môže na horizonte uzrieť obrysy Afriky (z hľadiska zakrivenia zemegule je to nemožné). 
  
 Rua de Carnival...každý rok tu dva dni bez prestávky hrajú bubny sambu, caipirinha tečie prúdom, fotoaparáty turistov pretínajú tmu, pri fakľovom osvetlení sa mihajú postavy černošiek, belošiek aj aziatok v kostýmoch najrozličnejších farieb, tvarov a strihov. Veľkolepá dúha. Mesto farieb. Doslova. 
  
 Maracanã...staňte sa na pár okamihov slávnym futbalistom, vstúpte na rovnaký trávnik, kde kedysi vybehli aj hviezdy ako Pelé, Beckenbauer, Puskás, Zidane a ďalší...Túžbou každého hráča je hrať aj na Maracane. Oprávnene. 
  
  
  
 Rio de Janeiro...je to ako sen. Sen, ktorý sa vám sníva pred očami. Sen, ktorého sa môžete dotknúť, cítiť ho a on nezmizne. Pokojne snívajte ďalej... 
  
   

