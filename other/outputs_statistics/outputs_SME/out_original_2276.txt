
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Katarína Gočová
                                        &gt;
                Reportáže
                     
                 Eurovision Song Contest 2010 - Prvé semifinále sklamalo aj potešilo 

        
            
                                    15.2.2010
            o
            9:45
                        |
            Karma článku:
                7.80
            |
            Prečítané 
            3519-krát
                    
         
     
         
             

                 
                    Deň 14. február nebol tento rok výnimočnejší kvôli milencom zapĺňajúcim ulice miest či kaviarne, ale aj vďaka prvým menám postupujúcich v Eurovision Song Contest 2010. Prvé semifinálové kolo nijako zvlášť neprekvapilo, ba ani nešokovalo. Výherca je svojím spôsobom známy, no ostataných interpretov to neodrádza bojovať ďalej.
                 

                 Prvá finálová šestka..   V tomto prípade už nerozhodovali len hlasy divákov. Za porotcovský stôl zasadla na rozdiel od superstáristov trojica serióznych slovenských hudobníkov. Žiadne namyslené hviezdy šoubiznisu so skúsenosťami muzikantských bohov.  Čistá kvalita v podaní vyštudovanej skladateľky a klaviristky Ľubice Salamon - Čekovskej, legendárneho  hudobníka a niekďajšieho lídra skupiny Modus Janka Lehotského a do tretice úspešného autora textov a bývalého hudboného publicistu Martina Sarvaša.   Nikoho pravdepodobne neprekvapilo, kto obsadil prvú priečku. Svidníčanka Kristína ospevávajúca „Horehronie" mala tradične veľký úspech. Pamätám sa na jeden ročník letnej akcie s názvom Šíravafest, keď som túto dievčinu začula spievať prvýkrát. Publikum tvorilo zväčša pivom opojený dav.  Prešiel však dlhší čas a vsadenie na text Kamila Peteraja a hudbu Martina Kuvaliča jej prinieslo úspech od prvého vdychu do mikrofónu. Keby mali ľudia štyri nohy, tak ona tromi z nich stojí  na škandinávskom poloostrove.   Veľká radosť ovládla publikum pri informácií, kto zasadol na druhom mieste. Obeť útokov z minulého kola Miro Jaroš oslovil väčšinu slovenského obyvateľstva. Zaujímavosťou, pre ktorú bol atakovaný skupinou Družina bola účasť tlmočníčok prekladajúcich do posunkovej reči.      „Hudba by nás mala spájať a nepáči sa mi, keď sa niekto snaží hádzať špinu na posolstvo môjho vystúpenia. Je to smutné," povedal spevák. Topiaci sa aj slamky chytá - viď skupina Družina. Pieseň „Bez siedmeho neba" však bola voči tomu odolná a dostala sa tak do finále eurovízie.   Klára, Pavol Remenár a Old School Brothers. Jedinečná kombinácia tohto ročníka priniesla úspech aj v tomto kole.  Imidžové kúzlo dosiahlo premenu dievčaťa na ženu, a tak sa jej „Figaro" zvádzal trochu ľahšie. Táto skladba z dielne Pavla Jursu a Petra Farnbauera sa pohybuje ako na ľadovej kryhe. Veľmi neisto a bez nejakých veľkých skúseností. Otázknikom ale je, kde sa počas vystúpenia nachádzala skupina Liqiud Error. Stúpla im snáď sláva do hlavy?   Ďalším postupujúcim sa stal Robo Opatovský. To, čo nepočulo Slovensko, zachránila porota. Minulý rok bol z postupu v šoku, tentokrát však nemusel pochybovať. Pieseň „Niečo máš" bola jednou z najkrajších celého večera. Malá oslava zamilovanosti či Óda na lásku. V ten valentínsky deň to bolo trefou do čierneho. Medzi menej známymi resp. menej frekvetovanými menami finalistov sú  Mayo so živou piesňou „Tón" a „Ty tu ticho spíš" od talentovaného Mariána Banga.           Dobrý ťah pred letom   Platí hlavne pre kapelu Get Explode. Títo chalani sa v dave nestratia. V semifinále mali medzi davom najväčšiu podporu, no ďalej ich to neposunulo.  Čo už je to za rozdiel O,3 %? Boli netradiční, atypickí, netuctoví a farební. V ich vystúpniach koloval život. Ľudia však asi majú radšej niečo... menej dramatické. S Lackom Kováčom a spol. sa tak určite stretneme niekde v tepne hudobého leta. Medzi vypadnutými sú tiež Robo Mikla, Michaella či Petra Humeňanská. Niektorí to môžu brať ako také 5-dňové zviditeľnenie, niektorých možno budeme počuť o rok. Je to však iba na nás. Či si ich piesne pripustíme k duši.      Foto: Zdenko Hanout 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            NOVAROCK 2010: Festivalová sezóna otvorená
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Undergroundová žúrka na BUM 2010 v čiernobielých fotografiách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Showdance v znamení filmových hitov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Eurovision Song Contest 2010 – A víťazom sa stáva!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Katarína Gočová 
                                        
                                            Eurovision Song Contest 2010 - Neprekvapivé druhé semifinále
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Katarína Gočová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Katarína Gočová
            
         
        gocova.blog.sme.sk (rss)
         
                                     
     
         ... osoba, ktorú zaujíma dianie na kultúrnej scéne a celkovo život, spájajúci ľudí a kultúru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2265
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Festivaly
                        
                     
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Koncerty
                        
                     
                                     
                        
                            Reportáže
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené stránky 
                    
                     
                                                 
                                            Blog - Roberta Dolinská
                                     
                                                                             
                                            Blog - Zdenko Hanout
                                     
                                                                             
                                            Blog - Jana Šlinská
                                     
                                                                             
                                            Hudobný portál MUSIC4U.sk
                                     
                                                                             
                                            Reality Prešov
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




