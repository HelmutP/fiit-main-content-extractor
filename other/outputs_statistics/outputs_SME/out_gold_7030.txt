

 18. mája podvečer z úst viacerých slovákov vyšli nadávky, hromženie, z oka vypadla nejaká tá slza. Nádeje našich na majstrovstvách sveta v ľadovom hokeji sa rozplynuli ako hmla nad tatrami. Túžobne očakavaný postup medzi osmičku najlepších tímov na svete sa nepodaril a prehra 2:1 s Nemeckom definitívne poslala slovenský hokejový tím baliť si kufre. 
   
 To čo sa odohralo tento rok však len odzrkadľuje súčasný stav slovenského hokeja. Pri hodnotení treba priznať, že v posledných rokoch nehráme svoj podpriemer, ale priemer. To len v predošlých rokoch sme ťažili z dobrej prípravy a práce s mládežou v 80.- tych a 90 - tych rokoch. Slovenský hokej upadá a zaraďuje sa medzi tímy ako Dánsko, Bielorusko či Lotyšsko. A ak to bude pokračovať takto ďalej, budeme radi, keď sa úpadok zastaví aspoň tam. 
   
 Každá krajina vie že národný tím bude úspešný, len ak mládež a práca s ňou bude presne mierená, plánovitá, postupná a hĺbková. A ak chceme budovať národný tím, prvou podmienkou je, aby sme mali priestory, kde dáme šancu na rozvoj prirodzeného talentu. A už pri tejto podmienke neostáva nič iné len chytať sa za hlavu. Podľa oficiálnych zdrojov je na Slovensku 45 krytých hokejových plôch. Týmto počtom sme  na úrovni Bieloruska, Rakúska či Dánska. Viac hokejových plôch majú nielen Kanada, Švédsko, či náš odveký rival z Česka (pričom ide i neporovnateľné čísla, kedy v zahraničí je počet klzísk niekoľkonásobne vyšší ako u nás), ale i hokejový trpaslíci, ako Čína, Japonsko či Francúzsko. Preto je možné že náš hokej sa v priebehu rokov ocitne až za týmito krajinami. 
   
 Ďalším problémom sú peniaze. Rodiny často nemajú na to, aby svojmu malému synovi kúpili korčule či výstroj. Nemôžeme očakávať že talenty prídu a ukážu sa samé. Nikdy a nikde to takto nefungovalo. Chýba zásadná podpora štátu. A krajina s hokejovou tradíciou prichádza o talenty a budúcnosť. 
   
 Negatívnym faktorom ktorý pri finančnej náročnosti hokeja vzniká, je diverzita. Teda hokej hrajú len tí, ktorí na to majú. A tak sa môže stať, že nás v budúcnosti bude reprezentovať úzka skupina nie príliš nadaných športovcov, ktorí majú ale za sebou vplyvných a peňažne zaopatrených rodičov. Potom sa snáď alenebudeme čudovať, že naše výsledky budú upadať. 
 Upadá i prestížnosť dresu s dvojkrížom na hrudi. Každý rok sa stáva že i dvadsať alebo tridsať špičkových hráčov odmietne reprezentovať, lebo sú unavení, zranení, atď. To môže byť pravda a verím že sezóna je dlhá a ťažká. Ale treba priznať i ľudský faktor. Ak vidím súpisku, a viem že je beznádejne očakávať výraznejší úspech, prečo potom precestovať cez pol sveta, riskovať zranenie a ohrozenie budúcej kariéry?Tu sa ukazuje že hokej upadol. 
   
 Každému , kto i po rokoch videl záznam finále v Gotteborgu, sa pri tých radostných výkrikoch a gestách tlačia slzy do očí. A napriek takýmto úžasným pocitom nechávame pomaly puky a hokejky zapadať prachom. Je smutné, že dnešní malí slovenskí hokejisti sa už nehrajú na  Bondru či Pálffyho. Hokej prestal byť tým čím bol. Stal sa len športom. Verím že nebude dlho trvať a stane sa opäť tým čím by na Slovensku vždy mal byť. Spoločenskou udalosťou, fenoménom. Lebo inak môžme zabudnúť na slávu a úspechy. A ako pôjde čas, zabudneme i na to že dakde v máji predsalen bývajú akési majstrovstvá sveta... 
   
   
   
   

