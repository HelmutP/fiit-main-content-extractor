
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gustáv Murín
                                        &gt;
                Politika
                     
                 Kto je vlastenec? 

        
            
                                    10.3.2010
            o
            9:00
                        (upravené
                10.3.2010
                o
                15:03)
                        |
            Karma článku:
                4.62
            |
            Prečítané 
            903-krát
                    
         
     
         
             

                 
                    O tzv. "vlasteneckom zákone" sa toho tu popísalo už dosť. Priznajme si, že slovo "vlastenec" sa u nás už roky nenosí. A predsa existujú. Aj bez SNS-áckeho zákona. Ale musíte za nimi vycestovať..
                 

                 To s čím prišiel Slota a jeho verný E.T. pripomína pasáže z knihy Hlava XXII. Tam museli americkí vojaci za druhej svetovej vojny spievať národnú hymnu, keď pri obede stáli v rade na polievku, potom ešte povinne zanôtili, aby dostali hlavné jedlo a zvlášť precítene museli spievať pri šaláte. Alebo len prisahali? Mám šesť vydaní Hlavy XXII a istotu, že to tam niekde je, len to neviem napochytre nalistovať. Ak ma doplníte, budem len rád. Podstatné ale je, že je to (ako koniec-koncov celý príbeh Hlavy XXII) absurdita. V týchto dňoch sa táto absurdita v podaní takej hanby Slovenska, ako je Slota, a takého výsmechu Slovenska, ako je Rafaj, stala skutočnosťou. A sám Rafaj sa tomu aj po schválení zákona len uchechtával. Veď veselá je aj jeho zmienka, že riaditelia, ktorí nezvládnu k 1. aprílu (Dňu vtákov a globálnej hlúposti) naplniť literu tejto frašky porušia (pozor!) "Komenského prísahu"! Výzvy, aby ten nezmysel, prezident Gašparovič nepodpísal, sú smiešne. Veď on je plagátový "vlastenec", kvôli tomu, aby vyzeral svojstojne sa aj na fujaru naučil hrať! Navyše nesmieme zabúdať na alibizmus tých poslancov mimo SNS, ktorí za tento nezmysel tiež ochotne zdvihli ruku.   Zhodou okolností v týchto dňoch navštívila delegácia Slovenského PEN Centra Srbsko. Primárne sme mali rokovanie s kolegami zo Srbského PEN Centra v Belehrade, kde bude o rok celosvetový kongres PEN International. Ale využili sme tú cestu aj na takú samozrejmosť, ako je priniesť knihy našim krajanom vo Vojvodine. Máme totiž už skúsenosť, že keď sme v roku 2004 priviezli do slovenskej základnej školy a gymnázia v Sarvaši 470 kníh najmä z produkcie vydavateľstva Mladé Letá a krátke filmy nášho člena Ivana Popoviča, tak to boli prvé slovenské knihy, ktoré od roku 1989 dostali!   Takže aj do Vojvodiny sme nabalili plný kufor auta kníh a DVD z Literárneho informačného centra, redakcie Slovenských pohľadov, Filmového ústavu, Divadelného ústavu, vydavateľstva Marenčin PT, vydavateľstva Domov a svet, vyd. SSS a Mestskej knižnice v Bratislave. Tie sme zanechali v rámci dlhodobého projektu "Nesieme vám slovo" postupne v knižnici v Báčskom Petrovci, Mestskej knižnici v Novom Sade, Ústave pre Vojvodinských Slovákov v Novom Sade, Katedre slovakistiky Filozofickej fakulty Univerzity v Novom Sade, knižnici Dositeja Obradovića v Starej Pazove a v miestnej knižnici v Kovačici. Tieto donácie sa stretli s pozoruhodným ohlasom. Opäť sme tým zapĺňali povinnosti, na ktoré si naši domáci "vlastenci" zo SNS ani nespomenú.   Slováci na Dolnej zemi sú tými pravými vlastencami, ktorí držia slovenskú reč i kultúru mimoriadne živými stovky kilometrov od materskej krajiny už viac ako dvestopäťdesiat rokov. A žiadny zákon na podporu vlastenectva na to nepotrebovali! Preto si myslím, že tento zákon je hlavne na podporu upadajúcich volebných preferencií príživníkov slovenského národa, ktorí si v SNS z témy vlastenectva urobili živnosť. Naháňajú si týmito prázdnymi gestami pseudo-zásluhy, čím sa z našich daní vyhadzujú nadarmo ťažké peniaze, ktoré chýbajú inde. A ktoré preflákali na svoje nástenkové a emisné "tendre". Oni sami by našej vlasti nedali zo svojho ani cent. Skôr naopak, čakajú, že si za ich vlastenecké cifrovanie budú môcť bezprácne užívať aj ďalšie volebné obdobie. Alebo sa mýlim? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Mikuláš Černák – oslobodený Európskym súdom!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Dnes prišli Rusi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Záhada Tutanchmónovej výstavy - mimozemšťania v Bratislave?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Je toto ešte leto, alebo už monzún?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gustáv Murín 
                                        
                                            Hnoj pána Ondřejíčka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gustáv Murín
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gustáv Murín
            
         
        gustavmurin.blog.sme.sk (rss)
         
                                     
     
         Vraciam sa na tento blog a verím, že tak opäť stretnem starých známych a aj nových čitateľov. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    147
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2261
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Civilizácia
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Dlhovekosť
                        
                     
                                     
                        
                            Cesty
                        
                     
                                     
                        
                            Rádioaktívni - doplňujúce text
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Jazykové hlavolamy
                        
                     
                                     
                        
                            Rovnoprávnosť
                        
                     
                                     
                        
                            Paradigmy zdravia
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Partnerské vzťahy a rodina
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Čo sa deje
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prvá slovenská cestovateľská wikipédia
                                     
                                                                             
                                            Interaktívny román "Rádioaktívni"
                                     
                                                                             
                                            Moje texty v ďalších jazykoch
                                     
                                                                             
                                            Moje texty v angličtine
                                     
                                                                             
                                            Príbehy z ciest
                                     
                                                                             
                                            Viac o mne
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog o šťastí
                                     
                                                                             
                                            Blog antikvariátu Mädokýš
                                     
                                                                             
                                            Blog Cez OKNO
                                     
                                                                             
                                            Blog tajomneho medialnika
                                     
                                                                             
                                            Blog Pala Barabáša
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Video-blogy na TV Blava
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




