
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Kuhn
                                        &gt;
                Politika
                     
                 Čo pre mňa urobila pravicová vláda a čo ľavicová vláda 

        
            
                                    4.7.2008
            o
            8:08
                        |
            Karma článku:
                20.47
            |
            Prečítané 
            12750-krát
                    
         
     
         
             

                 
                    Aj keď pán premiér oslovil novinárov a nie mňa, dovolím si mu takto prostredníctvom svojho blogu odpovedať.
                 

                 
 http://sk.wikipedia.org/wiki/Obr%C3%A1zok:Coin_1sk_small.jpg
       „Vaša milovaná pravicová vláda čo urobila pre deti – nič,“ vyhlásil v stredu na tlačovke premiér Fico.     Rozmýšľam, či mám napísať politicky korektne, že sa pán premiér mýli, alebo politicky nekorektne, že ten boľševik trepe ako vždy sprostosti. Takže prosím vás odkážte tomu pajácovi, že tá jeho „krajne pravicová“ vláda zaviedla daňový bonus, vďaka ktorému moja rodina minulý rok získala 26 280 Sk, ktoré som ušetril na dani z príjmu. A za tieto ušetrené peniaze som si nekúpil Jach&amp;Tu v Monaku, ale „investoval“ som ich do svojich detí.     A mimochodom, čo pre moje deti za prvé dva roky svojho vládnutia urobila ľavicová vláda s akousi „silnou sociálnou politikou“? Nuž zaviedla tzv. milionársku, teda skôr polmilionársku daň (© Jozef Mihál), takže ak tento rok zarobím viac ako 498 000 Sk tak moja rodina z toho nebude mať až tak veľa, lebo štátu odvediem vyššiu daň za to, že som sa viac snažil a chcel svojej rodine vylepšiť ekonomickú situáciu. A to sa v časoch ľavicovej vlády nemá. V časoch ľavicovej vlády treba čakať, kým pán premiér a teta z ministerstva sociálnych vecí láskavo rozhodnú o zvýšení prídavkov na deti o 100 Sk. Pokiaľ možno až v druhej polovici volebného obdobia, aby to až tak veľa nestálo a aby si to voliči pri najbližších parlamentných voľbách ešte pamätali.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (184)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Kuhn 
                                        
                                            O rozdeľovaní a spájaní pravice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Kuhn 
                                        
                                            Nové pracovné miesta v Rožňave - naozaj dôvod na radosť?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Kuhn 
                                        
                                            Kto pridáva prácu samosprávam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Kuhn 
                                        
                                            Ako Michal Sántai kryl nástenkový tender
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Kuhn 
                                        
                                            Prezident o vymenovaní generálneho prokurátora
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Kuhn
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Kuhn
            
         
        kuhn.blog.sme.sk (rss)
         
                        VIP
                             
     
        vyštudovaný politológ, bývalý zahranično-politický redaktor Denníka SME, v súčasnosti SZČO, konzultant v oblasti eurofondov, analytik Konzervatívneho inštitútu M.R.Štefánika, poslanec Mestského zastupiteľstva v Rožňave, podpredseda Občianskej konzervatívnej strany, otec štyroch detí
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3156
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Eurofondy
                        
                     
                                     
                        
                            Eurobarometer
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Desaťtisíckárska daň
                                     
                                                                             
                                            Ako sa rozdávajú odmeny primátorom
                                     
                                                                             
                                            Začiatok konca Európskej únie?
                                     
                                                                             
                                            Hernando de Soto: Neexistuje lepší systém než kapitalismus
                                     
                                                                             
                                            Naco mat deti, ale vazne ...
                                     
                                                                             
                                            John O’Sullivan - Values and Foreign Policy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Paul Johnson - Churchill
                                     
                                                                             
                                            John O'Sullivan - Prezident, papež a premiérka
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Tomáš Klus - Pánubohu do oken
                                     
                                                                             
                                            Slovenské Námestie nebeského pokoja
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Juraj Petrovič
                                     
                                                                             
                                            KIosk
                                     
                                                                             
                                            Martin Vystavil
                                     
                                                                             
                                            Ondrej Dostál
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Právo na dobrú samosprávu
                                     
                                                                             
                                            Monitoring fondov EU
                                     
                                                                             
                                            Konzervatívny inštitút M. R. Štefánika
                                     
                                                                             
                                            Občianska konzervatívna strana
                                     
                                                                             
                                            Cena štátu
                                     
                                                                             
                                            InfoRožňava
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Smer aneb Obyčajní lobisti a bizarné osobnosti
                     
                                                         
                       Ficova blondínka u Kotlebu
                     
                                                         
                       Zrazu príliš veľa vody v Saudi...
                     
                                                         
                       Saudské news na tento týždeň...
                     
                                                         
                       Avatarova krajina v Číne
                     
                                                         
                       Hnedý kôň volieb a úbožiak Fico
                     
                                                         
                       Voľby ako referendum o Ficovej vláde
                     
                                                         
                       Kto je s kým rodina na našich súdoch
                     
                                                         
                       O tradícii cestovania autobusmi 31 a 39
                     
                                                         
                       Ako uspokojiť J&amp;T aj voličov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




