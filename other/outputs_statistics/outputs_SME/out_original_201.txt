
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin S. Rončák
                                        &gt;
                Nezaradené
                     
                 Vďaka Tatra banke som najlepší a nechutí mi treska 

        
            
                                    4.10.2007
            o
            10:06
                        |
            Karma článku:
                17.60
            |
            Prečítané 
            36345-krát
                    
         
     
         
             

                 
                    Keď som sa v osemnástke prvýkrát zamestnal, povedal som si, že si v banke zriadim účet. Taký poriadny, dospelácky. Keďže som bol ešte mladý, hlúpy a naivný, za každú cenu som chcel byť najlepší. Z reklamy som veľmi dobre vedel, za kým chodia najlepší, tak som sa k ním pridal aj ja. Nehovorím, že teraz už nie som mladý, hlúpy a ani naivný. To stále som. Po tých pár rokoch som však zistil, že som aj najlepší blbec.
                 

                  Vďaka mojej banke už viem, ako sa žije v Ríši divov. Jedného krásneho večera sa mi podarilo v jednom staromestskom obchode zabudnúť kreditnú kartu, ktorou som platil. Prešiel celý pracovný týždeň, kým som zistil, že mi v peňaženke chýba. Našťastie, z Ríše divov mi posielajú esemesky, takže veľmi rýchlo som zistil, kedy som kartou platil naposledy. Vybral som sa do spomínaného obchodu s nádejou a očami pre plač, či tam tú svoju nešťastnú kartu nájdem. Mal som šťastie. Síce tam nebola, akčná predavačka ju pár dní dozadu zaniesla na pobočku mojej banky. Cítil som sa ako v siedmom nebi. Škoda, že iba päť minút.      PRVÁ NÁVŠTEVA      - Ako vám môžem pomôcť? – opýtala sa ma Alenka, hneď ako som vstúpil do Ríše divov.     - Pred pár dňami som si v obchode zabudol kreditnú kartu. Predavačka ju našťastie zaniesla na pobočku na Vajanskom nábreží. Mohol by som sa k tej karte nejakým spôsobom dostať? – opýtal som sa, tak ako to robia iba tí najlepší.     - Ale samozrejme. Zavolám tam, prezistím to a zajtra sa môžete po kartu zastaviť. – usmievala sa Alenka.      DRUHÁ NÁVŠTEVA      - Dobrý deň. Ako vám môžem pomôcť? – opýtal sa Ignác v zelenej košeli.     - Včera som riešil s vašou kolegyňou Alenkou moju stratenú kartu, povedala, že dnes sa po ňu môžem zastaviť. Tu, hľa, je môj občiansky preukaz.      Ignác zobral preukaz a odišiel. Čas, kým bol preč, mi jasne naznačil, že stratených kreditných kariet s rovnakým menom a priezviskom ako mám ja, je zrejme veľa a on teraz prostredníctvom odtlačkov prstov a DNA zisťuje, ktorá je tá moja. Našiel tam však určite aj odtlačky akčnej predavačky, tak to teraz konzultuje s odborníkmi z policajného zboru. Tí si však s touto náročnou úlohou určite nevedia poradiť, a tak sa konferenčným hovorom spojili s kolegami z C. S. I. kriminálky Las Vegas a zatiaľ čo Ignác čaká v sklade platobných kariet na telefonát z policajného zboru, lúšti sudoku. Ja sedím a rátam čas, ktorý mi zostáva do konca obednej prestávky. Keď sa Ignác vráti za stôl, pri ktorom sedím, výsledok je jasný.      - Vašu kreditnú kartu tu nemáme. – sucho skonštatuje Ignác a dodáva: Môžete mi povedať, s ktorou kolegyňou ste to včera riešili. Ja sa s ňou spojím a dáme vám telefonicky vedieť, že čo a ako.     - V poriadku. Tu je moje telefónne číslo. – dodávam s kľudným výrazom, tak ako to robia najlepší a vzápätí volám kolegyni, nech mi cestou z obedu kúpi tresku a dva rožky.      TRETIA NÁVŠTEVA      - Dobrý deň. Pomôžem vám? – vycerila na mňa zúbky Alenka.     - Ja som tu bol pred niekoľkými dňami a riešil som s vaším kolegom Ignácom moju stratenú kreditnú kartu. Povedal mi, že sa s vami dohodne a zavoláte mi, kedy si po ňu môžem prísť. Ale doteraz mi nikto nevolal.     - A-ha, to ste vy, ten pán... Áno, áno. Už viem. Zistili sme, že vašu kreditnú kartu poslali z Ríše divov z Vajanského nábrežia do Ríše divov na Račiansku ulicu, pretože to je vaša materská Ríša divov.     - Fí-ha. A čo teraz? Mám ísť na Račiansku?     - Nie, netreba. Stavte sa u nás o tri dni, zariadim, aby kartu poslali k nám. – ochotne mi ponúkla.      ŠTVRTÁ NÁVŠTEVA      - Dobrý deň. Ako vám môžem pomôcť? – opýtala sa Hortenzia.     - Ja som tu riešil s vašou kolegyňou Alenkou a kolegom Ignácom moju stratenú kreditnú kartu. Z Ríše divov na Vajanskom nábreží ju poslali do Ríše divov na Račianskej ulici a teraz by sa už mala nachádzať u vás. Mohol by som ju dostať? Nech sa páči, tu je môj najlepší občiansky preukaz. – vyrapotal som, ako básničku. Však som najlepší, nie?      Hortenzia pozerala na občiansky preukaz a premýšľala. Ústa mala zvláštne pootvorené, takže som niekoľko minút mohol dokonale analyzovať jej fixný strojček na zuboch. A potom jej zrejme napadlo to najlepšie možné riešenie.      - Moja kolegyňa Alenka by sa o chvíľku mala vrátiť z obedu, takže počkáme na ňu. – usmiala sa a ja som premýšľal, že taký obed by som si doprial aj ja. Treska mi už akosi prestávala chutiť.     - OK. – povedal som, tak ako to hovoria iba tí najlepší.     - Ale všimla som si, že vám zrak padol na náš katalóg s nehnuteľnosťami. Kým sa vráti Alenka z obedu, môžeme sa porozprávať o hypotéke. Ste mladý a myslím, že je najvyšší čas rozmýšľať o vlastnom bývaní. – oči je zaiskrili.     - Nie, ďakujem. Ja sa toto tak skoro nechystám riešiť. Potreboval by som svoju kartu. – odpovedal som.     - Ale viete, že ceny bytov idú čoraz vyššie. Toto by bola pre vás tá najlepšia investícia.     - Nie, ďakujem. – odpovedal som ako najlepší na ponuku k najlepšej investícii.     - Však môžeme urobiť len takú nezáväznú kalkuláciu. – naliehala.     - Viete, Hortenzia, ja neplánujem zostať v Bratislave do konca života.     - To nevadí. My vieme prefinancovať nehnuteľnosti aj mimo Bratislavy – škerila sa Hortenzia.     - Hovorím vám, slečna. Zbytočne budete robiť kalkulácie, nijako mi v tomto smere nepomôžete. Ďakujem. – už mi úsmev začínal kysnúť.     - Ale tak skúsme. Kde by ste chceli bývať? – nedala si pokoj.     - Dobre teda. O pár rokov sa chcem presťahovať do Malagy. Mohli by ste mi, prosím, urobiť kalkuláciu, ako prefinancovať dvojizbový podkrovný byt neďaleko centra? – usmial som sa.     - Malaga? To je aký okres, prosím vás? – žiarila Hortenzia.     - Okres, to vám neviem povedať, ale je to v Španielsku. – skonštatoval som a rozmýšľal nad tým, aký geniálny asi obed Alena má, keď jej to trvá tak dlho.     - No, ale to ja neviem, či sa dá prefinancovať nehnuteľnosť aj v Španielsku. – prehlásila a pozerala na mňa ako na blbca.     - A kto to má vedieť, milá moja?     - Opýtam sa kolegu. Moment. – povedala a zdvihla telefón. Po minúte mi oznámila: Žiaľ, prefinancovať môžeme iba nehnuteľnosť na Slovensku.      Alenka nechodila a nechodila, tak som sa zdvihol a zavolal kolegyni, nech mi kúpi tresku.      PIATA NÁVŠTEVA      - Dobrý deň. Ako vám môžem pomôcť? – opýtal sa ma Ignác.     - Ja tu už niekoľko týždňov s vami a vašou kolegyňou Alenkou pátram po mojej kreditnej karte, ktorá by už mala pricestovať z Ríše divov z Račianskej ulice. Mohli by ste mi ju, prosím, vystaviť? – vyrukoval som, tak ako to robia iba tí najlepší, na Ignáca.     - A há. To ste vy s tou vašou nešťastnou kreditnou kartou. – usmial sa Ignác.     - Áno, to som ja. Ten idiot, ktorý chce iba svoju kreditnú kartu.     - No, moment. – vyšlo z Ignáca.      Medzičasom prišla k stolu Hortenzia, ktorá sa ma ešte pred pár dňami snažila nalákať na hypotekárny úver.      - Dobrý deň. Ako vám môžem pomôcť? – opýtala sa.     - Ďakujem. Ja čakám na vášho kolegu Ignáca.     - Všimla som si, že váš zrak padol na našu ponuku hypotekárnych úverov. – vyškerila.     - Ďakujem, slečna, ale v Malage mi vraj hypotéku nevybavíte. – odpovedal som tak, ako to robia iba tí najlepší.     - Malaga? A to je aký okres? – opýtala sa Hortenzia.      Vzápätí však došiel Ignác.      - Žiaľ, vašu kreditnú kartu v Ríši divov na Račianskej nemajú. Údajne by sa mala nachádzať v centrálnej Ríši divov, tam sa však teraz nedovolám. Všetko vybavím. Stavte sa u nás o také tri dni. A ak by som vám mohol odporučiť, spíšme žiadosť o prenos všetkej dokumentácie z Ríše divov na Račianskej k nám. Budete to mať tu, pekne, krásne po ruke.      A tak sme s Ignácom spísali žiadosť o presun všetkej dokumentácie do Ríše divov na Františkánske námestie.      ŠIESTA NÁVŠTEVA      - Dobrý deň. Ako vám môžem pomôcť?     - Dajte mi moju kreditnú kartu! – vyletelo zo mňa, tak ako to nerobia najlepší.     - Akú kreditnú kartu? – opýtala sa Alenka.     - Robíte si zo mňa srandu?     - Jáj, to ste vy, pán Rončák, a vaša nešťastná kreditná karta.     - Áno, to som ja, najlepší pán Rončák a moja najlepšia kreditná karta!     - No, situácia je taká, že v centrálnej Ríši divov vašu kartu nemajú a predsa len by sa mala nachádzať v Ríši divov na Račianskej.     - Robíte si zo mňa srandu?     - Nie, nerobíme. To by sme si k najlepším nikdy nedovolili. Problém je v tom, že na Račianskej tvrdia, že ju nemajú.     - Tak je možno pod kobercom. Alebo si ju riaditeľ pobočky zobral, že ňou bude v zime oškrabávať námrazu z okien svojho BMW.     - Ha, ha, ha. – rehotala sa Alena a vôbec ju neštvalo, že platím za kartu, ktorú vôbec nemôžem využívať.- Stavte sa u nás budúci týždeň. Určite tu už bude.      Vyfáral som z Ríše divov a volal kolegyni, nech mi kúpi cestou z obedu tresku a dva rožky.      SIEDMA NÁVŠTEVA      - Dobrý deň, ako vám môžem pomôcť? – opýtala sa ma Hortenzia.     - Nechcem žiadne úvery, žiadne hypotéky, chcem len svoju kreditnú kartu, po ktorej mi tu pátrate už skoro dva mesiace. – vôbec som sa nechoval ako najlepší.     - A s kým ste to riešili.     - Divili by ste sa, ale už aj s vami, aj s vašou kolegyňou Alenkou, aj s Ignácom.     - Ukážte mi občiansky preukaz.     - Páči sa. Tu máte môj najlepší občiansky preukaz.      Hortenzia čumela do monitora, ťukala a ťukala ako na štátnici zo strojopisu, až z nej vybehlo...      - No, ale vaša kreditná karta pred pár dňami stratila platnosť. Darmo vám budeme pátrať po starej, keď vám o chvíľku v schránke zavíta nová. – cerila na mňa fixný strojček.     - Vy si zo mňa robíte dobrý deň? – opýtal som sa ako najhorší.     - Nie, nerobíme. Môžem vám ešte okrem tohto nejako pomôcť, keď už ste tu?      Hyperventiloval som ako o život.      - Tak keď tu už sedím, mám na vás jednu prosbu. Ako osemnásťročný najlepší, síce hlúpy a naivný, som si u vás zriadil super skvelé podielové investičné fondy so životným poistením, na ktoré ma ukecala vaša kolegyňa Kamila z Ríše divov na Račianskej, keď som čakal na schválenie bezúčelového úveru vo výške 30 000.     - Ale áno. Rada vám to prezistím.     - Ďakujem.     - Momentálne tam máte niečo cez dvadsaťtisíc korún.     - Paráda. Kedy by som sa k tým peniazom mohol dostať? – oči mi zažiarili.     - Až o pár mesiacov vám uplynie trojročné zmluvné obdobie. Potom si môžete vybrať päťdesiat percent. – profesionálne odpovedala.     - Päťdesiat percent? – nevychádzal som z údivu: Ale však mi vaša kolegyňa tvrdila, že po troch rokoch si môžem s nasporenými peniazmi robiť, čo chcem!?     - Áno, ale nemali ste si zvoliť aj životné poistenie.     - O.K., Alenka, tak mi povedzte, kedy sa k sto percentám môjho šetrenia dostanem. – chcelo sa mi plakať.     - Po skončení vášho šetriaceho programu. – odpovedala stále kľudná Hortenzia.     - A to je?     - V roku 2045.      Ôsma návšteva       - Dobrý deň. Ako vám môžem pomôcť? – opýtal sa ma Ignác.     - Už nijako. Len mi, prosím, zrušte moje podielové investičné fondy so životným poistením.     - Ale to sa vám neoplatí, prídete o veľké peniaze. Fondy stále rastú. – snažil sa ma odhovoriť Ignác.     - Nech rastie, čo chce. Ďakujem, nech ma to stojí, čo chce, prosím, zrušte to.      Ignác ťukal do počítača ako o život. Až z neho vyliezlo:      - Rád by som vám pomohol, pán Rončák, ale ak chcete skutočne vykonať túto stornáciu, musíte ísť na svoju materskú Ríšu divov. – povedal kľudne Ignác, ako vystrihnutý z obálky magazínu pre násťročné.     - Prosím? – opýtal som sa ako čisto čistý dement.     - Áno. Tento úkon môžu vykonať iba na vašej pobočke.     - Prosím? – opýtal som sa znovu.     - Neviem, o čo vám ide, pán Rončák, ale ak chcete zrušiť vaše fondy, musíte ísť do vašej materskej Ríše divov.     - A tá je kde, Ignác?     - Moment... Ignác ťukal a ťukal ako besný, ja som medzičasom volal svojej kolegyni, nech mi cestou z obedu kúpi tresku, dva rožky a Xanax.      - Vaša materská Ríša divov je na Račianskej ulici. – vyškieral sa Ignác.     - Ignác, vy ste sa tento rok v Ríši divov dohodli, že si zo mňa budete strieľať?     - Ale ja nechápem, o čo vám ide, pán Rončák? – divil sa Ignác.     - O nič. Iba o to, že pred vyše mesiacom ste mi tu, Ignác, riešili žiadosť o presun dokumentácie z Ríše divov na Račianskej na túto pobočku.      Ignác zmeravel.      - Možno je dokumentácia ešte na ceste. – odpovedal ako profesionálny poradca tých najlepších.     - Mesiac? – zajačal som.     - Môžeme spísať reklamáciu. – znervóznel Ignác.     - Počujte, to mi chcete povedať, že moja banková dokumentácia si teraz popíja miešaný alkoholický drink na Račiankom mýte, kým sa uráči dostaviť sem?     - Je mi to ľúto.     - Aj mne. – ukončil som návštevu.       Vybehol som z Ríše divov a akoby ma osvietilo, napadlo mi zavolať na super službu Ríše divov s názvom Dialóg:      - Dobrý deň.- Dobrý deň. Ako vám môžem pomôcť? – spýtala sa žena. Ak sa ma ešte raz niekto opýta, ako mi môže pomôcť, tak budem vraždiť.     - Ja by som sa chcel iba opýtať. Mám u vás brutálne nevýhodné investičné podielové fondy so životným poistením a chcem ich zrušiť. Na akej pobočne to môžem vyriešiť?     - Na ktorejkoľvek pobočke na Slovensku.     - Vy mi chcete privodiť infarkt, slečna?     - Nie prečo?     - Váš kolega Ignác z Ríše divov v centre mesta mi povedal, že to môžem zrušiť iba v mojej materskej Ríši divov.     - Tak sa za ním vráťte a keď vám bude toto tvrdiť, povedzte mu, nech zavolá na Dialóg.      Už neznášam tresku. Čas sú vraj peniaze. Keby som zostal v Ríši divov, prídem aj o oškrkátkované slipy. Nie ďakujem. Nepotrebujem byť za každú cenu najlepší.        Prosím, presvedčte môjho šéfa, aby mi dával výplatu v ponožke.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (217)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin S. Rončák 
                                        
                                            Deň môže byť lepší s Orange
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin S. Rončák 
                                        
                                            Zo Slobody a Solidarity je Smútok a Sklamanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin S. Rončák 
                                        
                                            Megakasíno a morálny úpadok Slovenska?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin S. Rončák 
                                        
                                            Lidl, správna voľba?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin S. Rončák 
                                        
                                            pondelOK
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin S. Rončák
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin S. Rončák
            
         
        roncak.blog.sme.sk (rss)
         
                                     
     
        Blázon. Miluje silné preso s mliekom, Piknik a leto. Neznáša zlých a zákerných ľudí, násilie a cviklu. Doraziť ho vedia najmä dôchodcovia v Tescu a pubertálne pipky v kine. Pracuje a študuje. Teda, snaží sa študovať. Jeho konanie sa nedá predvídať. Cez víkend s ním chcete ísť na pivo, on medzičasom zdrhne do Budapešti, Prahy alebo Kokavy nad Rimavicou.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    61
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2017
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pravidlá slovenského pravopisu
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Ahn Trio
                                     
                                                                             
                                            Tatabojs
                                     
                                                                             
                                            Puding pani Elvisovej
                                     
                                                                             
                                            Robyn
                                     
                                                                             
                                            Christophe Maé
                                     
                                                                             
                                            Tori Amos
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




