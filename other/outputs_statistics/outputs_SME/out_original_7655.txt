
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roland Cagáň
                                        &gt;
                Nezaradené
                     
                 Scenáre nešťastných párov 

        
            
                                    31.5.2010
            o
            11:35
                        (upravené
                31.5.2010
                o
                11:42)
                        |
            Karma článku:
                12.84
            |
            Prečítané 
            3240-krát
                    
         
     
         
             

                 
                    Sú chvíle, keď skončí sedenie a mne ostane ťaživý smútok a bezmoc, napriek tomu, že sme sa veľakrát učili, zažívali a presvedčili, že tieto pocity sú prenesené z našich klientov. Napriek vedomiu, že pre niektorý vzťah je lepšie sa rozísť, mi ostáva neskutočne smutno pri jednej predstave: Keby tento pár prišiel o pár rokov skôr, teraz by mohli byť šťastní. Hoci sa milovali, teraz sú kilometre vzdialení.
                 

                   
 Ako to vo vzťahu väčšinou chodí alebo opačný začiatok?   Počúvam často tento scenár: Na začiatku chceme toho partnera získať a tak sa snažíme zo všetkých síl byť najlepšími, akými sa len dá, hoci aj za cenu, že nebudeme sami sebou. Myslíme na blaho toho druhého, staráme sa o neho, naše nároky pre nás nie sú dôležité. Keď po pár rokoch príde kríza a naša energia sa vyčerpá, dôjdeme na svoje hranice a nechceme ustúpiť už ani krok. Tak veľmi sa zrazu staráme o to, čo potrebujeme iba my sami a nechceme zo svojich nárokov zľaviť ani milimeter. Veď sme už ustupovali dosť. Ak niekto pozeráte 30 Rock, tak to krásne vyjadrila jedna postava:   Co je láska? Můžu být prostě sama sebou. Není přesně tohle láska?   - Ne. Láska je nepřetržité schovávání toho, kdo doopravdy jsi. I ve spánku. Láska je spaní s make-upem a chození na záchod dolů do Burger Kingu. A schovávání alkoholu v lahvičkách od parfému. To je láska.   Naivná idealistická predstava – čo keby to bolo naopak? Na začiatku vzťahu sme sami sebou – vravíme, čo potrebujeme, cítime, očakávame, hneváme sa, keď nahnevaní, plačeme, keď sme smutní. Keď sa nám niečo nepáči, alebo nám niečo chýba, snažíme sa to presadiť, bojovať o to. Ak sa nám nepodarí zladiť, bolí to menej ak sa rozídeme. Ak áno, pustíme sa do vážneho vzťahu. Po rokoch prichádza aj tak kríza. Sme schopnejší počuť, čo tomu druhému chýba, čo by od nás chcel.   Zdá sa mi, že v tomto modeli máme väčšiu energiu na ústupky, na obetovanie kusu seba, na zľavenie zo svojich nárokov.  Lebo sme to nemuseli až tak robiť doteraz.   Čo sa stalo po ceste alebo zaštepenie stromu   Ešte bližšie realite je táto možnosť. Na začiatku sme si ozaj sadli. Mali sme síce ružové okuliare, ale vedeli sme, že ani ja ani partner nie sme ideálni. Ale ozaj dlho to výborne a neskôr aspoň dobre fungovalo. Potom prišli problémy. Ale keďže vieme, že vzťah znamená toleranciu, tak sme ustupovali, nevraveli, hnevali sa tak nanajvýš vnútri seba. A postupne sa vzďaľovali. Pripomína to vetvu zaštepeného stromu, ktorá si zrazu rastie do druhej strany a po čase mocnie a kvitne. (napríklad ako susedka, ktorá nás obdivuje). Môj dedo mal hrušku, ktorá z jedného kmeňa rodila tri druhy plodov – na každej vetve iný. Veľmi ťažko by sa z nej stal opäť monolitný strom – to by sme museli tie dve vetvy odpíliť. Keď už nič iné, mali by sme zrazu iba tretinu úrody. A nové vetvičky by rástli pomaly. Takto máme na záchranu vzťahu maximálne tretinu energie, ktorá ostala na vetve nášho pôvodného kmeňa. A čím sme nechali druhú vetvu hrubšiu vyrásť, tým ťažšie sa bude vracať.   „Ťažko sa po rokoch počúva, že sme to mali riešiť my sami a oveľa skôr.“   Toto sú nedávne slová jedného klienta, ktorý sám zistil, že čosi zameškal. Vetva do určitého času žila samotným životom a jeho otázka, či sa to dá ešte vrátiť späť a bez bolesti, bez straty energie, je veľmi ťažká na odpovedanie. To, čo sa dalo robiť kedysi je zachytiť vyrastanie novej vetvy v začiatkoch. Vo chvíľach, keď sa nám zdá, že to je iba malé odbočenie alebo zanedbateľný hnev (aj keď sa teda opakuje niekoľký krát). To čo sa dá jedine robiť teraz, je hnevať sa, hádať, vravieť čo potrebujeme a rozprávať a rozprávať - nielen keď problém už nie je udržateľný. Na jeho začiatku.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Magická kuchyňa intímnych vzťahov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            1000 druhov plaču
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Samota, hnev, smrť a egoizmus. Vivat!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Ani ten komplex menejcennosti nemám poriadny!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roland Cagáň 
                                        
                                            Buď, alebo, obidvoje spolu, ani jedno
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roland Cagáň
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roland Cagáň
            
         
        rolandcagan.blog.sme.sk (rss)
         
                        VIP
                             
     
        Psychológ, psychoterapeut, Modrý anjel na polovičný úväzok, zaujíma ma človečina z akejkoľvek stránky. Chcem sa podeliť o myšlienky, ktoré mi víria hlavou, keď sa s mojimi klientmi učím byť človekom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3107
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




