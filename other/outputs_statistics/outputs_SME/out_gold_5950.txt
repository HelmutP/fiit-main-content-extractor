

 Cierne bralo, 
 biela skala. 
 Tabletky tam pozierala. 
 Ta sa bala 
 spat na listi 
 pod zelenym nebom. 
 "Nebude viac s Tebou!" 
   
 Vsetok piesok pada hore 
 zo zeme na cierne zore. 
 Zaspavala na listi, 
 na cervavej hline 
 sta prst v zaludku. 
   
 Ziadne hromy, 
 ziadne blesky, 
 len zerava gula pali 
 ... na medene skaly, 
 kde jej oci pocitali 
 kolko dni je sam. 
   
 Vzduch jak biela muka kypi 
 a skorica v jej ociach. 
 Usina tam hore v brale. 
 Tam si nasla noclah. 
   

