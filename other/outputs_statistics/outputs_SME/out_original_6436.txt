
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Štefancová
                                        &gt;
                Politika
                     
                 SNS ide vlastnými billboardami sama proti sebe 

        
            
                                    13.5.2010
            o
            15:00
                        (upravené
                22.1.2013
                o
                18:03)
                        |
            Karma článku:
                14.57
            |
            Prečítané 
            2933-krát
                    
         
     
         
             

                 
                    Hlásajú veľkolepé heslá. Dívajú sa na vás z každej strany. No o SNS povedia len toľko, že sa zatiaľ nezbavila vlastnej paranoje a nepozná iný problém, než Maďarov a Rómov.
                 

                 Aby naše ženy nemuseli plakať.   Tento billboard ma naozaj zaujal, aj keď presne nerozumiem, čo tým chcela strana povedať. Ženy budú plakať, keď pôjdu ich muži a synovia do vojny s Maďarmi, alebo čo?? Naozaj si neviem vysvetliť hlavnú myšlienku, ktorú chcel dať tomuto dielu autor, ale môžem vysvetliť niečo iné.   Viete prečo veľa žien doma plače?   Náš dom je postavený rovno oproti dedinskej krčme, no počas šiestich rokov, čo tam bývame, som v nej nebola ani raz. Keby som mala stráviť v spoločnosti tých skrachovaných existencií čo len pol minúty, asi by som sa prepadla od hanby. (Názvom skrachované existencie tých ľudí nechcem urážať, iba opisujem ich reálny psychický, fyzický, emocionálny a ekonomický stav.) Muži, ktorí od siedmej rána sedia v krčme alebo pred ňou, už vtedy naliatí v alkohole, o deviatej doobeda spia na stole, na obed sa poberú na chvíľu domov (za svojimi plačúcimi ženami, ak im nejaké ostali), no poobede sa opäť vracajú za starý známy stôl a pohárik, opäť sa opijú (nejdem písať ako čo) a neskoro večer ich niekto musí odviesť domov, aby náhodou nezaspali niekde nad kanálom.   Myslím, že ženy takých mužov sa doma naplačú dosť. Ešte šťastie, že pán Slota ukazuje verejnosti len svoj obraz triezveho a striedmeho života. Aby sa podľa neho chlapi naučili a ženy nemuseli plakať.   Aby bol národ múdry.   Hm... na toto sa dá reagovať veľmi jednoducho - keby sa mal národ a vlastne hlavne deti vychovávať logikou SNS, myslím, že by sme tu onedlho mali samých malých rasistov. Okrem toho, neviem, či som ja nejaká nedovzdelaná, ak nepoužívam Slotov slovník. Úroveň jeho vyjadrovania je naozaj vysoká, tak neviem... možno keď budem stará a prepitá ako on...   Aby naše hranice zostali našimi hranicami.   Toto je tá ich večná paranoja. Nebezpeční útoční Maďari (toto nie je môj názor, len sa snažím vžiť do logiky SNS) chcú obsadiť Slovensko. Stav najvyššej pohotovosti! :):) To vážne? Choďte sa do nejakej pohraničnej maďarskej obce opýtať tých Maďarov, čo ich trápi. Naše územie to nebude, majú dosť vlastných starostí. Samozrejme sa nájde aj tam niekoľko extrémistov (veď aj my máme SNS), ale buďme trochu realisti. Naše hranice naozaj zostanú našimi hranicami.   Aby naše deti mali istotu.   Táto veta môže tiež vyjadrovať strach pred Maďarmi (viď. predchádzajúci odsek), alebo sa nám SNS snaží povedať niečo o inej istote? Snáď ekonomickej? Sociálnej? Morálnej? Nech napíšem čokoľvek, ani jedna istota sa mi nespája s SNS. Politická - to určite, veď by sa najradšej vrhli na Maďarov. Ekonomická - asi nie, pokiaľ budeme rodinkárčiť, dávať peniaze kamarátom, korumpovať a pod. Sociálna - pri spojení s SNS mi pri tomto slove nesvieti nič, oni sa proste starajú len o Maďarov. A morálna - opäť sa môžem odvolať na predchádzajúce odseky (viď. Vyjadrovanie Jána Slotu a jeho alkoholizmus). Tak akú istotu to teda mali na mysli?   Aby sme nekŕmili tých, čo nechcú pracovať.   To najlepšie som si nechala na koniec a bude veľmi jednoduché to vysvetliť. SNS si vzala na mušku Rómov. Viem, je veľa takých, čo nepracujú, ale v prípade tejto strany stačilo pohľadať tvár na billboard vo vlastných radoch.... hm... taký Janko Slota. Keď je niekto poslanec, berie za túto prácu plat. A ak už nič zmysluplné nenavrhuje a nezapája sa do činnosti parlamentu aktívne (neráta sa osočovanie ostatných poslancov), človek by očakával, že ten plat teda dostane za to, že sa zúčastňuje politického diania aspoň pasívne - sedením na zadku v parlamente. Ale pán Slota nie, pán veľkomožný má na všetko ľudí, dokonca aj takých, čo sa zaňho podpíšu, keď treba.   A tak sme sa dostali k tomu, že kŕmime aj skorumpovaných opilcov, ktorým sa nechce pracovať.       Tak prosím, skúsme Slotu nekŕmiť ďalšie štyri roky. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (19)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Blaženosť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Usmievať sa na svet
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Roman Holiday
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Teoretici
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Štefancová 
                                        
                                            Prečo radšej SaS ako Smer
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Štefancová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Štefancová
            
         
        stefancova.blog.sme.sk (rss)
         
                                     
     
        som blázon, som romantička, som dieťa revolúcie, som žena s veľkým potenciálom, čo čaká na správnu chvíľu, som zvedavá, som živel, som originál :)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    14
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1603
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            univerzita
                        
                     
                                     
                        
                            zo sveta
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keby tak Fico tušil
                     
                                                         
                       O čom sa písalo v novinách pred 100 rokmi
                     
                                                         
                       Otvorený list homosexuálnej aktivistke pani Hane Fábry
                     
                                                         
                       Chlapča v kuchyni
                     
                                                         
                       Pár čísel
                     
                                                         
                       Červené ruže a saudský Nový Rok...
                     
                                                         
                       Na rovinu: Deti z detských domovov nemajú šancu
                     
                                                         
                       Krupobitie
                     
                                                         
                       Súrodenci a láska nekonečná (alebo jej rôzne podoby...)
                     
                                                         
                       Staré diáre
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




