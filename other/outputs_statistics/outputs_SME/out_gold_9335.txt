

 Na smer Mara chodievame po tzv. starej ceste, kadiaľ premáva predsa len menej áut a dá sa toho aj viac vidieť. Cesta začína za vlakovou stanicou v Ružomberku  pod vrchom Mních a pokračuje cez obce Lisková, križuje sa s cestičkami do Turíka či Lúčiek a ďalej smeruje cez Liptovskú Teplú do Bešeňovej, kde mávame vždy kratšiu prestávku. Táto obec je známa vodným areálom a pre nás najmä zdrojom pitnej minerálnej  železitej vody. Prameň nájdete asi po 200 metrovom stúpaní za hotelom Summit a je na pravej strane cesty, často tu stoja nejaké autá, tak sa dá ľahko nájsť.  Na opačnej strane je aj zaujímavý skalný útvar s krížom, odkiaľ je pekný výhľad najmä na Nízke Tatry. 
  
  
 Keď už sme pri pohoriach, takmer celou cestou je na pravo dobre viditeľné pohorie Chočské vrchy a známy Veľký Choč. Za  Bešeňovou je krátky zjazd a potom sú dve možnosti cesty. My zvykneme hneď za týmto zjazdom zabočiť doprava na vedľajšiu cestu, ale dá sa pokračovať aj rovno cez obec Potok. Po spomínanom zabočení na cestičku, ktorá je v zime neudržiavaná, čo sa spomína na tabuli je náročné stúpanie, ale na jeho konci je perfektný výhľad na časť Mary, ktorý si zvykneme vychutnať, vlastne oddýchnuť po tom stúpaní J . Ďalej nasleduje nebezpečný zjazd po rozbitej ceste, takže neodporúčam sa uniesť a popustiť brzdy, je tu aj dosť možné, že tu stretnete nejaké auto a zákruta na rozbitej ceste s autom zrazu pred očami neveští nič dobré. Tu dole už človek obchádza cestu popri Mare a dostanete sa tu až na križovatku. My sme išli cez ňu doprava cez obce Vlachy a Vlašky. Za nimi sa vyskytne strmé stúpanie č. 2 a je to stúpanie po hrádzi, takže na jeho konci sa človek ocitne na priehradnom múre. 
  
 Asi prvý krát sme si tu všimli, že akýmsi bočným otvorom aj odtiaľto vypúšťali vodu. Nuž bolo obdobie povodní, tento výlet sa totiž odohráva v prvý slnečný krajší víkend po povodniach. Naša cesta po zhodnotení stavu vody a debate pokračovala ďalej. 
  
  Rozhodli sme sa totiž, že si ešte pozrieme obec Matiašovce, kamarátka chcela vidieť nejaký kostolík a tak ma prehovorila. Míňali sme archeologické múzeum v prírode Havránok  a známy kostolík pri Mare a pokračovali sme cez Bobrovník, kadiaľ sa cesta naozaj vychutnáva. 
  
  
   
 Pred nami výhľad, klesanie,.... takto nám zľahka pribúdali kilometre. Za Bobrovníkom sme pokračovali doprava, smerom na Mikuláš až sme sa dostali na križovatku, ktorá nás zaviedla na opačnú stranu a odtiaľ asi 4 km do tých Matiašoviec. Presnejšie Liptovské Matiašovce. Tu sa mi už moc nechcelo. Začala ma zmáhať akási únava a kamarátkine nadšené rozprávanie o tom, ako kostolík v tejto obci dobre vyzerá ma moc nepovzbudilo, tak som hundrajúc šlapala do pedálov. Ich každým otočením sme však boli bližšie až sme sa tu naozaj dostali. 
  
  
   
  
 Pred kostolíkom zo začiatku 16. stor., ktorý je údajne renesančno - barokový a zasvätený sv. Ladislavovi je ešte socha svätého za dedinou, ako to Eňa volala. Opevnenie okolo kostolíka je zo 17. Storočia a je naozaj na pohľad zaujímavé. Okrem iného je kostolík zapísaný aj na zozname kultúrnych pamiatok pod číslom. 331. Obec obklopujú nielen Chočské vrchy, ale aj Západné Tatry, tak výhľad je akousi odmenou, pre cyklo - turistov. 
  
 Cesta naspäť bola ešte dlhá, ale krátky oddych s výhľadom prospel. Aby sme nešli rovnakou cestou, tak sme z Matiašoviec išli cez Liptovskú Sielnicu a nevracali sme sa cez Bobrovník, ale pokračovali sme miernym dlhším stúpaním a potom vyše 10 % klesaním do obce Potok už po známej ceste do Bešeňovej a do Ružomberka. Toto klesanie je za odmenu. Po stúpaní s tu rada nechám uniesť a skúšam maximálnu rýchlosť, ktorá sa tu dá vyšlapať. Je dobre počkať si však, kým človek prejde prvú zákrutu, potom je rozhľad dopredu dobrý a ak nejde auto, tak to stojí za to, 50 km/ hod nie je problém tu dosiahnuť, kto má odvahu môže aj viac, ale naozaj pozor a predtým si to radšej prejsť aj menej riskantne, predsa len najlepšie je cestu pred tým poznať.   Celý tento náš výlet mal dĺžku približne 60 km a inšpiroval nás na ďalšie kilometre 

