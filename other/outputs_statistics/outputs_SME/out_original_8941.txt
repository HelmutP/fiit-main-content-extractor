
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Kinik
                                        &gt;
                Nezaradené
                     
                 Hanba STV, alebo faul zozadu na slovenského športového diváka 

        
            
                                    16.6.2010
            o
            12:30
                        |
            Karma článku:
                11.33
            |
            Prečítané 
            3319-krát
                    
         
     
         
             

                 
                    Včera som si v priamom prenose pozrel zápas MS vo futbale Slovensko - Nový Zéland. Ale tak, ako už niekoľkokrát predtým, hoci aj v hokejových prenosoch, ani tento som nedopozeral na STV3(STV1), ale od 61.minúty už na ČT2. Aj týmto sa chcem poďakovať našim bratom zato, že mi vďaka ich vysielaniu umožnili užiť si tento športový sviatok akým 1.účasť slovenskej futbalovej reprezentácie na svetovom šampionáte určite je. Nepovažujem sa za náročného diváka, ale keď vidím, ako sa stále opakujú tie isté vážne nedostatky športových prenosov STV, tak to snáď, aby platila STV koncesionárske poplatky divákom.
                 

                 
Športové prenosy STV.Ján Kinik.
   Napr. dosť mi vadí pri pozerní nejakého zápasu pri nejakej šanci pred bránou, ak o pol sekundy dopredu viem pred ukončením akcie  či gól padne, alebo nie na základe predstihujúcej zvukovej zložky. Je to dosť frustrujúce, lebo vtedy je TV prenos ponížený na rozhlasový s obrazovou dokrútkou. Neviem či je problém len z prenosov z iných kontinentov a či zvuk prichádza rýchlejšie ako obraz možno aj inými prenosovými cestami, ale tak snáď nieje nemožné tieto 2 zložky zosynchronizovať a zaviesť pozdržujúcu zvukovú slučku tak, aby sme na našich obrazovkách videli obraz s korešpondujúcim zvukovým komentárom.   Ak by mi však niekto z STV tvrdil, že je to technicky nemožné, čomu síce neuverím, tak potom navrhujem, aby tento nedostatok, korigovali "profesionálni" komentátori STV a nech sami osebe komentujú s polsekundovým zdržaním.   Apropo, čo sa týka slovesnkých komentátorov, to je kapitola sama o sebe, ale ani na tomto poli si nepripadám ako náročný divák a k žiadnému neprechovávam nejaké sympatie ani antipatie čo viem, že veľa ľudí áno a vcelku je mi jedno ako nemastno neslano kto komentuje. Menej je niekedy viac a v podstate mi stačí, ak komentatori nepokazia prenos, takže osobne mám radšej skôr flegmatikov ako tých premotivovaných, ktorí pôsobia vyslovene neprofesionálne a pritom to možno len hrajú a napodobňujú tých juhoamerických komentátorov predbiehajúcich sa v dĺžke trvania výkriku gól.   Najhoršia kombinácia je, keď sa spojí rýchlejší prenos zvukovej zložky s komentárom Merčiaka rýchlejšieho ako smrť. Tak ako som už písal, že neprechovávam žiadne emócie ku komentátorom a tak aj Merčiaka viem zobrať, ale keď mi chlap zakričí pri Vittekovej šanci v 59.min gól a potom sa poopraví, že nebol, tak toto je maximálne faux-pas, po ktorom už musíte preladiť na iný kanál. Toto by sa naozaj nemalo stávať na profesionálnej komentátorskej úrovni a to už je jedno v akom športe. Vytvoriť planú nádej pre fanúšika je to najhoršie. Toto sa inak stáva podguráženým divákom v puboch, že pri každom náznaku šance už dopredu kričia gól, čo mi síce vadí, ale tak môj problém, že som sa rozhodol pozerať prenos v nejakom podniku, ak.   Na záver zosumarizujem riešenia vyššie popísaných problémov pre diváka:   1. STV sa poučí a začnú synchronizovať zvuk pozdržaním v slučke a komentátorom zakážu piť energtické nápoje na utlmenie ich agilnosti.   2. Divák si preladí TV na nejaký zahraničný kanál.   3. Divák si jednoducho vypne zvuk pri sledovaní STV a tak ho nič nebude rušiť.   4. Divák si zaobstará HTPC (mediálnu stanicu na báze počítača, ktorá sa pripája k TV) , kde si nainštaluje software umožnujúci pozdŕžanie zvuku v zvukovej slučke. Toto však nerieši neprofesionalitu komentátora, ale raz možno naprogramujú software na odfiltrovanie hlasu komentátora zo zvukovej zložky :-) .     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (38)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kinik 
                                        
                                            Zachraňovať nemecké banky? Lebo požičkami Grécku ide jedine o to.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kinik 
                                        
                                            Jeden aktuálny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kinik 
                                        
                                            Nevolič nech radšej ostane doma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kinik 
                                        
                                            Nedávajme deťom mená po rodičoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Kinik 
                                        
                                            Vlastenecká paródia, alebo Evita na slovenský spôsob
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Kinik
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Kinik
            
         
        jankinik.blog.sme.sk (rss)
         
                                     
     
        Jeden z 5mil. Slovákov
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1868
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Prečo Fico prehrá voľby
                     
                                                         
                       Voľby? Áno... No nie pre všetkých...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




