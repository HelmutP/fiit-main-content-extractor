
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Sliacky
                                        &gt;
                Nezaradené
                     
                 Prečo urážame zvieratá? 

        
            
                                    25.3.2010
            o
            14:30
                        (upravené
                25.3.2010
                o
                14:39)
                        |
            Karma článku:
                5.87
            |
            Prečítané 
            1369-krát
                    
         
     
         
             

                 
                    Prirovnania ľudských vlastností k zvieracím je ošemetné. Ktovie, čo by si o ňom pomysleli samotné dotknuté zvieratá, keby mali takú mozgovú kapacitu ako ľudská lebka. Vari by sa i urazili. Veď napríklad taký somár vôbec nie je hlúpe zviera.
                 

                 Toto ľudsko-zvieracie porovnávanie veľmi podrobne rozobral slávny český fejtonista Jan Neruda vo svojej Nedeľnej kázni na základe Darwinovej náuky. Okrem iného si v nej povzdychol: „Áno, bývali sme kedysi tiež takí vznešení ako zvieratá, ale je to už dávno! Bolo to v raji.“       Veľa by sa dalo písať napríklad o vlastnostiach somára, ktorý je v očiach človeka hlúpym vari iba preto, že dokáže na svojom chrbte niesť chlapa ako hora, alebo náklad, pod ktorým by sa podlomili nohy aj Rambovi.             Človek označuje somára za hlúpe zviera. Neprávom. Ale majú zvieratá vôbec nejaké práva, keď sa ich neraz nemôžu dovolať ani ľudia? Tohto somárika chovajú pri kláštore na Raticovom vrchu nad Hriňovou.       Dnes sa však chcem pristaviť pri krkavcoch, ktoré práve v tomto skorom jarnom čase privádzajú na svet svoje potomstvo. Často totiž počujeme o krkavčej matke, čo je absolútna urážka tohto vtáka, ktorý sa za posledné polstoročie po druhej svetovej vojne rozšíril od východu na západ po celom území Slovenska a stal sa bežným obyvateľom našich hôr. Jeho domovom sú predovšetkým staré hory s neprístupnými bralami, a ktoré sú prerušované pasienkami, poľami, rúbaňami, kde nachádzajú dostatok potravy. Je známe, že krkavce sa živia uhynutými zvieratami, čím plnia v prírode dôležitú hygienickú úlohu. Pravda, dokážu aj sami uloviť rôzny hmyz alebo drobné cicavce a vyberú aj vajíčka alebo mláďatá z vtáčích hniezd, čo už nám nemôže znieť sympaticky, ale je to úplne v súlade s neúprosnými zákonmi prírody. Tento príklad by si človek od krkavcov nemal brať, hoci opak býva pravdou, rád si zapytliači v cudzom hniezde.       Krkavce hniezdia už v tomto čase. Už vo februári sme mohli pri výletoch do prírody sledovať ich svadobné vzdušné hry. Hniezdo, ktoré - ak nie sú vyrušované - využívajú mnoho rokov, si stavajú na neprístupných skalách, prípadne na vysokých stromoch. Podľa pozorovaní ornitológov stavebné práce robí len samička, zatiaľ čo jej druh prináša potrebný materiál. Aj na vajciach, ktorých býva tri až sedem, sedí len samička a samček jej prináša potravu. O mladé, ktoré sa vyliahnu zhruba po troch týždňoch, sa starajú obaja rodičia rovnakou mierou,  a to až okolo jeden a pol mesiaca. Aj neskôr však zostávajú mláďatá pod ochranou rodičov.       O krkavcoch a ich príbuzných (vranách, kavkách, havranoch...) píše svetoznámy nemecký zoológ Alfred Brehm vo svojom Živote zvierat ako o najstarostlivejších rodičoch. Porovnanie „krkavčia mať“ naozaj pokrivkáva na obe nohy. Zrejme vychádza aj z ľudových povier a rozprávok, v ktorých sa spomínajú napríklad zhavranení bratia. Omnoho priliehavejšie je označenie týchto vtákov za múdre. Práve krkavce patria k obľúbeným chovancom sokoliarov, ktorí s nimi dokážu spestriť svoje vystúpenia. Spomínam si, ako ľudia v parku svätoantonského kaštieľa líhali na zem, keď šibal krkavec prelietaval tesne nad ich hlavami. A nedávno sme mohli v televíznom spravodajstve vidieť a počuť krkavca, ktorý sa naučil celkom ľudsky nadávať. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Veľryba je hladná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Tak im treba?! ??? !!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Alarm
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Hanba!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Sliacky 
                                        
                                            Viete, čo má spoločné mandarínka s „pukaným“ vajcom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Sliacky
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Sliacky
            
         
        sliacky.blog.sme.sk (rss)
         
                                     
     
        59-ročný muž s energiou a dôverčivosťou mladíka.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    249
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1437
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




