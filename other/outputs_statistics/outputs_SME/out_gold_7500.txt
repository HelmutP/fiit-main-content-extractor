

   
  
 Nehanbím sa , že som klesol znova, 
 že sám klesám veľmi ľahko, celkom oddane. 
 Nehanbím sa recitovať chladnej dlažbe zvláštne slová, 
 ani sporiť na chrbáte ranu na rane. 
   
 Nehanbím sa, za ranného svitu čítať lemovanie 
 tvojho rúcha. Hláskovať si zázrak z tvojej dlane. 
 Chytať Ducha, čo si bezstarostne nebom vanie, 
 dych života, klokot bystrín v čiernych očiach Lane. 
   
 Nehanbím sa za otvory v ubolenom dreve, 
 za korunu, ktorú z času na čas s tebou nosím. 
 Nebojím sa zavrieť všetky ligotavo krásne dvere 
 a otvoriť tie posledné, obyčajné. Celkom nahý, bosý. 
   
 A už vôbec nehanbím sa, že som klesol zas a znova 
 na kolená v tichom chráme, pred oltárom Slova. 

