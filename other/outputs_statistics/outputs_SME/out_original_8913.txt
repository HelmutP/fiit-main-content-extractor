
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viktor Štrba
                                        &gt;
                Príbehy zo života
                     
                 Dobrodružstvo v Rakúsku 

        
            
                                    16.6.2010
            o
            8:32
                        (upravené
                2.9.2010
                o
                14:08)
                        |
            Karma článku:
                7.71
            |
            Prečítané 
            2063-krát
                    
         
     
         
             

                 
                    Keď mi pred necelým rokom kamarát povedal, že by rád šiel na cyklotúru po povodí Dunaja, začal som si o tejto trase vyhľadávať informácie a tešiť sa na plánovaný výlet, cez čarokrásne údolie rieky Dunaj. To čo sa nepodarilo minulý rok, sa vďaka skorému plánovaniu ako aj požehnaniu vo forme slnečného počasia podarilo zrealizovať teraz.
                 

                 Nakoľko zladiť časový harmonogram a plány troch ľudí nie je jednoduché, tak napokon vyhrala alternatíva, mierne skrátenej trasy vedúcej z Bratislavy, cez Viedeň a Krems, až do Melku, a následne spiatočná cesta cez St.Polten a Viedeň.   Na túru sme vyštartovali vo štvrtok ráno krátko pred 9tou, pričom už v tom čase bolo pomerne teplo. Naša trasa nás zaviedla cez Most Lafranconi, Wolfsthal až do Hainburgu, kde sme sa napojili na cyklotrasu vedúcu popri Dunaji, pričom na pravej i ľavej strane sme mohli sledovať len vysoké stromy, a sem tam sa započúvať do štebotu vtáctva. Občas sme si zaspievali aj nejaké chvály, najmä Paťo, a tak nám pomaly ubiehala cesta. Do Viedne sme prišli krátko po 12tej, a tam sme ístú chvílu brázdili uličkami v snahe nájsť nejakú vhodnú reštauráciu. Neskôr sme došli ku jednému parku, a tam sa pri strome na chvíľu zložili, pričom ja som zjedol jedlo z domu, zatiaľ čo chalani si šli niečo kúpiť do obchodu.   Neskôr sme ešte hodnú chvíľu blúdili uličkami Viedne, v snahe nájsť Schonbrunn, avšak napokon keď nás domáci čiastočne zle navigovali, a napokon nám povedali, že je to veľmi ďaleko, tak sme sa rozhodli, že sa tam možno stavíme na ceste späť a radšej pôjdeme už ďalej na sever smerom na Klosterneuburg. Vo Viedni sme ešte kúsok blúdili, až sa nám napokon, a to už hádam boli takmer 4 hodiny popoludní, podarilo nájsť cyklotrasu vedúcu na sever von z Viedne. V čase blúdenia vo Viedni sme už miestami boli trochu nervózni, a nie vždy sme sa vedeli zhodnúť na najsprávnejšej trase, ale napokon sme to zvladli. Keďže už bolo pomerne neskoro, a podľa pôvodného plánu sme mali byť podstatne ďalej, v Klosterneuburgu sme sa príliš nezdržovali, aj keď to bolo pomerne pekné mesto.   Pokračovali sme ďalej popri rieke Dunaj, pričom cesta bola pomerne pohodová a príjemná, aj keď slnko pomerne silno pražilo. Prešli sme popri dedinách Kritzendorf a Altenberg, a pomaly sa blížili k ďalšiemu väčšiemu mestečku Tulln an der Donau. V mestečku sme sa pokochali pekným historickým centrom, doplnili zásoby vody a pobrali sa pomaly ďalej, napriek tomu, že Paťo navrhoval, aby sme ostali kempe tu pri meste. Ja som však dúfal, že ešte kým vládzeme a je ešte len krátko po  6tej, tak by sme mali skúsiť ísť až do kempu, ktorý je vzdialený asi 20 kilometrov, aj vzhľadom k tomu, že podľa plánu by sme mali byť  o hodne ďalej.   Neskôr keď už bolo takmer 8 hodín, a spomínaný kemp, mal byť asi ešte viac než 8 kilometrov vzdialený, a my už hodne unavení a trochu aj bezradní tak sme  v jednej dedinke (kúsok za Erpersdorfom) stretli jeden starší manželský pár, ktorý nám ukázal cestu k najbližšiemu kempu a pán nás aj takmer ku vchodu odprevadil.  Keď sme spokojní s pocitom vďačnosti rozložili stan a osprchovali sa, stala sa menšia komplikácia keď na posledného z nás už nevyšla voda, bo mali nejaký problém s čerpadlom. Keď som následne potom chcel ísť vedúcemu zaplatiť, tak nám platbu odpustil, keďže sme mali problémy s vodou. Pred spaním sme sa ešte pomodlili, veď sme mali za čo, zvládnuť 125 km v takej horúčave a bez nejakého tréningu nebolo jednoduché, a navyše, to, že sme napokon tak rýchlo došli ku kempu bolo tiež hodné vďaky. Ja osobne som v noci bohužiaľ veľa nenaspal, spočiatku mi bolo veľmi teplo, a nevedel som nájsť vhodnú polohu. Ráno už potom voda tiekla a pomaly sme opäť vyštartovali na cesty.   Vyštartovali sme až po 9tej, napriek tomu, že ja som už bol od 6tej hore. Po krátkej trase po pravom brehu Dunaja sme prešli na druhý breh cez vodnú elektráreň, a pokračovali dobrým tempom do mesta Krems. Na Dunaji sme zbadali nákladnú loď s názvom Ferro, ktorá nám robila spoločnosť hodnú chvíľu. V meste Krems sme najprv šli cez nejakú priemyselnú zónu, až neskôr sme sa jedného staršieho pána spýtali, ako by sme sa dostali do historického centra, a tak nás tam na bicykli odprevadil. Keď sme si trochu prezreli historické centrum Krems, kde sme mimo iného stretli aj českých cyklistov idúcich z Passau do Viedne, a chystali sme sa opustiť mestečko, znovu sme stretli nášho „rakúskeho sprievodcu", ktorý nám ukázal cestu smerom na obec Rossatz.   Krátko po opustení mesta Krems, keď sme šli po cyklotrase na pravom brehu Dunaja, sme opäť zbadali nákladnú loď Ferro, ktorá nás dokázala na základe nášho zdržania sa v meste dobehnúť. Síce sme boli postupne viac a viac unavení, tento úsek cesty bol celkom pohodový. Šli sme prakticky stále popri brehu Dunaja a občas sme na druhej strane brehu zbadali nejakú zrúcaninu hradu, či menší kostolík. V dedinke Rossatz sme sa zastavili na obed, na ktorom sme si pochutili v jednej z miestnych reštaurácií, kde sme si opäť aj doplnili zásoby vody. Okolo 1nej sme opäť vyštartovali na cestu a snažili sa bez veľkého zdržania sa čo najskôr dostať do mesta Melk. Hrad s názvom Schonbuhel, sa mi nepodarilo nájsť a navyše nás mierne zdržal a vyčerpal jeden menší kopec, ktorý sme museli asi 10 km pred Melkom zdolať.   Napokon sa nám podarilo pred treťou hodinou popoludní doraziť do mesta Melk, kde sme sa chvíľu kochali pohľadom na katedrálu, a následne sa prechádzali úzkymi uličkami mesta. Podarilo sa nám nájsť informačnú kanceláriu, kde nám mladá slečna, ktorá (na rozdiel od väčšiny Rakušanov) vedela celkom dobre po anglicky, poradila ako sa dostať k zámku Schallaburg, či do mesta St.Polten a podarovala nám aj mapu. Krátko na to sme šli do supermarketu, kde sme si nakúpili nejaké potraviny a veľkú porciu zmrzliny, ktorú sme prakticky hneď zbaštili. Mne sa ešte podaril aj jeden dobrý skutok, keď v čase keď som pred supermarketom strážil bicykle, ku mne prišla jedna mladá slečna a niečo mi hovorila po nemecky. Po mojom konštatovaní, že nehovorím nemecky mi ukázala svoj bicykel a spadnutú reťaz, ktorú som jej následne nasadil, za čo mi ona následne povedala: "Thank you!"   Nakoľko sme vedeli, že nás čaká nie príliš jednoduchý úsek cesty s pár kopcami, tak sme sa príliš v Melku nezdržiavali, aj keď by tam zaiste bolo ešte čo vidieť. Hrad Schallaburg bol pomerne blízko, aj keď sme museli na ceste k nemu prekonať 2 kopce, čo nás mierne zdržalo. Horšie bolo, že sme odtiaľ nenašli cyklotrasu, ktorá by nás viedla smerom k St.Poltenu, a cesta bola trochu členitá s občasnými prevýšeniami. Navyše slnko (napriek tomu, že už bolo takmer 6 hodín popoludní) ešte stále silno pražilo, čo nám tiež nepridávalo na silách. Pri jednom takom stredne-náročnom kopci som sa tesne pod jeho vrcholom vyčerpaný zvalil do trávy na kraji lesíka, kde som pár minút oddychoval. Dobre bolo aj to, že ma Paťo zásoboval tabletkami magnézia, ktoré mi pomáhali pri únave svalov.   Neskôr keď sme prešli cez obce Hurm, Bischofsetten a Weinburg, kúsok na juh od Wilhelmsburgu sme sa zastavili v jednej malej pizzerii, kde sme objednali pizzu, a doplnili zásoby vody. Mne už v tom čase nechutilo ani jesť, a od obeda som prakticky zjedol len fit tyčinku, a jablko. Dozvedeli sme sa, že najbližší kemp je v obci Traisen, čo je asi 7 km na juh. Ja som však bol naklonený k alternatíve ísť ešte kúsok ďalej a radšej ako na juh zabočiť mierne na východ, aby sme mali kratšiu trasu v sobotu, pričom podľa mojich informácií tam mal byť asi 5 km východne od Traisenu kemp s názvom Rainfeld.   Obce cez ktoré sme v tomto čase prechádzali boli prakticky vyľudnené, a nemali sme sa koho spýtať na cestu. V jednej krčme nám nejaké dievčatá povedali, že najbližší kemp je až v Hainfelde, čo je asi 15 km. Ich nie celkom istú odpoveď, ktorú podali jazykom, ktorý sa podobal asi viac na nemčinu ako angličtinu, som bral s istou rezervou, a tak či tak sme nemali príliš na výber, len sa pobrať ďalej na východ a dúfať, že nájdeme kemp skôr ako až o tých 15 km. Neskôr sa už začalo postupne zmrákať, a Hainfeld, alebo akýkoľvek kemp bol ešte v nedohľadne. V tom sa k nám približovali dvaja cyklisti „v rokoch", ktorých sme sa spýtali, či nevedia o nejakom blízkom kempe, pričom nám povedali, že Hainfeld je asi 10km. Po našej otázke, či nie je niečo bližšie, lebo sme už veľmi unavení a je už skoro tma, tak nám jeden z pánov navrhol nech sa zložíme u neho v záhrade, že tam budeme za 3 minúty. Takúto skvelú ponuku sme v tom čase neodmietli (veď sme v ten deň už prešli 117 kilometrov), a boli sme mu veľmi vďační za jeho láskavosť. Dovolil sa nám osprchovať, ponúkol nám aj jedlo a pitie, a zoznámil nás aj s rodinou. Mimo iného nám spomenul aj mladú Slovenku, ktorá sa vo vedľajšom dome stará o jeho mamu, ktorá trpí Alzheimerom. Napokon sme u neho v obývačke sledovali zápas majstrovstiev sveta a diskutovali o živote. Okrem iných vecí sme sa dozvedeli, že jeho prvá manželka zomrela na rakovinu, a teraz s druhou ženou Číňankou Lee majú syna Maxima, ktorý má 3 roky.   V tú noc som spal trochu lepšie, aj keď opäť som sa zobudil v polovici noci, pričom tentoraz ma prebudil hlad. Ráno sme potom všetci zasadli za jeden stôl, pričom pán domu priniesol čerstvé pečivo, a tak sme si výborne pochutili. Napokon nám ešte poradil ktorou trasou máme ísť smerom do Viedne, a čo nás na nej čaká, nakoľko on skúsený cyklista už má všetky vyskúšané. S veľkou vďakou a spokojnosťou sme sa s láskavými Rakúšanmi rozlúčili a vybrali sa na záverečný úsek výletu.   Na úvod nás čakali dva pomerne náročné kopce, po zvládnutí ktorých sme síce boli dosť vyčerpaní, ale trasa už bola pomerne jednoduchá, vedúca cez Kaumberg, Altenmarkt a Berndorf, až do Leobersdorfu. Ešte v Hirtenbergu sme sa zastavili na obed, a potom už šli po cyklotrase do Viedne. Vyčerpaný sme boli už dosť, ale cesta ubiehala pomerne rýchlo, až na záverečnú komplikáciu, keď sme vo Vosendorfe,v  spleti obchodných domov, či nákupných centier nevedeli nájsť cestu do Viedne, nakoľko sme sa odchýlili od cyklotrasy, a všetky cesty vyzerali, že sú diaľnice. Bolo to mierne deprimujúce, byť už tak blízko a pritom tak ďaleko, ale našťastie nám jeden pán napokon povedal presne ako máme ísť na hlavnú cestu do Viedne.   Vo Viedni sme ešte kúsok blúdili, ale už to bolo relatívne v pohode. Akurát, že Martin sa rozhodol, že ešte má dostatok síl a pôjde z Viedne (nie vlakom ako Paťo a ja)  normálne cyklotrasou späť do Bratislavy, a tak sme sa na stanici vo Viedni rozlúčili a popriali mu veľa štastia. Náš konečný súčet za 3 dni činil 125+117+90=332 km, pri priemernej rýchlosti 18 km/h (Maťo dosiahol takmer 400km)   Síce s úrovňou angličtiny Rakúšanov nadšený byť nemôžem, ale o láskavosti a pohostinnosti niektorých z nich by sa dali azda písať romány. Tak snáď len dodám: Bohu vďaka za tých pár úžasných ľudí, ktorí nám neskutočným spôsobom zjednodušili a spríjemnili našu cestu po Rakúsku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (78)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Štrba 
                                        
                                            Riziká pred druhým kolom prezidentských volieb
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Štrba 
                                        
                                            Výlet na Skalku alebo splnený sen
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Štrba 
                                        
                                            Predmanželská intimita
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Štrba 
                                        
                                            Náročný výstup v pohorí Belledonne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktor Štrba 
                                        
                                            Výlet na Kráľovu hoľu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viktor Štrba
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viktor Štrba
            
         
        viktorstrba.blog.sme.sk (rss)
         
                                     
     
        Mladý muž, ktorý sa snaží žiť podľa istých pravidiel, a stavať sa k životu zodpovedne. V živote som už všeličo zažil na druhej strane ma ešte čaká dlhá cesta učenia sa pokory a lásky.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    74
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1791
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života ľudí
                        
                     
                                     
                        
                            Škola
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Príbehy zo života
                        
                     
                                     
                        
                            Súkromné
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Roxette
                                     
                                                                             
                                            Bryan Adams
                                     
                                                                             
                                            Richard Marx
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Pavol- človek s otvoreným a dobrým srdcom
                                     
                                                                             
                                            Lenka Ivančíková
                                     
                                                                             
                                            Zuzana Nešporová
                                     
                                                                             
                                            Kristína Janíková
                                     
                                                                             
                                            Martin Vystavil
                                     
                                                                             
                                            Martin Basila
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Študentský hlas
                                     
                                                                             
                                            Farský úrad Vrútky
                                     
                                                                             
                                            Literárny klub Generácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Diktatúra sexuálnych menšín
                     
                                                         
                       Bieda referenda na Slovensku
                     
                                                         
                       O slovenských chlapoch
                     
                                                         
                       Prečo Rytmus môže to, čo nemohol Mikla ?
                     
                                                         
                       Vždy Vás milí muži budeme potrebovať.
                     
                                                         
                       Oplatí sa investovať do dlhopisov od žralokov?
                     
                                                         
                       Tak ako sme ľahostajní k iným, sme ľahostajní aj k sebe samým.
                     
                                                         
                       Rozhodli sa postaviť hrad (château de Guédelon )
                     
                                                         
                       "Kamoš, nemáš požičať 50 centov? Vrátim ti ich."
                     
                                                         
                       Kde máš to dieťa?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




