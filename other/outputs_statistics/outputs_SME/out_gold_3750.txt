

  Neznášam tie dni, kedy su okolo vás kamaráti, známi, ľudia či už len v električke, v škole, alebo sa na vás usmeje alebo zamračí okoloidúci a vy máte chuť ich zrazu zmraziť, vygumovať a najviac čo by ste si na svete priali je, byť niekde sama.. nikto len vy a vaše druhé ja..  Neznášam dni, keď svieti slnko a ja sa usmievam, pričom vo vnútri plačem a trpím.. 
 Neznášam dni keď prší, ja sa usmievam, a pritom vo vnútri neviem čo so sebou, kričím od zúrivosti, smútku a bezmocnosti.. 
 Neznášam dni, keď sa so mnou niečo deje a ľudia to ignoruju..  Neznášam dni, keď sa so mnou niečo deje a ľudia to strašne riešia..  Neznášam dni, pomaly plynúce a hranicu nepoznajúce, dni, kedy mate doslova chuť všetkých "pozabíjať", všetko vás rozčúli a vám je to potom ľúto..  Neznášam také dni a neznášam to, že nikdy neviem ako proti nim bojovať.. Ako ich čo najrýchlejšie poslať preč a nedať im šancu aby kazdý deň, keď sa zobudím, každé ráno, ktoré otvára nový deň, nebolo také isté ako to predchádzajúce alebo ešte horšie..   Naozaj také dni nemám rada.. Ale som rada, že takých dni je naozaj veľmi málo a zakaždým mám pri sebe ľudí, ktorí dokážu pochopiť, že nie vždy svieti slnko.. 
 A práve vďKaka nim, rodine, priateľom- také dni neprerastajú do dlhých týždňov, nekonečnyýh mesiacov, rokov... 

