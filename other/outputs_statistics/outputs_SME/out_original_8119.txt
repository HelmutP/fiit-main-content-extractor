
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Eva Krajmerová
                                        &gt;
                Súkromné
                     
                 EXPO zazeračky 2010 

        
            
                                    5.6.2010
            o
            19:52
                        (upravené
                5.6.2010
                o
                20:18)
                        |
            Karma článku:
                4.12
            |
            Prečítané 
            999-krát
                    
         
     
         
             

                 
                    Až keď človek dorazí na opačný koniec zemegule, tak pochopí, akí sme my Slováci k sebe občas (alebo veľmi často?) neprajní. Mnohí naši ľudkovia dokážu totiž prvoplánovo hľadať chyby aj na sebalepšom výtvore našich kreatívcov a organizátorov medzinárodných podujatí. Zarážajúce je, že takýto postoj zaujímajú aj Slováci, ktorí nikdy nevytiahli päty z našej krajiny – takže posudzujú dačo iba z druhej ruky – aby reč nestála...
                 

                 
Slovenský pavilón v noci 
      Pôvodne som chcela písať o tom, akí sú Číňania nadšení zo  slovenského pavilónu  na EXPO 2010 v Šanghaji. Ale predbehli ma podaktorí neprajní novinári, architekti, diplomati, a pseudoodborníci na cestovný ruch - jednoducho typickí Slováci, ktorí nedokážu s hrdosťou povedať: vďaka za prezentáciu Slovenska v zahraničí  (veď ju potrebujeme ako soľ) ! Okrem toho treba dodať, že svetová  výstava trvá pol roka a v tomto časovom horizonte sa tiež kde-čo udeje: vystrieda sa niekoľko desiatok hostesiek, príde zopár stoviek oficiálnych návštev, ale najmä niekoľko stotisíc návštevníkov.       To všetko je ale šmahom jediného šomravého, ba až urážlivého pohľadu vetované, lebo  slovenská predstava  je väčšinou iná. Aká? To nie je dôležité. Ale vraj Česi majú lepšiu expozíciu. Áno, je priestrannejšia i pestrejšia, keďže dostali na pavilón 7x vyšší rozpočet. Alebo Nemci. Tí však mali v rozpočte na šanghajské EXPO 100x viac. Každý z týchto otrávených jedincov ale zabúda na návštevníkov: a čuduj sa svet, tí v Šanghaji sú nadšení, spokojní, vysmiati - a najmä v trpezlivej pohode (dokážu vyčkávať aj niekoľko hodín, aby si pozreli tú-ktorú expozíciu). Sú to totiž jedineční Číňania, ktorí doprajú úspech aj iným. Navyše nie sú žiadni nerváci a celý deň si pospevujú. A keď si pozriete návštevnú knihu slovenskej expozície, tak pozitívnym superlatívom niet konca-kraja. A státisíce fotografií usmiatych šikmookých človiečikov  s našimi švárnymi tanečníčkami, ale aj s dobovými exponátmi budú pre nich dlhé roky spomienkou na to, že navštívili slovenský pavilón. Dokonca akceptovali aj skutočnosť, že už nie sme Čechoslováci - veď český pavilón je vedľa slovenského, tak to rozdelenie je viac ako evidentné.      Nevadí, azda sa raz zmeníme. Za seba a množstvo dobroprajných Slovákov blahoželám slovenskému realizačnému tímu, ktorý rozhodne na nikoho nezazerá a nehrá sa na izolovanú republiku v ďalekom svete. Na vlastné oči som videla množstvo spokojných návštevníkov a jeden z čínskych novinárov mi povedal, že sme sa priam úžasne trafili do čínskej hravej nátury s našim príťažlivým pavilónom. Mne sa náš pavilón páčil, i film Pavla Barabáša  o našej krajine, aj správanie nášho personálu. Pravdaže, chcela by som raz vidieť nadšene omráčených návštevníkov po vzhliadnutí našej expozície (ako napr. nemeckej), ale ako sa hovorí, zatiaľ sa radšej držme pri zemi a vyskakujme do reálneho rozpočtu, ktorý nám bol pridelený.          Najprv som nechápala, čo si to Číňania pri vonkajšom pohľade na slovenský pavilón kreslia do svojich drobných dlaní; čínsky novinár mi to však rozlúštil: slimáčik-máčik... tento pavilón sa nám páči. Bravó Slováci!                                      foto: Eva Krajmerová                                                                                                                                                                                                                                                                         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Voniaš ako...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Týrané Slovenky, nedajte sa !
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Spod plesovej krinolíny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Prírodný detox ľudského organizmu zvládne každý
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Eva Krajmerová 
                                        
                                            Rok čínskeho draka má optimistický charakter
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Eva Krajmerová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Eva Krajmerová
            
         
        krajmerova.blog.sme.sk (rss)
         
                                     
     
        Autorka v júli 2012 zomrela. Blog nechávam na jej pamiatku. Dcéra Katka
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    235
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2101
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voniaš ako...
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




