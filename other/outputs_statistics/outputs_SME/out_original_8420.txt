
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nikolaj Vyšnikov
                                        &gt;
                Slovensko
                     
                 Hodnotová prázdnosť slovenskej politickej scény 

        
            
                                    9.6.2010
            o
            11:51
                        (upravené
                17.6.2010
                o
                22:58)
                        |
            Karma článku:
                6.61
            |
            Prečítané 
            1037-krát
                    
         
     
         
             

                 
                    Slovenské politické strany sú obyčajnými eseročkami so značne neschopným manažmentom a investormi, ktorí rátajú s návratnosťou svojich investícií. Ľudia s rozvinutou politickou pamäťou na celý systém už dávnejšie rezignovali, čo potvrdzuje aj znižujúca sa volebná účasť.
                 

                 
  
  Námet k napísaniu tohto článku som v sebe držal niekoľko rokov. Pôvodne som vám chcel poskytnúť niekoľko argumentov, prečo slovenská politická scéna nie je o boji ľavice a pravice, pričom výstupom mal byť apel na ignorovanie parlamentných volieb. Strany nás kŕmia prázdnymi rečami, nedodržiavajú svoje politické programy, ba dokonca ani programové vyhlásenia vlád. Slovenské politické strany sú obyčajnými eseročkami so značne neschopným manažmentom a investormi, ktorí rátajú s návratnosťou svojich investícií. Ľudia s rozvinutou politickou pamäťou na celý systém už dávnejšie rezignovali, čo potvrdzuje aj znižujúca sa volebná účasť.    Nevoliči (ak odpočítame niekoľko percent, ktorých nezaujíma nič) nie sú leniví a už vôbec nie spokojní. Neúčasť na voľbách je jasným dôkazom mizernej politickej ponuky. Medzi politickými stranami nevidieť rozdiely. Celá scéna sa delí na niekoľko táborov. Jedni sa hrajú na ľavičiarov, druhí na pravičiarov a tretí na národovcov resp. ochrancov národnostných menšín. Ale akonáhle dôjde na lámanie chleba, tak ich zaujíma len vlastný biznis.    Akonáhle dôjde ku štiepeniu strany, odídenci už nemajú čo stratiť a predávajú informácie. Informácie sa stávajú dôležitým politickým tovarom, ktorý treba použiť v tom najsprávnejšom čase. Všimli ste si, ako sú opoziční politici aktívni pri zverejňovaní rôznych káuz? Nebolo by nám všetkým lepšie, keby boli všetky politické strany v opozícii a krajinu by riadili úradníci s minimálne troma titulmi? Stačí nastaviť systém tak, aby boli politici zapriahnutí do koča a my by sme pred nich zavesili na udici mrkvu.    Ak spoločnosť nedokáže vyprodukovať osobu, ktorá má na to stať sa prezidentom/kou, tak by nemala mať prezidenta. V histórii samostatnosti sme zažili krátku dobu bez prezidenta a mne osobne nechýbal. Bolo by zaujímavé aplikovať niečo podobného aj na ostatné voľby. Kandidáti na primátora nestoja za nič? Voľme proti nim a mesto by najbližšie volebné obdobie riadila úradnícka vláda.    Ale späť ku veľkým stranám. Vyššie som spomenul, že sa len na niečo hrajú. Postaviť svoj program na národných záujmoch a záujmoch národnostných menšín dokážu len najprostejší politici. Je príliš jednoduché hádzať celé národy do jedného vreca. Nikto si pri narodení nevyberá, kde sa narodí a akú farbu pleti bude mať. Strany, rozlišujúce občanov na základe rasy alebo príslušnosti k národu, nemajú čo existovať. A hlavne ak zneužívajú konflikty na získanie prístupu ku korytu.    Ochrana štátnych záujmov? Akého štátu? Ja som sa narodil v Československu a za to, že dvaja pošahaní politici rozbili našu krajinu, nemienim súčasné usporiadanie povyšovať na našu modlu. Sme súčasťou Európskej únie, zrušili sme hmotné hranice, ktoré boli prežitkom dávno prekonaných čias. Jediné hranice, ktoré naďalej pretrvávajú, sú v našich hlavách. Tear them down!    Úvod by sme mali za sebou. Nasleduje moja obľúbená téma: ľavica vs. pravica. V prvom rade by sme si mali zadefinovať, čo vlastne ľavica a pravica je. Existuje spústa definícií, čiže v tomto prípade sa bude jednať čisto o moju predstavu. Hlavnou oblasťou záujmu pravice by mala byť politika zameraná na zamestnávateľov a samozamestnávateľov. Čo tieto skupiny v skutočnosti potrebujú? Dominujú ich potrebám nízke dane alebo niečo iné?    Pre investora je najdôležitejšia stabilita investície. To je hlavný dôvod, prečo sa stále držia v krajinách s vyšším daňovým zaťažením. Nižšie dane nestoja za riziko straty celej investície. Investor vyhľadáva bezpečnosť, domáhateľnosť práva, úspešný boj s korupciou, rovné šance pri obstarávaní, likvidáciu zbytočnej byrokracie a boj proti monopolom (prostredníctvom štátnych zásahov).    Toto sú oblasti, ktorým sa má venovať moderná pravica. Avšak na Slovensku sa udomácnila určitá vulgárna populistická pravica, ktorá nerieši najpálčivejšie problémy. Kupuje si svojich voličov, ako všetky ostatné slovenské politické strany. Svojou hlúpou politikou položila mnohé firmy na kolená. Opačný tábor reprezentuje pseudoľavicová strana, ktorá v žiadnom prípade nereprezentuje modernú socialnodemokratickú stranu, zameranú na rôznou formou znevýhodnených občanov, silnú environmentálnu politiku a liberálne témy.    Poslaním modernej ľavicovo-liberálnej strany nesmie byť slepé rozširovanie sociálneho štátu, ale jeho zefektívňovanie. Obrusovanie nerovností musí byť adresnejšie pri minimalizovaní plytvania. Takáto strana musí preferovať inovatívne hospodárstvo, kvalitné školstvo, zdravotníctvo, úspešný výskum a rozširovanie práv občanov. Ako sa pozerám, tak sa pozerám, Smer tieto kritériá vôbec nespĺňa.    Tak koho ísť voliť? Pred časom by som vám odvetil: nikoho. Situácia sa však zmenila. Jedinou verejne prospešnou motiváciou zúčastnenia sa týchto volieb je boj proti neonacizmu. Zlá politika plodí u voličov nezáujem, pričom zároveň otvára cestu pre skupiny parazitujúce na (po dekádach) vykvasených predsudkoch. Suma sumárum – svojou voľbou neposuniete Slovensko ďalej (posunú ho tam pracujúci ľudia), avšak aspoň znížite volebný zisk neonacistov a to je veľmi dôležité.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (23)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Prečo končím s blogovaním
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Komunistická hrozba v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Legalizácia marihuany
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Aj v Iraku rastú dubáky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolaj Vyšnikov 
                                        
                                            Reforma manažmentu krajiny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nikolaj Vyšnikov
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nikolaj Vyšnikov
            
         
        vysnikov.blog.sme.sk (rss)
         
                                     
     
        Nesympatizujem ani s jednou slovenskou politickou stranou.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    41
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1574
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Zahraničie
                        
                     
                                     
                        
                            Zelená revolúcia
                        
                     
                                     
                        
                            foto
                        
                     
                                     
                        
                            Ostatné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Prečo kríž a nie radšej cesnak?
                                     
                                                                             
                                            Pred 60 rokmi sa v čase Povstania zrodil týždenník Nové slovo
                                     
                                                                             
                                            Rozprášená ľavica
                                     
                                                                             
                                            Posudzovanie vplyvov na životné prostredie je trápne
                                     
                                                                             
                                            VŠE – líheň nové generace
                                     
                                                                             
                                            Prvý verzus druhý pilier
                                     
                                                                             
                                            rozhovor - Rado Geist
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Lukáš Zorád
                                     
                                                                             
                                            Jano Topercer
                                     
                                                                             
                                            Peter Daubner
                                     
                                                                             
                                            Daniel Hromada
                                     
                                                                             
                                            Soňa Svoreňová
                                     
                                                                             
                                            Juraj Draxler
                                     
                                                                             
                                            Monika Martišková
                                     
                                                                             
                                            Jaroslav Hrenák
                                     
                                                                             
                                            Michal Feik
                                     
                                                                             
                                            Milan Vaňko
                                     
                                                                             
                                            Viliam Búr
                                     
                                                                             
                                            Juraj Lukáč
                                     
                                                                             
                                            Kamil Kandalaft
                                     
                                                                             
                                            Jan K. Myšľanov
                                     
                                                                             
                                            Peter Weisenbacher
                                     
                                                                             
                                            Eduard Chmelár - aktualne.sk
                                     
                                                                             
                                            Eduard Chmelár - sme.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Al Jazeera
                                     
                                                                             
                                            CEPA   SPZ   VLK
                                     
                                                                             
                                            Guardian
                                     
                                                                             
                                            BBC NEWS
                                     
                                                                             
                                            Ekoporadňa
                                     
                                                                             
                                            Earthlings - VIDEO, en
                                     
                                                                             
                                            EurActiv.sk - všetko o EÚ
                                     
                                                                             
                                            Stiglitz o globalizácii - VIDEO, en
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




