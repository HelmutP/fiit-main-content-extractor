
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Šuraba
                                        &gt;
                Nezaradené
                     
                 Ako sme chodievali na futbal 

        
            
                                    18.5.2010
            o
            16:46
                        (upravené
                18.5.2010
                o
                17:42)
                        |
            Karma článku:
                3.47
            |
            Prečítané 
            1014-krát
                    
         
     
         
             

                 
                    Venujem Tomášovi  Minule som zakročil ku svojej strednej škole. Sadol som si na lavičku,  pozeral sa na čerešne a chechtal sa. Poďme sa trochu baviť  o huncútstvach, zlodejstvách, čo sme vystrájali. Ja som hrozný lapiduch,  derbák, potmehúd, lajdák a tak vám čosi napíšem o tom.
                 

                     Minule som zakročil ku svojej strednej škole. Sadol som si na lavičku, pozeral sa na čerešne a chechtal sa. Poďme sa trochu baviť o huncútstvach, zlodejstvách, čo sme vystrájali. Ja som hrozný lapiduch, derbák, potmehúd, lajdák a tak vám čosi napíšem o tom.       Chodievali sme každý piatok hrávať futbal, kdesi na Lechkého ulicu. Bol to úžasný únik od školy, každý tam vstúpil svojim humorom a darom priateľstva. Párkrát sa niektorým z nás podarilo miesto lopty kopnúť tenisku na Bankov. Boli to pamätné piatky, bavili sme sa o futbale a o dievčatách a randili s kadekým. Potom sme si rozprávali svoje zážitky, ako sme sa bozkávali a nejakí bezdomovci nám fandili. Krásna romantika. Na tých piatkoch som zväčša začínal rozprávať príbehy, „V piatok na NOSke", alebo „Keď som hral za Barcu." Keďže článok bude o mojom milovanom gymnáziu, začnem aj svojou obvyklou formulkou.       V piatok na NOSke sme sedeli s Ivanom v lavici. Pred nami sedel Tomáš a Mišo. Tomáš bol v tom čase mojim verným športovým spoločníkom. (Teraz pre mňa znamená oveľa viac.) Mišo bol dobrý chalan, naučil ma naobedovať sa za 10 minút. No ani sa mu nečudujem, bol veľkým labužníkom. Do školy si nosieval tlačenku s octom, v autobuse vytiahol držkovú polievku a ešte mal siahodlhé labužnícke sny o školskej jedálni. Jeho srdce a žalúdok túžilo po jedinom jedle, býčie žľazy. Okrem toho, že bol maškrtník, bol veľmi pozorný šuhaj. Raz sme čítali na španielčine článok o Aztékoch. Vyučujúca sa spýtala, kto dobyl spomínanú ríšu a niekto vpredu vykríkol, že Robo Kazík. (Ten niekto vpredu práve píše tento článok.) Mišo sa na mňa pozrel, profesorka div nevyskočila z kože a sladkým hlasom vykríkol, „Kruhy v obilí!" Veru, naša španielčinárka si s nami užila, ale ja ju mám dodnes desne rád. (To len na okraj, aby som ukázal charakteristiku.)     Skrátka a dobre, keby ste raz chceli vidieť divokú džungľu, mali by ste zájsť na exkurziu k nám v piatok na NOSke. Spolužiak drichmal v poslednej lavici, raz ako superman a druhýkrát ako hodinár. Ivan vedľa mňa odložil poznámky do kúta a skríkol na celú triedu, „Idem sa napapať!". Spolužiačky sa rozprávali o nejakom večernom programe a ja som myslel na sobotňajší futbalový zážitok. Vyučujúca sa prísne pozrela na nás dvoch s Ivanom. Ja vám to trochu opíšem.       Predstavte si stôl rozhádzaný papiermi, skrátka nejaké byrokratické smetisko. (Ja si myslím, že byrokracia nie je ekonomický smer, je to nejaký prazvláštny fetiš k papierom, alebo sa niekto pomstieva stromom.) Na tých papieroch nejaký chlieb so slaninou a fľaša čaju. Vyučujúca sa na nás prísne pozrela, my sme do seba buchli, papiere spadli na zem, čaj sa na nich vylial. Zhodou okolností na jednom papieri bol Imanuel Kant a my sme videli ako tento filozof naozaj plače.       „Martin! Ivan!" Skríkla a už bol oheň na streche. Potom hučala, vrešťala akoby jej vypadali vlasy.       My sme sa zatvárali ako martýri. Akože sme nikomu nič neurobili a tak. Potom nás upozornila, že máme byť ticho a správať sa kultivovane. Pozreli sme na seba, vyprskli sme smiechom a rozprávali sa ďalej. Je inak úžasné, aký má človek vzťah s človekom, ktorý s ním sedí. Vyučujúca sa však trochu usmievala a pozrela sa na tabuľu, aby sa naše pohľady nestretli.       Tomáš sa pozrel dozadu a ja naň. Vedel som, čo znamenajú tie iskričky v očiach. „Ideme zajtra na Viktorku?" Opýtal sa nás.       „Mne sa nechce ísť na Viktorku. Ale futbal je zajtra." Skríkol som akoby zajtra mali prísť do Košíc minimálne Gabriel Batistuta s Alessandrom Nestom.       „Zajtra je futbal? To hráme so Slovanom?" Povedal a začal sa tešiť.       No tak som prišiel domov, hodil tašku do kúta a tešil sa. Možno som prevetral svoj milovaný bicykel, vyspal sa a šiel na ten futbal.       Väčšinou v električkách jazdili samí experti. Ktorý športový fanúšik by nepoznal v Košiciach uja, ktorý predával pred Tescom Večerník? A ten nastúpil ešte na jazere v drese a začal vrieskať na celú električku, „Smrť Slovanu!" (Pokiaľ nenastúpili revízori.)       Niekde na Terase nastúpil aj Tomáš a po ňom nejaký ujo, ktorý kričal v električke, „Nech rozhodca umrie! Kto ide so mnou do Janka? Koľko stojí poldeci v Tryskáči?" Alebo ešte skríkol, „Taká je mi zima, že mi sopeľ v nose zamrzne." Podobné romantické reči viedol pri dievčatách, ktorým nebolo ani 15 rokov.       Úplne najkrajšie to bolo na amfiteátri. Nastúpil nejaký chlap, sadol si ku nám a začal, „Chlapci, ja som letec. Ja som bojoval na Pearl Harbor! Napoleón tam vyprášil tých boľševikov!" Ryčal ani tur.       No a potom sme konečne prišli na zápas a ako správni fanúšikovia sme chceli preskočiť cez plot. (Veru, duch mladého Emana sa v nás nezaprie.) Mali sme jedno také vyhliadnuté miesto, kde sme vystrájali podobné kúsky. Ale akosi sa pred to miesto postavili policajti, vraj kvôli zúrivým Slovanistom. Slovanisti nám inak ukazovali svoje zadky a iné národné pamiatky ich tela ešte z električky. Bol to kúzelný pohľad, ani sa nečudujem, prečo toľko ľudí utekalo k Jankovi a prečo tam sociálne zariadenia vyzerajú ako vyzerajú. (Neodporúčam, až po štvrtom pive.)       Ako sme tak hľadali nejaký ďalší bod, prišiel k nám nejaký chlap. „Chlapci! Chcete ísť na futbal?" Pozreli sme naň a mysleli, že je to policajt.       „No tak, nerobte zo mňa belzebuba! Ja vás pustím, mám tu garáž, to len tak prejdete. Ale nabudúce mi doneste dva litre vodky." Povedal a my sme šli na ten Slovan.       Keď ma pamäť neklame, nabudúce sme si už kúpili lístky. Predsa boli lacnejšie ako dva litre vodky.       Čo vy a vaše šibalstvá?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (18)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            O Košiciach
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Návrat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Železničná krčma
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Pierre Picaud
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šuraba 
                                        
                                            Veľmi, veľmi málo o rytieroch okrúhleho stola
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Šuraba
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Šuraba
            
         
        suraba.blog.sme.sk (rss)
         
                        VIP
                             
     
        


 
geovisite


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    295
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1672
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Malý princ
                        
                     
                                     
                        
                            Českí herci
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Sestra
                                     
                                                                             
                                            Najkrajšia milovníčka mačiek
                                     
                                                                             
                                            Moja zlatá Šárka
                                     
                                                                             
                                            Spolužiak-knihovník
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Veď kliknite:-)
                                     
                                                                             
                                            Blog nielen o bezpečnosti IT
                                     
                                                                             
                                            Kúpte si skriptá
                                     
                                                                             
                                            Jedna šikovná archivárka
                                     
                                                                             
                                            Nevinné duše troch kamarátov
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sila / Slabosť (?)
                     
                                                         
                       Giethoorn - holandským mestečkom na člne
                     
                                                         
                       Základy Poa
                     
                                                         
                       Električkové zízanie
                     
                                                         
                       Víkend v Derbyshire: Chatsworth House
                     
                                                         
                       Krátko o ženách a meškaní
                     
                                                         
                       Gionatanove farby
                     
                                                         
                       Divadlo na jednotku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




