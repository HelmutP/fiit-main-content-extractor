
 Uz v 9. storoci mozeme hovorit o kmenoch ceskych - bolo ich niekolko,
aj o Polanoch, aj o Starych Chorvatoch , Korutancoch - len o Starych
Slovakoch nie? Ak zoberieme premisu, ze predkovia dnesnych obyvatelov
Slovenska a Moravy boli totozna skupina, mozeme ich oznacit ako Stari
Slovaci ci Stari Moravania, az potom, zaberom Moravy Cechmi sa oddelili
dnesni Moravania od dnesnych Slovakov. Najvhodnejsie pomenovanie
obyvatelov Velkej Moravy je Slovieni, ktori su predkami Slovakov, ako
aj Moravanov. Teda aj pomenovanie pre nasich predkov a zaroven
obyvatelov jadra Velkej Moravy (Moravsko a Nitriansko) by malo byt
Slovieni, Sloveni, avsak je jasna kontinuita osidlenia a teda aj je
mozne tvrdit ze Slovieni su predkami dnesnych Slovakov. Ale urcite nie
je scestne dat rovnitko - Slovieni su Stari Slovaci, teda predkovia
dnesnych Slovakov, tak ako stotoznujeme starych Grekov a dnesnych
Grekov. A to, ze podla archeologickych nalezov je jasna kontinuita
osidlenia uzemia, predsa pan Kovac velmi dobre vie! 
Starosloviensky jazyk: 
D. Kovac tvrdi ze starosloviensky jazyk bol viac pribuzny ukrajincine a
rustine. To je pritiahnute za vlasy. Ak mysli jazyk, ktory pozname z
pisomnych zaznamov toho obdobia, bol to jazyk umelo vneseny
cyrilometodskou misiou. Ale domaci, hovorovy jazyk predsa bol iny. Aj v
pisomnostiach mozno zbadat stopy domaceho jazyka, ktory ma jasne crty
zapado- (juho)slovanskych jazykov. Dalej, jazykove rozdiely boli ovela
mensie ako dnes, co neznamena, ze hovorovy jazyk starych Slovienov bol
blizsi ukrajincine. 
Pomenovanie Slovak: 
Je jasne, ze nazov Slovak je cudzi. V zenskom tvare sa dodnes pouziva
nazov Slovenka, teda povodne pomenovanie obyvatela uzemia dnesneho
Slovenska je Sloven, alebo Slovien. Aj v slovencine pouzivame nazov
nasho uzemia ako Slovensko, nie Slovacko - to je opat cudzi vneseny
pojem Slovenska v casti Velkej Moravy, ktoru obsadili ceske kmene. Je
pravda, ze uvedomovanie si Slovakov prichadza az v obdobi stredoveku,
ale to je uvedomovanie dnesneho moderneho slovenskeho naroda, ludu,
ktory uz zil v hraniciach vylucne Uhorskeho kralovstva. 
Zaver: 
Slovaci opat ukazuju, ze sa snazia byt papezskejsi ako samotny papez.
Nedajboze sa prihlasime k tomu, co nam patri, nielen nam, ale aj nam...
a uz si sypeme popol na hlavu. To je tak nepochopitelne v mysleni
inych, aj malych narodov, az z toho boli srdce. vsetci ti, ktori
nejakym sposobom, aj laickym sa snazia identifikovat so svojimi
predkami jasnejsie, su sarlatani a nevzdelanci. Radsej prenechame cele
nase dejiny okolitym narodom, ktore vlastne vznikli z toho, co aj nasi
predkovia budovali, len aby sme boli "vedecki". 
Pan D. Kovac je jeden z najlepsich historikov akych na Slovensku mame a
ja osobne mu vzdavam za jeho dielo najhlbsiu uctu. Ale v tomto pripade
je jeho "vedecka opatrnost" skor scestna a zavadzajuca, lebo tak ako
znamy cesky historik Trestik si dovoli vraviet o Cechoch v 9. storoci,
tak ako najlepsi madarski historici ani len nepochybuju o Madaroch v 9.
storoci (aj ked v tychto dvoch pripadoch paradoxne sa ceske aj madarske
etnika skladali v tom obdobi z viacerych kmenov a neboli konstituovani
do naroda), my nedokazeme na zaklade archeologickych lingvistickych
pisomnych pramenov ani sami sebe potvrdit, ze sme PRIAMI dedici Velkej
Moravy, Nitrianska a Moravska. V tomto pripade sice sa mozeme priet o
vhodnosti pomenovania Stari Slovaci, ktory osobne nemam rad, ale
podstata je niekde inde.
 
