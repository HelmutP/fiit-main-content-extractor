
 Keď som vtedy prišla domov, fúkal vietor, už mi raz takto bolo, na tom istom mieste, rovnaká atmosféra, noc alebo skoré ráno, niečo sa zmenilo, nepadlo to z neba, a do toho ten vietor a únava, otázka, čo bude ďalej. Déjà vu. A nič. Nikto. Nazvem to takým obdobím, nech má to prázdno aspoň nejakú formu.  

A o pár dní celkom iný pocit, akoby som si bola v čistiarni po vlastné myšlienky, teraz sú tiché a nenápadné. Jún, zima, dážď a niekoľko krokov ku slobode.  

Prší, aj sa blýska, na dobré časy a koniec sveta, kvitne vlčí mak. V detstve mi hovorili, že oslepnem, keď ten červený kvet chytím a potom sa dotknem očí. A ešte vraveli, aby som počas búrky nestála pri okne. Lebo. 
A ja som nechytala a nestála. Dnes by som mohla, zajtra tiež. Ale nič by sa tým nezmenilo.  

Tak to už chodí s očakávanými magickými okamihmi, prekračovaním hraníc, násilnými zmenami k lepšiemu, horšiemu či k sebe.  

Teraz chcem dojiť kravy a nájsť miesto, kde mi bude srdce pišťať, až sa ľudia začnú otáčať. Nájsť, objaviť, pochopiť. Nikto, nič, a takto je dobre, ostáva tu čas a priestor, otvárajú sa, postupne ma vťahujú dnu. Je tam svet, farby, vône a zvuky, dá sa z toho maľovať, tvoriť, možno nie realita, ale život áno, aspoň na jeden by to mohlo stačiť. Na takú radosť.   
