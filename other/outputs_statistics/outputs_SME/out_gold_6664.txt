

  
 Pôvodne internetové duo Majlo Stefanik &amp; Lucas Perny, ktoré nahrávalo avantgardné skladby cez internet sa zmenilo po pridání ďalšieho člena, Miloslava Kollára, na koncertný projekt troch sólových hudobníkov, hrajúcich kombináciu experimentálnej hudby a živého drum and bass. Reálnu podobu dostal iba hodinu pred prvým koncertom, kedy sa hudobníci stretli prvý krát v živote. Koncert bol vlastne ich prvým skutočným a aj hudobným stretnutím. Svoj repertoár rozšírili o improvizácie v štýle drum and bass až po pridaní hosťujúceho hráča na didgeridoo Braňa "Kofiho" Hargaša, s ktorým sa zoznámili opäť priamo na pódiu. Od marca SPK pravidelne hráva na rôznych akciách bez akýchkoľvek kapelových skúšok (kluby, festivaly, verejné aj undergroundové akcie) a stalo sa možno jediným projektom svojho druhu na Slovensku. Prvý album SPK z Klubu Umelcov, všetky novinky a informácie nájdete tiež na nasledujúcich stránkach : 
     bandzone.cz  l last.fm  l   facebook  l  railman  l  hudba.sk l 
  
 Oficiálny "obkec" k živému albumu : 
 Internetová, art-rocková kapela, ktorá pricestovala strojom času zo 70s nahrala živý drum and bass naživo pod Čachtickým hradom spolu s ich hosťom Braňom "Kofim" Hargašom. Majú za sebou jeden živý internetový album, nikdy neskúšajú a vidia sa iba na internete a na koncertoch. Projekt vlastne vznikol ponukou hrania v Bratislavskom klube umelcov, ktoré dostali od kapely RSPM. Kapela ho prijala, zoznamili sa dve hodiny pred koncertom a internetový projekt odohral dve hodinky improvizačnej a nenacvičenej hudby. Hrali sa aj cover verzie od The Doors. Druhý koncert sa uskutočnil na festivale pod Čachtickým hradom, kde sa vrhli do neprebádanej hladiny živého drum and bass... Ako to dopadlo, posúďte sami. 
 Živý album obsahuje aj nahrávky z koncertu v Novom Meste nad Váhom a raritnú nahrávku z Metro Clubu (Šaľa). 
 Po koncerte v Čachticiach začali hudobníci pravidelne koncertovať po Slovenských mestách bez akýchkoľvek skúšok. 
 free download   l   last.fm download and listening 
 Tracklist   01 Channeling Of Lady Elizabeth Bathory free download      10:11   02 Journey To The Void free download                                   20:52   03 Sacrilegium free download                                                 16:59  04 Drum duet (Perny vs. Kofi) free download                          8:35   05 Lady Elizabeth Bathory free download                               11:58   06 May Day free download                                                        8:00  07 Return Of Bloody Lady free download                                 5:56   08 Improvsiation from Metro Club (bonus)                              6:58 
 Miloslav Kollár - basgitara Martin Majlo Štefánik - klávesy Lucas Perny - bicie Braňo "Kofi" Hargaš - didgeridoo, perkusie 
 






 SPK ft. Kofi - Višnové-Čachtice PROMO KLIP - Máj 2010 
 






 SPK ft. Kofi - Metro Club, Šaľa - Máj 2010 
 






 Video z prvého hudobného aj  reálneho stretnutia v BA klube Umelcov - Marec 2010 
  
 O zháňaní nahrávok The Doors alebo ako vznikla internetová kapela Martin "Majlo" Štefánik a Lukáš "Lucas" Perny sú hudobníci, inšpirovaný art rockom a progresívnym rockom konca šesťdesiatych a začiatka sedemdesiatych rokov. Spojil ich jeden mail, v ktorom Majlo prosil Lucasa o požičanie koncertov The Doors. Po rozpisovaní mailov sa na pôvodnú myšlienku zabudlo a niekedy začiatkom roku 2009 poslal Majlo Lucasovi, nahrávku s kozmickým zvukom klávesov. Kedže už Lucas nahrával s internetovou kapelou Twilight Of The Idols, nahral bicie aj do Majlovho pokusu a vznikla ich prvá spoločná internetová skladba, ktorú Lucas nazval "Oddysey". Vyšla 25.06.2009 na stránkach mkmb.sk spolu s improvizáciou "Majlo Boogie". O mesiac neskôr poslal Majlo Lucasovi dva ďalšie pokusy, ktoré tvoria súčasť súčasného repertoáru SPK - "Time Machine" a "Mystical Experience". Lucas nahral bicie počas horúceho augusta a opäť zverejnil na stránkach mkmb.sk, kde sa dostali na prvé miesta hitparád. Neskôr nahrali ešte zopár pokusov a v novembri nahral Lucas bicie do skladby "Pochod tieňov", pre inú fiktívnu internetovú kapelu s názvom Rose. Následne nastalo ticho... 
 Skladby z tohto obdobia vyšli na prvej živej platni "An Internet Band Live At Club Of Artist" ako bonus. 
 E-mailová ponuka od kapely RSPM na spoločný koncert odštartovala koncertnú šnúru O rok neskôr, niekedy v marci 2010 prišiel Lucasovi aj Majlovi e-mail s ponukou vystúpenia v bratislavskom Klube Umelcov. Kedže spolu nikdy neskúšali a videli sa raz v živote (paradoxne na festivale Víkend legiend) zavolal si Lucas na pomoc gitaristu z iného projektu s názvom Krumplipapricash, Miloslava Kollára. Veškerá komunikácia prebiehala cez internet. Stretli sa hodinu pred koncertom, naskúšali Doorsovky a išli na pódium. Odohrali koncert, počas ktorého sála burácala a to bol vlastne začiatok tohto projektu... 
 Čachtický festival a rozdelenia smerovania na dva prúdy Priamo pod Čachtickým hradom sa zrodil súčasný zvuk projektu, kde otestovali ďalšieho avant-gardného umelca Kofiho Hargaša, s ktorým Majlo už v minulosti spolupracoval. Samozrejme, opäť bez akejkoľvek skúšky v štýle jamovania. Smerovanie projektu sa tak rozdelilo na čast - art rock/blues/60s psychedelic rock a čast fúzie drum and bass/world music/ambient/jungle/breakbeat/ethno/avant-garde. Žánrová rôznorodosť otvorila možnosti a projekt absolvoval na jar a začiatkom leta 2010 niekoľko úspešných koncertov po rôznych mestách Slovenska. 
 Kto je kto? 
 Martin "Majlo" Štefánik je hráč na klávesové nástroje z Nového Mesta nad Váhom. Spolupracuje so skupinami Impress, Dharma Inc a tiež občas hráva sólovo s Kofim Hargašom. Začínal so skupinou Hocikedy a spolupracoval tiež so skupinou Dilusion. Bol  členom skupiny Rose. Majlo používa klávesy Clavia a Yamaha mm6, ktoré skvele imitujú zvuky vysnívaných Hammondov a Voxov. 
 Lucas Perny je sólový hráč na bicie nástroje, ktorý je známy najmä svojimi avantgardnými bubeníckymi sólami a pôsobením v rozličných kapelách ako napr. Dilusion, Krumplipapricash alebo Moonlight Drivers. S internetovými kapelami sa prvýkrát stretol v medzinárodnom projekte Twilight Of The Idols, ktorej lídrom bol írsky klávesák Pad Brady. Po rozpade Dilusion vydal sólový mini-album "Psychedelic Winterland" a začal znovu spolupracovať s hudobníkmi na internete, kde narazil na Majla Štefánika. 
 Miloslav Kollár je multižánrový, multiinštrumentalista zo Šale, známy z projektov Audiokatarzia, Capre zo Šaly a Krumplipapricash. V projekte spieva, hrá na gitaru, basgitaru a občas husle. Je sólovým hudobníkom, ktorý hosťuje s neskutočným množstvom rôznych umelcov a podobným spôsobom sa dostal aj k projektu SPK. Je autorom niekoľkých vlastných skladieb v štýle ethno a acid-folk. 
 Branislav "Kofi" Hargaš je hráč na didgeridoo, ktorý pôsobil v kapelách  Výkvet in tento les, Od ucha k duchu, Holotropic dance a Slniečko. Absolvoval niekoľko koncertov s Majlom Štefánikom a Maokom. Robí takžiež didgeridoo workshopy a vyrába perkusie a didgeridoo. 

