

 
 FOTO - TASR   

 Viacerí Bratislavčania si už zvykli nechodiť po jablká do obchodu či na trh, ale radšej si ich rovno natrhať v niektorom zo sadov v blízkom okolí. Obľúbené miesto na ceste medzi devínskym jazerom a Stupavou, kde stálo kilo vlastnoručne naoberaných jabĺk 12 korún, už brány zavrelo, samozber sa skončil. 
 Do sadu vo Vajnoroch záujemcov o jablká nevpustia. Majú strach, čo by urobili so stromami. Jablká oberá špeciálna brigáda a predávajú ich tu od 10.00 do 12.00 h a od 12.30 h do 15.30 h po 10 až 22 korún za kilogram podľa druhu a triedy. 
 Z Dunajskej Lužnej smerom na Šamorín stojí zelená budova s názvom Dobré jablká, tam si možno nakúpiť jabĺčka cez týždeň od 7.00 h do 17.00 h a v sobotu od 7.00 h do 12.00 h. Priemerná cena je 22,50 za kilogram, podľa druhu a triedy. Zatiaľ sa predávajú len jesenné druhy, jablká na zimné uskladnenie ešte nie.	(dro) 

