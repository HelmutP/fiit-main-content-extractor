

 Oba pruhy diaľnice pred Bratislavou od Trnavy boli plné osobných aut. V pravom pruhu bola rýchlosť aut asi 110 km/hod, ľavý pruh bol o trošku rýchlejší, asi 120 km/hod. Rovný úsek diaľnice, dobrá viditeľnosť. Vpredu sa  preradilo auto z pravého do ľavého a spolu s trochu natlačenia aut to vyvolalo reťazovú reakciu pribrzdenia celého ľavého pruhu, asi na rýchlosť 100 km/hod. 
Nešlo o nič výnimočné je to bežná situácia pri jazde v kolóne. 
 
 
Lenže teraz boli autá v pravom pruhu diaľnice rýchlejšie. 
Vodič tretieho auta pred mnou keď mal ísť rýchlejšie ako auto v ľavom pruhu, 
sa zľakol a začal splašene bezdôvodne brzdiť. 
Našťastie som mal dostatočný odstup od predchádzajúceho vozidla a vodič za mnou tiež.
 
 


Preto prosím vodičov aby v takejto situácii sa nebáli ísť v pravom pruhu rýchlejšie ako v ľavom.


 
 
Pomôcka ako zistiť, že idete v kolóne a ide o súbežnú jazdu:
 
 
Treba sa pozerať pred seba a do spätného zrkadla, čo je v cestnej premávke 
úplne samozrejmá činnosť. 
 
 
Ak je pred vami auto v obvyklej alebo menšej vzdialenosti  akou sa má jazdiť na diaľnici, 
tak ste súčasťou kolóny, prípadne ste posledné auto kolóny. 
V tomto prípade sa jazda aut v dvoch takýchto pruhoch považuje za súbežnú 
a rýchlejšia jazda v pravom pruhu sa nepovažuje za predchádzanie.
 
 
Ak pred vami, aj za vami nie je v auto, tak ide o sólo jazdu. 
Ak je auto len za vami, tak ste prvé auto v kolóne. 
V takomto prípade by bola rýchlejšia jazda v pravom pruhu porušeným dopravných predpisov.
 
 
Čo je to obvyklá vzdialenosť v akej sa má na diaľnici jazdiť.  
Je to aspoň minimálna vzdialenosť na ktorú  vidíte a spoľahlivo dokážete vozidlo zastaviť. 
Keďže sa na diaľnici  jazdí až 130 km/hod, tak je to niekde medzi 100 až 140 metrov,
čo je odhadom 4 až 6 stĺpikov popri ceste.
 
 
Poznám vodičov, ktorí za súbežnú jazdu považujú len také natrepanie vozidiel tesne za 
sebou, že medzi nich a predchádzajúce vozidlo sa už nič nevojde. A práve takí 
vodiči sú najčastejšie pôvodcami tragických dopravných nehôd.
 
 
Prajem príjemnú a bezpečnú jazdu.
  
 
 
Citácia zo zákona, k článku Nebojte sa ísť po diaľnici rýchlejšie ...
 Pôvodne ako samostatný članok bez diskusie
  
Zákon č. 315/1996 Z.z. z 20. septembra 1996 o premávke na pozemných komunikáciách vrátane novelizácií, hovori o súbežnej jazde nasledovné:
  
§ 8 Jazda v jazdných pruhoch 
 
(1) Mimo obce na ceste s dvoma alebo s viacerými jazdnými pruhmi vyznačenými na vozovke v jednom smere jazdy sa
jazdí v pravom jazdnom pruhu. V ostatných jazdných pruhoch sa smie jazdiť, ak je to potrebné na obchádzanie,
predchádzanie, otáčanie alebo na odbočovanie.
 
 
(3) Ak je na ceste s dvoma alebo s viacerými jazdnými pruhmi v jednom smere jazdy taká hustá premávka, 
že sa utvoria súvislé prúdy vozidiel, v ktorých vodič motorového vozidla môže jazdiť len takou rýchlosťou, 
ktorá závisí od rýchlosti vozidiel idúcich pred ním, vozidlá môžu ísť súbežne. Pritom sa nepovažuje 
za predchádzanie, ak vozidlá idú v jednom z jazdných pruhov rýchlejšie ako vozidlá v inom jazdnom pruhu 
(ďalej len "súbežná jazda").

 
 
§ 15 Rýchlosť jazdy
 
(1) Vodič je povinný rýchlosť jazdy prispôsobiť najmä svojim schopnostiam, vlastnostiam vozidla a nákladu,
poveternostným podmienkam a iným okolnostiam, ktoré možno predvídať. Vodič smie jazdiť len takou rýchlosťou, 
aby bol schopný zastaviť vozidlo na vzdialenosť, na ktorú má rozhľad.

 
 
§ 16 Vzdialenosť medzi vozidlami 
 
(1) Vodič je povinný za vozidlom idúcim pred ním dodržiavať takú vzdialenosť, aby mohol včas znížiť rýchlosť jazdy,
prípadne zastaviť vozidlo, ak vodič vozidla jazdiaceho pred ním zníži rýchlosť jazdy alebo zastaví.
 


