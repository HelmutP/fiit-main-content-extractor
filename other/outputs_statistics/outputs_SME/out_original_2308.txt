
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Chcel som vidieť rozsudok, kde Fico vyhral od novinárov milión 

        
            
                                    18.2.2010
            o
            17:26
                        (upravené
                18.2.2010
                o
                22:47)
                        |
            Karma článku:
                15.61
            |
            Prečítané 
            20304-krát
                    
         
     
         
             

                 
                    Súd ho poskytol tak, že jeho dôvody nemáte šancu overiť. A ani výpočet výšky odškodnenia nie je dostatočne vysvetlený.
                 

                 
Len XXX novinári nech UUUU lebo OOOO v PPP do MMMM.flickr-brymo
   V novembri minulého roka bratislavský krajský súd potvrdil starší rozsudok nižšieho súdu, že vydavateľ týždenníka Plus 7 dní má za článok zaplatiť Robertovi Ficovi milión korún, resp. 33 tisíc eur. Popýtali sme si v Transparency na základe infozákona od súdu ten rozsudok, aby sme vedeli posúdiť, ako spravodlivo sudkyne Darina Kriváňová, Monika Holická a Jana Vlčková rozhodli.   Rozsudok bol do elektronického systému pridaný a sprístupnený až na našu žiadosť. Ako vidno, stále sa to nerobí hneď a automaticky, aby verejnosť ale aj ďalší sudcovia v podobných prípadoch mohli porovnávať a zjednocovať judikatúru (zverejňovanie rozsudkov je aj v programovom vyhlásení vlády, aj keď „postupné“). Vec druhá, 10-stranový rozsudok s odôvodnením sme dostali brutálne anonymizovaný. Vychutnajte si úvod:   Súd prvého stupňa napadnutým rozsudkom určil, že odporca uverejnením článku v spoločenskom T., ročník X.., č. XX. zo dňa X.X.XXXX. na strane XX. pod názvom "K.", v ktorom sa okrem iného uvádzalo: "... P.", neoprávnene zasiahol do práva navrhovateľa na ochranu osobnosti, určil, že informácia "V." a "C." je nepravdivá, uložil odporcovi v najbližšom čísle T. pripravovanom po nadobudnutí právoplatnosti rozhodnutia uverejniť ospravedlnenie za uverejnenie nepravdivých informácií vytvárajúcich obraz, že navrhovateľ pri P. a pri vyšetrovaní trestného činu S. požíval protekciu a uložil odporcovi zaplatiť navrhovateľovi nemajetkovú ujmu v peniazoch v sume 1.000.000, - Sk a zaplatiť náhradu trov konania v sume 136.242, - Sk.  ... Z kontextu článku týkajúceho sa témy K., použitím nepravdivých údajov, že "V." a že "O." v spojení s podtitulom "Š.", ako aj nesprávneho údaju, že vyšetrovací spis bol uzavretý, vytvoril autor článku pre čitateľa o navrhovateľovi obraz, že navrhovateľ pri objasňovaní alebo vyšetrovaní alebo pátraní P. a že v tomto prípade došlo aj k O. ... Vzhľadom na zistený skutkový stav súd prvého stupňa určil, že uverejnením predmetných údajov, odporca neoprávnene zasiahol do osobnostných práv navrhovateľa.   Ako už z toho môže verejnosť posúdiť, či sudkyne rozhodli dobre alebo zle? Sudcovia sa tak v podstate kontrolujú len navzájom (cez odvolací súd), a ak rozhodujú zle alebo zaujato či pod politickým tlakom, tak posledná nádej po rokoch ostáva len ísť mimo Slovensko k cudzím sudcom. Naozaj chceme, aby bola takto vykonávaná spravodlivosť u nás? Jasné, istá anonymizácia niektorých rozsudkov je v poriadku, ale keď ide o súd politika s médiom a týka sa výkonu práce štátnych orgánov, stále je v poriadku nedať verejnosti ani len šancu na kontrolu spravodlivosti? S pravidlami na zverejňovanie rozsudkov tu zjavne niečo nie je v poriadku.   Kto okradol Ficovú   Na základe informácií zverejnených už v médiách a prístupe k platenej databáze médií sa článok dá nájsť. Šlo o článok "Kto okradol Ficovú". zo 6.mája 2005 v týždenníku Plus 7 dní od Kataríny Šelestiakovej. Citujem hlavné časti:   ...Polícia sa pochlapila  Podľa našich informácií nedávno niekto ukradol automobil, ktorý je napísaný na advokátku Svetlanu Ficovú. Polícia ani chvíľu nezaháľala. "V momente stiahli na pátranie aj policajtov, ktorí pracovali na iných prípadoch," opísal nám situáciu náš zdroj.... sa vraj nestáva často, aby sa obeťou autičkárov stal prominentný človek. "Dá sa teda predpokladať, že išlo o provokáciu alebo vyhrážanie," vysvetlil nám. Túto informáciu nám však líder Smeru nepotvrdil, keďže svoj služobný telefón už niekoľko dní nedvíha.  Šťastie či protekcia?  Podľa zdroja z policajného prostredia je objasnenosť krádeží automobilov minimálna, veď čísla hovoria za seba -celoslovenský priemer je asi päť percent prípadov. To znamená, že zo sto áut sa nájde asi päť. To však nebol prípad prominentného politika, či skôr jeho manželky. "Polícia po dvoch dňoch od nahlásenia krádeže vypátrala motorové vozidlo Škoda Octavia. Krádež vyšetroval Úrad justičnej a kriminálnej polície Bratislava IV. Vyšetrovací spis bol uzavretý a auto vrátili majiteľke Svetlane F.," potvrdila informáciu pre Plus 7 dní hovorkyňa PZ SR Jana Pôbišová.   Formulácie v článku vyzerajú dosť mierne, ale vzhľadom na anonymizáciu odôvodnenia ich neviem jednoznačne vyhodnotiť. Samozrejme, podstata náznaku protekčnosti stojí na anonymnom zdroji v článku, čo môže byť potom problém dokázať na súde. Podľa formulácie rozsudku nebolo zjavne ani číslo objasnenosti uvedené správne, a zdá sa, že svedkovia z polície nepotvrdili neštandardné konanie. Na druhej strane článok podľa mňa stojí na slušnom základe, keď vychádza z toho, že nájsť kradnuté auto do dvoch dní nie je vôbec bežné. Ak ale anonymný zdroj v článku nehovoril pravdu, Ficovi to naozaj u čitateľa mohlo neoprávnene priťažiť.   Aký trest?   Okrem nájdenia dvoch nepravdivých tvrdení sudkyne konštatujú, že áno, tlač má právo klásť otázky, obzvlášť pri verejne činných osobách (Fico bol vtedy opozičný poslanec, líder najpopulárnejšej strany). Ale že rozhodujúcim faktorom pre výšku odškodnenia - ten milión korún -   je „posúdenie motívu, ktorý viedol autorku článku k jeho napísaniu.“ Na moje prekvapenie sudkyne prišli na toto:   Určujúcim pri posudzovaní výšky peňažnej satisfakcie je motív, ktorý v danom prípade vybočil z medzí legitimity, keď zverejnenie nepravdivých informácií nebolo dominantne motivované snahou o objektívne informovanie verejnosti o veciach verejného záujmu, ale vyznievalo skôr útočne.   Tak neviem, ale takýto ľahký test pre údajne zlý motív môže aspoň raz v kariére „miliónovo“ odpísať väčšinu novinárov nielen u nás, ale na celom svete.   Pri výške odškodnenia vychádzali sudkyne podľa svojich slov aj podľa dopadu na verejnosť (čítanosť týždenníka, veľkosť publicity, ale aj podľa dopadu na kariéru dotknutého politika, ktorý si vraj zakladá povesť človeka bojujúceho s korupciou). Dobre, ale odkiaľ prišlo to číslo milión (Fico žiadal dva)? Ako to vyčíslili? Chýba mi prepojenie s minulými rozsudkami pri podobných prípadoch, a ešte dôležitejšie, porovnanie s odškodnením v iných prípadoch, ako napr. nedbanlivé zabitie človeka pri autonehode. Nič také v odôvodnení nie je a tak ani verejnosť nemá možnosť posúdiť, či sudkyne postupujú v súlade s doterajšími rozsudkami, resp. ako dobre táto suma zapadá do systému odškodňovania rôznych škôd, a či teda napĺňa základný pocit spravodlivého rozsudku.    V každom prípade takéto čítanie mi len potvrdilo názor, že sudcovia v sporoch politikov s médiami nemajú vždy presvedčivé argumenty a že ich odôvodnenia nemusia byť v súlade s bežným chápaním spravodlivosti.   Médiá - trestať, ale aj odmeňovať   Odhliadnuc od kvality rozhodovania súdov ostáva všeobecnejšia otázka, či nemáme systém príliš postavený proti médiám. Jasné, za každú škodu na povesti nech pykajú (napríklad aj peniazmi). Problém ale je, že za každú prínosnú kauzu, ktorú médiá otvoria, okrem dobrej publicity s minimálnym dopadom na ich príjmy takmer nič nedostanú. Potom je jasné, že médiá budú opatrnejšie, ako by sme ich v ideálnej situácii chceli mať. Čo s tým? Možno ich od štátu nejakou formou finančne zvýhodniť (nižšia DPH, možnosť 2% aj pre médiá?, nárokovateľné dotácie), alebo kryť časť ich prehratých súdov. V každej alternatíve sú nejaké praktické problémy (bulvár vs seriózne médiá, možnosť štátnej manipulácie pri dotáciách, otváranie výnimiek v daniach), ale tie treba vážiť oproti očakávaným prínosom. Možno by pre začiatok stačila aspoň osveta, aby politici médiá nesúdili o nesymbolické čiastky, či aby súkromný sektor nedával peniaze len na deti, chorých a psíkov. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (147)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




