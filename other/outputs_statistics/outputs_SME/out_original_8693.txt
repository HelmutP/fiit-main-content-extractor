
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Mikloško
                                        &gt;
                Nezaradené
                     
                 Spomienka na pád mečiarizmu v minulom tisícročí 

        
            
                                    12.6.2010
            o
            18:57
                        |
            Karma článku:
                11.36
            |
            Prečítané 
            2324-krát
                    
         
     
         
             

                 
                    Rýchlosť prevratov v postsocialistických krajinách bola takáto: Poľsku to trvalo 10 rokov, Maďarsku 10 mesiacov, NDR 10 týždňov, Česko-Slovensku 10 dní, Rumunsku 10 hodín a Albánsku 10 minút. O 10 sekundový prevrat súťažila Kuba a Severná Kórea. Po volebných výsledkoch v roku 1998 konkurz vyhralo Slovensko. Za desať sekúnd po oznámení volebných výsledkov mi zapálilo, že Mečiar je v háji. Onedlho sa dozvieme, či padne ďalší rekord.
                 

                Dni, keď sa písali dejiny a aura vyžarovala z ľudí si dobre pamätám. Takými bola sovietska invázia 21.8.1968, udelenie Nobelovej ceny za literatúru Solženicynovi v r. 1970, pád berlínskeho múru, 17.november 1989, návšteva pápeža v Bratislave v apríli 1990. Aura žiarila v uliciach aj vo voľbách 25.- 26.9.1998. Výsledkom bol pád mečiarizmu. Ak by všetko zostalo po starom, Slovensko by nebolo ani dnes v Európskej únii. Na Žabotovu som prišiel podľa výzvy dominikánov veľmi hladný 26.9. pred 14 hod. Ochranky rezignovali, do zasadačky, kde sa sledovali výsledky volieb sa dostal každý, kto mal záujem. Prvé správy boli tesné, ale potom prepukal stále väčší jasot, najmä keď SDK viedlo o 8%. Politici sa cítili víťazmi, ďakovali voličom. V správe o 23 hod. SDK vyhrávalo iba o 1,7%. Objavila sa cigánska hudba, obložené misy sponzorov mizli s rýchlosťou, akou sa zmenšoval náskok SDK. Najväčší úspech mala Džamore, džamore a Na Kráľovej Holi. Všetci sme boli bratia: Kňažko, Dzurinda, J. Čarnogurský, Kukan, F. Mikloško a Harach tancovali a spievali ako zbojníci. Nikto netušil, že o polnoci mal Mečiar 40% a SDK 20%. Platilo to fučíkovské: „Je jeden svet a predsa sú dva svety.“ Podvečer zazvonil telefón, Havel volal Čarnogurského. Nebol tam, tak to dali Františkovi Mikloškovi. Vysvetlil mu situáciu, Havel bol dojatý, gratuloval SDK. Slúchadlo dostal aj bývalý prezident Kováč, ktorý povedal: „Myslím si, že puč je vylúčený, polícia sa na to neprepožičia.“ Siskári mohli byť z tejto tajnej správy zdesení. Žabotova bola preplnená ľuďmi, ktorí ľahko prešli cez kontrolórov. Na vrátnici neboli žiadni pištoľníci, ani gorily či roentgeny. Vonku bolo veľa ľudí, niekto priniesol víno, všetci boli namäkko, všetci boli kamaráti. Mečiarova STV sa neodvážila dať žiadne prognózy, iba opakovala tie s víťazným HZDS zo 14 hod. Keď ani na druhý deň ráno o 7 hod. nikto nič nehlásil, vedel som, že niečo nie je v poriadku. O deviatej v správach sa povedala pravda: vyhrala HZDS. Moja rodina sa rozrevala, ale ja som vedel svoje – je koniec mečiarizmu! Pyrhovo víťazstvo HZDS o trochu viac ako 20-tisíc hlasov im umožnilo rokovať o budúcej vláde iba so SNS a s NEI (Nová erotická iniciatíva). Mladí prekvapujúco volili SDK, o 10 hod. na tlačovke volebnej komisie bol Čarnogurský mimoriadne usmiaty. Voľby boli pokojné. Keď babka v Petržalke opúšťala volebnú miestnosť a nevedela, čo so zvyšnými lístkami, poradili jej vziať si ich domov. Zasmiala sa a ukázala na lístok č. 1 – HZDS a povedala: „Dobre, vysteliem si s nimi truhlu, iba Mečiar ma bude v nej tlačiť.“ Široká koalícia získala 93 poslancov, viac ako 3/5 väčšinu. Deratizácia STV, štátnej správy, diplomacie a ministerstiev bola nutná, seriózni ľudia a odborníci sa o svoj chlieb nemuseli báť. Štátna kasa zostala prázdna, boli sme zadĺžení,ale Mečiar bol preč! Objavil sa na piaty deň, tváril sa urazene. Donedávna sa ho všetci báli, teraz plakali iba babky-demokratky. „Neublížil sem žádnemu z vás“ bola jeho rozlúčka, aj keď pravdou bolo, že mnohým ublížil a mnohých ponížil. Zasa sa raz potvrdil výrok Abrahama Lincolna: „Za bláznov možno držať niekoľko ľudí celý čas, všetkých ľudí nejaký čas, ale nie všetkých ľudí celý čas.“

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (59)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Mladí, príďte do Prahy, na stretnutie Taizé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Ďalší škandál vo verejnom obstarávaní
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Spomienka na tých, čo odišli do večných lovísk
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            Interpelácia na premiéra Fica
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Mikloško 
                                        
                                            11. Kalvárske olympijské hry - fotoreportáž
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Mikloško
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Mikloško
            
         
        jozefmiklosko.blog.sme.sk (rss)
         
                        VIP
                             
     
         Mám dve na tretiu krát tri na druhú plus dva rokov (bŕŕŕ), 49-ty rok ženatý, štyri deti, jedenásť vnukov. Do r.1989 som bol vedec - matematik, potom politik, poslanec, prorektor, publicista, veľvyslanec v Taliansku (2000-2005), spisovateľ. Od r.2005 som dôchodca, vydavateľ najmä vlastných kníh (7), hudobno-dramatického CD-čka Ona, Ono a On, predseda Združenia kresťanských seniorov Slovenska. Pravidelne športujem, počúvam klasiku a rád sa smejem. Zabudol som, že od 11.3.2012 som sa stal poslancom NR SR, takže som zasa politik. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    216
                
                
                    Celková karma
                    
                                                7.77
                    
                
                
                    Priemerná čítanosť
                    2376
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ďalší škandál vo verejnom obstarávaní
                     
                                                         
                       Tento november si už Paška zapamätá
                     
                                                         
                       Spomienka na tých, čo odišli do večných lovísk
                     
                                                         
                       Interpelácia na premiéra Fica
                     
                                                         
                       Obchod s ľudskými orgánmi - zbraň najťažšieho kalibru v informačnej vojne o Ukrajinu
                     
                                                         
                       Písať o tom, čo je tabu
                     
                                                         
                       Rozvod a cirkev
                     
                                                         
                       11. Kalvárske olympijské hry - fotoreportáž
                     
                                                         
                       Holokaust kresťanov v Iraku a Sýrii sa nás netýka?
                     
                                                         
                       Som katolíčka. Nejdem príkladom.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




