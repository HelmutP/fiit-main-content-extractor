
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Kohutiarová
                                        &gt;
                Nezaradené
                     
                 Smútok za nechtami duše 

        
            
                                    23.2.2010
            o
            16:36
                        |
            Karma článku:
                10.78
            |
            Prečítané 
            2432-krát
                    
         
     
         
             

                 
                    Takto chodím od rána. Nie som schopná pozrieť sa na seba do zrkadla ani vtedy, keď si umývam zuby. Grimasy dnes nebudú.
                 

                Včera v noci nám bolo horúco inak, ako inokedy.  Ja viem, občas musia byť také okamihy pravdy: máš na sebe všetko a predsa sa cítiš byť nahý.   Horúčava pobúrenia a vzdoru. A hneď za tým zimomriavky z toho, kto a čo ti to hovorí.   A bolí.   Taký zvláštny smutno-vážno-nežný tón. Veci, ktoré si nehovoríme, keď si potme hľadáme najbližšiu cestičku k sebe.   Toto je iná káva.   Ani ja presne nechápem chémiu vzniku. Vždy to príde ako nečakaná letná búrka.   Odhrmí slovom, ktoré nešetrí a je priame. Spáli bleskom poznania. A poláme vrcholce, na ktorých som  chcela ešte včera vidieť rásť nové ovocie.   Je po nej také zvláštne ticho. Možno na prvý pohľad sa nič dramatické nestalo, všetci prežili, ráno sme vstali a dala som ti dobrovoľne bozk na rozlúčku.   A predsa celý deň neviem nájsť kožu svojho ja.   Nemám miesta a nemám pokoja.   Prečo práve ty? Prečo práve ja? Prečo práve včera?   Mala som povedať niečo inak? Mala som mlčať?   Mala som byť tvrdá a neplakať?   Kráľovstvo za patent záchrany.   Celý deň na teba myslím a predsa sa ti bojím zavolať.   Akú Máriu to vlastne ľúbiš?   Mám strach, že ťa stratím a mám strach povedať ti:"Buď pri mne, objím ma, zostaň...   Je vo mne tak zvláštne čisto a prázdno, ako po veľkom upratovaní. Rany sú vymyté vlastnou soľou. A možno to bolo aj dobre, čo bolo povedané tebe a mne.   Čakám ťa ubolená a mám smútok za nechtami duše. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Rada volím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Tato
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Môj chlap sa skloňuje podľa vzoru hrdina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Hmmm, nič nebanujem...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Kohutiarová 
                                        
                                            Neberte nám školu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Kohutiarová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Kohutiarová
            
         
        kohutiarova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Divožienka. Manželka svelého muža. Mama 7 krásnych originálov. Človek, ktorý neprestáva snívať, smiať sa, vidieť veci inak, ako sú na povrchu.Občas to bolí iných i mňa - nie je to predsa "normálne".
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    205
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2172
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pastoračný plán Katolíckej cirkvi na Slovensku 2007-2013
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                            všetko o grantoch
                                     
                                                                             
                                            John a Stasi Eldredge: Očarujúca
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            rádio Lumen
                                     
                                                                             
                                            ticho
                                     
                                                                             
                                            Spirituál Kvintet
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            David Kralik
                                     
                                                                             
                                            Ivanka Iviatko
                                     
                                                                             
                                            Juraj Drobný
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            o zahradach a stavbe domu /hadajte, kvoli comu/
                                     
                                                                             
                                            o deťoch a rodine
                                     
                                                                             
                                            MC Bambulkovo
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       List Karolovi: Ficovo posolstvo v Modrom z neba
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                                                         
                       Môj chlap sa skloňuje podľa vzoru hrdina
                     
                                                         
                       Čo už, 50tka
                     
                                                         
                       Pochopila som ...
                     
                                                         
                       Učitelia nevydržia štrajkovať dlhšie ako tri dni...
                     
                                                         
                       Moja spoveď alebo cesta hľadania partnerského šťastia
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




