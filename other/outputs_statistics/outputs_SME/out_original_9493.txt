
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Kravčík
                                        &gt;
                VODA zrkadlo kultúry
                     
                 Zvonia vodolajdáctvu do hrobu? 

        
            
                                    24.6.2010
            o
            8:35
                        (upravené
                10.7.2010
                o
                23:45)
                        |
            Karma článku:
                14.08
            |
            Prečítané 
            2761-krát
                    
         
     
         
             

                 
                    Slovenskí vodohospodári sa zvyknú chváliť unikátnym vodohospodárskym systémom 60-tich umelých vodných nádrží v Štiavnických vrchoch s celkovým objemom 7 miliónov m3, ktoré boli navzájom pospájané zbernými (72 km), náhonovými (57 km) a spojovacími jarkami. Verejnosti sa tento systém ponúka ako silný argument, prečo je dobré budovať priehrady. Pri tejto ponuke verejnosti však zabúdajú povedať celú pravdu, že tento unikátny systém vybudovaný z 1. polovici 18. storočia je založený na zbieraní dažďovej vody do nádrží s následným jej využitím pre banský priemysel. Pre súčasné problémy s povodňami na Slovensku môže tento systém byť inšpiráciou. Ponúkam zopár záberov z vodného systému vo sv. Antone a z Počúvadla.
                 

                    Orientačná mapa vodohospodárskeho systému vo sv. Antone      ...jarok privádzajúci vodu do Horného jazierka      ... Horné jazierko      ...potok z Horného do Dolného jazierka      ...vodopád do Dolného jazierka      ...Dolné jazierko 4x               ...fontána na nádvorí kaštieľa napájaná z Dolného jazierka         Jazero Počúvadlo takmer v sedle Štiavnických vrchoch      Stromy na vodnej strane spevňujú korunu hrádze a nie sú prekážkou ako na hrádzach postavených v 20. storočí. Odporúčam všimnúť si oblukovitosť hrádze (tlak vody na hrádzu sa prenáša do svahov, čo zaručuje bezpečnosť a stabilitu hrádze pred roztrhnutím).      ...bezpečnostný prepad z nádrže      ...unikátna čistota pozbieranej dažďovej vody v nádrži      ...vzdušná strana hrádze je zalesnená, čo súčasní vodohospodári považujú za nemožné. Údajne po odumretí stromov sú rizikom zvýšenia priesakov.      ... na tomto sa podieľal Samuel Mikovíni pred viac ako 250 rokmi. Kam sa hrabú údolné nádrže postavené posledných 50 rokov na Slovensku? Okrem nanosov bahna v nich množstvo špiny a odpadu v rozklade! Snáď vodolajdáctvu na Slovensku zvonia do hrobu.   V každom chotári by mohli byť takéto systémy novej vodnej kultúry vybudované pre zatraktivnenie prostredia, ochranu pred povodňami, suchom, podpory biodiverzity, rozvoja lokálnej ekonomiky i pre radosť zo života. Tisíce ľudí by mohlo si týmto nájsť prácu a robiť dobro pre seba i pre spoločnosť. Namiesto toho sa apatický a bezprízorne potulujú v poškodenej krajine a frustrovaní z nedostatku užitočnosti i nezáujmu spoločnosti o ich ubolené duše, prispievaju k sociálnemu napätiu v komunitách.   Pozývam občanov pestovať novú vodnú kultúru na Slovensku! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (29)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Poloz na hrad!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Zavšivavená spoločnosť trollami s podporou SME?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Bratislava šiestym najbohatším regiónom EÚ. Západné Slovensko 239-tým…
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Nemajú Boha pri sebe!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Kravčík 
                                        
                                            Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Kravčík
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Kravčík
            
         
        kravcik.blog.sme.sk (rss)
         
                                     
     
         Presadzujem a podporujem agendu „VODA PRE OZDRAVENIE KLÍMY“. Jej cieľom je posilnenie environmentálnej bezpečnosti prostredníctvom zodpovedného prístupu v ochrane prírodného a teda i kultúrneho dedičstva. Napĺňanie agendy, založenej na prijatí novej, vyššej kultúry vo vzťahu k vode, môže na Slovensku vytvoriť viac ako 100 tisíc a v Európe vyše 5 miliónov pracovných príležitostí. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    629
                
                
                    Celková karma
                    
                                                7.10
                    
                
                
                    Priemerná čítanosť
                    2236
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Povodne
                        
                     
                                     
                        
                            Hladujúci potrebujú vodu
                        
                     
                                     
                        
                            Klimatická zmena
                        
                     
                                     
                        
                            VODA zrkadlo kultúry
                        
                     
                                     
                        
                            http://s07.flagcounter.com/mor
                        
                     
                                     
                        
                            Nová vodná paradigma
                        
                     
                                     
                        
                            Košice
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            http://moje.hnonline.sk/blog/4
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Kandidáti za poslancov do EP
                                     
                                                                             
                                            Ladislav Vozárik
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            http://www.clim-past.net/2/187/2006/cp-2-187-2006.pdf
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.aktualne.centrum.cz/blogy/jana-hradilkova.php
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            hospodarskyklub.sk
                                     
                                                                             
                                            ashoka.org
                                     
                                                                             
                                            bluegold-worldwaterwars.com
                                     
                                                                             
                                            holisticmanagement.org
                                     
                                                                             
                                            theglobalcoolingproject.com
                                     
                                                                             
                                            ludiaavoda.sk
                                     
                                                                             
                                            watergy.de
                                     
                                                                             
                                            waterparadigm.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nemajú Boha pri sebe!
                     
                                                         
                       Ako sa zbaviť straníckej korupcie v štáte. Časť eurofondy
                     
                                                         
                       Ako sa z jednej ochranárskej ikony stala obyčajná nula
                     
                                                         
                       10 rokov po víchrici v Tatrách stále v zákopoch
                     
                                                         
                       Primátor všetkých Košičanov?
                     
                                                         
                       Dnes zasadá Vláda v Ubli
                     
                                                         
                       Aspoň pokus o integráciu Rómov? Za primátora Rašiho? Zabudnite!
                     
                                                         
                       Róbert Fico je bezpečnostným rizikom pre Slovensko
                     
                                                         
                       Kto je zodpovedný za pád starenky do kanalizačnej šachty v Michalovciach
                     
                                                         
                       Zdravé Košice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




