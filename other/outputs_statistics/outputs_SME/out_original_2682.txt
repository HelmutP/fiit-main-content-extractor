
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            sona hruzikova
                                        &gt;
                úlomky
                     
                 Náš marec 

        
            
                                    14.3.2010
            o
            19:14
                        |
            Karma článku:
                5.84
            |
            Prečítané 
            1590-krát
                    
         
     
         
             

                 
                    Sú medzi nami rôzne formy blízkosti a možno ani na ničom inom nezáleží.
                 

                Pripravujeme oslavy, slávnosti, mohlo by sa tiež povedať. Najskôr tajnú, balóny, tma, sviečky, živjó, dojatie, a o týždeň neskôr dlho očakávanú. Je to treba zorganizovať, výzdoba, jedlo, hostia, príď, budeme sa tešiť. A potom leštím príbor, pýtam sa na striebro, na zámky, kým obnovujem jeho zašlú krásu. Otázka, či toľko zemiakov bude dosť pre ľudí, ktorí sa tu usadia, budú gratulovať, tak ešte ďalších päťdesiat rokov, a obrátia sa aj ku mne, no ako v škole, frajer poslúcha? Zdvorilé úsmevy, jednoduché odpovede, smiech, doliať víno, teraz nie je čas na analýzy.   Oslavy si pamätám. Toľko hlasov, vôní, kytica ruží pre starého otca, keď som mala štyri roky, ľudia usadení v našej panelákovej obývačke, televízor a v ňom zábery z revolúcie, keď som mala dva. Vždy mi niečo z nich utkvie, na atmosféru a detaily som odmala bola citlivá. A vôbec nemusí ísť o udalosti. Jednoducho sa vrstvia.   Ako keď sme na základnej škole chodili do knižnice, ktorá sa potom presťahovala, a tak si spomínam len na schody a úzky priestor medzi regálmi detského oddelenia, desať korún zápisné, na hodine čítania sme potom každý hovorili o inej knihe a bol to zážitok. Rovnako ako Deň učiteľov a meniny, kvety k sviatku pani učiteľky, čokoládu, lebo mám dnes mena, tak sa to robilo, zvláštny zvyk, deťom cukríky, balíky bonparov, viac než tridsať detí v jednej triede, a ja som z toho všetkého bola v rozpakoch.   Sú to také súčasti, podať ruku, objať, prekričať každodennosť, na niečo to hádam len bude dobré, sadnúť si a dbať na dobré spôsoby. A pýtať sa, kto si dá kávu, cukor, mlieko, aby všetkým chutilo. Prípravami zaplníme dni, ktoré sme inokedy trávili v záhrade, medzi semiačkami zeleniny a kvetov, medzi hriadkami, rýľmi, motykami, hrabľami. Rodičia pracovali a ja som sa hrala, vymýšľala som si miniatúrne krajiny medzi hrudami čerstvo prebudenej zeme. Tento rok je však zima, Tribeč aj Inovec je zasnežený, v dolinách iba červené strechy domov, o zeleň sa ešte nemôžeme ani pokúšať. No je tu aspoň priestor, aby sme sa plne sústredili, spisovali zoznamy, nakupovali, organizovali. A keď u babky na okne zbadám kvetináče s práve vyklíčenými rastlinkami, napadne mi, že náš marec je vlastne vždy o hádzaní Moreny do potoka. Takto je to zariadené.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Ortuť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Mariánska
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Imágo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Posledné prázdniny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        sona hruzikova 
                                        
                                            Zmeny
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: sona hruzikova
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                sona hruzikova
            
         
        hruzikova.blog.sme.sk (rss)
         
                        VIP
                             
     
        "Musí sa, pomyslela si a rozvážne ponorila štetec do farby, udržať na úrovni prostého zážitku, jednoducho cítiť, že toto je stolička, toto stôl, ale zároveň cítiť aj to, že je to zázrak, je to extáza." Virginia Woolfová
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    137
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1683
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            dušou
                        
                     
                                     
                        
                            úlomky
                        
                     
                                     
                        
                            spolu
                        
                     
                                     
                        
                            hrach o stenu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Oštiepok
                                     
                                                                             
                                            Behind the fence
                                     
                                                                             
                                            Respice Finem
                                     
                                                                             
                                            re-wilding
                                     
                                                                             
                                            Sami people
                                     
                                                                             
                                            Kenozero Dreams
                                     
                                                                             
                                            Rudo
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Michal Ajvaz - Lucemburská záhrada
                                     
                                                                             
                                            Olga Tokarczuk - Dom vo dne, dom v noci
                                     
                                                                             
                                            Sándor Márai - Čutora. Pes s charakterem
                                     
                                                                             
                                            Graham Swift - Krajina vôd
                                     
                                                                             
                                            Ladislav Ballek - Pomocník
                                     
                                                                             
                                            Dragan Velikić - Ruské okno
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Outliers
                                     
                                                                             
                                            Ryat
                                     
                                                                             
                                            Nils Frahm
                                     
                                                                             
                                            Julia Holter
                                     
                                                                             
                                            iamamiwhoami
                                     
                                                                             
                                            Ólafur Arnalds
                                     
                                                                             
                                            Julianna Barwick
                                     
                                                                             
                                            Jono McCleery
                                     
                                                                             
                                            Fever Ray
                                     
                                                                             
                                            Sophie Hutchings
                                     
                                                                             
                                            Soap &amp; Skin
                                     
                                                                             
                                            Hidden Orchestra
                                     
                                                                             
                                            Kraków Loves Adana
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jonathan
                                     
                                                                             
                                            Saša
                                     
                                                                             
                                            Simona
                                     
                                                                             
                                            Baša
                                     
                                                                             
                                            Samo
                                     
                                                                             
                                            Tony
                                     
                                                                             
                                            Janka
                                     
                                                                             
                                            Andrej
                                     
                                                                             
                                            Tomáš
                                     
                                                                             
                                            Daniela
                                     
                                                                             
                                            Džejn
                                     
                                                                             
                                            Anna
                                     
                                                                             
                                            Alenka
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            iGNANT
                                     
                                                                             
                                            Lemon
                                     
                                                                             
                                            Humno
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




