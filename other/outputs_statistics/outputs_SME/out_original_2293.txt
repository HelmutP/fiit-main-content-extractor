
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kamenský
                                        &gt;
                Nezaradené
                     
                 Podobenstvo o ľudskej spravodlivosti 

        
            
                                    16.2.2010
            o
            15:40
                        (upravené
                1.9.2012
                o
                13:07)
                        |
            Karma článku:
                2.97
            |
            Prečítané 
            598-krát
                    
         
     
         
             

                 
                    Málokto pozná pravý zmysel dodržiavania zákonov a zmysel trestu. Zväčša je to tak že pravý vinník, koreň zla ktoré sa stalo je nevytrhnutý ale aby sa aspoň povrchnej spravodlivosti učinilo zadosť, symbolicky sa ukáže vina ne nejakého obetného baránka, ktorý je nám najbližšie. Ľudia si totižto veci veľmi zjednodušujú. A práve o tom ako bežný človek súdi a odsudzuje a dokonca to niekedy robí aj oficiálne súdnictvo, o tom geniálne hovorí stará Egyptská rozprávka - podobenstvo.
                 

                 Podobenstvo o ľudskej spravodlivosti   Zlodej sa vybral uprostred noci vylúpiť dom. Keď sa vyškrabal, na parapetu okna, tá pod ním povolila, zlodej spadol a zlomil si nohu.   Ráno  s nohou v dlahách udal majiteľa domu za ublíženie na zdraví, lebo tak neporiadne dal urobiť okno, že  mu spôsobil ujmu na zdraví. Sudca si teda predvolal majiteľa domu ako obžalovaného. Majiteľ domu sa ale necítil byť vinným, tvrdil že na vine je tesár ktorý vyrábal okno. Určite on ho urobil neporiadne, a tak si chudák zlodej zlomil pre jeho lajdáctvo nohu.   Poslali teda pre tesára. Tesár sa od ľaku triasol, že čo s ním bude, ale spomenul si ako to bolo keď robil okno na ktorom si zlodej zlomil nohu. " Ja som nevinný, pán sudca. Keď som robil to okno, išla okolo pani v červených šatách a tie boli tak krásne, že som musel za ňou hľadieť a tak som neporiadne urobil to okno."   Dali zohnať pani v krásnych červených šatách. Nebolo to ťažké, také šaty si všimne každý. Pani  pred súdom hneď vedela čo má povedať: " Ja nemôžem za to že sa za mnou pozeral tesár a tak zle urobil okno. Vinníkom je farbiar ktorý namiešal tak krásnu červenú farbu, že sa musel každý za mnou otáčať.   Takže farbiar ... Taký šikovný farbiar bol v meste len jeden, a toho predvolali na súd. Sudca ho obvinil z toho že je vinný za zlomenú nohu  zlodeja, lebo ten si zlomil nohu na ráme okna, ktorý vyrobil tesár hľadiaci na šaty ktoré on farbil a preto to okno urobil zle.   Rozsudok znel: "Smrť obesením vo verajách dverí súdu." Farbiar bol ale taký vysoký že ho vo dverách súdu obesiť nemohli. Vyhľadali teda nízkeho farbiara a toho obesili. Rozsudok bol tým naplnený a spravodlivosti učinené ... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (5)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Oneskorene k 17. Novembru ....
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Milujeme tých, ktorých ľutujeme?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Liberalizmus trhu, ale otroctvo výroby?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            Naozaj ide o komunizmus?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kamenský 
                                        
                                            O liberalizme a pôvode slova idiot
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kamenský
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kamenský
            
         
        jozefkamensky.blog.sme.sk (rss)
         
                                     
     
         Zaváži len to, čo neumiera a pre nás neumiera to, čo umiera s nami. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    285
                
                
                    Celková karma
                    
                                                3.85
                    
                
                
                    Priemerná čítanosť
                    1019
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            filozofické zamyslenia
                        
                     
                                     
                        
                            poézia
                        
                     
                                     
                        
                            duchovno
                        
                     
                                     
                        
                            umenie
                        
                     
                                     
                        
                            portréty blogerov
                        
                     
                                     
                        
                            Duch pôsobiaci v čase
                        
                     
                                     
                        
                            alchýmia varenia
                        
                     
                                     
                        
                            angelológia
                        
                     
                                     
                        
                            história
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Otvorený list prezidentovi SR A. Kiskovi.
                     
                                                         
                       Demografia a druhý pilier
                     
                                                         
                       Andy Warhol za 105 miliónov dolárov
                     
                                                         
                       Maňka-Kotleba...0:1
                     
                                                         
                       Mužskosť a ženskosť ako sociálne implantáty?
                     
                                                         
                       Volím život. Aj pre počaté deti
                     
                                                         
                       Lotyšské obrázky
                     
                                                         
                       Štyri generácie
                     
                                                         
                       Slovensko treba spájať
                     
                                                         
                       Uličská dolina
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




