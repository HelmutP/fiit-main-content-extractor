
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Banáš
                                        &gt;
                Hokej
                     
                 Šarky, hráč z dvojkrížom na srdci 

        
            
                                    16.5.2010
            o
            21:52
                        (upravené
                16.5.2010
                o
                21:34)
                        |
            Karma článku:
                8.70
            |
            Prečítané 
            3508-krát
                    
         
     
         
             

                 
                    Ak je pre Kanaďanov Ryan Smyth tzv. Captain Canada, čo potom znamená pre Slovákov Miroslav Šatan? Útočník, ktorý stál pri prvých krokoch slovenského hokeja na medzinárodnom poli, ten, ktorý v pomerne mladom veku opantal štatistiky nielen domácej súťaže, ale aj tie na medzinárodnej scéne.
                 

                     „Vďaka bohu za Satana.“               Akokoľvek vtipne vyznie táto veta vymyslená bostonskými fanúšikmi, pripomína, ako veľmi si slovenského krídelníka v tomto hokejovom meste obľúbili. Vo vyraďovacích bojoch ho totiž zastihla skutočne výborná forma, Šatan po ľade doslova lietal a konečne mu to tam začalo aj padať. Našiel svoju pravú, stratenú tvár.        35 ročný rodák z Topoľčian patril kedysi v Národnej hokejovej lige k najlepším hráčom. Obdiv si zaslúžil hlavne pre svoje myslenie a chuť nikdy sa ľahko nevzdávať.    Už v juniorskom veku patril k najlepším hráčom. Za rodné Topoľčany nestihol v ešte vtedajšej federálnej lige odohrať ani desať stretnutí, keď putoval do Trenčína. A práve v A – mužstve vojakov prvýkrát nazrel do hlavnej mužskej súťaže a vôbec sa v nej nestratil. Rok ešte pendloval medzi juniormi a mužstvom Dukly vo federálnej lige, na ďalší ročník – ten ktorý bol prvým ročníkom samostatnej Slovenskej hokejovej extraligy – však naplno prerazil medzi mužmi a patril k trojici najlepších hráčov toho ročníka. Celkovo zaznamenal 48 bodov za 32 gólov a 16 gólových nahrávok.            „Ak niekto trikrát po sebe príde na Majstrovstvá sveta, a trikrát sa mu podarí ako kapitánovi priviesť medailu, tak to vôbec nemôže byť náhoda.“           Miroslav Šatan však navždy bude pre slovenských hokejových fanúšikov v mysliach a srdciach zapísaný, ako vodca a najväčší líder slovenskej hokejovej reprezentácie. Už v Lilehammeri patril k najlepším, v mladom veku exceloval v domácej súťaži, odkiaľ okamžiťe putoval za oceán. Od začiatku 90 rokov sa prebíjal nižšími zámorskými súťažami, až kým sa mu konečne v roku 1995 podarilo presadiť v NHL, v drese Edmontonu.    Najlepšie a najproduktívnejšie roky však „Šarky“, ako ho prezývajú, prežil v drese Buffala Sabres. Túto formu si potom vždy preniesol aj na svetové šampionáty, kde s kapitánskym céčkom patril nielen k našim najväčším hviezdam, ale najlepším hráčom na celom svete. Stal sa akýmsi symbolom slovenského hokeja a hoci jeho hviezda už pomaly zhasína, znovu je tu dôvod nielen dúfať, ale aj veriť v jeho schopnosti.           Majstrovstvá sveta v Nemecku.       Ten šampionát, keď sa náš realizačný tím rozhodol dať šancu mladým, medzinárodne zatiaľ neostrieľaným hráčom. Je síce pravda, že sčasti môže za túto nomináciu to, že šampionát sa koná v olympijskom roku, kedy väčšina hviezdnych hráčov dala prednosť práve olympijskému turnaju a následnému oddychu. Nemôžme sa hnevať na tých hráčov z NHL, ktorí nám odmietli účasť, mali na to rozličné dôvody. Konečne tak dostali priestor mladí, čo je skutočne rozumný ťah vzhľadom k budúcnosti nášho reprezentačného hokeja. Napriek tomu sa ale našli aj takí, ktorí na Slovensko nikdy nezanevrú a uvedomujú si, že nemôže byť pre hráča väčšej česti, než nastúpiť na zápas za svoju rodnú krajinu. A my môžeme byť radi, že k tým hráčom patrí aj Miroslav Šatan, ktorý tesne po vypadnutí bostonských medveďov z bojov play-off v NHL potvrdil generálnemu manažérovi Petrovi Bondrovi svoju účasť na šampionáte. K mužstvu by sa mal pridať už dnes a nastúpiť na zápas proti Fínom v pondelok popoludní. Pre Šatana to bude už ôsmy svetový šampionát, pre slovenských fanúšikov jedna z najlepších správ aké sme mohli dostať. Tak verme, že Šarky bude bojovať a dovedie náš tím ďalej.            „Väčšmi než medaily zo šampionátov si cením cestu k nim, spôsob, akým sa tvoril úspešný tím v Petrohrade, Göteborgu či Helsinkách. Väčšina chalanov prišla preto, lebo si chceli zahrať za Slovensko. Niečo také sa nedá odmerať nijakým víťazstvom, pretože v športe platí, že nemusí vždy vyhrať lepší tím a už vôbec nie ten s väčším počtom hviezd na súpiske. Ani my nemusíme mať vždy svoj najlepší deň, ale keď sa nám podarí udržiavať v slovenskej kabíne ducha, ktorý sa zrodil niekde medzi Petrohradom a Helsinkami, budeme môcť ľudom doma aj naďalej prinášať radosť.“   Miroslav Šatan, Helsinki 10. mája 2003       Čaro hokeja a vlastne celého športu sa skrýva v maličkostiach. V iskričke nádeje, ktorá núti jednotlivcov aj celé kolektívy ísť až na dno svojich možností, za jediným cieľom – nielen byť najlepším, ale urobiť radosť druhým, ktorým na vás záleží. Ak aj na majstrovstvách neskončíme vždy najlepšie, vždy si treba ceniť silu toho či onoho kolektívu, vnútorné odhodlanie položiť pre svoju krajinu všetky sily. A dúfam, že s príchodom jedného z najväčších hokejistov v našej novodobej ére sa aspoň čiastočne priblížime k týmto snom.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Ako za starých časov 2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            Dva póly filmového roka 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Banáš 
                                        
                                            I believe 2
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Banáš
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Banáš
            
         
        milanbanas.blog.sme.sk (rss)
         
                                     
     
        Ak chceš vedieť aký som - spoznaj ma. Ak chceš vedieť ako píšem - čítaj ma :) 
Čo viac dodať? Som začínajúci autor z Liptovského Mikuláša.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    129
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1458
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Liptovský Mikuláš
                        
                     
                                     
                        
                            Úvaha
                        
                     
                                     
                        
                            The Game
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Vyjednávač
                        
                     
                                     
                        
                            Záhady
                        
                     
                                     
                        
                            Legendy
                        
                     
                                     
                        
                            Poviedka
                        
                     
                                     
                        
                            Spektrum
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Luceno J. - Háv klamu
                                     
                                                                             
                                            Barry D. - Veľké trampoty
                                     
                                                                             
                                            Keel J. - Mothman Prophecies
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Lady GaGa
                                     
                                                                             
                                            Depeche Mode
                                     
                                                                             
                                            Hudba z prelomu 80 a 90 rokov
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Jiří Sčobák
                                     
                                                                             
                                            Peter Bombara
                                     
                                                                             
                                            Pavol Baláž
                                     
                                                                             
                                            Juraj Lisický
                                     
                                                                             
                                            Dominika Handzušová
                                     
                                                                             
                                            Roman Slušný
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Supermusic
                                     
                                                                             
                                            Gorilla.cz
                                     
                                                                             
                                            Hokejportál
                                     
                                                                             
                                            Facebook
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




