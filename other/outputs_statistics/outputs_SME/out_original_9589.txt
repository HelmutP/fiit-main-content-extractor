
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Slavomír Repka
                                        &gt;
                osobný názor
                     
                 Pomalá samovražda v priamom prenose 

        
            
                                    25.6.2010
            o
            13:16
                        (upravené
                25.6.2010
                o
                16:56)
                        |
            Karma článku:
                14.97
            |
            Prečítané 
            5214-krát
                    
         
     
         
             

                 
                    Nastolíme novú politickú kultúru a etiku a budeme dôsledne presadzovať verejný záujem, verejný záujem, verejný záujem,....
                 

                 Tak to začalo:   "... . Opýtal som sa ho z čoho štvrtinu. On, že z 20 miliónov. Ja, že čoho?  On,  že eur. Opýtal som sa ho, že kto by to zaplatil. On, že to mi  povedať  nemôže, ale že to môže nazvať tak, že je to ponuka z  podnikateľských  kruhov... ."   Tak to pokračovalo:   "Chvíľu po deviatej nám niekto ticho zaklopal na dvere. Keďže je to dva  kroky od stola, za ktorým som sedel, počul som. Otvorím dvere a za nimi  veľká kopa nešťastia. Človek, ktorý bol včera sebeistý zo sebeistých,  stojí zrazu pred dverami so sklonenou hlavou a zaslzenými očami. Zavolal  som ho dnu, do chodby. Prvé čo povedal bolo - Nerob to, prosím, ja som  si z teba len vystrelil. Naozaj, vystrelil. Čo som mu na to povedal, si  dokáže domyslieť každý... ."   Aby toho nebolo dosť, tak to ešte pokračovalo:   "... Prečo som teda hneď trestné oznámenie nepodal? Lebo som stále veril, že  môžem veriť kamarátovi, že by také svinstvo nespravil. Je to normálny  chalan a napriek tomu, že mi to viackrát potvrdil, stále som dúfal, že  to bola naozaj len sranda. To je celé. Samozrejme, ak by následne ešte  dobiedzal a opäť mi peniaze núkal, tak by som už konať musel a aj by som  konal... ."   A na záver toto:   "... Ak toto moje správanie sa považuje niekto za hlúpe a nečestné, pripadá  mi to smiešne. Osobne si myslím, že absolútna väčšina kritikov mnou  zvoleného postupu, by bez váhania ponuku tajne prijala. Aj bez trestného  oznámenia, aj bez zverejnenia... ."   Celý článok TU   Môj záver (záver voliča, ktorý nevolil SaS, ale fandil SaS a aj Vám pán Matovič):   Ja Vaše konanie pán Matovič pokladám za nehodné poslanca NR SR.   A namiesto toho, aby ste sklonili hlavu a vzdali sa mandátu poslanca, tak si ešte dovoľujete upodozrievať väčšinu vašich kritikov, že by bez váhania takú ponuku prijali (Najlepšia obrana je útok).   Vo všeobecných politických tézach novovzikajúcej vlády je hneď prvý bod, citujem: "Nastolíme novú politickú kultúru a etiku a budeme dôsledne presadzovať verejný záujem."   Vyhlásenie pána Matoviča zapôsobilo na spoločnosť veľmi negatívne a nastolilo "otázku" korupčnosti do diania a marketingového zorného poľa občana. Som presvedčený, že ak je pán Matovič čestný muž (a za takého ho  zatiaľ pokladám), tak toto bol iba nemiestny žart, ale v jeho postavení, nového poslanca NR SR   NEPRIJATEĽNÝ.   Pani Radičová, pán Sulík, pán Figeľ, pán Bugár národ očakáva novú politickú kultúru.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (113)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Ficova kvázi demisia je bohapustá vyhrážka
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Kam smeruje slovensko s vládou SMER-u
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Vrana k vrane sadá, alebo voľby sa blížia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            Stanislav Šváby - môj starosta
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Slavomír Repka 
                                        
                                            S Radom prichádza do éteru nová telenovela
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Slavomír Repka
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Slavomír Repka
            
         
        repka.blog.sme.sk (rss)
         
                                     
     
         Nezaujíma ma, koľko stojí liter benzínu. 
Zaujíma ma, koľko zarábam ja. 
(kurňa, ten benzín je ale drahý) 

 V roku 2005 (február) som na blogu SME začínal článkom PAŠTEKÁRI 

 Viac o mne a mojej práci nájdete na týchto stránkach 

 Pozrite si aj ODPORÚČAME.EÚ prípadne poďte sem publikovať. 

 Obývam krásnu časť zeme, nazývanú Slovensko. Bývam v dosahu hlavného mesta, ale aj hory a vodu mám blízko.  
Mimo rečou som východniar ;-), paštekár.... 

 Résumé môjho "NAJ" na blogu SME nájtete TU 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    533
                
                
                    Celková karma
                    
                                                14.55
                    
                
                
                    Priemerná čítanosť
                    2579
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Z pohľadu obchodníka
                        
                     
                                     
                        
                            PRE Consulting
                        
                     
                                     
                        
                            CESTOVNÝ RUCH
                        
                     
                                     
                        
                            postrehy z ciest
                        
                     
                                     
                        
                            osobný názor
                        
                     
                                     
                        
                            pohľady
                        
                     
                                     
                        
                            Analýzy
                        
                     
                                     
                        
                            AUTO - test
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Súťaž
                                     
                                                                             
                                            ODPORÚČAME.EÚ
                                     
                                                                             
                                            Paštekári
                                     
                                                                             
                                            Jahodná
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Aplikácia
                                     
                                                                             
                                            Rezervy vo firmách
                                     
                                                                             
                                            Najlepší obchodník
                                     
                                                                             
                                            Mystery shopping
                                     
                                                                             
                                            Dobré čítanie
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Špina, bezpečnosť a tichá(?) koalícia SMER - SDKÚ v Starom meste
                     
                                                         
                       Osudové okamžiky
                     
                                                         
                       Zamyslenie nad jedným Rozsudkom v mene Slovenskej republiky
                     
                                                         
                       Stanislav Šváby - môj starosta
                     
                                                         
                       Slovakia Ring 24hod: Aké to je atakovať takmer 800 kilometrov na bicykli za jediný deň?
                     
                                                         
                       Pinocchio, čo sa už nezmestí do parlamentu
                     
                                                         
                       Bratislava chce opäť zlegalizovať 400 billbordov
                     
                                                         
                       S Radom prichádza do éteru nová telenovela
                     
                                                         
                       Keď plynár má lepšiu províziu za predaj elektriny
                     
                                                         
                       Zpráva ze života malých šestiletých hominidů
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




