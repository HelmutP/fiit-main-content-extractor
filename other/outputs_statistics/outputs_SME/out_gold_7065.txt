

 Tentokrát som trajekt zo španielskeho územia vymenil za let Alicante – Fés. Dnes sa dá z Bratislavy dostať do Maroka (Fésu) pomerne jednoducho. S jedným prestupom v španielskom Alicante. Cez nízkonákladovú spoločnosť Ryanair. Ak niekto túži ochutnať miestnu kuchyňu (Tajine, Couscous...), prejsť sa úzkymi uličkami starej mediny, nasávať vôňu korenín a sušeného ovocia, alebo stráviť niekoľko dní na púšti, určite by mal nad Marokom porozmýšľať. Je pravda, že na naše pomery je hygiena niekedy biedna, no nepoznám nikoho, kto by mal nejaký zdravotný problém s miestnym jedlom. My sme sa stravovali výlučne miestnou kuchyňou. Akurát vodu je potrebné kupovať balenú. Ak niekto ešte polemizuje nad bezpečnosťou tejto severoafrickej krajiny, dôkazom môžu byť aj dve Španielky, ktoré sme stretli cestou z letiska v meste Fés. Prišli do Maroka opäť. Priznám sa, že sa cítim v Maroku aj po zotmení bezpečnejšie, ako na barcelónskej pláži. A ak to porovnám so skúsenosťami jednej kamarátky z Juhoafrickej republiky, je to ako iný svet. Tolerantnejší. 
  
 Ak je žena prirodzene pekná, ani berberská pokrývka hlavy veľmi nepomôže :-) Foto: Miloudi Nouiga  
 Mesto Fés patrí určite medzi mestá, ktoré sa oplatí vidieť. Fés je označovaný ako kultúrna metropola krajiny a najväčšie stredoveké mesto na svete. Je baštou tradícií a remesiel, sústredených v medine, ktorá je chránenou pamiatkou UNESCO. Pre mňa má väčšie čaro ako známy Marrakesh. Cesta z letiska stojí 20, alebo 150 dirhamov (teda cca 2, alebo 15 eur). Záleží, či si vyberiete autobus, alebo taxík. Nás taxikár cestou späť zobral za 110 dirhamov. Aj keď sa pri našej navrhnutej cene z chuti zasmial, nakoniec nenamietal. Arabi. 
  
 Jedna zo vstupných brán do mesta. 
  
 Spôsob farbenia koží, ktorý sa ani po niekoľkých storočiach nezmenil. Zhýčkaným turistom ponúkajú domáci čerstvé mätové lístky, aby bol zápach znesiteľnejší. Zápach naozaj nie je veľmi príjemný, no pohľad na fajnovú nemeckú turistku ma oslovoval ešte menej. 
  
 Pohľad na mesto. So satelitnými prijímačmi v starej medine má vraj problém aj samotné Unesco. Ak ide o ubytovanie, netreba ho veľmi hľadať pred príchodom. Hotel si vás nájde sám. Doslovne. Ak si dobre spomínam, navrhovaná suma v našom prípade za noc bola 70 Dh, potom ju majiteľ stiahol na 50 Dh. Aj tak sme sa otočili, že ideme pozrieť niečo lepšie. Po chvíli nás dobehol a do ceny zahrnul aj raňajky. To už sme neodolali. Noc za 4,60 eura aj s raňajkami bola uspokojivá.  
   
 Opevnenie mesta Fés.  
   
 Nie len staré a úzke uličky ponúka prechádzka starou medinou. Myslím, že aj lajik v oblasti architektúry ocení niektoré významné objekty v meste. 
   
 Niekedy je fajn nechať sa pozvať do domov miestnych obchodíkov. Interier tohto bol podobný našej modranskej keramike, aj keď s orientálnymi vzormi. Vstup úzkymi dverami s rovnako úzkou chodbou stál však určite za to.  
  
 Ale aj samotné prechádzanie uličkami má svoje čaro. Veď posúďte sami: 
  
 Ovocie a zelenina, ktorá asi nedozrieva v kamiónoch a v skladoch. 
    
  
   
 Hlava ťavy, ktorá u predajcu mäsa visela minimálne 7 dní. Ak však chcete vidieť celú ťavu, treba zísť južne do Merzougy. 
   
 Mačky to v Maroku naozaj nemajú ľahké...  
  
 Predaj čiernych olív. 
   
  
 Sušené ďatle, marhule, figy, hrozienka, rôzne druhy orechov... Mal som problem odolať. Kúpil som si ďatle, ktoré boli síce chutné, no donútili zastaviť náš nočný autobus cestou na juh Maroka. Počas nasledujúcich ôsmych hodín som si musel obľúbiť turecké záchody. A vďaka za ne. Možno som len nemal šťastie, ale ďalšie som si už nekúpil. Podobne neodporúčam kupovať si vytlačenú pomarančovú sťavu, ktorú majú pouliční predajcovia pripravenú v plastových fľašiach alebo v termoských. Môže byť zmiešaná s miestnou vodou, ktorá Európanovi takmer určite spôsobí hnačku. Ak vám však pomaranče vytlačia priamo pred vašimi očami, nie je dôvod sa obávať. Určite odporúčam. Opatrnosť je na mieste aj pri použití ľadu či pri kúpe zmrzliny. Je to určite na zváženie. 
  
 Čoho som sa však nebál, bola kúpa miestneho korenia. S kúpou mi pomohol starší Francúz, keďže som s angličtinou ani so španielčinou neuspel. Už samotný nákup je niekedy zážitok. A pre mňa to bol ďalší obchod "neskazený" globalizáciou.  
  
 Ešte počas prvej 1. ČSR sa u nás predávali niektoré suroviny z plátenných vriec. Kvalitu negarantovala značka, ale meno miestneho obchodníka. V Maroku v takýchto vreciach môžete nájsť bylinky, sušenú pikantnú papriku, múku, ale aj mandle a cestoviny. 
  
 Takto si nechal kamarát namiešať korenie za 20 dirhamov (necelé 2 eurá). Ja som od príchodu z Maroka vyskúšal len korenie na rybu. No korenie kúpené v španielskom obchode už nevyužijem. S tým z Maroka sa porovnať nedá. 
 My sme absolvovali cestu na juh Maroka k piesočným dunám v Merzouge. Cestu sme absolvobali nočným autobusom. Odchádzali sme okolo 21. hodine z Fésu a do Rissani sme dorazili okolo šiestej. Odtial sme vzali taxík do spomínanej Merzougy. Cesta do Rissani stála 150 dirhamov (cca 14 eur), no dá sa lístok kúpiť aj za 101 dirhamov (cca 9 eur). Už v prvý deň sa na nás "nalepil" miestny znalec a vďaka Španielkam sme sa ho už nezbavili. Vraj má bratranca, ktorý nám umožní cestu na severnú Saharu. Jedlo, voda, taxík z Rissani do Merzougy, ťavy, noc na púšti nás vyšla na 350 Dh/32 eur (pôvodná cena znela 600 Dh). Pre porovnanie, pred troma rokmi sme si dohádali cenu za dve noci, ťavy a jedlo pre jedného na približne 60 eur. Cena by sa možno ešte dala stlačiť nižšie, no chcelo to asi ešte väčšiu dávku drzosti a neodbytnosti. 
  
 Cieľ bol jasný.  
   
  
 Tajine, ktorý nám pripravil večer náš sprievodca. Je to vlastne udusené kuracie mäso s cibuľou, paprikou, zemiakmi, paradajkami a ochutené korením. Po ňom nasledoval žltý melón. A samozrejme nemohol chýbať sladký mätový čaj. 
   
   
  
 Vraj berberská omeleta. Či sa tomu naozaj dá veriť neviem, ale bola to asi najlepšia omeleta, akú som kedy jedol. Chybou bolo, že bola pre štyroch :-) 
   
 Merzouga. Domy z hliny a z pomletej slamy.  
   
   
   
 Na ďalší deň sme pokračovali späť na sever. Pred odchodom autobusu z Rissani sme stihli byť spestrením pre miestne deti. Jeden z nich sa nám pokúšal predať náramok za 10 dirhamov, jeho mladšiemu kamarátovi by stačil aj jeden dirham. Ten som mu nedal. Zakázal nám to náš sprievodca z predchádzajúceho dňa, keď sme túto tému preberali večer v púšti. Vraj si za to kupujú cigarety. 
  
 Bežne tento nápoj nepijem, ale uznajte, kto by takému obalu odolal. 
   
 Najskôr chcel jeden dirham, nakoniec mu stačil aj citrónový cukrík. Nezvyknutý na jeho kyslosť ho v pravidelných intervaloch vyťahoval z úst. Nakoniec si však zvykol a vypýtal si ďalší.  
   
 Informácia o "západnom" turistovi, ktorý rozdáva deťom cukríky sa celkom rozšírila. Nesmelo, ale predsa si prišli vypýtať. Im asi mamy neprizvukujú tak ako kedysi mne, že od cudzích ľudí si nič brať nemajú. Cukríky z kovovej plechovky mali úspech. 
   
 Železničná stanica v Casablanke. Naša cesta z púšte pokračovala do mesta Meknés, kde sme už autobus vymenili za pohodlnejší vlak smerujúci do Casablanky.  
   
 Obed v Casablanke pod dohľadom všadeprítomných mačiek. 
   
 Najväčšia mešita v Maroku. Je postavená na pobreží Casablanky a pri jej otváraní boli prizvaní predstavitelia Moslimov, Kresťanov a Židov. Sympatický prejav tolerancie voči iným náboženstvám. Trochu mi to pripomenulo starú Bratislavu, kde tesne vedľa seba stála židovská synagóga aj Dóm sv. Martina. 
   
   
 Na miestnom trhu sa dalo kúpiť naozaj čokoľvek. Rozdvojky, sprchy, kľučky od dvier... ponuka naozaj široká. 
   
  
 Čerstvé kura, sliepka či kohút? Stačí ukázať prstom. 
   
   
 Predajca marockého chleba v Casablanke. 
   
 Z domácej pece priamo na ulicu. Predaj domácich koláčikov miestnej ženy. Jeden stál 1 dirham (9 centov). 
   
 Kukuričné a pšeničné placky.  
   
 Moslimky európskeho typu, aspoň taký som z nich nadobudol pocit. Až nečakane priateľské a ochotné nás doviesť na železničnú stanicu. S ďalšou sme sa rozprávali cestou do hlavného mesta Rabat. 
  
 Výsledkom rozhovorov boli napísané štyri miesta, ktoré by sme nemali v Rabate obísť. 
   
 Rabat 
   
   
   
 Opevnené časti hlavného mesta. 
   
 Čestná stráž pred posledným odpočinkom kráľa. 
   
   
   
  
  
 Trasu, ktorú sme absolvovali. Pre predstavu ešte uvediem ceny za vlak. Meknés - Casablanka 90 Dh, Casablanka - Rabat 35 Dh (cesta trvala asi 1h), Rabat - Fés 135 Dh (cesta trvala asi 3h). Cestovanie vlakom je pohodlné a rýchle. Klimatizované vozne sú samozrejmosť. 
 Vraví sa, že zážitok nemusí byť dobrý, hlavne aby bol silný. V prípade Maroka to rozhodne platí. S tým, že zážitok určite dobrý je. Ak bude príležitosť, prídem opäť. Maroko za to určite stojí. 
   
   
   
   
   

