

   
  Ani netuším, či som tu rada alebo nie. Určite by som mohla napríklad napísať zoznam pre a proti. Nedá sa vylúčiť, že sa o to niekto len tak, sám pre seba, aj pokúsil. No keďže som nič podobne konkrétne nepočula, nazdávam sa, že tento počin asi nedopadol najlepšie. Radšej sa o to nebudem pokúšať ani ja. Ktovie, čo by mi vyšlo. A koniec koncov, ani neviem, čo by sa v prípade ktoréhokoľvek výsledku patrilo urobiť. Stálo by za to ďalej žiť, alebo podstúpiť ešte väčší risk a umrieť? 
 Ak sa aj vzdám tohto nemalého problému, z čisto praktických dôvodov si vyberiem druhú možnosť, čím sa mám tých pár rokov zaoberať? Na matematiku som nikdy nebola, ak by som šla maturovať z chémie, tak by som na strednej bola doteraz. Podľa horoskopu z jednej útlej knižočky som rodený vodca, ale podlieham stresu, reagujem roztržito a neviem poriadne zorganizovať ani vlastný život, čoho dôkazom je táto úvaha. Síce si myslím, že je absurdné určovať život podľa dátumu narodenia, ale sú aj zábavnejšie prístupy. Berúc do úvahy moje krstné meno by som mala byť podobná s toľkým ľuďom, že ma celkom fascinuje, ako niekto ľahko uverí, čo všetko určuje jeho charakter. Dokonca tým argumentuje.  
 Niekedy si skôr myslím, že ani meno nemám. Teda mám, tak ako aj dátum narodenia, ale nikoho nezaujíma. Často ma mrzelo, že moje krásne meno si ľudia nepamätajú, ale už som sa s tým zmierila a netrápi ma to do takej miery. Ešte by ma niekto mohol obviniť, že sa zapodievam malichernosťami, bola by to hanba.  
 Tak teda čo ma môže trápiť? Čo s mojou budúcnosťou? Neviem, v čom vynikám a aké je moje miesto, ešte nie. Ale ak to sa na to pozriem z druhej strany, každý mesiac dostanem výplatu, mám čo do úst. Ani to by ma nemalo sužovať. Asi.  
 Ak by som zabudla na všetky tie generalizujúce charakteristiky vychádzajúce z pochybných údajov, mojou výraznou povahovou črtou by bola nerozhodnosť. Nie je to veľmi pozitívne, hlavne keď stojím pred mraziacim boxom niekoľko minút nevediac, ktorú zmrzlinu si kúpiť. Ale nepovedala by som ani, že je to výrazne negatívna črta. No to je tak asi všetko. Tým som typická a to by mi mohlo tiež pridávať vrásky na čele, no doteraz som nemusela hľadať riešenie v hraničnej a dôležitej situácii. Usudzujem z toho, že ani to by nemuselo byť predmetom mojich úvah. Asi.  
 Čo sa ma tak bytostne a celkom akútne dotýka, je narastajúci pocit hladu. To ma určite o malú chvíľu začne trápiť ešte viac. Lenže to aspoň dokazuje, že vo vedľajšej miestnosti je veľká biela chladnička pripravená ponúknuť mi celý jej obsah. Takže ani z toho nič nebude. Asi. Keď sa na to teraz tak pozerám, uvedomujem si, že by som mala byť celkom bezstarostná osoba. V opačnom prípade by som sa mala hanbiť. Asi. Jedno je ale isté, som hladná. Čo z toho vyplýva?  
 Je úplne jedno, či sa volám Anna alebo Krakonoš, dokonca som sa skoro volala Jakub. Moji rodičia mali slabosť pre rozprávku Maťko a Kubko, meno Matej obsadil môj starší brat. Podstatné na tom celom je, či už som rak alebo blíženec, že ním neprestanem byť nikdy. Nevybrala som si to, no na mne je aspoň, ako s tým naložím. Môžem sa zaoberať hlúpymi otázkami, ale naozaj má zmysel hľadať na ne odpoveď, keď sú tu omnoho väčšie problémy, čakajúce na moje riešenie? Nemám na to veľa času. Myslím si, že by som ho preto nemala len tak premrhať. So sebou budem celý život a neuniknem. „Jediný človek, s ktorým budeš celý život, si ty sám. Musíš sa usilovať, aby to bola znesiteľná spoločnosť.“[1]  
 Nemala by som žiť pre seba, ale pre život sám, možno to nie je danajský dar, ako som si spočiatku myslela. Tak či onak, idem si pripraviť obed. Asi.  
 
  
 
 
 [1] FILAN, B.: Weverka. Bratislava: Slovart, 2006, ISBN 80 – 8085 – 118 – 2, s. 131 
 
 
   

