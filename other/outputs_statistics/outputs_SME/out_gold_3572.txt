

 
 
  
Ako som sa dozvedel z dennej tlače, grécky dôchodca dostáva mesačne v priemere 1 600 €. To je viac ako šesťnásobok toho, čo dostáva moja mama, ktorá si celý svoj produktívny život oddrela na poli a v drevárskej fabrike. O tom, že by si mohla ísť oddýchnuť k Egejskému moru, môže iba snívať.
   
 Priemerný plat v Grécku je vraj okolo 1 700 €. Tiež by som to bral. Ja zhruba túto sumu zarobím na zahraničnej brigáde, keď makám od svitu do mrku, v piatok i sviatok. 
   
 Pre gréckych štátnych zamestnancov platí asi iný kalendár, lebo dostávajú až 14 platov ročne. A nie platov hocijakých, ale dva a pol krát vyšších ako je mzda v gréckom súkromnom sektore. A ak prídu o robotu, na účte sa im objaví slušná sumička: vraj až stonásobok platu! 
   
 Zaujímavé sú aj ďalšie údaje. Takmer tretina gréckeho hospodárstva je „čierna“, čiže z nej štát nemá ani cent. Nečudujem sa preto ani ďalšej cifre: Každý Grék vraj ročne vydá na úplatky v priemere 1 355 €. (Ozaj, vie niekto, koľko na takúto bohumilú činnosť vynaloží bežný Slovák? A koľko "menej bežný" Slovák?) 
   
 Vlastne by to mohlo stačiť. Ale predsa ešte niekoľko viet... 
   
 Naše ponovembrové vlády nás viedli k pravidelnému uťahovaniu opaskov. Vraj sme si za socializmu „prežrali“ budúcnosť. Nuž, nie som ekonóm, takže sa len laicky môžem opýtať, ako je to so súčasným slovenským dlhom? 
   
 A keď som sa už dotkol čias socializmu, aj vtedy sme sa „dobrovoľne“ skladali na to a na ono a ani boh nevie, kde tie zbierky nakoniec skončili. 
   
 Pomôcť Grécku? Áno. Ale nemalo by to byť formou daru, ale skutočnej priateľskej pôžičky. A kto požičiava, ten obyčajne požaduje aj nejakú záruku, zálohu. Tou by mohol byť - napríklad, len tak  i napadlo - nejaký ostrovček alebo letovisko, v ktorom by si mohli slovenskí penzisti užiť mora a slnka za cenu úmernú ich dôchodkom. Napadá mi historická paralela, keď si ktorýsi uhorský kráľ požičal od svojho poľského kolegu a ako zálohu mu na dlhé stáročia založil niekoľko spišských miest. Napokon, keď už máme kdesi v Pacifiku kúsok oceánskeho dna, prečo by sme nemohli mať na Balkáne svoje, slovenské letovisko? 
   
 Teda: Som za pomoc Grécku, ale treba ho podmieniť tým, že si aj Gréci urobia v opasku nové dierky, aby si ich mohli utiahnuť. Inak taká pomoc nemá žiaden zmysel. Napokon s pomocou „sociálne slabším“ máme na Slovensku bohaté skúsenosti. Otázne napokon je aj to, kto je pri porovnaní Slovensko : Grécko sociálne slabším. 

