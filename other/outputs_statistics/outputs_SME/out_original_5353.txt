
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            miroslav galik
                                        &gt;
                greens
                     
                 Čo nového na Slovensku? 

        
            
                                    27.4.2010
            o
            11:01
                        (upravené
                27.4.2010
                o
                12:20)
                        |
            Karma článku:
                2.86
            |
            Prečítané 
            314-krát
                    
         
     
         
             

                 
                    Prvá aj druhá svetová vojna boli mementom pre celý svet. Boli znečené mestá, dediny, ľudia ktorí neprišli o život boli šťastní, ak našli po vojne aspoň niektorých členov rodiny. Koncentráky, zničená krajina, poznačené zdravie , rozbité rodiny... Väčšina ľudí, ktorí  prežili genocídu si uvedomili, že ľudia by mali žiť v mieri , pomáhať si, budovať spravodlivú spoločnosť pre všetkých. Postupne sa však zabudlo a zase zomierali ľudia v Kórei, Vietname, Iraku, Juhoslávii... Studená vojna medzi "Východom" a "Západom" opäť pretrhala rodinné vzťahy a priateľstvá. ľudia cítili potrebu zjednotenia Európy .  Niekto tvrdí, že od 1989 sa vraj nič nezmenilo iní to nevnímajú až tak pesimisticky.   Vtedy sme síce stáli v rade na banány, ale každý mal prácu a mladí mali šancu získať byt za symbolickú cenu prípadne  zadarmo. Nemali sme mobily, internet, zahraničné autá, ale nemuseli sme sa obávať toho, že človek ochorie a nebude si môcť dovoliť zaplatiť zdravotné ošetrenie alebo pobyt v nemocnici.  JRD používali  umelé hnojivá, ale nikto nehladoval a ak  zahynulo zviera nebol problém zabezpečiť jeho likvidáciu v kafilérke.
                 

                 
  
   Dnes je život pre majiteľov nehnuteľností ľahký, hlavne ak sú v centrách miest, ktoré majú v prenájme reštaurácie, banky... Nežije sa dnes zle ani privatizérom podnikov, majiteľom firiem úzko napojeným  na politické špičky, ktorým sa darí úspešne vyhrávať tendre. Horšie je to s tými ostatnými, ktorí sú radi, ak majú prácu a môžu od rána do večera vytvárať hodnoty , z ktorých zisk plynie niekedy do vreciek našich podnikateľov i nadnárodných korporácií . Dialnice potom staváme zo súkromných peňazí , ale verejnosť ich bude splácať celé desaťročia. Sme Európania a tak sa nám už veľa vecí neoplatí pestovať a vzrábať a tak potraviny , obuv, textil, elektroniku ... dovážame z Číny a  prestávame čudovať, že dokonca aj Land Rover,Jaguár alebo Volvo už patia Číňanom. Sme radi, že môžeme pracovať v Slevenskej Kii, Volswagene , či Peugeote, Citroene. Staváme jadrové elektrárne a tešíme sa ako sa nám darí. Teší nás ak môžeme  čokoľvek  vyrábať  a čo na tom, že v okolí fabrík Ľudia  trpia ochoreniami dýchacích ciest a je tam zaznamenaný výskyt rakoviny... veď ľudia majú aspoň  prácu. Vyrábame množstvo predmetov, ktoré  po pár rokoch odhadzujeme a ak sa nám podarí ich recyklovať, tak sa chválime , ako podporujeme ekológiu a staviame spaľovne, recyklačné haly a skládky odpadu v blízkosti miest  .  Na uliciach hynú a rozkladajú sa psy , mačky , srny, líšky , holuby  ...  V Nitre končí kafilérka , v Žiline  funguje posledná na Slovensku. Firmy, ktoré by mali odstraňovať kadaver sú drahé a nie každé mesto má s nimi zmluvy.Tvárime sa, že nám to nevadí. Rušia nám ministerstvo životného prostredia, rušia ochranu chránených území a nastupujú developeri. Bude práca - bude sa nám dobre žiť. Naozaj?   Blížia sa voľby do NRSR a tak by si občania opäť mali zvážiť, či tí, ktorým dali dôveru v posledných voľbách naozaj splnili ich očakávania. Opäť je tu možnosť a čas na zmenu. Využijeme ju?       http://stranazelenych.sk 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        miroslav galik 
                                        
                                            Zvolen
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        miroslav galik 
                                        
                                            Uź opäť ideme voliť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        miroslav galik 
                                        
                                            Je po voľbách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        miroslav galik 
                                        
                                            Uz je to tu!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        miroslav galik 
                                        
                                            Konečne prichádza deň D
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: miroslav galik
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                miroslav galik
            
         
        miroslavgalik.blog.sme.sk (rss)
         
                                     
     
        Paeddr.Miroslav Gálik,kandidát č.100 do NRSR 12.6.2010 za SZ na kandidátke SDĽ(6), v 2009 kandidát do EP za SZ a do VÚC v BBSK za MOST-HÍD a SZ,
pedagóg M-Zt na ZŠ , korepetítor TO a učiteľ hry na keyboard na ZUŠ vo Zvolene,  poslanec MsZ vo Zvolene, predseda komisie ziv. prostredia a predseda vyboru mestskej casti Zvolen-Zapad, clen skolskej komisie a komisie ochrany verejneho zaujmu pri MsZ vo Zvolene,clen DR v MPBH a.s., folklorista/Ponitran, Zornicka, Marina.../, ekoaktivista, spolupracovnik s MVO: CEPTA, Zdruzenie Slatinka, Zdrave mesto, CKO, Klimaticka Aliancia...kandidát do VÚC v BBSK za liberálno eko alternatívnu koalíciu MOST-HÍD, Strana zelených
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    355
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            greens
                        
                     
                                     
                        
                            politika
                        
                     
                                     
                        
                            Černobyl
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




