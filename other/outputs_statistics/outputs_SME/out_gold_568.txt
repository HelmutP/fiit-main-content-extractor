

 
Pôrodná miestnosť. Sestrička mi v skratke hovorí základné údaje. Do toho sa
miešajú vzlyky rodiacej ženy, šepot personálu. 
 
 
Stojím pri otvorenom okne. Povieva jarný vetrík. Rozospato si niečo rozprávajú
vtáci. Za pár hodín sa začne brieždiť... Mozog už beží...
 
 
Pripravené vyhriate lôžko, suché plienky. 
 
 
Oproti dňu je atmosféra akosi viac slávnostná. Zahalená v rúšku noci. Len
skupinka vyvolených. Dieťatku sa nechce na tento svet. Neukecali sme ho. Asi mu
nejaký špión prezradil, aké to tu je... 
 
 
Vydrhnem si ruky, oblečiem zelený plášť, natiahnem sterilné rukavice. Vezmem
sterilnú plienku. Cítim sa ako kňaz a toto je obrad zrodenia. Gynekológ robí
virtuózne ťahy na bruchu tvaru balóna. Prechádza skalpelom hlbšie až do jadra.
Posledný ťah a už je hlavička vonku. Zrazu je tu celá. Spojenie také jedinečné
medzi ňou a matkou je ukončené jedným ostrým strihom nožníc. Vezmem ju do
plienky a pozdvihnem ku matkinej rozžiarenej tvári. Je to akoby som naznačoval
darovanie Bohom všetkých končín sveta. Máte dievčatko! Matka plače slzy
šťastia. Malý batôžtek sa pomrvuje. Ozve sa zvuk na ktorý čakám v tejto
pokročilej hodine. Ten, ktorý mi vraví, že moja hladina adrenalínu zostane v
norme. Prebehnem chodbou k lôžku pevne stískajúc malý, kričiaci, špinavý
balíček. 
Poriadne ho poutieram. Zotriem z neho vodu a krv. Fonendoskop mi šepoce do uší
tik tak, tik tak. Myslím, že tieto hodinky sú nastavené perfektne. Postupne si
celú malú princeznu pozriem. Od hlavičky po pätičky. Dievčatko otvorí očká
pozerá prvýkrát na svet okolo seba. Kričí nám niečo tou svojou rečou, ktorú
nikto z nás nevie rozlúštiť. Čo nám to chce povedať? Dajte mi pokoj! Alebo
krása!, tak takto to tu vyzerá? Tichom priestoru sa rozlieha jej pokrik života...
 

