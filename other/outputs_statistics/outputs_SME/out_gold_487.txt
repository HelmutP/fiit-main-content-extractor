

 
Majestátnosť nemých svedkov večnosti 
 
 
 
 
 
 Zelená na skalách, ktorá sa nedá len tak ľahko namiešať 
 
 
 
 
 
 Schovávačka pod prístreškom 
 
 
 
 
 
Mama skala a jej dieťatko 
 
 
 
 
 
A jej starší brat 
 
 
Celý okruh okolo týchto krásavcov trvá asi pol hodinku. Ak sa kocháte, viac. Ak ste skalolezci, vezmite si lano a karabínky. Trasy sú vytýčene pre rôzne náročnosti výstupu. 
 
 
A na záver. Nezabudnite vziať rodinku a foťák. Ja som ani len netušil ako je tu pekne. 
 
 
.. jak hladina jazera, v ružovučkom lekne.. 
 

