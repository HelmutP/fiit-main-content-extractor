

   
 - A čo by ste si teda zobrali?, - pýtam sa a prekvapenia neočakávam. Predpokladám, že nejaké hračky, bábiky, svoje obľúbené veci.  - Mário povedal, že on by si so sebou zobral svoju posteľ a bazén. A ešte nejakého kamoša.  - Že bazén! - uškrnie sa starší Šimon a nahodí svetaznalý tón. - To ja by som si určite zobral vodu, jedlo a vzduchovku. Betka sa nachvíľu zamyslí, zrejme nad užitočnosťou vzduchovky a potom odhodlane vyhlási:  - Ja by som si zobrala loď. Aby som mohla odplávať. Veď ten ostrov by bol opustený! 
 Pozriem sa na ňu a zrazu viem, že na čudné otázky dospelých vedia dať správne odpovede iba tí, ktorí ešte neprestali vidieť slona vo veľhadovi. Veď kto by chcel byť opustený? 
   
   

