

 
  Aký je vzťah medzi študentmi a profesormi na Slovensku? Ako sa učí na Slovensku? Sú profesori na univerzite prísni? Je zvyčajne medzi niektorými slovenskými študentmi odpisovať od seba pri skúške? S toľkými otázkami som tu začala nový študentský život.  V Pekingu som trávila 14 rokov v školách, stretávala som úplne odlišných učiteľov a učiteľky, podrobne poznám náš školský systém, v ktorom ešte existuje mnoho nedostatkov, čo treba zlepšovať. Po polročnom štúdiu na univerzite v Bratislave, už nie som taká zvedavá ako predtým. Týchto päť mesiacov nebolo zbytočných, veľa som sa dozvedela vlastnými očami, nechala som si veľmi zvláštne dojmy z mojich zážitkov na Univerzite Komenského, aj z internátu, kde bývajú slovenskí moderní mladí študenti.  
 
 
  Na fakulte s profesorkami
 
 
  V Číne sa tak opisuje tento nadšený pocit, keď máme jedného veľmi vzácneho učiteľa, že sa cítime, akoby sme chodili po jarnom vetre. Predsa je to nesmierne príjemný pocit s vďakou, keď niekto nám otvára bránu a vedie nás do nového sveta. Tu som stretla veľa dobrých profesoriek, najviac som sa potešila z toho, že mám také fantastické šťastie, že učiteľka môjho čínskeho učiteľa slovenčiny sa stala mojou učiteľkou! To je milá pani docentka Pekarovičová z filozofickej fakulty. Počas môjho študentského života viem dobre spočítať, koľko úplne najlepších učiteliek som mala, a pravda, že nie je ich veľa. A teraz na Slovensku som sa našťastie stretla s tou vzácnou pani profesorkou.    
 
 
   Predovšetkým táto slovenská profesorka krásne artikuluje každé slovo v slovenčine, to pre nás cudzincov je mimoriadne vzácne a užitočné. Okrem toho najviac som ju obdivovala, že skoro jej všetky slová na vyučovaní majú svoj význam a zároveň celá jej myšlienka, ktorú nás chce naučiť, je veľmi múdra, niekedy musím nad jej slovami rozmýšľať niekoľko sekúnd a potom jej odpoviem s úsmevom, že už je mi jasné. (V Číne na vysokých školách niektoré profesorky stredného veku už by nevedeli tak dobre prednášať. Veľa slov pri prednášaní učiva nebolo treba povedať, dokonca sme sa o asi pätinu vyučovania nezaujímali.)   
 
 
  Táto profesorka má veľmi zlaté srdiečko, tak sa mi zdá, že ona je úplne iná od druhých „veriacich“, ktorí každý deň len majú pána Boha na perách. Pani docentka Pekarovičová nikdy nám ani neprezradila, v čo verí, ale to sa dá cítiť, lebo vždy porozumela a tolerovala chyby druhých ľudí, osvojila si ich myslenie a opravovala ich. Keď sme sa rozprávali o svete, k ľudstvu a k iným národnostiam vždy vyjadruje neuveriteľné hlboké porozumenie a rešpekt. Chápem takto, že ona CHCÉ porozumieť iným, má takú silnú túžbu. Niekedy naozaj tak obdivujem Slovákov, bez ohľadu na to,že Slovensko je malá vnútrozemská krajina, ale niekto dokáže mať také priestranné a obrovské srdiečko, veľmi som z toho dojatá. Tým viac milujem túto krajinu, tak som rada, že viem trošku slovenčinu!  
 
 
  Ako učiteľka pani docentka je skutočne perfektná, na vyučovaní nikdy nám nečítala nič z kníh, vždy na nás pozerala z očí do očí, tým pozorovala, ako reagujeme na jej otázky, či sme rozumeli. V Číne aj na Slovensku som stretla aj takého učiteľa, celý čas skrýval svoju hlavu do knihy, len stále čítal a čítal, a študenti museli písať ako divný robot, zdalo sa mi, že celé vyučovanie bolo ako cvičenie diktátu.   
 
 
  Väčšina profesoriek v škole je milá a príjemná. Ale každá má svoj štýl prednášania. Keby som teraz bola trošku kritickejšia, povedala by som, že niektoré učiteľky vážne musia sa zdokonaľovať, aby si zaslúžili dôveru od svojich študentov. V Číne na konci semestra študenti dostávajú dotazník od študentského oddelenia, môžu hodnotiť učiteľov podľa seba na A,B,C či ZLÉ. Ak jeden učiteľ dostával viac ako polovicu ZLÉ, tak na budúci semester už nemôže prednášať taký istý predmet, čiže tento predmet bude odkladať až na ďalší semester, tento učiteľ musí rozmýsľať nad svojím učením a písomne musí odovzdať škole, v čom bude sa zlepšovať. Na Slovensku zatiaľ som ešte nevidela taký dotazník, možno všetci študenti sú spokojní?  
 
 
  V škole s kamarátmi
 
 
  Slovenskí študenti v škole sa správajú k sebe veľmi milo a srdečne. V tomto čínski študenti sú tiež zlatí voči študentom zo zahraničia. Počula som od nášho slovenského lektora v Pekingu, že na Slovensku je veľmi prísny školský systém. V skúškovej dobe som videla, ako študenti sa usilovne pripravovali na rôzne skúšky na chodbe v škole. No, ale raz som videla jedno dievča, čo sedelo pri mne na lavicke, práve prepisovala z učebnice niečo na jeden veľmi malinký papier, bože, také malilinké písmená, hneď môj čínsky kamarát sa usmieval, tak mi žmurkol okom a povedal po čínsky „ vidíš, aj na Slovensku je tak!“ No, asi to je silný prejav globalizácie však? V dnešnej dobe už nie je taký obrovský rozdiel medzi mladými ľudmi ako v predchádzajúcich generáciach...  
 

