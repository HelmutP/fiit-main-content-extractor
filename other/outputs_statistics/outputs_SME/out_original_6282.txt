
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Kamil Krnáč
                                        &gt;
                Spoločnosť
                     
                 Všetko zverejniť 

        
            
                                    11.5.2010
            o
            8:36
                        (upravené
                11.5.2010
                o
                3:05)
                        |
            Karma článku:
                11.01
            |
            Prečítané 
            1487-krát
                    
         
     
         
             

                 
                    Je vám príjemné vybavovať súkromnú korešpondenciu, ak viete, že vám niekto cudzí stále nazerá cez plece? Pravdepodobne nie. Pozrime sa teda konečne politikom, úradníkom a verejným činiteľom na prsty a pomôžme im nájsť riešenie ich večnej dilemy, či majú vo svojej funkcii hájiť záujem vlastný alebo pod drobnohľadom radšej záujem verejný.
                 

                 Minule som dostal otázku, či program boja proti korupcii strany SaS vôbec prináša nejaké významné zvýšenie transparentnosti správy verejných zdrojov, keď už dnes máme k dispozícii Zákon o slobodnom prístupe k informáciám (tzv. infozákon). Moja odpoveď znie, že tých pár nenápadne pôsobiacich viet v programe prináša v skutočnosti jednu z najvýznamnejších zmien v chápaní transparentnosti hospodárenia s verejnými prostriedkami na Slovensku.   Vznik infozákona bol síce bezpochyby správnym krokom smerom k prehľadnejšiemu fungovaniu verejného sektora, má však jeden veľký nedostatok. Musíte pri ňom vedieť položiť konkrétnu otázku. Ťažko sa potom môžete opýtať na informáciu o udalosti, o ktorej vôbec neviete, že nastala. Typickým príkladom sú rôzne utajené zmluvy a záhadné finančné plnenia, o ktorých sa verejnosť dozvedá zásadne iba náhodou. Okrem tejto "drobnosti" verejné inštitúcie pri hľadaní výhovoriek, prečo požadované informácie nemožno sprístupniť, uplatňujú takú vysokú mieru kreativity, že ich v nej častokrát musia brzdiť dokonca súdy. Ak sa potom ešte jedná aj o hospodárenie s verejnými prostriedkami v akciových spoločnostiach vlastnených štátom alebo samosprávou, infozákon definitívne melie z posledného.   Jedinou cestou ako sa vyhnúť zbytočnému a nekonečnému naťahovaniu sa o to, čo sa má, kedy sa to má a komu sa to má zverejniť, je zavedenie povinnosti automaticky zverejňovať na internete "všetko". Všetky inštitúcie hospodáriace s verejnými zdrojmi (t.j. aj spoločnosti ovládané subjektmi verejnej správy) by mali každý mesiac zverejňovať informácie o všetkých ich uzatvorených zmluvách, všetkých faktúrach, a tiež aj všetkých významných finančných transakciách. Bez vypisovania žiadostí, bez časových prieťahov a bez infantilných výhovoriek.   Aby sa nikto nevyhováral na organizačnú náročnosť tejto požiadavky, plne postačujúcim riešením by bolo uverejňovanie údajov hoci aj v nespracovanej - tzv. "surovej" forme. To je totiž vo väčšine programov otázkou pár jednoduchých úkonov. Dozaista sa potom nájde dostatok šikovných analytikov, ktorí tieto údaje s nadšením premenia napr. na prehľadnejšie tabuľky a grafy presne šité na mieru potrebám širokej verejnosti.   Predstavte si ministra, primátora, starostu, či prednostu úradu v situácii, v ktorej má možnosť za šťavnatú odmenu dohodiť spriatelenej osobe superkšeft s vedomím, že maximálne do mesiaca sa o všetkých podstatných detailoch dozvie celé Slovensko. Porozprávať potom mrzutým občanom rozprávku o tom, že na školy, kanalizáciu, cesty a nemocnice peňazí ako obvykle niet, môže byť pre neho vskutku zaujímavým adrenalínovým zážitkom. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (34)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Zákonom chránené gorily
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Zmeňte požiadavky novembrového protestu, prosím
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Najvyšší kontrolný úrad
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Mám pokračovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Kamil Krnáč 
                                        
                                            Vráťme moc ľuďom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Kamil Krnáč
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Kamil Krnáč
            
         
        kamilkrnac.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2743
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Spoločnosť
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




