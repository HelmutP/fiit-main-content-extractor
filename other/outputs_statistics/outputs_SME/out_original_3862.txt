
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Beáta Babičová
                                        &gt;
                Všimla som si
                     
                 Dedkovský syndróm 

        
            
                                    2.4.2010
            o
            18:30
                        (upravené
                2.4.2010
                o
                18:43)
                        |
            Karma článku:
                7.43
            |
            Prečítané 
            1372-krát
                    
         
     
         
             

                 
                    Dnes ma o pol siedmej ráno zobudilo trojročné dieťatko menom Natália. Povedalo dobré ráno, odhalilo snehobiele mliečne zúbky a odišlo. Usmiala som sa tiež a vrátila sa späť do ríše snov. Na hry je totiž ešte dosť času...
                 

                 Včera k nám zavítala návšteva z Bratislavy v zložení sestrin priateľ, jeho mamča a jej vnučka. Malá Natália nie je dcéra mojej sestry, ako by sa mohlo zdať, ale dcéra sestry priateľa mojej sestry. Viem, znie to komplikovane, takže ich nazvime proste bližšími rodinnými priateľmi, ktorí u nás prežijú Veľkonočné sviatky.   Čaro malej Natálie dostalo aj môjho ocina. On je zarytý vyznávač šialenia sa s deťmi a ovláda dokonca aj maznavú detskú reč. Viem, na 48ročného chlapa trošku infantilné, ale mne to príde veľmi milé. Keď mu dnes Natálka sedela na kolenách tak to bol obraz vystrihnutý presne z fotografie asi spred 15 rokov, kde som ja a on spolu s veľkým macom a sedíme na našom modrom gauči. Gauč sme už vymenili a ocinovi pribudlo zopár šedivých vlasov, ale jeho veselý pohľad ostal.   Všimla som si, že sa na to malé decko pozerá takým nežným pohľadom, ktorý sa uňho v poslednom čase akosi stratil. Problémy v práci, financie a rôzne iné veci ovplyvnili aj jeho psychiku. Dnes som zistila, že by ho iste potešilo vnúča. Má dve staršie dcéry a jednu vo fáze dozrievania (mňa) a nejaké tie vnúčence by sa hádam už aj hodili. Ako to však býva v dnešnej dobe, prednejšia je škola a práca a tak je to aj u mojich sestier. Človek, ktorý nemá vlastný byt a zabezpečenú budúcnosť si na dieťa len málokedy trúfne. Snáď sa ocino dočká niekedy v blízkej budúcnosti...   Dnes ma čaká ešte kopa srandy s tým malým pološialeným zázrakom, pretože kým zalomí do spania, tak to ešte potrvá. Mám pocit, že ma nabíja pozitívnou energiou, pretože nik nevie dať tak najavo lásku ako malé dieťa. Aj keď je to možno len chvíľkové, lebo sa tu zase tak skoro neobjavia ale je to príjemné. :)   Prajem príjemné prežitie Veľkonočných sviatkov.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (3)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Babičová 
                                        
                                            Youcoco odštartovali klubovú sezónu v Bombura klube
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Babičová 
                                        
                                            ART Meditácie 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Babičová 
                                        
                                            Veci, čo sa dejú
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Babičová 
                                        
                                            Neviem sa dočkať...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Beáta Babičová 
                                        
                                            Sama a spokojná...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Beáta Babičová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Beáta Babičová
            
         
        beatababicova.blog.sme.sk (rss)
         
                                     
     
        Som parazit, ktorý sa priživuje na príbehoch cudzích ľudí...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    33
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    799
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vlastná tvorba
                        
                     
                                     
                        
                            Všimla som si
                        
                     
                                     
                        
                            Zaujalo ma
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




