
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Mária Dudová  Bašistová
                                        &gt;
                Keď mi je ťažko..
                     
                 Máš viac ako štyri deti? Nečakaj našu podporu! 

        
            
                                    14.6.2010
            o
            17:05
                        (upravené
                14.6.2010
                o
                17:31)
                        |
            Karma článku:
                5.03
            |
            Prečítané 
            696-krát
                    
         
     
         
             

                 
                    Výsledky volieb sú už známe. Pravdupovediac, neviem, či teraz počujem viac o futbale alebo o voľbách. Nikdy som sa do politiky zvlášť neangažovala. A je paradoxom, že jedny predvolebné noviny sa mi dostali do rúk až dnes a na moje veľké prekvapenie, patrili práve strane, ktorá sa do parlamentu dostala. Dorazila ma však až veta, ktorá bola, na moje veľké počudovanie, v kategórii tých "dobrých návrhov"!
                 

                 
  
   Pre rodinu:   "Výrazne zvýšime podporu detí.."   Nooo, to znie dobre, to sa mi páči, moja prvá myšlienka. Keď som však čítala ďalej, úsmev mi zamrzol..   "..ale len prvých štyroch."   Ako prosím?? Prvých štyroch? A toto je zas ČO za diskrimináciu? Pardón, ale neviem to inak nazvať! Prečo podporovať len štyri deti? A čo ak majú rodičia detí viac? Čo s nimi? Už sme na tom tak, ako v krajinách východu, kde robia všetky opatrenia len preto, aby bolo detí čo najmenej kvôli preľudneniu?   Fakt ma tento fakt dostal. Neviem, či preto, že ja osobne som z kategórie 4+ alebo preto, že v budúcnosti by som možno aj ja sama raz chcela mať viac detí. A tento prístup sa mi vonkoncom nepáči. Zdá sa mi to nespravodlivé. Ak sa dvaja ľudia cítia na to, že chcú mať viac detí a sú schopní ich vychovať, prečo takáto "podpora" zo strany štátu?   Opäť sa tu len budú prehlbovať sociálne rozdiely. Tí, čo budú mať 1- 2 deti dostanú peňazí viac, tí so štyrmi a viacerými deťmi nedostanú ani toľko. Táto matematika ma skutočne zaráža. Prečo nejde každému rovnako? Nie je aj to piate dieťa právoplatným občanom štátu? Je vari niečo menej?   Som naozaj zvedavá, aký bude mať tento predvolebný sľub pokračovanie. Či dôjde k jeho zreálneniu a čo to bude mať za následok v budúcnosti. Môj terajší pocit je však- strach. Kam speje rodina v našej spoločnosti..     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (44)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Dudová  Bašistová 
                                        
                                            Nad Tatrou sa blýska..
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Dudová  Bašistová 
                                        
                                            Futbal porazil poslancov- čo koho porazí potom..?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Dudová  Bašistová 
                                        
                                            Človek zlatého srdca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Dudová  Bašistová 
                                        
                                            Ako sa bleskovo dostať do médií (a nielen na Slovensku)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Mária Dudová  Bašistová 
                                        
                                            Niet nad cestovanie po slovensky alebo o komforte a prívetivosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Mária Dudová  Bašistová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Mária Dudová  Bašistová
            
         
        dudovabasistova.blog.sme.sk (rss)
         
                                     
     
        Dievča, ako mnohé iné. Zvláštne obyčajné a neobyčajne zvláštne. Ale v prvom rade som dcéra Nášho Ocka..
Snažím sa žiť podľa viery, aj keď často padám.. Ľúbim Boha, rodinu, blízkych a priateľov. Mám rada úprimnosť, úsmev, otvorenosť a pohľad do očí druhého.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    39
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1290
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kvôli čomu sa oplatí žiť..
                        
                     
                                     
                        
                            Keď adrenalín stúpa..
                        
                     
                                     
                        
                            Čo ma teda pobavilo..
                        
                     
                                     
                        
                            Keď mi je ťažko..
                        
                     
                                     
                        
                            Len také úvahy...
                        
                     
                                     
                        
                            Život priniesol..
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            sr. Faustina Kowalska- Denníček
                                     
                                                                             
                                            skriptá do školy :)
                                     
                                                                             
                                            Joshua Harris_ Zbohom lásky
                                     
                                                                             
                                            Michael Quist- Rozprávaj mi o láske
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Vlastičkaaa :)
                                     
                                                                             
                                             
                                     
                                                                             
                                            Miška
                                     
                                                                             
                                            Peter
                                     
                                                                             
                                            Danka
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                                                                             
                                             
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




