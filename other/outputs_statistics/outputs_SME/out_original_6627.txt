
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Šebeň
                                        &gt;
                Nezaradené
                     
                 Sranda skončila, už mám 25 rokov 

        
            
                                    16.5.2010
            o
            19:51
                        |
            Karma článku:
                10.49
            |
            Prečítané 
            2863-krát
                    
         
     
         
             

                 
                    A som rád, že mám ešte dosť síl napísať to sám a nemusím to diktovať opatrovateľovi v domove dôchodcov.
                 

                 Konkrétne trinásteho mája som dovŕšil 25. Tu chcem ešte podotknúť, že ak sa aj Jaroslav Halák narodil na Kramároch, tak som presvedčený, že sa talentová víla pomýlila o jedno bábätko a JA som dnes mal byť slávny hokejový brankár, ale nevraživosť bokom.   Deň som prežíval ako každé iné narodeniny netušiac, čo je to za veľký míľnik. Šok prišiel podvečer, keď mi mama povedala, že už mi nebude nosiť lodičku na hranie do vane a ževraj keď náhodou uvidím v izbe pavúčika, už ju nemám volať, ale vyriešiť to nejak sám. Že som už veľký. Od tej doby si v každom momente uvedomujem, čo to znamená mať 25 rokov.  Je to vstup do sveta, v ktorom človek horšie vidí a slabšie počuje. Do sveta, v ktorom už nenadáva na babky, ktoré sa šialenými spôsobmi snažia predrať ku miestu v autubuse, skôr sa snaží odpozorovať techniku, lebo vie, že ho to už onedlho čaká. Vstup do sveta, v ktorom si začínate spomínať, ako krásne sme postúpili do A skupiny majstrovstiev sveta v deväťdesiatom piatom  a päťnásťročné chlapča na vás hľadí s nejasným výrazom v tvári. Taký istý špunt vám už začne hovoriť "Dobrý deň." A keď k tomu pripočítame, že každá oslava musí byť tématická a okrem haldy alkoholu na nej treba podávať aj chlebíčky a iné občerstvenie, máme tu starobu ako vyšitú.   A to už ani nehovorím, že tento vek so sebou prináša aj očakávania od okolia do budúcnosti! Nebodaj teraz budem musieť byť zodpovedný a budem sa musieť vedieť postarať sám o seba, no to je nemysliteľné. Takisto sa budem musieť strániť skvelého toaletného humoru, ako aj upozorňovať ostatných, aby ho nepoužívali. Tých obmedzení bude veľa a bude to veľký tlak. Jedinou výhodou hádam bude, že keďže som už od malička nemal rád zmeny, odteraz sa mi nikto nebude čudovať. Jednoducho konzervatívny dedko. Nikoho neprekvapí, keď budem bojovať proti rekonštrukcii detského ihriska, lebo tí fagani robia strašný hluk a postarám sa, aby sme v byte mali príjemných 27 stupňov.   A pretože sa čas do konca života stále kráti, rozhodol som sa vždy si robiť plán, čo ešte chcem dosiahnuť. Do tridsiatky som si dal záväzok mať toľko peňazí, aby som vedel zaplatiť veľkú oslavu s kopou alkoholu bez témy a bez jedla. Ďalej to bude stretnúť Andreu Verešovú a nejakým zákonom dovoleným spôsobom ju priviesť k tomu, aby sa do mňa buchla. Nuž a samozrejme, nemôžme zabudnúť začať šetriť na pohreb. Do tridsiatky by mohol byť už aspoň pomník v suchu. V šetrení mi nezabráni ani vetička "Každý je tak starý, ako sa cíti." To si vymysleli deti, aby mohli chľastať aj fajčiť a platí to len z mladosti do staroby, nie naopak.   Nuž a tu aby som to už hádam ukončil, lebo mám v kostiach čoraz menej vápnika, a nechcem si polámať prsty na klávesnici. Už len to som chcel, že samozrejme neprestalo platiť "Aj keď starý, ale stále s chlapčenským šarmom! " 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Šebeň 
                                        
                                            Nuansy žitia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Šebeň 
                                        
                                            Rakúsky dôchodca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Šebeň 
                                        
                                            Ranné vstávanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Šebeň 
                                        
                                            Detské strasti
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Šebeň 
                                        
                                            Mobilné telefóny a ich páni
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Šebeň
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Šebeň
            
         
        seben.blog.sme.sk (rss)
         
                                     
     
         Som trápny, aj keď vždy sa snažím byť vtipný, tak nasilu. Aha, a som zatrpknutý. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1934
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Ako sme (ne)vyrobili nový web UK
                     
                                                         
                       Myseľ mnemonika. Malé trhliny vo veľkej pamäti
                     
                                                         
                       Fed Cup - Slovensko:Nemecko - Momenty fedcupového týždňa
                     
                                                         
                       Grand Central Terminál: Najväčšia železničná stanica sveta.
                     
                                                         
                       To mi hlava neberie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




