

 Neklamem, ak budem tvrdiť, že písomná (externá) časť maturity, nie je celkovo zameraná na vedomosti. Skôr logika, práca s textom, úsudok, vyvodzovanie a dôvtip. Samozrejme základné znalosti zo slovenskej gramatiky. Boli sme upozorňovaní: "Dávajte pozor! Ak bude v otázke, že chcú počuť jedno slovo, napíšte len jedno, ak chcú doplniť slová - chcú ich viac." 
 Ukážka: 
 ...bla bla bla.... už sa tešil ... bla bla bla... (úryvok bol dlhý) 
 Otázka: 
 Vypíšte z textu slovo, ktoré charakterizuje pocit postavy... 
 Moja odpoveď: "tešil" 
 Správna odpoveď: "tešil sa" alebo "tešiť sa" - doplnok  NÚCEM: uznajte oba tvary, musí byť však "sa" 
   
 A teraz moja otázka k vám, vážení čitatelia a gramatici: "tešil sa" a "tešiť sa" je slovo, alebo slová? (túto otázku myslím úplne vážne, pretože ani do tejto chvíle som sa to nedozvedel). Samozrejme, všetci študenti, ktorí nad touto otázkou chvíľu uvažovali, doplnili len "tešil". 
 Na záver jedna pikoška. 
 Otázka: 
 
  
  
 
 Napíšte pomenovanie pre zamestnanca-muža, ktorý obsluhuje v lietadle. 
 
 
Aké padali odpovede? - letuš, letušiak, letuško, pilot...
 Tá správna bola: stevard, steward 
 K tejto otázke dokonca vznikla pred 5 hodinami skupina na Facebooku. Počet členov? 5500 a stále pribúda. :D 
 Zajtra, nás čaká testovanie z cudzích jazykov. Podobných prekvapení sa dúfam nedočkáme. 
   

