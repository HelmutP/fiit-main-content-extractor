

 
Symbolika má svoje korene už v dávnych dobách, vlastne už pri našom vzniku. V praveku sme sa dorozumievali okrem primitívnych hlasových škrekov i vďaka kresbám na steny jaskýň. Zakresľovali sme tam hlavne životné úkazy; lov na mamutov, oslava narodenia, sviatky jari, ohňa, dažďa... V starovekom Egypte sa na steny pyramíd vyzobrazovali mýtické príbehy a Božské úmysly ako aj osud faraóna, ktorý bol pochovaný v danej pyramíde. V Grécku a Ríme by sme našli nádherné maľby na stenách chrámov i v starých spisoch znázorňujúce často najmä biblické výjavy. 
 
 
Symbolika prerástla so kódovania. Neskôr sa dôležité správy určené len niekomu kódovali do obrázkových reťazcov. Hieroglyfy sú doteraz pre nás záhadou. Toto všetko je súčasťou komunikácie a našej formy symbolov. 
 
 
V mandalách sa ich objavuje hneď niekoľko, uvediem len tie najčastejšie: 
 
 
Vtáci - starobylý symbol ľudskej duše, vzdušného živlu a procesu premeny. Zastupujú duchovnú stránku života ako protiklad k tomu hmotnému. Môžu reprezentovať duchov, anjelov a nadprirodzenú moc. Tiež sú oslavou spojenia dvoch duší - dve hrdličky. Taktiež majú spojenie s duchovným svetom - vták Fénix. Záleží na druhu vtáka, ktorý sa objavuje v mandalke, najčastejšie je symbolom ochrancu, vysloboditeľa, a transformácie. 
 
 
Motýle - symbol premeny vďaka svojmu pozoruhodnému životnému cyklu. Tri stupne jeho života - všedná húsenica, latentná fáza kukly a nakoniec zrodenie nádherného motýľa - sú prirovnávané k životu, smrti a znovuzrodeniu. Motýľ je taktiež nestálosť, dynamika, jemnosť a krása. Symbolizuje vzdušný živel a harmonizuje osoby narodené vo vzdušnom znamení - Blíženci, Váhy, Vodnár. Pôsobí priaznivo na odpútanie sa a na zlomené srdce. Motýle tiež symbolizujú prvotné štádium lásky - záblesk - zamilovanosť. Zamilovaní milujú motýle a sami majú pocit, že nimi na okamih sú a či ich cítia v podbrušku. 
 
 
Kruh - symbol jednoty, dokonalosti, večnosti. Kruh nemá koniec , ani začiatok, je to nekonečné množstvo bodov , tak ako náš vesmír. Kruh v spojení s osmičkou je zákon karmy. Túto filozofiu vyznávajú najmä východné náboženstvá a alternatívne moderné smery. V klasickom kresťanstve neuvažujeme, že existuje viacero životov a teda karma. V budhizme sa verí, že človek má také množstvo životov, koľko potrebuje k tomu, aby sa stal Budhom. Preto sa stále rodí na túto zem a snaží sa o sebazdokonalenie. Kruh môže mať i tento význam. Taktiež je kruh symbol bohatstva a to i toho hmotného (tarotové karty - mince). Súčasne povzbudzuje prijatie života a integráciu mladých do spoločnosti. 
 
 
Kríž - prepojenie zvislej a vodorovnej čiary vyjadruje spojenie protikladov - ducha a hmoty, svetla a tmy, vedomia a nevedomia, života a smrti ... Kríž sa užíva najmä v kresťanstve, symbolizuje božskú spravodlivosť. Kríže sú rôzne. V Egypte je kríž, ktorý ma na vrchu slučku, je spájaný s plodnosťou a patrí k bohyni Iris. Križ je trest, súd a vyslobodenie. Skrze trápenie a bolesť prichádzam k osvieteniu - cesta trpiteľa alebo svätca. 
 
 
Kvapky, dážď - dážď ako taký symbolizuje plodnosť. Hovorí sa, že keď na svadbe prší, tak bude mať mladý pár veľa deti a je to pre nich požehnanie od Boha. Kvapky ako slzy znamenajú vnútornú očistu. Krv - kvapka znamená vnútornú očistu smerujúcu k vnútornej radosti. 
 
 
Oko - reprezentuje vševedúceho a všadeprítomného Boha. Je spojované so schopnosťou vedieť na fyzickej i duchovnej úrovni (jasnozrivosť). Samozrejme u oka je dôležitá farba - hnedá, modrá, zelená. Pokiaľ v sne figuruje farba, je dôležitá a má význam svoj - viď článok o farbách ( "Výklad symbolov mandál - farby"). 
 
 
Kvety - je to symbol jari a sú vyjadrením pominuteľnej povahy života, symbolom krásy a večnej obnovy života. Opäť rozhoduje farba kvetu - žlté: žiarlivosť, priateľstvo; červené: plodnosť, vášeň, láska a uzemnenie; modré: duchovná stránka bytia; fialové: pokora, pochopenie, spojenie s Bohom; čierne: premena, koniec jednej etapy a začiatok novej etapy; biele: nevinnosť, čistota, detinskosť, začiatok; zlaté: duchovná dospelosť, človek sa stal sám sebe učiteľom a majstrom; strieborné: citlivosť, empatia, nerozhodnosť, chladivá energia mesiaca ako zrkadlo Slnka; ružové: sexualita, túžba po milovaní, romantika 
 
 
Ruky - ruka udáva smer, pozorujme tedy, kam v mandale ukazuje (do stredu mandaly znamená obráť sa viac do svojho stredu, do srdca a načúvaj tichu vo vnútri a skús sa započúvať do vnútornej melódie alebo vnútorného hlasu; k okraju mandaly - otvor sa viac k svetu, vnímaj to, čo je okolo seba, vyjdi z tej ulity, k symbolu - zvýrazňuje jeho význam a pôsobenie). Inak ruka znamená i to, že sa slová premieňajú v činy. 
 
 
Srdce - je symbol lásky a to nielen medzi dvoma ľuďmi. Záleží na jeho farbe. Ak je zlaté - Božská láska, bezpodmienečná a nekonečná, modré - láska duchovných bytostí - ochrancov (anjelov strážnych), fialové - láska spojená s pokorou a najmä na báze duší, červené - partnerská láska, ružové - láska medzi čerstvo zamilovanými, oranžové - láska človeka k Bohu, žlté - láska pretkaná majetníctvom a žiarlivosťou. I počet sŕdc hovorí o význame - 6-rodina, 3-telesná, 1-láska ku mne samotnému, 2-láska k tomu druhému, 4-vyrovnaná harmonická láska, 5-premenlivá láska, 7-duchovná láska, 8-osudová láska, 9- Božská láska. 
 
 
Blesk - aktivácia nových alebo hlboko ukrytých energií. Znamenajú prudkú zmenu, posun udalostí. Koniec starých životných vzorcov a začiatok novej životnej etapy. Po búrke vykukne nádherná dúha. 
 
 
Dúha - prekonanie prekážok a nájdenie kľudu. Dúha je nádej a odmena a minulé ťažké obdobie plné skúšok. Dodáva motiváciu v ťažkých obdobiach. Doporučuje sa vložiť do mandaly človeku, ktorý prežíva ťažké nevľúdne obdobie.  
 
 
Špirála - je to symbol cesty. Tiež symbolizuje nekonečno a neustály duchovný rozvoj. I ľudstvo a jeho evolúcia má tvar špirály. Špirály, ktoré sa zatáčajú doprava (v smere hodinových ručičiek) majú "pozitívny" smer a značia rozvoj. Špirály stočené doľava majú význam opačný, rušia niečo, čo už bolo. 
 
 
Štvorec - značí pevnosť a stabilitu. Je to vyváženosť štyroch prvkov - voda, oheň, zem, vzduch; láska, zdravie, duch, hmota. Je to mentálny ventilátor, ukazuje na racionalitu, schopnosť rozumovo uvažovať. Doporučuje sa pre duševne pracujúcich ľudí. Súčasne dáva stabilitu a pocit pevnej pôdy pod nohami, je to symbol zeme. 
 
 
Hviezda - znamená zmysel pre sebaocenenie a vysoké sebavedomie. Väčšie množstvo hviezd môže ukazovať na vysoký počet malých dielčich cieľov alebo na veľa možností, ktoré sa tvorcovi naskytujú. Hviezdy nám súčasne pripomínajú, že každý sme jedinečnou individualitou. Sú to méty, inšpirácia, akési vodítko osudu. 
 
 
Strom - symbol života. Stromov je veľké množstvo druhov a dajú sa zobrazovať rôznymi spôsobmi. Strom je vlastne nami samými ako celok. Všimnime si preto, či kvitne, aké ma plody, či opadáva alebo je holý. Sú korene stromu hlboko pod zemou alebo len na povrchu? To všetko hovorí o našom fyzickom alebo psychickom ja. Strom súčasne harmonizuje nás samých a každý druh nám dáva potrebnú energiu. 
 
 
Trojuholník - symbolizuje silu a ukazuje smer. Trojuholník so špičkou dole ukazuje ženský princíp a so špičkou hore mužský princíp. V mandale môžu harmonizovať ženskú alebo mužskú stránku našej osobnosti. Ale tiež môžu upozorňovať , že sa nevenujeme toľko vnútornému svetu a žijeme príliš povrchne. Zároveň symbolizuje svätú trojicu a tak Božskú jednotu. V Egypte bol symbolom dokonalosti a pokroku, či posunu. Je to tiež symbol transcendentálnosti. 
 
 
Anjel - ochranný symbol. Dodáva nám energiu - liečivú, láskyplnú, harmonizujúcu záleží na druhu anjela. Je to ochrana a požehnanie. Zároveň strážca a Božský posol. 
 
 
To sú symboly, ktoré bežne vidíme v mandalách. Sú v podstate akýmisi mandalskými slovami a posolstvom. Farby sú emóciami a čísla sú akoby časom. Už len v tomto vidíme štyri rozmery - čísla - čas; farby - hĺbka, symboly - šírka, obrysy - dĺžka a vyžarovanie je ostatných n - rozmerov. 
 
 
Želám vám krásne vyrovnané dni... nech vás sprevádza na vašej životnej ceste mier a spokojnosť. 
 
 

 
 
 

 
 
 
 
 
 
 
 
 

