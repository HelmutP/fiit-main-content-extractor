
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Marušiak
                                        &gt;
                Nezaradené
                     
                 Florko sa raduje, Hurikán smúti 

        
            
                                    2.5.2010
            o
            20:48
                        |
            Karma článku:
                2.89
            |
            Prečítané 
            981-krát
                    
         
     
         
             

                 
                      Nedeľný zápas medzi domácim Hurikánom a Florkom mohol rozhodnúť o ďalšom semifinalistovi mužskej extraligy. V hale FTVŠ mali mečbal košickí florbalistii. Priebeh zápasu a aj celej série bol  nadmieru vyrovnaný. Hurikán však na Florko nestačil a po víťazstve 3:1 v sérii sa z postupu tešilo Florko.  
                 

                 Hurikáni sa snažili zabudnúť na sobotňajší neúspech a s chladnou hlavou vybehli na štvrtý štvrťfinálový zápas. Ten musel byť pre problémy s rozhodcami posunutý zhruba o pol hodinku. Hráčov oboch tímov to však z miery nevyviedlo a už od úvodného buly rozohrali okulahodiaci florbal.  V 8. minúte zahral pritvrdo Ľudovít Vítek a išiel si sadnúť na trestnú lavicu. Nepobudol tam ani 30 sekúnd pretože Majerník našiel voľného Franca, a ten otvoril skóre strelou k bližšej žrdi Adama Kalčoka. Otrasený Hurikán inkasoval aj o dve minúty neskôr, keď ďalšiu  Majerníkovú finálnu prihrávku  zúžitkoval tentokrát nikým nepokrytý Zemen. V prvej tretine Hurikán zo svojich pološancí gól nestrelil. Spoľahlivý Konečný vychytal Hietajärviho, Kabáta aj Hančíka.   Do druhej časti hry vstúpili omnoho lepšie hostia. Svoju tretiu asistenciu v zápase zaznamenal Majerník. V 22. minúte našiel Zemena, a ten bez problémov zvýšil na 3:0. V tej istej minúte zasadili Košičania ďalší tvrdý úder. Na 4:0 upravil Dugas. Kormidelník vysokoškolákov Marek Marušinec si následne vyžiadal oddychový čas a rázne dohovoril svojim zverencom. Od tohto momentu sa hra VŠK zlepšila. Dôkazom bol gól Hietajärviho z 30. minúty. Lenže už o 2 minúty neskôr poslal Florko do štvorgólového vedenia výborne hrajúci Majerník.  Hurikán však zbrane nezložil. Presilovú hru využil Purc. Späť do zápasu dostal domácich Marek Kabát, ktorý znížil na rozdiel dvoch gólov strelou popod košického gólmana.   Na začiatku tretej tretiny bol stav rovnaký ako predchádzajúci zápas. Na výsledkovej tabuli však skóre 5:3 nesvietilo dlho. v 44. minúte svoj druhý presný zásah v zápase zaznamenal Majerník, prvá hviezda zápasu.  Výborní fanúšikovia hnali svojich miláčikov dopredu aj za nepriaznivého stavu. Ich hlasy vypočul Hietajärvi po Kalčokovom vyhodení. 4:6. Iskierku nádeje pre Hurikán vykresal opäť Hietajärvi, ktorý dovŕšil hetrik minútu a pol pred koncom tretej tretiny.  Avšak vyrovnanie nepriniesla ani záverečná hra bez brankára a tak v košickom tábore vypukla nespútaná radosť. Pre bratislavský Hurikán sa sezóna skončila. V tej ďalšej sa budú chcieť vysokoškoláci umiestniť určite vyššie ako je ich tohtoročná piata priečka.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Marušiak 
                                        
                                            Florko s mečbalom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Marušiak 
                                        
                                            Rozhovor s kormidelníkom Hurikánu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Marušiak 
                                        
                                            Po dvoch zápasoch stav vyrovnaný
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Marušiak 
                                        
                                            Prečo hokejisti nechcú reprezentovať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Marušiak 
                                        
                                            Hurikán v playoff proti Florku
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Marušiak
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Marušiak
            
         
        marusiak.blog.sme.sk (rss)
         
                                     
     
        Som známy ako Shushkin, som maturantom na GLN i súčasne hráčom extraligového florbalového tímu Hurikán Bratislava.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    891
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




