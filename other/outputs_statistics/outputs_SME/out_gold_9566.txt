

 Dávnejšie som dostal otázku ak je chodník dostatočne široký pre chodcov aj 
parkujúce vozidla a je tam umiestnená dopravná značka zákaz státia, tak platí 
pre vozovku alebo pre chodník alebo pre oboje, respektíve aké dopravné značenie 
použiť, ak chceme dovoliť parkovať na ceste a zakázať na chodníku, alebo zakázať 
parkovať na ceste a dovoliť na chodníku alebo zakázať parkovať tam aj tam. Po 
analýze zákona 8/2009 Z.z. a vyhlášky 9/2009 Z.z. som prišiel k záveru, že 
samostatné dopravné značky zákaz státia a zákaz zastavenia sa vzťahujú len na 
vozovku a nie na chodník. 
 Lenže v zapätí som dostal proti argumentáciu, že keď je značka vpravo od 
vozovky tak platí na vozovke a keď je vpravo od chodníka tak platí pre chodník. 
Túto argumentáciu som bral ako vtip až do doby, keď som takéto dopravné značenie 
zbadal. 

   
  
Obr. 1. Dopravná značka zákaz státia vpravo od vozovky a aj vpravo od chodníka. 
 Rozmýšľal som, čo chcel týmto dopravným značením autor dosiahnuť. 
Pravdepodobne, aby vozidlá neparkovali na ceste a neparkovali ani na chodníku, 
ani pol na pol, lebo chodci potrebujú v tomto mieste celé 3 metre šírky 
chodníka. 

   
  
Obr. 2. Fotografia v zimnom období, ktorá ukazuje, akú šírku chodníka využívajú 
chodci. 
 Keďže správca komunikácie odhŕňal sneh len v pravde polovici chodníka tak 
nemožno hovoriť o tom, že chodci nutne potrebujú celu šírku chodníka, lebo inak 
by bola odhrnutá.   

   
  
Obr. 3. Nádobe od žuvačiek je umiestnená 150 cm do pravého okraja chodníka. 
 Skúsme sa zamyslieť ako by to bolo s parkovaním keby tak dopravné značky 
zákaz státia neboli pripadne by bola len jedna z nich. Parkovanie len na 
chodníku nepripadá v úvahu lebo chodník má šírku takmer 3 metre a na ňom
nezaparkujete vozidlo tak, aby zostalo pre chodcov aspoň 150 cm. Parkovanie len 
na ceste taktiež nepripadá v úvahu lebo tak môžete spraviť iba v prípade, že by 
ostal voľný 3 metre široký jazdný pruh pre každý smer. Lenže vozovka je užšia. 
Jediný spôsob parkovania by bol taký, že vozidlo by bolo častou na vozovke a častou na chodníku. Prakticky by vozidlo mohlo zasahovať do chodníka až po 
uvedenú nádobu zo žuvačiek. 

   
 Keď by tam bola umiestnená hociktorá značka, tak je parkovanie  na 
vozovka zakázané. Keďže vozilo sa len na chodník nezmení a nie je tam dovolené 
parkovanie, tak sa tam parkovať nesmie a je jedno či dopravná značka zákaz zastavenia je na pravej strane chodníka, alebo na ľavej strane chodníka alebo na oboch stranách chodníka. Čiže duplicita dopravného značenie nemá z hľadiska zákazu význam. Jediný význam duplicity dopravného značenie sa ukázal dnes. 

   
  
Obr. 4. Dopravná značka zákaz státia umiestnená na ľavej strane chodníka 
nevydržala zrážku z nejakým vozidlom. Našťastie druha dopravná značka bola 
ušetrené a tak sa z dopravného hľadiska nič nemení. 

   
  
Obr. 5. Život dopravnej značky visí na vlásku, teda leží na vlásku ... 

   
 Záver: 
Toto dopravne značenie je v poriadku i keď je zbytočne zdvojené. S nedostatočnou šírkou vozovky a chodníka, dostatočne prísne zakazuje parkovanie aj na vozovke aj na chodníku aj prípadné čiastočné parkovania na vozovke a chodníku. Dopravné značky zákaz státia a zákaz zastavenia platia pre vozovku nie chodník, nech sú umiestnene vpravo alebo vľavo alebo v strede chodníka. 
Ak správca nechce aby sa na chodníku parkovalo, ani takým režimom, že pre chodcov zostane 150 cm, tak má na to viacero možností, od osadenia stĺpikov, až po značku zóna so zákazom parkovania okrem vyznačených miest, tak ako je realizované v centre Bratislavy. V takejto zóne je možné parkovať len tam kde je miesto / oblasť vyhradená lokálnym dopravným značením. 
   

