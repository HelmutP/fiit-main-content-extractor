



 

 
 FOTO - ARCHÍV 
 




 Počas studenej vojny míľniky kozmonautiky, ktoré dosiahla americká či sovietska strana, mnohí nevnímali ako úspechy ľudstva. Vtedy išlo najmä o body v globálnej konfrontácii. Takýto prístup sčasti pretrváva, ale pamätníci spomínajú s nostalgiou aj na staré časy veľkých  objavov. Odvtedy, čo sovietska sonda Luna 3 získala prvé zábery odvrátenej strany Mesiaca, uplynulo 45 rokov. Bola to vzácna chvíľa, kedy sa ľudstvo pozrelo na dovtedy neprístupné miesto. 
 Mesiac sa nedá ignorovať. Osvetľuje nočnú temnotu, dvíha morské prílivy. Následkom jeho gravitácie nás pred väčšinou kozmického žiarenia  chráni zemské magnetické pole. Je to jediný veľký prirodzený satelit Zeme. Obehne okolo nej raz za približne 27,3 dňa. Rovnako rýchlo aj rotuje. Preto pri obehu smeruje k Zemi vždy skoro tá istá časť jeho povrchu. Nazýva sa privrátená. Mesiac však akoby "tancoval twist". Prečo, to nás tu nemusí zaujímať. Vďaka tomu sa občas v našom  zornom poli ocitne niekoľko ďalších percent jeho povrchu spoza bežných hraníc privrátenej strany. Zvyšok, odvrátenú stranu, zo Zeme nikdy nevidno. 
 Odvrátená strana Mesiaca ľudstvu symbolizovala vrcholnú "terra incognita". Až do 7. októbra 1959. Vtedy sovietsky kozmický automat Luna 3 toto neznáme miesto vyfotografoval. Zábery, ktoré napokon dorazili na Zem, však  nemali vysokú kvalitu. Boli skôr mizerné. Rozmaznaní superostrými obrázkami z modernejších sond by sme si ich sotva všimli. Po počítačovom zosilnení však predsa len poskytli historický pohľad "za roh" Mesiaca. 
 Luna 3 bola treťou pozemskou sondou úspešne vypustenou k Mesiacu. Odštartovala z kozmodrómu Bajkonur 4. októbra 1959, presne na 2. výročie  štartu prvého umelého satelitu Zeme, Sputnika. Vyniesla ju zdokonalená verzia rakety známej na Západe ako SS-6. Sonda bola dlhá asi 1,3 metra a necelý meter hrubá, zvonku vybavená slnečnými článkami pre dobíjanie chemických batérií a šiestimi anténami. Vo vnútri bol zobrazovací systém Jenisej-2: kamera, systém na spracovanie filmu, špeciálny  skener, vysielač a pomocné systémy. 
 Za 40 minút nad odvrátenou stranou Luna 3 získala 29 záberov zo vzdialenosti 63 500 až 66 700 km. Po vyvolaní, ustálení a vysušení ich skener premenil na elektrické signály, ktoré putovali na Zem. Ľudstvo napokon zazrelo aj skrytú tvár Mesiaca. 
 Zábery zachytili 70 percent odvrátenej strany. Sonda sa vrátila  úspešne k Zemi. Od 8. do 18. októbra vyslala 17 rozpoznateľných, hoci "zašumených" záberov. Kontakt s ňou stratilo riadiace pracovisko 22. októbra. Údajne zhorela v atmosfére v apríli 1960. Možno však až v roku 1962. Ďalšia sovietska sonda letela k Mesiacu až v roku 1963. 
 Luna 3 zistila, že odvrátená strana Mesiaca je iná ako privrátená. Je tam viac  svetlých vysočín. Tmavé, nízko položené dopadové panvy, takzvané moria (tmavé, lebo ich zaliala čadičová láva), sú len dve: Mare Moscoviensis (Moskovské more) a Mare Desiderius (More snov). Pri druhom sa neskôr ukázalo, že ho tvorí menšie Mare Ingenii (More dômyslu) a zopár tmavých kráterov. Luna 3 tak umožnila zostaviť  základnú mapu odvrátenej strany Mesiaca. 
 Bol to cenný krok prvej fázy výskumu Mesiaca. Vyvrcholila v rokoch 1969-1972 pristátím šiestich dvojčlenných posádok lunárnych modulov amerického projektu Apollo. Názorne ich približuje aktuálna fotovýstava na Bratislavskom hrade, ktorú otváral astronaut slovenského pôvodu Eugene Cernan. Práve on sa po mesačnom povrchu  prechádzal ako posledný. Mesiac je dodnes jediným mimozemským telesom, kde vstúpila noha človeka. 

