

  
  
   
  Einsteinove vedomosti z matematiky.(Zéro)  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 V jednom referáte na webe, ospevujúcom genialitu Alberta Einsteina, našiel som článok v ktorom jeho autor opisuje (svoj osobný dojem) na geniálny odkaz snáď najslávnejšej Einsteinovej rovnice a to rovnice: 
 E = mc2 
 Z predmetného článku uvádzam krátku časť: 
 „Z tejto teórie vyplývajú viaceré pozoruhodné javy - napríklad, že čas a priestor závisia od rýchlosti pozorovateľa. To znamená, že za istých okolností dochádza k spomaľovaniu času, skracovaniu dĺžok a narastaniu hmotnosti. Ak by ste leteli rýchlosťou svetla, čas by vám ubiehal pomalšie. No rýchlosťou svetla letieť nemôžete, keďže podľa Einsteinovej špeciálnej teórie relativity ju môžu dosiahnuť len objekty s nulovou pokojovou hmotnosťou, napríklad fotóny. V tých časoch to znelo nepredstaviteľne.  S teóriou relativity súvisí aj preslávená rovnica:  
  
 - tá uvádza, že energia E sa vždy rovná hmotnosti telesa m vynásobenej konštantou, druhou mocninou rýchlosti svetla c2. 
 Koniec citácie. 
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Z celého rozsiahleho článku  vyberám iba vetu o tom, že „rýchlosť svetla môžu dosiahnuť len objekty s nulovou pokojovou hmotnosťou, napríklad fotóny“, ako aj rovnicu 
  E = mc2 .  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein-------  
 Rozborom týchto dvoch viet dokážem, že na akej úrovni boli matematické vedomosti Alberta Einsteina.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Ak podľa Einsteinovej teórie, rýchlosť svetla môžu dosiahnuť len materiálne objekty ktoré majú nulovú (pokojovú) hmotnosť, potom energia fotónu, pohybujúceho sa rýchlosťou svetla, podľa Einsteinovej špeciálnej teórie relativity má (musí mať) hodnotu: 
 E(f) = 0(mf) x c2 ! 
 Pre prípad, že tuto rovnicu budeme riešiť oficiálnymi matematickými pravidlami, teda na základe tej matematickej vety, ktorá tvrdí, že každé číslo násobené nulou je nula, tak dostaneme nasledovný výsledok. 
 E(f) = 0(mf) x c2  
 E(f)  =  0 ! 
 Takže energia fotónu môže mať jedinú hodnotu a to hodnotu 0 !  
 Nie podľa mňa, ale podľa Einsteina a jeho slávnej rovnice.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Teraz venujme pozornosť samotnej matematickej podstate slávnej Einsteinovej rovnice: 
 E = mc2 
 Pre lepšie pochopenie matematickej úrovne tejto rovnice, uvediem ju v nasledovnom tvare: 
 m x c2 = E 
 Úvodom vysvetlím si, čo ktorí činiteľ tejto rovnice znamená. 
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Písmeno  m znamená hmotnosť telies vyjadrenú v kg.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Písmeno  c2 znamená rýchlosť svetla na druhú. 
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Písmeno  E  znamená skratku pre literárne slovo zvané Energia. 
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Keďže  ku hodnote m  - kg nie priradená mocnina, exponent, tak z praxe môžeme usúdiť, že písmeno m je algebrické číslo s exponentom 1. Preto platí m1. 
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Písmeno  c2 znamená rýchlosť svetla na druhú, to je z matematického hľadiska algebrické číslo s exponentom 2.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Aj keby sme hodnotu c2 považovali za konštantu C, aj tak by v zmysle súčasnej matematiky to C muselo prezentovať algebrické číslo s exponentom 1, C1 lebo C0 by v zmysle súčasnej matematiky znamenalo číslo 1.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Keďže písmeno  E  nie je vyzbrojené žiadnym exponentom, a nepredstavuje hodnotu čísla 1, teda nie je číslom E0, predpokladajme, pripusťme, že ide o algebrické číslo s neuvedeným exponentom číslo 1. Potom platí E1.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Einstein o tom, že jeho rovnica nevyhnutne potrebuje čísla s konkrétnymi exponentmi, nikdy neuvažoval. Na to Einsteinov rozum nestačil.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 V zmysle uvedených matematických definícii predmetných algebrických čísel, Einsteinova rovnica vyzerá nasledovne: 
 m1 x c2 = E1 
 eventuálne (m1 x C1 = E1) 
   
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Už i len takouto triviálnou matematickou úpravou slávnej Einsteinovej rovnice dochádzam k záveru, že v tomto prípade ide o matematický nezmysel a nie matematickú rovnicu.  
 Lebo súčin: 
 m1 x c2 nemôže dávať jednorozmernú veličinu E1,  
 ale prinajmenšom E3 !!!  
 Lebo platí: m1 x c2  =  E3   
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 V algebrickom vyjadrení:  
 a1 x  b2 = c3  
 eventuálne  a1.b1 = c2 
  --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Jediným normálnym matematickým riešením predmetnej rovnice je toto riešenie: 
 m1 x c2 = m1c2 
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Pre prípad literárneho (nie matematického) vyjadrenia predmetného súčinu, správny zápis slávnej Einsteinovej rovnice vyzerá nasledovne: 
  --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
  „Súčin čísel m1 a c2,  je Energia“    
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Problém slávnej Einsteinovej matematickej rovnice E = mc2 je v tom, že to nie je matematická (ani slávna) rovnica, ale iba literárne, slovné, substitučné vyjadrenie súčinu hmoty a rýchlosti svetla na druhú, kombinovanou metódou a to pomocou slov a čísel. Ide o matematicko - literárny boršč.  
 --------hlúpy Einstein------múdry JÁRAY--------hlúpy Einstein------- 
 Z tu opísaného jednoznačne usudzujem, že Einsteinove vedomosti z matematiky majú hodnotu blízku nule, podobne ako jeho úvahy o relativite absolútneho pohybu hmoty. 
   

