
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Ornst
                                        &gt;
                Postrehy
                     
                 Filmy blízkej budúcnosti 

        
            
                                    3.3.2009
            o
            12:51
                        |
            Karma článku:
                5.46
            |
            Prečítané 
            2462-krát
                    
         
     
         
             

                 
                    Služba Street View umožňuje 360-stupňový panoramatický pohľad na ulice miest. Užívateľ sa tak môže postaviť na ľubovoľné miesto v meste a obzerať sa okolo seba. Skúste si predstaviť obrovské kopulovité kino, kde by sa premietali 3D filmy natočené prostredníctvom špeciálnej kamery, ktorú táto služba využíva.
                 

                 Pred nedávnom sme sa rozhodli opäť využiť jednu z akciových ponúk našich nízkonákladových spoločností. Tentoraz sme si vybrali jednodňový výlet do severotalianskeho mestečka Lecco, ležiaceho na úpätí Álp. Podstatou však nie je náš cestovateľský zážitok, ale niečo úplne iné. Zakaždým keď sa niekam vyberieme, danú oblasť najprv preskúmame na satelitnej mape, kde hľadáme zaujímavé miesta. Veľmi milo ma prekvapilo zistenie, že v prípade mestečka Lecco je dostupná služba Street View, a práve tej by som chcel venovať zopár riadkov. Služba Street View je dostupná v rámci projektu Google Maps. Umožňuje 360-stupňový panoramatický pohľad na ulice miest. Užívateľ sa tak môže postaviť na ľubovoľné miesto v meste a obzerať sa okolo seba. Prvé nafotené americké mestá boli sprístupnené 25. mája 2007. V USA už nechýba veľa k takmer dokonalému pokrytiu. V Európe sa služba rozmáha podstatne pomalšie, a to aj v dôsledku nevôle obyvateľov a vlád. Ľudia sa boja o súkromie a o svoje tváre, ktoré preto musia byť už teraz cenzurované. Najväčšie pokrytie má Francúzsko a nafotených je tiež niekoľko talianskych a španielskych miest. Tomuto projektu držím palce, pretože z môjho pohľadu je to niečo úžasné. Dúfam, že sa bude rozmáhať v ďalších a ďalších krajinách a umožní nám tak nahliadnuť do miest, kde sa možno nikdy nedostaneme. Keďže ma služba Street View neuveriteľne fascinuje, zisťoval som niečo o tom, ako funguje. Pravdepodobne ste ešte neprišli na súvis medzi týmto textom a nadpisom článku, ale zotrvajte prosím, postupne k tomu smerujem. Street View sa vytvára prostredníctvom špeciálnej guľovej kamery umiestnenej na streche auta. Kamera od spoločnosti Immersive Media má jedenásť šošoviek a štyri mikrofóny nasmerované do všetkých strán. Toto zariadenie dokáže snímať 360 stupňový obraz okolia a taktiež trojrozmerný zvuk. Popritom zaznamenáva súradnice miesta, kde sa nachádza. Google toto zariadenie zatiaľ využíva iba na statické fotografie v rôznych intervaloch, ale uvažuje sa aj o Street View videoprezentáciach. Trojrozmerné video vytvorené prostredníctvom tohto zariadenia môžete nájsť na oficiálnych stránkach výrobcu www.immersivemedia.com (v sekcii technology &amp; services klik na Dodeca 2360 system, tam napravo „Immersive demos“). Je to naozaj vynikajúci nápad. Video plynie a vy ho môžete akokoľvek otáčať a sledovať všetko naokolo. V jednom videu tak môžete vidieť dianie pred kamerou, za kamerou, ale aj vedľa nej alebo nad ňou.      Prvé čo ma napadlo keď som si pozrel prvé demo videá, bolo kino. Obrovské 3D kino. Nikde som nenašiel zmienku o formách reprodukcie takéhoto obrazu, ale verím, že aj toto nejakým spôsobom možné je. Skúste si predstaviť obrovské kopulovité kino, kde je celá kopula zároveň premietacím plátnom. Podobne ako v planetáriu, len s rozdielom, že tu by sa premietal film. Ak by ste sa nachádzali v strede, vedeli by ste sa pozerať na všetky strany a všade by sa niečo dialo. Ak by bol takýto spôsob premietania spojený s 3D zvukom a s 3D formou obrazu, ktorú už poznáme zo súčasných kín, myslím si, že zážitok z filmu by bol viac ako dokonalý. Forma záznamu je už vyriešená. Stačí vymyslieť správny spôsob reprodukcie, tak aby bol obraz celistvý. V tom momente môže začať vznikať úplne nová filmová škola. Režiséri a scenáristi by tak museli úplne zmeniť pohľad na stvárnenie reality. Film a jeho dej by musel byť vymyslený a natočený tak, aby sa odohrával nielen pred divákom, ale aj vedľa neho a za ním. Divák by sa tak ocitol v skutočne reálnom prostredí. Dúfam, že sa niečoho podobného dočkám.        Myslíte si, že je to reálne? Ako by sa vám takéto kino páčilo? 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Ornst 
                                        
                                            Je to výhra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Ornst 
                                        
                                            Krátko o hudbe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Ornst 
                                        
                                            Kto všetko, kde všade, a prečo vlastne vás stále niekto sleduje
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Ornst 
                                        
                                            Mediálna manipulácia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Ornst 
                                        
                                            Pútavé pútače
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Ornst
                        
                     
            
                     
                         Ďalšie články z rubriky veda a technika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Džunková 
                                        
                                            Prečo vôbec nevadí, ak nemáte DNA sekvenátor
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - Čo, podľa Poppera, veda dokazuje?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Prečo si niekedy nevšimneme červenú?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana K. Jones 
                                        
                                            Filozofia vedy - kritérium falzifikovateľnosti podľa Karla Poppera
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Tatra 87
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky veda a technika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Ornst
            
         
        ornst.blog.sme.sk (rss)
         
                                     
     
        Moje vedomie trpí vigilitou, luciditou, idiognóziou a svedomím.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1877
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Postrehy
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Krátko o hudbe
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




