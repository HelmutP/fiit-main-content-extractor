
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tibor Pospíšil
                                        &gt;
                Zábavná logika
                     
                 Ako škola robí z detí hlupákov 

        
            
                                    3.10.2008
            o
            8:15
                        |
            Karma článku:
                12.42
            |
            Prečítané 
            6033-krát
                    
         
     
         
             

                 
                    Školská reforma strieda školskú reformu ... Len ľudia zostávajú stále tí istí ... Posúďte sami, aká výzva pre (zatiaľ) zdravý rozum dieťaťa môže byť napríklad taká hodina zemepisu.
                 

                Počul som, že učiteľka v materskej škole vysvetľovala deťom, že v lete je teplejšie preto, že vtedy je Zem bližšie k Slnku, ako v zime. Zažil som, že učiteľka fyziky presviedčala chudáka žiaka o neplatnosti Archimedovho zákona keď tvrdila, že vztlaková sila, ktorá pôsobí na teleso, celkom ponorené v kvapaline, je závislá od hĺbky, v akej je toto teleso ponorené ... Podobné príklady žiaľ ako rodič zažívam častejšie, ako by si dokázal predstaviť aj človek s veľmi rezervovaným názorom k súčasnému školstvu.    Ale nasledovná pasáž z učebnice Zemepisu pre 1.ročník gymnázia, SPN Bratislava, 1.vydanie, rok 1983 ma dostala do kolien.        Takže kto sa chce pozrieť do budúcnosti, hor sa na 180. poludník. Pri jeho prekročení bude stále ten istý dátum. Ak sa však vrátite späť, bude o deň viac. A tak zasa rýchlo späť a nazad. A ste dva dni dopredu. Dobre, každé tam-späť môže trvať povedzme niekoľko hodín, ale stále sa budete do budúcnosti približovať rýchlejšie, ako bežný smrteľník.    A knihu "Okolo sveta za 80 dní" neberte vážne. Najmä záver, v ktorom sa cestovatelia (cestovali smerom na východ) vrátili naspäť do Londýna a zistili, že je tam o 1 deň menej, ako si mysleli. To len autor knihy nechodil do pokrokovej slovenskej školy a preto píše také bludy.    Zaujímavosťou citovanej knihy je to, že na obrázku nad textom je to správne. Ale text vylučuje akúkoľvek pochybnosť, že to autor myslel skutočne vážne. Že nejde o nejaký preklep, či nešťastnú formuláciu. Samozrejme najhoršie na tom všetkom je, že sa nájdu "učitelia", ktorí za správny považujú práve ten text ....           A na záver mená "pedagógov", ktorí by si zaslúžili buď Nobelovu cenu, alebo 25 na holú ... Dobre si prezrite tie tituly pri ich menách. To som zvedavý, za čo to dávajú. Za odborné znalosti a ich uplatnenie v praxi to isto nebude ...       

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (60)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Nekonečné čakanie na ropný zlom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Drahí „liberáli“, od detských izieb našich detí si držte výrazný odstup
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Ako cestovať lacno a pritom pohodlne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Aktuálne udalosti očami anarchistu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Tibor Pospíšil 
                                        
                                            Keď sa somrák a príživník stane celebritou
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tibor Pospíšil
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tibor Pospíšil
            
         
        pospisil.blog.sme.sk (rss)
         
                        VIP
                             
     
        Léta už tajím svoji příslušnost,
k zemi co vyměnila hrubost za slušnost,
bo se mi nelíbí morální předlohy,
čili papaláši hrající si na Bohy. 
...
Upřímně řečeno trochu teskno mi je,
že nám stačí DEMO-Demokracie.
ˇ(c) Tomáš Klus






        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    245
                
                
                    Celková karma
                    
                                                8.63
                    
                
                
                    Priemerná čítanosť
                    4152
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Menej štátu
                        
                     
                                     
                        
                            Túlavé zápisky
                        
                     
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Financie
                        
                     
                                     
                        
                            Finančná poradňa
                        
                     
                                     
                        
                            Zábavná logika
                        
                     
                                     
                        
                            Nebuď ovca
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Hovorme viac o rasizme Rómov
                                     
                                                                             
                                            Pokora libertariánů a domýšlivost sociálních inženýrů
                                     
                                                                             
                                            Ti dole nevolí o nic hloupěji než ti nahoře
                                     
                                                                             
                                            S eurovalom na večné časy
                                     
                                                                             
                                            Uhni mi z cesty, politik…
                                     
                                                                             
                                            Duševné vlastníctvo nie je vlastníctvo
                                     
                                                                             
                                            Válka právníků s ekonomy
                                     
                                                                             
                                            Nie som ovca
                                     
                                                                             
                                            Ako funguje mediálna demokracia
                                     
                                                                             
                                            Finančná kríza: dôsledok viditeľnej ruky štátu
                                     
                                                                             
                                            Kde udělali soudruzi z USA chybu?
                                     
                                                                             
                                            Verejné školstvo bolo vždy zdrojom útlaku
                                     
                                                                             
                                            Čo všetko je možné publikovať a predsa by publikované byť nemalo
                                     
                                                                             
                                            O slovenskom orlovi :-))
                                     
                                                                             
                                            Aj môj osobný spor s moderným liberalizmom
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Menej štátu, viac slobody
                                     
                                                                             
                                            Zaujímavý finančník
                                     
                                                                             
                                            Skutočne VIP blog
                                     
                                                                             
                                            Človek a Pán Fotograf
                                     
                                                                             
                                            Ukecaný :-)), ale obsažný
                                     
                                                                             
                                            Skutočný ochranca pracujúcich
                                     
                                                                             
                                            Maroš - rovný chlap
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Tu hľadám dobré letenky
                                     
                                                                             
                                            Moja nová fotogaléria
                                     
                                                                             
                                            Ako sa vyvíja eurokríza
                                     
                                                                             
                                            Keď nuly ovládnu svet
                                     
                                                                             
                                            Cestovateľský portál
                                     
                                                                             
                                            Magazín Ako investovať
                                     
                                                                             
                                            Kritické myslenie - zaručený recept na vtáčiu chrípku
                                     
                                                                             
                                            Moja fotogaléria
                                     
                                                                             
                                            Aj keď ja som conservative
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Lož menom GENDER
                     
                                                         
                       Tri esemesky z minulosti hľadajú svoju budúcnosť
                     
                                                         
                       Úradnícka vláda ako plán Ficovho návratu
                     
                                                         
                       Jedno víťazstvo nestačí
                     
                                                         
                       Niekoľko mecenášov bude po voľbách v Bratislave veľmi smutných
                     
                                                         
                       V lesoparku sa rúbe a stavia
                     
                                                         
                       Sme vyznávači filozofie „tychinizmu“
                     
                                                         
                       Mal som ja v tej Bratislave zostať
                     
                                                         
                       Schützen Schutz!
                     
                                                         
                       Nie je sused ako sused
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




