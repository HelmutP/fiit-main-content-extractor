

  
 Vystúpili sme z busu a museli ešte zdolať 162 schodov krytého dreveného schodiska, 
 aby sme sadostali ku kostolu Premenenia Pána. 
   
  
 Vyše kostola sme natrafili na sympatickú chalúpku. 
   
  
 Od nej nás viedol chodník medzi haldami sute k vyhliadkovému miestu nad dedinou. 
   
  
 Zhora bol nádherný výhľad na dominantu obce kostola Premenenia Pána. 
   
  
 Pastvou pre oči boli aj okolité hory. 
   
  
 Prechádzali sme sa banskými uličkami a obdivovali kvetinovú výzdobu medzi domami. 
   
  
 Z oblokov sa na nás usmievali červené muškáty. 
   
  
 Na lúkach rozvoniavala materina dúška, žlté a fialové klinčeky. 
   
  
 Na ďaľší deň sme sa vybrali do Kremnice, už na prvý pohľad nás zaujal mestský hrad. 
   
  
 Morový stĺp Najsvätejšej trojice obdivujeme s dáždnikmi, pretože prší, prší a prší ... 
   
  
 Na námestí si pozrieme Múzeum mincí a medailí, kde si zaspomíname so slzami v očiach 
 aj na našu dobrú slovenskú korunu. 
   
  
 Najlepšie nás čakalo na záver v Múzeu Gýča v Kremnici sme sa osobne zoznámili 
 aj s Mojsejovcami. 
   

