
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Urminský
                                        &gt;
                Veselé
                     
                 Sudcov prvý apríl 

        
            
                                    1.4.2010
            o
            12:39
                        (upravené
                1.4.2010
                o
                12:48)
                        |
            Karma článku:
                5.84
            |
            Prečítané 
            450-krát
                    
         
     
         
             

                 
                    Je to fantastické! Náš štát funguje aj prvého apríla. A bez problémov. Nevadí mu, že slovenskí sudcovia sa cítia diskriminovaní. Sú zle platení a majú veľa práce. A majú na to právo. Etika je vec osobná a v niektorých prípadoch aj flexibilná. Nikomu vlastne nič nevadí. Slniečko svieti. Blížia sa kresťanské sviatky. Deti zarobia. Ľudia sa budú pretvarovať, klamať, kradnúť i podvádzať ďalej.
                 

                 A vtom príde blesk z jasného neba. Sudca sa postaví a vážnym hlasom vyriekne ortieľ. „Deti sa zverujú do opatery otca a odporkyňa je povinná mesačne prispievať na ich výživu vo výške...  Celá súdna sieň stíchne a skamenie. Dramatické ticho trvá nekonečnú minútu. Každý čaká na zdôvodnenie. Neprichádza.   Sudca sa najprv usmeje. Zdvihne pohľad a začne sa rehotať hurónskym smiechom.   „Prvý apríl" vykríkne. „Je to samozrejme hlúposť" zahlási vecne, aby upokojil situáciu.   „Žiadna diskriminácia na základe pohlavia nebude. Deti sa zverujú do opatery matky." Pokračuje vecne. „Ale poprosím matku, aby si pred pojednávaním vybrala striekačku z ruky, v súdnej sieni nefajčila a ploskačku si odložila do kabelky. Fakt to nevyzerá dobre!"   Chcete niečo namietať?!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Urminský 
                                        
                                            Prečo končím s blogovaním na SME
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Urminský 
                                        
                                            Prehrali sme znova. Všetci.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Urminský 
                                        
                                            Vojna bude!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Urminský 
                                        
                                            Čert ako diabol a holandský kandidát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Urminský 
                                        
                                            Pokrytectvo slušných ľudí
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Urminský
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Urminský
            
         
        urminsky.blog.sme.sk (rss)
         
                                     
     
         filozof, andragóg, šuflíkový básnik, prekladateľ, tlmočník, píšuci muškár, cestovateľ, laický fotograf, vášnivý kuchár, aktivista, trápny humorista a nenásilný bojovník proti tomuto sebadeštruktívnemu systému...SME je sofistikovaným Rudým Právom súčasnosti. SME neinformuje. SME šíri propagandu. Už tu neblogujem...na protest proti cenzúre. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    50
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2547
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Vážne
                        
                     
                                     
                        
                            Veselé
                        
                     
                                     
                        
                            Poézia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jan Kysela
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Tomáše Klusa
                                     
                                                                             
                                            Simu Martausovú
                                     
                                                                             
                                            ZAZ
                                     
                                                                             
                                            Amy Winehouse
                                     
                                                                             
                                            Vysockého
                                     
                                                                             
                                            Luia Armstronga
                                     
                                                                             
                                            Ellu Fitzgerald
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.slobodnyvysielac.sk
                                     
                                                                             
                                            exposingtruth.com
                                     
                                                                             
                                            vz.ru
                                     
                                                                             
                                            sustainableman.org
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Borbély a mafián? Nie podľa izraelskej obchodnej komory
                     
                                                         
                       Príručka otrokára - Starostlivosť o mláďatá –dohľad nad pôrodom
                     
                                                         
                       Príručka otrokára
                     
                                                         
                       Ak by Píla neurobila v lesoch vodozádržné opatrenie, bola by zas vyplavená
                     
                                                         
                       Prečo končím s blogovaním na SME
                     
                                                         
                       Možná príčina vzniku povodňovej kamennej lavíny vo Vrátnej doline
                     
                                                         
                       Brazílski indiáni sa smejú z nášho volebného systému. Ukončíme to ?
                     
                                                         
                       Princíp prezumpcie nesúhlasu alebo prečo je Európska únia totalitným zriadením
                     
                                                         
                       Ako Mika ohýba Ficovi chrbát a ako prestávam platiť koncesionárske
                     
                                                         
                       Vojna bude!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




