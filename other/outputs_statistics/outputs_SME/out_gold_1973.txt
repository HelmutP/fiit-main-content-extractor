

 Čosi-kdesi pár rokov dozadu sa na rozhraní bratislavského Nového Mesta a Ružinova vybudoval veľký obytný komplex (nazvaný honosne - KOLOSEO). Developer mal evidentne čulé vzťahy s bankou (Slovenská sporiteľňa), pretože okrem financovania celej stavby si banka uňho objednala aj vybudovanie novej centrály hneď cez ulicu. Na tom by v zásade nebolo nič zlé, komu by už len mohla vadiť banka? 
 Pár mesiacov bolo spolužitie v princípe idylické, až kým sa marketing banky nerozhodol využiť priestory novej budovy na veľkoplošnú reklamu (no nech) a najmä na strechu nenainštalovali obrovské svietiace panely s logom banky. 
 Budova banky je, bohužiaľ, o dosť nižšia ako KOLOSEO. Takže ich strechu majú horné (9 a viac) poschodia KOLOSEA ako na dlani. Spolu s jej reklamou, ktorá svieti doďaleka. Aby ste si to vedeli predstaviť, budem to ilustrovať na príklade, ktorý všetci poznáme - spln Mesiaca. Viete si predstaviť Mesiac v splne, kedy je naozaj jasný a veľký? Zhruba tak svieti reklama SLSP mne do izby. Modrým svetlom. Pri troche snahy sa dá pri tom čítať kniha. No a náš byt je na druhej strane komplexu, vzdušnou čiarou asi 80 metrov od reklamy. Byty, ktoré sú najbližšie sú od reklamy zhruba 20-30 metrov. Skrátka také malé domáce solárko. 
 Prirodzene, že sa to nám - obyvateľom nepáčilo a začali sme zisťovat, čo sa s tým dá robiť. No a okrem iných vecí sme zistili, že Slovenská sporiteľňa na túto reklamu VÓBEC NEMÁ POVOLENIE! Miestny úrad ju dokonca údajne vyzval, aby to ihneď vypli, ale z toho si tam evidentne hlavu nerobia. 
 SLSP, samozrejme, požiadala o dodatočné schválenie, ktoré momentálne prebieha a proti ktorému, samozrejme brojíme. Oslovili sme všetky kompetentné inštitúcie, ale chyba lávky - proces vyhodnocovania žiadosti predsalen chvíľku trvá. 
 A SLSP si zatiaľ veselo svieti ďalej. Áno, hrozí im pokuta 13000 EUR - zhruba toľko zaplatia za jeden reklamný spot v primetime niektorej zo slovenských TV staníc. A ja sa pýtam - príde to iba mne choré? Ako je možné, že v tomto údajne právnom štáte neexistuje žiaden spôsob ako prinútiť porušovateľa predpisov neporušovať ich minimálne do právoplatného rozhodnutia? A ak teraz niektorý z úradov rozhodne, že to má SLSP vypnúť, oni sa odvolajú a aj tak to nevypnú, tak čo? Dostanú ďalšiu 13000 EUR pokutu? 
 Niečo zhnité je v štáte dánskom. 
   
 reportáž TA3 - http://www.ta3.com/sk/reportaze/148455_sporitelna-rusi-spanok 
   

