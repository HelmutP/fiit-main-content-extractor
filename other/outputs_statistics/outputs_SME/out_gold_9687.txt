

 Ale nakoniec sme na to prišli. Pracovne sme to nazvali "Varíme za gastráč". Takže témou je ako variť večeru pre rodinu a minúť pri tom čo najmenej. Rozpis bude vždy pre štyri osoby, ale pozor nesmie medzi nimi byť žiaden tínedžer ani žiaden z rodu medveďovitých. V tom prípade je nutné dávky zdvojnásobiť. 
 Na dnešný recept za gastráč budeme potrebovať. Jeden kilogram kyslej kapusty, pol kilogramu kuracích pečienok, dvadsať až tridsať dekagramov hladkej múky a deciliter oleja. 
  
 Kyslú kapustu zlejeme a nakrájame na drobno. Nežmýkame ju, potrebujeme aby bola vlhká. Ak je to poctivá kyslá kapusta, ktorá neobsahuje veľa tekutiny, tak ju necháme všetku. 
  
 K nasekanej kapuste pridáme hladkú múku. Nedá sa povedať koľko presne, pretože to záleží na tom, koľko tekutiny je v kapuste. 
  
 Múku pridávame postupne a vždy zapracujeme do kapusty, až kým nevznikne lepkavé a pružné cesto. Odložíme ho do chladničky a necháme odpočinúť, aby sa všetko pekne spojilo. 
  
 Rozohrejeme panvicu, potrieme ju olejom a lyžicou na nej vytvarujeme placky. 
  
 Opražíme z každej strany do zlatohneda a do chrumkava. Vypražené placky odložíme na teplé miesto a pripravíme si pečienky. 
  
 Očistíme ich a na horúcej panvici opražíme. Pečienky nesmú byť vo vnútri surové, ale zároveň musia zostať ružové a mäkké. 
  
 A áno všimli ste si dobre, nepridávali sme žiadnu soľ a tuku sme použili len minimum. Všetku chuť obstará kvalitná kyslá kapusta. 
  
 Dobrú chuť. 
  

