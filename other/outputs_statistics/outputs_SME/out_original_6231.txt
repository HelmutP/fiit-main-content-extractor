
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Lajčiak
                                        &gt;
                Nezaradené
                     
                 Slovensko a nový Európsky finančný fond 

        
            
                                    10.5.2010
            o
            9:19
                        (upravené
                10.5.2010
                o
                11:28)
                        |
            Karma článku:
                3.01
            |
            Prečítané 
            1062-krát
                    
         
     
         
             

                 
                    Ministri financií Európskej únie sa dnes na mimoriadnom stretnutí dohodli na vytvorení finančného fondu na záchranu štátov eurozóny ktorím sa rúcajú verejné financie. Slovensko pravdaže do eurozóny patrí. Naskytuje sa teda otázka koľko toto rozhodnutie môže Slovensko stáť?
                 

                 Podľa článku v SME (http://ekonomika.sme.sk/c/5367435/unia-zachrani-euro-za-kazdu-cenu-vytvori-fond-so-750-miliardami-eur.html) bude celková výška fondu dosahovať 750 miliárd eur, z čoho 60 miliárd poskytne Európska komisia, 440 miliárd sprostredkujú štáty eurozóny a 250 miliárd pridá Medzinárodný menový fond.   Dá sa predpokladať, že podiel jednotlivých členov eurozóny sa vypočíta podľa ich kapitálového podielu v Európskej centrálnej banke. Týmto spôsobom sa totiž vypočítavala účasť krajín na pomoci Grécku. V prípade Slovenska tento podiel pri vstupe do spoločnej meny činil 0.6934% (http://www.ecb.int/ecb/orga/capital/html/index.en.html), ale kedže naša pomoc Grécku dosiahne 816 miliónov eur z celkovej sumy 80 miliárd eu poskytnutej členskými štátmi, jednoduchý výpočet ukáže, že naša kapitálová účasť v ECB sa vyšplhala na 1.02%. Pri sume 440 miliárd a zachovaní rovnakých pomerov, čo sa dá považovať za pravdepodobné, teda suma za ktorú bude Slovensko zodpovedať na novovznikajúcom fonde bude predstavovať 4 miliardy 488 miliónov eur. Na podporu predstavivosti by sa dalo podotknúť, že toto číslo:     presahuje čistý tok fondov z Európskej unie na Slovensko za celú dobu nášho členstva viac ako štvornásobne    je porovnatelné s projektovanými výdavkami Slovenska na starobné dôchodky na rok 2010    presahuje projektovaný výber daní z príjmu fyzických aj právnických osôb na rok 2010 skoro a jednu miliardu eur      Okrem toho samozrejme netreba zabudnúť, že Slovensko má aj 0.16% kapitálovú účasť v MMF (tam sme už tie zdroje ale dávnejšie poskytli, dalšie nebude pravdepodobne treba) a pravdaže platí aj do rozpočtu Európskej komisie, takže sa bude nepriamo podielať aj na tých dalších 250tich respektíve 60tich miliardách, ktoré poskytnú tieto inštitúcie.   Je dosť možné, že tieto zdroje nebudú musieť byť poskytnuté priamo fondu - môže to byť skôr záväzok ich požičať v prípade, že to fond uzná za vhodné. To sa ešte uvidí, keď budú zverejnené dalšie podrobnosti.   Či táto suma bude stačiť na zvrátenie súčastnej dlhovej krízy a či je táto cesta to správne riešenie sú už otázky pre iný článok, ale dúfam, že sa mi aspoň podarilo priblížiť ako sa na tomto projekte bude Slovensko finančne podielať. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Lajčiak 
                                        
                                            Správny európsky štát
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Lajčiak 
                                        
                                            Ako ušetriť štátu zadarmo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Lajčiak 
                                        
                                            Rana z milosti pre druhý pilier
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Lajčiak 
                                        
                                            Parametrické verzus systémové zmeny dôchodkového systému
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Lajčiak 
                                        
                                            Slovenské produkty na pultoch: Škandál alebo kačica?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Lajčiak
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Lajčiak
            
         
        lajciak.blog.sme.sk (rss)
         
                                     
     
        Večný študent (snáď nie!)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1242
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




