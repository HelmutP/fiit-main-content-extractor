

   
 Sedím v Caffé Creco, 
 kde popíjali Casanova, Goethe, 
 Byron, Bizet, Liszt či Wagner víno, 
 myslím na susediace Španielske schody, 
 na Námestie Navona, fontánu Di Trevy 
 a že z tejto kaviarne je ťažké odísť triezvy. 
   
 Piazza di Spagna obsypaná ľuďmi 
 len ťažko dýcha pod ich váhou. 
 No po polnoci sa skvejú plnou krásou, 
 keď neskorí milenci upletú si lásku z kvetov.  
 V Caffé Creco ešte prúdom tečie červené víno 
 a ty si predstavuješ, že je bál a si domino.  
   
   
   
   

