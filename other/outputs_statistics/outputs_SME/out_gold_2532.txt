
 Týkali sa stavu detských ihrísk, ako my vidíme starostlivosť o ne,
opravy, nátery, ale i výmenu piesku v pieskovisku, kakaové pozdravy od
psíkov a podobne. Celkom som sa rozrečnil, keď tu sa z kočíka ozval aj
Leo. 
 
Redaktor sa usmial a strčil mu pred tvár mikrofón. Leo neváhal a
vyštartoval po zaujímavom objekte. Rukou si ho pritiahol a dal sa ho ochutnávať. Mali
ste vidieť tú paniku v tvári redaktora... 
 
To však nie je pointa. Tá nasledovala až vzápätí. Keď sa redaktorovi
podarilo mikrofón trochu odtiahnuť, Leo sa pustil do džavotu, niečo
ako "Amavavava, aeaoama, prrrrr," len to bolo dlhšie. To už sa
redaktor opäť škeril, ale dlho mu to nevydržalo. Totiž kameraman
zložil kameru a krútil hlavou. Chvíľku spolu debatovali a potom nám
poďakovali s tým, že možno moje odpovede použijú, ale Leove rečnenie
samozrejme na obrazovku nepôjde. 
 
Ako hrdého otca ma to ranilo, takže som sa opýtal, že prečo, veď tak
krásne džavotal. Redaktor pokrčil plecami, vraj nedá sa, jazykový
zákon nepustí. To vraj nikto neotitulkuje - a dnes nikto neriskne
vysielať cudziu reč bez nejakého prekladu, v najhoršom prípade
titulkového. 
 
Škoda, Leo si tak na televíznu premiéru ešte chvíľu počká. Ledaže by
jeho zábery dali ako podklad k rečiam mimo obraz. 
 
  
