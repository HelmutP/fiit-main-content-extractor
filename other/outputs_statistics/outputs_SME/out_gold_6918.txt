
 Je mi úplne jedno, ako voľby dopadnú. Nie preto, že nejavím záujem o veci verejné, ale preto, že občan v nich nijako nemôže dopadnúť dobre. 
Keď aj opomeniem, že žiadna strana nemá v programe a ani sa nesnaží odstrániť príčinu marazmu spoločnosti, ktorá tkvie v tom, že systém sociálneho zabezpečenia, školstva a zdravotníctva je ešte vždy systémom socialistickým, hoci sme v roku 1989 socializmu(?) povedali nie. A nie preto, že to niekoho prestalo baviť, ale preto, že ten systém nebol schopný (na vtedajšom a i teraišom stupni spoločenského vedomia) postarať sa o tvorbu zdrojov.
Tým, že tieto tri oblasti zostali na socialistickom princípe, sa na troskách rozpadnutého systému nevytvoril systém nový. Ten sa sám od seba sa ani nevytvori. Strany kandidujúce v týchto voľbách, nemajú ambíciu toto zmeniť, čo je prvým dôvodom dražby môjho hlasu.
Druhým a prízemnejším je skutočnosť, že žiadna z kandidujúcich strán nebude mať možnosť naplniť to, k čomu voľbami dostane mandát. 
Ľavicové strany nepochopili prečo musel padnuť socializmus(?) a žiadna nemá predstavu, ako zabezpečiť tvorbu a rast zdrojov. Bez toho nebudú nať v moci naplniť predvolebné sľuby. 
Pravicové strany zas preferujú také sľuby, za ktoré by sa nemusel hanbiť ani Fidel. Takže ak dostanú mandát, tak aj tak nebudú mať možnosť tieto sľuby naplniť.
Z tohto je zrejmé, že je jedno akého koňa voľbami dostaneme, lebo sa nám občanom z neho aj tak újde, len tá jeho nepopulárna časť. 
Možno som to však zle pochopil a všetko je úplne inak. Dajte mi vedieť o strane, ktorá kandiduje, je pravicová a nemá ľavicový program, alebo o strane ľavicovej, ktorá ponúka východiská na tvorbu a rast zdrojov a potom zruším dražbu a pôjdem voliť podľa svojho názoru. 
