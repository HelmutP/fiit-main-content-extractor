

 Nasleduje niekoľko rôznych spôsobov vyobrazenia panorám: 
 1. Kliknutím na náhľad sa v tom istom okne otvorí web stránka PANORAMY.SME.SK s danou panorámou - pre návrat na článok v blogu je potrebné použiť funkciu web prehliadača pre návrat (kliknutím na náhľad s pravým tlačítkom myši je možné vybrať voľbu pre otvorenie odkazu v novom okne/karte): 
  
 :-( Užívateľ je odvedený z článku a pre návrat musí kliknúť tlačítko "Späť" vo svojom web prehliadači, čo znova načítava náš článok (a znižuje karmu, ale to nám nevadí :-)) 
   
 2. Kliknutím na náhľad sa otvorí v novom okne/karte web prehliadača stránka PANORAMY.SME.SK s danou panorámou, po prehliadnutí panorámy môžete okno zavrieť: 
  
 :-( Ak je v článku viac náhľadov, odkazujúcich na viac panorám, užívateľovi sa po kliknutí na náhľad každá otvorí v novom okne/karte. Ak užívateľ pred návratom do okna s článkom ponechá okno panorámy otvorené a otvorí okno s ďalšou panorámou, môže mu to výrazne spomaliť systém, alebo dokonca spôsobiť pád prehliadača 
 3. Kliknutím na náhľad sa otvorí panoráma v novom, vyskakovacom okne: 
  
 :-( pri použití panorámy s hotspotom sa po kliknutí naň momentálne otvorí stránka PANORAMY.SME.SK s panorámou, na ktorú hotspot odkazuje (toto by sme vedeli upraviť tak, aby sa otvárala len samotná panoráma) 
 4. Panoráma vložená priamo do textu: 
 5. Iná - prosím, napíšte do diskusie 

 
BlueBoard.cz


 Ďakujeme za Váš názor! 
 Igor Socha, panoramy.sme.sk 

