

 
Po „prvej pollitre" sa rozhovoril aj on. Práve vyšiel z basy
a vracia sa domov. Času bolo hodne, cesta ďaleká, aj bumážky ukázal, ja som ich
videl - ja som uveril - je na Vás či uveríte aj vy:
 
 
So svojou mladou ženou, Sergej žil na hlbokom ruskom vidieku
a ako je tam zvykom, v dedinke všetci pijú. 
A ako je tradíciou, víkendy sa kultúrne prežívajú s fľašou. Bolo to aj
tú sobotu. Po nejakom čase, Sergej zistil, že zásoby sa skončili, dokonca aj
domáca samohonka. Sadol na bicykel a „davaj" do susednej dediny, bola väčšia a
obchod by mal byť otvorený aj v sobotu. Ako tak šľape, okolo príroda, vtáci, v
hlave jasno, ... uvolnil sa až tak, že pri jazde z kopca stratil riadenie a
trafil hlavou rovno do jablone pri ceste. 
 
 
Ďalej to vie len z rozprávania. Po tej istej ceste, a s tým
istým cieľom sa vybral do susednej dediny aj jeho sused. Len miesto bicykla sa
viezol autom. Zbadal pri ceste Sergeja, trasie ním a milý Sergej, žiadna
reakcia. Naložil ho do svojho auta a odviezol do najbližšieho mestečka, kde
bola nemocnica. Malé mestečká niekde v ruskej glubinke za Uralom, sa taktiež
držia tradícií:  lekár na príjme bol v
dobrej kondícii, čiže na mol. Vypočul si históriu havárie, popočúval tep, viečka
nadvihol: „Vsio!" hovorí "tvoj priateľ dobicykloval. Zatiaľ nech poleží v
márnici, v pondelok spravíme papiere." 
Takto sa Sergej dostal do márnice a jeho sused odišiel k "vdove",
oznámiť jej tragickú novinu. 
 
 
Po nejakom čase sa milý Sergej  prebral od chladu, hlava bolí, nič nevidí,
len spod dverí vidno úzky pás svetla. Prekontroloval si končatiny, všetko v
poriadku,  na hlave objavil poriadnu
hrču. Vybral sa za svetlom. Keď otvoril dvere, pochopil kde odpočíval. Hodiny
na stene ukazovali 10, podľa všetkého pred polnocou... Keďže sa mu po ceste
pritrafil len spiaci doktor , kľudne a hlavne nepozorovane  dostal sa na ulicu. Domov šliapal vyše
poldruha hodiny. To mu aj prospelo, svieži vzduch a bolesť hlavy - z toho aj
vytriezvel.  
 
 
Predstavoval si rôzne obrazy stretnutia so svojou ženuškou.
Chudáčik, jej už asi oznámili, že muž je po smrti. Domov treba vojsť tak aby ju
neprestrašil, pretože verila na duchov. Bol už v dedine. Všade tma, len v jeho
dome sa svietilo. Pomyslel si -„Plače, naisto". Pocítil pri tom „až" nehu k
svojej polovičke, „až" sa sám tomu trochu zadivil, veď v poslednej dobe sa k
nej už neobracal inakšie, ako s pomocou nadávok - to je tiež miestna
tradícia.   
 
 
Podišiel úplne k domu, k oknu. Vstal na špičky a pozrel
dovnútra. A tam... ...Jeho priateľ, sused ktorý ho  našiel, v tento moment "utešoval" jeho ženu
najjednoduchším spôsobom. Minút desať sa nemohol pohnúť a pozeral sa ako
novopečená "vdova" ochká a stoná v susedových rukách. „No do (ruské neprekladateľné
slovo určujúce miesto)!" povedal do ticha Sergej "radšej som mal byť mŕtvy, ako
sa dozvedieť, že 20 rokov som žil s ..." 
 
 
Vbehol   do domu, po
ceste zobral sekeru.  Uvidel hrôzu  v očiach tých dvoch. Ešte že! Asi si
pomysleli, že sa vrátil  z pekla, aby sa
pomstil, pretože mu v márnici neutreli zakrvavenú tvár.  
 
 
Ženu jednoducho udrel tupou stranou sekery po hlave. Rana
bola smrteľná. Potom začal mlátiť jej milenca a podľa všetkého, tiež by ho
zabil. Zbehli sa však na krik susedia, a záchrancu zachránili. 
 
 
Potom bol súd. Všetky obľahčujúce okolnosti súd prijal a
Sergej dostal 4 roky. A práve odsedel 3 a pol. Takže sa vracal domov. Žene
neodpustil,  veď, čo že potom, že bola
neverná, stáva sa - tiež miestna tradícia! Štvalo ho však, že ho podviedla,
hneď potom ako „umrel"...
 
 
Čo povieš???  Reálne
aj pre naše kopanice???
 

