

 Spoločnosť B+Business Centre, s. r. o., v ktorej je spoločníkom Vladimir Raiman, začali médiá spomínať pred niekoľkým týždňami v súvislosti s doporučujúcim listom ministra financií Ivana Mikloša. Ten vo februári adresoval ministrovi zahraničných vecí Eduardovi Kukanovi list, ktorý nepriamo pomohol súkromnej firme k získaniu štátnej zákazky v Kukanovom rezorte. 
 Zákazku v hodnote 7 miliónov dostala Raimanova spoločnosť B+Advertising, s. r. o., hoci Mikloš písal referenčný list na požiadanie inej Raimanovej firmy B+Business. 
 Minister financií sa obhajoval tým, že šlo o štandardný odporúčací list, „aké sa normálne píšu v každej normálnej krajine“. Mikloš považoval za nenormálne, že to bolo „nenormálnym spôsobom zneužité vrátane zneužitia médiami“. Zverejnil list na internete a povedal, že už žiadny taký - hoci „štandardný odporúčajúci list“ - nenapíše. 
 Čurný z odboru verejného obstarávania ministerstva zahraničných vecí médiám povedal, že Miklošov odporúčajúci list bol pre výberovú komisiu „taký ten závažnejší doklad“, na základe ktorého rozhodla. Firma B+Business Centre má základné imanie 200-tisíc korún, firma B+Advertising má základné imanie tiež 200-tisíc. V oboch je Raiman jedným z dvoch spoločníkov. 	(r) 

