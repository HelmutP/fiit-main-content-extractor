
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Robert Mihály
                                        &gt;
                Nezaradené
                     
                 SNS verzus zdravý rozum 1:0 

        
            
                                    6.5.2010
            o
            13:41
                        (upravené
                6.5.2010
                o
                14:10)
                        |
            Karma článku:
                9.45
            |
            Prečítané 
            2149-krát
                    
         
     
         
             

                 
                    Aj napriek faktu, že ohlasy médií, mimovládnych organizácii a politikov na protirómsky billboard a kampaň Slovenskej národnej strany sú zväčša striktne negatívne, SNS podľa môjho názoru opäť raz potvrdila svoje bravúrne propagandistické umenie. Jej predvolebná kampaň je teraz totiž zadarmo, celoštátna a hlavne (príznačne) slovenská! Neverím síce, že by sa hlavní predstavitelia tejto strany vedeli tak profesionálne pretvarovať na vulgárnych chrapúňov, ktorí zo seba nevedia vyžmoliť ani jednu súvislú vetu a následne by prepracovali túto stratégiu, no každopádne ich (zrejme výborne platení) zákulisní machiavelisti vedia čo robia a vedia to veľmi dobre!
                 

                 
  
   SNS už dlhšiu dobu balancovala v prieskumoch na spodnej hranici zvoliteľnosti do parlamentu, preto nikoho zrejme neprekvapil ich nedávny pokus pritahnuť voličov ich tradičným "národniarskym" spôsobom - vlasteneckým zákonom. Akonáhle sa však proti tomuto zákonu postavili riaditelia škôl, študenti a mnohí odborníci zákon bol vetovaný a druhý pokus znemožnil premiér Fico svojim hrdinským kompromisom, ktorým zákon radšej skryl za novelu o štátnych symboloch a prepísal na účet smeru. Bolo zrazu po predvolebnom ese, no i tak bol tento krok efektný. Nie zmyslom a rozumom, ale kontroverziou. Sporom o to kto je väčší, najväčší a nejmenší vlastenec a kto je zlý Maďar, ktorý chce prísť na tanku. A nebol to už len spor politikov, stal sa i sporom medzi bežnými ľudmi a témou v krčmách.   SNS však tentokrát pristúpila k ešte slovenskejšiemu kroku. Ako najlahšie predsa osloviť čo najväčší počet voličov na Slovensku? Urobiť protirómsky billboard, zneužiť protirómsku mienku ľudí a neponúknuť kroky k riešeniu tejto otázky. A na svete je druhé  predvolebné eso SNS! Najsmutnejšou vecou, ktorá mi zároveň robí i najväčšie obavy je fakt, že mnohí ľudia v tejto krajine sa s touto kampaňou skutočne stotožňujú i napriek tomu, že jej propagandisticko-populistický charakter bije do očí. Ja osobne som si billboard SNS nevšimol ešte nikde v Bratislave a to cestujem do centra každý deň. Zaregistroval som ho až v novinách a televízií. Média totiž takisto pristúpili na túto hru a robia SNS celoslovenskú kampaň zadarmo! Ako sa hovorí i negatívna reklama je reklama, avšak ako som spomínal už vyššie je otázne či je výsledný efekt v mienke ľudí na Slovensku skutočne až tak negatívny.   Obrovskou hanbou je to, že ľudia na tieto veci ešte stále naletia..a naletia aj tisíckrát a platňa preskočí.. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Gratulácia i kritika Lajčáka ohľadom obchodu so zbraňami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Slovensko zrejme porušilo zbrojné embargo voči Číne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Protest počas medzinárodnej bezpečnostnej konferencie v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Partnerom GLOBSEC 2013 je zbrojárska firma známa korupciou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Robert Mihály 
                                        
                                            Hlinkov prostredník pred Ústavom pamäti národa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Robert Mihály
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Robert Mihály
            
         
        robertmihaly.blog.sme.sk (rss)
         
                                     
     
        Som človek, ktorý sa snaží hľadieť svojimi očami. Som človek, ktorý navôkol vidí iba ďalších ľudí, nikoho viac a nikoho menej.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    82
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2006
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Indiáni v Izraeli
                     
                                                         
                       Mier nie je extrém
                     
                                                         
                       Gratulácia i kritika Lajčáka ohľadom obchodu so zbraňami
                     
                                                         
                       Slovensko zrejme porušilo zbrojné embargo voči Číne
                     
                                                         
                       Protest počas medzinárodnej bezpečnostnej konferencie v Bratislave
                     
                                                         
                       Partnerom GLOBSEC 2013 je zbrojárska firma známa korupciou
                     
                                                         
                       Hlinkov prostredník pred Ústavom pamäti národa
                     
                                                         
                       Kontraples pred operou!
                     
                                                         
                       Za rozdávanie jedla zdarma sú fyzicky napádaní!
                     
                                                         
                       Vycvičme si SS-ákov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




