

Papriková pomazánka I. 
 
2 kg červenej papriky,1 kg zelenej papriky, ½ kg mrkvy, 10 ks feferóniek, 1 kg cibule, 6 hlávok cesnaku, 130 g cukru,, 130 g soli, 2 dcl oleja, 2 dcl octu, 2 pol. Lyžice horčice, 2 lyžičky mletého čierneho korenia. Všetko zomelieme pridáme príchute a varíme 1 hodinu .Rozmixujeme a sterilizujeme v rúre 20 minút. 
 
 
Papriková pomazánka II. 
 
 
1,5 kg paradajok,2 kg červenej papriky,1 kg cibule, 10 feferóniek, 2 strúčiky cesnaku,20 dkg cukru, 6dkg soli, 1 dcl oleja, 2 dcl octu, všetko zomlieť, 1 hodinu variť, rozmixovať ,dať do pohárov a zasterilizovať 20 minút pri 100* C v rúre. 
 
 
 
Papriková pomazánka III. 
 
 
4 kg červenej papriky,1 kg cibuľa,10- 15 feferóniek, zomlieť ,pridať 3 dcl octu,2 dcl oleja ,1 deko- 2 balíky,1/2 kg cukru. Variť 20 minút , rozmixovať a dať do pohárikov a vysterilizovať v rúre. 
 
 
 
Papriková pasta na varenie štipľavá. 
 
 
2 kg červenej papriky a ½ kg červených feferóniek zomelieme na mäsovom mlynčeku, môžeme ale nemusíme pridať 6-10 paradajok. Varíme asi 1 hod ,pridáme 1 dcl octu ,5-6 polievkových lyžíc cukru, trošku soli, 1/2 dcl oleja, ešte chvíľu povaríme ,rozmixujeme, plníme do pohárikov a sterilizujeme v rúre .Plníme do malých pohárikov, lebo otvorené sa rýchlo kazia. 
 
 
Patizónová nátierka 
 
 
1kg patizóny, 1kg paradajky, 250 g cibule,2 dcl oleja, 4 kávové lyžičky soli, 4 lyžičky cukru, 1-2 feferónky, 4-5 strúčikov cesnaku. 
Nadrobno pokrájanú cibuľu podusíme na oleji, pridáme patizóny, paradajky a ostatné pochutiny a uvaríme do mäkka a horúcu rozmixovanú zmes plníme do malých pohárikov a sterilizujeme v trúbe 15 minút 
 
 
Dresing z cukety I. 
 
 
 
2 kg postrúhanej cukety, 2 veľké cibule,1 feferónka, 1 väčšia hlávka cesnaku, 3 ks papriky, necháme 20 minút variť. Do povarenej zmesi pridáme ½ pohára chrenovej horčice/ 1 dcl/, 3 dcl oleja, 10 dkg cukru, 1 dcl octu, 4 kávové lyžičky soli a čierne mleté korenie. Všetko varíme ešte 5 minút, rozmixujeme a sterilizujeme v rúre 20 minút. 
 
 
Dresing z cukety II. 
 
 
2 kg postrúhanej cukety, 2 zelené papriky,1-2 cibule, 1hlávka cesnaku, 1-2 feferónky, 4,lyžičky soli, trochu mletého čierneho korenia, 2a ½ dcl oleja, 10 dkg cukru. 
Všetko dať do hrnca variť. Nechať vychladnúť a pridať ½ pohára- 1dcl horčice a 1 dcl octu. Rozmixovať, vliať do pohárikov a sterilizovať 15-20 minút pri 80*C. 
 
 
 
 

