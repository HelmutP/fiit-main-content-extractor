



 
 
  
 
 
 
 
 
 
 




 
 
  



 Spravodajstvo 
 
 Spr?vy 
 Dom?ce 
 Zahrani?n? 
 Ekonomika 
 Videospravodajstvo 
 Reality 
 Kult?ra 
 Koment?re 
 Shooty 
 
 ?port 
 
 ?port 
 V?sledky 
 
 Slu?by 
 
 Blog 
 Vybrali SME 
 Predpove? po?asia 
 TV Program 
 Teraz 
 English news - Spectator 
 Magyar h?rek - UjSz? 
 


 Tematick? weby 
 
 Porad?a 
 AUTO 
 TECH 
 TV SME 
 ?ena 
 Prim?r (Zdravie) 
 R?movia 
 Vysok? ?koly 
 Cestovanie 
 Dom?cnos? 
 
 Pr?lohy a sekcie denn?ka SME 
 
 Rozhovory 
 TV OKO 
 V?kend 
 



 
Spr?vy
 

 
  



  Vybra? svoj regi?n  
 Z?pad 
 
 Bratislava 
 Levice 
 Nov? Z?mky 
 Nitra 
 Pezinok 
 Senec 
 Topo??any 
 Tren??n 
 Trnava 
 Z?horie 
 
 V?chod 
 
 Korz?r 
 Humenn? 
 Ko?ice 
 Michalovce 
 Poprad 
 Pre?ov 
 Star? ?ubov?a 
 Spi? 
 


 Stred 
 
 Bansk? Bystrica 
 Kysuce 
 Liptov 
 Novohrad 
 Orava 
 Pova?sk? Bystrica 
 Prievidza 
 Turiec 
 Zvolen 
 ?iar 
 ?ilina 
 



 
Regi?ny
 

 
  



 Praktick? slu?by 
 
 Druh? pilier 
 TV Program 
 Artmama 
 Deti 
 Dvaja.sme.sk 
 Fotky 
 Gul? 
 Horoskop 
 Inzer?ty 
 Jazykov? porad?a 
 Nani?mama 
 Napizzu.sk (don?ka pizze) 
 Pr?ca 
 Re?taur?cie 
 Torty od mamy 
 Tla?ov? spr?vy 
 ?ahaj 
 Zlat? fond (aj e-knihy) 
 


 Odborn? recenzie elektroniky 
 
 mobily 
 telev?zory 
 tablety 
 notebooky 
 fotoapar?ty 
 hern? konzoly 
 
 Tret? sektor 
 
 Cena ?t?tu 
 ?akujeme 
 Obce 
 Otvoren? s?dy 
 ?koly 
 Tender 
 Vysok? ?koly 
 



 
Slu?by
 

 
  



 
 Last minute dovolenka 
 Don?ka jedla 
 Reality 
 Z?avy 
 Autobaz?r 
 Filmy na DVD 
 Hodinky 
 Parf?my 
 SME knihy a DVD 
 SME predplatn? 
 Superhosting 
 Tla?iare? 
 V?no 
 Tvorba webov 
 



 
Nakupujte
 

 
  
    

  
 
 
 
Kl?vesov? zkratky na tomto webu - roz???en? 
Presko?i? hlavi?ku port?lu
 
 
  
   
     
Sme.sk | V?hodn? n?kup (vydanie z 23. 7. 2004) | V Po?sku Slov?ci kupuj? lacn? stavebn? materi?l 
     
       







       
     
   
  
  
   
    
     
      
       


 
 
diskutujte VYTLA?I? UPOZORNITE NA CHYBU Po?lite:
E-MAILOM na facebook
VYBRALI.SME | ?al?ie
 
 
 delicious.com 
 Google Bookmarks 
 MySpace 
 Twitter 
 
 
 
 V Po?sku Slov?ci kupuj? lacn? stavebn? materi?l 
 
 
 
 Na n?kupy do susedn?ho Po?ska chodia Slov?ci radi u? desiatky rokov. Cez v?kendy je na hrani?n?ch priechodoch plno, v preplnen?ch autobusoch sa ?udia vracaj? z trhov so svojimi ?lovkami - ratanov?m n?bytkom, k?vou, ko??kmi, krovkami, vetrovkami, top?nkami pochybnej kvality, bez z?ruky. Ni? na tom nezmenilo ani to, ?e sme v Eu?opskej ?nii. Cena je cena. 
 "Nem?me d?vod nakupova? v po?sk?ch obchodoch, nakupujeme na trhoch. To sa oplat?," hovoria v Skalitom, ktor? hrani?? s Po?skom. Kysu?ania vo v?eobecnosti hovoria, ?e chodi? do po?sk?ch obchodov je zbyto?n?. Nie?o je vraj lacnej?ie, ale ke? sa k cene prir?ta benz?n, vyjde to rovnako. In? hovoria t?, ?o stavaj?. Z Po?ska vozia stavebn? materi?l, zatep?ovacie dosky, obklada?ky, stre?n? krytiny, vane, um?vadl?, z?chody. 
 Ceny sa v?ak porovn?va? nedaj?. Zna?kov? materi?l stoj? rovnako ako na Slovensku, v niektor?ch pr?padoch aj drah??. Lacnej?ie s? materi?ly, kde je neraz ?a?k? vystopova? v?robcu. Obchodn?ci na slovenskej strane hovoria, ?e porovn?va? neporovnate?n? je nezmysel. 
 Slov?ci vozia materi?l na zatep?ovanie, lebo ho k?pia lacnej?ie. M? ni??iu hustotu, takmer ?iadne izola?n? vlastnosti. "Rovnak? je to s lacn?mi strechami, plastov?mi va?ami, bat?riami," hovor? ??f obchodu so stavebninami v ?adci, ne?aleko po?sk?ch hran?c. 
 
 
 

 Materi?l na zateplenie rodinn?ho domu ?tandardnej ve?kosti od 200 do 250 ?tvorcov?ch metrov vyjde na Slovensku na pribli?ne 100-tis?c kor?n, v Po?sku od 60- do 80-tis?c, z?vis? od nekvality. ?lovek za nezaru?en? kvalitu u?etr? v priemere tretinu ceny. 
 Na plastovej vani sa d? v Po?sku u?etri? aj tritis?c kor?n. Slovensk? predajcovia v?ak tvrdia: Nedr?ia farbu, nie s? odoln? vo?i ?krabancom, s? krehk?, maj? kr?tku z?ruku, ak v?bec. 
 V po?sk?ch obchodoch sa d? k?pi? lacnej?? tovar, ak ?lovek poriadne h?ad?. Tak napr?klad, pri k?pe kamery JVC za 1399 zlot?ch, m??ete oproti rovnakej u n?s u?etri? viac ako ?tyritis?c kor?n alebo dvetis?c na um?va?ke riadu Electrolux. 
 Pri prepo?toch je potrebn? bra? do ?vahy vzdialenos? a spotrebu benz?nu. Z Banskej Bystrice je na hrani?n? priechod Skalit? - Zwardo? 140 kilometrov a z Bratislavy asi 250. Z Bratislavy do Bielsko Bialej zaplat?te za cestu autom asi 800 a z Banskej Bystrice o 300 kor?n menej. 
 Pri ceste treba po??ta? aj s preplnen?mi ?zkymi cestami na Kysuciach a e?te hor??mi na po?skej strane. Po?sk? cesty s? ?zke, zle zna?en?, bez spevnen?ch krajn?c, n?jdete aj ?seky zo ?ulov?ch kociek. Priemern? r?chlos? sa pohybuje okolo 70 km/h a menej. 
 Ceny pohonn?ch l?tok s? v Po?sku ni??ie ako na Slovensku. Super 95 stoj? 35,60 Sk, ?o je o takmer korunu menej ako u n?s, za 98 o korunu a? dve menej a za naftu o ?tyri koruny menej za liter ako na Slovensku. 
 Zywiec 
 Prv?m v???m mestom po prekro?en? hrani?n?ho priechodu v Skalitom je Zywiec. Z Banskej Bystrice do Zywca je 180 kilometrov, ?o znamen?, ?e benz?n tam aj nasp? vyjde okolo tis?c kor?n. Odpor??ame dotankova? pln? n?dr? v Po?sku. Auto s dieselov?m motorom m??e na plnej n?dr?i u?etri? 200 kor?n. 
 Pre Bratislav?anov je Po?sko v?hodn?, len ak na n?kupoch u?etria viac ako tritis?c kor?n. Pribli?ne 1600 kor?n toti? stoj? iba cesta do prv?ho mesta a nasp?. 
 Zywiec nie je pr?ve ve?k? mesto pln? supermarketov, ale pr?ve sem chodia Kysu?ania nakupova? stavebn? materi?ly. Vstup do mesta od Zwardo?u lemuj? obchody pripom?naj?ce diskont, kde pred?vaj? stavebn? materi?ly ?i sanitu do k?peln? a WC. P?ta?ov a rekl?m je dos?. 
 Dobr? obchodn? centrum na n?kup van?, um?vadiel a bat?ri? je Eko-Instal bl?zko centra Zywca. Po vstupe do mesta pokra?ujete pribli?ne tri kilometre st?le rovno po hlavnej ceste a v?razn? firemn? p?ta? v?s zavedie v?avo priamo na priestrann? parkovisko firmy. Nev?hoda je, ?e hlavn? cesta je ?zka a frekventovan?, tak?e odbo?i? mimo cesty je probl?m. Po hlavnej ceste pokra?ujete priamo do centra mesta, ktor? pripom?na talianske uli?ky s mno?stvom zna?kov?ch obchodov vr?tane elektroniky, men??ch n?kupn?ch centier a kaviarni?iek. Stredom mesta v?s prevedie zna?enie smeru k n?kupn?mu centru DETAL MARKET na okraji Zywca. Problematick? je uv?dzanie vzdialenosti v min?tach, preto?e tri min?ty na zna?ke v ?pi?ke znamenaj? v skuto?nosti 15 min?t. ?al?ou ?pecialitou zna?enia je ??pka v tvare le?iaceho "U", ktor? znamen?, ?e ste ?plne mimo a mus?te sa vr?ti?! 
 V n?kupnom centre DETAL MARKET m??ete nakupova? v troch ?astiach vr?tane z?hradn?ckych potrieb, v potravin?ch, ale aj v ?asti AVANS s elektronikou, bielou technikou fotoapar?tmi vysava?mi, ve?ov?mi syst?mami, telev?zormi... Nev?hodou je mal?, st?le pln? parkovisko. 
 V?hodou Zywca je to, ?e v?etko podstatn? sa deje okolo hlavnej cesty. V Zywci je dostatok ?erpac?ch stan?c vr?tane LPG. Ceny benz?nu sa pohybuj? okolo 36 kor?n, nafta je lacnej?ia, dostanete ju za 29 kor?n. Rozdiely medzi ?erpadlami s? halierov?. 
 Bielsko Biala 
 Bielsko Biala je vzdialen? od Zywieca 20 kilometrov smerom na Katovice, teda pribli?ne 60 kilometrov od hrani?n?ho priechodu s Po?skom. ?tvorpruhov? severn? hlavn? ?ah vedie priamo centrom mesta popri hoteli Prezydent a? k n?kupn?m centr?m Hypermarket Tesco a ne?alekej Hypernove na opa?nom konci. Od ju?n?ho vstupu do mesta s? hypermarkety preh?adne zna?en?. Problematick? je prehusten? doprava v ?pi?k?ch a nedostatok parkovac?ch miest v centre. 
 V strede mesta je v??ina zna?kov?ch obchodov s oble?en?m, ale aj s obuvou ?i elektronikou, mno?stvo cenovo pr?stupn?ch pr?jemn?ch re?taur?ci? a kaviarn?. V?hodou je, ?e v?etko je situovan? v bl?zkosti hlavn?ho ?ahu a v pri?ahl?ch uli?k?ch. 
 K hypermarketu Tesco je dobr? a preh?adne zna?en? pr?stup. V Tescu je mno?stvo ?al??ch obchod?kov. D? sa plati? aj bankovou kartou. Nev?hodou je, ?e plecniaky a ta?ky mus?te pri vstupe do n?kupn?ch priestorov odovzda?. Fotografovanie vn?tri n?kupn?ho centra, ale aj na parkovisku pred hypermarketom je zak?zan?. V bl?zkosti je aj benz?nov? ?erpadlo s lacn?m benz?nom. 
 Z Tesca sa jednoduch?m sp?sobom dostanete na kruhov? objazd so smerovou tabu?ou Zyviec a odtial st?le po hlavnom ?ahu cez mesto a? na v?padovku. 
   
 Porovnanie cien v Po?sku a na Slovensku 
 
 



?erpacie stanice


 
Po?sko
Slovensko - Slovnaft


Benz?n 95
35,60 Sk
36,40 Sk


Benz?n 98
37 - 38 Sk
39 Sk


Diesel
29 Sk
33,40 Sk


 
Avans Zywiec
Nay Slovensko


Telev?zor


Philips 21PT 1967
999 Zl (9 140 Sk)
8 990 Sk (Nay)


Thomson 21DX 175
1199 Zl (10 970 Sk)
10 990 Sk (Nay)


Philips 21 PT 4406
899 Zl (8 230 Sk)
8 990 Sk (Nay)


Um?va?ka riadu


Whirlpool ADP 4520
1349 Zl (12 340 Sk)
13 990 Sk (Nay)


Electrolux ESF 6101
1319 Zl (12 070 Sk)
15 990 Sk (Nay)


Pr??ka


Electrolux EWF 810 
1349 Zl (12 340 Sk) 
13 990 Sk (Nay)


Kamera


JVC GR-FxM39 1399 
(12 800 Sk)
16 990 Sk (Nay)


Sony CCD-TRV 228E 
1499 Zl (13 720 Sk)
9 990 Sk (Nay)


Canon MV700 1899 Zl 
(17 380 Sk) 
12 340 Sk (Nay)


Chladni?ka


Whirlpool ARC 5520
1499 Zl (13 720 Sk)
13 500 Sk (Nay)


Electrolux ERB 3043
1749 Zl (16 000 Sk)
14 500 Sk (Nay)


 
Eko-Instal Zywiec
UNIMAT B. Bystrica


Va?a


plastov?
422 Zl (3 860 Sk)
7 500 Sk


plastov?
540 Zl (4 940 Sk)
7 600 Sk


rohov?
943 Zl (8 630 Sk)
6 288 Sk


rohov?
954 Zl (8 730 Sk)
8 420 Sk





 Um?vadlo
120 Zl (1 100 Sk)
750 - 900 Sk


 
145 Zl (1 130 Sk)
770 - 920 Sk


 
209 Zl (1910 Sk)
785 - 958 Sk





 WC

400 Zl (3 660 Sk)
3 000 Sk


 
500 Zl (4 580 Sk)
3 300 Sk


 
Tesco Bielsko-Biala
Tesco Slovensko


Vibra?n? br?ska
69 Zl (630 Sk)
699 Sk (akcia 299) Sk


Uhlov? br?ska
85 Zl (780 Sk)
899 Sk (akcia 499)


Pr?klepov? v?ta?ka
120 Zl (1 100 Sk)
1 199 Sk (akcia 599)


 
Potraviny - Po?sko
Potraviny - Slovensko


K?va


Tchibo 250 g exclusive
7,79 Zl (71 Sk)
83 Sk


Tchibo 250 g grand
6,79 Zl (62 Sk)
75 Sk


Tchibo 250 g family
4,97 Zl (45 Sk)
55 Sk


Jacobs Cronung 250 g
6,49 Zl (62 Sk)
80 Sk


Rama 500 g
3,19 Zl (29 Sk)
34 Sk


Cukor, 1 kg
2,99 Zl (27 Sk)
43 Sk


Kur?a, 1 kg
6,99 Zl (64 Sk)
60 Sk


Kar?, 1 kg
15,99 Zl (146 Sk)
145 Sk


Hov?dzie stehno, 1 kg
19,99 Zl (183 Sk)
200 Sk


Syr Eidam, 1 kg
15,99 Zl (146 Sk)
219 Sk


Syr Gouda, 1 kg
15,99 Zl (146 Sk)
199 Sk


Zemiaky, 1 kg
0,55 Zl (5 Sk)
17 Sk



 


 
 

 
  
 
 
 piatok 23. 7. 2004 | DANIEL VRA?DA &amp;copy 2004 Petit Press. Autorsk? pr?va s? vyhraden? a vykon?va ich vydavate?. Spravodajsk? licencia vyhraden?. 
 
 
 
  
 
 
 
 

 
 
 
   &lt; predch?dzaj?ci ?l?nok  Politol?g: Nez?visl? s? v?zvou pre strany 
 
   nasleduj?ci ?l?nok &gt;  Jos? Barroso je ??fom komisie 
 
   
 
 
 

				 
        
         
 
 
  
 
 
          
           
      
     
       
 
 4 hodiny 
 24h 
 3dni 
 7dn? 
 PLATEN? 
 
       
 
	 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 9?414 
 
Taliban zabil v ?kole v Pakistane 132 det? 4?409 
 
Lek?r vzal ako ?platok 500 eur a f?a?u, u? ho obvinili 3?646 
 
Ha???k sa boj? lietania na klubovom stroji Amuru 3?243 
 
Cynick? obluda: Medzi?asom na opustenom ostrove 3?072 
 
Premi?r Fico ?iline neodpust? dlh za k?rejsk? dedinu 3?045 
 
Cez obec Trhovi?te ?iel 109 km/h, pokutu odmietol zaplati? 2?249 
 
V Brit?nii zadr?ali trojicu podozriv?ch zo zotro?ovania Slov?kov 2?151 
 
?esko a Slovensko si vymenili ?lohy 2?141 
 
Obama podp?e nov? sankcie vo?i Rusku, Kerry hovoril opak 2?106 
	 
 
 
 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 80?198 
 
Taliban zabil v ?kole v Pakistane 132 det? 70?327 
 
Cynick? obluda: Medzi?asom na opustenom ostrove 48?706 
 
?esko a Slovensko si vymenili ?lohy 48?298 
 
Radovan Bondra m??e ?s? na majstrovstv? sveta. Je najmlad?? a najvy??? 39?973 
 
Cez obec Trhovi?te ?iel 109 km/h, pokutu odmietol zaplati? 38?191 
 
Prednostu z detskej nemocnice na Kram?roch prichytili pri bran? ?platku 30?799 
 
Erdogan odk?zal ?nii: Nechajte si svoje n?zory 28?706 
 
Lek?r vzal ako ?platok 500 eur a f?a?u, u? ho obvinili 28?442 
 
Prv? penzie z II. piliera nepote?ia 27?535 
 
 
 
 
 
?noscu zo Sydney pol?cia zastrelila, zomreli aj dvaja rukojemn?ci 189?868 
 
Za?al sa boj o ?eleznicu. ?lt? vlaky RegioJetu boli pln?, ale me?kali 113?518 
 
Putinov ropn? ?ok 99?590 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 80?198 
 
Prednostu z detskej nemocnice na Kram?roch prichytili pri bran? ?platku 79?361 
 
Taliban zabil v ?kole v Pakistane 132 det? 70?327 
 
Rube? st?le pad?, ropa m??e ?s? na 40 dol?rov 51?382 
 
Radovan Bondra m??e ?s? na majstrovstv? sveta. Je najmlad?? a najvy??? 51?022 
 
?esko a Slovensko si vymenili ?lohy 48?767 
 
Cynick? obluda: Medzi?asom na opustenom ostrove 48?706 
 
 
 
 
 
?noscu zo Sydney pol?cia zastrelila, zomreli aj dvaja rukojemn?ci 189?868 
 
?eleznice uk?zali svoje najlep?ie vozne. Porovnajte ich s RegioJetom 159?022 
 
Za?al sa boj o ?eleznicu. ?lt? vlaky RegioJetu boli pln?, ale me?kali 113?518 
 
Putinov ropn? ?ok 99?590 
 
U Kisku sa hl?si exek?tor, Ga?parovi? nechal ?al?? dlh 95?481 
 
Ke? sa Ham??k objav?, zatv?raj? obchody 95?047 
 
Rube? poc?til p?d na dno, Rusko je vo finan?nej kr?ze 80?198 
 
Prednostu z detskej nemocnice na Kram?roch prichytili pri bran? ?platku 79?361 
 
Pre drah? obedy v nemocniciach padli traja riaditelia aj ??f ?radu 77?072 
 
Taliban zabil v ?kole v Pakistane 132 det? 70?327 
 
 
 
 
 
?esko a Slovensko si vymenili ?lohy
 
 
Radovan Bondra m??e ?s? na majstrovstv? sveta. Je najmlad?? a najvy???
 
 
Prv? penzie z II. piliera nepote?ia
 
 
Pre?o sme tak? bohat? na ?kand?ly
 
 
??na buduje nov? ve?k? m?r, chce zabrzdi? p???
 
 
Z?pisn?k: Bradat? Conchita men? Rak?sko
 
 
Glv?? mal doda? zmluvu na pu?ky. Uk?zal dva listy
 
 
Sydney pom??e aj nemeck?m radik?lom
 
 
Premi?r Fico ?iline neodpust? dlh za k?rejsk? dedinu
 
 
Ga?parovi?a ako ?estn?ho ob?ana ?as? Bardejov?anov nechce
 
 
 
     
           
          
 
 
	
	 
	 
		Vybran?
		Najnov?ie
		Naj??tanej?ie
		 
	 
	
 
	 
 Zima je tu ? hor?ci n?poj spr?jemn? chladn? ve?ery 
 Za?ite kr?su beskydskej pr?rody na vlastnej ko?i 
 Vyberte peniaze z pan??ch!Vieme, kde zarobia aj 200-n?sobne viac 
 Ako zvl?dnu? predviano?n? stres bez vir?zy 
 Tepl? obed je d?le?itou s??as?ou d?a. Nevynechajte ho 
 Ani prechladnutie nezastav? doj?enie 
 Objavili sme viano?n? dar?ek, ktor? pote?? ?plne ka?d?ho 
 ?SOB Pois?ov?a je op? Pois?ov?ou roka na Slovensku 
 Nepr?jemn? vir?za: Komplik?cia pre cel? rodinu 
 Ako zvl?dnu? prechladnutie v tehotenstve? 

	 

 
 
	 
 Vernis? v?stavy ART BRUT v Brne do 15.janu?ra 
 Ako zvl?dnu? predviano?n? stres bez vir?zy 
 Tepl? obed je d?le?itou s??as?ou d?a. Nevynechajte ho 
 ?SOB venovala 10 000 eur na skvalitnenie onkologick?ch oper?ci? 
 V?etky inform?cie z v?ho telef?nu na z?p?st? 
 Rozpr?vkov? dar?ek na ka?d? de? od Baletu SND 
 ?tudenti Tren?ianskej univerzity vyhrali s??a? KIA 
 Jeden tablet, ve?a mo?nost? 
 Nov? podstr?nka o ADHD u dospel?ch 
 Ani prechladnutie nezastav? doj?enie 
	 

 
 
 
 
Kv?li pohodlnosti zbyto?ne prich?dzame aj o desiatky eur mesa?ne 10?217 
 
Top hodinky 2014 7?150 
 
Objavili sme viano?n? dar?ek, ktor? pote?? ?plne ka?d?ho 6?593 
 
Vyberte peniaze z pan??ch!Vieme, kde zarobia aj 200-n?sobne viac 6?224 
 
Sauna a prechladnutie - ve?k? nepriatelia 4?295 
 
Bojujete s v?rusom? Toto funguje! 3?253 
 
Success Academy chce pom?c? ?u?om vyhn?? sa pracovn?mu ?radu 2?889 
 
Ten spr?vny parf?m ako dar?ek 2?532 
 
Pr?roda pom??e prekona? prechladnutie 1?559 
 
Robotick? vys?va? ETA Bolero 1?557 
 

 
 

 
  
 


  
  
 


  
         
        
        


       
      
     
    
    
    

     
     
       
 Titulka 
 Spravodajstvo 
 REGI?NY 
 Zahrani?ie 
 Koment?re 
 Ekonomika 
 Kult?ra 
 ?port 
 TV.SME.SK 
 Denn?k 
 Predplatn? 
       
     
 
 
  
 
 
 
  
 
 
 Porad?a 
 Korz?r 
 Auto 
 Tech a veda 
 Zdravie 
 Horoskopy 
 Z?avy 
 Don?ka pizze 
 Denn? menu 
 Recenzie 
 Reality 
 V?no 
 Parf?my 
 Hodinky 
 Hosting 
 Tvorba webov 
 DVD 
 
 
 
 
     

   
   
  

 
 

 kultura.sme.sk 
 Margar?ta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 P?tnick? miesta The Beatles alebo ako ?udia kr??aj? po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lav?na slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 ?iadne retro, poriadny biznis. Takto sa lisuje plat?a 
 
	   
 
 



 
	
	
	 
Kontakty Predplatn? Etick? k?dex Pomoc Mapa str?nky
 
	 
		
		
		
		
		
		
		
		
		
		  
	 
 
 ?al?ie weby skupiny: Prihl?senie do Post.sk ?j Sz? Slovak Spectator 
	Vydavate?stvo Inzercia Osobn? ?daje N?v?tevnos? webu Predajnos? tla?e Petit Academy SME v ?kole © Copyright 1997-2014 Petit Press, a.s. 
	 
	
 

 
 



  





 

 

 









 
 
 
 
 






