
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michaela Herichová
                                        &gt;
                príbehy zo života
                     
                 Dg: učiteľka 

        
            
                                    20.5.2010
            o
            23:37
                        |
            Karma článku:
                12.46
            |
            Prečítané 
            2870-krát
                    
         
     
         
             

                 
                    V poslednom čase - a úplnou náhodou - trávim veľa času v spoločnosti týchto žien a mužov a zisťujem, že je to parádne korenie bežného života. Je to zaujímavá skupina ľudí, ktorí nám vstupujú osudovo do života. Radšej ich pomenujem takto, ako napísať, že je to u nich časom diagnóza. A pritom mám týchto ľudí rada a veľmi si ich vážim.
                 

                 
 internet
   Pamätám si svoju prvú učiteľku. stále ju volám Julinka. Pamätám si aj jednu "sekeru" učila prírodopis a volala sa Sekerová. Na učiteľku ruštinu nikdy nezabudnem a nielen preto, mala 1,90 metra a ostro červený rúž. Aj preto, že plakala s nami, keď sme boli utrápené a smiala sa, keď sme boli šťastné. Triedne učiteľky si budem pamätať vždy. A vždy, ale naozaj vždy pri každej spomienke mám úsmev na tvári. Spájajú sa mi s nimi príjemné spomienky. Teraz už príjemné, milé a úsmevné.   Matikárka mi celý prvý ročník hovorila Svetlana. Vraj tak vyzerám. Na brannej výchove som bola Maličká a nemala som sa smiať tak vehementne, lebo pôsobím deštruktívne. Slovenčinárka mi hovorila Ofélia a aj som zo Shakespeara maturovala.   Môj brat riešil veľkú dilemu asi v tretej triede, či bude chodiť recitovať nových občiankom, alebo chodiť miništrovať, lebo súdružka učiteľka mu povedala, že oboje stíhať nezvládze. Nakoniec spolu stíhali oboje. Moje dieťa mi raz zo školy prinieslo papier od svojej triednej učiteľky, kde som mala podpísať súhlas s jeho fajčením. Nakoniec zriadila na dvore školy fajčiarsky kútik a pravidelne so školníkom čistia popolník. Toto logicky nelogické správanie obdivujem.   Trošku mi ale príde zarážajúci prenos tohto chovania do denného života mimo školu.   Kamarátkin otec je učiteľ. Už ste niekedy viezli v aute učiteľa? Nie? Vyzerá to takto - "Teraz bude stopka, na nej zastavíme. Zastavíme tak, že kolesá budú stáť. Už za chvíľu tá stopka bude. Už je stopka. Tak, zastavíme. Teraz chvíľu budeme stáť. No, už by sme sa mohli pohnúť. Pozrieme ešte, čo ide vľavo-vpravo, áno, nejde nič, teraz sa rozbehneme. Tak, dobre." Išli sme spolu 15 km. Mala som kamenný úsmev na tvári.   Maminá známa, tiež učiteľka, keď rozpráva tretí krát behom desať minút tú istú príhodu, pred koncom zásadne povie:" už to vydržte, nech to dovysvetľujem."   Rodinná priateľka učiteľka na zvonček pri dverách reaguje:"Zvoní, končíme!"   Náš sused, učiteľ fyziky a matematiky vždy vo výťahu povedal, ktoré číslo poschodia treba stlačiť a aj presne navigoval: "ideme na šieste, to znamená číslo šesť, je to druhý gombík zhora."   Moja nebohá teta, tiež učiteľka, rada hostila návštevy kávou a sušienkami. ale zakaždým, keď vošla do miestnosti s táckou a dobrotami v rukách, prikázala: "sadnite si." Pochopila som, že je automatika z práce spojená so vstupom do miestnosti.   Priateľkina mama bola učiteľka. Občas som u nich bola. Občas som u nich jedla. Občas kamarátka pri podávaní jedla musela odbehnúť pre triednu knihu. Záhadou mi je, ako vždy presne vedela, že je to práve synonymum pre šalát, soľ alebo vidličku.   Učitelia sú zvláštna kasta. Ale my, žiaci im to občas vrátime.   Kamarátka - bývalá učiteľka prváčikov - má najkrajší zážitok s malým Jurkom. Jurko ktorý bol z rodiny, kde po pejoratívny výraz sa nešlo ďaleko a to, čo väčšina považuje za nádavku, tam bol daný výraz bežnou súčasťou hovorovej reči. V TV by to vypípali. Tak tento malý prváčik Jurko bol hneď v prvý deň školy tak nadšený pani učiteľkou, že cez prestávku prišiel za ňou na chodbu, kde stála s kolegyňami a cez dva chýbajúce predné zuby zaševelil: "Pani ucitevka, vy ste najjebackovatejsia." Vraj väčšiu pochvalu v živote nedostala.       Áno, syn maturuje, tak preto toto všetko. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Herichová 
                                        
                                            Jana Burclová - Ľudský údel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Herichová 
                                        
                                            Zase mi vzali klasiku.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Herichová 
                                        
                                            Salón výtvarníkov - hrad Červený kameň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Herichová 
                                        
                                            Retrospektíva - Magdaléna Frešo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Herichová 
                                        
                                            Tretie oko - vernisáž
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michaela Herichová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michaela Herichová
            
         
        herichova.blog.sme.sk (rss)
         
                                     
     
        Matka 24 ročného skvelého syna a žena, ktorá neholduje vareniu a domácim prácam.




        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    89
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1655
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            kultúrni turisti
                        
                     
                                     
                        
                            čo ma vytočilo
                        
                     
                                     
                        
                            poradňa
                        
                     
                                     
                        
                            moje divadelné predstavenia
                        
                     
                                     
                        
                            kreatívne drobnôstky
                        
                     
                                     
                        
                            moje recenzie - hotely
                        
                     
                                     
                        
                            príbehy zo života
                        
                     
                                     
                        
                            keď treba pochváliť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Dessire
                                     
                                                                             
                                            100 kardinálnch omylov
                                     
                                                                             
                                            Antická filosofie
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            muzikály
                                     
                                                                             
                                            Gott, Bíla
                                     
                                                                             
                                            INXS
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ladislav Šebák
                                     
                                                                             
                                            Karol Sudor
                                     
                                                                             
                                            Natália Blahová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            O všetkom
                                     
                                                                             
                                            K svadbe
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O darčekoch
                     
                                                         
                       Prvá cesta do Milána. I.
                     
                                                         
                       Naše néniky - Margot.
                     
                                                         
                       No, tak som žiarlivá, hoci nechcem...
                     
                                                         
                       Druhá cesta do Londýna. IV.
                     
                                                         
                       Naše néniky. Boba.
                     
                                                         
                       Asi budem ornitologička.
                     
                                                         
                       Môj bezdomovec.
                     
                                                         
                       Chvála bláznovstva - inteligentná komédia zo súčasnosti.
                     
                                                         
                       Žena si vždy poradí.
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




