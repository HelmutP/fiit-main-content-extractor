

 . 
 . 
  
 Ako chlapec som za letných večerov sedával na balkóne nášho bytu, v jednom z klonovaných panelákov bratislavského predmestia. Pozoroval som fabriku, ako sa v diaľke vyníma na tmavom pozadí Malých Karpát. Dymiace komíny, na ktorých sa večer čo večer zažali červené svetielka. Mojej chlapčenskej duši sa Fabrika javila ako sídlo mystického kultu, ktorého vyznávači magicky ovládajú najtemnejšie prírodné sily. Pri obradoch asi používali hodne kadidla, lebo zápach Fabriky sa nám zažieral do šiat vyvesených na balkóne, do pľúc i do duší.  
   
 
  
 
   
  
   
 Podobnú panorámu som vídal - no táto je z vrcholu novostavby 3veže. 
 (kliknutím sa obrázok sa zväčší) 
 . 
  
  
   
 Vietor prinášal od Fabriky ohavný pach sírouhlíka a sírovodíka (z výroby viskózy a gumárenských chemikálií), zavše okorenený kyslými exhalátmi z výroby umelých hnojív. Kto cestoval do hlavného mesta vlakom, už podľa zápachu cítil, že Bratislava je blízko. My domáci sme mali na výber; severný vietor prinášal arómy Dimitrovky, ak vialo od juhu, celé sídlisko sa pre zmenu ponorilo do ťažkého asfaltového pachu zo Slovnaftu. Keď v takom prostredí vyrastáte, nejako vás poznačí. Malo ma to odpudiť; namiesto toho s pribúdajúcimi rokmi rástla moja zvedavosť. Až natoľko, že sa zo mňa stal študovaný chemik. Bolo len otázkou času, kedy sa stretneme osobne. Fabrika a moja maličkosť. 
 
    
 
 
   
   
  
   
 JV Brána do hlavného areálu z Vajnorskej ulice 
   
 (kliknutím sa obrázok sa zväčší) 
 . 
 Keby som sa narodil o čosi skôr (asi tak o storočie), možno stretnem osobne Alfreda Nobela, keď jazdil do Prešporku dozerať na stavbu Fabriky. Písal sa rok 1873, a nová dynamitka sa stala najväčšou chemickou továrňou v celom vtedajšom Uhorsku. Dodnes je najstaršou chemičkou na Slovensku, a jej dni sú zrejme zrátané. Je to dobre, alebo zle? Je to škoda? Prirodzený vývoj? Lepšie časy pre mesto, nezaťažované exhalátmi? Strata pracovných miest a príležitosti na modernejšiu, sofistikovanejšiu výrobu? Na tieto otázky neviem odpovedať jednou vetou. Namiesto nej prinášam zopár faktov z histórie a súčasnosti, zopár úvah, doplnené množstvom fotografií. 
 
   
   
  
 (To je on, Alfred Nobel osobne, najbohatší chemik všetkých čias. Môj svetlý vzor:-) 
 . 
 Rozhodnutie Alfreda Nobela, postaviť ďalšiu fabriku dynamitového impéria práve v Prešporku, bolo strategické – mesto leží v pomyslenom strede medzi Horným Uhorskom (Slovenskom), Viedňou a Budapešťou, navyše na Dunaji. V Karpatoch je množstvo baní, stavala sa železnica, razili sa prvé tunely, spotreba dynamitu rástla. A rozrastala sa aj Fabrika, kvôli väčšej sebestačnosti v surovinách. Viaceré stavby z Nobelových čias stoja dodnes, i keď zväčša ako nefunkčné zrúcaniny. Dominantné sú najmä dve vodárenské veže – jedna chátrajúca v areáli továrne, druhá čerstvo rekonštruovaná vedľa hlavnej administratívnej budovy na Nobelovej ulici. 
   
  
   
 
   
  
 Východná vodárenská veža, v pozadí Karpaty nad Račou 
 (kliknutím sa obrázok sa zväčší) 
  
  
 Obe vodárenské veže v pozadí fabrického areálu. Po zväčšení ich uvidíte lepšie. 
 
  (kliknutím sa obrázok sa zväčší) 
 
 . 
   
 V prvej svetovej vojne sa pridala aj vojenská výroba (podľa dostupných zdrojov sa tu vyrábala napr. kyselina pikrová, chemickým názvom 2,4,6-trinitrofenol, známejšia pod obchodným menom Ekrazit ). Po vzniku prvej Československej republiky bola výroba výbušnín v Bratislave zastavená, výroba prenesená do Čiech, a fabrika len prežívala. (Vďaka tomu však nebola v 2. svetovej vojne tak dôkladne zbombardovaná, ako bratislavská rafinéria Apollo). Hneď po vojne Benešova vláda znárodnila priemysel. Národným správcom bratislavskej firmy Dynamit-Nobel sa stal Miloš Marko, po desťročia najvýznamnejší slovenský chemik. Po komunistickom prevrate 1948 Marko nevyhovoval, fabrika dostala nových pánov. Nevyhovovalo ani meno; prekrstili ju na Chemické závody Juraja Dimitrova (Prečo práve po bulharskom komunistovi, ťažko povedať – azda pre rovnakú začiatočnú slabiku Dynamitky a Dimitrova, lepšie vysvetlenie nenachádzam.) Sochu Dimitrova pri fabrickej bráne (smerom od Rače) som ešte zažil, keď som tade chodil do roboty. 
  
   
 Komunistickú prestavu pokroku zhmotňoval ťažký priemysel, a tak vo fabrike vyrástli obrovské haly na výrobu umelých hnojív, najmä superfosfátu a GVH (granulované viaczložkové hnojivo). V podstate išlo o využitie odpadových kyselín z výbušninárskej výroby na rozklad prírodných fosfátov. Vznikajúce plyny, obsahujúce trebárs fluorovodík či oxidy dusíka, sa púšťali rovno do vzduchu; pri nepriaznivom počasí jedovatý dym sadal dole, a cestou cez fabrický areál vám trhalo pľúca. Haly výroby hnojív dnes stoja prázdne, v stave rozpadu. Zmenili sa na surrealistické kulisy pre akčné filmy (neviem, či je historka pravdivá, ale vraj si ich svojho času prenajali zahraniční filmári). Posúďte túto kombináciu strašidelnosti a ohavnosti sami: 
 
  
 
   
  
   
  
  
  
 (kliknutím sa obrázky zväčšia) 
 . 
 
  Čo tu vôbec zostalo? Za bránou Fabriky (dnes zvanej Istrochem) sa rozprestierajú kilometre štvorcové chátrajúcich budov a nevyužitých plôch. Prázdne zrúcaniny, sem-tam v nejakom zachovalejšom objekte živoria firmy v podnájme. Komu by napadlo, že na sklonku socíka tu pracovalo vyše päťtisíc ľudí? Dnes je zamestnancov fabriky niečo cez stovku. Výrobňa kyseliny sírovej, za socíka vcelku moderná, bola na pokyn novodobých privatizérov rozrezaná autogénom a rozobratá do tla; ako pozostatok niekdajšej haldy síry ešte kde-tu nájdete žltú hrudku, ako kdesi na stráňach Etny či Vezuvu. Kyselinu sírovú potrebnú pri výrobe nitroglycerínu odvtedy musia dovážať. Niekto to geniálne vymyslel. Pár spomienok na niekdajšiu výrobňu kyseliny prinášajú archívne fotky z roku 1990: 
 
   
 
  
   
 
   
  
 Tu sa vyrábala kyselina sírová 
 (obrázky sa nedajú zväčšiť) 
 . 
   
  
   
 Treba uznať, že za komunistov sa budovali aj o sofistikovanejšie technológie. Napríklad smerom k Vajnorskej ulici vyrástol 2. cech, areál na výrobu pesticídov (čiže agrochemikálií, laicky povedané, postrekov na ochranu rastlín). Pamätá si niekto na organofosfátový insekticíd Metation, ohavne páchnúcu (ale účinnú!) hnedú brečku? Jeho výroba bola jedna z najotrasnejších (kým otrávite škodlivý hmyz, asi bolo treba otráviť nejakých robotníkov - a to v časoch, keď mala robotnícka trieda údajne vedúcu úlohu v spoločnosti!). Prevádzku odstavili už pred pádom socíka, bola v havarijnom stave a odbyt na ruské trhy viazol. V 90tych rokoch sa tá istá účinná látka (fenitrotion) dovážala pre potreby našich poľnohospodárov z Japonska. Na jednej strane dobre, že ekologicky katastrofálnu prevádzku zavreli, ale vlastne to bolo tiché priznanie porážky. Nemali sme na to. Dlhé roky sa neinvestovalo do modernizácii, ba ani do udržania existujúcich technológií v pojazdnom stave. Agrochemický cech ešte zotrvačnosťou produkoval niekoľko druhov herbicídov (Zeazin, Burex, Aminex), ale postupne všetko zakapalo. Z výrobných objetkov sú pusté zrúcaniny: 
 
    
 
 
  
   
  
 
  Zrúcaniny prevádzky Metationu. V pozadí východná vodárenská veža, za ňou Krasňany a Rača. 
 (kliknutím sa obrázok sa zväčší) 
 . 
 
   
 Na 5. cechu (medzi železničným násypom smerom na stanicu Nové mesto a Odborárskou ulicou) zasa vyrástla výroba chemických špecialít – gumárenské chemikálie, antioxidanty, rôzne medziprodukty. Na sklonku socíka ma raz šéf poslal na tamojšiu prevádzku Sulfenaxu (urýchľovač vulkanizácie kaučuku), nech odtiaľ donesiem za bandasku chlórnanu sodného. Od smradu som sa takmer povracal už pred budovou. V samotnej hale vládlo strašidelné prítmie, z potrubí kvapkali podozrivé tekutiny, pod nohami roztrhané vrecia žieravín a jamy dobré na zlomenie nohy. Hluk čerpadiel znemožňoval normálnu komunikáciu. Oceľové konštrukcie obalené kôrou hrdze a chemikálií, a všetkému vládol ozrutný Golem hlavného chemického reaktora, uväznený v oceľových okovách. Dvesto stupňov, dvesto atmosfér, a vnútri anilín, síra a tony sírouhlíka, ktorý by sa pri najmenšom úniku z reaktora okamžite zapálil. Dantovo inferno, či sopka pripravená k výbuchu? Chlapi na prevádzke vyzerali všetci viac mŕtvi ako živí, už v tých časoch brali trojnásobok inžnierskeho platu. Opilec, prepustený väzeň, dement či obyčajný otec rodiny, čo chcel rýchlo zarobiť na byt a auto, všetko jedno - brali sem každého, čo tu bol ochotný robiť. Keď sa ma neskôr šéf pýtal na dojmy z prevádzky, sponátnne zo mňa vyhŕklo: „Tak nejako som si predstavoval peklo.“ Šéf sa smial. Ale podobne to vyzeralo aj inde. Socialistický priemysel v realite. Neviem, koľkí tam nechali zdravie, a niekoľkí aj život. Z tých najhorších pracovísk zvykli ľudí so zlými pečeňovými testami preradiť inde. Vraj. Každopádne, 5. cech už neexistuje, väčšina budov je zbúraná, pozemok predaný iným firmám. A ozrutný železobetónový bunker na Odborárskej ulici (postavený ako kryt pre obyvateľov priľahlej štvrte, pre prípad výbuchu, požiaru či chemického zamorenia) prerobili na svojráznu reštauráciu Sfinga. 
   
 
   
 Najznámejším pojmom pre Bratislavčanov však býval prvý cech, výroba viskózy, takzvaný Závod mieru. Areál stál na opačnej strane Vajnorskej ulice ako hlavná časť Fabriky (pri ceste z mesta vpravo), a z diaľky sa dal identifikovať podľa zvlášť hrubého fabrického komína. Onen tučný komín ročne vypustil do bratislavského povetria najmenej za vagón prudko jedovatého sírouhlíka. Po páde socíka bolo otázkou času, kedy túto pohromu zrušia. Dnes je v jednej z hlavných hál niekdajšieho Závodu mieru veľkopredajňa nábytku (Meganábytok), ozrutný komín ešte stojí, ale jeho jedovatý dych patrí minulosti. Komín dnes slúži mobilným operátorom, ktorí naň umiestnili svoje antény. A je zďaleka vidteľnou dominantou celého industriálneho predmestia; konkurujú mu len komíny elektrární ZSE na Turbínovej. 

  
 1.cech -Závod mieru 
 (kliknutím sa obrázok zväčší) 
 . 
 Len dve výroby v areáli továrne ešte fungujú. Priemyslené trhaviny, a takzvaný nový Sulfenax - prevádzka gumárenských chemikálií postavená na samom konci socíka ako náhrada za ono Dantovo inferno, o návšteve ktorého som referoval vyššie. Nová prevádzka funguje vo východnej časti areálu, a keby sa dala naložiť na kolieska a odviezť, iste by bola dávno v Dusle Šaľa. Táto výroba je totiž sliepka znášajúca zlaté vajcia; gumárenské urýchľovače vulkanizácie idú vo svete stále na odbyt. Prevádzka Sulfenaxu je jediná fungujúca a súčasne viditeľná chemická výroba (výroba výbušnín je skvelo maskovaná a ukrytá, ako uvidíte v 2. časti tohoto článku). Ako vidno z fotiek, táto časť industriálnej architektry je ešte v porovnaní so zvyškom fabriky celkom fotogenická: 
 
 
  
 
  
   
  
   
  
 Tri pohľady na prevádzku gumárenských chemikálií (Sulfenax) 
 (kliknutím sa obrázky zväčšia) 
 . 
 Cech výroby priemyslených trhavín je vlastné a najpôvodnejšie jadro továrne. Ale o tom, i o súčasnosti dokonávajúcej fabriky v obkľúčení rozvíjajúceho sa mesta viac v pokračovaní článku (už čoskoro!). 
 . 
 Súvisiace linky: 
 http://bratislava.sme.sk/c/4163749/opusteny-areal-je-asocialne-uzemie.html 
 . 
  
   
 
   
   
  

