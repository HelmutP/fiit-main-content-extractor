

 
Darček pre ovečku, ktorá sa volá.....  
 
 
Bolo to veľmi dávno, v čase, keď na zemi žili poslušné ovečky.... 
 
 
Samozrejme, že medzi ne patrila aj táto ovečka, 
 
 
  
 
 
ktorá sa volá presne tak ako Ty... 
 
 
Bola veselá, usmievala sa, mala čo jesť, čo piť a hlavne mala v srdci RADOSŤ...  
 
Raz sa však stalo, že sa zašpinila. Špinavý prach jej napadal do očí, a ona sa stratila. 
 
Nevedela nájsť cestu domov. 
 
 
 Bola smutná. A blúdila.  
 

 
Namiesto radosti, nosila vo svojom srdci strach a samotu. 
 
 
 
 
 
V noci nemohla spať, lebo sa bála, že jej niekto ublíži.Myslela na svoju krajinu, na dobrého pastiera,ktorý ju ochránil pred vlkmi...  
 
 
Nieže by „teraz“ nemala pastierov, mala ich,ale ten prvý, ktorého stretla ju zatvoril do ohrádky 
 
 
a vôbec jej nedával jesť... 
 
 

 
 
 
 
 
Bola hladná a smädná a zdalo sa jej, že nikto ju nemá rád...  
 
 
Ten druhý, jej nedal ani domček a nechal ju bývať iba na lúke... 
 
 
A ona veľmi túžila po teple domova... 
 
Po niekom, kto by sa jej prihovoril... 
 
 
 
 
 
 Často myslela na to, ako jej prvý Pastier pripomínal,že „Prach hriechu“  jej môže ublížiť... 
 
 
Najskôr mu aj verila, aj miesto, kde bol ten prach obchádzala, ale potom...?  
 
 
Raz to nevydržala, rozbehla sa... a dobre sa v ňom vyváľala...  Odvtedy takto blúdi svetom... 
 
 
A nielen ona, ale aj ostatné ovečky... 
 
 
 
 
No predsa vo svojom srdci akosi tuší, že jej Pastier niečo vymyslí,nemôže ju predsa nechať len tak...  
 

 
 

 
 
Keď raz v noci spala v košiari u svojho tretieho pastiera, 
 
 
zazdalo sa jej, že žiari celé nebo 
 
 
 
 
 
A vlastne sa jej to ani nezdalo.... 
 
 
Bola to skutočnosť. Práve nad im salašom letel anjel a volal: 
 
 

 
 
 „Poďte do Betlehema, tam sa narodil Pastier pastierov, 
 
Ježiš – dieťa, ktoré Vám prinieslo Domov, pokoj, lásku, všetko to, čo ste stratili....“  
 
 
 
 
 
                   Ovečka sa pozrela na svojho pastiera. 
 
 
Práve si bral do rúk kapsu. 
 
 
Žmurkol na ňu a povedal : 
 
 
 „Ak chceš, tak poď“   
 

 

 
 
 
 
 
Rýchlo odtrhla prvý kvet, ktorý rástol pri cestičkea utekala za Anjelom i pastierom...  
 
 
A Anjel? 
 
 
Ten ich priviedol do Betlehema... 
 
 
  
 
 

 
 
Tam sa narodila Láska.... 
 
 
Tam sa oslavovali 
 
 
PRVÉ,   PRAVÉ,   NAOZAJSTNÉVIANOCE...  
 
 
Tam ovečka, našla svojho pastiera...  
 
 

 
 
Tam svojho Pastiera nájdeš aj ty... 
 
 
Tam sa ti vráti všetko, čo si stratil.... 
 
 
Dôstojnosť, dobré meno, láska, radosť, pokoj... 
 
 

 
 
 A kde je Betlehem? 
 
 
Je v tvojom srdci... 
 
 
Je tam, kde sa túžiš vrátiť k prameňu lásky, pravdy a pokoja, 
 
 
tam, kde túžiš objaviť hodnotu svojho človečenstva i hodnotu blížnych, 
 
 
tam, kde sa rozhodneš, že sa prestaneš váľať v prachu hriechu 
 
a budeš žiť blízko pri Kristovi... 
 
 

 
 

 
 
Nemusíš byť veriacim na to, aby si poznal tajomstvo Vianoc, 
 
 
ale musíš byť človekom,aby si žasol nad zázrakom Narodenia...  
 
 

 
 
Tak požehnaný čas na ceste do Betlehema, 
 
 
milé blogujúce ovečky :-)    
 
 
 
 

 
 
 
 
 
 
 

