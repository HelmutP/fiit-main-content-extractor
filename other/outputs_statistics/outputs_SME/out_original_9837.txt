
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ján Maršálek
                                        &gt;
                Drobnica
                     
                 KDH mohlo... 

        
            
                                    29.6.2010
            o
            13:40
                        |
            Karma článku:
                3.88
            |
            Prečítané 
            532-krát
                    
         
     
         
             

                 
                    Kresťansko-demokratické hnutie mohlo vstúpiť do dvojkoalície, v ktorej by zaujalo silnú pozíciu jedného z dvoch partnerov. Mohlo mať polovičný podiel na vládnutí a v nasledujúcom volebnom období mohlo významnou mierou ovplyvňovať dianie v našom štáte.
                 

                 Vedenie KDH sa však rozhodlo inak. KDH bude z hľadiska politického vplyvu najslabším článkom štvorkoalície, v ktorej majú prevahu liberálne sily. KDH urobilo pri rokovaniach o vytvorení novej vlády najviac ústupkov. Dostalo sa nezávideniahodnej situácie...   Vedenie strany to zdôvodňuje potrebou zmeny. Ide vraj o hodnotovú orientáciu. Pochybujem - lebo účasť v (akejkoľvek) dvojkoalícii dáva politickej strane omnoho väčšie možnosti presadiť svoj program ako účasť v nevyrovnanom a nevyváženom účelovom zlepenci viacerých strán.   Nie je to prehra pravdy, ale môže to byť prehra tohto hnutia. Po nasledujúcich voľbách budú možno predstavitelia a priaznivci strany veľmi smutní. Nie, neželám Kresťansko-demokratickému hnutiu zánik, želám mu múdrejších a statočnejších vodcov.   (Poznámka: glosovaniu spoločenského diania v širších súvislostiach sa venujem na stránke www.priestornet.com) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Maršálek 
                                        
                                            Máme čas na čítanie?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Maršálek 
                                        
                                            Ako hospodáriť s peniazmi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Maršálek 
                                        
                                            Cesta k vnútornej rovnováhe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Maršálek 
                                        
                                            Stopy v priestore
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ján Maršálek 
                                        
                                            Mánia premenovávania
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ján Maršálek
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ján Maršálek
            
         
        marsalek.blog.sme.sk (rss)
         
                                     
     
        Som, teda myslím...
Píšem a publikujem. Okrem iného redigujem internetový občasník PriestorNet.com



        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    170
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    613
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisník
                        
                     
                                     
                        
                            Zamyslenie
                        
                     
                                     
                        
                            Riport
                        
                     
                                     
                        
                            Metanoia
                        
                     
                                     
                        
                            Drobnica
                        
                     
                                     
                        
                            List z domova
                        
                     
                                     
                        
                            Jazykové okno
                        
                     
                                     
                        
                            Literárny klub
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Herci a komedianti
                                     
                                                                             
                                            Rozvod dobra a zla
                                     
                                                                             
                                            Sloboda a Hemingway
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Moja filozofia
                                     
                                                                             
                                            Moja literatúra
                                     
                                                                             
                                            Môj priestor
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            PriestorNet :: internetový občasník
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




