
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ladislav Piaček
                                        &gt;
                Nezaradené
                     
                 Prečo nemoralizovať?! 

        
            
                                    15.3.2010
            o
            1:37
                        (upravené
                26.1.2011
                o
                0:17)
                        |
            Karma článku:
                1.47
            |
            Prečítané 
            227-krát
                    
         
     
         
             

                 
                    Prečítal som si článok na jednom blogu a diskusiu k nemu. Stará otvorená téma: schválenie/zakázanie aborcií. A v diskusii bol niektorý diskutér obvinený z moralizovania. Právom. Myslím ale, že obvinený sa nemá za čo hanbiť. Nech sa len postaví na mieste, kde snáď pevne stojí a povie: "Áno. Moralizujem. A budem ďalej moralizovať, lebo to, čo sa deje, je zlé a musí to prestať!" Ak moralizátor neblúzni a vie o čom hovorí, nech povie viac. Bolo by nerozumné nevypočuť si varovanie pred nejakým nebezpečenstvom. Tak prečo nám kypí krv, keď otvorí moralizátor ústa alebo vynesie transparent?
                 

                 Nechcem sa púšťať do snahy presne definovať moralizátora a moralizovanie. Bolo by to zbytočné, lebo v týchto dvoch pojmoch sú teraz relevantné len dve veci: (1) moralizovanie obsahuje nejaký príkaz (resp. jeho negatívnu formu - zákaz) a (2) príkaz je podávaný v jednoduchej nekritickej podobe.   Ad (1):   Je bežné, že sa rozčuľujeme, keď nám niekto niečo prikazuje: "Nerob to!" Príkaz sa ale nestratí, keď zmeníme jeho vonkajšia formu na menej iritatívnu: "Prosím, nerobte to." Diplomacia a slušnosť vo vyjadrovaní je na koniec tiež "len" príkazom. Vyžaduje ju náš partner v diskusii alebo protokol. Príkaz je "len" príkazom preto, lebo stojí na vode - na našom súhlase s príkazom, čiže na autonómii vôle. Príkaz nie je kamenným faktom, ktorý by nás do niečoho nútil. Nasledovať ho môžem, ale aj nemusím. Akoby sa mi príkaz zastavil pred nosom, lebo ďalej nedosiahne a musí prosiť. Väčšiu silu nemá.   Ak ktosi nedokáže odmietnuť príkaz (resp. prosbu), je tu problém slabosti vôle, ktorým sa tu nejdem zaoberať. Ak ma ktosi vydiera, má určité donucovacie prostriedky, no autonómia vôle sa tak neruší, stále môžem príkaz odmietnuť, lebo nakoniec je len prosbou.   Moralizátor prikazuje ("Robte X a nerobte Y!"), ale to nás nemusí obťažovať, pokiaľ vonkajšia forma neprekračuje medze slušnosti a nepľuje nám do tváre, či nekričí do ucha. To si od neho skúsime vyprosiť zas my.       Ad (2):   Moralizátor je v prvej bojovej línii, keď je vnímané nejaké porušenie mravov. Nemá čas rozmýšľať, keď horí. Reaguje promptne, podáva nám len základný popis problému: "Musíte viac robiť X a prestaňte úplne robiť Y." Je na mieste spýtať sa, prečo by sme mali X a nemali Y, pretože zmysel prosby-príkazu nemusí byť zjavný. Pýtaním sa na dôvody tlačíme moralizátora, aby rozvinul svoju pozíciu na pevnejšiu pôdu a začal kriticky argumentovať.   Ak moralizátorovi padne sánka a povie, že nepozná žiadne dôvody, že len cíti, že X musí byť a Y nemôže byť, tak diskusia končí. Napriek tomu, ale bolo povedané niečo relevantné: moralizátor je dotknutý dôsledkami našich rozhodnutí, a nezáleží na tom, či je dotknutý priamo (ba až fyzicky), alebo "len" cíti nejaké neurčité nebezpečie. Fakt, že bola vyjadrená prosba, rozširuje repertoár našich možných volieb – stále môžeme a nemusíme prosbe vyhovieť.   Prechod z moralizátorskej pozície na pôdu kritickej argumentácie môže byť veľmi pomalý, pretože mnohé argumenty sa odvolávajú len na iné intuitívne zrejmé princípy. To môže byť dostatočné, ak sme sa dopracovali k intuíciám, ktoré bez konfliktov zdieľame. Domnievam sa, ba až verím tomu, že kritická argumentácia v prospech konkrétnych morálnych princípov môže byť dostatočne pevne založená na faktoch a logike. Na logike preto, lebo argumentácia musí mať určitú formu, vďaka ktorej rozumieme vlastným vetám. Na faktoch preto, lebo naše rozhodnutia (vedomé činy) určitým spôsobom menia svet a svet zas určitým spôsobom pôsobí na nás.   Som naivný, lebo verím, že kritické myslenie je morálne relevantné? Je pravda, že aj keby som mal poznatky Vševedúceho, mohlo by mi byť ľudstvo ľahostajné. Môžem si povedať na to poznanie: „No a čo? Nič nemusím. Nech trpia akokoľvek oni, alebo ja, je mi to jedno.“ Tejto pozícii sa už ani nedá nič vytknúť, lebo dotyčný vie všetko. Ak dodáme, že taký človek je bezcitný, už to nemá žiadnu relevanciu pre jeho rozhodnutie, lebo je to len ďalší fakt, ktorý už zvážil. Diskusiu je rozumné ukončiť.   Ďalším nedostatkom kritického morálneho myslenia je, že nie je zárukou šťastia. Aj keby som bol Vševedúci, viem len možné scenáre, ktoré vzniknú našimi rozhodnutiami. Ten pre celok najprijateľnejší scenár môže byť ale ten, kde budem ja neskutočne trpieť. Ak aj bude niektorý scenár pre mňa prijateľný (šťastný), mám len obmedzené prostriedky ovplyvniť ostatných, aby konali v môj prospech. Napriek týmto nepríjemným možnostiam ale pravdepodobnosť napovedá, že to umravnenie najprijateľnejšie pre celok, bude asi dobré aj pre mňa. Nebudem teda radšej kradnúť, klamať a zabíjať.   Pohodlnosť kritické myslenie odmieta. Nie je nad to, si kdesi sadnúť a čakať, čo dostanem. A keď na niečo dostanem chuť, tak si to vypýtam. Pivo a sviečkovú! Každý deň! Päťkrát denne! A s takým transparentom pôjdem pred tie správne inštitúcie. Dobrí ľudia dožičia...   (Autorom metódy skúmania a rozvíjania morálky, ku ktorej som sa týmto príspevkom snažil priblížiť je R. M. Hare.) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Piaček 
                                        
                                            O nerozprávaní
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Piaček 
                                        
                                            Neskôr
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Piaček 
                                        
                                            Hrabať sa v rohu či za roh?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Piaček 
                                        
                                            Ako ísť (s)pokojne spať?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Piaček 
                                        
                                            Svrbenie, omínanie, dráždenie, šteklenie... - óda na určitý pocit
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ladislav Piaček
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ladislav Piaček
            
         
        piacek.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    286
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




