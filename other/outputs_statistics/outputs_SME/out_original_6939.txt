
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rudo Hvizdos
                                        &gt;
                Nezaradené
                     
                 Ako sa volí na Bali 

        
            
                                    21.5.2010
            o
            8:33
                        (upravené
                21.5.2010
                o
                8:46)
                        |
            Karma článku:
                4.55
            |
            Prečítané 
            509-krát
                    
         
     
         
             

                 
                    Sedím v aute, ktoré sa hopsavým spôsobom vyrovnáva s kostrbatými balijskými cestami a neutíchajúcim hlučným klaksónom si zabezpečuje priestor pred sebou. Zvolebnieva sa a tak okrem zvyčajných motorkárov, kohútov, psov, prasiat, sliepok a hrajúcich sa detí možno vzhliadnuť množstvo politickej reklamy a la Bali style.  
   
                

                 
Balijský pohrebrudo hvizdos
   Televízne to majú domáci politici ešte ľahšie ako na Slovensku. Televízia na jednej strane rovnako štátna a provládna, je stále znakom vyššej vrstvy a majetkom každej siedmej rodiny a tak sa do štúdií nechodí ani na suché nekonfrontačné monológy. Bola by to drahá strata času.   Ruky Balijcov zamestnáva veľa vecí. Hladkanie kohútov pred ich zápasom, riadenie motoriek, rybárske siete, fajčenie a hlavne výroba rôzneho druhu obetín pre nekompromisných bohov. Nikdy v nich však nevidieť noviny a dokonca aj oheň radšej zapaľujú kokosovou kôrou. Politici sa teda vôbec netrápia s nejakými tlačovými posolstvami, nikto by ich aj tak nečítal.   Na Bali sa viac tancuje, smeje, polihuje a hlava, hlavne u žien, je okrem trápenia akú rybu spraviť dnes na večeru, zaťažená často viac ako 20 kilami nákladu. Politika sa tam už nezmestí. Pluralitný systém, tu ale predsa len funguje.   Ľudia si však nevyberajú podľa programu. Ten voličov príliš nezaujíma a ani nevedia, že možno dajaký existuje. Úprimne sám neviem, či niečo také na Bali vôbec majú, pretože všetky oficiálne rozhodnutia pôsobia viac hekticky ako plánovane a zásadne uprednostňujú rodiny a známich, trochu ako na Slovensku.   Ak chce niekto na Bali politicky uspieť, musí dobre vizerať a to hlavne na fotke. Krásna farebná fotografická koláž s peknou tvárou je prvý krok. Na plagáte kandidát nikdy nie je sám ale v páre. V našej samosprávnej časti sme mali štyri strany, vždy po dvoh kandidátoch. Jeden zvyčajne ten starší s fúzami a druhý na hladko oholený. Fúzatý reprezentuje skúsenosť, mladší silu a budúcnosť. Tak my aspoň povedali našinci. Ženy političky akokoľvek pekné na plagátoch nebývajú. Okrem iného im nerastú fúzy skúsenosti, tak sú bilbordovo diskriminované.  Každá strana má podobne ako u nás volebné číslo, ktoré ľudia napíšu na papier a hodia do krabice. Číslo si okrem výzoru kandidátov a úrovovne ich fotky, voliči vyberajú aj podľa množstva plagátov. Viac posterov v drevených rámčekoch priklicovaných všade, kde to len ide, znamená viac peňazí a väčšiu šancu na úspech. Balijci totiž nechcú voliť nikoho, kto prehrá. Byť voličom lúzrov, je ako byť ním sám a tak pospolitý ľud viac ako program, zaujíma reálna šanca strany na víťazstvo.   Šťastie, že my Slováci vieme aj prehrávať, lebo mať vo vláde 100 percentnú väčšinu Smeru by nás asi zabilo. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Ficovo registrované partnerstvo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Nevedomosť je sladká
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Kočpédia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Novinárske hrdinstvá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Poslanecká anarchia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rudo Hvizdos
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rudo Hvizdos
            
         
        hvizdos.blog.sme.sk (rss)
         
                                     
     
        ..infidel, co ma rad slobodu, racionalnost a zmysel pre humor..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    35
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    980
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nová tvár zneužívania psychiatrie v trestnom konaní
                     
                                                         
                       Tereškovová už zasa sklamala
                     
                                                         
                       Naozaj potrebujeme viac policajtov, pán minister?
                     
                                                         
                       Dejiny katolíckej cirkvi a neomylnosti podľa Hansa Künga
                     
                                                         
                       SPP: Klasický tunel v podaní muža s kolou
                     
                                                         
                       Manuál mladého konšpirátora
                     
                                                         
                       SaS opúšťajú len tí členovia, ktorí tam nepatrili
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                                                         
                       Ficoidi útočia - ďalšia územie obsadené, ďalšia nemocnica dobytá
                     
                                                         
                       Epitaf na hrobe starej pravice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




