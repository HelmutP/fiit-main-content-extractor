
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Kmecová
                                        &gt;
                Nezaradené
                     
                 Espresso House - www.espressohouse.se 

        
            
                                    5.5.2010
            o
            8:00
                        (upravené
                4.5.2010
                o
                23:27)
                        |
            Karma článku:
                3.47
            |
            Prečítané 
            669-krát
                    
         
     
         
             

                 
                    V jednom z predchádzajúcich blogov som opisovala švédsku sieť kaviarní Coffee House by George. Ďalšou v tejto kategórii je Espresso House, kde si opäť môžete pochutnať na výbornom latte, capuccino či obyčajnom espresse. Vôňa čerstvo praženej kávy sa mieša s vôňou chutných koláčikov - pastries, ktorým sa veľakrát nedá odolať. Ak predsa odoláte, tak sú tu naporúdzi ešte tzv. malé lákavé drobnosti ku káve alebo k čaju ako malá tabuľka čokolády alebo môj obľúbený mandľový keks. Ak ste veľmi hladní a káva nie je to, po čom Vaše chuťové poháriky túžia, tak práve tu si môžete vybrať z rôznych druhov chutných, čerstvých šalátov ako kurací šalát s bulgurom alebo lososom a tiež chutné a chrumkavé toasty.
                 

                 Interiéroví dizajnéri sa ako  vždy  pohrali s detailmi, ktoré tvoria interiér kaviarní. Znova je to spleť rôznych nápadov a kombinácií. Každá kaviareň z tejto siete je iná. Veľké, niekedy až nadrozmerné lampy vysielajú svoje SOS  a lákajú ľudí, aby  aspoň na chvíľu spomalili a posedeli si pri voňavej kávičke s priateľmi. Je to však aj vhodné miesto pre seriózne pracovné stretnutie. A ten, kto si myslí, že bez počítača ani na krok, tak je v tej správnej kaviarni. Majitelia tejto siete sa snažia držať krok s technologickým pokrokom a v každej kaviarni ponúkajú bezplatné pripojenie na internet. Takže smelo môžete surfovať. A toto všetko robí Espresso House treťou najrýchlejšie sa rozrastajúcou sieťou kaviarní v Európe.   Wayne´s Coffee - www.waynescoffee.se      Ďalšia švédska sieť kaviarní je Wayne´s Coffee. Prvá kaviareň bola otvorená v roku 1994 na Kungsgatan 14 v Štokholme. Veľmi rýchlo sa stala veľmi populárnym miestom na stretnutia, či už priateľské alebo pracovné. Väčšina kaviarní tejto siete umožňuje bezplatné pripojenie na internet. Ich interiér je rovnako veľmi  zaujímavý ako i v kaviarňach predchádzajúcej siete a snaží sa takisto aktívne priťahovať zákazníkov.   Káva Latte, mafiny a sendviče v tejto sieti odštartovali novú éru švédskej kaviarenskej kultúry. Počet kaviarní Wayne´s Coffee  neustále rastie a už sa stal jednou z popredných sietí vo Švédsku. Svoje zastúpenie má aj vo Fínsku, Nórsku, Poľsku, Dánsku a Rusku.   Samozrejme i tu zohráva hlavnú úlohu káva a vôňa kávy. Káva sa podáva výhradne 100% certifikovaná  Rainforest Alliance . Pripravia Vám ju do typických modrých hrnčekov so značkou Wayne´s Coffee a je na výber i veľkosť kávy malá, stredná, veľká. Tieto hrnčeky sa dajú i zakúpiť ako suvenír spolu s termoskami na kávu (mugs) ako aj s balenou značkovou kávou.   Okrem kávy a sladkostí sa skoro vo všetkých švédskych kaviarňach podávajú jedlá, ktoré sú jednoduché na prípravu a vo Švédsku obľúbené. Takže v čase obeda  sú tieto kaviarne prevažne preplnené.   Taktiež Wayne´s Coffee okrem rôznych druhov káv a pastries ponúka šaláty, cestoviny, sendviče, wraps - placka, v ktorom je zabalená zelenina, kúsky mäsa alebo syra . K tomu čerstvý, vychladený  pomarančový džús, ovocné  alebo jogurtové smoothie,  ľadový čaj alebo veľmi zaujímavý Apple crush (zloženie - čaj Chai, jablková esencia, napenené sójové mlieko a drvené ľadové kocky - crush).   Ak by ste sa rozhodli pobudnúť vo Švédsku dlhšie a táto sieť sa vám zapáčila, môžete sa stať členom siete Wayne´s Coffe. Dostanete ako keby creditnú kartu, ktorú si môžete finančne ľubovoľne dobíjať, káva je o niečo lacnejšia a zároveň ako VIP zákazník budete vedieť všetky novinky z kuchyne tejto kaviarne ako prví .   To je Wayne´s Coffee. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Berns Salonger – Stockholm
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Cafe Foam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Coffee House by George
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Kmecová 
                                        
                                            Prvý pozdrav zo Štokholmu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Kmecová
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Kmecová
            
         
        kmecova.blog.sme.sk (rss)
         
                                     
     
        Slovenka momentálne žijúca v Škandinávií. Zaujímam sa o interiérový dizajn, bývanie, životný štýl v tejto časti Európy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    720
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




