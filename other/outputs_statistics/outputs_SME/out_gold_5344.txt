

   
 tá učí veriť v nemožné, 
 predáva nádej za drobné. 
   
 Nádej falošnú a márnivú, 
 pozri, 
 vzdušný zámok, 
 šepká, 
 Uver mu! 
   
 Taká je myseľ naivná, 
 vraj správny smer , to je priorita. 
 Vyberieš sa po jej cestách, 
 zrazu, 
 veď tá cesta nemá cieľ. 
   
 Je diagnózou, 
 bez predpisu, bez lieku. 
   
 Čo však rany, 
 po chorobe s názvom naivita 
 zahojí? 
   

