

 Včera v noci nám bolo horúco inak, ako inokedy. 
 Ja viem, občas musia byť také okamihy pravdy: máš na sebe všetko a predsa sa cítiš byť nahý. 
 Horúčava pobúrenia a vzdoru. A hneď za tým zimomriavky z toho, kto a čo ti to hovorí. 
 A bolí. 
 Taký zvláštny smutno-vážno-nežný tón. Veci, ktoré si nehovoríme, keď si potme hľadáme najbližšiu cestičku k sebe. 
 Toto je iná káva. 
 Ani ja presne nechápem chémiu vzniku. Vždy to príde ako nečakaná letná búrka. 
 Odhrmí slovom, ktoré nešetrí a je priame. Spáli bleskom poznania. A poláme vrcholce, na ktorých som  chcela ešte včera vidieť rásť nové ovocie. 
 Je po nej také zvláštne ticho. Možno na prvý pohľad sa nič dramatické nestalo, všetci prežili, ráno sme vstali a dala som ti dobrovoľne bozk na rozlúčku. 
 A predsa celý deň neviem nájsť kožu svojho ja. 
 Nemám miesta a nemám pokoja. 
 Prečo práve ty? Prečo práve ja? Prečo práve včera? 
 Mala som povedať niečo inak? Mala som mlčať? 
 Mala som byť tvrdá a neplakať? 
 Kráľovstvo za patent záchrany. 
 Celý deň na teba myslím a predsa sa ti bojím zavolať. 
 Akú Máriu to vlastne ľúbiš? 
 Mám strach, že ťa stratím a mám strach povedať ti:"Buď pri mne, objím ma, zostaň... 
 Je vo mne tak zvláštne čisto a prázdno, ako po veľkom upratovaní. Rany sú vymyté vlastnou soľou. A možno to bolo aj dobre, čo bolo povedané tebe a mne. 
 Čakám ťa ubolená a mám smútok za nechtami duše. 

