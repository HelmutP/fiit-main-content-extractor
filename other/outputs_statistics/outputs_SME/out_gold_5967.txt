

 Všade okolo počujeme, že je to zlé, horšie ako v minulom roku a možno to bude ešte horšie... 
 Ale opýtajte sa tých, od ktorých počujete práve tieto slová, čo robia preto, aby to zmenili a dostanete odpovede, znižujem náklady, znížil som platy, veľmi zvažujem, čo nové kúpim, musel som prepustiť ľudí, alebo budem musieť prepustiť ľudí, .... 
 Ale nikoho som nepočul povedať, že investoval, alebo chce investovať do svojich ľudí. Investovať do tých, ktorí "vytváraju" svojou prácou so zákazníkom hlavný príjem a tvoria dobré meno, ktoré je potrebné k prežitiu. Nikto (alebo iba málo) podnikateľov práve teraz zabúda na to podstatné, na ľudí, ktorí sú v priamom kontakte so zákazníkom a tvoria to podstatné, dobré meno spoločnosti. 
 Sú to ľudia, ktorí sú v prvej línii, ktorá rozhoduje o spokojnosti zákazníka s ich službou a tým pádom rozhodujú o tom, či sa zákazník k nim opätovne a rád vráti. 
 Kde sa ja opätovne a rád vraciam? Tam, kde dostanem to čo chcem kúpiť za reálnu cenu a k tomu niečo naviac. Príjemnú a slušnú obsluhu (úsmev nikdy nevadí), čisté prostredie, ochotu poradiť (nutnosť znalosti sortimentu),... 
 Pokračovanie na budúce 
 Foto: google 

