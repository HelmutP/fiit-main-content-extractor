
 Podali ďalej svätožiaru predali, pristrihli krídla, biele rúcho sňali, vyhostený pred nebeskú bránu,  hodený až na zem, opäť s hriechmi,  smrtelný roztrúsený po svete, povedali, že neviem nič, a ja som sa im vysmial do tvárí, nechápali a ja som ich v tom nechal, aj keď Belphegor z pekla brechal, ľutoval som len, že sa nemôžem smiať z raja. 
