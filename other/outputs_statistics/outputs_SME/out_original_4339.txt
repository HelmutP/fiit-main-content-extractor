
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viera Šimkovičová
                                        &gt;
                Postrehy a komentáre
                     
                 Komentár k listu Benedikta XVI írskym katolíkom 

        
            
                                    11.4.2010
            o
            10:05
                        (upravené
                11.4.2010
                o
                12:42)
                        |
            Karma článku:
                8.64
            |
            Prečítané 
            2534-krát
                    
         
     
         
             

                 
                    Som zlostný, Bože! - pod týmto názvom sa na Zelený štvrtok konala vo Viedni omša, venovaná obetiam sexuálneho zneužívania a krytiu týchto škandálov zo strany cirkevnej hierarchie. Mnohí veriaci sa upokojujú tým, že všetko už vyriešil list Svätého Otca írskemu ľudu. Komentár experta na kánonické právo a problematiku sexuálneho zneužívania však svedčí o oveľa triezvejšom pohľade na nádeje, vkladané do tohto listu. V článku nájdete jeho preklad.
                 

                 Pápežov list Írsku: Od začiatku odsúdený na sklamanie   List pápeža Benedikta írskym veriacim bol stratený už v prvom odstavci. Priznáva, že jeho zdrojom informácií o situácii v Írsku boli Írski biskupi a že jeho analýzy a navrhované riešenia boli výsledkom spolupráce s Vatikánskymi predstavenými. Táto kombinácia garantovala však iba ďalší krátkozraký a povrchný pohľad na túto strašnú pohromu a neuveriteľne zavádzajúce riešenie, ktoré má zachraňovať skôr pôvodcov problému, než jeho obete.   List má určité pozitíva. Po prvýkrát sa muž na vrchole skutočne dostáva k rozpoznaniu reálnych prípadov tejto nočnej mory, aj keď tieto boli známe a viditeľné väčšine ľudí mnoho rokov. Nepotrebovali sme, aby nám pápež povedal, že „nevhodný záujem o zachovanie reputácie Cirkvi" a „tendencia" laikov uprednostňovať klérus prispela k tejto pohrome. Keď sa prihovára k obetiam, uznal, že aj keď  „mnohí mali odvahu hovoriť...nikto by  ich nepočúval." Faktom je, že mnohí... takmer všetci boli neúspešní a boli by odmietnutí znova, nebyť masívneho pobúrenia, ktoré konečne prelomilo stenu odmietania a popierania zo strany cirkevnej hierarchie a pápeža. Všetko, čo pápež povedal o zneužívaní, bolo známe takmer každému, okrem hierarchie. Dobrá správa je, že čokoľvek už vedel alebo nevedel, pápež rozoznal klerikalizmus, herézu, podľa ktorej kňazi a biskupi sú nadradení a môžu klamať ľuďom, pričom sa má s nimi inak zaobchádzať, než s bežnými ľuďmi, ako podstatnú časť problému.   Väčšiu časť listu pápež káže a napomína írsky ľud, radšej než by ukázal, že má na dosah ruky podstatu tejto masívnej trhliny v tele Petrovej berly. Hovorí o kriminálnej podstate súčasných prípadov zneužívania a pripomína opakovane sa prehrešujúcim kňazom a predstaveným hanbu a neúctu, ktorú priniesli kňazstvu, ale robí neslýchanú chybu, keď kladie „nesmiernu ujmu, spôsobenú obetiam" na roveň v naliehavosti s ujmou, ktorú utrpela Cirkev, čím podľa mňa určite myslí inštitucionálnu Cirkev, a reputáciu týchto kňazov a náboženského života. Tu neexistuje žiadne porovnanie. Násilnosti páchané voči obetiam vražednými zneužívateľmi a samoúčelné odmietanie biskupmi je oveľa horšie, ako „ujma". Je to úplná vražda ich duší.   Zborenie posvätného imidžu kňazstva a kultúry kléru je očakávaný a potvrdený výsledok. Jeho Svätosť karhá biskupov, ale tiež sa snaží nepresvedčivo o zbavenie ich viny. Zlyhanie, nie iba „niektorých z vás", ale každého biskupa, ktorý čelil prípadu zneužitia, pri použití kanonického práva potvrdzuje názor, že koreň problému môže byť aj jeho riešením. Kánonické právo nielenže bolo neúčinné a nepoužiteľné v ochrane laikov pred zneužitím, ale je hlavnou súčasťou problému. Utajovanie, ktoré krylo túto pohromu, je produktom kánonického práva. A navyše, hoci sexuálne zneužitie je nazvané trestným v cirkevnom právnom kódexe, je tiež zločinom v sekulárnej spoločnosti. Skúsenosti posledných desaťročí bezpochyby ukázali, že Cirkev a jej kánonické právo sú nielenže neefektívne pri riešení sexuálneho zneužívania príslušníkmi kléru, ale že posadnutosť vonkajším imidžom a bezpečnosťou cirkevných štruktúr umožnila abuzérom naďalej škodiť. Vo svojom vyjadrení ocenenia pre snahu biskupov zahojiť chyby minulosti, pápež nedokázal rozpoznať, že jediné efektívne snahy o zahojenie, ktoré biskupi zaviedli, boli výsledkom masívneho tlaku civilných súdov, vyjadrení médií, a nadovšetko pobúrením obetí a spoločnosti. Okrem jeho silných slov pokarhania pre biskupov, pápež Benedikt vyzerá byť úplne slepý k ich základnej roli v tejto tragédii. Je schopný vidieť, že ten intenzívny hnev a pobúrenie je namierené na biskupov, a nie úbohých zneužívateľov?   Pohľad na svet z Vatikánu je jasne krátkozraký. Pápež a starší predstavení, s ktorými konzultoval, očividne vidia inštitucionálnu Cirkev ako výhradný a univerzálny zdroj duchovnej bezpečnosti pre katolíkov, a spoločenský príklad pre sekulárny svet. List jasne ukázal, že pápež a biskupi veria, že konečné uzdravenie obetí a ich rodín je v uzmierení sa s inštitúciou a v obnovení „rešpektu a dobrej vôle írskeho ľudu voči Cirkvi". Uvádzaním účelových citátov o inštitucionálnej cirkvi v celom liste, pápež Benedikt prezrádza, že všetko, o čom to je, nie je uzdravenie obetí, alebo očistenie Cirkvi, ale je to o moci, ich moci, a o uistení, že viac z tejto moci už nebude stratenej.   Konkrétne riešenia (uvedené v liste) sú surealistické. Žiadať írsky ľud, aby konal pokánie za obnovu Írskej cirkvi, znamená žiadať ich, aby prijali hanbu za pohromu sexuálneho zneužívania. Toto je vrchol. Nebol to írsky ľud, kto spôsobil tento útok na integritu Cirkvi, ale tí muži, ktorí boli vysvätení, aby ju chránili. Výzva k programom modlitieb a národným misiám ako cesty z tsunami je to, čo odborníci na duševné zdravie nazývajú magické myslenie. Pápež a biskupi sú uprostred najvážnejšej krízy ktorej čelí katolícka viera za posledných tisíc rokov. Stratili všetku silu, aby urobili čokoľvek efektívne na to, aby ju vyriešili alebo ju aspoň zmiernili. Pred sedemnástimi rokmi pápež Ján Pavol II napísal list Americkým biskupom o presne tomto probléme. Povedal v ňom, že medzi prostriedkami ktoré mali biskupi na to, aby reagovali na problém „Prvou a najdôležitejšou je modlitba, vrúcna, pokorná, dôverná modlitba.! O sedemnásť rokov neskôr sa pohroma odhaľuje už nielen v USA, kde JP II a jeho kúria chabne tvrdila, že bola skrotená a zadržaná, ale odhaľuje sa po celej Európe, s jasným predpokladom,  s jasnými indikáciám, že budeme vidieť oveľa viac odhalení v cirkvi v ďalších krajinách. Martin Luther King raz povedal : „Je dobré sa modliť, ale teraz musíme pochodovať." Pochodovanie by znamenalo urobiť rozhodné kroky, ktoré obaja pápeži, JPII a aj Benedikt neboli schopní akceptovať, pretože tieto kroky by znamenali vypochodovať z pevnosti moci a uznať, že hierarchický systém a jeho posadnutosť mocou a kontrolou je nepriateľom, a nie sekulárny svet.   Pápež bol očividne hlboko zasiahnutý utrpením obetí a ich rodín, ale osobná úzkosť namiesto aktivity nie je dostatočná. Tí, ktorí hľadia na tento list ako konečný signál, že Cirkev sa vynorila z tejto dlhej, tmavej noci duše, budú veľmi sklamaní. Dni kedy „Svätý Otec to povedal, tak to tak musí byť" sú mŕtve a pochované. Pápež skutočne verí, že cirkev je jediná, kto môže zahojiť obetiam rany a skončiť túto nočnú moru. To môže byť skutočne pravda, ale nebude to Cirkev pápeža Benedikta, biskupov a elity som medzi kléru. Dôvera a viera v nich mizne ako voda v púšti. Bude to Ľud Boží, „cirkev" ktorá nesídli vo Vatikáne a v monarchických štruktúrach, ale v srdciach a dušiach tých, ktorí postavili podporu a pomoc tým najzraniteľnejším a poškodeným nad všetko ostatné.   Autor komentára Thomas P. Doyle je medzinárodne uznávaný ako najskúsenejší expert na kánonické právo v oblasti sexuálneho zneužívania klérom. Svoje odborné práce venuje riešeniu súčasnej krízy, zmierneniu utrpenia obetí a prevencii ďalšieho zneužívania. Originál listu je možné nájsť na stránkach Richarda Sipeho „Celibát - sex - katolícka cirkev", kde   sa zhromažďujú výsledky mnohoročných výskumov  a poskytuje príležitosť pre serióznu diskusiu o problematike sexuálneho zneužívania v katolíckej cirkvi. Richard Sipe je klinickým poradcom v oblasti zneužívania obetí v katolíckej cirkvi, ako terapeut a kriminalistický expert,  a realizoval 25 ročnú etnografickú výskumnú prácu o celibáte a sexualite kňazov a rehoľníkov.       Ku komentáru T.P. Doyla pridávam niekoľko svojich myšlienok (doplnené 12:40) :   Aj pre mňa osobne je predstava, ako málo uvedomenia o dopade zneužívania na život obetí existuje a existovalo v danom čase v katolíckej cirkvi, len veľmi ťažko prijateľná a pochopiteľná. Hovoríme o láske a súcite, a nevieme si pritom dostatočne uvedomiť rozsah škody, ktorú svojím konaním zanechávame. Pozícia moci nás v tejto slepote len utvrdzuje.   Podstatou zneužitia je práve nerovnováha moci medzi pácahteľom a obeťou. Páchateľ má moc, ktorú zneužije voči obeti, ktorá sa nemôže brániť. Nemôže z rôznych dôvodov, jedným z nich je práve postavenie páchateľa, ktoré mu dáva punc dôveryhodnosti a autority, rešpektu, ktorý by nemal byť narušený spochybňovaním. Či už ide o interpersonálnu moc, v prípade zneužitia v rodine, alebo o moc štrukturálnu, danú postavením páchateľa, podstatou týchto skutkov často nie je samotný sexuálny obsah, ale práve využitie bezmocnosti obete a zvýšenie pocitu moci, ktorý takto páchateľ získava. Otázky spochybňujúce obete znužívania alebo smerujúce k ich zodpovednosti za vlastné zneužitie, sú bezpredmetné a hovoria o tom, že podstatu zneužitia ten, kto takéto otázky kladie, nepozná alebo poznať nechce.   Dynamiku týchto procesov nie je ľahké spoznať, ani pochopiť, zvnútorniť, preto máme tendenciu zľahčovať problém a zužovať ho na nezodpovednosť rodičov alebo obetí, ktoré mali možnosť problémy hlásiť a neurobili to. Pristupuje k tomu aj pocit derealizácie, znásobený faktom, že mnohé prípady sú niekoľko desiatok rokov staré. K derealizácii prispieva aj fakt, že väčšina informácií sa k nám dostáva cez médiá, teda nie priamym kontaktom s obeťou a jej utrpením. "Pozerať sa na niečo v telke" v nás môže zanechať pocit, že sa nás to netýka a že je to len ďalší z filmov, ktorý zmizne, keď televíziu vypneme alebo zatvoríme noviny. Podobný fenomén bol popísaný počas vojny v Iraku a v bývalej Juhoslávii, ktoré väčšina sveta sledovala práve cez médiá, nie je teda úplne nový. Treba si tiež uvedomiť, že prípady zneužívania sa dostávali v USA na povrch už pred 20timi rokmi, avšak v Európe to trvalo ďlších 20 rokov, kým bola bariéra mlčania prelomená. Pre objektívneho hodnotiteľa to tiež nie je nič výnimočné, ak si porovnáme túto situáciu s faktom, že zneužitie moci iného charakteru, napríklad masaker v Katyni, bolo zamlčiavané zo strany Ruska 50 rokov.   Preklad tohoto článku nie je z mojej strany aktom nenávisti voči katolíckej cirkvi. Je konkrétnym skutkom, ktorý môžem urobiť v smere k tomu, aby problematika zneužívania bola lepšie pochopená, aby sa bolesť a utrpenie obetí mohli aspoň čiastočne uzdraviť a aby všetky kroky do budúcnosti smerovali k účinnému predchádzaniu podobným hrozným skúsenostiam bezbranných. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (364)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Milujú katolíci len tých, ktorí sú na kolenách?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Slovensko plné šťastných gayov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Rastú, keď sa nepozeráme
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Za plotom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viera Šimkovičová 
                                        
                                            Vyhodiť či nevyhodiť.. to je otázka
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viera Šimkovičová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viera Šimkovičová
            
         
        vierasimkovicova.blog.sme.sk (rss)
         
                                     
     
        Veľkosť človeka sa nemeria iba výšinami, na ktorých spočinula  jeho noha, ale aj temnými roklinami a prepadliskami, ktoré dokázal zdolať pri ceste do výšin...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    62
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2056
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Postrehy a komentáre
                        
                     
                                     
                        
                            Životné cesty
                        
                     
                                     
                        
                            Príbehy
                        
                     
                                     
                        
                            Psychológia a spiritualita
                        
                     
                                     
                        
                            Rozprávkovo
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Neonacizmus nie je zlo
                                     
                                                                             
                                            Scott Peck: Lidé lži
                                     
                                                                             
                                            Temnota je proto aby bylo světlo
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Scott Peck: Odmítnutí duše
                                     
                                                                             
                                            Christopher Paolini: Brisingr
                                     
                                                                             
                                            Robert Fullghum: Ach jo
                                     
                                                                             
                                            Scott Peck: Svět který čeká na zrození: Návrat k civilizovanosti
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janette Maziniová
                                     
                                                                             
                                            Katarína Pišútová
                                     
                                                                             
                                            Miro Kocúr
                                     
                                                                             
                                            Vlado Schwandtner
                                     
                                                                             
                                            Zuzana Roy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Mokranovci
                                     
                                                                             
                                            Škola Fantázia
                                     
                                                                             
                                            Annwin
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Zápisky o ľuďoch – v inom živote
                     
                                                         
                       Opentaní chlapci, bojovať!
                     
                                                         
                       Aké bude referendum o rodine? Drahé a posledné
                     
                                                         
                       Porozumenie medzi ateistami a veriacimi
                     
                                                         
                       Referendum o rodine: Slušní ľudia, ktorí vám ukradnú slobodu
                     
                                                         
                       Keď si Monika ugrgne z pravdy
                     
                                                         
                       Keď som stála nad hrobom
                     
                                                         
                       (Prečo) sú katolíci stádo?
                     
                                                         
                       Snahy o nápravu chýb v RKC v súlade s vierou a láskou
                     
                                                         
                       Varíme s medveďom - Kačacie prsia s gaštanovou omáčkou
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




