

 O vikendu sem neměl co robit, tuž sem osedlal kolo a vydal se na štreku z Mělnika přes Kokořin a Bezděz na hranicu rajchu, a druhy den přes Usti nad Labem, Litoměřice a Terezin zpatky do Praglu. Celu cestu sem přemyšlal, kaj je ten obyčejny člověk a co vlastně robi. A na druhu stranu jsem zjišťoval, co vlastně robi ten neobyčejny člověk. 
 Tady su vysledky meho hledani: 
  
  
 Obyčejny člověk věři všeckemu, co prezentuje televiza, radyjo a noviny. Bo novinař je přece panbuch. Tyto koleje se v dalce potkaju. Viděl sem to přece na Jojce na vlastni oči. 
 
y 
 Obyčejny člověk byva v takem obyčejnem šedivem domku a zavidi susedovi, že si namaloval barak na žlutoružovo a zrobil si tam kolonyjal. Obyčejny člověk nevidi, že je s tym hafo starosti a ani vydělek neni na vesnici s 500 lidma moc velky. Ale obyčejny člověk se těši, až ta lipa v prostředku spadne a rozmlati neobyčejnemu člověkovi střechu. 
  
 To je sranda, co? Stat nema prachy, ale politycke strany rozhazuju milijony na totalni cypoviny. 
  
 Neobyčejny člověk dokazal opravit stary barak tak, že z něho ma radost on aji lidi, keři vali okolo. Pokud si na to pořidil dluh u banky zvany hypoteka, bude si muset hodně dluho utrhovat od huby. 
  
 Mezitim obyčejny člověk dokazal tež zazraky. Objekt ziskal specijalni architektonycku cenu: Švajnerajn/Sajrajt roku. 
  
 Nadherne misto. Ale tady sem obyčejneho člověka nepotkal. Obyčejny člověk lapi doma, čumi na televizu a nadava na ty svině v polityce. 
  
 Neobyčejna hospoda neobyčejneho člověka. Uvnitř měli vybornu Svijansku 12° a dokonalu domaci prdelanku. Ale my Češi zme na tu reklamu hlupi. Misto abyzme napsali na cedulu "DOKONALA DOMACI PRDELANKA ZA 20Kč", napišeme tam HOSPODA ODEVŘENA. No, mame se furt co učit. 
  
 Čerstva pizza. To je ale kokotina. Čerstva pizza. Už ste někdy viděli reklamu na okoralu pizzu? 
  
 V te reklamě prostě ještě mame svoje muchy. 
  
 Ale co se tyka baraku, tady byva neobyčejny člověk. 
  
 Neobyčejny člověk totiž nespoleha na stat a socijalni davky. Neobyčejny člověk vi, že veškere bohatstvi pochazi z roboty a pořadku. 
  
 Zato obyčejny člověk se furt ohliža, co na to řeknu susedi. Best Bohemian Aussiger Beer. Nejlepši česke Aussiger pivo. Fuj, že se nestydite. 
  
 Neobyčejny člověk se nestydi přiznat, že tady žili němečti obyvatele a v prvni valce zme chcipali pod stejnu vlajku za našeho cisařa. Neobyčejny člověk si važi svoji historie, aji když to někdy nebylo zrovna hezke. 
  
 Obyčejny člověk si nevaži ničeho. 
  
 Obyčejny člověk se snaži vyryžovat co se da. Parkovne za dvacet korun? No fuj. Hlavně, že ma obyčejny člověk změřenu cestu k hradu. 720 metru. 
  
 Neobyčejny člověk v Doksach, pravděpodobně přistěhovalec - kratky zobak z Ostravy. 
  
 Hrozne. Oděvy, obuv und schuhe. Obyčejny člověk se stal obchodnikem. Hrozne. 
  
 Prubnul sem hledat obyčejneho člověka v Krušnych horach. Přiznavam, byla to cypovina. 
  
 Ne, že by tady obyčejny člověk nežil, to ne. Projevuje se vlastnictvim barevnych spreju. 
  
 Zatimco obyčejny člověk sprejuje šutry, neobyčejny člověk opravuje opravdove baraky. 
  
 V roce 1908 měla obec 805 stalych obyvatel. Pak tyto obyvatele obyčejny člověk vyhnal zpatky do rajchu. Slovy publikace: "...došlo k upadku cele oblasti a teprve od roku 1989 se zde opět začal obnovovat turistycky ruch." Odhadem zde žije asi 20 stalych obyvatel... 
  
 V mistni putyce robi naprosto dokonalu bramboračku schovanu do chleba. Užasne! Mimochodem, v knižce o Ducem sem se dočetl, že tento obyčejny člověk byl až do roku 1918 vyznamnym představitelem italskych socijalistu.... 
  
 Krasny most v Usti projektovany neobyčejnym člověkem. Z mostu, na kterem sem stal, hazeli obyčejni člověci dolu do Labe svoje spoluobčany. Včetně kočarku s děckama. Ale to už je davno... 
  
 Neobyčejny člověk naplanoval a vyprojektoval krasnu cyklostezku podel Elbe. Sorry, Labe. 
  
 A obyčejny blbec ju vybavil hejnem naprosto nesmyslnych, drahych, hlinikovych značek. 
  
 Neobyčejny člověk připravuje dobroty v mistnich knajpach. Bez ohledu na to, jestli to je podle předpisu EU, anebo ni. Mimochodem, tataraček byl vyborny. Vratil mi silu. A nocleh v litoměřickem penzionu U Borovičku byl tež dokonaly. Včetně ranni domaci šunky a hruškovice od suseda. Kdyby se to dozvěděl ten mamlas Gyntr Frhojgen z Unyje, asi by mu z teho střelilo v pale. 
  
  
 Ranni Litoměřice. Z kopečku Radobyl neobyčejny česky basnik a pyroman Karl-Heinz Macha spěchal hasit požar. Misto aby pil pivo, chlastal kontaminovanu vodu neschvalenu ČOI a za par dnu natahnul brka na choleru. O sto roku později obyčejny rakusky maliř přikazal kopeček Radobyl provrtat a nechal tam vyrabět motory pro V2. 
  
 Terezin. Smutne město plne obyčejnych lidi. Dnešni cedule pusobi tak nějak divně. 
  
 Historia vyliza ven. Možete ju přetřit tisickrat, stejně vyleze. 
  
 Některe baraky vypadaju furt jako uřadovny gestapa. 
  
 A tady leži kdo? Jak tady polityci rozliši obyčejne lidi od neobyčejnych? To je vlastně jedno. Mrtvy nema volebni pravo. Mrtvy je pro polityka obvykle bezcenny. 
  
  
 Ja vim, je to blbost, ale některe firmy by měly dat pozor na nazev sveho sidla. Pozor, střeženy objekt Židovice. Vyroba vonnych a chuťovych latek...par kilaku od Terezina. 
  
 Konec Židovic. A jakysik obyčejny člověk osadil značku 70km/h. Aji na kole sem jel rychleji. 
  
 Zatimco obyčejny člověk osazuje značky a dava přikazy a zakazy, neobyčejny člověk si založi neobyčejnu farmu. Mimochodem, pštrosi stejk sem poprve žral v jižni Africe. Užasne maso! 
  
 Neobyčejny člověk se neboji dat najevo svuj nazor. 
  
  
 Neobyčejny člověk vlastnici hospodu dobře vi, že drahota se překonava domacima produktama od mistnich sedlaku. Tlačenka za 20 kaček nebo gulašek za 56 kaček? Oboji vyborna žranica připravena s lasku. 
  
 Obyčejny člověk se mezitim věnuje jinym aktyvitam, jiny obyčejny člověk o tym informuje medyjalni scenu. 
  
  
 Obyčejny člověk robi reklamu na stěnach veřejnych budov, ale boji se zveřejnit kontakty. 
  
  
 Neobyčejny člověk se nestydi zveřejnit kde, kdy a co. 
  
 Černa 12° v pivovaru U Bansethu zrobena z nakuřovaneho sladu je užasnym vysledkem roboty neobyčejneho člověka. Ten chlap si može řict: Ja su sladek, kdo je vic? 
  
  
 Neobyčejny synek přemyšla o buducnosti. Obyčejny člověk žadnu buducnost nema. 
  
  
 Obyčejny člověk se vyjadřuje a chova jak hovado. Když nic nemam, aspoň cosik zničim. 
  
 Neobyčejny člověk pracuje a tvoři. 
  
 Obyčejny člověk niči a boři. 
  
 Po obyčejnem člověkovi obvykle zustane jen jedno. A uklidit to musi kdosik jiny. Za obecni penize. Za penize neobyčejnych lidi. Lidi, keři cosik robi. 
  
 Co zustane z polityckych hesel? 
  
 Napsal to obyčejny, nebo neobyčejny člověk? 
  
 Neobyčejny člověk neniči robotu druhych lidi. Ale co když to už je projev zoufalstvi? Co když už nema silu posluchat všechny ty vepřove hlavy? Za ty dva dny sem potkal obyčejne aji neobyčejne lidi. Strany, kere se schovavaju za obyčejneho člověka, su mi krajně podezřele. Obyčejny člověk je totiž prase. A co ste vy? Robite, tvořite, pracujete? Anebo ste obyčejni člověci? 

