
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Herman
                                        &gt;
                Nezaradené
                     
                 Hrudka 

        
            
                                    3.4.2010
            o
            15:54
                        |
            Karma článku:
                9.00
            |
            Prečítané 
            2688-krát
                    
         
     
         
             

                 
                    V nedeľu „varíme s Medveďom", ale keďže dnes je sobota a k tomu ešte Biela, musíme mať na Veľkonočnú nedeľu a pondelok navarené a pripravené všetky tradičné veľkonočné dobroty. Pretože posvätenie veľkonočného košíka je práve v túto sobotu, prípadne v nedeľu ráno.
                 

                     Viem , všade sa to nerobí, ale na východe Slovenska to neodmysliteľne patrí k Veľkonočnej tradícii. Nebudeme variť šunku a klobásky ani maľovať vajíčka. Chcel by som vám pripraviť veľkonočnú hrudku, niekde sa jej hovorí aj syrek alebo syrec.   Príprava je jednoduchá a množstvo, resp. veľkosť hrudky závisí len na tom, koľko použijete mlieka a vajec, pretože práve z týchto dvoch ingrediencií hrudka pozostáva.   Teda ja urobím hrudky dve, z dvoch litrov mlieka a 35 vajec.   Mlieko vylejeme do veľkého hrnca a do mlieka rozbijeme všetky vajcia. Pridáme trochu soli, lyžičku cukru, záleží len na Vás, či chcete hrudku slanšiu alebo sladšiu. Niekto pridáva aj vanilkový cukor. Habarkou vajcia poriadne v mlieku rozšľaháme, aby sme dosiahli peknú homogénnu žltkastú hmotu.      Hrudku sa oplatí variť v kvalitnom hrnci, aby nám spodok neprihorel, ja ešte dávam pod hrniec pre istotu aj železnú platničku . Stále miešame.      Kým sa nám hrudka varí , pripravíme si gázu a sitko, do ktorého budeme hrudku dávať keď bude uvarená. Že je hrudka uvarená zistíte ľahko, v hrnci sa celá hmota krásne zrazí do tvrda a pláva v riedkej srvátke.      Teraz príde najdôležitejšia časť výroby hrudky. Uvarenú hrudku dáme do gázy v sitku a poriadne so špagátom alebo drôtom stiahneme aby sa nám vytvorila tvrdá guľa. Túto zavesíme na vhodné, chladné miesto a necháme z nej vykvapkať všetku prebytočnú vodu . Po vychladnutí opatrne rozbalíme , hrudka je hotová.                         Nikde ešte hrudku zapekajú na pár minút v rúre, aby povrch trochu zhnedol a stvrdol. Receptov na hrudku nájdete pomerne veľa. Tento je ten najjednoduchší. Záleží len na vašej fantázii, čo do hrudky pridať. Niekde sa zvykne pridávať zelená petržlenová vňať, niekde pažítka, ale podľa mňa je to zbytočné. Zeleninku si k hrudke môžete pridať aj pri jedení. Ale najlepšia je so šunkou , klobáskou, čerstvým chrenom a voňavým chlebíkom.      Tak a takto vyzerá pripravený košík .      Pekné Veľkonočné sviatky     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            V Kauflande
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            Šče ne vmerla Ukrajiny ni slava, ni voľa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            K téme, na rovinu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            Ešte nie sú online
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Herman 
                                        
                                            Nevyužitá šanca
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Herman
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Herman
            
         
        herman.blog.sme.sk (rss)
         
                                     
     
        Prakticky - realista,teoreticky -idealista v skutočnosti celkom obyčajný jedinec.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    170
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1616
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            žena v najlepších rokoch
                                     
                                                                             
                                            kňaz z Lunikova
                                     
                                                                             
                                            milovník gruzínskeho čaju
                                     
                                                                             
                                            lekár-špecialista
                                     
                                                                             
                                            patavedec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            zive
                                     
                                                                             
                                            slobodná encyklopédia
                                     
                                                                             
                                            počítače.sme.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




