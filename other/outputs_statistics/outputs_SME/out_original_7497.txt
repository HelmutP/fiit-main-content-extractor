
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Čítanie SME môže voličov Smeru utvrdiť v ich názoroch 

        
            
                                    28.5.2010
            o
            19:19
                        |
            Karma článku:
                11.81
            |
            Prečítané 
            3580-krát
                    
         
     
         
             

                 
                    Podľa americkej štúdie sa pri čítaní kritických informácií obzvlášť  vyhranení voliči paradoxne utvrdia vo svojej pôvodnom názore
                 

                 
Tak veru, je to presne naopak, ja som to už dávno tušil.sme.sk
   Na záhadu, prečo je medzi čitateľmi voči vláde silne kritického SME zhruba rovnaký podiel voličov vládnych strán ako medzi bežnou populáciou, sa doteraz ponúkali tri odpovede. Že nie je na trhu názorovo alternatívny denník, že možno koaliční voliči čítajú SME pre jej nepolitické sekcie, respektíve že sa k nemu mnoho ľudí dostáva nie vlastným nákupom či predplatným, ale v práci a v kaviarňach z núdze, prípadne z povinnosti. Aj tak však ostáva záhada, prečo sa aspoň niektorí „núdzoví“ či náhodní čitatelia SME po čítaní predsa len neodkláňajú od Smeru či HZDS. To naozaj noviny svojich čitateľov v politických preferenciách vôbec neovplyvňujú?   V doterajších teóriách zvykli výskumníci potvrdzovať dve zistenia. Po prvé, ľudia s istým svetonázorom či presvedčením vnímajú nové informácie selektívne – skôr inklinujú k médiám, alebo článkom v médiách, ktoré ich doterajšie názory potvrdzujú. Po druhé, ak aj narazia na informácie, ktoré ich silné názory vyvracajú či spochybňujú, majú tendenciu ich podceňovať a rýchlo z hlavy vypustiť (viď kritiku Petra Breinera na konto .týždňa).   Nová štúdia amerických expertov naznačuje, aký komplikovaný ten vzťah medzi silnými názormi čitateľov a novými údajmi v médiách môže byť. Nielen že čitatelia môžu fakty, ktoré protirečia ich názorom ignorovať, ale dokonca ich môžu v pôvodných postojoch posilniť! Americkí vedci Brendan Nyhan a Jason Reifler uskutočnili dve štúdie počas dvoch rokov, v ktorých skupinám vysokoškolských študentov ponúkali spravodajské články podobné novinovým. Texty obsahovali zavádzajúce tvrdenia politikov o troch témach – že Irak mal pred inváziou zbrane hromadného ničenia (nepravda, resp. nepotvrdené), že zníženie daní na začiatku Bushovej éry priniesli zvýšenie daňových príjmov (nepravda) a že prezident Bush zakázal výskum kmeňových buniek (len obmedzil použitie štátnych peňazí, teda tiež nepravda).   Časť respondentov dostala text aj s dodatkom, ktorý uvádzal fakty dokumentujúce zavádzanie v prvej časti textu. Autori výskumu zistili očakávané: že Bushovi sympatizanti skôr verili zavádzajúcim informáciám v jeho prospech, a Bushovi odporcovia presne naopak. Skupina, ktorá dostala aj doplňujúce faktické spresnenia sa tiež správala podľa starších teórií – mierni konzervatívci skôr ich názorom protirečivé údaje ignorovali a trvali na svojom pôvodnom stanovisku, a liberáli sa zase v priemere ešte viac utvrdili vo svojich pôvodných názoroch (presne opačne tento efekt fungoval pri výskume kmeňových buniek, kde sa mýlili liberáli). Najprekvapujúcejšie však bolo, že pri silných konzervatívcoch sa opakovane objavil tzv. backfire effect. Teda že po prečítaní faktického doplnku dokonca ešte silnejšie verili v správnosť svojej pôvodnej pozície.   Autori štúdie nemajú na toto zistenie jasné vysvetlenie. Špekulujú, že ľudia s obzvlášť silnými názormi si pri strete s „nesúhlasnými údajmi“ vytvárajú v hlave akoby protilátky, protiargumenty, ktoré ich potom motivujú k zvýšenému presvedčeniu o sile svojho svetonázoru. Zaujímavé je, že vedci v tejto štúdii neobjavili vplyv značky média ako zdroja informácií (alternatívne skúšali FoxNews.com a New York Times). Zdá sa, že respondenti reagovali priamo na obsah textu, nie na označený zdroj.   Čo z toho vyplýva pre SME a ich čitateľov-koaličných voličov? Neviem. Žeby sa obetovali pre vedu a štyri roky spolupracovali na nejakej výskumnej úlohe?   PS Kdeže sú slovenskí univerzitní mediálni výskumníci a ich zistenia??       Čo s cenami nových bytov: podľa titulkov Hospodárskych novín   4.2., Novostavby majú ešte padať. S cenami  Ceny bytov. Hospodárska kríza zrazila ceny nehnuteľností o viac ako 11 percent. Priestor na ďalší pokles však je, hlavne pri nových bytoch.  22.3, Byty sú predražené. Trh čaká upratovanie Neprežijú mnohé developerské projekty ani realitky. Realitný trh na Slovensku sa stále nezbavil predražených bytov ani nereálnych očakávaní.  25.3, Trh sa zobudil, ceny môžu zamieriť nahor Spiaci realitný trh? Vyzerá to tak, že to je už minulosťou. Ak chcete kúpiť nové bývanie, mali by ste sa poponáhľať, pretože dopyt už začína rásť a nehnuteľnosti v lepších lokalitách môžu zamieriť aj nahor  6.5, Slováci musia konať, byty už lepšie nekúpia Éra zlacňovania nehnuteľností sa definitívne skončila. Čakanie na lacnejšie byty či domy sa oplatilo. Cenový trh s nehnuteľnosťami na Slovensku totiž definitívne dosiahol svoje dno.   Včera:  Byty opäť lacnejú. Musia Nepomohlo prvé zlacnenie, prichádza ďalšie kolo. Ceny bytov opäť klesajú. Zlacnenie sa však týka len bytov, ktoré sú v ponuke už dlho.   Problém je v robení titulkov bez kontextu. Niekedy stojí titulok na oficiálnom čísle ako priemere za celý trh, inokedy na pár tvrdeniach realitných kancelárií, a potom zase na číslach za časť nových bytov. Ak ale čitateľ tému nesleduje a len preletí očami titulky (čo mnohí robia), musí mať z toho vývoja v hlave chaos.       Kritika športových novinárov: Dobrý článok na ifutbale, akých slabých a mäkkých máme našich športových novinárov:   Ak si niekto v týchto dňoch prečíta ktorýkoľvek anglický denník, nájde tam vyčerpávajúce analýzy. Prečo je Crouch lepší ako Heskey, kto by mal byť jedničkou v bráne, či ako hrať bez Rooneyho. Na Slovensku však skutočný športový novinár, kritik či expert momentálne nie je. Pre novinárov znamená viac byť kamarátom s reprezentantom ako mať vlastný názor. A tak všade nájdeme množstvo servilných rozhovorov, nič nehovoriacich poznámok a žiadnu analýzu.       Dag Daniš o vyhliadkach opozície: Vzhľadom na súčasné preferenčne vyrovnané sily koalícia-opozícia je zaujímavé sa pozrieť na rok, dva roky staré predpovede komentátora Pravdy, dnes Hospodárskych novín:  Červené zadky, 22.7.2008     Závratná sila Ficovho Smeru, kríza pravice a zakladanie nových strán... Kombinácia týchto obrazov dohnala niektorých politológov k čudesným úvahám. Vraj sa tu otvára priestor pre nové pravicové strany, ktoré by mohli nahradiť tie staré. Bla-bla-bla... Úvahy tohto typu sa nedajú nazvať inak ako bláboly. A bludy.  To posledné, čo by mohlo oslabiť Smer a posilniť pravicu, je "nová pravicová strana". Stále platí pravidlo, že voličov treba loviť tam, kde ich je pretlak, nie tam, kde hladuje "posledný Mohykán". Navyše aj absolventovi pomocnej školy musí byť jasné, že čím viac strán sa bude deliť o hlasy pravicových voličov, tým viac hlasov prepadne pod 5-percentnou hranicou.... A ak popri tom existuje šanca na vznik novej strany, tak by to mala byť stredová strana podobná Smeru. Pre "akciových" voličov, ktorí radi skúšajú "novinky".  Zakladanie nových pravicových strán môže potešiť akurát tak Ficových socialistov.   Dzurindov zvrat, 28.7.2009,   Mikuláš Dzurinda rok pred voľbami srší sebavedomím. Najskôr zakričal (ako zajac na vlka), že "spolupráca so Smerom je neprijateľná". Cez víkend zas oznámil, že pravica sa po voľbách pokúsi "o zvrat".  Dzurindove sebavedomé pózy sú pritom tak trochu hrané.   Politický zvrat je pekná idea. Ficova zostava nezvláda ani boj s krízou, ani sama seba. Problém je "len" v tom, že Dzurinda nie je ten pravý, kto by mal dosť sily (volebnej a morálnej) zvrat presadiť. Jeho staré triky - prehrať voľby, kopnúť víťaza do zadku ako neprijateľného a spriadať široké koalície - už sú, ako sme videli v roku 2006, nepoužiteľné. Ficovi izolácia nehrozí. Naopak, so svojím "plánom" je opustený skôr Dzurinda. SMK ostáva pragmatická a úskočná. KDH bude aj po nástupe Figeľa prispaté a nerozhodné. A nalomená je dokonca aj SDKÚ, roky budovaná ako strana moci.    Veľká porada, 14.12.2009   Veľké stretnutie opozičných strán, ktoré zvoláva Dzurinda, bude možno zaujímavá udalosť. Ale bezvýznamná. Porada SDKÚ, KDH, SMK, Mostu, SaS a OKS (áno, je to únavne dlhý zoznam) neprinesie nič. Rok 1997 sa nezopakuje. Opozícia v tomto zložení totiž nemá čo ponúknuť. Dokonca ani v ideálnom prípade, keby si všetci prisahali vernosť (čo sa nestane). 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (34)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




