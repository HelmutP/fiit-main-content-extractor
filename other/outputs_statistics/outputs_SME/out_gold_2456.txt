

 Diskutovať donekonečna nemá zmysel. Pozrime si fakty, ktoré o vode z vodovodu zverejnil na svojich stránkach Úrad verejného zdravotníctva a Slovenská agentúra životného prostredia. Len pre zaujímavosť – tieto  nezávislé inštitúcie vykonávajúce kontrolu uvádzajú fakty,  nehovoria o „zdravej vode z vodovodu“. Na rozdiel od zadávateľov kampane na zdravú pitnú vodu z vodovodu jej dávajú len tie prívlastky, ktoré má, a to že je zdravotne bezchybná, teda aj bezpečná, ak spĺňa ukazovatele a ich limity kvality. 
   
 Výnimky, o ktorých mnohí netušia 
 Veľmi by ma zaujímalo, koľko ľudí, ktorých sa to bezprostredne týka, vie, že ich voda z vodovodu má dlhodobo udelenú výnimku, pretože nespĺňa hygienické limity ukazovateľov kvality pitnej vody. V praxi to znamená, že možno denne pijú vodu z vodovodu, ktorá prekračuje maximálnu limitnú hodnotu stanovenú pre niektorú látku. Maximálne hodnoty sú stanovené na to, aby definovali vodu, ktorá z dlhodobého hľadiska nepredstavuje hrozbu pre zdravie. 
 Takáto výnimka môže byť udelená na tri roky s možnosťou predĺženia. Udelené výnimky zverejňuje Úrad verejného zdravotníctva na svojej webovej stránke i vo výročných správach. Dlhé roky mal napríklad udelenú výnimku Čatajský skupinový vodovod v okrese Senec z dôvodu zvýšeného obsahu dusičnanov. Vo výročnej správe Úradu verejného zdravotníctva SR za rok 2008 sa uvádza,  že napríklad „v okrese Košice – okolie je v platnosti už druhá výnimka z dôvodu vysokej koncentrácie dusičnanov pre obec Žarnov, kde je na verejný vodovod napojených 366 obyvateľov. V roku 2008 bola povolená výnimka v ukazovateli antimón pre povrchový vodárenský zdroj Rožňavský potok, ktorý je jedným zo zdrojov pre mesto Rožňava...“ Uvedený rožňavský zdroj zásobuje podľa  http://pitnavoda.enviroportal.sk/ takmer 19 – tisíc obyvateľov!  
 Verejné studne, ktoré sú tiež zdrojom vody najmä v obciach bez verejného vodovodu alebo rekreačných oblastiach, sa tiež nemajú čím chváliť. Podľa spomínanej výročnej správy ÚVZ len napríklad v Banskobystrickom kraji sa nachádza približne 38 verejných studní. Vodu z verejných studní využíva odhadom 800 obyvateľov kraja. Kvalita pitnej vody z verejných studní vykazovala závadnosť  najmä v mikrobiologických a biologických ukazovateľoch. „V Košickom kraji je evidovaných približne 37 verejných studní. Vo všeobecnosti je ich zdravotné zabezpečenie – ochrana, technický stav, dezinfekcia a kvalita pitnej vody - nevyhovujúce,“ píše sa vo výročnej správe.  
 Zaujímavé čítanie o „zdravej“ pitnej vode z verejných vodovodov týmto výpočtom zďaleka nekončí. „V septembri 2008 Regionálny úrad verejného zdravotníctva hlavné mesto Bratislava zaslal lekárovi Bratislavského samosprávneho kraja upozornenie na zvýšené koncentrácie dusičnanov v pitnej vode vo verejných vodovodoch obcí Báhoň a Píla (okres Pezinok), Jablonové, Plavecké Podhradie a Rohožník (okres Malacky) a Čataj, Igram, Kaplna (okres Senec). Oznam požadoval upozorniť pediatrov pôsobiacich v uvedených obciach na nevhodnosť používania pitnej vody na prípravu dojčenskej stravy a potrebu jej nahradenia balenou vodou so zníženým obsahom nitrátov. Pri vydaní upozornenia sa vychádzalo zo skutočnosti, že v uvedených obciach je verejným vodovodom distribuovaná pitná voda s obsahom dusičnanov v rozmedzí od cca 30 do 50 mg/l (koncentrácie NO3 však neprekračovali hygienické limity pre dospelé obyvateľstvo),“ uvádza odborná správa. 
   
 Zaujímavá štatistika 
 Zaujímavé štatistické údaje nájdeme – ako inak - v zdrojoch kontrolórov Úradu verejného zdravotníctva SR, konkrétne z monitoringu a štátneho zdravotného dozoru nad hromadným zásobovaním pitnou vodou. Sú to síce údaje z roku 2008. Ale predpokladať významnú zmenu v roku 2009, kedy Asociácia vodárenských spoločností a jej členovia investovali veľký objem peňazí do diskutabilnej kampane „Pijem zdravú vodu, nápoj z vodovodu“ miesto do rozvoja vodárenskej siete, je utopické. A ako vyzerala kvalita vody z vodovodu v roku 2008? (http://www.uvzsr.sk/docs/vs/vyrocna_sprava_SR_08.pdf) 
   
 Bratislavský kraj?  V roku 2008 bolo v rámci monitoringu vyšetrených celkom 519 vzoriek pitnej vodovodnej vody. Z počtu 519 vzoriek nevyhovelo 14,5 %!  V rámci štátneho zdravotného dozoru bolo odobratých 701 vzoriek, z toho 14,41% prekračovalo povolené limity. 
   
 Trnavský kraj? V roku 2008 bolo v rámci monitoringu odobratých 576 vzoriek. Z celkového počtu bolo 13,9 % závadných. V rámci štátneho zdravotného dozoru bolo vykonaných 55 analýz, z toho bolo 43,6 % závadných.  
   
 Trenčiansky kraj? Odobratých a vyšetrených 775 vzoriek pitnej vody z verejných vodovodov, z tohto 28% vzoriek prekračovalo limitné hodnoty.  V rámci výkonu štátneho zdravotného dozoru bolo odobratých a vyšetrených 490 vzoriek, z ktorých 30,4% nespĺňalo požiadavky platnej legislatívy.  
   
 Nitriansky kraj? Bolo odobratých v rámci monitoringu kvality pitných vôd 931 vzoriek. Nevyhovelo 9,9 %.  
   
 Žilinský kraj? Odobratých v rámci monitoringu 748 vzoriek. Z celkového počtu analyzovaných vzoriek bolo 9,76 % nevyhovujúcich požiadavkám na kvalitu pitnej vody. V rámci štátneho zdravotného dozoru bolo odobratých 80 vzoriek, z ktorých nevyhovovalo legislatívnym požiadavkám na kvalitu pitnej vody 36,25 % vzoriek.  
   
 Banskobystrický kraj? V rámci monitoringu odobratých 1030 vzoriek pitnej vody z verejných vodovodov, z nich bolo až 30,78 % nevyhovujúcich.  
   
 Prešovský kraj? V rámci monitoringu vyšetrených celkom 905 vzoriek. Závadnosť 16,7%.  
   
 Košický kraj? V rámci monitoringu vyšetrených celkom 787 vzoriek. Závadnosť vyše 22 % z odobratých vzoriek.  
   
 Suverenita kampane na vodu z vodovodu má vážne trhliny 
 Ak tu máme takéto verejne dostupné a autoritami vydávané štatistiky, ako môžeme jedným dychom povedať, že výlučne voda z vodovodu pozitívne vplýva na naše zdravie a zdravie našich detí? Vždy, všade a na celom Slovensku a dokonca až tak veľmi, že to našim deťom môžu deti z iných krajín závidieť?  
   
 Kampaň o vode z vodovodu cielila priamo na deti. Jej súčasťou bola aj distribúcia nálepiek do ZŠ a SŠ, ktoré obsahovali nápis: „Nalep ma na umývadlo vo svojej triede a vyhraj MP4 pre všetkých spolužiakov“. 
   
 Je namieste otázka: Mali tieto nálepky lepiť aj deti z Rožňavy, alebo z ktorejkoľvek inej lokality Slovenska, kde sú udelené trojročné výnimky a kde boli zistené prekročené limity pre pitnú vodu? Majú sa aj tieto deti tešiť z toho, že majú takú zdravú vodu z vodovodu, že im ju môžu deti z iných štátov závidieť? 
   
 Záver je zrejmý. Nikto nechce tvrdiť, že voda z vodovodu je zlá a nedá sa piť. Dá, a na Slovensku máme to šťastie. Ale rovnako nikto nemôže tvrdiť, že je jediná zdravá pre všetkých, vždy a všade na celom Slovensku a, že pozitívne vplýva na naše zdravie a zdravie našich detí. Medzi evidentným pozitívnym vplyvom na zdravie a zdravotnou bezpečnosťou je totiž rozdiel... 
   
   
   
   
   
   

