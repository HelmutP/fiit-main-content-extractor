
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Červeň
                                        &gt;
                Lunikovo
                     
                 Na šváby chémia neplatí, iba fyzika! :-) 

        
            
                                    19.10.2007
            o
            13:30
                        |
            Karma článku:
                11.21
            |
            Prečítané 
            14773-krát
                    
         
     
         
             

                 
                    Život na Luniku IX so sebou prináša aj riešenie problémov, s ktorými sa inde nestretnete. Jedným z týchto problémov je zbavenie sa hávede, ktorá dokáže prežiť aj výbuch atómovej bomby. Overené to nemám, na vlastné oči som však videl, ako v mikrovlnke zovrel pohár vody, a šváb, ktorý z tela vytlačil veľkú kvapku vody, si veselo pobehoval okolo pohára, akoby sa to jeho netýkalo. Už sme vyskúšali všelijaké prípravky, no švábov sme sa nezbavili. Cez bytové jadro vždy dôjdu noví a tí, ktorí sú ďalšou generáciou po dospelých zdochýnajúcich šváboch, sú odolní voči tomu, čo zabilo ich rodičov. Chémia nepomáha, jedine, čo platí, je fyzika.
                 

                 
  
  Napríklad šváby nie sú odolné na tlak papuče, ktorá po nich pleskne. Má to iba jeden háčik. Tá papuča musí plesknúť po nich (najlepšie medzi oči), a oni majú veľmi radi zúžené priestory, kde sa papučou človek nedostane. Napríklad papučou nie je možné dostať sa na diódu mikrovlnky, na ktorej sa šváby radi vyhrievajú. Tá dióda je totiž pod sklom a takéto zabitie švába by bolo dosť drahé. Šváby našťastie nie sú odolné ani na zákon priľnavosti, a tak na nich napríklad platí aj lepidlo na myši. Pred niekoľkými dňami som sa rozhodol, že pre nich pripravím pascu. Na kartón som do stredu umiestnil kúsok klobásy a fašírky. Vedel som, že niečo také majú veľmi radi, pretože keď som si odrezal z fašírky a nechal som nôž pri mikrovlnke, po niekoľkých minútach som videl, ako od noža zutekal šváb, ktorému bolo ľúto neoblízaných zvyškov fašírky na noži. Tento kartón som večer položil vedľa ich hlavného stanu, a do rána sa "chytilo na lep" 25 švábov. Po troch dňoch ich tam je prilepených okolo 60 - malých i veľkých. Nedávno som sa dozvedel, že vraj neprežijú mráz pod mínus šesť stupňov. V zime to určite vyskúšam. Nech už tie potvory teraz počítajú s tým, že ich hlavný stan - mikrovlnka, sa na niekoľko dní ocitne vonku na balkóne. Aby ste mali predstavu o tomto malom, prítulnom zvieratku, ktoré tu bude aj vtedy, keď my tu už nebudeme (v prípade atómovej vojny), vyfotografoval som ho pre vás aj s priloženou mincou, aby ste mali predstavu aj o jeho veľkosti. PS.Dúfam, že ste už po obede, nerád by som vám pokazil chuť do jedla. :-)

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (69)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Fico vymetá všetky kúty aj v osadách...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Budú Fica oblizovať aj zozadu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Napíš europoslancovi, nech neblbne...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Ukrajina potrebuje EÚ ale aj EÚ potrebuje Ukrajinu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Červeň 
                                        
                                            Skončí postávanie pred kostolom?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Červeň
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Červeň
            
         
        cerven.blog.sme.sk (rss)
         
                        VIP
                             
     
        Mám rád život s Bohom a 7777 ľudí. S obľubou lietam v dobrej letke, no nerád lietam v kŕdli. Som katolícky kňaz, pochádzam z dediny Klin na Orave. Farár vo farnosti Rakúsy. (pri Kežmarku)
...........................................


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1475
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3095
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            BIRMOVANCI
                        
                     
                                     
                        
                            ŠTIPENDISTI
                        
                     
                                     
                        
                            7777 MYŠLIENOK PRE NÁROČNÝCH
                        
                     
                                     
                        
                            Aké mám povolanie?
                        
                     
                                     
                        
                            Akú mám povahu?
                        
                     
                                     
                        
                            Cesta viery
                        
                     
                                     
                        
                            Duchovné témy
                        
                     
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Fotografie prírody
                        
                     
                                     
                        
                            Fotografie Rómov
                        
                     
                                     
                        
                            História mojej rodnej obce
                        
                     
                                     
                        
                            Kniha o Rómoch
                        
                     
                                     
                        
                            Lunikovo
                        
                     
                                     
                        
                            Mladí vo firme
                        
                     
                                     
                        
                            Náčrt histórie slov. Saleziáno
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Pastorácia Rómov
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Pokec s Bohom
                        
                     
                                     
                        
                            Rómovia
                        
                     
                                     
                        
                            Sociálna práca
                        
                     
                                     
                        
                            Sociálna náuka Katolíckej cirk
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Vedeli sa obetovať
                        
                     
                                     
                        
                            Vzťah je cesta cez púšť
                        
                     
                                     
                        
                            Zábava
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Štipendium pre chudobné zodpovedné dieťa
                                     
                                                                             
                                            Prečo si firmy neudržia mladých ľudí?
                                     
                                                                             
                                            Dejiny mojej dediny
                                     
                                                                             
                                            Vzťah je cesta cez púšť
                                     
                                                                             
                                            Akú mám povahu?
                                     
                                                                             
                                            7777 MYŠLIENOK PRE NÁROČNÝCH
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Sociálny kódex Cirkvi - Raimondo Spiazzi
                                     
                                                                             
                                            Sociálna práca - Anna Tokárová a kolektív
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Free Download MP3 ruzenec
                                     
                                                                             
                                            MP3 ruženec
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Krištofóry - katolícky liberál
                                     
                                                                             
                                            Peter Herman - realistický idealista
                                     
                                                                             
                                            Michal Hudec sa nebojí písať otvorene
                                     
                                                                             
                                            Mária Kohutiarová - žena, matka, človek
                                     
                                                                             
                                            Branislav Sepeši - lekár o etike
                                     
                                                                             
                                            Katrína Janíková - neobyčajne obyčajná baba
                                     
                                                                             
                                            Martin Šabo - šikovný redemptorista
                                     
                                                                             
                                            Juraj Drobny - veci medzi nebom a zemou
                                     
                                                                             
                                            Peter Pecuš a jeho postrehy zo života
                                     
                                                                             
                                            Miroslav Lettrich - svet, spoločnosť, hodnoty
                                     
                                                                             
                                            Karol Hradský - témy dnešných dní
                                     
                                                                             
                                            Salezián Robo Flamík hľadá viac
                                     
                                                                             
                                            Salezián Maroš Peciar - fajn chlap
                                     
                                                                             
                                            Motorka a cesty: Martin Dinuš
                                     
                                                                             
                                            Ďuro Čižmárik a jeho mozaika maličkostí
                                     
                                                                             
                                            Paľo Medár a scientológovia
                                     
                                                                             
                                            Pavel Škoda chodí s otvorenými očami
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Modlitba breviára
                                     
                                                                             
                                            Nezabíjajte deti!
                                     
                                                                             
                                            Občiansky denník
                                     
                                                                             
                                            Dunčo hľadaj
                                     
                                                                             
                                            Správy z Cirkvi
                                     
                                                                             
                                            Prvý kresťanský portál
                                     
                                                                             
                                            Lentus
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Voľby nemožno vyhrať kúpenými hlasmi najbiednejších Rómov!
                     
                                                         
                       Jednoduchý rozdiel medzi kandidátmi na prezidenta.
                     
                                                         
                       Doprajme Ficovi pokoru!
                     
                                                         
                       Fico vymetá všetky kúty aj v osadách...
                     
                                                         
                       tam kam Fico nemôže, TA3 mu pomôže
                     
                                                         
                       Vďakyvzdanie
                     
                                                         
                       Treba homofóbiu liečiť?
                     
                                                         
                       Procházka, Kňažko, Kiska... a jediný hlas
                     
                                                         
                       Napíš europoslancovi, nech neblbne...
                     
                                                         
                       Prezidentské voľby: Prečo budem voliť Procházku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




