

 Streda: 9. júna 2010, krásne slnečné počasie.... 
   
 Na rozcvičenie som sa rozhodol, že zo Starého Smokovca na Hrebienok nepôjdem zubačkou ako drvivá väčšina turistov, ale pekne krásne peši. Táto cesta trvá približne 35 minút takým svižnejším tempom a kedysi - pred výchricou - to bola krásna prechádzka lesom, teraz žial je to už len prechádzka niečim čo lesom kedysi bolo. Ako som sa tak škriabal na Hrebienok uvažujúc o tom, či nejdem moc rýchlo, aby som sa neunavil ešte pred samotnou túrou, prebehli okolo mňa zrazu akýsi športovci, ktorí sú v Tatrách pravdepodobne na sústredení - a to prebehli myslím do slova. No na môj hlasný komentár „A to som si myslel, že ja sa ponáhľam" sa vrámci šetrenia dychom zmohli len na ťažko zrozumiteľné zvuky, takže som sa nedozvedel čo im stálo za toľku námahu. Pokračoval som teda ďalej rozmýšľajúc, či sa naozaj cítim na Zbojnícku chatu. Vedel som, že z Hrebienka trvá cesta 2 hod a 50 min, no a napokon som si povedal, že kondične som na tom dobre a teda ide sa. Zbojnícka chata sa nachádza vo Veľkej studenej doline vo výške 1960 m. A cesta k nej sa rozhodne oplatí, už len kvôli nádherným scenériam, ktoré človek jednoducho len tak niekde neuvidí. Nebudem už zbytočne kvákať, pozrite si aké obrázky sa mi naskytli počas cesty. 
 Ak som postupoval správne na obrázok môžete kliknúť a mala by sa vám zobraziť jeho zväčšená verzia. Tak dúfam, že tieto technické prvky som zvládol úspešne - prijemné prezeranie. 
   
   
   
   
  
   
   
  
   
  
   
 Vidíte dobre, naozaj na niektorých miestach cez chodník pretekala voda a tade sa suchou nohou prejsť nedalo. Bez naozaj dobrej obuvy sa táto túra určite nedá zvládnuť. 
   
  
   
  
   
  
   
   
  
   
   
  
   
   
  
   
  
   
 nádherná čistá vodička... 
  
   
  
   
  
   
  
   
   
 Keďže fotiek je pomerne dosť a prílohový manažér nie je až taký veľký ako som si želal rozdelil som tento miničlánoček, práve kvôli fotkám na tri časti. Ak som niečo prehliadol nech ma administratori prosím opravia a napíšu mi, ako som všetky fotografie mohol dať do jedného článku. 
   
   
 Autor: Mgr. Pavol Minarčák 
 Foto autor: Mgr. Pavol Minarčák 
   

