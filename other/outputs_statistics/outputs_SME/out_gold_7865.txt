

 Hladina rieky Potočnica začala nezadržateľne stúpať a všimli si to všetci až na slepého Braňa, ktorý sa tváril, že sa ho nič netýka. Pred časom sa začala odluka dediny od kostola a pán farár Modlivka začal uvažovať ako odvrátiť tváre Nerozumničanov od vlastného života a donútiť ich aby mu zvýšili návštevnosť svätostánku. Pravda aj na kostole bolo roboty jak na Kremli a on sám to s kostolníkom a zvonárom v jednej osobe, Hrbáčikom nezvládal. V skutočnosti nešlo zo strany Modlivku ani tak o pohnútky duchovné ako skôr čisto svetské. Modlivka bol egocentrický človek a zameranie pozornosti druhých na seba, považoval za Božiu vôľu. A záplavy boli práve tou najlepšou príležitosťou na zviditeľnenie sa a prípadné zásluhy. 
 Voda v rieke stúpala aj naďalej. Situácia začala byť aj trochu nebezpečná, lebo silný prúd vody strhol všetky mosty. Všetci sa tvárili, že nič aby nemuseli ani len prstom pohnúť na opravu. Všetci sa tvárili, akoby nič aj napriek tomu, že slepého Braňa, ktorý chcel prejsť cez most pochytila veľká voda a zaniesla ho až do Čierneho mora. 
 Až nakoniec, voda zatopila pivnicu rodiny Tĺčikovcov. Problém bol, že Tĺčikovci už dlhé roky z neznámej príčiny obývali vo svojom dome len a len pivnicu. Práve obedovali, keď ich kuchyňu, obývačku a spálňu v jednom, navštívila rieka Potočnica. Našťastie pre nich, starý Tĺčik vedel plávať motýlika, pretože to raz videl v televízii. Zachránili sa aj za cenu toho, že im voda odniesla obed na, ktorý sa celý deň tak tešili. 
 Po tejto udalosti už starosta nemohol naďalej zatvárať oči pred skutočnosťou a odišiel niekam k moru na dovolenku. Poverenie zachrániť situáciu s radosťou odovzdal farárovi Modlivkovi. Ten sa pustil hneď do práce. Dal zhromaždiť všetky svoje ovečky. Zástup ľudí sa teraz pozeral na súvislú vodnú hladinu z ktorej sem-tam vytŕčali strechy domov či telegrafné stĺpy. Pán farár sa postavil pred ľudí a dlho sa na nich majestátne díval. Potom hromovým hlasom zvolal ponad hlavy prizerajúcich sa: 
 - Bratia a sestry, náš Pán ma povolal do svojich služieb, aby som vás prostredníctvom jeho moci, suchou nohou previedol cez túto vodu! 
 Od kostolníka Hrbáčika, ktorý sa pri ňom krčil si vzal palicu a z udrel ňou po vodnej hladine. To čo nasledovalo sa dalo tak trochu očakávať. Až na mierne rozčerenie vody sa vskutku nestalo nič. Tak isto sa to opakovalo asi desať krát, vždy sprevádzané väčším tlmením smiechu. Krčmár Moše, to už nevydržal a spýtal sa pána farára: 
 - Čaká vaše blahorodie až tá voda upadne do bezvedomia? Ak by ste ju chceli zabiť, môžem ísť domov po sekeru a pomôcť vám! 
 - Myslíš si syn môj, že vás chcem oklamať? -spýtal sa Modlivka začudovanie. - Si slabý vo svojej viere Moše a mal by si sa hanbiť! 
 - Ale ja vám verím vaše blahorodie, o tom niet pochýb len sa akosi nič nedeje. Nič. Ani modriny ste tej vode nespravili. - sklopil hlavu krčmár Moše. 
 - Ešte je jedna možnosť! - vykríkol víťazoslávne farár. 
 Vyzul si topánky a pokúsil sa chodiť po vode. Akosi mu to nešlo a neustále sa prepadal hlbšie a hlbšie. Stále sa snažil postaviť na hladinu, ale vždy len čľupol do kalnej vody. Už bol celý zúrivý, skoro sa až triasol. Niekto z prizerajúcich sa ho spýtal: 
 - Pán farár, aká je voda? 
 - Studená... - zahundral Modlivka skleslo. 
 Zástup sa mu otočil chrbtom a všetci šli po vrecia s pieskom, vedrá a iné pomôcky na  záchranu toho čo sa ešte dalo zachrániť. 
   

