
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Horňák
                                        &gt;
                Kresťanstvo
                     
                 Čo ak je Boh iný? 

        
            
                                    18.6.2010
            o
            13:26
                        (upravené
                31.10.2011
                o
                17:25)
                        |
            Karma článku:
                4.64
            |
            Prečítané 
            358-krát
                    
         
     
         
             

                 
                    Už tisíce rokov nám mocní ponúkajú obrazy  Boha ako  nezničiteľné a nesmrteľné charakteristiky. Obrazy Pána dejín  i vesmíru,  najmocnejšieho, najsilnejšieho, najvznešenejšieho vládcu,  kráľa,  prvotného hýbateľa všetkého, spravovateľa tejto zeme, udeľovateľa  moci  náboženskej i svetskej, spravodlivého sudcu a niekedy sa spomenie  aj tá  nekonečná láska. Čo ak to ale môže byť všetko inak? Čo ak sám Boh  cez  mocných a víťazov hovoriť nechce?  Čo ak Boh hovorí cez slabých   a vytesnených?
                 

                 
  
      Nie je Boh snom  o nesmiernej moci? Existuje nejaká trvalejšia prítomnosť moci než je  pojem Boh, pod ochranu ktorého sa utiekajú všetky náboženské mocnosti?  Existuje niečo, čo by silnejšie podporovalo nastolený poriadok, niečo  direktívnejšie a neohybnejšie než je náboženstvo a Boh náboženstva? Nie  je samotná idea Boha ako Pána vesmíru základným modelom každej pozemskej  patriarchie? Nebola často modelom teokratického teroru? Bolo niečo  krvavejšie, patriarchálnejšie, hierarchickejšie, autoritatívnejšie,  inkvizičnejšie, kolonialistickejšie a militaristickejšie?   Čo ak opustíme model Boha ako jediného všemohúceho Otca, jediného  kráľa, ktorý vládne Zemi v prospech modelu Boha, ktorý sa vyvlieka  z prospechárstva moci? Čo ak Boh nechce patriť k poriadku moci, ale skôr  sa odpútava od sveta aby sa usídlil vo všetkom čím svet pohŕda? Čo ak  si Boh stavia prístrešok najmä medzi bezdomovcami aby ani On nemal kde  skloniť hlavu?   Čo ak sa náboženstvo a teológia mýlia, pretože sú príliš ľudské  a systematicky smerujú k tomu, aby na Boha zabudli a radšej uzavreli  spojenectvou s mocou silnej a pompéznej teológie? Čo ak má teológia  smerovať presne naopak? K popretiu moci a smerom k slabosti? Čo ak je  Boh náboženstva brzdou skutočnému obraz Boha?   Podľa Izaiáša(1,11-17), Amosa(5,21-24) a Ozeáša(6,6) sa Boh zaobíde  bez náboženstva, ak je náboženstvom len kultová obeta a rituál, no  nezaobíde sa bez spravodlivosti a milosrdenstva, ktoré nie sú vždy tým,  po čom náboženstvo túži.   Čo ak Boh nie je prvým nehybným hýbateľom fyzického pohybu, ale  spravodlivosti? Čo ak nepohybuje silou, ale skôr príťažlivosťou, ktorá  nás ťahá k sebe? Čo ak by Boh nebol zvrchovaný vladár nad nebeskou  klenbou, ale bol by ten kto poriadok narúša práve kvôli cieľom  spravodlivosti? Čo ak by Boh nebol pevnou skalou na ktorej stojí  teologicko-hierarchická stavba zvrchovanosti, ale bol by ten kto sa  systematicky spája s inými, s vyhnancami, outsidermi, s tými čo sú  znivočený a zgniavený(Amos 8,4)?   Čo ak by bol preto Boh akýmsi revolučným impulzom a nie ako stabilné  centrum spoločnosti? Čo ak je cieľom Boha neustála konfrontácia  s nastoleným poriadkom, s ľudskými sklonmi zmýšľania, s autoritou  človeka nad človekom, muža nad ženou, človeka nad zvieratami a samotnou  zemou?   Čo ak o ňom prestaneme uvažovať ako o základe politickej  zvrchovanosti, ako o udržiavateľovi nastoleného poriadku? Čo ak by bol  ten, ktorý narieka za slobodou, ktorá sa v práve, vládach a národoch  vytratila? Čo ak by bol ten, ktorý neupevňuje, ale pohýna inštitúciami  a štruktúrami, aby ich udržal spravodlivými?   Čo ak je skutočný Boh skrytý za nánosom Boha náboženstva a pompéznej  teológie, ktorý je modlou, vyrytým obrazom, nástrojom inštitucionálnej  moci, morálnej melanchólie a konfesionálneho rozdelenia?   Čo ak je Boh ten čo v tichosti pulzuje ako srdce necitlivého sveta?  Čo ak by nebol všetkým po čom túžime, ale všetkým čo túži po nás,  všetkým čo nás vyťahuje von z nás samých, čo nás volá k tomu  najlepšiemu, čo je v nás? Čo ak by bol ten čo nás žiada aby sme opustili  naše ľudské spôsoby bytia a žili veľkoryso, žili a nechali žiť,  milovali a nechali milovať, aby sme jednoducho žili nepodmienene?   Obraz tohto Boha so starým Bohom vytvára iskry anarchie, no posvätné  iskry a posvätnú anarchiu. V novom zákone znamená svet držbu skutočnej  moci tohto sveta, ktorému má čeliť Božie kráľovstvo, ktoré je medzi  nami. Svet je to, čo skutočne existuje, zatiaľčo Božie kráľovstvo volá  po niečom inom. Svetu zaleží na zvyčajnom chode pre mocných  a zvýhodnených , záleží mu na nanútenom poriadku a buduje si bohatstvo  na chrbtoch opovrhnutých a odlišných. Božie kráľovstvo nesie v sebe tú  posvätnú anarchiu, ktorá volá po niečom, čo je protikladné s týmto  svetom.   Aj keď...čo ak je to všetko ešte inak?             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Horňák 
                                        
                                            Meditovať denne? Načo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Horňák 
                                        
                                            Tri jednoduché spôsoby ako sa naučiť sústrediť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Horňák 
                                        
                                            Lucidné sny - realita skrytá za našimi snami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Horňák 
                                        
                                            Kam smeruje kresťanstvo?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Horňák 
                                        
                                            Zastavte sa, sadnite si a pozerajte sa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Horňák
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Horňák
            
         
        michalhornak.blog.sme.sk (rss)
         
                                     
     
        introvert.intuitivny.mysliaci.pozorujúci.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    70
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    693
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Katolícka Cirkev
                        
                     
                                     
                        
                            Kresťanstvo
                        
                     
                                     
                        
                            Evolúcia
                        
                     
                                     
                        
                            Sexualita
                        
                     
                                     
                        
                            životný štýl
                        
                     
                                     
                        
                            Myšlienky
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Patríte aj vy ku generácii strachu?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




