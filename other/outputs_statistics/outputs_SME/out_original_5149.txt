
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Špurek
                                        &gt;
                kresťanské umenie
                     
                 Tri devy na troch vrchoch 

        
            
                                    24.4.2010
            o
            0:26
                        |
            Karma článku:
                0.27
            |
            Prečítané 
            275-krát
                    
         
     
         
             

                 
                    Možno by som mal rozdeliť na troch ľudí, všetci by - som rozpažil ruky a všetci by – som kráčal tromi lúkami, tromi šľapajami podkúvať tri svety. Tri krát by sa zablískalo, tri krát by aspoň zaiskrilo a hral by Vivaldy, ale tri koncerty.
                 

                 Tak bež alebo sa rozbehni alebo niečo hoď. Len nestoj, nestoj, len nespor na horší čas a daj mi niečo pekné. Tri krát denne utekám, lebo viem, aká vzácna je krása. Aspoň tak ako pokoj a ku nim ten cit. Ale ten nechcem prezraziť, ani zatajiť, tak možno prípadne žiť.       Všetko natrojo, keď nie je ani raz. Je to fatálna diléma, je to myslené bytie, je to teraz.   Horská lúka a dve úbočia. Tri tmy, tri oddelenia od možnosti niekam ísť, prísť a byť objatý. Sú aspoň tri slnká nad tými stráňami. Každý chce mať svoje, skryjem tie dvoje a jedno si nechám, kým mi uniká.       Horská lúka a tri kvety. Cuzie deti v podobe troch stromov. Trochu vody do troch kvapiek.       Trochu písať, trochu čarbať, len neodložiť atrament. Tri stromy v jednom vetre, spektrum farieb a biele svetlo.                      Čo nikdy neuhasím smäd? A pri menuete na chvíľu na to zabudnúť, tri kroky vpravo, tri kroky vľavo, tri kroky doprostred. Občas vymeniť menuet za smäd.       Na tú horskú stráň by prišli tri devy, tri krát by zatlieskali, dva razy sa pousmiali a raz zakývali. Lebo počet nikdy nepustí, je neúprosný a raz nás zničí. Keď odídu, ostanú mi tri kríže nad stráňou ukryté, jemne zaryté, drevené. Ten prostredný je tretí, je nežný, je menší.       Ako sa traja zmestíme na jednu stráň, všetko je malé, nikdy nič nestačí a všetko sa hýbe. Mocný Bože, vyšší ako kríže, povznesený, nikdy mi nedaj zahynúť. A predsa tu nechcem chodiť večne. Všetko sa uberá vyššie ako som ja.                  Ak chcem ostať pravdivý, vezme ma to so sebou. Budem sám, jedno nezdobné rúcho, opustené telo. Predpovedať zánik na krásnej stráni, to patrí k sebe. To je to, čo som už napísal. Hlas organu.       Tá lúka pravdaže nie je moja, je alpská, je cudzia. Mnoho rázy som rozmýšľal, hoci tu sa to nepatrí, či naše lúky.... ťažká spomienka, blízka veža kostola, známe domy. Bol by som viac sebou, keby som tam bol, alebo snáď je to jedno? Neviem. Nikdy sa to už nedozviem. Preto traja výjdeme na tri cudzie lúky, ale ani na jednu svoju.       Človek je tak naplno sám, hoci kráča ako traja. Jednými očami pozerám a tromi pármi, svojskými očami. Samota je asi taký dar ako pravda, ako voda. Je a nie je, je všade a uniká. A keď zahrmí, zas spadne. A prídu tri iskry. Všetko sa vo mne obnoví, niečo zostane, a z čohosi ubúda. Len žiaľ je ako kameň, ako skala, ako pietra seréna. Dobrý na sochy.       Len jedny nohy, krájajúce krajinu na tri, jeden Boh a jedna Biblia. Do seba som zavrel tri priepasti, ba aj seba, iba Boh odchádza. A zase príde v tej istej sekunde rozhovoru. A písania.       Stráň sa nemení, je už navždy taká, navždy tam stojím, navždy sa nehýbem.       Tri iskry ma blažia, sú len moje, tri perly, tri nábrežia. A vrchovatosť snov a rozprávok. Vrchovatý Boh nado mnou. To je tichá modlitba, nepravá extáza a Duch vo mne. A ak príde po úvodnej radosti plač, tak má to tak byť. Aby sa to mohlo otočiť, pootočiť, zameniť tvár zeme.       Krývam za modlitbou a hoci tromi pármi nôh, krývam. Bolia ma kolená, ťažko kľakám, ale ťažko sa dvíham. Takto si predstavujem príbeh olivovej hory, ale bez kalichu a bez anjela.                                                      + 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Super vec! Poďte to skúsiť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Svetlo Ježiša Krista premôže tmu! Určite!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Moje telo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Dojatie je mŕtve
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Špurek 
                                        
                                            Cyril a Metod, Róbert Bezák
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Špurek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Špurek
            
         
        spurek.blog.sme.sk (rss)
         
                                     
     
        Kresťan. Človek fascinovaný Ježišom Kristom. Tvorca a konzument umenia. Doktorand...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    75
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    384
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            literatúra
                        
                     
                                     
                        
                            kresťanské umenie
                        
                     
                                     
                        
                            galéria u Maxa
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľudmila Onuferová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            des Jano
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




