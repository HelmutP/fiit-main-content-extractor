

   
 ...bol to domov. Tak som si trochu zaspomínal...že na čo sa dobre pamätám. 
 ...pamätám sa, ak som v škôlke nemohol ísť na výlet do Bojníc lebo som pohrýzol spolužiaka. 
 ...ako som každú sobotu ráno chodil s otcom na bazén, vždy som sa vyhováral, nikdy sa mi nechcelo a keď to skončilo, bolo mi za tým smutno. 
 ...na moju prvú pani učiteľku (vďaka ktorej môžem písať aj svoj blog, patrí jej ďakujem), ktorá mi písala poznámky lebo som sa bil s cigáňmi a ja som jej vždy k zrkadielku (tam sa písali poznámky) pribalil jeden Bonpar lebo som si myslel, že to zmierni môj trest. 
 ...na poznámke, ktorú som dostal lebo som počas vyučovacích hodín chodil príliš často na toaletu. 
 ...na skvelé desiaty od mojej mamy, ktoré mi vždy závidela celá trieda a teraz mi neskutočne chýbajú. 
 ... na pohovor v riaditeľni lebo som spolužiačke napchal do termosky rožok. 
 ...ako som na narodeniny dostal prvého Harryho Pottera a odvtedy som každé ráno chodil do školy na imaginárnej metle. 
 ...ako sme išli s triedou cez park a mňa hrabľami naháňal ujo, ktorý zhŕňal lístie lebo som mu ho rozkopal. 
 ...ako zvolili Fica, on utvoril koalíciu so Slotom a Mečiarom a ja som už vtedy vedel, že je zle a celú noc som usedavo plakal. 
 ...ako som s bratom hrával hokej na terase domu, vždy to skončilo hádkou a vždy sme nakoniec hrali znovu. 
 ...ako sa otec na mňa rozčuľoval, keď som jazdil na lyžiach po lese. 
 ...ako som utekal dedovi na lyžiach, naháňal ma, lebo som mu vyplazil jazyk. 
 ...ako som si v noci hrkol oleja lebo som si myslel, že je to čaj. 
 ...ako som pozdravil rodičov na letisku a otec mi povedal, že v máji ma tam budú čakať. 
 ...ako na ten máj stále čakám a je stále bližšie. 
 ...na toto som si spomenul, keď som včera zisťoval, čo mi chýba. 
   

