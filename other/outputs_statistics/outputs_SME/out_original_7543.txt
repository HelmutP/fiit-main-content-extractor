
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Šášky
                                        &gt;
                Futbal
                     
                 Po prípravnom zápase (zrejme) známa brankárska trojica 

        
            
                                    29.5.2010
            o
            20:00
                        (upravené
                30.5.2010
                o
                1:11)
                        |
            Karma článku:
                2.63
            |
            Prečítané 
            578-krát
                    
         
     
         
             

                 
                    Slovenská futbalová reprezentácia odohrala druhý prípravný zápas pred majstrovstvami sveta vo futbale v Juhoafrickej republike. Na neďaleký Klagenfurt prišlo zopár áut i autobusov, vidieť údajne bolo aj autá z košickou poznávacou značkou, čo by mohlo trochu kopnúť usporiadateľov zápasov slovenskej reprezentácie.
                 

                 Slovenská futbalová reprezentácia mala totiž dlho domáce prostredie v hlavnom meste Slovenskej republiky, na Tehelnom poli. Trávnik a štadión sa však rozpadal priamo počas zápasov a naberal čoraz viac podoby múzijného formátu.   Staručké Tehelné pole už malo byť niekoľko mesiacov zrútené, keď na ňom hrala posledný zápas slovenská reprezentácia zápas proti USA (1:0). Trávnik zápasu neprial, akcie sa veľmi tvoriť nedali. Brankárom sa však na trávnik padalo určite lepšie, ako dnes v Klagenfurte...   Aby som však hneď neodbiehal do Rakúska, dokončím myšlienku. Domov v Bratislave na Tehelnom poli sa ide rekonštruovať a Weissova družina si vyhliadla nový domov. Krásny žilinský stánok, teda štadión terajšieho majstra Slovenska im bol taký akurát.   Hralo sa na západe, hralo sa na strednom Slovensku. Avšak fanúšikovia východného Slovenska si za kvalitnými zápasmi našej reprezentácie musia cestovať. Ako museli hľadať cesty do Žiliny pri prvom prípravnom zápase proti Nórsku, svoje cesty si našli aj do pre nich ďalekého Klagenfurtu.   Fanúšikovia z východu tak už prejazdili mnohé kilometre a na zaujímavé zápasy musia čakať podľa výkonov klubu. Naposledy im klub z východného Slovenska pripravil zaujímavý zápas MFK Košice verzus AS Rím. To však bolo v auguste minulého roku. Odvtedy na veľké zápasy musí východoslovenský fanúšik cestovať.   Posledný prípravný zápas proti Kostarike (5.jún) sa bude hrať na pôde bratislavských Pasienkov, a tak východ Slovenska prišiel o ďalší zápas, čo sa mohol hrať u nich. Prečo sa nehrá tento zápas v Košiciach? „My sme skutočne vážne uvažovali nad odohratím dokonca aj kvalifikačného zápasu s Českom na východe Slovenska, ale niektoré logistické a iné technické problémy nám tento plán znemožnili. Pokiaľ to však bude možné, reprezentanti sa určite prídu ukázať aj na východné Slovensko,” povedal pre Futbal magazín (4) generálny sekretár SFZ Miloš Tomáš.   Naposledy cestovali približne 800 kilometrov, aby dnes o 14:00 videli, ako sa stretne slovenská reprezentácia s tou kamerunskou. Tvrdý terén v rakúskom Klagenfurte hráčom a hre na kvalite hry neprial, práve naopak.   Prípravný zápas s Kamerunom ako prípravný veľmi nevyzeral. Kamerun skôr trénoval filmovanie, čo si však rozhodca neuvedomoval a kamerunským hráčom pripískol na všetky pády. Rozhodcovi, Rakúšanovi Lechnerovi, boli podľa môjho skromného názoru, počas celého zápasu sympatickejší hráči Kamerunu, našťastie to ale na zápase nenechalo veľké stopy.   Ako povedal i tréner Vladimír Weiss, obrana hrala prakticky bez chýb. Vyriešila sa zrejme aj brankárska otázka a do Afriky tak asi pocestuje trojica: náš záchranca Ján Mucha, „Weissov kôň” Ľuboš Kamenár a dnešný náhradník Dušan Perniš. Napriek tomu, že stav Dušana Kuciaka ešte nie je presne jasný.   Ťažko sa tiež dá hovoriť aj o Miroslavovi Karhanovi. Či sa mu jeho achillovka do šampionátu vylieči ťažko predpokladať, rozhodujúce by mali byť dnešné vyšetrenia. Každopádne, dnešní hráči, ktorí hrali na poste defenzívneho záložníka, čiže Kamil Kopúnek a Juraj Kucka, rozhodne nesklamali.   Nesklamali ani ostatní záložníci. Musím uznať, že sa mi páčil Miňo Stoch a dokonca aj hráč, ktorého až tak moc nemám rád, Vladimír Weiss ml. Naopak, ťažko hovoriť o Šventovi, ktorý toho dnes veľa neukázal a hlavne ťažko hovoriť o útoku. Tam sa vynikajúco ukázal Stanislav Šesták, ktorý dnes napol kamerunskú sieť dvajedenkrát.   Erik Jendrišek však ničím neoslnil a Martin Jakubko tiež. Róbert Vittek a Filip Hološko sú polopripravení hrať zápas. Do úvah však, ako povedal Vladimír Weiss, stále pripadajú buď Ján Novák alebo Jakub Sylvestr. „Jakub Sylvestr proti chorvátskej dvadsaťjednotke nabehal trinásť kilometrov a o tom je dnešný futbal - o pohybe,” prezradil, čo je pravdy na fámach o Jakubovi Sylvestrovi, reprezentačný tréner Vladimír Weiss.   KOHO BY SOM BRAL DO AFRIKY JA? Ak by som sa odrážal od predpripravenej nominácie Vladimíra Weissa, z týchto štyroch brankárov by som bral trojicu Mucha - Perniš - Kuciak, no pri poslednom menovanom by rozhodoval jeho zdravotný stav. Ďalej by som nominoval sedem obrancov, deväť záložníkov a štyroch útočníkov. V Rakúsku je na sústredení deväť obrancov: Peter Pekarík, Martin Petráš, Martin Škrteľ, Ľubomír Michalík, Ján Ďurica, Radoslav Zabavník, Marek Čech, Tomáš Hubočan, Kornel Saláta. Netajím sa tým, že som proti tomu, aby bol Martin Petráš nominovaný do Afriky a vôbec na nejaký zápas slovenskej reprezentácie. Zo zvyšných hráčov by som do Afriky nebral buď Ľubomíra Michalíka alebo Kornela Salátu. Michalík je typ podobný Martinovi Škrteľovi, ale vysokí hráči by sa nám v Afrike zišli. Kornel Saláta je zase hráč Corgoň ligy a na taký veľký futbal, ako je v Afrike, asi nebude mať dostatočne veľa síl. Avšak, v Poľsku patril medzi najlepších hráčov a dokazuje, že si miesto zaslúži, navyše vie vynikajúco hlavičkovať a vie hrať i na mieste defenzívneho záložníka. A je Slovanista, to je zase pre mňa plus...   Stredopoliari na sústredení v Rakúsku sú Kamil Kopúnek, Ján Kozák ml., Juraj Kucka, Miroslav Karhan, Marek Sapara, Mário Pečalka, Marek Hamšík, Vladimír Weiss ml., Miroslav Stoch, Dušan Švento a Zdeno Štrba. Tých je jedenásť a tiež bude treba škrtnúť dve mená. Štrba patrí medzi skúsených záložníkov a i keď sme ho v zápase proti Kamerunu nevideli, určite má svoje miesto v reprezentácii. Takisto si neviem predstaviť MS ani bez Stocha, Weissa, Hamšíka, Kozáka, či Karhana. Žiaľ, posledne menovaný sa nenominuje asi sám. Preto by som škrtol Mária Pečalku, ktorý je asi najmenej pripravený na MS v Afrike a jedného z dvojice Švento - Karhan. Podľa zdravotného stavu Miroslava Karhana. Švento by na MS byť nemusel, na kraj zálohy by sme vedeli dosadiť i jedného z dvojice útočníkov Šesták-Jendrišek.   Útočníci na MS v Afrike. To je špeciálna kategória. Erik Jendrišek, Róbert Vittek, Martin Jakubko, Filip Hološko, Stanislav Šesták. Jendrišek proti Kamerunu nenadchol, Jakubko tiež, Vittek a Hološko nie sú pripravení naplno. Vittek a Šesták sú hráči, bez ktorých si MS neviem predstaviť, Jakubko a Jendrišek sú pre mňa dôležití tiež. Viem si predstaviť MS bez Filipa Hološka, ale na druhej strane aj bez Martina Jakubka. Žeby sa predsalen donominoval Jakub Sylvestr? Aj keď to tak nevyzeralo počas sezóny, góly dávať vie...   MOJA NOMINÁCIA NA MS: Ján Mucha, Dušan Perniš, Dušan Kuciak - Peter Pekarík, Radovan Zabavník, Martin Škrteľ, Ján Ďurica, Kornel Saláta, Tomáš Hubočan, Marek Čech - Kamil Kopúnek, Juraj Kucka, Zdeno Štrba, Marek Sapara, Marek Hamšík, Miroslav Karhan (D. Švento), Miroslav Stoch, Vladimír Weiss, Ján Kozák - Stanislav Šesták, Róbert Vittek, Erik Jendrišek, Martin Jakubko, Jakub Sylvestr.   KOHO PODĽA MŇA ZOBERIE WEISS? Okolo tohto omáčku robiť nemôžem. Nevidím do mysle pána trénera. Tipnúť si ale môžem, takže:   Ján Mucha, Ľuboš Kamenár, Dušan Perniš - Peter Pekarík, Radovan Zabavník, Martin Škrteľ, Ján Ďurica, Ľubomír Michalík, Tomáš Hubočan, Marek Čech - Kamil Kopúnek, Juraj Kucka, Zdeno  Štrba, Marek Sapara, Marek Hamšík, Dušan Švento, Miroslav  Stoch, Vladimír Weiss, Ján Kozák - Stanislav Šesták, Róbert Vittek, Erik Jendrišek, Martin Jakubko, Filip Hološko. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Pekarík potešil mladých slovenských futbalistov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            3 veci, ktoré chýbajú Slovanu k priemernému európskemu mužstvu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Prišlo na moje slová, komentovať bude Slávo Jurko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Keď aj futbalový fanúšik pociťuje hrdosť, že je Slovák
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Šášky 
                                        
                                            Weiss nadáva na fanúšikov, fanúšikovia na Weissa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Šášky
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Šášky
            
         
        sasky.blog.sme.sk (rss)
         
                                     
     
        Večne meškajúci maniak do športovej novinárčiny, s extrémne vyvinutým zmyslom pre ironické a sarkastické poznámky, predseda vlastnej kvázipolitickej strany SPI a človek prácu vykonávajúci vždy na 100% (pondelok 12%, utorok 23%, streda 40%, štvrtok 20% a piatok 5%...)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    139
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    980
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            MS v hokeji 2011
                        
                     
                                     
                        
                            MS v hokeji 2012
                        
                     
                                     
                        
                            Iné športy
                        
                     
                                     
                        
                            Z môjho nudného života
                        
                     
                                     
                        
                            Televízor
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Mono pohľad na to čo vidím keď nevyzriem natoľko,
                     
                                                         
                       3 veci, ktoré chýbajú Slovanu k priemernému európskemu mužstvu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




