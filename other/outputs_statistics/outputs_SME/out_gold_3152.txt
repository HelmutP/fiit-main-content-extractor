

   
 Cintoríny sú pietne miesta, pred ktorými by mali mať ľudia rešpekt, ale v obci Soľ, z ktorej pochádzam, tomu tak nie je. U nás totiž vandali poškodili 35 hrobov. Zničené a porozbíjané svietniky a náhrobné kamene, ktoré v dnešných časoch nie sú lacnou záležitosťou vyvolali pobúrene a nielen to, otázkou zostáva kto za to všetko bude niesť zodpovednosť a či sa vôbec vlastníci dočkajú náhrady. 
 Už v minulosti boli problémy, s krádežami a vandalizmom, ktoré sa pripisovali malým deťom z vedľajšej rómskej osady. No tentoraz to už je škoda omnoho väčšia. Páchatelia nie sú známi, a pozostalým nezostáva nič iné, ako neveriacky krútiť hlavou. 
 Je naozaj smutné, že zatiaľ čo my ľudia máme vlastné problémy, ani naši nebohí nemajú pokoj. Cintorín sa nachádza v bezprostrednej blízkosti rómskej osady a otázkou zostáva, či sa takéto výtržníctvo nebude v budúcnosti opakovať. 
 Pri strate blízkeho človeka, sa ľudia dlho vyrovnávajú s jeho odchodom a snažia si ho uctiť čo najdôstojnejším náhrobkom, a zničenie takejto pamiatky je nielen z finančného ale aj citového hľadiska veľkou ranou. 
 No najsmutnejšie na tejto situácii je to, že páchatelia si možno ani neuvedomujú čo svojím nerozvážnym konaním spôsobili. 
   

