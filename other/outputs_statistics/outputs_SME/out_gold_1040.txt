

 Med je sladká hmota produkovaná včelou medonosnou. Vlastne chcel som povedať včelí MED... Pretože čuduj sa svete, na Slovensku, ktoré je známe svojou včelárskou tradíciou máme viac medu ako MEDU.  Keďže som zo včelárskej rodiny, vysvetľovaním čo je to ten včelí med som strávil niekoľko rokov.   
 Tak tu máte pár overených rád ako spoznať kvalitu medu, s ktorými som sa počas mojej kariéry predajcu včelieho medu a amatérskeho včelára stretol:   
 1.      Predajná cena – pohybuje sa okolo   
 120sk za 1kg kvetového                                                             
 140sk za 1kg lipového                                                             
 140sk za 1kg agátového                                                             
 150sk za 1kg horského                                                             
 190sk za 1kg medovicového  
 Pozn. autora: Cena medu stále stúpa vplyvom jeho nedostatku. Môže sa objaviť dobrá duša, ktorá by včelí med dávala za nižšiu cenu (tzn. - 20sk a viac od hore uvedených cien) ale to už hraničí s extrémistickým poňatím kresťanskej lásky k blížnemu resp. s podvodníkom. Samozrejme hovoríme o cene, ktorá je bežne dávaná na trhoch a jarmokoch. Súkromne, ako sekundárny tovar, vo väčšom množstve alebo ak je predajca z blízkeho okolia – dá sa kúpiť aj lacnejšie. Podvodníci väčšinou dajú do neba volajúcu nízku cenu aby predali čo najviac a potom sa už neukážu.  
 2.      Značka, stálosť, dobré meno, súkromník = dobrá voľba 
 Snažte sa vyhliadnuť si v meste, v okolí vášho vlastného predajcu medu, u ktorého si budete pravidelne kupovať. Pokúste sa zistiť či vlastní včely alebo je iba výkupca resp. iba predajca medu. Známe veľké firmy väčšinou skupujú med od malo–včelárov a potom predávajú ako vlastný. To je ešte dobrý prípad. Už sa našli mudrlanti, ktorí to začali predávať ako pravý med zmiešaný s inými sračkami alebo začali pridávať potravinové farbivá aby dosiahli požadovanú farbu. Potom ho predávali ako kvalitnejší tmavší med za viac peňazí. Neodporúčam kupovať vo veľkoobchodoch. Ak aj narazíte na pravý, nefalšovaný včelí med budete platiť mastnú sumičku. Stretli sme sa už s pravým medovicovým medom s cenou 130sk za štvrť kila. Súkromník je schopný predať vám ho lacnejšie vďaka tomu, že ho sám produkuje (včely mu ho produkujú–aby som im neukrivdil). Väčšinou je veľmi zložité nájsť kvalitný medovicový med. 
   
 3.      Pravý včelí med musí skryštalizovať, 
       
       pokiaľ sú v ňom nečistoty vytlačí ich nahor, a keď ho pretočíte v pohári vzduch bude v bubline stúpať nahor (ak je v tekutom skupenstve). Najrýchlejšie stúpa bublina v agátovom a kvetovom mede. Najpomalšie v medovicovom – záleží na hustote. Kryštalizácia prebieha u každého medu za rozdielny čas. U kvetového je to okolo mesiaca, zvyšné medy kryštalizujú do pol roka. Najdlhšie vydrží v tekutom stave agátový. Približne rok. Ako som už vravel kryštalizácia je prirodzená vlastnosť medu ochrániť sa pred zubom času. Pokiaľ ho chcete vrátiť späť do tekutého skupenstva vložte ho do 50 – 60 stupňovej vody. Nie teplejšej – aby ste nezničili minerály a vitamíny, ktoré obsahuje.       
   
 4.   Ak chcete zamachrovať  
          pred predajcom alebo odhaliť či je zbehlý v tom čo predáva, spýtajte sa ho na vodivosť – čiže obsah minerálov a vitamínov. Každý med má svoju špecifickú vodivosť. Napríklad medovicový sa odlišuje od horského práve vodivosťou, ktorá je nad 10 stupňov. Osobne som sa stretol už aj s vodivosťou „medu“, ktorý ju mala menšiu ako destilovaná voda 0,0 niečo... a toto ľudia jedia. Hlavne, že to stojí o dvacku menej. Tu ma napadá jedno rómske príslovie: Aj hovno je drahé, lebo musíš veľa jesť aby si ho mal. 
  Už len v krátkosti zhrniem základné medy, ktoré sa dajú kúpiť:  
 Repkový: Väčšinou sa predáva začiatkom sezóny – na jar. Má typickú škrabľavú chuť a vonia akoby po síre. Obsahuje protirakovinové látky. Práve kvôli chuti nie je moc obľúbený.  
  Kvetový: Je ho najviac. Zhruba dve tretiny zo všetkých medov. Včely ho zbierajú z nektáru kvetov na lúke a z ovocných stromov. Obsahuje najviac peľu, preto nie je vhodný pre alergikov. Posilní, ukludní. Bežný med na chlieb, či do čaju.  
 Agátový: Doporučuje sa malým deťom od jedného roku, pretože je najstráviteľnejší. Blahodárne pôsobí na priedušky a dýchacie cesty. Najneskôr kryštalizuje.  
 Lipový: Výnimočný svojou lipovou vôňou. Najaromatickejší z medov. Vynikajúci na pečenie a do čaju. Lieči nádchu a prechladnutie.  
 Horský: Obsahuje stopové prvky, podporuje srdcovú činnosť. Obsahuje aj nektár horskej maliny či lipy takže malé percento peľu. Má tmavo hnedú farbu.   
 Medovicový: je v podstate čistý horský. Včely ho zbierajú po odkvitnutí všetkých kvetov koncom leta z lístia, ihličia a stromov. Dub, jedľa, smrek. Neobsahuje peľ – je vhodný pre alergikov. Obsah minerálov a vitamínov je v ňom najvyšší spomedzi všetkých medov. Má čiernu farbu, niekedy s odtieňom červenej. Obľúbený príklad: svetlých medov musíte zjesť dve tri lyžičky aby ste dosiahli obsah minerálov jednej lyžičky medovicového. Fakt, že Mr. Honey.   
 Tak milí blogeri dúfam, že sa necháte zlákať a tieto Vianoce si doprajete sladkú ale hlavne zdravú dobrotu. Ak bude o článok záujem napíšem niečo aj na tému peľ, propolis, prípadne viac o medoch. Nebojte sa investovať – med je určite lepší ako vitamíny alebo lieky. Príroda vymyslela skvelú vec. Škoda, že si ľudia nevedia vážiť nekonečnú prácu, ktorá za tým je. Radšej platia v lekárňach tisíce korún a pohoršujú sa mi pre stánkom ako to môže toľko stáť. Spopod čiapky, zahrabaný v desiatich svetroch im s úsmevom na perách odkazujem: Veselé Vianoce:) !!! 
 Nový článok (nielen) o medovicovom mede... 
   
  

