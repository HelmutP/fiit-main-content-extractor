

 - sex - pokiaľ možno, tak veľa, ale pozor, zase žiadna pornografia, autori si zrejme myslia, že keď tam bude veľa akože-sexu, čitateľa to šokuje, niečo ako majora Terazkyho pri jeho pamätnom vyjadrení „Oni mi tu jebú!" Akurát efekt je väčšinou opačný, keďže prekvapivo veľa ľudí už sex malo, alebo aspoň vedia, ako sa robí; 
 - rozhovory o sexe - little more conversation, little less action... áno, niekomu stačí aj listovať v kuchárskej knihe namiesto jedla, takisto kecať o sexe je zrejme v modernej spoločnosti oveľa častejším javom, než jeho vykonávanie... 
 - sekundárne dejové prvky môžu byť tvorené napríklad alkoholom, neverou (hm, aj keď to je zase sex), rozvodom, atď.... v kocke sa to dá vyjadriť asi tak, že keď som sa pýtal frajerky po tom, čo dočítala od Urbaníkovej Stalo sa mi všetko na to, aké to bolo, tak povedala, že „celkom dobré, aj keď výstižnejší názov by bol asi Stalo sa mi hovno". 
 - free cool, ale pritom reálne postavy - ideálnou postavou slovenskej sexuálnej knižnej moderny je žena (keďže tieto knižky čítajú najmä ženy, je to pochopiteľné), mladá, pekná (keďže kto už by chcel čítať o starej škaredej žene? alebo nebodaj chlapovi?) s dobrou prácou, skvelými kamarátkami, prípadne rodinou, ktorej ale aj tak stále niečo chýba, a síce pocit sebarealizácie, duševného blahobytu a akéhosi vyššieho zmyslu bytia... haha, ste naleteli... väčšinou jej chýba akurát sex. 
 - vyššie uvedenému popisu vyhovujú potom všetky postavy v knihe, ak sa tam náhodou vyskytne napríklad dieťa, alebo starší človek, tak len kvôli šteku (napríklad aby hlavné postavy prichytil pri sexe). Treba povedať, že chvalabohu, lebo napríklad detské postavy sú vlastne dospelí s degeneratívnym genetickým ochorením, takže síce vyzerajú ako deti, ale rozprávajú a konajú ako tridsiatnici (žiarivý príklad je napríklad tu), čo je podľa mňa dosť desivé. 
 - mužské postavy sú väčšinou bezvýznamné a spĺňajú iba úlohu akéhosi podporného systému pre penis. A okrem toho sú to svine. Šak vieme. 
 - free cool, ale pritom reálna postava musí mať free cool, ale pritom reálne meno, najlepšie niečo bezpohlavné a nadkultúrne (autori zrejme dúfajú, že so svojimi písačkami uspejú aj v zahraničí, preto svojim postavám zriedka dávajú bežné slovenské mená ako Ondrej, alebo Ľudmila), najlepšie nejaká dvojslabičná prezývka, ako Veve, Lalo, Mimi alebo Riki.  
 V podstate keď už máte vyššie uvedené elementy, máte polovicu práce hotovej, stačí už iba vymyslieť, kedy a kde sa Veve s Lalom budú rozprávať o sexe, zatiaľ čo Mimi bude sexovať s Rikim, ale myslieť pritom bude na Lafiho.  
 No ale aby to zase nevyzeralo, že len tak kecám a nič som nedokázal/závidím/nevyznám sa v tom/pre slovenskú literatúru som nič neurobil, tak tu je prvý aj posledný diel môjho nového románu na pokračovanie pod názvom Klamanie telom (prípadne Slzy duše, prípadne Horkosť vernosti... názvy mi nikdy nešli). 
   
 - Zase pondelok, vzdychla si Vany sediac za stolom slnečnej kancelárie. Riki ťukal do klávesnice a zasnene sa pri tom usmieval. 
 - Asi si zase píše s tou svojou kurvičkou, povedala si v duchu Vany, neuvedomujúc si, že na Rikiho zamračene zazerá. Ten zachytil jej pohľad a doširoka sa usmial svojim perlovo bielym úsmevom. Vany z toho úsmevu vždy zvlhli nohavičky a aj tentoraz znovu zacítila šteklenie v podbrušku... 
 - Čo moja? spýtal sa Riki. 
 - Nič, ja len... čo robíš? rýchlo sa Vany spamätala. Rikiho chcela už od prvého dňa, čo do firmy nastúpila ako marketingová manažérka a nenútená konverzácia s ním jej robila menšie problémy. 
 - Nič dôležité. Prečo? prehodil Riki preťahujúc si chrbát, čo mu vyrysovalo jeho mužnú hruď pod bledomodrou košeľou. 
 - Tak to by si ma mohol ošukať na kopírke, povedala si v duchu Vany. 
 - Tak to by som ťa mohol ošukať na kopírke, povedal Riki. 
 Vany naňho zarazene zazrela. - Áno?, prehodila. - Nechodíš už s Tami? Lebo jej by sa to asi nepáčilo. Pomaly Vany nadobúdala stratenú sebakontrolu. 
 - Páčilo, nepáčilo... keď jej nič nepovieme, tak je to jedno, nie? Riki sa usmieval stále širšie. 
 - Ja krava, do čoho som sa to zase namočila? premýšľala usilovne Vany, - Nie že by som si to s ním nechcela rozdať na kopírke, ale s Tami sme boli kedysi kamošky... až kým ma nepodviedla s Lakym, suka jedna! 
 *** 
 Miestnosť s kopírkou bola za kuchynkou v tmavom kúte chodby. Riki zručne vyložil Vany na kopírovací stroj a začal si povoľovať opasok. Keď si Vany vyzliekala nohavičky, ozvala sa hlasná rana a Vany s Rikim zažili krátky voľný pád. Naštastie firma sídlila na prvom poschodí a pod jej sídlom bol bar, preto Vany, Riki a kopírka dopadli cez tenký azbestový strop rovno na mäkkú koženú sedačku. Oproti v kresle sedela Vanina najlepšia kamoška Lola - umelkyňa na voľnej nohe, ktorá väčšinu svojho dňa trávila v baroch pri cigarete, alebo v posteli s bývalými väzňami. 
 - Vany, čo ty tu? radostne vykríkla. 
 - Ale, odskočila som si z roboty. 
 - A kto je to, ani nás nepredstavíš? Ale to už Riki sám naťahoval ruku k Lole. 
 Pribehol čašník a priniesol fľašu sektu. 
 - To je na mňa, povedala kopírka. Sekt sa lial prúdom a alkoholické bublinky prúdili v krvi ako malé molekuly instantného šťastia. 
 - Žime a pime! zakričal Riki a začal sa vyzliekať. 
 - Omrdaj ma zozadu, navrhla Lola Rikimu a hneď aj odbehli na záchod. 
 - Hm, jasné a na mňa zase zostane akurát rozbitá kopírka, smutne si vzdychla Vany.  
 Koniec poslednej časti. 
   

