
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magda Kotulová
                                        &gt;
                Nezaradené
                     
                 Viťúz 

        
            
                                    6.4.2010
            o
            12:06
                        |
            Karma článku:
                4.91
            |
            Prečítané 
            1039-krát
                    
         
     
         
             

                 
                    Nebo bolo zázračne belasé. Taký farebný letný deň. A decká šantili na uzavretom dvore sídliska. Chvíľu bolo mĺkvo, chvíľu sa ozývali detské hlásky, tu krik, tu smiech, tu dohadovanie. A potom zasa smiech.
                 

                 Z okna schodiska na treťom poschodí sa vykláňalo chlapčiatko. „Ivo,“ skríklo nadol do dvora, „bol som u tvojej mamy a povedal som jej, že si mi pokazil vozík s koníkmi.“   Zdola sa nič neozvalo.   Chlápä v okne nepohnuto stálo. Potom znova sa vykláňalo. Príliš hlboko von. „Ivoóó... povedala, že dostaneš na zadok, keď prídeš hore... Počuješ?“   Z dvora sa opatrnícky ohlásilo: “To nie je pravda, nebol si u mojej mamy!“   „A bol!“   „Nebol!“   „Bol!“   „No dobre teda, tak bol!... Povedz mi, aké vlasy má moja mama?... Nooóó?“   Chlápä v okne rozmýšľalo. Zrazu vyhŕklo: „Biele!“   „Čoóó? Biele?... Ha-ha-ha-aha... moja mama ma čierne... To nebola moja mama... ha-ha-ha-hi-hi-hi...“   Chlápä v okne prekvapene pozeralo nadol, opäť uvažovalo, potom sa stiahlo dnu a rozbehlo sa dupotavo dolu schodiskom.       xxxxxxxxxxxxx       Krik a štabarc. Z dvora. Deti po dvoroch. Veď dýchajú posledné hodiny prázdnin. Sloboda a voľnosť: bezbrehá...   Vtom smiechoty. A plač.   „Veď si mi ocikal šaty...“   „Hi-hi-ha-ha-ha...“   „Zajtra som si ich chcela vziať do škôlky...“ Mrnkot. Vzlyky. „V čom teraz pôjdem?...“   „Ty hlúpa... však sa ti do rána usušia. Ha-ha-ha-ha-hi-hi-hi...“     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            U kaderníčky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Oprstienkovaná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Daniel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Keď zakape mobil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Registrácia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magda Kotulová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magda Kotulová
            
         
        magdakotulova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som mama štyroch detí, stará mama pätnástich vnúčat a dvoch pravnúčat. Príležitostná publicistka. Občas sa "niečo" pokúsim napísať, keď ma čosi nahnevá alebo urobí spokojnou. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    318
                
                
                    Celková karma
                    
                                                10.84
                    
                
                
                    Priemerná čítanosť
                    1470
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kódex blogera na blog.sme.sk (aktualizovaný 10. 7. 2012)
                     
                                                         
                       Ocko náš
                     
                                                         
                       Príjemné výročia si treba  pripomínať
                     
                                                         
                       Lezúň
                     
                                                         
                       Garancia vrátenia peňazí. To určite!
                     
                                                         
                       Feťáčkina matka
                     
                                                         
                       Vôňa ženy
                     
                                                         
                       Vodopády Slunj
                     
                                                         
                       Mladý, nervózny vodič a starká
                     
                                                         
                       Nikdy, nikdy, nikdy sa nevzdávaj
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




