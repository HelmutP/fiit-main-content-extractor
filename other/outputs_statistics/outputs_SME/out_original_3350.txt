
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Branislav Pírek
                                        &gt;
                Bratislava
                     
                 Čechoslovakizmus v akcii 

        
            
                                    26.3.2010
            o
            1:57
                        (upravené
                10.9.2013
                o
                1:11)
                        |
            Karma článku:
                3.55
            |
            Prečítané 
            636-krát
                    
         
     
         
             

                 
                    Šokuje ma, na čo len tí naši poslanci a hlavne starosta A. Petrek nemyslia... Nehanbia sa vyhodiť úplne zbytočne okolo 16 500 eur daňových poplatníkov Bratislavy za sochu T. G. Masaryka namiesto leva pred budovou Slovenského národného múzea.
                 

                 Už väčší dôkaz čechoslovakizmu si nemôžeme ani nainštalovať. Ešte nedávno boli búrlivé diskusie, či lev je český alebo nie a teraz ho má nahradiť socha Masaryka. Svojim popredným osobnostiam nie sme schopní od revolúcie postaviť adekvátne pamätníky a my tu budeme stavať sochy ľuďom, ktorí pre Slovensko nespravili nič. Ešte na hanbu sveta udelí náš primátor čestné občianstvo V. Havlovi a jeden redaktor sa ho na to margo opýtal najprimitívnejšiu otázku, či Slovákom odpustil za tie vajíčka v 1991 na Nám. SNP. A V. Havel dostatočne inteligentný odpovedal, že nemá čo odpúšťať, lebo dobre sám vie, prečo sa tak stalo.   Najhoršie na tom je, že mestskí poslanci rozhodli bez akejkoľvek  diskusie s verejnosťou, resp. s občanmi (a platcami dane) nášho mesta!  Tákéto záležitosti by mali byť schválené na základe verejnej mienky.  Dnes tomu už nič nebráni - hlasovanie na internete, hlasovacie lístky,  ktoré sa odovzdajú na magistráte, audiotext cez telefón.   Nehovoriac o tom, v akom stave máme svoje pamätihodnosti a ako sa o ne  (ne)staráme. Ako vtip pôsobí dodnes neodstránená socha M. Čulena ešte z  komunistických čias, ktorá je blízko pri úrade vlády v parčíku. Niektorí  svojráznejší ľudia mu občas do ruky dajú fľašku...   Pred budovu SNM určite patrí bez akýchkoľvek pochybností socha Andreja Kmeťa, a nie socha Masaryka.   Diskusia k soche T. G. Masaryka -  + anketa.       Poznámka k diskusii:   V diskusii o soche Masaryka si zástanca sochy a iniciátor p. Dr. Osuský sám protirečí. Vyjadril sa, že Masaryk bol hlavne prezidentom, čo však nie je pravda. Masaryk bol v prvom rade filozof a vysokoškolský profesor. Masaryk sa stal prezidentom vo veku 68. rokov, čo bol neprijateľný vek pre prezidenta republiky. Masarykovi bola udelená výnimka ako spoluzakladateľovi štátu, ktorá musela byť zakomponovaná aj do právneho konceptu. Ďalej vyzdvihuje Čechov za sochu Štefánika, ktorá je pri jeho hvezdárni v Prahe a že ako Česi predbehli Slovákov. Aj my môžeme dať rovnako veľkú sochu Masaryka do nejakého dvorčeka, trebárs knižnice, keď bol tým filozofom. Štefánik opačne nebol hlavne letcom, bol hlavne vedcom, politikom a vojakom! Vedel by nejako vysvetliť, prečo stojí socha Štefánika ako letca pri jeho hvezdárni? Keď sa ďalej tak pán Osuský zastrájal, že socha nie je podobizňou občianskeho preukazu - socha musí na prvý pohľad evokovať o koho ide! O tvár ako takú nejde, ide o výzor, aby bolo hneď zreteľné, koho socha symbolizuje. Spomenul, že pri sčitovaní ľudu sa Dr. J. Tiso prihlásil za Maďara. Každý znalý človek dobre vie, ako fungovali tieto súpisy. Evidencia sa nerobila na základe toho, za čo sa daný človek považoval alebo sa k niekomu hlásil. Schvaľujem ďalej návrh p. Dr. Slivkovej, že Masaryk by mal stáť niekde, kde by vynikal ako filozof. Postavením Masaryka pred SNM predbehneme Čechov na celej čiare, lebo takú úctu a miesto Štefánikovi oni nedali. Ja by som bol za kompletné súsošie Štefánika, Masaryka a Beneša, niekde v nejakom parčíku, keď už by sme mali mať v Starom Meste pamätník k 1. ČSR. Myslím si však, že doterajší lev na pylóne bohato stačil.       V medzivojnovom období, v rozpore so skutočnosťou a s tvrdeniami aj českých spolupodpisovateľov, vehementne popieral existenciu Pittsburskej dohody (o autonómii Slovenska v rámci budúceho Česko-Slovenska), ktorú sám podpísal. Takisto treba spomenúť, že sám Edvard Beneš (ktorý sa raz vyjadril, že o samostatnom slovenskom národe „nemôže byť ani reči“) uviedol, že musel Masarykovi protirečiť, pretože mu dôverne vyslovil - prinajmenšom neskôr a prinajmenšom vo vzťahu k politike - veľmi negatívny názor na Slovákov, tvrdiac že im nemožno dôverovať a pod. („Vy je neznáte...“). Masaryk nikdy nepochopil emancipačné úsilie Slovákov, a naopak snažil sa ho titulovať ako prejav hungarizmu a protičeského separatizmu.   Zdroj: Wikipedia       Žiaľ sa veľmi zabúda na dôležitú vec, keby nebolo Štefánika, Masaryk ani  Beneš sa do vysokej zahraničnej politiky nikdy nedostanú.   Masaryk ani Beneš si nezaslúžia žiadnu sochu, a nie to ešte v Bratislave a na takom mieste (aj napriek faktu, že Masaryk bol z jednej polovice Slovák a z druhej Nemec)!   V Prahe socha M. R. Štefánika je, ale pozrime kde? Nemá žiadne svoje osobité miesto, ako by sa patrilo, ale je umiestnená pred hvezdárňou, ktorá nesie jeho meno. Takže socha vlastne patrí k objektu. Socha nie je dokonca ani na žiadnom piedestály ale na úrovni chodníka v miernej nadživotnej veľkosti (samozrejme neopomenú to, že Štefánik bol čechoslovakista, iste, nie ale národne, od začiatku presadzoval záujmy Slovákov). Takže asi táka úcta k človeku, ktorý skutočne založil krvopotne ČSR, namiesto Beneša a Masaryka, ktorí si hoveli v teple v Londýne a v Paríži. Sami Česi odstrčili Štefánika a my máme teraz ako niečo im splácať? Nemáme čo. Históriu nik nemôže oklamať, môže ju len zmanipulovať, tak ako to robili komunisti počas celej svojej éry. Stále si Čechov dávame do popredia, ako keby sme im boli povinne niečo dlžní a neštítia sa ani arogantne a s povýšenectvom správať voči Slovákom, aj v zahraničí. Dôkazom sú aj katedry spoločných dejín na zahraničných univerzitách, kde sa vám českí učitelia nepredstavia ako členovia katedry českých a slovenských dejín, ale len českých. História sa opakuje - československé znamenalo české, takto bolo vnímané aj v zahraničí - „czecho, czecho” ...   Česi nerobili nám láskavosť, keď sme sa spojili počas 1. sv. vojny, bola to nevyhnutnosť oboch národov od tyranie Rakúsko-Uhorska, zbaviť sa nátlaku Nemcov a Maďarov. Rovnako Česi zabúdajú na fakt, že bez pomoci zahraničných Slovákov v USA a v Kanade by ČSR neexistovalo (rovnako si vzali aj slovenské zlato po 2. sv. vojne, a do tretice aj v 1992). Česi nesplynili ani jednu zo vzájomných dohôd so Slovákmi už od prvej republiky. Česi dodnes porušujú dohodu o nových štátnych symboloch a názvoch vo svete po rozdelení ČSFR v roku 1992. Neoprávnene si ponechali pôvodnú zástavu s modrým klinom a rovnako si nechali aj iné označenia (ČSA, ČSAD a iné).   Alebo aj nemenej závažný fakt, že nepíšu Česko-Slovensko, ale  Československo, aj keď podľa všetkých vzájomných dohôd mal názov  obsahovať spojovník a oba rovnocenné názvy krajín.   Čo presne charakterizuje postoj Čechov, vyjadruje aj fakt, že počas vojen a tvrdej komunistickej normalizácie, keď sa každý deň vešali ľudia v celej republike, bez obáv si točili svoje veselohry s Janom Werichom či V. Burianom (ktorého neskôr aj tak „odstránili” a uchýlil sa potom na to zlé Slovensko).   Treba uznať, Česi vedia aj z h***a  niečo získať. Vedia podať veci tak, aby to vyhovovalo im a zároveň aby o tom druhá strana vôbec nepochybovala. Česká propaganda dokonalosti.   „Nedělat, ale vydělat.”   Stačí aj vidieť postoj k prvej Slovenskej republike v 1939, že to bola zrada. A takýto názor majú dodnes a implantovali ho aj nám. Slovensko malo legitímne právo na osamostatnenie od ČSR. Ešte pred rozdelením ČSR vyhlásilo Slovensko spolu s Podkarpatskou Rusou autonómiu (najdôležitejší krok k samostatnosti). Žiaľ, prišla vojna a po Mníchovskej dohode bolo Slovensko nútené vyhlásiť mimo ČSR samostatnosť. Došlo k tomu, čo vlastne Slovensko plánovalo v budúcnosti. Nový štát Slovenská republika (tzv. slovenský štát, nazývaný hlavne Čechmi ako výsmech) bol uznaný 27 štátmi sveta. Problém spočíval v čase a za akých podmienok sa táto udalosť stala. Rozhodne však 1. SR nebola profašistickým štátom - ako keby Slovensko bolo jediným satelitom Nemecka či iné podmanené krajiny Európy. Iba to zlé malé Slovensko vyvážalo ľudí do táborov, nie?! Treba si uvedomiť, že aj keď Slovensko bolo samostatné, podliehali nemeckej diktatúre, tak ako ostatní, bez výnimky.   Ako vidíme, máme stále čo doháňať.   „Historia, magistra vitae.”   Nemáme jasno v základných veciach a princípoch!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Pírek 
                                        
                                            Posledné dni Windows XP?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Pírek 
                                        
                                            AD: Koľko ľudí naozaj pochodovalo v Košiciach?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Pírek 
                                        
                                            Hlavička dokumentu ako dôkaz pôvodu? To snáď nemyslíte vážne!?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Pírek 
                                        
                                            Dallas mimo záujmu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Branislav Pírek 
                                        
                                            Zabudnite na kamenné banky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Branislav Pírek
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Branislav Pírek
            
         
        pirek.blog.sme.sk (rss)
         
                                     
     
        
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    25
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    586
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Bratislava
                        
                     
                                     
                        
                            Počítače
                        
                     
                                     
                        
                            Kultúra a spoločnosť
                        
                     
                                     
                        
                            Dabing
                        
                     
                                     
                        
                            Iné
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




