
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Monošová
                                        &gt;
                O vzťahoch
                     
                 Ako predchádzať nevere u mužov v strednom veku 

        
            
                                    13.5.2010
            o
            13:22
                        (upravené
                13.5.2010
                o
                13:38)
                        |
            Karma článku:
                8.19
            |
            Prečítané 
            2317-krát
                    
         
     
         
             

                 
                    Neviem, či je to kvôli téme mojej novej knihy, ale odkedy vyšla, akoby sa v mojom okolí roztrhlo vrece so skutočnými nevernými "klišé", ktoré prežívajú moje priateľky a známe. Ale asi to bude skôr vekom: štyridsiatka na obzore alebo už na krku, romantická láska zmizla niekde medzi plienkami ratolestí, snahou o akú-takú kariéru a prvými vážnejšími chorobami našich rodičov. A potom to jedného dňa príde ako blesk z jasného neba: manželova nevera.
                 

                 Svatbou okolo dvadsiatky akoby niekto odštartoval dostihy - lenže my v tejto súťaži nie sme džokej, ale bežiaci kôň. Pri výbere princeznovských bielych šiat, najkrajšej torty a štýlových topánok nám ktosi zabudol dať návod na to, čo sa v rozprávkach lakonicky zhrnie ako: "...a žili šťastne až do smrti." Mohli sme teda tušiť, že ďalej to už nebude taký veselý a ľahký príbeh.   Väčšinou ďaleko od rodičov, ktorý by mohli pomôcť, snažíme sa akosi vypiplať bábätká, aby mali slušný základ do života. Okrem boja o uchovanie vlastnej strechy nad hlavou, kojíme, prebaľujeme, nespíme, potom prvé kroky a zúbky, vychovávame ich, určujeme hranice, dávame im to najlepšie, čo vieme, špeciálne škôlky, školy a znova nespíme, keď v puberte chodia domov neskoro po polnoci.   Zarábame (v lepšom prípade robíme skutočnú kariéru, ktorá nás aj teší), nosíme nákupy, varíme, žehlíme, upratujeme, skrátka glancujeme do najkrajšej podoby celý náš svet. Aj na peknú dovolenku sa nájde trochu času - veď rodina, jej zdravie a stmeľovanie, je pre nás to najposvätnejšie!   Občas skočíme do telocvične alebo si s kamoškou zabeháme a aj diétka raz za čas nám nie je cudzia - veď treba sa aj o seba starať! Pri pohľade do zrkadla vyzeráme omnoho lepšie, než sa cítime a pozítívny postoj vyčarujeme ako na stlačenie tlačidla.   Ako všetky vďačné a dobré dcéry chodíme s rodičmi po lekárskych vyšetreniach, pomáhame si navzájom rozdýchavať ťažké a zákerné diagnózy, berieme si na plecia starostlivosť o ich posledné dni...   Zvládame toho tak veľa! Všetko predsa musí byť v poriadku - čo tam potom po nejakom hlodavom pocite niekde hlboko, že "čosi" by malo byť inak?!   Dostih sa ale nekončí a s pribúdajúcimi rokmi nám ubúda síl. Áno, sme stále omnoho mladšie, než ženy v našom veku pred sto rokmi. Sme krásne, žiadúce, samostatné, šikovné, emancipované...   ... ale načo nám to je, keď si nás jedného (väčšinou rozprávkovo krásneho) dňa manžel posadí do kresla a prizná sa, že si niekoho našiel?	Prečo? napadne nám prvá otázka. Žartuje! napadne nám ďalej. Toto predsa NEMÔŽE myslieť vážne! Náhle nám dôjde celá vážnosť situácie a príde nám zle.   Cítite sa oklamaná, ale najviac vás trápi, kde ste urobili chybu. Máte chuť sa uzavrieť do seba, ale rodina vás potrebuje aj naďalej. A tak z posledných síl varíte, periete, žehlíte a balíte deti do školy, snažíte sa nezblázniť, pričom váš neverný manžel si práve vtedy najviac užíva. Nedokáže pochopiť, že mu nerozumiete a najradšej by svoju milenku nasťahoval do spálne hneď vedľa vás. Prečo?   Z jeho hľadiska ste ho zradili už veľmi skoro: rodina sa pre vás stala väčšou prioritou ako on. Drvivá väčšina mužov sa nežení preto, aby sa stali súčasťou rodiny - oni potrebujú mať svoju ženu. Tá lepšia skupina z neverníkov svoju manželku na začiatku aj skutočne miluje a neveru neplánuje. (Horšia skupina, ktorá nedá pokoj jedinej sukni, nestojí za zmienku...)   Mnoho z týchto mužov by ani nebolo neverných, ale do hry vstupujú rôzne ďalšie faktory. Najhorší z nich je asi zakomplexovanosť. Títo muži si stále potrebujú niečo dokazovať a manželka si veľmi skoro všimne, že jej muž otvorene flirtuje s jej kamarátkami. U muža, ktorý nie je spokoný sám so sebou, je od flirtovania iba krôčik k skutočnej nevere.   Ďalším prípadom je syndróm "moja žena mi nerozumie". Rozumie mu rovnako, ako mu rozumela vždy, ibaže manželka už nemá energiu, aby zvládala problémy, o ktorých jej manžel nepovedal. Je to klasické zlyhanie manželskej komunikácie (väčsinou je spojené s dlhodobými problémami v spálni).   Muži bývajú neverní ešte aj z jedného dôležitého dôvodu: boja sa starnutia viac, než ženy. Keď muž starne, väčšinou si potrebuje dokázať, že ešte na to má, aby zbalil mladú pipku... a občas býva dôvodom na neveru aj strata dobrého zamestnania, kde mal muž dôležitú pozíciu.   K vážnej nevere teda treba nejaký problém zo strany muža a aby sa na obzore objavila mladá barakudka so zlatokopeckými úmyslami. (Ako vidíte, skoro vôbec tu nejde o to, čo urobila alebo neurobila manželka!)   V týchto prípadoch muž krátkodobo podlieha dojmu, že našiel vílu, ktorá ho spasí, ale to iba preto, že nová milenka nemá - okrem sexu - žiadne nároky. A to mužovi strašne vyhovuje! Konečne od neho nikto nič nechce, konečne ho nikto nekritizuje a môže si iba užívať. Má pocit, že sa dostal z väzenia.   Milenkine nároky však prídu, akonáhle muža uloví. Prvým znakom ulovenia je, že muža prinúti, aby manželke povedal "pravdu". Barakuda veľmi dobre vie, aký ťažký úder tým uštedrí svojej konkurentke a manželka väčšinou okamžite padá na kolená - záleží to od toho, do akej miery svojho muža miluje. Čím miluje hlbšie, tým viac je zranená.   Ako tento príbeh pokračuje, záleží od manželkinej schopnosti tolerancie a sebaobetovania. Výjde síce s najväčšími ranami, ale jediná môže naozaj rozhodnúť, či tí dvaja môžu alebo nemôžu spolu žiť. Ak si totiž manželka povie, že si nenechá zničiť rodinu, možu sa tí dvaja aj na hlavu postaviť. Čas býva najväčším nepriateľom vzťahov: čím dlhšie sú tí dvaja spolu, o to viac problémov medzi nimi vzniká. Je len otákou času, aby muž pochopil, kde mal lepší servis (lebo vzťah nie je iba o sexuálnych radovánkach, ktoré nie sú, koniec-koncov, závislé na veku a mnohé trvalé dvojice si ho veselo užívajú do vysokého veku!)   Maželka, ktorá nájde v sebe tú silu a dokáže muža prijať späť by však mala vedieť, že:  -svojmu manželovi už nikdy nebude dôverovať  -nikdy mu neveru skutočne neodpustí  -nikdy si nebude istá, či ju znovu neoklamal  -ak už spolu viac nebudú spávať, dobrovoľne sa vydala na cestu "vydatej starej panny" (nehovoriac o tom, že manžel bude mať o to viac chuť znovu sa poobzerať po "dákej tej zelenšej lúke"...)   -ak aj začne so svojim manželom spávať, jej láska sa nikdy neobnoví  -žena častokrát stratí schopnosť uveriť v lásku, hoci manžel sa do nej môže znovu skutočne zaľúbiť  -naučí svoje deti, aby neverili v skutočnú lásku (pretože cítia a vedia, že ich matka otca už nemiluje a uspokojila sa s "vrabcom v hrsti", než aby nabrala odvahu začať znova)   Dve z mojich dobrých známych sa za posledného pol roka stali podvádzanými manželkami, tretia sa rovno rozviedla.   Prvá zaprela vlastnú podstatu a prijala manžela naspäť, ale odvtedy jej začínajú tráviace ťažkosti.   Druhá stále verí, že manžel sám konečne odíde a prestane ju týrať tým, že sa nevie rozhodnúť, kde chce byť, hoci ho už niekoľkokrát vyhodila z domu.   Tretia sa u mňa zastavila cestou z rozvodu a po dvoch hodinách sa ma s uplakanými očami vážne opýtala: "A čo je to vlastne tá láska? Ja to asi ani nepoznám..." Tretia priateľka tvrdí, že jej útrapy sa začali, keď si ju posadil do kresla a prosto jej povedal, že sa chce rozviesť, lebo on potrebuje jednoduchšiu ženu - no podľa mňa ide o ďalší klasický trojuholník...   Nech už muž svoju neveru ospravedlňuje akýmkoľvek dôvodom, a či už je alebo nie je "objektívny", milé dámy v kritickom veku okolo štyridsiatky: občas sa večer šikovne zbavte detí a venujte sa IBA manželovi. Povzneste sa nad vlastné problémy a nehovorte o nich - aj tak už predsa dávno viete, ako všetko vyriešite!   Buďte ženou, akú vo vás potrebuje. Počúvajte ho, rozprávajte sa s ním, prekvapte ho niečím novým nielen v spálni alebo s ním choďte na film, ktorý vás inak nezaujíma (na futbal chodiť nemusíte, ak futbal neznášate). Dajte mu skrátka pocítiť, že je stále stredom vášho sveta.  Spomeňte si, čo ste robievali, kým ste spolu iba chodili, ale nesnažte sa oživovať starú romantiku! Nájdite si novú.   Chce to trochu odvahy zmeniť rytmus života, ale uvedomte si, že táto zmena vás bude stáť určite menej energie, než zožierať sa oklamaná, nad neistou budúcnosťou počas bezsenných nocí v prázdnej spálni!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (381)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Nebuďte ovce a poďte voliť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Nielen za zvukov hudby, no aj popri jej videoklipe...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Zuhair Murad
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Ellie Saab
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Jean Paul Gaultier
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Monošová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Monošová
            
         
        monosova.blog.sme.sk (rss)
         
                                     
     
         O všetkom krásnom alebo šokujúcom, čo ma prinútilo napísať o tom aj čosi iné ako román:-) Som autorka románov (nielen pre ženy): Lásky o piatej (2005, 2012) Anglické prebúdzania (2006) Klišé (2010) Zlodeji bozkov (2012) Sladké sny (2013) Lekcie z nenávisti (2013) Miluje-nemiluje, idem! (2014) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    119
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1068
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Celkom mimo
                        
                     
                                     
                        
                            Mixtúra
                        
                     
                                     
                        
                            Odraz spoločnosti
                        
                     
                                     
                        
                            Móda
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            O vzťahoch
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Etela Farkašová: Čas na ticho
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Caro Emerald
                                     
                                                                             
                                            Tiesto
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Súčasní slovenskí spisovatelia
                                     
                                                                             
                                            Iné názory
                                     
                                                                             
                                            Big girl likes fashion too
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            moja osobná stránka
                                     
                                                                             
                                            moda a tak
                                     
                                                                             
                                            super móda
                                     
                                                                             
                                            klasický interiér
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Haute couture jar-leto 2014: Ellie Saab
                     
                                                         
                       Haute couture jar-leto 2014: Jean Paul Gaultier
                     
                                                         
                       Haute couture jar-leto 2014: Chanel
                     
                                                         
                       Haute couture jar-leto 2014: Dior
                     
                                                         
                       Haute couture jar-leto 2014: Valentino
                     
                                                         
                       Akú cenu pre nás má Zlatý slávik?
                     
                                                         
                       Každý deň stretnúť človeka...
                     
                                                         
                       Robert Fico, Vy úbohý frajer, teraz zaplatíte 30 mega z Vašich?
                     
                                                         
                       Ako stratiť lásku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




