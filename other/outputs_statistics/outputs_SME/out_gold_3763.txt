

 Včera sa k vzniknutej situácií vyjadrila aj Únia ligových klubov vo svojom komuniké, v ktorom sa vyjadrila k organizácií a bezpečnosti na ligových štadiónoch. Jeho obsah možno zhrnúť v štyroch bodoch, ktoré by mali zamedziť negatívnym prejavom: 
   
 
 
 kamerové identifikačné systémy 	na štadiónoch 
 
 
 vytvorenie databázy nežiadúcich 	osôb 
 
 
 funkčný turniketový systém 	spojený s predajom lístkov na meno 
 
 
 iniciovanie potrebných 	legislatívnych zmien 
 
 
   
 Kamerové identifikačné systémy a databázy nežiadúcich osôb 
 ...sú určite jedným z možných riešení na odhalenie potencionálnych výtržníkov na štadiónoch a spoločne s databázou nežiadúcich osôb môžu pomôcť k predchádzaniu nebezpečných situácií alebo potrestaniu vinníkov. Už od minulého roka je v platnosti zákon podľa ktorého majú byť štadióny nad kapacitu 2000 divákov vybavené kamerovým systém. Až na niekoľko výnimiek zákon nie je „dodržaný“. Klubom stačí znížiť kapacitu štadióna a problém je vyriešený, i keď počet divákov môže byť vyšší, oficiálne uvedený počet nepresiahne hranicu 2000. 
 Funkčný turniketový systém spojený s predajom lístkov na meno 
 … je riešenie s úplne nulovým účinkom. Pokiaľ by boli štadióny dostatočne vybavené kamerovým systémom, k identifikácií výtržníka postačí, pracovníkom na to určeným, vyviesť podozrivého zo štadióna a tam môže byť legitimovaný na základe občianskeho preukazu. Náklady spojené s výstavbou takéhoto systému môžu byť použité napr. na vybudovanie sociálnych zariadení na štadiónoch. Vykonávať potrebu do prenosných WC odradí nejedného návštevníka zápasu. 
 Iniciovanie potrebných legislatívnych zmien 
 … je jediné riešenie ako dostať skutočných výtržníkov zo štadiónov. To by však takéto zmeny nemali byť iba predvolebným populistickým ťahom ako zriadenie mobilných súdov. Predkladateľka návrhu sa inšpirovala Českou republikou, kde boli tieto súdy zrušené z dôvodu   neúčinnosti. Pokiaľ nebude predvedený absolútne „nesvojprávny“, takýto súd na neho nebude mať akýkoľvek dosah. Policajné orgány doteraz ešte nikdy nenašli vinníkov za pomoci kamerových systémov v priebehu niekoľkých dní, nie to v priebehu oveľa kratšieho časového úseku ako to bude v prípade mobilných súdov. 
 Bude zaujímavé sledovať, kde vôbec mobilné súdy budú sídliť. Za určitých okolností môžu byť za rizikové zápasy označené aj stretnutia v Dubnici, Senici, Trenčíne,atď... Pokiaľ návštevnosť na štadiónoch neprekročí počet divákov 2000 polícia nebude mať dostatočné dôkazy na odsúdenie podozrivých z dôvodu výnimky týkajúcej sa kamerových systémov. Zmena legislatívy vyzerá ako najjednoduchší krok k usvedčeniu výtržníkov a zaisteniu bezpečnosti na štadiónoch, ale naväzuje na množstvo ďalších opatrení, ktoré sú neúčinné. 
   
   
 Posledné odohraté derby medzi Spartakom Trnava a Slovanom Bratislava malo dohru aj pred Disciplinárnou komisiou SFZ. Belasí odohrajú minimálne jeden zápas bez divákov, Trnava dostala podmienku. Jeden z najnavštevovanejších slovenských klubov doplatil na pár výtržníkov a množstvo skutočných fanúšikov si teraz nevychutná zápas s mužstvom z Košíc. Motto, futbal sa hrá pre divákov, zostalo opäť iba prázdnou metaforou. Za súčasný stav môžu aj kompetentní, ktorý vymýšľajú nezmyselné opatrenia. Ani po niekoľkých stretnutiach, ktoré sprevádzali výtržnosti sa SFZ nepoučilo a do praxe zavádza také príkazy a zákazy, ktoré riešia až vzniknutý problém, ale možným problémom nepredchádzajú. 
 Požiaru, ktorý vznikol v nedeľu sa dalo ľahko prejsť nariadením, ktoré zo sektorov pre hostí odstráni všetky sedačky, prípadne zachová iba vrchné rady sedadiel. Toto nariadenie by bolo účinné aj v prípade vytrhávania sedadiel. Nehovoriac o nákladoch spojených s výmenou poškodených sedadiel. 
 Existuje množstvo ďalších nariadení, ktoré by neriešili problém výtržníctva, ale mu predchádzali. Týkajú sa napríklad pyrotechniky, ktorá často „nájde“ svoje miesto aj na ihrisku. To by však jej zákaz používania musel byť pozmenený na takú formu, ktorá by jej používanie zverila iba do rúk osôb na to určených. Myslím, že aj v záujme fanklubov by sa dala dosiahnuť taká dohoda, ktorá by vyhovovala obom stranám a nie iba jednej, ktorá má za následok jej absolútne nedodržiavanie a zbytočné pokutovanie klubov. 
   

