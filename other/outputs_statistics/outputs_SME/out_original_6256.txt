
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Dratvová
                                        &gt;
                Nezaradené kadečo
                     
                 Červené guľôčky -  gurmánska pochúťka aj liek 

        
            
                                    11.5.2010
            o
            7:29
                        (upravené
                11.5.2010
                o
                7:35)
                        |
            Karma článku:
                8.82
            |
            Prečítané 
            2763-krát
                    
         
     
         
             

                 
                    Jedli  ste už vyprážaný camembert, sviečkovú na smotane, divinu alebo pečené mäsko s  brusnicovým kompótom? Uvarili ste si spomínané jedlá sami, alebo ste sa pľasli po vrecku a vybrali ste si radšej z ponuky jedálneho lístka renomovanej  reštaurácie? Ale to je vlastne úplne jedno, cha .  Vedeli ste, že konzumáciou tejto dobroty robíte veľa aj pre svoje zdravie?
                 

                 
  
   Ak to všetko viete a poznáte, tak nestrácajte čas čítaním môjho článku. Ale ak ste sa doteraz o tieto červené trpkasté korálky nezaujímali, tak nech sa páči, práve vám si dovolím venovať nasledujúce písmenká...   Keď manžel prvý raz  z jeho rodného kraja  priniesol vedro týchto krásnych červených guľôčok, tak som zalomila rukami. Nevedela som čo s nimi. Manžel ich ako kompót miloval od detstva a nemal v pláne vzdať sa ich ani v jeho vlastnej domácnosti. Nič iné neostávalo, len zistiť recept a zavariť ich. Odvtedy už Váhom pretieklo veľa vody, cha.  Priznám sa, že ja som ich moc nepoznala, že som sa doslova učila prijať ich chuť. Najprv mi ich trpká príchuť vadila, teraz ich milujem práve kvôli nej.   Zaiste ste už zhliadli nejednu reklamu na výživové doplnky vyrobené práve z brusníc. Sú určené predovšetkým na prevenciu, ale aj liečbu chorôb spojených s močovým ústrojenstvom. Je to bomba v boji so zápalom močového mechúra. Vyskúšané, overené na sebe samej! Na pultoch predajní nájdete už aj brusnicový džús.   Brusnicu pravú (Vaccinium vitis-idaea L.) nájdete na slovenských  porastoch od nížin až po horské polohy. V súčasnosti sa dajú  brusnice,  vďaka vyšľachteným odrodám, pestovať aj na záhrade. Užitočné a zároveň veľmi dekoratívne rastlinky...   Brusnice majú vysoký obsah vitamínu C, K, taktiež polyfenolov, ktorým vďačia za svoje silné antioxidačné, antibakteriálne vlastnosti. Výskum stále pokračuje a najnovšie štúdie poukazujú aj na protirakovinový účinok.   Extrakty z brusníc sa používajú pri výrobe liekov, doplnkov výživy, v kozmetickom priemysle (krémy, toniky), pridávajú sa do zubných pást, ústnych vôd. Sú akoby čističkou v našich ústach, pretože ničia baktérie, tým znižujú tvorbu zubného kameňa. Dozvedela som sa, že sa už vyrábajú aj žuvačky s pridaním brusníc, ale zatiaľ som ich nevidela, neskúsila. Ak ich objavím, iste si kúpim. Nedávno som si kúpila urologický čaj s pridaním výťažku z brusníc. Chutil,tešil zrak krásnou červenou farbou, v neposlednej rade aj liečil.   Najväčšiu reklamu brusniciam  robí ich  nezanedbateľný význam v  už spomínanej prevencii pred ochoreniami močového mochúra a obličiek. Ich pôsobením sa vytvorí (obrazne povedané)  múr, cez ktorý sa baktérie nedostanú a  tak nemôžu priľnúť na sliznici a zahájiť svoju devastačnú činnosť.   Túto  liečivú pochúťku si môžeme pripraviť aj doma. K brusniciam sa dostaneme vlastným úsilím, vlastným zberom (nie je to prechádzka ružovou záhradou), alebo sa poobzeráme po tržnici, alebo si ich vypestujeme v záhrade. Sú vhodné na kompótovanie, na prípavu želé.   Ak si niekto nemôže zvyknúť na ich typickú horkosť, je možné do kompótu pridať trebárs  hrušky. Je zaujímavé, že kompót nie je nutné ani sterilizovať, brusnice vďaka látkam,v nich obsiahnutých, sa nepokazia. Ale nenavádzam, istota je istota... Brusnice sa môžu aj sušiť, môžeme tak urobiť sami, alebo si ich jednoducho kúpime. Je to vhodný mls hlavne pre tých, ktorí mávajú  opakované problémy s močovými cestami.   Brusnice si sami zavárame už niekoľko rokov. Používam menšie fľaštičky, ktoré sa takmer vždy jednorázovo k obedu skonzumujú. Nevadí však ani zavarenie do väčších fliaľ, kompót aj po otvorení vydrží v chlade dlho, bez ujmy na kvalite. Nemusíte sa báť, že sa začne kaziť.   Narobila som vám chute? Sebe veru áno... Ešte, že stačí siahnuť do zásob a  nechať si na jazyku povaľovať tieto trpko - sladké korálky a vychutnávať si ich úžasnú chuť.   Ešte malý tip: kompót je vhodným obohatením aj pre tých, čo majú radi vločky. Vmiešajte si do nich lyžičku a získate chutné, zdravé raňajky! Dobrú chuť. Mňam.           Foto: zdroj internet 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (102)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Chcem byť strom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Zabudnutá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Sama sa  rozhodla
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Kľúč nosíš v sebe
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Dratvová 
                                        
                                            Vraj
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Dratvová
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Dratvová
            
         
        dratvova.blog.sme.sk (rss)
         
                                     
     
        Milujem písmenká a rada sa s nimi hrám.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    590
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1365
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Sprcha duše vo verši
                        
                     
                                     
                        
                            jedlo, bylinky
                        
                     
                                     
                        
                            oprášené spomienky
                        
                     
                                     
                        
                            Môj zverinec
                        
                     
                                     
                        
                            Čriepky dňa
                        
                     
                                     
                        
                            Uletené, pokus o fejtón
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené kadečo
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




