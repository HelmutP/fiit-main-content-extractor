
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Peter Kotrčka
                                        &gt;
                OpenSource
                     
                 Jazyky 

        
            
                                    20.4.2010
            o
            12:41
                        (upravené
                20.4.2010
                o
                13:03)
                        |
            Karma článku:
                1.03
            |
            Prečítané 
            435-krát
                    
         
     
         
             

                 
                    Ako som sa rozhodoval o jazyku na učenie (plus pár vecí okolo).
                 

                 Objednával som si knižky. Napek tomu, že vraj je všetko na webe, osobne nerád kradnem (rozumej - sťahujem). Aj keď pár krát som tak učinil - obsah, ktorý inak nie je možné na Slovensku a v blízkom okolí zohnať.   Knižka ja však iná zležitosť. Milujem knihy. Milujem jazyky - aj keď pravdupovediac, ani jeden neviem na celých 100 percent (ani slovensky, priznám to bez mučenia). No rád sa učím, rád spoznávam, rád venujem časť volného času na štúdium - aj keď som po skončení školy povedal, že učenie nikdy viac.   Takže späť ku knižkám, jazykom. Učím sa fínsky, to je dlhodobejšia záležitosť. Ale v poslednej dobe ma zaujali aj ostatné "severské" jazyky a ja som sa rozhodoval, že čo si k exotickej fínčine priberiem. Ono totiž, je ťažko sa plne sústrediť na jeden jazyk cez 2-3 hodiny denne.   Vytipovaných jazykov bolo viac, no výber bol obmedzovaný z viacerých strán. Tak po prvé, nech je okolo tohto jazyka dobrá komunita na jazykovom serveri Unilang (viď predchádzajúci zápis na mojom blogu). Spĺňa to viacero jazykov, to bez diskusie, ale... Kto bol na tomto serveri, tak zistil, že kopec jazykov vykazuje aktivitu len pri hre "Osoba podo mnou". Hra OK, precvičvať pomôže, inak slabota.   Jazyk som si vyberal samozrejme aj podľa toho, či sa mi páči (vypadla nemčina - čím sa ospravedlňujem nemecky hovoriacim, nie je mi vôbec nemecko a ani jazyk sympatický), či je reálne jeho učenie (vypadli jazyky s klikyhákmi - nemám veľmi chuť učiť sa pol roka rozoznávať symboly, aby som bol schopný plynule čítať písať, no stále ešte nie rozumieť), či je reálna návšteva oblasti, kde sa používa (vypadlo takmer všetko mimo Európy a blízkeho okolia).   V skratke - páčia sa mi severské jazyky, niektoré z nich sú aj gramaticky jednoduché. Páčia sa mi baltické jazyky, fakt radosť počúvať. Páčia sa mi niektoré slovanské jazyky (bulharský, ukrajinský a ruský jazyk). Vždy sa mi páčila francúzština - no tá musí počkať. Takže prišiel na rad rozhodovací test - bude to jazyk, ktorý bude mať slušnú učebnicu.   Ruština bez problémov, len trošku mimo mojich možností (finančných - respektíve mimo sumy, ktorú som bol ochotný za dobrú učebnicu utratiť). Severské jazyky bledšie - nórska učebnica malá a vraj nie kvalitná, švedska pre istotu nikde. Baltické jazyky boli s výnimkou lotyšského slovníka beznádejné. Dánčina však dobrú učebnicu má, dokonca za slušnú cenu. 260 strán lekcií, 170 strán gramatiky, prekladov a minislovníka. To sa mi páči. Kde ju však objednať, keď kopec netových kníhkupectiev ju má nedostupnú. Dúfam, že vyjadrenie spokojnosti a pochvaly nebude reklamou - našiel som Artfórum (síce knižky si pozerám často, na toto kníhkupectvo si nepamätám), knižku objednal, onedlho bola doma. Personál komunikoval, všetko super. Teda super - aby som neprechválil. Všetko bolo tak, ako má byť - čo však na Slovensku je pomerne veľký úspech.   Takže záver - inšpiráciu na učenie jazykov mám asi do konca života, je ich kopec krásnych a príťažlivých. Zatiaľ však skúsim dánčinu. 3, 2, 1, štart. Jeg hedder Peter... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Správy STV/RTV:
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Červená tabuľka
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            02 je drahé
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Dlhodobý problém 02
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Peter Kotrčka 
                                        
                                            Prečo nie je 02 môj hlavný operátor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Peter Kotrčka
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Peter Kotrčka
            
         
        kotrcka.blog.sme.sk (rss)
         
                                     
     
        25/184/72 - narodený v deň, keď má meniny Adolf, IT, hamradio a cyklofanatik...

 
  

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    63
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1383
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            OpenSource
                        
                     
                                     
                        
                            Fotografovanie
                        
                     
                                     
                        
                            Cestovanie a iné
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Sex na pracovisku
                     
                                                         
                       Má Ficova vláda prepojenie na scientológov?
                     
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Privatizácia Smeru
                     
                                                         
                       Cargo – obrovské dlhy, vysoké platy a pokútne spory
                     
                                                         
                       Na ceste po juhovýchodnom Poľsku
                     
                                                         
                       Osobnosti na ceste
                     
                                                         
                       Počiatkov pochabý nápad: zadržiavanie turistov za 30 EUR
                     
                                                         
                       "Rómsky problém"
                     
                                                         
                       Rómska otázka – aké sú vlastne fakty?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




