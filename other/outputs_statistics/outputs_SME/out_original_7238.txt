
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rado Tomek
                                        &gt;
                Hudba
                     
                 Ida Kelarová dnes v Nu Spirit Clube spojí svety Rómov a jazzu 

        
            
                                    25.5.2010
            o
            9:08
                        (upravené
                25.5.2010
                o
                9:16)
                        |
            Karma článku:
                3.38
            |
            Prečítané 
            788-krát
                    
         
     
         
             

                 
                    Nu Spirit v máji predstaví bratislavčanom prvú dámu rómskej hudby zo susednej Českej republiky. Ida Kelarová je synonymom spojenia dvoch svetov – rómskej kultúry a sveta jazzu. Svoj nový album “Aven Bachtale” predvedie naživo dnes 25. mája v Nu Spirit Clube na Šafárikovom námestí 7.
                 

                 Čaká na vás večer plný emócií, ktoré si podmania každého, kto má otvorené srdce. Veď Aven Bachtale znamená v rómčine “Buďte štastní.” V jej novom albume pulzuje rómska krv, je to však komplexná spoveď zrelej ženy, ktorá svoju cestu životom zapisuje v notách. Ida sa na pódium postaví spolu s gitaristom, spevákom a kapelníkom Desideriom Duždom a formáciou Jazz Famelija. Toto zoskupenie mixuje rómske a jazzové prvky, spája rómske harmónie a latinsko-americké rytmy so zvukom jazzovej kapely.   Akcia sa začne o niečo skôr ako ostatné koncerty v Nu Spirit Clube, už o ôsmej. Lístky si môžete kúpiť v predpredaji u Dr. Horáka, v Artfóre a na všetkých predajných miestach siete Ticketportál.   “Moje nejsilnější inspirace a vzpomínky mi dávalo to, když mě táta posadil na skútr a vzal mě s sebou k jeho romské rodině do Horných Salib. Svoboda. Láska. Pravda. Prostor. Život, o jakém jsem snila. Tam jsme vždycky viděli tatínka šťastného. Tam jsem zjistila, co to s člověkem udělá, když je volný, když se nedusí, když může svobodně dýchat. Táta byl jinak celý život smutný, ale v Salibách byl doma. Hrálo se, zpívalo se, plakalo se, radovalo se. Tam byly opravdové city.”    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (11)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Keď sa hip hop stretáva s jazzom - pozvánka na Roberta Glaspera
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Depeche a Billy Idol Vs Robert Glasper, Kurt Elling a Zappov band
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Nebojte sa japonskej scény alebo pozvánka na stredu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Prečo sa teším na Hot 8 Brass Band (alebo Nebojte sa dychovky)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rado Tomek 
                                        
                                            Ako skoncovať so strachom z hip hopu: Lekcia 2, Dámy za mikrofónom
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rado Tomek
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rado Tomek
            
         
        tomek.blog.sme.sk (rss)
         
                                     
     
        som ekonomicky novinar a okrem toho aj DJ Kinet, promoter, majitel bratislavskeho Nu Spirit Baru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    102
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1408
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď sa hip hop stretáva s jazzom - pozvánka na Roberta Glaspera
                     
                                                         
                       Bloger Ondrášik zavádza o kardinálovi Turksonovi
                     
                                                         
                       Depeche a Billy Idol Vs Robert Glasper, Kurt Elling a Zappov band
                     
                                                         
                       Oracle Slovensko systémovo napomáha korupcii a miliónovému tunelu
                     
                                                         
                       Nebojte sa japonskej scény alebo pozvánka na stredu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




