
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Adela Zábražná
                                        &gt;
                Nezaradené
                     
                 Nad Tatrou sa blýska.. na horšie časy? 

        
            
                                    12.3.2010
            o
            0:51
                        |
            Karma článku:
                11.00
            |
            Prečítané 
            2875-krát
                    
         
     
         
             

                 
                    Slovenská republika sa opäť raz vyznamenala. Do platnosti pravdepodobne od 1.apríla 2010 vstúpi tzv. "vlastenecký zákon". Od apríla sa teda z každého Slováka stane národovec a patriot, ako sa patrí. Aspoň pro forma.
                 

                     Slovenská republika, jedna z najmladších demokracií v rámci Európy, sa opäť raz vyznamenala. Po schválení tlačového zákona a zákona o štátnom jazyku by si človek myslel, že na jedno volebné obdobie už snáď stačilo. V posledných dňoch však stránky domácich aj svetových médií plní vlastenecký zákon z dielne nacionalistickej SNS. Pre nezainteresovaných čitateľov, vlastenecký zákon v týchto chvíľach leží na stole slovenského prezidenta Ivana Gašparoviča. Ten má do 20.marca 2010 právo rozhodnúť sa, či zákon podpíše, alebo ho vráti do NR SR na prerokovanie. V prípade, že pán prezident si bude stáť za svojimi slovami a sloganom „Myslím národne, cítim sociálne“, ktorý použil v predvolebnej kampani v roku 2009 a zákon podpíše, od 1.apríla 2010 sa z každého občana Slovenska povinne stane Slováčisko ako repa. Aspoň pro forma.   Podľa nového zákona totiž hymna Slovenskej republiky bude znieť vždy na začiatku týždňa nielen vo všetkých triedach základných a stredných škôl, ale aj pred každým zasadnutím parlamentu, zastupiteľstiev a zasadnutí obcí. Verejnoprávne médiá budú musieť povinne začleniť hymnu do svojho vysielania a v deň štátneho sviatku vlastenecky zameraným vysielacím blokom utvrdzovať divákov a poslucháčov v tom, že sú naozaj Slováci. Okrem toho musí byť vo všetkých učebniach a miestnostiach zastupiteľstiev vyvesený štátny znak, zástava, text štátnej hymny a preambula Ústavy SR. Na nákup týchto štátnych symbolov si podľa vyjadrení vlády musí nájsť prostriedky každá škola v rozpočte obce, pod ktorú patrí. A verte či neverte, pre priemernú školu s 30 učebňami sa náklady len za štátnu vlajku a znak do každej triedy vyšplhajú na približne na 500 eur   Na prvý pohľad je zákon neškodný, hymna, ruka na srdci a sľub vlasti sú pre pol miliardy občanov Spojených štátov amerických rutinnou záležitosťou. Takisto možno súhlasiť s obhajobou SNS, že Slováci by mali byť hrdí, že po dlhom období utláčania a popierania práva na samostatný štát, sa Slovensku podarilo postaviť sa na vlastné nohy. Ako povedal predseda SNS, Ján Slota, „ešte aj tatíček Masaryk stále tvrdil, že sme československý národ“. Odhliadnuc od iných historických súvislostí, o krivdách na Slovákoch sme už počuli mnoho a každého pravého patriota zabolí pri srdci, keď číta Mor ho!. Prečo teda zákon vyvoláva tak rozporuplné reakcie medzi študentmi a učiteľmi, akademickou obcou či samotnými politikmi?   Dôvodov je hneď viacero. Snáď najpodstatnejším je načasovanie schválenia zákona. Tri mesiace pred voľbami do parlamentu, v období, keď Slovenskom otriasajú mnohé politické a korupčné škandály a politici vyčkávajú na každú najmenšiu chybu svojho protivníka sa nemožno diviť, že proti zákonu hlasovali iba poslanci z radov maďarskej menšiny a takmer celá opozícia sa takticky zdržala hlasovania. Prečo? Dôvod je jasný. Slovenská politická scéna je značne polarizovaná a medzi najvýraznejšie štepné línie patrí nacionalismus verzus príslušnosť k etnickým a národnostným menšinám. Takzvaná „maďarská karta“ sa úspešne využíva pred každými voľbami a priemerný slovenský volič na ňu neustále reaguje. Kto by si v tejto situácii dovolil riskovať nálepku „zradcu národa“ a „posluhovača Maďarov“?   Ďalšou príčinou odporu voči vlasteneckému zákonu je strana, ktorá za zákonom stojí. Ján Slota, Anna Belousovová a Rafael Rafaj, hlavní predstavitelia SNS a obhajcovia zákona, sa vryli veľkej časti Slovákov do pamäte ako arogantní a neznášanliví politici, pre ktorých etika a morálka sú skôr nadávky. Od nástupu koalície SNS-HZDS-SMER sa vzťahy s Maďarskom obojstranne zhoršili a nad Slovenskom sa opäť vznáša tieň politiky diskriminujúcej národnostné menšiny. V tomto kontexte nemožno zákon vnímať ako niečo pozitívne, čo by z nás malo vychovávať národne cítiacich a zároveň tolerantných občanov etnicky heterogénneho štátu. Práve naopak.   Študenti, učitelia, príslušníci akademickej obce či dokonca spisovatelia teraz žiadajú prezidenta, aby zákon nepodpísal. Vyzývali na to písomne, v médiách aj protestom, ktorý zorganizovali samotní študenti a zúčastnilo sa na ňom asi tisíc ľudí. Vyzývali Slotu a Rafaja, aby prišli medzi nich a o zákone s nimi diskutovali. Transparenty s nápismi „Vlastenectvo je cit a cit sa nedá prikázať zákonom!“ alebo „Nedemotivujte nás k vlastenectvu!“ zaplnili námestie pred Prezidentským palácom. Zneli hlasy z radov učiteľov aj študentov, ktoré volali po budovaní demokratického a k menšinám tolerantného štátu, k pozitívnemu motivovaniu mladých k vlastenectvu inými cestami a o tom, že hymna nie je šláger, ktorý by mal každý týždeň povinne znieť v ušiach tisícok školákov. Reakciou na pokojnú výzvu protestujúcich na diskusiu s predkladateľmi návrhu bola ignorácia a urážky zo strany SNS. Predseda SNS Slota sa dokonca vyjadril o mladých ako o „feťákoch, ktorí mu vôbec nevadia a neberie ich na vedomie“. V takejto atmosfére len ťažko očakávať, že si zákon získa podporu občanov a že si študáci spolu s učiteľmi budú každý pondelok radostne vykračovať zo školy s hymnou na perách.   Akú dohru bude mať táto tragikomédia a či prezident vypočuje hlas národa, ktorý ho zvolil, je otázkou, ktorej odpoveď sa dozvieme v najbližších dňoch. Už teraz je však jasné, že ak zákon prejde, 1.apríl – deň bláznov, sa stane dňom, kedy sa nad Tatrou poriadne zablýska.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Adela Zábražná
            
         
        adelazabrazna.blog.sme.sk (rss)
         
                                     
     
        Momentálne študentka. Potom sa uvidí.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2875
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




