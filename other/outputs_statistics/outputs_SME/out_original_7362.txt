
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Helena Jakubčíková
                                        &gt;
                Nezaradené
                     
                 Krása je ... 

        
            
                                    30.5.2010
            o
            21:15
                        |
            Karma článku:
                4.72
            |
            Prečítané 
            1148-krát
                    
         
     
         
             

                 
                    Čo je krása? Položili ste si niekedy túto otázku? Ako by ste definovali pojem krása?
                 

                 Nedávno som navštívila prednášku Emila Páleša s názvom "Prečo záleží na kráse". Na úvod citoval filozofa Rogera Scrutona, ktorý sa zamýšľa nad tým, ako je krása vnímaná dnes a ako sa k nej začalo v dvadsiatom storočí pristupovať z pohľadu čistého funkcionalizmu. Uvažuje o dôležitosti krásy, lebo voľne povedané krása sa redukuje na účelnosť a s účelnosťou prichádza k strate krásna a so stratou krásna prichádza ku strate zmyslu života ľudí, k strate životných cieľov, k nude, konzumu a k hľadaniu umelých mystických zážitkov napríklad aj v podobe drog.   Inšpirovaní myšlienkami pána Scrutona a pod taktovkou pána Páleša sme sa snažili nájsť odpovede na otázku čo je to krása a prečo je pre nás dôležitá. Prvé odpovede boli značne rozpačité. Nuž nikdy som sa špeciálne nad týmto pojmom nezamýšľala a ako som videla, nebola som sama. Na druhej strane nie je až takým zvykom v našich končinách prísť na prednášku a namiesto vypočutia si prednášajúceho byť ním vťahovaná do dialógu. Ale postupne nás pán Páleš navádzal na možné odpovede a dopracovali sme sa k niekoľkým definíciám. K prekvapeniu ich nebolo málo. Dovolím si ponúknuť niekoľko z nich aj vám.   Krása je:   -         most medzi duchovným a hmotným   -         je pojem spoločný i individuálny a zmysel pre ňu je vrodený i naučený   -         je meradlom účelnosti   -         nemá účel, lebo sama je zmyslom   -         hodnotovým kompasom spoločnosti   -         inšpiráciou   -         umelkyňa   -         mystička   -         anarchistka a revolucionárka   -         novátorka   -         prísľubom budúcnosti   -         dôvodom, pre ktorý sa umiera   -         vyzývateľkou v dosiahnutí niečoho   -         inšpirátorkou lásky a má moc premeniť človeka   -         tá, ktorá rozžaruje ideály v dušiach ľudí   -         tá, ktorá skrýva tajomstvo nesmrteľnosti, dáva pocit sprítomnenia večnosti   -         Sfinga, ktorá nám kladie otázku „Kto si? Čo znamenáš?"   -         tá, ktorá nám ukazuje zmysel života   -         tá, ktorá nám pomáha poznať samého seba a svoj zmysel života       Krása je ako viera. Krása pôsobí na ľudskú dušu. Nemusím hádam ani pripomínať, že nehovoríme len o kráse vonkajšej. Lebo krása je kvet na lúke, svižne kráčajúca dievčina po tej lúke, úsmev na tvári, jas v očiach, hrajúce sa deti i dve zvráskavené dlane držiace sa v láskyplnom objatí. Krása je ... všetko, čo sa dotkne našej duše a vyvolá v nej zvláštny pocit nadčasovosti. Ako hovorí pán Scruton, a dá sa s ním len súhlasiť, krása je aj všetko to, čo nám pomáha prekonať ťažkosti, akceptovať smútok, nájsť útechu, pochopiť a vyjadriť pohnutia duše.   Zamysleli ste sa niekedy, čo pre vás znamená krása?             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (39)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Emočné konto
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Idiot s Kitty
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Pomenuj svoj problém
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Preto lebo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Helena Jakubčíková 
                                        
                                            Kruhový objazd na ceste
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Helena Jakubčíková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Helena Jakubčíková
            
         
        jakubcikova.blog.sme.sk (rss)
         
                                     
     
        Som to, čo zo mňa spravili otec, mama a ja. No a samozrejme mnoho ďalších ľudí:-)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    164
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1048
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Čudnosti prečudesné
                        
                     
                                     
                        
                            Tretia alternatíva
                        
                     
                                     
                        
                            Snové arabesky
                        
                     
                                     
                        
                            Malé pohladenia
                        
                     
                                     
                        
                            Murko a tí ostatní
                        
                     
                                     
                        
                            Zápisky z ciest
                        
                     
                                     
                        
                            Mačacím pazúrikom
                        
                     
                                     
                        
                            Moje zlatíčka
                        
                     
                                     
                        
                            Závoz alebo životná križovatka
                        
                     
                                     
                        
                            Len tak
                        
                     
                                     
                        
                            Profesionálna deformácia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            všeličo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Mňamky
                                     
                                                                             
                                            Snílek
                                     
                                                                             
                                            Pavel "Hirax" Baričák
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Šťastný deň
                                     
                                                                             
                                            Ister Felis
                                     
                                                                             
                                            MačkySOS
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Susedské spolunažívanie.
                     
                                                         
                       Ako sa zo sestry stal triedny nepriateľ
                     
                                                         
                       Víťazstvo tolerancie, alebo showbiz marketingu?
                     
                                                         
                       Prírodné repelenty
                     
                                                         
                       Mnoho systému, žiadna človečina
                     
                                                         
                       Objatie
                     
                                                         
                       Snáď ho raz zabijeme!
                     
                                                         
                       Zápisky z veteriny – dni, keď nemôžem hovoriť
                     
                                                         
                       Návrat nehy medzi muža a ženu 1.časť
                     
                                                         
                       Svedok namočil smerákov, daňovákov a policajtov do podvodu na DPH
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




