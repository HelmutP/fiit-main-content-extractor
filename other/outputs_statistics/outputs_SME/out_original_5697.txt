
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ester Demjanová
                                        &gt;
                Rozhadzujem rukami
                     
                 Tak toto ma dnes dostalo... 

        
            
                                    2.5.2010
            o
            21:47
                        (upravené
                2.5.2010
                o
                21:53)
                        |
            Karma článku:
                9.65
            |
            Prečítané 
            1879-krát
                    
         
     
         
             

                 
                    Internet je dobrá vec. Ale aj veľmi nebezpečná. Po dlhom dni si sadám za počítač, lebo ma k tomu opäť čosi dohnalo -  skupina Naše Slovensko a moji priatelia na facebooku, ktorí túto stranu  verejne podporujú a „pôjdu ju voliť" (mnohí z nich sotva násťroční).
                 

                 Telefonát od priateľa ma dostal - naši spoloční kamaráti na facebooku otvorene podporujú rasistické myšlienky. Niekto si totiž uvedomil a plne začal využívať silu média. Za rovnako inteligentný ťah považujem i ideu „presvedčme ich mladých, o pár rokov budeme mať veľa slepých ovečiek".   Video nachádzajúce sa na niekoľkých desiatkach (!!!) profilov mojich facebook-(pseudo)priateľov sa mnohým pozdáva - použili všetko, čo „pravý Slovák" potrebuje - šesťročné dievčatko, zničené domy, budovy, mestá a podpis „Ďakujeme, že myslíte na našu budúcnosť, VAŠE DETI", o irónii ani nehovoriac.   Na facebooku je aj skupina s 10 983 (!!!) fanúšikmi nesúca tak strhujúce meno Naše Slovensko a podporujúca „spoločný boj proti parazitom". Po toľkých hodinách dejepisu, NOS-ky a etickej výchove ste ma vážne sklamali, Slováci pod (aj nad) osemnásť.   Internet je nebezpečná vec.   Koľkí z tých ľudí, ktorí sa dnes hlásia k ideálom, ktoré nechápu, raz bude chcieť začať nejakú kariéru?   Koľkí z nich budú chcieť byť politikmi, politológmi, diplomatmi?   A koľkí z nich budú chcieť byť aspoň dobrými rodičmi?   Dobrému informatikovi nebude trvať viac ako pár minút vytiahnuť na vás tieto vaše „chyby mladosti", človeku, čo vás bude chcieť zničiť potrvá pár minút nájsť na vás na internete špinu - a toto je úžasná príležitosť. Už vidím tie titulky: Ten a ten - mladý rasista! Dôkazy nájdete na strane 3.   Preto by som sa vás chcela spýtať, viete, za čím si vlastne stojíte? Koľkí z vás sa skutočne zaoberali stranou, ktorú na svojich profiloch propagujete? Videli ste video, videli ste, čo kritizuje, videli ste i to, čo toto video ponúka ako riešenie na problémy, ktoré vyzdvihuje? Nie? Viete prečo nie? Lebo žiadne reálne riešenie toto video neponúka (keďže podľa mňa ani žiadne jednoznačné a okamžité riešenie na tento problém, áno, uznávam, že je to problém, neexistuje). Má úcta človeku, ktorý vie takto dobre spracovaným videom pôsobiť na city (prvo)voliča.   Výroky typu „je lepšie keď ma zbije biely ako čierny, lebo cigán má lepšiu imunitu ako poslanec, toho nemôžem dať na súd" ma skutočne robia pyšnou na spoločnosť, v ktorej žijem.   ĽUDIA! Koľko tu bolo takých, ktorí prišli, vymenovali vám všetky problémy, ktoré máte, vy ste si ich zvolili a čo z toho bolo? Keď niekto iba kritizuje a žiadne riešenia priamo neponúka, je to vždy podozrivé.   Kto bude koho voliť - to nechám na každého osobnú vôľu. Toto však považujem za rasistickú propagandu, ktorá je, ak sa nemýlim, trestná. Stranou ako takou sa nezaoberám - na to nemám vek ani vzdelanie. Zaoberám sa tým, čo sa ma týka, názorom mojich kamarátov a priateľov.   Toto nie je v poriadku. Nie je v poriadku, že to dovolíme a už vôbec nie je v poriadku, že to podporujeme.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (63)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Založili ste si živnosť? Prvý list, ktorý dostanete, bude podvod
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Taká obyčajná autonehoda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            9 dôvodov, prečo som po bakalárskych štátniciach nikto
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Tri školy, dva testy, jedna hodina a stále nemám jazykovku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ester Demjanová 
                                        
                                            Diplomové práce na kľúč sú zlyhaním školiteľa a komisie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ester Demjanová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ester Demjanová
            
         
        demjanova.blog.sme.sk (rss)
         
                        VIP
                             
     
         "Pri pohľade na smrť a na tmu sa bojíme iba neznámeho, ničoho iného." (JKR) 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    74
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4282
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Moje osobné školstvo
                        
                     
                                     
                        
                            Rozhadzujem rukami
                        
                     
                                     
                        
                            Ockovi
                        
                     
                                     
                        
                            Aj o tom peknom
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Pán Spisovateľ, dobrý aj pečený
                                     
                                                                             
                                            Poberaj sa, starec
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            SlovoSvet - aby vás slová počúvali na slovo
                                     
                                                                             
                                            nailepsy slovencynar na svete
                                     
                                                                             
                                            vdacim jej za moju radost :)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




