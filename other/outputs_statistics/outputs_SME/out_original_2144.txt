
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Tomáš Dérer
                                        &gt;
                Nezaradené
                     
                 Umierajúca fabrika a genius loci (časť druhá) 

        
            
                                    28.1.2010
            o
            23:35
                        (upravené
                29.1.2010
                o
                0:15)
                        |
            Karma článku:
                12.95
            |
            Prečítané 
            6974-krát
                    
         
     
         
             

                 
                    Dynamitka, Dimitrovka, Istrochem. Základný kameň slovenského chemického priemyslu, dnes vo fáze postupného zániku. Pokračovanie včerajšej fotoreportáže .
                 

                 
  
    .   .   .   Ten úpadok a zánik samozrejme neprišiel zo dňa na deň.  Na vine nie je vek , veď vo svete prosperujú aj staršie firmy. Dokonca ani poloha nemusí byť osudná, hoci v časoch založenia továrne spoločnosti Dynamit-Nobel sa okolo fabrických pozemkov rozkladali polia, lúky a vinohrady. (Prvými obytnými štvrťami na okolí boli práve zamestnanecké kolónie.) Fabrika sa z miesta nepohla, no mesto sa rozrástlo až k nej, okolo nej, dnes sa sem doťahuje širšie centrum. Z fabriky vidno výškové budovy Polusu. A skleníkový mrakodrap Lakeside Park stojí vzdušnou čiarou necelý kilometer od prevádzok výroby výbušnín. I menšia havária v podobe detonácie by tie stovky sklených tabúľ vytĺkla. Vedenie fabriky argumentuje, že za posledných štyridsať rokov na prevádzke k žiadnemu výbuchu nedošlo (ak nerátame pravidelné testovanie vzoriek trhavín na konci denných smien, pri ktorom sa tiež triasli okná, a od ktorého sa v 90tych rokoch upustilo). Darmo, pre zástupcov mesta je výroba tohoto druhu uprostred mestských štvrtí predstavou sedenia na sude pušného prachu (to možno sedí, v skutočnosti je to však dynamit). No v Istrocheme bežali aj iné výroby, ktoré by pri splnení ekologických štandardov kľudne akceptované byť mohli - produkovali sa tu predsa prípravky na ochranu rastlín, či rôzne prísady do plastov. Švajčiarske mesto Basel (Bazilej) vykazuje mnohé analógie s Bratislavou (poloha na veľkej rieke, na hranici troch štátov), okrem iného tam tiež sídlia veľké chemické koncerny. Z nich najmä bývalá Ciba-Geigy mala podobný výrobný program ako Istrochem (pesticídy, chemické špeciality) - a nik v Baseli nerieši, že Ciba (dnes Novartis resp. Syngenta) má výrobné areály na území mesta, rovnako ako farmaceutický gigant Hoffmann-La Roche. Keby po páde socíka nastali na Slovensku usporiadanejšie pomery, je možné, že práve Ciba by Istrochem kúpila. Dopadlo to všetko inak.         Spôsob, akým Istrochem za Mečiara privatizovali, po zmene vlády r.1998 napadol súd. Až tak, že noví majitelia museli fabriku vrátiť Fondu národného majetku. Po rôznych peripetiách napokon továreň roku 2002 kúpil český miliardár slovenského pôvodu Andrej Babiš, ktorému už vtedy patrila polovica českého chemického priemyslu, a na Slovensku kúpil ďalšieho giganta – Duslo Šaľa. Istrochem sa takstal súčasťou Babišovho impéria Agrofert, a to v podradnej úlohe dcérskej firmy (či odštepného závodu) Dusla. Aké mal Babiš plány, ťažko povedať, dnes to skôr vyzerá na ústup. Odpredaj, prenájom, v podstate žiadne investície, ak nerátame nákladnú rekonštrukciu administratívnej budovy (neviem, či mala byť budova hrdou bratislavskou rezidenciou manažmentu; teraz ponúka kancelárske priestory na prenájom).              Hlavná administratívna budova na Nobelovej ulici, po rekonštrukcii   (kliknutím sa obrázok zväčší)   .   Hneď za administratívnou budovou sa v areáli továrne nachádza VUCHT (Výskumný ústav chemickej technológie), kedysi závodný výskum fabriky. Značná časť toho, čo sa v slovenskom chemickom priemysle za socíka postavilo, má svoje korene tu - v tunajšom výskume a vývoji. Nebola to možno špička svetových trendov, ale niekotré tu vyvinuté technológie ešte aj po desaťročiach znášajú terajším majiteľom chemických fabrík zlaté vajcia, či už v Dusle, v Chemku Strážske, alebo aj v samotnom Istrocheme. Dnes žije VUCHT v podstate z prenájmu priestorov; chemický výskum tu robí možno dva a pol človeka. Že bez výskumu a inovácii bude napokon každá technológia zastaralá a neschopná konkurencie? Koho to na Slovensku trápi? Kým niečo sype peniaze, nech to beží.            Nová budova VUCHT. Dokončená okolo 1990, práve vtedy, keď už výskum na Slovensku nik nepotreboval.        Ani predaj zahraničným investorom nezaručuje, že tu skutočne budú investovať. Napríklad výbušninársku výrobu Istrochemu predal Babiš českej firme Explosia Pardubice-Semtín (najväčší výrobca trhavín v strednej Európe, odtiaľ pochádza i známy Semtex). Pre Explosiu je Istrochem vážny konkurent, veď zásobuje trhavinami zhruba tie isté trhy. takže na jeseň 2009 bolo zverejnené stanovisko Explosie, podľa ktorého bratislavskú prevádzku odstavia a pozemok predajú; odvtedy je dynamitka oficiálne v likvidácii, zbavuje sa zásob a postupne prepúšťa.       Čo je hlavnou príčinou, vedia len majitelia. Isté je, že pozemky v Bratislave závratne stúpli na cene, takže si Česi zrejme zrátali, že radšej jednorazový zisk, než neistá budúcnosť výroby v susedstve mesta, na neistom Slovensku. Pôda v Istrocheme je síce dlhoročnou činnosťou fabriky zamorená a otrávená, no, pri takých cenách pozemkov sa sá uvažovať aj o hĺbkovej asanácii. Developeri si brúsia zuby, rôznych stavebných projektov majú plné zásuvky, uvidíme, čo napokon na hrobe niekdajšej dynamitky vyrastie.       Babiš by sa zrejme stiahol z areálu Istrochemu celkom, zatiaľ ho tu drží jediná výroba, ktorá mu (po predaji trhavín Explosii) ešte prináša zisk: gumárenské chemikálie (prevádzka Sulfenax):      (kliknutím sa obrázok zväčší)   .   Po týchto úvahách mi zostáva už len dokončiť fotoreporáž z minulej časti článku, sľúbeným pokračovaním o cechu výbušnín. Táto výroba má tradíciu od založenia fabriky r. 1873, a okrem 1.svetovej vojny sa vždy zameriavala na priemyslené trhaviny, teda pre použitie v kameňolomoch, baniach, na demoláciu starých stavieb a pod. (Na výrobu vojenských trhavín, najmä hexogénu a pentritu, bola za socíka založená osobitná fabrika: Chemko Strážske na Východnom Slovensku). V Bratislave sa vyrábali trhaviny dynamitového typu, to jest také, ktorých hlavnou zložkou je nitroglycerín (glyceroltrinitrát), obvykle v zmesi s glykoldinitrátom; pod obchodným názvom Danubit. Teda, je ich viac druhov - pridajme aspoň ilustračné foto:      K vysoko brizantným dynamitom časom pribudli i produkty na báze dusičnanu amónneho, buď ako jednoduché ANFO-zmesi (dusičnan amónny- palivo, skrátene DAP), čo sú "lenivé", ale súčasne bezpečné a lacné trhaviny, alebo sypké zmesi s prísadou tritolu.       Urobiť fotoreportáž zo (zatiaľ ešte existujúcej) prevádzky trhavín je problém, kedže jej areál je od ostatných častí fabriky oddelený ďalším ostnatým plotom, a má osobitnú strážnu službu (čo asi neprekvapí).  Výrobné objekty sú prevažne prízemné stavby, obohnané zemnými valmi, takže dovnútra sa vchádza tunelmi. Úlohou týchto násypov je chrániť okolie pred tlakovou vlnou v prípade výbuchu vovnútri. Aby ste mali predstavu, výrobne v dynamitkách vyzerajú asi takto:         Takýchto bunkrovitých stavieb je tu okolo tridsať, a sú takmer dokonale maskované hustým, na tento účel vysadeným lesným porastom. To je napokon bežná prax i vo svete, ale kým na družicových snímkach Google Earth uvidíte v iných fabrikách aspoň samotné stavby medzi stromami, tu je maskovanie skoro dokonalé. Napokon, presvedčte sa sami -vidíte viac, než len akýsi lesopark?                            (kliknutím sa obrázky zväčšia)       Skutočne, až na tej poslednej snímke sa dajú rozoznať zasnežené zemné valy. Kto chce, môže skúsiť zabúchať na bránu, že si chce v stráženom areáli urobiť nejaké fotky - pochopením zo strany strážnej služby si ale nie som istý. OK, v časoch reálnej hrozby terorizmu nie je opatrnosti nazvyš.       Takže vám môžem akurát ukázať jednu fotku z netu, kde nejaký pán inštaluje v tomto areáli vzduchotechniku:       Mimochodom, výroba nitroglycerínu je v Istrocheme na modernej úrovni, riziko výbuchu pri tzv. injektorových reaktoroch je minimálne, a ak, na jednom mieste je naraz vždy len malé množstvo výbušniny. Nehrozí tu žiadna apokalypsa, porovnateľná s touto (BASF, 1921):      .A kvalita finálnych trhavín je tiež slušná, včetne špeciálnych produktov, ako je napr. bezpečnostná banská trhavina Harmonit AD, použiteľná i v rizikových uhoľných baniach. Je pravdepodobné, že terajší vlastník - Explosia Pardubice - si pri likvidácii bratislavskej prevádzky vezme najlepšie produkty do svojho výrobného portfólia.   Týmto by sme exkurziu po najstaršej chemickej fabrike Slovenska mohli aj ukončiť. Budúcnosť nepozná nik. Je škoda, že z Istrochemu nevznikla zmodernizovaná, na vysoko sofistikované chemické produkty zameraná high-tech fabrika (liečivá, pesticídy, farbivá, antioxidanty, atď.), tým viac, že existovala infraštruktúra, a u niektorých typov výrob i tradícia a skúsenosť. Zánikom tejto fabriky budeme zasa o krok bližšie k banánovej republike. Škoda.   Napokon je ešte otázny osud jednotlivých objektov. Časť Závodu mieru bude údajne pamiatkovo chránená. V hlavnom areáli je ešte vidno industriálnu architektúru z 19. storočia. Nie som síce veľkým fanúšikom projektov typu Kulturfabrik, ale historická cena niektorých stavieb je zrejmá. Nuž ale, v meste, kde sa developeri a stavebné firmy nebabrali so starou stavbou kablovky (ani s novším PKO), by som nemal prehnané očakávania. Takže predtým, než ten svet zanikne, ešte fotka na rozlúčku:            A celkom na záver, jeden takmer malebný pohľad z diaľky, z vinohradov na úpatí Malých Karpát:      (kliknutím sa obrázok zväčší)    Tým, ktorí to vládali dočítať až sem, ďakujem za pozornosť.   .              

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (34)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Dojmy a pojmy fotoamatéra (1.časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Čo majú podľa mňa urobiť učitelia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            Novembrové impresie po litri vína
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            O živote po živote (3.časť)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Tomáš Dérer 
                                        
                                            O živote po živote (2.časť)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Tomáš Dérer
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Tomáš Dérer
            
         
        tomasderer.blog.sme.sk (rss)
         
                                     
     
        Chemik, euroskeptik, klimaskeptik, slobodomyseľný bezočivec a kacír, spochybňovač autorít, podozrivý zo sympatií k libertariánom. Súkromník slúžiaci zahraničnému kapitálu. Sodoma-Gomora? Čo už, nič lepšie o sebe mi nenapadá. To najlepšie na mne sú moja žena a moje deti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2117
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




