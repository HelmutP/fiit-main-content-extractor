
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriela Oremová
                                        &gt;
                Nezaradené
                     
                 Chlebík a iné... 

        
            
                                    23.3.2010
            o
            10:42
                        (upravené
                23.3.2010
                o
                13:41)
                        |
            Karma článku:
                13.57
            |
            Prečítané 
            1139-krát
                    
         
     
         
             

                 
                    Pred časom Anglicko sledovalo príbeh dvoch mužov s vysokoškolským vzdelaním, ktorí sa rozhodli žiť len z toho, čo supermarkety vyhodia. Každý večer navštevovali kontajnery, miesto toho, aby nakupovali potraviny, ako ostatní obyvatelia.  S touto svojou záľubou sa všetkým pochválili, nakoľko to nebola činnosť ,len tak z rozmaru. Chceli ňou obrátiť pozornosť širokej verejnosti na  to, ako sa s potravinami dnes nakladá. Vyslúžili si obdiv, odsudzovane aj opovrhovanie, ba našli sa aj takí, čo ich chceli postaviť pred súd.
                 

                 Neviem , či sa im podarilo v Anglicku niečo zmeniť, lebo som sa vrátila na Slovensko. A čuduj sa svete,  na nedostatok sa to ani u nás  nepodobá. To, čo hlavne po víkendoch a sviatkoch trčí z kontajnerov v našich mestách, ani trochu nevypovedá o kríze, ktorú práve prežívame.    Vyzerá to tak, že čím viac ľudstvo napreduje, tým menej si váži svoju potravu. Kde sú tie časy, keď sa úcta ku chlebíku vštepovala deťom od kolísky. Ja si ešte spomínam na to, že bochník chleba sa žehnal pred krájanim a aj na to, že omrvinky sa po zemi nesmeli mrviť. Ani v škole sme nedojedenú desiatu nehádzali do koša a spolužiaci sa neobhadzovali čokoládovými vajíčkami po veľkonočnej oblievačke, ako som to videla pred rokom.   Dnes, keď sa mnohé premiešalo a zotreli sa rozdiely, keď si v nákupných centrách vyberáme z rozmanitej ponuky, akoby sme zrazu strácali zdravý úsudok. Reklamy nás často presviedčajú, že bez toho či onoho výrobku ,nebudú naše sviatky ozajstné a tak neodoláme. No všetko má svoju záručnú dobu a človek má aj cez víkend aj cez sviatok len jeden žalúdok.   Skutočne v tom horšom za svetom nezaostávame, dokonca nám patria mnohé smutné prvenstvá. Zdá sa, že len málokto spája chorobu srdca, ciev, čriev.....s jedlom. Žiaľ stále na Slovensku vo všeobecnosti platí, že žijeme len raz, tak hádam nebudeme hladovať, keď si konečne možeme dovoliť všetko a ani v rade  netreba stáť.   Takto nebadane sa nám vytracajú zo života schodíky na hodnotovom rebričku, vlastne ich nahrádzame inými, modernejšími. No úcta k potravinám aj k našej Zemi je len obrazom toho, akú úctu prechovávame každý sám k sebe! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Skúsim
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Miesto pre život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Vôbec nechápem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Nečakaj
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriela Oremová 
                                        
                                            Zázračná hruška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriela Oremová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriela Oremová
            
         
        oremova.blog.sme.sk (rss)
         
                                     
     
        Som presvedčená ,že ak človek do svojho zámeru vloží silu svojej duše a teplo svojho srdca, celé nebo sa spojí, aby mu pomohlo!

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    43
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    779
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




