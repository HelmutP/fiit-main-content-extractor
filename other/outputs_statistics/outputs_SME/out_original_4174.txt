
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marek Bartkovský
                                        &gt;
                Nezaradené
                     
                 Sú normálne útoky na pápeža? 

        
            
                                    8.4.2010
            o
            16:28
                        (upravené
                8.4.2010
                o
                17:44)
                        |
            Karma článku:
                8.31
            |
            Prečítané 
            1795-krát
                    
         
     
         
             

                 
                    V posledných dňoch zažívame šok zo zneužívania maloletých kňazmi. Média prinášajú rôzne smutné správy, ktoré sa odohrali v minulosti. Ľud obviňuje pedofilných kňazov, ale aj biskupov, že nekonali proti nim. Najnovšie niektorí nešetria i samého pápeža.
                 

                 
  
       "Murphyho prípad"   Denník New York Times 25. marca 2010 napadol pápeža Benedikta XVI a štátneho sekretára Vatikánu Tarcisia Bertoneho na svojej titulnej strane. Podľa denníka kardinál Ratzinger a Bertone ututlali v roku 1996 prípad, ktorý sa týkal pedofilného kňaza L. Murphyho z arcidiecézy Milwaukee. A takisto denník tvrdí, že neurobili nič proti americkému kňazovi, ktorý mal údajne zneužiť až dvesto nepočujúcich chlapcov.   Kňaz Lawrence Murphy pracoval medzi rokmi 1950 až 1974 v škole pre nepočujúcich. Tento kňaz  zomrel v roku 1998.   Kardinál J. Ratzinger bol vtedajším prefektom Kongregácie pre náuku viery. V roku 2005 sa stal pápežom. Prijal meno Benedikt XVI. Tento rok si Cirkev pripomenie 5. výročie zvolenia pápeža.   Niektoré združenia obetí a kritici žiadajú, aby súčasná viditeľná hlava Cirkvi rezignovala. Ako to môžu požadovať? Pápež nie je minister.  J. Ratzinger sa stal Benediktom XVI. Petrovým nástupcom. To nie my sme si ho zvolili za pápeža, ale niekto Väčší, ako sme my.   New York Times sa môže hanbiť! Zo strany Kongregácie pre náuku viery to prebehlo bezchybne, z pohľadu kánonického práva ako aj z pohľadu morálky.  Okolo roku 1975 Murphyho obvinili. Prípad bol hneď oznámený civilným autoritám, ktoré nenašli dostatočné dôkazy, aby mohli ďalej postupovať proti nemu. V tejto veci bola Cirkev prísnejšia ako Štát. „Žiadne utajovanie, žiadny zákaz ohlásiť prípady zneužívania, Kongregácia pre náuku viery bola informovaná o prípadoch až o 20 rokov neskôr" - takto reagoval hovorca Svätej stolice p. Federico Lombardi SJ na obvinenia, ktoré smerujú na Vatikán v takzvanom "Murphyho prípade". Ďalej páter Lombardi SJ hovorí: ,,...Na konci 90. rokov, po viac ako dvoch desaťročiach od oznámenia zneužití diecéznym predstaviteľom a polícii, bola po prvýkrát položená Kongregácii pre náuku viery otázka, ako kánonicky postupovať v Murphyho prípade. Kongregácia bola informovaná o záležitosti, pretože išlo o zvádzanie v spovednici, čo je narušením sviatosti zmierenia. Je dôležité poznamenať, že kánonická otázka predstavená Kongregácii, nebola nijakým spôsobom spätá s akýmkoľvek potenciálnym občianskym či trestným konaním proti otcovi Murphymu..." A vyjadrenie k tomuto prípadu končí slovami: ,,...Vo svetle faktu, že otec Murphy bol starší a nachádzal sa v zlom zdravotnom stave, a že žil v ústraní, ako aj toho, že počas obdobia viac ako 20 rokov neboli ohlásené ďalšie prípady zneužitia, Kongregácia pre náuku viery navrhla, aby arcibiskup Milwaukee vyriešil situáciu napríklad tým, že obmedzí verejnú službu otca Murphyho a bude vyžadovať, aby prevzal plnú zodpovednosť za závažnosť svojich skutkov. Otec Murphy zomrel asi štyri mesiace po tom, bez ďalších incidentov."   Takže ak je nejaký denník, ktorý by nám prišiel na myseľ, keď sa hovorí o antikatolíckych lobby, je to práve New York Times.   Benedikt XVI. je pápežom tretieho tisícročia   „Benedikt XVI. je silným pápežom, pápežom tretieho tisícročia", povedal kardinál Tarcisio Bertone vo svojom príhovore novinárom na letisku v Santiagu de Čile, kam včera pricestoval, aby navštívil ľudí postihnutých zemetrasením.  Štátny sekretár Vatikánu označil za nepodložené obvinenia publikované niektorými novinami ohľadom prípadu amerického pedofilného kňaza Murphyho. Dekan zboru kardinálov Angelo Sodano v rozhovore pre vatikánsky denník L´Osservatore Romano uviedol, že na pápežovi Benediktovi XVI. sa prejavuje „kultúrny konflikt". „Pápež, stelesňuje morálne pravdy, ktoré mnohí neprijímajú,   a tak sa chyby a zlyhania kňazov stávajú zbraňami proti Cirkvi... Teraz sa proti Cirkvi útočí obvineniami z pedofílie".       Pastiersky list Benedikta XVI. írskym katolíkom   Svätému Otcovi záleží na poriadku a pokoji v Cirkvi, aj preto zaslal do Írska pastiersky list.   Obvinenia a útoky na pápeža sú nelogické a nenormálne.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (450)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Dopisoval som si s europoslankyňou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Nech si taliansky umelec vystavuje svoju rodinu a nie pápeža!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            25. marec - Deň počatého dieťaťa
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Kaddáfího koniec, Ficovo ticho
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marek Bartkovský 
                                        
                                            Lesbičky prehrávajú, tak nech sa už vzdajú!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marek Bartkovský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marek Bartkovský
            
         
        bartkovsky.blog.sme.sk (rss)
         
                                     
     
        Chcem písať o súčasnom dianí...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    23
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    601
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Rodina
                        
                     
                                     
                        
                            Spoločnosť
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zaujímavé blogy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            TK KBS
                                     
                                                                             
                                            Postoy
                                     
                                                                             
                                            Svet kresťanstva
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Dopisoval som si s europoslankyňou
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




