
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Natália Blahová
                                        &gt;
                nezaradené
                     
                 Iskra - zväzák - pionier 

        
            
                                    27.2.2008
            o
            1:15
                        |
            Karma článku:
                11.92
            |
            Prečítané 
            9886-krát
                    
         
     
         
             

                 
                    Sľubujem pred svojimi druhmi, že budem budovať a brániť socialistickú vlasť. Tak mi pánboh pomáhaj!
                 

                   - Mami, to čo ste boli, keď ste boli tí pionieri?     - Hm... to sme sa tak pripravovali, aby z nás boli dobrí komunisti.   Len si pokojne sadni, milé dieťa a počúvaj svoju starú matku, ako sa žilo za jej mladých čias.   Namiesto prvého svätého prijímania sme všetci tretiaci stáli na námestí a sľubovali pred svojimi druhmi. Odmenou do nás zabodli slušivý odznačik a dali červenú šatku na krk. Na iskričkovských schôdzach sme sa predtým naučili, ako ju správne uviazať, aby bola v tvare jednotky. Bol v tom hlboký symbol toho, že si mienime svoju vlasť uctiť jednotkami v žiackych knižkách. Niektorí nemali k svojej vlasti žiadnu úctu.  A učili nás aj iné veľmi dôležité veci - nakresliť pioniersky znak, riadne po pioniersky sa pozdraviť, čo značí, na výzvu Budovať a brániť socialistickú vlasť buď pripravený! - zarevať odpoveď - Vždy pripravený!, nakresliť sovietsku vlajku a Matriošku, poznať detstvo Voloďu Ulianova, neskôr zvaného Lenin...     - Viem, to je ten sušený...     - Pioniersku rovnošatu sme potom obliekali pri každej vhodnej príležitosti. Neodmysliteľne patrila k oslavám Sviatku práce. Dlho pred prvým májom sme so súdružkami učiteľkami nacvičovali v telocvičniach pochodový krok a naše bojové pokriky, ktorými sme mali pozdraviť tribúnu.   - Nech žije prvý máj! V duchu počítať - raz, dva - Nech žije KSČ! - raz, dva.     - To čo ste boli hladní?     - Hm?     - Keď ste kričali, že nech žije KFC...          - Šťastné to dieťa! Nie KFC, lež KSČ. Komunistická strana Československa. Na tribúne boli zhromaždení všetci jej poprední funkcionári a pritom vtedy nikoho nenapadlo túto ich hustú koncentráciu na meter štvorcový zneužiť a spáchať podvratnú činnosť.  My deti sme počas dlhého čakania, kým príde rad na náš pochod pod tribúnou, trávili pozorovaním alegorických výjavov. Prechádzali okolo nás ľudia v károvaných košeliach a montérkach, držiac v rukách kelne a kladivá, lekári v bielych plášťoch s fonendoskopmi na krku, ženy v ručníkoch ťahajúce za sebou drevené kravy...a my sme si aspoň pri tých šialených dospelých nepripadali až tak hlúpo, že držíme v rukách obrie busty súdruhov Lenina, Stalina a Gottwalda. Keď nás omrzeli pochodujúce davy aj špáranie sa v Klementovom nose, vymysleli spolužiaci alegoricky vystrojení ako športovci z atletického oddielu, že budú vrhať po súdruhovi Stalinovi oštepom. Oko bolo bodované najvyššie. Bohužiaľ sa prepočítali v tvrdosti materiálu. V momente, keď trafil prvý oštep zbožňovanú podobizeň, rozpadla sa na tisíc nezlepiteľných kúskov. Tie v strachu nenápadne odniesli do smetiaku, no zmiznutie svetového politického lídra v nadživotnej veľkosti nenápadné nebolo a preto neuniklo pozornosti súdružky učiteľky. Tá vinníka rýchlo odhalila a jeho opovrhnutiahodné konanie potrestala tým, že mu pridelila miesto oštepu trojmetrovú zástavu Sovietskeho zväzu. Bohužiaľ jej konanie sa minulo účinkom. Previnilec bol totiž nižšieho vzrastu a mušej váhy a keďže v ten deň výdatne pofukovalo, zástava vodila jeho a nie on ju, čo bolo obzvlášť mrzuté hlavne pred tribúnou. Väčšina spolužiakov sa smiala tak, že zabudli aj na Nech žije Prvý máj, raz-dva.   Nuž za našich mladých čias sa masové akcie nosili. Napríklad taká Spartakiáda. To bola pekná akcia.     - To poznám. Raz som to pozeral v telke. Majú to aj v Číne. Robili tam také obrazce z farebných tabuliek. Jeden chlapík zabudol vymeniť tabuľku, potom sa stále mýlil, tak ho zastrelili.     - Áno, Čína bola naším veľkým spojencom a tradície si zachováva dodnes. U nás sa za prehrešky posielalo napríklad do uránových dolov alebo do väzenia. Chlapíci v dlhých čiernych kabátoch zvaný ŠTB, vyskákali z Volgy, zazvonili pri dverách a už ťa nikdy nikto nevidel.   Ale nás deti tieto taľafatky nezaujímali. Čítali sme Agaňók a učili sme sa spievať budovateľské piesne. Napríklad:  Žiari šatka pionierska sťa červený krásnýýý kvééét  žiari šatka pionierska už po mnoho dávnych liééét  utkaná je z boja z práce, utkaná je z radostíííí,  nesieme jej pozdrav vzácny plný detskej radostííí...     - Mami, prosím ťa, nespievaj!     - Ja viem, verše trochu invalidné, no melódia chytľavá. Veľa týchto piesní zaznelo za sprievodu gitary pri vatrách v pionierskych táboroch. A veľa invalidných veršov bolo odrecitovaných pri príležitosti schôdzí a štátnych sviatkov. Aj na toto sa používali pionieri. Strážili večné ohne, nakládli vence k pamätníkom, otvárali zasadania a zhromaždenia. A keďže zhromaždenie je od slova hromada, poniektorí sa tou hromadou ľudí nechali vystresovať tak, že občas čosi domotali. Niekedy až tak, že sme čakali, či nepôjdu do uránových dolov.  Spolužiak Mišo s vážnou tvárou predniesol na celoslovenskom zjazde KSS: - ...no našťastie prišiel hysterický rok 1948...- potom už pokračoval dobre:   - Sláva ti Február, všetci kričať budú,  že si dal víťazstvo do rúk nášho ľudu...  Spolužiačka Mišela zasa - ...za lásku - lásku, vždy tak bude, Boh vždy miluje každého...-   Pôvodné znenie textu - ľud vždy miluje každého - zamenila evidentne pod dojmom prípravy na prvé sväté prijímanie.     - A čo Boh bol zakázaný?     - Boh bol najzakázanejší. Bol pre stranu príliš tvrdou konkurenciou. Zakázané boli aj zahraničné vysielania, zahraničné pesničky, zahraničné rifle. A keďže v Sovietskom zväze rifle nevyrábali, chodili všetci v tesilákoch z Makyty a v košeliach z česaného silonu a dederónu, čo bolo veľmi nepríjemné pre potiacu sa adolescentnú mládež. Toto oblečenie malo temer vlastnosti neoprénu a keď k tomu pridáme fakt, že antiperspiranty neexistovali, nie je ťažké si predstaviť ten odér.     - Aspoň ste sa viacej umývali...     - Áno, museli sme. Zamoriť napríklad Izbu revolučných tradícií by bolo neprístojné. Tam sme sa chodili starať o našu zbierku búst, aktualizovať nástenky, besedovať so súdruhmi partizánmi, intenzívne sa pripravovať na súťaž Pioniersky semafor alebo Čo vieš o ZSSR.     - A potom sa z vás stali komouši?     - Z nás už nie. Niečo sa im pokazilo. Neviem, kde súdruhovia urobili chybu, no mali to tak dobre zorganizované... Stalo sa, že Víťazný február vystriedal víťazný november. Niektorí súdruhovia však tento fakt vôbec nezaregistrovali a preto doposiaľ zotrvávajú na svojich miestach.     - A ktorí sú to?     - Keď ich budeš počuť, tak ich spoznáš.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (51)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Alipaškovia a ich majetky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Čo bude po SME
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Závažné podozrenie zo sexuálneho zneužívania dieťaťa odfláknuté
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Žasnem a neprestávam
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Natália Blahová 
                                        
                                            Trnava – mesto za euro alebo za zlámaný groš
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Natália Blahová
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Natália Blahová
            
         
        nataliablahova.blog.sme.sk (rss)
         
                        VIP
                             
     
          
 
 
Srdečne ďakujem za Vašu priazeň. Nájdete ma aj tu: nataliablahova.sk
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    363
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5102
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Rodina
                        
                     
                                     
                        
                            úvahy
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            pocitovky
                        
                     
                                     
                        
                            knižnica
                        
                     
                                     
                        
                            Komentáre
                        
                     
                                     
                        
                            Politika
                        
                     
                                     
                        
                            u nás
                        
                     
                                     
                        
                            foto
                        
                     
                                     
                        
                            nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vymierame a vlastná hlúposť nás dorazí
                                     
                                                                             
                                            nataliablahova.sk
                                     
                                                                             
                                            Rada Vás privítam medzi priateľmi
                                     
                                                                             
                                            301
                                     
                                                                             
                                            Pre mamičky, detičky a iné -ičky
                                     
                                                                             
                                            SaS plní sľuby a drží slovo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            nataliablahova.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            nataliablahova.sk
                                     
                                                                             
                                            Rada Vás privítam medzi priateľmi
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




