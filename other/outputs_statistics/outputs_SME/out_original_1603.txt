
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Steven Nagy
                                        &gt;
                Vtipné
                     
                 Klokan na cestách Slovenska 

        
            
                                    18.8.2009
            o
            4:23
                        |
            Karma článku:
                13.61
            |
            Prečítané 
            2715-krát
                    
         
     
         
             

                 
                    Posledne — pred pár mesiacmi som sem zavesil blog - ako sa chystám na maturitné stretko kdesi tam ďaleko v srdci Europy — na Slovensku. Práve som sa vrátil do Austrálie a je čas na moju reportáž  série  asi štyroch - piatich mojich zážitkov, ktoré ak mi dovolí čas, napíšem a sem zavesím. A prvá reportáž je o mojom príchode a prvých skúsenostiach...
                 

                 
  
   Brisbane – Dubaj – Viedeň….asi 24 hodín nad mrakmi, plus extra hodiny v tranzite… Po pristáti  vo Viedni  všetko  naopak ako u protinožcov a  ja som sa musel prispôsobit. A tam ma už čakal kamarát.  Dobre načasoval svoj príchod - On v momente ako vystúpil zo svojho auta, ja z letištnej haly. “Steven,  vítaj! Hybaj mám pre teba auto”. Keď ešte bol u mňa v  Austrálii navrhoval  aby som si kúpil "sekáča", ako to urobil on v Austrálii, lebo vraj prenájom za auto na Slovensku za dobu akú som tam plánoval pobudnúť, mi  odľahčí kapsu tak za dve spiatočné letenky do Austrálie ... a mal pravdu. Kúpil to iba hodinu pred mojím príletom!  Previezol ma do Bratislavy a tam mi predstavil môj  „tátoš“... skôr by som povedal oslík - Feliciu. Hneď som sa do neho takto nevyspatý natrepal ...čože vidím?  Nejaký figliar mi tam namontoval volant na špatnú stranu... No zobuď sa! Si v Európe...Tam na tej strane, kam si sa posadil u nás sedávajú  pasažieri! Aha...sorry!   OK, ok, mea culpa...preliezol som teda tam, kde bol ten volant a  sranda  sa začala. Za krátko som navykol na manuálnu prevodovku a iné odlišnosti ako páčku smerovky kde mal byť stierač a hlavne sa držať na pravej strane cesty, ako aj priateľovho VW Touranu  ako kliešť.  A veru  tých vyše 200 km k jeho domu ubehlo hravo.  S šťastím sme prefŕkli  cez  tri policajné kontroly. Len tak na margo: Auto nebolo poistené, nemalo TK,  pochybujem, že malo správnu výbavu, nebolo prepísané na moje meno...  ale jazdilo bez chyby... a chválabohu na tých troch  kontrolách,  policajti boli zaneprázdnení inými nešťastníkmi.  Nuž a po asi troch dňoch servisu a všetkých tých procesoch pod ktoré musia podstúpiť Slovenskí vodiči a ich autá - napočítal som ich asi 14   (a mnohé som kvalifikoval ako ...odpusťte mi,  ale verím, že ma mnohí z vás za môj koment virtuálne kopnú do "kýstera" :-)...  ako zbytočnú buzeráciu. Totiž u klokanov, v štáte kde žijem, stačí aby šofér mal  jeden jediný dokument – vodičák, žiadne doplnky ako krabica prvej pomoci, s nejakou  kartou návodu, oranžová vesta, non stop zapnuté svetlá atď. atď...). Nezdá sa mi, že takéto puntičkárske nariadenia ušetria životy.  Môj pohľad do tabuliek zabitých na cestách Slovenska, Austrálie  a zbytku sveta môj názor potvrdil. Nehodovosť je skôr závislá na mentalnej vyspelosti šoférov. Ale o tom druhýkrát...možno v diskusii ...som sa konečne po vybavení všetkých tých vecí dostal  za volant a vydal som sa sólo po cestách krajín  Vyšehradskej  štvorky.  Za tie dva mesiace a  6000 kilometroch,  mohol by som  niečo napísať o miestnych šoféroch. Nepredpokladám, že so mnou  všetci súhlasia, to celkom pochopím. Ale tí čo jazdili v Austrálii snáď áno. A takto som to videl  ja:  Na cestách  Slovenska, Česka, Maďarska a Poľska  som zažil hodne zaujímavých príbehov. Nikde inde však nie také ako na Slovensku.    Skoro okamžite  som  vycítil prvé nepísané cestné pravidlo: Čím väčšie, modernejšie, drahšie auto sa musí nad tým menším, starším, lacnejším skoro vždy   ukázať svoju nadradenosť. Slovenský šofér za jeho volantom perfektne ovláda pravidlá je suverén a vždy má pravdu.  My s malými lacnými a staršími autami sme plevel, ktorá im kazí vzduch a iba ucpáva cesty a prekáža im, aby sa dostali z bodu A do bodu B o pár sekúnd skôr.  A svoju suverenitu  niektorí prejavuju dosť  hm....puberťácky...  V Leviciach napríklad som sedel v záhradnej reštaurácii. V parkovisku stálo pekné  žlté Lamborghini.  Jeho majiteľ dojedol, zaplatil, nasadol, a mohol odisť ako každý iný. On však aby ukázal nám smrtelníkom, a zvýšil efekt akú má dobrú, peknú a drahú hračku, šliapol na plyn, motor zareval a pneumatiky s piskotom brúsili asfalt ako sa vyrútil... Škoda. V mojich očiach sa zhodil.               Pre nich je najväčšia provokácia, keď  to malé, staré a lacné auto ich super tátoše nedajbože predbehne!!!  To je obrovská urážka. Trochu mi pripopmínali  býka v korride:  Ani býka nezaujíma toreador, toho si moc nevšíma, ten je druhotný.  Útočí na červenú handru toreadora. Presne  ako ten macher. On nevidí  za volantom človeka, spoluobčana, kamaráta, uživateľa ciest s rovnakými právami. On vidí tú červenú handru - to malé, staré lacné auto, ktoré ho dráždi.    Moja Felicia na prekvapenie bola celkom živá a občas som ju musel krotiť aj na diaľnici, keď dosahovala neprípustnú rýchlosť -  140 Km za hodinu. No ale inak nechal som sa kľudne predbiehať,  veľkými, drahými a modernými autami....robilo im to radosť. Občas som bol varovaný diaľkovými svetlami alebo klaksónom. (V Austrálii nie sú vodiči anjeli, ale tam klaksón malokedy počuť. Osobne ja sa nepamätám kedy ma tam niekto varoval...roky a roky!)  Veľmi skoro som narazil na svojho "býka".  Bolo to niečo veľké čierne nachrómované. A ja som sa ho odvážil predbehnúť s tou mojou starenkou.  A nastal súboj, ktorý sem tam každý z nás určite zažil:    Na diaľnici som mal pred sebou kamión, ktorý šiel tou správnou rýchlosťou ako to kamióny majú dovolené.  Nemajúc nikoho v ľavom prúde, som vyhodil smerovku a  začal som ho predbiehať. Felicia nie je Ferrari a predbehovanie mu  trvá dlhšie. Ako blesk z modrého neba sa objavil   ten môj býk.  Priblížil sa tak asi meter  k môjmu výfuku a  svietil a trúbil napriek tomu, že som šiel skoro maximálnou dovolenou rýchlosťou.            Samozrejme, trpezlivosť...  hneď to bude kamarát.... skromne som sa vtiahol do pravého prúdu a pokračoval.  Ako  ma predbehoval, vrhol na mňa zlostný pohľad  a zaradil sa tiež do pravého prúdu  a aj spomalil. Bol ukojený, ukázal mi kto je boss.  Ale akosi sa potom neponáhľal  a držal som sa za ním asi desať minút.  Krátko ako som zišiel z diaľnice,  som roztúroval svoje staré autíčko a než  sa býk prebral, bol predbehnutý!    Ty brďo! Tak to teda nie! To som nemal! Asi aj on mal tú istú trasu ako ja, alebo chcel mi dať lekciu.. nikdy sa nedozviem...zabočil za mnou.  Cesta bola cez akési lesy a kopce a poriadne kľukatá.  Hravo sa dostal  na  môj  „chvost“  a skoro ma začal tlačiť.  Netrúbil ani nesvietil, to nemalo ceny.  Ale  predbehnúť  ma tiež nemohol, lebo zákruty boli neprehľadné a  oproti sa valili autá....Jeho frustrácia bola až hmatateľná!  Priblížil sa na centimetre a veru nebolo to príjemné....A vtedy, viacmenej z obáv že ma môže na tej osamelej ceste vytlačiť  do priekopy a potom zmiznúť, začal som pridávať.  Felicia po tých cestách  tentoraz mala istú  výhodu.  Ľahké čiperné auto krásne bralo zátačky kým on asi mal bod gravitácie vyššie,   musel spomaliť  a  tým pádom stratiť trochu  priestoru medzi nami a veru začal som mu aj unikať.  Ale on  sa nedal. Vždy na rovinke ma dobehol a tlačil...Možno niekto sa pamätá na starší Spielbergov film  Duel...bolo to podobné.  No a vtedy som sa rozhodol riskovať.  V ďalších zákrutách som sa poriadne rozbehol a začal ho strácať z dohľadu, až mi úplne zmizol z spätného zrkadla. A ako sa tak rútim, ma napadlo: čo blbneš?  Zabiješ sa k vôli nejakému idiotovi. Cool it!  V najbližšej zákrute som si všimol dopravnú značku  biele “P” - čko v modrom poli - parkovisko. Bolo za porastom a neviditeľné z cesty. Spomalil som a zabočil tam, vypol motor aj svetlá, otvoril dvere a blažene čakal :-)   Za menej ako dvadsať sekúnd  počujem rev motora ...    Bol to náš El torro!   Tak dobre som bol schovaný že ani netušil, že som za porastom. Preletel ako drak a zmizol za zákrutou...  Po krátkom oddychu som naštartoval a kľudne  pokračoval v ceste.   A El Torro?  Už som ho potom viac nevidel...a dokonca  si myslím, že ešte stále hľadá to drzé malé lacné staré auto.... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Tajomný Milionár
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Slovenské predavačky - úprimný nezáujem
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Skutočná Austrálska "Love Story"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Hanbite sa pán M.!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Steven Nagy 
                                        
                                            Lietajúce sosáky
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Steven Nagy
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Steven Nagy
            
         
        stevennagy.blog.sme.sk (rss)
         
                        VIP
                             
     
        Don't cry for me Argentina.. I'm on my way to see you soon.

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    115
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3789
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Poznávacie
                        
                     
                                     
                        
                            Vtipné
                        
                     
                                     
                        
                            Smutno-vážne
                        
                     
                                     
                        
                            Politické
                        
                     
                                     
                        
                            Informačné
                        
                     
                                     
                        
                            Existencionalistické
                        
                     
                                     
                        
                            Osobné
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                             
                                     
                                                                             
                                            Fastball
                                     
                                                                             
                                            James Blunt
                                     
                                                                             
                                            Jason Mraz
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ivka Vrablanska
                                     
                                                                             
                                            Ján Urda
                                     
                                                                             
                                            Ľudmila Schwandtnerová,
                                     
                                                                             
                                            Martin Marušic
                                     
                                                                             
                                            Jozef Klucho
                                     
                                                                             
                                            Jozef Javurek
                                     
                                                                             
                                            Boris Burger
                                     
                                                                             
                                            Ján Babarík
                                     
                                                                             
                                            Natália Bláhova
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Počítače
                                     
                                                                             
                                            Zo zahraničia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Deň na ceste okolo sveta na Novom Zélande
                     
                                                         
                       Oracle Slovensko systémovo napomáha korupcii a miliónovému tunelu
                     
                                                         
                       Vodičov pokutujeme aj za drobnosti, ale ostatných nie
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




