
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Slušný
                                        &gt;
                Nezaradené
                     
                 Hrešíte alebo nadávate? Tak aspoň buďte originálny! 

        
            
                                    17.4.2009
            o
            8:12
                        |
            Karma článku:
                12.62
            |
            Prečítané 
            6163-krát
                    
         
     
         
             

                 
                    Minule mi napadlo, že sa pokúsim zistiť, ako sme my Slováci na začiatku 21. storočia na tom s nadávaním v našom írečitom materinskom jazyku. Využil som momentálne azda najpopulárnejší internetový portál sveta – sociálnu sieť zvanú Facebook a vyhlásil som na nej pre svojich približne 600 priateľov súťaž o najoriginálnejšiu slovenskú nadávku. Chcete vedieť, ako to dopadlo?
                 

                 Možno poznáte ten vtip, ako ku Niagarským vodopádom prídu Nemci a povedia: „Das is schone!“, po nich Angličania: „This is beautiful!“ Rusi: „Eto prekrásnoje“ a nakoniec Slováci: „Ko...ot, ja to j...m, do pi....e...“   Nuž asi je to trochu prehnané, veď Slováci predsa sú jedným z najkultúrnejších národov žijúcich na Slovensku, ale faktom je, že niektorí jeho príslušníci si naozaj akosi zvykli používať vulgarizmy aj namiesto vyjadrenia obdivu. A tým pádom sa niektoré škaredé slová dostali do hovorovej reči a stali sa jej súčasťou natoľko, že pomaly už aj prestávajú byť nadávkami. Poznám niekoľko ľudí, ktorí oslovenie: „Ty k...t“ používajú asi rovnako často a intenzívne ako susedia spoza rieky Moravy slovné spojenie: „Ty vole“. Pri vyhlásení mojej súťaže som vôbec netušil s akou veľkou odozvou sa stretne. A veľkým pozitívom bolo, že moji známi mi poslali doslova až stovky návrhov originálnych slovenských nadávok, ktoré som si dovolil pre väčší prehľad rozdeliť do nasledujúcich kategórii:   Ľúbozvučné slovenské nadávky:   Ogrgeľ, Papľuh, Chrchceľ vycicmaný, Zakysaná geleta, Čremocha, Chrapúň, Odkundes, Otrapľa, Pačmaga, Mamľas, Bimbas, Chumaj, Truľo, Grgôň, Pimpeľ, Trtko, Ťuťko, Pišišvor, Mrcina, Sajdák, Otrtúľ, Ciciak,  Chrapač, Budzogáň, Lapikurkár, Degeš, Dilino, Mršina, Paskuda, Skuhroš, Rarášok, Ťuťmák, Korheľ, Chruňo, Trubka, Čúza, Galgan, Cúra, Cundra, Gauner, Loptoš,  Ozembuch, Hovädo, Vôl, Somár, Lotor, Bzdocha, Dindaj, Kačura, Grco, Prdomil, Nikhaj, Lazňa,  Sraľo, Odroň, Truhlík, Trubiroh, Buran, Buroň, Rupoň, Nemehlo, Posran, Zasran, Motovidlo, Táraj,  Jelito, Pňak, Chriašteľ, Nimrod, Teľa, Krava, Koza, Suka, Chriašteľ, Dilino, Sedlák, Špina, Odroň, Svoloč, Cibazol, Trpák, Krepáň, Buľo, Vajce, Hovno, Sviňa, Filcka,   Nadávky inšpirované inými jazykmi:   Fasinger, Spuchra, Ohyzda, Subra, Sršo, Moribundus, Selhámoš, Lúzer, Pokémon, Ludra, Smrha, Mantinel, Kváder, Megera, Megero, Mešuge, Lafa,   Cudzie slová vyjadrujúce telesné alebo duševné postihnutie:   Debil, Idiot, Imbecil, Impotent, Blázon, Schizofrenik, Šalený.   Nadávky znevažujúce výzor:   Dengľavé tintítko, Škaredica škaredá, Kikimora, Oštinoha, Pľuha pandravská, Skaderukskadenoha, Sralbotka, Oščinoha jedna oščaná smrallavá, Do frasovej kaliky.   Vulgarizmy a ich modifikácie:   Vriťpich, Pičús, Pičiriak, Cicina, Frčina, Chuj, Riťopich, Hnidopich, Kôkôtiäk, Kurva, Piča, Kokot, Jebo, Jeblina, Idiotkokot, Pičorus, Odjebok, Vyjebanec, Hajzeľ, Čurák.   Asi najhoršie nadávky, aké existujú:   Ty poslanec, Ty Paviel, Ty Fico...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (65)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Slušný 
                                        
                                            Prišiel ŠK Slovan Bratislava o svoju futbalovú slávu, krásu a česť? Určite nie!!!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Slušný 
                                        
                                            Miro Šatan je pre úspech Slovenska na zimnej olympiáde dôležitý...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Slušný 
                                        
                                            Kotleba a Dobrovodský - sú to naozaj rasisti a extrémisti?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Slušný 
                                        
                                            Hľadáte skvelú kapelu na vašu akciu či párty?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Roman Slušný 
                                        
                                            Lolla Fukari dostala justíciu na kolená!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Slušný
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Slušný
            
         
        slusny.blog.sme.sk (rss)
         
                                     
     
        Píšem, lebo ma to baví. Som autorom knihy Karol Duchoň - Muž s veľkým srdcom a publikácií Nezostalo po nich ticho I a II. Pracoval som ako novinár, momentálne sa venujem podpore slovenskej vážnej a populárnej hudby a výtvarného umenia.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    85
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3990
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Tomáš Holetz
                                     
                                                                             
                                            Tina Turnerová
                                     
                                                                             
                                            Silvia Turnerová
                                     
                                                                             
                                            Boris Burger
                                     
                                                                             
                                            Tamara Heribanová
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kotleba a Dobrovodský - sú to naozaj rasisti a extrémisti?
                     
                                                         
                       Markíza úplne nezmyselne bojkotuje nový slovenský film!
                     
                                                         
                       List nahnevaného futbalového fanúšika do TV Dajto
                     
                                                         
                       Karol Duchoň porazil Gotta i všetky ostatné hviezdy čs. pop-music!
                     
                                                         
                       Nová väčšina sa zmôže len na Krajcera?
                     
                                                         
                       Vážený pán Golonka...
                     
                                                         
                       Ako dvaja mladíci vybabrali s Chrisom Normanom v Senici...
                     
                                                         
                       Pomáhať chorým deťom treba aj bez Modrého z neba!
                     
                                                         
                       Futbalisti Slovenska - hanbite sa!
                     
                                                         
                       Najskôr vás okradnú v električke a potom ešte aj v banke!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




