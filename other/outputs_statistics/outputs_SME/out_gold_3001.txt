

  &lt;!--  /* Style Definitions */  p.MsoNormal, li.MsoNormal, div.MsoNormal 	{mso-style-parent:""; 	margin:0cm; 	margin-bottom:.0001pt; 	mso-pagination:widow-orphan; 	font-size:12.0pt; 	font-family:"Times New Roman"; 	mso-fareast-font-family:"Times New Roman";} @page Section1 	{size:595.3pt 841.9pt; 	margin:70.85pt 70.85pt 70.85pt 70.85pt; 	mso-header-margin:35.4pt; 	mso-footer-margin:35.4pt; 	mso-paper-source:0;} div.Section1 	{page:Section1;} --&gt; 
 
 Prehrabávate sa stále dokola v mapách Slovenska, ktoré, samozrejme, vyzerajú úplne rovnako, až na to, že v jednej sú zakreslené rieky a v druhej stupne lesov. Ale stále nemôžete pochopiť, že okolo vás už neležia roztrhané kúsky vianočného papiera a návody na použitie. Na sebe máte nové spodné prádlo a nemôžete sa zmieriť s tým že to, čo ste mali včera sa už pokladá za staré. Môže vlastne ponožka zostarnúť? A teraz, za malú chvíľu, už počujete: „ Prečo máš tie staré ponožky? Veď si dostal nové. Daj si tie.“ Vyberiete z medovníka nerozomletý kúsok, pravdepodobne orecha, a zistíte, že je už tvrdý. Medovníky nevydržia veľa na vzduchu, ale keď ich necháte v mrazničke, vydržia aj do ďalších Vianoc. Nesneží, aby ste sa nemohli isť sánkovať a tiež preto, aby ste neboli šťastní. Nemrzne, aby nesnežilo. Nikto nie je na internete, aby ste mali pocit, že ste niečo viac ako povianočný závislák. Nobelovu cenu za literatúru získali ľudia, o ktorých väčšina doteraz nepočula, ale to nevadí, lebo písali na múdre témy, písali hlavne o vojne. Preto o niečom takom snívate. O cene, na ktorú musíte niečo prežiť. Niečo svetové, niečo prevratné. 
   
 A potom... 
   
 Snívate, aby sa zopakovali Vianoce. Aby povinnosti znova nadobudli sviatočný charakter. 

