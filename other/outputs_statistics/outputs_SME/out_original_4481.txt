
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Buno
                                        &gt;
                Nezaradené
                     
                 Paulo Coelho ako na dlani 

        
            
                                    13.4.2010
            o
            14:30
                        |
            Karma článku:
                6.25
            |
            Prečítané 
            1862-krát
                    
         
     
         
             

                 
                    Paulo Coelho dostal niekoľko ponúk na napísanie svojho životopisu, ale povolil to iba brazílskemu novinárovi Fernandovi Moraisovi. Ten mal však jednu podmienku – Coelho si nebude môcť prečítať rukopis. Obsah knihy sa dozvie až po jej vydaní ako každý iný čitateľ. Dosť trúfalé od Moraisa...a značne riskantné od Coelha, keď súhlasil. Tak vznikla unikátna kniha Mág. Životopis Paula Coelha.
                 

                 Coelho totiž vo chvíli, keď mu Morais predložil danú podmienku, pozeral na akúsi kaplnku. A na nej bolo napísané: „Spoznajte pravdu, pravda vás vyslobodí.“ Práve preto Coelho súhlasil, aby vznikol jeho životopis. Aby odhalil druhú tvár seba samého. A to by mu dalo pocit väčšej slobody...  Kniha Mág Fernanda Moraisa si neberie pred ústa servítku. Bez predsudkov a bez príkras popisuje skúsenosti, ktoré budúceho spisovateľa formovali – vzťahy s rodičmi, pobyty v psychiatrických liečebniach, rozmanité sexuálne zážitky, inklinácia k satanizmu, stretnutie so smrťou. Bez problémov opisuje ako ho rodičia dali tri razy internovať v psychiatrickej liečebni, kde dostával elektrošoky. Ako dostáva záchvaty zlosti, keď niekam príde a nečakajú ho skupiny paparazzov, reportérov, filmárov a zástupy fanúšikov. Už prvé riadky sú veľavravné – návšteva Coelha v Budapešti v roku 2005.   Na druhej strane však Morais vykresľuje Coelha ako človeka, ktorý  napriek miliónom žije v podstate asketický život. Nerád míňa peniaze. Prvé slušné auto mal až vtedy, keď mu ho podarovala istá automobilka. Alebo je to predsa len lakomosť? Možno to pramení z detstva, ktoré nebolo najšťastnejšie... Veď vlastná matka odrádzala Coelha stať sa spisovateľom so slovami: „Syn môj, je len jeden Jorge Amado.“ Na vysvetlenie – Amado je jeden z najlepších a najznámejších  moderných brazílskych spisovateľov. Jeho dielo bolo preložené do takmer päťdesiatich jazykov.   Tridsať kapitol je doslova nabitých vzrušujúcim, pohnutým, dramatickým ale aj veľmi dobrodružným životom spisovateľa.  Za prácou Moraisa je vidno kus poctivo odvedenej lopoty. Musel sa prelúskať 170. zošitmi-denníkmi, ktoré mu Coelho poskytol a využil pri tom svoj novinársky, investigatívny pud. Milovníci životopisov a zvlášť Paula Coelha majú naozaj jedinečnú príležitosť, ako hlbšie pochopiť jeho dielo.   A možno si na záver uznanlivo povzdychnete, že je doslova zázrak, ako dokázal človek s takýmto osudom nielenže prežiť, ale stať sa jedným z najúspešnejších spisovateľov sveta.   Prečítajte si ukážky z knihy „Mág. Životopis Paula Coelha.“     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (75)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Ako šli Sovieti kradnúť nápady do Ameriky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Marián Gáborík. Inšpirujúci príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Buno
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Buno
            
         
        buno.blog.sme.sk (rss)
         
                        VIP
                             
     
         Robím to, čo milujem...po dlhých rokoch v médiách som sa vrhol na knihy a vo vydavateľstve Ikar mám všetky možnosti a príležitosti, ako si ich vychutnať naplno. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    297
                
                
                    Celková karma
                    
                                                7.34
                    
                
                
                    Priemerná čítanosť
                    2449
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nesbov SYN sa blíži. Pozrite si obálku a čítajte 1.kapitolu
                                     
                                                                             
                                            Zažite polárnu noc (nielen) s Jo Nesbom!
                                     
                                                                             
                                            Jony Ive - Geniálny dizajnér z Apple
                                     
                                                                             
                                            Konečne prichádza Láska na vlásku so Celeste Buckingham!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Tretí hlas
                                     
                                                                             
                                            Mandľovník
                                     
                                                                             
                                            Dôvod dýchať
                                     
                                                                             
                                            Skôr než zaspím
                                     
                                                                             
                                            Tanec s nepriateľom
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            M.Oldfield: The Songs of Distant Earth
                                     
                                                                             
                                            Ludovico Einaudi
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Severské krimi
                                     
                                                                             
                                            Knižný svet na Facebooku
                                     
                                                                             
                                            Titulky.com
                                     
                                                                             
                                            Jozef Banáš
                                     
                                                                             
                                            Táňa Keleová-Vasilková
                                     
                                                                             
                                            BUXcafé.sk všeličo o knihách
                                     
                                                                             
                                            Noxi.sk
                                     
                                                                             
                                            Tyinternety.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Šup, šup, najesť sa. A utekať!
                     
                                                         
                       Ako šli Sovieti kradnúť nápady do Ameriky
                     
                                                         
                       Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                     
                                                         
                       Marián Gáborík. Inšpirujúci príbeh
                     
                                                         
                       Knižný konšpiračný koktail alebo čože to matky na severe miešajú deťom do mlieka?
                     
                                                         
                       Paulo Coelho
                     
                                                         
                       Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                     
                                                         
                       Obdobia, z ktorých by deti nemali vyrásť
                     
                                                         
                       Tánina pekáreň
                     
                                                         
                       Malala - dievča, ktoré rozhnevalo Taliban
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




