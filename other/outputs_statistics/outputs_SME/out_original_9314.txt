
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michal Lobodáš
                                        &gt;
                Hokej
                     
                 Zúfalý ťah hrdých Habs 

        
            
                                    21.6.2010
            o
            11:59
                        (upravené
                21.6.2010
                o
                12:10)
                        |
            Karma článku:
                6.96
            |
            Prečítané 
            1949-krát
                    
         
     
         
             

                 
                    Mnohých fanúšikov slovenského hokeja šokovala informácia o výmene Jaroslava Haláka do St. Louis. Táto informácia je hlavne po skvelých Halákových výkonoch v druhej polovici uplynulej sezóny a v play-off nanajvýš prekvapujúca.
                 

                 
Halák a Montreal. Toto spojenie fungovalo na výbornú... 
   Najväčším prekvapením je však protihodnota, ktorú manažment Canadiens za Jara získal. Talentovaný dánsky útočník Lars Eller a junior Ian Schultz skutočne nie sú maximom, ktoré mohol Montreal za Haláka vyťažiť. V kuloároch sa napríklad šírili informácie, že nový GM Tampy Bay Steve Yzerman je ochotný za Halákovho konkurenta Careyho Price ponúknuť jednu z hviezd ligy - Martina St. Louisa.   Vedenie Habs, konkrétne generálny manažér Pierre Gauthier sa však zachoval tak, ako nečakal asi nikto. Halák Pricea v minulej sezóne doslova prevalcoval, bol úspešnejší vo všetkých ukazovateľoch, štatisticky patril k špičke v NHL, pretlačil Habs nielen do play-off, ale v ňom následne cez najlepšie ofenzívy v lige - Washington a Pittsburgh. Okrem toho sa stal miláčikom fanúšikov.  V montrealskom Gazzette ho dokonca označili ako Mr. Hero, kým jeho konkurent v bránkovisku si vyslúžil titul Mr. Zero.   V Montreale však potvrdili, že stále čakajú na následníka legendárneho Patricka Roya. V bránke Canadiens jednoducho musí byť Kanaďan, najlepšie frankofónneho pôvodu. Po neúspechu s Jose Theodorom sa zjavil mladučký Carey Price a okamžite oživil spomienka na bývalú legendu. Postupne však jeho výkony upadali, a len zaslepení priaznivci v ňom stále vidia Royovho nástupcu. Bohužiaľ je medzi nimi i GM Pierre Gauthier. Priceovi sa tak opäť otvára možnosť, ktorú si nezaslúžil.   Halákova výmena vzbudila medzi fanúšikmi búrku nevôle a na manažment Habs sa spustila spŕška kritiky. Je pravda, že Halákov štatút obmedzeného voľného hráča mu dával šancu na arbitráž, ktorej sa chcelo vedenie vyhnúť, ale tí, čo poznajú Halákove ľudské kvality vedia, že pre neho nie sú prvoradé peniaze a v Montreale by bol ochotný zostať i za relatívne horších podmienok, aké by mohol dostať na trhu.   Jeho nové pôsobisko však ešte zďaleka nie je definitívne, pretože so St. Louis sa musí najskôr dohodnúť na kontrakte. Manažment Blues s ním ráta na miesto Chrisa Masona, bývalého konkurenta Jána Lašáka v Nashville. St. Louis sa síce v poslednej sezóne nedostalo do play-off, ale vekové zloženie mužstva napovedá, že bluesmani budujú mladý, rýchly a vývojaschopný tým, ktorý by mohol už v nasledujúcej sezóne okúsiť atmosféru play-off.  Istota v bránkovisku menom Jaroslav Halák by im k tomu mala výrazne dopomôcť. Slovenský brankár bude aj v novom pôsobisku určite čeliť veľkému počtu striel a šancí súpera a práve v takýchto situáciách sa dokáže vytiahnuť. Mužstvo okolo T.J. Oshieho, Backesa, Steena, Perrona, Erika Johnsona, Berglunda, Pietrangela či Romana Poláka má v sebe obrovský potenciál. Bude Jaroslav Halák jeho najdôležitejším hráčom?     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Nominačné okienko č. 1: Brankári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Lev zareval na odchod
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Najlepšie vývozne artikle extraligy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Tímy NHL podľa draftu: Anaheim Ducks
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michal Lobodáš 
                                        
                                            Vlna, ktorú sme nezachytili
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michal Lobodáš
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michal Lobodáš
            
         
        lobodas.blog.sme.sk (rss)
         
                                     
     
        Športový fanatik, ktorého zaujíma všetko, čo sa týka futbalu a hokeja, ale miluje aj kvalitnú hudbu a filmy.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    32
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2618
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Hudba
                        
                     
                                     
                        
                            Filmy
                        
                     
                                     
                        
                            Hokej
                        
                     
                                     
                        
                            Futbal
                        
                     
                                     
                        
                            Ostatné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nominačné okienko č. 1: Brankári
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




