
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Doprava
                     
                 Analýza nehodovosti v cestnej doprave za rok 2009 

        
            
                                    8.2.2010
            o
            0:01
                        (upravené
                8.2.2010
                o
                15:14)
                        |
            Karma článku:
                11.38
            |
            Prečítané 
            6111-krát
                    
         
     
         
             

                 
                    Minister vnútra Robert Kaliňák a viceprezident Policajného zboru Stanislav Jankovič informovali na tlačovej besede o mimoriadne priaznivom výsledku vývoja dopravnej nehodovosti za rok 2009. Pozrime sa aké faktory prispeli k tomuto priaznivému výsledku a či je udržateľný aj tento rok.
                 

                 Najprv čo hovoria základné údaje zo štatistiky za rok 2009 v porovnaní s predchádzajúcim rokom.     
    2008 2009 rozdiel % rozdiel   Počet dopravných nehôd 59 008 25 975 -33 033 -55,98   Počet zranených 10 040 8 527 -1 513 -15,07   Počet usmrtených 558 347 -211 -37,81   Tabuľka 1. Štatistika za Slovensko spolu.    
    2008 2009 rozdiel % rozdiel   Bratislava 51 27 -24 -47,06   Trnava 72 45 -27 -37,50   Trenčin 64 28 -36 -56,25   Nitra 74 40 -34 -45,95   Žilina 63 55 -8 -12,70   B.Bystrica 85 66 -19 -22,35   Prešov 73 37 -36 -49,32   Košice 76 49 -27 -35,53   Spolu 558 347 -211 -37,81   Tabuľka 2.  Počet usmrtených pri dopravných nehodách po jednotlivých krajoch.    Počet dopravných nehôd medziročne poklesol o 55,98 percenta. Lenže poisťovne tvrdia, že voči minulému roku sa počet poistných udalostí nezmenil. Ako je to možné?    V roku 2002 bolo na Slovensku registrovaných 1,700 milióna vozidiel, v roku 2008 2,155 milióna a v roku 2009 2,236 milióna, čo prestavuje medziročný nárast takmer o 80 tisíc vozidiel.  V roku 2009 bolo z evidencie osobných vyradených  101 308 tisíc vozidiel trvale a 7054 dočasne, pričom priamo z titulu šrotovného bolo vyradených 44 200 najmä starších vozidiel. Novo prihlásených osobných vozidiel bolo 148 370 čo predstavuje významnú obnovu vozového parku. V sumáre ide o zvýšenie počtu vozidiel, čo by sa malo  v poisťovniach pri tej istej nehodovosti štatisticky prejaviť ako nárast poistných udalosti, ale nestalo sa tak.    Rozdiel v počte nehôd je spôsobený zmenou metodiky, čo sa považuje za dopravnú nehodu. Pokiaľ nedošlo k zraneniu  alebo väčšej škode, tak ide len o škodovú udalosť, ktorú síce poisťovňa rieši ale v štatistike dopravných nehôd sa neobjaví. Preto pokles počtu dopravných nehôd nie je možné objektívne vyhodnotiť i keď na zvyšujúci sa počet vozidiel jednoznačne poklesol.    Počet zranených a usmrtených tiež výrazne poklesol a tu sa metodika počítania nemenila. Preto tieto údaje je možné objektívne považovať  za veľmi priaznivý výsledok.        Čo sa v roku  2009 voči roku 2008 zmenilo?   Najzákladnejšou zmenou bol nový zákon o cestnej premávke 8/2009 Z. z., ktorý priniesol viacero zmien. Z nich nasledujúce zmeny boli podľa mňa najväčším prínosom k plynulosti a bezpečnosti dopravy:    1, Zníženie rýchlosti v obci zo 60 na 50 km/hod čo skrátilo brzdnú dráhu vozidla a v prípade stretu vozidla s chodcom prinieslo nižšie deštruktívne následky.    2, Celodenné svietenie, následkom čoho bolo prehliadnutých menej vozidiel.   3, Zvýšenie rýchlosti pre nákladné vozidla mimo obce z 80 na 90 km/hod a zvýšenie rýchlosti na diaľnici obci z 80 na 90 km/hod. Táto zmena zvýšila plynulosť dopravy, lebo všetky vozidlá mimo obce a na diaľnici v obci majú rovnakú maximálnu povolenú rýchlosť, čo znížilo počet predbiehaní z titulu maximálnej dovolenej rýchlosti, čím sa znížil aj počet potenciálnych rizikových situácii a tým aj počet dopravných nehôd a ich následkov.  
  4, Povinné používanie reflexných prvkov u chodcov mimo obce a cyklistov vždy, čo znížilo počet nehôd z titulu prehliadnutia chodca v tmavom oblečení.  
  Lenže ani dokonalejší zákon nemohol priniesť až tak významnú  zmenu, lebo dopravné nehody vznikajú najmä z titulu jeho porušovania, či už nechceného alebo zámerného.  
  Ministerstvo vnútra, ktoré je garantom nového zákona o cestnej premávke spravilo ešte dve rozhodujúce veci a ním bola úprava zákona o priestupkoch a veľmi výrazné zvýšenie vymožiteľnosti zákona.  
  Zmenila sa kategorizácie priestupkov na závažné a nezávažné. Do závažných sa dostali tie, ktoré majú hlavnú a priamu príčinu na vzniku dopravných nehôd a ich následkov. Ako a jazda pod vplyvom alkoholu, útek z miesta dopravnej nehody, neprimeraná rýchlosť jazdy. U týchto priestupkov mnohonásobne stúpla výška pokuty. Ale ani to nestačí na zmenu správania vodičov.    Na zmenu správania vodičov zapôsobili intenzívne kontroly na cestách. Zvýšil sa ich počet ale aj pravidelnosť. K tomuto účelu Ministerstvo vnútra prezieravo využilo ušetrených policajtov cudzineckej polície, ktorých presunulo do dopravnej polície a zabezpečilo pre nich aj technické vybavenie.    Všetky doteraz uvedené zmeny spolu majú najväčší prínos v priaznivej štatistike v znížení počtu zranených  usmrtených v cestnej doprave.   K priaznivému výsledku prispeli aj niektoré sekundárne vplyvy.  Už počas prípravy a schvaľovania zákona o cestnej premávke, informovali média o zmenách a tak aj vodiči, ktorí získali vodičské oprávnenie veľmi dávno sa dozvedeli o zmenách. K tomu prispela aj čiastočná obnova vozového parku za bezpečnejšie vozidlá.  Z titulu hospodárskej krízy bol o niečo znížený počet kamiónov na cestách a bolo pomerne priaznivé počasie počas celého roka.        Zákon 8/2009 Z.z. priniesol aj negatíva.   Boli síce prijaté v najlepšom úmysle, ale v konečnom dôsledku zvyšujúce počet usmrtených chodcov.   Najväčšie negatívum vidím v povinnom používaní prilieb pre cyklistov mimo obce. Samo o sebe je to dobrý úmysel, ale na základe tejto povinnosti, obyvatelia viacerých obcí požiadali starostov o posunutie tabúl označujúci začiatok a koniec obce, až za záhrady a polia na konci obce, aby nemuseli na bicykli nosiť prilbu. V niektorých obciach zašli až tak ďaleko, že značky umiestnili podľa katastrálnych hraníc a tak z obce do obce je stále územie v obci, v ktorom netreba používate prilbu, hoci sa nachádza aj niekoľko kilometrov v neobývanej oblasti.   Lenže posúvanie začiatkov a koncov obcí má dva veľmi negatívne dopady. Prvý dopad je na viditeľnosť chodcov, ktorí v obci nemusia mať na sebe reflexný prvok a z titulu posunutia začiatku obce ho nemusia mať ani na neosvetlenom území mimo obce. Práve z dôvodu tmavého oblečenia bez reflexných prvkov, mimo verejného osvetlenia, najčastejšie pri chôdzi po vozovke z titulu, že medzi obcami nie sú chodníky, bolo vodičmi prehliadnutých a usmrtených viacero chodcov. Povodne dobrý úmysel sa takto stal životu ohrozujúcou záležitosťou. Preto osobne navrhujem  aby povinnosť prilieb pre dospelých cyklistov zo zákona sa nahradila iba odporúčaním   Druhý dopad posunutia začiatkov a koncov obcí stavia dopravnú políciu do veľmi negatívneho svetla, lebo vodiči jej konanie pokladajú za šikanovanie a oprávnene.    Posunutím značiek sa komunikácia, ktorá má všetky znaky komunikácie mimo obce stáva po právnej stránke komunikáciou v obci. Keď vodič ide po nej na okolnosti primeranou rýchlosťou 90 km/hod a polícia vyhodnotí prekročenie rýchlosti voči limitu v obci, tak zvyčajne za plus 30km/hod dostane pokutu na úrovni mesačného zárobku a hrozí mu aj strata vodičského oprávnenia. Preto nečudo, že väčšina vodičov považuje takéto konanie dopravnej polície za šikanovanie čo, znižuje v konečnom dôsledku kredit celej polície.   Vôbec by sa nemalo stávať, že značka začiatok a koniec obce je umiestnená inde ako na začiatku a konci fyzicky zastavaného územia. Preto polícii odporúčam posudzovať prekročenie rýchlosti vo fyzicky nezastavanom území ako prekročenie rýchlosti mimo obce vrátane limitu a nie ako v obci.   Navyše odporúčam vykonať plošnú revíziu týchto značiek, lebo napríklad z titulu kontroly začiatkov a koncov obci pre elektronický výber mýta, som zistil, že skutočné umiestnenia sa nezhodujú s oficiálnym záznamom v Cestnej databanke, čo znamená, že boli posunuté bez riadneho schvaľovania a teda svojvoľne.    Na druhej strane je potrebné zmeniť zákon tak, aby chodci mali povinnosť používať reflexné prvky vždy za súmraku a tmy mimo súvislého verejného osvetlenia ak sa pohybujú po vozovke, vrátane krajnice, mimo vyznačeného priechodu pre chodcov, bez ohľadu na to či sú alebo nie sú v obci.        Ako dopadlo Slovensko z titulu európskeho záväzku?   Slovensko sa zaviazalo do roku 2010 znížiť počet usmrtených pri dopravných nehodách aspoň o polovicu. K dosiahnutia cieľa 305 má Slovensko ešte jeden rozhodujúci rok. Za rok 2009 zomrelo na našich cestách 347 osôb čo je o 263 osôb menej než v referenčnom roku 2002, keď ich bolo 610. Znamená to, že k cieľu sme sa priblížili na dosah, presnejšie na 86,2 percenta a chýba nám 13,8 percent, čo predstavuje 42 z 305 zachránených ľudských životov.     
  Rok Počet usmrtených Maximum na dosiahnutie cieľa   1991 614     1992 667     1993 584     1994 633     1995 660     1996 616     1997 788     1998 819     1999 647     2000 628     2001 614     2002 610  Základ pre výpočet 1/2   2003 645     2004 603     2005 560     2006 579     2007 627 627   2008 558 519   2009 347 412   2010 ? 305   Tabuľka 3. Počet usmrtených pri dporavných nehodách. Údaj maximum usmrtených pri dopravných nehodách začína od roku 2007 a nie od roku 2002, preto lebo intenzívne sa na tomto cieli pracovalo až az v roku 2007 a to prípravou nového zákona o cestnej premávke.     Tabuľka 4, Grafické znázornenie vývoja počtu usmrtených pri dopravných nehodách a zobrazenie maximálneho počtu usmrtených na dosiahnutie cieľa.       Čo je potrebné spraviť aby aj budúci rok bol úspešný?   1, Treba v pokračovať v intenzívnych kontrolách vodičov s tým, že sa treba zameriavať na hlavé príčiny tragických nehôd, ako je,  neprimeraná rýchlosť, nedostatočných odstup od predchádzajúceho vozidla,  alkohol za volantom a ťažko dokázateľné, ale z hľadiska nehôd veľmi významné nedostatočné sa venovanie vedeniu vozidla.   2,  Keďže, na cestách zomrelo 104 chodcov pri 1691 nehodách, ktoré priamo v 681 prípadoch zapríčinili chodci, je potrebné sa adekvátne venovať aj im aj vrátene represie.   3, Treba sa zamerať na kritické nehodové lokality a tie riešiť nielen  vyššou kontrolou vodičov, ktorá je jednoznačne oprávnená, ale aj úpravami v cestnej infraštruktúre a v dopravnom značení.   4, Zmenou legislatívy a to nielen zmenou zákona o cestnej premávke, kde je potrebné aplikovať niektoré vylepšenia, ale aj zásadné zmeny v cestnom zákone, aby bol termínovo kompatibilný zo zákonom o cestnej premávke a dostala sa do neho povinnosť pre správcov komunikácií zakresľovať do cestnej databanky aj  miestne a účelové komunikácie, vrátane dopravného značenia na úroveň jazdných pruhov, aby aj verejnosť mala k dispozícii aktuálny popis cestnej infraštruktúry, ktorý je možné použiť aj v GPS navigácii a zariadeniach automaticky kontrolujúcich konanie vodiča. Na druhej strane preto aby správcovia komunikácii boli verejne kontrolovateľný a znížil sa počet dopravných nehôd z titulu chybného alebo nelogického dopravného značenia.   Záver:  Tak výrazne nízke počty usmrtených, ťažko zranených, ale aj ľahko zranených v cestnej doprave, ešte za Slovenskej republiky nikdy neboli dosiahnuté. Potešilo ma aj konštatovanie ministra vnútra, keď poďakoval všetkým vodičom za správanie sa na cestách, vďaka ktorému bol dosiahnutý takýto pozitívny výsledok.   Dodatok:  Ak radi počúvate besedy, tak táto téma bola diskutovaná aj v pravidelnej relácii Slovenského rozhlasu - kontakty.  Záznam relácie: Vývoj nehodovosti na slovenských cestách v uplynulom roku. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (32)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




