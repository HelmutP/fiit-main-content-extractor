

 K takýmto jednoduchým voľbam nám chýba možnosť poznať kandidátov osobne, alebo aspoň lepšie ako suseda či brata. 
 Keďže takáto možnosť nie je reálna, potrebujeme prísnejšie kritériá (aby sme nemali "vládu akú si zaslúžime", ale radšej lepšiu - aj ako sme my sami). 
 Nemožno voliť: 
 - člena zločinneckej skupiny, aj bývalého (napríklad člena KSČ), zvykne mrzko rozprávať a používa meno Božie nadarmo - "boha, ale sme im dali, novinárom!" - autor Googlovič. 
 - pávoplatne odsúdeného človeka za krádež (môžeme aj neodsúdeného, záujemcov je dostatok) 
 - rozvedeného človeka, aj nevinne (čo to má s politikou? iba predpoklad, že kto nevie spravovať vlastnú rodinu, štát určite nedokáže) 
 Takéto kritériá by vyvetrali parlament viac, ako voľby v Čechách. Súdnictvo by zdecimovali. Gauneri a oplani by to mali ťažké. 
 http://www.youtube.com/watch?v=4QRm0ByyupI 

