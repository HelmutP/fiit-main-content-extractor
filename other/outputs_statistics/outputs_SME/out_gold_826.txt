

 Odjakživa prikladám menám význam hádam aj väčší, ako si zaslúžia. Ako malá som si vymýšľala imaginárnych kamarátov nie preto, že by som sa cítila sama a túžila po spoločnosti, ale preto, aby som mohla niekoho nejako nazvať. Dať mu meno - také, aké sa mi v ten deň páčilo. Vymýšľala som mená aj pre seba a ostatných členov rodiny. Keď som mala štyri roky, snažila som sa všetkých presvedčiť, že sa volám Adliatka Hotanová. Bohužiaľ, nik si tento pseudonym nevedel zapamätať (len ja si ho pamätám doteraz). Moja fantázia nepadla na úrodnú pôdu a tak som sa vzdala a moje ďalšie akože-mená boli inšpirované už len kalendárom. No ešte dodnes občas nenápadne sledujem ľudí a hádam, ako sa volajú, alebo aké meno sa komu hodí. A teraz mám dovoliť niekomu aby dal môjmu (síce zrejme aj svojmu) dieťaťu meno Matej alebo Rasťo? To teda nie. 
 
Už som skoro začala premýšľať nad hľadaním nového potenciálneho otca. Našťastie len skoro. A keďže zatiaľ nijaký možný nositeľ mena nie je na ceste, moja polovička usúdila, že na takúto debatu je priskoro a odmietol ďalej diskutovať. No mne to vŕtalo v hlave. Stále vŕta. A tu nastal ten zlom. Pretože práve toto je osvedčený ťah ako presvedčiť moju tvrdú hlavu. Povedať, len načrtnúť a nechať tak. A nechať, aby mi to vŕtalo v hlave. Nechať ma premýšľať bez nátlaku. A ja som premýšľala a pritom som si asi šesťkrát prelistovala kalendár a rôzne internetové fóra a stránky s menami. Aj s menami slovanskými. Hlavne s tými.  
 
Veď vlastne nie sú až také zlé. Niektoré sú celkom pekné. Dokonca aj v kombinácii s partnerovým zvláštnym priezviskom. Napríklad Mojmír (pôvodný význam môj mier, môj svet). Alebo Kvetoslava. Alebo Vladimír (vládni svetu, veľký vládca). Či Ľubomíra. Aj Lada (podľa mena slovanskej bohyne krásy), ale tak sa už volá sesternicina dcérka a staré mamy (a nielen tie) dosť krútili hlavami. Ale ani Tichomír (s pôvodným významom tichý mier) nie je zlé meno. Ani Bronislava a Branislav (brániaci slávu, slávni obrancovia) (ale nie Branislava a Bronislav!). Našla som dokonca aj také, o ktorých som doteraz ani nevedela, že existujú – Borimír (bojujúci o mier) alebo Radana (variant mena Radovana). 
 
 
 
Neviem, či sa mi tieto mená budú páčiť viac ako Jonáš, Jeremiáš, Ondrej, Hana, Petra a Paulína. A Cyril a Cecília pre dvojičky. Ostatne, tieto mám rada už dlhší čas. A pre staré mamy (a nielen tie) to budú mená rovnako podivné, ako tie s rýdzo slovanskými koneňmi (i keď ktovie...). Takže je to vlastne jedno. Hlavne keď je mi dopredu jasné, že budúci otecko práve s týmito menami súhlasiť nebude (možno okrem toho Mojmíra) a že nás konzultácie ohľadom mien budú stáť ešte veľa prebdených nocí a zrejme sa nevyhneme ani početným výmenám názorov. Aspoň že starý otec by bol rád. Ten bol Dionýz. Prastarký Karol Koloman (bežne mali aj viac mien, vlastne ja som tiež podľa krstného listu okrem Lívie ešte aj Viera) a jeho otec Baltazár. Je to už akási Pongrácovská tradícia – nezvyčajné mená.  A vyzerá to tak, že v nej budeme pokračovať. 
 
Zas mám chuť, ako keď som mala štyri roky, niekoho nejako nazvať. Dať mu meno – také, aké sa mi práve páči. Napríklad Ratibor alebo Slávka. Len Rastislava doma určite mať nebudeme (bez urážky, vec vkusu a o tom našom vkuse už po tomto článku aj tak možno pochybovať). 
 
Nevyriešilo sa nič (vlastne ani nebolo čo), len som si trochu popremýšľala a vymyslela kompromis. Slovanské mená sú krásne (minimálne tak krásne - ak nie ešte krajšie - ako tie nemecké, hebrejské a latinské). Ak raz budeme mať deti, určite ich naučíme, aby boli hrdé na svoj pôvod. Je na čo byť hrdý. Minimálne meno bude toho symbolom. 
 
 
 
 
 
Viac o menách a o ich pôvode:
 
 
www.fodor.sk 
 
http://slovnik.dovrecka.sk/etymologicky-slovnik-mien 
 
 

