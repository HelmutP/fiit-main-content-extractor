
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 STV: Amíci si dvojičky odpálili sami, Obamova manželka je rasistka 

        
            
                                    20.11.2009
            o
            16:40
                        |
            Karma článku:
                12.96
            |
            Prečítané 
            48727-krát
                    
         
     
         
             

                 
                    V južnej Afrike zavládli apartheid a genocída belochov. Za vraždami Lincolna a Kennedyho sú bankári. Za nežnou revolúciou či prasacou chrípkou je CIA, respektíve farmaceutické firmy. To všetko ponúka divákom STV pravidelne ich zahranično-politický redaktor Ľubomír Huďo.
                 

                 
Mravec, ale aj ty sa priznaj. Kto ťa platí za tie trápne otázky? 
   V prvej polovici dekády pracoval Huďo ako redaktor zahraničnej politiky v STV, pravidelne vystupoval v hlavnej spravodajskej relácii. Vtedy v éteri ešte nevykazoval (resp. mu nebolo dovolené vykazovať) známky konšpiračného stihomamu. Po roku 2005 sa z obrazovky Správ vytratil. Tento rok sa každý týždeň objavuje v Rannom magazíne STV, čo je informačno-zábavná zmes správ, rozhovorov a hudby. Pozeráva ju do 30 tisíc divákov. Ranné magazíny sú v televíziách bežné. Ale nie typ konšpiračných komentárov aké Huďo ponúka. Televízia ho zvykne predstavovať ako „redaktora STV.“ V profile na webe televízie o sebe sám vyhlasuje:   Učarovalo mi medzinárodné dianie a zákulisie zahraničnopolitických udalostí. Vzhľadom na sklon plávať proti prúdu a nepodliehať davovej psychóze rád poznávam, čo sa skrýva za vonkajším pozlátkom reality okolo nás. Väčšina sa zvykne väčšinou mýliť a preto pochybovať o „jasných“ pravdách je pre mňa povinnosťou a výzvou, aj keď to okolie považuje za neprijateľné.   Ako sa to prejavuje v praxi? Minulý týždeň si Huďo zafilozofoval o nežnej ale aj iných demokratických revolúciách vo východnej Európe. Kým diváci sa pozerali na archívne zábery veľkej novembrovej demonštrácie na pražskej Letnej a hovoriacemu Václavovi Havlovi, divák počul Huďove varovné slová:   Na úspechu [revolúcií] sa výraznou mierou podieľajú aktívne skupiny – profesionálni revolucionári s finančnou podporou svojich sponzorov. Majú títo sponzori revolúcií nezištné záujmy? A ide im len a len o blaho a prosperitu občanov vzdialených krajín?                 Samozrejme nie, ako odhaľuje ďalej v rozhovore Huďo: "vždy treba si uvedomiť, že existuje skupina ľudí dobre financovaná, ktorá manipuluje masou, väčšinou, entuziazmom ľudí, aby dosiahla vyslovene ekonomické ciele." Spomína sa napojenie tých „profesionálnych revolucionárov“ na americké nadácie, na CIA, na miliardára Sorosa. Vraj chceli a vlastne aj dosiahli našu liberalizáciu cien, divokú privatizáciu, inštaláciu neoliberalizmu. František Guldan, jedna z novembrových osobností, to včera v SME okomentoval že „taký novinársky odpad.. by sa nezohrial v slušnej TV ani päť minút“.  Keď naši futbalisti postúpili na MS do Južnej Afriky, Huďo divákov upozornil, že vlastne v tej africkej krajine prebieha skrytá genocída belochov, opačný apartheid, a že svet a Mandela tentoraz zahanbujúco mlčia. Moderátorka na záver tohto výstupu dodala, že už sa ani neteší, že tam naši postúpili,  a že keď tam už pôjdu „naši by sa mali minimálne poriadne opáliť.“               Pozrel som si zhruba 20 takýchto vystúpení. Huďova argumentácia stojí na viacerých „zisteniach.“ Svet je podľa neho rafinovane ovládaný malou tajomnou skupinkou elít. Politici sú vlastne bábky, nitkami ťahajú bohatí, najmä bankári Rothschildovci (Huďo opakovane vyťahuje 200 rokov starý citát zakladateľa tejto bankárskej rodiny ako dôkaz ich nekalých úmyslov) či priemyselníci Rockefellerovci. Ale machrami manipulácií je aj americká a izraelská vláda. Bystrí sa z Huďových slov dovtípia, že často menovaní vládcovia sveta majú židovské mená a žijú najmä v USA (sám redaktor do priameho antisemitizmu ale neskĺzol). Hlavné svetové i naše médiá sú zaujaté, lebo sú ovládané bohatými bankármi a investormi. Taja nám podstatu, manipulujú nami. Chcú nám siahnuť aj na internet a umlčať alternatívne webstránky prinášajúce pravdu ako je tá Huďova (John D. Rockefeller IV navrhoval tento rok za určitých okolností silne regulovať prevádzku na internete v záujme ochrany proti kyberterorizmu). Vo všeobecnosti za všetkým dôležitým vo svete je neukojená žiadostivosť po peniazoch. Dvojičky si Američania zbúrali sami, aby predsa mohli viesť vojny v Iraku a Afganistane a tak zarobiť na predaji zbraní a ovládnuť nové územia. Bin Ládin s tým nič nemal, hoci terorista to je, varuje redaktor STV.                Huďov štýl pripomína ekonóma Petra Staněka – fakty a cudzie mená chrlí rýchlosťou vodopádu, tvári sa veľmi znalecky a presvedčivo, no pri pozornejšom počúvaní zisťujete, že argumenty nesedia a autor splieta jablká s brokolicou. Verejnosť je podľa neho zmanipulovaná médiami a preto takí ako on ľuďom otvárajú oči, no viackrát zase tiež hovorí, že samozrejme platí jeho konšpirácia (smrť JFK či odpor proti protiraketovému štítu), lebo veď ani verejnosť oficiálnym verziám neverí. Z Huďových zoznamu faktov a faktíkov, čo som narýchlo vedel overiť, sú mnohé pravdivé. No on spája tieto malé, samo osebe málo významné príbehy a výroky do obludných konšpiračných celkov, ktoré teoreticky môžu s faktami sedieť, ale pritom existuje aj 10 realistickejších vysvetlení (čo je inak klasická metóda práce konšpirátorov). Nikdy divákom neponúkne podstatu väčšinovo akceptovaných zistení a argumentov. Často cituje obskúrnych novinárov a vedcov. Schopnosť filantropie nemá u neho miesto, ľudia všetko robia len pre seba a pre peniaze.   Občas sa Huďo uchýli rovno k dezinformáciám. Významný americký denník Washington Post nevlastnia ani nikdy nevlastnili Rockefellerovci. Michelle Obamová, manželka prezidenta, nikdy vo svojej dizertačnej práci netvrdila, že „bieli Američania sú nevykoreniteľní rasisti so sklonmi k zločinu a nenávisti.“ Ide o mýtus, ktorí šíria blázni na tých fantastických alternatívnych weboch. Stačí si pozrieť tú Obamovej prácu.   Problém našej verejnoprávnej televízie nie je, že by Huďove názory nemali nikdy zaznieť (hoci, raz za rok by aj stačilo). Nemálo ľudí takým rečiam verí. Ale zodpovedné médium ponúkne takéto tvrdenia v kontexte, s primeranou oponentúrou, a s tvrdou kontrolou faktov pri tých konšpiračných tvrdeniach. Namiesto toho ich STV divákom dávkuje pravidelne, bez kontroly faktov a bez prítomnosti skutočných odborníkov či znalcov.   PS V rokoch 2007 a 08 bol Huďo hlasmi divákov poctený nomináciou na Osobnosť televíznej obrazovky (cena OTO).  Zatiaľ to na cenu nebolo.   PPS Na Huďovu estrádu ma upozornil kolega Dušan Zachar. Dve inštitúcie, ktoré Huďo spomína ako hlavných manipulátorov sveta – americká vláda, nadácia G. Sorosa – sú donormi či už tohto blogu alebo viacerých aktivít Transparency, kde pracujem. Nikdy som od nich nepočul príkazy, čo písať a koho podporovať či kritizovať. Práve rozmýšľam, čo má z toho CIA, ak bude u nás menej korupcie, ak vláda bude robiť poctivé súťaže v obstarávaní, či ak bude menej plagiátorstva v našich médiách. Ako bábka to ale zrejme ani nepotrebujem vedieť.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (605)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




