
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ladislav Wolf
                                        &gt;
                Nezaradené
                     
                 Globalizácia 

        
            
                                    28.6.2010
            o
            14:49
                        |
            Karma článku:
                4.44
            |
            Prečítané 
            843-krát
                    
         
     
         
             

                 
                    Globalizácia predstavuje pre niekoho nástroj pre zvýšenie životnej úrovne a pre iných je to diablov nástroj na zvyšovanie rozdielov medzi chudobnými a bohatými krajinami. Je nutné zmeniť ponímanie tohto pojmu. Je smutným faktom, že výučba na základných a stredných školách o tomto fenoméne úplne absentuje. Je potrebné zodpovedať si otázku : „Predstavuje pre nás globalizácia príležitosť alebo hrozbu?“
                 

                 Globalizácia je jav, ktorý nás obklopuje a azda s výnimkou občanov Severnej Kórey sa pred ňou uniknúť nedá. Existuje množstvo analýz a tvrdení aké sú prínosy globalizácie z ekonomického hľadiska. Avšak nemenej potrebná je aj diskusia prečo nemalá skupina ľudí tak vehementne protestuje či už formou slovných argumentov v tom lepšom prípade alebo hádzaním dlažobných kociek v tom horšom.   Protestujúcich skupín je mnoho napr. ekologický či bojovníci za mier, ale dve z nich svojou početnosťou prevyšujú ostatných. Prvou skupinou sú samozvaní bojovníci proti všetkému čo má nádych kapitalizmu a v "súťaží" o najväčšieho bojovníka počas protestov na stretnutí G 8 je ten, ktorý rozbije viac sklenených tabúľ vo fast-foodoch.Druhú skupinu tvoria ľudia, ktorí odmietajú z rôznych dôvodov, ale spojovacím tmelom je predovšetkým strach. Strach z nového, strach z nových požiadaviek na nich ako na ľudí. Globalizácia predstavuje zbližovanie krajín a to predovšetkým zdokonaľovaním informačných technológií a schopnosťou komunikovať vo svetovom jazyku. A práve schopnosť komunikovať v angličtine a taktiež dosahovať potrebnú počítačovú gramotnosť sú najväčšie požiadavky, v ktorých má predovšetkým staršia generácia na Slovensku najväčšie rezervy. Nikomu neprináleží výsmech z takýchto ľudí, ktorí nevedia čo je mail či chatovanie, pretože žili v priestoru, kde nie trh, ale papaláši z ústredného výboru "plánovali" a tým pádom sa logicky nekládol dôraz na flexibilitu zamestnancov a upevňovania nových schopností.   Globálizácia predstavuje prekročenie pomyselných hraníc, a to nielen tých geografických. Je to obrovská príležitosť a nie hrozba. Využime túto príležitosť a prekonajme myšlienky o rôznych konšpiračných teóriách, ako nás ovláda malá skupina vlastníkov nadnárodných spoločností...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Wolf 
                                        
                                            Neobyčajný poslanec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Wolf 
                                        
                                            Klub Dosluhujúcich Hošanov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Wolf 
                                        
                                            Naučme sa učiť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Wolf 
                                        
                                            Rozkol v SNS
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ladislav Wolf 
                                        
                                            Vysokoškolské vzdelanie
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ladislav Wolf
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ladislav Wolf
            
         
        wolf.blog.sme.sk (rss)
         
                                     
     
        Študent Ekonomickej Univerzity v BA . . .
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    24
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    936
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




