
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Nikolas Winkler
                                        &gt;
                Recenzie a analýzy
                     
                 Najlepší hokejista súčasnosti 

        
            
                                    11.3.2010
            o
            13:04
                        (upravené
                18.3.2010
                o
                15:43)
                        |
            Karma článku:
                5.69
            |
            Prečítané 
            2712-krát
                    
         
     
         
             

                 
                    Vždy musí byť niekto najlepší. Dnes sa proti sebe postavia Sid´The Kid´Crosby a Alexander´Ovie´Ovechkin. Súboj titanov. Večne nezmieriteľní. Vyberte si svojho favorita a pustime s k porovnávaniu.
                 

                    1. OSOBNOSŤ   Obaja sú kapitáni a samozrejme najdôležitejšími hráčmi svojich klubov. Sidney je medializovanejší a populárnejší u jemnejšieho pohlavia. Nežije bežným životom, v jeho prípade by to nebolo možné. Má vlastný vchod na štadión. Alex chodí nakupovať osobne. Kde sa ukážu spôsobujú okamžitý rozruch. Sidney je pravý vodca, a je o niečo charizmatickejší. Byť v jeho spoločnosti je o stupeň elektrizujúcejšie. Prvý bod ide na konto kanadskej star.   CROSBY - OVECHKIN 1-O      2. HOKEJOVÉ MYSLENIE   Sidney má post centra v krvi. Je to playmaker. Saša je typický krídelník. Sid má najlepšie hokejové ruky od čias Gretzkyho. Má lepší prehľad v hre, je schopný vždy vymyslieť nečakané riešenie. Rozhodujúci gól vo finále ZOH 2010 bol jasným príkladom jeho geniality. Ešte ani poriadne neprebral puk a už strieľal. Bolo to pre ostatných príliš rýchle, aby stihli zareagovať. Crosby však presne vedel čo robí. Ovečkinova hra je viac postavená na fyzickej kondícii než na hokejovom rozume. Aj druhý bod teda berie ´The Kid´.   CROSBY - OVECHKIN 2-0       3. STRELA   Obaja sú bezpochyby skvelými strelcami. Ale góly, ktoré dáva ´ruský cár´ sú veľkolepé a prekrásne. Dokáže skórovať prakticky z každej pozície. Jeho gól zo 16.januára 2006 proti Phoenixu Coyotes označili zámorské média za gól storočia NHL . ´Agent 008´ďalej vyniká veľmi jedovatým a presným príklepom, problémy mu nerobia ani strely bekendom či švihom. Čo sa týka rozmanitosti strely - Ovechkin poráža Crosbyho a znižuje skóre.          CROSBY - OVECHKIN 2-1       4. PRIHRÁVKA   Rus ostane rusom. Ovechkin má rád puk a radšej zakončuje než prihráva. Zato Crosby je rodený nahrávač so schopnosťou presne servírovať prihrávky priamo do zakončenia. Jednoducho jeho nahrávky ´majú oči´. Jeho špecialitou je prudká prihrávka bekendom alebo takzvaná ´žabička´. Sid vie presne kde sa na ľade práve nachádzajú všetci hráči a patrične to využíva.   CROSBY - OVECHKIN 3-1       5. SILA   Fyzická sila je devíza rusa. Jeho tvrdá hra hraničí až s brutalitou. Neraz prišpendlí súpera na mantinel silou rozbehnutého vlaku. Špecialitou sú jeho ´neviditeľné hity´. Je to typický obrázok jeho hry: Ovečkin na svoju obeť vytrvalo čaká, a jemne si ju nadbehne a potom nečakane udrie. Súper často ani nevie, z ktorej strany prišiel náraz. Alex sa púšťa do kontaktu aj s fyzicky väčšími protivníkmi a málokedy v týchto minisúbojoch prehráva. Crosby tak často neatakuje...   CROSBY - OVECHKIN 3-2       6. TECHNIKA   Je to tu. Najvyrovnanejšia disciplína. Obaja sú technicky mimoriadne zdatní. Alex má o niečo efektnejšie kľučky. Často využíva prudké zmeny smeru celého tela.  Je lepší v súboji jeden na jedného. Ale Crosby dokáže svojím klamlivým pohybom naraz zmiasť aj dvoch či troch protihráčov. Obľubuje jemnú techniku vo veľkej rýchlosti. Keď vedie puk tak si ho neustále prehazuje z forhendu na bekend. Jednoducho puk na jeho hokejke je neustále v pohybe. Je to nezvyčajný obrázok, pretože obranca musí byť neustále v strehu. Bod je len jeden a preto treba rozhodnúť. Efektivita vyhráva na efektnosťou - bod získava Crosby.   CROSBY - OVEČKIN 4-2       7.ODOLNOSŤ   Crosby je o niečo predvídavejší a trafiť ho kvalitným hitom je náročné. Je to taká ´lašička´na korčuliach. Je fyzicky o niečo slabší než jeho kolega. Alexander toho znesie jednoznačne viac, jeho vytrvalostný fond sa zdá byť nevyčerpateľný. Čo sa týka osobných súbojov Saša viac rozdá a viac prijme. Bod berie ruský hokejista.   CROSBY - OVECHKIN 4-3       7. RÝCHLOSŤ   Ovechkin je o niečo výbušnejší a aktívnejší v pohybe. Jeho nohy pôsobia pri korčuľovaní ako dva prúdové motory. Pri rozbiehaní spraví viac krokov, niekedy sa zdá že stráca balans, na úkor zrýchlenie. Je to len zdanie, pretože jeho rovnováha a stabilita na korčuliach patria v NHL k najlepším. Sid je ladnejší korčuliar. Rozbehne sa, a ďalej už len mení smer, využíva výborný sklz. To neznamená, že by bol pomalý. V rýchlosti sú opäť obaja na vysokej úrovni. Ale rus je predsa len vo všeobecnosti považovaný za rýchlejšieho hráča.   CROSBY - OVECHKIN 4-4      9. PRODUKTIVITA   Crosbyho bilancia v čase písania tejto recenzie činí: 355 zápasov - 176 gólov - 305 asistencií - 481 bodov. Priemer bodu na zápas: 1.35. Ovečkinove čísla sú: 383 zápasov - 263 gólov - 251 asistencií - 514 bodov. Jeho priemer je 1.34. Alex teda nazbieral bodov o niečo viac, ale potreboval na to aj viac zápasov. Ich bodový priemer je vzácne vyrovnaný a pri ich veku ide o veľmi lichotivé čísla. Kanaďan je v play-off o niečo produktívnejší. Máme za sebou ďalšiu vyrovnanú disciplínu a opäť vyhráva o ´chlp´ Crosby.   CROSBY - OVECHKIN 5-4       10. OSOBNÁ KONFRONTÁCIA   Samozrejme, že hlavným lákadlom bol súboj druhého kola play-off v roku 2009 medzi Penguins a Capitals. V druhom zápase série zaznamenali obaja hráči hetriky. Celú sériu napokon zvládol lepšie Pittsburgh a postúpil 4-3 na zápasy. Očakávaným súbojom bolo olympijské štvťfinále Kanada-Rusko. Crosbyho družina vyhrala 7-3. Keď sa stretnú zoči-voči Crosby má častejšie jemne navrh.   CROSBY - OVECHKIN 6-4       11. ÚSPECHY   Ovechkin je majstrom sveta /2008/. Má o niečo viac individuálnych ocenení...ale Sidney Crosby má aj v tejto disciplíne navrch pretože získal dve najcennejšie trofeje aké sa dajú v hokeji vyhrať. Získal Stanley Cup /2009/ a vyhral Olympiádu /2010/. Keďže kolektívne úspechy sa cenia viac ako individuálne, záverečný zárez robí severoameričan.   CROSBY - OVECHKIN 7-4       ZÁVEREČNÝ VERDIKT   Víťazom sa teda stal Sidney Crosby. Vo svojom veku /nar. 7.augusta 1987/ je mimoriadne úspešným hráčom. Je to špecifický zjav a klenot dnešnéh hokeja. Dnes teda môťe spať kľudne. Alexander Ovechkin bol vyrovnaný súper, ostáva mu dať klobúk dole a hlavu hore. Pretože dnešný súboj neznamená, že v budúcnosti sa nemôže situácia otočiť v jeho prospech...          Foto zdroj: nhl.com 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Jeden na jedného: Forsberg vs Lindros
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Hokejové veľmoci: USA
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Hokejové veľmoci: Česká republika
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Hokejové veľmoci: Fínsko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Nikolas Winkler 
                                        
                                            Marián Hossa hviezdou NHL? Ani náhodou.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Nikolas Winkler
                        
                     
            
                     
                         Ďalšie články z rubriky šport 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Fabian 
                                        
                                            Žreb Ligy Majstrov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Duda 
                                        
                                            Rozprávka o slávnom futbalovom kráľovstve
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marian Čapla 
                                        
                                            Za dvadsaťjeden tristoštyridsaťdva. Je to veľa alebo málo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Jačiansky 
                                        
                                            Ako chutí dobrá francúzska polievka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Adrián Kocur 
                                        
                                            Halák hviezdou nových Islanders
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky šport
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Nikolas Winkler
            
         
        nikolaswinkler.blog.sme.sk (rss)
         
                                     
     
        Žijem v USA. Mojou najväčšou záľubou je hokej. Hlavne severoamerický.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    55
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2121
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            TOP
                        
                     
                                     
                        
                            Recenzie a analýzy
                        
                     
                                     
                        
                            Hráči
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




