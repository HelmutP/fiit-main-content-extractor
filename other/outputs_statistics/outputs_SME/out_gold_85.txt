

 
 
Alex, zvykla som si, že ťa vidím každý polrok, možno okolo Vianoc, či na chvíľu
cez letné prázdniny, zvykla som si, že mám pre Teba v telefónnom zozname
desať mien a pod každým iné číslo, tak často sa sťahuješ z krajiny do
krajiny. Zvykla som si, že máš priateľku, zvykla som si, že na Teba letia moje
rovesníčky, zvykla som si na to, aký si občas uštipačný, ako občas nechápeš, že
som trocha vyrástla, ako si nevieš predstaviť, koľko stoja ženské topánky,
alebo aký dôležitý je školský ples... zvykla som si na to, ako si doberáš
servírky, na to, ako stále meníš práce, na to všetko som si zvykla a na
veľa vecí si si musel zvyknúť aj Ty... je to neuveriteľne ťažké, zvykať si
každý rok na zmenu, na čosi iné a Ty vieš, ako veľmi nenávidím zmeny...
ale prispôsobím sa, to vieš, si môj ocko...
 
 
            Šaňo, jednej veci
sa však odmietam prispôsobiť a je mi úplne jedno, nakoľko sa ma na to
ktokoľvek bude snažiť pripraviť... proste odmietam pomyslenie, že ťa už nikdy
neuvidím. Aj raz za pol roka bolo málo, ale nikdy? Oci... to prosím nemyslíš
vážne, prosím... 
 
 
            Tato, toto všetko
píšem len pre Teba, s vedomím, že si to prečítajú stovky ľudí, ktorí sa na
tom budú smiať, ktorí si neuvedomia, že každodenné katastrofy číhajú hneď za
rohom, ktorí možno nevedia, aké je to vyrastať s jedným rodičom
v dvojizbovom byte na jednom z prešovských sídlisk... nikto nevie,
aké je to byť mnou, ani ja neviem, aké je to byť kýmsi iným, nemôžem byť taká
egoistka, ako si mi stále vyhadzoval na oči, však? Spravím hocičo, oci, čo
budeš chcieť, vzdám sa všetkých peňazí, prestanem protestovať proti malému
vreckovému, nechám si ten starý mobil, čo mám... ak chceš, už nikdy sa
nestretnem so žiadnym chlapcom (hoci v tomto smere si vždy bol ústretový)
a nikdy už nebudem chcieť drahé šaty, keď chceš, už nikdy neprinesiem
domov zlú známku, neurobím Ti hanbu, napíšem Ti každý deň mail a vydám
básnickú zbierku, budem s Tebou hrávať šach a pri ceste autom mi už
nič nespadne... všetky veci, čo Ti na mne kedy vadili už nikdy neurobím,
nebudem sa na Teba nikdy hnevať, ani Ti nič vyčítať, neodsúdim ťa, nebudem sa
ťa pýtať na tvoj osobný život... sľubujem, už nikdy nič takého nespravím... len
sa vráť, príď tak, ako si sľúbil, buď chvíľu so mnou a znova môžeš ísť
zabávať svet... si môj ocko... nemáš len Ty mňa, ale aj ja vlastním Teba. Aj
keď je to možno majetnícke, ale je to tak. Nikdy nezmeníš, že som tvrdohlavá,
ako Ty, že neviem odhadnúť čas, že jedno vravím a druhé robím, že rovnako
krivým obočie a mám rovnako slabý backhand... to nemôžeš zaprieť, toho sa
nemôžeš vzdať. To proste nejde.
 
 
            Ocko, keď som si
pred siedmymi rokmi musela navyknúť na polovičný život a vyrastať bez
polky rodiny, ešte som to nevnímala ako krivdu. Nezdvíhaš ani jedno
z desiatich čísel, zopár ich už používa niekto iný... nechodíš si pozerať
maily, tak vsádzam na poslednú možnosť. Moje blogy si si nikdy nenechal ujsť.
Moja tajná túžba je, že Ti neujde ani tento. Viem, že niekde tam si, že niekde
po Tebe letia všetky baby, keď ich ohuruješ svojim britským akcentom
a neuveriteľnou sčítanosťou, obrovskou inteligenciou. Dúfam, že tam, kde
si je Ti dobre... ale dúfam, že ťa raz nájdem, lebo ja sa ťa nevzdám. Poznáš
ma, vieš, že čo si zaumienim, to dosiahnem. Neviem, kedy to bude, ale raz si ťa
nájdem, čím skôr, tým lepšie. Prosím ťa, ozvi sa... 
 
 
 
 
 
Ľúbim ťa a z celého srdca napriek všetkému, čo sa kedy stalo
budem, možno som ti to nikdy nepovedala, ale si môj vzor v mnohých
veciach,
 
 
Ester 
 
 
            
 

