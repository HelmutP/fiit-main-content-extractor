

   
   
   
   
 Kantáta slov 
 rinie sa mysľou 
 objímam všetkých 
 myšlienkou čistou 
   
 Posielem slnečné 
 pozdravy z Turca 
 svieti konečne 
 mám silu borca 
   
 Zatváram ponurosť 
 na dno truhlice 
 lúčom nastavím 
 bozkuchtivé líce 
   
 Ten božtek slnečný 
 uchovám v pamäti 
 nech hreje aj vtedy 
 keď slnko nesvieti 
   
 Máj ešte dažďom 
 našu zem zmáča 
 po dnešku už (ale) nebudem 
 zblúdené vtáča 
   
 Posielam veršíky 
 l e n   t a k 
 všetkým pre radosť 
 prianí úprimných 
 nikdy nie je dosť 
   
 Krásnu sobotu! 
   
   
 PS: Venujem Martuške Novotnej, ktorá si veľmi praje, aby dnešný  manželský sľub jej Moniky bol spečatený (aj)  slnečnými lúčmi. 
 *** 
   
 ( Pretože šťastie nikdy nie je niečo definitívne dané. Denno-denne si ho treba získať...)  
   

