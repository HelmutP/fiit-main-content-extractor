

 Na nákupy do susedného Poľska chodia Slováci radi už desiatky rokov. Cez víkendy je na hraničných priechodoch plno, v preplnených autobusoch sa ľudia vracajú z trhov so svojimi úlovkami - ratanovým nábytkom, kávou, kočíkmi, krovkami, vetrovkami, topánkami pochybnej kvality, bez záruky. Nič na tom nezmenilo ani to, že sme v Euŕopskej únii. Cena je cena. 
 "Nemáme dôvod nakupovať v poľských obchodoch, nakupujeme na trhoch. To sa oplatí," hovoria v Skalitom, ktoré hraničí s Poľskom. Kysučania vo všeobecnosti hovoria, že chodiť do poľských obchodov je zbytočné. Niečo je vraj lacnejšie, ale keď sa k cene priráta benzín, vyjde to rovnako. Iné hovoria tí, čo stavajú. Z Poľska vozia stavebný materiál, zatepľovacie dosky, obkladačky, strešné krytiny, vane, umývadlá, záchody. 
 Ceny sa však porovnávať nedajú. Značkový materiál stojí rovnako ako na Slovensku, v niektorých prípadoch aj drahší. Lacnejšie sú materiály, kde je neraz ťažké vystopovať výrobcu. Obchodníci na slovenskej strane hovoria, že porovnávať neporovnateľné je nezmysel. 
 Slováci vozia materiál na zatepľovanie, lebo ho kúpia lacnejšie. Má nižšiu hustotu, takmer žiadne izolačné vlastnosti. "Rovnaké je to s lacnými strechami, plastovými vaňami, batériami," hovorí šéf obchodu so stavebninami v Čadci, neďaleko poľských hraníc. 
 Materiál na zateplenie rodinného domu štandardnej veľkosti od 200 do 250 štvorcových metrov vyjde na Slovensku na približne 100-tisíc korún, v Poľsku od 60- do 80-tisíc, závisí od nekvality. Človek za nezaručenú kvalitu ušetrí v priemere tretinu ceny. 
 Na plastovej vani sa dá v Poľsku ušetriť aj tritisíc korún. Slovenskí predajcovia však tvrdia: Nedržia farbu, nie sú odolné voči škrabancom, sú krehké, majú krátku záruku, ak vôbec. 
 V poľských obchodoch sa dá kúpiť lacnejší tovar, ak človek poriadne hľadá. Tak napríklad, pri kúpe kamery JVC za 1399 zlotých, môžete oproti rovnakej u nás ušetriť viac ako štyritisíc korún alebo dvetisíc na umývačke riadu Electrolux. 
 Pri prepočtoch je potrebné brať do úvahy vzdialenosť a spotrebu benzínu. Z Banskej Bystrice je na hraničný priechod Skalité - Zwardoň 140 kilometrov a z Bratislavy asi 250. Z Bratislavy do Bielsko Bialej zaplatíte za cestu autom asi 800 a z Banskej Bystrice o 300 korún menej. 
 Pri ceste treba počítať aj s preplnenými úzkymi cestami na Kysuciach a ešte horšími na poľskej strane. Poľské cesty sú úzke, zle značené, bez spevnených krajníc, nájdete aj úseky zo žulových kociek. Priemerná rýchlosť sa pohybuje okolo 70 km/h a menej. 
 Ceny pohonných látok sú v Poľsku nižšie ako na Slovensku. Super 95 stojí 35,60 Sk, čo je o takmer korunu menej ako u nás, za 98 o korunu až dve menej a za naftu o štyri koruny menej za liter ako na Slovensku. 
 Zywiec 
 Prvým väčším mestom po prekročení hraničného priechodu v Skalitom je Zywiec. Z Banskej Bystrice do Zywca je 180 kilometrov, čo znamená, že benzín tam aj naspäť vyjde okolo tisíc korún. Odporúčame dotankovať plnú nádrž v Poľsku. Auto s dieselovým motorom môže na plnej nádrži ušetriť 200 korún. 
 Pre Bratislavčanov je Poľsko výhodné, len ak na nákupoch ušetria viac ako tritisíc korún. Približne 1600 korún totiž stojí iba cesta do prvého mesta a naspäť. 
 Zywiec nie je práve veľké mesto plné supermarketov, ale práve sem chodia Kysučania nakupovať stavebné materiály. Vstup do mesta od Zwardoňu lemujú obchody pripomínajúce diskont, kde predávajú stavebné materiály či sanitu do kúpelní a WC. Pútačov a reklám je dosť. 
 Dobré obchodné centrum na nákup vaní, umývadiel a batérií je Eko-Instal blízko centra Zywca. Po vstupe do mesta pokračujete približne tri kilometre stále rovno po hlavnej ceste a výrazný firemný pútač vás zavedie vľavo priamo na priestranné parkovisko firmy. Nevýhoda je, že hlavná cesta je úzka a frekventovaná, takže odbočiť mimo cesty je problém. Po hlavnej ceste pokračujete priamo do centra mesta, ktoré pripomína talianske uličky s množstvom značkových obchodov vrátane elektroniky, menších nákupných centier a kaviarničiek. Stredom mesta vás prevedie značenie smeru k nákupnému centru DETAL MARKET na okraji Zywca. Problematické je uvádzanie vzdialenosti v minútach, pretože tri minúty na značke v špičke znamenajú v skutočnosti 15 minút. Ďalšou špecialitou značenia je šípka v tvare ležiaceho "U", ktorá znamená, že ste úplne mimo a musíte sa vrátiť! 
 V nákupnom centre DETAL MARKET môžete nakupovať v troch častiach vrátane záhradníckych potrieb, v potravinách, ale aj v časti AVANS s elektronikou, bielou technikou fotoaparátmi vysavačmi, vežovými systémami, televízormi... Nevýhodou je malé, stále plné parkovisko. 
 Výhodou Zywca je to, že všetko podstatné sa deje okolo hlavnej cesty. V Zywci je dostatok čerpacích staníc vrátane LPG. Ceny benzínu sa pohybujú okolo 36 korún, nafta je lacnejšia, dostanete ju za 29 korún. Rozdiely medzi čerpadlami sú halierové. 
 Bielsko Biala 
 Bielsko Biala je vzdialená od Zywieca 20 kilometrov smerom na Katovice, teda približne 60 kilometrov od hraničného priechodu s Poľskom. Štvorpruhový severný hlavný ťah vedie priamo centrom mesta popri hoteli Prezydent až k nákupným centrám Hypermarket Tesco a neďalekej Hypernove na opačnom konci. Od južného vstupu do mesta sú hypermarkety prehľadne značené. Problematická je prehustená doprava v špičkách a nedostatok parkovacích miest v centre. 
 V strede mesta je väčšina značkových obchodov s oblečením, ale aj s obuvou či elektronikou, množstvo cenovo prístupných príjemných reštaurácií a kaviarní. Výhodou je, že všetko je situované v blízkosti hlavného ťahu a v priľahlých uličkách. 
 K hypermarketu Tesco je dobrý a prehľadne značený prístup. V Tescu je množstvo ďalších obchodíkov. Dá sa platiť aj bankovou kartou. Nevýhodou je, že plecniaky a tašky musíte pri vstupe do nákupných priestorov odovzdať. Fotografovanie vnútri nákupného centra, ale aj na parkovisku pred hypermarketom je zakázané. V blízkosti je aj benzínové čerpadlo s lacným benzínom. 
 Z Tesca sa jednoduchým spôsobom dostanete na kruhový objazd so smerovou tabuľou Zyviec a odtial stále po hlavnom ťahu cez mesto až na výpadovku. 
   
 Porovnanie cien v Poľsku a na Slovensku 
 
 



Čerpacie stanice


 
Poľsko
Slovensko - Slovnaft


Benzín 95
35,60 Sk
36,40 Sk


Benzín 98
37 - 38 Sk
39 Sk


Diesel
29 Sk
33,40 Sk


 
Avans Zywiec
Nay Slovensko


Televízor


Philips 21PT 1967
999 Zl (9 140 Sk)
8 990 Sk (Nay)


Thomson 21DX 175
1199 Zl (10 970 Sk)
10 990 Sk (Nay)


Philips 21 PT 4406
899 Zl (8 230 Sk)
8 990 Sk (Nay)


Umývačka riadu


Whirlpool ADP 4520
1349 Zl (12 340 Sk)
13 990 Sk (Nay)


Electrolux ESF 6101
1319 Zl (12 070 Sk)
15 990 Sk (Nay)


Práčka


Electrolux EWF 810 
1349 Zl (12 340 Sk) 
13 990 Sk (Nay)


Kamera


JVC GR-FxM39 1399 
(12 800 Sk)
16 990 Sk (Nay)


Sony CCD-TRV 228E 
1499 Zl (13 720 Sk)
9 990 Sk (Nay)


Canon MV700 1899 Zl 
(17 380 Sk) 
12 340 Sk (Nay)


Chladnička


Whirlpool ARC 5520
1499 Zl (13 720 Sk)
13 500 Sk (Nay)


Electrolux ERB 3043
1749 Zl (16 000 Sk)
14 500 Sk (Nay)


 
Eko-Instal Zywiec
UNIMAT B. Bystrica


Vaňa


plastová
422 Zl (3 860 Sk)
7 500 Sk


plastová
540 Zl (4 940 Sk)
7 600 Sk


rohová
943 Zl (8 630 Sk)
6 288 Sk


rohová
954 Zl (8 730 Sk)
8 420 Sk





 Umývadlo
120 Zl (1 100 Sk)
750 - 900 Sk


 
145 Zl (1 130 Sk)
770 - 920 Sk


 
209 Zl (1910 Sk)
785 - 958 Sk





 WC

400 Zl (3 660 Sk)
3 000 Sk


 
500 Zl (4 580 Sk)
3 300 Sk


 
Tesco Bielsko-Biala
Tesco Slovensko


Vibračná brúska
69 Zl (630 Sk)
699 Sk (akcia 299) Sk


Uhlová brúska
85 Zl (780 Sk)
899 Sk (akcia 499)


Príklepová vŕtačka
120 Zl (1 100 Sk)
1 199 Sk (akcia 599)


 
Potraviny - Poľsko
Potraviny - Slovensko


Káva


Tchibo 250 g exclusive
7,79 Zl (71 Sk)
83 Sk


Tchibo 250 g grand
6,79 Zl (62 Sk)
75 Sk


Tchibo 250 g family
4,97 Zl (45 Sk)
55 Sk


Jacobs Cronung 250 g
6,49 Zl (62 Sk)
80 Sk


Rama 500 g
3,19 Zl (29 Sk)
34 Sk


Cukor, 1 kg
2,99 Zl (27 Sk)
43 Sk


Kurča, 1 kg
6,99 Zl (64 Sk)
60 Sk


Karé, 1 kg
15,99 Zl (146 Sk)
145 Sk


Hovädzie stehno, 1 kg
19,99 Zl (183 Sk)
200 Sk


Syr Eidam, 1 kg
15,99 Zl (146 Sk)
219 Sk


Syr Gouda, 1 kg
15,99 Zl (146 Sk)
199 Sk


Zemiaky, 1 kg
0,55 Zl (5 Sk)
17 Sk




