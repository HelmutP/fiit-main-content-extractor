
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Drahovský
                                        &gt;
                Doprava
                     
                 Technická kontrola, emisná kontrola a ich termíny 

        
            
                                    6.2.2008
            o
            10:08
                        |
            Karma článku:
                10.69
            |
            Prečítané 
            37855-krát
                    
         
     
         
             

                 
                    Nakoľko často dostávam otázky na lehotu povinnej technickej kontroly TK a emisnej kontroly EK, rozhodol som sa zhromaždiť všetky údaje a spraviť prehľadnú tabuľku s termínmi. Najväčšie nedorozumenie spôsobuje ustanovenie o tom, že vozidlá prihlásené do evidencie po 1.1.2000 musia vykonať EK v čase od 1.1.2008 do 30.6.2008.
                 

                 
  
   Legislatívny rámec tejto povinnosti určuje vyhláška Ministerstva dopravy, pôšt a telekomunikácií Slovenskej republiky, číslo 578/2006 Z.z., ktorou sa vykonáva zákon 725/2004 Z. z. o podmienkach prevádzky vozidiel v premávke na pozemných komunikáciách.   Technická kontrola.  U bežných vozidiel a motoriek je prvá lehota na technickú kontrolu do štyroch rokov od prvého prihlásenia a potom každé dva roky. Malé motocykle, trojkolky a podobne majú štvorročnú lehotu, traktory dvojročnú. U vozidiel pre taxi, záchrannú službu a ostatné je lehota vždy jeden rok.    § 47 Lehoty technických kontrol pravidelných pre jednotlivé kategórie vozidiel a podrobnosti o podrobení vozidla technickej kontrole na základe rozhodnutia obvodného úradu dopravy (k § 50 ods. 3 zákona)  (1) Technickej kontrole pravidelnej podlieha vozidlo  a) kategórie L1e a L2e v lehote štyroch rokov po jeho prvom prihlásení do evidencie a potom periodicky v štvorročných lehotách,  b) kategórie L3e, L4e, L5e, L6e, L7e, M1, N1, O1 a O2 v lehote štyroch rokov po jeho prvom prihlásení do evidencie a potom periodicky v  dvojročných lehotách,  c) kategórie M2, M3, N2, N3, O3 a O4 v lehote jedného roka po jeho prvom prihlásení do evidencie a potom periodicky v jednoročných lehotách,  d) kategórie M, N a O používané na zdravotnícku záchrannú službu, banskú záchrannú službu a poruchovú službu plynárenských zariadení a vozidlo používané na taxislužbu v lehote jedného roka po jeho prvom prihlásení do evidencie a potom periodicky v jednoročných lehotách,  e) kategórie T a R v lehote dvoch rokov po jeho prvom prihlásení do evidencie a potom periodicky v dvojročných lehotách,  f) kategórie L, M, N, O, T a R používané v autoškole ako výcvikové vozidlo v lehote jedného roka po jeho prvom prihlásení do evidencie a potom periodicky v jednoročných lehotách.  (2) Prvým prihlásením vozidla do evidencie sa na účely tejto vyhlášky rozumie dátum prvého pridelenia evidenčného čísla v Slovenskej republike alebo v inom štáte, ktorý sa vyznačuje v osvedčení o evidencii. Ak sa tento dátum nedá zistiť, ale známy je rok výroby vozidla, za prvé prihlásenie vozidla do evidencie sa považuje 1. deň roku výroby vozidla.  (3) Ak sa rok prvého prihlásenia vozidla do evidencie v Slovenskej republike uvedený v osvedčení o evidencii nezhoduje s rokom výroby vozidla, pričom medzi rokom výroby vozidla a rokom prvého prihlásenia vozidla do evidencie je rozdiel väčší ako 12 mesiacov, za prvé prihlásenie vozidla do evidencie sa považuje 1. deň roku výroby vozidla.  (4) Ak rok výroby vozidla nie je známy, považuje sa zaň modelový rok vozidla, ktorý možno zistiť z identifikačného čísla vozidla VIN.    Emisná kontrola.  Emisná kontrola sa nevzťahuje ma motorky, trojkolky a podobne. U bežných aut so schváleným katalyzátorom je lehota štyri roky od prvého prihlásenia a potom každé dva roky. Pozor na staršie vozidlá s benzínovým motorom bez katalyzátora, alebo s neschváleným katalyzátorom, kde je lehota každý rok, tak isto ako u taxi, záchranných služieb, hromadnej prepravy osôb, nákladných vozidiel ap. okrem traktorov, kde je lehota dvojročná.   § 67 Lehoty emisných kontrol pravidelných pre jednotlivé kategórie vozidiel (k § 68 ods. 3 zákona)  (1) Emisnej kontrole pravidelnej podlieha vozidlo  a) kategórie M1 a N1 so zážihovým motorom so zdokonaleným emisným systémom v lehote štyroch rokov po jeho prvom prihlásení do evidencie a potom periodicky v dvojročných lehotách,  b) kategórie M1 a N1 so vznetovým motorom v lehote štyroch rokov po jeho prvom prihlásení do evidencie a potom periodicky v dvojročných lehotách,  c) kategórie M2, M3, N2 a N3 v lehote jedného roka po jeho prvom prihlásení do evidencie a potom periodicky v jednoročných lehotách,  d) kategórie M a N používané na zdravotnícku záchrannú službu, banskú záchrannú službu a poruchovú službu plynárenských zariadení a vozidlo používané na taxislužbu v lehote jedného roka po jeho prvom prihlásení do evidencie a potom periodicky v jednoročných lehotách,  e) kategórie T v lehote dvoch rokov po jeho prvom prihlásení do evidencie a potom periodicky v dvojročných lehotách,  f) kategórie M, N a T používané v autoškole ako výcvikové vozidlo v lehote  jedného roka po jeho prvom prihlásení do evidencie a potom periodicky v  jednoročných lehotách,  g) kategórie M1 a N1 so zážihovým motorom s nezdokonaleným emisným systémom v lehote jedného roka po jeho prvom prihlásení do evidencie a potom v jednoročných lehotách.  (2) Emisnej kontrole pravidelnej nepodlieha vozidlo kategórie M1 mazané zmesou paliva a mazacieho oleja a vozidlo kategórie L.     Prechodné ustanovenia.  Z dôvodu aby stanice technickej a emisnej kontroly (STK) stihli skontrolovať všetky vozidla, ktoré sa pred účinnosťou vyhlášky nekontrolovali, vytvorilo sa prechodné obdobie. Ak ste vozidlo prihlásili do evidencie napríklad v roku 2005, tak sa na vás prechodné ustanovenie o termíne EK do 30.juna 2008 nevzťahuje, ale platí lehota 4 roky od prvého prihlásenia.   § 104 Prechodné ustanovenia  (1) Technickej kontrole pravidelnej podlieha prvýkrát vozidlo  a) kategórie L1e a L2e uvedené v § 47 ods. 1 písm. a), počnúc 1. júlom 2007,  b) kategórie T a R uvedené v § 47 ods. 1 písm. e), počnúc 1. januárom 2008.  (2) Oprávnená osoba technickej kontroly, ktorá vykonáva a prevádzkuje stanicu technickej kontroly zriadenú podľa doterajších predpisov, ktorá nespĺňa najmenšie rozmery podľa § 33 ods. 4 alebo ods. 5 a 8, môže prevádzkovať takúto stanicu technickej kontroly do jedného roka odo dňa nadobudnutia účinnosti tejto vyhlášky.  (3) Oprávnená osoba emisnej kontroly, ktorá vykonáva emisné kontroly a prevádzkuje pracovisko emisnej kontroly zriadené podľa doterajších predpisov, ktoré nespĺňa priestorové vybavenie podľa § 52 ods. 1 tejto vyhlášky, môže prevádzkovať takéto pracovisko emisnej kontroly do jedného roka odo dňa nadobudnutia účinnosti tejto vyhlášky.  (4) Emisnej kontrole pravidelnej podlieha prvýkrát vozidlo kategórie M1 a N1 so zážihovým motorom so zdokonaleným emisným systémom uvedené v § 67 ods. 1 písm. a) v lehote  a) od 1. apríla 2007 do 30. júna 2007; to platí pre vozidlo, ktoré bolo prvýkrát prihlásené do evidencie do 31. decembra 1993,  b) od 1. júla 2007 do 31. decembra 2007; to platí pre vozidlo, ktoré bolo prvýkrát prihlásené do evidencie od 1. januára 1994 do 31. decembra 1999.  (5) Vozidlo kategórie M1 a N1 so zážihovým motorom so zdokonaleným emisným systémom uvedené v § 67 ods. 1 písm. a), ktorého lehota na vykonanie emisnej kontroly pravidelnej uplynie do 30. júna 2008, podlieha emisnej kontrole pravidelnej v lehote od 1. januára 2008 do 30. júna 2008; to platí pre vozidlo, ktoré bolo prvýkrát prihlásené do evidencie od 1. januára 2000.  (6) Emisnej kontrole pravidelnej podlieha prvýkrát vozidlo kategórie T uvedené v § 67 ods. 1 písm. e), počnúc 1. januárom 2008.  (7) Oprávnená osoba montáže plynových zariadení, ktorá vykonáva montáže plynových zariadení a prevádzkuje pracovisko montáže plynových zariadení zriadené podľa doterajších predpisov, môže vykonávať montáže plynových zariadení a prevádzkovať pracovisko montáže plynových zariadení do jedného roka odo dňa nadobudnutia účinnosti tejto vyhlášky s výnimkou automatizovaného informačného systému montáže plynových zariadení podľa § 112 ods. 27 zákona.  (8) Ustanovenia podľa § 34 ods. 2 písm. p) a ods. 5 písm. p) sa uplatňujú od 1. januára 2009, ustanovenie podľa § 34 ods. 5 písm. q) sa uplatňuje od. 1. júla 2007 a ustanovenia podľa § 53 ods. 3 písm. d), ods. 4 písm. d) a ods. 5 písm. d) sa uplatňujú od 1. januára 2009.      Prehľadová tabuľka prechodného obdobia .    Termín prvej emisnej kontroly pre vozidlá M1 a N1, okrem vozidiel s neschváleným katalyzátorom   Prvé prihlásenie do evidencie Posledný deň na vykonanie prvej emisnej kontroly   od 01.01.0000 do 31.12.1993  do 30.6.2007   od 01.01.1994 do 31.12.1999 do 31.12. 2007   od 01.01.2000 do 30.06.2004 do 30.6.2008   od 01.07.2004  do 4 rokov od prvého prihlásenia        Po absolvovaní TK aj EK dostanete nálepku na čelne sklo s termínom kontroly mesiac/rok okrem toho, preukazný lístok/preukaz kde je vyznačený deň, mesiac, rok. Dajte si pozor keď sa budete orientovať len podľa mesiaca a roka na nálepke, lebo môžete dostať pokutu a dôjsť aj o evidenčné osvedčenie o vozidle. Sťažoval sa mi nejeden čitateľ, že niektorí policajti veľmi radi pokutujú neplatnú TK a EK s presnosťou na deň, pričom na nálepke je vyznačený len rok a mesiac. Keď zadržia OEV, nemáte doklad s ktorým by ste mohli absolvovať predpísané kontroly. Preto z praktického hľadiska odporúčam mať odloženú kópiu OEV, pre prípad jeho zadržania ako následok jazdy s vozidlom, ktoré nemá platnú TK alebo EK. Ak z vozidlom nejazdíte a vypršala platnosť TK alebo EK a netrúfnete si jazdu na stanicu technickej kontroly, tak odporúčam dať si vozidlo odviesť odťahovou službou.       Záver:  Nakoľko čas veľmi rýchlo beží a človek zabúda, odporúčam dať si do mobilu pripomienku. Ešte jedna dôležitá vec, ktorá nepoteší predajcov vozidiel, ale vám môže ušetriť nemálo peňazí. Požiadať o TK a EK môžete aj z vlastnej vôle a kedykoľvek, odporúčam o ňu požiadať asi dva miesiace pred skončením záruky. V tom prípade, sa na prípadnú opravu ešte vzťahuje záruka vozidla, ale po termíne záruky, už len vaša peňaženka.       Doplnené  6.2.2008 12:49  Krátko po zverejnení tohto článku som bol upozornení na kategóriu M3 - Autobusy, u ktorej došlo k zmene vyhláškou 482/2007 Z. z. Čitateľa si iste spomenú na tragickú nehodu autobusu pri zjazde z Poľany, neďaleko Detvy.   Práve po tejto tragickej nehode bola sprísnená TK pre autobusy staršie ako 8 rokov, ktoré sa musia podrobiť TK každých 6 mesiacov, namiesto 1 krát ročne.   Konkrétny text novelizácie z 482/2007 Z. z.:  13. V § 47 ods. 1 sa za písmeno c) vkladá nové písmeno d), ktoré znie:  „d) kategórie M3 po prvom prihlásení do evidencie periodicky v jednoročných lehotách, po ôsmich rokoch od prvého prihlásenia do evidencie periodicky v polročných lehotách,“.  Doterajšie písmená d), e) a f) sa označujú ako písmená e), f) a g).   Z operatívne upozornenie ďakujem pracovníkom zo spoločnosti www.testek.sk . Som rád, že moje články sú po významovej stránke prísne kontrolované čitateľmi - odborníkmi.  Je to síce nepísaná, negarantovaná ale veľmi hodnotná záruka obsahovej správnosti, odbornosti a dôveryhodnosti jednotlivých článkov, za ktorú som veľmi vďačný. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Zneužívanie dopravného značenia a podnet na prokuratúru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Akou rýchlosťou sa smie a je vhodné ísť v danom mieste?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Poznáte dopravné značky súvisiace s parkovaním?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Darwinova cena za súkromné dopravné značenie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Drahovský 
                                        
                                            Aký vek a aké oprávnenie je potrebné na motorku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Drahovský
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Drahovský
            
         
        drahovsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracuje v oblasti informačných technológií a elektroniky.  Rád jazdí autom a venuje sa analýzam v doprave, najmä z pohľadu plynulosti a bezpečnosti.
  
Facebook
  
  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    1036
                
                
                    Celková karma
                    
                                                8.45
                    
                
                
                    Priemerná čítanosť
                    5398
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Doprava
                        
                     
                                     
                        
                            Rôzne
                        
                     
                                     
                        
                            Zoznam článkov
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            blog.sme.sk/*
                                     
                                                                             
                                            spravodaj.madaj.net
                                     
                                                                             
                                            4m.pilnik.sk
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            JASPI
                                     
                                                                             
                                            profivodic.sk
                                     
                                                                             
                                            Pripomienkové konanie
                                     
                                                                             
                                            GeoInformatika.sk
                                     
                                                                             
                                            OpenStreetMap
                                     
                                                                             
                                            osel.cz
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




