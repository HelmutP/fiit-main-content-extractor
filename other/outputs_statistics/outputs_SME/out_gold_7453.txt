

 O čo v krátkosti ide? 
 OLO malo nepotrebný pozemok, o ktorom predstavenstvo rozhodlo (nie na môj podnet!), že bude odpredaný. Pôvodne sa tu malo stavať recyklačné centrum, ale mestská časť Vajnory nedala k tomu súhlas a tak bolo recyklačné centrum postavené vedľa spaľovne vo Vlčom hrdle. 
 Bolo uverejnených sedem inzerátov (SME, Pravda, HN a Večerník) a celkovo sa zúčastnilo 18 firiem, medzi nimi aj FaxCopy a.s., v ktorej som mal vtedy 48 percentný podiel. 
 FaxCopy odovzdala najvyššiu ponuku a keby sa súťaže nezúčastnila, OLO a.s. by inkasovalo o 2 milióny korún menej. Toť vsjo. Koho zaujímajú detaily, nájde všetko na mojej stránke. 
 Jedna poznámka k Ficovému bludu, že "pozemok bol predaný pod cenu". To nebol ani náhodou, veď súťaž bola verejná (nie ako nástenkový tender, ale naozaj verejná). Zúčastnilo sa dosť firiem a ktokoľvek mohol ponúknuť akúkoľvek cenu a FaxCopy ponúkla najviac! Práveže dosiahnutá cena vyše 2600 Sk v roku 2005 bola reálna a trhová. 
 Koho zaujíma, čo som vlastne v OLO a.s. robil, môže sa pozrieť tu. 
 Čo to chcelo vlastne byť? 
 Fico zvolá mimoriadnu tlačovku kvôli päť rokov starému predaju pozemku a oduševnene rozpráva o niečom, čo bolo medializované v roku 2006 a čo sa odvtedy kompletne nachádza na mojej webstránke. Navyše, som pripravený zodpovedať akékoľvek otázky týkajúce sa predaja pozemku. 
 Mne z toho vyplýva, že smeráci musia byť hodne nervózni, keď kvôli mimoparlamentnej strane zvolajú mimoriadnu tlačovku. Zrejme im tečie do topánok. To aj chápem, lebo mať na krku nevysvetlených 284 miliónov by aj mňa robilo hodne nervóznym. Za to totiž hrozí niekoľko rokov basy. Preto je aj logické, že všetky strany, ktorým aspoň trochu záleží na ich povesti, sa od SMERu dištancujú. Ako prvá SaS, potom SDKÚ, po nej KDH a nakoniec MOST. 
 Celá vec je o to pikantnejšia, že bol to práve Fico, ktorý útočil na Dzurindu s oveľa menej závažnými obvineniami. Ako vieme, Dzurinda následne odstúpil z kandidátky. A keďže sa to celé stalo ešte pred odovzdaním kandidátnych listín (15. marec), je teraz líderkou Iveta Radičová. Keby sa Fico bol zdržal do 15. marca, mala by SDKÚ vážny problém, no takto jej práve naopak pomohol. 
 Je o mne známe, že Dzurindovi poviem, čo si myslím a že si myslím, že v politike je už pridlho. Ale Dzurinda môže právom nosiť titul najúspešnejší premiér Slovenska, zatiaľ čo Fico si zaslúži akurát tak titul najväčšieho babráka akého kedy Slovensko zažilo. Niet sa ani čo čudovať u človeka, ktorý nikdy nič poriadne v živote nerobil a vždy žil iba z cudzích peňazí. Preto Fico, ktorý čelí oveľa závažnejším podozreniam akým čelil Dzurinda, by mal z kandidátky  odstúpiť tiež. To sa samozrejme nestane, lebo by prišiel o imunitu a naozaj by mohol skončiť v base. 
 Potom, pán Fico, buďte aspoň natoľko chlap, že sa postavíte súperovi zoči voči. Keď sú ostrieľaní politici na Vás priveľa, skúste to aspoň so začiatočníkom ako som ja. 
 Preto Vás vyzývam, aby ste išli so mnou do televíznej diskusie. Vyberte si formát, miesto a čas. Poďme si na rovinu vydiskutovať, čo je transparentne kúpený pozemok od OLO a.s. oproti emisiám, najdrahším diaľniciam na svete, skorumpovaným Eurofondom a rekordnému zadlžovaniu. 
 Inak, obávam sa, budete pred celým národom za obyčajného zbabelca. 

