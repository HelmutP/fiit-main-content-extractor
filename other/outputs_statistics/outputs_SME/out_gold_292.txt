

 
Nechajme tradíciu tradíciou. Pri stavbe dreveného artikulárneho kostola v Kežmarku mohli okrem miestnych staviteľov pridať ruku k dielu aj spomenutí Švédi, ale je to málo pravdepodobné. No ale keď sme už pri tej Škandinávii, historicky je doložené, že keď sa slovenskí a nemeckí evanjelici z Kežmarku rozhodli postaviť si kostol, ich cesta smerovala aj k dánskemu a švédskemu kráľovi. A už som na pochybách. Nechám to radšej tak. V každom prípade, keď navštívite kostol, teta sprievodkyňa vám určite rada rozpovie aj o týchto námorníkoch. Aspoň pred niekoľkými rokmi v čase mojej univerzitnej exkurzie to bolo tak. 
 
 
 
 
 
Drevený artikulárny kostol 
 
 
 
 
 
Do 18-tisícového Kežmarku som sa prednedávnom dostal opäť. Vďaka rodičom. Ako som sa len potešil, keď sme sa v prvú novembrovú sobotu vybrali celá rodina do Tatier. Ako za starých čias. Boli sme zvedaví ako sa naše veľhory majú, či sa aspoň trochu pozviechali. No a cestou tam sme sa zastavili v tomto bývalom slobodnom kráľovskom meste. Zaparkovali sme neďaleko spomenutého kostola. Bolo to symbolické a každému odporúčam návštevu mestečka práve tu. 
 
 
Na prvý pohľad to nevyzerá, že by bol kostol drevený. Ale naozaj je. Údajne bol postavený bez jediného klinca. Fasáda je však omietnutá. Netreba sa dať pomýliť. V období protihabsburských stavovských povstaní, ktoré rozdelili Uhorsko do dvoch táborov, mohli mať protestanti v kráľovskom meste iba jeden kostol. Musel byť postavený mimo hradieb a z najlacnejšieho materiálu – v našich podmienkach teda z dreva. Kežmarčania si najprv postavili drevený kostol v rokoch 1687–1688. Ten však veriacim nestačil a tak ho roku 1717 prestavali do dnešnej podoby. Evanjelická cirkev v posledných rokoch vrazila do opravy kostola nemalé prostriedky a je sympatické, že popri mnohých donoroch prispela aj rímskokatolícka cirkev. 
 
 
 
 
 
 
 
 
Nový evanjelický kostol 
 
 
 
 
 
A keď už spomínam sympatické prejavy náboženskej tolerancie v súčasnosti, tak tá druhá súvisí s novým evanjelickým kostolom. Keď sme do neho pred rokmi na školskej exkurzii nahliadli, zbadali sme tam zopár tetušiek. Sprievodkyňa nám povedala, že tam majú bohoslužbu gréckokatolíci. Paráda! No a parádna je aj samotná stavba viedenského architekta Theofila Hansena z konca 19. storočia. Na prvý pohľad môže pripomínať mešitu. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Meštianske domy jeden pri druhom  
 
 
 
 
 
Ale poďme do mesta! Kežmarské uličky sú teraz v jeseni malebné. Vidno, že to bolo kráľovské mesto. Vzniklo na mieste starších slovenských osád a osady nemeckých kolonistov. Prvá písomná zmienka o meste je vraj v listine uhorského kráľa Bela IV. z roku 1251. Postupne získal Kežmarok mnohé mnohé privilégiá. Veľkú úlohu v ekonomickom rozvoji zohrávali najrôznejšie cechy. Dodnes po nich zostali celkom slušne zachované domy mešťanov. 
 
 
 
 
 
 
 
 
Renesančná zvonica rímskokatolíckeho kostola, napravo je stará škola 
 
 
 
 
 
 
 
 
Výzdoba renesančnej zvonice 
 
 
 
 
 
Pri potulkách starým Kežmarkom netreba obísť Baziliku sv. Kríža, gotickú stavbu z 15. storočia, ktorej veža už dostala renesančnú podobu. Pri kostole je typická renesančná zvonica zo 16. storočia, údajne vraj najstaršia na Slovensku. Je nádherná! Z ďalších zaujímavých stavieb upúta na Hlavnom námestí niekoľkokrát prestavaná radnica a tiež reduta, na Hradnom námestí barokový paulínsky kostol zo 17. storočia a najmä mestský hrad. Ten dala v 15. storočí postaviť rodina Zápoľských, ale v nasledujúcich dvoch storočiach boli pánmi hradu Tököliovci. Dnes slúži ako múzeum. 
 
 
 
 
 
 
 
 
Radnica 
 
 
 
 
 
 
 
 
Erb Kežmarku na budove radnice 
 
 
 
 
 
 
 
 
Mestský hrad 
 
 
 
 
 
 
 
 
Časť obvodového múru hradu 
 
 
 
 
 
 
 
 
Nápis nad vstupnou bránou do hradu 
 
 
 
 
 
          
 
 
V Kežmarku malo školstvo vysokú úroveň, študoval tu Hviezdoslav i bratia Petzvalovci  
 
 
 
 
 
No a ešte jedna chuťovka na záver – historická budova lýcea pri evanjelických kostoloch. Nachádza sa v ňom jedna z najväčších historických školských knižníc v strednej Európe so 150 tisíc zväzkami rôznych vedných odborov a v rôznych jazykoch. To miesto má svoje čaro a históriou to tam doslova dýcha. A keď už sme pri školstve, neviem, či ste vedeli, že v Kežmarku študovali viaceré známe slovenské osobnosti, okrem iných Šafárik, bratia Chalupkovci, Záborský, Hviezdoslav, Kukučín, Jesenský, Rázus, ale aj maliari Peter Bohúň a  Ladislav Medňanský či matematici bratia Petzvalovci. 
 
 
Mestečko Kežmarok odporúčam. Má toho na pozeranie naozaj dosť. A keby nestačilo, je z neho dobrý výhľad na pocukrované Tatry.  
 

