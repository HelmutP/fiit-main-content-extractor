
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivana Pajanková
                                        &gt;
                Ona
                     
                 Neopodstatnená! 

        
            
                                    16.6.2010
            o
            17:30
                        (upravené
                11.8.2010
                o
                20:27)
                        |
            Karma článku:
                22.16
            |
            Prečítané 
            13086-krát
                    
         
     
         
             

                 
                    Päť rokov o ničom. Už viac ako päť rokov, "žije" moja matka vo vegetatívnej kóme.
                 

                 Hodiny plynú,   práve tak dni, mesiace, roky;   zašlý čas sa niky nevráti,   a nemôžeme vedieť   ani to, čo bude.   (Cicero)   5 rokov   Za päť rokov som napríklad ja porodila dcéru, vypiplala ju a vystrojila prvý krát do škôlky. Dnes už  chystáme školské veci na september. Za päť rokov sa z môjho citlivého prítulného umravčaného syna stal mladý muž. Celý je zarastený, búcha dverami, odvrkuje na pol úst a tvrdí že tak to teenageri bežne robia a že my (rodičia) tomu prd rozumieme aké on má problémy.   Za päť rokov som stihla prísť o prácu, byť nezamestnanou, rekvalifikovať sa a začať pracovať v úplne  inej sfére. Za päť rokov sa zmenila aj moja rodinná situácia, kvôli tomu, že môj partner zo zdravotných dôvodov ostal na invalidnom dôchodku.   Keď sa obzriete späť, akých bolo vašich päť rokov? Čo bolo fajn a čo by ste chceli zmeniť, zvrátiť, napraviť? Je šanca, že stále môžete. Alebo nemusíte. Riadite si svoj meniaci sa život. Jednoducho žijete.   Moja mama neovplyvňuje už päť rokov nič. Za päť rokov ju niečie ruky prebalili cca 5.500 krát, niečie ruky ju kŕmia cez PEG priamo do žalúdka, odsávajú jej hlieny cez tracheostómiu, umývajú ju, češú, strihajú, krémujú, polohujú, kontrolujú, strážia.   .. pre silnejšie nátury, foto: otec kŕmi mamu..      Mama už viac ako päť rokov neprítomne pozerá do priestoru. A prečo?? Pretože sa v tejto nemocnici pri spoločnom obede pridusila kúskom mäsa. Pokým sa dočkala adekvátnej odbornej pomoci stihlo jej odumrieť neviem koľko mozgových buniek. Zrejme všetky. Je absolútne života neschopná. Mozog zomrel. Aj pacient zomrel.   Neopodstatnená   Tak reagoval na našu sťažnosť Úrad pre dohľad nad zdravotnou starostlivosťou. Všetko je v poriadku, veď sa zasa až tak veľa nestalo, bola to nehoda a naša sťažnosť je neopodstatnená.   Moja mama si toto nezaslúžila. To si nezaslúži nijaká mama. Nijaký človek. Ani ten najväčší hajzel.   Z nás všetkých sa s tým doteraz najťažšie vyrovnáva otec. Sedáva denne pri jej posteli a čaká na zázrak, na náznak poznania v jej očiach. Vypisuje listy, žiadosti a sťažnosti. Dovoláva sa pravdy a spravodlivosti. Pôvodne chcel počuť iba MRZÍ nás to, PREPÁČTE. Hoci to možno bola nehoda, chcel počuť BERIEME na seba svoj podiel zodpovednosti. PRIZNÁVAME sa. ZABEZPEČÍME. Pomôžeme. Ale nie, nikto nič. Na vine je Ona lebo nevie poriadne jesť. Áno presne to vyplýva z odpovedí inštitúcii na ktoré sa otec obrátil s prosbou o prešetrenie a teraz už aj s prosbou o pomoc, aby sme i naďalej zvládali financovať jej pobyt v súkromnom zariadení. Veru v súkromnom. Priamo "v tom mojom". Nikde inde ju nechceli v jej stave prijať. V nijakom štátnom zariadení. Pretože štát nemá záujem starať sa o takýchto neperspektívnych a neproduktívnych ľudí. Na matkin pobyt mesačne prispieva časťou zdrav. poisťovňa, mama dôchodkom, otec dôchodkom, prispieva sestra aj ja. Je to pre nás ťažké nielen psychicky, ale aj finančne.   Ja napr. dávam z 500 eurového zárobku sumu 150 eur, 300 dávam na vlastnú domácnosť a 50 mi ostane na zmrzlinu. Môžem si dať skoro 4 kopčeky denne.   Otec žiadal o pomoc v hmotnej núdzi. Úrad práce, soc. vecí a rod. mu okrem iného odpísal: "Žiadateľ a spoločne posudzovaná osoba nie sú v hmotnej núdzi, pretože ich príjmy nie sú pod hranicou životného minima." To je fakt, sú niečo nad. Za normálnych okolností, by ani nepípli. Ale čo v prípade, keď platíme za ústavné zariadenie mesačne sumu 750,- Eur?  A ich dva dôchodky dokopy sú vo výške 682,- Eur? A aby sme nezabudli aj otec musí trochu jesť. Aj keď vraví, že už toho veľa nepotrebuje.   Po ďalších žiadostiach sme dostali priamo z rozpočtu Ministerstva práce, soc. vecí a rodiny SR, jednorazový príspevok vo výške 200 Eur. Vďaka aj za to pani ministerka T.   Prosím, dávajte si pozor a jedzte opatrne, aby ste sa pre svoju krajinu nestali neopodstatnenými, alebo inými slovami bezpredmetnými, bezúčelnými, zbytočnými....             

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (63)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Pajanková 
                                        
                                            Diagnóza povolanie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Pajanková 
                                        
                                            spomienkovy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Pajanková 
                                        
                                            SOI-ku na nich!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Pajanková 
                                        
                                            Bankár
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivana Pajanková 
                                        
                                            odlahčene
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivana Pajanková
                        
                     
            
                     
                         Ďalšie články z rubriky súkromné 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marek Strapko 
                                        
                                            Najbližší mi boli tí "kakaoví"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Mária Malinová 
                                        
                                            Sexuálna výchova na Slovensku neexistuje a nikdy neexistovala
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Helena Smihulova Laucikova 
                                        
                                            Mágia kina
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Dušan Koniar 
                                        
                                            Tri esemesky z minulosti hľadajú svoju budúcnosť
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Zuzana Gerhátová 
                                        
                                            ako ma upratal sedemročný chlapec
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky súkromné
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivana Pajanková
            
         
        pajankova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Som "pozitívna" (v optimistickom zmysle). Chcela by som byť - spravodlivá. Niekedy šijem veci horúcou ihlou a ony sú potom zle ušité. 
 
Od novembra 2009 pracujem ako ošetrovateľka v hospici. 

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    108
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3960
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Popri práci zdrav.asistentky
                        
                     
                                     
                        
                            Zo zápisníka bežnej sanitárky
                        
                     
                                     
                        
                            čokoľvek
                        
                     
                                     
                        
                            Ona
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            http://blog.sme.sk/
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Aj tu som doma
                                     
                                                                             
                                            Martinus
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bratislava trochu inak
                     
                                                         
                       Dnu
                     
                                                         
                       Múzeum ošetrovatelstva v Kodani, u nás živý skanzem
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




