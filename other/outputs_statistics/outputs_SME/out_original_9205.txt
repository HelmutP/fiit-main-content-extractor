
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Silvia Bohúnová
                                        &gt;
                Nezaradené
                     
                 Krajina Zázrakov 

        
            
                                    19.6.2010
            o
            15:54
                        (upravené
                19.6.2010
                o
                16:33)
                        |
            Karma článku:
                3.50
            |
            Prečítané 
            598-krát
                    
         
     
         
             

                 
                    Konečne mám chvíľku pre seba.Opúšťam byt.Asfaltový chodník ma dovedie k obľúbenému miestu ktoré som nazvala ,,u anjela,,
                 

                 
  
   Cestou premýšľam nad tým čo som vybavila,čo som ešte nevybavila a na čo sa asi úplne vykašlem.Sadám si na lavičku,obzerám sa,nikde nikoho.Odrazu ma však napadne,že som dávno nebola v lese.Obzriem sa za seba smerom k ceste ktorá vedie priamo do lesa.Neváham ani minútu a už kráčam spomínanou cestou.Úzka cesta ma doviedla na najbližší kopec.Výhľad ktorý sa mi naskytol z kopca vyvolal vo mne pocit,že sa nachádzam stovky kilometrov od domova,od ľudí.   Moj život plynul vo vychodených koľajách bežnej rutiny a v tú chvíľu tam na tom kopci prehovorila ku mne túžba po vzrušení,po zmene.Mračná sa tiahli nízko na nebi aby sa mohli usadiť priamo nad lesom.Potom čo som vyšla o čosi vyššie,sa mi naskytla brána vytvorená z kríkov a z konárov stromov siahajúcich až k zemi.Nebola to obyčajná brána.Bola to pozvánka do lesa.A tak som vošla.Les mi pomáhal stratiť sa z dohľadu civilizácie.Vzduch sa ochladil a netrvalo dlho a ja som stratila akúkoľvek predstavu o smere.Začala som sa cítiť zvláštne dezorientovaná.Stále som však mala potrebu ísť po úzkom lesnom chodníčku,ktorý končil nevedno kde.Ako som tak kráčala,napadlo ma že aj v živote je to podobné.Aj v živote kráčame,cestami,cestičkami,chodníkmi ktorých cieľ nepoznáme.Nevieme ani to čo nás na tých cestách postretne.Sme cestami ale sme aj pútnikmi čo po tej ceste kráčajú v ,,Krajine Zázrakov,, Tam je dovolené všetko a nič tam nemusíme.Záleží iba na nás aké dary si zo svojich ciest prinesieme.   Vnútorný pocit ma vyviedol z tmavého lesa.Zastala som.Predo mnou sa rozprestierala nádherná zelená lúka.Zeleň bola popretkávaná drobnými farebnými kvietkami ktoré sa mihali v slabom vánku.Mračná sa poponáhľali aby slnko dokončilo tú nádheru.Neviem ako dlho som tam stála a dívala sa na ten obraz.Bola som však vyrovnaná a blažená.   Keď som prišla domov,bola už takmer tma.Bola som unavená a hladná.No prechádzka ,,Krajinou Zázrakov,, zostane pre moju dušu - cestu,orientačným bodom a vždy keď budem rozladená z každodenných starostí,spomeniem si na obraz ktorý pre mňa namaľovala ,,Krajina Zázrakov,, :-) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Bohúnová 
                                        
                                            Aj ženy majú milencov.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Bohúnová 
                                        
                                            Diskusia.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Bohúnová 
                                        
                                            Máš nesympatickú ŠPZ-ku? Dostaneš papučku!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Bohúnová 
                                        
                                            Supermarketové priateľstvo.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Bohúnová 
                                        
                                            Pssst.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Silvia Bohúnová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Silvia Bohúnová
            
         
        bohunova.blog.sme.sk (rss)
         
                                     
     
        Mám všetko..túto chvíľu a tak som sa vydala na cestu po svojich a bezstarostne.Zdravá,slobodná,svet majúc pred sebou..Tá dlhá cesta ma povedie kam len budem chcieť :-)

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    11
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1199
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




