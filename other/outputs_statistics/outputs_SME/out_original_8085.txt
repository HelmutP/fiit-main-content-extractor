
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Magda Kotulová
                                        &gt;
                Nezaradené
                     
                 Červený klinček 

        
            
                                    5.6.2010
            o
            7:48
                        |
            Karma článku:
                7.63
            |
            Prečítané 
            946-krát
                    
         
     
         
             

                 
                    Odchádzala som s nákupom z Tesca. Vtom som skoro vrazila s ťažkým transportom tlačeným pred sebou o podstavec-pult, kde trónilo množstvo kvetov, kytíc, jedna krajšia od druhej. Potlačila som nákupný vozík k vôni, ktorá sa z tohto kútika ťahala až k môjmu nošteku. Ružičky, klinčeky, gerbery a množstvá iných pestrofarebných, dráždivých, jasavých kvietkov vábili.
                 

                 Zastala som. Nadýchla som sa prenikavej vône živých kvetín. Zaváhala som, či začať s výberom. Mrkla som sa na cenu. Cena nebola až taká hrozivá. Ruka mi nechtiac vhupla na prvú kyticu drobných, čajových ružičiek. Vložila som ju však rýchlo nazad do nádoby s vodou. Nie, tie nie... Krásne sú... Čarovné, neprekonateľné... Ale skoro uvädnú...  Ale čo tieto tu: klinčeky, drobné kvietky, ktorých vôňa umára, len čo som sa k nim  sklonila... Klinčeky?.. Komunistické karafiáty?... V žiadnom prípade!... Už dlhé roky som ich nekupovala. Prejedla som sa ich za komunizmu, keď tento prekrásny, zvodný, voňavý kvet výhradne kraľoval v chudobných socialistických kvetinárstvach.   Váhala som. Vybrala som jeden zväzok bordových drobných klinčekov, ktoré preukrutne rozvoniavali. Vybrala som ešte jeden zväzok. Však sa odlišujú od tých komunistických červených, velikánskych, „stachanovských hrebíčkov“. Vložila som ich navrch plného vozíka, aby sa mi nebodaj, nepomliaždili. Čím som sa blížila viac k pokladni, tým som bola z nákupu kytičiek spokojnejšia.   Keď som sa po zaplatení poberala smerom k autu, naložila náklad-nákup na celý nasledujúci týždeň do kufra vozidla, s nehou som položila zamatové klinčeky do vnútra, na okno auta. Posadila som sa do voza, pripútala a rozbehla autíčko. Celou cestou domov mi brnkalo v hlave konečné rozhodnutie: oddnes kupujem, už nesocialistické klinčeky. Však sú jemné, nežné, voňavé, dráždivé a lacné.   Vo vázičke doma vyzneli prekrásne. Chodila som sa na ne z času na čas pozrieť. Dráždili ma. Svojou skromnou, ale očarujúcou tajomnosťou. Ako dráždil kedysi socializmus – komunizmus a jeho kvety, červené klinčeky -  občanov. Ale v tom negatívnom zmysle. Ten nebol tajomný. Práve naopak: prejavil sa svojou otvorenou brutálnosťou. Nebol ani skromný. Žiadal stále viac a viac celých občanov, bez zvyšku. Ich telá i duše. Telá ničil otrockou nezaplatenou drinou a duše magorením prázdnymi, neobohacujúcimi nezmyslami.   Dajme si pozor, aby nám tí, čo nás opätovne, a až doposiaľ, obdaruvávajú červenými klincami – nepriviedli k dobe, ktorú už nechceme, aby sa niekedy vrátila...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (20)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            U kaderníčky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Oprstienkovaná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Daniel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Keď zakape mobil
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Magda Kotulová 
                                        
                                            Registrácia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Magda Kotulová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Magda Kotulová
            
         
        magdakotulova.blog.sme.sk (rss)
         
                        VIP
                             
     
         Som mama štyroch detí, stará mama pätnástich vnúčat a dvoch pravnúčat. Príležitostná publicistka. Občas sa "niečo" pokúsim napísať, keď ma čosi nahnevá alebo urobí spokojnou. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    318
                
                
                    Celková karma
                    
                                                10.84
                    
                
                
                    Priemerná čítanosť
                    1470
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kódex blogera na blog.sme.sk (aktualizovaný 10. 7. 2012)
                     
                                                         
                       Ocko náš
                     
                                                         
                       Príjemné výročia si treba  pripomínať
                     
                                                         
                       Lezúň
                     
                                                         
                       Garancia vrátenia peňazí. To určite!
                     
                                                         
                       Feťáčkina matka
                     
                                                         
                       Vôňa ženy
                     
                                                         
                       Vodopády Slunj
                     
                                                         
                       Mladý, nervózny vodič a starká
                     
                                                         
                       Nikdy, nikdy, nikdy sa nevzdávaj
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




