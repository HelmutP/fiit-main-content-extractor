
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matúš Krátky
                                        &gt;
                Škola
                     
                 Nekompetentní hľadali nekompetentných 

        
            
                                    6.6.2010
            o
            10:43
                        (upravené
                11.6.2010
                o
                11:07)
                        |
            Karma článku:
                11.38
            |
            Prečítané 
            1248-krát
                    
         
     
         
             

                 
                    Pred pár mesiacmi našu školu navštívila inšpekcia. Aby sa presvedčila, či je všetko v poriadku. Po pár mesiacoch si na ňu opäť raz spomínam. V dobrom určite nie.
                 

                     Na hospitácie sme pomerne zvyknutí. Nebýva ich síce mnoho, väčšinou jedna alebo dve za rok, no nikdy na nich nebol väčší problém. Aj keď nášmu riaditeľovi sa pár veci nepáčilo, veľmi kritický nebol.   Lenže inšpekcia už kritická mohla byť, a tak týždeň pred ňou bola všade nervozita. Všetci chceli, aby naša škola vyzerala dobre.   Na chodbách vymenili osvetlenie, pribudli nové lavičky a pod. V pondelok bolo všetko pripravené na príchod delegácie. Možno až na samotné učiteľky, ktoré neboli vo svojej koži. Teda po väčšinou.   Niektoré si z toho hlavu nerobili, no niektoré na tom boli veľmi zle. Počas výkladu hovorili nesúvisle, miesto hodiny to bol organizovaný chaos...   Toto som však nechcel povedať. Chcel som sa vyjadriť ku záverom inšpekcie. Pýtam sa vás: „Myslíte si, že 33 členná trieda bude pri skupinovej práci ( práca cca 5 skupín) úplne ticho?“ Osobne si myslím, že nie. Šesť ľudí sa nemôže radiť a dohadovať šeptom. Je to nereálne, keďže máme vyvodiť nejaké závery našej práce, t.j. dohodnúť sa na správnych odpovediach na niekoľko otázok. Nedá sa byť jednoducho ticho, keby tie otázky robil každý sám, možno by sa to dalo. Ale nie vo veľkých skupinách.   Inšpekcia vyčítala aj to, že študenti nevyužívajú IT technológie pri výučbe. Podľa jednej asi 60-ročnej „odborníčky“ by mal riaditeľ kúpiť počítače do školy každému študentovi. Dotyčná pani by mohla prestať pozerať lacné americké sci-fi filmy, pretože na Slovensku toto nie je na žiadnej škole. Teda myslím si to, pretože väčšina riaditeľov má problém opraviť obyčajný vchod do školy a nie ešte aby nakúpil každému žiakovi notebook. Lenže pani, ktorá dvadsať rokov neučí môže len ťažko hodnotiť učiteľov v praxi. Bo učiteľstvo by nemalo byť iba podľa príručky, každý učiteľ by mal do tej svojej hodiny vložiť niečo čo mu je vlastné.   „Žiačka si na hodine nepísala poznámky“ – Toto si do notesu naškrabala jedna inštruktorka. V prvom rade to nebola žiačka, no žiak s dlhými vlasmi. No a pokiaľ ja viem, nikde nie je presne napísaná povinnosť, že žiaci si musia písať poznámky. Áno mali by, od prvej triedy sme si predsa poznámky písali. Ale pokiaľ si niekto niečo nepíše, je to na jeho škodu. A rozhodne za to nemôže učiteľka, ktorej to kontrola vyčítala.   Na záver by som povedal rozhodne jednu vec. Áno viem, že takéto kontroly majú len strašiť, pretože reálne toho veľa nemôžu urobiť, ale nemuseli by dotyční kontrolóri buzerovať za každú cenu. Potom je jasné, že kontrola dopadne zle. Podľa mňa by kontroly mali robiť tí, ktorí priamo ešte stále pôsobia v školstve ako učitelia. Pretože tí presne vedia, čo najviac môžu očakávať od učiteľov a od školy. Nevravím, že takto vyzerajú všetky inšpekcie, no tá na našej škole bola presne takáto.        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (16)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Krátky 
                                        
                                            Memorandum a autonómia ako slepá ulica slovenskej politiky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Krátky 
                                        
                                            Číta sa dnes málo kníh?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Krátky 
                                        
                                            Slovenský futbal čaká zmena
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Krátky 
                                        
                                            Učebníc je veľa a nestoja za nič
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Krátky 
                                        
                                            Nominácia veľmi neprekvapila, sila sa ukáže neskôr
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matúš Krátky
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matúš Krátky
            
         
        kratky.blog.sme.sk (rss)
         
                                     
     
         
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    6
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1030
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            História
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Memorandum a autonómia ako slepá ulica slovenskej politiky
                     
                                                         
                       Číta sa dnes málo kníh?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




