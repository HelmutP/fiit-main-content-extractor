

   
 V pamäti mám aj požiarnika Sama :-) a ostatných mužov činu, ktorí starostlivo chránili svoje mestečko pred zločincami. Ako to už v rozprávkach býva, realita je tam úplne naopak (síce ani dnešné telenovely od nich ďaleko neutekajú), no teraz mám namysli realitu v našich uliciach, konkrétne slovenských „mužov činu"- pánov policajtov. 
   
 Len čo vykuknú prvé slnečné lúče, spolu s nimi ruka v ruke vykúkajú spoza zelených kríkov tmavo zelené postavičky. Jeden z nich, samozrejme ten aktívnejší, s úsmevom popod fúzy a za pomoci šarmantnej asistentky „paličky", zvoláva nič netušiacich vodičov na krátke posedenie. Istotou však ostáva, že za toto hoci nie vždy príjemné stretnutie musíte zaplatiť aj tak vy! 
   
 Oceňujem ich starostlivosť o bezpečnosť nás chodcov. To sa ráta. Avšak čo s istou nemenovanou frekventovanou oblasťou, kde sú vodiči natoľko ohľaduplní a pozorní, že sa pánom policajtom nepodarilo vyzbierať ani mizerný cent? Vtedy zaúraduje ešte vyššia moc, ktorá obviní týchto poctivých mužov činu zo skorumpovanosti. Namiesto pochvaly a radosti, že aspoň niektorí vodiči si vstúpili do svedomia, si musia nájsť nové ešte viac frekventované miestečko za zelenými kríkmi, kde sa euríky len tak pohrnú... 
   
 A tak namiesto toho, aby sme sa pri pohľade na zelenú policajnú fábiju tešili, že je tu niekto kto nás chráni a sme v bezpečí, schovávame sa, lebo vieme, že každé stretnutie zoči- voči niečo stojí... prinajmenšom poznámku v ich zápisníku. 
   
 Aspoňže v detských rozprávkach sú policajti mužmi činu a vždy na správnom mieste. Každý tretí chlapec chce byť policajtom...bodaj by! Možno sa zrodí nová generácia...Dosť. Myslím, že sa treba vrátiť naspäť do reality, kde policajt znamená- strach, pokutu, alebo Nič 
   

