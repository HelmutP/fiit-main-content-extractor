
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stanislav Krištofík
                                        &gt;
                Súkromné
                     
                 Stretnutie pred kamerami v STV a Markíze 

        
            
                                    20.6.2010
            o
            16:27
                        |
            Karma článku:
                9.58
            |
            Prečítané 
            1416-krát
                    
         
     
         
             

                 
                      Priznám sa, mám radšej, ak môžem vidieť a aj počuť, čo si prečítam síce v korektnom  printovom médiu akceptujem, ale nemám ten dokonale vyvážený pôžitok jednoty duše i tela! Jasné, je to akurát v tej dokonalosti zážitku, lebo ak vidíte človeka klamať, zavádzať, spochybňovať či podceňovať, ak si uvedomujete jeho nervozitu, nonverbálne reakcie signalizujúce paniku či nesúhlas, hneď je vám všetko pochopiteľnejšie i uveriteľnejšie. Práve preto vydržím pri televíznej obrazovke aj vtedy, ak je na nej niečo tak programovo nepríťažlivé ako je politická relácia s aktuálnou témou a naviac, poteší ma, ak o tom môžem poslať rešerš či glosu tým, pre ktorých to jednoducho nie je nič viac než nuda. Rozumiem tomu, je v tom nedočkavosť, mnohokrát odmietanie konkrétnej osoby či témy, mnohí z nás majú politickú problematiku doslova „na háku“, ale ak to majú s vysvetlivkami či priamo s návodom, čo sa stalo, ako to prebiehalo a komu sa darilo či nedarilo, potom sa to už dá. Je to pre mňa nedeľná hodinka, pre ktorú celkom rád obetujem zvyčajnú pohodu obeda s tým, že aj tak nič nezmeškám, jedlo sa dá dobehnúť.
                 

                     Tá originálna atmosféra a možnosť voľajakej senzácie už nie a to veru nie je ono - ani pri zázname! Aj dnes to bolo presne tak. Už od včera som vedel, že v STV bude Pavol Hrušovský z KDH a Marek Maďarič, podpredseda strany SMER  a ešte stále minister kultúry slovenskej vlády, pravá ruka a najčastejší reprezentant premiéra Fica pred kamerami, ak sa jedná o kontaktnú reláciu. Tým samozrejme myslím stretnutie s voľakým z opozičného tábora a vždy si zatipujem – bude to Kaliňák či Maďarič a akurát ten býva víťazom. Aby ste mi dobre porozumeli, to nehovorím o víťazstve v tom dueli na obrazovke, to naozaj nie, ale naozaj iba o tom, že je častým hovorcom v tom najpresvedčivejšom slova zmysle. Vie si svojho predsedu i premiéra chrániť ako oko v hlave, nedopustí, zabojuje, ironizuje a často aj kamufluje či zatĺka a spodobuje, ale veď práve preto to robí tak často, no nie?!   A Pavol Hrušovský, ako stará škola, tak trošku fádny, ale neohrozený diskutér, s veľkými skúsenosťami a právnickými fintami v talóne je dobrý súper, vždy a za každých okolností a aj keď posledné roky ťahával za kratší koniec, lebo za sebou nemal argumenty výkonnej moci, nikdy to nevzdával a bojoval vždy rytiersky. Tešil som sa teda a nesklamal som sa! Dôvod je naporúdzi – tak trošku sa zmenili pravidlá hry, ihrisko a veru aj tromfy v ruke diskutujúcich.   Bolo mi ľúto Maďariča akurát na samom začiatku relácie, keď bola reč o povinnosti Slovenska vyjadriť sa k záchrannému valu EÚ a svojimi argumentami mal navodiť atmosféru nástojčivosti takejto právnej nevyhnutnosti. Štyri pravicové strany čo chcú zostaviť vládu premiér vyzval rokovať a oni nechcú. Tú dohodu musíme podpísať, ináč bude Slovensko... , domyslite si, čo všetko Slovensko bude a zodpovednosť tak padá na ne!   Nuž nepochodil a bolo mi úplne jasné, prečo je tu Maďarič a nie Fico, lebo kým práve on tieto slová rozprával v televízii i v rozhlase po návrate z Bruselu sólovo, protiargumenty by ho celkom detronizovali. Je premiér premiérom – je, má právomoci – má , zastupuje túto vládu a túto republiku – zastupuje – tak nech rozhodne! Sme iba štyri politické strany, čo sa chcú dohodnúť – sme, máme mandát vládnuť – nemáme, môžeme za túto republiku rozhodovať – nemôžeme  a naviac, radil sa niekto niekedy s nami ako to vidíme a chceme riešiť – neradil, takže čo sa od nás chce!? A akosi naviac si takúto „krížovú cestu“ vybral sám, veď už dávno mohol povedať prezidentovi, že sa s ním nikto z tých štyroch nechce a nemá o čom rozprávať, tak nech sa nesťažuje, že sa márni čas! A veru aj od prezidenta je to nezodpovedné, čas falošných  nádejí a ilúzií už je dávno preč!   Arogancia moci, čo sa prejavila v konaní Ficovej vlády naozaj vždy, keď sa dala prezentovať moc dostala výchovné zaucho a hoci to Maďarič odmietol bolestne ironickým úškrnom, kľud už do konca kruto prežívanej hodinky nenašiel. Bude mi ešte dlho znieť v ušiach ako Maďarič zvolával pred kamery všetky bohyne sveta so špecializáciou pre spravodlivosť, lebo neustále a dookola pripomínal, že čo je to za krivdu, keď SMER získal o 20% hlasov viac ako strana na druhom mieste a nikto s ním ani nechce rokovať! To je neslušnosť. SMER by nemal byť mimo, je to predsa len jasný víťaz! A ešte niečo, prečo nemá SMER delegáciu na miesto predsedu parlamentu, veď sa k tomu hlásime, chceme byť ako víťazi „spíkri“, je to predsa demokratická norma a až keď Hrušovský pokojne veci vysvetlil, čo je zvyk a pravidlo u nás, mohlo sa pokračovať.   Ešte nikdy tak zúfalstvo z prehratých volieb z úst voľakoho z tejto strany nezaznelo tak úprimne a tak zúfalo. Čo je to za demokracia? A tak mu musel Hrušovský pripomenúť, aké sú naozaj tie parlamentné počty, ako to v priebehu štyroch rokov svojej vlády realizoval SMER a ako je zbytočné spomínať na to akurát teraz, keď sa napríklad aj ten problém s pomocou Grécku či euru mohol prebrať už viac ako pred dvomi mesiacmi a premiér by mal, alebo nemal mandát zmluvy aj takéhoto významu podpísať s garanciou. Aj v tejto neľahkej pozícii, čo si za pomoci Gašparoviča aj tak zapríčinil sám. Ba zachytil som aj jeden odkaz o novej atmosfére v novozvolenom parlamente – mala by byť úprimná a mal by tam vládnuť duch súťaživosti a nie nenávisti, ako je to teraz, no úprimne povedané, Maďarič to neakceptoval. Radšej pripomenul, že vo vzduchu je ešte konflikt, lebo sa uvidí, ako to bude s rokovaním o Vatikánskej dohode, ktorá nikomu inému okrem KDH nevonia, veď uvidíme! A nebojte sa, tie reči, že Fico nebude v parlamente, lebo to už povedal a vážne i s vysvetlivkami vlastne deklaroval, to nie je realita! Budeme opozícia, čestná a dôstojná. Aj to bola zmena, lebo premiér akurát tieto slová povedal presne tak ako vyhrážku – veď uvidíte!      Na škodu veci STV prenechala záverečné slová politológovi Rastislavovi Tóthovi, ktorý nepovedal nič podstatné, snáď iba pokazil dojem z prezentácie Pavla Hrušovského, ktorý celkom jednoznačne vysvetlil všetko, čo sa dotýkalo rokovaní o postupe budúcej vládnej koalície. Trúfol si odkázať Radičovej, aby si nedovolila v Bruseli povedať to, čo už na našej scéne odznelo, že sa nám politika pôžičiek nezodpovedným nepáči! Hrušovský a vlastne všetci účastníci týchto rokovaní už dávno spoločne oznámili, že hovoriť, čo a ako bude sa dozvieme až keď bude skalopevná dohoda vo všetkých bodoch programu. Keď by som ho mal poruke, tak ho prinajmenšom okríknem, bol ako slon v porceláne! V priebehu chvíľky porozbíjal celý výklad!   Na Markíze to bolo celkom ináč – fádne, ale empaticky a perspektívne. Puškárová si posedela s Bugárom a s Miklošom a potom v druhej zostave s Figeľom a Sulíkom. Hovorili presne o tom, čo povedal aj Hrušovský a čo ten politologický expert svojim spôsobom celkom detronizoval a bolo cítiť, že moderátorke chýba vždy prítomné napätie a hroziaci konflikt. Tak sám pre seba a možno aj za veľa z vás som prítomným zaprial, aby im to vydržalo, lebo aj tak to nebude iba jednoduché. V tomto štáte, hoci to nie je ten naozaj „dánsky“, je aj tak mnohé zhnité, vôbec nie na poriadku a už vôbec nie iba v pohode. Stretnutie pred kamerami je teda za mnou, bolo iné než inokedy a už sa teším, keď sa prvý raz stretneme v zložení opozičník proti koaličníkovi, je to tak predsa len väčšie dobrodružstvo.       . 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (39)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Tak akí sú to napokon ministri?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Prekvapujúci advokát ex offo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Slovensko je krajina úspešná, v každom ohľade a rozmere!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            Ľady sa pohli
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stanislav Krištofík 
                                        
                                            O úplatkoch, iba pravdu!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stanislav Krištofík
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stanislav Krištofík
            
         
        kristofik.blog.sme.sk (rss)
         
                                     
     
        Vyskusal som si v zivote tolko dobreho i zleho, ze mi moze zavidiet aj Hrabal. Vzdy som zostal optimistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    2342
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    607
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Tak akí sú to napokon ministri?
                     
                                                         
                       Prekvapujúci advokát ex offo
                     
                                                         
                       Slovensko je krajina úspešná, v každom ohľade a rozmere!
                     
                                                         
                       Post scriptum
                     
                                                         
                       Seniori a digitálna ponuka s internetom
                     
                                                         
                       Ľady sa pohli
                     
                                                         
                       O úplatkoch, iba pravdu!
                     
                                                         
                       A zasa som sa potešil
                     
                                                         
                       Naša diplomacia je kreatívna
                     
                                                         
                       Nedalo mi čušať!
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




