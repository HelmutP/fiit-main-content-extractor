

 Zápletka je nasledovná: Gróf XY zúfalo miluje vojvodkyňu Olíviu, ktorá ale jeho lásku neopätuje, lebo smúti za svojim zosnulým bratom. Šľachtičná ZW stroskotá s loďou, pričom sa domnieva, že jediná prežila. Dozvie sa o grófovi, do ktorého sa ihneď zamiluje, lebo je zasiahnutá zamilovaným šípom dvorného šaša/blázna Roba Rotha, ktorý vo voľnom čase pôsobí aj ako Cupid. Prezlečie sa teda za muža, poberie meno Cesario a zamestná sa na dvore grófa XY, aby k nemu bola bližšie, no ten ju pošle dvoriť v jeho mene vojvodkyni Olívii, ktorú tiež zasiahne šíp šaša/blázna a zamiluje sa do Cesaria/šľachtičnej mysliac si, že je muž. Medzitým niekde inde: brat šľachtičnej menom Sebastian sa prekvapivo neutopil, a zachránil ho... hm, nejaký muž. Ten sa doňho zamiluje, lebo ho tiež omylom trafil šíp šaša/blázna/amora. Neskôr do neďalekého jazera havaruje kozmická loď, z ktorej unikne zákerný organizmus zvaný Xenox, ktorý je schopný morfovať do akejkoľvek podoby a.... aha, to už tam nebolo, to som asi videl inde. Skrátka, to nevymyslíš, brácho, to je Shakespeare. 
   
 Ono väčšina zápletiek divadelných veselohier je v zásade takých, že ona chce hentoho, henten chce inú a ten tretí si niekoho pomýlil a chce vlastne štvrtého. Takže tam problém nevidím. Lenže niekde medzi tým, ako Shakespeare Večer trojkráľový napísal a tým, ako ho včera uviedli v SND sa stala nejaká zákerná kozmická náhoda, ktorá spôsobila, že si niekto povedal, že klasický Shakespeare je už pre dnešného divadelného diváka príliš nudné sústo, tak to treba ozvláštniť a aktualizovať na dnešné pomery. Júúúj, to bol nápad. 
   
 Takže začína to klasickým pateticko-lyrickým veršovaným monológom Vajdu, zatiaľ všetko v poriadku. Potom, čo sa Mórová prezlečie za chlapa a začne sa hrať na transsexuála, to začína byť veľmi... hm... zvláštne. Od tohto momentu veškerý humor spočíva v týchto elementoch: 
 - chytanie sa za gule (chápete, akože Mórová sa chytá za gule, akože žiadne nemá, lebo je iba akože chlap, chápete... ), ako možno prvý raz to aj bolo vtipné, ale keď to spravila asi po x-tý krát, už človek nevládze ani zívnuť; 
 - fascinujúci pohybový humor ako z čias Charlieho Chaplina - ľudia sa tam hádžu o zem, chodia ako opice a znovu sa hádžu o zem, hm.. haha; 
 - úžasné sureálne slovné hračky, niekedy až dadaistické, alebo časté opakovanie rovnakých slov (ok, beriem, tu sa niekedy zadarilo); 
 - vulgarizmy - strašne rád by som si prečítal originálne Shakespearovo dielo, lebo ma zaujíma ako sa v staroangličtine povie „skurvené prachy", „zdrbem sa z rebríka" alebo „kokot" a či už v stredovekom Anglicku poznali gesto s vystrčeným prostredníkom. 
   
 Teda, chvíľu som si myslel, že chyba je vo mne a hovoril som si „Aha, všetci sa smejú a tlieskajú. Tak sa bav, ty kretén. Bav sa!". Ale nie, nedalo sa. Naozaj som mal chvíľami pocit, že sledujem nejaký nepodarený diel Monty Python´s Flying Circus, teda nie ten dobrý skeč, napríklad ako John Cleese nakupuje syr, ale asi ako by Flying Circus dostali na starosti tvorcovia Susedov. A keď Mórová doslova zajebala, že „Ty rozumíš, čo ja chápem?", tak som si vytiahol lístok a skontroloval, či som sa nejakým omylom svojim alebo Ticketportalu nedostal na predstavenie English is easy. Nedostal. Už som len čakal kedy niekto začne hovoriť s maďarským prízvukom, keďže iný veľmi populárny slovenský humoristický prvok - prezliekanie sa za opačné pohlavie, už bol zastúpený vrchovato. 
   
 Na druhej strane, herci boli fajn, najmä Horváth ml. a Dančiak. Akurát pri ňom som mal veľmi zmiešané pocity, keď ho chudáka museli držať a viesť po javisku kvôli jeho slepote. Ale bolo vidieť, že si inak hranie užíva. Je fajn, že aspoň niekto si to užil. Teda, rozumiete mi, sú rôzne typy divadelnej nudy, ako napríklad tá „No nič moc, ale hodinku už vydržím" a potom ten zožieravý deštruktívny typ nudy, kedy máte chuť odtrhnúť si vlastnú končatinu, hodiť ju na javisko a vo vzniknutom zmätku utekať kade ľahšie. Lebo som zabudol napísať, že tento masterpiece trval tri hodiny. No hej, šak čo to sú tri hodiny v živote galaxie. 
 Takže vzhľadom na vyššie uvedené nemôžem udeliť viac ako tri zdvihnuté prostredníky z desiatich a milovníci divadla po mne môžu hádzať aj zľavnené vstupenky 1+1, kúpené na ulici od chlapíka zo Save maxu. Mfp. 
 Oznam pre rýpalov: 
 - áno, viem, že Shakespeare je klasika a je mi to jedno, 
 - nula divadelných hier som zatiaľ vytvoril, 
 - nesúhlasím s tým, že som to nepochopil, 
 - áno, viem, že Shakespeare je klasika a je mi to jedno, 
 - áno, viem, že to tu už raz mám. 

