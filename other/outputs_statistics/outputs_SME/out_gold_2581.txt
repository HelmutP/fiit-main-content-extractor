

   
 Pamätám si ako som sa učil na strednej škole o romantizme ako literárnom smere. Keďže nie je verejným tajomstvom, že ja som zasnívaná duša, je pochopiteľné, že sa mi romantizmus páčil. Ono to malo niečo spoločné s takým bojom, takým nenásilným a milým. Vlastne aj Don Quijote bol romantik, zasnívaný, iracionálny a všetko si idealizoval. Mlyny, ovce, princeznú. Nehovoriac o mladom chlapcovi, ktorý chcel byť mušketierom. 
   
 Páčilo sa mi učiť sa o romantizme a čítať si to málo, čo som z romantizmu prečítal. Napríklad Erbenove balady, alebo jeho rozprávky. Milujem jeho jazyk, každé slovo odhaľuje niečo nové a krásne, čo v človeku urobí miesto do duše. Čítať Erbena, to je ako prechádzať sa po lese s čiernymi stromami. Na začiatku vidieť iba vrany na stromoch, potom sa stromy zafarbia dozelena a na konci tie stromy zomrú. (Nehovoriac o tom, že pri Erbenovi by sa dalo hovoriť o horore ako žánri. Skúste si čítať nahlas pred spaním jeho balady.) 
   
 Otázne je, akým smerom sa romantizmus pobral z 19. storočia. Resp. čo je dnes romantická literatúra? 
   
 Je skupina ľudí, ktorá tvrdí, že dobrá romantická literatúra sa musí končiť šťastne. Ak literatúra, ktorá má umeleckú hodnotu a končí šťastne, mlčím. Čo však, keď to síce dobre skončí, ale čítate to a čítate a ide vás poraziť. Za najhoršie knihy ja osobne považujem tie, ktoré nedávajú človeku nič. Prečítate to a poviete si, „No a teraz čo?" To ako keď idem na rande a dievča začne rozprávať o tom, aká je nešťastná a ako celý život túži po veľkej láske. 
   
 Veľká láska. To je ďalší znak romantickej literatúry. V romantike musí byť veľká láska, hlavne nešťastná. Karel Poláček píše, že ľudia milujú nešťastnú lásku, je v nej niečo krásne. (Nehovoriac o dvojici jeho priateľov, ktorí tvrdili, že človek vidí rád nešťastie na iných, lebo on nič nešťastné neprežíva.) Ale úprimne, kde v literatúre človek nájde veľkú lásku. Inak sa spýtam. Ktoré dielo z literatúry má lásku? Nemyslím teraz tie fotopríbehy z Brava, ale niečo, kde si človek povie, „Tak toto bolo vážne o láske." Keď nad tým rozmýšľam, napadne mi jeden spisovateľ. Je to síce bolestivé a slzy mi stekajú pomaly po očiach, zimomriavky mi behajú po chrbte. William Styron. 
   
 Pamätám si na ďalšiu charakteristiku tej dobrej romantiky. Krása. Literárne dielo, musí byť pekné. Je to relatívne, ale čo je pekné v literatúre? Hesse? Áno, Hesseho literatúra je nádherná. Píše o láske, o hľadaní šťastia, cítiť v diele budhistickú filozofiu a hlavne vnútorný pokoj. 
   
 Čo je v literatúre romantické? Možno je to rovnaké ako pri ľuďoch. Niekto vidí romantické, keď sa prechádza okolo rieky, vidí špinavý sneh a pozerá sa na vlak. Iný vidí romantiku niekde pri sviečkach, v drevenej chate v nejakom ihličnatom lese. Pozor však, aby sviečky nespálili celý les. 
   
 Ja si osobne myslím, že najhoršie na romantike je jej násilná forma. Najhoršie je, keď človek chce byť romantický stoj, čo stoj. Ono je dosť pekné, keď dievča povie chlapcovi, že je romantik. Ale je ešte krajšie, keď mu povie, že je detský človek. 
   
 Ďalšia vec, o ktorej by sa dalo polemizovať, je vzťah romantizmu a lásky. Romantika musí z človeka a takisto aj z knihy vychádzať priamo. Nesmie mať násilnú formu, nesmie sa hrať na príliš krásnu. To máte ako keď vytiahnete dievča niekam na lúku, donesiete deku, syr, víno a ani sa jej nespýtate, či nie je alergická na peľ. Jedno z negatív násilného romantizmu. 
   
 Druhé negatívum romantickej literatúry vidím v tom, aký ma na nás vplyv. Ťažko ho definovať, ale je to zaujímavý paradox. Dielo, ktoré je poprepletané nenormálnym vplyvom lásky, v nás tu lásku ničí. Najhoršie je v živote, keď sa človek neriadi svojimi pocitmi, svojim srdcom, ale len kopíruje okolie, ktoré ani sám nepozná. 
   
 Čo vy a romantická literatúra? Dáte mi odpoveď na otázku? 
   
 Zoznam obrázkov: 
   
 &lt;http://www.smashingapps.com/wp-content/uploads/2008/02/valentine_love.jpg&gt; [cit. 2010-03-11] 
 &lt;http://www.nahuby.sk/images/fotosutaz/2008/05/15/Taraxacum-officinale/frantisek_frimmer_107171.jpg&gt; [cit. 2010-03-10] 
 

 
   

