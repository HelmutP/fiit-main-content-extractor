

 Mám rád kompaktné mestá - mestá, kde je všetko pekne pokope, kde nemusíte používať preplnenú mestskú hromadnú dopravu a dôležité miesta dosiahnete po svojich. Frankfurt je presne týmto typom mesta. Prechádzate sa po nádhernom moste Eiserner Steg, po ľavej strane vás uchváti pohľad na moderné centrum mesta, po pravej strane sa na pokojnej rieke odráža obraz jedného z mnohých frankfurtských chrámov - Kostol troch kráľov. Je tu neskutočný pokoj.  O pár metrov ďalej, smerom ku Európskej centrálnej banke to už tak nie je. Frankfurt je najbohatším európskym mestom a je to aj mesto bánk a financií. Okrem ECB tu má svoju centrálu aj Nemecká národná banka a svoje pobočky tu majú centrálne banky takmer z celej Európy. 
   
  
 Západ slnka nad riekou Mohan 
   
  
 Európska centrálna banka 
   
   
 Nemecká centrálna banka 
   
 Ak ku tomu všetkému pripočítate, že Frankfurt je dopravným uzlom Európy, s najväčšou železničnou stanicou a s jedným z najväčších letísk na kontinente, vyjde vám,  že v najfrekventovanejších uliciach mesta to musí byť poriadna divočina. Pomedzi presklenými monštrami sa hemžia húfy biznismenov v oblekoch, niektorí na bicykloch, niektorí si dokonca ovlažujú nohy v obrovskom počte fontán. Všetko to však prebieha organizovane, na uliciach sa nevytvárajú žiadne zápchy a to aj pre dokonalý systém dopravy. Či už je to neskutočná diaľničná sieť, tak typická pre Nemecko, alebo premyslené pokrytie hromadnou dopravou zahrňujúca U-Bahn, či S-Bahn. Bežnou súčasťou dopravy je už spomínaný bicykel, pre používanie ktorého sú v meste vytvorené adekvátne podmienky. V našich končinách stále science fiction. 
   
 Počas obeda vedú v týchto dňoch všetky cesty finančníkov pred krásnu Alte Oper. Je to miesto, kde sú momentálne rozložené stánky s kulinárskymi dobrotami z celého sveta a obed v takomto prostredí rozhodne odporúčam. 
   
  
 Alte Oper 
   
 Centrum Hesenska nie je iba mestom mrakodrapov. Je to aj mesto so siahodlhou históriou. Tá na vás bude v plnej miere dýchať pri prechádzke Starým mestom. Centrum tejto historickej časti, námestie Römerberg je vôbec jedným z najkrajších námestí Európy. Nachádzajú sa tu pôvodné budovy, z ktorých najstaršou je radnica zo 14.storočia. V tesnej blízkosti sa týči nad mestom impozantná Frankfurtská katedrála alebo Kostol sv. Bartolomeja, taktiež zo 14.storočia. Na tomto mieste prebiehala v minulosti korunovácia nemeckých cisárov. Pred katedrálou sú voľne prístupné archeologické vykopávky z čias rímskeho impéria. V tomto meste sa narodil aj Johann Wolfgang Goethe, ktorého rodný dom je dnes múzeum. 
   
  
 Römerberg 
   
  
 Kostol sv. Bartolomeja 
   
  
 Kostol troch kráľov 
   
 Novou históriu mesta a zároveň typickou tvárou sú mrakodrapy. Commerzbank Tower je s výškou 300m najvyššou budovou v Európskej únii. Druhou najvyššou je architektonicky oveľa zaujímavejšie riešená Messeturm. Táto Veľtržná veža je majestátnym symbolom ďalšieho prvenstva Frankfurtu. Je to prvé veľtržné mesto na svete a tovar sa tu ponúkal už v časoch rímskej ríše. V súčasnosti je najväčším lákadlom svetoznámy frankfurtský autosalón. 
   
  
   
  
 Messeturm 
 Jeden z najväčších zážitkov pri návšteve Frankfurtu je vyhliadka na budove Main Tower. Je to jediná výšková budova v Nemecku so sprístupnenou vyhliadkou a pohľadom z väčšej výšky sa môžete kochať iba z Eiffelovky. Za 3,60 € (študentský lístok) vás čaká naozaj dych berúci pohľad  z výšky 200m nad úrovňou ulice. 
   
  
   
  
   
 Ďalším miestom, kde to výrazne žije, je obchodná ulica Zeil. Tu sa oplatí navštíviť obchodné centrum Zeil Gallery. Keď nechcete nakupovať, stačí sa kochať naozaj nekonvenčným architektonickým riešením tejto budovy. 
   
  
 Moderná architektúra v Zeil Gallery 
   
 Človek môže mať mylný dojem, že Frankfurt je iba mesto betónu, ocele a skla. Kompenzáciou za megalomanskú zástavbu je množstvo zelene v meste. Celým centrom sa tiahne prekrásny park s množstvom jazierok, fontán, trás pre cyklistov a veľkým prekvapením boli aj voľne pobehujúce zajace. Za návštevu určite stojí aj rozsiahla Palmová záhrada nachádzajúca sa severne od centra. Pri tom všetkom vyznievajú trápne vyhlásenia našich miest, ako sa považujú za tie najzelenšie. 
   
 Ak chcete nájsť pokoj a príjemne sa poprechádzať, určite využite promenády popri rieke Mohan. Tak ako v centre aj tu je všetko ukážkovo upravené a čisté. Na chodníkoch a na tráve nenájdete ani stopu po špakoch, žuvačkách alebo papierikoch. Nikde žiadne graffiti, otrhané plagáty a otrasné billboardy. Ak to doženiem do extrému, v tomto meste je problém nájsť trhlinu, nedajbože výmoľ na ceste, či chodníku. Rovnako je to v obytných štvrtiach. Nádherne upravené trávniky, živé ploty až máte pocit, že každý nemec je záhradkár. Pohľad na slovenské mestá je po takejto skúsenosti veľmi bolestivý. 
   
  
   
  
   
 Mám rád históriu, ale aj modernú architektúru. Frankfurt spája obe tieto zložky, o ktoré ak je postarané, môžu pre turistu vytvárať tie najkrajšie zážitky. Frankfurt poskytuje komfort, čistotu, pocit bezpečia a jedinečnú atmosféru, ktorú len tak niekde nenájdete. 
   
  
   

