
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Hanigovská
                                        &gt;
                Nezaradené
                     
                 Egyptský revolucionár 

        
            
                                    27.3.2010
            o
            20:39
                        (upravené
                28.3.2010
                o
                12:05)
                        |
            Karma článku:
                2.08
            |
            Prečítané 
            429-krát
                    
         
     
         
             

                 
                    Achnaton otvoril oči. Vedel, že dnes nastane  významná zmena pre celý Egypt. Obliekol si len jemne tkanú tuniku. S úsmevom na perách sledoval svoju veľkú kráľovskú manželku, pokojne spiacu  na lôžku. Keď si túto chvíľku uložil hlboko  do pamäti, bozkom prebudil svoju ,,slnečnú krásavicu“, ako ju rád volával. Po krátkom sledovaní vychádzajúceho božského slnka  obaja vykročili pevným krokom k novej a nepoznanej ceste. Dvaja mladí ruka v ruke, proti kráľovskému dvoru – najmocnejším kňazom v krajine, za ktorými stálo veľa  stúpencov ale i samotnému ľudu.
                 

                 Dieťa, ktoré pútalo pozornosť  Nazývaný aj Amenhotep IV. alebo Amenofis IV. bol faraónom 18.dynastie (asi 1367-1350 pred n. l.). Už od útleho veku sa veľmi líšil od rovesníkov. Svojim rozprávaním a uvažovaním si podmaňoval ľudí. Možno práve pre tieto skúsenosti ľahšie podstúpil v nasledujúcich rokoch bremeno vlády. Taká veľká zodpovednosť ťažila na útlych pleciach sotva 17 - ročného mladíka. Amonovi kňazi (Amon bol najvyšším a najuctievanejším bohom v krajine) si mädlili ruky. Pokladali za samozrejmosť držať v rukách politické, spoločenské a náboženské opraty. Od nepamäti bol boh Amon ochrancom faraóna a jeho rodiny, tak isto ako celej krajiny. No prepočítali sa. Chlapec s dušou veľkého bojovníka sa nemienil stať bábkou v rukách kňazov.    Kráska a zviera  Teje dlho zvažovala výber manželky pre svojho syna. Náročnú úlohu musí zvládnuť silná osobnosť, ktorá bude hrdo niesť kráľovskú korunu. Nepísané pravidlo spĺňala mitanská princezná, dcéra kráľa Tušrata a kráľovnej Juni. Dnes, jedna z najznámejších egyptských kráľovien nepútala pozornosť len podmanivou krásou, ale aj vzdelaním, priebojnosťou a odvážnosťou. Jemnú vápencovú bustu objavenú 6. novembra 1912 obdivuje každý rok milióny návštevníkov dodnes. Egyptský ľud sa zamiloval no snedej krásky a prostoreko ju začal volať, Nefertiti („krásavica, ktorá prichádza“). Ako v panovníckej, tak isto aj v egyptskej kráľovskej rodine, láska dvoch ľudí nehrala žiadnu úlohu. Musela byť obetovaná v prospech politickej situácie a pre blaho krajiny. Nefertiti a Achnaton sa však vôbec nemuseli premáhať k láske. Kráľovskú manželku oslovoval ,,najkrajšia žena všetkých čias“. Ich vzájomné štastie zdobia do dnešného dňa množstvo zachovaných malieb, na ktorých sa mladá zaľúbená dvojica bozkáva, alebo preukazuje svoje city k deťom. Takéto čosi však bolo dosiaľ nemožne, pretože to etika striktne zakazovala. Harmóniu však narúšali neustále intrigy ale aj vkrádajúca sa choroba. Panovníkovo telo pravdepodobne zasiahla lipodystrofia. Nemé sochy a maľby sú dôkazom hormonálnej zmeny spôsobujúce deformáciu mužského tela na ženské. Okrem toho trpel aj hydrocefáliou - vodnatením mozgu. Nič nebolo ako predtým. Zamilovanosť, ktorú si kráľovský pár prejavoval v súkromí, ale aj na verejnosti, sa začal pomaly ale isto vytrácať.    Kacírsky panovník  Takto častovali svojho faraóna. Obyvatelia uctievajúci mnohobožstvo si museli zvykať na jedného oficiálneho Boha – Atona, slnečného kotúča. To nie vždy malo pozitívne ohlasy. Egypťania pravdepodobne nedokázali pochopiť silu jedného boha, nekonečne milujúceho slnka. Okrem toho, to bol pravdepodobne aj prirýchly krok k takej veľkej zmene. Včera obyvateľstvo Nílu uctievalo svojich miláčikov, no dnes sa mali utiekať k niečomu, čo si nevedeli ani predstaviť. Bohovia Egypta boli opradení mýtmi a tradíciou, zatiaľ čo nové náboženstvo, nemalo takú pevnú základňu. Slnečný kotúč s otvorenými dlaňami nevoňal ani Amonovim kňazom a ich prisluhovačom. Vrcholom náboženskej revolúcie bolo postavenie chrámu v Karnaku. Amonovi kňazi preklínali faraóna. Najtvrdší uder ich ešte len očakával. Príkaz najmocnejšieho muža znel zreteľne – zhabať majetok a zrušiť kult Amona.    Faraón opätuje lásku k mužovi  Postupujúca choroba nezasiahla len telesnú schránku, ale aj psychiku mladého revolucionára. V dvanástom roku vlády opustil ženu, ktorej preukazoval nesmiernu lásku a úctu. Zahľadel sa do muža. Okrem spoločného lôžka vraj zdieľali aj trón. Mnohé nástenné maľby poukazujú na dve nápadne pripomínajúce mužske postavy na tróne. Krásna a nádherná s korunou z peria, veľká dedičná princezná v paláci, veľká je radosť, všetko jasá, keď počuť jej hlas, vládkyňa spanilosti, nesmierne obľúbená, žena...ako volával jednu z oficiálnych titulov svojej Nefertiti však už nebolo počuť. Vzdialila sa z verejného života.    Smrť ,,našiel“ vo vlastnej rodine  V sedemnástom roku vlády však nastal významný zlom. Faraón zomrel, pravdepodobne násilnou smrťou. Na život mu siahol svokor Aje, bažiaci po moci. To bol koniec uctievania zlatistého kotúča na oblohe. Amonovi kňazi mali dôvod pre bujaré nekonečné oslavy. Snahu, ktorá sa zrodila v hlave 17 - ročného chlapca, sprevádzaná až do skonu pohltil piesok, a to doslovne. Meno faraóna, a s ním všetko spájané, bolo do veľkej miery odstránené, vrátane stavieb, sôch.    Achnaton fascinuje verejnosť do dnes  Populárny sa stal omnoho neskôr. Dokázal ,,počarovať“mnoho známym spisovateľom. Christian Jacq, Guy Rachet, Michelle Moran a mnoho dalších nedokázali odolať nenapísať fascinujúci príbeh a zachytiť tak kúsok Amenofisovho života.    Zdroj - http://www.egyptica.sk/faraoni/nefertiti.php    Foto - internet         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Hanigovská 
                                        
                                            Muž bez mena: Mojím cieľom sú prostitútky a budem ich párať
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Hanigovská 
                                        
                                            Stavba, ktorá nás prežije po ďalšie tisícročia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Hanigovská 
                                        
                                            Kedy budeme milovať Barmu?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Hanigovská 
                                        
                                            Deväťročný (bábko)vládca
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Hanigovská 
                                        
                                            Nepochopená „cisárovná Sisi“
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Hanigovská
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Hanigovská
            
         
        monikahanigovska.blog.sme.sk (rss)
         
                                     
     
        Drobím všetko okolo:D.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    8
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    713
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Virtuálna galéria
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




