

 So sklonenou hlavou 
 ...v pohári, 
 kde slová mi padali 
 zamyslená nad pojmom 
 Požieračka vzťahov. 
   
 Čo by ona zato dala, 
 keby niečo mala. 
 Niečo v tom istom pohári 
 na jej svadbe, 
 na oltári... 
   
 Požieračka vzťahov 
 ... keby zato stála... 

