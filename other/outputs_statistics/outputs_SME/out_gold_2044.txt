

 Áno, dobre ste počuli. Tento nápad vznikol vďaka nesmelej poznámke jedného učiteľa počas školenia v Holíči. Vraj: "Ako len tých puberťákov zaujať na prírodovede, keď sa učia ľudské telo?" 
 Odvtedy ubehlo trochu času a ja som našiel riešenie. Stačí, ak si deti zapnú na hodine mobil, odfotia sa a v akomkoľvek kresliacom programe (stačí aj v PAINTE) popisujú kosti, či svaly seba samého (vôbec by som sa nebránil tomu, ak by sa niekto čosi podobné rozhodol skúsiť aj na angličtine, ak bude treba precvičiť slovíčka týkajúce sa ľudského tela :o). Je to omnoho zaujívejšie a zábavnejšie, než popisovonie anonymnej figúry, či obrázku. Pre starších žiakov (náročnejších zákazníkov :o) odporúčam požiadať deti, aby si priniesli do školy natočené svoje videá (povedzme, ak radi jazdia na skateboardoch, tak môžu byť aj akčné scény s touto tematikou :o), tie sa dajú vložiť do softu interaktívnej tabule a decká môžu rovno pri skúšaní na známky popisovať, ako fungujú svaly ich tela pri rozličných neuveriteľných skokoch na skateoch.) 
 Keďže ide o čerstvý nápad, skúsil som ho najskôr nanečisto, počas posledného školeniav Prievidzi, zameraného na inovatívne (m)učenie. Nápad sa u poslucháčiek zjavne ujal. Veď posúďte sami:o) 
   
  
   
  
  
   
   
   
 Ďakujem týmto skvelým kolegynkám z Prievidze, ktoré mi dávajú nádej, že napriek povrchnosti našej vrchnosti má reforma školstva na Slovensku stále šancu... 

