

 Bozky 
 
Líce mi horí, blčia mi pery 
prítulné stvory, 
pred kým to vlastne unikám? 
 
 
Jedna mi hlavou vykrúca 
na tú istú stranu, 
druhá imituje kapríka. 
 
 
Zlákali ste ma, moje Sirény, 
má to však háčik, 
kým vás to baví, mne sa to páči. 
 

