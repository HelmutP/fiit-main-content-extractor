

 Napriek veku a tomu ako vyzeral, hovoril jasne. Hovoril ako pred Veľkou porotou. Zrozumiteľne a k veci, slovami neplytval. Hovoril o svojom utrpení, o rozpadávajúcom sa dome. O bolesti, ktorú prežíva každý deň a o samote, čo ho obklopuje. 
 Hovoril, že na tomto svete už pre neho nie je nič. Nepasuje tu už. Veď to nebude ani minútka, kým On otočí svoj zrak na neho, aby mu už bolo lepšie... a zároveň jedným dychom dodával, že svet má iste aj väčšie starosti. 
 Mala som 23 rokov, keď som ho videla takto kľačať pred krížom prvýkrát. Sledovala som ho s obdivom a strachom... veď žiadal o smrť... Aké veľké muselo byť jeho utrpenie, aká samota ho musela dusiť. 
 Občas ho ešte zazriem, ako pomaly kráča na kopec za krížom. Akoby si vyberal najťažšiu cestu schválne. Možno mu chce dokázať, že je vytrvalý a možno sa zmieril so svojim vlastným životom a v pokore čaká, kým príde jeho čas. Chcela by som veriť, že objavil nový zmysel a snáď trochu radosti na sklonku života. 
 Zdá sa, že nestratil vieru. 
 Dnes si prajem pevnú vôľu, prajem si vieru a hlavne to, aby som nestratila to najdrahšie, čo mám. Pretože ak už z môjho života odídu všetci a budem v samote hľadať odpovede, chcem veriť, že ich nájdem na tom istom mieste ako on... A nebudem sa viac cítiť stratená a sama. 
 _________________________________________________________________ 
   
 Ďakujem 

