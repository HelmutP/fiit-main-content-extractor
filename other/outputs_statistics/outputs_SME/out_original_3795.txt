
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Kuric
                                        &gt;
                Názory
                     
                 A opäť ma nachytali...1. apríl! 

        
            
                                    1.4.2010
            o
            16:35
                        (upravené
                1.4.2010
                o
                20:55)
                        |
            Karma článku:
                10.51
            |
            Prečítané 
            3529-krát
                    
         
     
         
             

                 
                    Prvoaprílové žartíky nie sú mojou doménou, teda nestojím v ich centre diania ako tvorca a zosnovateľ, ale skôr obeť. Nemyslím si, že som až tak prehnane dôverčivý, ale od niektorých ľudí, alebo skupín to proste nečakám. Stalo sa mi to raz aj počas mojej pedagogickej praxe, keď ako sa povie, ma nachytali moji žiaci. Ako triedny učiteľ som zorganizoval odbornú exkurziu do hlavného mesta. Nezúčastnila sa jej celá trieda, pretože som chcel jej časť výchovne potrestať. Mojim zámerom bolo, že ich za horšie výsledky v správaní a v učení vytrestám, a to učením sa a sedením, pre nich v nudnej škole.
                 

                 Exkurzia prebiehala úspešne, až do jedného telefonátu. Volal mi zástupca v škole ponechaných žiakov, že oni sa na celú školu vykašľali a hneď ráno zakotvili v nejakej krčme a radšej popíjali zdraviu prospešné pivo. Keďže boli v čase normálneho školského vyučovania na inom ako školskom mieste, mala ich policajná hliadka v tejto krčme skontrolovať a legitimovať (už tu som mal zbystriť). Samozrejme nie všetci mali ešte dosiahnutú plnoletosť a boli v nesprávnom čase, na nesprávnom mieste. Do telefónu hovoriaci žiak, k tomu javil známky opitosti a oznamoval mi, že práve mi telefonuje z policajnej stanice, kde sú všetci zadržiavaní a vypočúvaní. Oznamoval mi, že ho vyšetrovateľ požiadal, aby som si ich prišiel vyzdvihnúť a podal vysvetlenie prečo nie sú v škole. Samozrejme som ich prostredníctvom hovorcu celej akože opitej skupiny pokarhal a oznámil som ich, že nech pánom policajtom vysvetlia, že sa nemôžem dostaviť, pretože som služobne vzdialený aj s väčšinou triedy a nie je v mojich silách sa tam okamžite dostaviť. Domnieval som sa, že tým je to zatiaľ vybavené, a že všetko sa dorieši na druhý deň na pôde školy aj za prítomnosti rodičov.   Utvrdil som sa o svojom pedagogickom majstrovstve, že som ich so sebou nezobral, pretože opäť dokázali, že sa im nedá dôverovať. Bol som presvedčený, že som predvídal správne, a že niečo podobné by mi možno vykonali aj v cudzom meste. No potom prišiel druhý telefonát, že ono to nie je také jednoduché, že okrem opitosti majú aj iný problém. Vraj konzumovali aj jednu ľahkú drogu, ktorej množstvo, v ich vreckách sa nedalo tak úplne považovať za dávku pre svoju potrebu. Aj preto je moja prítomnosť potrebná, pretože vraj v čase vyučovania som za nich zodpovedný.Vtedy som sa fakt nahneval a myslím, že som do mobilného telefónu spustil aj spŕšku jemných vulgarizmov, ktoré by učiteľ žiakom nemal adresovať. Už som si predstavoval tie dlhé naozaj zmysluplné výsluchy a vysvetlovania pánom policajtom.   Dôveryhodnosť celého príbehu o zatknutí znásobil, ešte telefonát od môjho kolegu, na hodinách, ktorého mali títo delikventi sedieť. Ten mi potvrdil celý príbeh a ja som bol rozhodnutý, že ukončím predčasne exkurziu a urýchlene som zvolával celú skupinu, ktorú som mal na starosti. Vysvetlil som im celý problém, použil som nezodpovedné konanie ich spolužiakov ako výchovný príklad a rozhodol som sa, že ešte obvolám aj rodičov týchto vinníkov, nech vedia čo majú doma. V mojom možno zbrklom konaní ma zastavil, chvalabohu ďalší telefonát a z mobilu sa ozývajúce zborové : PRVÝ APRÍL!   Namiesto nich som bol vytrestaný ja a telo mi zaplavili stresové hormóny. Keby som nemal na starosti svojich žiakov, tak možno by som jeden pohárik destilátu do seba sotil.   Nejako podobne som sa dnes cítil aj pri sledovaní dnešných blogov, kde som objavil aj jeden od samotných tvorcov a správcov blogu. Blog oznamoval, že SME definitívne 4.apríla ukončí prevádzkovanie blogov. V prvých minútach ma zachvátil vnútorný hnev a hryzúc si do pier som rozmýšľal nad zrušením blogu, čo som považoval za rozumnejší krok ako blogovať pre nejaký ženský magazín. Bol som smutný z toho, že aj do tohto príjemného mediálneho priestoru zavítalo tvrdé podnikanie a handrkovačky o peniaze. Bol som nešťastný, že po dlhom váhaní, či si mám vôbec založiť bloga a písať, teraz budem musieť prestať. Pustil som sa čítať ešte príspevky v diskusii, s tým, že jeden šťavnatý tam možno pridám. No potom prišlo vytriezvenie a zborové: PRVÝ APRÍL 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Vianočné knižné tipy. Toto by som odporučil dobrým priateľom
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Strach, ktorý sa vykŕmi deťmi, trúfne si aj na dospelých
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Kto sa bojí vlka, nech nejde do lesa a prečíta si radšej dobrú detektívku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Kuric 
                                        
                                            Svet včerajška na hrane zajtrajška
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Kuric
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Kuric
            
         
        jozefkuric.blog.sme.sk (rss)
         
                        VIP
                             
     
          
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    323
                
                
                    Celková karma
                    
                                                5.98
                    
                
                
                    Priemerná čítanosť
                    4080
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            História
                        
                     
                                     
                        
                            Na ceste
                        
                     
                                     
                        
                            Sranda musí byť
                        
                     
                                     
                        
                            Bulvár
                        
                     
                                     
                        
                            Školstvo
                        
                     
                                     
                        
                            Politická realita
                        
                     
                                     
                        
                            Hudobná sekcia
                        
                     
                                     
                        
                            Zápisky a spomienky
                        
                     
                                     
                        
                            Názory
                        
                     
                                     
                        
                            Pokus o literatúru
                        
                     
                                     
                        
                            Cogito ergo...
                        
                     
                                     
                        
                            Tvorivosť
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            insitný predčítač
                                     
                                                                             
                                            Píšem aj sem
                                     
                                                                             
                                            a trocha aj sem
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




