
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslav Stankovič
                                        &gt;
                Radzim
                     
                 Radzim 

        
            
                                    15.6.2010
            o
            9:05
                        (upravené
                15.6.2010
                o
                10:11)
                        |
            Karma článku:
                9.50
            |
            Prečítané 
            2213-krát
                    
         
     
         
             

                 
                    Sú také kopce, ktoré sa vám vždy nejako postavia do cesty. Nedá sa uhnúť. Pohľad sa vždy uprie priamo na nich. Pritom to nesúvisí  s nejakým individuálnym zážitkom alebo pocitom, ale je to objektívna všeobecná skúsenosť.
                 

                 Napríklad taký Kriváň. Okrem našej slovenskej mytológie ma k nemu takmer nič naviaže a keďže som individualista a národné symboly nie sú v mojom hodnotovom systéme na vrchole, tak naozaj takmer nič. Dokonca som na Kriváni nikdy nebol. Ani ako turistu, ani ako horolezca, ma tento kopec nelákal. Ďaleko viac ma lákal napríklad Malý Kežmarský štít, ktorý sa mi ale nijako nestavia do cesty. Napriek tomu, keď cestujem popod Tatry, vždy mi Kriváň stojí v ceste. Určite to súvisí aj s tvarom týchto výnimočných kopcov ale hlavný dôvod je karma ďaleko vyššia ako má ten najvyššie postavený VIP bloger. Karma sa mi nepáči. Ani Génius loci sa mi už nepáči. Duchov miesta všetci poznajú, aj keď ich nikto nevidel, pretože sme ich z krajiny vyhnali. Páči sa mi označiť tieto výnimočné miesta za prírodné chrámy. Václav Cílek ich charakterizoval ako: „...taková uskupení, kdy samotná konfigurace krajiny působila na lidi stejně posvátně jako o tisíciletí později gotická katedrála". Práve tieto tisícročné pocity hýbu našimi očnými svalmi.       V mojom blízkom okolí sa mi takto stavajú do cesty dva kopce. Drienovec a Radzim. Výnimočné sú nielen svojim tvarom, ale aj duchom miesta, ktorého sa tu nepodarilo vyhnať, pretože sídli pod zemou. Sú to skutočné prírodné chrámy, ktoré nemajú len vznosné piliere, ale aj tajomné podzemie. Hlboké priepasti smerujú k zatiaľ neobjaveným podzemným riekam - cievnemu systému hory, ktorý roznecuje našu predstavivosť. Radzim vzrušuje moju predstavivosť už od detstva. Môj otec pochádza z Vyšnej Slanej, ktorá leží pod severnými svahmi Radzima a jeho príbehy, ktoré pod Radzimom prežil, som vždy počúval  a počúvam dodnes so zatajeným dychom. Radzim vytŕča ako dvojhrbá ťava z bočného hrebeňa Rudohoria, vybiehajúceho zo Stolice smerom na východ. Najlepšie ho vidieť z Dobšinského kopca, kde je aj tradičná vyhliadka, aby si turisti mohli pozrieť nielen Radzim, ale aj Dobšinú a Slanskú dolinu. Tu sa vám Radzim prvýkrát postaví do cesty, hoci je pomerne ďaleko.       Na Radzim sa môžeme dostať z niekoľkých smerov. Najlepšie je to z Brdárky. Už návšteva tejto obce stojí za to, aby sme sem merali cestu akejkoľvek dĺžky. Z Brdárky sa môžeme vydať na západ po zelenej značke do sedla Široké pole a potom neznačenou cestou na lúku do sedla medzi jeho dvoma vrcholmi. Odtiaľ môžeme pokračovať na západný vrchol po značenej trase na vyhliadku smerom na Nízke aj Vysoké Tatry. Alebo východným smerom na jeho hlavný východný vrchol a k vyhliadke Vdovčíkovo kreslo. Ďalej zase značenou trasou juhovýchodným smerom do sedla Hora a naspäť po žltej turistickej značke do Brdárky.      Do sedla Hora sa môžeme žltou značkou dostať aj od lyžiarskeho sedla Július. Podobne ako z Brdárky aj z Vyšnej Slanej môžeme po poľných (neznačených) trasách vystúpiť do oboch sediel a pokračovať na Radzim. Cesty na Radzim vedú aj z Kobeliarova a Nižnej Slanej. Lepšie je absolvovať ich na horskom bicykli, pretože je to trochu ďaleko. Čo môžeme na Radzime zažiť, budem sa Vám snažiť priblížiť v ďalších príspevkoch v tejto rubrike. Teraz len ľahké predjedlo.       Radzim je pravá divočina. Dnes už takmer žiadna pravá divočina na Slovensku neexistuje. Viem to, lebo som zažil pravú divočinu na Stolovej hore Chimantá, kde som chodil po miestach, kde ešte nestála ľudská noha. Za divočinu však môžeme dnes považovať aj miesto, kde prírodné procesy majú navrch nad ľudskou činnosťou. Divočina by mala byť bezzásahová a spontánna. Aj keď sa do sedla medzi vrcholmi Radzima V3S-ka dostane a nedávno tam aj bola, na Radzime sú miesta, kde ľudská noha nestála desiatky rokov a vstúpiť tam môžu len skúsení a zdatní ľudia. Napríklad priestor nad ústím Veľkej priepasti pod Radzimom, kde pokračuje extrémny svah na hranici schodnosti. Ale aj na označených trasách môžeme divočinu zažiť a to je unikátne.      Radzim je zaujímavá speleologická lokalita. Nachádza sa tam niekoľko pozoruhodných jaskýň, napríklad Veľká jaskyňa na Radzime  a Veľká priepasť pod Radzimom. Cesta k nim je slabo označená z Vyšnej Slanej. Majú zaujímavú výzdobu, z ktorej, čo sa dalo poškodiť, je už poškodené. Zaujímavý objekt na vyhlásenie za verejnosti voľne prístupnú jaskyňu alebo v sprievode s turistickým sprievodcom, podobne ako sa prezentuje Dúpna diera formou dní otvorených dverí. Tieto organizujú niekoľkokrát ročne jaskyniari starajúci sa o túto lokalitu.       Radzim je unikátom aj z floristického hľadiska. Vyskytuje sa tu množstvo vzácnych chránených rastlín a jeden unikát -  jazyčník sivý. Okolnosti jeho výskytu prekvapia aj úplného laika. To, že na ostro ohraničenej ploche asi 20 krát  30 metrov dominuje rastlina akoby z iného sveta, ma úplne dostalo.   Horskí cyklisti, ktorých zaujíma aj prírodná kulisa ich športových výkonov, tu nájdu El Dorádo. Moja najobľúbenejšia cyklistická trasa vedie z Nižnej Slanej cez Rimperg, Bučinu, popod Spúšťadlo do sedla Hora. Tu nechám bicykel a vyjdem na Radzim. Túra okolo Radzima patrí k tomu najkrajšiemu, čo sa dá v sedle horského bicykla zažiť. Na bicykli sa dá vystúpiť aj do sedla na Radzime. To som ešte neskúšal. V každom prípade si pripravte ľahšie prevody, aby ste nemuseli bicykel tlačiť.    A to najlepšie na záver. Višňové sady. Freud by sa potešil. Samozrejme, sú to čerešňové sady. Pre mňa sa ich kultúrno - estetický význam vyrovná Čechovovej dráme podobne, ako sa Radzim vyrovná gotickej katedrále v Štítniku. Preto ten chybný úkon. Kto ešte nevidel rozkvitnuté čerešňové sady v okolí Brdárky, len ťažko si vie predstaviť Čechovove višňové sady. Vyberte sa na jar, keď kvitnú čerešne, do Brdárky a vystúpte do sedla Hora a odtiaľ zbojníckym chodníkom na vyhliadku Vdovčíkovo kreslo. Určite na to nikdy nezabudnete. A každú jar vás to bude ťahať práve sem. Netreba zbytočne čakať. Aj tento kút je súčasťou miznúceho sveta. Čerešne postupne hynú po stojačky rovnako ako nádherné domčeky v Brdárke.    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            50. výročie objavu Krásnohorskej jaskyne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Jeden pohľad 2 koruny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Najkrajšia hrebeňovka Volovských vrchov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Peter
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Okolo Rákoša
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslav Stankovič
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslav Stankovič
            
         
        jaroslavstankovic.blog.sme.sk (rss)
         
                        VIP
                             
     
        Som starý blázon, ktorý si zo záľuby urobil profesiu a z jaskyniara sa stal správcom Krásnohorskej jaskyne.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2042
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Silné zážitky
                        
                     
                                     
                        
                            Jaskyne
                        
                     
                                     
                        
                            Návštevníci
                        
                     
                                     
                        
                            O dobrých knihách
                        
                     
                                     
                        
                            Radzim
                        
                     
                                     
                        
                            Venezuela
                        
                     
                                     
                        
                            Rožňavské cyklotrasy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jan Piasecki, Rukopis nalezený v Zaragoze
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




