

 
Svet IT technológií ale už nielen ten sa len tak hemží slovíčkami ako "kilo", "mega", "giga", v poslednej dobe aj "tera"... Nepomenúvávajú objekty, ale čísla alebo lepšie povedané násobky čísel. 
 
 
 

	
		
			Skratka 
Znamená to 
Približne # 

Skutočne # 
		
		
			
K 
			Kilo 
			1 000 (tisíc) 
			1 024 
		
		
			
M 
			Mega 
			1 000 000 (milión) 
			1 048 576 
		
		
			
G 
			Giga 
			1 000 000 000 (miliarda) 
			1 073 741 824 
		
	


 
 Ak v bežnom (nepočítačovom) svete chcete používať tieto násobky, môžete zabudnúť na "Skutočné" hodnoty uvedené v tejto tabuľke. 
Pokiaľ idete od "K" cez "M" až po "G", vždy pridávate "000" na konci čísla. 
 
 
Ak teda niekto napíše Windows 2k3, znamená to, že myslí Windows 2003. 
 
 
Ak vám počítač napíše, že súbor má 35 840 kB, znamená to, že má približne 35 MB. 
 
 
Prečo približne? 
 
 
Ak nechcete čítať teóriu, kľudne tieto šedé písmeká preskočte  
Počítač spracúva informácie v tzv. dvojkovej - binárnej sústave. 
Každé číslo je reprezentované kombináciou núl a jednotiek. 
Napríklad číslo 20 v desiatkovej sústave je reprezentované v binárnej sústave ako číslo 10100. To dovoľuje počítaču pracovať s dvomi stavmi. "0" - vypnutý, "1" - zapnutý. 
Táto matematika bola premietnutá aj do počítania s údajmi v počítači. 
Binárne číslo 10000000000 sa po prepočte do desiatkovej sústavy zjaví ako číslo 1024. 
Celé sa to premieta do prepočtu bitu (mäkké "i" a malé "b" - bit) na Bajt (v angličtine tvrdé "y" a veľké "B" - Byte), kde 1 Bajt = 8 bitov. 
 
 
Jeden KiloBajt (kiloByte, kB) = 1024 Bajtov (Bytes, B). 
 
 
Ak chceme robiť prepočet medzi Bajtami, KiloBajtami, MegaBajtami, GigaBajtami, musíme si zobrať za základný násobok 1024 a nie 1000. 
 
 
35 840 kB = 35 840 kB ÷ 1 024 kB = 35 MB 
 
 
 
Ako je to asi s rýchlosťami prenosu? 
 
 
Stretli ste sa s lákavou ponukou poskytovateľov internetového pripojenia, kde ponúkajú niekoľko (desiatok) Megabitov za sekundu? Po objednaní si takéhoto pripojenia vám ale pri sťahovaní súborov z internetu počítač ukazuje len niekoľko desiatok/stovák kiloBajtov za sekundu. 
 
 
V čom je teda problém? 
 
 
V prepočte. 
Podľa šedých písmenok v predošlej stati platí, že 1 Byte (po slovensky Bajt) = 8 bits (po slovensky bitov). 
 
 
Čo teda, ak máte balík, ktorý vám umožňuje sťahovať 4 Mbit/s? 
 
 
4 Mbit = 4 Mega bity. Koľko je to však Bajtov? 
4MBit/s ÷ 8 = 0,5 MByte/s (pol MegaBajtu za sekundu). 
 
 
Prečo bity a Bajty?  
Na internet sme pripojení cez sériovú linku. Sériová znamená, že sa po nej dáta posúvajú za sebou - v sérii. Naraz tak môže byť prenesený LEN 1 bit. 
V paralelných linkách, napríklad v komunikácii s tlačiarňou po paralelnom (súbežnom) pripojení sa presúvalo naraz 8 bitov = 1 Bajt. 
Operačný systém nepoužíva na zobrazovanie veľkosti súborov jednotku bit, ale bajt kvôli ušetreniu miesta v operačnej pamäti. Predstavte si, že 3 MB by sme museli napísať ako 24 Mb. To je len jedna číslica navyše. Čo ak pracujeme so súbormi veľkými niekoľko GigaBajtov? 
 
Preto vidíme veľkosti súborov udané v kilo/mega/gigaBajtoch (Bytes). 
No a viete si predstaviť ten chaos, keby vám prehliadač alebo akýkoľvek program, ktorý dokáže sťahovať súbory z internetu vyjadroval jednu hodnotu v bitoch a druhú v bajtoch? Mne by sa to v hlave počítať nechcelo. 
 

	
		
			Pripojenie 
Rýchlosť 
bitov za sekundu  

Čas sťahovanie 1 MB 
		
		
			Dial-up 
			56 Kbps 
			56 000 
			143 sekúnd 
		
		
			ISDN 
			128 Kbps 
			128 000 
			63 sekúnd 
		
		
			Broadband 
			1 Mbps 
			1 000 000 
			8 sekúnd 
		
		
			T1 
			1.5 Mbps 
			1 500 000 
			5 sekúnd 
		
	

 
 
Takto môžete vidieť približný čas stiahnutia 1 MB cez rôzne typy pripojenia a rôzne rýchlosti. 
 
 
Malé zopakovanie na záver: 
 
1 Bajt (Byte, B) = 8 bitov 
1 KiloBajt (KiloByte, kB) = 1024 Bajtov = 8 192 bitov 
1 MegaBajt (MegaByte, MB) = 1024 × 1024 Bajtov = 1 048 576 B = 8 388 608 bitov. 
 

