

 Vypadnúť na weekend z domu. Patince -plne. Trenčianske Teplice - festival.  Nakoniec a úplnou náhodou Šťúrovo. Od piatka obeda pohoda, klídek a žiaden tabáček. V hangári s občerstvením sú veľké elcedečka. V nedeľu o štvrť na dve, kupujem žene pivo a langoš so syrom a sebe kofolu. Idem za dievčaťom čo predáva menu a neviem si vybrať. 
 -Robíme čerstvé halušky za dve eurá, počkáte si päť minút? - Fajn. Budem sedieť tam, potom mi kývnite, prídem si ich zobrať. - Ja Vám ich donesiem. 
 Žena chváli langoš, ja sledujem, ako sa posadí pred nás poltucet mladých optimistov. Do 20 minutý zápasu sa natláčam národnou špecialitou. Bryndza a halušky hrajú na tanieri 1:1. Naši Maďari varia a čapujú a očkom poškuľujú na obrazovku. Rovnako starí českí fotri sledujú zápas pri plzenskom. Poliaci hlučne zapíjajú vodku pivom a futbal je im ukradnutý. Vyšegrád ako vyšitý. 
 0:1 
 - Prehrajú, idem do vody. - povie žena 
 Trpím ako pes. Ani dobre presso mi nepomôže. Keď vyfasujeme druhy, idem aj ja preč. 
 A taký pekný weekend to bol. Odfotil som si famóznu sochu bojovníka na ostrihomskom kopci. Znovu a pomaly  sme si prezreli klenotnicu katedrály. V kresťanskom múzeu pod kopcom sme pokecali s pani, čo na rozdiel od našich babiek v múzeách, nielen že vedela čo vystavujú, ale aj nemecky. Ukázala mi „líšku ryšavú" cisára, českého a uhorského kráľa Žigmunda Luxemburského na gotickom oltári s Ukrižovaním z Hronského Beňadiku. Stará škola, ešte aj Chemnitz, kde majú podobný skvostný Boží hrob na kolieskach, nazvala Karl-Marx-Stadt. 
 Čvachtal som sa v teplej vode, všade bolo čisto, úsmevy a ochota. Dobre jedol a zalieval blaženosť čapovaným pivom a ružovým vínom. A taký strašný koniec. 
 V aute po ceste domov 
 - Ešte s kým hrajú?- 
  - S Talianmi. Ale to by museli nad Talianmi vyhrať, Taliani by museli remizovať s Novým Zélandom a Paraguaj by musel poraziť Zelandanov. 
 - Nesnívaj. 
 Nesníval som. Ale mal. Dobrý weekend nemôže skončiť  úplne zle. 
 http://www.keresztenymuzeum.hu/index.php 

