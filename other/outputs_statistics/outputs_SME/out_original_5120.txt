
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Lenka Šoltésová
                                        &gt;
                Nezaradené
                     
                 Miroslava Roguľová- fashion fotografka 

        
            
                                    23.4.2010
            o
            16:03
                        (upravené
                24.4.2010
                o
                11:06)
                        |
            Karma článku:
                3.65
            |
            Prečítané 
            1529-krát
                    
         
     
         
             

                 
                    Existovala by móda bez fotografa? O Jakubovi Klímovi všetci dobre vieme, že je majster svojho remesla, ale čo s fotografmi, ktorí sú menej známi? Práve preto, som sa začala zaujímať o túto skupinu ľudí, ktorí sa venujú "fashion"fotografii. Po dlhom čase ma ako jediná zaujala Miroslava Roguľová. Jej fotky majú príbeh a čo sa mi páčilo úplne najviac, boli tie najnetradičnejšie miesta, ktoré si Miroslava pre svoje fotky vyberá.
                 

                 
  
   Pri našom prvom stretnutí z nej sálala dobrá energia, radosť a zároveň extravagancia a svojim spôsobom nežná drzosť. Miroslava má 22rokov pochádza z Prešova, no väčšinu svojho života prežila v New Yorku. Fotografovaniu sa venuje celý život, no do sveta módnych fotografov, sa začlenila približne pred rokom. Táto mladá fotografka, ktorej kariéra sa veľmi sľubne vyvíja, mi do môjho blogu poskytla aj rozhovor,  takže, nech sa páči.   Ako si sa dostala k fotografovaniu ako takému?   Vyrastala som medzi fotografmi. Už ako malá, som im robila modelku a to bol štartovný moment v mojej kariére "fotografky". V živote ma tiež ovplyvnil môj priateľ, fotograf Tomáš Molčan, ktorý ma zasvätil do technického sveta fotografie.   Prečo si použila slovo fotografka v úvodzovkách?   Lebo sa za fotografku nepovažujem. Existuje veľa fotografov, no ja sa len snažím z krátkonohých a atypických žien, vytvoriť dlhonohé a žiadúce.   Čo ťa viedlo práve k foteniu fashion fotografií?   Ako každá žena mám rada módu a samozrejme, to boli aj financie.   Odkiaľ čerpáš námety pre svoje fotky?   Nepredstavujem si nič konkrétne, skôr to je o tom, čo vidím a kde sa pohybujem. Nápad príde všade, dokonca aj na balkóne môjho bytu.(smiech)   Pracuješ s modelkami-neprofesionálkami. Podľa čoho si ich vyberáš?   V prvom rade ma musí zaujať, kašlem na to, čo si o nej myslia iní, dôležité je to, že sa páči mne. Modelky, ktoré fotím, by nemali byť komerčne pekné.   Ďakujem za rozhovor a praje veľa úspechov v tvojej ďalšej kariére.   Ďakujem veľmi pekne, vážim si to.   V skratke:    Meno: Miroslava Roguľová   Miesto pôsobenia: New York, Miláno(leto 2010)   Voľný čas trávim...: Fotením a jazdou na skútri s priateľom.   Som...: Typický vodnár!   Chcela by som...: ...byť "creative editor of photography"   Obľúbené jedlo: Som vegetariánka, takže všetko zo zeleniny.   Obľúbená hudba: Oldies a cinematic.   "MOTTO": "Sprostá si, doma seď."        GALÉRIA      Zdroj: www.flickr.com/photos/36541666@N04 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (17)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Šoltésová 
                                        
                                            Najhorúcejšie módne trendy III- "Nôžky v teple"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Šoltésová 
                                        
                                            Najhorucejšie módne trendy II- Donna Karan
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Šoltésová 
                                        
                                            Najhorúcejšie módne trendy - jeseň 2010
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Šoltésová 
                                        
                                            V znamení luxusu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Lenka Šoltésová 
                                        
                                            Street Chic- Apríl, Košice
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Lenka Šoltésová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Lenka Šoltésová
            
         
        lenkasoltesova.blog.sme.sk (rss)
         
                                     
     
        Som študentka,začínajúca "blogerka" a módny maniak
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    20
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2564
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




