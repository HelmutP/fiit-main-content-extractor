

 Na hotelovú izbu som prišiel až v noci. Do druhej sa mi nedalo zaspať. Pre môj organizmus bolo stále len „slovenských" osem hodín večer, teda stred dňa. Cez okná mi dunela muzika z dámskeho salóna, ktorý sa nachádzal v dome naproti. Konečne som začal driemať... 
 Niečo ma uštiplo do ruky. Slnečný svit už vnikal do izby, mohlo byť tak okolo šesť hodín. Snažil som sa vrátiť do snov, i keď miesto v okolí bodnutia ma nepríjemne pálilo. Odvedľa som periférne vnímal cupitanie dámskych opätkov. Vravím si v duchu: „Vedľa mňa býva asi prostitútka, vracia sa z nočnej". Po chvíľke však otváram oči a nejaká pekná deva je v mojej izbe. Skláňa sa nad stolom a niečo tam hľadá. Tentoraz mi napadlo, že upratovačka, ale keď som sa pozrel lepšie, bola na túto profesiu príliš pekným zjavením. 
 Milo som na slečnu prehovoril, nech nepotratí: 
 - Dobré ránko. Kto vám dal kľúč od mojej izby? 
 Preľaknuto sa otočila: 
 - Mal si otvorené. 
 Na dôkaz mi podáva kľúče od svojej motorky. Musela by byť thajskou odrodou Copperfielda, aby s nimi dokázala otvoriť moju izbu. Ďalej to neriešim - vracia sa mi spomienka, že som sa večer nezahasproval, ani nezatlačil dverovú poistku - proste som sa nezamkol. Slečna zatiaľ ku mne svižne priskočila do postele, lahodne sa ku mne pritúlila, chytila ma za rozkrok (prvý gól, 1:0) a bez okolkov sa ma spýtala, či chcem sex. 
 Odpovedal som jej mojimi klasickými vetami, ktoré tak rád používam v krajinách s prekvitajúcou prostitúciou: 
 - Na Slovensku robím džigola, baby mi za to platia. Máš peniaze? A môžeš mi predtým ešte ukázať preukaz, že nemáš aids či iné pliagy? 
 Dobre viem, že by sa musel stať zázrak, aby som na obidve otázky dostal kladnú odpoveď. Som pokojný. Aj slečna. Miesto odpovede ma znova chytá za rozkrok (2:0), žmolí pery ukazujúc mi, čo s nimi túži stvárať a stanovuje cenu. Potom si ľahne na brucho a otŕča mi pekne stavaný zadoček, nech si okukám a ochytám kvalitu. Pripadám si ako posledný biely kôň na čiernom trhu. 
 Stále som rozospatý, moja myseľ ešte nevníma naplno absurditu celej situácie: Cudzia bytosť narušila môj osobný priestor, vnikla mi do izby, ponúka mi sex - asi nejaký dohodnutý domáci komplot, kolorit, folklór (či ako to nazvať) medzi recepciou a domácimi prostitútkami. Čierna čokoládková diva mi zatiaľ strieľa ďalšie dva góly (3:0, 4:0) a klesá s cenou na smiešnych 300 báthov (čo je okolo 7 Eur). To sa mi už vonkocom nepozdáva. Vyrážam do útoku a chytám ju za rozkrok. Pipíka má síce dobre stiahnutého a uviazaného, ale i tak tam JE! Vedľa mňa leží muž, ktorý vyzerá ako krásna žena!!! Bŕŕŕ... 
 Od tejto chvíľky som bezradný, neviem ako ju skloňovať. Je to ON, ONA či ONO? Má to prsia, nemá to vagínu. Má to pipíka, no oči vnímajú sexi ženu. Som v Thajsku po druhýkrát, je mi jasné, že najkrajšie baby sú tu transvestity (po domácky „lady-man"). Neviem ako to robia, ale je tomu tak. Dá sa ich veľmi jednoducho odhaliť cez hrubší hlas a dlhé chodidlá, ale táto (tento, toto) to nejako obišla (obišiel, obišlo). 
 Hra ma prestala baviť. S úsmevom vyprevádzam dobre stavanú tetu smerom k dverám. Tá mi za ohrdnutie posiela vztýčený prostredník a pridáva nejakú vetu, ktorá obsahuje slovíčko fuck. Rozumiem, bez toho by sa to neobišlo. Nezdá sa mi však, ako rýchlo práši preč. Z balkóna sledujem, ako sadá na motorku a odchádza. Nestihol som si zapísať číslo. Kriminálka sa začína. 
 Prechádzam si veci. Pas i peniaze mám. Ošacované nohavice ležia na dlážke, zásuvky sú pootvárané. Rozum mi konečne začína fungovať a všetko mi dochádza. Našťastie mi chýbajú iba tri strieborné prstene. Prsteň-ruženček mi dotyčná duša nechala. Ja to stále vravím - veriaci sú dobrí ľudia. 
   
 Ponaučenia, ktoré som z celej bizarnej udalosti prijal: 
 1, Ďakujem ti chrobáčik, že si ma uštipol. Spal som tak tvrdo, že by ma vyčistil (a, o) nadobro. 
 2, Verím v Boha (ale bez všetkého, čo k tomu pridal človek!), no auto si treba zamykať. 
 3, Vesmír, mal som veľa prsteňov. Vo vrcholnej forme som ich nosil dokonca osem. Tri som stratil, jeden som daroval. Do Thajska som šiel so štyrmi. Teraz už viem, že ich nepotrebujem a že mi nemali patriť. Pošli mi teda niečo, čo ma bude tešiť dlhodobejšie. Ďakujem! 
 4, Bože, nech sú jej (jemu) tie prstienky dobré. 
 5, I pri výsledku 4:1 pre súpera môžem vyhrať! 
 6, Vesmír, ďakujem ti, že si ma takto milo varoval. Vnímam to ako upozornenie, lebo nabudúce to mohlo skončiť ináč a oveľa horšie. 
 7, Náhody neexistujú, hovorte si čo chcete. 
 8, Život je prekrásny! 
 9, Deviatka znamená duševný vrchol, musím sem niečo napísať. Ale čo? Aha, tak teda môj otčenáš: Ďakovať, odpúšťať, prijímať a prosiť sa oplatí. 
   
 Buďte šťastní, Pavel "Hirax" Baričák 
   
   
 PS: Najbližšie čítačky Hiraxa k najnovšej vydanej knihe Kým nás láska nerozdelí: Nachádzania (druhý a konečný diel k románu Kým nás láska nerozdelí oficiálne vyšiel 15.5.2009): 23.5 o 14:00 - 15:00 hod, Martin, Turčiansky  festival zdravého životného štýlu (organizuje Slnko v srdci) a v utorok 26.5 Nitra, od 18 hod, priestory v Starom  divadle Karola Spišáka, kaviareň Tatra (bude aj projektor, poviem teda čo-to aj  o mojich potulkách Panamou, Indiou a "čerstvým" Thajskom). Bratislava je  posunutá na jún. Dátum a priestor zverejním pod mojimi novými blogmi. 
   
 Fotky: Nejdem písať popisky. Legenda je jednoduchá: To najkrajšie na fotke je aj tak zasa iba muž.  
  
  
  
  
  
  
  
  
   
   
   
   

