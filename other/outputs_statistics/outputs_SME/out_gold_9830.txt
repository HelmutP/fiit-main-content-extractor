

 Určite si pri tejto príležitosti nespomenuli sami na seba ani ľudia na Gemeri, kde bol v júni vyhlásený tretí stupeň povodňovej aktivity na riekach Dobšinský potok a Slaná.  
 Poďme sa teda pozrieť do pramennej oblasti Dobšinského potoka pri Čuntave, nad mestom Dobšiná. 
 Už prítomnosť podivných strojov pri zdroji vzácnej vody zarazí. 
  
 Pri ďalšom postupe už človek začne rozmýšľať či je na Marse, Venuši alebo na Mesiaci? 
  
 Tu rástli ešte pred rokom stromy a chránili nielen vodu ale predovšetkým pôdu. Čo sa stalo? Globálne klimatické zmeny, nadmerné dažde alebo predsa len boží zásah? 
  
 Nie. Konkrétni ľudia a konkrétne firmy sú zodpovední za túto krajinu pri prameňoch Dobšinského potoka. 
  
 Chlapci na Gemeri si fajne zarobili a novinári mali týždeň pri povodniach o čom písať. Všetci sú teda spokojní. Už je všetko zabudnuté a tak sa môžem vrátiť k svojmu obľúbenému Platónovi a jeho knihe Kritias: 
 Tehdy však, dokud byla ještě neporušena, byly její hory vysoké kopce hlíny, měly roviny, které nyní se nazývají kamenité, plny tučné prsti, i měla na horách mnoho lesů, po nichž jsou ještě nyní patrné stopy; kdežto totiž některé z hor chovají nyní jen potravu pro včely, byla dříve ze stromů tam nakácených tesána břevna na vazby největších staveb a není tomu příliš dávno, co ty vazby, ještě držely. 
 Mnoho tam bylo ušlechtilých vysokých stromů a půda poskytovala nezměrné pastvy dobytku. Také byla rok co rok napájená vodou z Diových dešťů, které neztrácela jako nyní, kdy voda stéká z holé země do moře, nýbrž majíc hojně prsti, přijímala vypitou vodu do ní, uchovávala ji pod vrchní hlinitou vrstvou a vypouštěla pak z výšin do údolí; tak vytvářela na všech místech bohaté zdroje studánek a řek, po nichž ještě i nyní zbývají posvátné stopy u dřívějších pramenů, svědčící, že pravda jest co se nyní vypravuje. 
 A ja teraz rozmýšľam ani nie tak nad príčinami povodní, ale nad tým, kto je tu vlastne idiot.  

