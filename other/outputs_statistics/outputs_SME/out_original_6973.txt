
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Viktoria Laurent
                                        &gt;
                Osobné a politické
                     
                 Preteky s vekom 

        
            
                                    21.5.2010
            o
            16:59
                        |
            Karma článku:
                3.58
            |
            Prečítané 
            370-krát
                    
         
     
         
             

                 
                    Nedavno k nam opat z Estonska dosla kamaratka, Olesja. Spoznala som ju vdaka manzelovi, ktory precestoval pekny kus sveta a vsade si nasiel priatelov. Cloveku to pomaha nazriet do kultury inej krajiny aj si precvicit ine jazyky. Ale nie o tom som chcela...
                 

                 Prechadzky Parizom su v tejto dobe mimoriadne prijemne. Zimny cas konecne odisiel na odpocinok a slnko umoznuje vytiahnut  zo satnika sukne, saty a sandale. Na terasach vysedavaju turisti a Parizania (zalezi od toho, v ktorej stvrti sa nachadzate) a uzivaju si slnecne pohladenia na tvari do polovice zakrytej slnecnymi okuliarmi podla poslednej mody.   Pri diskusii o vsetkom aj o nicom sme padli na temu "krasa a esteticke operacie". Olesja sa uznanlivo vyjadrila o francuzskych hereckach (tych starsich, klasickych). Vraj sa neboja zostarnut a vela proti tomu nerobia. Hm, zalezi od toho, ktora.   Catherine Deneuve nanestastie podlahla natlaku priemyslu krasy a uz davnejsie si dala chirurgicky upravit ocne viecka. Nie velmi vydareny kusok! Co je vsak pravdou je, ze Catherine sa neboji pribrat a napriek svojmu veku si zachovala urcitu "class". Je vzdy elegantna. Niektore zeny to jednoducho maju v sebe!   Jeanne Moreau vek na krase nepridal. Je to vynikajuca herecka, ale vek ju nesetril. Akoby jej to, co na nej bolo kedysi krasne, zvacsil a zohavil. Rozpravam hlavne o jej ustach... Ale vela s tym nenarobi a ani sa nesnazi...   Charlotte Rampling je tiez uz v peknom veku. Vrasky jej pristanu, talent nezaprie a zachovala si eleganciu. Jednoducho prava dama !   Ako priklad neuspechu plastickej chirurgie by sa dalo rozpravat o Isabelle Adjani. Jej kedysi nadherna tvar sa nafukla a dnes skor pripomina gumenu babiku ako svoju podobu za mlada. Plet ma neprirodzene vyhladenu a usta ako dva cervy nazrate na prasknutie.       Olesja je v rovnakom veku ako ja. V Estonsku je clenom hudobnej kapely, ktoru zalozil jej brat. Maju tam pomerne velky uspech a pohybuje sa aj v prostredi novinarciny.   Ked sme zacali rozoberat pouzitie botoxu v soubiznise, zahlasila, ze proti tomu nic nema. A vlastne si je ista, ze okolo styridsiatky si da vpichnut botox aj ona. V Estonsku je to vraj uz bezne (predpokladam, ze na Slovensku tiez).   Osobne som nikdy neuvazovala nad pouzitim plastickej chirurgie alebo inych skraslovacich zakrokov na sebe v buducnosti. Nechcem sa riadit diktatom spolocnosti podriadenej mode, ovplyvnenej titulkami v casopisoch ako Vogue alebo Elle.   Irituje ma tiez svet pod vplyvom modnych prehliadok a primitivnych nazorov Karla Lagerfielda. Vsetky prostriedky na schudnutie a diety. Akoby sme zabudli zit normalnym zdravym zivotom.   Skutocne dosledky pouzivania botoxu nie su zname. Predsa sa len jedna o chemicku zluceninu cudziu ludskemu organizmu. Vela uzivateliek (aj uzivatelov) botoxu sa stazuje, ze od prveho pouzitia akoby vstupili do nekonecneho kolotoca. Stali sa na botoxe zavislymi. Je to pomerne drahe a navyse je potrebne proceduru pravidelne opakovat. Dobry biznis pre kozmeticky priemysel !!!   Uplne naopak suhlasim s pouzivanim botoxu na lekarske ucely ako napr. na liecene svalovych problemov alebo problemov s prostatou.     Starnutie patri k zivotu. Tak ako sedive vlasy a vrasky. Mimicke vrasky vela prezradzaju o osobnosti cloveka a myslim, ze je skoda ich odstranovat. Ako sa hovori "Nasa tvar je zrkadlom nasej duse", tak preco ju deformovat?   Jednym som si ista. K pouzitiu Botoxu sa nikdy v buducnosti neznizim. Vrasky som vzdy povazovala za symbol zivotnych skusenosti a krasy starnutia. A ked natrafim pri mojich cestach svetom na sedemdesiatrocnu zenu s perami nafuknutymi kolagenom a tvarou natiahnutou liftingom, z hlbky duse ju lutujem. Jej tvar v porovnani s rukami poznacenymi stareckymi skvrnami a vraskami posiatym krkom  vyzera komicky! Odfarbene peroxidove blond vlasy vyfukane à la Marilyn Monroe jej z veku neuberu. Zradi ju chodza na urovni mechanickej babky. Ale mozno coskoro vymyslime chemicku latku na obnovu kosti, ktoru si budeme moct vpichovat a tak si navzdy udrzat mladistvu aj chodzu... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Viktoria Laurent 
                                        
                                            Záhrada v Paríži
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktoria Laurent 
                                        
                                            Ako písali Francúzi o Slovensku v rokoch šesťdesiatych?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktoria Laurent 
                                        
                                            Cesta do Dusseldorfu za The Wall
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktoria Laurent 
                                        
                                            Rozhodli sa postaviť hrad (château de Guédelon )
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Viktoria Laurent 
                                        
                                            Ďalší psí deň je na svete
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Viktoria Laurent
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Viktoria Laurent
            
         
        laurent.blog.sme.sk (rss)
         
                                     
     
        Nebyt jednou zo stada oviec! Som proti blbcom a egoistom.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    167
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1255
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Prvé oťukávanie sa
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Ekológia
                        
                     
                                     
                        
                            Osobné a politické
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Svadba pre všetkých
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




