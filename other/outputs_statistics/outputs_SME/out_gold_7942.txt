

 SNS sa chytá topiacej slamky. Smer jej prebral jedinú tému, na ktorú vábi voličov. Do volieb zostáva málo času a strata voličských hlasov v prospech Smeru je viac ako reálna.  Smer promptne zareagoval na prijatie zákona o dvojitom občianstve. SNS už mohla len pritakávať. Úvodné a hlavné protimaďarské slovo na protimaďarskej tlačovej besede mal tentokrát nie Slota ale Fico. 
 Našťastie pre SNS, tohto roku je výročie podpisu Trianonskej zmluvy. Zmluvy, ktorá už de facto len právne ustanovila to, na čom sa víťazné mocnosti dohodli už po skončení prvej svetovej vojny. 
 Niektorí maďarskí občania a politici verdikt Trianonu dodnes nestrávili. Nevedia sa zmieriť s tým, že ako prehratá mocnosť si nemohli vyberať. Nevedia stráviť skutočnosť, že dejiny vždy tvoria víťazi...a tak teraz, kedy sa Maďarsko zmieta v ekonomických a sociálnych problémoch je táto téma populárna. Veď čo je lepšie ako pripomínať ľuďom minulosť - namiesto riešenia problémov súčasnosti a budúcnosti... 
 A SNS neváha. 4. júna sa chce po dlhšej odmlke znova architektonicky činiť - postaviť pamätník Trianonu v Komárne. 
 Prečo to vlastne robí? 
 Slota oficiálne tvrdí, že je to snahou o ukázanie Maďarom kde sú hranice atď. Každý však vie prečo to SNS robí - nemá už na výber, buď sa prezentuje teraz alebo už inde voličov nezíska, čas k voľbám sa kráti. 
  K čomu to bude dobré? Ako tým napomôže vývoju Slovenska? 
 Toto je podľa mňa oveľa dôležitejšia otázka. Mnoho ľudí na Slovensku ani netuší, čo to tá Trianonská zmluva je.  Všade vo svete sa stavajú pamätníky osobnostiam a udalostiam, ktoré majú ľudia stále pred sebou, ktoré pre nich niečo naozaj znamenajú. 
 SNS na Slovensku stavia pamätník udalosti, ktorú každý bežný občan považuje za úplne samozrejmú (vojna skončila, rozpadla sa monarchia, nakreslili sa hranice). 
 SNS na Slovensku stavia pamätník udalosti, ktorá je v Maďarsku hlavnou témou nacionalistov, populistov a podobných vymývačov ľudských mozgov. 
   
 Netvrdím, že udalosť táto udalosť z roku 1920 nebola dôležitá pre vývoj budúcej ČSR a neskôr SR. 
 Ale pýtam sa vás pán Slota a spol.: Na to ste prišli až teraz, keď sa zrazu  chystáte stavať pamätníkov po celom južnom Slovensku, a to 10 dní pred voľbami???? 
 Nesmrdí toto práve tým farizejstvom, ktorým tak radi častujete KDH???? 
   
 Namiesto toho, aby sa Slováci nad výrokmi členov maďarského parlamentu hrdo povzniesli, im na úder nadhadzujú ďalšie a ďalšie loptičky. 
 Ale to vám pán Slota a spol. vyhovuje. A momentálne to vyhovuje aj Ficovi a jeho s.r.o., ktorý vás v tom všetkom vymývaní podporuje.  Vám skutočne nejde o národnú hrdosť všetkých obyvateľov Slovákov! Pretože keby ju majú, tak ľudia ako vy nesedia vo vláde... 
 Po výstavbe dvojkrížov je tu zo strany SNS ďalší ponižujúci nezmyseľ a desím sa toho, čo môže prísť, ak by vo vláde boli aj po 12. júni.... 

