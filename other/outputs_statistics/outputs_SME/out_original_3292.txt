
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Maroš Markovič
                                        &gt;
                Nezaradené
                     
                 Je živnosť lepšia ako Trvalý pracovný pomer? 

        
            
                                    25.3.2010
            o
            12:58
                        (upravené
                25.3.2010
                o
                13:06)
                        |
            Karma článku:
                11.48
            |
            Prečítané 
            8126-krát
                    
         
     
         
             

                 
                    Už 6 rokov pracujem ako živnostník a na túto otázku som odpovedal nespočetne krát. Odpoveď na ňu nie je jednoduchá ani jednoznačná a každý si bude musieť urobiť názor z jeho pohľadu na vec. Tento článok prináša stručný prehľad výhod a nevýhod živnostníckeho podnikania.
                 

                 Podnikanie, respektíve pracovanie na živnostenský list sa u nás stalo bežne používanou formou zamestnania a veľa firiem ponúka svojim zamestnancom možnosť výberu pracovať na trvalý pracovný pomer (TPP), alebo na živnosť. Samotná práca pritom je presne tá istá, jej náplň, zodpovednosti a rozsah je ten istý. Tak v čom je rozdiel? Rozdiel je v právach a povinnostiach zamestnávateľa voči zamestnancovi, takisto ako v právach a povinnostiach samotného zamestnanca, v našom prípade živnostníka. Je teda živnosť lepšia ako TPP?   Výhody podnikania na živnosť   1) Správa financií   Za najdôležitejšiu výhodu považujem možnosť úplného riadenia financií. Živnostník tak na rozdiel od zamestnanca na TPP nenecháva žiadne operácie na jeho zamestnávateľovi (ak odhliadneme od toho, že ním je v danej chvíli on sám) a všetko si robí sám, resp. s pomocou účtovníka. Rozmenené na drobné to znamená, že sám rozhoduje o tom, koľko daní zaplatí, aké budú jeho náklady a príjmy apod. V ďalších bodoch si povieme konkrétnejšie o tom či to prináša viac muziky za menej peňazí.   2) Nižšie daňové odvody   Táto časť je trochu zložitejšia než sa zdá z nadpisu, takže sa na to pozrime bližšie. Živnostník, ktorý si spravuje financie sám rozhoduje o svojich výnosoch a nákladoch. Rozdiel medzi výnosmi a nákladmi tvorí živnostníkov zisk, ktorý mu štát zdaňuje Daňou z príjmu (DP). DP je znižovaná o rôzne položky, ale k tomu prídeme neskôr. Pre jednoduchosť príkladu o tom zatiaľ neuvažujme.   Predstavme si, že existujú dvaja pracovníci, Žigo je živnostník, Tomáš pracuje na trvalý pracovný pomer. Pracujú v rovnakej firme, robia rovnakú prácu úradníka, a navyše si obaja pre súkromné účely kúpili nový počítač za 600 Eur, na ktorom sa budú vo voľných (mimo práce) hrávať svoje obľúbené počítačové hry. Obaja zarobia mesačne 1000 Eur hrubej mzdy. Tomášovi zamestnávateľ automaticky strhne DP vo výške 19%, čiže mu na účet príde 810 Eur. Žigovi zaplatí zamestnávateľ na faktúru 1000 Eur, ale keďže je živnostník, tak pred výpočtom DP si môže uplatniť náklady vynaložené na dosiahnutie toho príjmu - v tomto prípade by to mohol byť napríklad počítač, ktorý si kúpil, a o ktorom bude tvrdiť, že ho potrebuje na svoju prácu. Čiže Žigova mesačná bilancia bude 1000 Eur - 600 Eur (náklady na počítač) = 400 Eur. Z tejto sumy (teda základu, z ktorého sa bude počítať daň) zaplatí DP, ktorá bude 76 Eur, čiže mu oproti Tomášovi z jeho výplaty ostane o 190 - 76 = 114 Eur viac, pretože Tomáš si svoje náklady na súkromný počítač nemôže nikde uplatniť.   Tento príklad je veľmi jednoduchý a situácia je v skutočnosti trochu zložitejšia, pretože ešte existujú odvody do sociálnej a zdravotnej poisťovne a zopár iných faktorov, ale o tom zasa nižšie.   Tento bod je mimochodom zdrojom všetkých škandálov o daňových podvodoch, pretože si firmy a živnostníci do nákladov dávajú veci, ktoré tam nemajú čo robiť.   Výhodou však je, že niektoré náklady ani nemusia byť vaše, aby ste si ich do nákladov zaradili. Predstavme si, že Žigo od zamestnávateľa dostane všetko, čo k práci potrebuje, vrátane tlačiarne k počítaču, takže aj súkromné veci si na nej môže vytlačiť. Má však kamaráta, ktorý pracuje v inej firme na TPP a ten si kúpi tlačiareň. Má od nej blok, ale do žiadnych nákladov si ho nemôže uplatniť. Žigo ho teda poprosí, či by mu blok nedal a následne si ho zaradí do svojich nákladov. Žigo si teda zvýšil náklady, čo znamená, že zaplatí menšie dane. Problém vznikne vo chvíli, kedy sa mu ohlási daňová kontrola a bude chcieť onú tlačiareň vidieť na vlastné oči. To by musel Žigo utekať za kamarátom a požičať si ju od neho na ukázanie daňovej kontrole. Ak si takto dal do nákladov okrem tlačiarne aj fotoaparát, kopírku, skener, pričom každý blok je od iného kamaráta, tak sa celkom nabehá. Šanca na vznik tohto problému je však pomerne nízka, pretože tlačiareň isto nepresiahne sumu, pri ktorej sa musí odpisovať viac rokov, a teda pokiaľ daňová kontrola nepríde ešte v daný rok, tak Žigo nemusí nič ukazovať a s kľudným svedomím môže povedať, že už ju vyhodil, lebo bola stará. A ďalší rok si Žigo môže „kúpiť" inú tlačiareň a dať si ju opäť do nákladov.   3) Živnostník zarobí viac   Tento bod úzko nadväzuje na predchádzajúci, pretože logika vraví, že ak zaplatím menej na daniach, tak zarobím viac. Ale ešte ho možno doplniť o ďalší aspekt - o odvody do poisťovní. Odvody do poisťovní sa totiž vypočítavajú z daňového základu (DZ). DZ je suma peňazí, ktorá bude zdanená DP. DZ je teda rozdiel medzi výnosmi a nákladmi, znížený o nezdaniteľné položky. Tie tvorí kopec vecí, predovšetkým však nezdaniteľná časť platu, platby do životnej poistky, odpočty na deti apod. Kto chce vedieť viac nech si prečíta tento článok o nezdaniteľných položkách. Vyššie sme si povedali, že Žigov DZ teda bude 400 Eur, kým Tomášov 810 EUR. Čím vyšší je DZ, tým vyššie odvody do sociálnej a zdravotnej poisťovne budú platiť. Odvody sa nevypočítavajú percentom, ale tabuľkovým prepočtom, takže nejde urobiť nejaký jasný príklad. Napriek tomu je však výsledkom to, že Žigo, pretože má nižší DZ bude platiť nižšie odvody do poisťovní. Ak je dostatočne šikovný, tak minimálne sumy.   To samozrejme vedie k tomu, že Žigo si do svojho dôchodku sporí menšiu sumu peňazí, ale o tom aký bude naozaj Žigov a Tomášov dôchodok si vedia urobiť celkom jasnú predstavu pri pohľade na politiku krajiny. A ak je Žigo múdry, tak takto ušetrené peniaze si môže vložiť do samostatného kapitálového sporenia, respektíve životnej poistky, ktorá mu, ako sme si povedali vyššie, (do určitej výšky) zníži jeho DZ. Viac informácii nájdete tu.   4) So živnostníkom má zamestnávateľ menej práce   S pracovníkom na TPP sa zamestnávateľovi tvorí práca, ktorú so živnostníkom nemá, lebo ten si rieši všetko sám. Pri TPP musí zamestnávať mzdovú účtovníčku, ktorá spracováva odvody, podklady pre daň, výplatné pásky a podobne. To všetko stojí zamestnávateľa peniaze a nervy. Z tohto pohľadu má s živnostníkom výrazne menej byrokratickej práce, pretože len spracuje jeho faktúru.   5) Živnostník stojí zamestnávateľa menej peňazí   Tento bod je trochu sporný, ale moja skúsenosť je bohužiaľ taká. Mnoho ľudí nevie, že za zamestnanca na TPP platí zamestnávateľ časť odvodov. Skutočnosť je taká, že z hrubej mzdy si zamestnanec na TPP platí 13,4% do sociálnej a zdravotnej poisťovne (SP a ZP). Jednotlivé percentá sa dočítate na stránke Veličiny používané v mzdovom účtovníctve 2010.  Ale okrem toho za neho do tej sociálnej poisťovne platí zamestnávateľ 35,2 %. Skúsme si dať príklad. Tomáš zarobí 1000 Eur hrubej mzdy. Z toho zaplatí do SP a ZP 134 Eur, zvyšok sa mu odpočítaní ďalších nezdaniteľných položiek zdaní DP. Ale z pohľadu zamestnávateľa je to tak, že ak chce Tomášovi vyplatiť 1000 Eur, tak okrem toho musí do SP a ZP za neho zaplatiť 352 Eur. Zamestnávateľa teda Tomáš nestojí 1000 Eur, ako by sme sa mohli nazdávať, ale oných 1352 Eur.   Ak by teda Žigo toto všetko vedel, tak môže diskutovať so svojim zamestnávateľom, aby mu namiesto oných 1000 Eur dal 1352 Eur, pretože len vtedy budú náklady na neho a na Tomáša pre firmu rovnaké (ak odhliadneme od nákladov firmy na mzdové účtovníctvo). Ako som ale písal vyššie, v skutočnosti s týmto argumentom uspieť nejde a živnostník dostane menej peňazí, ako by mu podľa tohto výpočtu náležalo.   Navyše zamestnávateľ živnostníkovi nemá povinnosť vyplácať príspevky na stravné lístky. Je to taká dvojsečná zbraň. On totiž zamestnávateľ časť sumy stravných lístkov prepláca a zvyšok si platí zamestnanec z výplaty. Živnostník si ich musí (môže, ale nemusí) celé hradiť sám, ale na druhej strane sa mu to zarátava do nákladov podnikania, čiže mu to znižuje DZ.   6) Živnostník sa môže stať dobrovoľným platcom Dane z pridanej hodnoty   Tento bod je veľmi komplikovaný na jednoduché vysvetlenie a ukážku príkladu, takže v tejto chvíli to treba brať ako fakt a výhodu a v budúcnosti možno na túto tému vznikne samostatný článok.   7) Živnostník môže pracovať naraz na viacerých projektoch   Ak aj predpokladáme, že Tomáš a Žigo robia rovnakú prácu, tak Tomáš má obmedzenia na to koľko práce môže urobiť na vedľajší pracovný pomer, prípadne na dohodu o vykonaní práce. Toto sa Žiga netýka. Kľudne môže pracovať 24 hodín denne, na sedemnástich projektoch a bez problémov môže fakturovať paralelnú prácu na projektoch. Samozrejme v rámci istých logických princípov. Nebude problém ak v rámci cesty po Bratislave vyfakturuje fotenie fotiek na internetovú stránku pre tri rôzne firmy, ale asi bude ťažko obhájiteľné ak vykáže, že bol celý deň na stretnutí s klientom v Žiline, ale do účtovníctva si dá blok o tankovaní benzínu v Košiciach.   8) Živnostník môže pri otvorení živnosti žiadať Úrad práce o príspevok   Ak sa nemýlim tak aj pre zamestnanca na TPP existuje nejaká forma príspevku za nájdene pracovného miesta, ale príspevok pre živnostníka je takmer istý (teda, isté je, že existuje, nie, že vám ho automaticky dajú). Väčšinou sú to účelovo viazané nenávratné príspevky, ale suma cca 2500 Eur, ktorú netreba vrátiť vždy poteší. Viac informácií o tomto príspevku nájdete na stránkach Úradu práce alebo na stránke Príspevku z úradu práce na začatie podnikania.   Nevýhody podnikania na živnosť   Nevýhody sú zväčša druhou stranou mince výhod.   1) Živnostník má väčšie výdavky   Živnostník si totiž musí platiť účtovníka, ktorý mu jeho účtovnú agendu spracuje. Nie je to samozrejme nevyhnutnosť (platiť si účtovníka), sú živnostníci, ktorí si robia účtovníctvo sami, ale ja osobne to nepovažujem za celkom dobrý nápad a to ani napriek tomu, že ako absolvent Ekonomickej Univerzity by som to asi zvládol. Ide o to, že dobrý účtovník sleduje zmeny v zákonoch a informuje svojich klientov o to čo treba zmeniť, čo, kam a kedy zaplatiť, dáva pozor na správne účtovanie položiek apod. Živnostník síce v konečnom dôsledku preberá celkovú zodpovednosť za vedenie účtovníctva, ale zároveň môže mať zmluvu s účtovníkom, ktorá upravuje situácie, kedy by mal živnostník s účtovníctvom problém.   2) Živnostník oficiálne nemá právo na dovolenku a zamestnanecké výhody   Túto časť môže upravovať zmluva medzi živnostníkom a zamestnávateľom, ale oficiálne to živnostník nemá právo od zamestnávateľa žiadať. Zároveň musí živnostník musí zabudnúť na 13. a ďalšie platy. Je dobré si preto dĺžku dovolenky či ostatné zamestnanecké benefity dohodnúť už pri podpise zmluvy.   3) Živnostník oficiálne nemá právo na skúšobnú dobu, odstupné a ostatné ochranné lehoty   Živnostník v podstate nenastupuje do práce, ale na pracovný projekt, resp. vykonáva prácu na základe zmluvy o dielo, alebo inej vhodnej zmluvy. Čiže nastupuje ako odborník na danú činnosť a ako takému mu neprináleží odstupné, alebo lehoty výpovednej doby. Všetky tieto veci môže obsahovať ona zmluva, ktorú podpíše zamestnanec a zamestnávateľ, ale v prípade problémov, ktoré nie sú zahrnuté v zmluve neexistuje žiadna odvolacia inštancia ako je to v prípade TPP (Zákonník práce). Existuje Obchodný zákonník upravujúci obchodné vzťahy, ale špecifickosť jednotlivých situácií medzi živnostníkom a zamestnávateľom môže byť neprepracovaná, alebo sporná.   4) Živnostník má viac byrokratickej práce   Za svoju prácu totiž musí za svoju prácu vystavovať faktúry, zbierať doklady o nákladoch, komunikovať s účtovníkom a podľa jeho rád upravovať platobné príkazy v banke apod.   5) Živnostník nemá istotu práce   Tento bod je opäť trocha sporný, ale na pozadí sa predsa len premieta. Zmluva so zamestnávateľom môže upraviť rozsah a dĺžku projektu, ale v prípade práce na neurčito môže byť jeho zmluva ukončená v podstate zo dňa na deň, bez akýchkoľvek výpovedných lehôt, odstupných súm apod.   Príklad výpočtu ročnej odmeny živnostníka a zamestnanca na TPP   Aby sme mohli spočítať rozdiel v ich odmenách musíme stanoviť zopár základných faktov:   Obaja vykonávajú tú istú prácu, čiže Žigo ani Tomáš nerobia na žiadnych ďalších projektoch. Obaja si platia svoje kapitálové životné sporenie v ideálnej výške (minimálnej na to, aby sa im všetky náklady presne premietli do výšky nezdaniteľnej časti. Do tej sa totiž započítava len určená výška a teda ak by aj platili viac, z daňového hľadiska by im to nebolo nič platné), ktoré sa im započítava do nezdaniteľného DZ. Obaja sú bezdetní, slobodní, niekto z nich nie je telesne postihnutý. Keďže nie sme schopní určiť aké vysoké dokáže Žigo vygenerovať náklady nechajme ho využiť zákonnú inštanciu paušálnych nákladov, ktorá hovorí o tom, že pokiaľ si ju živnostník uplatní, nemusí predkladať žiadne bloky, nákladové faktúry ani nič podobné, jednoducho spočíta, že jeho náklady boli vo výške 40% (keby bol remeselník, tak dokonca až 60%). Predpokladajme, že Tomáš dostáva výplatu 1000 eur mesačne, ale Žigo ako živnostník dokázal presvedčiť šéfa o jeho odmene 1200 Eur, ako kompenzáciu za to, že si všetky odvody a náklady apod. platí sám. Zároveň si Žigo musí platiť odvody sám a keďže bol v minulom roku šikovný do Sociálnej a zdravotnej poisťovne platí minimálne odvody. Obaja majú účet v banke, kam ich chodí mesačná výplata.             Žigo (živnostník)                                                                                             1.     Hrubé výnosy:     12     mesiacov     x     1 200,00 €     =     14 400,00 €       2.     Paušálne náklady:     40%           z     14 400,00 €     =     5 760,00 €       3.     Zisk:           14 400,00 €     -     5 760,00 €     =     8 640,00 €                                                               Odpočítateľné položky                                           4.     nezdaniteľné minimum     12     mesiacov     x     335,47 €     =     4 025,64 €       5.     odvody do Soc. poist. (minimálne)     12     mesiacov     x     96,60 €     =     1 159,20 €       6.     odvody do Zdrav. poist. (minimálne)     12     mesiacov     x     44,74 €     =     536,88 €       7.     odpočet za životnú poistku     1     ročne           398,33 €     =     398,33 €                                                         8.     Základ dane                                   2 519,95 €       9.     Daň z Príjmu     19%                       =     478,79 €                                                               Napriek paušálnym nákladom ale niečo   ešte reálne zaplatiť musí:                                                 účtovník                                   300,00 €                                                               Žigovi teda v rukách reálne ostane                                    11 526,80 €                                                                Tomáš (zmestnanec na TPP)                                                                                             1.     Hrubé výnosy:     12     mesiacov     x     1 000,00 €     =     12 000,00 €       2.     Paušálne náklady:     0%           z     12 000,00 €     =     -   €       3.     Zisk:           12 000,00 €     -     -   €     =     12 000,00 €                                                               Odpočítateľné položky                                           4.     nezdaniteľné minimum     12     mesiacov     x     335,47 €     =     4 025,64 €       5.     odvody do Soc. Poist.     13,4%     z výplaty     x     12     =     1 608,00 €       7.     odpočet za životnú poistku     1     ročne           398,33 €     =     398,33 €                                                         8.     Základ dane                                   5 968,03 €       9.     Daň z Príjmu     19%                       =     1 133,93 €                                                               Tomášovi teda v rukách reálne ostane                                    8 859,74 €                                                                Rozdiel                                   2 667,06 €           Ako vidíme, za daných podmienok, je rozdiel medzi Žigom a Tomášom zaujímavý. Samozrejme ráta s tým, že Žigo si vymôže na šéfa oných 1200 Eur namiesto 1000 Eur. Ak by tak neurobil, rozdiel medzi nimi by klesol na 540 Eur ročne, čo tiež nie je na zahodenie. Treba si však uvedomiť, že šikovný živnostník vie vygenerovať náklady vo výške viac ako 40% bez ohľadu na to, v akej oblasti podniká.   Záverom   Sumy sú aktuálne k marcu 2010 a dúfam, že som na nič podstatné nezabudol. Pokiaľ by sa tak stalo, kľudne ma opravte. Dúfam, že tento článok vám pomôže pri rozhodovaní sa o tom, či sa viac oplatí živnosť, alebo TPP.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (37)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Markovič 
                                        
                                            Je Solčanka naozaj zdravá?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Markovič 
                                        
                                            Sme lepší ako taxikári ošklbávajúci turistov?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Markovič 
                                        
                                            Ráno v Jemene
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Markovič 
                                        
                                            Zmysel života
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Maroš Markovič 
                                        
                                            Nebuďte hlúpym užívateľom internetu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Maroš Markovič
                        
                     
            
                     
                         Ďalšie články z rubriky ekonomika 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Podnikanie vysokých škôl a zamestnávanie študentov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marián Pilat 
                                        
                                            Komunálne voľby sa skončili, ale pre mňa sa všetko len začína
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Juraj Miškov 
                                        
                                            Gabžigovo?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alexander Ač 
                                        
                                            Nečakaný ropný šok
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Spišák 
                                        
                                            Sociálne istoty pre mladých.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky ekonomika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Maroš Markovič
            
         
        markovic.blog.sme.sk (rss)
         
                                     
     
        Fotograf ignorujúci absenciu talentu. Občan ignorujúci existenciu televízoru.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    15
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1853
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Cormac McCarthy: Cesta
                                     
                                                                             
                                            Douglas Adams: Losos pochybnosti
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Simek &amp; Grossman: Navstevny den 1-7
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Pupek
                                     
                                                                             
                                            MO Plusko
                                     
                                                                             
                                            Hogno
                                     
                                                                             
                                            Pospo
                                     
                                                                             
                                            Peter Rolný
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Poviedky.com
                                     
                                                                             
                                            Moje fotky na Picassa
                                     
                                                                             
                                            Mo Plusko
                                     
                                                                             
                                            Bazardielov.sk
                                     
                                                                             
                                            Biorecepty.sk
                                     
                                                                             
                                            Predatfotky.sk
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




