
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Karol Herceg
                                        &gt;
                Nezaradené
                     
                 Človek nemá nikdy naozaj dosť toto, čo najmenej potrebuje 

        
            
                                    6.6.2010
            o
            23:18
                        (upravené
                6.6.2010
                o
                23:24)
                        |
            Karma článku:
                3.54
            |
            Prečítané 
            525-krát
                    
         
     
         
             

                 
                    Je večer a mne sa dostal k nahliadnutiu film 11. hodina (2007). Krásny film o planéte Zem, hrozivé zábery a krutá pravda. Síce je najviac spomína USA a všeobecne Amerika, problém znečisťovania a devastovania prírodného prostredia má globálne rozmery. Dovolil som si teda, niekoľko myšlienok poznamenať a napísať do blogu...
                 

                 
pohľad na vodnú prihradu RužínKahe
     
 Otázka, ktorá nás ani nenapadne keď sa pozrieme na strom je „aký ma objem?“, „koľko vody dokáže zadržať?“.   Ukázalo sa, že to je 216 tisíc litrov vody pri 20 až 30 cm prívalového dažďa. Toľko vody dokáže zachytiť, zabrániť jej aby tiekla ďalej, nasiaknuť ju ako huba, vyčistiť ju a vrátiť ju naspäť do pôdneho systému. Dajte preč jeden strom a máte tu záplavy a eróziu pôdy. Miestne podzemné vody prídu o 216 tisíc litrov vody a tá voda iba tečie ďalej, ohrozuje ľudí a nakoniec znečistí oceán.       Naskytla sa otázka, koľko by stálo to čo pre nás robí Zem zadarmo.. Koľko by napríklad stálo odstrániť oxid uhličitý z ovzdušia a vrátiť doňho kyslík. Momentálne neexistuje technológia ktorá by dokázala opeliť všetky rastliny v tak krátkom čase ako to dokáže príroda. Ak by sme niečo také mali, koľko by stála prevádzka toho niečoho?   Je možné približne vypočítať, koľko by nás stálo nahradiť prírodu.   Ekologický ekonóm Róbert Constanza (http://en.wikipedia.org/wiki/Robert_Costanza), pred niekoľkými rokmi odhadol, že urobiť to, čo pre nás príroda robí zadarmo by stálo 35 x 1018 dolárov. Aby sme to dali do súvislosti: keby ste v tom čase sčítali ekonomiky všetkých krajín na svete, vyšlo by to 18 x 1018 dolárov. Čiže ekonomika celého sveta by nestačila na pokrytie toho, čo pre nás robí príroda zadarmo.       To sú len dve hlboké myšlienky z celého filmu v trvaní 1 h a 34 minút.   Kto si nájde čas a v tomto článku ho aspoň trochu zaujala téma filmu, nájdite si film na internete a pozrite si ho. Je to naozaj pravda a krutá realita  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (6)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Karol Herceg 
                                        
                                            Ako zaniká právo na reklamáciu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Herceg 
                                        
                                            Chyby sa nevypalacaju
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Herceg 
                                        
                                            Vyjadrenie k článku Rodič- kamarát alebo nepriateľ
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Herceg 
                                        
                                            Rodič - kamarát alebo nepriateľ
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Herceg 
                                        
                                            Nočná seansa pouličanov
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Karol Herceg
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Karol Herceg
            
         
        herceg.blog.sme.sk (rss)
         
                                     
     
        človeku sa nechce žiť v prítomnosti a pritom prítomnosť je jediná podobná večnosti, keďže minulosť je už daná a budúcnosť neistá
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    539
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




