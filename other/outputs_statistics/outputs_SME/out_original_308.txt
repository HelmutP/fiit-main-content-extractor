
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s medveďom - Domáci zemiakový majonézový šalát 

        
            
                                    25.11.2007
            o
            12:00
                        |
            Karma článku:
                10.37
            |
            Prečítané 
            44999-krát
                    
         
     
         
             

                 
                    Tak a je to tu. Blíži sa to nezadržateľne a kto to nemá rád, aj tak sa tomu nevyhne. Niektorí z nás to nazývajú predvianočné šialenstvo, iní radostné očakávanie sviatkov Pokoja so všetkými rituálmi a zvykmi čo k tomu patria a je samozrejme množstvo takých, ktorí na to kašlú.
                 

                  Nech už je tak, či onak, my medvede sme zveri šikovné a dané sľuby radi plníme. Začíname preto so sériou vianočných receptov. Dnes to bude zemiakový šalát majonézový, budúci týždeň kapustnica a potom prídu na rad nejaké sladké témy.     A čo očakávame od vás? Nuž od vás očakávame to najdôležitejšie. Boli by sme veľmi radi, keby ste sa aktívne zapojili do diskusie k receptu svojimi postrehmi, nápadmi  a skúsenosťami. Kto máte nejaké grify, či dobré nápady a vylepšenia neváhajte a podeľte sa s nimi s ostatnými.     Potrebujeme:     Na dobrý zemiakový šalát predovšetkým zemiaky varného typu A. Ostatne sa aj volajú šalátové. Uvaria sa do mäkka a pri tom sa nedrobia. Vynikajúci variant sú ružiaky. Ja som v tomto prípade použil 2,5 kg zemiakov. Ďalej potrebujeme koreňovú zeleninu. Mala by to byť mrkva, petržlen a zeler. Najmä ten zeler je dosť dôležitý. Dáva šalátu vynikajúcu príchuť. Napriek tomu sa mnohí z nás tejto skvelej zelenine vyhýbajú. Ďalej je to, pohár 7,5 dcl kyslých uhoriek, konzerva sterilizovaného hrášku, 5 vajec,  soľ, čierne korenie, vegeta a plnotučná horčica.     Na majonézu potrebujeme:      5 vajec, olej, ocot, šľahačkovú smotanu, čierne korenie, cukor, soľ a horčicu.          Tak tak, milí moji. My medvede si majonézu nekupujeme v obchode, ale si ju doma sami pripravujeme. Môžete teraz namietať, že je to zbytočné zdržovanie, veď tá 'kúpenská' je tiež dobrá a ... možno máte pravdu. Je tu však jeden moment. Keď si majonézu pripravíte sami zistíte:     a) že ste veľmi šikovní kuchári     b) že šalát získal absolútne skvelý domáci šmak     c) že pri varení nie je vždy dôležitý čas a nenáročnosť prípravy, ale aj láska s ktorou pokrm a poťažmo svojich hostí zahrniete     Tak a teraz spomienka.     Bolo to pred dobrými dvadsiatimi piatimi rokmi na Adama a Evu u nás doma v Štiavnici.     "Ty mama. Ten šalát je dnes iný. A je taký ľahší, sviežejší. Čo sa stalo?"     Mama dumá, dumá, ako je to možné, keď jej naraz svitne:     "Veď ja som tam zabudla pridať cibuľu."     Nuž a odvtedy robíme majonézový šalát bez cibule. Niekedy skúste. Bude oveľa ľahší a nesmierne jemný.     Tak a dosť rečí. Ide sa variť. Deduško Simsalabim na to opäť kašle, takže zemiaky uvaríme v šupke do mäkka a necháme vychladnúť. Koreňovú zeleninu očistíme a tiež uvaríme. Prvých päť vajíčok uvaríme na tvrdo. Všetky ingrediencie (zemiaky, vajíčka, koreňovú zeleninu a kyslé uhorky) nakrájame na malé kocky. Samozrejme zemiaky pretlačíme cez sieťovaný krájač na zemiaky.     Zeleninu nasypeme do väčšej nádoby, aby sa nám s ňou ľahšie pracovalo. Sterilizovaný hrášok nasypeme vrátane slaného nálevu. Ďalej pokračujeme s nakrájanými uhorkami a prilejeme aj precedený uhorkový sladko - kyslý nálev. Ja keď použijem od 2 kg zemiakov viac (...a to je vždy:)), nalievam celý nálev zo 7,5 dcl pohára. Pri menších množstvách pridávame iba časť, aby šalát nebol príliš kyslý a riedky.      Ďalej pridáme trochu soli, čierne mleté korenie, lyžicu vegety a aspoň dve lyžice plnotučnej horčice. Všetko dobre premiešame a necháme odstáť. Už teraz mi na to ten môj Medveď chodí. :O)         Tak a ideme na majonézu. Najprv si z ďalších piatich vajec oddelíme žĺtka od bielkov. Poviem vám, bola to tento krát fuška, lebo vajíčka neboli veľmi kvalitné. Žĺtka sa rýchlo rozpadali. Značku radšej nespomeniem.        Žĺtka vylejeme do menšieho kastrólčeka. Na jeden žĺtok pridáme 0,5 dcl vody a lyžicu octu. Metličkou dobre rozšľaháme. Za ten čas necháme vo väčšom hrnci zovrieť vodu. Menší do neho potom ponoríme a vo vriacom vodnom kúpeli šľaháme.          Nuž malo by to trvať aspoň 20 - 25 minút a mali by sme šľahať dosť intenzívne. Pozor, nemusíme šľahať v jednom kuse po celý čas, ale aspoň miešať by sa to malo nepretržite. Tak striedame miešanie so šľahaním, zavoláme na pomoc postupne všetkých členov domácnosti a kým sa vystriedajú, dielo je hotové. Výsledok vidíme nižšie. A u nás sa veľmi podarilo.          A teraz upozornenie. Ak nám to nepodarí do krémova, teda bude to redšie, absolútne nevadí. Zakrátko zistíme prečo.     Majonézový základ teraz necháme vychladnúť. Ideálny na to je balkón.  Potom ho vlejeme do mixéra. Mixér spustíme na najvyšší stupeň a postupne prilievame olej. Ak však chceme majonézu skutočne hustú, potrebovali by sme na jedno vajce až zhruba jedno celé deci oleja. V našom prípade by to bolo spolu 0,5 litra. Ak si želáme mať skutočne hustú majonézu, tak po tom, čo sme vliali 3 dcl oleja, prúd oleja zmiernime a už prilievame po nitkách s malými prestávkami. Sami zistíme, kedy nám majonéza zhustne do krémova. Je to individuálne od kvality vajec aj oleja.          Tiež to však bola moja mama, čo ma naučila, že to vôbec nie je nutné a že základ v šaláte je hlavne v chuti a nie konzistencii majonézy. Preto zostaneme len pri troch deckách oleja. Majonéza bude redšia, ale keď sa nám spojí so šalátom, vôbec to nebude vadiť. Šalát bude skvelý, kompaktný a pri tom šťavnatý.     Tak majonéza je vyšľahaná a teraz do nej pridáme aspoň 2 - 3 lyžice cukru, 2 lyžice horčice (správne aj do šalátu aj do majonézy) kávovú lyžičku soli, šľahačkovú smotanu a ešte premixujeme. Samozrejme aj šľahačka nám majonézu môže zahustiť. Nemali by sme však zmútiť maslo.:) A prečo šľahačka a nie kyslá smotana? Lebo šľahačka je do majonézového šalátu proste jedinečná. Dodá mu jemnosť a kyslosť si pritom môžeme upraviť podľa vlastnej ľubovôle. Je výborné nechať pred použitím šľahačku skysnúť.          Tak a teraz našu riedku, ale chutnú majonézu vlejeme do šalátu a dobre pomiešame.          A ochutnávame a ochutnávame. Môžeme pridávať, soľ, cukor, horčicu, vegetu, korenie či ocot podľa vlastnej ľubovôle a chuti. Potom necháme šalát aspoň 12 hodím odstáť.            No a nižšie som ilustračne vyfotil náš šalát spolu so 7,5 dcl zaváraninovým pohárom, aby ste mali predstavu o množstve výsledného produktu. Je slušné. Pokiaľ chceme množstvo šalátu navýšiť, pri použití povedzme 3,5 kg zemiakov nám bude stačiť 7,5 dcl pohár uhoriek a jedna konzerva hrášku. Mali by sme však navýšiť množstvo koreňovej zeleniny a namiesto 5 vajec natvrdo by sme mohli použiť 6 - 7. Množstvo majonézy meniť nemusíme.          Dobrú chuť a tešíme sa na vaše  podnety.           tenjeho  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (108)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




