

 
 
 Im v skutočnosti nezáleží na histórii, ani na Svätoplukovi. Sú to myslením kolektivistickí ľudia, ktorí túžia zotročiť každú tvorivú alebo slobodnú myšlienku, túžia zlomiť v ľuďoch vieru v seba samých. 
 Toto je Svätopluk, váš kráľ, toto sú tri prúty, ktoré zviažeme, aby sme držali spolu. Kto nepôjde s nami? 
 Títo predstavitelia nášho štátu majú radi pompu, spev a ovácie. Individuálne slobodné myslenie nemajú radi, pretože by nikdy nepristúpili na túto nikto nevie presne ako vyšpekulovanú oslavu. Oni chceli len kričať, čo najhlasnejšie. 
 Nejdú svojou cestou. Kráčajú po cestách, kde im tlieskajú. V dave sa cítia najlepšie, lebo tam nie je vidno ich malodušnosť. V ich hlavách nie je jediná slobodná a tvorivá myšlienka. Sú prisatí na myšlienkach tvorivých ľudí, vykrádajú ich, znehodnocujú a budujú zotročenú spoločnosť. Zotročení potrebujú otrokára. 
 Toto sa ma nedotýka, toto sa udialo niekde na hradnom kopci a televízia z toho vysielala obrázky. Môj televízor je iba kus nábytku. Niečo z neho prská, keď ho vypínam. 
 V meste stojí ďalšia bezduchá socha. 
   

