
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Šálek
                                        &gt;
                Všeobecne
                     
                 Malcolm McLaren 

        
            
                                    9.4.2010
            o
            21:03
                        (upravené
                9.4.2010
                o
                21:56)
                        |
            Karma článku:
                5.09
            |
            Prečítané 
            619-krát
                    
         
     
         
             

                 
                    Bývalý manažér Sex Pistols zomrel vo veku 64 rokov.
                 

                 Názory na Malcolma McLarena a jeho prínos do punku sa zvyknú rôzniť. Mnohí mu vyčítajú, že za Sex Pistols bola iba snaha zviditeľniť sa a zarobyť ťažké prachy. Možno. Ale ich existencia spustila niečo, čo žije a funguje až dodnes.       McLaren dal Sex Pistols dokopy v roku 1976, zo štyroch chalanov, ktorí navštevovali jeho obchod Sex. Ten založil spolu so svojou partnerkou a neskôr svetoznámou návrhárkou Vivienne Westwood a bol zameraný na všetko od rock n rollu cez motorkárov až po S&amp;M. Mclarenovou ideou bolo vytvoriť britskú verziu glam-rockových New York Dolls.  Johnny "Rotten" Lydon, Steve Jones, Paul Cook a Glen Matlock znamenali skutočný prielom vo vývoji rock n rollu. Faktom ostáva, že Pistols boli komerčne úspešnou kapelou umiestňujúcou sa v mainstreamových hitparádach a vystupujúcich v televízii. Fungovali z väčšej časti podľa pokynov McLarena (aj keď treba uznať, že hudbu a texty si skladali sami). McLaren neskôr po vyhodení Matlocka dotiahol do kapely legendárneho Sida Viciousa. Hlavne preto, že "kurevsky dobre vyzeral" a kapele tým zabezpečil ešte väčšiu publicitu.       Najdôležitejšie ale je, že aj keď kapela fungovala len do roku 1978, ovplyvnila obrovský počet ďalších kapiel. A keď komerčná mánia punku koncom sedemdesiatych rokov opadla a všetci pózeri a bohaté deti prešli na post-punk a new wave (na tejto scéne pôsobil neskôr aj McLaren, asi s úmyslom zas niečo z toho vytrieskať), vynorili sa skutočné kapely hrajúce pre ľudí, ktorí mali o hudbu aj celú subkultúru úprimný zájem. Či už to boli mladšie kapely štýlu Punk 77, alebo kapely hrajúce Punk 82, Oi! alebo Hardcore. A táto scéna, ako som napísal vyššie, žije a funguje dodnes, bez zbytočného mrzačenia módnymi trendami a komerciou (samozrejme ak si odmyslíme neopunk, Emo a podobné tragédie). Na začiatku tohoto všetkého stáli Sex Pistols (spolu s pár ďalšími kapelami). Preto sa mi zdalo vhodné pripomenúť ich objaviteľa aspoň pár vetami. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Egypt a prevrat
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Arabský komunista a Pražská jar
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Jichak Šamir
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Egypt po voľbách
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Šálek 
                                        
                                            Situácia v Sýrii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Šálek
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Šálek
            
         
        salek.blog.sme.sk (rss)
         
                                     
     
        Politológ so záujmom o dejiny, subkultúry a iné veci, ktoré človeka ťažko uživia.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    44
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    878
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politika
                        
                     
                                     
                        
                            Všeobecne
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Marek Čejka- Izrael a Palestina
                                     
                                                                             
                                            Irvine Welsh- Lepidlo
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Zdenek Muller
                                     
                            
             
         
            

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




