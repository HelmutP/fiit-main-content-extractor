

 ©®© 
      Tento článok si vyžaduje úpravu, aby zodpovedal vyššiemu štandardu kvality. 
 ©®© 
      Táto veta je prvá v tomto článku. 
 ©®© 
      Táto myšlienka má dvadsaťpäť slov. 
 ©®© 
      Dnes je piatok trinásteho. 
 ©®© 
      Tvoj sused má zároveň nízky aj vysoký tlak. 
 ©®© 
      Slovenská republika je jednoznačne najbohatším štátom na svete, s najvyšším podielom HDP na obyvateľa. 
 ©®© 
      Slovensko je podľa očakávania odborníkov najchudobnejším štátom tejto planéty, s najvyšším počtom chudobných obyvateľov v rámci štatistiky OSN. 
 ©®© 
      Na Slovensku by ste sa márne snažili nájsť akýkoľvek bordel. 
 ©®© 
      Včera sa konalo stretnutie našich pätnástich kozmonautov, podujatie podporil aj prezident Ján Slota. 
 ©®© 
      Pred troma dňami opustil Slovenskú republiku posledný člen Komunistickej strany Slovenska (podľa recipročnej zmluvy s Raulom Castrom). 
 ©®© 
      Voľby v júni tohto roka vyhrá Strana maďarskej koalície. 
 ©®© 
      Mali by sme urýchlene zhromažďovať všetky dostupné materiály okolo osoby arcibiskupa Jána Orola, aby jeho posmrtné svätorečenie nebolo zdržované našou neschopnosťou. 
 ©®© 
      Všetci úradníci nášho štátu sa tešia skvele fungujúcej ekonomike (chodia sa k nám učiť zástupcovia vyspelých štátov Eurozóny). 
 ©®© 
      Neexistuje ani jedno jediné ministerstvo Slovenskej republiky, ktoré by malo problémy s akýmikoľvek nástenkami (pod článkom nájdete kontakt na internetový obchod, ktorý dodáva „nové" do 24 hodín). 
 ©®© 
      Totálne celé obyvateľstvo našej krajiny privítalo prijatie takzvaného vlasteneckého zákona (vlajky, CD s hymnou, dresy a aj busta Zakladateľov štátu - idú na dračku). 
 ©®© 
      Všetci podnikatelia platia poctivo dane (niektoré daňové úrady prepúšťali časť zamestnancov zo zamestnania). 
 ©®© 
      Ktorékoľvek zahraničné firmy by tu zostali aj za predpokladu, že by z toho nemali žiadne ekonomické výhody. 
 ©®© 
      Všetci slovenskí futbalisti idú do JAR predovšetkým preto, aby vyhrali majstrovstvá sveta vo futbale 2010. 
 ©®© 
      Všetky kresťanské strany na Slovensku sa vždy a za každých okolností správajú kresťansky. 
 ©®© 
      Ani jeden sociálny demokrat SR nie je vykorisťovateľ. 
 ©®© 
      Ak by bola súťaž o najnárodnejšieho politika Európy, vyhral by to tankista, ktorý tiahne na Budapešť. 
 ©®© 
      Keď bude treba, prezident položí aj život za svojich občanov. 
 ©®© 
      Všetky autá, ktoré sú vyrábané na Slovensku, sú najlacnejšie na svete a ľahko dostupné komukoľvek zo slovenských občanov. Žiadne auto nie je chybové. 
 ©®© 
      Najlepšie sa majú tí, čo nič nemajú. 
 ©®© 
      Tí, čo niečo majú, majú málo. 
 ©®© 
      Tí, čo majú veľa, majú ešte menej, ako iní tam na Západe. 
 ©®© 
      Zajtra žiadne počasie nebude. 
 ©®© 
      Na najbližšie voľby do nášho parlamentu príde voliť 98,89 % riadnych voličov. 
 ©®© 
      21.marca 2010 začína skutočná zima, leto bude definitívne zrušené. 
 ©®© 
      Globálne otepľovanie Zemi vôbec nehrozí, (ako predznamenal aj Václav Klaus). 
 ©®© 
      Čašníci na Slovensku neberú nikdy všimné. (Jazykový ústav Ľudovíta Štúra sa rozhodol vyňať zo slovníka pojem „všimné", takže v nasledujúcom vydaní ho nenájdete). 
 ©®© 
      Nikto z tých, ktorí sa dočítali až sem, sa ani raz nezasmial. 
 ©®© 
      Žiadny návštevník - čitateľ - tohto článku neklikne na obdĺžnik „Páčilo sa mi". 
 ©®© 
      Včera som sa dožil 578 mesiacov. Nikto mi negratuloval. 
 ©®© 
      Lebo medveď - už ani tie medvede nie sú to, čo bývalo za totality. Medveďovi dajú obojok! 
 ©®© 
      Ak vyhrá najbližšie voľby „Most", ešte večer začnú padať pečené holuby do huby. 
 ©®© 
      Kto nemá žiadnu hubu, pôjde si natrhať huby do lesa. 
 ©®© 
      To bude na Zemi raj! 
   
   

