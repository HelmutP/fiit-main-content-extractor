

 
 
Mier, to nie je žiadna rvačka, 
ktorá zachvátila nás, 
ale každá detská hračka, 
teplo, čo sa skrýva v nás. 
 
Mier je radosť, detské radovánky, 
žiadna starosť, letia k nám škovránky. 
Škovránky letia, každý si píska. 
Vojny, hádky, preč z ihriska! 
 
Vojna je len čierna mačka, 
ktorej sa chceme zbaviť. 
Ako? Vyhútať je hračka. 
Mierom! Ten nám to pomôže spraviť! 
 
Mier my všetci radi máme, 
za vojnu ho nepredáme! 
A ak by sa niekto pokúsil?! 
Ej, veď ten by zakúsil! 
 
Takých ľudí vyhnať treba! 
Nie sú potrební pre mier! 
V mieri žiť sa učiť treba. 
To je pravda, to mi ver!
 
 
 
 
 

