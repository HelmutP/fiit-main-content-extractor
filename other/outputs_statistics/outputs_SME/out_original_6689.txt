
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Silvia Mancelová
                                        &gt;
                Nezaradené
                     
                 Sme netolerantná spoločnosť 

        
            
                                    17.5.2010
            o
            17:37
                        (upravené
                22.5.2010
                o
                14:22)
                        |
            Karma článku:
                8.50
            |
            Prečítané 
            2282-krát
                    
         
     
         
             

                 
                    Budúcu sobotu sa koná v Bratislave Dúhový Pride pochod za  zrovnoprávnenie gejov, lesieb, bi, transgender a intersex ľudí. I keď  nepatrím ani do jednej z vymenovaných skupín fandím tomuto pochodu,  pretože rešpektujem právo každého človeka žiť život svojim vlastným  spôsobom, pokiaľ tým neubližuje ostatným.
                 

                 
 www.duhovypride.sk
   V článku sa nebudem zaoberať otázkami či homosexualita je choroba, alebo to nie je choroba, či za inú orientáciu môže rodina alebo trauma, či je možné sexuálnu orientáciu liečbou zmeniť a pod. Na tieto témy už existuje dosť iných zdrojov a kto má záujem má možnosť si ich vyhľadať a sám si spraviť názor.   Chcem poukázať na inú skutočnosť v našej spoločnosti. A tou je nedostatok tolerancie. Tolerovať znamená akceptovať niečo čo sa nám nepáči, s čím možno nesúhlasíme, niečo čo je iné. Nejde teraz iba o reakcie na dúhový pochod. Zamyslime sa, aké sú naše najbežnejšie reakcie keď sa k vám do paneláku nasťahuje cudzinec, keď sa vašim známym narodí zdravotne postihnuté dieťa, keď váš nový kolega v práci je Cigáň alebo keď vaše dieťa sedí v škole v lavici s dieťaťom z detského domova.   Väčšinou naša reakcia na veci ktoré sú iné, neštandardné je negatívna. Odsudzujeme, hodnotíme a občas i zatracujeme. Asi je aj trochu prirodzené, že máme v sebe takéto pocity. Preto si myslím, že aj z tohto dôvodu je dôležité, aby sa  pochod konal. Uskutočnenie takejto akcie vyvolá v ľuďoch rôzne reakcie, ale čo je podstatné vyjdú na povrch tieto naše skryté pocity, ktoré v sebe nosíme. A zrazu možno zistíme, že ako spoločnosť sme naozaj veľmi netolerantná a máme problém akceptovať čokoľvek čo je iné a čo sa nám nepáči.   Tým, že sa začne o veciach verejne hovoriť môže dôjsť aj k tomu, že sa niečo zmení v myslení ľudí. Často krát veci odsudzujeme len preto, že ich nepoznáme a máme strach.   Rozšíreným argumentom odporcov dúhového pochodu je, že homosexualita ohrozuje tradičné hodnoty našej spoločnosťi. Podľa štatistík je na Slovensku rozvedené takmer každé druhé manželstvo. To je hrozné číslo. A kto za toto môže? Kde sú teraz všetky tie pochody za ochranu rodiny a debaty o degenerácii spoločnosti. Neviem si predstaviť ako by väčšia akceptácia homosexuálne orientovaných ľudí mohla ohroziť spoločnosť. Pokiaľ viem s inou orientáciou sa človek narodí, nevyberá si ju. Prijatie tohto faktu by mohlo iba uľahčiť život takto orientovaným ľudom. Zarazili ma prehlásenia istých skupín publikované v médiách, v ktorých sa skoro stavali do pozície Boha a rozhodovali o tom, koho čaká večné zatratenie. Práve teraz by som ocenila, keby sa nedeľná kázeň v kostole venovala tomu, ako skutočne plníme hlavné prikázanie z evanjelia milovať druhých, ako seba samých.   A na záver jedná aktuálna príhoda o tolerancii. Kamarát oslavoval narodeniny a tradične zorganizoval spoločné stretnutie v meste. Oslavu zarezervoval v podniku, ktorý vybral na základe internetovej stránky. Vyzeralo to dobre, stránka hovorila o príjemnom prostredí, dobrej hudbe a širokej ponuke miešaných nápojov. Keď sme s kamarátkami s menším oneskorením  na oslavu prišli, mužská časť partie tam už bola. No nevyzerali nijak nadšene a oslávenec už vôbec. Tvárili sa skôr vydesene. Na otázku čo sa deje som iba dostala odpoveď, že sa mám lepšie pozrieť okolo seba. Či mi nie je nič divné. A fakt. Sedeli sme vo veľkom gay podniku. Tak som potom pochopila tie ich bledé výrazy, chlapci boli trochu citlivki a ťažšie znášali keď sa na nich barman usmial. Nakoniec sme sa rozhodli, že zostaneme. Pivo tam bolo dobré a hudba tiež. Druhá časť podniku, kde sa dalo tancovať bola skôr pre lesby, ale to nám neprekážalo a zatancovali sme si dobre.   Aká by bola reakcia obsluhy a ostatných hostí, keby do bežného podniku prišlo dvadsať homosexuálov? Ani si nechcem predstaviť tie úškrnky. Tiež mohli byť na nás v tom bare protivní a odmietnuť nás obslúžiť s tým že tam nepatríme a máme ísť do svojho podniku. Alebo, že máme ísť inam, pretože sa im nechce na nás pozerať, lebo sme iní ako oni. Nič také sa nestalo. Vzájomne sme sa rešpektovali. Lebo sme prijali, že síce sme iní, ale zároveň  môžeme fungovať vedľa seba. A o tomto by dúhový pochod budúcu sobotu mohol byť.                   

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (325)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Mancelová 
                                        
                                            Štát bez spravodlivosti je veľkým lúpežníctvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Mancelová 
                                        
                                            Ignorácia právneho štátu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Mancelová 
                                        
                                            Ad: Príprava budúcich advokátov bude prísnejšia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Mancelová 
                                        
                                            Legitimita zlatých padákov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Silvia Mancelová 
                                        
                                            Právnických fakúlt je príliš veľa
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Silvia Mancelová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Silvia Mancelová
            
         
        mancelova.blog.sme.sk (rss)
         
                                     
     
        advokátska koncipientka v Prahe
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    35
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4520
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Iné právo - blogy
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Optimista- dokument o Winterovi, zakl. piešťanských kúpeľov
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




