
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Karol Kaliský
                                        &gt;
                PRÍRODA
                     
                 Posolstvo jari 

        
            
                                    15.4.2009
            o
            12:30
                        |
            Karma článku:
                15.45
            |
            Prečítané 
            5882-krát
                    
         
     
         
             

                 
                    Poslov jari je v živočíšnej aj rastlinnej ríši obrovské množstvo. U nás na Slovensku ich vítame každý po svojom. Jedni si vyjdú na nich zo zábavy zastrieľať, napríklad aby sa mohli popýšiť slučím pierkom za klobúkom, potom sú tu takí, ktorí si ich natrhajú do vázičky, aby sa aj doma potešili ich krásou a vôňou skôr ako uvädnú, iným zase ostávajú poslovia jari naveky ukrytí, alebo skôr ukradnutí.
                 

                 No a potom sú tu ľudia, ktorí sa na poslov jari úprimne tešia, očakávajú ich a vítajú, len tak, s otvorenými očami a srdcom. A týmto ľuďom chcem priblížiť niekoľko z tých poslov jari, ktorí najčastejšie symbolizujú prichádzajúcu jar mne.    Okrem tých úplne najbežnejších druhov, ktoré väčšina z nás dobre pozná, ako napríklad trasochvost biely, je pre mňa jedným z najmilších poslov jari trasochvost horský.    Hneď ako začnú po zime rozmŕzať neresiská žiab, začnú sa ozývať hlasitým kŕkaním skokany hnedé. Pozorovať ich môže byť skutočne nevšedným zážitkom.    Ani motýle na seba nenechajú dlho čakať. Táto babôčka osiková rozťahuje krídla, aby zachytila čo najviac lúčov Slnka, ktoré ešte stále nemá takú silu ako v lete.    Niektoré červienky tu prezimovali, iné sa po zime vracajú na staré miesta, aby mohli založiť ďalšiu generáciu.    Jelene už zhodili svoju ozdobu a na hlavách im začínajú rásť nové parohy, pokryté jemným lykom.    V očiach čiernej vretenice akoby horel plameň. Veľkonočné sviatky oslávila síce ešte trochu skrehnutá, ale už v najbližších dňoch to bude len lepšie.    Niektoré chrobáky sa už nevedia dočkať a tak si vyjdú aj na snehové polia, ktoré sa veľmi rýchlo topia. Tento hnojník si so sebou zobral na výlet aj nejakého parazita.    V horských sedlách je pobyt v tomto čase skutočným zážitkom. Druhová pestrosť vtákov, ktoré prelietajú z juhu na sever, vždy znovu prekvapí aj toho najostrieľanejšieho ornitológa.    Vôňa podbeľov neodmysliteľne patrí k jari.    Tetrov hoľniak sa v plnej kráse predvádza v prvých ranných lúčoch, spievajúc svoju pieseň jari menej nápadným sliepočkám.    Zároveň s odchádzajúcim snehom sa objavujú prvé šafrany. V polovici apríla je už lúka v ústí Tichej doliny krajšia ako ten najkrajší kvetinový záhon.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Samozvaní ochranári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            10 rokov po ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            O čom sú (pre nás) Tatry
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Neodrezávajte svojim miláčikom hlavy!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Karol Kaliský 
                                        
                                            Vlčie hory
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Karol Kaliský
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Karol Kaliský
            
         
        kalisky.blog.sme.sk (rss)
         
                        VIP
                             
     
        lesník, trošku aj fotograf prírody (viac na www.wildlife.sk)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    29
                
                
                    Celková karma
                    
                                                12.48
                    
                
                
                    Priemerná čítanosť
                    4833
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            PRÍRODA
                        
                     
                                     
                        
                            FOTOGRAFIE
                        
                     
                                     
                        
                            NEZARADENÉ
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            V čase krízy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Pokorný, Bárta: Něco překrásného se končí
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Janci Topercer
                                     
                                                                             
                                            Juro Lukáč
                                     
                                                                             
                                            Mišo Wiezik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            www.arollafilm.com
                                     
                                                                             
                                            www.wildlife.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Desať rokov úžasnej divočiny v Tichej a Kôprovej doline
                     
                                                         
                       Vlčie hory
                     
                                                         
                       130 vlkov
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




