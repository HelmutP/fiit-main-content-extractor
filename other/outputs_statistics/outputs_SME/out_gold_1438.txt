

  
 Projekt bol dostatočne medializovaný a prinášal množstvo konfliktných situácií. Keď sa napríklad ochranári rozhodli vybudovať zopár drobných stavbičiek (hrádzok) v katastri obce Nižné Repáše, prišli úrady a bez mihnutia oka udelili pokutu. Vraj sme zasiahli do vodného toku bez povolenia na činnosť. Potom sa ochranári rozhodli zrealizovať projekt v suchej eróznej rokline pri obci Tichý Potok.  Zrealizovaný pilotný projekt Modrej alternatívy prispel k zastaveniu priehrady Tichý Potok, napriek tomu že jeho realizátori boli tiež pokutovaní. 
  
 Po 13-tich rokoch sme projekt Modrej alternatívy navštívili, aby sme poodhalili výsledok vplyvu projektu na životné prostredie. A tu je výsledok. V pôvodne suchej zerodovanej rokline bez vody a vegetácie je teraz prostredie plne zelene, zatiaľ čo iné časti Slovenska trpia suchom. Takže priatelia, Modrá aternatíva žije! Poodhaliť toto 13 rokov, zabudnuté tajomstvo pred verejnosťou, dunútil štáb nemenovanej zahraničnej televízie, za čo im patrí vďaka, že sa rozhodli prísť a zdokumentovať projekt, aby ponúkli svojim divákom námet, ako si majú chrániť najvzácnejšie prírodné dedičstvo VODU. 
 Diskutéri k príspevku, žiadajú zverejniť fotky pred realizáciou. Podarilo sa mi zopár nájsť. Ospravedlňujem sa, že zábery nie sú z tých istých miest ako tohtoročné. 
 Stav rokliny pred začatím prác v tábore Modrá alternatíva dokumentuje táto fotka z roku 1995: 
  
 Toto sú prvé hrádzky vybudované v léte 1995 ochranármi z MVO Ľudia a voda, PČOLA a SOSNA. 
  
 Hrádzky na jeseň 1995 - prvé litre zadržanej dažďovej vody v rokline 
  
 Hrádzka v dolnej časti rokliny, stav v roku 1996 - léto 
  
 Tá istá hrádzka po 13-tich rokoch - v krúžku je vyznačená, kde sa nachádza drevená hrádza z horného obrázku pohltená zeleňou. 
  
 Celkový objem pre zadržanie dažďovej vody, ktorý bol vybudovaný počas tábora Modrá alternatíva v roku 1995 je cca 87 m3. Odhadujem, že za 13 rokov projekt zadržal cca 3.500 m3 dažďovej vody a umožnil jej vsiaknuť, oživiť vegetáciu i vrátiť vodu do vodného cyklu. 

