

 ...keby niet osoby Róberta Fica. 
 Áno toho Fica, ktorého populistická ideológia tak často spomína boj proti bohatým, rôznym korporáciám a iným "zdieračom z kapitalistického sveta". 
 Toho Fica, ktorý hlása boj za chudobnejšie vrstvy na úkor rozvoja ambicióznych stredných. 
 Toho Fica, ktorá tvrdil, že finančná kríza sa nás nedotkne a teraz ju považuje za príčinu všetkých nepriaznivých ekonomických a sociálnych ukazovateľov. 
 Toho Fica, ktorý poukazuje na okrádanie občanov a sám posúva zákazky svojím podporovateľom (PPP projekty, bratislavský hrad...) 
 Toho Fica, ktorý niečo vyhlási a za pár dní tvrdí niečo iné (stalo sa to X krát). 
 Fica, ktorý má okrem populisticko-politických aj investigačné schopnosti a dokáže prinášať strhujúce konšpiračné teórie. 
 Fica, ktorý vyhlásil vojnu celebritám a pritom sa mu starajú o kultúrny program počas volebných mítingov. 
 Fica, za ktorého vlády euro prinieslo nové pracovné príležitosti, ktorému slúbili investori, že k nám prídu, ten ktorý ochránil slovenský národ pred Maďarmi... 
 atď., atď., atď. 
   
 Čo to má všetko spoločné s informáciami ohľadom financovania volebnej kampane Smeru?  
 Asi toľko, že Fico očividne a dokázateľne klamal toľkokrát, že bežných politikov by to politicky zruinovalo. 
 Lenže Fico nie je len taký bežný politik. Ak o Mečiarovi kedysi Fedor Gál vyhlásil, že je človekom s neskutočnou túžbou po moci, tak o Ficovi sa to dá povedať takisto.  Ficova charizma však dosahuje väčších rozmerov a to je už čo povedať.  Svojho voliča dokáže presvedčiť, že tráva je modrá a obloha zelená. 
 Pri tomto škandáli by sa dalo povedať, že aj na psa prišiel mráz (alebo ako tu bolo asociované: Prišiel na psa Hanzel).  Myslím si však, že Smeru to nielen voličsky nepoškodí , ale dokonca to môže voličov Smeru zopnúť. Už včera Fico plakal na tlačovke, že je to nehanebný útok na jeho strany pred voľbami. Nepriamo tým vyzval svoje "ovečky", aby sa nezľakli a stáli pri ňom. 
 Je až neskutočné ako môže niekto tak slepo dôverovať politikovi. Bol som svedkom na mítingoch, kedy ľudia s dojatím podávali ruku svojmu spasiteľovi, takže pochybujem, že ho aj v týchto zlých časoch opustia.  Tým ľuďom by Fico musel prísť osobne zobrať z peňaženky 5 EUR aby pochopili, že ten politiik nie je svätý. 
   
 Jedinou šancou je, že sa chytia voliči, ktorí neboli rozhodnutí, prípadne tí, čo nechceli ísť voliť. Ale takisto je otázne, či ďalší politický škandál posilní ich volebnú iniciatívu, alebo naopak posilní ich politickú apatiu a skepsu. 
   
 Väčšina ľudí si už dávno uvedomovala, že za smerom stoja "silné osobnosti", že tie kampane niečo aj stoja. Tento dôkaz im to len potvrdil. 
 Fico však všetko poprie, prípadne to nejak otočí, odpúta pozornosť na iné témy (Maďari, možno povodne...) a pre smerákov bude stále Mesiášom, ktorý ich vykúpi z otroctva kapitalizmu.  
   
   

