
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matúš Baňas
                                        &gt;
                Nezaradené
                     
                 Učitelia, dajte najavo svoj názor! (podporte petíciu za ...) 

        
            
                                    12.4.2010
            o
            1:11
                        (upravené
                12.4.2010
                o
                1:21)
                        |
            Karma článku:
                13.41
            |
            Prečítané 
            5352-krát
                    
         
     
         
             

                 
                    Podporte petíciu za prepracovanie systému kariérneho rastu pedagogických zamestnancov. Tento blog slúži ako oznam a výzva všetkým pedagogickým zamestnancom, ale aj všetkým ľudom, ktorým nie je ľahostajný vývoj a smerovanie dnešného školstva na Slovensku. Od začiatku školskej reformy prebehnú čoskoro dva roky a jej výsledkom je jedným slovom chaos. Jeden by možno dúfal, že po úvodných zmätkoch sa veci dajú do poriadku, ale v skutočnosti je opak pravdou.
                 

                 Najlepším príkladom toho, aký chaos v školstve vládne, je to, že pokiaľ sa skontaktujete či už s krajským školským úradom, metodickým centrom alebo so samotným ministerstvom, nikto a opakujem ešte raz, NIKTO vám nevie mnohé problémové alebo nejasné veci (a tých nejasných vecí je viac ako dosť) v nových školských zákonoch a vyhláškach poriadne vysvetliť. A ak vám to vysvetlia, tak si spravidla tieto vysvetlenia týkajúce sa jednej problematiky od vyššie spomenutých úradov protirečia.   Jednou z doslova absurdných epizód slovenského školstva je dlhodobé platové nedocenenie učiteľov. Viem, že sa opäť objaví množstvo ľudí, ktorí so mnou nebudú súhlasiť, ale samotní učitelia najlepšie vedia, o čom píšem. Doteraz bol základný tarifný plat začínajúceho učiteľa 539 € (to píšem o hrubom príjme). Toto je ocenenie tohto štátu pre vysokoškolsky vzdelaného človeka, ktorý má vzdelávať budúce generácie. Myslím, že sa niet čomu čudovať, keď mladí absolventi vysokých pedagogických škôl odmietajú ísť učiť. Priam katastrofálne do tejto situácie zasiahla zmena, ktorú priniesol zákon č. 317/2009 Z.z. a s ním súvisiace vyhlášky ministerstva školstva. Podľa týchto právnych predpisov platí, že mladí začínajúci učitelia od septembra 2010 budú zarábať v hrubom už len 494 € (keďže boli preradení z 10. do 9. platovej triedy). Nehovoriac o tom, že po roku učenia na škole musia absolvovať záverečný pohovor a otvorenú hodinu pred skúšobnou komisiou. Ak ho neabsolvujú, môžu byť zo školy vyhodení. A len absolvovaním tejto podmienky sa môžu dostať do 10. platovej triedy. Pýtam sa teda načo študovali vysokú školu? Načo absolvovali prax na vysokej škole? Načo robili štátnice? A na záver najpodstatnejšia otázka - Toto je podľa Vás, pán minister školstva, krok ktorým chcete prilákať viac schopných a vzdelaných ľudí do školstva? Je to jednoducho výsmech všetkým budúcim, ale aj terajším učiteľom!               Pán minister školstva sa neustále snaží vytvárať dymovú clonu okolo celého problému neustálym omieľaním klamstiev a poloprávd. Je zbytočné sa zmieňovať o učebniciach, z ktorých mnohé ešte ani na školách nie sú, hoci mali byť už v septembri 2008 (alebo septembri 2009, podľa ročníka ZŠ). Takisto je zbytočné sa zmieňovať o plánoch, osnovách, nezrozumiteľných a zle natlačených pedagogických dokumentoch (ako napr. klasifikačné záznamy, triedne knihy, katalógové listy k triednym výkazom, vysvedčenia, ...). Je zbytočné zmieňovať sa o týchto veciach preto, lebo o tom už boli povedané a napísané množstvá príspevkov a správ. A nič sa nestalo, nezmenilo.   Štrajk učiteľov by asi nezískal väčšinovú podporu (mnoho učiteľov by sa bálo ozvať, alebo by nemalo záujem - lebo od učiteľského príjmu nie sú závislí - dôvody nebudem rozoberať, myslím, že každý si ich vie domyslieť). Právom zaručeným v Ústave SR je však aj petícia. Na rozdiel od štrajku musí petíciu štátny orgán prijať, prerokovať a vyjadriť sa k nej.   Preto spolu s kamarátmi - učiteľmi - predkladáme petíciu, ktorou žiadame prepracovať systém kariérneho rastu pedagogických zamestnancov. Petíciu si môže každý stiahnuť a podpísať na internetovej stránke http://peticiaucitelov.szm.com. Ďalej bude petícia rozposlaná na všetky základné a stredné školy na Slovensku. Preto ak niekto z učiteľov bude čítať tieto riadky a v jeho škole sa s touto petíciou nestretne, tak ju jednoducho zamlčalo vedenie danej školy. Ak chcete pomôcť zmene školstva a prilákať doň kvalitných učiteľov, prosím podporte túto petíciu.   Blog, ktorý konkrétne rozoberá body petície si môžete prečítať po kliknutí na odkaz http://ivodudas.blog.sme.sk/clanok.asp?cl=225514&amp;bk=39855. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (52)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Baňas 
                                        
                                            Podivné štatistiky a porušovanie Ústavy SR by Čaplovič a ZMOS
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Baňas 
                                        
                                            Mikolaj a jeho kraviny!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Baňas 
                                        
                                            Byť starostom je úžasné!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Baňas 
                                        
                                            Klamár klamár alebo ako dokáže cestár "reformovať" školstvo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matúš Baňas
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Pazič 
                                        
                                            Prečo školské zariadenie v Pakistane?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matúš Baňas
            
         
        banas.blog.sme.sk (rss)
         
                                     
     
        Človek ako každý iný so svojimi názormi a pohľadom na svet o ktorý sa rád podelím a zároveň sa obohatím o postrehy iných.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    5
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3345
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Podivné štatistiky a porušovanie Ústavy SR by Čaplovič a ZMOS
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




