
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Kaštier
                                        &gt;
                názory
                     
                 Kto a kde vyhral (voľby)? 

        
            
                                    17.6.2010
            o
            23:25
                        (upravené
                18.6.2010
                o
                12:39)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            330-krát
                    
         
     
         
             

                 
                    Jednoduchý grafický pohľad na volebné výsledky. Predstava o geografickej štruktúre výsledkov môže byť užitočná z viacerých hľadísk: prispeje k pochopeniu efektivity billboardov a výjazdov strán; podporí plánovanie pred komunálnymi voľbami; vytvorí približnú predstavu, kde sú "najsilnejšie miesta odporu", ktorým sa bude potrebné venovať.
                 

                 Graf vizualizuje hlasy získané stranami "novej koalície", "starej koalície" (v nej som ponechal SNS) a strán mimo parlamentu, rozdelené podľa krajov. Obsah obdĺžnika znázorňuje počet získaných hlasov v obvode.   Výsledky interpretujem v minimálnej miere - nechávam to na Vás.   Obr1. Všetky strany      Nová koalícia (4), Stará koalícia (4)   Nová štvorkoalícia strán SDKU, SAS, MOST a KDH zvíťazila v štyroch krajoch: Nitrianskom, Bratislavskom, Košickom a Trnavskom. Stará koalícia SMER, SNS a HZDS zvíťazila v taktiež v štyroch krajoch: Žilinskom, Prešovskom, Banskobystrickom a Trenčianskom   Obr2. Všetky kraje - avšak vyfiltrovnané iba strany novej koalície      SDKU (4), Most (2), KDH (1), SAS (1).   Pri porovnaní iba hlasov novej koalície, SDKU získalo najaviac hlasov v štyroch krajoch: Bratislavskom, Košickom, Banskobystrickom a Trenčianskom, Most v dvoch: Nitrianskom a Trnavskom, KDH v Prešovskom a SAS v Žilinskom. To, že z pravicových strán sa SASke niekde podarilo zvíťaziť je na novú stranu veľký úspech. Zaujímavé je však, že práve v Žilinskom kraji. To, že Most v porovnaní s pravicovými stranami zvíťazil až v dvoch krajoch hovorí o tom, že aj nové strany to môžu dotiahnuť ďaleko.   Obr3. Všetky kraje, avšak vyfiltrovnané iba strany starej kolície      Smer (8)   Smer všade ultimátny víťaz, na druhom mieste päť krát SNS na tri krát HZDS. Pre mňa je osobne zaujímavé, že v Košickom a Prešovskom kraji uspelo viac HZDS.   Obr4. Všetky kraje, vyfiltrovnané zostávajúce strany      Zaujímavé je sledovať podporu ĽSNS.   Obr 5. Všetky strany, hlasy rozdelené podľa krajov   Farba obvodu znázorňuje podiel hlasov, ktoré strana získala v danom obvode. Červená je 50%, biela 0%.      Jediná pravicová strana s "baštou" bol Most v Dunajskej Strede. Bašty SMERu sú hlavne v menších obvodoch.   Bonusový - samovysveľujúci sa - obrázok      Údaje zdroj: www.volbysr.sk/   Software: www.macrofocus.com   Zobrazenia s ručením obmedzeným. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            20 kníh môjho roka 2012
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            25 kníh roka 2011
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            Ako reformovať dane a odvody - príspevok do diskusie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            50 kníh za rok
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Kaštier 
                                        
                                            Wikileaks v dobe xeroxu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Kaštier
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Kaštier
            
         
        kastier.blog.sme.sk (rss)
         
                                     
     
        ...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    16
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    714
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            názory
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Boston bigpicture
                                     
                                                                             
                                            TED
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                             
                                     
                                                                             
                                             
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Keď stretávate Andreja Kisku každé ráno, nemusíte počúvať Fica
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




