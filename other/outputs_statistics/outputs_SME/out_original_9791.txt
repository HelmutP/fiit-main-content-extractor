
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Alena Matejovska
                                        &gt;
                Nezaradené
                     
                 Ako byť šťastná ....  je na to recept? 

        
            
                                    28.6.2010
            o
            19:47
                        |
            Karma článku:
                6.66
            |
            Prečítané 
            1714-krát
                    
         
     
         
             

                 
                    Vždy keď som v nákupnom centre a mám kratšiu časovú rezervu, napríklad pred premietaním filmu v kine, stretnutím s kamoškami, prípadne inými aktivitami,  vyplním ju návštevou kníhkupectva. Rada sa prechádzam medzi knihami, beriem ich do ruky, listujem v nich.... u niektorých zaujmú názvy, u iných autor, u ďalších obal.... každá je jedinečná, nová, krásna. Áno, knihy sú také malé poklady. Sú v z nich je príbehy, múdrosť, ponaučenie, rady, úlety do sveta fantázie .. čo kniha, to niečo jedinečné. Minule, ako som sa prechádzala a pozerala čo je nové,  v kútiku v ktorom svietil nápis AKCIE ma zaujala knižôčka s názvom Ako byť šťastná.  No kto by po tom nesiahol? Už len zo zvedavosti, aké tajomstvo sa skrýva vnútri? Kniha so šťastne sa usmievajúcou ženou na obale, ktorá mala cez dokonalo perfektný chrup prelepenú nálepku s akciovou cenou
                 

                 
  
  sľubovala v podtitule prostriedky na zlepšenie nálady, psychotriky a rôzne rady. Vnútri sa dávalo zbohom smútku, krotili sa depresie, určovali príčiny smútku, radilo sa ako si urobiť deň čo najkrajší .... proste jedna rada za druhou. Pýtam sa, dá sa to? Dá sa nájsť recept na to aby človek bol šťastný? Dá sa vyčítať recept na dobrú náladu? Všetko je v našich hlavách. Zmiešať margarín, múku práškový cukor a iné ingrediencie, to je recept. Ale zmiešať dobrú náladu,  pozitívne myslenie, duševnú pohodu a iné stavy, vo chvíli keď ich nemáme poruke je už  obtiažnejšie. Všetko záleží od nás, ale nič nie je na povel. Áno, chceme byť šťastné, ale recept na to nevyčítame z knihy. Nestačí prečítať vetu typu „Okamžite zabudnite na svoje problémy“, zdvihnúť zrak od písmenok  a hneď to zrealizovať. Tak to nefunguje. Je to zložitejšie. Je to o našich radostiach, o ľuďoch ktorí sú okolo nás a robia náš život krajším, je to o tom že sme zdraví, je to tom že milujeme svoju prácu, je to o tom že si nedáme znepríjemňovať život zlými ľuďmi ktorí nám chcú škodiť, je to o tom, že si vieme uvedomiť,  že pre niektorých ľudí na tomto svete sme užitoční, potrební, a dôležití. Je to o tom, že v správnej chvíli si vieme nájsť čas na seba, zrelaxovať, tešiť sa z úspechov našich blízkych ľudí ... je to o svete okolo nás. Áno, určite sú tieto veci aj v knihách ktoré nám radia ako byť šťastné. Ale šťastie nie je na povel. Možno tá dáma na titulke knihy s dokonalým chrupom prelepeným  akciovou cenou v nás vzbudí pocit že takéto šťastie dosiahneme ľahko. Ale skutočné šťastie okolo seba si musíme uvedomovať a skutočne ho precítiť.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (35)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Matejovska 
                                        
                                            Možno ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Matejovska 
                                        
                                            Keď prajeme zdravie.... (to nie je fráza)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Matejovska 
                                        
                                            Články o diétach?  Ďakujem, nie!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Matejovska 
                                        
                                            O podgurážených chlapoch, alebo ako som chcela byť neviditeľná
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Alena Matejovska 
                                        
                                            Pani, nekúpite mi chleba?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Alena Matejovska
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Alena Matejovska
            
         
        alenamatejovska.blog.sme.sk (rss)
         
                                     
     
        ...život stále prináša niečo nové, škoda že to nie sú vždy len pozitívne veci ... ale tak to asi musí byť a nič s tým nenarobíme ...
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    18
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1770
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nezaradené
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Poézia
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                             
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Barbara Delinsky - Vinice
                                     
                                                                             
                                            Nigel Cawthorne - Najväčší tyrani a diktátori v dejinách
                                     
                                                                             
                                            Simon Brett - Prevítem snadno a rychle
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Robbie Williams- Sexed up
                                     
                                                                             
                                            The Wonders - That Thing You Do!
                                     
                                                                             
                                            Israel Kamakawiwo'Ole
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Adriana Markovičová
                                     
                                                                             
                                            Martin Šuraba
                                     
                                                                             
                                            Ľubo Kužela
                                     
                                                                             
                                            Lenka Jíleková
                                     
                                                                             
                                            Renáta Holá
                                     
                                                                             
                                            Jana Slobodová
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Vtedy a dnes: Krotitelia duchov (1984)
                     
                                                         
                       Vtedy a dnes: Dempsey a Makepeaceová (1985 - 1986)
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




