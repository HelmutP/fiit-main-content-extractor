

  
 Veľká miestnosť bola plná. Zmiešané rozhovory sladkožrútov sa odrážali z dlažby i kachličiek na stenách, ale im to vôbec neprekážalo. Hoci si chceli toho veľa porozprávať, nikam sa neponáhľali. Tu zrazu pristúpi k ich útlemu okrúhlemu stolíku bielej farby (také tie, čo už tradične stoja v cukrárni na jednej nohe), tak tu stojí jeden evidentne zahraničný študent Ekonomickej univerzity v Bratislave. Rád by si prisadol, lebo inde miesto nevidí. A možno tušil, že si s pánmi dobre porozumie. Starí priatelia ho privítajú s upozornením, že ich päťdesiatročné debaty a stokrát opakované vtipy (aj váš otec má ten dar?) nemusia byť príliš zaujímavé. Biele zuby prezradia, že to príšelcovi vadiť nebude. Ako bolo tak bolo, náhoda je niekedy najlepší plán. Všetci traja páni sa dali do družnej debaty, ktorá trvala dlhšie ako ktorýkoľvek z nich pôvodne plánoval. Nakoniec bol najzodpovednejší náš čierny študent a so slovami, že štúdium volá, ohlási pomalý odchod. Keď vstáva od stola, ešte raz sa poďakuje za poskytnutú stoličku a príjemnú spoločnosť. A dodá: „Už dávno som sa tak dobre so Slovákmi neporozprával.“ Obaja páni boli slušní a preto sa úctivo poďakujú, počkajú až ich nový spoločný známy celkom nezájde. Potom z nich vyletí hlboký úprimný smiech, ktorý vyruší všetkých ostatných hostí. 
 Naši bodrí spolužiaci (Maďar a Ukrajinec) sa spoznali pred tridsiatimi rokmi počas zahraničného štúdia. Tam si našli i svoje slovenské manželky. Dnes už zo zvyku medzi sebou používajú reč svojich žien.  Slovenčinu. 
 I keď treba priznať, že Maďar sa chodí „koňovať“, jeho deti sú občas „ohoreté“ a tiež trávi víkendy na „hate“. Ukrajinec nepoužíva „sa, si“ pri zvratných slovesách a pravidelne mäkčí tam, kde netreba a z času na čas chytí v dome „myšu“. Ale to nevadí. Alebo áno? 

   

