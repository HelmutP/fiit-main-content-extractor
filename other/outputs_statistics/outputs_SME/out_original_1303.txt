
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Bohuš (Viliam) Rosinský
                                        &gt;
                Foto-pokusy
                     
                 Hubovanie 1 

        
            
                                    20.8.2005
            o
            17:59
                        |
            Karma článku:
                10.70
            |
            Prečítané 
            6372-krát
                    
         
     
         
             

                 
                     Počas minulej návštevy v rodnej obci sme sa hrali na veľkých hubárov. A darilo sa nám. Našli sme kopec dubákov, ktoré sa v našej rodine považujú za hríb číslo jedna. Kuriatka neboli. Ale zato bolo dosť bedlí; a ich jedlých príbuzných. Ja osobne mám najradšej ich – opražené na oleji, posolené, okorenené, s chlebom.
                 

                Jednou z nich je tento druh (alebo odroda) muchotrávky - nebojte, je jedlá; raz určite:     Toto je bedľa vysoká. Podobajú sa. A chutia:     A toto je najžiadanejší hríb, dubák; krásne vonia - usušiť a pridať do kapustnice. Ale ani vysmažené ako rezne nie sú najhoršie:    Počas práve prebiehajúcej návštevy rodnej obce som sa do lesa vybral hlavne fotiť. Žiadne hubárske očakávania som nemal. Napriek tomu, proti svojej vôli, vrátil som sa domov s plnou bundou húb. Boli to prevažne kuriatka; do zeleninovej polievky s mrveničku. Alebo omáčku z nich. Alebo do praženice s nimi:     Ale aj masliakom (do praženice, do omáčky) sa dobre darí, po tých dažďoch:     Medzi jedlé huby patrí aj vatovec – ten sa ale v našej rodine neteší veľkej obľube:     Koziu briadku máme radi tiež len ako estetický artefarkt:     Rovnako tak nezbierame ani plávky. Sme skrátka veľmi prieberčiví; môj brat uznáva len dubáky. Toto je jeho dnešný úlovok – našiel ho na kraji lesa, kde je predsa len teplejšie ako v tôni:     Okrem týchto jedlých húb som nafotil aj množstvo tých, ktoré konzumovať neodporúčam. Ak sa podarí, pridám zopár snímok a dáky ten pokec aj o nich.

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohuš (Viliam) Rosinský 
                                        
                                            ZačiatoKoniec
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohuš (Viliam) Rosinský 
                                        
                                            nie Som
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohuš (Viliam) Rosinský 
                                        
                                            Ako som išou na Lišov a vernisáž som obišou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohuš (Viliam) Rosinský 
                                        
                                            in som ni ja
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Bohuš (Viliam) Rosinský 
                                        
                                            alebo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Bohuš (Viliam) Rosinský
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Bohuš (Viliam) Rosinský
            
         
        rosinsky.blog.sme.sk (rss)
         
                        VIP
                             
     
         ... 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    525
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1558
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            ďeňík
                        
                     
                                     
                        
                            A.B.e.C.e.D.a
                        
                     
                                     
                        
                            Človečina
                        
                     
                                     
                        
                            Dilema
                        
                     
                                     
                        
                            dilet-oviny
                        
                     
                                     
                        
                            momentky
                        
                     
                                     
                        
                            Hypochondroviny
                        
                     
                                     
                        
                            Muzička
                        
                     
                                     
                        
                            Skromné
                        
                     
                                     
                        
                            VIKI-Pédia
                        
                     
                                     
                        
                            Literatúra
                        
                     
                                     
                        
                            Poézia? Chyba! Rýmovačky iba..
                        
                     
                                     
                        
                            Poézia a P-óza
                        
                     
                                     
                        
                            Apokriffy
                        
                     
                                     
                        
                            Hlúposti
                        
                     
                                     
                        
                            Nezarandené
                        
                     
                                     
                        
                            Foto-pokusy
                        
                     
                                     
                        
                            Fotografia a výtvarno
                        
                     
                                     
                        
                            - a sp@m -
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Hojdačky
                                     
                                                                             
                                            Navždy v srdci
                                     
                                                                             
                                            Muž ako ženská
                                     
                                                                             
                                            Pomedzi sny
                                     
                                                                             
                                            O mužovi kytici a skle
                                     
                                                                             
                                            O slovenskej literatúre
                                     
                                                                             
                                            O filmoch/ Grečner
                                     
                                                                             
                                            Kafka/ Milan Richter
                                     
                                                                             
                                            Mama/ Novanská
                                     
                                                                             
                                            Dnes ma nič nebolí
                                     
                                                                             
                                            koľko slov - toľko slov
                                     
                                                                             
                                            Prečo nemôžem písať blogy
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vlastný horoskop
                                     
                                                                             
                                            Básnické dielo/ M.Válek
                                     
                                                                             
                                            detské veršovaky a uspávanky
                                     
                                                                             
                                            Prorocké knihy
                                     
                                                                             
                                            Kniha
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            JOURNAL FOR PLAGUE LOVERS
                                     
                                                                             
                                            Psí Vojáci - U sousedu vyje pes
                                     
                                                                             
                                            Psí Vojáci - Brutální lyrika
                                     
                                                                             
                                            100 Much - KúPIL SOM SI RHODES
                                     
                                                                             
                                            Para - DOMA DOBRE
                                     
                                                                             
                                            Into The Wild Soundtrack
                                     
                                                                             
                                            detský smiech, plač, zvuky...
                                     
                                                                             
                                            Send Away The Tigers
                                     
                                                                             
                                            Beauty &amp; Crime
                                     
                                                                             
                                            SHINE (Jana Kirschner)
                                     
                                                                             
                                            SOUL II SOUL
                                     
                                                                             
                                            Sultans of swing (Dire Stratis)
                                     
                                                                             
                                            Boatmans Call (Nick Cave)
                                     
                                                                             
                                            Sci Fi Lullabies
                                     
                                                                             
                                            How Dismantle An Atomic Bomb
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            lyrik
                                     
                                                                             
                                            pohľadnice
                                     
                                                                             
                                            turista zeff
                                     
                                                                             
                                            100percer ekoblóg
                                     
                                                                             
                                            pfm - fanúšik fšetkého
                                     
                                                                             
                                            dragon
                                     
                                                                             
                                            objavná
                                     
                                                                             
                                            cakino
                                     
                                                                             
                                            ema ma talent
                                     
                                                                             
                                            hevierov diár
                                     
                                                                             
                                            kamarát milan
                                     
                                                                             
                                            kamoška ivett
                                     
                                                                             
                                            jaromir_n
                                     
                                                                             
                                            download-ový blog
                                     
                                                                             
                                            blog poézie v próze (o.i.)
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Knihy.sme.sk
                                     
                                                                             
                                            Levné knihy.. a CD.. a DVD
                                     
                                                                             
                                            ..a Knihy
                                     
                                                                             
                                            umenie.net
                                     
                                                                             
                                            iný pohľad na môj blog
                                     
                                                                             
                                            - o mojom rodnom meste-
                                     
                                                                             
                                            -kľúč 28 -
                                     
                                                                             
                                            pokus 2006
                                     
                                                                             
                                            moje alter ego
                                     
                                                                             
                                            tretie...
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




