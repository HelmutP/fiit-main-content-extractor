

 Ak túžiš 
 Spraviť človeka šťastným, nauč sa 
 Rozumieť jeho snom 
 I jeho túžbam, radostiam, žiaľom 
 Buď prosto 
 Jeho priateľom 
   
 Vieš, že je krásne 
 Dni spolu zdieľať 
 Všetky dni, čo nám život dal 
 Spoločne dúfať 
 Spoločne veriť 
 Väčšia je radosť, menší žiaľ 
   
 Keď zomrie priateľ 
 Čo ti bol milý 
 S ktorým si zdieľal život svoj 
 Spolu ste žili 
 Vedno ľúbili 
 Každý z tých dní je len 
 Jeho a tvoj 
   
 To, čo nás spája 
 Je 
 Naveky živé 
 A to nám nemôže nikto vziať 
   
 Zdieľajme všetci navzájom život 
 Niečo 
 Nás bude v srdci hriať 
   
 Veď láska vždy bude to jediné VEČNÉ 
 Čo môžeme si 
 Darovať 

