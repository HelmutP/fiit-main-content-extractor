

 V prvom rade sa Fico môže začať obávať, že sa jeho obytný priestor radikálne zmenší a na cele nebude mať plastové okná. Jeho sponzori a donori na neho tlačia, pretože vypovedať pod prísahou na súde asi nie je pre nich to, o čo majú nejaký veľký záujem. 
 Teraz dostávajú slová premiéra, ktoré vyriekol vo februári, že má také informácie, že sa bojí o svoj život, svoj skutočný zmysel. Nad Robertom visí Damoklov meč, preto prská a žaluje všetkých naokolo. Fico odkázal, že praje všetkým príjemný sexuálny zážitok. V jeho prípade to však znamená skôr výmenu plienky. 
 Už v sobotu rozhodneme, či takáto karikatúra predsedu vlády bude pokračovať vo svojom vládnutí alebo nie. 

