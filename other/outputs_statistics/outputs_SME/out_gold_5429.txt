

   
 Presne pred rokom som totiž v hlavnom meste Estónska, Talline absolvoval rozhovor, ktorý zmenil môj pohľad na to, čo je a čo nie je možné v sektore služieb. Bol som na ceste do Helsínk a k dosiahnutiu tohto cieľa mi už chýbal iba jeden krok. Úspešne v Talline nastúpiť na trajekt a potom z neho v Helsinkách ešte úspešnejšie vystúpiť. A tak som sa teda vybral k okienku, za ktorým sedela veľmi pohľadná mladá slečna a nechal veciam priestor, nech sa dejú ako sa majú stať. 
 Naša debata bola priam neuveriteľná. Na jej konci som si totiž uvedomil, že ak si o mne nemyslí, že som magor, tak je asi divná ona. Na každú otázku som jej totiž odpovedal „nie“, prípadne „neviem“. Celé sa to zbehlo asi takto: 
 – Dobrý deň. 
 – Dobrý deň, čo pre Vás môžem urobiť? 
 – Prosím Vás, ja by som sa potreboval dostať do Helsínk. 
 – A kedy by ste chceli ísť? 
 – No, neviem, kedy to ide? 
 – Najbližší trajekt odchádza za 15 minút. Ďalší až o dve hodiny, ale ten má trochu inú trasu, ide dlhšie a je aj drahší. Takže by som Vám odporúčala ten prvý. 
 – Hmm.. no neviem, či to stihneme. 
 – Určite stihnete. Ak chcete, tak môžem do istoty zavolať na check-in, aby tam na vás počkali. 
 – Tak teda dobre, tak by sme šli na ten prvý. 
 – Fajn, poprosím Vás občianske preukazy všetkých pasažierov. 
 – Noo.. keď ja tu mám len ten svoj. 
 – To nevadí, ostatné ukážete na check-ine. Ešte potrebujem technický preukaz od auta. 
 – Noo, viete, ja tu nemám ani ten. 
 – Dobre, tak napíšem, že na check-ine ukážete aj ten technický. Mohli by ste mi dať aspoň ŠPZ? 
 – Jasné, to môžem. 
 – Tak tu máte pero a papier, napíšte mi to prosím. 
 – Uff, keď, viete, ja som si práve spomenul, že sme tu prišli novým autom a ja nepoznám ani tú ŠPZ. 
 – Hehe, no nevadí. Ja to tu nejako vyriešim. Môžeme pristúpiť k platbe? 
 – Noo.. keď ja tu vlastne nemám ani peniaze, pôvodne som sa totiž prišiel iba poinformovať. 
 – Aha, tak dobre. Napíšem vám, že to ešte nie je zaplatené, na check-ine to vybavia. Nech sa páči, vaše lístky. 
 – Ďakujem, ďakujem veľmi pekne! Dovidenia! 
 – Dovidenia! 
  
 Na check-ine nás naozaj počkali, naozaj vybavili všetky formality vrátane platby, ponavigovali nás až do samotnej lode a v momente ako sme zaparkovali, zatvorili podpalubie a vytiahli kotvy. Keď som potom sedel na palube, pozeral sa na more a pofajčieval cigaretu, napadlo mi, ako by asi takýto rozhovor dopadol na Slovensku. Vyhútal som nasledovné: 
 – Dobrý deň. 
 – Dobrý. 
 – Prosím Vás, ja by som sa potreboval dostať tam a tam. 
 – Nech sa páči, tam na stene máme cestovný poriadok. 
 ... 
 – Dobrý deň, tak som si prečítal, že to ide za 15 minút. 
 – Ide, ale to už asi nestihnete. 
 – Tak by som teda šiel tým druhým spojom, ktorý ide o 2 hodiny. 
 – Fajn, poprosím Vás občianske preukazy všetkých pasažierov. 
 – Noo.. keď ja tu mám len ten svoj. 
 – Tak sa vráťte aj s ostatnými. 
 ... 
 – Dobrý deň, tak tu teda mám tie občianske preukazy. 
 – Dobre, ešte potrebujem technický preukaz od auta. 
 – Noo, viete, ja tu nemám ani ten. Nestačila by ŠPZ, aby som sa poň nemusel znovu vracať? 
 – Ja tu mám na tlačive číslo preukazu, nie ŠPZ. 
 ... 
 – Dobrý deň, tak tu teda mám ten technický preukaz od auta. 
 – Dobre, môžeme pristúpiť k platbe? 
 – Noo.. keď ja tu vlastne pri sebe peniaze nemám. 
 – A čo tu vlastne hľadáte človeče?!? 
   
 A Boh ochraňuj takého človeka v prípade, že by nehovoril po slovensky. Samozrejme, bolo by zbytočné čokoľvek zovšeobecňovať a ani týmto článkom nechcem nikoho uraziť. Možno som mal v Estónsku len šťastie a na Slovensku zase mávam smolu. Vážim si prácu úradníkov a ešte viac tých, ktorí majú denne do činenia s takými magormi, akým som bol v ten deň ja. Ale úsmev mladej blonďavej slečny z Estónska a jej spôsoby ma privádzajú k myšlienke, že veci sa naozaj dajú naozaj robiť aj inak, lepšie. 
   

