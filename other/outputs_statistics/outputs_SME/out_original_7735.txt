
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Vozárová
                                        &gt;
                odlesky dní, spomienky
                     
                 Po čom ženy túžia? 

        
            
                                    1.6.2010
            o
            10:45
                        |
            Karma článku:
                8.28
            |
            Prečítané 
            1869-krát
                    
         
     
         
             

                 
                    Keď som sa tak včera ponáhľala do mesta, všimla som si zamysleného chlapca. Mohol mať tak sedemnásť. Hľadel do zeme, tíško sa usmieval a v ruke opatrne držal žltú ružu. Nevdojak som si spomenula na jednu príhodu zo školy...
                 

                 
 zv
   ... akurát z maturitného ročníka, keď sme sa ako triedy delili podľa zvolených predmetov na viac skupín. Keďže padol piaty maturitný predmet pre gymnáziá, zostali mi hodiny dejepisu viac-menej ako voľné. Ale späť ku tým kvetom ;).   Dvojhodinovka dejepisu sa ledva začala, keď dejepisárka kdesi na chvíľku zmizla. Čosi vybaviť s tými jej nepodarenými žiakmi. Vtedy došiel spolužiak (váham, či napísať skutočné meno), s ktorým sa dalo rozprávať o čomkoľvek. Pustili sme sa do reči. Akosi sme sa dostali k téme kvetov a ja som poznamenala, že pre ženu je to veľmi príjemné dostať len tak kvietok. Trebárs aj nejaké poľné kvety, natrhané päť minút pred stretnutím. Keďže to bolo začiatkom decembra, poznamenala som, že to by sa teraz asi nedalo.  On na to podotkol, že však to sa dá hocikedy. Po chvíľke utrúsil niečo v tom zmysle, že už mi dávno nič nedal. Krátko na to sa zdvihol, že to môže predsa spraviť aj hneď teraz. A vytratil sa z triedy.   O pár minút na to sa vrátila do triedy dejepisárka. Bola taká nahnevaná z tých svojich nepodarkov, že si ani nevšimla, keď vošiel ten môj kamarát do triedy a v rukách niesol...   ...kvetináč. Prišiel až ku mne a položil mi ho slávnostne na lavicu so slovami, že pre mňa všetko. :)       Nemusím snáď ani spomínať, že v tú chvíľu to bol pre mňa ten najkrajší kvet. Keďže sa jednalo o typický školský chodbový kvet, bolo ho treba trošku ovlažiť. A krásne nám spríjemňoval aj ďalšie hodiny v dotyčnej učebni.     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (27)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Vozárová 
                                        
                                            Modrá... červená... nakoniec žltá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Vozárová 
                                        
                                            Čo takto dať si siestu...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Vozárová 
                                        
                                            Jeseň
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Vozárová 
                                        
                                            Ženy...les femmes...women
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Vozárová 
                                        
                                            Nočný...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Vozárová
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Vozárová
            
         
        zuzanavozarova.blog.sme.sk (rss)
         
                                     
     
        dieťa Mesiaca s otvorenými očami hľadajúce všetky odtienky života
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    994
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            fotografie
                        
                     
                                     
                        
                            odlesky dní, spomienky
                        
                     
                                     
                        
                            moje výtvory
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Iľja Čičvák - Básne za mrežami
                                     
                                                                             
                                            Jack Kerouac - Osamělý poutník
                                     
                                                                             
                                            Jack Kerouac - Na ceste
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            mix všetkého možného...
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Silans
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




