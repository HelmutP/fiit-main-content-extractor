
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Šubová
                                        &gt;
                Nie súkromné
                     
                 Čo nechceš, aby robili tebe, nerob ani ty tým druhým 

        
            
                                    1.8.2010
            o
            10:15
                        (upravené
                1.8.2010
                o
                14:58)
                        |
            Karma článku:
                10.49
            |
            Prečítané 
            2302-krát
                    
         
     
         
             

                 
                    Ako zas mala moja babka pravdu.
                 

                 Bol to taký miestny trapko, synáčik súdruha predsedu MNV - vždy vymydlený, v nedeľu nosil kravatu a virtuozne žaloval. Kolektívne sme ho neznášali. Akákoľvek hádka vrámci ulice skončila vždy, keď sa zjavil s tým svojim "a to sa pooovieeee".  Vtedy bolo úplne jedno, či sme sa predtým hádali, kto je "mešťan" a "dedinčan", kto je "za" Slovan či Kovosmalt, kto počúva O3 a kto Zákrutu. Spoločný nepriateľ skočil zmydlený rukou jednotnou a nedielnou.  Raz si ma zavolala babka na "bešprechung". Vedela som, že niečo prasklo. Susedovie okno, nezamknuté auto s vytrhnutými káblami, družstevné marhule alebo nápis na farskom plote, za ktorý mohla konkurenčná partia? Nie. Šlo o Jožka, čo bolo trapkove občianske meno. "Ako by sa ti páčilo, keby si bola na jeho mieste?"  Netvrdím,  že odvtedy som vyhľadavala príslušného Pepu a kamarátila sa s ním. Ale ako príklad poslúžil výborne. Nie, nechcela by som, aby mi robili to, čo jemu a nežalovala som.   Prešli dni, roky, desaťročia... Jožo je miestnym papalášom a vracia všetkým v celých, čo si v drobných požičali v detstve a keďže má po ockovi správny politický čuch, ak chce, nájde si aj tých, čo sa z rodného sídla odsťahovali.  Mne povedal, že si "váži, čo som preňho urobila" a dokonca sa ma opýtal, či sa mi môže nejak "revanšovať".  Jediné, čo ma napadlo, bolo, že by bolo ideálne, keby zabudol, že sme sa kedy v živote videli.   Len že zas mala raz babka pravdu. Šikanovaný bude šikanovať o to viac, hneď ako dostane možnosť. Skúsme to s tou občianskou spoločnosťou. Nerobme iným to, čo nechceme, aby robili nám. Možno nebudú... Bez záruky. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (24)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Od inakiaľ XVIII
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Domov sú ruky, na ktorých smieš plakať...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Príspevok z kuchyne k výročiu Veľkého Novembra
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Osudové okamžiky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Šubová 
                                        
                                            Veď nejde o život
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Šubová
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Šubová
            
         
        subova.blog.sme.sk (rss)
         
                        VIP
                             
     
        Nealternatívna mamina

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    585
                
                
                    Celková karma
                    
                                                9.02
                    
                
                
                    Priemerná čítanosť
                    1919
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Nie súkromné
                        
                     
                                     
                        
                            Pod bielym krížom
                        
                     
                                     
                        
                            Služobne
                        
                     
                                     
                        
                            Damals
                        
                     
                                     
                        
                            Privat
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            o rakovine pri pive
                                     
                                                                             
                                            moja videovizitka
                                     
                                                                             
                                            Recenzia na Pána Prsteňov 1977
                                     
                                                                             
                                            Epitaf
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Falco
                                     
                                                                             
                                            Karel Kryl
                                     
                                                                             
                                            Daniel Landa
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Apollo
                                     
                                                                             
                                            Lujza
                                     
                                                                             
                                            fan
                                     
                                                                             
                                            Hliva
                                     
                                                                             
                                            Kar-ma
                                     
                                                                             
                                            waboviny
                                     
                                                                             
                                            štvorica
                                     
                                                                             
                                            vlčko
                                     
                                                                             
                                            stranger
                                     
                                                                             
                                            štvorlístok
                                     
                                                                             
                                            germa píše
                                     
                                                                             
                                            Maroško bloguje
                                     
                                                                             
                                            Ywka
                                     
                                                                             
                                            Čára
                                     
                                                                             
                                            Halúzky zo života
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Varíme s medveďom - Rok v kuchyni
                     
                                                         
                       Domov sú ruky, na ktorých smieš plakať...
                     
                                                         
                       Okolo nedeľného obeda
                     
                                                         
                       Žiť si na morskej MHD
                     
                                                         
                       Tak vchádza svetlo je kanadská severská krimi
                     
                                                         
                       Nechceme podnikať, chceme pracovať pre euroinštitúcie
                     
                                                         
                       Grabstein - nem. náhrobný kameň
                     
                                                         
                       O čase a bytí
                     
                                                         
                       Bratislavčanka
                     
                                                         
                       Ľudia nechápu.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




