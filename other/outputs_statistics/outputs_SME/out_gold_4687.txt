

 O Poliakoch tieto naše elity národa rozprávať nepotrebujú. Poliaka totiž ich voličská základňa veľmi dobre pozná. Hold Poliak je len obyčajný človek, lenivý rovnako ako našinec, šušle trochu viac a keď nás ošmekne, urobí to tak elegantne, že sa mu za to ešte aj poďakujeme. 
 Zato o Maďaroch treba informovať pravidelne a podrobne. Veď väčšina ovečiek zo Slovenského Najnárodnejšieho Salaša živého Maďara v živote nestretla. Treba im pomôcť, nech si vedia v hlavičke vytvoriť dôverný obraz tejto nebezpečnej šelmy. Treba ich vystríhať do nemoty, nech sa majú pred ňou na pozore. Veru do nemoty, ktorá kiež by raz tiež nastala. 
 Na záver ešte jedna kacírska otázka. Vieme si predstaviť, ako by asi vyzerali nálady v našej malebnej krajinke a rečnícke prejavy oných národných elít, keby sa nedajbože podobná tragédia stala nášmu susedovi na juhu? 
 Nuž tak... 

