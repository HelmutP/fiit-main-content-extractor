
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Róbert Oružinský
                                        &gt;
                OCHRANA PRÍRODY
                     
                 500 + :-) 

        
            
                                    2.4.2010
            o
            15:18
                        (upravené
                2.4.2010
                o
                16:14)
                        |
            Karma článku:
                14.39
            |
            Prečítané 
            2715-krát
                    
         
     
         
             

                 
                    V noci z nedele na pondelok som odoslal pripomienky k návrhu nariadenia vlády o zonácii Tatranského národného parku. Tento návrh zonácie totiž nie je v prospech prírody, ale v prospech jej ničiteľov. Vypracovaniu pripomienok som v priebehu posledných troch týždňov venoval niekoľko dní a nocí. V rámci mojich možností som do toho vložil to najlepšie.
                 

                 Potom som sa začal uchádzať o podporu verejnosti, aby som mojím pripomienkam dodal patričnú váhu. Podľa legislatívnych pravidiel vlády (ktoré okrem iného upravujú aj proces pripomienkovania návrhov právnych predpisov) je na to, aby sa pripomienky fyzickej alebo právnickej osoby stali zásadnými, potrebné, aby ich podporilo aspoň 500 ľudí. Podpora môže mať aj formu zaslania e-mailu predkladateľovi príslušnej právnej normy (v tomto prípade bolo predkladateľom návrhu nariadenia vlády o zonácii TANAP-u Ministerstvo životného prostredia).   Hoci z developerského návrhu zonácie som bol skôr smutný, prístup ľudí ma veľmi potešil. Ukázalo sa, že k Tatrám majú ľudia silnú citovú väzbu. Citovú väzbu nie k hotelom, zjazdovkám a lanovkám, ale v prvom rade k unikátnej tatranskej prírode. Veľa ľudí si uvedomuje, že práve tatranská príroda je tou skutočnou hodnotou, ktorá si zaslúži ochranu. A dnes ochranu potrebuje - paradoxne - práve pred tým, kto by ju mal chrániť - pred samotným Ministerstvom životného prostredia, ktorého čelní predstavitelia sa zrejme úplne zapredali.   Ľudia to pochopili.   Za tri dni som získal neuveriteľnú podporu. Mám spätnú väzbu o vyše 700 e-mailoch zaslaných na ministerstvo (a v kópii na vedomie na Úrad vlády SR). Posledné dva marcové dni, posledné dva dni pripomienkového konania, bolo kvôli záplave e-mailov ministerstvo hore nohami. Môže si za to samo, keďže postavilo princíp národných parkov na hlavu.   5-krát vyslovujem „ďakujem" všetkým, ktorí ma podporili:   - za to, že si dali námahu stiahnuť moje pripomienky do svojho počítača a priložiť ich k svojmu e-mailu,  - za to, že mi dali vedieť, že pripomienky odoslali, - za dôveru, že to myslím úprimne a nemám bočné úmysly, - za presvedčenie, že akákoľvek forma pomoci je lepšia ako žiadna, - za to, že im osud Tatier nie je ľahostajný.   Táto podpora ma veľmi zaväzuje. Avšak prosím o trpezlivosť. Na komunikáciu s vyše 700 podporovateľmi som sám. Budem v maximálnej miere využívať na komunikáciu tento blog, pretože je to efektívnejšie a aktuálne informácie sa dostanú aj k ďalším ľuďom, ktorí by Tatrám radi pomohli.   Nádej stále žije, ale čert nikdy nespí. Ideme ďalej. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (14)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Vládnu klamári hlupákom?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Sociálna demokracia alebo stranícky centralizmus?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Dodrží premiér Fico svoje vlastné pravidlá?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Postaví sa "dolných" 10 tisíc svojvôli "horných" 10 tisíc?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Róbert Oružinský 
                                        
                                            Už od vás vaša škola pýtala mydlo a toaletný papier?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Róbert Oružinský
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Alžbeta Slováková Spáčilová 
                                        
                                            Selfie s nádychom hyenizmu a krutosti
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Róbert Oružinský
            
         
        oruzinsky.blog.sme.sk (rss)
         
                        VIP
                             
     
        1/∞
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    120
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2310
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Inžinieri bojujú...
                        
                     
                                     
                        
                            ...šírime strach a paniku
                        
                     
                                     
                        
                            kauza Tichá a Kôprová dolina
                        
                     
                                     
                        
                            ÚLOŽISKO NEBEZPEČNÉHO ODPADU
                        
                     
                                     
                        
                            Hyperboly
                        
                     
                                     
                        
                            POLITIKA
                        
                     
                                     
                        
                            PYROEKOLÓGIA
                        
                     
                                     
                        
                            Zo života cicavcov...
                        
                     
                                     
                        
                            OCHRANA PRÍRODY
                        
                     
                                     
                        
                            LZ The Bark Beatles
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Čo práve počúvam 
                    
                     
                                                 
                                            Eoin Duignan
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




