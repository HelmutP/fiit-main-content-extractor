
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matúš Lazúr
                                        &gt;
                Nezaradené
                     
                 V stopách grófa Drakulu 

        
            
                                    4.6.2010
            o
            13:51
                        (upravené
                4.6.2010
                o
                14:46)
                        |
            Karma článku:
                8.36
            |
            Prečítané 
            2673-krát
                    
         
     
         
             

                 
                    Mnohí z nás Rumunsko poznajú ako jednu z dovolených prímorských destinácií za bývalého režimu. Dnes je skôr synonymom niekoho chudobnejšieho v EU ako sme my. Aj tak okrem pár turistov ktorým učarovala miestna príroda, do vnútrozemia zablúdi málokto. A veru, treba si vedieť vybrať miesto a presvedčíte sa, že Rumunsko má aj svoju krásnu tvár. Tu je.
                 

                 Na dlho plánovaný výlet do Rumunska sme sa vybrali v jeseni 2007. Len tak, nie k moru ale po nedlhom plánovaní len tak vlakom čosi vidieť za štyri dni. A čo by to bolo za Rumunsko bez snáď najznámejšej postavy grófa Drakulu. Zastávkami na našej krátkej ceste boli teda nakoniec tri miesta, Sinaia, Bran a Sighisoara.   Sinaia je male horske stredisko skoro v strede Rumunska. Po nekonečnej ceste vlakom z Budapešti a prekvapení zo zrekonštruovaných staníc na ktoré sa chytá len máloktorá naša, sme dorazili do Sinaie. Ubytovanie sme zohnali veľmi rýchlo a lacno. Ďalšia výhoda Rumunska.   Sinaia      Hlavnou atrakciou okrem lyžovania a pre nemotorizovaného turistu nedostupných lokalít sú kaštiele pomerne mladého dáta, ale veľmi krásne.  Pelisor ktorého architektom bol dokonca čech a starší Peles Castle. Okolie vyzerá jednoducho nádherne.         Kaštieľ      Druhý kaštieľ...               Celkom luxusne to pôsobí. Kto by povedal že ste v Rumunsku.      Železničná stanica.      Ďalšou zastávkou na ceste bol hrad Bran, teda ten najoficiálnejší Drakulovský hrad. Na počudovanie bol aj v októbri cieľom kopy turistov aj keď vlastne nič svetoborné a už vôbec nie týkajúce sa Drakulu v ňom priamo nie je. Nuž ale reklama je reklama. Nepamätám si už cenu vstupného ale lacné to tiež nebolo. Hold Drakula je svetový. Hrad je na kopci priamov dedinke a celkovo je vnútri strašne malý. Teda stále sa len točiť po schodoch okolo vnútorného dvora.   Bran, oficiálny drakulov hrad...         Vstup po úzkom schodisku.      V podzámčí...      Celkom to vyzerá "turisticky". Pod hradom bábušky predávaju syr či bryndzu...          Poslednou zastávkou bola Sighisoara. Okrem iného je v nej aj originálny rodný dom Vlada Tepesa teda zijucej predlohy groófa Drakulu. V dome na rohu námestia je dnes reštaurácia. Víno padlo dobre, samozrejme červené ako krv....               Nuž, ale tak pochmúrna strašidelná atmosféra to znesie...      Miestna turistická atrakcia Clock Tower...      Aj Drakula má pamätnú tabuľu...      Rodný dom "Vlada"...      Celkom strašidelné, že?         Mesto ponúka ešte kopu zaujímavostí, teda čo by si mal turista odfotiť je veža na tom istom námesti s hodinami a miestne staré uličky. Nuž čosi sme stihli a dá sa predpokladať že v Rumunsku je toho oveľa viac na pozeranie, len treba zísť z obvyklých trás...         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (9)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            O troch vozoch
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Lovci a zberači
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Bratislava - Talin a späť 2. Kurská kosa.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Bratislava - Talinn a späť 1.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Lazúr 
                                        
                                            Spomienky na Baltik 3. Sopoty
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matúš Lazúr
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matúš Lazúr
            
         
        matuslazur.blog.sme.sk (rss)
         
                                     
     
        Z ďalekého východu späť na blízkom západe.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    45
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1162
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Austrália
                        
                     
                                     
                        
                            Malajzia
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       O čase a bytí
                     
                                                         
                       Kráľom je otec
                     
                                                         
                       O dobre mienených radách
                     
                                                         
                       Zbohatlíci kolonizujú Petržalskú hrádzu. Polícia nekoná.
                     
                                                         
                       Niečo je zhnité v slovenskej pošte (porovnanie s rakúskou)
                     
                                                         
                       Aký je rozdiel medzi buzerantom a homosexuálom
                     
                                                         
                       Vďaka, pani chatárka, za otrasný víkend v Tatrách
                     
                                                         
                       Otec, odpusť mi...
                     
                                                         
                       O pracovnej sile (zamyslenie čakateľa v rade pred pokladňou).
                     
                                                         
                       Ako žiť?
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




