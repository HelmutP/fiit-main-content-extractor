
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Monika Albertiová
                                        &gt;
                Nezaradené
                     
                 feminizmus existuje 

        
            
                                    5.7.2010
            o
            13:55
                        (upravené
                6.7.2010
                o
                16:03)
                        |
            Karma článku:
                2.61
            |
            Prečítané 
            369-krát
                    
         
     
         
             

                 
                    „Feminizmus existuje z toho dôvodu,aby sa škaredé ženy integrovali do spoločnosti." (Charles Bukowski)
                 

                     Táto veta vystihuje názor mnohých mužov,zarytých antifeministov na ženy,ktoré nevidia nič iba seba,nemajú radi mužov,mužatky bez príťažlivosti,ktorým sa zdá,že sú utláčané,ale nerobia nič preto, aby to tak nebolo."Fúzatá sufražetka" je pre nich urážka najhrubšieho zrna...Zabudli,že to boli radikálne bojovníčky za práva žien,ktoré pre nás vybojovali dnes takú samozrejmosť- akou je volebné právo.   „Neuznávam autoritu polície,tohto súdu alebo akéhokoľvek súdu a zákona spísaného mužmi",vyhlásila pred rokmi na súde Teresa Billingtonová,klasička hnutia za práva žien.Zákony v minulosti tvorili iba muži,boli považovaní za „pánov tvorstva". A dnes?   „Všetko je inak". Povedal by židovský učenec renesancie Izák Nuria. Ženy šoférujú,zarábajú peniaze,sú v riadiacich funkciách,dokonca lietajú do vesmíru. Sú samostatné...Osud mužov je v rukách žien. Žena rozmýšľa a rozhoduje s kým bude mať deti a muž má neľahkú úlohu: Musí udržať status quo - byť povinne úspešný, priebojný,neustále demonštrovať svoju silu a súťažiť...   Tak vzniká súťaž pohlaví,kto je múdrejší,šikovnejší,kto viac zarába,ale rozdiely medzi pohlaviami stále existujú vo všetkom.   Bolo by ideálne,nech sú muži mužmi a ženy ženami...Som za klasický model rodiny s mamou a otcom. Je to najlepší model pre výchovu detí. Ale to neznamená,že žena upratuje,varí,perie,žehlí poslúcha a muž pozerá televízor s fľašou piva...   Pre mňa je feminizmus synonymum pre možnosť slobodne sa rozhodnúť,vybočiť z historických stereotypov,synonymom rovnosti medzi mužmi a ženami. Možnosť voľby ako žiť,ale hlavne mať úctu človeka k človeku. Je jedno, či je to muž, alebo žena.   Feministická ikona Gloria Steinem povedala:   „To nie je revolúcia,to je evolúcia.Takže veci sa nezmenia za rok,ani za dva,tri,päť ani desať rokov.Je to veľmi pomalý proces,pretože všetko čo sa deje s ľudským vedomím trvá veľmi dlho."     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Albertiová 
                                        
                                            Vojna potkanov alebo Prečo pravica neporazí Fica
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Albertiová 
                                        
                                            Kto sa nás pokúša vodiť za nos na Námestí SNP?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Albertiová 
                                        
                                            Keď má človek z politiky triašku.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Albertiová 
                                        
                                            Nebudem voliť s rozvahou, ale ani srdce, rozum a charakter.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Monika Albertiová 
                                        
                                            Svet je miesto, kde na názoroch žien záleží
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Monika Albertiová
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Monika Albertiová
            
         
        albertiova.blog.sme.sk (rss)
         
                                     
     
         Som človek a to je moja politika. 

 Nezávislá. 

 občianka SR a Európskej únie. 

 Metodička, aktivista. 

 Kandidovala som za 99%  . 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    47
                
                
                    Celková karma
                    
                                                5.67
                    
                
                
                    Priemerná čítanosť
                    640
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Básne
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kto sa nás pokúša vodiť za nos na Námestí SNP?
                     
                                                         
                       Chudobní a hladní nemajú čo stratiť.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




