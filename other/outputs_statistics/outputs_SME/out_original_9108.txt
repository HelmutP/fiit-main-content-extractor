
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rasťo Podoba
                                        &gt;
                Pocity
                     
                 99% 

        
            
                                    18.6.2010
            o
            8:33
                        (upravené
                25.1.2011
                o
                11:28)
                        |
            Karma článku:
                1.76
            |
            Prečítané 
            236-krát
                    
         
     
         
             

                 
                    
                 

                 Nevieme čo chceme...ale vieme, čo nechceme. Hľadáme, no nenachádzame. Žijeme sny, ktoré pre nás vysnívali iní. Existujeme? Alebo existuje len obraz, ktorý ostatní vnímajú podľa svojej vôľe a rozmaru? Čo ma činí mnou? Moje slová, moje skutky, moje oblečenie?   ...Koľko masiek vlastne máme v šuflíku? Kedy som ja a kedy je to ten druhý? Neznášam ho. Neznášam ich. Koľko ich vlastne je? Som sám so sebou ale nie sám sebou...tak mi povedz kto som, kto si...   Smeješ sa mi do tváre. Hovoríš ako bude všetko dobré. Si hlupák, vieš sa  len smiať. Žiadal som ťa o radu a ty si mi len sypal soľ do očí.  Nemôžem sa sťažovať, viem. V podstate sa vôbec nič nedeje. Nesťažujem  sa. Nemôžem. Nevtesnal som sa do obrazu horlivého fanúšika života. Som  lenivý a apatický voči dianiu okolo mňa. Som bezcenný. Len tak sa  potĺkam životom. Pijavica. Prečo je vo mne ten nepokoj? Nemlč!! Pomôž  mi...doriti...neviem, nechcem, nepotrebujem. Kto som? Kto si?    Dám si facku, idem ďaľej. Musím nájsť obraz, do ktorého zapadnúť. Musím  nebyť sebou aby som zachránil ľudstvo. Musím žiť ich sny, inak ma  zabijú...    Všetko je fajn 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Podoba 
                                        
                                            Júl
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Podoba 
                                        
                                            Dobré úmysly
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Podoba 
                                        
                                            Detský kútik
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Podoba 
                                        
                                            Počuj, amicenko...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rasťo Podoba 
                                        
                                            Psie nebo
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rasťo Podoba
                        
                     
            
                     
                         Ďalšie články z rubriky próza 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        David Stojaspal 
                                        
                                            Dukelské zápisky 1944-2014
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Igor Čonka 
                                        
                                            Ellis Parker Butler - Prasce sú prasce I
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Kto nezažil, neuverí. . . (46)
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Oni
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Darina H. Lacková 
                                        
                                            Odrazový mostík
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky próza
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rasťo Podoba
            
         
        podoba.blog.sme.sk (rss)
         
                                     
     
        Autobiografista amatér

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    9
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    188
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky
                        
                     
                                     
                        
                            Pocity
                        
                     
                                     
                        
                            Poviedky
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            svetielko nádeje :)
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




