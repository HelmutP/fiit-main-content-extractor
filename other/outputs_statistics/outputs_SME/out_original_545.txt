
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Aneta Čižmáriková
                                        &gt;
                Kreatívec notorik
                     
                 Náušnice pre začiatočníkov 

        
            
                                    4.4.2008
            o
            10:00
                        |
            Karma článku:
                10.50
            |
            Prečítané 
            14768-krát
                    
         
     
         
             

                 
                    S výrobou bižutérie sa dá začať aj jednoducho - skladaním z polotovarov. Dajú sa bežne kúpiť v kreatívnych obchodoch a jednoduchá práca s nimi vás povzbudí k náročnejším projektom.
                 

                    No predtým, než začnete vyrábať/skladať bižutériu pomocou drôtu, potrebujete základné náradie - malé klieštiky. Dajú sa kúpiť jednotlivo alebo v sade. Buď ich budete zháňať jednotlivo - v špecializovaných obchodoch s pomôckami pre výrobu bižutérie - alebo začnete so sadou pre elektrikárov ako ja.   Finta je v tom, že na výrobu šperkov absolútne nevyhnutne potrebujete kliešte s okrúhlymi kónickými čeľusťami.   Budete s nimi pracovať prakticky neustále - pri ohýbaní drôtu, vyrábaní očiek, špirál, atď.  A tieto pokiaľ viem existujú len ako hobby náradie alebo príslušenstvo pre elektrikátov.  Samozrejme, hneď od začiatku sa hodia aj klieštiky pre pridržiavanie, štípanie a ďalšiu jemnú prácu s drôtom. Môžete to skúsiť s tými veľkými, ktoré máte doma povedzme po otcovi, ale pochybujem, že by ste cez ich čeľuste vôbec dovideli na to, čo robíte. (V dohľadnej dobe urobím krátky článoček, kde ukážem svoje najpoužívanejšie náradie.)     Náušnice na prvej fotografii som kúpila ako polotovar. V balíčku za nejakých 49 SK boli náušnicové závesy, ozdobné kruhy aj 10 kusov bižutérnych nitov v staromedenej farbe. Dokúpila som 2 mm a 4 mm rokajlové korálky a výroba mohla začať.           Ako vidíte, stačilo na nity navliecť korálky, zahnúť očká a zároveň ich pripevniť k uškám na kruhu. Kamarátke Lenke sa blížili narodeniny, tak som vymyslela inú farebnú kombináciu, ktorá by ladila so staromeďou a vyrobila druhé:               Samozrejme, skladať z polotovarov sa dajú nielen náušnice. Dostať kúpiť aj rôzne vopred pripravené náhrdelníky, náramky či prívesky na mobil. Balíček vždy obsahuje všetko potrebné a nemal by chýbať ani návod. Mne sa bohužiaľ stalo, že fialový náhrdelník Mandala  od maďarského výrobcu síce obsahoval návod po maďarsky (chvalabohu s obrázkami), lenže bol akosi odstrihnutý, nedokončený. Spodná časť chýbala. Asi ma to ako začiatočníka zabrzdilo, lebo aj keď sa mi veľmi páči, už rok leží medzi ostatným materiálom a stále sa naň chystám.       Aby som nemala zlé svedomie, že výroba narodeninových náušníc bola taká jednoduchá, rozhodla som sa kamarátke ešte jedny ušiť - nechcela som nič pokaziť a radšej som zostala pri vyskúšanom vzore. A keďže rovnako ako mám rada výrazné farby, mám rada aj pastelové, rozhodla som sa pre jemnú bielo-krémovú kombináciu.          Vo väčšine prípadov (keď nemám v hlave konkrétny šperk) korálky nakupujem bezhlavo - aké sa mi páčia. Tak som kúpila aj pár kúskov mušľových gombíkov s dvoma dierkami. Čoskoro sa pre ne našlo originálne a krásne využitie v podobe nasledujúcich náušníc. Popri tom som si vyskúšala aj zatláčanie perličiek, čiže to, čo tu veľmi nevidieť - špeciálnu kovovú korálku použitú ako držiak pomocou jej stlačenia na konci nite, lanka alebo vlasca. Ja som použila netrhavú šmykľavú niť, ktorou som pred rokmi rokúcimi šila šaty pre bábiky, aby naozaj vydržali. Ktovie či to sa stalo náušniciam osudným, pretože pri jednej príležitosti sa nitka jednoducho vyvliekla a náušnica sa mi zviezla do ruky. Dodnes nie sú opravené, ale zostávajú začiatočníckym kúskom, na ktorý som pyšná. A stále chovám nedôveru voči zatláčacím perličkám :-)                      Mimochodom, ak chcete skladať bižutériu, kým si ešte netrúfate na seriózne drôtovanie, jasné že sa nemusíte uspokojiť s predpripravenými sadami. Takisto sa dajú kúpiť všetky potrebné komponenty samostatne - háčiky, ramienka, nity alebo retiazky, zapínania a korálky k nim.    A tu je ešte na ukážku iná varianta šitých náušničiek - tieto som podarovala kolegyninej dcére.  So spodnou časťou sa dá urobiť naozaj všeličo, iné varianty som však už neskúšala.           Veľa zdaru!      fotografie bižutérie © Aneta Čižmáriková, foto klieští © Koralky.sk    

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (21)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Zmena je život
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Pojednanie o čiernej smrti, 4. časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Pojednanie o čiernej smrti, 3. časť
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Za čo ma buzeruje anonymný admin SME blogov?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Aneta Čižmáriková 
                                        
                                            Pojednanie o čiernej smrti, 2. časť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Aneta Čižmáriková
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Mráziková 
                                        
                                            Ako tri Slovenky z UKF precestovali Ameriku
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Martin Pinter 
                                        
                                            Aké sú výhody exkluzívnej zmluvy pri predaji vašej nehnuteľnosti?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Spišiak 
                                        
                                            Sprayeri dejín.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Aneta Čižmáriková
            
         
        cizmarikova.blog.sme.sk (rss)
         
                                     
     
        Tento blog je od 16.4.2009 uložený k spánku, nájdete ma tu:    ~~~ Riddick's Realm ~~~   
  
aj tu:   


        
    
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    199
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    3351
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Knihy
                        
                     
                                     
                        
                            Z knihovničky knihovníčky
                        
                     
                                     
                        
                            Moje knižné lásky
                        
                     
                                     
                        
                            Sci-fi a Fantasy
                        
                     
                                     
                        
                            SF&amp;F cony
                        
                     
                                     
                        
                            • Dialógy o fantastike •
                        
                     
                                     
                        
                            Fanovinky
                        
                     
                                     
                        
                            Blogový nedenník
                        
                     
                                     
                        
                            Rozpravy o Islande
                        
                     
                                     
                        
                            Všeli-len-čo
                        
                     
                                     
                        
                            Kreatívec notorik
                        
                     
                                     
                        
                            Bavíme sa s Tubou
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            čitateľka, mama, Milenka
                                     
                                                                             
                                            Oľga Pietruchová
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            - od roku 2005 hodnotím filmy
                                     
                                                                             
                                            - ďalej píšem o knihách
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  




     






            
    
        
    
  
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 


 


