

   
 Všade, kam sa človek pohne, je obmedzovaný. Žije v krajine, ktorú si nevybral a musí poslúchať všetky tie zákony, ktoré si niekto zo 150 ľudí vymyslí a ostatní ich uznajú za dosť dobré na to, aby sa nimi zvyšných päť miliónov ľudí v štáte riadilo. Kde ostal obyčajný človek? O toho sa nikto nezaujíma. Ak sa ti nepáči, môžeš sa vysťahovať. Ja sa mám vysťahovať? Akým právom to môže niekto povedať. Dokonca aj ja (čo som sa narodil nie tak dávno), už žijem na Slovensku dlhšie ako Slovenská Republika. Tá má 17 rokov. A táto republika v zastúpení niekoľkých nemorálnych a bezcharakterných jedincov mi má rozkazovať, zakazovať a prikazovať. Neuveriteľné. 
   
 Ak človek nie je ochotný odovzdať svoj podiel na vedení štátu, akú má alternatívu? Ja ako jedinú vidím skutočne iba to, že nepôjdem voliť a od tejto nespravodlivosti sa dištancujem. Navrhnite mi lepšiu možnosť, ak nejakú  vidíte. 
   
 Teda človek, ktorého predkovia už na svete žijú X rokov (teda odkedy sa objavili prví ľudia - musí byť ich potomkom, inak by tu nebol), je uväznený v pravidlách, ktoré nespadli z neba, ale ktoré NIEKTO VYMYSLEL, a nazval ich zákonmi. Sloboda je povinnosť vybrať si z nejakých možností, vraví sa. Nikto však už nepovie že tých možností pri skutočnej slobode je neobmedzené množstvo, zatiaľ čo parlamentná demokracia ponúka na výber niekoľko možností - a jednu horšiu ako druhú. Toto je sloboda v ktorej žijeme. 
   
 Vyberte si nejakú stranu, o ktorej si myslíte že je najlepšia. Ak sa vám žiadna nepáči, trhnite si nohou. Nejaká vyhrá aj bez vášho hlasu. A bude vládnuť najbližšie štyri roky a rozhodovať o podmienkach, v akých žijete. Pretože žijete na území štátu XY. Kto by sa staral o to, čo vy chcete. Mali ste ísť voliť a bolo by všetko podľa vašich predstáv. Mohli ste sa - rovnocenne s ostatnými - podieľať na riadení štátu. A presadzovať v parlamente vaše postoje. Keby ste šli voliť, zaiste by ste boli spokojní. Nešli ste. 
   
 Tí čo nejdú voliť by sa teda mali zastupovať sami (keďže nedôverujú nikomu z ponuky "zástupcov"). Ale takto to nefunguje. Musia poslúchať vrchnosť ktorú nechceli. Milovaná demokracia. Úžasné práva človeka.Ba dokonca sú "nevoliči" aj osočovaní druhými ľuďmi pre tisíc a jeden dôvodov, pretože títo ľudia ("voliči") majú iný NÁZOR, ktorý im dáva právo kritizovať ten váš. 
   

