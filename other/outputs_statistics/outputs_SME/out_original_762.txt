
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Roman Daniel Baranek
                                        &gt;
                Varíme s medveďom
                     
                 Varíme s Medveďom - Fazuľové lusky sú prosto hmmm... 

        
            
                                    27.7.2008
            o
            12:00
                        |
            Karma článku:
                10.80
            |
            Prečítané 
            10610-krát
                    
         
     
         
             

                 
                    Ahojte, milí moji.  Dúfam, že tieto dva nenáročné recepty vás  oslovia napriek tomu, že čas pre fazuľové lusky už trochu pokročil.   Moja mama je tak trochu záhradný fanatik. A ako každý rok, tak aj tento mi dala poriadnu tlstú igelitku fazuľových luskov. Zvyčajne som z nich urobil skvelý prívarok. Tento krát som sa však rozhodol inak...
                 

                  Moja mama zvyčajne nenecháva fazuľové lusky dozrieť. A mne to príde, ako veľmi prezieravé. Veď suchú fazuľu možno kúpiť prakticky za 'pár šupov'. Ale také dobré voňavé očistené fazuľové lusky, práve vytiahnuté z mrazničky niekedy vo februári... Veď je to 'orgazmu blízka' rozkoš. :)     Moja mama to vie. A preto ich vyťahuje (v tom pre lusky najnevhodnejšom čase) z mrazničky a primiešava ich do polievok, omáčok a hlavne do zapečených jedál. A ja si vždy veľmi pochutím.     No a úplne na záver mojej luskovej ódy by ste mali vedieť to, čo už moja mama :) dávno vie, teda že fazuľové lusky po rozmrazení nehorknú, nestrácajú tvar a chutia ako čerstvé.     Zapečené lusky s bryndzou:     Potrebujeme:     Asi 0,5kg luskov, vrecko cestovín (kolienka, vretienka či hrubšie rezance...), 2 vajíčka, 125g bryndze (deti, veď skúste aj tvaroch, či balkánsky syr, ak chcete dietovať), olivový olej pomace, 1 malú smotanu na varenie, mletý muškátový kvet, sušené oregano, mleté čierne korenie a soľ.           A ďalej máme celkom ľahkú robotu:     Lusky očistíme, odkrojíme konce, (kde sa lusk prirastá k samotnej byline), prekrojíme na menšie napríklad 3cm kúsky, poriadne opláchneme v studenej vode a v posolenej vriacej vode uvaríme do mäkka. (cca 20 min varenia) Samozrejme môžeme lusky podusiť v pare.          Cestovinu uvaríme 'al dente'. Viete ako? Celkom jednoducho. Ubezpečujem vás, že 3/4 všetkých druhov cestovín na svete uvaríte 'na skus' tak, že ich vo vriacej vode necháte variť 1 minútu a potom ďalšie 2 minúty necháte v odstavenom hrnci vo vriacej vode postáť. A precedíte. Pridáte trochu oleja a premiešate. Pokiaľ potrebujete, aby vychladli, premiešavate opakovane.          Tak a teraz do olejom vymasteného pekáča nasypeme cestoviny, uvarené lusky, prilejeme trochu oleja, pridáme soľ, muškátový kvet, oregano a čierne korenie. Nepreháňame s množstvom korenín. Všetko pomiešame a uhladíme.          Na vrch nalejeme rozšľahanú zmes smotany, bryndze a 2 vajec.          Pečieme v stredne vyhriatej rúre tak 30 minút. Takto to vyzerá pred pečením a...          ... takto po ňom.             Deti, tesne po vytiahnutí z rúry sa to rozpadalo. Na fotku úhľadného štvorca by sme museli čakať do druhého dňa, a tak sme si (jazykom našej obľúbenej blogerky) povedali: NO A ČO? :)          Luskový šalát s grilovanými kuracími prsiami.     Potrebujeme:     0,5 kg kuracích pŕs, olivový pomace olej, 70 dkg fazuľových luskov, 0,5kg cherry rajčín, čierne korenie a soľ.     Na zálievku potrebujeme balsamikový  ocot, olivový 'extra vergine', horčicu, tymian, bazalku (sušené či čerstvé), med a soľ.          Začneme s kuracími prsiami. Žiadne veľké 'štráchy'. Opatrne ich rozrežeme od stredu najprv na jednu a potom na druhú stranu. Nemusia byť ani naklepané, ani nijako špeciálne tenké. Z každej strany osolíme a posypeme mletým čiernym korením. Necháme postáť.          Pripravíme si zeleninu. Hlávku šalátu rozdelíme na listy a dobre premyjeme. Lusky očistíme, umyjeme a v slanej vode uvaríme. Cherry rajčiny umyjeme a prekrojíme na polovice.          Pripravíme si nálev na šalát. 4 lyžice olivového oleja, 3 lyžice balsamicového octu, 2 lyžice medu, 1 lyžicu horčice, soľ a bylinky dobre pomiešame. Samozrejme ak máme čerstvé bylinky, mali by byť jemne nasekané.          Grilovaciu panvicu natrieme pomace olivovým olejom a zohrejeme na horáku na strednom stupni. Kuracie prsia položíme na panvicu a kým sa grilujú, sa ich ani nedotkneme.:) Počkáme (cca 4-5 min.) kým nám nezbelejú tak, ako je na druhom obrázku, teda väčšia časť pŕs už je biela. Počas grilovania vrchnú časť natrieme olejom.          Obrátime na druhú stranu a ešte cca 2 minúty grilujeme.           Vlastne je všetko hotové. Na každý tanier (vyjdú 4 veľmi slušné porcie) zvlášť položíme šalátové listy, nasypeme fazuľové lusky, na polovice prekrojené rajčiny a všetko mierne osolíme.          Na vrch pokladieme na kocky nakrájané ugrilované kuracie prsia a pomaly po lyžiciach polievame pripravenou zálievkou.          Podávame s čerstvým pečivom.          Dobrú chuť.           tenjeho        

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (62)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rok v kuchyni
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Adriana Macháčová, Róbert Dyda: Vieme prví
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Oškvarkové pagáče
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Rýchlovka - Hruškové tartelky
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Roman Daniel Baranek 
                                        
                                            Varíme s medveďom - Plnené kura
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Roman Daniel Baranek
                        
                     
            
                     
                         Ďalšie články z rubriky jedlo 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Kúpte si chladničku na webe, bude vám bez nej HEJ.SK!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Carlos Arturo Sotelo Zumarán 
                                        
                                            Je banán ovocie, alebo zelenina?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        MUDr. Peter Žiak, PhD. 
                                        
                                            Multivitamíny znižujú riziko vzniku sivého zákalu
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Peter Krško 
                                        
                                            Výživa podľa piatich elementov - tepelné pôsobenie potravín na organizmus
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Larisa Adámková 
                                        
                                            Bylinky a korenie v kuchyni dokážu liečiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky jedlo
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Roman Daniel Baranek
            
         
        baranek.blog.sme.sk (rss)
         
                                     
     
         Som chlap odvedľa. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    612
                
                
                    Celková karma
                    
                                                10.28
                    
                
                
                    Priemerná čítanosť
                    6507
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zápisky medveďa
                        
                     
                                     
                        
                            Varíme s medveďom
                        
                     
                                     
                        
                            Medveď mudruje
                        
                     
                                     
                        
                            Kapitolky z tanca a baletu
                        
                     
                                     
                        
                            Neďeľná chvíľka poézie
                        
                     
                                     
                        
                            Poviedka na Nedeľu
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Vylepšovač nálady
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Zuzana Navarova
                                     
                                                                             
                                            Jaromír Nohavica
                                     
                                                                             
                                            Mari Boine
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marek - psovitá šelma na SME
                                     
                                                                             
                                            Marian - skvele od skoromenovca
                                     
                                                                             
                                            Helena - si ma prekvapila, príjemne
                                     
                                                                             
                                            Intoni - vnímavé až éterické
                                     
                                                                             
                                            Janka - ľudské a dýchajúce
                                     
                                                                             
                                            Ivana - o ľuďoch a neľuďoch
                                     
                                                                             
                                            Andrea - písanie numero uno
                                     
                                                                             
                                            Natalia - múdre a citlivé
                                     
                                                                             
                                            Veronika - srdiečko a príroda
                                     
                                                                             
                                            Dušan - básničky ako hrom
                                     
                                                                             
                                            Karol - ako to, ze som ta zabudol
                                     
                                                                             
                                            Katarina - velmi nezne
                                     
                                                                             
                                            Iviatko - paradne fotky
                                     
                                                                             
                                            Zuzibeth  - nasa krvna skupina
                                     
                                                                             
                                            Boris - velmi silne citanie
                                     
                                                                             
                                            David - tesim sa vždy na všetkých
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Chlieb nas kazdodenny
                                     
                                                                             
                                            VEGETARIÁNI POZOR!!!
                                     
                                                                             
                                            Recepty na Netcabinet.sk
                                     
                                                                             
                                            Recepty na Objav.sk
                                     
                                                                             
                                            Kuchár len tak z pasie...
                                     
                                                                             
                                            Recepty od GABRIELA
                                     
                                                                             
                                            Malomestský gurmán - dalsi skvely o vareni
                                     
                                                                             
                                            delikatesy.sk - este nieco o vareni
                                     
                                                                             
                                            Lepsiu o vareni nepoznam
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nehaňte hipstera môjho
                     
                                                         
                       Milujú katolíci len tých, ktorí sú na kolenách?
                     
                                                         
                       Varíme s medveďom - Opité kuracie závitky
                     
                                                         
                       Byť ženou je úžasné...
                     
                                                         
                       Čo ešte mi chcete vziať?
                     
                                                         
                       Novodobý hon na čarodejnice alebo upaľovanie homosexuálov na hranici
                     
                                                         
                       Jozef Bednárik. Niet čo dodať.
                     
                                                         
                       Toto nie je diskriminácia, to len nefunguje
                     
                                                         
                       Z politiky sa vytratila česť?
                     
                                                         
                       Geeky coffee post
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




