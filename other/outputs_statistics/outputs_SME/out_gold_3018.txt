
 Otázky majú svoje
definitívne odpovede
a v hlase počuť ston
pevných objatí.

Na plátne zostávajú
vypálené dva tiene.
A časom vášeň 
stuhne ako chlad,

medzi brehmi rieka
zmení svoj smer.
Márne v nás 
hľadám mier.
 
