
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Milan Buno
                                        &gt;
                Nezaradené
                     
                 Ako úspešne podnikať? 

        
            
                                    5.5.2010
            o
            9:55
                        |
            Karma článku:
                7.39
            |
            Prečítané 
            2405-krát
                    
         
     
         
             

                 
                    Nečakajte, kým budú všetky svetlá zelené. Aj preto nie je viac ľudí úspešných, pretože majú strach z neúspechu a možných omylov. Niektorí ľudia zakrývajú svoj strach tým, že sa snažia byť dokonalí. Proste čakajú, kým všetko sadne do seba na milimeter presne, hviezdy na nebi sa dostanú do perfektnej konštelácie a až potom sa do niečoho pustia. Akoby nechceli vyjsť z garáže, kým nebudú všetky semafory na cestách zelené...a bežia s motorom naprázdno.
                 

                 Do rúk sa mi dostala kniha Roberta Kiyosakiho „Ako podnikať úspešne“, moja prvá od tohto autora. Musím povedať, že som si čítanie užíval, bavilo ma to, hoci by sa dala 300 stranová kniha vtesnať do stostranovej. Ale opakovanie je matkou múdrosti, všakže?   Kiyosaki upozorňuje na to, že stále je príliš veľa ľudí, ktorí chcú začať podnikať, ale nevedia vlastne ako. Chcú biznis, ktorý zarába veľa peňazí a ktorý budú riadiť ich zamestnanci, aby  mali oni sami viac voľného času a priestor na rodinu. A tiež nechcú na začiatku do toho až tak veľa investovať... Tak sa to však nedá. Kiyosaki rozoberá zle naplánovaný biznis a vysvetľuje prečo napokon neúspechy vedú k úspechom.  Veľmi inšpiratívna je kapitola o tom, ako zmeniť nepriaznivú situáciu na skvelú príležitosť (prečítajte si kapitolu Môj prvý biznis).   Musím súhlasiť, že školy nás často učia mať strach z toho, že by sme mohli vyzerať hlúpo. Niekedy máte možno aj vy pocit, že keď sa čosi spýtate, vlastne sa tým priznáte, že niečo neviete. Takýto mentálny blok máme mnohí a len ťažko si zvykneme na to, že človek sa vlastne učí tým, že kladie otázky. Schopnosť klásť otázky a ochota občas „vyzerať hlúpo“ otvára nové svety aj vynikajúcim študentom a vzdelaným ľuďom.   Kiyosaki samozrejme neobchádza staré známe a osvedčené – najdôležitejšie je majstrovsky využiť peniaze a zdroje iných ľudí!    Zaujímavo opisuje ako sa prihlásil na konkurz do Xeroxu, bez diplomu a skúseností, napokon ho prijali ako nádejného...a ďalšie dva roky bol najhorším predajcom. Nezlomilo ho to a dokázal sa prepracovať vyššie...musel však čosi obetovať, robiť istý čas zadarmo, učiť sa na svojich neúspechoch. V ďalších kapitolách tak rozoberá proces ako stať milionárom. Existuje niekoľko ciest:   -    Môžete zdediť peniaze -    Môžete sa pre peniaze vydať/oženiť -    Môžete žgrlošiť -    Môžete sa stať podvodníkom -    Môžete mať šťastie -    Môžete sa stať bystrými podnikateľmi.   Jednoduché? Nie tak celkom:-), ale Kiyosaki dokáže inšpirovať, navnadiť, upozorniť na zaujímavé aspekty.  Názorné sú napríklad jeho paralely medzi službou vo Vietname a podnikaním. V skratke – v oboch sú dôležité poslanie, vedenie a tím.   Menšiu výhradu mám k prekladu názvu – výstižnejší je podľa mňa originál Before you quit your job. Lebo o tom to je: čo potrebujete pre základný úspech pri podnikaní a čo treba vedieť a urobiť, skôr ako dáte výpoveď v práci. Tiež mi občas prekážalo to jeho self promo – na svoje ďalšie knihy, na svoju hru a niektoré spriaznené duše. Každopádne je však Kiyosaki dobrý rečník, vie podať univerzálne životné pravdy, ktoré si však neuškodí zopakovať, píše bez servítky, priamo, zrozumiteľne.   A zakončím to skvelým postrehom, ktorý mňa osobne nadchol:-)   Podnikateľský prístup znamená využívanie príležitostí bez ohľadu na zdroje, ktoré máte práve k dispozícii!  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (25)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Ako šli Sovieti kradnúť nápady do Ameriky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Marián Gáborík. Inšpirujúci príbeh
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Milan Buno 
                                        
                                            Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Milan Buno
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Milan Buno
            
         
        buno.blog.sme.sk (rss)
         
                        VIP
                             
     
         Robím to, čo milujem...po dlhých rokoch v médiách som sa vrhol na knihy a vo vydavateľstve Ikar mám všetky možnosti a príležitosti, ako si ich vychutnať naplno. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    297
                
                
                    Celková karma
                    
                                                7.40
                    
                
                
                    Priemerná čítanosť
                    2449
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Nesbov SYN sa blíži. Pozrite si obálku a čítajte 1.kapitolu
                                     
                                                                             
                                            Zažite polárnu noc (nielen) s Jo Nesbom!
                                     
                                                                             
                                            Jony Ive - Geniálny dizajnér z Apple
                                     
                                                                             
                                            Konečne prichádza Láska na vlásku so Celeste Buckingham!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Tretí hlas
                                     
                                                                             
                                            Mandľovník
                                     
                                                                             
                                            Dôvod dýchať
                                     
                                                                             
                                            Skôr než zaspím
                                     
                                                                             
                                            Tanec s nepriateľom
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            M.Oldfield: The Songs of Distant Earth
                                     
                                                                             
                                            Ludovico Einaudi
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Severské krimi
                                     
                                                                             
                                            Knižný svet na Facebooku
                                     
                                                                             
                                            Titulky.com
                                     
                                                                             
                                            Jozef Banáš
                                     
                                                                             
                                            Táňa Keleová-Vasilková
                                     
                                                                             
                                            BUXcafé.sk všeličo o knihách
                                     
                                                                             
                                            Noxi.sk
                                     
                                                                             
                                            Tyinternety.cz
                                     
                                                                             
                                            Mediálne.sk
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Šup, šup, najesť sa. A utekať!
                     
                                                         
                       Ako šli Sovieti kradnúť nápady do Ameriky
                     
                                                         
                       Štyri mesiace sedel mŕtvy pred televízorom, kým ho našli!
                     
                                                         
                       Marián Gáborík. Inšpirujúci príbeh
                     
                                                         
                       Knižný konšpiračný koktail alebo čože to matky na severe miešajú deťom do mlieka?
                     
                                                         
                       Paulo Coelho
                     
                                                         
                       Môže odfláknutá redakcia knihy pokaziť pôžitok z čítania?
                     
                                                         
                       Obdobia, z ktorých by deti nemali vyrásť
                     
                                                         
                       Tánina pekáreň
                     
                                                         
                       Malala - dievča, ktoré rozhnevalo Taliban
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




