
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martin Mojzeš
                                        &gt;
                Malý krok
                     
                 Malý krok č.1: Prispejem mesačne aspoň 7€ na neziskovku 

        
            
                                    30.1.2010
            o
            18:11
                        (upravené
                30.1.2010
                o
                18:24)
                        |
            Karma článku:
                5.17
            |
            Prečítané 
            951-krát
                    
         
     
         
             

                 
                    Haiti priťahuje našu pozornosť a sme ochotní venovať peniaze nejakej organizácii, o ktorej si myslíme, že ich použije tak, aby uľahčila utrpenie ľudí. Avšak toto naše rozpoloženie čoskoro skončí. A čo potom? A čo pri problémoch, ktoré na prvý pohľad nevyzerajú tak tragicky?
                 

                Mojim prvým malým krokom je pravidelný mesačný príspevok aspoň 7€ nejakej neziskovke.  Pravidelne túto sumu už takmer štyri roky posielam na účet organizácie Človek v ohrození. Dôverujem im a chcem v tom pokračovať, kým to bude aspoň čo i len trošku možné.   Prečo?  Sám pôsobím v neziskovke (Mládežnícka organizácia Plusko) a viem, ako veľmi môžu takéto peniažky pomôcť. Myslím si, že je dôležité, aby som sa ako človek zaujímal o to, čo sa deje okolo mňa a pokúsil sa veci ovplyvniť. Prispieť tak, aby ten malý svet naokolo (činžiak či ulica, v ktorej bývam, neďaleký park alebo škôlka, mladí ľudia či naopak starší v obci, ...) bol o kúsok lepší. Sú veci, pre ktoré som ochotný urobiť niečo aj priamo - zorganizovať sebapoznávací tábor pre mladých, upozorňovať e-mailami na nevyhovujúce podmienky pre nevidiacich v rôznych častiach mesta, pomôcť s vymaľovaním detského ihriska, podporiť na stretnutí ľudské práva na Kube či v Číne, atď.    Sú však aj veci, ktoré neviem priamo ovplyvniť, prípadne nechcem, nemám čas. No napriek tomu si myslím, že je dôležité, aby sa im niekto venoval. Nemôžem niečo pomôcť priamo zmeniť, no chcem zmenu podporiť. Neviem priamo pomôcť ľuďom na Haiti, neviem zlepšiť celé životné prostredie na Slovensku, neviem ubytovať všetkých bezdomovcov ani hneď zlepšiť politickú kultúru v krajine. Viem však podporiť tých, ktorí sa s tým snažia niečo urobiť. Aj keď len siedmimi eurami.        ---------------------  Malý krok č.0: Zapíšem si malé kroky  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Mojzeš 
                                        
                                            AIDS sa dá liečiť modlitbou, ale ...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Mojzeš 
                                        
                                            Malý krok č.2: Donesiem aspoň o 1 kus viac odpadu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Mojzeš 
                                        
                                            Malý krok č.0: Zapíšem si malé kroky
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Mojzeš 
                                        
                                            Aha: Prečo listnatým stromom na jeseň opadáva listie
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martin Mojzeš 
                                        
                                            Vďaka správam sa teším každé ráno do práce
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martin Mojzeš
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            KOMENTÁR PETRA SCHUTZA
             Len korupčné škandály preferencie Smeru nepotopia 
             Ak chce opozícia vo voľbách zvrhnúť Smer, bez rozšírenia repertoáru si neporadí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 23:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martin Mojzeš
            
         
        mojzes.blog.sme.sk (rss)
         
                                     
     
        alias Hoňo   
Ten, ktorý stojí počas splnu v kruhu na hrade. Ten, ktorý číta pri sviečke na opustenom ostrove. Ten, ktorý sa pozerá okolo seba a žasne.
  +ák telom i dušou :+)  
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    35
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2026
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Aha
                        
                     
                                     
                        
                            Malý krok
                        
                     
                                     
                        
                            Hľadanie
                        
                     
                                     
                        
                            Ďakujem
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Akože technika
                        
                     
                                     
                        
                            Ostatné
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Krásne dozrievanie
                                     
                                                                             
                                            Erich Fromm - The Art of Loving
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Cílená spětná vazba
                                     
                                                                             
                                            Erich fromm - Umenie milovať
                                     
                                                                             
                                            Gymnasion - časopis o zážitkovej pedagogike
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Plusko - ...o zážitok viac
                                     
                                                                             
                                            Mon' - úsmev plný života
                                     
                                                                             
                                            Bilinka - kvietočková princeznÁ
                                     
                                                                             
                                            Rybka = Degas + Remarque + pohľadenie slovami
                                     
                                                                             
                                            Le Pospo - príbehy s dušou
                                     
                                                                             
                                            Astronom - život inými slovami
                                     
                                                                             
                                            Rolo - ...
                                     
                                                                             
                                            Pupek - proste Pupek:)
                                     
                                                                             
                                            Ďalší Mojzeš...
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            O zážitok viac...
                                     
                                                                             
                                            Máš lepší počítač? Využi ho...
                                     
                                                                             
                                            Všetko o hoaxoch...
                                     
                                                                             
                                            Tí, čo pomáhajú ohrozeným deťom
                                     
                                                                             
                                            Tí, čo robia Bielu pastelku
                                     
                                                                             
                                            Pre pisálkov...
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




