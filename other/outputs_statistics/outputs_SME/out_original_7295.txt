
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Martina Monošová
                                        &gt;
                Celkom mimo
                     
                 Kristína, vďaka! 

        
            
                                    25.5.2010
            o
            23:54
                        (upravené
                26.5.2010
                o
                0:11)
                        |
            Karma článku:
                3.94
            |
            Prečítané 
            649-krát
                    
         
     
         
             

                 
                    Tak ako žiadnu súťaž krásy obyčajne nevyhráva tá skutočne najkrajšia, rovnako aj dnešné prvé semifinálové kolo Eurovízie neposlalo do finále len to najlepšie.
                 

                 Najkrajšie stromy sú možno na Horehroní, ale krupobitie z dnešného popoludnia zasiahlo prvý semifinálový večer v Oslo aj našu pesničku. Slovenskí hôrni tanečníci, milúčko vyzerajúca spevačka a vokalistka boli svojim vystúpením viac než len adekvátnym zástupcom, lebo melódia Horehronia mala naozaj veľkú nádej na úspech - v tomto kontexte nechápem napríklad postup belgického speváka...   Keď som pred týždňom na nete hľadala nejaké informácie o blížiacej sa Eurovízii, našla som zopár pesničiek, ktoré sa okamžite dostávajú pod kožu. You tube nimi len tak praskal vo švíkoch, kým o našej interpretke som sa toho veľa nedozvedala. Bola som veľmi zvedavá, čo ponúkame (tj. čo Slovensko ponúka) smerom "von", či už sa konečne vieme ponúknuť, zviditeľniť, no znovu sa raz potvrdilo staré ruské príslovie: Snažili sme sa, ale dopadlo to ako vždy.   Ako veľmi dobre vie každé marketingové oddelenie vo firme aj tretej kategórie: najdôležitejšia je reklama. Kristína a Horehronie malo byť nielen vidno, ale poriadne kričať! Veď sa nie je za čo hanbiť - práve naopak: konečne máme byť na čo hrdí! V prípade Kristíny sme reklamnú stránku Eurovízie hlboko podcenili.   Ak by som sa totiž chcela dostať k našej pesničke nepriamo, nedostala som sa vo vyhľadávači nikam a pri priamom zadaní našej interpretky do vyhľadávača som našla len záznam z televízie z nášho narodného finále. Tam, ako si všetci dobre pamätáme, Kristínka nepredviedla najlepší výkon, ale tento dojem sa hneď dal napraviť kliknutím na rozhlasovu verziu pesničky. Potom sa dalo dostať ku Kristíniným ďalším pesničkám, čo jej z hľadiska Eurivízie body nepridalo. Tiež sa mi zobrazil anglický preklad slov Horehronia, čo som brala ako pozitívum. No, ak vezmem do úvahy to, že odkazov na Kristínu s jej Horehroním bolo dohromady iba zopäť (ešte tam bol záznam z nejakého nácviku na javisku v Oslo, kde nebola tiež najhoršia), najviac ma zarazil záznam interview, ktoré Kristína poskytla (zrejme) po tom skúšobnom vystúpení...   Neviem, či sa od speváčky (podobne ako modelky) akosi automaticky očakáva znalosť angličtiny a neviem, ako je na tom Kristína so vzdelaním v tomto smere. Ale na otázky moderátora sa tvárila pomerne kyslo a neochotne. Ano, sama viem, aké je nepríjemné odpovedať na všetečné otázky moderátorov - mňa osobne prednedávnom jedna z istej známej a nemenovanej telky zarazila s povrchnou otázkou "Čo je to šťastie?", a keď sa jej potom nepáčila moja odpoveď, tak ma uprostred vety arogantne prerušila. Teda chápem Kristínu.   Chápem, že človek očakáva, že jeho práca hovorí zaňho... lenže showbiznis nie je literatúra! Kristínka, nikto nečakal, že budeš mať dokonalú angličtinu, ale snažiť si sa mohla! Aspoň zosmoliť nejakú základnú vetu alebo ťa ju niekto mohol naučiť! K takému milému stvoreniu ako ty nesporne si, by sa hodilo viac nadšenia z toho, že "spievam na Eurovízii"! Neviem pochopiť, ako je možné, že na otázku, o čom je tvoja pieseň, si mu nevedela odpovedať ani po slovensky! Všetko si to nechala na prekladateľa a pritom si sa tvárila znudene! Keďže si sa neunúvala s opisom toho, o čom spievaš, aspoň základná zúčastnenosť by sa bola hodila! Nemyslím si, že aj Madonnu baví dvadsať-dvadsaťpäť rokov spievať Like a virgin stále dokola, ale je to jej práca! Fanúšikovia to chcú počuť, a tak im to ona stále dokola spieva. K povolaniu speváčky skrátka už patrí nadšenie a energia - a po tejto stránke si sa podcenila sama...   Napriek relatívne malej propagácii našej pesničky na nete si myslím, že Kristína mala postúpiť do finále. Možno mala aj vyhrať. Bola rozhodne dobrá: mali sme krásnu speváčku, ktorá nepodľahla tréme, na javisku podala veľmi slušný výkon, a mali sme pesničku, ktorá nás svojou podstatou nielenže dobre reprezentovala. Bola melodická, veselá a aj živá. Zrejme ale iba ak by súťažili Česi, tak by mal odkiaľ zaduť vietor, ktorý by Slovenskej interpretke privial dostatok európskych hlasov! Takto je totiž našou jedinou šancou na presadenie sa iba pesnička, z ktorej všetci Európania v jedinom konkrétnom momente absolútne nadšení padnú na zadok! A také pesničky určite predznamenalo Horehronie.   Milá Kristína, napriek zopár drobnostiam, o ktorých som sa zmienila, si pre mňa bola jasná favoritka!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Nebuďte ovce a poďte voliť!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Nielen za zvukov hudby, no aj popri jej videoklipe...
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Zuhair Murad
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Ellie Saab
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Martina Monošová 
                                        
                                            Haute couture jar-leto 2014: Jean Paul Gaultier
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Martina Monošová
                        
                     
            
                     
                         Ďalšie články z rubriky kultúra 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Kuric 
                                        
                                            Zapletení - detektívka veľkého kalibru
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Hoferek 
                                        
                                            Referendum na tému "poďme všetko zakázať, všetko bude lepšie"
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Elena Antalová 
                                        
                                            Nebyť Wintonovho "dieťaťa", nevznikla by Francúzova milenka (dnes ČT 2, 21,45).
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Milena Veresová 
                                        
                                            Z hrnca do rozprávky
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Petra Jurečková 
                                        
                                            Oblečie sa za pútnika a vy mu dáte svoje peniaze
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky kultúra
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Martina Monošová
            
         
        monosova.blog.sme.sk (rss)
         
                                     
     
         O všetkom krásnom alebo šokujúcom, čo ma prinútilo napísať o tom aj čosi iné ako román:-) Som autorka románov (nielen pre ženy): Lásky o piatej (2005, 2012) Anglické prebúdzania (2006) Klišé (2010) Zlodeji bozkov (2012) Sladké sny (2013) Lekcie z nenávisti (2013) Miluje-nemiluje, idem! (2014) 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    119
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1068
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Celkom mimo
                        
                     
                                     
                        
                            Mixtúra
                        
                     
                                     
                        
                            Odraz spoločnosti
                        
                     
                                     
                        
                            Móda
                        
                     
                                     
                        
                            Recenzie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            O vzťahoch
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Etela Farkašová: Čas na ticho
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Caro Emerald
                                     
                                                                             
                                            Tiesto
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Súčasní slovenskí spisovatelia
                                     
                                                                             
                                            Iné názory
                                     
                                                                             
                                            Big girl likes fashion too
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            moja osobná stránka
                                     
                                                                             
                                            moda a tak
                                     
                                                                             
                                            super móda
                                     
                                                                             
                                            klasický interiér
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Haute couture jar-leto 2014: Ellie Saab
                     
                                                         
                       Haute couture jar-leto 2014: Jean Paul Gaultier
                     
                                                         
                       Haute couture jar-leto 2014: Chanel
                     
                                                         
                       Haute couture jar-leto 2014: Dior
                     
                                                         
                       Haute couture jar-leto 2014: Valentino
                     
                                                         
                       Akú cenu pre nás má Zlatý slávik?
                     
                                                         
                       Každý deň stretnúť človeka...
                     
                                                         
                       Robert Fico, Vy úbohý frajer, teraz zaplatíte 30 mega z Vašich?
                     
                                                         
                       Ako stratiť lásku
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




