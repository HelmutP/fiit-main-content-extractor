
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Matúš Demiger
                                        &gt;
                Majstrovstvá sveta v sudoku 20
                     
                 V. majstrovstvá sveta v sudoku, 2010, Philadelphia, USA - 1. deň 

        
            
                                    12.6.2010
            o
            17:16
                        (upravené
                10.7.2010
                o
                22:08)
                        |
            Karma článku:
                4.17
            |
            Prečítané 
            841-krát
                    
         
     
         
             

                 
                    Na prelome apríla a mája sa v americkej Philadelphii stretlo vyše 120 súťažiacich z celého sveta, aby si zmerali sily v riešení sudoku. Majstrovský titul obhájil Poliak Jan Mrozowski, v tímoch prekvapil nemecký mančaft a slovenská reprezentácia sklamala na plnej čiare.
                 

                 Diel 1. - "Nakúkanie do kultúry amerického národa ..."   Síce s vyše mesačným oneskorením, ale predsa len prinášam neobjektívnu správu z účinkovania slovenského tímu na piatych MS v sudoku.   Po úmornej kvalifikácii sa rozhodlo, že Slovensko budú na World Sudoku Championship (ďalej WSC) reprezentovať dva tímy. A-tím v zložení Pali Jaselský (Vojtovce; Gumkáči), Pišta Gašpár (Púchov; Gumkáči), Ivka Štiptová (Ružomberok; K1Z) a v B-tíme Pišta Gyürki (Šalov; K1Z), Zuzka Hromcová (Bytča; K1Z), Matúš Demiger (Zlaté Moravce; K1Z). Kapitánkou bola, tak ako vlani, Blanka Lehotská, zvyšok výpravy tvorili ešte Táňa Bezaniuková (hosť) a Rišo Hromec (hosť, otec). Keďže vlani v Žiline získal náš prvý tím zlaté medaily, tak aj tento rok mal len tie najvyššie plány. Tie však značne klesli odstúpením najväčšej hviezdy Petra Hudáka (zlato v tímoch 2009, bronz v jednotlivcoch 2007), preto bolo hlavným cieľom potvrdenie účasti v svetovej špičke a pokúsenie sa o čo najlepšie umiestenia v jednotlivcoch. Už teraz môžem prezradiť, že sa to vôbec nepodarilo ... ale ak chcete vedieť podrobnosti, tak čítajte ďalej.   1. deň - utorok, 27. apríl   Na začiatok by som chcel poďakovať E...joküllu za to, že sa po tých všade premieľaných peripetiách konečne (aspoň na chvíľu) umúdril a umožnil nám tak úspešný štart celého tohto šialeného výletu.    Tak sme sa všetci (rozumej všetci okrem Pištu Gašpára, ktorý mal prísť až po dvoch dňoch) spoločne vybrali z Bratislavy na viedenské letisko, aby sme tak započali našu dlhočiznú cestu pri dobývaní amerického kontinetu. V letišnom kníhkupectve sme síce nenašli žiadne Heineho knihy (pravdepodobne najznámejší autor sudoku, ktorého knihy sa celkom ťažko zháňajú), ale inak všetko prebiehalo prakticky bez problémov, vlastne jediné "obavy" vyvolávala skutočnosť, že amíci sa ešte stále neuráčili zvejerniť oficiálny inštruktážny bulletin (mali tak urobiť už týždeň pred súťažou) a tak sme okrem typov úloh (tie predsa len kostrbato publikovali včas) nevedeli prakticky nič. Pri čakaní na odlet sa nám síce niečo ďalšie podarilo objaviť na oficiálnej stránke WSC, ale to pokazený počiatočný dojem nenapravilo. A tak sme tam hodnú chvíľu sedeli, debatovali o pravidlách k tímovým kolám, sledovali čistenie fontány a pohľadom vyberali možných účastníkov majstrovstiev spomedzi spolučakajúcich. Najbližšie k tomu mala istá domáca dôchodkyňa, ktorá sa tam promenádovala s Heineho časopisom, ale napokon si to rozmyslela a nastúpila na iné lietadlo. My sme si už len vystáli šialene dlhý rad (od Atén až po Franfurkt) a mohli tak konečne opustiť pevnú pôdu Schwechatu.   Cesta do Franfurktu prebehla rýchlo a účelne, asi aj preto si nepamätám vôbec nič, čo by mohlo stáť za zmienku. Skúsim teda vynechať zbytočné detaily, napríklad aj to, že ani tu, v Nemecku (krajine známej predovšetkým pivom a Heineho sudoku), žiadne spomínané knihy nemali.   Nastal tak čas odletu do USA. Najskôr mňa a Pištu tety letušky zákerne presadili do druhej časti lietadla, ale našťastie naše pôvodné sedadlá ostali prázdne a tak sme sa tam nasáčkovali, či už sa im to páčilo alebo nie. Najskôr sme sa tam chvíľu aklimatizovali (kvôli klimatizácii), potom riešili nejaké bookletové úlohy, niektorí spali, potom sa znova lúštilo a to všetko sme stihli za šesť hodín oblúkovitého letu ponad popolavý Island, modrý Atlantik a zelené Grónsko. Aby som nezabudol, tak zlatým klincom celého programu v lietadle bolo dvojhodinové vypĺňanie vstupných formulárov, kde sme potvrdzovali, že v žiadnom prípade do USA "nenesieme" jedlo, potraviny, pochutiny a ani nič z tejto kategórie ... keďže všetci skôr vypĺňali ako čítali, tak im nemohlo byť čudné, že sme ôsmi použili asi 12 bielych a 14 zelených dotazníčkov. Tesne pred pristátím nás stihol nemecký kapitán informovať, že Bayern práve porazil Lyon (a dostal sa tak do finále Ligy majstrov), no nezdalo sa mi, že by to okrem neho niekoho trápilo. To sa skôr všetci tešili na pevninu a "krajinu neobmedzených možností" pod nami, lebo sedieť osem hodín v lietajucej konzerve nie je nič príjemné.   Po prílete, chvíľu po druhej poobede, sme prešli niekoľkými pasovými kontrolami, kde sme sa tvárili ako turisti, svetoví riešitelia sudoku a niektorí sa pre istotu netvárili vôbec. Napokon to dopadlo celkom dobre, nech už si o nás mysleli čokoľvek, do pasu nám prispinkovali akýsi zelený papier, ten nám vzápätí ďalší odtrhli a tak nám tam ostal len úbohý zelený úšklbok so spinkou, tým menej šťastným ostala len spinka. Následne sme dostali svoju batožinu, ale bezpečnostné prehliadky, pri ktorých sa mohlo objaviť prepašované jedlo, sa vôbec nekonali, jednoducho nás pustili ďalej. Po nádýchaní sa čerstvého neklimatizovaného vzduchu sme sa taxíkmi nechali odviesť do nášho stanoviska, malomesta Mount Laurel, vzdialeného asi 30 kilometrov od letiska. Cestou sa Pišta stihol porozprávať s afrikánskym vodičom o všetkom možnom, od sudoku, cez jeho rodinné vzťahy až po vieru v Boha. Druhá skupinka zase pre zmenu vyfasovala takého hudobne nadaného, ktorý si po celý čas spieval a tlieskal. Ale účel to splnilo, celkom pohodlne a rýchlo sme sa dostali na určené miesto a rovnako rýchlo a pohodlne sme sa tam ubytovali.   Po krátkom vybaľovaní a skúmaní všetkých možností a vymožeností ktosi prišiel na to, že bude dobrý nápad, ak skúsime nájsť miestnu autobusovú stanicu. Na druhý deň bol totiž naplánovaný trip do New York City a aby sme sa ráno nezdržiavali, tak to bol veľmi dobrý nápad. Lenže podľa amerického vzoru boli cesty a hlavne chodníky uspôsobené tomu, že sa tu dalo pohybovať asi len autom, pri výstavbe evidentne nikto nemyslel na početné slovenské turistické skupiny a tak chodníky končili surovo niekde uprostred trávnika. Po pár minútach sme to teda vzdali a prešli na plán B. Keďže volať kvôli tomu taxík sa niektorým zdalo malicherné, tak nám neostalo nič iné ako prebehnúť cez štvorprúdovú cestu a pokračovať v putovaní tam (alebo ako vravia slimáky o prechode na druhú stranu diaľnice "tam sa musíš narodiť"). Stanicu vôbec nebolo ťažké nájsť (akurát sme museli velikým oblúkom obísť plot), tak sme zistili, čo bolo treba a mohli sa vybrať späť do motela, znova krížom cez autostrádu. Pár ľudí sa ohlodlalo navštíviť neďaleké rýchloobčerstvenie (nebudem konkretizovať), ale potom sme sa už všetci pobrali do postelí, napriek tomu, že bolo ešte len nejakých deväť hodín večer a vonku svetlo ... aj keď tento deň predsa netrval len 24 hodín.   Tak, to by bolo úvodných 30 hodín našej misie, nabudúce (verím, že čo najskôr) sa dočítate viac o celodennom zájazde do New York City a o tom, čo sme tam videli a aj nevideli ... 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Demiger 
                                        
                                            Raster ku Krížovkárskemu päťboju IV-3
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Demiger 
                                        
                                            Raster ku Krížovkárskemu päťboju IV-2
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Demiger 
                                        
                                            Raster ku Krížovkárskemu päťboju IV-1
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Demiger 
                                        
                                            Časopis Lišiak - Výsledky korešpondenčnej súťaže
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Matúš Demiger 
                                        
                                            Slovenské sudoku v Indii
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Matúš Demiger
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Matúš Demiger
            
         
        demiger.blog.sme.sk (rss)
         
                                     
     
        Som študent novinárčiny, ktorý nemá na nič čas a preto sa venuje takým veciam ako je lúštenie hlavolamov a písanie blogu.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    605
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Majstrovstvá sveta v sudoku 20
                        
                     
                                     
                        
                            Majstrovstvá sveta v hlavolamo
                        
                     
                                     
                        
                            Lišiak - KP I
                        
                     
                                     
                        
                            Internetové sudoku turnaje
                        
                     
                                     
                        
                            Sudoku turnaje SZHK 2010
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




