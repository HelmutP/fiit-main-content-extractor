

   
 Ako vieš, u nás do volieb ostáva týždeň a ja dúfam, že o parlamentný flek neprídem. Asi ti nepoviem nič nové, keď poviem, že v tej hnusnej budove nejako veľmi čas nezabíjam, ale imunita a vplyv sa hodí, čo ti budem rozprávať. Dokonca moje auto minule nespoznala policajtka a robila problémy, p.ča jedna. Ďakujem bohu, že si to zdvojené občianstvo, alebo ký fras stihol vytiahnuť ešte pred našimi voľbami. Určite vieš, aký vietor si u nás zodvihol a to je vietor, ktorý teraz potrebujem, ako my jachtári hovorievame. 
 Všetci nám tu ubližujú. Fico nám bral jedného ministra za druhým a nakoniec nám zobral aj celé ministerstvo. Nemá v sebe kúska veľkorysosti. A zrazu aký národovec, aj voličov mi chce zobrať. Aj som sa nasral, aj ešte stále som, ale soptiť môžem len do výšky môjho vplyvu. A keby som prišiel aj o ten čo mám, tak je so Slovenskom amen. Čo tam po politike, presnejšie, po riešení problémov štátu a občanov, dobre je, kým sa vezieme. Keby som mal tvoje percentá, to by tu ináč vyzeralo, ku.va! 
 Teší ma, že ani ja ti nie som ľahostajný. Mám taký teplý pocit pri srdci, že aj mňa v tom vašom Maďarsku rešpektujete a som tam niekto. Vieš, že to nemyslím v zlom, keď na teba a na Maďarov ukazujem prstom a občas mi ujde aj nejaké to sprosté slovo, ale v tom som dobrý a vtedy bodujem. Ďakujem, že si mi dal takýto terč, veď akú inú kosť môžem voličom hodiť? Aj Trianon je fasa téma, len si zasa nevystrieľaj všetky patróny. Vždy je lepšie, keď hru vedieme my, ako nejakí sku.vení radikáli. 
 Len ťa prosím, nikomu, nikde a nikdy nespomínaj, že som ti písal. A ešte ti chcem povedať, že tak ako doteraz, aj odteraz sa na mňa môžeš spoľahnúť. Len aby sme ku.va toho 12-teho preliezli do parlamentu. Ak sa podarí, máš to u mňa! 
 S priateľským objatím, 
 tvoj Jano 
 P.S. Hľadám si nejaký nový pozemok, kam by som sa občas zašil. Nevieš o niečom pri Balatone, ako vy nesprávne voláte Blatenské jazero? Na Jadrane ma už tie ku.vy novinárske vyňuchali a u vás ma isto nikto nebude hľadať... 
   

