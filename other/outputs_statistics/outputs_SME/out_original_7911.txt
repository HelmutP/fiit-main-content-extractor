
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jaroslav Stankovič
                                        &gt;
                Silné zážitky
                     
                 Kaňoning v podzemí Slovenského krasu 

        
            
                                    3.6.2010
            o
            8:31
                        (upravené
                3.6.2010
                o
                9:49)
                        |
            Karma článku:
                7.68
            |
            Prečítané 
            1887-krát
                    
         
     
         
             

                 
                    Aká absurdná predstava. Veď kaňoning sa robí v kaňonoch, roklinách nie v podzemí tmavých studených jaskýň. A navyše ešte v Slovenskom krase. Keby tak vo vysokých horách. A predsa práve v Slovenskom krase máme jaskyňu, kde sa podzemný kaňoning dá zažiť.
                 

                 
Najväčší vodopád v podzemí SlovenskaJaroslav Stankovič
   Jaskyniari aj odborníci - speleológovia sa už viac ako 10 rokov vyrovnávajú s genetickou absurditou jaskyne Skalistý potok pod južnými svahmi Jasovskej planiny, neďaleko od Turne nad Bodvou. A práve vďaka tejto absurdite sa tu dá zažiť aj podzemný kaňoning. Táto jaskyňa (inak je to najväčšia jaskyňa v Slovenskom krase) má totiž stúpajúcu vetvu, ktorá sériou vodopádov a kaskád vystúpi viac ako 300 m do výšky.    Objavitelia postupne prekonávali vodopády smerom hore. Prieskum tu bol možný iba pri minimálnych prietokoch, kedy sa vodopády skôr podobali na sprchu, ako na bystrú podzemnú rieku. A navyše výstup smerom hore nijako neevokoval podzemný kaňoning. Dlho to vyzeralo tak, že táto jaskyňa bude stále vyhradená iba tým najvýkonnejším, najodolnejším speleopotápačom a za poriadneho prietoku ju aj tak nikto neuvidí.    Situácia sa diametrálne zmenila keď sa 3 hlavní organizátori prieskumu tejto jaskyne rozhodli, že musia prekopať do jaskyne druhý vchod vysoko vo svahu planiny, na mieste, kde sa podľa zamerania jaskyne podzemné priestory dostávajú najbližšie k povrchu. Trvalo to 7 rokov kopania, geofyzikálnych výskumov, pokusov a omylov, kým sa títo nadšenci prekopali v roku 2008 do priestorov stúpajúcej vetvy jaskyne. Bola to obrovská sláva a úspech, aký v Slovenskom krase nemá obdobu. Prieskum a výskum jaskyne mohol znova nabrať na intenzite. Nebolo potrebné podávať speleopotápačské výkony na hranici ľudských možností a pri každej akcii riskovať život.    Tým sa otvorila aj pre ostatných (suchých) jaskyniarov možnosť uvidieť tento zázrak prírody. Skutočnosť v mnohom prekonala moje predstavy, ktoré som o tejto jaskyni mal. Horný vchod viedol práve na jedno z najúžasnejších miest v podzemí Slovenska – k 31 m vysokému vodopádu. Vďaka dlhšiemu suchému obdobiu sme rozvodnený podzemný Skalistý potok dlho nevideli. Až čudný nástup zimy 2009/10 nás obdaril niekoľkými povodňami. Nemohli sme si nechať ujsť pohľad na tento  rozvodnený vodopád.    Ako sa blížime k vodopádu stupňuje sa nielen hluk, ale jaskyňa akoby sa celá chvela. Zostúpime pod vodopád po fixných rebríkoch. Prietok je asi 100 l/s. Voda búši do dna siene s výkonom 30 kW. Chvíľku stojím v nemom údive nad silou prírody. Potom si sadám asi 10 m od vodopádu. Po pár minútach som úplne mokrý. Jaskyňa na mňa pôsobí ako nejaká prastará bytosť. Prihovára sa mi čudným nízkofrekvenčným reliktným jazykom, ktorý zablokuje bubienky a preniká mi priamo do duše. Vidinou sa prenášam do katedrály v Štítniku a počujem Idu Rapaičovú ako recituje Proglas. Skutočne ho recitovala? Veď tie staroslovienské akordy viac vychádzali z priestoru katedrály, ako z jej úst. Celé telo mi vibruje. Pocit rezonancie je dokonalý. Zmocňuje sa ma akýsi ošiaľ, jaskyňa ma vťahuje do svojho magického vnútra. Podvedome cítim, že to je smer, ktorý vedie na koniec cesty. Celý život chodím po tejto ceste tam a doteraz aj zase späť. Cesta a človek musia byť vyrovnaní protivníci, aby bola aspoň rovnaká šanca dôjsť až na koniec, alebo sa vrátiť. 30 kW je veľa. S tým nemáme šancu bojovať. Samovražda nie je cesta. Treba počkať, kým sa sily vyrovnajú a potom sa vydať na cestu.   Koniec zimy – topenie snehu. Jaskyňa stále žije svojím búrlivým životom. Výkon klesol na tretinu, túžba vydať sa na cestu dosahuje euforický vrchol a sú tu aj tí správni ľudia, s ktorými sa dá takáto exkurzia absolvovať. Vlastne skôr ako kráčať, budeme zlanovať vodopády a prebíjať sa nimi naspäť. Piati vyrážame v ústrety podzemnému kaňoningu, dúfame, že dnes nás čakajú len samé radosti. Úzkym horizontálnym meandrom sa presúvame k prvému „závojovému“ vodopádu. Závoj sa však dnes prejavuje ako stena vody. Tí, čo nemajú aspoň vodeodolné kombinézy, sú okamžite do nitky mokrí. Voda má asi 8 °C. Táto nádhera nás natoľko vyburcuje, že zimu ani nepociťujeme. Ďalší úsek je pomerne schodný. Postupujeme na dne až 70 m vysokého meandra cez početné kaskády a menšie vodopády. Steny sú pokryté bielymi nástrekovými koralitmi. Výkonné svetlo robí z týchto priestorov pastvu pre oči. Prichádzame do Prachovej siene. Korality sa zmenili na biely prach. Začína sa objavovať aj pekná kvapľová výzdoba. Väčšina kvapľov sú však unikáty. Prevláda tu rozstriekaná a rozprášená voda nad skvapom. Kríčkovité kvázi stalagmity tvorené zhlukom rozvetvených koralitov som u nás v žiadnej inej jaskyni nevidel. Videl som ich však vo Venezuele na konci jaskyne Charlesa Brewera na stolovej hore Churí. Tieto sú kalcitové, venezuelské sú tvorené opálom.    Komínujeme, traverzujeme, obliezame vodu, aby sme nevymieňali teplú vodu na tele za novú dávku studenej. Je to nádhera. Hluk postupne stúpa. Čaká nás ďalší vodopád. Je to druhý najväčší vodopád v jaskyni, vysoký takmer 20 m. Rebrík však nevedie priamo hlavným prúdom, takže by nemal byť problém ho vyliezť aj naspäť. Zlaňujeme, robíme kyvadlá, aby sme sa vyhli hlavnému prúdu. Celkom úspešne sa nám to darí. Vďaka atom-bordelu (niektorí si ho budú pamätať ešte z vojenskej služby) som stále pomerne suchý. Natieklo mi iba do gatí. Ďalší vodopád už taká sranda nie je. Je síce vysoký (alebo skôr hlboký) iba 10 m, ale povrazový rebrík je priamo v hlavnom prúde vody. Pri vyliezaní budeme musieť tento 3 kilowatový motor prekonať. Chvíľu zvažujeme, či vôbec pokračovať. Cesta však vedie ďalej. Sily sú stále vyrovnané treba ísť. V slovinskom Sušci by sme podobný vodopád asi skákali, tu je hĺbka vody iba do 1 m skočiť sa nedá. Škoda! Zlaníme štyria, posledný ostáva čakať nad vodopádom. Tu už sranda naozaj skončila a je potrebné, aby každý sám zvážil svoje sily. Odporúčam aby sa tí, čo nemajú nepremokavé kombinézy vrátili naspäť. Peter pôjde s nimi a zaistí ich zhora lanom pri výstupe po rebríku. Určite by radšej postupoval ďalej, ale bezpečnosť priateľov je prvoradá. Spolu so Štefanom pokračujeme do útrob planiny.    Postupne zlaníme ešte 3 väčšie vodopády, z ktorých posledný bude tuhý oriešok pri výstupe. Zlezieme množstvo perejí a menších stupňov. Utkvel mi v pamäti jeden asi 5 metrový skok, kde voda priteká sprava, akoby na list turbíny a odteká doľava. Napredujeme vo dvojici ďalej. Veľmi sa nezdržujme, lebo vieme, že na povrchu budeme túžobne očakávaní. Za takýchto podmienok, ešte v tejto časti jaskyne nikto nikdy nebol. Nevedno, ako to môže dopadnúť. O Pištových schopnostiach vôbec nepochybujem, mne však zdravotné problémy za posledný polrok hodne ubrali na sile. Silu mi však dodáva „geroičeskaja neulavimosť truda“ ako by napísal ruský klasik. Nie je to presne to isté čo ma ženie ďalej, ale podstata je rovnaká. Blížime sa k sifónu. Zastaví nás však hlboké jazero. Voda pri takomto prietoku vystúpila o niekoľko metrov a ďalej je potrebné plávať. Nemáme však plávacie vesty, musíme sa vrátiť.    Prichádzame k prvému vodopádu. Tu pocítime plný tlak vody. Rebrík je priamo v hlavnom prúde. Zapnem sa do istiaceho lana a kyvadlom sa prehupnem cez vodu k rebríku. Ten je však krátky. Visím na rukách a nedokážem vložiť nohy do priečok. Prúd vody ma na chvíľu ochromil. Topím sa, neviem dýchať, voda ma tlačí dole. Zaberiem z posledných síl a „vymakám“ niekoľko priečok na rukách. Som natiahnutý ako červík, preto sa mi stále nedarí stúpiť na rebrík. Spustím sa naspäť, odsadnem do lana a nechám chvíľu na seba pôsobiť prúd vody, kým sa neprispôsobím jeho rytmu. Žiadna hrubá sila, musím to spraviť ako Gionov António z vody, ktorý dokázal v rieke prekonať aj tie najdivokejšie prúdy. Splynul s vodou - stal sa jej prúdom, rieka ho prijala. Akoby som našiel vo vodopáde nejakú medzeru. Krátko zabojujem na hranici možného a na plný výkon zdolávam vodopád. Úžasný pocit. Moje skvelé nefajčiarske pľúca idú úplne na doraz. Postupne sa skľudňujem. Dýcham normálne, tep klesá. Vylieza ku mne Štefan. Viem, že sa mi podarilo niečo podobné, ako Antóniovi - splynul som s podzemnou riekou. Necítim už v nej žiadnu hrozbu. Zdá sa mi priateľská, primeraná. Cesta na koniec cesty sa mení na šťastný návrat.   Kaňoning je adrenlínová aktivita v nádhernom prírodnom prostredí, ktorá má priniesť ľuďom nielen radosť, ale aj podporiť ich odvahu a sebavedomie. So skúsenými vodcami je navyše primerane bezpečná. Jaskyniarsky kaňoning perspektívu nemá. Je to ťahanie čerta za nohu. Stačí drobná chyba, alebo prítomnosť človeka, ktorý precenil svoje sily, a veľká radosť sa môže zmeniť na veľký žiaľ. Jaskyniarstvo nie je adrenalínová záležitosť. Riziko pri prieskume jaskýň nie je samoúčelné a múdri jaskyniari sa ho snažia v maximálnej možnej miere eliminovať. Nielen technickými prostriedkami, ale často iba svojimi schopnosťami, skúsenosťami a zodpovednosťou. Návšteva rozvodneného Skalistého potoka výrazne posunula moje schopnosti a skúsenosť, aj keď musím priznať, že za cenu vystavenia sa extrémnemu riziku.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (4)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            50. výročie objavu Krásnohorskej jaskyne
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Jeden pohľad 2 koruny
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Najkrajšia hrebeňovka Volovských vrchov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Peter
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jaroslav Stankovič 
                                        
                                            Okolo Rákoša
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jaroslav Stankovič
                        
                     
            
                     
                         Ďalšie články z rubriky zábava 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jozef Bednár 
                                        
                                            Ako Mikuláš moju dcérku o čižmičku „obral“
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Paculíková 
                                        
                                            Aké sú vaše topánky?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Marian Letko 
                                        
                                            Maroško to má rád zozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Fero Škoda 
                                        
                                            Nemecký vlog o horách, vode a takmer stratených nervoch
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ján Vilček 
                                        
                                            Kritizovať politiku Absurdistanu inotajmi absurdít
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky zábava
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jaroslav Stankovič
            
         
        jaroslavstankovic.blog.sme.sk (rss)
         
                        VIP
                             
     
        Som starý blázon, ktorý si zo záľuby urobil profesiu a z jaskyniara sa stal správcom Krásnohorskej jaskyne.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    76
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2042
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Silné zážitky
                        
                     
                                     
                        
                            Jaskyne
                        
                     
                                     
                        
                            Návštevníci
                        
                     
                                     
                        
                            O dobrých knihách
                        
                     
                                     
                        
                            Radzim
                        
                     
                                     
                        
                            Venezuela
                        
                     
                                     
                        
                            Rožňavské cyklotrasy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Jan Piasecki, Rukopis nalezený v Zaragoze
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




