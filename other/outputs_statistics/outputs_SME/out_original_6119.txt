
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Dominika Kalánková
                                        &gt;
                Nezaradené
                     
                 Ovládame jazyky 

        
            
                                    8.5.2010
            o
            15:43
                        (upravené
                8.5.2010
                o
                16:21)
                        |
            Karma článku:
                6.36
            |
            Prečítané 
            1806-krát
                    
         
     
         
             

                 
                    V dnešnej dobe už nestačí ovládať jeden jazyk. Na školách sa už od základnej školy učia dva - jeden je angličtina , z ktorej sa neskôr maturuje a druhý môže byť španielčina , nemčina ruština alebo francúzština. Avšak v škole nás nenaučia ovládať oba jazyky rovnako dobre. Preto si možno niektorí hovoríte , že by ste vyskúšali jazykovú školu, ktorá však nie je zadarmo. Raz dva či tri krát vynecháte kurz a máte pocit , že neviete o čo na hodinách ide. Preto to vzdáte a peniaze sú zbytočne vyhodené...
                 

                 V každom prípade cudzí jazyk musíte ovládať a už nielen kvôli maturite, alebo príjimačkám na vysokú školu , ale v každom druhom zamestnaní je potrebné vedieť po anglicky alebo po nemecky. Čo s tým , keď nemáte čas pravidelne chodiť na hodiny do jazykovej školy ? Odpoveď je jednoduchá - "navštívte" svoju jazykovú školu priamo doma. Z pohodlia domova stačí zapnúť internet a "naťukať" príslušnú webovú stránku. Stačí "Spustiť kurz" a učiť sa. Výhod to má plno , jednou z nich je aj to , že to nie je obmedzené , je to zadarmo a môžte sa učiť kedy len chcete resp kedy máte čas ( ráno , večer ,poobede) , časovo skutočne neobmedzené - môžte sa učiť hodiny, či pár minút. K dispozícii je stovka cvičení pre všetky úrovne. Samozrejme , má to i jednu výraznú nevýhodu a to , že pre úplných začiatočníkov , ktorí ani nechyrovali o jazyku ako takom to nie je vhodné . Hneď pri prvých cvičeniach sa totižto dostávate k základným slovesám , ktoré podľa počítača by ste mali ovládať - problém by to nebol keby ste si ich vyhľadali v slovníku , lenže je tam potrebné ich už aj skloňovať . Po pár ďalších cvičeniach sú slovesá náročnejšie aj nepravidelné. To je však len jedna nevýhoda..Vhodné je to však pre všetkých , ktorí si chcú jazyk zdokonaliť alebo sa naučiť ropzrávať a pod.   Prvou stránkou , ktorú vrelo odporúčam je :   http://vyuka.lide.cz/home.aspx   Výhody :   - 6 ponúkaných kurzov ( angličtina , nemčina ,ruština, francúzština, taliančina, španielčina )   -3-5 jazykových úrovní   -500-1500 cvičení   -200-400 jazykových hodín   -zvuková výučba   -obrázky a fotografie   -úplne zadarmo   -nie je potrebné sa registrovať   -ak sa zaregistrujete máte k dispozícii aj viac funkcií a možností + ak sa prihlásite - kde skončíte , tam vás to zapne na druhý deň , týždeň ... funkcia pamäte   -najnovšie jazykové "reformy"   Nevýhody:   -nie je vhodné pre úplných začiatočníkov       Druhá stránka , ktorú uvediem , nie je vhodná pre anglicky nehovoriacich pretože je to anglická stránka , kde sa však dá naučiť aj iným jazykom ako 6 základným.   http://www.livemocha.com/   Výhody   -možnosť štúdia viacerých jazykov naraz   -tiež je možné naučiť sa jazykom, ktoré nie sú veľmi známe (hlavne pre tých , ktorí majú dostatok času a schopností učiť sa )   -aj keď je to stránka písaná v angličtine je zrozumiteľná (aspoň pre mňa)   -zadarmo   Nevýhody   -je potrebné sa zaregistrovať , čo aj nejaký ten čas trvá (kým pošlú spätný mail), avšak je to úplne zadarmo   Myslím si , že takéto stránky, kde sa dá online učiť jazykom, sú veľmi dobré hlavne pre študentov, ktorí veľa času trávia hlavne na zoznamkách. Je to zbytočné mrhanie časom , zatiaľ čo jazyk využijeme naozaj všade a to nielen u nás doma , ale aj  vo svete.   Dúfam , že aspoň jedna z týchto dvoch stránok vám pomôže zdokonaliť si jazykové schopnosti , či už v angličtine alebo hocijakom inom jazyku. Držím palce pri učení  :-) 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kalánková 
                                        
                                            Skutočný príbeh - Chcel zomrieť pre lásku
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kalánková 
                                        
                                            Žijeme pre lásku ?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Dominika Kalánková 
                                        
                                            Začalo to jednou poštou
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Dominika Kalánková
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Dominika Kalánková
            
         
        kalankova.blog.sme.sk (rss)
         
                                     
     
        "Život je ako bonboniera , nikdy nevieš aký kúsok ochutnáš." Citát ako stvorený pre mňa ;)
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    4
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2147
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Láska a vzťahy
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




