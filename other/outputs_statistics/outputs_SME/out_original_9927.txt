
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Marcel Burkert
                                        &gt;
                Potulky po USA
                     
                 Východným Utahom 

        
            
                                    14.7.2010
            o
            20:35
                        (upravené
                14.3.2014
                o
                15:58)
                        |
            Karma článku:
                7.82
            |
            Prečítané 
            1432-krát
                    
         
     
         
             

                 
                    Tak ako v Colorade, ktorému bol venovaný predošlý článok, je aj v Utahu toho dosť na obdivovanie nie len v národných parkoch. Niečo naznačuje aj ani nie 100 kilometrový úsek z hraníc medzi týmito štátmi popri rieke Colorado do mestečka Moab.
                 

                    Cesta z hraníc do východiska národných parkov Arches a Canyonlands - Moabu vedie spočiatku cez široké trávnaté pláne.      Z cesty je vidieť pohorie La Sal Mountains, ktoré veľkosťou pripomína Vysoké Tatry. Tiahne sa do dĺžky 26 km a jeho najvyšší štít Mount Peale má 3.877 m. Výšku 3.700 m v pohorí presahuje ešte ďlších 8 štítov. Na tejto fotografii to síce vidieť nie je, ale ešte začiatkom júla v tejto miestami polopúštnej krajine sa na vrcholoch ešte drží sneh.      Reklama na pivo Corona alebo tiež zakázaný alkolický pitný režim na cestách v horúčavach dosahujúcich takmer štyridsiatku.      Prvý dotyk s riekou Colorado v Utahu a to tesne za sútokom s tiež splavnou Dolores river. Colorado možno nevyzerá nejako zvlášť široká, má však vzhľadom na svoju šírku hlboké koryto.      Polovica cesty do Moabu kopíruje nám neznámy kaňon, resp. kaňony rieky Colorado. V miestach kde je kaňon úzky sa cesta od rieky odkláňa no napiek tomu ponúka takmer dych vyrážajúce prírodné scenérie:      a to nejde o žiadny národný park.         Viaceré tieto fotografie boli robené z auta. Niektoré pekné zábery mi ušli, keď som šoféroval a moji spolupútnici už boli natoľko presýtení krásou, že na jej zvečňovanie časom rezignovali.      Ale ja som sa, keď som mal možnosť, nevzdal...      ...lebo som vedel, že doma toho budem v opačnom prípade ľutovať, keďže sa tu s veľkou pravdepodobnosťou už nikdy nevrátim.      Na chvíľu sa objavili aj zavlažované polia, ale civilizácia stále žiadna.            Konečne aká taká "civilaziation". "Keď bývate v típi nie je vašim domovom iba samotné típi, ale aj pole, lúka, rieka okolo, živočíchy...".      Fascinujúce vinice v polopúšti. Jeden z mojich najväčších zážitkov tohto úseku cesty po USA.               Toto samozrejme nie je Colorado, ale len jeden z jeho menších prítokov vyvierajúcom v Národnom parku Arches.      Most cez Colorado tesne pred Moabom.      Do mestečka Moab sme dorazili pri zapadajúcom slnku, ktoré okolité púštne hory sfarbilo do zlatova. Pre vyprahnutého "pútnika" dospelého jedinca neexistovalo po takejto ceste asi nič lepšie ako vychladená mexická Corona s citrónom. (V USA sa v obchodoch predáva za ľudové ceny, trojnásobne lacnejšie v porovnaní so Slovenskom. Podobne je to aj s írskym pivom Guinness).      "Údolie Moabu" sa tiahne zo severu na juh. Jeho severný okraj križuje rieka Colorado (na obrázku ukrytá v zeleni na pravo) a na juhu sa vypína už spomínané pohorie La Sal Mountains.      Pohľad z mostu na Colorado, ktoré pri Moabe trochu pripomína šírkou a okolím rieku Níl. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (12)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Východ najzápadnejšieho Orientu
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Komu hlavne slúži mestská polícia. Napr. v bratislavskej Dúbravke
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            B. Dúbravka a krásy D. Kobyly a okolia XV - XVI. Plus retro
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Aspoň na bilbordoch som "nezávislým"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Marcel Burkert 
                                        
                                            Kto chce občanom slúžiť, musí si ich najprv kúpiť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Marcel Burkert
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Marcel Burkert
            
         
        burkert.blog.sme.sk (rss)
         
                        VIP
                             
     
         Spoluzakladateľ OZ Stop alibizmu, učiteľ, príležitostný turistický sprievodca po horách a krajinách a od 15.12.2014 poslanec miestneho zastupiteľstva BA-Dúbravka za stredopravú koalíciu. Prvé články na blogu SME zverejnil v júni 2007: "Mýty a zvrátenosti vo vzdelávaní" a "Matka prírody - Boh materialistov". Od r. 2010 prispieva aj na blog spomínaného OZ.  
        
     
     
                     
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    276
                
                
                    Celková karma
                    
                                                7.07
                    
                
                
                    Priemerná čítanosť
                    2343
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Výroky na zamyslenie 11/2014
                        
                     
                                     
                        
                            Rómska otázka
                        
                     
                                     
                        
                            Duchovné otázky
                        
                     
                                     
                        
                            Polícia s.r.o.
                        
                     
                                     
                        
                            Marenie výkonu cestnýh kontrol
                        
                     
                                     
                        
                            Školstvo a výchova
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Muž a žena
                        
                     
                                     
                        
                            Ekonomika
                        
                     
                                     
                        
                            Politika, spoločnosť, mediácia
                        
                     
                                     
                        
                            Zahraničná politika
                        
                     
                                     
                        
                            Ochrana a bytosti prírody
                        
                     
                                     
                        
                            Environmentálna kriminalita
                        
                     
                                     
                        
                            Cesty z bludného kruhu
                        
                     
                                     
                        
                            Dúbravka, Bratislavsko
                        
                     
                                     
                        
                            Krásy Devínskej Kobyly
                        
                     
                                     
                        
                            Potulky Slovenskom a ČR
                        
                     
                                     
                        
                            Potulky po Európe a okolí
                        
                     
                                     
                        
                            Potulky po USA
                        
                     
                                     
                        
                            Potulky maratónske
                        
                     
                                     
                        
                            Šport netradične
                        
                     
                                     
                        
                            Zvrátenosť vrcholového športu
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Ako eliminovať vlády s.r.o.
                                     
                                                                             
                                            Ako prichádza polícia o produktívnych policajtov
                                     
                                                                             
                                            Manželské sexuálne povinnosti??? Duchovný pohľad
                                     
                                                                             
                                            Hodnotová slepota - Rusko vs. USA
                                     
                                                                             
                                            Manuál mladého konšpirátora
                                     
                                                                             
                                            Rodinné prídavky len na 2 deti=1. krok k riešeniu rómskej otázky
                                     
                                                                             
                                            21 zvrátenosti v Policajnom zbore
                                     
                                                                             
                                            Sonda do duše politika
                                     
                                                                             
                                            Museli sme zabiť Božieho Syna, aby sme mohli byť spasení...
                                     
                                                                             
                                            Čím neškodnejšia polícia, tým väčšia korupcia
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Vo svetle Pravdy
                                     
                                                                             
                                            Zaviate doby sa prebúdzajú
                                     
                                                                             
                                            Kráľ Lear
                                     
                                                                             
                                            Sokrates (od D. Millmana)
                                     
                                                                             
                                            Rešpektovať a byť rešpektovaný
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Rudolf Pado
                                     
                                                                             
                                            Martin Škopec Antal
                                     
                                                                             
                                            Ján Macek
                                     
                                                                             
                                            Radovan Bránik
                                     
                                                                             
                                            Miroslav Kocúr
                                     
                                                                             
                                            Marek Strapko
                                     
                                                                             
                                            Ján Galan
                                     
                                                                             
                                            Ján Žatko
                                     
                                                                             
                                            Peter Konček
                                     
                                                                             
                                            Peter Farárik
                                     
                                                                             
                                            Slavomír Repka
                                     
                                                                             
                                            Jozef Kamenský
                                     
                                                                             
                                            Michal Legelli
                                     
                                                                             
                                            Karol Kaliský
                                     
                                                                             
                                            Michal Wiezik
                                     
                                                                             
                                            Boris Kačáni
                                     
                                                                             
                                            Vlkáč Juraj Lukáč
                                     
                                                                             
                                            Smädný pútnik
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Svet Grálu
                                     
                                                                             
                                            O cigánoch. Jozef Varga
                                     
                                                                             
                                            Môj iný blog
                                     
                                                                             
                                            Dúbravčan
                                     
                                                                             
                                            Šachový klub Dúbravan
                                     
                                                                             
                                            Na skládky nie sme krátki
                                     
                                                                             
                                            Lesoochranárske združenie VLK
                                     
                                                                             
                                            JUDr. Ing. Ivan Šimko
                                     
                                                                             
                                            Roman Kučera - cestovateľ
                                     
                                                                             
                                            Toulky
                                     
                                                                             
                                            Bežecké dojáky
                                     
                                                                             
                                            Vitariánska reštaurácia
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Hrozba vojnového konfliktu nie je zahranično-politickou otázkou! Váš minister zahraničia
                     
                                                         
                       Zákaz bilbordovej kampane - prvý krok k zrovnoprávneniu šancí kandidátov
                     
                                                         
                       Nechci zmeniť celý svet. Zmeň len svoje okolie
                     
                                                         
                       Dear John, odchádzam z blogu SME
                     
                                                         
                       111 krokov, ako vrátiť Bratislavu Bratislavčanom
                     
                                                         
                       Páni Milanovia, mňa nezastavíte
                     
                                                         
                       Tak oblečme si dresy, ale ...
                     
                                                         
                       Sonda do duše politikov a voličov
                     
                                                         
                       Ficov minister posadil svojho šéfa do bytu Goríl a on neprotestuje!
                     
                                                         
                       Je potrebné deštruovať ľudskú blbosť a nie volať po rekonštrukcii štátu
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




