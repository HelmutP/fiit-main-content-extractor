
 Ani neviem ako, a zrazu som sa ocitla na psychiatrii. Teraz sklamem všetkých svojich neprajníkov, pretože som tam nebola kvôli sebe, ale kvôli jednému nášmu klientovi. Volali nám, že potrebuje doniesť pár vecí, tak vyslali mňa.
Nebolo ľahké dostať sa tam, moje zvonenie a búchanie zaregistrovali až zhruba po štvrť hodinke, čo však bolo ešte ťažšie - dostať sa odtiaľ von. Na dverách gule, personál rozlietaný po oddelení... 
Zrazu však niekto odomkol dvere zvonku a vstúpil. Bol to mladý sanitár. Pribehla som k nemu:
"Dobrý deň, prosím vás odomknete mi?" 
Mladý na mňa nedôverčivo pozrel.
"Nebojte sa, ja neutekám" - chcela som ho posmeliť.
Vytiahol kľúče z vrecka a začal odomykať dvere.
"Vás práve pustili, však?" - spýtal sa ma s kamennou tvárou, ale chápanlivým tónom.
Vybuchla som smiechom a odkráčala preč.

Už dávnejšie počúvam od rôznych ľudí, že sa nemám tváriť tak vážne a viac sa usmievať, ale zistenie, že pôsobím aj ako duševne chorá bolo pre mňa nové.
Neprívetivý cvok, no to som to teda dopracovala... Asi by som sa mala nad sebou vážne zamyslieť, no mne je to neskutočne smiešne. Ale aby ste nepovedali, že som si vôbec nevstúpila do seba, dávam si predsavzatie. Nikdy to síce nerobím, ale len kvôli vám si jedno jarné dám:
- nebudem sa nasilu priblblo usmievať, len aby niekto nepovedal, že som priveľmi vážna
- neznášam zdvorilostné rozhovory, vediem ich len vo výnimočných prípadoch a nemienim na tom nič meniť
- minimálne na najbližších 365 dní sa zriekam boja o titul "zdvorilý cvok", alebo nebodaj "milá mladá dáma"
- aj ja mám občas pocit, že mi mierne zašibáva, takže s kľudom si to o mne myslite aj vy.

Nebudem brať život príliš vážne, pretože on si zo mňa každý deň strieľa.  ;-)

 
