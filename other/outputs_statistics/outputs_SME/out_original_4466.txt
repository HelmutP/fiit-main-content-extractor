
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Rudo Hvizdos
                                        &gt;
                Nezaradené
                     
                 Prečo je Fico naozaj bez chrbtovej kosti 

        
            
                                    13.4.2010
            o
            8:57
                        (upravené
                8.6.2010
                o
                9:03)
                        |
            Karma článku:
                5.00
            |
            Prečítané 
            1089-krát
                    
         
     
         
             

                 
                    Každý z nás si určite dobre pamätá na nie až tak dávne vyhlásenia súdruha Fica o tom, ako ten život pred nežnou revolúciou nebol až taký zlý, a že tie štrngajúce kľúče ktovie akú zmenu jemu osobne nepriniesli.
                 

                 
 Shooty
                   Či tomu tak naozaj bolo a mladomanželské odskočenie rovno na imperialistickú Maltu, bolo skutočne také nevinné a jednoduché, opísal veľmi pekne Shooty vo svojej rubrike denníček z 18. 11. 2009. Každého, kto nečítal, vrelo odporúčam na shooty www, fakt dobré. Čo však na jeho výrokoch zaráža mňa, je tá hlboká ignorancia, sprostosť, obmedzenosť, necitlivosť a neskutočný alibizmus jedného veľkého pokrytca.   Pozrime sa na to takto. Ak by som aj dal súdruhovi Ficovi za pravdu, čo mi fakt ani pri všetkej snahe nechce ísť. Ale dobre, skúsme! Povedzme, že sa hráme na krásnu červenú rozprávku a prižmúrime obe oči. Predstavme si krajinu pod krásnou červenou hviezdou, kde každý poslušný a nádejný právnik má dvere otvorené nielen do strany, ale aj do sveta a vôbec nevadí, keď si po ceste do tej ružovej, pardon červenej budúcnosti, odskočí každú nedeľu aj na prijímanie. O peniaze tiež nieje žiadna núdza, nakoľko štipendiá pre strane oddané deti predavačiek, sú viac ako štedré a ešte ľahšie dostupné.  Life is just beautiful.   Dobre, ale čisto hypoteticky, čo ak som ja vnuk deda, ktorého poslali na 12 rokov na sibír, lebo na zlej strane fronty proti nacizmu bojoval. Otca mi pre vykonštruované obvinenie z kontrašpionáže, dali jadrový priemysel v Jáchymovských baniach, rozvíjat a ja som tak celkom rád, že si aspoň učňovku môžem dokončiť. Terka je zas vnučka starkej, čo odmietla svoj statok, len tak za nič strane podarovať a dcéra mami, ktorej sa červená budúcnosť veľmi ružová nezdala a tak chartu podpísala. Čo ona? Ona je rada, že má mamu po dvoch rokoch zase doma a "nie" z každej školy, jej preto ako veľká tragédia nepríde. Čo tých "x" obetí komunistického režimu a ich potomkovia. Čo oni? Oni si na Maltu s mladomanželkami užívať, za cudzie peniaze určite nechodili.   Ako môže niekto povedať, že ten režim nebol až taký zlý. Veď posielal ľudí na smrť len pre verbálny nesúhlas, kritiku, nešťastné okolnosti, strach z ich osobností či nevyhovujúcich predkov. Nech sa tam v tej diktatúre Fico a jemu podbní kľudne mali dobre. My ostatní, tí s chrbtovými kosťami však nemôžeme zabudnúť na tých, ktorým podobné šťastie dovolené nebolo. Žiadny systém, ktorý zatvára ľudí len pre ich názor na veci verejné, nie je dobrý a vôbec nezáleží na tom, či produkuje zopár sťastných individuí bez chrbtových kostí, ktorí sa tým ešte ani nehambia chváliť. Ak je niečo skutočne dobré, musí to obstáť aj kritiku. Veď to by Fico nabudúce mohol povedať, veď ani taký nacizmus nebol iba zlý. Ono je však morálne irelevantné, či nemorálny režim postranne vypustí nejake pozitíva. Ľudia s chrbtovými kosťami, môžu len ťažko budovať svoje šťastie v prostredí, kde iní trpia len preto, že sa na veci pozerajú trocha inak.   Preto si myslím, že tá chrbtová kosť nášmu súdruhovi premiérovi skutočne chýba, a keď sa tak pozerám na to množstvo jeho žalôb na denníky, možno mu chýbajú aj časy, keď sa za názory zatváralo. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (10)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Ficovo registrované partnerstvo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Nevedomosť je sladká
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Kočpédia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Novinárske hrdinstvá
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Rudo Hvizdos 
                                        
                                            Poslanecká anarchia
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Rudo Hvizdos
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Rudo Hvizdos
            
         
        hvizdos.blog.sme.sk (rss)
         
                                     
     
        ..infidel, co ma rad slobodu, racionalnost a zmysel pre humor..
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    35
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    980
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Nová tvár zneužívania psychiatrie v trestnom konaní
                     
                                                         
                       Tereškovová už zasa sklamala
                     
                                                         
                       Naozaj potrebujeme viac policajtov, pán minister?
                     
                                                         
                       Dejiny katolíckej cirkvi a neomylnosti podľa Hansa Künga
                     
                                                         
                       SPP: Klasický tunel v podaní muža s kolou
                     
                                                         
                       Manuál mladého konšpirátora
                     
                                                         
                       SaS opúšťajú len tí členovia, ktorí tam nepatrili
                     
                                                         
                       Ako je to skutočne medzi premiérom, Janou a Modrým z neba
                     
                                                         
                       Ficoidi útočia - ďalšia územie obsadené, ďalšia nemocnica dobytá
                     
                                                         
                       Epitaf na hrobe starej pravice
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




