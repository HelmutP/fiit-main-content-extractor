
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jana Del Moral
                                        &gt;
                Nezaradené
                     
                 "Tí Španieli sa naozaj zabavia na hocičom!" 

        
            
                                    6.5.2010
            o
            22:09
                        (upravené
                7.5.2010
                o
                21:39)
                        |
            Karma článku:
                3.87
            |
            Prečítané 
            857-krát
                    
         
     
         
             

                 
                    "Tí Španieli sa naozaj zabavia na hocičom!" Povedal môj otec, keď prvý krát počul rozprávať o Fallas, a potom objavil nádhernú tradíciu z Valencie, kde sa stavajú do ulíc figuríny niektoré až 32 metrov vysoké. Figuríny sa pripravujú počas celého roka a postavia sa do ulíc 15- teho Marca kde sú volne vystavene až do 19-teho Marca, (toto obdobie sa vola fallas ) kedy sa v noci, jedna za druhou a v prítomnosti hasičov pália.
                 

                 
  
   Počas fallas sa nespi, iba zabáva a pije. V každej ulici sú diskotéky, koncerty, stánky s pitivom, kolotoče a hlavne petardy, tie sa strieľajú nonstop.   Takže aj keby ste spať chceli,( čo sa nechce), hudba a buchot, ktorý otriasa celým okresom, vás vyženú do ulíc určite.   Deti majú v škole prázdniny, dospelí si berú dovolenky - nikto si fallas nenechá ujsť.  Dokonca aj tí najstarší, sú v uliciach a s hrdosťou obzerajú a fotografujú figuríny z každého uhla.   Je to predsa ich tradícia!   V skutočnosti sa tento sviatok koná na počesť svätého Jozefa , patróna tesárov, známy, ako deň otcov. Jeho počiatky sa vzťahujú do VIII storočia, kde sa Valencijsky tesári, koncom každej zimy pripravovali na deň svojho patróna, tak, že pálili odpad a staré haraburdy zo svojich dielní. Pálili hlavne drevené štruktúry na ktorých viseli lampáse ktoré im osvetľovali dielne počas zimy , keď že ich po príchode jari a predlžovaním sa dní už nepotrebovali.   Niečo ako jarné upratovanie, kde pálenie symbolizuje odstránenie problémov a zla.   Lenže Valencijčania sú veľmi vynaliezaví a tesárske haraburdy čoskoro začali nadobúdať ľudskú podobu. Stačilo pár starých kúskov oblečenia, a úmyselne vyberaných doplnkov, aby figúra nadobudla satirický a groteskný obsah a výsmešne takto upozornila na konfliktných susedov, alebo polemických starostov.   V súčasnosti,( ako už určite predpokladáte), sú figuríny zamerané hlavne na politickú kritiku a aktuálne dianie v satirickej scenérii.   Aj materiály sa počas histórie menili a vyvíjali, od tradičného dreva, cez praktickejší vosk, prechádzali, cez  sadro-karton čo povoľovalo vytvárať čoraz precíznejšie figuríny.   Dnes sa postavičky známe ako "ninots" vyrábajú z mäkkého korku,  pretože tento materiál umožňuje čoraz väčšie  a ľahšie formy.   Sú to priam umelecké diela, ktoré  prichádza obdivovať takmer milión turistov ročne .Iba v meste Valencia je vystevenych 385 figurín a ďalších 250 je rozmiestnených po celom okrese. A aj keď sa oficiálne  stavajú do ulíc  až 15-teho Marca, v poslednej dobe sa s ohľadom na rozmery sa začínajú stavať už niekoľko týždňov vopred (niektore uz 27 februára) a za pomoci žeriavu.   Neskutočné nákladná práca, ktorá už po štyroch dňoch zhorí v plameňoch.Uz 19-teho Marca je "ohnivá noc" kde nočné ohňostroje a pirotechnické kreácie  ohlasujú koniec slávnosti. Tento najúžasnejší ohňostroj rozsvieti oblohu Valencie a trvá okolo 25 minút pričom sa spáli asi 2500=3500 kg  strelného prachu  najrôznejších farieb zvuku  a efektov.   Strelný prach a ohňostroje sú pre Valenciáncov veľmi obľúbené a nenechajú si ich ujsť pri žiadnej slávnosti, sú ale menej pochopiteľné pre turistov a návštevníkov.   Aby sme pochopili je veľmi dôležité byť v blízkosti miesta kde explodujú, pretože  to nie je iba otázka pohľadu, ale je nutné to cítiť a počuť. Ak sa necháme uniesť hlukom a buchotom podobný koncertu dosiahneme pocit, ktorý nás opantá a unesie, keď že za 5 až 7 minút dosiahne viac ako 120 decibelov.   Fallas rozhodne najlepšie definuje kultúru  a históriu Valencie aj jej obyvateľov  oheň, hudba a život v uliciach...   !Visca Valencia! 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Del Moral 
                                        
                                            Vycikaj sa do sprchy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Del Moral 
                                        
                                            Deformované nohy a zlomené srdcia
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Del Moral 
                                        
                                            Horor na tanieri
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Del Moral 
                                        
                                            Španieli  šampióni
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jana Del Moral 
                                        
                                            Cervení to vyhrajú!!!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jana Del Moral
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jana Del Moral
            
         
        delmoral.blog.sme.sk (rss)
         
                                     
     
        Potrebujem lásku, vieru , mier a spravodlivost a bojím sa nenávisti, ignorancie, vojny a nemilosrdnosti.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1097
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            El simio y el aprendiz de sushi, De Waal, F.B.M. (2002)
                                     
                                                                             
                                            La utopía Skinneriana, J.L.Prieto
                                     
                                                                             
                                            Anieli a démoni, Dan Brown
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Antonio Orozco, Renovatio
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            biras.blog.sme.sk
                                     
                                                                             
                                            http://blogs.telecinco.es/loquemesaledelbolo/
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            http://www.pesvnudzi.sk/psiky-na-adopciu
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




