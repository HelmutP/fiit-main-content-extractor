
 
  Slováci, ktorých som stretol v zahraničí, nemajú život ľahký. Aj v prípade, že sú úspešní, sú odtrhnutí od svojich príbuzných, priateľov a známych zo Slovenska. Chcel by som vám priblížiť, niektoré ich príbehy: 
 Stretol som človeka, ktorý nebol na Slovensku, keď ho jeho brat po tažkej autohavárii potreboval. 
 Stretol som človeka, ktorý nebol na Slovensku, keď sa vydavala jeho sestra, lebo si nemohol dovoliť pricestovať. 
 Stretol som človeka, ktorý nebol na Slovensku, keď jeho otec čakal 4 hodiny na sanitku a potreboval pomoc. 
 Stretol som človeka, ktorý nebol na Slovensku, keď ho potrebovali priatelia. 
 Stretol som človeka, ktorému zomrela mama, kým on bol v Amerike. 
 A vždy pri podobnom stretnutí som si dal záväzok, že keď budem môcť spraviť niečo pre to, aby Slováci nemuseli emigrovať, tak to spravím. Dnes je ten deň, keď môžem niečo spraviť. 
 S pokorou vás prosím, poďme dnes voliť. 
 
