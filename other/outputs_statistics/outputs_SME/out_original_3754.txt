
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavol Hofbeck
                                        &gt;
                Nezaradené
                     
                 Najväčšie Chyby Mužov Ktoré Odrádzajú Ženy 

        
            
                                    1.4.2010
            o
            1:23
                        (upravené
                1.4.2010
                o
                17:23)
                        |
            Karma článku:
                5.15
            |
            Prečítané 
            1514-krát
                    
         
     
         
             

                 
                    Po búrlivých reakciách a diskusiách na môj predchádzajúci článok ktoré ma náhodou celkom rozosmiali i potešili som sa rozhodol splniť svoj sľub a napísať o tom, čo už dlho pozorujem okolo seba a niekedy možno aj na sebe:) Nik nemôže spochybniť tvrdenie, že získať krásnu ženu je snom takmer každého muža. Ako človek ktorý sa už presekal krovím amaterizmu môžem povedať, že na zvedenie ženy s kvalitnou genetickou výbavou musí mať človek zmes schopností ktoré sa tréningom môžu zdokonaľovať. Nejedná sa o žiadnu príručku ako na ženy, ani nechcem byť žiaden majster ktorý vám za poplatok dá čarovnú paličku. Chcem iba dať radu ľuďom ktorí robia chyby, radu ktorú by som sám pred rokmi uvítal. Spozoroval som, že niekedy aj naozaj inteligentní jednici robia pri ženách stále tie isté chyby dokola.
                 

                 
  
   Zrejme im primitívne pudy z minulosti a malý mozog ktorý riadi dôležité životné funkcie dokonale zablokuje prenos vzruchov v šedej hmote. A ako správny masochista ako negatívny príklad uvediem samého seba, prípad spred dvoch rokov keď som sa bezhlavo zaľúbil a robil všetko pre to, aby som si udržal pozornosť dotyčnej osoby.  Keď si na to spomeniem teraz, príde mi to humorné, no pamätám sa, že vtedy som to naozaj bral vážne. Šlo o takú peknú a inteligentnú čiernovlásku. Dalo sa s ňou baviť naozaj o všetkom, o umení ktorému sa venujem, literatúre a dokonca aj kvantovej fyzike a filozofii, jednoducho témy ktoré sa nedajú rozoberať s každým/každou. Problém bol ten, že dotyčná mala priateľa a tak som sa ju snažil brať ako dobrú kamošku. Čo čert nechcel, jedného dňa sme boli nútení z určitých dôvodov ostať dvaja v práci. Počúvali sme hudbu na youtube, popíjali a bolo nám príjemne. Stalo sa čo sa stalo. Samozrejme ostala s nim aj keď mi pred tým tvrdila iné. Vo mne ten dojem však ostal veľmi silný a ostal som jej naklonený dlhú dobu. Počas tohto obdobia som na sebe spozoroval zmeny správania. Sformuloval by som to do týchto chýb ktoré vídam bežne.   Byť na ňu príliž dobrý - chyba ktorej sa dopúšťame v slepej vidine toho, že naše milé správanie bude opätované. Kiež by to tak fungovalo, príroda sa však riadi svojimi zákonmi ktoré nezmeníme. Žena má v hlave mechanizmust ktorý ju varuje pred evolučne nevyhovujúcimi jedincami. Správať sa milo v dobe keď žena k mužovi nič necíti je ako dať sa kastrovať a nemať v sebe žiaden testosterón. Z miliónov rozhovorov je jansé, že drvivá väčšina žien má rado, keď ich občas muž trošku potiahne za vlasy a je na ne mierne hrubý. Dobrý usmievajúci chlapec sa peknej žene zíde akurát tak ako vešiak na šaty, bútlavá vŕba a možno aj ako vianočný stromček, keď ľuduje vyrúbať jedličku. Tieto prípady vidím naozaj denne. Viem, že niektorí muži majú túto dobrotu v povahe a väčšinou sa ich partnerky stávajú sekery ktoré neznesú pri sebe osobnosť. Muži pod papučov, tak to nie je veľká výhra. Testosterón na bode mrazu a priebojnosť pri obhajobe svojho názoru nulová. Tieto prípady ma naučili, že nech mám akokoľvek silné sympatie voči nejakej inteligentnej a peknej žene, kým nie je zaľúbená, je prílišné precukrenie akurát jednosmerný lístok na zaradenie sa medzi vešiaky na kabáty.   Dať jej všetku svoju pozornosť a čas - pamätám si, že kým som k spomínanej slečne nič necítil, venoval som sa jej len občas a keď zmizla z dohľadu, zmizla z mysle. Po tej noci som sa však začal správať ako tragéd, zrazu som bol ochotný ju počúvať neustále. Je to ako droga ktorej sa človek nedokázal nabažiť. Nech má človek akokoľvek veľa voľného času (ak je ho príliž veľa, niečo fakt nebude v poriadkuJ, mal by vytvoriť ilúziu zanepráznenosti, aby nebol stále k dispozícii. Ak sa zadarí a žena začne pociťovať sympatie, prečo nenechať jej fantáziu pracovať a využiť vlastnú neprítomnosť vo svoj prospech?       Ďalšie chyby ktoré som okolo seba spozoroval:   Neschopnosť vycítiť, že žena nemá záujem zo signálov ktoré vysiela.   Nie každá žena je tak úprimná, že dokáže mužovi do ksichtu vykričať, nech ostane sedieť kde sedí a nech vevystrkuje tie svoje paprče. Väčšina žien sa spolieha na to, že to muž pochopí z menej nápadných signálov. Niektorí muži sú ale fakt hrozní v tomto a ja sa iba smejem keď také čosi vidím.   Prílišná precitlivelosť na seba samého:   Človek a jeho drísty majú nad nami takú moc ako sami rozhodneme. Niektorí muži sú ale hyperurážliví a v ženskom oku je ľahko urážlivý muž - ľahko zraniteľný muž. A uznajme, ktorá zdravá samica by už len chcela za svojho ochrancu a otca svojich detí nejakú padafku čo sa nechá rozhádzať nejakými blúdivými myšlienkovými pochodmi ktoré nabrali odvahu zmeniť sa na vlnenie vo vzduchu narážajúce na ušné bubienkyJ           Nabudúce niečo o Casanovovi, ako som sa dočítal v diskusiách, veľa zaujímavých vecí o tomto labužníkovi a špekulantovi ešte stále neviete.       Prajem len to najlepšie a nachytajte čo najviac ľudí na prvého Apríla   Vaša dobrá Víla...:)     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (189)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            Slepé Srdce
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            PREHLAD
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            Pripravme sa na Duel
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            Prebudený a osvietený mesiac pred voľbami
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Pavol Hofbeck 
                                        
                                            REAKCIA
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavol Hofbeck
                        
                     
            
                     
                         Ďalšie články z rubriky lifestyle 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Xenie Suhayla Komers 
                                        
                                            Vydat, či nevydat se ve stopách svého osudu?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Ivana Lešičková 
                                        
                                            Nenávidím Vianoce! Zn: tie v októbri
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Jana Ambrošová 
                                        
                                            Filipínsky občasník: 4. Peklo v raji
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Daniela Pikulova 
                                        
                                            Za oponou.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Teodor Komenda 
                                        
                                            Škola života
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky lifestyle
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavol Hofbeck
            
         
        hofbeck.blog.sme.sk (rss)
         
                                     
     
        Obdivovateľ života, kreatívna bytosť, človek, ktorý po sebe chce niečo zanechať.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    734
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




