
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Ivan Klinka
                                        &gt;
                Zo života
                     
                 Že to je taký systém.. 

        
            
                                    8.6.2010
            o
            13:56
                        (upravené
                2.8.2010
                o
                0:26)
                        |
            Karma článku:
                18.75
            |
            Prečítané 
            3628-krát
                    
         
     
         
             

                 
                    Časenkový systém u lekára je geniálna vec. Ráno prídete, potiahnete si lístoček s poradovým číslom a idete na kávu, alebo domov upratať, a približne viete, kedy sa máte do čakárne vrátiť. Samozrejme, len za predpokladu, že systém funguje.
                 

                 Prezieravo som sa už včera telefonicky informoval, kedy sa vykladajú časenky. Reku, o pol siedmej ráno a ordinovať sa začína o ôsmej. Tak som si privstal, 6:30 som naklusal do čakárne - a potiahol si číslo 6. Stretol som tam zástup dôchodkýň, ktoré už zrejme od štvrtej ráno stáli prilepené na dverách a čakali. Ale veď nevadí, zbehnem domov, vyvenčím psa a vrátim sa.   Môj skromný odhad bol, že sa na rad dostanem okolo pol desiatej. O 9:15 som sa teda blížil ošarpanými chodbami nemocnice do čakárne. Čakáreň bola plná starých ľudí, ktorí prežúvali v ústach zubné protézy a polemizovali o cenách potravín v rôznych obchodných reťazcoch. Spýtal som sa teda, aké číslo je vnútri. Že 1. Zarazil som sa - za hodinu a pol sa dnu dostal len 1 človek?   Skočil som si teda kúpiť bulvárny denník, nech mi čas rýchlejšie ubieha. Po mojom návrate bola situácia rovnaká. Začínal som byť nervózny a rozmýšľal som, čo do boha tak dlho trvá. Vrátil som sa teda znova domov, prezliekol si prepotené tričko, niečo porobil a cca za hodinku a pol som sa tam vrátil. Dôchodcov v čakárni nebezpečne pribudlo.   Zase nasledovala otázka na čakajúcich, že aké číslo je dnu. Čakal som niečo okolo 3-4. Ale po odpovedi som už druhý krát v ten deň nechápavo pozeral. Dnu bolo číslo 13! Slušne som teda zaklopal a pokúsil sa sestričke vysvetliť moju situáciu. Že mám číslo 6, že rýchlosť vybavovania pacientov sa vôbec nedá odhadnúť, že čakajúcemu číslu 14 je z absolútneho hľadiska jedno či pôjdem teraz, alebo či som išiel predtým a jeho pozíciu to nezhorší. Napriek tomu, že som s ňou zviedol 5 minútový rap battle, nedokázala pochopiť moje racionálne argumenty. Vraj, na to sú tam tie časenky, podľa nich sa určuje poradie. Že to je taký systém.   Aký ma teda preboha zmysel tento systém? Potiahnuť si ráno lístok s číslom a čakať tam tak či tak? Veď to si v princípe odporuje s tým, ako by mal tento systém fungovať. Slovenské zdravotníctvo je na katastrofálnej úrovni. A systém, ktorý by mal pracovať pre ľudí, pracuje nakoniec proti nim.   PS: Zajtra tam idem znova 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (67)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Jeseň v modranskom chotári
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Mali to premyslené!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Sme síce v EÚ, ale do Ruska to tiež nemáme ďaleko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Rožok v Bille stojí 1,1€
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Ivan Klinka 
                                        
                                            Zavolal som "fízlov"
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Ivan Klinka
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Ivan Klinka
            
         
        klinka.blog.sme.sk (rss)
         
                                     
     
        Nemám rád kritiku. Ani pozitívnu. Budem to tu mať asi tažké.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    22
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2929
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Zo života
                        
                     
                                     
                        
                            Úvahy
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




