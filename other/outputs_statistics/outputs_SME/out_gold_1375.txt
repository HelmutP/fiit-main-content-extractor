

 Píše sa ten pamätný rok, je prvý máj a miestny rozhlas dáva budíček všetkým súdruhom a súdružkám. Trochu som si vždy v tom hurhaji ešte pospal a potom som zbaštil pripravené raňajky, na plece starý fotoaparát a poďme za prvomájovými povinnosťami. 
  
 Zväčša som namiesto sprievodu chodil len na určené miesto zrazu nášho pracovného kolektívu, tam som pozdravil súdruha vedúceho, s kolegami sme prebrali novosti a zhodnotili sme počasie. Niekedy bolo dosť ťažké sa dorozumieť na hlukovom pozadí rečnenia z tribúny, zaplavujúceho celý stred mesta. Reči o úspechoch národného hospodárstva v našom meste a o boji za svetový mier sa aj tak podobali z roka na rok ako vajce vajcu a nikoho nezaujímali. 
 Raz, alebo dvakrát som prešiel so sprievodom až na amfiteáter, na kultúrny program s klobásou, či cigánskou pečienkou a s pivom v nechutnom impregnovanom papierovom pohári a aby sme z toho mali aj nejaký kultúrny dojem, niekedy sme si dali k tomu aj borovičku v skle. 
 Keď som nechcel ísť popod tribúnu, tak som sa kolegom odporúčal už na mieste stretnutia, že idem fotiť, nech ma nečakajú. V tom prípade som urobil zopár záberov z nášho stretnutia, potom z rozkvitnutých čerešní na sídlisku a o chvíľu som už bol doma v pravej prvomájovej pohode, len za starými oknami sa rozliehalo prvomájové nadšenie. 
 V tento prvomájový deň ma nezobudila hudba z miestneho rozhlasu. Ráno prebehlo v pokoji, bolo pekné počasie, tak sme sa odpoludnia vybrali na slávnostné prvomájové stretnutie občanov mesta s minulosťou roku 1989, teda posledný socialistický prvý máj pred slávnym novembrom. 
  
 Pionieri a iskričky zložili svoj sľub vernosti ČSSR, ako ho splnia, keď tá republika už neexistuje, ostane na ich divadelnom svedomí, nechajú ho na doskách, ktoré kedysi aj dnes znamenajú svet... 
  
 Na Trojičnom námestí už bolo pol mesta a na tribúne sa skveli vyobliekaní vrcholní predstavitelia minulosti... 
  
 Atmosféru doby niesli na svojich mávatkách aj pionierky a zväzáčky... 
   
 Hostí na tribúne hádam ani netreba predstavovať, všetci si ešte pamätáme na súdruha Gorbačova a soudruha Husáka.  
  
 Najslávnejšia kozmonautka sveta, Valentína Tereškovová vystúpila s plamenným prejavom (po rusky) k mládeži o vedecko-technickej revolúcii... 
  
 Valentína zožala uznanlivý potlesk na tribúne i dolu v masách... 
  
 Ktovie prečo sú mi nesympatickí títo páni s bradami (štvrtého už pred časom vylúčili z dejín, lebo mal iba fúzy :)? 
  
 Ilúzia bola dokonalá aj v hľadisku, mládež to vzala s pochopením a patričnou dávkou recesie. 
  
 Na oblohu vyletel(a) z rúk pioniera aj holub(ica) mieru a zaradila sa do kŕdľa ostatných mestských holubov na kostolnej veži. 
  
 Nastal čas, keď sa zapoja do osláv aj manifestujúce sprievody prostých ľudí, tu miestne JRD s predsedom držiacim Lenina pekne pod krkom... 
  
 Na čelo sprievodu sa zaradili naše kone... 
  
 Hneď za nimi krásny zelený traktor Zetor so Zväzom žien na vlečke. 
  
 Veru, mládež na večné časy... a nikdy inak. 
  
 Zlatý klinec z DDR, zánovný Trabantík, trochu zadymil, ale sprievod zdolal bez ťažkostí. 
   
 Ako inak, na konci sú vždy naši hokejisti... :)  
   
 Opona dolu a moje dojmy 
 Keď sme prišli na námestie, ktorým razantne burácal typický slávnostný prejav minulých dôb totality, necítil som sa príjemne. Tak blízko je to, napriek rokom, že tá atmosféra na mňa zapôsobila hrozne. 
 Rád by som vedel, ako takéto oživenie minulosti vnímajú mladí ľudia, čo to v nich vyvoláva? Lebo len to je dôležité. My, starší vieme o čom to je a preto by sme sa k tomu nikdy nechceli naozaj vrátiť v žiadnej podobe. 
   
   
 
   
 Aktuálne fotografie sú z prvomájového predstavenia Divadla Šok v Šali s názvom: Pamätáte sa na... 1. máj 1989, so sprievodom a z príhovormi predstaviteľov politbyra z tribúny.  
 Účinkovali: Majo Labuda, Roman Hatala, Viktor Vincze, Erik Peťovský, Alena Demková...   
   
   
   

