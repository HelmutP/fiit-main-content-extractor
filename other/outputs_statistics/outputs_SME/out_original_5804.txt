
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Pavel Baričák
                                        &gt;
                NEVERŠOVAČKY (Básne)
                     
                 Preľakol som sa 

        
            
                                    4.5.2010
            o
            10:06
                        (upravené
                7.5.2010
                o
                11:40)
                        |
            Karma článku:
                0.00
            |
            Prečítané 
            1292-krát
                    
         
     
         
             

                 
                    
                 

                 
  
   keď mi kvet lipy   spadol do vína.   Rozbúchalo sa mi srdce,   že ma svet   vytiahol k tabuli.       A pritom to bol   len obyčajný bozk,   ktorý mi mal pripomenúť,   že žijem.               Buďte šťastní, Hirax       Aktivity Hiraxa:   Pondelok, 18. 5. 2010, Svet, Oficiálne vydanie  Hiraxovej druhej básnickej zbierky Nech je nebo všade, ktorá bude  pokračovaním priaznivo prijatého debutu More srdca. Knižočka bude  obsahovať ďalších, približne 90 básni, aforizmov, bájok, zamyslení a tak  ako pri prvých básniach bude ilustrovaná skvelými obrázkami od Janky  Thomkovej. Druhý titul bude panamsko-kostarický cestopis Úcta k prírode a  úsmev ako zmysel života. Väčšia časť knihy sa bude venovať pobytu v  pralese Darién u Indiánov z kmeňa Emberá. Cestopis bude obsahovať  približne 130 plnofarebných fotiek a okrem Hiraxovho autentického popisu  písaného priamo zo srdca, bude titul niesť aj teoretické časti o živote  a zvykov spomínaných Indiánov. Obidve knihy vyjdú u knižného  vydavateľstva HladoHlas.   Streda, 19. 5. 2010 o 17:32, Žilina, kaviareň Kaťuša  (Mariánske námestie - medzi Tatra bankou a reštauráciou Voyage).  Rozprávanie a premietanie Hiraxa o jeho potulkách Panamou a Kostarikou s  hlavným dôrazom na pobyt v panamskej džungli Darién u Indiánov Emberá. V  tomto čase by mala byť vonka už aj knižná podoba tohto panamského  cestopisu (a troška aj Kostarického :-)) s názvom "Úcta k prírode a  úsmev ako zmysel života", ktorý vyjde začiatkom mája 2010. Vstup jeden  úsmev.   Štvrtok, 20. 5. 2010 o 11:11, Sučany, gymnázium -  súkromná recitácia. Čítačka Pavla "Hiraxa" Baričáka.   Štvrtok, 20. 5. 2010 o 18:32, Martin, kaviareň  Kamala. Rozprávanie a premietanie Hiraxa o jeho potulkách Panamou a  Kostarikou s hlavným dôrazom na pobyt v panamskej džungli Darién u  Indiánov Emberá. V tomto čase by mala byť vonka už aj knižná podoba  tohto panamského cestopisu (a troška aj Kostarického :-)) s názvom "Úcta  k prírode a úsmev ako zmysel života", ktorý vyjde začiatkom mája 2010.  Vstup jeden úsmev.   Máj-jún 2010: Výstava Hiraxových fotografií z Thajska v martinskej  kaviarničke Kamala. Pozor, nejedná sa o žiadnu "galériu". Bude sa jednať  o 12-14 záberov, ktorými som sa snažil vystihnúť túto krajinu. Vstup  jeden úsmev.   Utorok, 1. 6. 2010, Čechy, Oficiálne vydanie českej  verzie románu Vteřina před zbláznením (Všecho je, jak je).   Pondelok, 28. 6. 2010, Topoľčany, 17.00 hod Galéria  (program pre mladých), 18.00 reštaurácia Kaiserhof Nám.M. R. Štefánika  (čítačka Hirax).         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Ako urýchliť zámer? Obetou a pokorou
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (2.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Od zámeru k myšlienke: „Na čo myslíte, to násobíte“ (1.časť)
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Všetci sme mágovia
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Pavel Baričák 
                                        
                                            Vnútorným dialógom k osobnému šťastiu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Pavel Baričák
                        
                     
            
                     
                         Ďalšie články z rubriky poézia 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Maťo Chudík 
                                        
                                            Niečo sa stalo
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Fuček 
                                        
                                            Posúvam sa dozadu
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Filip Glejdura 
                                        
                                            . . .  a bolo mu to aj tak jedno
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Drahoslav Mika 
                                        
                                            Chmúrava
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Janka Bernáthová 
                                        
                                            Iba dozrievam
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky poézia
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Pavel Baričák
            
         
        baricak.blog.sme.sk (rss)
         
                        VIP
                             
     
         Ľudia majú neuveriteľnú schopnosť pripísať všetko, čo bolo napísané autorovi, že to na vlastnej koži aj sám prežil. Prehliadajú schopnosť vnímania, pozorovania sveta a pretransformovania ho do viet s úmyslom pomôcť druhým. Ale na druhej strane sa mi dobre zaspáva s pomyslením, že ostatok sveta na mňa myslí. Cudzí ľudia o mne vedia všetko, teda aj to, čo neviem ani ja sám. Ďakujem im teda za ich všemocnú starostlivosť! Buďte všetci šťastní, prajem Vám to z celého môjho srdca. Hirax &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/anketa_0.php?id=754643" href="http://blueboard.cz/anketa_0.php?id=754643"&amp;amp;gt;Anketa&amp;amp;lt;/a&amp;amp;gt; od &amp;amp;lt;a data-cke-saved-href="http://blueboard.cz/" href="http://blueboard.cz/"&amp;amp;gt;BlueBoard.cz&amp;amp;lt;/a&amp;amp;gt; 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    558
                
                
                    Celková karma
                    
                                                7.11
                    
                
                
                    Priemerná čítanosť
                    3664
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Šlabikár šťastia 1.
                        
                     
                                     
                        
                            Šlabikár šťastia 2.
                        
                     
                                     
                        
                            Vzťahy (Úvahy)
                        
                     
                                     
                        
                            Tak plače a smeje sa život
                        
                     
                                     
                        
                            Zachráňte malého Paľka!
                        
                     
                                     
                        
                            Po stopách komedianta
                        
                     
                                     
                        
                            Sárka Ráchel Baričáková
                        
                     
                                     
                        
                            Sex (Úvahy)
                        
                     
                                     
                        
                            NEVERŠOVAČKY (Básne)
                        
                     
                                     
                        
                            PRÍBEH MUŽA (Básne a piesne)
                        
                     
                                     
                        
                            SEKUNDU PRED ZBLÁZNENÍM (Román
                        
                     
                                     
                        
                            KÝM NÁS LÁSKA NEROZDELÍ (Román
                        
                     
                                     
                        
                            RAZ AJ V PEKLE VYJDE SLNKO/Rom
                        
                     
                                     
                        
                            ČESKÁ REPUBLIKA (CESTOPIS)
                        
                     
                                     
                        
                            EGYPT (Cestopis)
                        
                     
                                     
                        
                            Ekvádor (Cestopis)
                        
                     
                                     
                        
                            ETIÓPIA (Cestopis)
                        
                     
                                     
                        
                            FRANCÚZSKO (Cestopis)
                        
                     
                                     
                        
                            INDIA (Cestopis)
                        
                     
                                     
                        
                            JORDÁNSKO (CESTOPIS)
                        
                     
                                     
                        
                            KOSTARIKA (Cestopis)
                        
                     
                                     
                        
                            Mexiko (Cestopis)
                        
                     
                                     
                        
                            Nový Zéland (Cestopis)
                        
                     
                                     
                        
                            PANAMA (Cestopis)
                        
                     
                                     
                        
                            POĽSKO (Cestopis)
                        
                     
                                     
                        
                            SLOVENSKO (Cestopis)
                        
                     
                                     
                        
                            THAJSKO (Cestopis)
                        
                     
                                     
                        
                            USA (Cestopis)
                        
                     
                                     
                        
                            VIETNAM (CESTOPIS)
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Niečo o zrkadlení a učiteľoch
                                     
                                                                             
                                            Vlastné šťastie ide cez prítomnosť
                                     
                                                                             
                                            Áno, dá sa naraz milovať dvoch ľudí
                                     
                                                                             
                                            Chcete zmeniť vzťah? Zmeňte najprv seba.
                                     
                                                                             
                                            Chcete byť šťastnými? Prijmite sa.
                                     
                                                                             
                                            Ohovárajú vás? Zraňujú vás klebety? Staňte sa silnými!
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Liedloffová Jean - Koncept kontinua
                                     
                                                                             
                                            Alessandro Baricco - Barbari
                                     
                                                                             
                                            Christopher McDougall - Zrození k běhu (Born to run)
                                     
                                                                             
                                            Ingrid Bauerová - Bez plenky
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            The Killers
                                     
                                                                             
                                            Brandom Flowers
                                     
                                                                             
                                            Sunrise Avenue
                                     
                                                                             
                                            Nothing
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Ľubomíra Romanová
                                     
                                                                             
                                            Martin Basila
                                     
                                                                             
                                            Spomíname:
                                     
                                                                             
                                            Moskaľ  Peter
                                     
                                                                             
                                            Miška Oli Procklová Olivieri
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            HIRAX Shop
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Skutočná Anna Kareninová bola Puškinovou dcérou
                     
                                                         
                       Máte pocit, že komunisti zničili Bratislavu? Choďte do Bukurešti
                     
                                                         
                       Surinam - zabudnutá juhoamerická exotika
                     
                                                         
                       Hodvábna cesta - 56 dní z Pekingu do Teheránu 2.časť ( Uzbekistan, Turkménsko, Irán)
                     
                                                         
                       Detskí doktori, buďte detskí
                     
                                                         
                       Nemohli nič viac
                     
                                                         
                       Metalový Blog - Folk metal crusade
                     
                                                         
                       Veľká noc vzišla z pohanských sviatkov
                     
                                                         
                       Stačilo p. premiér, oblečte sa prosím
                     
                                                         
                       Jiřina Prekopová: Podmienečná láska je bezcenná
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




