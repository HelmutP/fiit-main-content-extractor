

 Ja toho Zlodeja hračiek sem raz naženiem! - vyhrážam sa vlastne len samej sebe. Ani už vlastne neviem, ako vyzeral náš byt predtým, než ho zaplnili hračky a iné detské rekvizity. Máme ich veľa - konštatuje často môj muž, najmä pri večernom upratovaní. Máme, rozhodne viac, než som mávala ja. Keby to bolo len na mne, s veľkou radosťou by som sa mnohých zbavila. A výber by to vôbec nebol ťažký... No naschvál, či sa trafím aj do preferencií iných rodičov. 
   
 Kategórie hračiek s obrovskou výhradou: 
 
 - Hračky vyludzujúce zvuky 
 
 Akékoľvek zvuky. Možno nie som dosť zbehlá, ale žiaden hračkársky zvuk ma dlhodobo neočaril. Bábätká stimulujú, pútajú ich pozornosť a to beriem. Ale. Výrobcovia možno neberú do úvahy, že dieťa si hračku nepustí raz ani päťkrát. Pustí si ju stopäťkrát. Púšťa si ju dovtedy, kým vydržia baterky. Potom nastupuje iný zvuk - pílenie uší rodičom, aby dali baterky nové. 
   
 
 - Hračky rozpadávajúce sa 
 
 Podľa mojich priateľov a mňa by každú hračku mali testovať tak, že ju na hodinu dajú do rúk dvom rozblázneným trojročným deťom. Len tá, ktorá ostane pokope, by sa mala dostať do predaja. V škatuliach, kam dávame obľúbenosti mojich detí (ostáva mi záhadou podľa akého kľúča ich vyberajú), je kopa úlomkov, zvyškov, koliesok, súčiastok... - ktoré vydržali na pôvodnom predmete sotva pár dní. Jasné, hračky sú spotrebný tovar. Aj tak väčšinu dostávame, tak by mi to nemalo byť ľúto. Ale - na čo sú hračky pre deti, ktoré väčšinu svojho hračkárskeho života prežijú v niekoľkých kusoch? 
   
 
 - Hračky przniace skutočnosť 
 
 Modrý zajac? Zeleno bodkovaná krava? Oranžová ovečka? Hybrid dieťaťa - koňa a vtáka, ktoré spieva detskou rečou? Fialový kôň, ktorý nedokáže stáť, ale zato má hrivu po zem, ktorá sa má česať (a v skutočnosti sa po dvoch prečesaniach totálne zauzlí a zvyšok času len prekáža)? Načo??? Asi som staromódna. Ale keď boli malí úplne malí, podľa tých čudesných hračiek sme ich rozoznávať zvieratká  a ich zvuky radšej ani neučili. A teraz, keď sú väčší, sa čudným tvorom posmievajú sami. 
   
 
 - Trendy a brand hračky 
 
 Aj Krteček má svoje korporátne doplnky. Aj tí dvaja motáci z A je to. Macko Puf tiež. No dobre, tie akosi ešte akceptujem. Ale desím sa chvíle, keď dieťa príde domov s tým, že bez Hannah Montana nemôže existovať, či že Batman forever. Prišlo mi smiešne, keď sme boli nedávno na karnevale a behali tam piati metroví spajdrmeni. Ako by sme zabúdali na to, že deti majú predstavivosť a je škoda nenechať im trochu priestoru aj na to, aby si svet, ktorý vnímajú cez rozprávky, dotvárali a domýšľali sami. 
   
 Bolo by ich ešte viac. Našťastie ešte existujú... Hračky s plusmi: 
 
 - Holý Havko 
 
 Určite ho má aj vaše dieťa. Spoločník našej dcéry od jej pol roka, nikdy sa od neho nevzdialila na dlhšie ako na hodinu. Už je, chúďa plyšák, vypelichaný, stokrát vypratý, silne nereprezentatívny - ale náš. Nikto netuší, prečo mu dala také meno, keď je to obyčajný pes s mašľou pri krku a rozpáranými fúzami (raz ich teta v škôlke odstrihla, mali sme doma národnú tragédiu a museli sme našívať nové). Rozhodne je to najdôležitejšia hračka, ktorú máme. 
   
 
 - Knihy, omaľovánky, vystrihovačky 
 
 „No čítaj", je u nás najčastejšou vetou, keď nás dospelých deti prizvú k svojim hrám. Čítame úplne všetko, čo má obrázky a písmenká. Bez vnucovania si to krpatí tak žiadajú od vždy. Vďaka Bohu za to... 
   
 
 - Pastelky a fixky 
 
 Ako dieťa sme ich vždy mávali kade tade porozhadzované a mamka ich brala často ako neporiadok. Ani tie naše nie sú úplne výstavné. Preto, že ich veľmi aktívne používame. Nerobím si ambície vychovať z detí výtvarníkov. Ale je fakt, že s ich kresbičkami sa lúčim veľmi, veľmi ťažko... 
    
 
 - Kocky, skladačky, stavebnice 
 
 Nikdy ich nemáme dosť. Keď sa stavia čokoľvek, urobí sa pri tom neskutočný bordel. A nech. Je to skvelý neporiadok! 
   
 
 - Spoločenské hry 
 
 Malí sa naučili rátať vďaka kartám, teraz sa učia postreh na pexese a ohľaduplnosti na Človiečik nehnevaj sa. A aj my dospelí sme vedeli Silvester stráviť do pol siedmej ráno nad hracím plánom. 
   
 
 - Všetko, čo podporuje tvorivosť 
 
 No, ako tak premýšľam, to je vlastne u nás skoro úplne každý predmet. Dokonca sem patria aj tie čudá prečudesné, ktoré neznášam. Lebo aj z tých dokáže Matúško urobiť zlých rytierov a Julinka pacientov či publikom, ktorému rozpráva svoje príbehy. 
   
 Občas, keď od niekoho dostaneme hračku, sa na seba s manželom uškrnieme. Znamená to: „Počkaj, toto ti vrátime!" To u ľudí, ktorí väčšinou deti ešte nemajú. Dávajú náhodne to, čo na nich padá z výkladov hračkárstiev. Aj ja som taká bola, samozrejme. Ale dnes už viem - menej je viac. A u detí, prosím, myslite aj na nás rodičov... 

