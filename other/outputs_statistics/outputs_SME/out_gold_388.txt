

 
Animovaný GIF sa skladá z viacerých obrázkov, ktoré rotujú. Tieto obrázky sa volajú rámce a najprv si ich pripravíme v grafickom editore. Ja používam Paint.NET, profesionáli uprednostnia dosť drahý ale vynikajúci Photoshop, zástancovia Open Source použijú GIMP  ale nič nebráni použitiu všadeprítomného Microsoft Paint.
 
 
GIMP a Photoshop by mali podporovať vytváranie animácií priamo, ale bežný grafický laik ako napríklad ja uprednostní iné programy či už kvôli náročnosti ale aj cene. Takže budeme potrebovať ešte jeden program na spojenie do animácie a jeden zo známejsích je unFREEz. Tento program vyžaduje použitie GIF formátu aj pre vstupnú grafiku, takže si treba zvoliť grafický editor podporujúci GIF formát.  
 
 
Teraz môžeme pristúpiť k vytvoreniu jednotlivých GIF obrázkov.
 
 
Obrázok č. 1 (5.9kB) 
 
 
 
 
 

 
 
 
 
 
Obrázok č. 2 (6.4kB)
 
 
 
 
 

 
 
 
 
 
Obrázok č. 3 (6.5kB)
 
 
 
 
 

 
 
 
 
 
Samozrejme, že sa dá vložiť aj viac obrázkov, ale v danom prípade by som prekročil maximálnu veľkosť výsledného animovaného GIFu.
 
 
Teraz spustite unFREEz program a potiahnite myšou tieto súbory jeden po druhom do bieleho okienka v programe a zvoľte ako rýchlo sa obrázky majú striedať v centi-sekundách (stotinách sekúnd).
 
 
 
 
 
 
 
 
 
 
 
A už je treba len stlačiť Make Animated GIF a vybrať si kde ho má program uložiť. Výsledok má 18.7kB, čiže je vidno, že jednotlivé obrázky sú vnútri len uložené bez nejakej vzájomnej kompresie. Dokonca aj keď pridáte viackrát ten istý obrázok, tak sa vo výslednej animácii uloží viackrát, tak pozor na to pri vytváraní mnoho-rámcových GIF animácií. Pri takých už bude výhodnejšie použiť FLASH. 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

