
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Almir Strinič
                                        &gt;
                Nezaradené
                     
                 Máme na Slovensku druhý neoficiálny jazyk? Áno, češtinu. 

        
            
                                    27.6.2010
            o
            16:37
                        (upravené
                27.6.2010
                o
                16:59)
                        |
            Karma článku:
                7.14
            |
            Prečítané 
            1978-krát
                    
         
     
         
             

                 
                    Jediným úradným jazykom na Slovensku je síce slovenčina no pri pozeraní televízie či návšteve kníhkupectva mám iný pocit...
                 

                 
  
     V prvom rade chcem upozorniť ž nemám nič proti Čechom a Čechám, práveže naopak, naozaj tam veľmi rád cestujem a mám k tej krajine pekný vzťah pretože som tam v detstve každé leto cestoval za strýkom na Šumavu a vždy som sa tam nesmierne tešil. Nechcem aby niekto bral tento článok ako útok na Čechov a českú kultúru, cieľom článku je rozoberať neprimerane veľký vplyv češtiny v každodennosti priemerného Slováka.                                  Dôkaz národnej vzájomnosti alebo šetrenie nákladov a nezáujem?     Česi ak chceme či nechceme sú naši najbližší priatelia a národ s ktorým máme veľa spoločného. Slovenská kultúra a jazyk sú odjakživa veľmi naviazané na tie české nielen kvôli jazykovej a rasovej príbuznosti našich dvoch národov ale aj vďaka 20. storočiu, kedy sme boli prakticky skoro po celý čas v úzkom kontakte s Čechmi v jednom štáte. No kvôli tomu že Slovensko tvorilo slabší článok federácie, vplyv českej kultúry a jazyka bol na Slovákov veľký a pretrváva až dodnes v podobe pomerne dosť rozšírenej češtiny v televízii a vo filmoch, českého prekladu publikácii rôznych kníh či populárnosti českých umelcov na Slovensku. Toto sa trochu bije s faktom že veľa Slovákov vyčíta Čechom tých 40 rokov spoločného štátu a chce aby sa Slovensko konečne dostalo spod českého kvázi materinského vzťahu voči Slovákom ako mladík hľadajúci nezávislosť ktorý chce vykročiť do sveta svojpomocne a bez pomoci svojich rodičov.   Český vplyv na Slovákov je po revolúcii a rozdelení federácie možno ešte väčší než predtým, no naopak Česi strácajú akékoľvek vedomosti a poznatky o Slovensku, lebo slovenčinu tam v televízií počuť málokedy a natrafiť na slovensky nadabovaný film v kine či v telke je naozaj nemožné. Niektorý ľudia hovoria že je to zlá skutočnosť a že by sa to malo zmeniť, ale prečo? Dobre, máme naozaj podobné jazyky čo je logické keďže patríme medzi spoločnú západoslovanskú jazykovú rodinu ale zas to preháňať s kultúrnymi stykmi našich národov nie je správne, respektívne z tej slovenskej strany.Napríklad veľa cudzincom sa zdajú čeština a slovenčina pomerne dosť rozdielne jazyky a nechápu akoto že nemáme problém češtine porozumieť. Odmalička deti pozerajú kreslené filmy a seriály väčšinou v češtine a používajú vďaka tomu veľa českých slov čo v podstate dosť deformuje hovorený jazyk no v Čechách má malé dieťa niekde od Dečína veľmi malú, prakticky žiadnu šancu dostať sa do styku so slovenčinou.   Dá sa povedať že na Slovensku funguje istý druh bilingvalizmu a vďaka tým 40 rokom spoločného štátu došlo k miernemu a nevedomému počešteniu Slovákov takže je tu podobná situácia ako na Ukrajine, kde ľudia dokážu navzájom rozprávať v ukrajinčine a ruštine a nemajú problém s porozumením za to  v Rusku sa s ukrajinčinou samozrejme ťažko dohovoríte. Dodnes sa Slováci nedokázali vymaniť spod českého vplyvu a stále sa ich držíme ako maminej sukne a nevieme sa vydať samostatnou cestou.   Ked si pustím hociktorú zo zahraničných televízií na ponuke rozšíreného programu, väčšina ich je predabovaná do češtiny a maďarčiny resp. sa na tvorbe danej televízie podieľajú výlučne Česi a Maďari a tu je veľmi dobrý príklad kulinárskej TV Paprika ktorá je určená aj slovenskému divákovi no slovenčinu by sme tam márne hľadali a skoro všetko sú maďarské a české relácie. Podobne je to aj s televíziou Spektrum či MTV, kde je český dabing určený pre Česko aj Slovensko. Nie je výnimočné natrafiť ani na vysokoškolské skriptá v českom jazyku! Síce je pravda že Slováci rozumejú češtine, ale z princípu nie je dobré že sa pozeráme na programy v inom než rodnom jazyku. Akokoľvek sa Česi obhajujú svojou náklonnosťou voči Slovákom, myslím si že by im veľmi vadilo kebyže majú 70 %  programu v televízii v inom t.j slovenskom jazyku tobôž nie ako jazyk učebníc v školstve.           Situácii nepomáhajú veľmi ani spoločné „talenty" a „superstar" kde čeština taktiež dominuje a ide v podstate o českú súťaž, čiže ked pôjde výherca Talentu na svetové finále tak ako to vždy bolo ho budú považovať rovno za Čecha lebo súťaž sa začína názvom Cesko.. a to v nich bude evokovať už klišé Hradčany a Karlov most t.j Prahu a druhé Slovenska si všímať nebudú, čiže o reprezentáciu Slovenska veľmi nepôjde no možno to by nebol až taký problém.    Jednoducho si myslím že aj napriek nášmu blízkemu vzťahu k Čechom nie je veľmi dobré zahatať si väčšinu sfér života češtinou lebo mať sympatie k inému národu neznamená podporovať cudzí jazyk namiesto rodného.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (217)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Almir Strinič 
                                        
                                            Kto skutočne môže za nekvalitné potraviny?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Almir Strinič 
                                        
                                            O električkovej doprave v Bratislave
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Almir Strinič 
                                        
                                            Ukrivdené Rumunsko
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Almir Strinič 
                                        
                                            Môj prvý pracovný pohovor
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Almir Strinič 
                                        
                                            Obchodná ul. je bratislavským faux-pas
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Almir Strinič
                        
                     
            
                     
                         Ďalšie články z rubriky spoločnosť 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Mária Levočská 
                                        
                                            Ďalšia obeť pomsty monsignora Jaraba
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Nebite ma, doktor Plzák... alebo POZVANIE NA VEČERU
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Kristián Straka 
                                        
                                            Oklamaní Ježiškom ?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Buno 
                                        
                                            Šup, šup, najesť sa. A utekať!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Katarína Lorenčíková 
                                        
                                            Par uprimnych oslobodzujucich riadkov...
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky spoločnosť
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár v utorok stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Almir Strinič
            
         
        strinic.blog.sme.sk (rss)
         
                                     
     
        Som človek, idelista, ktorý sa často zamýšla nad vecami, nad ktorými sa vačšina ľudí ani nepozastaví.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    13
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1567
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




