
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Vladimír Inocent Szaniszló
                                        &gt;
                Verejný život
                     
                 Jednoduchý návod proti žabomyším víziam krátkozrakej politiky 

        
            
                                    11.5.2010
            o
            9:45
                        (upravené
                22.9.2010
                o
                14:21)
                        |
            Karma článku:
                6.75
            |
            Prečítané 
            982-krát
                    
         
     
         
             

                 
                    Po obrovskom debakli antipopulistickej politiky v NRW štáte, kedy grécke problémy spôsobili odklon voličov od reality v najľudnatejšom štáte Spolkového Nemecka sa uskutočnila u slávne neslávneho Beckmanna na prvom kanáli nemeckej televízie zaujímavá debata dvoch donedávna nezmieriteľnych nepriateľov spred jedného desaťročia. Už len ich spoločná kniha (!) nás núti nad zamyslením v prehriatom slovenskom priestore. Ich názory zase jasne ukazujú, že problémy Európy sú oveľa hlbšie: http://www.daserste.de/beckmann/sendung_dyn~uid,19gssumv6u7kuwea27koj9fd~cm.asp
                 

                 
Dvaja prednedávnom úhlavní nepriateliaARD Deutschland
   Dvaja donedávna nezmieriteľní nepriatelia: "superminister" G. Schroedera za SPD Wolfgang Clement a ekonomický expert CDU Friedrich Merz sa poznajú už skutočne dlhú dobu. Ich vzájomné útoky za červeno-zelenej vlády v Nemecku boli povestné. Clement vždy pôsobil dojmom človeka, ktorý Titanic nad vodou udržať nemôže, ale Merz svojim štýlom predsa len odpudzoval aj vlastných voličov. Je priamočiary a nemecky strohý s veľkou dávkou sakrazmu. Hovorí zásadne vecne a ide priamo k téme a jej pozadiu. Presne tak sa po dlhej dobe znova predstavili v nemeckej televízii.    1. poznámka: je unikátne, ak dvaja úhlavní politickí nepriatelia napíšu spoločnú knihu o budúcich víziach (nielen nemeckej politiky). Dokázať zahodiť nevraživoť a ideologické rozdiely a pokúsiť sa spoločne pomenovať východiská z marazmu dnešnej doby dokážu len veľkí ľudia veľkých národov, ktorým ide o veľké veci (ach tá malichernosť našej politiky na Slovensku)!   2. poznámka: Ich úvahy boli skutočne hlboké. Hlavne Merz sa pokúšal populistické a často bulvárom ovplyvnené otázky moderátora Beckmanna kontrovať skutočnými problémami verejného priestoru ako spoločná akcieschopnosť Európy a nutná zmena vzdelávacieho systému. Zásahy štátu do ekonomiky budú vždy nutné, ale otázka do akej miery, akého štátu ostane na programe dňa.    3. poznámka: Debata poukázala na, podľa mňa, skutočné problémy Európy. Myslíme lokálne a chceme konať globálne. Ak chceme uspieť (ako Európa) voči veľmociam ako je USA, Čína, India (dosaďťe si ďaľšie podľa jednotlivých kategórii) tak sa musíme naučiť jednať spoločne, byť spoločne schopní konkurencie a ovplyvňovania svetovej politiky. Nedá sa pracovať na politicky jednotnej mene, ak dostávame údery hlavne z Londýna a pod. bez možnosti kontroly a jednotného ovplyňovania. Nedá sa myslieť len na národné záujmy, ak sme občania Európy a budeme mať čo robiť obstáť vo svetovej konkurencii. Jednoducho globalizácia nás už predbehla, hranice skončili, ale my sme tam, odkiaľ máme vychádzať, ešte nedošli.   Nuž ako smiešne vyzerajú voľby a hádky v jednotlivých štátoch EU popri cieľoch a víziach byť veľkým hráčom na veľkom ihrisku! Smerovanie k jednému európskemu ministrovi financii a jednej európskej ratingovej agentúre, ktorá nebude určovať hodnoty z Wall Street, hľadanie spoločnej akcieschopnosti- to je bolestivý zrod Európy 60 rokov po Schumannovej deklarácii. Kiežby sme vedeli aj bez diktátorov smerovať spoločne k jednému tak dôležitému cieľu pre všetkých občanov Európy- mieru a kvalitnej životnej úrovni.  

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (2)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Zbytočná Európska Únia?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Základné právo na náboženskú slobodu a nezávislosť od štátu (IV.)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Príčina náboženskej slobody v modernej dobe III.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Príčina náboženskej slobody v modernej dobe II.
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Vladimír Inocent Szaniszló 
                                        
                                            Pápež a kondómy?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Vladimír Inocent Szaniszló
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Vladimír Inocent Szaniszló
            
         
        szaniszlo.blog.sme.sk (rss)
         
                                     
     
        Vysokoškolský učiteľ, večný hľadač pravdy, dialógu a vzájomnej výmeny ideí. Spolu s mladšími a staršími kolegami a študentmi rozvíjame obzory nášho poznania./
L´enseignant academique, le chercheur eternel de la vérité, du dialogue et du changement des idées. Avec ses collegues et étudiants nous élargisons l´horisont de notre conaisance.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    31
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    849
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Politická filozofia
                        
                     
                                     
                        
                            Verejný život
                        
                     
                                     
                        
                            Hľadanie koreňov
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené knihy 
                    
                     
                                                 
                                            Klaus Berger: Die Wahrheit ist Partei
                                     
                                                                             
                                            Notker Wolf-Matthias Drobinski: Regeln zum Leben
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Laura Passini
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Primitívne nedeľné politické debaty na RTVS a Markíze
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




