

 Keď sa pozerám z okna obývačky rodičovského domu vidím ranogotický kostol pochádzajúci z 13. storočia. 
   
  
   
 Na Kostol sv. Ducha sa môžem pozerať aj z dvora, na ktorom som sa celé detstvo hrával. 
   
  
   
 Keď vyjdem v našej záhrade trochu vyššie, vidím takmer celú dedinu. Prvá písomná zmienka o nej pochádza vraj z roku 1245. 
   
  
   
 Na náhornú plošinu Dreveník som vedel rýchlo vybehnúť. Bavilo ma objavovať a obdivovať toto travertínové územie. 
   
  
   
   
  
   
 Medzi skalnými jaskyňami a tektonickými zlomami som tu skoro vždy našiel nejaké nové zákutie, zaujímavú rastlinu alebo živočícha. 
   
   
  
   
  
   
  
   
  
   
  
   
  
   
  
   
  

