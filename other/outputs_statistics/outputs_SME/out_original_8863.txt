
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Michaela Urban
                                        &gt;
                Nezaradené
                     
                 Slovensko do toho! 

        
            
                                    15.6.2010
            o
            12:03
                        (upravené
                15.6.2010
                o
                12:09)
                        |
            Karma článku:
                2.77
            |
            Prečítané 
            1071-krát
                    
         
     
         
             

                 
                    Dnešný deň je pre nás, Slovákov, výnimočný. Spojme svoje sily a pošlime našim chlapcom energiu, ktorú budú potrebovať. Nech v Afrike cítia, že sme s nimi. Chlapci, ste naši hrdinovia...
                 

                 Písal sa 14. október 2009. Pre celý svet obyčajný októbrový deň, pre niekoho výnimočný. Dátum, ktorý sa do histórie slovenského futbalu zapísal zlatými písmenami. Zasnežený štadión v Chorzówe. Náš tím bez svojich opôr v defenzíve. Vlastný gól Severina Gancarczyka v tretej minúte. Katastrofálny terén. Zahodené šance Poliakov. Nevyužitá možnosť Kozáka. Bravúrne zákroky Muchu. Hvizd hlavného rozhodcu po uplynutí nadstavenia. Obrovská radosť, krik, potlesk, objatia, slzy v očiach hráčov, trénera, fanúšikov. Celý slovenský národ bol na nohách. Slová Marcela Merčiaka ešte dodnes spôsobujú zimomriavky na tele. Letenky na "čierni kontinent" sú konečne v rukách slovenských hráčov. To, čomu na začiatku kvalifikácie nikto neveril, sa stalo skutočnosťou. Sen o Afrike sa mení na realitu.   Dnes svieti v kalendári 15. jún 2010. Majstrovstvá sveta, ktoré sú pre celý svet 19. v poradí, sú pre nás všetkých prvými. Hráči už nemusia pozerať zápasy z pohodlia domova a rozmýšľať, aké by to bolo zahrať si na vrcholnom podujatí. Fanúšikovia nedržia palce len "cudzím". Konečne je Slovensko medzi 32 najlepšími. Štvrtý deň svetového šampionátu je ďalším medzníkom slovenského futbalu. Mužstvo pod vedením trénera Weissa odohrá svoj premiérový zápas. Súperom našich chlapcov bude Nový Zéland. Na Slovensku vypukla futbalová eufória. Zraky miliónov fanúšikov sa budú upínať na televízne obrazovky. S dresmi, vlajkami, šálmi, bubnami či pokrikmi budeme povzbudzovať náš národný tím. Všetci si prajeme, aby debut Slovákov na MS skončil ziskom troch bodov. Presne o 13:30 sa na chvíľu akoby zastaví čas. Nech nám moment, na ktorý Slovensko čakalo od svojej samostatnosti, utkvie v pamäti a slzy šťastia, ktoré nám padali na líca po postupe, sa opäť objavia spojené s hrdosťou pri počutí našej štátnej hymny. Spolu s celou veľkou futbalovou rodinou si zakričme: Slovensko do toho!     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (0)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Urban 
                                        
                                            Čo chcem v roku 2011?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Urban 
                                        
                                            Futbalové puzzle
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Urban 
                                        
                                            Keď sa remíza na prvý pohľad zdá byť len prehrou
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Urban 
                                        
                                            Život je jedna veľká horská dráha- úryvok č.1
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Michaela Urban 
                                        
                                            Pozná každý z nás svoje silné stránky?
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Michaela Urban
                        
                     
            
                     
                         Ďalšie články z rubriky nezaradené 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Pavel Ondrejmiška 
                                        
                                            Dnes farnosť Žitavany, pred tisíc rokmi kráľovská fara v Kňažiciach
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Milan Removčík 
                                        
                                            Zimný Šíp.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                          esc-sr 
                                        
                                            Čas (ne)vhodných darčekov
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Michal Filek 
                                        
                                            Ako je ťažké darovať bezdomovcom polievku?
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anna Kováčová 
                                        
                                            Na sexi postavu, zdravie, energiu a šťastný život nepotrebujete tabletky!
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky nezaradené
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            ZAHRANIČIE
             Protestujúci v Maďarsku strácajú dych, Fidesz ich mätie novými návrhmi 
             Na demonštrácii proti Orbánovej vláde bolo v utorok už len niekoľko tisíc ľudí. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 21:30
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ 21:50
             Taliban zabil v škole v Pakistane 132 detí 
             Pakistan až teraz poriadne zatlačil na islamistov. Odplatou je útok na deti vojakov. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Michaela Urban
            
         
        michaelaurban.blog.sme.sk (rss)
         
                                     
     
        Som 21-ročná baba, ktorá študuje na vysokej škole učiteľstvo slovenského jazyka a literatúry a etickej výchovy. Veľmi rada keciam, píšem, počúvam hudbu a trávim čas s priateľmi.Okrem toho pracujem ako redaktorka pre športový portál a jednou z mojich vášní je futbal:) ale len z pozície diváka:) to by bolo v skratke asi tak všetko
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    10
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1048
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Marcel Páleš
                                     
                                                                   
                     Obľúbené knihy 
                    
                     
                                                 
                                            Matkin
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Elán
                                     
                                                                             
                                            Desmod
                                     
                                                                             
                                            DJ EKG
                                     
                                                                             
                                            Senor Kuros
                                     
                                                                             
                                            Armin van Buuren
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Marcel Páleš
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




