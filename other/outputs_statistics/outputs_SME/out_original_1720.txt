
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Jozef Javurek
                                        &gt;
                Slovensko
                     
                 Estakáda v Považskej Bystrici 

        
            
                                    29.9.2009
            o
            20:05
                        |
            Karma článku:
                12.50
            |
            Prečítané 
            5229-krát
                    
         
     
         
             

                 
                    Nie je veľa takých miest, ktoré sa môžu pochváliť vzdušnou diaľnicou na stračích nôžkach nad domami.
                 

                 Považská Bystrica ju už čo nevidieť bude mať a už by si pomaly mali hovoriť, že čo nás nezabije, to nás posilní... Nie že by bola Považská Bystrica v tomto jedinečná, ani u nás nie je, niečo podobné máme v Podtúrni, aj v Prahe slávny Nuselský most.   Nechcem tu obhajovať koncepciu prieťahu diaľnice  mestom, tá už je v Považskej Bystrici faktom, na ktorý si treba len zvyknúť a tešiť sa na rýchle spojenie a upokojenie situácie v meste.   Do Považskej Bystrice nás priviedlo rodinné stretnutie. Toto mesto a okolie nemám veľmi preskúmané, tak som sa na návštevu tešil a nesklamala ma, bola to síce rodinná oslava, ale doplnená turistikou po okolí, presne podľa našich predstáv a možností.   V Považskej Bystrici som kedysi dávno oslávil svoje 33. výročie v príjemnom prostredí medzi priateľmi v Kolibe niekde na konci mesta. Aj také bývali akcie Hifiklubu. Súľovské skaly sme už dvakrát prešli, ale ešte som nebol na Považskom hrade.    Do mesta  sme vošli bez problémov, na sídlisko Rozkvet sa dá našťastie odbočiť krátko po zídení z nedokončenej diaľnice. Prvý výlet nás viedol do Javorníkov, na Česko - Slovenskú hranicu.   Z Považskej Bystrice sme šli v dvoch skupinách a keďže malý Rišo, náš oslávenec ešte potrebuje kočík, tak s ním išli cez Lazy pod Makytou až na parkovisko pri rekreačnom zariadení Kohútka. My sme išli cez Hornú Maríkovú do osady Vlkov k osade Stolečné a odtiaľ pomerne strmým výstupom k horskému hotelu Portáš.      Na Portáši sme sa stretli na prvé občerstvenie. Po hraničnej čiare vedie pohodlná cesta, horský hotel Portáš leží na Českej strane.      Obrovské parkovisko neďaleko rekreačného zariadenia Kohútka je prístupné z českej, aj zo slovenskej strany. Z našej strane je potrebné prekonať jeden úsek cesty vo veľmi zlom stave, podobne aj pri ceste k osade Stolečné.   Cestou hore, úzkou cestou lesom som si hovoril, že nedajbože stretnúť nákladiak naložený drevom a vtom sa objavil za najbližšou zákrutou. Všetko dobre dopadlo, ale vidieť nad sebou kolísajúcu sa horu dreva, ktovie ako zaistenú, je pre mňa vždy horor.      Na Kohútke majú kohúta, ktorý kikiríkaním ohlasuje, že kuchár má jedlo pripravené na servírovanie. Príjemné prostredie, na okolí množstvo zjazdoviek a vlekov, aj neďaleko chaty sa stavia nová lanovka z českej strany. Tak som prvýkrát spoznal pohodu na Slovensko - Českom pomedzí...   Poďme teraz na hrad...      Na Považský hrad sa stúpa z Považského Podhradia, na vlastné nebezpečie, lebo hradné múry nie sú dostatočne zabezpečené. Výstup je tiež pomerne strmý, ale relatívne krátky a stojí za to nielen hrad, ale aj tie výhľady do okolia a na novú diaľnicu.      Stavba diaľnice od Vrtižeru pokračuje na estakáde vedľa Hričovského kanála.      Diaľničný privádzač Vrtižer v detailnom priblížení, tam to zatiaľ končí. Zaujímavo o Považskom hrade píše Peter Martinisko na svojom blogu a má tam aj záber na túto križovatku z roku 2006. Predpovedá skorý zánik hradu, v tom dúfam, že nebude mať celkom pravdu.      Z hradu, nad Považským Podhradím, most cez kanál, za ním diaľnica.      Detail konštrukcie estakády.      O kúsok ďalej ďalší privádzač a diaľnica vstupuje nad mesto, na pylóny. Toľko dovidieť z hradu.      Záber z auta za jazdy, stračia nôžka...   Manínska tiesňava...      Prechádzame Manínskou tiesňavou.      Na lúky nad dedinou Záskalie.      Pohľad na málo známu Kostoleckú tiesňavu.      S krásnymi útvarmi skál.      Mohutný skalný dóm.    Dva dni v príjemnom rodinnom prostredí medzi horami,  s pekným počasím ako na objednávku sa skončili. Na spiatočnej ceste, pred Trenčínom nás chytil prívalový dážď, ale už nám nemohol pokaziť náladu... Stierače stierali, kolesá sa točili...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (30)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Vieme prví, len či pravdu
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            November
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Moja prvá fantastika
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Papierové mestá
                                        
                                    
                                 
                                                             
                                    
                                                                                                    

                                                                        
                                    
                                        Jozef Javurek 
                                        
                                            Spisovatelia v Smoleniciach
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Jozef Javurek
                        
                     
            
                     
                         Ďalšie články z rubriky cestovanie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Horná brána v Modre
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Martina Rúčková 
                                        
                                            Z Európy na Kaukaz: Çanakkale a bájna Trója
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Andrea Grešová 
                                        
                                            To je moje Rumunsko
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Serbák 
                                        
                                            Pamätník čs. armády s vojnovým cintorínom na Dukle
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Peter Kover 
                                        
                                            22 krajín (miest), ktoré sa naozaj oplatí vidieť
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky cestovanie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Jozef Javurek
            
         
        jozefjavurek.blog.sme.sk (rss)
         
                        VIP
                             
     
        Neoznačené fotografie na blogu výlučne z vlastnej tvorby (c). Inak je všetko vo hviezdach...
 
"štandardný už tradičný bloger, s istou mierou poctivosti" (jeden čitateľ)
 
 


Click for "Your New Digest 1".
Powered by RSS Feed Informer

počet návštev:







 
        
     
     
                     
                                                        
                        
                    
                                                        
                        
                    
                                                        
                        
                    
                             
             

     
        
            
                
                    Počet článkov
                    650
                
                
                    Celková karma
                    
                                                4.59
                    
                
                
                    Priemerná čítanosť
                    2950
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Slovensko
                        
                     
                                     
                        
                            Český raj
                        
                     
                                     
                        
                            Cestovanie
                        
                     
                                     
                        
                            Fotografie
                        
                     
                                     
                        
                            História
                        
                     
                                     
                        
                            Kultúra
                        
                     
                                     
                        
                            Poézia
                        
                     
                                     
                        
                            Próza
                        
                     
                                     
                        
                            Myšlienky, názory
                        
                     
                                     
                        
                            Blogovanie
                        
                     
                                     
                        
                            Technika a technológia
                        
                     
                                     
                        
                            Domácnosť
                        
                     
                                     
                        
                            Záhrada
                        
                     
                                     
                        
                            Zábava
                        
                     
                                     
                        
                            Zdravie
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            EÚ
                        
                     
                                     
                        
                            Hory, lesy
                        
                     
                             
         
    

    

        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




