
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Daniel Mikletič
                                        &gt;
                Nezaradené
                     
                 Politici nenávidia hudbu 

        
            
                                    26.5.2010
            o
            19:32
                        (upravené
                26.5.2010
                o
                19:44)
                        |
            Karma článku:
                4.14
            |
            Prečítané 
            1195-krát
                    
         
     
         
             

                 
                    Neviem, aký má ktorý politik vzťah k slovenskej hudbe - ale vyzerá to na nenávisť. Minister financií, hoc dodnes pôsobí dojmom odrastenejšieho punkera, nikdy nebol pristihnutý pri navyšovaní rozpočtových kapitol o podporu hudby, či kultúry. Príležitostný fujarista a prezident nestráca často slová o podpore hudby, čo i ľudovej. Minister kultúry sa skôr upäl na podporu jemu vlastnej filmárskej obce, hrdí sa audiovizuálnym fondom, no napríklad ním koncipovaná Zmluva so štátom a STV neumožňuje podporu programov s hudobnými dielami slovenských autorov. Nekorunovaná šéfka opozície raz ročne navštívi spriaznený megafestival, koalícia hor sa do Východnej - a to je tak asi všetko... Iní politici slovenskú hudbu ku šťastiu nepotrebujú...
                 

                      Iba ak raz štyri roky, keď sa schyľuje k voľbám, zrazu z pódií mítingov náhle slovenská hudba znie. Takí či onakí muzikanti rozohrievajú potencionálnych voličov v tričkách takých či onakých politických strán. Nemajte im to za zlé, robia len to, čo vedia a za čo im platia. Možno v kúte zvyškov duše dúfajú, že im to nejako po voľbách „vynahradia"... Zdá sa ale, že nie. Vo volebných programoch sa o podpore slovenskej kultúry píše len ledabolo, o hudbe vôbec nie. Jediné pozitívum je prísľub zrušenia diskriminačnej 2% dane autorov pre umelecké fondy, ktorú presadzujú strana Sloboda a Solidarita (SaS) a Únia - Strana pre Slovensko. Nebudem preto prekvapený ak ich väčšina autorov (hoci aj potajme, lebo čo keby...) podporí vo voľbách.   Vrátil som sa práve z Valného zhromaždenia Slovenského ochranného zväzu autorského (SOZA). Hŕstka slovenských autorov zastupovaných touto organizáciou kolektívnej správy práv k hudobným dielam si vypočula Ročnú správu a viac ako v sále diskutovala v hlúčkoch pri nej. Už pri zbežnom náhľade na zoskupených autorov možno povedať, že autori slovenských hudobných diel sú ohrozeným druhom. Nielen vekovým priemerom, ale aj ich statusom. Vypočul som si rôznorodé ponosy možno tucta z nich. Správa Výboru SOZA hovorila o poklese príjmov, prejavoch krízy, ktoré sa prejavujú znížením príjmov od vysielateľov a používateľov hudobných diel. Narastajú dlhy komerčných rádií, nedodržujú sa splátkové kalendáre, preťahujú sa súdne spory s neplatičmi, vyhovára sa na pokles príjmov od zadávateľov reklám. Organizuje sa čímďalej menej koncertov, ale aj firemných eventov, poriadatelia nenahlasujú použitie hudby (playlisty), nie sú ochotní platiť za niečo také - čo by podľa nich malo byť „zadarmo".   Popritom Správa SOZA hovorí aj o poklese hranosti diel slovenských autorov viac v súkromných rádiách, no i vo verejno-právnych. Regulátor, Rada pre vysielanie a retransmisiu (politkmi volený orgán) nemá dosť odvahy odobrať licenciu neplatičom, Rady Slovenskej televízie a Slovenského rozhlasu (takisto volené len politikmi) vôbec nesnívajú o vyššom podiele diel slovenských autorov vo vysielaní médií verejnej služby. V Rade STV sa sem-tam placho nejaký radný ozve, že na čo je nám ďalšia prezentácia československých hitov, no Liptákovci v manažmente STV si aj tak urobia po svojom. Presadiť do plánov Slovenskej televízie program postavený na slovenskej hudbe je neriešiteľný problém. Taký program nemá predsa oporu v Zmluve so štátom, to skôr prejde námet na telenovelu z fiktívneho života v ordináciách v ružových panelákoch.   Štát podporuje všelikoho, farmárov znížením DPH pri predaji z dvora, zahraničné automobilky „šrotovným", sociálne odkázaných napríklad v ich sociálnych podnikoch... Formálne aj neformálne diskutujúci autori na Valnom zhromaždení SOZA už dlhé roky nabádajú kdekoho, aby diela slovenských autorov dostali viac miesta. SOZA ako „plynomer" použitia hudby v tom nemá ako pomôcť. Žiadna profesná „odborová" organizácia autorov (akým bol kedysi Zväz autorov a interpretov - ZAI) nejestvuje. Niet autority, združenia, čo by mohlo vyvíjať tlak na legislatívu, poslancov, politikov tak, aby sa uzákonil nejaký Zákon na podporu slovenskej hudby, kde by boli definované povinné kvóty (percentá tak ako je to v iných krajinách EÚ) vysielania slovenských hudobných diel, či vytvorenia okruhu verejno-právneho Slovenského rozhlasu vysielajúceho len slovenskú hudbu. Slovenskí autori sú už viac než skeptickí, neveria, že by sa to dalo.   Možno mi budete oponovať a poviete: „slovenská hudba aj tak za nič nestojí!" V dobe empétrojiek, sťahovania z netu, stráca hudba hodnotu. Je jej všade veľa a zadarmo, nech si tí autori, muzikanti hrávajú len kdesi po kluboch, nech sú radi, že im kde-tu ktosi čosi zaplatí. Aj tak podľa vás berú niektoré hviezdy milióny (ako kedysi vravel šéf komunistov Jakeš). Boli by ste iste prekvapení, ktorí autori a za akú hudbu „berú" od SOZA najväčšie tantiémy... Ak sa však raz pristihnete, že si spievate, či počúvate niektorú slovenskú pieseň, trochu sa zahanbite.   Kolega textár, Boris Filan nedávno povedal, že: „žijeme v dobe, za ktorú sa raz budú naši vnuci hanbiť." Súhlasím s ním, aj keď to nepovedal v súvislosti so slovenskou hudbou. Musel som tieto riadky napísať, aby som sa za „túto dobu" a „tento stav" medzi slovenskými autormi hudobných diel nemusel hanbiť. A politici?, tí sa predsa hanbiť už dávno odnaučili...     

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (36)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Mikletič 
                                        
                                            Bushido nie je z ho*en, no neprepískol to?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Mikletič 
                                        
                                            Vláda istôt výtvarníkov
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Mikletič 
                                        
                                            Ako ďalej s „povojnovou“ STV?
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Mikletič 
                                        
                                            Bébé volá Hamšíkovi
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Daniel Mikletič 
                                        
                                            Kúpim stranu. Zn.: Fico
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Daniel Mikletič
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Daniel Mikletič
            
         
        mikletic.blog.sme.sk (rss)
         
                                     
     
        scenárista, textár, novinár, režisér, hudobný producent (nezávislý)

        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    44
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    2076
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Bushido nie je z ho*en, no neprepískol to?
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




