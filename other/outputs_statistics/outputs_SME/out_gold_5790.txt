

   
 Môj kamarát Milan má veľkú hlavu, na nej dve oči farby zmrzliny „hentej vedľa oríškovej". Dôstojné čelo a ustupujúce vlasy, na sluchoch mu vyrastajú šediny, o ktorých vraví, že to má len ufrckané od zubnej pasty. 
   
 Milan rád sedí v podnikovom bufete, obhrýza svoju menovku, ktorú zapíja fľaškou limonády a rozpráva príbehy o tom kde bol, čo zažil a čo vie. Najradšej vraví o globalizácii. Má rád žltú limonádu, občas pije aj zelenú, ktorú volá kiwi de tutti frutti. Milan je z dobrej rodiny a keď sa rozohní, každý štvrtok kupuje všetkým chlapom, ktorí fandili Barce žuvačky Pedro, potom beží domov a zahrá si FIFU. Ale občas tie žuvačky len ukradne. 
   
 Milanov humor zamrzol niekde medzi bejby karotkou a hlboko mrazenými kuracími prsiami. I tak má milú priateľku, ktorú volá životnou partnerkou a v súkromí jonatánkou, hoci je biela ako sušená  srvátka,  no pritom sladká ako langenharsove ostrovčeky. Často vraví o jeho nadčasovom humore a rozkošnej komike pri sŕkaní polievky. 
   
 Milan má rad sobotu, doobeda to dá na otočku do Győru na nákupy a večer si robí maslové pukance, ktoré volá butterfly pop corn a pozerá Segala. Potom spraví pätnásť klikov na pästiach, nafúkne sa zo dva razy do zrkadla a ide s jonatánkou drochmiť. 
 Nedeľa je futbalová, lúska tekvičné jadierka (dvojdecový pohár za dve evry kúpený na Starej seneckej), šupky pľuje ľuďom za golier. Po ňom vypije jedno kvasinkové pivo s nezmyselným názvom, aj keď je naň alergický, opitý sa vozí na kruhovom objazde v nákupnom košíku. 
 Pondelok nadáva na Expres, lebo ho namerali keď obiehal v dedine stodvadsať. Hrá sa na rockera, ale strašne rád si pustí Miley Cyrus a má svoju Ameriku a v aute party. 
   
   
 (podobnosť s akýmkoľvek Milanom vylúčená) 

