

 Tak po malej pauze (bolo potrebné trošku napísať aj obsah mojej vlastnej diplomovejpráce) bude dnes spoločne pokračovať s prácou s pravítkom, konktértne s tabulátormi a zarážkami tabulátora. 
 My sme si už povedali o tom, ako sa stlačenie tabulátora prejaví pri zobrazení netlačených znakov aj o jeho nevhodnom použití. Dnes sa teda dozviete, k čomu kláves TAB vo Worde naozaj slúži. 
 Takto to vidím bez zapnutia netlačených znakov: 
   
 Takýto pohľad sa mi naskytne pri ich zobrazení: 
   
 Medzi jednotlivými záznamami v riadkoch vidíme netlačený znak stlačenia tabulátora a v ukážke tri odseky. Akým spôsobom bol takýto zoznam vytvorený? 
 Riadok nadpisu (Prvý riadok ukážky) 
 Všetky ľavé zarážky sú pri ľavom okraji, pravá pri pravom okraji. Kurzor umiestnený na začiatku prvého odseku. Pri zobrazenom pravítku sa na ľavej strane vedľa horizontálneho pravítka (nad vertikálnom pravítku) nachádza tlačidlo na výber zarážky tabulátora . Klikaním ľavým tlačidlom myši na toto tlačidlo sa preklikávate medzi jednotlivými druhmi zarážok. 
 Bola zvolená ľavá zarážka tabulátora , ktorá zarovnáva text zľava doprava od tejto značky, a umiestnená (kliknutím ľavým tlačidlom myši) do pravítka na 1 cm . Pozor zarážku najprv zvolíte, potom uvoľníte tlačidlo myši, presuniete kurzor myši tak, aby hrot šípky bol v pravítku približne tam, kde vidíte v obrázku zarážku a kliknutím zvolenú zarážku umiestnite. 
 Potom bola zvolená stredná zarážka tabulátora , ktorá zarovnáva text na stred značky a rovnomerne ho rozmiestňuje vpravo a vľavo. Umiestnená bola do pravítka na približne šiesty a druhá na približne deviaty centimeter . 
 Ako posledná bola zvolená pravá zarážka tabulátora , ktorá zarovnáva text sprava doľava od tejto značky a umiestnená do pravítka na 15 cm . 
 Prvé stlačenie klávesu TAB znamenalo presun kurzora pod zarážku umiestnenú na 1 cm. A bol napísaný text Kraj. Druhé stlačenie klávesu TAB znamenalo presun kurzora pod zarážku umiestnenú na cca. 6 cm a bol napísaný text Podiel mužov, ktorý sa zarovnal na stred tejto značky. Tretie stlačenie klávesu TAB znamenalo presun kurzora pod zarážku umiestnenú na cca. 9 cm a podobne ako v predchádzajúcom prípade sa napísaný text Podiel žien zarovnal na stred tejto zarážky. Štvrté stlačenie klávesu TAB znamenalo posun k poslednej zarážke, bol napísaný text zdroj a text sa zarovnal doprava. Keďže ďalšie riadky zoznamu obsahujú iné údaje, ktoré sú inak formátované, odsek bol ukončený stlačením klávesu ENTER. 
   
 Vzhľadom na to, že zarážky tabulátora patria do formátovania odseku automaticky ostali pripravené v pravítku aj pre odsek nasledujúci. Pre nasledujúci odsek vyhovovala zarážka na 1 cm a na 15 cm. Nevyhovovali mu však zvyšné dve zarážky. Boli preto z pravítka odstránené ťahom myši (chytíte zarážku ľavým tlačidlom myši a ťaháte dolu von z pravítka, mimo pravítka pustíte tlačidlo myši). 
 Na pravítko pre nový odsek boli umiestnené ďalšie zarážky. Desatinná zarážka tabulátora na 5,5 cm a 9 cm. Pravá zarážka tabulátora na 6,5 cm a 10 cm. 
   
 Jedno stlačenie TAB, posun k prvej zarážke, napísanie textu Bratislavský. Druhé stlačenie TAB, posun k druhej zarážke, napísanie desatinného čísla 47,48 (desatinná čiarka sa zarovnala pod zarážku). Tretie stlačenie TAB, posun k tretej zarážke, napísanie znaku %. Štvrté stlačenie TAB, posun k štvrtej zarážke, napísanie desatinného čísla 52,52. Piate stlačenie TAB, posun k piatej zarážke, napísanie znaku %. Šieste stlačenie TAB, posun k šiestej zarážke, napísanie textu Štatistický úrad SR. Bolo potrebné prejsť do nového riadku, bez vytvorenia ďalšieho odseku, takže bol zalomený riadok Shift+Enter. Pokračovalo sa rovnakým spôsobom pri vytvorení obsahu druhého a tretieho riadku. Tretí riadok je zároveň posledným riadkom odseku, takže odsek bol ukončený ENTER. V nasledujúcom odseku už nebude potrebné zarážky používať, takže boli z pravítka ťahom myši (viď vyššie) odstránené. 
   
 Akú má výhodu, že celý zoznam (okrem záhlavia) bol pripravený ako jeden odsek? 
 Keď boli pridané do pravítka zarážky zvislej čiary  pre tento odsek (kurzor bol umiestnený v tomto odseku) na 4 cm, 8 cm a 11 cm, automaticky sa do celého odseku zvislé čiary umiestnili. 
 Ak by ste posúvali ktoroukoľvek zarážkou, už umiestnenou na pravítku (ťahom myši), automaticky by sa vám posúvali texty v každom riadku (výhodne, keď neviete odhadnúť, kde majú byť presne zarážky umiestnené) a tým by ste dosiahli efektívnejšie formátovanie. 
 Čo by sa stalo, keby nebol celý zoznam (okrem záhlavia) pripravený ako jeden odsek, ale každý riadok by tvoril samostatný odsek? 
 Pri každej zmene zarážok (ich pridávaní, odstraňovaní, posúvaní) by ste museli označovať všetky tri odseky, pretože inak by ste túto úpravu robili iba pre konkrétny riadok, odsek. 
 Dajú sa zarážky tabulátora umiestniť na pravítko aj iným spôsobom? 
 Áno, dajú. Umiestnite kurzor myši do odseku, pre ktorý chcete zarážky umiestniť. Vo verzii 2003 volíte Formát/Tabulátory. Vo verziách 2007-2010 karta Domov/Rozširujúci dialóg pre skupinu Odsek/Tlačidlo tabulátory. 
 Verzia 2007-2010: 
         
 Obe verzie: 
   
 Do Pozícia zarážky tabulátora (Tab stop position) napíšte požadované umiestnenie na pravítku napr. 1 cm. Zvoľte Zarovnanie (Alignment) napr. vľavo (Left). Zvoľte Vodiaci znak (Leader) napr. žiadny (None). Nastavte zarážku Nastaviť (Set). Môžete pokračovať ďalšou zarážkou napr. 15 cm, vpravo, bodkovaná čiara. 
   
 V pravom rohu dialógu si všimnite nastavenie štandardnej šírky tabulátora (o toľko sa posunie kurzor v texte po stlačení klávesu TAB, pokiaľ nie sú na pravítku pre daný odsek žiadne zarážky tabulátora). V dialógu môžete po vybratí jednotlivé zarážky vymazať, alebo odstrániť z pravítka všetky naraz Vymazať všetko (Clear All). Potvrďte OK. 
 Teraz môžete dostať takto formátovaný odsek (aj keď samozrejme obsah nebudeme vytvárať takýmto spôsobom): 
  
  

