

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
Prekvapili vás články o čiernych zoznamoch na mediálne.sk  a SPW?  
 
 
	 Na clankoch o blacklistoch ma prekvapilo len to, ze sa dostali medzi novinarov, teda von zrejme z interneho prostredia televizie. Ten konkretny zoznam, ktory je teraz medializovany ako excelovsky subor, som nikdy nevidel ani som o takom nevedel. Ja som mal len kratky mail so zoznamom firiem, ktore mozem oslovit. Mal som aj svoj vyber firiem a k nemu sa vyjadrilo obchodne oddelenie, alebo pan generalny riaditel. 
 
Máte tiež skúsenosti, že vedenie televízie usmerňovalo výber oslovených firiem či osôb podľa komerčných záujmov TA3? 
 
	Ano, z mojej skusenosti, pokial islo o publicisticke odborne relacie, tak vedenie televizie vyber respondentov / hosti do relacii casto usmernovalo.  
 
Pre TA3 ste okrem spravodajstva moderovali aj relácie Firmy a Peňaženka. Ich nosnou castou je rozhovor s hosťom, takmer vždy zástupcom komerčnej firmy. Kto vyberal týchto hostí, na základe akých kritérií? 
 
	 Relaciu Firmy som autorsky pripravoval spociatku sam a nakrucali sme ju v prostredi oslovenej firmy. Prinasali sme zabery priamo z vyroby, co relaciu robilo atraktivnou.  Oslovoval som firmy so zaujimavym pribehom, alebo silne a vyznamne firmy, ktore divak pozna a moze sa dozvediet zaujimave informacie. Dolezite bolo, aby sme vo firme mohli nakrucat a ziskat pekne obrazy a aby bola v Bratislave. Neskor sa format relacie zmenil, technicky a kapacitne nebolo mozne dalej vyrabat relaciu dovtedajsim sposobom. Nastupila studiova debata s hostom, zvacsa s najvyssim riaditelom firmy. Kontakty na oslovenie hosti som dostaval z obchodneho oddelenia, alebo od vedenia. Bol to kratky zoznam konkretnych firiem, ktore je alebo nie je mozne oslovit v zavislosti od spoluprace na reklamnej baze. Okrem vlastneho vyberu firiem som preto do relacie volal aj zastupcov firiem zo zoznamu, ktory bol priebezne aktualizovany.  Relacia Penazenka bola od zaciatku vedena formou studiovej diskusie na temy tykajuce sa financnych produktov, alebo sluzieb v oblasti financii. Spravidla boli v studiu dvaja hostia a spravidla to boli zastupcovia (odbornici na diskutovanu temu) z niekolkych velkych financnych spolocnosti. Vysloveny zakaz existoval na oslovovanie hosti zo spolocnosti Winterthur. 
 
Prečo ste z TA3 odišli? 
 
	Z televizie som odisiel pre vysoku mieru zbytocneho stresu zo strany priamych nadriadenych pri tvorbe spravodajskych sotov, ako aj pre coraz mensi podiel slobodnej tvorby v publicistickych ekonomickych relaciach. 
 
 

