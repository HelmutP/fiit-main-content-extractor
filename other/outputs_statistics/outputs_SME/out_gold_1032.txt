

 
V obklopení analógových
syntetizátorov, bez použitia akejkoľvek digitálnej techniky či počítačov zahral
skladby z legendárneho albumu Oxygene takmer presne tak, ako vyšli
v roku 1976 na LP platni. K tomu pridal aj dva kúsky z druhej
série Oxygene 7-13. 
 
Podľa slovenských médií bol
koncert vypredaný, ja mám však vážne podozrenie, že bol dokonca nadpredaný. Ľudia
nemali kde sedieť a tak obsadzovali každý voľný decimeter štvorcový
bratislavskej športovej haly. Aj napriek tomu, že som prišiel
v dostatočnom predstihu, som mal veľmi vážny problém dostať sa na moju
tribúnu. Keď som sa dožadoval uvoľnenia koridora, dostal som odporúčanie, aby
som sa vzniesol. To nehovorím o tom, že som nemal kde zaparkovať
a tak som auto odstavil rovno uprostred obrovskej mláky kdesi medzi
Kauflandom a Športovou halou. 
 
Jarre prišiel do Bratislavy vo
vynikajúcej forme. V sprievode troch hudobníkov ovládal naraz nástroje,
ktorých počet odhadujem na dvadsať až tridsať. Niektoré z nich vyzerali vskutku
kreatívne - pripomínali kríženca gitary a klávesov, či kusy oceľových
pásov, z ktorýho trčal elektrický kábel a vydávali zvuky v rôznych
frekvenciách nápadne sa podobajúce elektrickým výbojom. 
 
Jarre je podľa mňa zvukový
vynálezca. Pripadá mi to tak, ako keby celé dni sedel vo svojom štúdiu
a pokúšal sa do elektrickej zásuvky zapojiť všetko, čo mu padne do oka.
Inak si nemožno vysvetliť odkiaľ má všetky tie šumenia, vŕzgania
a kvílenia, ktoré ale neuveriteľným spôsobom ladia s akordmi,
melódiami a rytmom jeho skladieb. Niektoré nástroje zase vydávali zvuky
ako staré rádio, ktoré sa Jean Michel pokúšal počas koncertu naladiť na
neexistujúcu rozhlasovú stanicu. 
 
Ako uviedol Jarre na začiatku
koncertu „chcel som hrať v Československu, pretože prvé ohlasy na môj
album Oxygene prichádzali práve odtiaľto.“ Jarre vydaním tohto albumu
upozorňuje na zhoršujúce sa životné prostredie. Obal albumu je preto viac ako
výstižný: ľudská lebka vytŕčajúca spoza roztrhnutého zemského plášťa. Na
bratislavskom koncerte sme si mohli vďaka vizuálnej show vnútrom tejto lebky aj
virtuálne zacestovať. 
 
Mimochodom, sprievodné vizuálne
efekty koncertu neboli až také predimenzované ako som nadobudol pocit
z médií, ale veľmi príjemne dotvárali atmosféru koncertu. Tesne pred
legendárnou (a asi najznámejšou) skladbou Oxygene 4 sa nad hudobníkom spustili
obrovské zrkadlá, v ktorých bolo pohľadom zhora možné sledovať súhru
Jarreho prstov na všetkých tých klávesoch, zariadeniach a výrabačoch zvuku,
ktoré mal dôsledne logisticky rozmiestnené na javisku. 
 
Jarre zožal v Bratislave
obrovský aplaus a jeho „thank you, merci, ďakujem“ zaznelo snáď
tridsaťkrát. Ako prídavok zahral dve skladby: syntetizátorovú lahôdku Oxygene
13, ktorá sa obzvlášť vynímala svojou jednoduchosťou a príťažlivou
melódiou v ostrom kontraste so všetkými predchádzajúcimi experimentmi so
zvukovými  efektmi. Túto skladbu pri
intímnom osvetlení  Jarre akoby hral
súkromne pre každého účastníka koncertu zvlášť. Záver koncertu patril repríze
Oxygene 4, o ktorej si dovolím tvrdiť, že ju počúvajú aj na planétach
obiehajúcich hviezdu Sirius. 
 
Oxygene Tour bola nepochybne
nostalgia. Jarre určite nesplnil očakávania tým, ktorí očakávali Equinoxe,
Randez-Vous, Chronologie, Magnetic Fields či ďalšie. Dokázal však na jeho
starých analógových „Old Ladies“ vrátiť čas na koniec rokov sedemdesiatych kedy
elektronická hudba ešte pomaly iba snívala o svojom zrode a zahrať
prakticky celý Oxygene tak ako ho kedysi nahral. A najfascinujúcejšie na
tom je, že to všetko na koncerte dokázal skutočne naživo.
 

