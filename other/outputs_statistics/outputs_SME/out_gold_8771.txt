

 Hodnotiť výsledky je priskoro, možno akurát s úsmevom skonštatovať - idem od Vás :) No, teraz prichádza na rad rovnako významná udalosť ako voľby - rokovania o novej vláde. 
 Ako sa vraví karty sú rozdané. Kto však drží v rukách čierneho Petra? Jednoznačne KDH. A síce neprekonalo magických 10%, čo je možno pre babky demokratky smutné, ale má dosť percent nato, aby usmernilo misku váh tým správnym Smerom. 
 Z trpkého úsmevu, ktorý nedokázal prekryť ani zlatý mok možno tušiť Figeľovo sklamanie z výsledku. Pocitu akejsi letargie nepomohlo ani bratríčkovanie sa SDKÚ a SaS, či nemenej okatá radosť Bélu... KDH chce novú cestu, nie smer. Ale!!! Ak vezmeme v úvahu volebný výsledok, postoje ostatných pravicových strán nebolo by divu, ak by KDH zvažovalo predsa len nový smer namiesto tej novej cesty. 
 Neopomenuteľné sú i liberálne názory SaS, či rezervovaný postoj voči "svedomiu" zo strany SDKÚ. Myslím, že po 4 rokoch v opozícii KDH nechce byť tichý partner alebo len do počtu. 
 Fico je skvelý stratég a politik, preto určite vie, že jeho jediná šanca je KDH. Uvidíme však kadiaľ KDH pôjde ... (no asi každému je jasné kde dostane viac) 

