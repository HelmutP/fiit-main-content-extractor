
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Zuzana Roy
                                        &gt;
                Všetkým
                     
                 Páni novinári, čo takto vykašlať sa na Slotu a Belousovovú? 

        
            
                                    4.5.2010
            o
            11:34
                        (upravené
                4.5.2010
                o
                17:03)
                        |
            Karma článku:
                17.11
            |
            Prečítané 
            2548-krát
                    
         
     
         
             

                 
                    Myslíte si, že my, ako občania a občianky Slovenskej Republiky, by sme z hľadiska informačného, z hľadiska relevantného obsahu, ktorý krajinu kvalitatívne posúva vpred, z hľadiska hodnotového - teda z hľadiska vyššej kvality nášho života, našej každodennej slušnosti (po ktorej, ako sa zdá, mnohí tak prahnú) -  stratili tak veľa, keby ste sa na Slotove a Belousovovej tlačovky úplne vykašľali?
                 

                 
"Janko, myslíš, že dnes už konečne niekto príde?" 
   Pár otázok:   Načo sú nám ich jednoduché akože názory, ich primitivne - nič neriešiace - akože myšlienky?   Načo sa máme dívať na neovládajúci sa ksicht, ktorý nedokáže jasne artikulovať, ktorému klipkajú oči (raz jedno, raz druhé), vykrúcajú sa mu ústa - do prapodivných grimás, ktoré nás (opäť) privádzajú k závážnym domnienkam, že onen, jednoduchý Janko, je zas ožratý, že zas nedokáže ani len prísť tirezvy do práce - napr. do diskusnej relácie?   Načo máme počúvať tárajúceho úbožiaka, ktorý nám v 21. storočí ako smerodajné prednáša prosté, ľudsky a národne nezrelé politické fantázmagórie zdedené pravdepodobne po jeho starom otcovi?    A načo sa máme divať a počúvať verbálne agresívnu ženu, ktorá sa nedokáže ani len príjemne (žensky, ľudsky) usmiať tak, aby sme jej uverili, že jej úsmev je úsmevom človeka, ktorému na inom (človeku a národe) aj záleží?   Naozaj máte pocit, že my - ako občania, a vy - ako novinári sa nezaobídeme bez tejto smutnej komédie, že nám je potrebná, aby sme si na nej (cez ňu) formovali názor či nebodaj s niečím takým vstúpili do diskusie?   Naozaj si myslíte, že musíte utekať na každú ich - akože tlačovú besedu, ktorá nám je, ako už tradične, posledných 20 rokov - ale ÚPLNE NANIČ!   Prečo sa na nich nevykašlete?   Čo vám to dáva? Čo to dáva vašim čitateľom?   A kde je vaša novinárska (a ľudská) etika, vaša spoluzodpovednosť (prostredníctvom vašej práce) za stav tejto krajiny?   Prečo ich neignorujete, keď - ako ste nás vy sami často informovali - tieto politicky patologické typy v kultúrnom zahraniční v médiách miesta nemajú? Prečo im ho teda dávate vy? Že podporujete takto predajnosť, sledovanosť vášho média? OK, ale potom sa znovu vrátim k otázke: Kde je vaša novinárska (a ľudská) etika, vaša spoluzodpovednosť za mentálny, morálny stav tejto krajiny? Za jej ľudskú úroveň?       Prečo sa doteraz nestalo, že Nový Čas (lebo neviem, kto iný by to mal priniesť, publikovať), zatiaľ ani raz  nepriniesol fotografiu z tlačovej besedy so Slotom a Belousovovou, kde sú stoličky určené pre novinárov úplne prázdne (pravda okrem tých pre Nový Čas)?   Tak prečo?          "Nechodia a nechodia!  K***a, čo sa stalo?"         

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (78)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            O Zaujatí
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Situácia je fakt lepšia: Už sa len Klame, Kradne a Kupujú hlasy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Ako sme volili v porodnici alebo O "prave nevolit"
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Prochazka &amp; Knazko, alebo Nevolim zbabelcov!
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Zuzana Roy 
                                        
                                            Nemozem uz citat spravy zo Slovenska
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Zuzana Roy
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Zuzana Roy
            
         
        zuzanaroy.blog.sme.sk (rss)
         
                                     
     
         V posledných rokoch ma najviac oslovili myšlienky a knihy Anselma Grúna - nemeckého benediktína, a slovenských feministiek z ASPEKTu. Píšu o tom, kde nám to drhne (v živote) a ako z toho von. 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    152
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1606
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Foto: Waldorfská škola/škôlka
                        
                     
                                     
                        
                            Všetkým
                        
                     
                                     
                        
                            Verše bez veršov
                        
                     
                                     
                        
                            Letters to Animus
                        
                     
                                     
                        
                            Môj pes Dasty
                        
                     
                                     
                        
                            Budúci prezident - aký si?
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Alternatívne školy - Bratislavský kuriér
                                     
                                                                             
                                            Môj Prvý pokus - Démonizácia Waldorf. pedagogiky v slov.médiách
                                     
                                                                             
                                            Odkaz Sorena Kierkegaarda
                                     
                                                                             
                                            Tomáš Halík: O výchove a vzdelávaní
                                     
                                                                   
                     Čo práve počúvam 
                    
                     
                                                 
                                            Joni Mitchell - absolútna nádhera: Both Sides Now
                                     
                                                                             
                                            Anselm Grün: Zauber des Alltäglichen
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            Dr.Christiane Northrup - Womens Wisdom
                                     
                                                                             
                                            Sloboda Zvierat
                                     
                                                                             
                                            Anselm Grün
                                     
                                                                             
                                            Tomáš Halík
                                     
                                                                             
                                            Dominik Bugár - Fotograf, kolega - rodič
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       "Suma bola zaplatená pánovi dekanovi, ktorú pán dekan schoval do zásuvky"
                     
                                                         
                       David Deida - "Intimní splynutí - Cesta za hranice rovnováhy"..
                     
                                                         
                       All inclusive a piesok na zadku. Je toto dovolenka?
                     
                                                         
                       Moja Pohoda
                     
                                                         
                       Lož na kolesách: Pád Lancea Armstronga
                     
                                                         
                       Rasizmus po slovensky
                     
                                                         
                       Gentlemani na Wimbledone
                     
                                                         
                       Druhá šanca pre človeka
                     
                                                         
                       Facka Ficovi
                     
                                                         
                       Mantra národa.
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




