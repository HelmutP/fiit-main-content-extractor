

 Na prvom mieste by mohli byť peniaze. A určite aj sú. Druhá premenná je verejný záujem, ktorý v tomto prípade predstavuje diaľnica. Tretím, na ktorý nesmieme zabudnúť je lobing, a napokon na štvrtom mieste, ale určite nemenej dôležitý populizmus. 
 Prienikom vyššie uvedeného vzniká zaujímave teleso. Na jednej strane reči plné verejného záujmu, neuhasiteľná potreba stavať a rozvíjať za každú cenu. Na druhej strane občan a jeho peniaze. Pretože, ak si niekto myslí, že to za nás zaplatí niekto iný, nechajme ho tíško snívať. 
 Nikto z nás by nespochybňoval potrebu diaľnice - komfort pri cestovaní, rýchlosť, jednoduchá preprava tovarov. No neviem, ako by sme sa tvárili, keby za nami prišiel niekto a vypýtal si napríklad 30 eur na  diaľnicu... Ktovie, či by sme radšej trochu nepočkali a poriadne ušetrili. Avšak opäť pripomínam, to že nikto nestojí pred vchodom a nepýta 30 eur, to neznamená, že nám štát tie peniaze nevezme! 
 A ak som ešte pri diaľnicu, nezabúdajme na aktuálnu zákonnú úpravu jej výstavby. Nejdem čitateľov zbytočne zdržiavať, ale pre záujemcov 669/2007 Z.z. 
 Je jednoduché sa oháňať novou diaľnicou, ktorú stavia firma blízka mojej strane. Je jednoduché označiť tých, čo majú odlišný názor ako nebezpečných. Je jednoduché rozdávať z cudzieho... 
 Ak prienikom vzniká zaujímavé teleso, interakciou vyššie uvedeného vzniká pekné svinstvo. Kto si to vie spočítať si aj spočíta alebo to spočíta im? 
 Pretože, nakoniec to nebudú Po Prdeli Projekty, ale iba Poriadne Po Prdeli. 

