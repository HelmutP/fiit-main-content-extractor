
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Štefan Ráchela
                                        &gt;
                Fejtóny
                     
                 Momentka z našej dediny 

        
            
                                    26.5.2010
            o
            6:19
                        |
            Karma článku:
                7.82
            |
            Prečítané 
            1736-krát
                    
         
     
         
             

                 
                    V to horúce letné dopoludnie nebolo na uliciach južnoslovenskej dedinky ani človiečika. Všade vládol pokoj, dedina bola ako vymretá. Proste - uhorková sezóna, ktorá tu trvá po celý rok. Vtom sa z obecného rozhlasu ozval Robo Kazík a jeho “Vzdialená je moja láska, vzdialená...” Nezamestnaní nasávajúci si z pivových fliaš pod starou morušou pri bufete prestali nachvíľu nadávať na opozíciu, psy natiahnuté na podstení zastrihali ušami, sliepky zahrabané v popole zdvihli z neho hlavy a začudovane sa obzerali okolo seba. Pomaly zavŕzgali zhrdzavené pánty brán a občania začali vychádzať na ulicu. Postávali pred domami v očakávaní novôt. Po Kazíkovi nasledoval Daridó s rezkými maďarským čardášom, a potom už zaznel z rozhlasu oznam dvojjazyčne, v slovenčine i v maďarčine, že sa bude konať 23. riadne zasadnutie obecného zastupiteľstva a že na rokovanie sa môžu prísť pozrieť aj občania, pokiaľ majú záujem.
                 

                 - Aké dvadsiate tretie, keď ešte nebolo ani prvé? Čo sa deje, že sa starosta obťažuje? - čudoval sa Béla. - Však nech sa len žerú medzi sebou Slováci a Maďari, aspoň dajú pokoj nám, Cigánom. Dobre je, keď má drak aspoň dve hlavy, môžu sa navzájom žrať. Keď má drak len jednu hlavu, začne si obhrýzať vlastný chvost, - skonštatoval skepticky človek, ktorý videl gadžom - Maďarom i Slovákom, do žalúdka. Voľakedy sa aj pokúšal v dedine čosi pozitívne ovplyvniť, ale potom rezignoval, lebo zakaždým sa mu dostalo odpovede: - Cigán nás poučovať nebude! Kým na obecnom úrade úradníčka Gizka hlásila do miestneho rozhlasu, starosta ju niekoľkokrát potľapkal po zadku a s úľubou sa naň pozeral. Ona ním zakaždým zavrtela. Až keď jeho ruka-šmátralka zašla dole nižšie, zvýskla Gizka do mikrofónu a capia mu po drzej ruke. - Súdruh predseda, pán starosta, aspoň keď hlásim, by si mi mohol dať pokoj. Ako to vyzerá, keď zvýsknem do mikrofónu! Radšej sa chystaj na zastupiteľstvo! - rozčuľovala sa Gizka, keď skončila hlásenie. - Netreba sa mi chystať, však poslanci sú sprostí! - mávol rokou dedinský generalissimus. Po miestnom ozname zaznel z reproduktorov obecného rozhlasu ešte jeden Daridó a jeden Kazík a nad južnoslovenskou dedinkou zavládlo opäť lenivé, ospalé ticho, len spod starej moruše zneli nadávky na opozíciu prerušované grganím. Psy sa spokojne natiahli na podstienkach a sliepky sa zahrabali ešte hlbšie do popola.  P.S.: Všetky mená osôb (okrem mien spevákov) sú vymyslené, možná zhoda je len náhodná. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (1)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Malé porovnanie dvoch veľkých krajín
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Kampaň pred prezidentskými voľbami ukazuje slabiny pravice
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Ukrajina - nešťastná krajina
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Nad výsledkami župných volieb v jednom kraji a v jednom okrese
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Štefan Ráchela 
                                        
                                            Za tú našu slovenčinu
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Štefan Ráchela
                        
                     
            
                     
                         Ďalšie články z rubriky politika 
                         
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Tomáš Hudec 
                                        
                                            Chceme zvýšiť účasť ľudí vo voľbách? Zaveďme eVoľby!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Robert Bestro 
                                        
                                            Výmena vedenia parlamentu zjavne prospela
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Júlia Piraňa Mikolášiková 
                                        
                                            Obvinia ďalšiu novinárku z údajného ohovárania policajného šéfa Ševčíka?
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Stanislav Martinčko 
                                        
                                            Stužka z kvetinárstva za 16 centov a rozkradnuté milióny!!
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Veronika Remisova 
                                        
                                            Elektronické mýto podľa Ficovej vlády – prehľadnejšie sa to skrátka nedá
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky politika
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 20:22
             Slovenky v hlavnej skupine nebodovali, Švédky vyhrali o 9 gólov 
             Naše hádzanárky hrali na ME piaty zápas. V Záhrebe proti Švédkam. 
         
     
         
        
                                

        
         
            AKTUALIZOVANÉ O 20:00
             Rubeľ pocítil pád na dno, Rusko je vo finančnej kríze 
             Dolár včera stál už takmer 80 rubľov, pod hladinu ho tlačia najmä nízke ceny ropy. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zaútočil na citlivé miesto Pakistanu. Na deti 
             Taliban zabil v škole v Pakistane najmenej 141 ľudí, väčšinou deti. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Štefan Ráchela
            
         
        rachela.blog.sme.sk (rss)
         
                                     
     
        Skeptik s kritickým myslením.
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    60
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    1197
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fejtóny
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

    

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Kampaň pred prezidentskými voľbami ukazuje slabiny pravice
                     
                                                         
                       Július píše Oľge
                     
                                                         
                       Daňové licencie alebo škola podnikania hrou
                     
                                                         
                       Ficov veleodvážny sľub
                     
                                                         
                       Výhodná ročná percentuálna miera nákladov: 59140.89%
                     
                                                         
                       Vďaka, herr Kotleba, za búranie mýtov o tejto krajine
                     
                                                         
                       Hnedý kôň volieb a úbožiak Fico
                     
                                                         
                       Kto za to môže?
                     
                                                         
                       Extrémizmus, moc a politická paradigma spoločnosti
                     
                                                         
                       Volíme menšie zlo, len aby sme v hanbe neostali. Už zasa
                     
                             
         
        

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




