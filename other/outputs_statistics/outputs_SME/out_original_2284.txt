
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Gabriel Šípoš
                                        &gt;
                Nezaradené
                     
                 Aj Počiatek rád zavádza o indexe Transparency 

        
            
                                    15.2.2010
            o
            18:03
                        (upravené
                15.2.2010
                o
                21:10)
                        |
            Karma článku:
                13.49
            |
            Prečítané 
            11850-krát
                    
         
     
         
             

                 
                    Vládni predstavitelia stále nevedia zareagovať na index naznačujúci nárast korupcie u nás inak ako výmyslami
                 

                 
Čo tak mať celosvetovú mapu korupcie v každom štátnom úrade namiesto prezidenta?transparency.org
   Potom, čo sa lacný útok na najznámejší svetový index korupcie CPI od Transparency International minulý víkend nevydaril predsedovi vlády a ministrovi vnútra, skúšal v piatkovej diskusii na TA3 šťastie aj minister financií Ján Počiatek. Opäť neúspešne.   Počiatek o indexe povedal:   ...teraz na margo toho, čo tu prezentuje opozícia ako dogmu alebo písmo sväté, nejaký prieskum Transparency International.... to je nejaký kumulatívny index viacerých subindexov... napríklad jeden z tých indexov je index, ktorý monitoruje mediálne pokrytie káuz, čiže akým spôsobom alebo koľko káuz bolo zmedializovaných. ...ak je súčasťou toho kompozitu, čo neviem teda zistiť aká je váha toho subindexu, ktorý hovorí "as reported in media", čiže koľko toho bolo napísané v mediách, ťažko na toto reagovať... Hovorím, že súčasťou toho s veľkou váhou je nejaký subindex, ktorý hovorí, že ako boli médiami popísané škandály alebo nejaké korupčné škandály.   CPI je počítaný priemerom z 8 veľkých prieskumov, ako som už písal. Jeden z týchto 8 prieskumov, od americkej mimovládky Freedom House, sa odvoláva okrem iného aj na médiá:   Extent of corruption as practiced in governments, as perceived by the public and as reported in the media, as well as the implementation of anticorruption initiatives   Odtiaľ pochádza aj ten Počiatkov citát „as reported in the media“. Už tu vidno, že je to len časť hodnotenia. Zoznam všetkých otázok je prístupný na stránke Transparency. Samotní hodnotitelia tohto jediného prieskumu však odpovedajú spolu na 10 otázok, pričom o vnímaní médií je jedna jediná, č.9:   1. Has the government implemented executive anticorruption initiatives? 2. Is the country’s economy free of excessive state involvement? 3. Is the government free from excessive bureaucratic regulations, registration requirements, and other controls that increase opportunities for corruption? 4. Are there signifcant limitations on the participation of government officials in economic life? 5. Are there adequate laws requiring financial disclosure and disallowing confict of interest? 6. Does the government advertise jobs and contracts? 7. Does the state enforce an executive legislative or administrative process— particularly one that is free of prejudice against one’s political opponents— to prevent, investigate, and prosecute the corruption of government officials and civil servants? 8. Do whistle-blowers, anticorruption activists, investigators, and journalists enjoy legal protections that make them feel secure about reporting cases of bribery and corruption? 9. Are allegations of corruption given wide and extensive airing in the media? 10. Does the public display a high intolerance for official corruption?   Takže – priamo o médiách je jedna z desiatich otázok jedného z ôsmich prieskumov, ktoré spolu tvoria index Transparency. Tiež si všimnite, že Počiatek najprv povie, že netuší, akú váhu majú tie médiá, nakoniec aj tak bohorovne povie, že ide o subindex s "veľkou váhou."   Počiatek tiež tvrdil, že lepší je prieskum verejnej mienky Eurostatu:   ...je to štatistické vzorkovanie na normálnej vzorke obyvateľstva, kde sa pýtajú, že aké % ľudí alebo koľkí sa stretli s korupciu priamo. Čiže nie je taká akože fikcia, ale pýtajú sa konkrétnych ľudí a štatisticky zisťujú koľkí z nich sa stretli s korupciou za posledný rok..    Zaujímavé. CPI je robený každý rok prieskumom z polovice medzi podnikateľmi a z polovice medzi miestnymi a zahraničnými expertami. Eurostat to robí raz za dva roky medzi širokou populáciou (tradičná vzorka tisíc ľudí). Kto už asi viac môže byť ovplyvnený tými zaujatými médiami, tí prví, podnikatelia, alebo tí druhí, bežní ľudia? (doplnené - otázka Eurostatu totiž meria nielen či niekto od respondenta pýtal, ale aj očakával úplatok). A najmä, kto má šancu viac vedieť o tendroch, o Interblue, mýte, emisiách? Manažéri firiem a analytici alebo bežný ľud od Skalice po Sninu?? Počiatek si nevybral prieskum Eurostatu ako dôveryhodnejší kvôli lepšej metodológii, ale len kvôli pre neho prijateľnejšiemu výsledku.   Minister financií v relácii tiež spomenul, že skladba indexu Transparency nie je transparentná. Má pravdu v tom, že nemôžeme uverejniť dáta pre jednotlivé subindexy, aby bolo napríklad jasné, ktoré jednotlivé prieskumy najviac ťahajú výsledok hore či dole. Dôvodom je to, že viaceré tie subindexy ich samotní autori (IMD, EIU, IHS) predávajú komerčne, a teda dávajú ich ústrediu Transparency pod podmienkou nezverejnenia. Inak, starostlivosť o transparentnosť mi pripadá celkom dojemná od ministra, ktorý pri prezentácii rozpočtu odmietol na priamu otázku novinárov povedať taký banálny údaj, že aké je očakávané zadĺženie krajiny v bežnom roku, a dokonca také číslo rezort vyškrtol aj zo samotného návrhu rozpočtu.   PS Michal Lehuta dobre zanalyzoval zavádzanie Počiatka aj o náraste nezamestnanosti.  PPS Pre otvorenosť - v piatkovej debate na TA3 bol Počiatkovým oponentom Eugen Jurzyca, môj bývalý šéf v INEKO, teraz kandidát SDKÚ. Od októbra minulého roku pracujem v Transparency International Slovensko.   Tichučké joj o Jasovskom: Šéf NKÚ Ján Jasovský bol na tom J&amp;T turnaji konského póla v Tatrách. Ako šéf úradu, ktorý kontroluje hospodárenie so štátnym majetkom, sa tak vystavil vážnym podozreniam, že ku kontrole mnohých obchodov štátu s  J&amp;T nebude pristupovať dostatočne dôsledne. Jeho konflikt záujmov bude riešiť parlament. TV Joj, ktorú J&amp;T vlastní, zareagovala na túto kauze klasicky – minimalizovaním informácií pre svojich divákov. Prípad z televízií prvá preberala Markíza ešte minulý pondelok v celej reportáži. Potom, čo parlamentný výbor schválil prešetrovanie Jasovského, spravila plnohodnotný príspevok aj STV a krátku čítanú informáciu TA3 (bývalým vlastníkom bola J&amp;T).  A čo TV Joj? Mlčala až do štvrtka, keď Jana Krescanko-Dibáková utrúsila na tému jednu vetu:   Vladimír Mečiar dnes nielen pred koaličnými partnermi bránil celou váhou svojej osobnosti aj šéfa NKÚ Jasovského, ktorý sa do Tatier prišiel pozrieť na pólo.    Takže nič o tom, že to ide vyšetrovať parlamentný výbor, nič o podstate, prečo je to vlastne problém, a na rozdiel od všetkých televizií, nulová zmienka o J&amp;T. Inak, koľko divákov Joj vie, čo to je NKÚ?  PS Ako ma upozornil čitateľ, STV bola minulý týždeň jediná televízia, ktorá divákom v hlavných správach nepovedala, že aj 5 poslancov SNS hlasovalo za novelu vysokoškolského zákona, ktorú potom vedenie SNS tak hlasno kritizovalo.    Skrytá reklama na lyžiarske stredisko?: Čitateľovi sa príspevky D.Kleinovej z Markízy o rakúskom lyžiarskom regióne Paznaun nápadne podobali na reklamu. Najmä ten sobotňajší:   Do rakúskeho alpského regiónu Paznaun prichádzajú turisti najmä za dobrou lyžovačkou. Výzva je veľká. 4 strediská a nekončiace sa kilometre tratí a zjazdoviek. Na centrálnych miestach to vyzerá ako na rušnej križovatke lyžiarov a vlekov. No potom sa takmer 20 tisíc lyžiarov v úvodzovkách stratí na rôznych svahoch. Ak by sa vám stovky tratí na rakúskej strane málilo, nevadí. Stačí prejsť kopec a ste vo Švajčiarsku. Tam na vás čakajú ďalšie stovky upravených zjazdoviek. Tieto strediská sa nemali problém spojiť so Švajčiarskom. Len v Silvaparku majú 40 najmodernejších vlekov.  Andreas STEIBL, Turistický zväz Paznaun - Ischgl -------------------- Pre turistov je to veľká výhoda. Nie je žiaden problém. Na jednu permanentku lyžujete po celom stredisku.   Danica KLEINOVÁ, redaktorka -------------------- Postupne začínajú Paznaun objavovať aj Slováci. Ročne ich tu príde okolo 80 tisíc.   Jaroslava PRIBLINCOVÁ, zástupca Österreich Werbung na SR -------------------- Slováci milujú Rakúsko, predovšetkým lyžovačky a majú radi servis, ktorý tu dostanú....   A práve toto stredisko ponúka najnovšie v osobitnej kolonke pre novinárov aj oficiálna rakúska agentúra pre podporu návštevnosti krajiny Österreich Werbung.   A jej zástupkyňu reportáž cituje. Podľa šéfredaktora spravodajstva Markízy Lukáša Dika nešlo o nič nekalé:   Spravodajstvo TV Markíza využíva ponuky agentúr na zahraničné cesty len veľmi výnimočne, a to v prípadoch, keď ich považujeme za dostatočne zaujímavé pre nášho diváka.   V tomto konkrétnom prípade sme využili aktuálnu ponuku na cestu. Po reportážach zo slovenských lyžiarskych stredísk, ktoré sme doteraz vysielali, sme chceli pre porovnanie ukázať fungovanie zahraničného strediska. Náklady hradí TV Markíza. 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (56)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Najcitovanejšími analytikmi v roku 2010 boli J. Baránek a V. Vaňo
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Druhý najcitovanejší politológ STV mal problémy s plagiátorstvom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            .týždeň a J&amp;T majú spoločné obchodné záujmy
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Slovenské médiá sú vnímané ako najmenej skorumpované vo V4
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Gabriel Šípoš 
                                        
                                            Ako sa názormi na Wikileaks vyfarbujú komentátori
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Gabriel Šípoš
                        
                     
            
                     
                         Ďalšie články z rubriky médiá 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Ján Dulovič 
                                        
                                            Zabi kapra, zachrániš strom
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Radovan Bránik 
                                        
                                            Úžasná hra pre sociálne siete: nájdi svojho tajného agenta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Filip Letovanec 
                                        
                                            O nepravdivosti titulkov a konci sveta
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Linka Detskej Istoty 
                                        
                                            Známe osobnosti, punč, príbeh a pozvanie
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Marek Duda 
                                        
                                            Výroky na úrovni "televíznych novín" - zabite ma pre môj názor
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky médiá
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Gabriel Šípoš
            
         
        spw.blog.sme.sk (rss)
         
                        VIP
                             
     
        Pracujem v Transparency International Slovensko. Slovak Press Watch bol projektom

 
        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    663
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    5880
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené články 
                    
                     
                                                 
                                            Výročná správa o stave médií za rok 2008
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2007
                                     
                                                                             
                                            Kto vlastní slovenské médiá
                                     
                                                                             
                                            Výročná správa o stave médií za rok 2006
                                     
                                                                             
                                            Inflačná kalkulačka 1993-2011
                                     
                                                                             
                                            O Slovak Press Watch
                                     
                                                                   
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog slovenskej Transparency
                                     
                                                                             
                                            Blogy eTRENDU
                                     
                                                                             
                                            Romenesko - US media news
                                     
                                                                             
                                            Medialne.sk - slovenské media news
                                     
                                                                             
                                            Archív SPW 2002-04
                                     
                                                                   
                     Obľúbené stránky 
                    
                     
                                                 
                                            ProPublica
                                     
                                                                             
                                            Demagog.SK
                                     
                                                                             
                                            NY Times Magazine
                                     
                                                                             
                                            The New Yorker
                                     
                                                                             
                                            American Journalism Review
                                     
                                                                             
                                            Columbia Journalism Review
                                     
                            
             
         
            

             
             Čo ma zaujalo 
            Týmto článkom bloger klikol/klikla na karmu.
             
                                                         
                       Študenti žurnalistiky, mám pre vás niekoľko tém na diplomovku
                     
                                                         
                       Prišiel čas skladania účtov
                     
                                                         
                       Interrupcie, znásilnenie a spoločenská diskusia
                     
                                                         
                       Kto ešte nespáchal bombový atentát, nech sa prihlási u Gusta Murína
                     
                             
         
           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




