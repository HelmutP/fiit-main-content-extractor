
 
  Na poplatkoch a províziách z úverov zarobili za rok 2009 slovenské banky 117 455 000 eur, čo je takmer 322-tisíc eur denne. Všetky čísla, ktoré tu uvádzam, čerpám zo štatistiky Národnej banky Slovenska, výkazov ziskov a strát bánk.  
 Čistý zisk bánk, pôsobiacich na slovenskom trhu, bol za minulý rok takmer 279 MILIÓNOV EUR, po starom OSEM MILIÁRD korún. 
 A to vo svete aj na Slovensku zúrila kríza. Tento rok tiež zatiaľ nevyzerá pre banky zle.  
 Za január a február 2010 priznali banky čistý zisk  82 828 000 eur, po starom takmer 2,5 miliardy korún.  
 Je dobré, že naše banky sa netopia v problémoch ako niektoré ich zahraničné matky a hospodária v čiernych číslach. Rozdiely v poplatkovej politike jednotlivých bánk sú ale obrovské. Napríklad rozdiel medzi aktuálne najvýhodnejšou a najnevýhodnejšou hypotékou na Slovensku je 15 percent, čo pri 30-ročnom horizonte splácania predstavuje sumu, za ktorú sa dá napríklad už kúpiť  slušné auto strednej triedy. A to pre bežného človeka nie je žiadna maličkosť.  
 Poplatkový vankúš slovenských bánk je ešte dosť mäkký a nafúknutý na to, aby bolo z čoho uberať. V prospech nás, bežných ľudí a klientov. Je najvyšší čas bankám ukázať, kde sú ich hranice. Porovnať si jednotlivé produkty a vybrať si najvýhodnejší, a nie ten, ktorý má najchytľavejšiu a najmasovejšiu reklamu. Ide predsa o naše peniaze. Neviem ako vy, ale ja si svoju peňaženku dosť vážim. Nepatrím k tým, ktorí donekonečna len nadávajú, ako nás banky zdierajú na poplatkoch. Ja sa otočím chrbtom  tam, kde mám pocit, že platím priveľa alebo platím za niečo, čo nedosahuje kvalitu. 
 
