
      
    
    

     
	 
	 
 SME.sk 
 Zľavy 
 Zoznámenie 
 Reality 
 Dovolenky 
 Pizza 
 Reštaurácie 
 Recenzie 
 Inzeráty 
 Naničmama 	 
Nákupy 
		 
 Filmy na DVD 
 Hodinky 
 Parfémy 
 SME knihy a DVD 
 Superhosting 
 Tlačiareň 
 Víno 
 Tvorba webov 
 Predplatné SME 
 ProSenior 		 
	 
	 
 
	 
 


    
    
        
        
         
          
         
    

    
      
         
            
             
     
        
        
     
    Blog.SME.sk
 

    

         
        
                 
     
         
            Sme.sk
         
         
            Domov
         
         
            Regióny
         
         
            Ekonomika
         
         
            Svet
         
         
            Komentáre
         
         
            Kultúra
         
         
            Šport
         
         
            TV
         
         
            Auto
         
         
            Tech
         
         
            Poradňa
         
         
            Žena
         
         
            Bývanie
         
         
            Zdravie
         
         
            Blog
         
     
 

         
            
     
                                                         Politika 
                                                                                     Spoločnosť 
                                                                                     Cestovanie 
                                                                                     Ekonomika 
                                                                                     Fotografie 
                                                                                     Jedlo 
                                                                                     Kultúra 
                                                                                             
Ďalšie
                             
                                                                                         Lifestyle 
                                                                                                                                                                             Médiá 
                                                                                                                                                                             Nezaradené 
                                                                                                                                                                             Súkromné 
                                                                                                                                                                             Šport 
                                                                                                                                                                             Veda a technika 
                                                                                                                                                                             Zábava 
                                                                                                                                                                             Poézia 
                                                                                                                                                                             Próza 
                                                                                                                                                                             Kódex blogera 
                                                                                 
                         
                                                 
            Blogeri
         
     


            

    
    Prihlásiť sa


    
    Založte si blog



    
    Odhlásiť sa


    
    




         

        
        
         
            
         
         
                
     
                 
            SME.sk &gt;
            Blog.SME.sk &gt;
                            Stano Orolin
                                        &gt;
                Z môjho herbára
                     
                 Lúka lieči (ďatelina proti hnačke?) 

        
            
                                    9.6.2005
            o
            23:09
                        |
            Karma článku:
                10.33
            |
            Prečítané 
            12977-krát
                    
         
     
         
             

                 
                       Z môjho fotoherbára dnes ponúkam ďalšie  rastliny, ktoré hojne rastú na lúkach. Pri prechádze v prírode si môžme všimnúť ďatelinu lúčnu (Trifolium pratense L.), ktorú tiež pestujú poľnohospodári ako krmovinu. Pre liečivé účely sa zbierajú červené kvetné hlávky. Trhajú sa hneď po rozkvitnutí bez podporných lístkov. Rýchlo ich treba usušiť, aby nestratili pôvabnú farbu. Ďatelinové hlávky obsahujú farbivá, triesloviny, silicu a živicu. Oddávna bola používaná ako prísada do aromatických kúpeľov na kožnú dezinfekciu a čajov  proti hnačkám, pri liečbe bronchitídy a cukrovky.
                 

                  
 Ďatelina lúčna 
  
    Ďatelina plazivá (Trifolium repens L.). Túto vytrvalú rastlinu nájdete tiež na lúkach, pastvinách i na okrajoch ciest. Kvitne od mája do novembra. Zvyčajne je pestovaná v zmesi s inými ďatelinami alebo travami. Je to významná medonosná rastlina. Z jedného hektára sa dá získať až 100 kg medu. V liečiteľstve má ďatelina plazivá podobné účinky na ďatelina lúčna. Nuž, ak najbližšie zavítate do prírody, nezabudnite si natrhať pár kvetov ďateliny. Môžu sa vám zísť. 
  
  
 Ďatelina plazivá. 
  
  
 Snímky: Stano Orolin 
 
   
 
 
 
 
 
 
 
 

                 
                                        
                        
                        Zdieľať
                    

                    
                            
                                    
                                    Zdieľať na
                            
                             
                                    
                             
                    

                                                                                                                        
                            
                            Diskusia: (13)
                        
                    
                    
                        
                        Zvýšte karmu
                    
                 
             

                             
            
                     
                         Ďalšie články blogera 
                         
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stano Orolin 
                                        
                                            Trochu žltej z mojej záhrady
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stano Orolin 
                                        
                                            Jahoda
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stano Orolin 
                                        
                                            Jednoducho mucha (supermakro)
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stano Orolin 
                                        
                                            Potôčik (ne)zadržíš ani fotoaparátom
                                        
                                    
                                 
                                                             
                                    
                    	                                                                                        

                                                                        
                                    
                                        Stano Orolin 
                                        
                                            Reťaze bez farby (foto)
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články blogera: Stano Orolin
                        
                     
            
                     
                         Ďalšie články z rubriky fotografie 
                         
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Miroslav Lisinovič 
                                        
                                            Motorový rušeň 754.045 ČD "Brejlovec"
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jan Pražák 
                                        
                                            Jeníček v zemi divů - Lurdy
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Anton Lisinovič 
                                        
                                            Detaily života v lese.....
                                        
                                    
                                 
                                                             
                                    
                                                                                                                    

                                                                                                
                                    
                                        Jaromír Šiša 
                                        
                                            Porazit Rusy? Šlo to jedině roku 1805 u Slavkova.
                                        
                                    
                                 
                                                             
                                    
                                                            	                                                                                        

                                                                                                
                                    
                                        Gabriela Szabová 
                                        
                                            Nezabudnuteľní
                                        
                                    
                                 
                                                     
                        
                            Ďalšie články z rubriky fotografie
                        
                     
                 
            
                    
 
     Hlavné správy 
         
        
                                

        
         
            AKTUALIZOVANÉ 16:00
             Rubeľ kolabuje, ruská vláda má mimoriadne rokovať 
             Jeden dolár stál v utorok poobede už viac ako 78 rubľov, razantné zvýšenie úrokov nepomohlo. 
         
     
         
        
                                

        
         
            ZAHRANIČIE
             Taliban zabil v škole v Pakistane vyše sto ľudí, väčšinou deti 
             Militanti vtrhli do vojenskej školy v policajných rovnošatách. 
         
     
         
        
                                

        
         
            KOMENTARE.SME.SK
             Cynická obluda: Medzičasom na opustenom ostrove 
             Stroskotanci majú veľké šťastie, že sú dvaja. Lacné vtipy na úrovni ich autora. 
         
     
     

                 
     
    
     
        
 

     
                
                            
                    



         
            
                Stano Orolin
            
         
        orolin.blog.sme.sk (rss)
         
                        VIP
                             
     
        Kdesi som čítal, že internet je najlepšie miesto, kde sa dá stopercentne klamať. Tak neverte mojim slovám ani fotografiám.

Snímka portrétu: Fero Demský 


        
     
     
                     
                             
             

     
        
            
                
                    Počet článkov
                    79
                
                
                    Celková karma
                    
                                                0.00
                    
                
                
                    Priemerná čítanosť
                    4382
                
            
        
     

             

             Zoznam rubrík 
            
             
                                     
                        
                            Fotografia
                        
                     
                                     
                        
                            Z môjho herbára
                        
                     
                                     
                        
                            Záhrada
                        
                     
                                     
                        
                            Súkromné
                        
                     
                                     
                        
                            Šport
                        
                     
                                     
                        
                            Nezaradené
                        
                     
                             
         
    

                     
                        
                                                            
                     Obľúbené blogy 
                    
                     
                                                 
                                            Blog - Milan Barlog
                                     
                                                                             
                                            Blog - Ľubo Mercery Pecho
                                     
                                                                             
                                            Blog - Jozef Javurek
                                     
                                                                             
                                            Blog - Bohuš M. Rosinský
                                     
                                                                             
                                            Blog - Miro Veselý
                                     
                                                                             
                                            Mesto Michalovce
                                     
                                                                             
                                            Blog - Peter Bigoš
                                     
                                                                             
                                            Moja osobná stránka
                                     
                            
             
         
            

           
     Pošlite odkaz blogerovi 
    
         
            
         

         
         
         
         

         
         
Odosielateľ (e-mail)
 
         
Text
 
         
             
Protispamová ochrana 
 
 Zle. Skúste znova. 
Opíšte slová z obrázku: Načítať iné slová 
 Get an image CAPTCHA 
 
 
         
                 
            
         
    
  


 

     






            
    
        
    
 

        
         
         
             Už ste čítali? 
         
         
 

 kultura.sme.sk 
 Margaréta sa spriahla s diablom, no mala Kristovu pravdu 
 
 

 www.sme.sk 
 Pútnické miesta The Beatles alebo ako ľudia kráčajú po zebre 
 
 

 cestovanie.sme.sk 
 Lovia za svitania a v hmle 
 
 

 www.sme.sk 
 Ako sa valila lavína slobody. Ako padal komunizmus 
 
 

 kultura.sme.sk 
 Žiadne retro, poriadny biznis. Takto sa lisuje platňa 
 
	   
 


        
         
            
         

     

    
     
 
 
	 

		Kontakty
		Predplatné
		Etický kódex
		Pomoc
		Mapa stránky
		
	 
 
 
	 
		 
			Widget
			RSS
			Newsletter
		 
		 
			Mobil
			Smartphone
			Kindle
			Televízor
		 
		 
			Facebook
			Twitter
			Google+
		 
	 
	 
		
	 
	 
		Ďalšie weby skupiny: Prihlásenie do Post.sk
		Új Szó Slovak Spectator
		Agentúrne správy 
		Vydavateľstvo
		Inzercia
		Osobné údaje
		Návštevnosť webu
		Predajnosť tlače
		Petit Academy
		SME v škole 		© Copyright 1997-2014 Petit Press, a.s. 
	 
 
 
 


          
        
            
            

        
    

    
    


    


    
        
        
        
        
        
        
        
            
            
        
        
        
        
         
                                
               





 

 



        

 




